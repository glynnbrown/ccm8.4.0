﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800

// V8-26421 : N.Foster
//  Created
// V8-25543 : L.Ineson
//  Added properties for applied Orientation types
// V8-27738 : N.Foster
// Refactored as a POCO for performance 
// V8-27773 : A.Kuszyk
//  Added closest divider positions to TotalSpace calculations.
// V8-27946 : A.Kuszyk
//  Removed dividers from TotalSpace calculations.
//  Added GetReservedSpace method.
// V8-28256 : A.Kuszyk
//  Amended GetReservedSpace to return RectValue with adjusted coordinates.
// V8-28015 : L.Ineson
//  Added TraySize property so that tray size can be calculated from unit sizes when tray
//  dimensions are 0.
// V8-27814 : A.Kuszyk
//  Updated CalculateBlockSize to take into account product nesting sizes.

#endregion
#region Version History : CCM801

// V8-25529 : A.Kuszyk
//  Ensured finger space is always added to the main block total size when there is an X block on the right.
// V8-28828 : A.Kuszyk
//  Updated the way in which the total slot space is calculated in GetSpaceChangeForDividers.
// V8-28560 : A.Kuszyk
//  Corrected issue with calculated nested units in CalculateBlockSize().

#endregion
#region Version History : CCM802

// V8-29032 : L.Ineson
//  Added additional properties and CalculateMinimumBlockSizes method for use 
//  when applying product squeeze.

#endregion
#region Version History : CCM810

// V8-30107 : J.Pickup
//  There was a precision floating point issue in calculateblocksize(). I have added rounding to resolve this and seems to be ok.
// V8-30103 : N.Foster
//  Autofill performance enhancements

#endregion
#region Version History : CCM811

// V8-30406 : A.Silva
//  Added GetBlockRotatedUnitSize to obtain the correct block rotated unit size given its axis.

#endregion
#region Version History : CCM820
// V8-30818 : A.Kuszyk
//  Re-factored GetReservedSpace into from PlanogramPositionPlacement.
// V8-30859 : N.Foster
//  Fixed infinite loop issue when product is nested and orientated
// V8-31212 : M.Brumby
//  Pegged positions are now more aware of orientations
// V8-31544 : D.Pleasance, A.Silva
//  Amended CalculateBlockSize() so that no rounding is applied, because rounding was only being applied to the main block so when we came to validating 
//  values from other presentation styles (X,Y,Z) that get calulated here we would have issues because of the loss of precision. 
#endregion
#region Version History : CCM830
//V8-31702 : L.Ineson
//  Amended total units calculation for trays and cases.
// V8-31907 : L.Ineson
//  Exposed rotated nesting sizes
//  Added GetAnticipatedBlock size to help autofill
//  X,Y & Z block rotated unit sizes are now calculated even if the block is not placed.
// V8-32460 : M.Brumby
//  Peg products that are an exact multiple of the peg spacing value should have a spacing of 0
//  instead of the full peg spacing value. This is to match other spaceplanning products.
// V8-31981 : L.Ineson
//  Added helpers for getting the actual pegx and pegy values to use.
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Provides calculated details about a position
    /// when placed on a sub component taking into
    /// account factors such as orientation and
    /// merchandising style
    /// </summary>
    [Serializable]
    public class PlanogramPositionDetails
    {
        #region Fields

        private PlanogramProductMerchandisingStyle _mainMerchStyle;
        private PlanogramProductOrientationType _mainOrientationType;
        private PointValue _mainCoordinates;
        private RotationValue _mainRotation;
        private WidthHeightDepthValue _mainUnitSize;
        private WidthHeightDepthValue _mainRotatedUnitSize;
        private WidthHeightDepthValue _mainTotalSize;
        private WidthHeightDepthValue _mainMinimumSize;
        private WidthHeightDepthValue _mainRotatedMaxSqueeze;
        private WidthHeightDepthValue _mainRotatedNestingSize;

        private Boolean _hasXBlock;
        private PlanogramProductMerchandisingStyle _xMerchStyle;
        private PlanogramProductOrientationType _xOrientationType;
        private PointValue _xCoordinates;
        private RotationValue _xRotation;
        private WidthHeightDepthValue _xUnitSize;
        private WidthHeightDepthValue _xRotatedUnitSize;
        private WidthHeightDepthValue _xTotalSize;
        private WidthHeightDepthValue _xMinimumSize;
        private WidthHeightDepthValue _xRotatedMaxSqueeze;
        private WidthHeightDepthValue _xRotatedNestingSize;

        private Boolean _hasYBlock;
        private PlanogramProductMerchandisingStyle _yMerchStyle;
        private PlanogramProductOrientationType _yOrientationType;
        private PointValue _yCoordinates;
        private RotationValue _yRotation;
        private WidthHeightDepthValue _yUnitSize;
        private WidthHeightDepthValue _yRotatedUnitSize;
        private WidthHeightDepthValue _yTotalSize;
        private WidthHeightDepthValue _yMinimumSize;
        private WidthHeightDepthValue _yRotatedMaxSqueeze;
        private WidthHeightDepthValue _yRotatedNestingSize;

        private Boolean _hasZBlock;
        private PlanogramProductMerchandisingStyle _zMerchStyle;
        private PlanogramProductOrientationType _zOrientationType;
        private PointValue _zCoordinates;
        private RotationValue _zRotation;
        private WidthHeightDepthValue _zUnitSize;
        private WidthHeightDepthValue _zRotatedUnitSize;
        private WidthHeightDepthValue _zTotalSize;
        private WidthHeightDepthValue _zMinimumSize;
        private WidthHeightDepthValue _zRotatedMaxSqueeze;
        private WidthHeightDepthValue _zRotatedNestingSize;


        private WidthHeightDepthValue _facingSize;
        private WidthHeightDepthValue _traySize;
        private WidthHeightDepthValue _totalSize;
        private WidthHeightDepthValue _minimumSize;
        private WideHighDeepValue _totalUnits;
        private Int32 _totalUnitCount;
        private WidthHeightDepthValue _totalSpace;

        #endregion

        #region Properties

        #region Main Block

        #region MainMerchStyle

        /// <summary>
        /// Returns the merchandising style for the Main position block
        /// </summary>
        public PlanogramProductMerchandisingStyle MainMerchStyle
        {
            get { return _mainMerchStyle; }
        }

        #endregion

        #region MainOrientationType

        /// <summary>
        /// Returns the orientation type for the Main position block
        /// </summary>
        public PlanogramProductOrientationType MainOrientationType
        {
            get { return _mainOrientationType; }
        }

        #endregion

        #region MainCoordinates

        /// <summary>
        /// Returns the coordinates of the main position block
        /// </summary>
        public PointValue MainCoordinates
        {
            get { return _mainCoordinates; }
        }

        #endregion

        #region MainRotation

        /// <summary>
        /// Returns the rotation of the main position block
        /// </summary>
        public RotationValue MainRotation
        {
            get { return _mainRotation; }
        }

        #endregion

        #region MainUnitSize

        /// <summary>
        /// Returns the unrotated unit size for the main position block
        /// </summary>
        public WidthHeightDepthValue MainUnitSize
        {
            get { return _mainUnitSize; }
        }

        #endregion

        #region MainRotatedUnitSize

        /// <summary>
        /// Returns the rotated unit size for the main position block
        /// </summary>
        public WidthHeightDepthValue MainRotatedUnitSize
        {
            get { return _mainRotatedUnitSize; }
        }

        #endregion

        #region MainTotalSize

        /// <summary>
        /// Returns the total size for the main position block
        /// </summary>
        public WidthHeightDepthValue MainTotalSize
        {
            get { return _mainTotalSize; }
        }

        #endregion

        #region MainMinimumSize

        /// <summary>
        /// Returns the miniumum size that the main position block must occupy
        /// with products fully squeezed.
        /// </summary>
        public WidthHeightDepthValue MainMinimumSize
        {
            get { return _mainMinimumSize; }
        }

        #endregion

        #region MainRotatedMaxSqueeze

        /// <summary>
        /// Returns the maximum squeeze percentages for the units 
        /// of the main position block, rotated to the correct orientation.
        /// </summary>
        public WidthHeightDepthValue MainRotatedMaxSqueeze
        {
            get { return _mainRotatedMaxSqueeze; }
        }

        #endregion

        #region MainRotatedNestingSize

        /// <summary>
        ///  Returns the rotated nesting size for the main position block
        /// </summary>
        public WidthHeightDepthValue MainRotatedNestingSize
        {
            get { return _mainRotatedNestingSize; }
        }

        #endregion

        #endregion

        #region X Block

        #region HasXBlock

        /// <summary>
        /// Indicates if the position has an x block
        /// </summary>
        public Boolean HasXBlock
        {
            get { return _hasXBlock; }
        }

        #endregion

        #region XMerchStyle

        /// <summary>
        /// Returns the merchandising style for the x position block
        /// </summary>
        public PlanogramProductMerchandisingStyle XMerchStyle
        {
            get { return _xMerchStyle; }
        }

        #endregion

        #region XOrientationType

        /// <summary>
        /// Returns the orientation type for the X position block
        /// </summary>
        public PlanogramProductOrientationType XOrientationType
        {
            get { return _xOrientationType; }
        }

        #endregion

        #region XCoordinates

        /// <summary>
        /// Returns the coordinates of the x position block
        /// </summary>
        public PointValue XCoordinates
        {
            get { return _xCoordinates; }
        }

        #endregion

        #region XRotation

        /// <summary>
        /// Returns the rotation of the x position block
        /// </summary>
        public RotationValue XRotation
        {
            get { return _xRotation; }
        }

        #endregion

        #region XUnitSize

        /// <summary>
        /// Returns the unrotated unit size for the x position block
        /// </summary>
        public WidthHeightDepthValue XUnitSize
        {
            get { return _xUnitSize; }
        }

        #endregion

        #region XRotatedUnitSize

        /// <summary>
        /// Returns the rotated unit size for the x position block
        /// </summary>
        public WidthHeightDepthValue XRotatedUnitSize
        {
            get { return _xRotatedUnitSize; }
        }

        #endregion

        #region XTotalSize

        /// <summary>
        /// Returns the total size for the x position block
        /// </summary>
        public WidthHeightDepthValue XTotalSize
        {
            get { return _xTotalSize; }
        }

        #endregion

        #region XMinimumSize

        /// <summary>
        /// Returns the miniumum size that the X position block must occupy
        /// with products fully squeezed.
        /// </summary>
        public WidthHeightDepthValue XMinimumSize
        {
            get { return _xMinimumSize; }
        }

        #endregion

        #region XRotatedMaxSqueeze

        /// <summary>
        /// Returns the maximum squeeze percentages for the units 
        /// of the X position block, rotated to the correct orientation.
        /// </summary>
        public WidthHeightDepthValue XRotatedMaxSqueeze
        {
            get { return _xRotatedMaxSqueeze; }
        }

        #endregion

        #region XRotatedNestingSize

        /// <summary>
        ///  Returns the rotated nesting size for the x position block
        /// </summary>
        public WidthHeightDepthValue XRotatedNestingSize
        {
            get { return _xRotatedNestingSize; }
        }

        #endregion

        #endregion

        #region Y Block

        #region HasYBlock

        /// <summary>
        /// Indicates if the position has an y block
        /// </summary>
        public Boolean HasYBlock
        {
            get { return _hasYBlock; }
        }

        #endregion

        #region YMerchStyle

        /// <summary>
        /// Returns the merchandising style for the y position block
        /// </summary>
        public PlanogramProductMerchandisingStyle YMerchStyle
        {
            get { return _yMerchStyle; }
        }

        #endregion

        #region YOrientationType

        /// <summary>
        /// Returns the orientation type for the Y position block
        /// </summary>
        public PlanogramProductOrientationType YOrientationType
        {
            get { return _yOrientationType; }
        }

        #endregion

        #region YCoordinates

        /// <summary>
        /// Returns the coordinates of the y position block
        /// </summary>
        public PointValue YCoordinates
        {
            get { return _yCoordinates; }
        }

        #endregion

        #region YRotation

        /// <summary>
        /// Returns the rotation of the y position block
        /// </summary>
        public RotationValue YRotation
        {
            get { return _yRotation; }
        }

        #endregion

        #region YUnitSize

        /// <summary>
        /// Returns the unrotated unit size for the y position block
        /// </summary>
        public WidthHeightDepthValue YUnitSize
        {
            get { return _yUnitSize; }
        }

        #endregion

        #region YRotatedUnitSize

        /// <summary>
        /// Returns the rotated unit size for the y position block
        /// </summary>
        public WidthHeightDepthValue YRotatedUnitSize
        {
            get { return _yRotatedUnitSize; }
        }

        #endregion

        #region YTotalSize

        /// <summary>
        /// Returns the total size for the y position block
        /// </summary>
        public WidthHeightDepthValue YTotalSize
        {
            get { return _yTotalSize; }
        }

        #endregion

        #region YMinimumSize

        /// <summary>
        /// Returns the miniumum size that the Y position block must occupy
        /// with products fully squeezed.
        /// </summary>
        public WidthHeightDepthValue YMinimumSize
        {
            get { return _yMinimumSize; }
        }

        #endregion

        #region YRotatedMaxSqueeze

        /// <summary>
        /// Returns the maximum squeeze percentages for the units 
        /// of the Y position block, rotated to the correct orientation.
        /// </summary>
        public WidthHeightDepthValue YRotatedMaxSqueeze
        {
            get { return _yRotatedMaxSqueeze; }
        }

        #endregion

        #region YRotatedNestingSize

        /// <summary>
        ///  Returns the rotated nesting size for the y position block
        /// </summary>
        public WidthHeightDepthValue YRotatedNestingSize
        {
            get { return _yRotatedNestingSize; }
        }

        #endregion

        #endregion

        #region Z Block

        #region HasZBlock

        /// <summary>
        /// Indicates if the position has an z block
        /// </summary>
        public Boolean HasZBlock
        {
            get { return _hasZBlock; }
        }

        #endregion

        #region ZMerchStyle

        /// <summary>
        /// Returns the merchandising style for the z position block
        /// </summary>
        public PlanogramProductMerchandisingStyle ZMerchStyle
        {
            get { return _zMerchStyle; }
        }

        #endregion

        #region ZOrientationType

        /// <summary>
        /// Returns the orientation type for the Z position block
        /// </summary>
        public PlanogramProductOrientationType ZOrientationType
        {
            get { return _zOrientationType; }
        }

        #endregion

        #region ZCoordinates

        /// <summary>
        /// Returns the coordinates of the z position block
        /// </summary>
        public PointValue ZCoordinates
        {
            get { return _zCoordinates; }
        }

        #endregion

        #region ZRotation

        /// <summary>
        /// Returns the rotation of the z position block
        /// </summary>
        public RotationValue ZRotation
        {
            get { return _zRotation; }
        }

        #endregion

        #region ZUnitSize

        /// <summary>
        /// Returns the unrotated unit size for the z position block
        /// </summary>
        public WidthHeightDepthValue ZUnitSize
        {
            get { return _zUnitSize; }
        }

        #endregion

        #region ZRotatedUnitSize

        /// <summary>
        /// Returns the rotated unit size for the z position block
        /// </summary>
        public WidthHeightDepthValue ZRotatedUnitSize
        {
            get { return _zRotatedUnitSize; }
        }

        #endregion

        #region ZTotalSize

        /// <summary>
        /// Returns the total size for the z position block
        /// </summary>
        public WidthHeightDepthValue ZTotalSize
        {
            get { return _zTotalSize; }
        }

        #endregion

        #region ZMinimumSize

        /// <summary>
        /// Returns the miniumum size that the Z position block must occupy
        /// with products fully squeezed.
        /// </summary>
        public WidthHeightDepthValue ZMinimumSize
        {
            get { return _zMinimumSize; }
        }

        #endregion

        #region ZRotatedMaxSqueeze

        /// <summary>
        /// Returns the maximum squeeze percentages for the units 
        /// of the Z position block, rotated to the correct orientation.
        /// </summary>
        public WidthHeightDepthValue ZRotatedMaxSqueeze
        {
            get { return _zRotatedMaxSqueeze; }
        }

        #endregion

        #region ZRotatedNestingSize

        /// <summary>
        ///  Returns the rotated nesting size for the z position block
        /// </summary>
        public WidthHeightDepthValue ZRotatedNestingSize
        {
            get { return _zRotatedNestingSize; }
        }

        #endregion

        #endregion

        #region General

        #region FacingSize

        /// <summary>
        /// Returns the facing size dimensions, which represent the space between
        /// each facing, used by finger space, dividers and peg hole spacings.
        /// </summary>
        public WidthHeightDepthValue FacingSize
        {
            get { return _facingSize; }
        }

        #endregion

        #region TraySize

        /// <summary>
        /// Returns the unorientated size of a single tray.
        /// This is calculated from the unit size if the product tray dimensions have
        /// not been populated.
        /// </summary>
        private WidthHeightDepthValue TraySize
        {
            get { return _traySize; }
        }

        #endregion

        #region TotalSize

        /// <summary>
        /// Returns the total size for the position, taking into finger spacing and divider spacing 
        /// between facings and white space between facings due to peg holes. TotalSize represents
        /// the bounding box of the facings inside the position only, and does not take into account
        /// any additional space that the position may occupy or use.
        /// </summary>
        public WidthHeightDepthValue TotalSize
        {
            get { return _totalSize; }
        }

        #endregion

        #region TotalUnits

        /// <summary>
        /// Returns the total units for this position
        /// </summary>
        public WideHighDeepValue TotalUnits
        {
            get { return _totalUnits; }
        }

        #endregion

        #region TotalUnitCount

        /// <summary>
        /// Returns the total unit count for this position
        /// </summary>
        public Int32 TotalUnitCount
        {
            get { return _totalUnitCount; }
        }

        #endregion

        #region TotalSpace

        /// <summary>
        /// Returns the total size of the position (that is, the total space occupied by all of the facings
        /// and the gaps in between them), plus the finger space surrounding the outside of the position.
        /// TotalSpace simple represents the total size of the position plus it's bounding finger space,
        /// and does not take into account any additional space. For additional space occupied, use the
        /// <see cref="PlanogramPositionPlacement.GetReservedSpace()"/> method.
        /// </summary>
        public WidthHeightDepthValue TotalSpace
        {
            get { return _totalSpace; }
        }

        #endregion

        #region MinimumSize

        ///<summary>
        ///Returns the minimum size for the position with maximum squeeze applied, taking into finger spacing and divider spacing 
        /// between facings and white space between facings due to peg holes. MinimumSize represents
        /// the bounding box of the facings inside the position only, and does not take into account
        /// any additional space that the position may occupy or use.
        /// </summary>
        public WidthHeightDepthValue MinimumSize
        {
            get { return _minimumSize; }
        }

        #endregion

        #endregion

        #endregion

        #region Constructors

        private PlanogramPositionDetails()
        {
        } // force use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionDetails NewPlanogramPositionDetails(
            PlanogramPosition position,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramPositionDetails item = new PlanogramPositionDetails();
            item.Create(position, product, subComponentPlacement, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionDetails NewPlanogramPositionDetails(
            PlanogramPosition position,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            PlanogramPositionDetails item = new PlanogramPositionDetails();
            item.Create(position, product, subComponentPlacement, merchandisingGroup);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionDetails NewPlanogramPositionDetails(
            PlanogramPositionPlacement positionPlacement)
        {
            PlanogramPositionDetails item = new PlanogramPositionDetails();
            item.Create(positionPlacement.Position, positionPlacement.Product, positionPlacement.SubComponentPlacement,
                positionPlacement.MerchandisingGroup);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            PlanogramPosition position,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            this.Initialize(position, product);
            this.CalculateRotatedUnitSizes(position, product);
            this.CalculateFacingSpace(position, product, subComponentPlacement, merchandisingGroup);
            this.CalculateBlockSize(position, product);
            this.CalculateCoordinates(position, product);
            this.CalculateTotalUnits(position, product);
            this.CalculateMinimumBlockSizes(position, product);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the position details
        /// </summary>
        private void Initialize(PlanogramPosition position, PlanogramProduct product)
        {
            #region Main Block

            _mainMerchStyle = position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main);
            _mainOrientationType = position.GetAppliedOrientationType(product, PlanogramPositionBlockType.Main);
            WidthHeightDepthValue unorientatedSqueeze = ProductOrientationHelper.GetUnorientatedSize(this.MainOrientationType,
                position.HorizontalSqueeze, position.VerticalSqueeze, position.DepthSqueeze);
            _mainUnitSize = product.GetUnorientatedUnitSize(this.MainMerchStyle, unorientatedSqueeze.Height,
                unorientatedSqueeze.Width, unorientatedSqueeze.Depth);

            #endregion

            #region X Block

            _xMerchStyle = position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.X);
            _xOrientationType = position.GetAppliedOrientationType(product, PlanogramPositionBlockType.X);
            unorientatedSqueeze = ProductOrientationHelper.GetUnorientatedSize(this.XOrientationType, position.HorizontalSqueezeX,
                position.VerticalSqueezeX, position.DepthSqueezeX);
            _xUnitSize = product.GetUnorientatedUnitSize(this.XMerchStyle, unorientatedSqueeze.Height,
                unorientatedSqueeze.Width, unorientatedSqueeze.Depth);

            #endregion

            #region Y Block

            _yMerchStyle = position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Y);
            _yOrientationType = position.GetAppliedOrientationType(product, PlanogramPositionBlockType.Y);
            unorientatedSqueeze = ProductOrientationHelper.GetUnorientatedSize(this.YOrientationType, position.HorizontalSqueezeY,
                position.VerticalSqueezeY, position.DepthSqueezeY);
            _yUnitSize = product.GetUnorientatedUnitSize(this.YMerchStyle, unorientatedSqueeze.Height,
                unorientatedSqueeze.Width, unorientatedSqueeze.Depth);

            #endregion

            #region Z Block

            _zMerchStyle = position.GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Z);
            _zOrientationType = position.GetAppliedOrientationType(product, PlanogramPositionBlockType.Z);
            unorientatedSqueeze = ProductOrientationHelper.GetUnorientatedSize(this.ZOrientationType, position.HorizontalSqueezeZ,
                position.VerticalSqueezeZ, position.DepthSqueezeZ);
            _zUnitSize = product.GetUnorientatedUnitSize(this.ZMerchStyle, unorientatedSqueeze.Height,
                unorientatedSqueeze.Width, unorientatedSqueeze.Depth);

            #endregion

            _traySize = product.GetUnorientatedTraySize();
        }

        /// <summary>
        /// Calculates the facing space for this position
        /// </summary>
        private void CalculateFacingSpace(PlanogramPosition position, PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            // We first need to check if the subcomponent is part of a merchandising group as we may need to take 
            // values from the first sub in the set rather than the one the position actually sits on.
            PlanogramSubComponent settingsSubComponent = merchandisingGroup == null
                ? subComponentPlacement.GetCombinedWithList().First().SubComponent
                : merchandisingGroup.SubComponentPlacements.First().SubComponent;

            #region X Block

            Single spaceX = product.FingerSpaceToTheSide;

            if (settingsSubComponent.IsDividerObstructionByFacing)
            {
                spaceX += settingsSubComponent.DividerObstructionWidth;
            }

            if (settingsSubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                Int32? merchConstraintRow = position.GetPegRowNum(this.MainRotatedUnitSize.Height, 
                    product, settingsSubComponent, subComponentPlacement.Planogram.LengthUnitsOfMeasure);
                    
                if (merchConstraintRow.HasValue)
                {
                    Single rowSpacingX = (merchConstraintRow == 1)
                        ? settingsSubComponent.MerchConstraintRow1SpacingX
                        : settingsSubComponent.MerchConstraintRow2SpacingX;

                    Single width = (this.MainRotatedUnitSize.Width + spaceX);
                    if (rowSpacingX > 0 && (width % rowSpacingX) > 0)
                    {
                        spaceX = (rowSpacingX - (width % rowSpacingX));
                    }
                }
            }

            #endregion

            #region Y Block

            Single spaceY = 0;
            if (settingsSubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                spaceY += product.FingerSpaceAbove;
                
                Int32? merchConstraintRow = 
                    position.GetPegRowNum(this.MainRotatedUnitSize.Height, product, settingsSubComponent, 
                    subComponentPlacement.Planogram.LengthUnitsOfMeasure);

                if (merchConstraintRow.HasValue)
                {
                    Single rowSpacingY = (merchConstraintRow == 1)
                        ? settingsSubComponent.MerchConstraintRow1SpacingY
                        : settingsSubComponent.MerchConstraintRow2SpacingY;

                    if (rowSpacingY > 0 && this.MainRotatedUnitSize.Height % rowSpacingY > 0)
                    {
                        spaceY += (rowSpacingY - (this.MainRotatedUnitSize.Height % rowSpacingY));
                    }
                }
            }

            #endregion

            #region Z Block

            // TODO
            Single spaceZ = 0;

            #endregion

            // set the facing space
            _facingSize = new WidthHeightDepthValue(spaceX, spaceY, spaceZ);
        }

        /// <summary>
        /// Calculates the size of each rotated unit
        /// </summary>
        private void CalculateRotatedUnitSizes(PlanogramPosition position, PlanogramProduct product)
        {
            #region Main Block

            _mainRotation = ProductOrientationHelper.GetRotation(this.MainOrientationType);

            if (this.MainMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                _mainRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(
                    this.MainOrientationType,
                    this.TraySize.Width.GreaterThan(0) ? this.TraySize.Width : (Math.Max((Byte)1, product.TrayWide) * product.Width),
                    this.TraySize.Height.GreaterThan(0) ? this.TraySize.Height : (Math.Max((Byte)1, product.TrayHigh) * product.Height),
                    this.TraySize.Depth.GreaterThan(0) ? this.TraySize.Depth : (Math.Max((Byte)1, product.TrayDeep) * product.Depth)
                    );
            }
            else
            {
                _mainRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(this.MainOrientationType,
                    this.MainUnitSize.Width, this.MainUnitSize.Height, this.MainUnitSize.Depth);
            }

            #endregion

            #region X Block

            WideHighDeepValue xUnits = new WideHighDeepValue(position.FacingsXWide, position.FacingsXHigh,
                position.FacingsXDeep);

            _xRotation = ProductOrientationHelper.GetRotation(this.XOrientationType);

            if (this.XMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                _xRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(
                        this.XOrientationType,
                        this.TraySize.Width.GreaterThan(0) ? this.TraySize.Width : (Math.Max((Byte)1, product.TrayWide) * product.Width),
                        this.TraySize.Height.GreaterThan(0) ? this.TraySize.Height : (Math.Max((Byte)1, product.TrayHigh) * product.Height),
                        this.TraySize.Depth.GreaterThan(0) ? this.TraySize.Depth : (Math.Max((Byte)1, product.TrayDeep) * product.Depth)
                        );
            }
            else
            {
                _xRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(this.XOrientationType,
                    this.XUnitSize.Width, this.XUnitSize.Height, this.XUnitSize.Depth);
            }

            if (xUnits.High > 0 && xUnits.Wide > 0 && xUnits.Deep > 0)
            {
                _hasXBlock = true;
            }
            //else
            //{
            //    _xRotation = new RotationValue();
            //    _xRotatedUnitSize = new WidthHeightDepthValue();
            //    _xTotalSize = new WidthHeightDepthValue();
            //}

            #endregion

            #region Y Block

            WideHighDeepValue yUnits = new WideHighDeepValue(position.FacingsYWide, position.FacingsYHigh,
                position.FacingsYDeep);

            _yRotation = ProductOrientationHelper.GetRotation(this.YOrientationType);

            if (this.YMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                _yRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(
                         this.YOrientationType,
                         this.TraySize.Width.GreaterThan(0) ? this.TraySize.Width : (Math.Max((Byte)1, product.TrayWide) * product.Width),
                         this.TraySize.Height.GreaterThan(0) ? this.TraySize.Height : (Math.Max((Byte)1, product.TrayHigh) * product.Height),
                         this.TraySize.Depth.GreaterThan(0) ? this.TraySize.Depth : (Math.Max((Byte)1, product.TrayDeep) * product.Depth)
                         );
            }
            else
            {
                _yRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(this.YOrientationType, 
                    this.YUnitSize.Width, this.YUnitSize.Height, this.YUnitSize.Depth);
            }


            if (yUnits.High > 0 && yUnits.Wide > 0 && yUnits.Deep > 0)
            {
                _hasYBlock = true;
            }
            //else
            //{
            //    //_yRotation = new RotationValue();
            //    //_yRotatedUnitSize = new WidthHeightDepthValue();
            //    _yTotalSize = new WidthHeightDepthValue();
            //}

            #endregion

            #region Z Block

            WideHighDeepValue zUnits = new WideHighDeepValue(position.FacingsZWide, position.FacingsZHigh, position.FacingsZDeep);

            _zRotation = ProductOrientationHelper.GetRotation(this.ZOrientationType);

            if (this.ZMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                _zRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(
                        this.ZOrientationType,
                        this.TraySize.Width.GreaterThan(0) ? this.TraySize.Width : (Math.Max((Byte)1, product.TrayWide) * product.Width),
                        this.TraySize.Height.GreaterThan(0) ? this.TraySize.Height : (Math.Max((Byte)1, product.TrayHigh) * product.Height),
                        this.TraySize.Depth.GreaterThan(0) ? this.TraySize.Depth : (Math.Max((Byte)1, product.TrayDeep) * product.Depth)
                        );
            }
            else
            {
                _zRotatedUnitSize = ProductOrientationHelper.GetOrientatedSize(this.ZOrientationType, 
                    this.ZUnitSize.Width, this.ZUnitSize.Height, this.ZUnitSize.Depth);
            }

            if (zUnits.High > 0 && zUnits.Wide > 0 && zUnits.Deep > 0)
            {
                _hasZBlock = true;
                
            }
            //else
            //{
            //    //_zRotation = new RotationValue();
            //    //_zRotatedUnitSize = new WidthHeightDepthValue();
            //    _zTotalSize = new WidthHeightDepthValue();
            //}

            #endregion
        }

        /// <summary>
        /// Calculates the size of each position block
        /// </summary>
        private void CalculateBlockSize(PlanogramPosition position, PlanogramProduct product)
        {
            #region Main Block

            WideHighDeepValue mainUnits = new WideHighDeepValue(position.FacingsWide, position.FacingsHigh, position.FacingsDeep);
           
            // Calculate the size of the block. This depends on whether or not the products are nested
            // so we calculate each dimension independently.
            _mainRotatedNestingSize = ProductOrientationHelper.GetOrientatedSize(
                this.MainOrientationType, product.NestingWidth, product.NestingHeight, product.NestingDepth);

            Int16 nestedUnits = Convert.ToInt16(new[]
            {
                _mainRotatedNestingSize.Width.GreaterThan(0) ? mainUnits.Wide : 0,
                _mainRotatedNestingSize.Height.GreaterThan(0) ? mainUnits.High : 0,
                _mainRotatedNestingSize.Depth.GreaterThan(0) ? mainUnits.Deep : 0
            }.Max() - 1);

            Single width;
            Single height;
            Single depth;

            // Width
            if (_mainRotatedNestingSize.Width.GreaterThan(0))
            {
                width = this.MainRotatedUnitSize.Width + (nestedUnits * _mainRotatedNestingSize.Width);
            }
            else
            {
                width = (mainUnits.Wide * this.MainRotatedUnitSize.Width) + ((mainUnits.Wide - 1) * this.FacingSize.Width);
                
                // If there is an x block on the right, make sure we had finger space to the main block.
                if (position.FacingsXHigh > 0 && position.FacingsXWide > 0 && position.FacingsXDeep > 0 
                    && !position.IsXPlacedLeft)
                {
                    width += product.FingerSpaceToTheSide;
                }
            }
            // Height
            if (_mainRotatedNestingSize.Height.GreaterThan(0))
            {
                height = this.MainRotatedUnitSize.Height + nestedUnits * _mainRotatedNestingSize.Height;
            }
            else
            {
                height = mainUnits.High * this.MainRotatedUnitSize.Height +  (mainUnits.High - 1) * this.FacingSize.Height;
            }
            
            // Depth
            if (_mainRotatedNestingSize.Depth.GreaterThan(0))
            {
                depth = this.MainRotatedUnitSize.Depth + (nestedUnits) * _mainRotatedNestingSize.Depth;
            }
            else
            {
                depth = mainUnits.Deep * this.MainRotatedUnitSize.Depth + (mainUnits.Deep - 1) * this.FacingSize.Depth;
            }

            _mainTotalSize = new WidthHeightDepthValue(width, height, depth);

            #endregion

            #region X Block

            WideHighDeepValue xUnits = new WideHighDeepValue(position.FacingsXWide, position.FacingsXHigh, position.FacingsXDeep);

            _xRotatedNestingSize = ProductOrientationHelper.GetOrientatedSize(
                    this.XOrientationType, product.NestingWidth, product.NestingHeight, product.NestingDepth);

            if (xUnits.High > 0 && xUnits.Wide > 0 && xUnits.Deep > 0)
            {
                // Calculate width, height and depth separately if the products are nested.
                Single xWidth;
                Single xHeight;
                Single xDepth;
               
                nestedUnits = Convert.ToInt16(new[]
                {
                    _xRotatedNestingSize.Width.GreaterThan(0) ? xUnits.Wide : 0,
                    _xRotatedNestingSize.Height.GreaterThan(0) ? xUnits.High : 0,
                    _xRotatedNestingSize.Depth.GreaterThan(0) ? xUnits.Deep : 0
                }.Max() - 1);

                // Width
                if (_xRotatedNestingSize.Width.GreaterThan(0))
                {
                    xWidth = this.XRotatedUnitSize.Width + (nestedUnits) * _xRotatedNestingSize.Width;
                }
                else
                {
                    xWidth = (xUnits.Wide*this.XRotatedUnitSize.Width) + ((xUnits.Wide - 1)*this.FacingSize.Width);
                }

                // Height
                if (_xRotatedNestingSize.Height.GreaterThan(0))
                {
                    xHeight = this.XRotatedUnitSize.Height + (nestedUnits) * _xRotatedNestingSize.Height;
                }
                else
                {
                    xHeight = (xUnits.High*this.XRotatedUnitSize.Height) + ((xUnits.High - 1)*this.FacingSize.Height);
                }

                // Depth
                if (_xRotatedNestingSize.Depth.GreaterThan(0))
                {
                    xDepth = this.XRotatedUnitSize.Depth + (nestedUnits) * _xRotatedNestingSize.Depth;
                }
                else
                {
                    xDepth = (xUnits.Deep*this.XRotatedUnitSize.Depth) + ((xUnits.Deep - 1)*this.FacingSize.Depth);
                }

                _xTotalSize = new WidthHeightDepthValue(xWidth, xHeight, xDepth);
            }
            else
            {
                _xTotalSize = new WidthHeightDepthValue();
            }

            #endregion

            #region Y Block

            WideHighDeepValue yUnits = new WideHighDeepValue(position.FacingsYWide, position.FacingsYHigh, position.FacingsYDeep);

            _yRotatedNestingSize = ProductOrientationHelper.GetOrientatedSize(
                    this.YOrientationType, product.NestingWidth, product.NestingHeight, product.NestingDepth);

            if (yUnits.High > 0 && yUnits.Wide > 0 && yUnits.Deep > 0)
            {
                // Calculate width, height and depth separately if the products are nested.
                Single yWidth;
                Single yHeight;
                Single yDepth;
                
                nestedUnits = Convert.ToInt16(new[]
                {
                    _yRotatedNestingSize.Width.GreaterThan(0) ? yUnits.Wide : 0,
                    _yRotatedNestingSize.Height.GreaterThan(0) ? yUnits.High : 0,
                    _yRotatedNestingSize.Depth.GreaterThan(0) ? yUnits.Deep : 0
                }.Max() - 1);

                // Width
                if (_yRotatedNestingSize.Width.GreaterThan(0))
                {
                    yWidth = this.YRotatedUnitSize.Width + (nestedUnits) * _yRotatedNestingSize.Width;
                }
                else
                {
                    yWidth = (yUnits.Wide*this.YRotatedUnitSize.Width) + ((yUnits.Wide - 1)*this.FacingSize.Width);
                }

                // Height
                if (_yRotatedNestingSize.Height.GreaterThan(0))
                {
                    yHeight = this.YRotatedUnitSize.Height + (nestedUnits) * _yRotatedNestingSize.Height;
                }
                else
                {
                    yHeight = (yUnits.High*this.YRotatedUnitSize.Height) + ((yUnits.High - 1)*this.FacingSize.Height);
                }

                // Depth
                if (_yRotatedNestingSize.Depth.GreaterThan(0))
                {
                    yDepth = this.YRotatedUnitSize.Depth + (nestedUnits) * _yRotatedNestingSize.Depth;
                }
                else
                {
                    yDepth = (yUnits.Deep*this.YRotatedUnitSize.Depth) + ((yUnits.Deep - 1)*this.FacingSize.Depth);
                }

                _yTotalSize = new WidthHeightDepthValue(yWidth, yHeight, yDepth);
            }
            else
            {
                _yTotalSize = new WidthHeightDepthValue();
            }

            #endregion

            #region Z Block

            WideHighDeepValue zUnits = new WideHighDeepValue(position.FacingsZWide, position.FacingsZHigh, position.FacingsZDeep);

            _zRotatedNestingSize = ProductOrientationHelper.GetOrientatedSize(
                    this.ZOrientationType, product.NestingWidth, product.NestingHeight, product.NestingDepth);

            if (zUnits.High > 0 && zUnits.Wide > 0 && zUnits.Deep > 0)
            {
                // If our products are nested, the size of the z block will be affected. As such
                // we need to calculate the width, height and depth independently.
                Single zWidth;
                Single zHeight;
                Single zDepth;
                
                nestedUnits = Convert.ToInt16(new[]
                {
                    _zRotatedNestingSize.Width.GreaterThan(0) ? zUnits.Wide : 0,
                    _zRotatedNestingSize.Height.GreaterThan(0) ? zUnits.High : 0,
                    _zRotatedNestingSize.Depth.GreaterThan(0) ? zUnits.Deep : 0
                }.Max() - 1);

                // Width
                if (_zRotatedNestingSize.Width.GreaterThan(0))
                {
                    zWidth = this.ZRotatedUnitSize.Width + (nestedUnits) * _zRotatedNestingSize.Width;
                }
                else
                {
                    zWidth = (zUnits.Wide*this.ZRotatedUnitSize.Width) + ((zUnits.Wide - 1)*this.FacingSize.Width);
                }

                // Height
                if (_zRotatedNestingSize.Height.GreaterThan(0))
                {
                    zHeight = this.ZRotatedUnitSize.Height + (nestedUnits) * _zRotatedNestingSize.Height;
                }
                else
                {
                    zHeight = (zUnits.High*this.ZRotatedUnitSize.Height) + ((zUnits.High - 1)*this.FacingSize.Height);
                }

                // Depth
                if (_zRotatedNestingSize.Depth.GreaterThan(0))
                {
                    zDepth = this.ZRotatedUnitSize.Depth + (nestedUnits) * _zRotatedNestingSize.Depth;
                }
                else
                {
                    zDepth = (zUnits.Deep*this.ZRotatedUnitSize.Depth) + ((zUnits.Deep - 1)*this.FacingSize.Depth);
                }

                _zTotalSize = new WidthHeightDepthValue(zWidth, zHeight, zDepth);
            }
            else
            {
                _zTotalSize = new WidthHeightDepthValue();
            }

            #endregion
        }

        /// <summary>
        /// Calculates the coordinates for each position block
        /// </summary>
        private void CalculateCoordinates(PlanogramPosition position, PlanogramProduct product)
        {
            RectValue mainBlock = new RectValue(
                (position.IsXPlacedLeft) ? XTotalSize.Width : 0,
                (position.IsYPlacedBottom) ? YTotalSize.Height : 0,
                (position.IsZPlacedFront) ? 0 : ZTotalSize.Depth,
                MainTotalSize.Width,
                MainTotalSize.Height,
                MainTotalSize.Depth);

            RectValue xBlock = new RectValue(
                (position.IsXPlacedLeft) ? 0 : MainTotalSize.Width,
                mainBlock.Y,
                (mainBlock.Z + MainTotalSize.Depth) - XTotalSize.Depth,
                XTotalSize.Width,
                XTotalSize.Height,
                XTotalSize.Depth);

            RectValue yBlock = new RectValue(
                mainBlock.X,
                (position.IsYPlacedBottom) ? 0 : MainTotalSize.Height,
                (mainBlock.Z + MainTotalSize.Depth) - YTotalSize.Depth,
                YTotalSize.Width,
                YTotalSize.Height,
                YTotalSize.Depth);

            RectValue zBlock = new RectValue(
                mainBlock.X,
                mainBlock.Y,
                (position.IsZPlacedFront) ? MainTotalSize.Depth : mainBlock.Z - ZTotalSize.Depth,
                ZTotalSize.Width,
                ZTotalSize.Height,
                ZTotalSize.Depth);

            if (!position.IsXPlacedLeft)
            {
                //if z intersects with x shift x right
                if (xBlock.CollidesWith(zBlock))
                {
                    xBlock.X = (zBlock.X + zBlock.Width);
                }
            }

            if (position.IsYPlacedBottom)
            {
                //see if x and z blocks can move down.
                xBlock.Y = 0;
                if (yBlock.CollidesWith(xBlock))
                {
                    xBlock.Y = mainBlock.Y;
                }

                zBlock.Y = 0;
                if (yBlock.CollidesWith(zBlock))
                {
                    zBlock.Y = mainBlock.Y;
                }
            }
            else
            {
                //push the y block up if it intersects with x or z.
                if (yBlock.CollidesWith(zBlock))
                {
                    yBlock.Y = (zBlock.Y + zBlock.Height);
                }

                if (yBlock.CollidesWith(xBlock))
                {
                    yBlock.Y = (xBlock.Y + xBlock.Height);
                }
            }

            Single minZ = new[] {mainBlock.Z, xBlock.Z, yBlock.Z, zBlock.Z}.Min();
            if (minZ < 0)
            {
                minZ = Math.Abs(minZ);
                mainBlock.Z += minZ;
                xBlock.Z += minZ;
                yBlock.Z += minZ;
                zBlock.Z += minZ;
            }

            RectValue allBlocks = mainBlock;
            _mainCoordinates = new PointValue(mainBlock.X, mainBlock.Y, mainBlock.Z);

            if (this.HasXBlock)
            {
                allBlocks = allBlocks.Union(xBlock);
                _xCoordinates = new PointValue(xBlock.X, xBlock.Y, xBlock.Z);
            }
            if (this.HasYBlock)
            {
                allBlocks = allBlocks.Union(yBlock);
                _yCoordinates = new PointValue(yBlock.X, yBlock.Y, yBlock.Z);
            }
            if (this.HasZBlock)
            {
                allBlocks = allBlocks.Union(zBlock);
                _zCoordinates = new PointValue(zBlock.X, zBlock.Y, zBlock.Z);
            }

            // set the total size
            _totalSize =
                new WidthHeightDepthValue(
                    allBlocks.Width,
                    allBlocks.Height,
                    allBlocks.Depth);

            // set the total space including finger space around the border of the position
            _totalSpace = new WidthHeightDepthValue(_totalSize.Width, _totalSize.Height, _totalSize.Depth);
            _totalSpace.Width += product.FingerSpaceToTheSide;
            _totalSpace.Height += product.FingerSpaceAbove;
        }

        /// <summary>
        /// Calculates the total units for this position
        /// </summary>
        private void CalculateTotalUnits(PlanogramPosition position, PlanogramProduct product)
        {
            #region Main Block

            Int16 mainUnitsHigh = position.FacingsHigh;
            Int16 mainUnitsWide = position.FacingsWide;
            Int16 mainUnitsDeep = position.FacingsDeep;
            Int32 mainTotalUnits = Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);

            if (this.MainMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                mainUnitsHigh *= ((product.TrayHigh > 0) ? product.TrayHigh : (Int16)1);
                mainUnitsWide *= ((product.TrayWide > 0) ? product.TrayWide : (Int16)1);
                mainUnitsDeep *= ((product.TrayDeep > 0) ? product.TrayDeep : (Int16)1);

                mainTotalUnits =
                    (product.TrayPackUnits > 1) ? mainTotalUnits * product.TrayPackUnits
                    : Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);
            }
            else if (this.MainMerchStyle == PlanogramProductMerchandisingStyle.Case)
            {
                mainUnitsHigh *= ((product.CaseHigh > 0) ? product.CaseHigh : (Int16)1);
                mainUnitsWide *= ((product.CaseWide > 0) ? product.CaseWide : (Int16)1);
                mainUnitsDeep *= ((product.CaseDeep > 0) ? product.CaseDeep : (Int16)1);

                mainTotalUnits =
                    (product.CasePackUnits > 1) ? mainTotalUnits * product.CasePackUnits
                    : Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);
            }

            if (mainUnitsHigh == 0 || mainUnitsWide == 0 || mainUnitsDeep == 0)
            {
                mainUnitsHigh = 0;
                mainUnitsWide = 0;
                mainUnitsDeep = 0;
            }
            #endregion

            #region X Block

            Int16 xUnitsHigh = position.FacingsXHigh;
            Int16 xUnitsWide = position.FacingsXWide;
            Int16 xUnitsDeep = position.FacingsXDeep;
            Int32 xTotalUnits = Convert.ToInt32(xUnitsHigh * xUnitsWide * xUnitsDeep);

            if (this.XMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                xUnitsHigh *= ((product.TrayHigh > 0) ? product.TrayHigh : (Int16)1);
                xUnitsWide *= ((product.TrayWide > 0) ? product.TrayWide : (Int16)1);
                xUnitsDeep *= ((product.TrayDeep > 0) ? product.TrayDeep : (Int16)1);

                xTotalUnits =
                   (product.TrayPackUnits > 1) ? xTotalUnits * product.TrayPackUnits
                   : Convert.ToInt32(xUnitsHigh * xUnitsWide * xUnitsDeep);
            }
            else if (this.XMerchStyle == PlanogramProductMerchandisingStyle.Case)
            {
                xUnitsHigh *= ((product.CaseHigh > 0) ? product.CaseHigh : (Int16)1);
                xUnitsWide *= ((product.CaseWide > 0) ? product.CaseWide : (Int16)1);
                xUnitsDeep *= ((product.CaseDeep > 0) ? product.CaseDeep : (Int16)1);

                xTotalUnits =
                    (product.CasePackUnits > 1) ? xTotalUnits * product.CasePackUnits
                    : Convert.ToInt32(xUnitsHigh * xUnitsWide * xUnitsDeep);
            }
            if (xUnitsHigh == 0 || xUnitsWide == 0 || xUnitsDeep == 0)
            {
                xUnitsHigh = 0;
                xUnitsWide = 0;
                xUnitsDeep = 0;
            }


            #endregion

            #region Y Block

            Int16 yUnitsHigh = position.FacingsYHigh;
            Int16 yUnitsWide = position.FacingsYWide;
            Int16 yUnitsDeep = position.FacingsYDeep;
            Int32 yTotalUnits = Convert.ToInt32(yUnitsHigh * yUnitsWide * yUnitsDeep);

            if (this.YMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                yUnitsHigh *= ((product.TrayHigh > 0) ? product.TrayHigh : (Int16)1);
                yUnitsWide *= ((product.TrayWide > 0) ? product.TrayWide : (Int16)1);
                yUnitsDeep *= ((product.TrayDeep > 0) ? product.TrayDeep : (Int16)1);

                yTotalUnits =
                   (product.TrayPackUnits > 1) ? yTotalUnits * product.TrayPackUnits
                   : Convert.ToInt32(yUnitsHigh * yUnitsWide * yUnitsDeep);
            }
            else if (this.YMerchStyle == PlanogramProductMerchandisingStyle.Case)
            {
                yUnitsHigh *= ((product.CaseHigh > 0) ? product.CaseHigh : (Int16)1);
                yUnitsWide *= ((product.CaseWide > 0) ? product.CaseWide : (Int16)1);
                yUnitsDeep *= ((product.CaseDeep > 0) ? product.CaseDeep : (Int16)1);

                yTotalUnits =
                    (product.CasePackUnits > 1) ? yTotalUnits * product.CasePackUnits
                    : Convert.ToInt32(yUnitsHigh * yUnitsWide * yUnitsDeep);
            }

            if (yUnitsHigh == 0 || yUnitsWide == 0 || yUnitsDeep == 0)
            {
                yUnitsHigh = 0;
                yUnitsWide = 0;
                yUnitsDeep = 0;
            }


            #endregion

            #region Z Block

            Int16 zUnitsHigh = position.FacingsZHigh;
            Int16 zUnitsWide = position.FacingsZWide;
            Int16 zUnitsDeep = position.FacingsZDeep;
            Int32 zTotalUnits = Convert.ToInt32(zUnitsHigh * zUnitsWide * zUnitsDeep);

            if (this.ZMerchStyle == PlanogramProductMerchandisingStyle.Tray)
            {
                //TODO: What if tray is orientated?
                zUnitsHigh *= ((product.TrayHigh > 0) ? product.TrayHigh : (Int16)1);
                zUnitsWide *= ((product.TrayWide > 0) ? product.TrayWide : (Int16)1);
                zUnitsDeep *= ((product.TrayDeep > 0) ? product.TrayDeep : (Int16)1);

                zTotalUnits =
                  (product.TrayPackUnits > 1) ? zTotalUnits * product.TrayPackUnits
                  : Convert.ToInt32(zUnitsHigh * zUnitsWide * zUnitsDeep);
            }
            else if (this.ZMerchStyle == PlanogramProductMerchandisingStyle.Case)
            {
                zUnitsHigh *= ((product.CaseHigh > 0) ? product.CaseHigh : (Int16)1);
                zUnitsWide *= ((product.CaseWide > 0) ? product.CaseWide : (Int16)1);
                zUnitsDeep *= ((product.CaseDeep > 0) ? product.CaseDeep : (Int16)1);

                zTotalUnits =
                    (product.CasePackUnits > 1) ? zTotalUnits * product.CasePackUnits
                    : Convert.ToInt32(zUnitsHigh * zUnitsWide * zUnitsDeep);
            }

            if (zUnitsHigh == 0 || zUnitsWide == 0 || zUnitsDeep == 0)
            {
                zUnitsHigh = 0;
                zUnitsWide = 0;
                zUnitsDeep = 0;
            }


            #endregion

            #region Totals

            Int16 totalUnitsHigh = Convert.ToInt16(new[] { mainUnitsHigh, xUnitsHigh, zUnitsHigh }.Max() + yUnitsHigh);
            Int16 totalUnitsWide = Convert.ToInt16(new[] { mainUnitsWide, yUnitsWide, zUnitsWide }.Max() + xUnitsWide);
            Int16 totalUnitsDeep = Convert.ToInt16(new[] { mainUnitsDeep, xUnitsDeep, yUnitsDeep }.Max() + zUnitsDeep);
            _totalUnits = new WideHighDeepValue(totalUnitsWide, totalUnitsHigh, totalUnitsDeep);
            _totalUnitCount = Convert.ToInt32(mainTotalUnits + xTotalUnits + yTotalUnits + zTotalUnits);

            #endregion
        }

        /// <summary>
        /// Calculates the minimum size of blocks with full squeeze applied.
        /// </summary>
        private void CalculateMinimumBlockSizes(PlanogramPosition position, PlanogramProduct product)
        {
            // Main Block:
            _mainRotatedMaxSqueeze = ProductOrientationHelper.GetOrientatedMaxSqueeze(product, this.MainMerchStyle,
                this.MainOrientationType);

            _mainMinimumSize = new WidthHeightDepthValue(
                this.MainRotatedUnitSize.Width*_mainRotatedMaxSqueeze.Width*position.FacingsWide,
                this.MainRotatedUnitSize.Height*_mainRotatedMaxSqueeze.Height*position.FacingsHigh,
                this.MainRotatedUnitSize.Depth*_mainRotatedMaxSqueeze.Depth*position.FacingsDeep);


            //X Block:
            _xRotatedMaxSqueeze = ProductOrientationHelper.GetOrientatedMaxSqueeze(product, this.XMerchStyle,
                this.XOrientationType);

            _xMinimumSize = new WidthHeightDepthValue(
                this.XRotatedUnitSize.Width*_xRotatedMaxSqueeze.Width*position.FacingsXWide,
                this.XRotatedUnitSize.Height*_xRotatedMaxSqueeze.Height*position.FacingsXHigh,
                this.XRotatedUnitSize.Depth*_xRotatedMaxSqueeze.Depth*position.FacingsXDeep);

            //Y Block
            _yRotatedMaxSqueeze = ProductOrientationHelper.GetOrientatedMaxSqueeze(product, this.YMerchStyle,
                this.YOrientationType);

            _yMinimumSize = new WidthHeightDepthValue(
                this.YRotatedUnitSize.Width*_yRotatedMaxSqueeze.Width*position.FacingsYWide,
                this.YRotatedUnitSize.Height*_yRotatedMaxSqueeze.Height*position.FacingsYHigh,
                this.YRotatedUnitSize.Depth*_yRotatedMaxSqueeze.Depth*position.FacingsYDeep);


            //Z Block
            _zRotatedMaxSqueeze = ProductOrientationHelper.GetOrientatedMaxSqueeze(product, this.ZMerchStyle,
                this.ZOrientationType);

            _zMinimumSize = new WidthHeightDepthValue(
                this.ZRotatedUnitSize.Width*_zRotatedMaxSqueeze.Width*position.FacingsZWide,
                this.ZRotatedUnitSize.Height*_zRotatedMaxSqueeze.Height*position.FacingsZHigh,
                this.ZRotatedUnitSize.Depth*_zRotatedMaxSqueeze.Depth*position.FacingsZDeep);


            #region Overall Minimum.

            RectValue mainBlock = new RectValue(
                (position.IsXPlacedLeft) ? XMinimumSize.Width : 0,
                (position.IsYPlacedBottom) ? YMinimumSize.Height : 0,
                (position.IsZPlacedFront) ? 0 : ZMinimumSize.Depth,
                MainMinimumSize.Width,
                MainMinimumSize.Height,
                MainMinimumSize.Depth);

            RectValue xBlock = new RectValue(
                (position.IsXPlacedLeft) ? 0 : MainMinimumSize.Width,
                mainBlock.Y,
                (mainBlock.Z + MainMinimumSize.Depth) - XMinimumSize.Depth,
                XMinimumSize.Width,
                XMinimumSize.Height,
                XMinimumSize.Depth);

            RectValue yBlock = new RectValue(
                mainBlock.X,
                (position.IsYPlacedBottom) ? 0 : MainMinimumSize.Height,
                (mainBlock.Z + MainMinimumSize.Depth) - YMinimumSize.Depth,
                YMinimumSize.Width,
                YMinimumSize.Height,
                YMinimumSize.Depth);

            RectValue zBlock = new RectValue(
                mainBlock.X,
                mainBlock.Y,
                (position.IsZPlacedFront) ? MainMinimumSize.Depth : mainBlock.Z - ZMinimumSize.Depth,
                ZMinimumSize.Width,
                ZMinimumSize.Height,
                ZMinimumSize.Depth);

            if (!position.IsXPlacedLeft)
            {
                //if z intersects with x shift x right
                if (xBlock.CollidesWith(zBlock))
                {
                    xBlock.X = (zBlock.X + zBlock.Width);
                }
            }

            if (position.IsYPlacedBottom)
            {
                //see if x and z blocks can move down.
                xBlock.Y = 0;
                if (yBlock.CollidesWith(xBlock))
                {
                    xBlock.Y = mainBlock.Y;
                }

                zBlock.Y = 0;
                if (yBlock.CollidesWith(zBlock))
                {
                    zBlock.Y = mainBlock.Y;
                }
            }
            else
            {
                //push the y block up if it intersects with x or z.
                if (yBlock.CollidesWith(zBlock))
                {
                    yBlock.Y = (zBlock.Y + zBlock.Height);
                }

                if (yBlock.CollidesWith(xBlock))
                {
                    yBlock.Y = (xBlock.Y + xBlock.Height);
                }
            }

            Single minZ = new[] {mainBlock.Z, xBlock.Z, yBlock.Z, zBlock.Z}.Min();
            if (minZ < 0)
            {
                minZ = Math.Abs(minZ);
                mainBlock.Z += minZ;
                xBlock.Z += minZ;
                yBlock.Z += minZ;
                zBlock.Z += minZ;
            }

            RectValue allBlocks = mainBlock;
            if (this.HasXBlock)
            {
                allBlocks = allBlocks.Union(xBlock);
            }
            if (this.HasYBlock)
            {
                allBlocks = allBlocks.Union(yBlock);
            }
            if (this.HasZBlock)
            {
                allBlocks = allBlocks.Union(zBlock);
            }

            // set the min size
            _minimumSize = new WidthHeightDepthValue(allBlocks.Width, allBlocks.Height, allBlocks.Depth);

            #endregion
        }

        /// <summary>
        ///     Get the given <paramref name="axis"/>' block <c>Rotated Unit Size</c>.
        /// </summary>
        /// <param name="axis">The axis to determine which block's <c>Rotated Unit Size</c> return.</param>
        /// <returns>A <see cref="WideHighDeepValue"/> with the size for the axis' block.</returns>
        public WidthHeightDepthValue GetBlockRotatedUnitSize(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return XRotatedUnitSize;
                case AxisType.Y:
                    return YRotatedUnitSize;
                case AxisType.Z:
                    return ZRotatedUnitSize;
                default:
                    Debug.Fail("Unknown axis when calling GetBlockRotatedUnitSize(axis, positionDetails)");
                    break;
            }
            return new WidthHeightDepthValue();
        }

        /// <summary>
        /// Returns the expected size of the block if it were to have the given number of facings in the axis.
        /// This method takes into consideration nesting sizes.
        /// </summary>
        /// <param name="blockType">the block type to check</param>
        /// <param name="axis">the axis to check in</param>
        /// <param name="facings">the number of facings to apply</param>
        /// <returns></returns>
        public Single GetAnticipatedBlockSize(PlanogramPositionBlockType blockType, AxisType axis, Int32 facings)
        {
            if (facings <= 0) return 0;

            Single firstFacingSize;
            Single additionalFacingSize;

            switch (blockType)
            {
                default:
                case PlanogramPositionBlockType.Main:
                    firstFacingSize = this.MainRotatedUnitSize.GetSize(axis);
                    additionalFacingSize = this.MainRotatedNestingSize.GetSize(axis).GreaterThan(0) ? this.MainRotatedNestingSize.GetSize(axis) : this.MainRotatedUnitSize.GetSize(axis);
                    break;

                case PlanogramPositionBlockType.X:
                    firstFacingSize = this.XRotatedUnitSize.GetSize(axis);
                    additionalFacingSize = this.XRotatedNestingSize.GetSize(axis).GreaterThan(0) ? this.XRotatedNestingSize.GetSize(axis) : this.XRotatedUnitSize.GetSize(axis);
                    break;

                case PlanogramPositionBlockType.Y:
                    firstFacingSize = this.YRotatedUnitSize.GetSize(axis);
                    additionalFacingSize = this.YRotatedNestingSize.GetSize(axis).GreaterThan(0) ? this.YRotatedNestingSize.GetSize(axis) : this.YRotatedUnitSize.GetSize(axis);
                    break;

                case PlanogramPositionBlockType.Z:
                    firstFacingSize = this.ZRotatedUnitSize.GetSize(axis);
                    additionalFacingSize = this.ZRotatedNestingSize.GetSize(axis).GreaterThan(0) ? this.ZRotatedNestingSize.GetSize(axis) : this.ZRotatedUnitSize.GetSize(axis);
                    break;
            }

            if (facings == 1) return firstFacingSize;

            return firstFacingSize + ((facings - 1) * additionalFacingSize);
        }

        /// <summary>
        /// Returns the peg x and peg y values for the product with
        /// any defaults applied.
        /// </summary>
        public static void  GetProductPegXPegY(
            PlanogramProduct product, PlanogramPositionDetails posDetails, Planogram plan,
            out Single pegX, out Single pegY)
        {
            pegY = Math.Abs(product.PegY);
            pegY = GetDefaultPegYOffset(pegY, plan.LengthUnitsOfMeasure);

            pegX = Math.Abs(product.PegX);
            if (pegX.EqualTo(0)) pegX = Math.Abs(posDetails.MainRotatedUnitSize.Width / 2F);
        }

        public static void GetProductPegXPegY(
            PlanogramProduct product, PlanogramPositionDetails posDetails, Planogram plan,
            out Single pegX, out Single pegY, out Single pegX2, out Single pegY2, out Single pegX3, out Single pegY3)
        {
            pegY = Math.Abs(product.PegY);
            pegY = GetDefaultPegYOffset(pegY, plan.LengthUnitsOfMeasure);

            pegX = Math.Abs(product.PegX);
            if (pegX.EqualTo(0)) pegX = Math.Abs(posDetails.MainRotatedUnitSize.Width / 2F);

            pegY2 = Math.Abs(product.PegY2);
            pegY2 = GetDefaultPegYOffset(pegY2, plan.LengthUnitsOfMeasure);

            pegX2 = Math.Abs(product.PegX2);
            if (pegX2.EqualTo(0)) pegX2 = Math.Abs(posDetails.MainRotatedUnitSize.Width / 2F);

            pegY3 = Math.Abs(product.PegY3);
            pegY3 = GetDefaultPegYOffset(pegY3, plan.LengthUnitsOfMeasure);

            pegX3 = Math.Abs(product.PegX3);
            if (pegX3.EqualTo(0)) pegX3 = Math.Abs(posDetails.MainRotatedUnitSize.Width / 2F);
        }


        /// <summary>
        /// Determines the default y position offset for a peghole
        /// </summary>
        public static Single GetDefaultPegYOffset(Single pegY, PlanogramLengthUnitOfMeasureType uom)
        {
            //TODO: pull this from settings?
            if (pegY == 0)
            {
                switch (uom)
                {
                    default:
                    case PlanogramLengthUnitOfMeasureType.Centimeters:
                        return 0.8F;

                    case PlanogramLengthUnitOfMeasureType.Inches:
                        return 0.25F;
                }
            }

            return pegY;
        }

        #endregion
    }
}
