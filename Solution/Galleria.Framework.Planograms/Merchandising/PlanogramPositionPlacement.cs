﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26421 : N.Foster
//  Created
// V8-27738 : N.Foster
//  Refactored slightly to centralise some of the logic for manipulating positions
//  Changed to a POCO for performance
// V8-27762 : N.Foster
//  Refactored to support the rollback of changes in planogram position placement
// V8-27738 : N.Foster
//  Overrided GetHashCode and Equals
// V8-27832 : A.Kuszyk
//  Added GetBounds method.
// V8-27852 : N.Foster
//  Added GetMerchandisingGroupRelativeCoordinates
// V8-27858 : A.Kuszyk
//  Added GetDesignViewPosition method and re-named GetBounds to GetMerchandisingGroupRelativeBounds.
// V8-28014 : L.Ineson
//  Added peghole snap to nearest by distance for manual strategies.
// V8-28620 : N.Foster
//  Fixed issue regarding edit levels

#endregion
#region Version History: CCM801

// V8-28711 : L.Ineson
//  Tweaked peghole snap so that clipstrips place correctly.

#endregion
#region Version History: CCM802

//V8-29028 : L.Ineson
//  Added methods to help apply product squeeze.
// V8-29082 : L.Ineson
// Corrected GetMerchandisingGroupRelativeCoordinates to consider
// the coordinate of the merch group.
// V8-29423 : A.Silva
//      Added RecalculateUnits method to recalculate the units in the Position.

#endregion
#region Version History: CCM803

// V8-29137 : A.Kuszyk
//  Removed unused factory methods and made remaining method internal - Position PLacements should be instantiated by a Merchandising Group.
//  Also amended GetMerchandisingGroupRelativeCoordinates to simply return position coords - if the position placement was
//  instantiated by a merch group, the coords will already be merch group relative.
// V8-28766 : N.Foster
//  Stubbed out autofill methods

#endregion
#region Version History: CCM810

// V8-28766 : J.Pickup
//  Remove() all caps method introduced.
// V8-29826 : N.Foster
//  Enhance behavior of merchandising group when dealing with Begin/Cancel/Apply of edits
// V8-28766 : J.Pickup
//  Introduced an overload on IncreaseUnits() that takes a merch group as a parameter SetMinimumUnits accepts parameters.
// V8-29854 : A.Silva
//  Added IsOutsideMerchandisingSpace to help determine whether the placement has issues.
// V8-29903 : L.Ineson
//  Added GetOriginalState()
// V8-29866 : A.Kuszyk
//  Added checks to SnapToPegHole to ensure that the position does not snap out of bounds.
// V8-29895 : A.Kuszyk
//  Updated GetMerchGroupRelativeCoords to calculate relative coords if not relative already.
// V8-30033 : A.Silva
//  Added GetPegHoles.
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30141 : A.Silva
//  Refactored use of GetAvailableSpaceOnAxis.
// V8-30168 : L.Ineson
//  GetPlanogramRelativeCoordinates & Bounds now take into account if the position is merch group relative.
// V8-30145 : J.Pickup
//  Added a method to check whether the current placement overfills a hang component that does not contain pegs.
// V8-30173 : L.Ineson
//  Moved GetReservedSpace(Axis) method to here.
// V8-30206 : A.Kuszyk
//  Changed single operators in use in SnapPositionToPegHole method to use framework extension methods.
#endregion
#region Version History: CCM811

// V8-30406 : A.Silva
//  Refactored calls to Increase/Remove caps on the underlying Position.

#endregion
#region Version History: CCM820
// V8-30744 : N.Foster
//  Improved performance by caching PlanogramPositionDetails
// V8-30818 : A.Kuszyk
//  Re-factored GetReservedSpace in from PositionDetails.
// V8-30795 : A.Kuszyk
//  Implemented IPlanogramSubComponentSequencedItem
// V8-30936 : M.Brumby
//  Added slotwall
// V8-31230 : A.Silva
//  Modified SnapPositionToPegHole so that Hang components always snap Y at least. (Bars now will hang from the top of the component).
// V8-31212 : M.Brumby
//  Pegged positions are now more aware of orientations
// V8-31407 : J.Pickup
//  Slight autofill performance increase - getreservedspace no longer calculated an exponential amount of times. 
#endregion
#region Version History: CCM820
//V8-31672 : L.Ineson
//  Added overloads for Increase and Decrease units.
#endregion
#region Version History: CCM830
// V8-31828 : L.Ineson
//  Moved squeeze logic into PlanogramPosition
// V8-31856 : J.Pickup
//  Change to set minimum units to pay attention to is manually placed to fix 
//  an autofill issue that shrank all groups effected by hand merch groups.
// V8-31804 : A.Kuszyk
//  Amended BeginEdit to use GetOptimisationClone.
// V8-32002 : L.Ineson
//  Changed how pegs are snapped for components with no holes.
// V8-31884 : M.Brumby
//  Create new function IsMaxExceeded so there is a central place to check max values for an axis.
//  This also includes checking MaxCaps and Max Right caps which previously some areas of code missed.
// V8-32316 : A.Kuszyk
//  Protected SnapPositionToPegHoleX from iterating onto a negative peg hole number.
// V8-31385 : A.Silva
//  Amended DoesPositionOverfillXAxisForSubcomponentWithoutPegholes to account for overhang.
// V8-31981 : L.Ineson
//  Added helpers for getting the actual pegx and pegy values to use.
// V8-32647 : A.Silva
//  Amended GetPegHoles to remove an unnecesary line of code (it was being done already when calculating pegX).
// V8-32267 : L.Ineson
//  Updated SnapPositionToPegHole to support PegProngOffsetX and PegProngOffsetY
// V8-32787 : A.Silva
//  Refactored Increase, Decrease and Set Units, removed ducplicated code and reorganized.
// V8-32982 : A.Kuszyk
//  Implemented minor performance enhancements.
// V8-32875 : L.Ineson
//  Stopped peghole snap for y ever going off the board.
// CCM-13761 : L.Ineson
//  Added GetBoundaryInfo for improved collision detection.
// CCM-18462 : A.Kuszyk
//  Added IsConsecutive helper method.
#endregion
#region Version History : CCM831
// CCM-18673 : A.Kuszyk
//  Corrected issue in GetReservedSpace for even components with dividers.
#endregion
#region Version History : CCM840-CP02
// CCM-19302 : J.Mendes
//  Changed the rule that checks the overfill on a hang position that exists on a component without pegholes (For example a bar). 
//  The rule now does NOT assume that half the products first/last facing must fit on the subcomponent.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Provides a mechanism for manipulating
    /// a planogram position and then committing
    /// changes once you are happy with them
    /// </summary>
    [DebuggerDisplay("{DebugValue()}", Name = "{DebugName()}")]
    public class PlanogramPositionPlacement : IDisposable, IPlanogramSubComponentSequencedItem
    {
        #region Nested Classes
        /// <summary>
        /// Class used to capture the state of this placement
        /// during edit operations
        /// </summary>
        private class State
        {
            #region Properties
            /// <summary>
            /// The position state
            /// </summary>
            public PlanogramPosition Position { get; private set; }

            /// <summary>
            /// The sub component placement state
            /// </summary>
            public PlanogramSubComponentPlacement SubComponentPlacement { get; private set; }

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public State(PlanogramPositionPlacement placement)
            {
                this.Position = placement.Position;
                this.SubComponentPlacement = placement.SubComponentPlacement;
            }

            #endregion
        }

        /// <summary>
        /// A simple container for a Coordinate and Space value.
        /// </summary>
        private class CoordinateSpaceChange
        {
            #region Properties
            /// <summary>
            /// The amount by which a co-ordinate should be changed.
            /// </summary>
            public Single Coordinate { get; set; }

            /// <summary>
            /// The amount by which a size should be changed.
            /// </summary>
            public Single Space { get; set; } 
            #endregion

            #region Constructors
            private CoordinateSpaceChange()
            {
                Space = 0;
                Coordinate = 0;
            }

            private CoordinateSpaceChange(Single coordinate, Single space)
            {
                Coordinate = coordinate;
                Space = space;
            }
            #endregion

            #region Methods

            /// <summary>
            /// Gets the amount that the space needs to change to take into account dividers
            /// around the position.
            /// </summary>
            /// <param name="axis">The direction in which the method should evaluate dividers.</param>
            /// <param name="space">The space value whose change should be determined.</param>
            /// <param name="positionPlacementCoords">The coordinates of the position being evaluated.</param>
            /// <param name="merchandisingGroup">The merchandising group upon which the position is placed.</param>
            /// <param name="positionPlacementsCoords">The coordinates of all the positions on the merchandising group.</param>
            /// <returns>
            /// A value containing the amount the coordinate should be increased and the amount that the space value should be increased.
            /// </returns>
            public static CoordinateSpaceChange GetSpaceChangeForDividers(
                AxisType axis,
                Single space,
                PointValue positionPlacementCoords,
                PlanogramMerchandisingGroup merchandisingGroup,
                IEnumerable<PointValue> positionPlacementsCoords,
                PlanogramPositionDetails positionDetails)
            {
                // First of all, identify the size of the dividers. If there are no dividers,
                // we can just return.
                Single dividerSize = GetDividerSize(axis, merchandisingGroup);
                if (dividerSize.EqualTo(0f)) return new CoordinateSpaceChange();

                PlanogramMerchandisingGroupAxis merchGroupAxis = merchandisingGroup.GetAxis(axis);
                Single positionPlacementCoord = positionPlacementCoords.GetCoordinate(axis);
                Single positionPlacementSpace = positionDetails.TotalSpace.GetSize(axis);

                // Work out if this is the first or last position in the axis.
                Boolean isStartPosition = positionPlacementCoord.EqualTo(positionPlacementsCoords.Min(p => p.GetCoordinate(axis)));
                Boolean isEndPosition = positionPlacementCoord.EqualTo(positionPlacementsCoords.Max(p => p.GetCoordinate(axis)));

                // If the position is not after the divider start position, then we don't need to account
                // for dividers at all.
                if (!ShouldAccountForDividers(merchGroupAxis, positionPlacementCoord)) return new CoordinateSpaceChange();

                // If the merchandising strategy for this axis is not stacked or even,
                // then dividers won't be placed, so we don't need to account for their space.
                if (!IsStrategyStackedAndNotManual(merchGroupAxis)) return new CoordinateSpaceChange();

                // In the X direction, dividers can be placed between product facings. This overrides all
                // other divider settings, so we need to check for it here as a special case.
                if (axis == AxisType.X && merchandisingGroup.AreDividersPlacedByFacing())
                {
                    return CoordinateSpaceChange.GetSpaceChangeForDividersByFacing(
                        merchandisingGroup, dividerSize, isStartPosition, isEndPosition);
                }
                else if (merchandisingGroup.AreDividersPlacedNormally(merchGroupAxis))
                {
                    return CoordinateSpaceChange.GetSpaceChangeForDividers(
                        space,
                        merchandisingGroup,
                        dividerSize,
                        merchGroupAxis,
                        positionPlacementCoord,
                        positionPlacementSpace,
                        isStartPosition,
                        isEndPosition);
                }

                // We should never reach this stage, but if we do, just return an empty space change.
                return new CoordinateSpaceChange();
            }

            /// <summary>
            /// Gets the space change to a position's co-ordinate and space when dividers are placed by facings.
            /// </summary>
            /// <param name="merchandisingGroup"></param>
            /// <param name="dividerSize"></param>
            /// <param name="isStartPosition"></param>
            /// <param name="isEndPosition"></param>
            /// <returns></returns>
            /// <remarks>
            /// In this case, dividers only appear between facings. The space that is occupied
            /// by these dividers has already been taken into account in the position details
            /// TotalSize calculation, so we don't need to account for it. However, TotalSize
            /// does not take into account the dividers that are placed between positions or at 
            /// that start or the end of the merch group.
            /// </remarks>
            private static CoordinateSpaceChange GetSpaceChangeForDividersByFacing(
                PlanogramMerchandisingGroup merchandisingGroup, Single dividerSize, Boolean isStartPosition, Boolean isEndPosition)
            {
                CoordinateSpaceChange deltas = new CoordinateSpaceChange();

                // Dividers at the end or in the middle.
                if (isEndPosition && merchandisingGroup.PlaceEndDivider || !isEndPosition)
                {
                    deltas.Space += dividerSize;
                    // We don't need to change the coordinate here, because these are at the "end"
                    // or positive axis direction.
                }

                // Dividers at the start.
                if (isStartPosition && merchandisingGroup.PlaceStartDivider)
                {
                    deltas.Space += dividerSize;
                    // We need to change the coordinate here, because these are at the "start"
                    // or negative axis direction.
                    deltas.Coordinate -= dividerSize;
                }

                return deltas;
            }

            /// <summary>
            /// Gets the space change for a position when dividers are placed normally.
            /// </summary>
            /// <param name="space"></param>
            /// <param name="merchandisingGroup"></param>
            /// <param name="dividerSize"></param>
            /// <param name="merchGroupAxis"></param>
            /// <param name="positionPlacementCoord"></param>
            /// <param name="positionPlacementSpace"></param>
            /// <param name="isStartPosition"></param>
            /// <param name="isEndPosition"></param>
            /// <returns></returns>
            /// <remarks>
            /// If the dividers are placed normally, we need to take into account the space
            /// between the position boundaries and the nearest slots on either side, as well
            /// as the thickness of the divider to the right (if the position is in the middle
            /// or on the end when a divider is placed at the end). The thickness of dividers 
            /// on the left is only taken into account when this divider is the first in the 
            /// merchandising group.
            /// </remarks>
            private static CoordinateSpaceChange GetSpaceChangeForDividers(
                Single space,
                PlanogramMerchandisingGroup merchandisingGroup,
                Single dividerSize,
                PlanogramMerchandisingGroupAxis merchGroupAxis,
                Single positionPlacementCoord,
                Single positionPlacementSpace,
                Boolean isStartPosition,
                Boolean isEndPosition)
            {
                // First, we need to calculate the number of divider slots covered.
                Int32 dividerSlotsCovered = GetNumberOfDividerSlotsCovered(
                    dividerSize, merchGroupAxis, positionPlacementCoord, positionPlacementSpace);

                // Next, we need the amount of space covered by this many slots. This is just the number of slots
                // multiplied by the divider spacing. Note that, at this stage, this does not take into account the
                // size of dividers at either end.
                Single dividerAtStartSize =
                    isStartPosition && merchGroupAxis.Strategy != PlanogramPlacementStrategy.Max ?
                    (merchandisingGroup.PlaceStartDivider ? dividerSize : 0) :
                    dividerSize;
                Single spaceOfAllDividerSlots = dividerSlotsCovered * merchGroupAxis.DividerSpacing - dividerAtStartSize;

                // Now, update the space delta based on the difference between the original space and that of all divider slots.
                CoordinateSpaceChange spaceChange = new CoordinateSpaceChange(0, spaceOfAllDividerSlots - space);

                // We need to update the coordinate if our merch strategy is max, because this change is to the 
                // space before the position.
                if (merchGroupAxis.Strategy == PlanogramPlacementStrategy.Max)
                {
                    spaceChange.Coordinate -= spaceOfAllDividerSlots - space;
                }

                // Dividers at the end or in the middle.
                if (isEndPosition && merchandisingGroup.PlaceEndDivider || !isEndPosition)
                {
                    spaceChange.Space += dividerSize;
                    // We don't need to change the coordinate here, because these are at the "end"
                    // or positive axis direction.
                }

                // Dividers at the start.
                if (isStartPosition && merchandisingGroup.PlaceStartDivider)
                {
                    spaceChange.Space += dividerSize;
                    // We need to adjust the coordinate, because this space adjustment is at the "start".
                    spaceChange.Coordinate -= dividerSize;
                }

                // If the position is the first position and there is a difference between the axis min
                // and the divider axis min, then we need to add this space to the co-ordinate delta. This
                // situation is likely to come up with negative overhangs, but not with chests.
                if (isStartPosition && merchGroupAxis.MinOverhang.LessThan(0))
                {
                    spaceChange.Coordinate += merchGroupAxis.MinOverhang;
                }

                // If the position is not the first position and the merchandising group is even,
                // we need to adjust the coordinate delta to take into account the space between the 
                // start of the position and the nearest divider to the left. The space has already
                // taken this into account.
                if (!isStartPosition && merchGroupAxis.Strategy == PlanogramPlacementStrategy.Even)
                {
                    // The coordinate change should be negative, because this is the space before the position's co-ordinate.
                    // We need to take the floor of the divider spacings not covered by the position, multiplied by the
                    // divider spacing to get the amount of space covered and subtract this from the position co-ordinate.
                    // This gives the amount of space from the left hand edge of the position to the nearest divider slot.
                    // We deduct the divider size, because this is accounted for in the space change already calculated.
                    spaceChange.Coordinate -= Convert.ToSingle(
                        positionPlacementCoord
                        - (Math.Floor((positionPlacementCoord - merchGroupAxis.DividerStart) / merchGroupAxis.DividerSpacing) * merchGroupAxis.DividerSpacing)
                        - dividerSize);
                }

                // Finally, we need to ensure that the calculated co-ordinate and space change won't put the space
                // outside of the merchandising group's bounds (this could happen if the merchandising group does not
                // fit a whole number of divider spacings within its length).
                Single adjustedCoord = positionPlacementCoord + spaceChange.Coordinate;
                if ((adjustedCoord + positionPlacementSpace + spaceChange.Space).GreaterThan(merchGroupAxis.Max) &&
                    // This adjustment is only relevant if the actual space does not exceed the merch group's max.
                    // Otherwise, we'll never know that we've exceeded the merch group's space!
                    !(adjustedCoord + positionPlacementSpace).GreaterThan(merchGroupAxis.Max))
                {
                    spaceChange.Space = merchGroupAxis.Max - adjustedCoord - positionPlacementSpace;
                }

                // This also applies to the start of the merchandising space too - we need to check that we're not below the
                // axis min as well.
                adjustedCoord = positionPlacementCoord + spaceChange.Coordinate;
                if(adjustedCoord.LessThan(merchGroupAxis.Min))
                {
                    Single amountByWhichAdjustedCoordIsWrong = merchGroupAxis.Min - adjustedCoord;
                    spaceChange.Coordinate += amountByWhichAdjustedCoordIsWrong;
                    spaceChange.Space -= amountByWhichAdjustedCoordIsWrong;
                }

                return spaceChange;
            }

            private static Int32 GetNumberOfDividerSlotsCovered(Single dividerSize, PlanogramMerchandisingGroupAxis merchGroupAxis, Single positionPlacementCoord, Single positionPlacementSpace)
            {
                Int32 dividerSlotsCovered;
                if (merchGroupAxis.Strategy == PlanogramPlacementStrategy.Even)
                {
                    Single dividerOrigin = merchGroupAxis.MinOverhang.LessThan(0) ? 0 : merchGroupAxis.Min;
                    Single positionCoordRelativeToDividerStartAndOrigin = positionPlacementCoord - dividerOrigin - merchGroupAxis.DividerStart;
                    Single dividerSpacingCoveredByPositionCoord = positionCoordRelativeToDividerStartAndOrigin % merchGroupAxis.DividerSpacing;
                    dividerSlotsCovered = Convert.ToInt32(Math.Ceiling(
                        (dividerSpacingCoveredByPositionCoord + positionPlacementSpace) / merchGroupAxis.DividerSpacing));
                }
                else
                {
                    dividerSlotsCovered = Convert.ToInt32(Math.Ceiling(
                        (dividerSize + positionPlacementSpace) / merchGroupAxis.DividerSpacing));
                }

                return dividerSlotsCovered;
            }

            private static Boolean IsStrategyStackedAndNotManual(PlanogramMerchandisingGroupAxis merchGroupAxis)
            {
                return merchGroupAxis.IsStackedStrategy && merchGroupAxis.Strategy != PlanogramPlacementStrategy.Manual;
            }

            private static Boolean ShouldAccountForDividers(PlanogramMerchandisingGroupAxis merchGroupAxis, Single positionPlacementCoord)
            {
                Boolean accountForDividers = false;
                if (merchGroupAxis.Strategy == PlanogramPlacementStrategy.Min || merchGroupAxis.Strategy == PlanogramPlacementStrategy.Even)
                {
                    accountForDividers = positionPlacementCoord.GreaterOrEqualThan(
                        merchGroupAxis.Min + merchGroupAxis.DividerStart);
                }
                else if (merchGroupAxis.Strategy == PlanogramPlacementStrategy.Max)
                {
                    accountForDividers = positionPlacementCoord.LessOrEqualThan(
                        merchGroupAxis.Max - merchGroupAxis.DividerStart);
                }

                return accountForDividers;
            }

            private static Single GetDividerSize(AxisType axis, PlanogramMerchandisingGroup merchandisingGroup)
            {
                Single dividerSize;
                switch (axis)
                {
                    case AxisType.X:
                        dividerSize = merchandisingGroup.DividerWidth;
                        break;
                    case AxisType.Y:
                        dividerSize = merchandisingGroup.DividerHeight;
                        break;
                    case AxisType.Z:
                        dividerSize = merchandisingGroup.DividerDepth;
                        break;
                    default: // Should never happen.
                        dividerSize = 0;
                        break;
                }

                return dividerSize;
            }
            #endregion
        }

        /// <summary>
        ///     An equality comparer to compare using the placement's Product Gtin.
        /// </summary>
        internal class ByGtinComparer : IEqualityComparer<PlanogramPositionPlacement>
        {
            /// <summary>
            /// Determines whether the specified <see cref="PlanogramPositionPlacement"/> instances are equal.
            /// </summary>
            /// <returns>
            /// true if the specified <see cref="PlanogramPositionPlacement"/> instances have the same Product Gtin; otherwise, false.
            /// </returns>
            /// <param name="x">The first object of type <see cref="PlanogramPositionPlacement"/> to compare.</param><param name="y">The second object of type <see cref="PlanogramPositionPlacement"/> to compare.</param>
            public Boolean Equals(PlanogramPositionPlacement x, PlanogramPositionPlacement y) => x?.Product?.Gtin == y?.Product?.Gtin;

            /// <summary>
            /// Returns a hash code for the specified <see cref="PlanogramPositionPlacement"/> instance.
            /// </summary>
            /// <returns>
            /// A hash code for the specified <see cref="PlanogramPositionPlacement"/> instance.
            /// </returns>
            /// <param name="placement">The <see cref="PlanogramPositionPlacement"/> for which a hash code is to be returned.</param><exception cref="T:System.ArgumentNullException">The type of <paramref name="placement"/> is a reference type and <paramref name="placement"/> is null.</exception>
            public Int32 GetHashCode(PlanogramPositionPlacement placement) => placement?.Product?.Gtin.GetHashCode() ?? -1;
        }

        #endregion

        #region Constants
        #endregion

        #region Fields
        private Stack<State> _stateStack = new Stack<State>();
        private PlanogramPosition _position;
        private PlanogramProduct _product;
        private PlanogramSubComponentPlacement _subComponentPlacement;
        private PlanogramMerchandisingGroup _merchandisingGroup;
        private Boolean _isRelativeToMerchandisingGroup;

        private Object _positionDetailsLock = new Object(); // object used for locking
        private PlanogramPositionDetails _positionDetails; // caches the position details

        private Boolean _isSnappingToPeghole;
        private Object _id;
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Returns a unique id for this placement
        /// </summary>
        public Object Id
        {
            get { return _id; }
        }
        #endregion

        #region Position
        /// <summary>
        /// Returns the position that is being manipulated
        /// </summary>
        public PlanogramPosition Position
        {
            get { return _position; }
            private set
            {
                this.Unsubscribe(_position);
                _position = value;
                _id = _position.Id.GetHashCode();
                this.Subscribe(_position);
                this.ClearCache();
            }
        }
        #endregion

        #region Product
        /// <summary>
        /// Returns the planogram product
        /// </summary>
        public PlanogramProduct Product
        {
            get { return _product; }
            private set { _product = value; }
        }
        #endregion

        #region SubComponentPlacement
        /// <summary>
        /// Returns the sub component placement
        /// </summary>
        public PlanogramSubComponentPlacement SubComponentPlacement
        {
            get { return _subComponentPlacement; }
            set
            {
                _subComponentPlacement = value;

                //if we are already editing then associate the position.
                if (_stateStack.Count != 0)
                {
                    _subComponentPlacement.AssociatePosition(this.Position);
                }

                this.ClearCache();
            }
        }
        #endregion

        #region MerchandisingGroup
        /// <summary>
        /// Returns the merchandising group
        /// details for the positions
        /// </summary>
        public PlanogramMerchandisingGroup MerchandisingGroup
        {
            get { return _merchandisingGroup; }
            private set { _merchandisingGroup = value; }
        }
        #endregion

        #region EditLevel
        /// <summary>
        /// Returns the edit level of this placement
        /// </summary>
        public Int32 EditLevel
        {
            get { return _stateStack.Count; }
        }
        #endregion

        #region IsRelativeToMerchandisingGroup
        /// <summary>
        /// Indicates if the coordinates of this position
        /// are relative to the merchandising group or not
        /// </summary>
        public Boolean IsRelativeToMerchandisingGroup
        {
            get { return _isRelativeToMerchandisingGroup; }
            internal set { _isRelativeToMerchandisingGroup = value; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramPositionPlacement() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramPositionPlacement NewPlanogramPositionPlacement(
            PlanogramPosition position,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            PlanogramPositionPlacement item = new PlanogramPositionPlacement();
            item.Create(
                position,
                product,
                subComponentPlacement,
                merchandisingGroup);
            return item;
        }

        #endregion

        #region Data Access
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramPosition position, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            this.Position = position;
            this.Product = product;
            this.SubComponentPlacement = subComponentPlacement;
            this.MerchandisingGroup = merchandisingGroup;
        }

        #endregion

        #region Methods

        #region Planogram Stuff
        /// <summary>
        /// Returns the position details for this placement
        /// </summary>
        public PlanogramPositionDetails GetPositionDetails()
        {
            if (_positionDetails == null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails == null)
                        _positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this);
                }
            }
            return _positionDetails;
        }

        /// <summary>
        /// Returns a tuple per position facing. 
        /// Val1 = x start, Val2 = facing width.
        /// </summary>
        public List<Tuple<Single, Single>> GetFacingsXAndWidth()
        {
            return this.Position.GetFacingsXAndWidth(this.Product, this.SubComponentPlacement);
        }

        /// <summary>
        /// Sets the position to fill the given height
        /// </summary>
        public void FillHigh(Single targetHeight)
        {
            this.Position.FillHigh(this.Product, this.SubComponentPlacement, this.MerchandisingGroup, targetHeight);
        }

        /// <summary>
        /// Sets the position to fill the given width
        /// </summary>
        public void FillWide(Single targetWidth)
        {
            this.Position.FillWide(this.Product, this.SubComponentPlacement, this.MerchandisingGroup, targetWidth);
        }

        /// <summary>
        /// Sets the position to fill the given depth
        /// </summary>
        public void FillDeep(Single targetDepth)
        {
            this.Position.FillDeep(this.Product, this.SubComponentPlacement, this.MerchandisingGroup, targetDepth);
        }

        /// <summary>
        /// Gets the Planogram relative co-ordinates and the total size of the Position.
        /// </summary>
        /// <returns></returns>
        public RectValue GetMerchandisingGroupRelativeBounds()
        {
            return new RectValue(
                GetMerchandisingGroupRelativeCoordinates(),
                GetPositionDetails().TotalSpace);
        }

        /// <summary>
        /// Gets the planogram relative coordinates
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates()
        {
            if (this.IsRelativeToMerchandisingGroup)
            {
                return this.Position.GetPlanogramRelativeCoordinates(
                    this.MerchandisingGroup.SubComponentPlacements.First());
            }
            else
            {
                return this.Position.GetPlanogramRelativeCoordinates(this.SubComponentPlacement);
            }
        }

        /// <summary>
        /// Calculates the space reserved by the given position placement, that cannot be used
        /// by another position placement and returns it as a RectValue positioned at the bounds
        /// of the position placement's space.
        /// </summary>
        /// <param name="positionPlacement">The position placement to which this position details refers.</param>
        /// <param name="merchandisingGroup">The merchandising group to which the position placement belongs.</param>
        /// <returns>
        /// <para>
        /// A RectValue representing the space occupied by the position placement, positioned at the bounds of the space.
        /// The space calculation takes into account:
        /// </para>
        /// <list type="bullet">
        /// <item>Finger spacing between product facings;</item>
        /// <item>White space between product facings;</item>
        /// <item>Divider placements between product facings;</item>
        /// <item>Unusable inter-slot space at the boundaries of the position;</item>
        /// <item>Divider thickness to the right/top/front of the position, if it is not the final position on the subcomponent;</item>
        /// <item>Divider thickness to the right/top/front of the position, if it is the final position on the subcomponent and dividers are placed at the end;</item>
        /// <item>Divider thickness to the left/bottom/back of the position, if it is the first position on the subcomponent and dividers are placed at the start.</item>
        /// </list>
        /// </returns>
        /// <remarks>If method is called repeatedly and positions placements do not move between calls, provide <param name="positionPlacementsCoords">for performance gains e.g. Autofill.</param></remarks>
        public RectValue GetReservedSpace(IEnumerable<PointValue> positionPlacementsCoords = null)
        {
            if (MerchandisingGroup == null)
            {
                Debug.Fail("MerchandisingGroup was invalid for GetReservedSpace calculation on this PositionPlacement");
                return new RectValue();
            }

            PlanogramPositionDetails positionDetails = GetPositionDetails();

            // Initialise return value to the current total space, which takes into account finger spacing,
            // white space and divider space between facings.
            PointValue occupiedCoordinates = GetMerchandisingGroupRelativeCoordinates();
            WidthHeightDepthValue occupiedSpace = new WidthHeightDepthValue(
                positionDetails.TotalSpace.Width, positionDetails.TotalSpace.Height, positionDetails.TotalSpace.Depth);

            // No adjustment is required to these co-ordinates, because TotalSpace is taken into account
            // when the Merchandising Group places positions.

            // Get the merch group relative coords of the position and all the positions in the group.
            PointValue positionPlacementCoords = occupiedCoordinates;
            if (positionPlacementsCoords == null)
            {
                positionPlacementsCoords = MerchandisingGroup.PositionPlacements
                    .Select(p => p.GetMerchandisingGroupRelativeCoordinates())
                    .ToList();
            }
                 
            // For each axis, we need to add on the space between the boundaries of the position
            // and the nearest divider slots, as well as the thickness of the dividers that might 
            // be positioned on either side. We do this for each axis in turn, because divider
            // placements may vary in each axis.

            CoordinateSpaceChange xChange = CoordinateSpaceChange.GetSpaceChangeForDividers(
                AxisType.X, occupiedSpace.Width, positionPlacementCoords, MerchandisingGroup, positionPlacementsCoords, positionDetails);
            occupiedCoordinates.X += xChange.Coordinate;
            occupiedSpace.Width += xChange.Space;

            CoordinateSpaceChange yChange = CoordinateSpaceChange.GetSpaceChangeForDividers(
                AxisType.Y, occupiedSpace.Height, positionPlacementCoords, MerchandisingGroup, positionPlacementsCoords, positionDetails);
            occupiedCoordinates.Y += yChange.Coordinate;
            occupiedSpace.Height += yChange.Space;

            CoordinateSpaceChange zChange = CoordinateSpaceChange.GetSpaceChangeForDividers(
                AxisType.Z, occupiedSpace.Depth, positionPlacementCoords, MerchandisingGroup, positionPlacementsCoords, positionDetails);
            occupiedCoordinates.Z += zChange.Coordinate;
            occupiedSpace.Depth += zChange.Space;

            return new RectValue(occupiedCoordinates, occupiedSpace);
        }

        /// <summary>
        /// Returns the reserved space value for the given axis.
        /// </summary>
        /// <param name="axis">The axis to return for.</param>
        public Single GetReservedSpace(AxisType axis, IEnumerable<PointValue> positionPlacementsCoords = null)
        {
            switch (axis)
            {
                case AxisType.X:
                    return GetReservedSpace(positionPlacementsCoords).Width;
                case AxisType.Y:
                    return GetReservedSpace(positionPlacementsCoords).Height;
                case AxisType.Z:
                    return GetReservedSpace(positionPlacementsCoords).Depth;
                default:
                    Debug.Fail("Unknown axis type.");
                    return 0.0F;
            }
        }

        /// <summary>
        /// Returns the planogram relative bounding box for this position.
        /// </summary>
        public RectValue GetPlanogramRelativeBoundingBox()
        {
            if (this.IsRelativeToMerchandisingGroup)
            {
                return this.Position.GetPlanogramRelativeBoundingBox(
                    this.MerchandisingGroup.SubComponentPlacements.First(),
                    this.GetPositionDetails());
            }
            else
            {
                return this.Position.GetPlanogramRelativeBoundingBox(this.SubComponentPlacement,
                    this.GetPositionDetails());
            }
        }

        /// <summary>
        /// Returns boundary info for this position placement.
        /// </summary>
        public BoundaryInfo GetPlanogramRelativeBoundaryInfo()
        {
            if (this.IsRelativeToMerchandisingGroup)
            {
                return this.Position.GetBoundaryInfo(
                    this.MerchandisingGroup.SubComponentPlacements.First(),
                    this.GetPositionDetails());
            }
            else
            {
                return this.Position.GetBoundaryInfo(this.SubComponentPlacement,
                    this.GetPositionDetails());
            }
        }

        /// <summary>
        /// Determines if the placement is exceeding any max values for the
        /// supplied axis.
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Boolean IsMaxExceeded(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:

                    //only check right cap if not a break tray up
                    if (!(GetPositionDetails().MainMerchStyle == PlanogramProductMerchandisingStyle.Tray
                        && GetPositionDetails().XMerchStyle == PlanogramProductMerchandisingStyle.Unit))
                    {
                        return Position.IsMaxRightCapExceeded(Product);
                    }
                    break;


                case AxisType.Y:
                    if (Position.IsMaxStackExceeded(Product)) return true;

                    //only check top cap if not a break tray top
                    if (!(GetPositionDetails().MainMerchStyle == PlanogramProductMerchandisingStyle.Tray
                        && GetPositionDetails().YMerchStyle == PlanogramProductMerchandisingStyle.Unit))
                    {
                        return Position.IsMaxTopCapExceeded(Product);
                    }
                    break;


                case AxisType.Z:
                    return Position.IsMaxDeepExceeded(Product);

            }

            return false;
        }
        #endregion

        #region Collisions

        /// <summary>
        /// Returns true if this position collides
        /// with the specified position
        /// </summary>
        public Boolean CollidesWith(PlanogramPositionPlacement otherPos)
        {
            if (Equals(otherPos)) return false;

            PlanogramPositionDetails positionDetails = this.GetPositionDetails();
            PlanogramPositionDetails otherDetails = otherPos.GetPositionDetails();

            Boolean intersectsWith =
                (otherPos.Position.X.LessOrEqualThan((this.Position.X + positionDetails.TotalSpace.Width)) &&
                 ((otherPos.Position.X + otherDetails.TotalSpace.Width).GreaterOrEqualThan(this.Position.X)) &&
                 (otherPos.Position.Y.LessOrEqualThan(this.Position.Y + positionDetails.TotalSpace.Height)) &&
                 ((otherPos.Position.Y + otherDetails.TotalSpace.Height).GreaterOrEqualThan(this.Position.Y)) &&
                 (otherPos.Position.Z.LessOrEqualThan(this.Position.Z + positionDetails.TotalSpace.Depth)) &&
                 ((otherPos.Position.Z + otherDetails.TotalSpace.Depth).GreaterOrEqualThan(this.Position.Z)));

            if (intersectsWith)
            {
                Single intersectX = Math.Max(this.Position.X, otherPos.Position.X);
                Single intersectY = Math.Max(this.Position.Y, otherPos.Position.Y);
                Single intersectZ = Math.Max(this.Position.Z, otherPos.Position.Z);

                Single sizeX =
                    Math.Min(this.Position.X + positionDetails.TotalSpace.Width,
                        otherPos.Position.X + otherDetails.TotalSpace.Width) - intersectX;
                if (sizeX == 0)
                {
                    return false;
                }

                Single sizeY =
                    Math.Min(this.Position.Y + positionDetails.TotalSpace.Height,
                        otherPos.Position.Y + otherDetails.TotalSpace.Height) - intersectY;
                if (sizeY == 0)
                {
                    return false;
                }

                Single sizeZ =
                    Math.Min(this.Position.Z + positionDetails.TotalSpace.Depth,
                        otherPos.Position.Z + otherDetails.TotalSpace.Depth) - intersectZ;
                if (sizeZ == 0)
                {
                    return false;
                }

                return true;
            }
            return false;
        }

        #endregion

        #region Peg Holes

        /// <summary>
        /// Snaps the position to a peg hole
        /// </summary>
        internal void SnapPositionToPegHole()
        {
            if (_isSnappingToPeghole) return;
            _isSnappingToPeghole = true;

            //just return out if this is not a hanging group.
            if (MerchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang) return;

            PlanogramPositionDetails positionDetails = this.GetPositionDetails();
            PlanogramMerchandisingGroupAxis xInfo = this.MerchandisingGroup.XAxis;
            PlanogramMerchandisingGroupAxis yInfo = this.MerchandisingGroup.YAxis;

            Single pegX, pegY;
            GetProductPegXPegY(positionDetails, out pegX, out pegY);

            Single snapX, snapY;
            GetProductPegSnapXandY(positionDetails, out snapX, out snapY);


            #region No Holes
            //deal with components which have no pegholes (i.e bars)
            if (!this.MerchandisingGroup.HasRow1PegHoles && !this.MerchandisingGroup.HasRow2PegHoles)
            {
                Single newSnapY = snapY;
                switch (yInfo.Strategy)
                {
                    case PlanogramPlacementStrategy.Min:
                        newSnapY = yInfo.Min;
                        break;

                    case PlanogramPlacementStrategy.Even:
                        newSnapY = (yInfo.Max - ((yInfo.Max - yInfo.Min) / 2F));
                        break;

                    case PlanogramPlacementStrategy.Max:
                        newSnapY = yInfo.Max;
                        break;

                    case PlanogramPlacementStrategy.Manual:
                        newSnapY = Math.Min(yInfo.Max, snapY);
                        newSnapY = Math.Max(yInfo.Min, newSnapY);
                        break;
                }

                //snap the position y to the new value.
                this.Position.Y = newSnapY + pegY - positionDetails.TotalSpace.Height - this.Product.PegProngOffsetY;


                //we should also make sure that the position x is within the accepted range for the strategy.
                Single newSnapX = snapX;
                switch (xInfo.Strategy)
                {
                    case PlanogramPlacementStrategy.Min:
                        newSnapX = Math.Max(xInfo.Min, newSnapX);
                        break;

                    case PlanogramPlacementStrategy.Max:
                        newSnapX = Math.Min(xInfo.Max, snapX);
                        break;
                }
                if (!MathHelper.EqualTo(newSnapX, snapX))
                {
                    this.Position.X = newSnapX - pegX + this.Product.PegProngOffsetX;
                }


                _isSnappingToPeghole = false;
                return;
            }
            #endregion


            #region Snap Y
            //Here we use subcomponents to get a list of pegs on the Y axis
            List<Single> pegs = new List<Single>();
            foreach (var subComp in this.MerchandisingGroup.SubComponentPlacements.Select(s => s.SubComponent))
            {
                pegs.AddRange(subComp.GetPegHoles().All.Select((p => p.Y)));
            }
            pegs = pegs.Distinct().OrderByDescending(y => y).ToList();

            //Determine the max and min hole values:
            Single maxYHole = this.MerchandisingGroup.GetPegHoleY(0, pegs); // this is the top hole

            Int32 minYHoleNo = 0;
            Single minYHole = maxYHole;
    

            while (minYHole.GreaterThan(yInfo.Min))
            {
                Single newHoleY = this.MerchandisingGroup.GetPegHoleY(minYHoleNo + 1, pegs);
                if (newHoleY == minYHole) break;

                if (newHoleY.GreaterThan(yInfo.Min))
                {
                    minYHole = newHoleY;
                    minYHoleNo++;
                }
                else break;
            }

            //correct the values if any are outside the bounds of the pegboard.
            Boolean adjusted = false;
            if (snapY.GreaterThan(maxYHole))
            {
                snapY = maxYHole;
                adjusted = true;
            }
            if (snapY.LessThan(minYHole))
            {
                snapY = minYHole;
                adjusted = true;
            }

            //check snap to y
            Int32 yHoleNum = 0;
            Single holeY = snapY;
            Single row1YMod = (yInfo.PegRow1Start + snapY)%yInfo.PegRow1Spacing;
            if (adjusted || !(row1YMod.EqualTo(0) && ((xInfo.PegRow1Start + snapX)%xInfo.PegRow1Spacing).EqualTo(0)))
            {
                Boolean hasYSpacing = !yInfo.PegRow1Spacing.EqualTo(0);
                if (yInfo.Strategy == PlanogramPlacementStrategy.Max)
                {
                    //Snap to the closest hole downwards.
                    holeY = maxYHole;
                    while (hasYSpacing && snapY.LessThan(holeY))
                    {
                        yHoleNum++;
                        holeY = this.MerchandisingGroup.GetPegHoleY(yHoleNum, pegs);
                    }
                }
                else if (yInfo.Strategy == PlanogramPlacementStrategy.Min ||
                         yInfo.Strategy == PlanogramPlacementStrategy.Even)
                {
                    //Snap to the closest hole upwards.
                    yHoleNum = minYHoleNo;
                    holeY = minYHole;

                    while (hasYSpacing && snapY.GreaterThan(holeY))
                    {
                        yHoleNum--;
                        holeY = this.MerchandisingGroup.GetPegHoleY(yHoleNum, pegs);
                    }
                }
                else
                {
                    //Snap to the nearest hole by distance.
                    yHoleNum = minYHoleNo;
                    holeY = minYHole;

                    while (hasYSpacing && snapY.GreaterThan(holeY))
                    {
                        yHoleNum--;
                        holeY = this.MerchandisingGroup.GetPegHoleY(yHoleNum, pegs);
                    }

                    //check if we need to snap down again
                    if (yHoleNum != minYHoleNo)
                    {
                        Single prevHolY = this.MerchandisingGroup.GetPegHoleY(yHoleNum + 1, pegs);
                        Single distUp = holeY - snapY;
                        Single distDown = snapY - prevHolY;

                        if (distDown.LessThan(distUp))
                        {
                            holeY = prevHolY;
                            yHoleNum = yHoleNum + 1;
                        }
                    }
                }

                //snap the position y to the new value.
                this.Position.Y = holeY + pegY - positionDetails.TotalSpace.Height - this.Product.PegProngOffsetY;
            }

            #endregion

            #region Snap X

            if (xInfo.PegRow1Spacing > 0)
            {
                SnapPositionToPegHoleX(positionDetails, xInfo, yInfo, pegX, pegY, snapX, snapY, yHoleNum, minYHoleNo, adjusted, holeY);
            }
            else
            {
                SnapPositionToSlotRowX(positionDetails, xInfo, yInfo, pegX, pegY, snapX, snapY, yHoleNum, minYHoleNo, adjusted);
            }
            #endregion

            _isSnappingToPeghole = false;
        }

        /// <summary>
        /// Snaps the position to the nearest peg hole in the X axis
        /// </summary>
        private void SnapPositionToPegHoleX(PlanogramPositionDetails positionDetails,
            PlanogramMerchandisingGroupAxis xInfo,
            PlanogramMerchandisingGroupAxis yInfo,
            Single pegX, Single pegY,
            Single snapX, Single snapY,
            Int32 yHoleNum, Int32 minYHoleNo,
            Boolean adjusted, Single holeY)
        {
            //Here we again obtain a list of peg holes on the x axis
            List<Single> pegs = new List<Single>();
            foreach (var subComp in this.MerchandisingGroup.SubComponentPlacements.Select(s => s.SubComponent))
            {
                pegs.AddRange(subComp.GetPegHoles().All.Where(p => p.Y.EqualTo(holeY)).Select((p => p.X)));
            }
            pegs = pegs.Distinct().OrderBy(x => x).ToList();

            //get min and max x hole.
            Single minHoleX = this.MerchandisingGroup.GetPegHoleX(0, pegs);

            Int32 maxXHoleNo = 1;
            Single maxXHole = minHoleX;

            while (maxXHole < xInfo.Max && maxXHoleNo < pegs.Count)
            {
                Single nextHole = this.MerchandisingGroup.GetPegHoleX(maxXHoleNo++, pegs);
                if (nextHole.GreaterOrEqualThan(xInfo.Max)) break;
                
                maxXHole = nextHole;
            }


            //correct the values if any are outside the bounds of the pegboard.
            if ((xInfo.Strategy == PlanogramPlacementStrategy.Max || xInfo.Strategy == PlanogramPlacementStrategy.Manual ||
                 xInfo.Strategy == PlanogramPlacementStrategy.Even))
            {
                if (snapX.GreaterThan(maxXHole))
                {
                    snapX = maxXHole;
                }

                if ((snapX + positionDetails.TotalSpace.Width - pegX).GreaterThan(xInfo.Max))
                {
                    while ((snapX + positionDetails.TotalSpace.Width - pegX).GreaterThan(xInfo.Max))
                    {
                        if (maxXHoleNo == 0) break;
                        maxXHoleNo--;
                        maxXHole = this.MerchandisingGroup.GetPegHoleX(maxXHoleNo, pegs);
                        snapX = maxXHole;
                    }
                }

                adjusted = true;
            }
            if (xInfo.Strategy != PlanogramPlacementStrategy.Max && snapX.LessThan(minHoleX))
            {
                snapX = minHoleX;
                adjusted = true;
            }

            Single row2YMod = (yInfo.PegRow2Spacing.GreaterThan(0))
                ? (yInfo.PegRow2Start + snapY) % yInfo.PegRow2Spacing
                : 0;
            if (adjusted || !(row2YMod.EqualTo(0) && ((xInfo.PegRow2Start + snapX) % xInfo.PegRow2Spacing).EqualTo(0)))
            {
                //now snap by x
                Int32 xHoleNum = 0;
                Single holeX;

                if (xInfo.Strategy == PlanogramPlacementStrategy.Max)
                {
                    //go left
                    xHoleNum = maxXHoleNo;
                    holeX = maxXHole;

                    while (((holeX - pegX).GreaterThan(this.Position.X)) && (holeX.GreaterThan(minHoleX)))
                    {
                        xHoleNum--;
                        holeX = this.MerchandisingGroup.GetPegHoleX(xHoleNum, pegs);
                    }

                }
                else if (xInfo.Strategy == PlanogramPlacementStrategy.Min ||
                         xInfo.Strategy == PlanogramPlacementStrategy.Even)
                {
                    //go right
                    holeX = this.MerchandisingGroup.GetPegHoleX(xHoleNum, pegs);

                    while ((snapX.GreaterThan(holeX)) && (holeX.LessThan(maxXHole)))
                    {
                        xHoleNum++;
                        holeX = this.MerchandisingGroup.GetPegHoleX(xHoleNum, pegs);
                    }
                }
                else
                {
                    //Snap to the nearest hole by distance.

                    //start by going right
                    holeX = this.MerchandisingGroup.GetPegHoleX(xHoleNum, pegs);
                    while (snapX.GreaterThan(holeX))
                    {
                        xHoleNum++;
                        holeX = this.MerchandisingGroup.GetPegHoleX(xHoleNum, pegs);
                    }

                    //check if we need to snap left again
                    if (yHoleNum != minYHoleNo)
                    {
                        Single prevHolX = this.MerchandisingGroup.GetPegHoleX(xHoleNum - 1, pegs);
                        Single distUp = holeX - snapX;
                        Single distDown = snapX - prevHolX;

                        if (distDown.LessThan(distUp))
                        {
                            holeX = prevHolX;
                        }
                    }
                }

                //snap the positionto the new X value.
                this.Position.X = holeX - pegX + this.Product.PegProngOffsetX;
            }

        }

        /// <summary>
        /// Snaps the position to the slot row in the x axis.
        /// </summary>
        private void SnapPositionToSlotRowX(PlanogramPositionDetails positionDetails,
            PlanogramMerchandisingGroupAxis xInfo,
            PlanogramMerchandisingGroupAxis yInfo,
            Single pegX, Single pegY,
            Single snapX, Single snapY,
            Int32 yHoleNum, Int32 minYHoleNo,
            Boolean adjusted)
        {

            //get min and max x hole.
            Single startX = this.MerchandisingGroup.XAxis.PegRow1Start;
            Single endX = this.MerchandisingGroup.XAxis.Max;

            Single positionStartX = snapX;
            Single positionEndX = snapX - (2 * pegX) + positionDetails.TotalSpace.Width;


            if (xInfo.Strategy == PlanogramPlacementStrategy.Min &&
                positionStartX.LessThan(startX))
            {
                Single neededSpace = startX - positionStartX;
                Single availSpace = endX - positionEndX;

                if (availSpace > neededSpace)
                {
                    snapX = startX;
                    adjusted = true;
                }
            }

            if (xInfo.Strategy == PlanogramPlacementStrategy.Max &&
                positionEndX.GreaterThan(endX))
            {
                Single neededSpace = positionEndX - endX;
                Single availSpace = snapX - startX;

                if (availSpace > neededSpace)
                {
                    snapX = snapX - neededSpace;
                    adjusted = true;
                }
            }
            
                

            if (adjusted)
            {
                //snap the positionto the new X value.
                this.Position.X = snapX - pegX + this.Product.PegProngOffsetX;
            }

        }

        /// <summary>
        /// Returns the peg x and peg y values for the product with
        /// any defaults applied.
        /// </summary>
        private void GetProductPegXPegY(PlanogramPositionDetails posDetails, 
            out Single pegX, out Single pegY)
        {
            PlanogramPositionDetails.GetProductPegXPegY(
                this.Product, posDetails, this.SubComponentPlacement.Planogram,
                out pegX, out pegY);
        }


        /// <summary>
        /// Gets the peg snap coordinates for the top facing relative to the merch group.
        /// </summary>
        internal void GetProductPegSnapXandY(out Single snapX, out Single snapY)
        {
            GetProductPegSnapXandY(GetPositionDetails(), out snapX, out snapY);
        }

        /// <summary>
        /// Gets the peg snap coordinates for the top facing relative to the merch group.
        /// </summary>
        /// <param name="posDetails"></param>
        internal void GetProductPegSnapXandY(PlanogramPositionDetails posDetails, out Single snapX, out Single snapY)
        {
            Single pegX; Single pegY;
            PlanogramPositionDetails.GetProductPegXPegY(
                this.Product, posDetails, this.SubComponentPlacement.Planogram,
                out pegX, out pegY);

            snapY = this.Position.Y + posDetails.TotalSize.Height - pegY + this.Product.PegProngOffsetY;
            snapX = this.Position.X + pegX - this.Product.PegProngOffsetX;
        }


        #endregion

        #region Product Squeeze

        /// <summary>
        /// Squeezes the width of this position to the target given
        /// or the minimum allowed amount.
        /// No change will be made if already below the target.
        /// </summary>
        /// <param name="targetWidth"></param>
        /// <returns></returns>
        public Boolean SqueezeWidthTo(Single targetWidth)
        {
            return this.Position.SqueezeWidthTo(
                 targetWidth,
                 this.Product,
                 this.SubComponentPlacement,
                 this.MerchandisingGroup);
        }

        /// <summary>
        /// Squeezes the height of this position to the target given
        /// </summary>
        public Boolean SqueezeHeightTo(Single targetHeight)
        {
            return this.Position.SqueezeHeightTo(
                  targetHeight,
                  this.Product,
                  this.SubComponentPlacement,
                  this.MerchandisingGroup);
        }

        /// <summary>
        /// Squeezes the depth of this position to the target given
        /// </summary>
        public Boolean SqueezeDepthTo(Single targetDepth)
        {
            return this.Position.SqueezeDepthTo(
                targetDepth,
                this.Product,
                this.SubComponentPlacement,
                this.MerchandisingGroup);
        }

        #endregion

        #region Begin/Cancel/Apply

        /// <summary>
        /// Begins a new edit operation on this placement
        /// </summary>
        public void BeginEdit()
        {
            // push the current position and sub component placement
            // onto our state stack
            _stateStack.Push(new State(this));

            // now change the current position to a clone
            // of the existing position so that any changes
            // do not modify the original position
            this.Position = this.Position.GetOptimisationClone();


            //if this is the first edit, then make sure that the position is associated now.
            if (_stateStack.Count == 1
                && this.SubComponentPlacement != null
                && !this.SubComponentPlacement.IsPositionAssociated(this.Position))
            {
                this.SubComponentPlacement.AssociatePosition(this.Position);
                this.ClearCache();
            }
        }

        /// <summary>
        /// Cancels the current edit operation
        /// </summary>
        public void CancelEdit()
        {
            // ensure we have a state to roll back to
            if (_stateStack.Count == 0) return;

            // pop the last state of the stack
            // and apply back to the placement
            State state = _stateStack.Pop();

            // apply the state to this position
            this.Position = state.Position;
            this.SubComponentPlacement = state.SubComponentPlacement;
        }

        /// <summary>
        /// Applies all changes to this placement
        /// </summary>
        public void ApplyEdit()
        {
            // get the initial state from the state stack
            State state = null;
            while (_stateStack.Count > 0)
            {
                state = _stateStack.Pop();
            }

            // if the state is null, then there is nothing to do
            if (state == null) return;

            // get the position from the state,
            // apply the changes, and set as the current position
            PlanogramPosition position = state.Position;

            // Resolve the parent of the position if it doesn't have one.
            if (position.Parent == null)
            {
                SubComponentPlacement.Planogram.Positions.Add(position);
            }

            position.CopyState(this.Position);
            this.Position = position;

            // ensure that the position is associated to
            // the current sub component placement
            this.SubComponentPlacement.AssociatePosition(this.Position);
        }

        /// <summary>
        /// Resets all pending edits on this position placement
        /// </summary>
        public void Reset()
        {
            // only need to reset if the state stack
            // count is greater than zero
            if (_stateStack.Count <= 0) return;

            // get the initial state from the state stack
            State state = null;
            while (_stateStack.Count > 0)
            {
                state = _stateStack.Pop();
            }

            //  Nothing to reset if the next state was invalid.
            if (state == null) return;

            // reset the position placement
            this.Position = state.Position;
            this.SubComponentPlacement = state.SubComponentPlacement;

            // ensure that the position is associated to
            // the current sub component placement
            this.SubComponentPlacement.AssociatePosition(this.Position);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Determines if this instance is consecutive in sequence with the given <paramref name="positionPlacement"/>.
        /// Two positions are considered adjacent if they have the same sequence in two directions and a difference
        /// of sequence values in the third direction of no more than one.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns>True if the positions are consecutive.</returns>
        public Boolean IsConsecutiveInSequenceWith(PlanogramPositionPlacement positionPlacement)
        {
            Boolean sameSequenceX = this.Position.SequenceX == positionPlacement.Position.SequenceX;
            Boolean sameSequenceY = this.Position.SequenceY == positionPlacement.Position.SequenceY;
            Boolean sameSequenceZ = this.Position.SequenceZ == positionPlacement.Position.SequenceZ;

            if (sameSequenceX && sameSequenceY)
            {
                if (Math.Abs(this.Position.SequenceZ - positionPlacement.Position.SequenceZ) == 1)
                {
                    return true;
                }
            }
            else if (sameSequenceX && sameSequenceZ)
            {
                if (Math.Abs(this.Position.SequenceY - positionPlacement.Position.SequenceY) == 1)
                {
                    return true;
                }
            }
            else if (sameSequenceY && sameSequenceZ)
            {
                if (Math.Abs(this.Position.SequenceX - positionPlacement.Position.SequenceX) == 1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Subscribes to events related to the position
        /// </summary>
        private void Subscribe(PlanogramPosition position)
        {
            if (position != null)
            {
                position.PropertyChanged += Position_PropertyChanged;
            }
        }

        /// <summary>
        /// Unsubscribe from events related to the position
        /// </summary>
        private void Unsubscribe(PlanogramPosition position)
        {
            if (position != null)
            {
                position.PropertyChanged -= Position_PropertyChanged;
            }
        }

        /// <summary>
        /// Returns the original position instance
        /// </summary>
        /// <returns></returns>
        internal PlanogramPosition GetOriginalState()
        {
            if (this.EditLevel > 0)
            {
                return _stateStack.ToArray()[0].Position;
            }
            else
            {
                return this.Position;
            }
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when a property changes on the position
        /// </summary>
        private void Position_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.MerchandisingGroup == null)
            {
                this.Unsubscribe((PlanogramPosition)sender);
                return;
            }

            // if the coordinates of the position changes then
            // ensure the position is snapped to peg holes
            if ((e.PropertyName == PlanogramPosition.XProperty.Name) ||
                (e.PropertyName == PlanogramPosition.YProperty.Name) ||
                (e.PropertyName == PlanogramPosition.ZProperty.Name))
            {
                this.SnapPositionToPegHole();
            }

            // clear the position details cache if required
            if ((e.PropertyName != PlanogramPosition.XProperty.Name) &&
                (e.PropertyName != PlanogramPosition.YProperty.Name) &&
                (e.PropertyName != PlanogramPosition.ZProperty.Name) &&
                (e.PropertyName != PlanogramPosition.SequenceProperty.Name) &&
                (e.PropertyName != PlanogramPosition.SequenceXProperty.Name) &&
                (e.PropertyName != PlanogramPosition.SequenceXProperty.Name) &&
                (e.PropertyName != PlanogramPosition.SequenceXProperty.Name))
            {
                this.ClearCache();
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        public void Dispose()
        {
            this.Unsubscribe(this.Position);
        }
        #endregion

        #region Position Options

        #region Units

        #region Increase

        /// <summary>
        /// Increases units for this position to the next increment
        /// </summary>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns>True if successful, else false</returns>
        public Boolean IncreaseUnits(IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return Position.IncreaseUnits(Product, SubComponentPlacement, MerchandisingGroup, assortmentRuleEnforcer);
        }

        /// <summary>
        /// Increases units for this position to the next increment
        /// </summary>
        /// <param name="axis">The axis to increase for.</param>
        /// <returns>True if successful, else false</returns>
        public Boolean IncreaseUnits(AxisType axis)
        {
            return Position.IncreaseUnits(axis, Product, SubComponentPlacement, MerchandisingGroup);
        }

        #endregion

        #region Decrease

        /// <summary>
        /// Decreases units for this position to the next decrement
        /// </summary>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns>True if successful, else false</returns>
        public Boolean DecreaseUnits(IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return Position.DecreaseUnits(Product, SubComponentPlacement, MerchandisingGroup, assortmentRuleEnforcer);
        }

        /// <summary>
        /// Decreases units for this position to the next decrement
        /// </summary>
        /// <returns>True if successful, else false</returns>
        public Boolean DecreaseUnits(AxisType axis)
        {
            return Position.DecreaseUnits(axis, Product, SubComponentPlacement, MerchandisingGroup);
        }

        #endregion

        #region Set

        /// <summary>
        /// Sets the units to match the specified target units
        /// </summary>
        /// <param name="targetUnits">The target units</param>
        /// <param name="targetType">The target type</param>
        /// <param name="assortmentRuleEnforcer"></param>
        public void SetUnits(
            Int32 targetUnits, 
            PlanogramPositionInventoryTargetType targetType, 
            AssortmentRuleEnforcer assortmentRuleEnforcer,
            PlanogramMerchandisingInventoryChangeType inventoryChangeType = PlanogramMerchandisingInventoryChangeType.ByUnits)
        {
            Position.SetUnits(
                Product, 
                SubComponentPlacement, 
                MerchandisingGroup, 
                targetUnits, 
                targetType, 
                assortmentRuleEnforcer, 
                inventoryChangeType);
        }

        /// <summary>
        /// Sets the mimum possible units for the position, based on information known about the product and the paramters given.
        /// </summary>
        /// <remarks>Does not shrink manually placed products unless you explicitly state to do so.</remarks>
        public void SetMinimumUnits(Boolean applyOnX = true,
                                    Boolean applyOnY = true,
                                    Boolean applyOnZ = true,
                                    Boolean removeCaps = true,
                                    Boolean processManuallyPlaced = false,
                                    AssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            //  If manually placed are not to be processed, ignore any manually placed.
            if (!processManuallyPlaced &&
                Position.IsManuallyPlaced) return;

            Position.SetMinimumUnits(Product, applyOnX, applyOnY, applyOnZ, removeCaps);
        }

        #endregion

        #region Recalculate
        /// <summary>
        /// Recalculates the total units for the <see cref="Position"/>.
        /// </summary>
        public void RecalculateUnits()
        {
            _positionDetails = null;
            this.Position.RecalculateUnits(GetPositionDetails());
        }

        #endregion

        #endregion

        #region Facings

        #region Increase
        /// <summary>
        /// Increases facings for this position
        /// </summary>
        /// <returns>True if successful, else false</returns>
        public Boolean IncreaseFacings(IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return this.Position.IncreaseFacings(
                this.Product,
                this.SubComponentPlacement,
                this.MerchandisingGroup,
                assortmentRuleEnforcer);
        }
        #endregion

        #region Decrease
        /// <summary>
        /// Decreases facings for this position
        /// </summary>
        /// <returns>True if successful, else false</returns>
        public Boolean DecreaseFacings( IAssortmentRuleEnforcer assortmentRulenEnforcer = null)
        {
            return this.Position.DecreaseFacings(
                this.Product,
                this.SubComponentPlacement,
                this.MerchandisingGroup,
                assortmentRulenEnforcer);
        }
        #endregion

        #region Caps

        /// <summary>
        /// Remove all caps along all axis.
        /// </summary>
        /// <remarks>Currently just sets all cap facings = 0 for all axis.</remarks>
        public void RemoveCaps()
        {
            this.Position.RemoveAllCaps();
        }

        #region Increase Caps

        /// <summary>
        ///     Try increasing cap units on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The axis to increase cap units on for this instance.</param>
        /// <returns><c>True</c> if cap units could be increased. <c>False</c> otherwise.</returns>
        public Boolean TryIncreaseCapUnits(AxisType axis)
        {
            return Position.TryIncreaseCapUnits(axis, Product, SubComponentPlacement, MerchandisingGroup);
        }

        #endregion

        #region Decrease Caps

        /// <summary>
        ///     Try decreasing cap units on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The axis to decrease cap units on for this instance.</param>
        /// <returns><c>True</c> if cap units could be decreased. <c>False</c> otherwise.</returns>
        public Boolean TryDecreaseCapUnits(AxisType axis)
        {
            return Position.TryDecreaseCapUnits(axis, Product, SubComponentPlacement, MerchandisingGroup);
        }

        #endregion

        #endregion

        #region Helpers
        /// <summary>
        /// Returns the number of facings for this position
        /// in the axis based on the sub component merch type
        /// </summary>
        public Int32 GetFacings()
        {
            return this.Position.GetFacings(this.SubComponentPlacement);
        }
        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Checks if the current position is placed in such a way that we expect it to exist on a peg.
        /// </summary>
        /// <returns>True when displayed on a peg.</returns>
        public Boolean IsAPeggedPosition()
        {
            if (Product.NumberOfPegHoles > 0 && Product.PegDepth > 0
                && SubComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Enumerates all the peg hole coordinates for each facing of the <c>Placement</c>.
        /// </summary>
        /// <returns>List of <see cref="PointValue"/> representing each <c>Facing</c>'s peg hole coordinates.</returns>
        /// <remarks>If the <c>Merchandising Group</c> has no pegs an empty list will be returned.</remarks>
        public IEnumerable<PointValue> GetPegHoles()
        {
            var pointValues = new List<PointValue>();
            if (!MerchandisingGroup.HasRow1PegHoles && !MerchandisingGroup.HasRow2PegHoles)
                return pointValues; //return straight out if the block doesnt have pegholes

            PlanogramPositionDetails positionDetails = GetPositionDetails();

            for (var x = 0; x < Position.FacingsWide; x++)
            {
                Single pegX, pegY;
                GetProductPegXPegY(positionDetails, out pegX, out pegY);

                Single snapX = Position.X + x*(Product.Width + positionDetails.FacingSize.Width) + pegX + this.Product.PegProngOffsetX;

                for (var y = 0; y < Position.FacingsHigh; y++)
                {
                    Single snapY = Position.Y + positionDetails.TotalSpace.Height -
                                   y*(Product.Height + positionDetails.FacingSize.Height) - pegY - this.Product.PegProngOffsetY;
                    pointValues.Add(new PointValue(snapX, snapY, 0));
                }
            }

            return pointValues;
        }

        /// <summary>
        ///    Calculates the available space on the given <paramref name="axis"/> after taking into account all the <c>Reserved Space</c> used up by the positions on the same row (sharing sequence in the normal axes to the requested one).
        /// </summary>
        /// <param name="axis">The specific axis on which to calculate available space.</param>
        /// <returns>The space left over on the given <paramref name="axis"/> on the <c>Placement</c>'s <c>Merchandising Group</c> after substracting used space by positions.</returns>
        /// <remarks>This method calculates linear available space based on the <c>Placement</c>'s <c>Merchandising Group</c>'s dimensions.</remarks>
        public Single GetAvailableSpaceOnAxis(AxisType axis)
        {
            //  Determine total space for the given axis.
            PlanogramMerchandisingGroup merchandisingGroup = MerchandisingGroup;
            List<PlanogramPositionPlacement> positionsAtSameSequence =
                merchandisingGroup.GetPositionsAtSameSequence(this, axis.GetNormals().ToArray()).ToList();
            positionsAtSameSequence.Add(this);
            Single totalSpace = 0;
            Single usedSpace = 0;
            switch (axis)
            {
                case AxisType.X:
                    totalSpace = merchandisingGroup.MaxX - merchandisingGroup.MinX;
                    usedSpace = positionsAtSameSequence.Sum(p => p.GetReservedSpace().Width);
                    break;
                case AxisType.Y:
                    totalSpace = merchandisingGroup.MaxY - merchandisingGroup.MinY;
                    usedSpace = positionsAtSameSequence.Sum(p => p.GetReservedSpace().Height);
                    break;
                case AxisType.Z:
                    totalSpace = merchandisingGroup.MaxZ - merchandisingGroup.MinZ;
                    usedSpace = positionsAtSameSequence.Sum(p => p.GetReservedSpace().Depth);
                    break;
                default:
                    System.Diagnostics.Debug.Fail("Type not recognized");
                    break;
            }

            return totalSpace - usedSpace;
        }

        /// <summary>
        ///     Check whether the placement is outside of its merchandising space or not.
        /// </summary>
        public Boolean IsOutsideMerchandisingSpace()
        {
            var merchandisingBounds =
                new RectValue(new PointValue(MerchandisingGroup.MinX, MerchandisingGroup.MinY, MerchandisingGroup.MinZ),
                    new WidthHeightDepthValue(MerchandisingGroup.MaxX - MerchandisingGroup.MinX,
                        MerchandisingGroup.MaxY - MerchandisingGroup.MinY,
                        MerchandisingGroup.MaxZ - MerchandisingGroup.MinZ));
            Boolean isHangingPosition = SubComponentPlacement.SubComponent.MerchandisingType ==
                                        PlanogramSubComponentMerchandisingType.Hang
                                        ||
                                        SubComponentPlacement.SubComponent.MerchandisingType ==
                                        PlanogramSubComponentMerchandisingType.HangFromBottom;
            return isHangingPosition
                ? !merchandisingBounds.IntersectsWith(GetReservedSpace())
                : !merchandisingBounds.Contains(GetReservedSpace());
        }

        /// <summary>
        /// Checks the overfill on a hang position that exists on a component without pegholes. For example a bar. 
        /// </summary>
        /// <returns>True if overfilled.</returns>
        public Boolean DoesPositionOverfillXAxisForSubcomponentWithoutPegholes()
        {
            // This method should only be used for hang types that don't have pegs.
            if (this.MerchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang) return false;

            List<PlanogramSubComponentPlacement> combinedSubsOrderedByX =
                this.MerchandisingGroup.SubComponentPlacements.OrderBy(cs => cs.SubComponent.X).ToList();

            //  V8-31385 Need to account for any overhang
            PlanogramSubComponent firstSubcomponent = combinedSubsOrderedByX.First().SubComponent;
            float xMinCombinedSubs = firstSubcomponent.X - firstSubcomponent.LeftOverhang;
            float xMaxCombinedSubs = firstSubcomponent.X + combinedSubsOrderedByX.Sum(sc => sc.SubComponent.Width) + firstSubcomponent.RightOverhang;

            PlanogramPositionDetails positonDetails = this.GetPositionDetails();

            IEnumerable<PointValue> pegHolesOrderedByX = this.GetPegHoles().OrderBy(ph => ph.X);

            if (!pegHolesOrderedByX.Any() || (this.Product.PegX == 0))
            {
                // Mid point and above on first facing in group on x must intersect with the first subcomponent on x.
                if ((this.Position.X) < xMinCombinedSubs) return true;

                // Mid point and bellow on last facing in group on x must intersect with the last subcomponent on x.
                float lastFacingStartX = this.Position.X +positonDetails.TotalSize.Width;
                if (lastFacingStartX > xMaxCombinedSubs) return true;
            }
            else
            {
                // Mid point and above on first facing in group on x must intersect with the first subcomponent on x.
                if ((this.Position.X) <
                    xMinCombinedSubs - (positonDetails.MainRotatedUnitSize.Width - this.Product.PegX)) return true;

                // Mid point and bellow on last facing in group on x must intersect with the last subcomponent on x.
                float lastFacingStartX = this.Position.X + positonDetails.TotalSize.Width - this.Product.PegX;
                if (lastFacingStartX > xMaxCombinedSubs) return true;
            }

            // If could not prove otherwise we asume didn't overflow.
            return false;
        }

        /// <summary>
        /// Applies the given <paramref name="positionPresentation"/> state to this instance's <see cref="Position"/>.
        /// </summary>
        /// <param name="positionPresentation"></param>
        public void ApplyPositionPresentation(PlanogramMerchandisingPositionPresentationState positionPresentation)
        {
            Position.MerchandisingStyle = positionPresentation.MerchandisingStyle;
            Position.MerchandisingStyleX = positionPresentation.MerchandisingStyleX;
            Position.MerchandisingStyleY = positionPresentation.MerchandisingStyleY;
            Position.MerchandisingStyleZ = positionPresentation.MerchandisingStyleZ;
            Position.OrientationType = positionPresentation.OrientationType;
            Position.OrientationTypeX = positionPresentation.OrientationTypeX;
            Position.OrientationTypeY = positionPresentation.OrientationTypeY;
            Position.OrientationTypeZ = positionPresentation.OrientationTypeZ;
            Position.FacingsWide = positionPresentation.FacingsWide;
            Position.FacingsHigh = positionPresentation.FacingsHigh;
            Position.FacingsDeep = positionPresentation.FacingsDeep;
            Position.FacingsXWide = positionPresentation.FacingsXWide;
            Position.FacingsXHigh = positionPresentation.FacingsXHigh;
            Position.FacingsXDeep = positionPresentation.FacingsXDeep;
            Position.FacingsYWide = positionPresentation.FacingsYWide;
            Position.FacingsYHigh = positionPresentation.FacingsYHigh;
            Position.FacingsYDeep = positionPresentation.FacingsYDeep;
            Position.FacingsZWide = positionPresentation.FacingsZWide;
            Position.FacingsZHigh = positionPresentation.FacingsZHigh;
            Position.FacingsZDeep = positionPresentation.FacingsZDeep;
        }

        #endregion

        /// <summary>
        ///     Encapsulates a call to the contained <see cref="PlanogramPosition"/> instance to enforce Assortment Rules.
        /// </summary>
        /// <param name="assortmentRuleEnforcer">[Optional] Instance that implements <see cref="IAssortmentRuleEnforcer"/> to be used in the enforcing of rules. If none is provided, a new one will be created.</param>
        /// <returns></returns>
        public Boolean EnforceRules(IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            if (assortmentRuleEnforcer == null) assortmentRuleEnforcer = new AssortmentRuleEnforcer();

            return Position.EnforceRules(Product, SubComponentPlacement, MerchandisingGroup, assortmentRuleEnforcer);
        }

        #endregion

        #region Overrides
        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instance are equal
        /// </summary>
        public override bool Equals(Object obj)
        {
            PlanogramPositionPlacement other = obj as PlanogramPositionPlacement;
            if (other != null)
            {
                return other.Id == this.Id;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Relative Coordinates
        /// <summary>
        /// Returns the coordinates of this position
        /// relative to the merchandising group
        /// </summary>
        public PointValue GetMerchandisingGroupRelativeCoordinates()
        {
            if (IsRelativeToMerchandisingGroup)
            {
                return new PointValue(Position.X, Position.Y, Position.Z);
            }
            else
            {
                // In order to transform the position's coords into merch group relative space, we just need to adjust
                // the X coordinate for the width of the subcomponents before this one.
                Single cumulativeWidth = 0f;
                foreach (PlanogramSubComponentPlacement subCompPlacement in MerchandisingGroup.SubComponentPlacements)
                {
                    if (subCompPlacement == this.SubComponentPlacement) break;
                    cumulativeWidth += subCompPlacement.SubComponent.Width;
                }
                return new PointValue(Position.X + cumulativeWidth, Position.Y, Position.Z);
            }
        }

        /// <summary>
        /// Returns the Design View Position of this Sub-Component to which this placement relates.
        /// </summary>
        /// <returns></returns>
        public DesignViewPosition GetDesignViewPosition()
        {
            Single height;
            Single width;
            Single offset;
            this.SubComponentPlacement.Planogram.GetBlockingAreaSize(out height, out width, out offset);
            return new DesignViewPosition(this, offset);
        }

        #endregion

        #region Caching

        /// <summary>
        /// Clears the position placement cache
        /// </summary>
        private void ClearCache()
        {
            if (_positionDetails != null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails != null)
                    {
                        _positionDetails = null;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region DebuggerDisplay Methods

        internal String DebugValue()
        {
            String debugCoords = String.Format("(X={0}, Y={1}, Z={2})", Position.X, Position.Y, Position.Z);
            String debugFacings = String.Format("(FW={0}, FH={1}, FD={2})", Position.FacingsWide, Position.FacingsHigh, Position.FacingsDeep);
            return String.Format("{0} | {1}", debugCoords, debugFacings);
        }

        internal String DebugName()
        {
            RecalculateUnits();
            String debugCoords = String.Format("{0}", Product.Gtin);
            String debugFacings = String.Format("Units={0}(UW={1}, UH={2}, UD={3})",
                                                Position.TotalUnits,
                                                Position.UnitsWide,
                                                Position.UnitsHigh,
                                                Position.UnitsDeep);
            return String.Format("{0} | {1}", debugCoords, debugFacings);
        }

        #endregion

        #region IPlanogramSubComponentSequencedItem Members

        /// <summary>
        /// The sequence value of the item in the X direction.
        /// </summary>
        Int16 IPlanogramSubComponentSequencedItem.SequenceX
        {
            get { return Position.SequenceX; }
        }

        /// <summary>
        /// The sequence value of the item in the Y direction.
        /// </summary>
        Int16 IPlanogramSubComponentSequencedItem.SequenceY
        {
            get { return Position.SequenceY; }
        }

        /// <summary>
        /// The sequence value of the item in the Z direction.
        /// </summary>
        Int16 IPlanogramSubComponentSequencedItem.SequenceZ
        {
            get { return Position.SequenceZ; }
        }

        /// <summary>
        /// Gets the <see cref="PlanogramSubComponentPlacement"/> that this item is sequenced on.
        /// </summary>
        /// <returns></returns>
        PlanogramSubComponentPlacement IPlanogramSubComponentSequencedItem.GetParentSubComponentPlacement()
        {
            return SubComponentPlacement;
        }

        #endregion
    }
}