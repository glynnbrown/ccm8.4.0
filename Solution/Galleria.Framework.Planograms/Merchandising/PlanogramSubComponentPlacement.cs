﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
// V8-27477 : L.Ineson
//  Added more world space helpers.
// V8-27738 : N.Foster
//  Changed to a POCO for performance
// V8-27742 : A.Silva
//  Added IsOverfilled check calculation.
// V8-22738 : N.Foster
//  Overrided GetHashCode and Equals
// V8-28069 : L.Ineson
//  Corrections to GetCombinedWithList
// V8-26850 : A.Probyn
//  Added defensive code to Create(PlanogramPosition position)
// V8-27959 : A.Silva
//  Amended IsOverfilled to ignored width-wise overfilling for Hang or HangFromBottom components.
//  Amended GetWorldMerchandisingSpaceBounds for HangFromBottom case so that the merchandising volume is calculated correctly.
// V8-28458 : A.Kuszyk
//  ResequencePositionsByX amended to account for HangFromBottom components.
// V8-28567 : D.Pleasance
//  Amended GetWorldMerchandisingSpaceBounds to use Collides with method instead of IntersectsWith
// V8-28613 : A.Silva
//  Amended GetWorldMerchandisingSpaceBounds Hang to make sure it starts at the bottom of the fixture when no merchandising height is set.
//  Amended GetWorldMerchandisingSpaceBounds HangFromBottom to make sure it calculates Depth (was setting it to 0 always).
#endregion
#region Version History: CCM801
// V8-29028 : L.Ineson
//  Amended GetWorldMerchandisingSpaceBounds rules on how merch height is applied.
//  Corrected equality checks on create from subcomponent.
// V8-29291 : L.Ineson
//  Added extra new factory methods.
#endregion
#region Version History: CCM803
// V8-29589 : A.Silva
//  Amended GetCombinedWithList to make sure all the subcomponents in a combined set return the same set, regardless of where in the set they are.
// V8-29622 : L.Ineson
//  Moved sequencing methods to the merch group.
#endregion
#region Version History: CCM810
// V8-29192 : L.Ineson
//  Removed old IsOverfilled method - should use ones against merch group instead.
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-30103 : J.Pickup
// GetCombinedWithList() now retrieves a cached combined with list in order to improve performance - huge benefit when tested with loupe.
// V8-30085 : A.Kuszyk
//  Added GetPlanogramRelativeTranslation method and rolled back changes from 30103.
// V8-30034 : A.Silva
//  Added GetIncreaseAxesStack to return the preferred axis order in which to increase units.
// V8-30103 : N.Foster
//  Autofill performance enhancements
#endregion
#region Version History: CCM811
// V8-30334 : A.Silva
// Amended GetWorldMerchandisingSpaceBounds to require the collection of sub component placements that might obstruct this one.
//  This allows to optimize the use of the method when looping through several sub components belonging to the same planogram.
// V8-30456 : A.Kuszyk
//  Updated GetWorldMerchandisingSpaceBounds to use the rotated merchandising space.
#endregion
#region Version History: CCM820
// V8-30818 : N.Foster
//  Performance improvements
// V8-30936 : M.Brumby
//  Added slotwall
// V8-30920 : A.Kuszyk
//  Added MoveFixtureComponent method.
#endregion
#region Version History: CCM830
//V8-31727 : L.Ineson
//  Amended ToLocal method to actually consider the local subcomponent xyz.
// V8-32523 : L.Ineson
//  Corrected IsAnnotationAssociated check.
// V8-32539 : A.Kuszyk
//  Implemented IPLanogramSequencableItem interface.
// CCM-13761 : L.Ineson
//  Added GetPlanogramRelativeBoundaryInfo
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Tracks the placement of a sub component
    /// within the fixture
    /// </summary>
    [Serializable]
    public class PlanogramSubComponentPlacement : IPlanogramSequencableItem
    {
        #region Fields

        private PlanogramSubComponentPlacementId _id;
        private Planogram _planogram;
        private PlanogramSubComponent _subComponent;
        private PlanogramComponent _component;
        private PlanogramAssemblyComponent _assemblyComponent;
        private PlanogramAssembly _assembly;
        private PlanogramFixtureAssembly _fixtureAssembly;
        private PlanogramFixture _fixture;
        private PlanogramFixtureComponent _fixtureComponent;
        private PlanogramFixtureItem _fixtureItem;

        private Object _planogramRelativeBoundingBoxLock = new Object();
        private RectValue? _planogramRelativeBoundingBox = null;
        private BoundaryInfo _planRelativeBoundaryInfo;
        private Object _planRelativeBoundaryInfoLock = new Object();

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Returns a unique id for the sub component placement
        /// </summary>
        public Object Id
        {
            get { return _id; }
        }

        #endregion

        #region Planogram
        /// <summary>
        /// Returns the parent planogram
        /// </summary>
        public Planogram Planogram
        {
            get { return _planogram; }
        }
        #endregion

        #region SubComponent
        /// <summary>
        /// Returns the planogram sub component
        /// </summary>
        public PlanogramSubComponent SubComponent
        {
            get { return _subComponent; }
        }
        #endregion

        #region Component
        /// <summary>
        /// Returns the planogram component
        /// </summary>
        public PlanogramComponent Component
        {
            get { return _component; }
        }
        #endregion

        #region AssemblyComponent
        /// <summary>
        /// Returns the planogram assembly component
        /// </summary>
        public PlanogramAssemblyComponent AssemblyComponent
        {
            get { return _assemblyComponent; }
        }
        #endregion

        #region Assembly
        /// <summary>
        /// Returns the planogram assembly
        /// </summary>
        public PlanogramAssembly Assembly
        {
            get { return _assembly; }
        }
        #endregion

        #region FixtureAssembly
        /// <summary>
        /// Returns the planogram fixture assembly
        /// </summary>
        public PlanogramFixtureAssembly FixtureAssembly
        {
            get { return _fixtureAssembly; }
        }
        #endregion

        #region Fixture
        /// <summary>
        /// Returns the planogram fixture
        /// </summary>
        public PlanogramFixture Fixture
        {
            get { return _fixture; }
        }
        #endregion

        #region FixtureComponent
        /// <summary>
        /// Returns the planogram fixture component
        /// </summary>
        public PlanogramFixtureComponent FixtureComponent
        {
            get { return _fixtureComponent; }
        }
        #endregion

        #region FixtureItem
        /// <summary>
        /// Returns the planogram fixture item
        /// </summary>
        public PlanogramFixtureItem FixtureItem
        {
            get { return _fixtureItem; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramSubComponentPlacement() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramSubComponent subComponent)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(subComponent);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramSubComponent subComponent,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(subComponent, fixtureItem, fixtureComponent);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramSubComponent subComponent,
            Planogram planogram, PlanogramFixtureItem fixtureItem, PlanogramFixture fixture,
            PlanogramFixtureComponent fixtureComponent, PlanogramComponent component)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(subComponent, planogram, fixtureItem, fixture, fixtureComponent, component);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramSubComponent subComponent,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, PlanogramAssemblyComponent assemblyComponent)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(subComponent, fixtureItem, fixtureAssembly, assemblyComponent);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramSubComponent subComponent,
            Planogram planogram, PlanogramFixtureItem fixtureItem, PlanogramFixture fixture,
            PlanogramFixtureAssembly fixtureAssembly, PlanogramAssembly assembly,
            PlanogramAssemblyComponent assemblyComponent, PlanogramComponent component)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(subComponent, planogram, fixtureItem, fixture, fixtureAssembly, assembly, assemblyComponent, component);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponentPlacement NewPlanogramSubComponentPlacement(PlanogramPosition position)
        {
            PlanogramSubComponentPlacement item = new PlanogramSubComponentPlacement();
            item.Create(position);
            return item;
        }
        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramSubComponent subComponent)
        {
            _subComponent = subComponent;
            _component = subComponent.Parent;
            _planogram = _component.Parent;

            // as we have only been provided with
            // the sub component details, we must
            // try and locate the correct details
            // for the component

            // we first try and locate if the component
            // is directly associated to a fixture
            // if not, then attempt to locate the first
            // fixture assembly that the component is
            // associated with

            // go through the planogram fixture items
            foreach (PlanogramFixtureItem fixtureItem in _planogram.FixtureItems.OrderBy(i => i.BaySequenceNumber))
            {
                // get the associated fixture for this fixture item
                PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();

                // determine if the component is directly associated to the fixture
                PlanogramFixtureComponent fixtureComponent = fixture.Components
                    .Where(i => Object.Equals(i.PlanogramComponentId, _component.Id))
                    .OrderBy(i => i.X)
                    .ThenBy(i => i.Y)
                    .ThenBy(i => i.Z)
                    .FirstOrDefault();

                if (fixtureComponent != null)
                {
                    // the component is associated to a fixture component
                    _fixtureItem = fixtureItem;
                    _fixture = fixture;
                    _fixtureComponent = fixtureComponent;

                    // and return
                    return;
                }

                // if we got this far, then we need to search through
                // assemblies to find if the component is associated to this fixture
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies.OrderBy(i => i.X).ThenBy(i => i.Y).ThenBy(i => i.Z))
                {
                    // get the planogram assembly
                    PlanogramAssembly assembly = fixtureAssembly.GetPlanogramAssembly();

                    // determine if the component is associated to the assembly
                    PlanogramAssemblyComponent assemblyComponent = 
                        assembly.Components.FirstOrDefault(i => Object.Equals(i.PlanogramComponentId, _component.Id));
                    if (assemblyComponent != null)
                    {
                        // the component is associated to an assembly component
                        _fixtureItem = fixtureItem;
                        _fixture = fixture;
                        _fixtureAssembly = fixtureAssembly;
                        _assembly = assembly;
                        _assemblyComponent = assemblyComponent;

                        // and return
                        return;
                    }
                }
            }

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            PlanogramSubComponent subComponent,
            PlanogramFixtureItem fixtureItem,
            PlanogramFixtureComponent fixtureComponent)
        {
            _subComponent = subComponent;
            _component = subComponent.Parent;
            _planogram = _component.Parent;
            _fixtureItem = fixtureItem;
            _fixtureComponent = fixtureComponent;
            _fixture = fixtureComponent.Parent;

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            PlanogramSubComponent subComponent,
            Planogram planogram,
            PlanogramFixtureItem fixtureItem,
            PlanogramFixture fixture,
            PlanogramFixtureComponent fixtureComponent,
            PlanogramComponent component)
        {
            _subComponent = subComponent;
            _component = component;
            _planogram = planogram;
            _fixtureItem = fixtureItem;
            _fixtureComponent = fixtureComponent;
            _fixture = fixture;

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            PlanogramSubComponent subComponent,
            PlanogramFixtureItem fixtureItem,
            PlanogramFixtureAssembly fixtureAssembly,
            PlanogramAssemblyComponent assemblyComponent)
        {
            _subComponent = subComponent;
            _component = subComponent.Parent;
            _planogram = _component.Parent;
            _fixtureItem = fixtureItem;
            _fixtureAssembly = fixtureAssembly;
            _fixture = fixtureAssembly.Parent;
            _assemblyComponent = assemblyComponent;
            _assembly = assemblyComponent.Parent;

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(
            PlanogramSubComponent subComponent,
            Planogram planogram,
            PlanogramFixtureItem fixtureItem,
            PlanogramFixture fixture,
            PlanogramFixtureAssembly fixtureAssembly,
            PlanogramAssembly assembly,
            PlanogramAssemblyComponent assemblyComponent,
            PlanogramComponent component)
        {
            _subComponent = subComponent;
            _component = component;
            _planogram = planogram;
            _fixtureItem = fixtureItem;
            _fixtureAssembly = fixtureAssembly;
            _fixture = fixture;
            _assemblyComponent = assemblyComponent;
            _assembly = assembly;

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramPosition position)
        {
            _subComponent = position.GetPlanogramSubComponent();
            if (_subComponent != null) _component = _subComponent.Parent;
            if (_component != null) _planogram = _component.Parent;

            if (position.PlanogramFixtureItemId != null) _fixtureItem = _planogram.GetPlanogramFixtureItem(position.PlanogramFixtureItemId);
            if (_fixtureItem != null) _fixture = _fixtureItem.GetPlanogramFixture();
            if (position.PlanogramFixtureComponentId != null) _fixtureComponent = _planogram.GetPlanogramFixtureComponent(position.PlanogramFixtureComponentId);

            if (position.PlanogramAssemblyComponentId != null) _assemblyComponent = _planogram.GetPlanogramAssemblyComponent(position.PlanogramAssemblyComponentId);
            if (_assemblyComponent != null) _assembly = _assemblyComponent.Parent;
            if (position.PlanogramFixtureAssemblyId != null) _fixtureAssembly = _planogram.GetPlanogramFixtureAssembly(position.PlanogramFixtureAssemblyId);

            // create the id
            _id = new PlanogramSubComponentPlacementId(
                _planogram,
                _subComponent,
                _component,
                _assemblyComponent,
                _assembly,
                _fixtureAssembly,
                _fixture,
                _fixtureComponent,
                _fixtureItem);
        }

        #endregion

        #endregion

        #region Methods

        #region Transformation methods

        /// <summary>
        /// Returns the matrix tranform for this item relative to the planogram.
        /// </summary>
        /// <param name="includeSubComponent">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTransform(Boolean includeSubComponent)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //SubComponent
            if (includeSubComponent && _subComponent != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        _subComponent.X, _subComponent.Y, _subComponent.Z,
                        _subComponent.Angle, _subComponent.Slope, _subComponent.Roll));
            }

            if (_fixtureComponent != null)
            {
                //Fixture Component
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        _fixtureComponent.X, _fixtureComponent.Y, _fixtureComponent.Z,
                        _fixtureComponent.Angle, _fixtureComponent.Slope, _fixtureComponent.Roll));

            }
            else if (_assemblyComponent != null)
            {
                //Assembly Component
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        _assemblyComponent.X, _assemblyComponent.Y, _assemblyComponent.Z,
                        _assemblyComponent.Angle, _assemblyComponent.Slope, _assemblyComponent.Roll));

                //Fixture Assembly
                if (_fixtureAssembly != null)
                {
                    relativeTransform.Append(
                        MatrixValue.CreateTransformationMatrix(
                            _fixtureAssembly.X, _fixtureAssembly.Y, _fixtureAssembly.Z,
                            _fixtureAssembly.Angle, _fixtureAssembly.Slope, _fixtureAssembly.Roll));
                }
            }

            //Fixture
            if (_fixtureItem != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        _fixtureItem.X, _fixtureItem.Y, _fixtureItem.Z,
                        _fixtureItem.Angle, _fixtureItem.Slope, _fixtureItem.Roll));
            }

            return relativeTransform;
        }

        /// <summary>
        /// Returns the rotation for this component relative to the planogram
        /// </summary>
        private RotationValue GetPlanogramRelativeRotation(Boolean includeSubComponent)
        {
            RotationValue rotation = new RotationValue();

            //SubComponent
            if (includeSubComponent && _subComponent != null)
            {
                rotation.Angle += _subComponent.Angle;
                rotation.Slope += _subComponent.Slope;
                rotation.Roll += _subComponent.Roll;
            }

            if (_fixtureComponent != null)
            {
                //Fixture Component
                rotation.Angle += _fixtureComponent.Angle;
                rotation.Slope += _fixtureComponent.Slope;
                rotation.Roll += _fixtureComponent.Roll;
            }
            else if (_assemblyComponent != null)
            {
                //Assembly Component
                rotation.Angle += _assemblyComponent.Angle;
                rotation.Slope += _assemblyComponent.Slope;
                rotation.Roll += _assemblyComponent.Roll;

                //Assembly
                if (_fixtureAssembly != null)
                {
                    rotation.Angle += _fixtureAssembly.Angle;
                    rotation.Slope += _fixtureAssembly.Slope;
                    rotation.Roll += _fixtureAssembly.Roll;
                }
            }

            //Fixture
            if (_fixtureItem != null)
            {
                rotation.Angle += _fixtureItem.Angle;
                rotation.Slope += _fixtureItem.Slope;
                rotation.Roll += _fixtureItem.Roll;
            }

            return rotation;
        }

        /// <summary>
        /// Converts the given local point to a planogram relative one.
        /// </summary>
        public static PointValue ToWorld(PointValue localPoint, PlanogramSubComponentPlacement context)
        {
            //get the relative transform
            //TODO: Should this include local or not - i think maybe it should...
            MatrixValue relativeTransform = context.GetPlanogramRelativeTransform( /*incSub*/false);

            //trasnform and return.
            PointValue worldPoint = localPoint.Transform(relativeTransform);
            return worldPoint;
        }

        /// <summary>
        /// Converts the given world point to one which is local
        /// to this subcomponent placement.
        /// </summary>
        public static PointValue ToLocal(PointValue worldPoint, PlanogramSubComponentPlacement context)
        {
            //get the relative transform inc local.
            MatrixValue relativeTransform = context.GetPlanogramRelativeTransform( /*incSub*/true);

            //invert the transform.
            relativeTransform.Invert();

            return worldPoint.Transform(relativeTransform);
        }

        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates()
        {
            if (_subComponent == null) return new PointValue();

            PointValue localCoordinates = new PointValue(_subComponent.X, _subComponent.Y, _subComponent.Z);

            //get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform( /*incSub*/false);

            //trasnform and return.
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate)
        {
            // get the relative parent transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform( /*incSub*/false);

            // invert it.
            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);
            _subComponent.X = newLocalCoordinate.X;
            _subComponent.Y = newLocalCoordinate.Y;
            _subComponent.Z = newLocalCoordinate.Z;
        }

        /// <summary>
        /// Returns the rotation of this component relative to the planogram as a whole.
        /// </summary>
        public RotationValue GetPlanogramRelativeRotation()
        {
            return GetPlanogramRelativeRotation(/*incSub*/true);
        }

        /// <summary>
        /// Sets the local rotation values based on the given world rotation.
        /// </summary>
        /// <param name="worldRotation"></param>
        public void SetRotationFromPlanogramRelative(RotationValue worldRotation)
        {
            //get the parent relative rotation
            RotationValue parentRotation = GetPlanogramRelativeRotation( /*incSub*/false);

            //strip away the parent rotation to get the new local values.
            _subComponent.Angle = worldRotation.Angle - parentRotation.Angle;
            _subComponent.Slope = worldRotation.Slope - parentRotation.Slope;
            _subComponent.Roll = worldRotation.Roll - parentRotation.Roll;
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetPlanogramRelativeBoundingBox()
        {
            if (_planogramRelativeBoundingBox == null)
            {
                lock (_planogramRelativeBoundingBoxLock)
                {
                    if (_planogramRelativeBoundingBox == null)
                    {

                        RectValue bounds = new RectValue(0, 0, 0,
                            _subComponent.Width,
                            _subComponent.Height,
                            _subComponent.Depth);

                        //create the relative transform.
                        MatrixValue relativeTransform = GetPlanogramRelativeTransform( /*incSub*/true);
                        _planogramRelativeBoundingBox = relativeTransform.TransformBounds(bounds);
                    }
                }
            }
            return (RectValue)_planogramRelativeBoundingBox;
        }

        
        /// <summary>
        /// Returns the boundary info for this subcomponent relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public BoundaryInfo GetPlanogramRelativeBoundaryInfo()
        {
            if (_planRelativeBoundaryInfo == null)
            {
                lock(_planRelativeBoundaryInfoLock)
                {
                    if (_planRelativeBoundaryInfo == null)
                    {
                        _planRelativeBoundaryInfo = 
                            new BoundaryInfo(_subComponent.Height, _subComponent.Width, _subComponent.Depth,
                            GetPlanogramRelativeTransform( /*incSub*/true));
                    }
                }
            }
            return _planRelativeBoundaryInfo;
        }


        /// <summary>
        /// Returns the planogram relative matrix transform for this placement.
        /// </summary>
        /// <returns></returns>
        internal MatrixValue GetPlanogramRelativeTransform()
        {
            return GetPlanogramRelativeTransform(/*incSub*/true);
        }

        /// <summary>
        /// Moves this subcomponent's fixture component from its current fixture to the target provided.
        /// </summary>
        /// <param name="targetFixture"></param>
        /// <param name="targetFixtureItem"></param>
        public void MoveFixtureComponent(PlanogramFixture targetFixture, PlanogramFixtureItem targetFixtureItem)
        {
            foreach(PlanogramPosition position in this.GetPlanogramPositions())
            {
                position.PlanogramFixtureItemId = targetFixtureItem.Id;
            }

            _fixtureComponent = _fixtureComponent.Move<PlanogramFixtureComponentList>(_fixture.Components, targetFixture.Components);
            _fixture = targetFixture;
            _fixtureItem = targetFixtureItem;
        }

        #endregion

        #region Position Methods

        /// <summary>
        /// Associates a position to this
        /// sub component placement
        /// </summary>
        public void AssociatePosition(PlanogramPosition position)
        {
            // associate the position to the
            position.PlanogramFixtureItemId = (_fixtureItem == null) ? null : _fixtureItem.Id;
            position.PlanogramFixtureAssemblyId = (_fixtureAssembly == null) ? null : _fixtureAssembly.Id;
            position.PlanogramAssemblyComponentId = (_assemblyComponent == null) ? null : _assemblyComponent.Id;
            position.PlanogramFixtureComponentId = (_fixtureComponent == null) ? null : _fixtureComponent.Id;
            position.PlanogramSubComponentId = (_subComponent == null) ? null : _subComponent.Id;
        }

        /// <summary>
        /// Adds a product to this placement
        /// </summary>
        public PlanogramPosition AddPosition(PlanogramProduct product)
        {
            if (_planogram == null) return null;
            return _planogram.Positions.Add(product, this);
        }

        /// <summary>
        /// Adds a position to this placement
        /// adding it to the planogram if it
        /// does not already exist
        /// </summary>
        public void AddPosition(PlanogramPosition position)
        {
            if (_planogram == null) return;
            if (!_planogram.Positions.Contains(position)) _planogram.Positions.Add(position);
            AssociatePosition(position);
        }

        /// <summary>
        /// Returns all positions associated to 
        /// this sub component placement
        /// </summary>
        public IEnumerable<PlanogramPosition> GetPlanogramPositions(Boolean orderBySequence = false)
        {
            return orderBySequence
                ? EnumeratePlanogramPositions().OrderBy(p => p.Sequence).ToList()
                : EnumeratePlanogramPositions().ToList();
        }

        /// <summary>
        /// Enumerates through all positions on the plan that are associated with this sub placement.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramPosition> EnumeratePlanogramPositions()
        {
            if (_planogram == null) yield break;
            IEnumerable<PlanogramPosition> planogramPositions = _planogram.Positions.Where(IsPositionAssociated);
            foreach (PlanogramPosition position in planogramPositions)
                yield return position;
        }

        /// <summary>
        /// Indicates if a position is associated to this placement
        /// </summary>
        public Boolean IsPositionAssociated(PlanogramPosition position)
        {
            // fixture item
            if ((_fixtureItem == null) && (position.PlanogramFixtureItemId != null)) return false;
            if ((_fixtureItem != null) && (position.PlanogramFixtureItemId == null)) return false;
            if ((_fixtureItem != null) && (!_fixtureItem.Id.Equals(position.PlanogramFixtureItemId))) return false;

            // fixture assembly
            if ((_fixtureAssembly == null) && (position.PlanogramFixtureAssemblyId != null)) return false;
            if ((_fixtureAssembly != null) && (position.PlanogramFixtureAssemblyId == null)) return false;
            if ((_fixtureAssembly != null) && (!_fixtureAssembly.Id.Equals(position.PlanogramFixtureAssemblyId)))
                return false;

            // assembly component
            if ((_assemblyComponent == null) && (position.PlanogramAssemblyComponentId != null)) return false;
            if ((_assemblyComponent != null) && (position.PlanogramAssemblyComponentId == null)) return false;
            if ((_assemblyComponent != null) && (!_assemblyComponent.Id.Equals(position.PlanogramAssemblyComponentId)))
                return false;

            // fixture component
            if ((_assemblyComponent == null) && (position.PlanogramAssemblyComponentId != null)) return false;
            if ((_assemblyComponent != null) && (position.PlanogramAssemblyComponentId == null)) return false;
            if ((_assemblyComponent != null) && (!_assemblyComponent.Id.Equals(position.PlanogramAssemblyComponentId)))
                return false;

            // sub component
            if ((_subComponent == null) && (position.PlanogramSubComponentId != null)) return false;
            if ((_subComponent != null) && (position.PlanogramSubComponentId == null)) return false;
            if ((_subComponent != null) && (!_subComponent.Id.Equals(position.PlanogramSubComponentId))) return false;

            // if we got this far, return true
            return true;
        }


        #endregion

        #region Annotation Methods

        /// <summary>
        /// Associates an annotation with this
        /// sub component placement
        /// </summary>
        public void AssociateAnnotation(PlanogramAnnotation annotation)
        {
            annotation.PlanogramFixtureItemId = (_fixtureItem == null) ? null : _fixtureItem.Id;
            annotation.PlanogramFixtureAssemblyId = (_fixtureAssembly == null) ? null : _fixtureAssembly.Id;
            annotation.PlanogramAssemblyComponentId = (_assemblyComponent == null) ? null : _assemblyComponent.Id;
            annotation.PlanogramFixtureComponentId = (_fixtureComponent == null) ? null : _fixtureComponent.Id;
            annotation.PlanogramSubComponentId = (_subComponent == null) ? null : _subComponent.Id;
        }

        /// <summary>
        /// Indicates if a position is associated to this placement
        /// </summary>
        public Boolean IsAnnotationAssociated(PlanogramAnnotation annotation)
        {
            // fixture item
            if ((_fixtureItem == null) && (annotation.PlanogramFixtureItemId != null)) return false;
            if ((_fixtureItem != null) && (annotation.PlanogramFixtureItemId == null)) return false;
            if ((_fixtureItem != null) && (!_fixtureItem.Id.Equals(annotation.PlanogramFixtureItemId))) return false;

            // fixture assembly
            if ((_fixtureAssembly == null) && (annotation.PlanogramFixtureAssemblyId != null)) return false;
            if ((_fixtureAssembly != null) && (annotation.PlanogramFixtureAssemblyId == null)) return false;
            if ((_fixtureAssembly != null) && (!_fixtureAssembly.Id.Equals(annotation.PlanogramFixtureAssemblyId)))
                return false;

            // assembly component
            if ((_assemblyComponent == null) && (annotation.PlanogramAssemblyComponentId != null)) return false;
            if ((_assemblyComponent != null) && (annotation.PlanogramAssemblyComponentId == null)) return false;
            if ((_assemblyComponent != null) && (!_assemblyComponent.Id.Equals(annotation.PlanogramAssemblyComponentId)))
                return false;

            // fixture component
            if ((_fixtureComponent == null) && (annotation.PlanogramFixtureComponentId != null)) return false;
            if ((_fixtureComponent != null) && (annotation.PlanogramFixtureComponentId == null)) return false;
            if ((_fixtureComponent != null) && (!_fixtureComponent.Id.Equals(annotation.PlanogramFixtureComponentId)))
                return false;

            // sub component
            if ((_subComponent == null) && (annotation.PlanogramSubComponentId != null)) return false;
            if ((_subComponent != null) && (annotation.PlanogramSubComponentId == null)) return false;
            if ((_subComponent != null) && (!_subComponent.Id.Equals(annotation.PlanogramSubComponentId))) return false;

            // if we got this far, return true
            return true;
        }

        /// <summary>
        /// Adds an annotation to the sub component
        /// </summary>
        public void AddAnnotation(PlanogramAnnotation annotation)
        {
            if (_planogram == null) return;
            if (!_planogram.Annotations.Contains(annotation))
            {
                _planogram.Annotations.Add(annotation);
            }
            AssociateAnnotation(annotation);
        }

        /// <summary>
        /// Enumerates through all annotations which are linked directly to this placement.
        /// </summary>
        public IEnumerable<PlanogramAnnotation> GetPlanogramAnnotations()
        {
            if (_planogram == null) yield break;
            IEnumerable<PlanogramAnnotation> planogramAnnotations = _planogram.Annotations.Where(IsAnnotationAssociated);
            foreach (PlanogramAnnotation anno in planogramAnnotations)
                yield return anno;
        }

        #endregion

        #region Merchandising Methods

        /// <summary>
        /// Returns the ordered list of other subcomponent placements that this 
        /// should be be combined with when relaying positions.
        /// </summary>
        public List<PlanogramSubComponentPlacement> GetCombinedWithList()
        {
            return GetCombinedWithList(_planogram.GetPlanogramSubComponentPlacements().ToArray());
        }

        /// <summary>
        /// Returns the ordered list of other subcomponent placements that this 
        /// should be be combined with when relaying positions.
        /// </summary>
        public List<PlanogramSubComponentPlacement> GetCombinedWithList(
            IEnumerable<PlanogramSubComponentPlacement> availablePlacements)
        {
            PlanogramSubComponentPlacement targetSubPlacement = this;
            PlanogramSubComponent targetSub = targetSubPlacement.SubComponent;
            List<PlanogramSubComponentPlacement> subsToRespread = new List<PlanogramSubComponentPlacement>()
            {
                targetSubPlacement
            };

            //if the sub can't combine then just return out.
            if (!targetSub.IsCombinable()) return subsToRespread;

            //PointValue subWorldPos = targetSubPlacement.GetPlanogramRelativeCoordinates();
            RotationValue subWorldRotation = targetSubPlacement.GetPlanogramRelativeRotation();

            //get the list of adjacent subs the target can combine with and their coordinates local to the target sub.
            List<Tuple<PlanogramSubComponentPlacement, PointValue>> adjacentSubs =
                new List<Tuple<PlanogramSubComponentPlacement, PointValue>>();
            foreach (PlanogramSubComponentPlacement planSubPlacment in availablePlacements)
            {
                // dont include this
                if (Object.Equals(planSubPlacment, targetSubPlacement)) continue;

                PlanogramSubComponent planSub = planSubPlacment.SubComponent;

                //They could potential combine
                if (planSub.CanCombineWith(targetSub))
                {
                    //that they have the same rotation
                    RotationValue sRotation = planSubPlacment.GetPlanogramRelativeRotation();
                    if (sRotation == subWorldRotation)
                    {
                        //dont bother checking position as this will get dealt with when
                        // the subs are ordered.
                        adjacentSubs.Add(new Tuple<PlanogramSubComponentPlacement, PointValue>(
                            planSubPlacment,
                            ToLocal(planSubPlacment.GetPlanogramRelativeCoordinates(), targetSubPlacement)));
                    }
                }
            }
            if (adjacentSubs.Count == 0) return subsToRespread;


            //order by x
            adjacentSubs = adjacentSubs.OrderBy(a => a.Item2.X).ToList();


            PlanogramSubComponentPlacement curSub;

            //check to left
            if (targetSub.CombineType == PlanogramSubComponentCombineType.LeftOnly
                || targetSub.CombineType == PlanogramSubComponentCombineType.Both)
            {
                curSub = targetSubPlacement;

                Single leftAdjust = 0;
                while (curSub != null)
                {
                    curSub = null;

                    foreach (var asItem in adjacentSubs.Where(a => a.Item2.X < 0))
                    {
                        PlanogramSubComponent aSub = asItem.Item1.SubComponent;
                        PointValue posLocalToTarget = asItem.Item2;

                        if (aSub.CombineType == PlanogramSubComponentCombineType.RightOnly
                            || aSub.CombineType == PlanogramSubComponentCombineType.Both)
                        {
                            PointValue expectedPosition = new PointValue(-leftAdjust - aSub.Width, 0, 0);

                            //check values individually so that we can be a bit more tolerant of variances.
                            if(Framework.Helpers.MathHelper.EqualTo((-leftAdjust - aSub.Width), posLocalToTarget.X, 1)
                                && Framework.Helpers.MathHelper.EqualTo(0, posLocalToTarget.Y, 1)
                                && Framework.Helpers.MathHelper.EqualTo(0, posLocalToTarget.Z, 1))
                            //if (posLocalToTarget == expectedPosition)
                            {
                                subsToRespread.Insert(0, asItem.Item1);
                                leftAdjust += aSub.Width;
                                    // V8-29589: LeftAdjust is substracted to go left on the combined components. It needs to grow, or we will be going right.

                                //set this as the current sub if it can combine the other way too.
                                curSub =
                                    (aSub.CombineType == PlanogramSubComponentCombineType.Both)
                                        ? asItem.Item1
                                        : null;

                                break;
                            }
                        }
                    }
                }
            }


            //to right
            if (targetSub.CombineType == PlanogramSubComponentCombineType.RightOnly
                || targetSub.CombineType == PlanogramSubComponentCombineType.Both)
            {
                curSub = targetSubPlacement;

                Single rightAdjust = targetSub.Width; //start from the end of the target
                while (curSub != null)
                {
                    curSub = null;

                    foreach (var asItem in adjacentSubs.Where(a => a.Item2.X > 0))
                    {
                        PlanogramSubComponent aSub = asItem.Item1.SubComponent;
                        PointValue posLocalToTarget = asItem.Item2;

                        if (aSub.CombineType == PlanogramSubComponentCombineType.LeftOnly
                            || aSub.CombineType == PlanogramSubComponentCombineType.Both)
                        {
                            PointValue expectedPosition = new PointValue(rightAdjust, 0, 0);

                            //check values individually so that we can be a bit more tolerant of variances.
                            if (Framework.Helpers.MathHelper.EqualTo(rightAdjust, posLocalToTarget.X, 1)
                                && Framework.Helpers.MathHelper.EqualTo(0, posLocalToTarget.Y, 1)
                                && Framework.Helpers.MathHelper.EqualTo(0, posLocalToTarget.Z, 1))
                            //if (posLocalToTarget == expectedPosition)
                            {
                                subsToRespread.Add(asItem.Item1);
                                rightAdjust += aSub.Width;

                                //set this as the current sub if it can combine the other way too.
                                curSub =
                                    (aSub.CombineType == PlanogramSubComponentCombineType.Both)
                                        ? asItem.Item1
                                        : null;

                                break;
                            }
                        }
                    }

                }

            }

            return subsToRespread;
        }

        /// <summary>
        /// Calculates the volume (relative to the planogram) that represent 
        /// the space that can be merchandised on, in or around this subcomponent placement.
        /// </summary>
        /// <param name="ignoreCollisions">True indicates that the space occupied by other subcomponents should be ignored.</param>
        /// <param name="subComponentPlacements">Enumeration of subcomponent placements to determine collisions and obstructions.</param>
        /// <returns>A <see cref="RectValue"/> representing the merchandisable space of this <see cref="PlanogramSubComponentPlacement"/>.</returns>
        public RectValue GetWorldMerchandisingSpaceBounds(Boolean ignoreCollisions,
            IEnumerable<PlanogramSubComponentPlacement> subComponentPlacements)
        {
            var merchSpace = new PlanogramMerchandisingSpace(this, subComponentPlacements);
            return ignoreCollisions ? merchSpace.RotatedUnhinderedSpace : merchSpace.RotatedSpaceReducedByObstructions;
        }

        #endregion

        #region Position Options
        /// <summary>
        /// Returns the facing axis for this sub component placement
        /// </summary>
        public AxisType GetFacingAxis()
        {
            return _subComponent.GetFacingAxis();
        }

        /// <summary>
        ///     Stacks the increase units axes available, so that the first out is the first to try increasing units in.
        /// </summary>
        /// <param name="isStacked">
        ///     A <see cref="Dictionary{AxisType, Boolean}" /> of axes and whether they have a stacked
        ///     <c>Merchandising Strategy</c>.
        /// </param>
        /// <returns>A stack of axes to try increasing units in, extracted in the order they should be processed.</returns>
        /// <remarks>
        ///     The particular order of X, Y and Z axes depends on the component and whether the <c>Merchandising Strategy</c>
        ///     is stacked or not on each axis.
        /// </remarks>
        public Stack<AxisType> GetIncreaseAxesStack(Dictionary<AxisType, Boolean> isStacked)
        {
            var increaseAxesStack = new Stack<AxisType>();

            //  NB: The priority order is stored in a stack for processing which will reverse it, 
            //  so the last axis on the list is actually the first to be increased on.
            var baseAxesPriority = new List<AxisType> {AxisType.X, AxisType.Y, AxisType.Z};

            switch (Component.ComponentType)
            {
                case PlanogramComponentType.Shelf:
                case PlanogramComponentType.Pallet:
                    break;
                case PlanogramComponentType.Bar:
                    baseAxesPriority = new List<AxisType> {AxisType.X, AxisType.Z};
                    break;
                case PlanogramComponentType.Peg:
                    baseAxesPriority = new List<AxisType> {AxisType.Y, AxisType.X, AxisType.Z};
                    break;
                case PlanogramComponentType.Chest:
                    baseAxesPriority = new List<AxisType> {AxisType.X, AxisType.Z, AxisType.Y};
                    break;
                case PlanogramComponentType.Rod:
                    baseAxesPriority = new List<AxisType> {AxisType.Z};
                    break;
                case PlanogramComponentType.ClipStrip:
                    baseAxesPriority = new List<AxisType> {AxisType.Y, AxisType.Z};
                    break;
                case PlanogramComponentType.SlotWall:
                    baseAxesPriority = new List<AxisType> { AxisType.Y, AxisType.X, AxisType.Z };
                    break;
                default:
                    Debug.Fail("Unknown Componenty Type or not valid for PlanogramPosition.GetIncreaseAxesStack.");
                    return new Stack<AxisType>();
            }

            //  Removed for now as it seems unnecesary.
            //AxisType facingAxis = GetFacingAxis();
            //increaseAxesStack.Push(facingAxis);
            //baseAxesPriority.Remove(facingAxis);

            //  Add stacked axes.
            foreach (AxisType axis in baseAxesPriority.Where(axis => isStacked[axis]).ToList())
            {
                increaseAxesStack.Push(axis);
                baseAxesPriority.Remove(axis);
            }

            //  Add any left over axes.
            foreach (AxisType axis in baseAxesPriority)
            {
                increaseAxesStack.Push(axis);
            }

            return increaseAxesStack;
        }

        #endregion

        #region Overrides
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return _id.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override bool Equals(Object obj)
        {
            PlanogramSubComponentPlacement other = obj as PlanogramSubComponentPlacement;
            if (other != null)
            {
                return _id.Equals(other.Id);
            }
            else
            {
                return false;
            }
        }
        #endregion
        
        /// <summary>
        /// Gets the coordinates by which to sequence this item, implementing the <see cref="IPlanogramSequencableItem"/> interface,
        /// </summary>
        /// <returns></returns>
        public PointValue GetSequencableCoords()
        {
            return GetPlanogramRelativeCoordinates();
        }

        #endregion

    }

    /// <summary>
    /// A class used to uniquely identify
    /// a planogram sub component placement
    /// </summary>
    [Serializable]
    public class PlanogramSubComponentPlacementId
    {
        #region Fields
        private readonly Object _planogramId;
        private readonly Object _subComponentId;
        private readonly Object _componentId;
        private readonly Object _assemblyComponentId;
        private readonly Object _assemblyId;
        private readonly Object _fixtureAssemblyId;
        private readonly Object _fixtureId;
        private readonly Object _fixtureComponentId;
        private readonly Object _fixtureItemId;
        private readonly Int32 _hashCode;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramSubComponentPlacementId(Planogram planogram,
            PlanogramSubComponent subComponent,
            PlanogramComponent component,
            PlanogramAssemblyComponent assemblyComponent,
            PlanogramAssembly assembly,
            PlanogramFixtureAssembly fixtureAssembly,
            PlanogramFixture fixture,
            PlanogramFixtureComponent fixtureComponent,
            PlanogramFixtureItem fixtureItem)
        {
            _planogramId = (planogram == null) ? null : planogram.Id;
            _subComponentId = (subComponent == null) ? null : subComponent.Id;
            _componentId = (component == null) ? null : component.Id;
            _assemblyComponentId = (assemblyComponent == null) ? null : assemblyComponent.Id;
            _assemblyId = (assembly == null) ? null : assembly.Id;
            _fixtureAssemblyId = (fixtureAssembly == null) ? null : fixtureAssembly.Id;
            _fixtureId = (fixture == null) ? null : fixture.Id;
            _fixtureComponentId = (fixtureComponent == null) ? null : fixtureComponent.Id;
            _fixtureItemId = (fixtureItem == null) ? null : fixtureItem.Id;

            // generate the hash code
            _hashCode = HashCode.Start
                .Hash(_planogramId)
                .Hash(_subComponentId)
                .Hash(_componentId)
                .Hash(_assemblyComponentId)
                .Hash(_assemblyId)
                .Hash(_fixtureAssemblyId)
                .Hash(_fixtureId)
                .Hash(_fixtureComponentId)
                .Hash(_fixtureItemId);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return _hashCode;
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramSubComponentPlacementId other = obj as PlanogramSubComponentPlacementId;
            if (other != null)
            {
                if (!AreEqual(other._planogramId, _planogramId)) return false;
                if (!AreEqual(other._subComponentId, _subComponentId)) return false;
                if (!AreEqual(other._componentId, _componentId)) return false;
                if (!AreEqual(other._assemblyComponentId, _assemblyComponentId)) return false;
                if (!AreEqual(other._assemblyId, _assemblyId)) return false;
                if (!AreEqual(other._fixtureAssemblyId, _fixtureAssemblyId)) return false;
                if (!AreEqual(other._fixtureId, _fixtureId)) return false;
                if (!AreEqual(other._fixtureComponentId, _fixtureComponentId)) return false;
                if (!AreEqual(other._fixtureItemId, _fixtureItemId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Asserts if two object references
        /// are equal
        /// </summary>
        private static Boolean AreEqual(Object one, Object two)
        {
            if (one == null)
                return two == null;
            else
                return one.Equals(two);
        }
        #endregion
    }
}
