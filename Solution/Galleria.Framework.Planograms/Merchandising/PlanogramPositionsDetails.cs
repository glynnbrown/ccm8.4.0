﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27738 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Logging;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Provides aggregrated details about a
    /// group of positions
    /// </summary>
    public class PlanogramPositionsDetails
    {
        #region Fields
        private WidthHeightDepthValue _totalSize = new WidthHeightDepthValue();
        private Int32 _totalUnitCount = 0;
        private WidthHeightDepthValue _totalSpace = new WidthHeightDepthValue();
        #endregion

        #region Properties

        #region TotalSize
        /// <summary>
        /// Returns the total size of all positions
        /// </summary>
        public WidthHeightDepthValue TotalSize
        {
            get { return _totalSize; }
        }
        #endregion

        #region TotalUnitCount
        /// <summary>
        /// Returns the total unit count across all positions
        /// </summary>
        public Int32 TotalUnitCount
        {
            get { return _totalUnitCount; }
        }
        #endregion

        #region TotalSpace
        /// <summary>
        /// Returns the total space occupied by all positions
        /// </summary>
        private WidthHeightDepthValue TotalSpace
        {
            get { return _totalSpace; }
        }
        #endregion

        #endregion

        #region Constructors
        private PlanogramPositionsDetails() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionsDetails NewPlanogramPositionsDetails(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            PlanogramPositionsDetails item = new PlanogramPositionsDetails();
            item.Create(positionPlacements);
            return item;
        }
        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            foreach (PlanogramPositionPlacement position in positionPlacements)
            {
                PlanogramPositionDetails positionDetails = position.GetPositionDetails();
                _totalSize += positionDetails.TotalSize;
                _totalUnitCount += positionDetails.TotalUnitCount;
                _totalSpace += positionDetails.TotalSpace;
            }
        }

        #endregion

        #endregion
    }
}
