﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27439 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// A class representing the totals of all the metrics in the supplied Performance Data.
    /// </summary>
    public class PlanogramPerformanceDataTotals
    {
        #region Properties

        public Single? P1 { get; private set; }
        public Single? P2 { get; private set; }
        public Single? P3 { get; private set; }
        public Single? P4 { get; private set; }
        public Single? P5 { get; private set; }
        public Single? P6 { get; private set; }
        public Single? P7 { get; private set; }
        public Single? P8 { get; private set; }
        public Single? P9 { get; private set; }
        public Single? P10 { get; private set; }
        public Single? P11 { get; private set; }
        public Single? P12 { get; private set; }
        public Single? P13 { get; private set; }
        public Single? P14 { get; private set; }
        public Single? P15 { get; private set; }
        public Single? P16 { get; private set; }
        public Single? P17 { get; private set; }
        public Single? P18 { get; private set; }
        public Single? P19 { get; private set; }
        public Single? P20 { get; private set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates and calculates the totals for all the metrics in the Performance Data provided.
        /// </summary>
        public PlanogramPerformanceDataTotals(IEnumerable<PlanogramPerformanceData> performanceData)
        {
            foreach (var item in performanceData)
            {
                if (item.P1 != null) P1 = (P1 == null ? item.P1 : P1 + item.P1);
                if (item.P2 != null) P2 = (P2 == null ? item.P2 : P2 + item.P2);
                if (item.P3 != null) P3 = (P3 == null ? item.P3 : P3 + item.P3);
                if (item.P4 != null) P4 = (P4 == null ? item.P4 : P4 + item.P4);
                if (item.P5 != null) P5 = (P5 == null ? item.P5 : P5 + item.P5);
                if (item.P6 != null) P6 = (P6 == null ? item.P6 : P6 + item.P6);
                if (item.P7 != null) P7 = (P7 == null ? item.P7 : P7 + item.P7);
                if (item.P8 != null) P8 = (P8 == null ? item.P8 : P8 + item.P8);
                if (item.P9 != null) P9 = (P9 == null ? item.P9 : P9 + item.P9);
                if (item.P10 != null) P10 = (P10 == null ? item.P10 : P10 + item.P10);
                if (item.P11 != null) P11 = (P11 == null ? item.P11 : P11 + item.P11);
                if (item.P12 != null) P12 = (P12 == null ? item.P12 : P12 + item.P12);
                if (item.P13 != null) P13 = (P13 == null ? item.P13 : P13 + item.P13);
                if (item.P14 != null) P14 = (P14 == null ? item.P14 : P14 + item.P14);
                if (item.P15 != null) P15 = (P15 == null ? item.P15 : P15 + item.P15);
                if (item.P16 != null) P16 = (P16 == null ? item.P16 : P16 + item.P16);
                if (item.P17 != null) P17 = (P17 == null ? item.P17 : P17 + item.P17);
                if (item.P18 != null) P18 = (P18 == null ? item.P18 : P18 + item.P18);
                if (item.P19 != null) P19 = (P19 == null ? item.P19 : P19 + item.P19);
                if (item.P20 != null) P20 = (P20 == null ? item.P20 : P20 + item.P20);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the metric in the given slot number.
        /// </summary>
        /// <param name="metricNumber"></param>
        /// <returns></returns>
        public Single? GetTotalByMetricNumber(Int32 metricNumber)
        {
            switch(metricNumber)
            {
                case 1: return this.P1;
                case 2: return this.P2;
                case 3: return this.P3;
                case 4: return this.P4;
                case 5: return this.P5;
                case 6: return this.P6;
                case 7: return this.P7;
                case 8: return this.P8;
                case 9: return this.P9;
                case 10: return this.P10;
                case 11: return this.P11;
                case 12: return this.P12;
                case 13: return this.P13;
                case 14: return this.P14;
                case 15: return this.P15;
                case 16: return this.P16;
                case 17: return this.P17;
                case 18: return this.P18;
                case 19: return this.P19;
                case 20: return this.P20;

                default:
                    throw new ArgumentOutOfRangeException("pNumber");

            }
        }

        #endregion
    }
}
