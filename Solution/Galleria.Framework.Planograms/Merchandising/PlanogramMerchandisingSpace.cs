﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802
// V8-29137 : A.Kuszyk
//  Created.
// V8-29023 : M.Pettit
//  Added PlanogramMerchandisingSpace now takes account of merchandising Depth for HangFromBottom-type sub components
#endregion

#region Version History : CCM802
// V8-29192 
//  Modified GetMerchSpaceForHangToInfinity to prevent overfill width checking on a clipStrip
//  as a product is usually wider than the clipstrip itself. Obstruction tests with other components are not affected.
#endregion

#region Version History : CCM803
// V8-29192 : M.Shelley
//  Modified the GetMerchSpaceForStackToInfinity method to allow for merchandising overhangs in
//  bounds checking
// V8-29137 : A.Kuszyk
//  Removed optional subcomp space bounds parameters.
#endregion

#region Version History : CCM810
// V8-29983 : A.Kuszyk
//  Introduced maximiseDepthForHang parameter to control whether or not merchandisable depth is calculated if 
//  the subcomponent's value is zero.
// V8-29192 : L.Ineson
//  Removed old metadetails constructor
// V8-29982 : A.Kuszyk
//  Updated merch space methods to take account of overhangs.
// V8-30004 : A.Kuszyk
//  Ensured that merch height can never be negative for a shelf.
// V8-30032 : A.Kuszyk
//  Updated merch space for stack components to use the merch height of the tallest product in the plan if the component
//  doesn't have any obstructions above it (i.e. if it's the top shelf). Added IsTopSubComponent property to indicate this.
// V8-30084 : A.Kuszyk
//  Added CombineWithAndMinimiseHeight method.
// V8-30085 : A.Kuszyk
//  Added Rotation, IsPlanogramRelative and IsMerchandisingGroupRelative properties and re-factored transformation logic.
#endregion

#region Version History : CCM811
// V8-30152 : A.Kuszyk
//  Re-factored transform logic to make more robust to repeated rotations.
// V8-30456 : A.Kuszyk
//  Added RotatedSpaceReducedByObstructions property.
// V8-30544 : A.Kuszyk
//  Amended GetMerchSpaceForHangToInfinity to allow merch depth to win when greater than fixture depth.
// V8-30637 : A.Kuszyk
//  Re-factored CombineWithAndMinimiseHeight into AddToRight which now adds the space it has been given to the right of this
//  one, irrespective of its actual co-ordinates or rotation.
#endregion
#region Version History : CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark flag
#endregion
#region Version History : CCM830
// V8-31674 : L.Ineson
//  Unhindered merch bounds for top shelf no longer uses max product height.
//  Add to right now uses height from first sub.
// V8-31696 : L.Ineson
//  Amended get obstructions so that merch height is ignored when checking as this later causes issues when combining space.
//V8-32001 : L.Ineson
//  Amended AddToRight to ignore the overhang width when combining.
// V8-32239 : A.Kuszyk
//  Fixed issue with AddToRight not accounting for rotation.
// V8-32893 : L.Ineson
//  GetObstructions now considers scenarios where x axis has overlap but depth does not - i.e. bar above rod.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Represents the merchandisable space of a <see cref="PlanogramSubComponentPlacement"/> and any obstructions
    /// to that space that may exist.
    /// </summary>
    /// <remarks>
    /// By default, <see cref="PlanogramMerchandisingSpace"/> objects are instantiated in the Planogram frame of reference. As such,
    /// all the co-ordinates in their properties are relative to the origin of Planogram world space.
    /// <para></para>
    /// However, the <see cref="ToMerchandisingGroupRelative"/> method may be used to transform the <see cref="PlanogramMerchandisingSpace"/>
    /// into it's <see cref="PlanogramMerchandisingGroup"/>'s frame of reference, in which case co-ordinates will now be relative
    /// to the origin of the <see cref="PlanogramMerchandisingGroup"/>'s space.
    /// <para></para>
    /// The <see cref="Transform"/> method may also be used to transform the <see cref="PlanogramMerchandisingSpace"/> through
    /// a custom translation and rotation.
    /// <para></para>
    /// In all cases, co-ordinates are relative the current frame of reference, but sizes always remain un-rotated. The <see cref="Rotation"/>
    /// property denotes the rotation that the space actually has to ensure that the actual size of the space is always preserved.
    /// </remarks>
    public sealed class PlanogramMerchandisingSpace
    {
        #region Properties

        /// <summary>
        /// The un-rotated volume of space that is merchandisable, relative to the subcomponent placement, ignoring any obstructions.
        /// </summary>
        /// <remarks>
        /// This space is relative to the subcomponent placement and represents the world relative space when used in combination
        /// with the Rotation property.
        /// </remarks>
        public RectValue UnhinderedSpace { get; private set; }

        /// <summary>
        /// A collection of volumes that obstruct the unhindered space, relative to the subcomponent placement.
        /// </summary>
        public IEnumerable<RectValue> Obstructions { get; private set; }

        /// <summary>
        /// The un-rotated volume of space that is merchandisable, relative to the subcomponent placement, reduced to the maximum size
        /// before an obstruction is encountered.
        /// </summary>
        /// <remarks>
        /// This space is relative to the subcomponent placement and represents the world relative space when used in combination
        /// with the Rotation property.
        /// </remarks>
        public RectValue SpaceReducedByObstructions { get; private set; }

        /// <summary>
        /// The SubComponent Placement to which this merchandisable space refers.
        /// </summary>
        public PlanogramSubComponentPlacement SubComponentPlacement { get; private set; }

        /// <summary>
        /// Indicates whether or not this subcomponent is positioned at the top of the plan,
        /// I.E. with no obstructing subcomponents positioned above it.
        /// </summary>
        public Boolean IsTopSubComponent { get; private set; }

        /// <summary>
        /// The rotation of this merchandising space relative to the <see cref="RotationPivot"/>.
        /// </summary>
        public RotationValue Rotation { get; private set; }

        /// <summary>
        /// The pivot point around which the <see cref="Rotation"/> should be applied.
        /// </summary>
        public PointValue RotationPivot { get; private set; }

        /// <summary>
        /// Indicates if the space is relative to the planogram.
        /// </summary>
        public Boolean IsPlanogramRelative { get; private set; }

        /// <summary>
        /// Indicates if the space is relative to the merchandising group.
        /// </summary>
        public Boolean IsMerchandisingGroupRelative { get; private set; }

        /// <summary>
        /// This item's <see cref="UnhinderedSpace"/> rotated by its <see cref="Rotation"/> around the co-ordinates
        /// of its <see cref="RotationPivot"/>.
        /// </summary>
        public RectValue RotatedUnhinderedSpace
        {
            get
            {
                return UnhinderedSpace.Rotate(Rotation, RotationPivot);
            }
        }

        /// <summary>
        /// This item's <see cref="SpaceReducedByObstructions"/> rotated by its <see cref="Rotation"/> around the co-ordinates
        /// of its <see cref="RotationPivot"/>.
        /// </summary>
        public RectValue RotatedSpaceReducedByObstructions
        {
            get
            {
                return SpaceReducedByObstructions.Rotate(Rotation, RotationPivot);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates a new <see cref="PlanogramMerchandisingSpace"/> from the given unhindered space and obstructions,
        /// re-calculating <see cref="SpaceReducedByObstructions"/>.
        /// </summary>
        /// <param name="merchandisingSpace">The <see cref="PlanogramMerchandisingSpace"/> to use as a basis for the new space.</param>
        /// <param name="newObstructions">A collection of <see cref="RectValue"/> to use as obstructions to the new space.</param>
        /// <param name="newUnhinderedSpace">A <see cref="RectValue"/> representing the <see cref="UnhinderedSpace"/> of the new space.</param>
        /// <param name="rotationPivot">
        /// The pivot around which the rotation of the <paramref name="merchandisingSpace"/> should be applied to this new
        /// <see cref="PlanogramMerchandisingSpace"/>, if different to the coordinates of the <paramref name="newUnhinderedSpace"/>
        /// relative to the <see cref="UnhinderedSpace"/> of <paramref name="merchandisingSpace"/>.
        /// </param>
        private PlanogramMerchandisingSpace(
            PlanogramMerchandisingSpace merchandisingSpace,
            RectValue newUnhinderedSpace,
            IEnumerable<RectValue> newObstructions,
            PointValue? rotationPivot = null)
        {
            RotationPivot =
                rotationPivot ??
                (newUnhinderedSpace.ToPointValue() - merchandisingSpace.UnhinderedSpace.ToPointValue()).ToPointValue();
            UnhinderedSpace = newUnhinderedSpace;
            Obstructions = newObstructions.ToList();
            SubComponentPlacement = merchandisingSpace.SubComponentPlacement;
            IsTopSubComponent = merchandisingSpace.IsTopSubComponent;
            IsPlanogramRelative = merchandisingSpace.IsPlanogramRelative;
            IsMerchandisingGroupRelative = merchandisingSpace.IsMerchandisingGroupRelative;
            Rotation = merchandisingSpace.Rotation;
            SpaceReducedByObstructions = CalculatedSpaceReducedByObstructions();
        }

        /// <summary>
        /// Instantiates a new <see cref="PlanogramMerchandisingSpace"/> from <paramref name="merchandisingSpace"/>, applying
        /// the given <paramref name="translation"/> and <paramref name="rotation"/>.
        /// </summary>
        /// <param name="merchandisingSpace">The <see cref="PlanogramMerchandisingSpace"/> to transform into a new space.</param>
        /// <param name="rotation">The rotation to apply to the <paramref name="merchandisingSpace"/>.</param>
        /// <param name="translation">The translation to apply to the <paramref name="merchandisingSpace"/>.</param>
        /// <param name="isPlanogramRelative">Indiciates if the new space should be considered to be relative to the Planogram's reference frame.</param>
        /// <param name="isMerchandisingGroupRelative">Indiciates if the new space should be considered to be relative to the reference frame of its <see cref="PlanogramMerchandisingGroup"/>.</param>
        /// <param name="rotationPivot">
        /// The <see cref="PointValue"/> around which to apply the <see cref="Rotation"/> of <paramref name="merchandisingSpace"/>.
        /// If omitted, this pivot is taken as the co-ordinates of the <see cref="UnhinderedSpace"/> of the new <see cref="PlanogramMerchandisingSpace"/>
        /// relative the to co-ordinates of the <see cref="UnhinderedSpace"/> of <paramref name="merchandisingSpace"/>.
        /// </param>
        private PlanogramMerchandisingSpace(
            PlanogramMerchandisingSpace merchandisingSpace,
            RotationValue rotation,
            VectorValue translation,
            Boolean isPlanogramRelative,
            Boolean isMerchandisingGroupRelative,
            PointValue? rotationPivot = null)
        {
            UnhinderedSpace = TranslateSpace(merchandisingSpace.UnhinderedSpace, translation);
            RotationPivot =
                rotationPivot ??
                (UnhinderedSpace.ToPointValue() - merchandisingSpace.UnhinderedSpace.ToPointValue()).ToPointValue();
            SpaceReducedByObstructions = TranslateSpace(merchandisingSpace.SpaceReducedByObstructions, translation);
            Obstructions = merchandisingSpace.Obstructions.Select(o => TranslateSpace(o, translation)).ToList();
            SubComponentPlacement = merchandisingSpace.SubComponentPlacement;
            IsPlanogramRelative = isPlanogramRelative;
            IsMerchandisingGroupRelative = isMerchandisingGroupRelative;
            IsTopSubComponent = merchandisingSpace.IsTopSubComponent;
            Rotation = new RotationValue(
                merchandisingSpace.Rotation.Angle + rotation.Angle,
                merchandisingSpace.Rotation.Slope + rotation.Slope,
                merchandisingSpace.Rotation.Roll + rotation.Roll);
        }

        private RectValue TranslateSpace(RectValue rectValue, VectorValue translation)
        {
            PointValue newCoords = new PointValue(
                rectValue.X + translation.X,
                rectValue.Y + translation.Y,
                rectValue.Z + translation.Z);
            RectValue translatedSpaceWithNewCoords = new RectValue(
                newCoords.X,
                newCoords.Y,
                newCoords.Z,
                rectValue.Width,
                rectValue.Height,
                rectValue.Depth);
            return translatedSpaceWithNewCoords;
        }

        /// <summary>
        /// Creates a new merchandising space representing the volume (in planogram relative space) that can be merchandised for the given
        /// subcomponent placement and any obstructions within that space that cannot be merchandised.
        /// </summary>
        /// <param name="subComponentPlacement">The subcomponent placement whose merchandising space should be evaluated.</param>
        /// <param name="allSubComponentPlacements">All the subcomponent placements on the plan, including the one to be evaluated.</param>
        /// <param name="ignoreSpaceOverlaps">
        /// If true, all space overlaps will be ignored.
        /// If false, neighbouring subcomponents will be considered to ensure that the space space is not returned for two
        /// different subcomponents.
        /// </param>
        /// <param name="maximiseWidthForHang">If true, additional width will be found for hang subcomponents out to the edge of the plan or other subcomponents.</param>
        /// <param name="maximiseDepthForHang">If true, additional depthwill be found for hang subcomponents out to the edge of the plan or other subcomponents.</param>
        /// <exception cref="ArgumentNullException">Thrown if subComponentPlacement is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if allSubComponentPlacements is null.</exception>
        public PlanogramMerchandisingSpace(
            PlanogramSubComponentPlacement subComponentPlacement,
            IEnumerable<PlanogramSubComponentPlacement> allSubComponentPlacements,
            Boolean ignoreSpaceOverlaps = true,
            Boolean maximiseWidthForHang = false,
            Boolean maximiseDepthForHang = false)
        {
            //using (CodePerformanceMetric m1 = new CodePerformanceMetric())
            {

                if (subComponentPlacement == null) throw new ArgumentNullException("subComponentPlacement");
                if (allSubComponentPlacements == null) throw new ArgumentNullException("allSubComponentPlacements");

                SubComponentPlacement = subComponentPlacement;
                IsPlanogramRelative = true;
                IsMerchandisingGroupRelative = false;
                Rotation = SubComponentPlacement.GetPlanogramRelativeRotation();

                // Get the size and world coordinates of the subcomponent for an accurate representation
                // of the subcomponent and the space it might occupy (if un-rotated).
                PointValue planRelativeCoords = subComponentPlacement.GetPlanogramRelativeCoordinates();
                WidthHeightDepthValue subCompSize = subComponentPlacement.SubComponent.GetWidthHeightDepth();

                // The pivot for rotation should always start off as the world coordinates of the subcomponent placement.
                // The UnhinderedSpace may be offset from this, which is important for any subsequent transformations.
                RotationPivot = planRelativeCoords;

                // Get the world bounds of the fixture. This is useful for determining the distance between the subcomponent
                // and the top of the plan. However, we're only really interested in its world coordinates and its un-rotated
                // size, so we get both here and then package them up into a RectValue.
                RectValue fixtureWorldBounds =
                    SubComponentPlacement.FixtureItem.GetPlanogramRelativeBoundingBox(SubComponentPlacement.Fixture);
                PointValue fixtureCoords = new PointValue(fixtureWorldBounds.X, fixtureWorldBounds.Y, fixtureWorldBounds.Z);
                WidthHeightDepthValue fixtureSize = new WidthHeightDepthValue(
                    SubComponentPlacement.Fixture.Width,
                    SubComponentPlacement.Fixture.Height,
                    SubComponentPlacement.Fixture.Depth);
                fixtureWorldBounds = new RectValue(fixtureCoords, fixtureSize);

                // Build a dictionary of the bounding boxes for each of the subcomponent placements that are merchandisable. 
                //Getting the planogram relative bounding boxes gives us an approximate
                // volume of space that is occupied by the other subcomponents.
                Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompPlacementsWorldBounds =
                    allSubComponentPlacements
                        .Where(s =>
                            !s.Equals(subComponentPlacement) &&
                            s.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None)
                        .Distinct()
                        .ToDictionary(p => p, p => p.GetPlanogramRelativeBoundingBox());

                // If there are any subcomponents above this one, then it isn't the top subcomponent. We perform this
                // comparison with plan relative bounding boxes so that all world rotations are taken into account.
                RectValue thisWorldBounds = subComponentPlacement.GetPlanogramRelativeBoundingBox();
                
                this.IsTopSubComponent = !otherSubCompPlacementsWorldBounds.Values
                    .Where(b =>
                        // Higher than the subcomp.
                        b.Y.GreaterThan(thisWorldBounds.Y + thisWorldBounds.Height) &&
                            // Overlaps in X.
                        Math.Max(b.X, thisWorldBounds.X)
                            .LessThan(Math.Min(b.X + b.Width, thisWorldBounds.X + thisWorldBounds.Width)) &&
                            // Overlaps in Z.
                        Math.Max(b.Z, thisWorldBounds.Z)
                            .LessThan(Math.Min(b.Z + b.Depth, thisWorldBounds.Z + thisWorldBounds.Depth)))
                    .Any();

                // Make sure bars never get maximized width.
                if (subComponentPlacement.Component.ComponentType == PlanogramComponentType.Bar)
                {
                    maximiseWidthForHang = false;
                }

                // Calculate the unhindered space and any obstructions.
                switch (subComponentPlacement.SubComponent.MerchandisingType)
                {
                    case PlanogramSubComponentMerchandisingType.Stack:
                        {
                            // If the subcomponent has face thickness, then products are merchandised within its volume.
                            if (subComponentPlacement.SubComponent.HasFaceThickness())
                            {
                                UnhinderedSpace = GetMerchSpaceForStackWithFaceThickness(planRelativeCoords, subCompSize);
                                // Because the merch space is within the subcomponent, we assume there are no obstructions.
                                Obstructions = new List<RectValue>();
                            }
                            else // If the subcomponent does not have face thickness, products are merchandised on its top.
                            {
                                UnhinderedSpace = GetMerchSpaceForStackToInfinity(planRelativeCoords, subCompSize, fixtureWorldBounds);

                                Single toFixtureTop = Math.Max(fixtureWorldBounds.Height - planRelativeCoords.Y - subCompSize.Height, 0);

                                Obstructions = GetObstructions(
                                    ignoreSpaceOverlaps,
                                    subComponentPlacement.GetPlanogramRelativeBoundingBox(),
                                    otherSubCompPlacementsWorldBounds,
                                    above: true, toFixtureTop: toFixtureTop);
                            }
                        }
                        break;


                    case PlanogramSubComponentMerchandisingType.Hang:
                        {
                            UnhinderedSpace = GetMerchSpaceForHangToInfinity(
                                subComponentPlacement.GetPlanogramRelativeBoundingBox(),
                                planRelativeCoords,
                                subCompSize,
                                fixtureWorldBounds,
                                maximiseWidthForHang,
                                otherSubCompPlacementsWorldBounds,
                                maximiseDepthForHang);

                            // Because the merch space is in front of the subcomponent, we assume there are no obstructions.
                            Obstructions = new List<RectValue>();
                        }
                        break;

                    case PlanogramSubComponentMerchandisingType.HangFromBottom:
                        {
                            UnhinderedSpace = GetMerchSpaceForHangFromBottomToInfinity(
                                planRelativeCoords,
                                subCompSize,
                                fixtureWorldBounds,
                                otherSubCompPlacementsWorldBounds,
                                maximiseWidthForHang);

                            Obstructions = GetObstructions(
                                ignoreSpaceOverlaps,
                                subComponentPlacement.GetPlanogramRelativeBoundingBox(),
                                otherSubCompPlacementsWorldBounds,
                                above: false);
                        }
                        break;

                    case PlanogramSubComponentMerchandisingType.None:
                    default:
                        break;
                }

                // Finally, calculate the space reduced by obstructions. Obstructions are only found the y direction, so we
                // just reduce there.
                SpaceReducedByObstructions = CalculatedSpaceReducedByObstructions();
            }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Combines the given list of spaces into a new collection of continous spaces.
        /// </summary>
        /// <param name="spaces">
        /// The list of spaces to be combined, arranged in a combinable order. E.g. if these items are expected to combine
        /// in the X direction, they should be arranged in X order.
        /// </param>
        /// <returns></returns>
        public static IEnumerable<PlanogramMerchandisingSpace> CombineSpaces(List<PlanogramMerchandisingSpace> spaces)
        {
            List<PlanogramMerchandisingSpace> combinedSpaces = new List<PlanogramMerchandisingSpace>();
            for (Int32 i = 0; i < spaces.Count; i++)
            {
                // Start with this item and iterate through subsequent items, combining if possible.
                PlanogramMerchandisingSpace combinedSpace = spaces[i];
                while (true)
                {
                    // If this is the last item or it isn't continuous with the next item, just break and add this item
                    // to the return list.
                    if (i < spaces.Count - 1 &&
                        spaces[i].RotatedUnhinderedSpace.IsContinuousWith(spaces[i + 1].RotatedUnhinderedSpace))
                    {
                        combinedSpace = spaces[i].CombineWith(spaces[i + 1]);
                        spaces[i] = combinedSpace;
                        spaces[i + 1] = combinedSpace;
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
                combinedSpaces.Add(combinedSpace);
            }
            return combinedSpaces;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Applies the the given <paramref name="translation"/> and <paramref name="rotation"/> transformation values.
        /// </summary>
        /// <param name="translation">A <see cref="MatrixValue"/> representing the translation to apply.</param>
        /// <param name="rotation">A <see cref="RotationValue"/> representing the rotation to apply.</param>
        /// <returns>A new <see cref="PlanogramMerchandisingSpace"/> instance with the given transformation applied.</returns>
        public PlanogramMerchandisingSpace Transform(MatrixValue translation, RotationValue rotation)
        {
            return new PlanogramMerchandisingSpace(
                this,
                rotation,
                new VectorValue(translation.OffsetX, translation.OffsetY, translation.OffsetZ),
                isPlanogramRelative: false,
                isMerchandisingGroupRelative: false);
        }

        /// <summary>
        /// Applies the reverse of the given <paramref name="translation"/> and <paramref name="rotation"/> transformation values.
        /// </summary>
        /// <param name="translation">A <see cref="MatrixValue"/> representing the translation to reverse.</param>
        /// <param name="rotation">A <see cref="RotationValue"/> representing the rotation to reverse.</param>
        /// <returns>A new <see cref="PlanogramMerchandisingSpace"/> instance with the given transformation reversed.</returns>
        public PlanogramMerchandisingSpace ReverseTransform(MatrixValue translation, RotationValue rotation)
        {
            RotationValue reversedRotation = new RotationValue(-rotation.Angle, -rotation.Slope, -rotation.Roll);
            return new PlanogramMerchandisingSpace(
                this,
                reversedRotation,
                new VectorValue(-translation.OffsetX, -translation.OffsetY, -translation.OffsetZ),
                isPlanogramRelative: false,
                isMerchandisingGroupRelative: false);
        }

        /// <summary>
        /// Returns a new <see cref="PlanogramMerchandisingSpace"/>instance with properties transformed through
        /// the inverse of the Planogram relative transformation matrix of the the first <see cref="PlanogramSubComponentPlacement"/> 
        /// in <paramref name="merchandisingGroup"/>.
        /// </summary>
        /// <param name="merchandisingGroup">The <see cref="PlanogramMerchandisingGroup"/> to make this <see cref="PlanogramMerchandisingSpace"/> instance relative to.</param>
        /// <returns></returns>
        public PlanogramMerchandisingSpace ToMerchandisingGroupRelative(PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (this.IsMerchandisingGroupRelative)
            {
                return new PlanogramMerchandisingSpace(this, this.UnhinderedSpace, this.Obstructions);
            }

            // First, we need the translation of the first subcomponent in the merch group.
            MatrixValue merchGroupRelativeTransform =
                merchandisingGroup.SubComponentPlacements.First().GetPlanogramRelativeTransform();
            VectorValue merchGroupRelativeVector = new VectorValue(
                -merchGroupRelativeTransform.OffsetX,
                -merchGroupRelativeTransform.OffsetY,
                -merchGroupRelativeTransform.OffsetZ);

            // Now invert it to get the transform from plan relative (which is what we get out of the merch space by default)
            // to subcomp relative.
            //merchGroupRelativeTransform.Invert();

            // We also need the rotation value separately, so that we can append this rotation to the new merchandising space.
            RotationValue merchGroupRotation =
                merchandisingGroup.SubComponentPlacements.First().GetPlanogramRelativeRotation();
            RotationValue merchGroupRelativeRotation = new RotationValue(
                -merchGroupRotation.Angle, -merchGroupRotation.Slope, -merchGroupRotation.Roll);

            return new PlanogramMerchandisingSpace(
                this,
                merchGroupRelativeRotation,
                merchGroupRelativeVector,
                isPlanogramRelative: false,
                isMerchandisingGroupRelative: true);
        }

        /// <summary>
        /// Combines this merchandisable space with that given, combining spaces and obstructions and re-calculating
        /// the space reduced by obstructions.
        /// </summary>
        /// <param name="otherSpace"></param>
        /// <returns>A combined merchandising space.</returns>
        public PlanogramMerchandisingSpace CombineWith(PlanogramMerchandisingSpace otherSpace)
        {
            return new PlanogramMerchandisingSpace(
                this,
                RotatedUnhinderedSpace
                    .Union(otherSpace.RotatedUnhinderedSpace)
                    .ReverseRotation(Rotation, RotationPivot),
                Obstructions.Union(otherSpace.Obstructions).Distinct());
        }

        /// <summary>
        /// Combines this merchandisable space with that given, combining spaces and obstructions and re-calculating
        /// the space reduced by obstructions, whilst maintaining the height of the first merch space (this one). 
        /// <paramref name="otherSpace"/> is added to the right of this one, regardless of its actual position, although
        /// it is assumed that the two spaces are in the same frame of reference in order to account for overhangs.
        /// </summary>
        /// <param name="otherSpace">The space to add to the right of this one.</param>
        /// <returns>A combined merchandising space.</returns>
        public PlanogramMerchandisingSpace AddToRight(PlanogramMerchandisingSpace otherSpace)
        {
            // When we create the new space, we're going to use the co-ordinates, rotation and Y/Z size of this space
            // for the new space. However, we need to add some or all of the width of otherSpace to our
            // current width in order to "add" the space to the right. When doing so, we need to account for the fact
            // that the spaces might be rotated and also exclude any areas of intersecting width (due to overhangs).

            // First, we calculate the amount of other space that intersects with this space to take account of
            // overhangs. This assumes that the two spaces are in the same frame of reference.
            Single intersectingWidth;
            RectValue? intersectingRotatedSpace = this.RotatedUnhinderedSpace.Intersect(otherSpace.RotatedUnhinderedSpace);
            if (!intersectingRotatedSpace.HasValue)
            {
                intersectingWidth = 0;
            }
            else
            {
                intersectingWidth = intersectingRotatedSpace.Value.ReverseRotation(Rotation).Width;
            }

            // Next, we calculate the amount of the other space's width to use, bu subtracting any
            // intersecting width.
            Single otherWidth = otherSpace.UnhinderedSpace.Width - intersectingWidth;

            // Finally, we calculate the size of the new space.
            RectValue newUnhinderedSpace = new RectValue(
                this.UnhinderedSpace.X, // Use the co-ordinates from this space.
                this.UnhinderedSpace.Y,
                this.UnhinderedSpace.Z,
                this.UnhinderedSpace.Width + otherWidth, // Sum this width and the amount of the other width to use.
                this.UnhinderedSpace.Height, // Use height from this space.
                Math.Max(this.UnhinderedSpace.Depth, otherSpace.UnhinderedSpace.Depth));

            return new PlanogramMerchandisingSpace(
                this,
                newUnhinderedSpace,
                Obstructions.Union(otherSpace.Obstructions).Distinct());
        }

        /// <summary>
        /// Returns a new merchandising space reduced in space to fit within the given bounding space. If this space
        /// does not intersect with the given bounding space, null is returned.
        /// </summary>
        /// <param name="boundingSpace">The space to reduce this space to.</param>
        /// <returns>A reduced merchandising space, or null if this space does not intersect with that given.</returns>
        /// <remarks>
        /// This method will apply this space's <see cref="Rotation"/> before comparing the space
        /// with <paramref name="boundingSpace"/> to ensure that <see cref="UnhinderedSpace"/> is compared in the 
        /// right reference frame. This process does, of course, assume that <param ref="boundingSpace"/> is in the
        /// same reference frame as <see cref="UnhinderedSpace"/>.
        /// <para></para>
        /// <para></para>
        /// The <see cref="Rotation"/> is applied before the intersection comparison and then reversed afterwards. This
        /// works well for angles that are a multiple of Pi/2, but will result in an expansion of the space for other
        /// angles.
        /// </remarks>
        public PlanogramMerchandisingSpace ReduceToFit(RectValue boundingSpace)
        {
            // Get the rotated unhindered space that intersects with the bounding space.
            RectValue? possibleIntersectingUnhinderedSpace = RotatedUnhinderedSpace.Intersect(boundingSpace);
            if (!possibleIntersectingUnhinderedSpace.HasValue ||
                possibleIntersectingUnhinderedSpace.Value.Width.EqualTo(0) ||
                possibleIntersectingUnhinderedSpace.Value.Height.EqualTo(0) ||
                possibleIntersectingUnhinderedSpace.Value.Depth.EqualTo(0))
            {
                return null;
            }

            // Reverse the rotation on the intsecting unhindered space.
            // Note: the simple process of reducing the rotated space and then reversing the rotation again works well
            // with rotations that are multiples of Pi/2, but will result in an expansion of the space if other angles
            // are used.
            RectValue intersectingUnhinderedSpace = possibleIntersectingUnhinderedSpace.Value.ReverseRotation(Rotation,RotationPivot);

            // Get the rotated obstructions that intersect with the bounding space.
            IEnumerable<RectValue?> possibleIntersectingObstructions = Obstructions.Select(o => o
                .Rotate(Rotation, RotationPivot)
                .Intersect(boundingSpace));
            Int32 obstructionIndex = 0;

            // Reverse the rotation on those obstructions that intersect with the bounding space.
            List<RectValue> intersectingObstructions = new List<RectValue>();
            foreach (RectValue? intersectingObstruction in possibleIntersectingObstructions)
            {
                if (intersectingObstruction.HasValue)
                {
                    intersectingObstructions.Add(
                        intersectingObstruction.Value.ReverseRotation(Rotation, RotationPivot));
                }
                obstructionIndex++;
            }

            return new PlanogramMerchandisingSpace(
                this,
                intersectingUnhinderedSpace,
                intersectingObstructions,
                RotationPivot);
        }

        /// <summary>
        /// Returns the linear value of this space.
        /// </summary>
        public Single GetMerchandisableLinear()
        {
            if (this.SubComponentPlacement == null
                || this.SubComponentPlacement.SubComponent == null)
                return 0;

            if (this.SubComponentPlacement.SubComponent.MerchandisingType
                == PlanogramSubComponentMerchandisingType.None)
                return 0;

            return this.SpaceReducedByObstructions.GetSize(GetMerchandisableLinearAxis());
        }

        /// <summary>
        /// Returns the axis to use when calculating the linear axis for this 
        /// merch space.
        /// </summary>
        /// <returns></returns>
        public AxisType GetMerchandisableLinearAxis()
        {
            switch (this.SubComponentPlacement.SubComponent.MerchandisingType)
            {
                default:
                case PlanogramSubComponentMerchandisingType.None:
                case PlanogramSubComponentMerchandisingType.Hang:
                case PlanogramSubComponentMerchandisingType.Stack:
                    return AxisType.X;

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return AxisType.Z;
            }
        }

        /// <summary>
        /// Returns the area value of this space.
        /// This is according to the merchandising type of the subcomponent.
        /// </summary>
        public Single GetMerchandisableArea()
        {
            if (this.SubComponentPlacement == null
                || this.SubComponentPlacement.SubComponent == null)
                return 0;


            switch (this.SubComponentPlacement.SubComponent.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.None:
                    return 0;

                //TODO: Isn't the merch area of a shelf by depth?
                //case PlanogramSubComponentMerchandisingType.Stack:
                //    return this.SpaceReducedByObstructions.Width * this.SpaceReducedByObstructions.Depth;

                case PlanogramSubComponentMerchandisingType.Stack:
                case PlanogramSubComponentMerchandisingType.Hang:
                    return this.SpaceReducedByObstructions.Width*this.SpaceReducedByObstructions.Height;

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return this.SpaceReducedByObstructions.Width*this.SpaceReducedByObstructions.Depth;

                default:
                    Debug.Fail("Type not handled");
                    return this.SpaceReducedByObstructions.Width*this.SpaceReducedByObstructions.Height;
            }
        }


        /// <summary>
        /// Returns the volume of this space.
        /// </summary>
        public Single GetMerchandisableVolume()
        {
            return
                this.SpaceReducedByObstructions.Width
                *this.SpaceReducedByObstructions.Height
                *this.SpaceReducedByObstructions.Depth;
        }

        #endregion

        #region Private Methods

        private RectValue CalculatedSpaceReducedByObstructions()
        {
            Single reducedHeight = UnhinderedSpace.Height;
            Single reducedY = UnhinderedSpace.Y;

            // Identify if we need to be reducing the y coord and the height or just the height.
            Boolean reduceFromBottom =
                SubComponentPlacement.SubComponent.MerchandisingType ==
                PlanogramSubComponentMerchandisingType.HangFromBottom;

            if (this.Obstructions != null)
            {
                foreach (RectValue obstruction in Obstructions)
                {
                    if (reduceFromBottom)
                    {
                        if (reducedY.LessThan(obstruction.Y + obstruction.Height))
                        {
                            Single oldReducedY = reducedY;
                            reducedY = obstruction.Y + obstruction.Height;
                            reducedHeight -= Math.Abs(oldReducedY - reducedY);
                        }
                    }
                    else
                    {
                        if (reducedHeight.GreaterThan(obstruction.Y - UnhinderedSpace.Y))
                        {
                            reducedHeight = obstruction.Y - UnhinderedSpace.Y;
                        }
                    }
                }
            }

            return new RectValue(
                UnhinderedSpace.X,
                reducedY,
                UnhinderedSpace.Z,
                UnhinderedSpace.Width,
                reducedHeight,
                UnhinderedSpace.Depth);
        }

        /// <summary>
        /// Gets the volume below the subcomponent as the merchandisable volume.
        /// </summary>
        /// <param name="subCompWorldBounds"></param>
        /// <param name="fixtureWorldBounds"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown if maximiseWidth is true and otherSubCompWorldBounds is null.</exception>
        private RectValue GetMerchSpaceForHangFromBottomToInfinity(
            PointValue planRelativeCoords,
            WidthHeightDepthValue subCompSize,
            RectValue fixtureWorldBounds,
            Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompWorldBounds,
            Boolean maximiseWidth = false)
        {
            Single merchHeight;
            if (SubComponentPlacement.SubComponent.MerchandisableHeight.EqualTo(0))
            {
                merchHeight = planRelativeCoords.Y - fixtureWorldBounds.Y;
            }
            else
            {
                merchHeight = SubComponentPlacement.SubComponent.MerchandisableHeight;
            }

            // Merchandising space depth for hang-from-bottom type subcomponents is the distance from the rear of
            // the subcomponent to the front of the subcomponent, or the merch depth if smaller
            Single merchDepth;
            if (SubComponentPlacement.SubComponent.MerchandisableDepth.EqualTo(0))
            {
                merchDepth = subCompSize.Depth;
            }
            else
            {
                merchDepth = Math.Min(subCompSize.Depth, SubComponentPlacement.SubComponent.MerchandisableDepth);
            }

            // If we are maximising width, we need to check for the space available until we hit the nearest
            // components. If not, we just use the Y coordinate and width of the subcomponent.
            Single x;
            Single width;
            CalculateXAndWidth(out x, out width, maximiseWidth, planRelativeCoords, subCompSize,otherSubCompWorldBounds);

            PlanogramSubComponent subComp = SubComponentPlacement.SubComponent;
            return new RectValue(
                x - subComp.LeftOverhang,
                planRelativeCoords.Y - merchHeight,
                planRelativeCoords.Z - subComp.BackOverhang,
                width + subComp.LeftOverhang + subComp.RightOverhang,
                merchHeight,
                merchDepth + subComp.BackOverhang + subComp.FrontOverhang);
        }

        /// <summary>
        /// Gets the space between the hang subcomponent and the front of the fixture.
        /// </summary>
        /// <param name="subCompWorldBounds"></param>
        /// <param name="fixtureWorldBounds"></param>
        /// <param name="maximiseWidth">Indicates if widths should be maximised. If true, an argument should be supplied for otherSubCompWorldBounds.</param>
        /// <param name="otherSubCompWorldBounds">Should be supplied if maximiseWidth is true.</param>
        /// <exception cref="ArgumentException">Thrown if maximiseWidth is true and otherSubCompWorldBounds is null.</exception>
        /// <returns></returns>
        private RectValue GetMerchSpaceForHangToInfinity(
            RectValue subCompWorldBounds,
            PointValue planRelativeCoords,
            WidthHeightDepthValue subCompSize,
            RectValue fixtureWorldBounds,
            Boolean maximiseWidth,
            Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompWorldBounds,
            Boolean maximiseDepth)
        {
            // Depth is the distance from the front of the subcomponent (i.e. the front of the pegboard backplate) to the front of 
            // the fixture, or the merch depth if smaller than this. 
            // This is because hang-type sub-components have no measurement for peg length, this is determined by the plangram 
            // product which hangs on the peg.
            Single merchDepth;
            if (SubComponentPlacement.SubComponent.MerchandisableDepth.EqualTo(0))
            {
                if (maximiseDepth)
                {
                    merchDepth = fixtureWorldBounds.Z + fixtureWorldBounds.Depth - planRelativeCoords.Z -
                                 subCompSize.Depth;
                }
                else
                {
                    merchDepth = 0f;
                }
            }
            else
            {
                merchDepth = SubComponentPlacement.SubComponent.MerchandisableDepth;
            }

            // Merchandisable height for hang it the height below the subcomponent that can be merchandised.
            Single y;
            Single height;
            CalculateYAndHeight(out y, out height, subCompWorldBounds, planRelativeCoords, subCompSize, otherSubCompWorldBounds);

            // If we are maximising width, we need to check for the space available until we hit the nearest
            // components. If not, we just use the Y coordinate and width of the subcomponent.
            Single x;
            Single width;
            CalculateXAndWidth(out x, out width, maximiseWidth, planRelativeCoords, subCompSize, otherSubCompWorldBounds, y, height);


            PlanogramSubComponent subComp = SubComponentPlacement.SubComponent;
            return new RectValue(
                x - subComp.LeftOverhang,
                y - subComp.BottomOverhang,
                planRelativeCoords.Z + subCompSize.Depth - subComp.BackOverhang,
                width + subComp.LeftOverhang + subComp.RightOverhang,
                height + subComp.BottomOverhang + subComp.TopOverhang,
                merchDepth);
        }

        private void CalculateYAndHeight(out Single y, out Single height, RectValue subCompWorldBounds, PointValue planRelativeCoords, WidthHeightDepthValue subCompSize, Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompWorldBounds)
        {
            if (SubComponentPlacement.SubComponent.MerchandisableHeight.EqualTo(0))
            {
                IEnumerable<RectValue> rectValuesBelow = otherSubCompWorldBounds.Values
                    .Where(r =>
                        planRelativeCoords.Y.GreaterThan(r.Y + r.Height) &&
                        Math.Max(subCompWorldBounds.X, r.X).LessThan( // Overlaps in X
                            Math.Min(subCompWorldBounds.X + subCompWorldBounds.Width, r.X + r.Width)) &&
                        Math.Max(subCompWorldBounds.Z, r.Z).LessThan( // Overlaps in Z
                            Math.Min(subCompWorldBounds.Z + subCompWorldBounds.Depth, r.Z + r.Depth)));
                y = rectValuesBelow.Any()
                    ? rectValuesBelow.Max(r => r.Y + r.Height)
                    : 0f;
                height = planRelativeCoords.Y + subCompSize.Height - y;
            }
            else
            {
                if (SubComponentPlacement.SubComponent.MerchandisableHeight.LessOrEqualThan(subCompWorldBounds.Height))
                {
                    y = planRelativeCoords.Y;
                    height = subCompWorldBounds.Height;
                }
                else
                {
                    y = planRelativeCoords.Y + subCompSize.Height -
                        SubComponentPlacement.SubComponent.MerchandisableHeight;
                    height = SubComponentPlacement.SubComponent.MerchandisableHeight;
                }
            }
        }

        private void CalculateXAndWidth(
            out Single x,
            out Single width,
            Boolean maximiseWidth,
            PointValue planRelativeCoords,
            WidthHeightDepthValue subCompSize,
            Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompWorldBounds,
            Single? y = null,
            Single? height = null)
        {
            x = planRelativeCoords.X;
            width = subCompSize.Width;

            if (!maximiseWidth || SubComponentPlacement == null) return;

            RectValue subCompWorldBounds = SubComponentPlacement.GetPlanogramRelativeBoundingBox();
            RectValue merchSpace = new RectValue(
                x, 
                y.HasValue ? y.Value : subCompWorldBounds.Y, 
                subCompWorldBounds.Z, 
                width, 
                height.HasValue ? height.Value : subCompWorldBounds.Height, 
                subCompWorldBounds.Depth);

            List<RectValue> rectValuesToLeft;
            List<RectValue> rectValuesToRight;
            GetRectValuesToTheSides(merchSpace, otherSubCompWorldBounds.Values, out rectValuesToLeft, out rectValuesToRight);

            // set the Min X to be the left most coordinate or the start of the planogram if none.
            x = rectValuesToLeft.Any() ? rectValuesToLeft.Max(r => r.X + r.Width) : 0f;

            if (rectValuesToRight.Any())
            {
                width = rectValuesToRight.Min(r => r.X) - x;
            }
            else
            {
                PlanogramFixtureList fixtures = SubComponentPlacement?.Planogram?.Fixtures;
                width = fixtures != null && fixtures.Any() ? fixtures.Sum(f => f.Width) - x : 0f;
            }
        }

        /// <summary>
        /// Gets the obstructions to unhindered merchandising space above or below the subcomponent.
        /// </summary>
        /// <param name="ignoreSpaceOverlaps"></param>
        /// <param name="thisWorldBounds"></param>
        /// <param name="otherSubCompPlacementsWorldBounds"></param>
        /// <returns></returns>
        private IEnumerable<RectValue> GetObstructions(
            Boolean ignoreSpaceOverlaps,
            RectValue thisWorldBounds,
            Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompPlacementsWorldBounds,
            Boolean above, Single toFixtureTop = 0)
        {
            // First, get the sub components that overlap with this one.
            IEnumerable<PlanogramSubComponentPlacement> obstructingSubCompPlacements =
                GetObstructingSubCompPlacements(thisWorldBounds, otherSubCompPlacementsWorldBounds, above, toFixtureTop);

            // Now, iterate over all the overlapping subcomponents and, for those that have "line of sight"
            // to this one, get the spaces between. 
            List<RectValue> obstructions = new List<RectValue>();
            foreach (PlanogramSubComponentPlacement obstructingSubCompPlacement in obstructingSubCompPlacements)
            {
                // This other subcomponent is above this one and overlaps by some amount in the X
                // and Z directions.
                RectValue obstructingSubCompPlacementWorldBounds =
                    otherSubCompPlacementsWorldBounds[obstructingSubCompPlacement];

                // Get the line of sight width and depth.
                Single overlappingWidth = GetLineOfSightSizeInAxis(
                    AxisType.X,
                    obstructingSubCompPlacementWorldBounds,
                    thisWorldBounds,
                    otherSubCompPlacementsWorldBounds.Values);
                Single overlappingDepth = GetLineOfSightSizeInAxis(
                    AxisType.Z,
                    obstructingSubCompPlacementWorldBounds,
                    thisWorldBounds,
                    otherSubCompPlacementsWorldBounds.Values);

                //if both are 0 then dont bother. We do need to still consider if there is an overlap in one of the axis however.
                if (overlappingDepth.EqualTo(0) && overlappingWidth.EqualTo(0)) continue;

                // Calculate the obstruction volume based on the location of the obstruction relative to this
                // sub comp and the amount of its size that overlaps with this sub comp. We take the height only,
                // because if we're not worried about space overlaps then this is all we need.
                RectValue obstruction = new RectValue(
                    Math.Max(thisWorldBounds.X, obstructingSubCompPlacementWorldBounds.X),
                    // The coord within this subcomp that the overlap begins at.
                    obstructingSubCompPlacementWorldBounds.Y, // The y coord of the other sub comp.
                    Math.Max(thisWorldBounds.Z, obstructingSubCompPlacementWorldBounds.Z),
                    // The coord within this subcomp that the overlap begins at.
                    (overlappingWidth.GreaterThan(0))? overlappingWidth : obstructingSubCompPlacementWorldBounds.Width, // The overlapping width between the two
                    obstructingSubCompPlacementWorldBounds.Height, // Just the height of the other sub comp.
                    (overlappingDepth.GreaterThan(0))? overlappingDepth : obstructingSubCompPlacementWorldBounds.Depth); // The overlapping depth between the two

                // If we're worried about returning spaces that might overlap with each other, we'll make sure that this 
                // obstruction contains any space that might subsequently be claimed by the obstructing sub comp as its own
                // merch volume.
                if (!ignoreSpaceOverlaps)
                {
                    // There might be a conflict of space in two situations:
                    // 1. This is stack and the obstruction is hang from bottom.
                    // 2. This is hang from bottom and the obstruction is stack.
                    // In both cases, the merch spaces could intersect.

                    Boolean thisIsStack = SubComponentPlacement.SubComponent.MerchandisingType ==
                                          PlanogramSubComponentMerchandisingType.Stack;
                    Boolean thisIsHangFromBottom = SubComponentPlacement.SubComponent.MerchandisingType ==
                                                   PlanogramSubComponentMerchandisingType.HangFromBottom;
                    Boolean obstructionIsStack = obstructingSubCompPlacement.SubComponent.MerchandisingType ==
                                                 PlanogramSubComponentMerchandisingType.Stack;
                    Boolean obstructionIsHangFromBottom = obstructingSubCompPlacement.SubComponent.MerchandisingType ==
                                                          PlanogramSubComponentMerchandisingType.HangFromBottom;

                    if (thisIsStack && obstructionIsHangFromBottom && above) // obstruction should be above
                    {
                        // If the obstruction doesn't have a merch height, then it should occupy all the space
                        // between this sub comp and the obstruction.
                        if (obstructingSubCompPlacement.SubComponent.MerchandisableHeight.EqualTo(0))
                        {
                            obstruction.Height += obstruction.Y - thisWorldBounds.Y - thisWorldBounds.Height;
                            obstruction.Y = thisWorldBounds.Y + thisWorldBounds.Height;
                        }
                        else // The obstruction should occupy just the merch height (below itself).
                        {
                            obstruction.Height += obstructingSubCompPlacement.SubComponent.MerchandisableHeight;
                            obstruction.Y -= obstructingSubCompPlacement.SubComponent.MerchandisableHeight;
                        }
                    }
                    else if (thisIsHangFromBottom && obstructionIsStack && !above) // obstruction should be below
                    {
                        // if the obstruction is stack, then we leave it as is, because we arbitrarily let
                        // the hang from bottom components win all the space.
                    }
                }

                // Finally, we need to ensure that the obstruction casts a "shadow" in the merchandising space.
                // I.e. if we are looking for obstructions above this subcomponent, then any space above the obstruction
                // should also be added to the obstruction's volume.
                if (above)
                {
                    obstruction.Height = Math.Max(UnhinderedSpace.Y + UnhinderedSpace.Height - obstruction.Y,
                        obstruction.Height);
                }
                else
                {
                    Single oldObstructionY = obstruction.Y;
                    obstruction.Y = UnhinderedSpace.Y;
                    obstruction.Height += Math.Abs(oldObstructionY - obstruction.Y);
                }

                obstructions.Add(obstruction);
            }

            return obstructions;
        }

        private IEnumerable<PlanogramSubComponentPlacement> GetObstructingSubCompPlacements(
            RectValue thisWorldBounds,
            Dictionary<PlanogramSubComponentPlacement, RectValue> otherSubCompPlacementsWorldBounds,
            Boolean above,
            Single toFixtureTop = 0)
        {
            //if we are stacked type with a merch height
            //then check obstructions to the top of the fixture instead.
            Single checkHeight = this.UnhinderedSpace.Height;
            if (above && toFixtureTop.GreaterThan(0)
                && this.SubComponentPlacement.SubComponent.MerchandisableHeight.GreaterThan(0)
                && this.SubComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack
                && !this.SubComponentPlacement.SubComponent.HasFaceThickness())
            {
                checkHeight = toFixtureTop;
            }

            foreach (KeyValuePair<PlanogramSubComponentPlacement, RectValue> kvp in otherSubCompPlacementsWorldBounds)
            {
                RectValue boundingBox = kvp.Value;
                Boolean isObstruction =
                    (above
                        ? boundingBox.Y.GreaterOrEqualThan(thisWorldBounds.Y) &&
                          boundingBox.Y.LessOrEqualThan(UnhinderedSpace.Y + checkHeight)
                        : boundingBox.Y.LessOrEqualThan(thisWorldBounds.Y) &&
                          boundingBox.Y.GreaterOrEqualThan(UnhinderedSpace.Y))
                    &&
                    boundingBox.X.LessOrEqualThan(thisWorldBounds.X + thisWorldBounds.Width) &&
                    (boundingBox.X + boundingBox.Width).GreaterOrEqualThan(thisWorldBounds.X) &&
                    boundingBox.Z.LessOrEqualThan(thisWorldBounds.Z + thisWorldBounds.Depth) &&
                    (boundingBox.Z + boundingBox.Depth).GreaterOrEqualThan(thisWorldBounds.Z);
                if (isObstruction) yield return kvp.Key;
            }
            yield break;
        }

        /// <summary>
        /// Gets the amount of space that can be merchandised for the subcomponent, ignoring all obstructions.
        /// </summary>
        /// <param name="thisWorldBounds"></param>
        /// <param name="fixtureWorldBounds"></param>
        /// <returns></returns>
        private RectValue GetMerchSpaceForStackToInfinity(
            PointValue planRelativeCoords,
            WidthHeightDepthValue subCompSize,
            RectValue fixtureWorldBounds)
        {
            Single maxHeight =
                Math.Max(fixtureWorldBounds.Height - planRelativeCoords.Y - subCompSize.Height, 0);
            
            Single merchandisableHeight =
                SubComponentPlacement.SubComponent.MerchandisableHeight.EqualTo(0)
                    ? maxHeight
                    : SubComponentPlacement.SubComponent.MerchandisableHeight;

            PlanogramSubComponent subComp = SubComponentPlacement.SubComponent;

            return new RectValue(
                planRelativeCoords.X - subComp.LeftOverhang,
                planRelativeCoords.Y + subCompSize.Height,
                planRelativeCoords.Z - SubComponentPlacement.SubComponent.BackOverhang,
                subCompSize.Width + subComp.LeftOverhang + subComp.RightOverhang,
                merchandisableHeight,
                subCompSize.Depth + subComp.BackOverhang + subComp.FrontOverhang);
        }

        /// <summary>
        /// Returns the volume inside the subcomponent, taking face thickness into account. E.g. for a chest.
        /// </summary>
        /// <param name="thisWorldBounds"></param>
        /// <returns></returns>
        private RectValue GetMerchSpaceForStackWithFaceThickness(PointValue planRelativeCoords,
            WidthHeightDepthValue subCompSize)
        {
            PlanogramSubComponent subComp = SubComponentPlacement.SubComponent;

            // The merchandisable height is either that of the subcomponent, or the height of the subcomponent if zero.
            Single merchandisableHeight =
                SubComponentPlacement.SubComponent.MerchandisableHeight.EqualTo(0)
                    ? Math.Max(0, subCompSize.Height - SubComponentPlacement.SubComponent.FaceThicknessBottom)
                    : SubComponentPlacement.SubComponent.MerchandisableHeight;

            // One merchandisable space is required, because we're dealing with the volume of the subcomponent.
            RectValue merchSpace = new RectValue(
                planRelativeCoords.X + subComp.FaceThicknessLeft,
                planRelativeCoords.Y + subComp.FaceThicknessBottom,
                planRelativeCoords.Z + subComp.FaceThicknessBack,
                subCompSize.Width - subComp.FaceThicknessLeft - subComp.FaceThicknessRight,
                merchandisableHeight,
                subCompSize.Depth - subComp.FaceThicknessBack - subComp.FaceThicknessFront);

            return merchSpace;
        }

        /// <summary>
        /// Gets the amount of the size of the space to evaluate that overlaps with
        /// the space to compare and is not blocked in direct "line of sight" by any of
        /// the spaces in all spaces.
        /// </summary>
        /// <param name="axis">The X or Z direction to evaluate. Y will throw an exception.</param>
        /// <param name="allSpaces">All the spaces in which obstructions may appear.</param>
        /// <param name="spaceToCompare">The space against which the width should be evaluated.</param>
        /// <param name="spaceToEvaluate">The space to evaluate for line of sight overlap.</param>
        /// <returns>The amount of the space to evaluate's width that overlaps without obstruction with the space to compare.</returns>
        /// <exception cref="ArgumentException">Thrown if axis is Y.</exception>
        private Single GetLineOfSightSizeInAxis(
            AxisType axis, RectValue spaceToEvaluate, RectValue spaceToCompare, IEnumerable<RectValue> allSpaces)
        {
            if (axis == AxisType.Y) throw new ArgumentException("axis cannot be Y");

            // If the space to evaluate does not overlap with the space to compare, just return zero.
            Boolean overlapsInX =
                spaceToEvaluate.X.LessThan(spaceToCompare.X + spaceToCompare.Width) &&
                (spaceToEvaluate.X + spaceToEvaluate.Width).GreaterThan(spaceToCompare.X);
            Boolean overlapsInZ =
                spaceToEvaluate.Z.LessThan(spaceToCompare.Z + spaceToCompare.Depth) &&
                (spaceToEvaluate.Z + spaceToEvaluate.Depth).GreaterThan(spaceToCompare.Z);
            if (!overlapsInX || !overlapsInZ) return 0;

            // Store the space which is above the other, so that we can check for intervening spaces.
            RectValue maxYSpace = spaceToCompare.Y.GreaterThan(spaceToEvaluate.Y) ? spaceToCompare : spaceToEvaluate;
            RectValue minYSpace = spaceToCompare.Y.GreaterThan(spaceToEvaluate.Y) ? spaceToEvaluate : spaceToCompare;

            // Get the spaces between spaceToEvaluate and spaceToCompare. These are the spaces
            // that we will check to see if the obstruct spaceToEvaluate.
            List<RectValue> possibleSpaces = new List<RectValue>();
            foreach (RectValue r in allSpaces)
            {
                Boolean overlaps =
                    // Overlaps spaceToEvaluate in X and Z
                    Math.Max(r.X, spaceToEvaluate.X)
                        .LessThan(Math.Min(r.X + r.Width, spaceToEvaluate.X + spaceToEvaluate.Width)) &&
                    Math.Max(r.Z, spaceToEvaluate.Z)
                        .LessThan(Math.Min(r.Z + r.Depth, spaceToEvaluate.Z + spaceToEvaluate.Depth)) &&
                    // Lies between spaceToEvalate and spaceToCompare.
                    r.Y.GreaterThan(minYSpace.Y + minYSpace.Height) && r.Y.LessThan(maxYSpace.Y);
                if (overlaps) possibleSpaces.Add(r);
            }
            IEnumerable<RectValue> spacesToConsiderOrderedByAxis = possibleSpaces.OrderBy(r => r.GetCoordinate(axis));

            // Now that we've got the relevant subcomponent spaces between our two subcomponents, we can
            // iterate through them adding up the chunks of obstructing space that we will eventually need
            // to remove from the spaceToEvaluate's width.
            List<Single> obstructedChunksOfSize = new List<Single>() {0};
            Single minAxis = spaceToEvaluate.GetCoordinate(axis);
            Single maxAxis = minAxis;
            foreach (RectValue obstructingSpace in spacesToConsiderOrderedByAxis)
            {
                // If there's a gap between the start of this space and the end of the previous one, then
                // add a new chunk of space. We'll use the last entry in the list, so if there is no gap
                // we'll just be incrementing the chunk that we were on for the last space item.
                if (obstructingSpace.GetCoordinate(axis).GreaterThan(maxAxis))
                {
                    obstructedChunksOfSize.Add(0);
                }

                // Re-calculate the min and max axis values.
                minAxis = Math.Max(obstructingSpace.GetCoordinate(axis), maxAxis);
                maxAxis = obstructingSpace.GetCoordinate(axis) + obstructingSpace.GetSize(axis);

                // Increment the last chunk in the list by the distance between the min and max X values.
                obstructedChunksOfSize[obstructedChunksOfSize.Count - 1] += (maxAxis - minAxis);
            }

            // Now, we've identified all the chunks of which that we need to reduce the width of the spaceToEvaluate
            // by. The final thing we need to do is actually evaluate how much of spaceToEvaluate overlaps with
            // spaceToCompare. If all of it, we can just return its size minus the sum of the chunks. If some of it,
            // then we need to return the overlapping portion minus the sum of the chunks.
            Single minCoord =
                spaceToEvaluate.GetCoordinate(axis).LessThan(spaceToCompare.GetCoordinate(axis))
                    ? spaceToCompare.GetCoordinate(axis)
                    : spaceToEvaluate.GetCoordinate(axis);
            Single maxCoord =
                (spaceToEvaluate.GetCoordinate(axis) + spaceToEvaluate.GetSize(axis))
                    .LessThan(spaceToCompare.GetCoordinate(axis) + spaceToCompare.GetSize(axis))
                    ? spaceToEvaluate.GetCoordinate(axis) + spaceToEvaluate.GetSize(axis)
                    : spaceToCompare.GetCoordinate(axis) + spaceToCompare.GetSize(axis);

            return Math.Max(0, maxCoord - minCoord - obstructedChunksOfSize.Sum());
        }

        /// <summary>
        ///     Get the <see cref="RectValue"/> matches from the <paramref name="candidates"/> collection that are to the sides of <paramref name="target"/>.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="candidates"></param>
        /// <param name="rectValuesToLeft"></param>
        /// <param name="rectValuesToRight"></param>
        private static void GetRectValuesToTheSides(
            RectValue target,
            IEnumerable<RectValue> candidates,
            out List<RectValue> rectValuesToLeft,
            out List<RectValue> rectValuesToRight)
        {
            rectValuesToLeft = new List<RectValue>();
            rectValuesToRight = new List<RectValue>();

            foreach (RectValue candidate in candidates)
            {
                // The candidate must overlap in Y.
                if (!Math.Max(target.Y, candidate.Y).LessThan(Math.Min(target.Y + target.Height, candidate.Y + candidate.Height))) continue;

                // The candidate must overlap in Z.
                if (!Math.Max(target.Z, candidate.Z).LessThan(Math.Min(target.Z + target.Depth, candidate.Z + candidate.Depth))) continue;

                // The candidate must not overlap in X, because these obstructions will already have been
                // accounted for when the height was calculated.
                if (Math.Max(target.X, candidate.X).LessThan(Math.Min(target.X + target.Width, candidate.X + candidate.Width))) continue;

                // If we reach this stage, the candidate is either on the left or the right. We
                // just need to put it in the appropriate list.
                if (target.X.GreaterOrEqualThan(candidate.X + candidate.Width))
                {
                    rectValuesToLeft.Add(candidate);
                }
                else
                {
                    rectValuesToRight.Add(candidate);
                }
            }
        }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            PlanogramMerchandisingSpace otherSpace = obj as PlanogramMerchandisingSpace;
            if (otherSpace == null) return false;

            if (Obstructions.Count() != otherSpace.Obstructions.Count()) return false;
            for (Int32 i = 0; i < Obstructions.Count(); i++)
            {
                if (Obstructions.ElementAt(i) != otherSpace.Obstructions.ElementAt(i)) return false;
            }

            return UnhinderedSpace == otherSpace.UnhinderedSpace && SpaceReducedByObstructions == otherSpace.SpaceReducedByObstructions;
        }

        public override int GetHashCode()
        {
            return UnhinderedSpace.GetHashCode() + Obstructions.GetHashCode() + SpaceReducedByObstructions.GetHashCode();
        }

        #endregion
    }
}
