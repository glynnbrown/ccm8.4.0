﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-27858 : A.Kuszyk
////  Created.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.Planograms.Model;
//using Galleria.Framework.DataStructures;
//using Galleria.Framework.Planograms.Helpers;
//using Galleria.Framework.Helpers;
//using Galleria.Framework.Enums;

//namespace Galleria.Framework.Planograms.Merchandising
//{
//    /// <summary>
//    /// Represents the space available on a Merchandising Group, within a Blocking Location.
//    /// </summary>
//    public sealed class PlanogramMerchandisingGroupBlockSpacing
//    {
//        #region Fields
//        private List<PlanogramBlockingLocation> _locationsIntersectingMerchGroup;
//        private Dictionary<PlanogramBlockingLocation, WidthHeightDepthValue> _availableSpaceByBlockingLocation;
//        private WidthHeightDepthValue _totalAvailableSpace; 
//        #endregion

//        #region Properties
//        /// <summary>
//        /// The Merchandising Group that this item refers to.
//        /// </summary>
//        public PlanogramMerchandisingGroup MerchandisingGroup { get; private set; }

//        /// <summary>
//        /// The Blocking Group that MerchandisingGroup intersects with.
//        /// </summary>
//        public PlanogramBlockingGroup BlockingGroup { get; private set; }

//        /// <summary>
//        /// Gets the Blocking Locations that intersect with the MerchandisingGroup.
//        /// </summary>
//        public IEnumerable<PlanogramBlockingLocation> IntersectingLocations 
//        {
//            get { return _locationsIntersectingMerchGroup; }
//        }

//        /// <summary>
//        /// Indicates whether or not the BlockingGroup intersects with the MerchandisingGroup.
//        /// </summary>
//        public Boolean IntersectsMerchGroup 
//        { 
//            get 
//            { 
//                return _locationsIntersectingMerchGroup.Count > 0; 
//            } 
//        } 
//        #endregion

//        #region Constructor
//        /// <summary>
//        /// Creates a new object from the given merchandising group and blocking group, 
//        /// calculating whether the merchandising group intersects with the blocking group
//        /// and exposing the result through the IntersectsMerchGroup property.
//        /// </summary>
//        public PlanogramMerchandisingGroupBlockSpacing(PlanogramMerchandisingGroup merchandisingGroup, PlanogramBlockingGroup blockingGroup)
//        {
//            MerchandisingGroup = merchandisingGroup;
//            BlockingGroup = blockingGroup;

//            _locationsIntersectingMerchGroup = new List<PlanogramBlockingLocation>();
//            foreach (var location in blockingGroup.GetBlockingLocations())
//            {
//                var locationDvp = location.GetDesignViewPosition();

//                Boolean xOverlap = BlockingHelper.GetOverlap(
//                    locationDvp.BoundX,
//                    locationDvp.BoundWidth,
//                    merchandisingGroup.DesignViewPosition.BoundX,
//                    merchandisingGroup.DesignViewPosition.BoundWidth).
//                    GreaterThan(0);

//                Boolean yOverlap = BlockingHelper.GetOverlap(
//                    locationDvp.BoundY,
//                    locationDvp.BoundHeight,
//                    merchandisingGroup.DesignViewPosition.BoundX,
//                    merchandisingGroup.DesignViewPosition.BoundHeight).
//                    GreaterThan(0);

//                if (xOverlap && yOverlap) _locationsIntersectingMerchGroup.Add(location);
//            }
//        } 
//        #endregion

//        #region Methods
//        public Single GetAvailableSpaceX(PlanogramBlockingLocation blockingLocation, Single y, Single z)
//        {
//            return MerchandisingGroup.GetAvailableSpace(AxisType.X, new PointValue(0, y, z), blockingLocation);
//        }

//        public Single GetAvailableSpaceY(PlanogramBlockingLocation blockingLocation, Single x, Single z)
//        {
//            return MerchandisingGroup.GetAvailableSpace(AxisType.Y, new PointValue(x, 0, z), blockingLocation);
//        }

//        public Single GetAvailableSpaceZ(PlanogramBlockingLocation blockingLocation, Single x, Single y)
//        {
//            return MerchandisingGroup.GetAvailableSpace(AxisType.Z, new PointValue(x, y, 0), blockingLocation);
//        }
//        #endregion
//    }
//}
