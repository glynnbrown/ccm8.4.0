﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32982 : A.Kuszyk
//  Created.
// CCM-13871 : A.Silva
//  Amended how MerchandiseInventoryChange undoes changes after a failed layout so that it is faster and accurate.
// CCM-18503 : A.Kuszyk
//  Ensured that edits are applied and cancelled inline with merchandising group behaviour.
// CCM-18552 : A.Kuszyk
//  Protected against situation where products are multi-sited within sequence group.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;
using static Galleria.Framework.Planograms.Logging.DebugLayout;
using System.Reflection;
using Galleria.Framework.Helpers;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Encapsulates some of the functionality required to merchandise inventory changes on 
    /// plans, utilising the layout code in the <see cref="PlanogramMerchandisingBlocks"/> object
    /// graph.
    /// </summary>
    /// <typeparam name="TTaskContext"></typeparam>
    public class PlanogramMerchandiser<TTaskContext> where TTaskContext : class, IPlanogramMerchandiserContext
    {
        #region Fields

        /// <summary>
        /// The context required for the merchandising methods to execute.
        /// </summary>
        private TTaskContext _taskContext;

        /// <summary>
        /// Required to ensure assortment rules are enforced.
        /// </summary>
        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();

        /// <summary>
        /// Stores the Gtins that were last cleaned by the <see cref="CleanPositionsToIgnore"/> method.
        /// </summary>
        private IEnumerable<String> _gtinsRemovedOnLastClean = new List<String>();

        /// <summary>
        /// Used to cache the gtins of the top ranekd products, because we only want to evaluate this once
        /// (otherwise, we risk it changing as products are added to the Positions to Ignore collection).
        /// </summary>
        private HashSet<String> _topRankedProductGtins = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Prepares a new instance for merchandising operations with the given <paramref name="context"/>.
        /// </summary>
        /// <param name="context"></param>
        public PlanogramMerchandiser(TTaskContext context)
        {
            _taskContext = context;
        } 

        #endregion

        #region Methods

        /// <summary>
        /// Performs a blanket inventory change to all the <paramref name="blockingGroups"/> provided,
        /// using the <paramref name="targetUnitsBySequenceColour"/> for the unit information.
        /// </summary>
        /// <param name="blockingGroups"></param>
        /// <param name="targetUnitsBySequenceColour"></param>
        /// <returns>True if the inventory change was successful, or false if it wasn't.</returns>
        /// <remarks>
        /// Unsuccessful inventory changes are reverted by this method back to the previously successful
        /// inventory state. False is returned simply to indicate that the change was unsuccessful.
        /// </remarks>
        public Boolean MerchandiseInventoryChange(
            IEnumerable<PlanogramBlockingGroup> blockingGroups,
            Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour)
        {
            var sequenceColorSet = new HashSet<Int32>(targetUnitsBySequenceColour.Keys);
            List<PlanogramBlockingGroup> groupsInSequenceColorSet =
                blockingGroups.Where(blockingGroup => sequenceColorSet.Contains(blockingGroup.Colour)).ToList();
            var isLayoutSuccess = true;
            _taskContext.MerchandisingGroups.BeginEdit();
            foreach (PlanogramBlockingGroup blockingGroup in groupsInSequenceColorSet)
            {
                if (TryBlockingGroupInventoryLayout(blockingGroup, targetUnitsBySequenceColour)) continue;

                isLayoutSuccess = false;
                break;
            }

            if (isLayoutSuccess)
            {
                // If the layout was a success, commit the changed unit values to the context's
                // target units dictionary, which holds the baseline units information.
                _taskContext.MerchandisingGroups.ApplyEdit();
                SaveNewUnitsToTargetUnits(groupsInSequenceColorSet, targetUnitsBySequenceColour);
                return true;
            }

            Debug.WriteLine($"{nameof(MerchandiseInventoryChange)} will roll back layouts as some failed.");
            
            // Cancel all edits to restore the merchandising groups back to their initial state.
            foreach(PlanogramMerchandisingGroup merchandisingGroup in _taskContext.MerchandisingGroups)
            {
                while(merchandisingGroup.EditLevel > 1)
                {
                    merchandisingGroup.CancelEdit();
                }
            }

            return false;
        }

        /// <summary>
        /// Commits the <paramref name="blockUnitsBySequenceColour"/> the context's target units dictionary.
        /// </summary>
        /// <param name="blocksToIncrease"></param>
        /// <param name="blockUnitsBySequenceColour"></param>
        private void SaveNewUnitsToTargetUnits(
            IEnumerable<PlanogramBlockingGroup> blocksToIncrease, 
            Dictionary<Int32, Dictionary<String, Int32>> blockUnitsBySequenceColour)
        {
            // commit block inventory face up
            foreach (PlanogramBlockingGroup blockingGroup in blocksToIncrease)
            {
                Dictionary<String, Int32> blockUnits;
                if (!blockUnitsBySequenceColour.TryGetValue(blockingGroup.Colour, out blockUnits)) continue;

                Dictionary<String, Int32> targetUnits;
                if (!_taskContext.TargetUnitsBySequenceColour.TryGetValue(blockingGroup.Colour, out targetUnits)) continue;

                foreach (String gtin in blockUnits.Keys.ToList())
                {
                    _taskContext.LogFinalUnits(gtin, blockUnits[gtin]);

                    // commit block inventory increase
                    targetUnits[gtin] = blockUnits[gtin];
                }
            }
        }

        /// <summary>
        /// Merchandises a <paramref name="blockingGroup"/> using the target units in the given
        /// <paramref name="targetUnitsBySequenceColour"/> dictionary.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="targetUnitsBySequenceColour"></param>
        /// <returns></returns>
        private Boolean TryBlockingGroupInventoryLayout(
            PlanogramBlockingGroup blockingGroup,
            Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour)
        {
            // Prepare the merchandising blocks that represent the space that we're going to try to make
            // a change into.
            PlanogramMerchandisingBlock merchandisingBlock = new PlanogramMerchandisingBlocks(
                _taskContext.Blocking,
                _taskContext.ProductStacksBySequenceColour,
                _taskContext.MerchandisingGroups,
                _taskContext.BlockingWidth,
                _taskContext.BlockingHeight,
                _taskContext.BlockingHeightOffset,
                blockingGroup,
                _taskContext.SpaceConstraint,
                AssortmentRuleEnforcer)
                .First(b => b.BlockingGroup == blockingGroup);

            merchandisingBlock.BeginMerchandisingEdit();

            // Try to find the target units dictionary for this block.
            Dictionary<String, Int32> targetUnits;
            if (!targetUnitsBySequenceColour.TryGetValue(merchandisingBlock.SequenceGroup.Colour, out targetUnits)) return false;

            // Layout the block prepared for the change.
            LayoutBlock(_taskContext, merchandisingBlock, targetUnits);

            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin = GetPositionPlacementsByGtin(merchandisingBlock);

            if (IsLayoutSuccess(targetUnits, positionPlacementsByGtin))
            {
                // We do not apply edits here, because we want to preserve the merchandising group
                // edit history until we have layed out all the blocks. At this stage, we can then
                // apply or cancel all edits. If we apply edits here, then the changes are committed
                // to the planogram and can't be cancelled later.
                return true;
            }
            else
            {
                merchandisingBlock.CancelMerchandisingEdit();
                DebugTakeSnapshot(blockingGroup.Parent.Parent.Parent.Copy().Planograms.First(), $"{nameof(TryBlockingGroupInventoryLayout)} - layout failure");
                return false;
            }
        }

        /// <summary>
        /// Merchandises an inventory change of one additional unit (although a larger change may result) for
        /// the given <paramref name="positionPlacement"/>, basing the inventory change on the target units dictionary
        /// in the context.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns>True if an inventory change was possible, or false if it wasn't.</returns>
        public Boolean MerchandiseInventoryChange(PlanogramPositionPlacement positionPlacement)
        {
            return MerchandiseInventoryChange(new[] { positionPlacement });
        }

        /// <summary>
        /// Merchandises inventory changes of one additional unit (although a larger change may result) for the
        /// given <paramref name="positionPlacements"/>, basing the inventory changes on the target units dictionary
        /// in the context.
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <returns>True if the inventory changes were possible, or false if not.</returns>
        public Boolean MerchandiseInventoryChange(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            PlanogramPositionPlacement firstPosition = positionPlacements.First();
            if (!firstPosition.Position.SequenceColour.HasValue) return false;
            Int32 sequenceColour = firstPosition.Position.SequenceColour.Value;

            PlanogramBlockingGroup blockingGroup;
            if (!_taskContext.BlockingGroupsByColour.TryGetValue(sequenceColour, out blockingGroup)) return false;

            Stack<PlanogramProduct> productStack;
            if (!_taskContext.ProductStacksBySequenceColour.TryGetValue(sequenceColour, out productStack)) return false;

            Dictionary<String, Int32> targetUnits;
            if (!_taskContext.TargetUnitsBySequenceColour.TryGetValue(sequenceColour, out targetUnits)) return false;

            if (IncreaseTargetUnits(positionPlacements, targetUnits)) return true;

            // Prepare the merchandising blocks that represent the space that we're going to try to make
            // a change into.
            PlanogramMerchandisingBlock merchandisingBlock = new PlanogramMerchandisingBlocks(
                _taskContext.Blocking,
                _taskContext.ProductStacksBySequenceColour,
                _taskContext.MerchandisingGroups,
                _taskContext.BlockingWidth,
                _taskContext.BlockingHeight,
                _taskContext.BlockingHeightOffset,
                blockingGroup,
                spaceConstraintType: _taskContext.SpaceConstraint,
                assortmentRuleEnforcer: AssortmentRuleEnforcer)
                .FirstOrDefault(b => b.BlockingGroup == blockingGroup);
            if (merchandisingBlock == null) return false;
            merchandisingBlock.BeginMerchandisingEdit();

            // Layout the block prepared for the change.
            LayoutBlock(_taskContext, merchandisingBlock, targetUnits);

            // Find the position that we increased (it isn't the same instance anymore and will be a clone).
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin = GetPositionPlacementsByGtin(merchandisingBlock);

            // Ensure that we placed all products at their target units and that we didn't drop any products.
            if (IsLayoutSuccess(positionPlacements, targetUnits, positionPlacementsByGtin))
            {
                ApplyChanges(blockingGroup, merchandisingBlock, positionPlacementsByGtin);
                DebugTakeSnapshot(blockingGroup.Parent.Parent.Parent.Copy().Planograms.First(), $"{nameof(MerchandiseInventoryChange)} - layout success");
                return true;
            }
            else
            {
                RevertChanges(positionPlacements, blockingGroup, targetUnits, merchandisingBlock);
                DebugTakeSnapshot(blockingGroup.Parent.Parent.Parent.Copy().Planograms.First(), $"{nameof(MerchandiseInventoryChange)} - layout failure");
                return false;
            }
        }

        /// <summary>
        /// Cancels the merchandising edit on the <paramref name="merchandisingBlock"/> and restores unit
        /// values in the context's target units dictionary.
        /// </summary>
        /// <param name="placements"></param>
        /// <param name="blockingGroup"></param>
        /// <param name="targetUnits"></param>
        /// <param name="merchandisingBlock"></param>
        private void RevertChanges(
            IEnumerable<PlanogramPositionPlacement> placements,
            PlanogramBlockingGroup blockingGroup,
            Dictionary<String, Int32> targetUnits,
            PlanogramMerchandisingBlock merchandisingBlock)
        {
            // If the layout was not a success, we need to reverse the change we have made. This involves 
            // reverting our previous merchandising block edit.
            merchandisingBlock.CancelMerchandisingEdit();

            // We need to re-get the position placements, because the instances may have changed after the cancel.
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin = GetPositionPlacementsByGtin(merchandisingBlock);

            // Next, revert the target units for all the postitions.
            foreach (PlanogramPositionPlacement placement in placements)
            {
                targetUnits[placement.Product.Gtin]--;
            }


            if (!IsLayoutSuccess(placements, targetUnits, positionPlacementsByGtin))
            {
                System.Diagnostics.Debug.Fail("Products should never be dropped after an unsuccessful inventory change");
            }

            // only ignore if we are increasing a single position
            if (placements.Count() == 1)
            {
                String firstPositionGtin = placements.First().Product.Gtin;

                // If no units could be increase, ignore this position in the future.
                if (!_taskContext.PositionsToIgnore.Contains(firstPositionGtin))
                {
                    _taskContext.PositionsToIgnore.Add(firstPositionGtin);
                }

                System.Diagnostics.Debug.WriteLine("Failed product inventory {0} - Block {1} Rank {2}", firstPositionGtin, blockingGroup.Name, _taskContext.RanksByProductGtin.ContainsKey(firstPositionGtin) ? _taskContext.RanksByProductGtin[firstPositionGtin] : 0);
                System.Diagnostics.Debug.WriteLine("---");
            }
        }

        /// <summary>
        /// Applies the merchandising edit to the <paramref name="merchandisingBlock"/> and logs the final units.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="merchandisingBlock"></param>
        /// <param name="positionPlacementsByGtin"></param>
        private void ApplyChanges(
            PlanogramBlockingGroup blockingGroup, 
            PlanogramMerchandisingBlock merchandisingBlock, 
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin)
        {
            foreach (IGrouping<String,PlanogramPositionPlacement> placedPositions in positionPlacementsByGtin)
            {
                Int32 placedUnits = placedPositions.Sum(g => g.Position.TotalUnits);
                _taskContext.LogFinalUnits(placedPositions.First());
                System.Diagnostics.Debug.WriteLine("Product Inventory increased {0} - Units {1} - Block {2} Rank {3}", placedPositions.Key, placedUnits, blockingGroup.Name, _taskContext.RanksByProductGtin.ContainsKey(placedPositions.Key) ? _taskContext.RanksByProductGtin[placedPositions.Key] : 0);
                System.Diagnostics.Debug.WriteLine("---");
            }

            // Apply any changes to the merchandising groups for the planogram.
            merchandisingBlock.ApplyMerchandisingEdit();
        }

        /// <summary>
        /// Determines if the layout was a success, based on range change and inventory achievement.
        /// </summary>
        /// <param name="placements"></param>
        /// <param name="targetUnits"></param>
        /// <param name="positionPlacementsByGtin"></param>
        /// <returns>True if the layout was a success.</returns>
        private static Boolean IsLayoutSuccess(
            IEnumerable<PlanogramPositionPlacement> placements,
            Dictionary<String, Int32> targetUnits,
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin)
        {
            // If there was no change in range, we didn't drop products. If there was, the layout wasn't successful.
            Int32 gtinCount = positionPlacementsByGtin.Select(g => g.Key).Count();
            Boolean rangeRemainedTheSame = gtinCount == targetUnits.Keys.Count;
            if (!rangeRemainedTheSame)
            {
                Debug.WriteLine($"Layout was unsuccesful because the range ({gtinCount}) was different than expected ({targetUnits.Keys.Count})");
                return false;
            }

            foreach (PlanogramPositionPlacement placement in placements)
            {
                // Find the position that we increased (it isn't the same instance anymore and will be a clone).
                if (!positionPlacementsByGtin.Contains(placement.Product.Gtin)) continue;
                IEnumerable<PlanogramPositionPlacement> placedPositions = positionPlacementsByGtin[placement.Product.Gtin];

                // Ensure that we placed all products at their target units..
                Int32 placedUnits = placedPositions.Sum(p => p.Position.TotalUnits);
                Boolean achievedTargetUnits = placedUnits >= targetUnits[placement.Product.Gtin];
                if (achievedTargetUnits) continue;

                Debug.WriteLine($"Layout was unsuccesful because the product ({placement.Product.Gtin}) did not achieve its target {targetUnits[placement.Product.Gtin]} units (was {placedUnits})");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if the layout was a success, based on range change and inventory achievement.
        /// </summary>
        /// <param name="targetUnits"></param>
        /// <param name="positionPlacementsByGtin"></param>
        /// <returns>True if the layout was a success.</returns>
        private static Boolean IsLayoutSuccess(
            Dictionary<String, Int32> targetUnits,
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin)
        {
            // If there was no change in range, we didn't drop products. If there was, the layout wasn't successful.
            Int32 gtinCount = positionPlacementsByGtin.Select(p => p.Key).Count();
            Boolean rangeRemainedTheSame = gtinCount == targetUnits.Keys.Count;
            if (!rangeRemainedTheSame)
            {
                Debug.WriteLine($"Layout was unsuccesful because the range ({gtinCount}) was different than expected ({targetUnits.Keys.Count})");
                return false;
            }

            foreach (IGrouping<String,PlanogramPositionPlacement> placedPositions in positionPlacementsByGtin)
            {
                // Ensure that we placed all products at their target units..
                Int32 placedUnits = placedPositions.Sum(p => p.Position.TotalUnits);
                Boolean achievedTargetUnits = placedUnits >= targetUnits[placedPositions.Key];
                if (achievedTargetUnits) continue;

                Debug.WriteLine($"Layout was unsuccesful because the product ({placedPositions.Key}) did not achieve its target {targetUnits[placedPositions.Key]} units (was {placedUnits})");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Increases the target units for the given <paramref name="placements"/> in the <paramref name="targetUnits"/>
        /// dictionary.
        /// </summary>
        /// <param name="placements"></param>
        /// <param name="targetUnits"></param>
        /// <returns>True if all the placements were already at their target units, or false if at least one was not.</returns>
        private Boolean IncreaseTargetUnits(
            IEnumerable<PlanogramPositionPlacement> placements, Dictionary<String, Int32> targetUnits)
        {
            Boolean allPlacementsAlreadyAtTargetUnits = true;
            foreach (PlanogramPositionPlacement placement in placements)
            {
                //  If there are no starting units for this product,
                //  register the current units, this must be the first time
                //  that increase inventory is attempted on this product.
                _taskContext.LogStartingUnits(placement);

                // Increase the target units by one so that when we layout the block, the units
                // will be increased.
                targetUnits[placement.Product.Gtin]++;

                if (placement.Position.TotalUnits < targetUnits[placement.Product.Gtin])
                {
                    allPlacementsAlreadyAtTargetUnits = false;
                }
            }
            return allPlacementsAlreadyAtTargetUnits;
        }

        /// <summary>
        /// Invokes the layout of the <paramref name="merchandisingBlock"/>.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="merchandisingBlock"></param>
        /// <param name="targetUnits"></param>
        private static void LayoutBlock(
            TTaskContext context, 
            PlanogramMerchandisingBlock merchandisingBlock, 
            Dictionary<String, Int32> targetUnits)
        {
            IPlanogramMerchandisingLayoutContext<TTaskContext> layoutContext =
                new PlanogramMerchandisingLayoutContext<TTaskContext>(
                    targetUnits,
                    context.Planogram,
                    /*shouldCompactSpace*/ true,
                    new Stack<PlanogramProduct>(context.ProductStacksBySequenceColour[merchandisingBlock.SequenceGroup.Colour].Reverse()),
                    context.InventoryChangeType);
            merchandisingBlock.Layout<TTaskContext>(layoutContext);
        }

        /// <summary>
        /// Re-fetches the position placements from the <paramref name="merchandisingBlock"/>.
        /// </summary>
        /// <param name="merchandisingBlock"></param>
        /// <returns></returns>
        private static ILookup<String, PlanogramPositionPlacement> GetPositionPlacementsByGtin(
            PlanogramMerchandisingBlock merchandisingBlock)
        {
            return merchandisingBlock.BlockPlacements
                .Select(bp => bp.MerchandisingGroup)
                .Distinct()
                .SelectMany(bp => bp.PositionPlacements.Where(p => p.Position.SequenceColour.Equals(merchandisingBlock.BlockingGroup.Colour)))
                .ToLookup(p => p.Product.Gtin, p => p);
        }

        /// <summary>
        /// Removes gtins from the PositionsToIgnore property on the context, which relate to positions
        /// that may have enough space on their merchandising groups to have an increase applied.
        /// </summary>
        public void CleanPositionsToIgnore()
        {
            ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin = _taskContext.MerchandisingGroups
                .SelectMany(m => m.PositionPlacements)
                .ToLookup(p => p.Product.Gtin);

            // Build a list of gtins that we should remove from the positions to ignore list.
            // I.e. the products should have another inventory increas attempted for them.
            List<String> gtinsToRemove = new List<String>();
            foreach(String gtin in _taskContext.PositionsToIgnore)
            {
                // Find the positions for this product gtin.
                if (!positionPlacementsByGtin.Contains(gtin)) continue;
                IEnumerable<PlanogramPositionPlacement> matchingPositions = positionPlacementsByGtin[gtin];
                if (!matchingPositions.Any()) continue;

                // Calculate the minimum possible size that this product will required in order
                // to increase its units. This is the minimum of its squeezed width, height or depth.
                PlanogramProduct product = matchingPositions.First().Product;
                Single minSize = Math.Min(Math.Min(product.SqueezeWidthActual, product.SqueezeHeightActual), product.SqueezeDepthActual);

                // Do any of the positions sit on a merchandising group where the available white space
                // in the facing axis allows for the minimum unit size of the product.
                Boolean anyMatchingPositionHasSpaceOnMerchGroup = matchingPositions.Any(p =>
                {
                    Single whiteSpace = p.MerchandisingGroup.GetWhiteSpaceOnAxis(p.MerchandisingGroup.GetFacingAxis(), p);
                    return whiteSpace.GreaterOrEqualThan(minSize);
                });

                // If there is space for another unit, add the gtin to the list of gtins to re-try.
                if (anyMatchingPositionHasSpaceOnMerchGroup) gtinsToRemove.Add(gtin);
            }

            // Remove the gtins from the positions to ignore list, but beware of adding these gtins over
            // and over. We could conceivably be adding the products to be re-tried here, despite the
            // fact that they won't actually fit on the plan. We only attempt the gtins that we didn't
            // try last time this method was called.
            IEnumerable<String> gtinsToRemoveExceptTheOnesOnTheLastCall = gtinsToRemove
                .Except(_gtinsRemovedOnLastClean).ToList();
            foreach (String gtinToRemove in gtinsToRemoveExceptTheOnesOnTheLastCall)
            {
                Debug.WriteLine($"Re-added a previously removed product: {gtinToRemove}");
                _taskContext.PositionsToIgnore.Remove(gtinToRemove);
            }

            _gtinsRemovedOnLastClean = gtinsToRemoveExceptTheOnesOnTheLastCall;
        }

        #region Enumerate Products Methods
        /// <summary>
        /// Enumerates a position for each product on the plan, taking the position with the lowest units
        /// in the case of multi-sited products.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramPositionPlacement> EnumerateProductPositionsWithMinUnits()
        {
            // Get a list of all the positions on the plan.
            IEnumerable<PlanogramPositionPlacement> allPlacements = _taskContext.MerchandisingGroups
                .SelectMany(m => m.PositionPlacements)
                .Where(p => !_taskContext.PositionsToIgnore.Contains(p.Product.Gtin))
                .ToList();

            // Arrange the positions in ascending units order.
            IEnumerable<PlanogramPositionPlacement> allPositionsInUnitsOrder = allPlacements
                .OrderBy(p => p.GetPositionDetails().TotalUnitCount)
                .ToList();

            // For each distinct product in the positions, take the first position that appears in the
            // ordered positions list. This gives us a position for each placed product, with the minimum
            // units in the case of multi-sited products.
            return allPlacements
                .Select(p => p.Product)
                .Distinct()
                .Select(prod => allPositionsInUnitsOrder.First(pos => pos.Product.Id.Equals(prod.Id)));
        }

        /// <summary>
        /// Enumerates a position for each placed product that passes the selection criteria, in the order 
        /// specified in <see cref="ProcessingOrder"/>.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramPositionPlacement> EnumerateAllProductsInOrder(PlanogramMerchandiserProcessingOrderType processingOrder)
        {
            switch (processingOrder)
            {
                case PlanogramMerchandiserProcessingOrderType.ByAchievedDos:
                    // Return positions for all products with performance data in 
                    // ascending achieved days of supply order.
                    return EnumerateProductPositionsWithMinUnits()
                        .Where(HasAchievedDosValue)
                        .OrderBy(GetAchievedDos)
                        .ThenBy(p => HasRank(p) ? GetRank(p) : Int32.MaxValue)
                        .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByRank:
                    // Return positions for all products that have a rank, in ascending
                    // rank order.
                    return EnumerateProductPositionsWithMinUnits()
                        .Where(HasRank)
                        .OrderBy(GetRank)
                        .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByLowestDos:
                    // Return positions with the lowest dos value.
                    IEnumerable<PlanogramPositionPlacement> positionsWithAchievedDos = EnumerateProductPositionsWithMinUnits()
                        .Where(HasAchievedDosValue)
                        .ToList();
                    if (!positionsWithAchievedDos.Any()) return positionsWithAchievedDos;
                    Single lowestDos = GetLowestDos(positionsWithAchievedDos);
                    return positionsWithAchievedDos
                        .Where(p => GetAchievedDos(p).EqualTo(lowestDos))
                        .OrderBy(p => HasRank(p) ? GetRank(p) : Int32.MaxValue)
                        .ThenBy(p => p.Product.Gtin);


                default:
                    System.Diagnostics.Debug.Fail("Unknown ProcessingOrderType");
                    return new List<PlanogramPositionPlacement>();
            }
        }

        /// <summary>
        /// Finds the lowest achieved DOS value for the given <paramref name="positionPlacements"/>.
        /// </summary>
        /// <param name="positionPlacements"></param>
        /// <returns>Returns the max single value if there are no products with a dos of greater than zero.</returns>
        private Single GetLowestDos(IEnumerable<PlanogramPositionPlacement> positionPlacements)
        {
            IEnumerable<PlanogramPositionPlacement> positionPlacementsWithNonZeroDos =
                positionPlacements.Where(p => GetAchievedDos(p).GreaterThan(0));

            if (!positionPlacementsWithNonZeroDos.Any()) return Single.MaxValue;

            return positionPlacements.Min(p => GetAchievedDos(p));
        }

        /// <summary>
        /// Enumerates products that have an achieved days of supply value that is less than or equal to the average,
        /// in the order specified in <see cref="ProcessingOrder"/>.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramPositionPlacement> EnumerateProductsBelowAverageDosInOrder(
            PlanogramMerchandiserProcessingOrderType processingOrder,
            Single averageAchievedDos)
        {
            if (!_taskContext.PerformanceDataByProductId.Values.Any(p => p.AchievedDos.HasValue))
            {
                return new List<PlanogramPositionPlacement>();
            }

            switch (processingOrder)
            {
                case PlanogramMerchandiserProcessingOrderType.ByAchievedDos:
                    // Return positions for all products with performance data in 
                    // ascending achieved days of supply order.
                    return EnumerateProductPositionsWithMinUnits()
                        .Where(HasAchievedDosValue)
                        .Where(p => GetAchievedDos(p).LessOrEqualThan(averageAchievedDos))
                        .OrderBy(GetAchievedDos)
                        .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByRank:
                    // Return positions for all products that have a rank, in ascending
                    // rank order.
                    return EnumerateProductPositionsWithMinUnits()
                        .Where(HasAchievedDosValue)
                        .Where(p => GetAchievedDos(p).LessOrEqualThan(averageAchievedDos))
                        .Where(HasRank)
                        .OrderBy(GetRank)
                        .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByLowestDos:
                    IEnumerable<PlanogramPositionPlacement> belowAveragePositions = EnumerateProductPositionsWithMinUnits()
                        .Where(HasAchievedDosValue)
                        .Where(p => GetAchievedDos(p).LessOrEqualThan(averageAchievedDos))
                        .ToList();
                    if (!belowAveragePositions.Any()) return belowAveragePositions;
                    Single lowestDos = GetLowestDos(belowAveragePositions);
                    return belowAveragePositions
                        .Where(p => GetAchievedDos(p).EqualTo(lowestDos))
                        .OrderBy(p => HasRank(p) ? GetRank(p) : Int32.MaxValue)
                        .ThenBy(p => p.Product.Gtin);

                default:
                    System.Diagnostics.Debug.Fail("Unknown ProcessingOrderType");
                    return new List<PlanogramPositionPlacement>();
            }
        }

        /// <summary>
        /// Enumerates the top ranked products in the order specified in <see cref="ProcessingOrder"/>.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramPositionPlacement> EnumerateTopRankedProductsInOrder(
            PlanogramMerchandiserProcessingOrderType processingOrder,
            Single? percentageOfRankedProducts)
        {
            if (!percentageOfRankedProducts.HasValue) return new List<PlanogramPositionPlacement>();

            if (_topRankedProductGtins == null)
            {
                _topRankedProductGtins = GetTopRankedProductsGtins(processingOrder, percentageOfRankedProducts);
            }

            switch (processingOrder)
            {
                case PlanogramMerchandiserProcessingOrderType.ByAchievedDos:
                    return EnumerateProductPositionsWithMinUnits()
                    .Where(p => _topRankedProductGtins.Contains(p.Product.Gtin))
                    .OrderBy(GetRank)
                    .ThenBy(p => p.Product.Gtin)
                    .OrderBy(GetAchievedDos)
                    .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByRank:
                    return EnumerateProductPositionsWithMinUnits()
                        .Where(p => _topRankedProductGtins.Contains(p.Product.Gtin))
                        .OrderBy(GetRank)
                        .ThenBy(p => p.Product.Gtin);

                case PlanogramMerchandiserProcessingOrderType.ByLowestDos:
                    IEnumerable<PlanogramPositionPlacement> topRankedProducts = EnumerateProductPositionsWithMinUnits()
                        .Where(p => _topRankedProductGtins.Contains(p.Product.Gtin))
                        .Where(HasAchievedDosValue)
                        .ToList();
                    if (!topRankedProducts.Any()) return topRankedProducts;
                    Single lowestDos = GetLowestDos(topRankedProducts);
                    return topRankedProducts
                        .Where(p => GetAchievedDos(p).EqualTo(lowestDos))
                        .OrderBy(p => HasRank(p) ? GetRank(p) : Int32.MaxValue)
                        .ThenBy(p => p.Product.Gtin);

                default:
                    System.Diagnostics.Debug.Fail("Unknown ProcessingOrderType");
                    return new List<PlanogramPositionPlacement>();
            }
        }

        private HashSet<String> GetTopRankedProductsGtins(
            PlanogramMerchandiserProcessingOrderType processingOrder, Single? percentageOfRankedProducts)
        {
            Int32 numberOfRankedProducts =
                Convert.ToInt32(Math.Ceiling(_taskContext.RanksByProductGtin.Count * percentageOfRankedProducts.Value));

            switch (processingOrder)
            {
                case PlanogramMerchandiserProcessingOrderType.ByAchievedDos:
                case PlanogramMerchandiserProcessingOrderType.ByLowestDos:
                    // Achieved dos and lowest dos both require the position to have an achieved
                    // dos value in addition to being in the top ranking positions.
                    return new HashSet<String>(EnumerateProductPositionsWithMinUnits()
                        .Where(HasRank)
                        .OrderBy(GetRank)
                        .Where(HasAchievedDosValue)
                        .Take(numberOfRankedProducts)
                        .Select(p => p.Product.Gtin));

                case PlanogramMerchandiserProcessingOrderType.ByRank:
                    // Return positions for all products that have a rank, in ascending
                    // rank order.
                    return new HashSet<String>(EnumerateProductPositionsWithMinUnits()
                        .Where(HasRank)
                        .OrderBy(GetRank)
                        .Take(numberOfRankedProducts)
                        .Select(p => p.Product.Gtin));

                default:
                    System.Diagnostics.Debug.Fail("Unknown ProcessingOrderType");
                    return new HashSet<String>();
            }
        }

        /// <summary>
        /// Enumerates the positions with the minimum units for the selected products, in the order that has
        /// been specified by the user.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramPositionPlacement> EnumerateSelectedPositionPlacementsInOrder(
            PlanogramMerchandiserProductSelectionType productSelection,
            PlanogramMerchandiserProcessingOrderType processingOrder,
            Single averageAchievedDos,
            Single? percentageOfRankedProducts)
        {
            switch (productSelection)
            {
                case PlanogramMerchandiserProductSelectionType.AllProducts:
                    return EnumerateAllProductsInOrder(processingOrder);

                case PlanogramMerchandiserProductSelectionType.ProductsBelowAverageDos:
                    return EnumerateProductsBelowAverageDosInOrder(processingOrder, averageAchievedDos);

                case PlanogramMerchandiserProductSelectionType.TopRankedProducts:
                    return EnumerateTopRankedProductsInOrder(processingOrder, percentageOfRankedProducts);

                default:
                    System.Diagnostics.Debug.Fail("Unknown ProductSelectionType");
                    return new List<PlanogramPositionPlacement>();
            }
        }
        #region Rank and Achieved Dos methods

        /// <summary>
        /// Returns the rank of the given <paramref name="positionPlacement"/>, or 0 if it cannot be found.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns></returns>
        private Int32 GetRank(PlanogramPositionPlacement positionPlacement)
        {
            Int32 rank;
            if (_taskContext.RanksByProductGtin.TryGetValue(positionPlacement.Product.Gtin, out rank))
            {
                return rank;
            }
            return 0;
        }

        /// <summary>
        /// Returns the achieved days of supply value of the given <paramref name="positionPlacement"/>, or 0 if it cannot be found.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns></returns>
        private Single GetAchievedDos(PlanogramPositionPlacement positionPlacement)
        {
            PlanogramPerformanceData performanceData;
            if (_taskContext.PerformanceDataByProductId.TryGetValue(positionPlacement.Product.Id, out performanceData))
            {
                if (!performanceData.AchievedDos.HasValue) return 0;
                return performanceData.AchievedDos.Value;
            }
            return 0;
        }

        /// <summary>
        /// Returns true if the <paramref="positionPlacement"/> has an entry in the achieved dos dictionary.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns></returns>
        private Boolean HasAchievedDosValue(PlanogramPositionPlacement positionPlacement)
        {
            PlanogramPerformanceData performanceData;
            if (_taskContext.PerformanceDataByProductId.TryGetValue(positionPlacement.Product.Id, out performanceData))
            {
                return performanceData.AchievedDos.HasValue;
            }
            return false;
        }

        /// <summary>
        /// Returns true if the <paramref name="positionPlacement"/> has an entry in the ranks dictionary.
        /// </summary>
        /// <param name="positionPlacement"></param>
        /// <returns></returns>
        private Boolean HasRank(PlanogramPositionPlacement positionPlacement)
        {
            return _taskContext.RanksByProductGtin.ContainsKey(positionPlacement.Product.Gtin);
        }

        #endregion

        #endregion

        #endregion
    }
}
