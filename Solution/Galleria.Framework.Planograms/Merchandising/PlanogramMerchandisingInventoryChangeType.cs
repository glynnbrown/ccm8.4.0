﻿#region Header Information
// Copyright © Galleria RTS Ltd 201
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// Denotes the possible ways for inventory changes to be made when merchandising a plan.
    /// </summary>
    public enum PlanogramMerchandisingInventoryChangeType
    {
        /// <summary>
        /// Indicates that units should be added individually.
        /// </summary>
        ByUnits = 0,
        /// <summary>
        /// Indiciates that units should be added in blocks of facings.
        /// </summary>
        ByFacings = 1
    }
}
