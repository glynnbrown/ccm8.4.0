﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31784 : A.Kuszyk
//  Created.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32504 : A.Kuszyk
//  Added SkippedSubGroupProducts and removed unused constructor.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Merchandising
{
    /// <summary>
    /// An implementation of <see cref="IPlanogramMerchandisingLayoutContext"/> that provides the required information
    /// for a layout method to be executed.
    /// </summary>
    /// <typeparam name="TPostLayoutContext">The type of the context object that is used as a parameter for the <see cref="PostLayoutMethods"/>.</typeparam>
    public sealed class PlanogramMerchandisingLayoutContext<TPostLayoutContext> : IPlanogramMerchandisingLayoutContext<TPostLayoutContext> where TPostLayoutContext : class
    {
        #region Fields

        /// <summary>
        /// Holds the product stack that was originally provided to the constructor of this instance.
        /// </summary>
        private Stack<PlanogramProduct> _originalProductStack;

        #endregion

        #region Properties

        /// <summary>
        /// Stores the gtins of the products that were placed in the previous layout pass.
        /// </summary>
        public ICollection<String> PreviouslyPlacedGtins { get; private set; }

        /// <summary>
        /// A dictionary containing the ranks of all the products in the planogram, keyed by gtin.
        /// </summary>
        public Dictionary<String, Int32> RankByGtin { get; private set; }

        /// <summary>
        /// Indicates whether placed products must achieve the units specified in <see cref="TargetUnitsByGtin"/>.
        /// True indicates that the target units must be achieved, whilst false ensures that the <see cref="MinimumUnitsByGtin"/>
        /// are satisfied, even if the target units are not.
        /// </summary>
        public Boolean MustAchieveTargetUnits { get; private set; }

        /// <summary>
        /// The minimum units that products should be placed at. If these units cannot be achieved, then products
        /// will not be placed.
        /// </summary>
        public Dictionary<String, Int32> MinimumUnitsByGtin { get; private set; }

        /// <summary>
        /// The target units that products should be placed at. If <see cref="MustAchieveTargetUnits"/> is true, 
        /// then products will only be placed if these units can be achieved. Otherwise, <see cref="MinimumUnitsByGtin"/>
        /// will be satisifed and the units specified here will be achieved if possible.
        /// </summary>
        public Dictionary<String, Int32> TargetUnitsByGtin { get; private set; }

        /// <summary>
        /// The method that should be used to re-stack the <see cref="ProductsToPlace"/> stack.
        /// </summary>
        public Action<Stack<PlanogramProduct>> GenerateProductStackMethod { get; private set; }

        /// <summary>
        /// The planogram being layed out by the layout method.
        /// </summary>
        public Planogram Planogram { get; private set; }

        /// <summary>
        /// Indicates whether or not the primary sequence direction is currently being reversed in the layout.
        /// </summary>
        public Boolean ReversePrimary { get; set; }

        /// <summary>
        /// Indicates whether or not the secondary sequence direction is currently being reversed in the layout.
        /// </summary>
        public Boolean ReverseSecondary { get; set; }

        /// <summary>
        /// Indicates whether or not the tertiary sequence direction is currently being reversed in the layout.
        /// </summary>
        public Boolean ReverseTertiary { get; set; }

        /// <summary>
        /// Indicates whether or not the block space should be compacted after a successful layout.
        /// </summary>
        public Boolean ShouldCompactBlockSpace { get; private set; }

        /// <summary>
        /// The context object used by the <see cref="PostLayoutMethods"/>.
        /// </summary>
        public TPostLayoutContext PostLayoutContext { get; private set; }

        /// <summary>
        /// The methods to be executed after each layout pass, but before compacting block space.
        /// Each of these methods should return a boolean. True indicates that another layout pass
        /// should be executed immediately and this method should be executed again in the next pass. 
        /// False indicates that this method has completed its action and the next method in the collection
        /// should be used. When false is returned, the method will not be executed on subsequent layout passes.
        /// </summary>
        public IEnumerable<Func<TPostLayoutContext, Boolean>> PostLayoutMethods { get; private set; }

        /// <summary>
        /// The stack of products that should be placed by the layout code. They will be popped off in order,
        /// so this stack will have the first products to be placed at the top of the stack. This stack will be 
        /// populated by the <see cref="GenerateProductStackMethod"/>.
        /// </summary>
        public Stack<PlanogramProduct> ProductsToPlace { get; private set; }

        /// <summary>
        /// The products that have been skipped due to sequence sub-groups. This stack is cleared and re-populated
        /// on each layout pass.
        /// </summary>
        public Stack<PlanogramProduct> SkippedSubGroupProducts { get; private set; }

        /// <summary>
        /// The gtins of products that have been skipped due to first on block behaviour.
        /// </summary>
        public ICollection<String> SkippedFirstOnBlock { get; set; }

        /// <summary>
        /// The current placement direction.
        /// </summary>
        public PlanogramBlockingGroupPlacementType? CurrentSequenceDirection { get; set; }

        /// <summary>
        /// Indicates if merchandising dimension should be ignored when placing products.
        /// </summary>
        public Boolean IgnoreMerchandisingDimensions { get; private set; }

        /// <summary>
        ///     The collection of <see cref="PlanogramMerchandisingBlockPlacement"/> instances that did not get any placements 
        /// due to the <c>First on Block</c> rule.
        /// </summary>
        public HashSet<PlanogramMerchandisingBlockPlacement> SkippedBlockPlacement { get; } = new HashSet<PlanogramMerchandisingBlockPlacement>();


        /// <summary>
        /// Indicates whether the inventory of positions should be modified by increasing units or facings.
        /// </summary>
        public PlanogramMerchandisingInventoryChangeType InventoryChangeType { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new layout context, allowing post-layout methods to be injected from another location. Written
        /// for use with the Merchandise Planogram task, which has a requirement for post-layout steps.
        /// </summary>
        /// <param name="targetUnitsByGtin"></param>
        /// <param name="productsToPlace"></param>
        /// <param name="generateProductStackMethod"></param>
        /// <param name="planogram"></param>
        /// <param name="shouldCompactSpace"></param>
        /// <param name="postLayoutContext"></param>
        /// <param name="postLayoutMethods"></param>
        /// <param name="ignoreMerchandisingDimensions"></param>
        /// <param name="previouslyPlacedGtins"></param>
        /// <param name="rankByGtin"></param>
        /// <param name="skippedSubGroupProducts"></param>
        public PlanogramMerchandisingLayoutContext(
            Dictionary<String, Int32> targetUnitsByGtin,
            Stack<PlanogramProduct> productsToPlace,
            Action<Stack<PlanogramProduct>> generateProductStackMethod,
            Planogram planogram,
            Boolean shouldCompactSpace,
            TPostLayoutContext postLayoutContext,
            IEnumerable<Func<TPostLayoutContext, Boolean>> postLayoutMethods,
            Boolean ignoreMerchandisingDimensions,
            ICollection<String> previouslyPlacedGtins,
            Dictionary<String, Int32> rankByGtin,
            Stack<PlanogramProduct> skippedSubGroupProducts)
        {
            TargetUnitsByGtin = targetUnitsByGtin;
            ProductsToPlace = productsToPlace;
            GenerateProductStackMethod = generateProductStackMethod;
            Planogram = planogram;
            ShouldCompactBlockSpace = shouldCompactSpace;
            PostLayoutContext = postLayoutContext;
            PostLayoutMethods = postLayoutMethods.ToList();
            IgnoreMerchandisingDimensions = ignoreMerchandisingDimensions;
            PreviouslyPlacedGtins = previouslyPlacedGtins;
            RankByGtin = rankByGtin;
            MinimumUnitsByGtin = targetUnitsByGtin;
            SkippedSubGroupProducts = skippedSubGroupProducts;
            InventoryChangeType = PlanogramMerchandisingInventoryChangeType.ByUnits;

            SkippedFirstOnBlock = new List<String>();
            MustAchieveTargetUnits = true;
        }

        public PlanogramMerchandisingLayoutContext(
            Dictionary<String, Int32> targetUnitsByGtin,
            Planogram planogram,
            Boolean shouldCompactBlockSpace,
            Stack<PlanogramProduct> productsToPlace,
            PlanogramMerchandisingInventoryChangeType inventoryChangeType = PlanogramMerchandisingInventoryChangeType.ByUnits)
        {
            MustAchieveTargetUnits = true;
            MinimumUnitsByGtin = new Dictionary<String, Int32>();
            TargetUnitsByGtin = targetUnitsByGtin;
            Planogram = planogram;
            ShouldCompactBlockSpace = shouldCompactBlockSpace;
            ProductsToPlace = productsToPlace;
            InventoryChangeType = inventoryChangeType;
            _originalProductStack = new Stack<PlanogramProduct>(productsToPlace);

            GenerateProductStackMethod = (Action<Stack<PlanogramProduct>>)GenerateProductStack;
            ProductsToPlace = productsToPlace;
            SkippedFirstOnBlock = new List<String>();
            IgnoreMerchandisingDimensions = false;
            PostLayoutContext = null;
            PostLayoutMethods = new List<Func<TPostLayoutContext, Boolean>>();
            PreviouslyPlacedGtins = new List<String>();
            RankByGtin = planogram.Assortment.Products.ToDictionary(p => p.Gtin, p => (Int32)p.Rank);
            SkippedSubGroupProducts = new Stack<PlanogramProduct>();
        }

        /// <summary>
        /// Generates a product stack using the original stack that was provided to the constructor of this instance.
        /// </summary>
        /// <param name="productStack"></param>
        private void GenerateProductStack(Stack<PlanogramProduct> productStack)
        {
            productStack.Clear();
            foreach (PlanogramProduct product in _originalProductStack)
            {
                productStack.Push(product); 
            }
        }
 
        #endregion
    }
}
