﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25881 : A.Probyn
//  Updated so NumberOfSubComponents, NumberOfMerchandisableSubComponent, NumberOfShelfEdgeLabels are meta data
// V8-25270 : L.Ineson
//  Added IdentifyComponentType helper method.
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-26442 : I.George
//  Added Custom Attributes
// V8-27126 : L.Ineson
//  Added UpdateSizeFromSubComponents
// V8-27058 : A.Probyn
//  Added CalculateMetaData
//  Added ClearMetaData
// V8-27403 : A.Kuszyk
//  Added IsMerchandisable helper property.
// V8-27468 : L.Ineson
// Added IsMerchandisedTopDown
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
// V8-28466 : A.Silva
//  Amended IsCarPark to have an internal setter instead of public.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
#endregion
#region Version History: CCM810
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
// V8-30873 : L.Ineson
//  Removed IsCarPark.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramComponent object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramComponent : ModelObject<PlanogramComponent>, ICustomAttributeDataParent
    {
        #region Constants

        public const Int32 MaximumComponentNameLength = 50;           

        #endregion

        #region Constructors
        static PlanogramComponent()
        {
            FriendlyName = Message.PlanogramComponent_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get
            {
                PlanogramComponentList parentList = base.Parent as PlanogramComponentList;
                if (parentList == null) return null;
                return parentList.Parent;
            }
        }
        #endregion

        #region Properties

        #region Mesh3DId
        /// <summary>
        /// Mesh3DId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> Mesh3DIdProperty =
            RegisterModelProperty<Object>(c => c.Mesh3DId);
        /// <summary>
        /// The PlanogramComponent Mesh3DId
        /// </summary>
        public Object Mesh3DId
        {
            get { return this.GetProperty<Object>(Mesh3DIdProperty); }
            set { this.SetProperty<Object>(Mesh3DIdProperty, value); }
        }
        #endregion

        #region ImageIdFront
        /// <summary>
        /// ImageIdFront property
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdFrontProperty =
            RegisterModelProperty<Object>(c => c.ImageIdFront);
        /// <summary>
        /// The front image id
        /// </summary>
        public Object ImageIdFront
        {
            get { return this.GetProperty<Object>(ImageIdFrontProperty); }
            set { this.SetProperty<Object>(ImageIdFrontProperty, value); }
        }
        #endregion

        #region ImageIdBack
        /// <summary>
        /// ImageIdBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdBackProperty =
            RegisterModelProperty<Object>(c => c.ImageIdBack);
        /// <summary>
        /// The back image id
        /// </summary>
        public Object ImageIdBack
        {
            get { return this.GetProperty<Object>(ImageIdBackProperty); }
            set { this.SetProperty<Object>(ImageIdBackProperty, value); }
        }
        #endregion

        #region ImageIdTop
        /// <summary>
        /// ImageIdTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdTopProperty =
            RegisterModelProperty<Object>(c => c.ImageIdTop);
        /// <summary>
        /// The top image id
        /// </summary>
        public Object ImageIdTop
        {
            get { return this.GetProperty<Object>(ImageIdTopProperty); }
            set { this.SetProperty<Object>(ImageIdTopProperty, value); }
        }
        #endregion

        #region ImageIdBottom
        /// <summary>
        /// ImageIdBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdBottomProperty =
            RegisterModelProperty<Object>(c => c.ImageIdBottom);
        public Object ImageIdBottom
        {
            get { return this.GetProperty<Object>(ImageIdBottomProperty); }
            set { this.SetProperty<Object>(ImageIdBottomProperty, value); }
        }
        #endregion

        #region ImageIdLeft
        /// <summary>
        /// ImageIdLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdLeftProperty =
            RegisterModelProperty<Object>(c => c.ImageIdLeft);
        /// <summary>
        /// The left image id
        /// </summary>
        public Object ImageIdLeft
        {
            get { return this.GetProperty<Object>(ImageIdLeftProperty); }
            set { this.SetProperty<Object>(ImageIdLeftProperty, value); }
        }
        #endregion

        #region ImageIdRight
        /// <summary>
        /// ImageRightId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ImageIdRightProperty =
            RegisterModelProperty<Object>(c => c.ImageIdRight);
        public Object ImageIdRight
        {
            get { return this.GetProperty<Object>(ImageIdRightProperty); }
            set { this.SetProperty<Object>(ImageIdRightProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The PlanogramComponent Name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.PlanogramComponent_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramComponent Height
        /// </summary>
        public Single Height
        {
            get { return this.GetProperty<Single>(HeightProperty); }
            set { this.SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramComponent_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramComponent Width
        /// </summary>
        public Single Width
        {
            get { return this.GetProperty<Single>(WidthProperty); }
            set { this.SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramComponent_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramComponent Depth
        /// </summary>
        public Single Depth
        {
            get { return this.GetProperty<Single>(DepthProperty); }
            set { this.SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region IsMoveable
        /// <summary>
        /// IsMoveable property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMoveableProperty =
            RegisterModelProperty<Boolean>(c => c.IsMoveable, Message.PlanogramComponent_IsMoveable);
        /// <summary>
        /// The PlanogramComponent IsMoveable
        /// </summary>
        public Boolean IsMoveable
        {
            get { return this.GetProperty<Boolean>(IsMoveableProperty); }
            set { this.SetProperty<Boolean>(IsMoveableProperty, value); }
        }
        #endregion

        #region IsDisplayOnly
        /// <summary>
        /// IsDisplayOnly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDisplayOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsDisplayOnly, Message.PlanogramComponent_IsDisplayOnly);
        /// <summary>
        /// The PlanogramComponent IsDisplayOnly
        /// </summary>
        public Boolean IsDisplayOnly
        {
            get { return this.GetProperty<Boolean>(IsDisplayOnlyProperty); }
            set { this.SetProperty<Boolean>(IsDisplayOnlyProperty, value); }
        }
        #endregion

        #region CanAttachShelfEdgeLabel
        /// <summary>
        /// CanAttachShelfEdgeLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanAttachShelfEdgeLabelProperty =
            RegisterModelProperty<Boolean>(c => c.CanAttachShelfEdgeLabel, Message.PlanogramComponent_CanAttachShelfEdgeLabel);
        /// <summary>
        /// The PlanogramComponent CanAttachShelfEdgeLabel
        /// </summary>
        public Boolean CanAttachShelfEdgeLabel
        {
            get { return this.GetProperty<Boolean>(CanAttachShelfEdgeLabelProperty); }
            set { this.SetProperty<Boolean>(CanAttachShelfEdgeLabelProperty, value); }
        }
        #endregion

        #region RetailerReferenceCode
        /// <summary>
        /// RetailerReferenceCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RetailerReferenceCodeProperty =
            RegisterModelProperty<String>(c => c.RetailerReferenceCode, Message.PlanogramComponent_RetailerReferenceCode);
        /// <summary>
        /// The PlanogramComponent RetailerReferenceCode
        /// </summary>
        public String RetailerReferenceCode
        {
            get { return this.GetProperty<String>(RetailerReferenceCodeProperty); }
            set { this.SetProperty<String>(RetailerReferenceCodeProperty, value); }
        }
        #endregion

        #region BarCode
        /// <summary>
        /// BarCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BarCodeProperty =
            RegisterModelProperty<String>(c => c.BarCode, Message.PlanogramComponent_Barcode);
        /// <summary>
        /// The PlanogramComponent BarCode
        /// </summary>
        public String BarCode
        {
            get { return this.GetProperty<String>(BarCodeProperty); }
            set { this.SetProperty<String>(BarCodeProperty, value); }
        }
        #endregion

        #region Manufacturer
        /// <summary>
        /// Manufacturer property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
            RegisterModelProperty<String>(c => c.Manufacturer, Message.PlanogramComponent_Manufacturer);
        /// <summary>
        /// The PlanogramComponent Manufacturer
        /// </summary>
        public String Manufacturer
        {
            get { return this.GetProperty<String>(ManufacturerProperty); }
            set { this.SetProperty<String>(ManufacturerProperty, value); }
        }
        #endregion

        #region ManufacturerPartName
        /// <summary>
        /// ManufacturerPartName propert definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerPartNameProperty =
            RegisterModelProperty<String>(c => c.ManufacturerPartName, Message.PlanogramComponent_ManufacturerPartName);
        /// <summary>
        /// The PlanogramComponent ManufacturerPartName
        /// </summary>
        public String ManufacturerPartName
        {
            get { return this.GetProperty<String>(ManufacturerPartNameProperty); }
            set { this.SetProperty<String>(ManufacturerPartNameProperty, value); }
        }
        #endregion

        #region ManufacturerPartNumber
        /// <summary>
        /// ManufacturerPartNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerPartNumberProperty =
            RegisterModelProperty<String>(c => c.ManufacturerPartNumber, Message.PlanogramComponent_ManufacturerPartNumber);
        /// <summary>
        /// The PlanogramComponent ManufacturerPartNumber
        /// </summary>
        public String ManufacturerPartNumber
        {
            get { return this.GetProperty<String>(ManufacturerPartNumberProperty); }
            set { this.SetProperty<String>(ManufacturerPartNumberProperty, value); }
        }
        #endregion

        #region SupplierName
        /// <summary>
        /// SupplierName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SupplierNameProperty =
            RegisterModelProperty<String>(c => c.SupplierName, Message.PlanogramComponent_SupplierName);
        /// <summary>
        /// The PlanogramComponent SupplierName
        /// </summary>
        public String SupplierName
        {
            get { return this.GetProperty<String>(SupplierNameProperty); }
            set { this.SetProperty<String>(SupplierNameProperty, value); }
        }
        #endregion

        #region SupplierPartNumber
        /// <summary>
        /// SupplierPartNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SupplierPartNumberProperty =
            RegisterModelProperty<String>(c => c.SupplierPartNumber, Message.PlanogramComponent_SupplierPartNumber);
        /// <summary>
        /// Gets or sets the supplier part number
        /// </summary>
        public String SupplierPartNumber
        {
            get { return this.GetProperty<String>(SupplierPartNumberProperty); }
            set { this.SetProperty<String>(SupplierPartNumberProperty, value); }
        }
        #endregion

        #region SupplierCostPrice
        /// <summary>
        /// SupplierCostPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierCostPriceProperty =
            RegisterModelProperty<Single?>(c => c.SupplierCostPrice, Message.PlanogramComponent_SupplierCostPrice, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Gets or sets the supplier cost price
        /// </summary>
        public Single? SupplierCostPrice
        {
            get { return GetProperty<Single?>(SupplierCostPriceProperty); }
            set { SetProperty<Single?>(SupplierCostPriceProperty, value); }
        }
        #endregion

        #region SupplierDiscount
        /// <summary>
        /// SupplierDiscount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierDiscountProperty =
            RegisterModelProperty<Single?>(c => c.SupplierDiscount, Message.PlanogramComponent_SupplierDiscount, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Gets or sets the supplier discount
        /// </summary>
        public Single? SupplierDiscount
        {
            get { return GetProperty<Single?>(SupplierDiscountProperty); }
            set { SetProperty<Single?>(SupplierDiscountProperty, value); }
        }
        #endregion

        #region SupplierLeadTime
        /// <summary>
        /// SupplierLeadTime property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierLeadTimeProperty =
            RegisterModelProperty<Single?>(c => c.SupplierLeadTime, Message.PlanogramComponent_SupplierLeadTime);
        /// <summary>
        /// The supplier lead time
        /// </summary>
        public Single? SupplierLeadTime
        {
            get { return this.GetProperty<Single?>(SupplierLeadTimeProperty); }
            set { this.SetProperty<Single?>(SupplierLeadTimeProperty, value); }
        }
        #endregion

        #region MinPurchaseQty
        /// <summary>
        /// MinPurchaseQty property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPurchaseQtyProperty =
            RegisterModelProperty<Int32?>(c => c.MinPurchaseQty, Message.PlanogramComponent_MinPurchaseQty);
        /// <summary>
        /// The minimum purchase quantity
        /// </summary>
        public Int32? MinPurchaseQty
        {
            get { return this.GetProperty<Int32?>(MinPurchaseQtyProperty); }
            set { this.SetProperty<Int32?>(MinPurchaseQtyProperty, value); }
        }
        #endregion

        #region WeightLimit
        /// <summary>
        /// WeightLimit property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> WeightLimitProperty =
            RegisterModelProperty<Single?>(c => c.WeightLimit, Message.PlanogramComponent_WeightLimit, ModelPropertyDisplayType.Weight);
        /// <summary>
        /// Gets or sets the component weight limit
        /// </summary>
        public Single? WeightLimit
        {
            get { return this.GetProperty<Single?>(WeightLimitProperty); }
            set { this.SetProperty<Single?>(WeightLimitProperty, value); }
        }
        #endregion

        #region Weight
        /// <summary>
        /// Weight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> WeightProperty =
            RegisterModelProperty<Single?>(c => c.Weight, Message.PlanogramComponent_Weight, ModelPropertyDisplayType.Weight);
        /// <summary>
        /// Gets or sets the component weight
        /// </summary>
        public Single? Weight
        {
            get { return this.GetProperty<Single?>(WeightProperty); }
            set { this.SetProperty<Single?>(WeightProperty, value); }
        }
        #endregion

        #region Volume
        /// <summary>
        /// Volume property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> VolumeProperty =
            RegisterModelProperty<Single?>(c => c.Volume, Message.PlanogramComponent_Volume, ModelPropertyDisplayType.Volume);
        /// <summary>
        /// Gets or sets the component volume
        /// </summary>
        public Single? Volume
        {
            get { return GetProperty<Single?>(VolumeProperty); }
            set { SetProperty<Single?>(VolumeProperty, value); }
        }
        #endregion

        #region Diameter
        /// <summary>
        /// Diameter property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> DiameterProperty =
            RegisterModelProperty<Single?>(c => c.Diameter, Message.PlanogramComponent_Diameter, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the component diameter
        /// </summary>
        public Single? Diameter
        {
            get { return this.GetProperty<Single?>(DiameterProperty); }
            set { this.SetProperty<Single?>(DiameterProperty, value); }
        }
        #endregion

        #region Capacity
        /// <summary>
        /// Capacity property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> CapacityProperty =
            RegisterModelProperty<Int16?>(c => c.Capacity, Message.PlanogramComponent_Capacity);
        /// <summary>
        /// Gets or sets the component capacity
        /// </summary>
        public Int16? Capacity
        {
            get { return GetProperty<Int16?>(CapacityProperty); }
            set { SetProperty<Int16?>(CapacityProperty, value); }
        }
        #endregion

        #region ComponentType
        /// <summary>
        /// ComponentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComponentType> ComponentTypeProperty =
            RegisterModelProperty<PlanogramComponentType>(c => c.ComponentType, Message.PlanogramComponent_ComponentType);
        /// <summary>
        /// Gets or sets the component type
        /// </summary>
        public PlanogramComponentType ComponentType
        {
            get { return GetProperty<PlanogramComponentType>(ComponentTypeProperty); }
            set { SetProperty<PlanogramComponentType>(ComponentTypeProperty, value); }
        }
        #endregion

        #region IsMerchandisedTopDown
        /// <summary>
        /// IsMerchandisedTopDown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMerchandisedTopDownProperty =
            RegisterModelProperty<Boolean>(c => c.IsMerchandisedTopDown, Message.PlanogramComponent_IsMerchandisedTopDown);
        /// <summary>
        /// Indicates if this component is the car park shelf
        /// </summary>
        public Boolean IsMerchandisedTopDown
        {
            get { return GetProperty<Boolean>(IsMerchandisedTopDownProperty); }
            set { SetProperty<Boolean>(IsMerchandisedTopDownProperty, value); }
        }
        #endregion

        #region SubComponents
        /// <summary>
        /// SubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramSubComponentList> SubComponentsProperty =
            RegisterModelProperty<PlanogramSubComponentList>(c => c.SubComponents, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The sub components
        /// </summary>
        public PlanogramSubComponentList SubComponents
        {
            get
            {
                return this.GetPropertyLazy<PlanogramSubComponentList>(
                    SubComponentsProperty,
                    new PlanogramSubComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The sub components
        /// </summary>
        public PlanogramSubComponentList SubComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramSubComponentList>(
                    SubComponentsProperty,
                    new PlanogramSubComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }
        #endregion

        #region MetaNumberOfSubComponents
        /// <summary>
        /// NumberOfSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfSubComponentsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfSubComponents, Message.PlanogramComponent_MetaNumberOfSubComponents);
        /// <summary>
        /// The PlanogramComponent NumberOfSubComponents
        /// </summary>
        public Int16? MetaNumberOfSubComponents
        {
            get { return GetProperty<Int16?>(MetaNumberOfSubComponentsProperty); }
            private set { SetProperty<Int16?>(MetaNumberOfSubComponentsProperty, value); }
        }
        #endregion

        #region MetaNumberOfMerchandisedSubComponents
        /// <summary>
        /// NumberOfMerchandisedSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMerchandisedSubComponentsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMerchandisedSubComponents, Message.PlanogramComponent_MetaNumberOfMerchandisedSubComponents);
        /// <summary>
        /// The PlanogramComponent NumberOfMerchandisedSubComponents
        /// </summary>
        public Int16? MetaNumberOfMerchandisedSubComponents
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty); }
            private set { this.SetProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty, value); }
        }
        #endregion

        #region Custom Attributes
        /// <summary>
        /// CustomAttributes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomAttributeData> CustomAttributesProperty =
            RegisterModelProperty<CustomAttributeData>(c => c.CustomAttributes, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child custom attributes object for this plan.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get
            {
                return this.GetPropertyLazy<CustomAttributeData>(
                    CustomAttributesProperty,
                    new CustomAttributeData.FetchCriteria(this.DalFactoryName,
                        (Byte)CustomAttributeDataPlanogramParentType.PlanogramComponent, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child CustomAttributeData of this planogram
        /// </summary>
        public CustomAttributeData CustomAttributesAsync
        {
            get
            {
                return this.GetPropertyLazy<CustomAttributeData>(
                    CustomAttributesProperty,
                    new CustomAttributeData.FetchCriteria(this.DalFactoryName,
                        (Byte)CustomAttributeDataPlanogramParentType.PlanogramComponent, this.Id),
                    false);
            }
        }


        #endregion

        #region Helper Properties

        /// <summary>
        /// Determines if the component is merchandisable. I.e. it is not a backboard, base
        /// or car park shelf.
        /// </summary>
        public Boolean IsMerchandisable
        {
            get
            {
                return SubComponents.Any(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            }
        }

        #endregion

        #region ParentType
        /// <summary>
        /// Returns the custom attribute data parent type
        /// </summary>
        Byte ICustomAttributeDataParent.ParentType
        {
            get { return (Byte)CustomAttributeDataPlanogramParentType.PlanogramComponent; }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(RetailerReferenceCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(BarCodeProperty, 13));
            BusinessRules.AddRule(new MaxLength(ManufacturerProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerPartNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerPartNumberProperty, 50));
            BusinessRules.AddRule(new MaxLength(SupplierNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(SupplierPartNumberProperty, 50));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramComponent NewPlanogramComponent()
        {
            PlanogramComponent item = new PlanogramComponent();
            item.Create(
                Message.PlanogramComponent_Name_Default,
                PlanogramComponentType.Shelf, 0, 0, 0, false);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramComponent NewPlanogramComponent(String componentName, PlanogramComponentType componentType,
            Single width, Single height, Single depth, Int32 fillColour = -16777216)
        {
            PlanogramComponent item = new PlanogramComponent();
            item.Create(componentName, componentType, width, height, depth, true, fillColour);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramComponent NewPlanogramComponent(String componentName, PlanogramComponentType componentType,
            IPlanogramSettings settings)
        {
            PlanogramComponent item = new PlanogramComponent();
            item.Create(componentName, componentType, settings, true);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String componentName, PlanogramComponentType componentType,
            Single width, Single height, Single depth, Boolean createSubComponent, Int32 fillColour = -16777216)
        {
            // initialize the component
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, componentName);
            this.LoadProperty<PlanogramComponentType>(ComponentTypeProperty, componentType);
            this.LoadProperty<Single>(WidthProperty, width);
            this.LoadProperty<Single>(HeightProperty, height);
            this.LoadProperty<Single>(DepthProperty, depth);
            this.LoadProperty<PlanogramSubComponentList>(SubComponentsProperty, PlanogramSubComponentList.NewPlanogramSubComponentList());
            this.LoadProperty<Boolean>(IsMoveableProperty, componentType == PlanogramComponentType.Pallet);
            this.LoadProperty<Boolean>(IsMerchandisedTopDownProperty, componentType == PlanogramComponentType.Chest);
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.MarkAsChild();
            base.Create();

            // create a sub component if required
            if (createSubComponent)
            {
                this.SubComponents.Add(PlanogramSubComponent.NewPlanogramSubComponent(
                    String.Format(CultureInfo.CurrentCulture, Message.PlanogramSubComponent_Name_New, 1),
                    componentType,
                    width,
                    height,
                    depth,
                    fillColour));
            }
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String componentName, PlanogramComponentType componentType, IPlanogramSettings settings, Boolean createSubComponent)
        {
            switch (componentType)
            {
                case PlanogramComponentType.Backboard:
                    this.Create(componentName, componentType, settings.FixtureWidth, settings.FixtureHeight, settings.BackboardDepth, createSubComponent, -2894893); // light gray
                    break;
                case PlanogramComponentType.Bar:
                    this.Create(componentName, componentType, settings.BarWidth, settings.BarHeight, settings.BarDepth, createSubComponent, settings.BarFillColour);
                    break;
                case PlanogramComponentType.Base:
                    this.Create(componentName, componentType, settings.FixtureWidth, settings.BaseHeight, settings.ShelfDepth, createSubComponent, -5658199); // dark gray
                    break;
                case PlanogramComponentType.Chest:
                    this.Create(componentName, componentType, settings.ChestWidth, settings.ChestHeight, settings.ChestDepth, createSubComponent, settings.ChestFillColour);
                    break;
                case PlanogramComponentType.ClipStrip:
                    this.Create(componentName, componentType, settings.ClipStripWidth, settings.ClipStripHeight, settings.ClipStripDepth, createSubComponent, settings.ClipStripFillColour);
                    break;
                case PlanogramComponentType.Shelf:
                    this.Create(componentName, componentType, settings.ShelfWidth, settings.ShelfHeight, settings.ShelfDepth, createSubComponent, settings.ShelfFillColour);
                    break;
                case PlanogramComponentType.Peg:
                    this.Create(componentName, componentType, settings.PegboardWidth, settings.PegboardHeight, settings.PegboardDepth, createSubComponent, settings.PegboardFillColour);
                    break;
                case PlanogramComponentType.Rod:
                    this.Create(componentName, componentType, settings.RodWidth, settings.RodHeight, settings.RodDepth, createSubComponent, settings.RodFillColour);
                    break;
                case PlanogramComponentType.Pallet:
                    this.Create(componentName, componentType, settings.PalletWidth, settings.PalletHeight, settings.PalletDepth, createSubComponent, settings.PalletFillColour);
                    break;
                case PlanogramComponentType.Panel:
                    this.Create(componentName, componentType, 10, 10, 10, createSubComponent, -16777216); // black
                    break;
                case PlanogramComponentType.SlotWall:
                    this.Create(componentName, componentType, settings.SlotwallWidth, settings.SlotwallHeight, settings.SlotwallDepth, createSubComponent, settings.SlotwallFillColour);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ImageIdFront
            Object imageIdFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdFrontProperty));
            if (imageIdFront != null) this.LoadProperty<Object>(ImageIdFrontProperty, imageIdFront);

            // ImageIdBack
            Object imageIdBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdBackProperty));
            if (imageIdBack != null) this.LoadProperty<Object>(ImageIdBackProperty, imageIdBack);

            // ImageIdTop
            Object imageIdTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdTopProperty));
            if (imageIdTop != null) this.LoadProperty<Object>(ImageIdTopProperty, imageIdTop);

            // ImageIdBottom
            Object imageIdBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdBottomProperty));
            if (imageIdBottom != null) this.LoadProperty<Object>(ImageIdBottomProperty, imageIdBottom);

            // ImageIdLeft
            Object imageIdLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdLeftProperty));
            if (imageIdLeft != null) this.LoadProperty<Object>(ImageIdLeftProperty, imageIdLeft);

            // ImageIdRight
            Object imageIdRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdRightProperty));
            if (imageIdRight != null) this.LoadProperty<Object>(ImageIdRightProperty, imageIdRight);
        }

        /// <summary>
        /// Returns the expected component type based on the 
        /// actual subcomponent values.
        /// </summary>
        public PlanogramComponentType GetPlanogramComponentType()
        {
            //if it has multiple subcomponents just mark as custom
            if (this.SubComponents.Count != 1)
            {
                return PlanogramComponentType.Custom;
            }

            //check the part itself.
            PlanogramSubComponent sub = this.SubComponents[0];

            switch (sub.MerchandisingType)
            {
                #region Non-Merchandisable types
                case PlanogramSubComponentMerchandisingType.None:
                    {
                        if (this.ComponentType == PlanogramComponentType.Backboard)
                        {
                            return PlanogramComponentType.Backboard;
                        }
                        else if (this.ComponentType == PlanogramComponentType.Base)
                        {
                            return PlanogramComponentType.Base;
                        }
                        else
                        {
                            //sub is not merchandisable so mark as a panel.
                            return PlanogramComponentType.Panel;
                        }
                    }
                #endregion

                #region Stacked Types
                case PlanogramSubComponentMerchandisingType.Stack:
                    {
                        //Chest: sub does not have the top facing.
                        if (sub.FaceThicknessTop == 0
                            && sub.FaceThicknessBottom > 0
                            && sub.FaceThicknessFront > 0
                            && sub.FaceThicknessBack > 0
                            && sub.FaceThicknessLeft > 0
                            && sub.FaceThicknessRight > 0)
                        {
                            return PlanogramComponentType.Chest;
                        }

                        //pallet: sub was created as a pallet - not sure of any specific differences..
                        else if (this.ComponentType == PlanogramComponentType.Pallet)
                        {

                            return this.ComponentType;
                        }

                        //otherwise it is a shelf.
                        else
                        {
                            return PlanogramComponentType.Shelf;
                        }
                    }
                #endregion

                #region Hanging Types
                case PlanogramSubComponentMerchandisingType.Hang:
                    {

                        //clipstrip: sub was created as a clipstrip - not sure of any specific differences..
                        //SlotWall: CHECK THIS
                        if (this.ComponentType == PlanogramComponentType.ClipStrip || this.ComponentType == PlanogramComponentType.SlotWall)
                        {
                            return this.ComponentType;
                        }
                        else
                        {
                            return PlanogramComponentType.Peg;
                        }
                    }
                #endregion

                #region HangFromBottom Types
                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    {
                        if (sub.Depth > sub.Width)
                        {
                            //sub is deeper than wide - its a rod.
                            return PlanogramComponentType.Rod;
                        }
                        else
                        {
                            //sub is wider then deep - its a bar.
                            return PlanogramComponentType.Bar;
                        }
                    }
                #endregion

                default:
                    Debug.Fail("Type not handled");
                    break;
            }

            //return anything else as custom.
            return PlanogramComponentType.Custom;
        }

        /// <summary>
        /// Returns all sub component placements
        /// that are associated with this component
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetSubComponentPlacements()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.Component))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        /// <summary>
        /// Updates the Height, Width and Depth values of this component
        /// based on its child subcomponents.
        /// </summary>
        public void UpdateSizeFromSubComponents()
        {
            //only update the value if it has child subcomponents.
            if (this.SubComponents.Count == 0) return;

            RectValue bounds = new RectValue();

            //union the bounds of the child subcomponents.
            foreach (PlanogramSubComponent subComponent in this.SubComponents)
            {
                RectValue childBounds = new RectValue(0, 0, 0,
                    subComponent.Width, subComponent.Height, subComponent.Depth);

                MatrixValue childTransform =
                    MatrixValue.CreateTransformationMatrix(
                    subComponent.X, subComponent.Y, subComponent.Z,
                    subComponent.Angle, subComponent.Slope, subComponent.Roll);

                childBounds = childTransform.TransformBounds(childBounds);

                bounds = bounds.Union(childBounds);
            }

            if (this.Width != bounds.Width) this.Width = bounds.Width;
            if (this.Height != bounds.Height) this.Height = bounds.Height;
            if (this.Depth != bounds.Depth) this.Depth = bounds.Depth;

        }

        /// <summary>
        /// Enumerates through all associated image ids.
        /// </summary>
        public IEnumerable<Object> EnumerateImageIds()
        {
            if (ImageIdFront != null) yield return ImageIdFront;
            if (ImageIdLeft != null) yield return ImageIdLeft;
            if (ImageIdTop != null) yield return ImageIdTop;
            if (ImageIdBack != null) yield return ImageIdBack;
            if (ImageIdRight != null) yield return ImageIdRight;
            if (ImageIdBottom != null) yield return ImageIdBottom;
        }

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <param name="includePlacementProperties">If true then properties from fixture component will be included</param>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(PlanogramComponent);
            String typeFriendly = PlanogramComponent.FriendlyName;

            String detailsGroup = Message.PlanogramComponent_PropertyGroup_Details;
            String customGroup = Message.PlanogramComponent_PropertyGroup_Custom;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BarCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanAttachShelfEdgeLabelProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CapacityProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ComponentTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DiameterProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsDisplayOnlyProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsMerchandisedTopDownProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsMoveableProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerPartNameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerPartNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinPurchaseQtyProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RetailerReferenceCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SupplierCostPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SupplierDiscountProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SupplierLeadTimeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SupplierNameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SupplierPartNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VolumeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WeightLimitProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);

            //return properties from custom attribute data.
            foreach (var property in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, property, customGroup);
                fieldInfo.PropertyName = CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            //Metadata fields
            if (includeMetadata)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfMerchandisedSubComponentsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfSubComponentsProperty, detailsGroup);
            }

        }

        /// <summary>
        /// Returns the value of the given field for this product.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            Debug.Assert(field.OwnerType == typeof(PlanogramComponent), "Field is not for this object type");

            Object value = null;

            try
            {
                if (field.PropertyName.StartsWith(CustomAttributesProperty.Name + "."))
                {
                    //Custom attribute data field.
                    value = WpfHelper.GetItemPropertyValue(this.CustomAttributes, field.PropertyName.Split('.')[1]);
                }
                else
                {
                    value = field.GetValue(this);
                }
            }
            catch { }

            return value;
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Override of base calculate metadata method.
        /// </summary>
        protected override void CalculateMetadata()
        {
            //enumerate though children:
            // none to do as subs don't have meta properties.
            // and it wastes time cycling through them.
            base.CalculateMetadata(
                new Object[] { });
        }

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            this.MetaNumberOfMerchandisedSubComponents = (Int16)this.SubComponents.Count(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            this.MetaNumberOfSubComponents = (Int16)this.SubComponents.Count;
        }

        /// <summary>
        /// Override of base ClearMetadata method.
        /// </summary>
        protected override void ClearMetadata()
        {
            //enumerate though children:
            // none to do as subs don't have meta properties.
            // and it wastes time cycling through them.
            base.ClearMetadata(
                new Object[] { });
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            //Set meta data properties
            this.MetaNumberOfMerchandisedSubComponents = null;
            this.MetaNumberOfSubComponents = null;
        }
        #endregion

        #endregion
    }
}