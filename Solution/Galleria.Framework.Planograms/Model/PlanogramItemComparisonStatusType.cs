﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31947 : A.Silva
//  Created.
// V8-32107 : A.Silva
//  Amended PlanogramItemComparisonStatusType members and added FriendlyDescriptions to the Enum Helper.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     The different values for the Comparison Status that a <c>planogram item</c> may have in the master planogram.
    /// </summary>
    public enum PlanogramItemComparisonStatusType
    {
        /// <summary>
        ///     The Planogram Item has the same values in other Compared Planograms.
        /// </summary>
        Unchanged = 0,
        /// <summary>
        ///     The Planogram Item is not found in at least one other Compared Planogram.
        /// </summary>
        New = 1,
        /// <summary>
        ///     The Planogram Item was not found in the Master Planogram, but exists in at least another Compared Planogram
        /// </summary>
        NotFound = 2,
        /// <summary>
        ///     The Planogram Item has different values in at least one other Compare Planogram.
        /// </summary>
        Changed = 3,
    }

    /// <summary>
    ///     PlanogramItemComparisonStatusType Helper Class
    /// </summary>
    public static class PlanogramItemComparisonStatusTypeHelper
    {
        /// <summary>
        ///     Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramItemComparisonStatusType, String> FriendlyNames =
            new Dictionary<PlanogramItemComparisonStatusType, String>
            {
                {PlanogramItemComparisonStatusType.Unchanged, Message.Enum_PlanogramItemComparisonStatusType_Unchanged},
                {PlanogramItemComparisonStatusType.New, Message.Enum_PlanogramItemComparisonStatusType_New},
                {PlanogramItemComparisonStatusType.NotFound, Message.Enum_PlanogramItemComparisonStatusType_NotFound},
                {PlanogramItemComparisonStatusType.Changed, Message.Enum_PlanogramItemComparisonStatusType_Changed},
            };

        /// <summary>
        ///     Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramItemComparisonStatusType, String> FriendlyDescriptions = 
            new Dictionary<PlanogramItemComparisonStatusType, String>
            {
                {PlanogramItemComparisonStatusType.Unchanged, Message.Enum_PlanogramItemComparisonStatusType_Unchanged_Description},
                {PlanogramItemComparisonStatusType.New, Message.Enum_PlanogramItemComparisonStatusType_New_Description},
                {PlanogramItemComparisonStatusType.NotFound, Message.Enum_PlanogramItemComparisonStatusType_NotFound_Description},
                {PlanogramItemComparisonStatusType.Changed, Message.Enum_PlanogramItemComparisonStatusType_Changed_Description},
            };
    }
}