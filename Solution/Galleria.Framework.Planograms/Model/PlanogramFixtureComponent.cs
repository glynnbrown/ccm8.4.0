﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26569 : L.Ineson
//  Added on methods to get world rotation and position
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-27058 : A.Probyn
//  Added new meta data properties calculations
//  Added ClearMetadata
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber property.
// V8-27510 : A.Kuszyk
//  Changed references to positions to use DesignViewHelper.
// V8-27477 : L.Ineson
//  Added more world space helper methods
// V8-27570 : A.Silva
//      Amended to include ComponentSequenceNumber to DisplayablePropertyInfos.
// V8-27589 : A.Silva
//      Refactored calculations needed for Metadata AND Validation Warnings.
// V8-27742 : A.Silva
//      Added IsOverfilled validation warning.
// V8-25976 : L.Ineson
//  Added GetBrokenValidationWarnings method.
// V8-28078 : A.Probyn
//      Added defensive code to GetPlanogramRelativeCoordinates
// V8-27442 : L.Ineson
//  Added notch methods.

#endregion

#region Version History: CCM802

// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29044 : M.Shelley
//  Added NotchNumber property to allow saving to the database
// V8-29369 : L.Luong
//  Modified Meta data calculation so they are correct
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-29341 : J.Pickup
//  Component Sequence number is now public - for custom column path.
// V8-30210 : M.Brumby
//  Case pack metadata was being converted to Int16 erroneously

#endregion

#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29777 : L.Ineson
//  Corrected white space properties display types
// V8-30019 : L.Ineson
//  Corrected meta IsOverMerchandisedHeight, Width and Depth values
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-28382 : L.Ineson
//  Bypassed collision warning between sloped components for now.

#endregion

#region Version History: CCM811

// V8-30397 : N.Haywood
//  Commented out unused meta data properties that aren't being calculated
// V8-30352 : A.Probyn
//  Updated OnCalculateMetadata so that the white space is calculated at a cumulative merchandising space level, 
//  not as a total for the planogram. Components which are overfaced were masking white space on other shelves.
// V8-30398 : A.Silva
//  Amended MetaTotalFacings to use PlanogramHelper.CalculateMetaTotalFacings (that accounts for tray real facings).
#endregion

#region Version History: CCM830
//V8-32474 : L.Ineson
//  Corrected linear white space calculation to consider that merch axis may not be width 
//  and positions can overlap linear space.
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
// V8-32588 : J.Pickup
//  Added Made MetaIsOutsideOfFixtureArea now takes into account the component as a world relative point, rather than just relative to the fixture.
// V8-32522 : A.Silva
//  Exposed IsEntirelyOutsideParentFixture as part of the IPlanogramFixtureComponent interface.
// V8-32890 : M.Brumby
//  Components under a bay are flagged as outside merch area.
// CCM-13761 : L.Ineson
//  Added GetBoundaryInfo for improved collision detection.
// CCM-13565 : L.Ineson
//  EnumerateComponent collisions now checks against subcomponent bounds.
// CCM-18492 : D.Pleasance
//  Added IsMerchandisibleComponent()
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramFixtureComponent Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramFixtureComponent : ModelObject<PlanogramFixtureComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramFixture Parent
        {
            get { return ((PlanogramFixtureComponentList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region PlanogramComponentId
        /// <summary>
        /// PlanogramComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramComponentId);
        /// <summary>
        /// The PlanogramFixtureComponent PlanogramComponentId
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object PlanogramComponentId
        {
            get { return GetProperty<Object>(PlanogramComponentIdProperty); }
            set { SetProperty<Object>(PlanogramComponentIdProperty, value); }
        }
        #endregion

        #region ComponentSequenceNumber

        /// <summary>
        ///		Metadata for the <see cref="ComponentSequenceNumber"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> ComponentSequenceNumberProperty =
            RegisterModelProperty<Int16?>(o => o.ComponentSequenceNumber, Message.PlanogramFixtureComponent_ComponentSequenceNumber);

        /// <summary>
        ///		Gets or sets the sequence number (order) for this instance.
        /// </summary>
        public Int16? ComponentSequenceNumber
        {
            get { return this.GetProperty<Int16?>(ComponentSequenceNumberProperty); }
            set { this.SetProperty<Int16?>(ComponentSequenceNumberProperty, value); }
        }

        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.Generic_X, ModelPropertyDisplayType.Length);
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.Generic_Y, ModelPropertyDisplayType.Length);
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.Generic_Z, ModelPropertyDisplayType.Length);
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, Message.Generic_Slope, ModelPropertyDisplayType.Angle);
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
       RegisterModelProperty<Single>(c => c.Angle, Message.Generic_Angle, ModelPropertyDisplayType.Angle);
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, Message.Generic_Roll, ModelPropertyDisplayType.Angle);
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace, Message.PlanogramFixtureComponent_MetaTotalMerchandisableLinearSpace);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace, Message.PlanogramFixtureComponent_MetaTotalMerchandisableAreaSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace, Message.PlanogramFixtureComponent_MetaTotalMerchandisableVolumetricSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace,
            Message.PlanogramFixtureComponent_MetaTotalLinearWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace,
            Message.PlanogramFixtureComponent_MetaTotalAreaWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace,
            Message.PlanogramFixtureComponent_MetaTotalVolumetricWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced, Message.PlanogramFixtureComponent_MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsPlacedProperty, value); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts, Message.PlanogramFixtureComponent_MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
            set { this.SetProperty<Int32?>(MetaNewProductsProperty, value); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount, Message.PlanogramFixtureComponent_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
            set { this.SetProperty<Int32?>(MetaChangesFromPreviousCountProperty, value); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating, Message.PlanogramFixtureComponent_MetaChangeFromPreviousStarRating);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
            set { this.SetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, value); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped, Message.PlanogramFixtureComponent_MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
            set { this.SetProperty<Int32?>(MetaBlocksDroppedProperty, value); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory, Message.PlanogramFixtureComponent_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Int32?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, Message.PlanogramFixtureComponent_MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFacings
        /// <summary>
        /// MetaAverageFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaAverageFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaAverageFacings, Message.PlanogramFixtureComponent_MetaAverageFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFacings
        /// </summary>
        public Int16? MetaAverageFacings
        {
            get { return this.GetProperty<Int16?>(MetaAverageFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaAverageFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, Message.PlanogramFixtureComponent_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }
        #endregion

        #region MetaAverageUnits
        /// <summary>
        /// MetaAverageUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaAverageUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaAverageUnits, Message.PlanogramFixtureComponent_MetaAverageUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageUnits
        /// </summary>
        public Int32? MetaAverageUnits
        {
            get { return this.GetProperty<Int32?>(MetaAverageUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaAverageUnitsProperty, value); }
        }
        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos, Message.PlanogramFixtureComponent_MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
            set { this.SetProperty<Single?>(MetaMinDosProperty, value); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos, Message.PlanogramFixtureComponent_MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
            set { this.SetProperty<Single?>(MetaMaxDosProperty, value); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos, Message.PlanogramFixtureComponent_MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
            set { this.SetProperty<Single?>(MetaAverageDosProperty, value); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases, Message.PlanogramFixtureComponent_MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
            set { this.SetProperty<Single?>(MetaMinCasesProperty, value); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases, Message.PlanogramFixtureComponent_MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
            set { this.SetProperty<Single?>(MetaAverageCasesProperty, value); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases, Message.PlanogramFixtureComponent_MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
            set { this.SetProperty<Single?>(MetaMaxCasesProperty, value); }
        }
        #endregion

        #region MetaSpaceToUnitsIndex
        /// <summary>
        /// MetaSpaceToUnitsIndex property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaSpaceToUnitsIndexProperty =
            RegisterModelProperty<Int16?>(c => c.MetaSpaceToUnitsIndex, Message.PlanogramFixtureComponent_MetaSpaceToUnitsIndex);
        /// <summary>
        /// Gets/Sets the meta data MetaSpaceToUnitsIndex
        /// </summary>
        public Int16? MetaSpaceToUnitsIndex
        {
            get { return this.GetProperty<Int16?>(MetaSpaceToUnitsIndexProperty); }
            set { this.SetProperty<Int16?>(MetaSpaceToUnitsIndexProperty, value); }
        }
        #endregion

        #region MetaIsComponentSlopeWithNoRiser
        /// <summary>
        /// MetaIsComponentSlopeWithNoRiser property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsComponentSlopeWithNoRiserProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsComponentSlopeWithNoRiser, Message.PlanogramFixtureComponent_MetaIsComponentSlopeWithNoRiser);
        /// <summary>
        /// Gets/Sets the meta data MetaIsComponentSlopeWithNoRiser
        /// </summary>
        public Boolean? MetaIsComponentSlopeWithNoRiser
        {
            get { return this.GetProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty); }
            set { this.SetProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedDepth
        /// <summary>
        /// MetaIsOverMerchandisedDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedDepthProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedDepth, Message.PlanogramFixtureComponent_MetaIsOverMerchandisedDepth);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedDepth
        /// </summary>
        public Boolean? MetaIsOverMerchandisedDepth
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedHeight
        /// <summary>
        /// MetaIsOverMerchandisedHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedHeightProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedHeight, Message.PlanogramFixtureComponent_MetaIsOverMerchandisedHeight);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedHeight
        /// </summary>
        public Boolean? MetaIsOverMerchandisedHeight
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedWidth
        /// <summary>
        /// MetaIsOverMerchandisedWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedWidthProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedWidth, Message.PlanogramFixtureComponent_MetaIsOverMerchandisedWidth);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedWidth
        /// </summary>
        public Boolean? MetaIsOverMerchandisedWidth
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty, value); }
        }
        #endregion

        #region MetaTotalComponentCollisions
        /// <summary>
        /// MetaTotalComponentCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentCollisions, Message.PlanogramFixtureComponent_MetaTotalComponentCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentCollisions
        /// </summary>
        public Int32? MetaTotalComponentCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalPositionCollisions
        /// <summary>
        /// MetaTotalPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalPositionCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalPositionCollisions, Message.PlanogramFixtureComponent_MetaTotalPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalPositionCollisions
        /// </summary>
        public Int32? MetaTotalPositionCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalPositionCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalPositionCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, Message.PlanogramFixtureComponent_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaTotalFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, Message.PlanogramFixtureComponent_MetaAverageFrontFacings);

        private short _componentSequenceNumber;

        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
            set { this.SetProperty<Single?>(MetaAverageFrontFacingsProperty, value); }
        }

        #endregion

        #region MetaPercentageLinearSpaceFilled
        /// <summary>
        /// MetaPercentageLinearSpaceFilled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageLinearSpaceFilledProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageLinearSpaceFilled, Message.PlanogramFixtureComponent_MetaPercentageLinearSpaceFilled, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPercentageLinearSpaceFilled
        /// </summary>
        public Single? MetaPercentageLinearSpaceFilled
        {
            get { return this.GetProperty<Single?>(MetaPercentageLinearSpaceFilledProperty); }
            set { this.SetProperty<Single?>(MetaPercentageLinearSpaceFilledProperty, value); }
        }
        #endregion

        #region MetaWorldX
        /// <summary>
        /// MetaWorldX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldXProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldX, Message.PlanogramFixtureComponent_MetaWorldX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldX
        /// </summary>
        public Single? MetaWorldX
        {
            get { return this.GetProperty<Single?>(MetaWorldXProperty); }
            set { this.SetProperty<Single?>(MetaWorldXProperty, value); }
        }
        #endregion

        #region MetaWorldY
        /// <summary>
        /// MetaWorldY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldYProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldY, Message.PlanogramFixtureComponent_MetaWorldY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldY
        /// </summary>
        public Single? MetaWorldY
        {
            get { return this.GetProperty<Single?>(MetaWorldYProperty); }
            set { this.SetProperty<Single?>(MetaWorldYProperty, value); }
        }
        #endregion

        #region MetaWorldZ
        /// <summary>
        /// MetaWorldZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldZProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldZ, Message.PlanogramFixtureComponent_MetaWorldZ, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldZ
        /// </summary>
        public Single? MetaWorldZ
        {
            get { return this.GetProperty<Single?>(MetaWorldZProperty); }
            set { this.SetProperty<Single?>(MetaWorldZProperty, value); }
        }
        #endregion

        #region MetaWorldAngle
        /// <summary>
        /// MetaWorldAngle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldAngleProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldAngle, Message.PlanogramFixtureComponent_MetaWorldAngle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldAngle
        /// </summary>
        public Single? MetaWorldAngle
        {
            get { return this.GetProperty<Single?>(MetaWorldAngleProperty); }
            set { this.SetProperty<Single?>(MetaWorldAngleProperty, value); }
        }
        #endregion

        #region MetaWorldSlope
        /// <summary>
        /// MetaWorldSlope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldSlopeProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldSlope, Message.PlanogramFixtureComponent_MetaWorldSlope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldSlope
        /// </summary>
        public Single? MetaWorldSlope
        {
            get { return this.GetProperty<Single?>(MetaWorldSlopeProperty); }
            set { this.SetProperty<Single?>(MetaWorldSlopeProperty, value); }
        }
        #endregion

        #region MetaWorldRoll
        /// <summary>
        /// MetaWorldRoll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldRollProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldRoll, Message.PlanogramFixtureComponent_MetaWorldRoll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldRoll
        /// </summary>
        public Single? MetaWorldRoll
        {
            get { return this.GetProperty<Single?>(MetaWorldRollProperty); }
            set { this.SetProperty<Single?>(MetaWorldRollProperty, value); }
        }
        #endregion

        #region MetaIsOutsideOfFixtureArea
        /// <summary>
        /// MetaIsOutsideOfFixtureArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOutsideOfFixtureAreaProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOutsideOfFixtureArea, Message.PlanogramFixtureComponent_MetaIsOutsideOfFixtureArea);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOutsideOfFixtureArea
        /// </summary>
        public Boolean? MetaIsOutsideOfFixtureArea
        {
            get { return this.GetProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty, value); }
        }
        #endregion

        #endregion

        #region NotchNumber property

        /// <summary>
        /// NotchNumber property definition 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> NotchNumberProperty =
            RegisterModelProperty<Int32?>(c => c.NotchNumber);

        public Int32? NotchNumber
        {
            get { return this.GetProperty<Int32?>(NotchNumberProperty); }
            set { this.SetProperty<Int32?>(NotchNumberProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureComponent NewPlanogramFixtureComponent()
        {
            PlanogramFixtureComponent item = new PlanogramFixtureComponent();
            item.Create(null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureComponent NewPlanogramFixtureComponent(PlanogramComponent component)
        {
            PlanogramFixtureComponent item = new PlanogramFixtureComponent();
            item.Create(component);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramComponent component)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            if (component != null) this.LoadProperty<Object>(PlanogramComponentIdProperty, component.Id);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Copy

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramFixtureComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }
        #endregion

        #region Resolve
        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramComponentId
            Object planogramComponentId = context.ResolveId<PlanogramComponent>(this.ReadProperty<Object>(PlanogramComponentIdProperty));
            if (planogramComponentId != null) this.LoadProperty<Object>(PlanogramComponentIdProperty, planogramComponentId);
        }
        #endregion

        #region Placement Related

        /// <summary>
        /// Returns the associated planogram component
        /// </summary>
        public PlanogramComponent GetPlanogramComponent()
        {
            PlanogramComponent component = null;
            if ((this.PlanogramComponentId != null) && (this.Parent != null))
            {
                Planogram plan = this.Parent.Parent;
                if (plan != null)
                {
                    component = plan.Components.FindById(this.PlanogramComponentId);
                }
            }
            return component;
        }

        /// <summary>
        /// Returns all sub component placements associated
        /// to this fixture component
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            PlanogramFixture fixture = this.Parent;
            if (fixture == null) yield break;
            Planogram planogram = fixture.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.FixtureComponent))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTransform(PlanogramFixtureItem fixtureItem, Boolean includeLocal)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //Component
            if (includeLocal)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        this.X, this.Y, this.Z,
                        this.Angle, this.Slope, this.Roll));
            }

            //Fixture
            if (fixtureItem != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixtureItem.Angle, fixtureItem.Slope, fixtureItem.Roll));
            }


            return relativeTransform;
        }

        /// <summary>
        /// Returns the rotation for this component relative to the planogram
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private RotationValue GetPlanogramRelativeRotation(PlanogramFixtureItem fixtureItem, Boolean includeLocal)
        {
            RotationValue rotation = new RotationValue();

            //Component
            if (includeLocal)
            {
                rotation.Angle += this.Angle;
                rotation.Slope += this.Slope;
                rotation.Roll += this.Roll;
            }

            //Fixture
            if (fixtureItem != null)
            {
                rotation.Angle += fixtureItem.Angle;
                rotation.Slope += fixtureItem.Slope;
                rotation.Roll += fixtureItem.Roll;
            }

            return rotation;
        }

        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates(PlanogramFixtureItem fixtureItem)
        {
            PointValue localCoordinates = new PointValue(this.X, this.Y, this.Z);
            if (fixtureItem == null) return localCoordinates;

            //get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/false);

            //trasnform and return.
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate, PlanogramFixtureItem fixtureItem)
        {
            //get the relative parent transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/false);

            //invert it.
            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);

            this.X = newLocalCoordinate.X;
            this.Y = newLocalCoordinate.Y;
            this.Z = newLocalCoordinate.Z;
        }

        /// <summary>
        /// Returns the rotation of this component relative to the planogram as a whole.
        /// </summary>
        public RotationValue GetPlanogramRelativeRotation(PlanogramFixtureItem fixtureItem)
        {
            return GetPlanogramRelativeRotation(fixtureItem, /*incLocal*/true);
        }

        /// <summary>
        /// Sets the local rotation values based on the given world rotation.
        /// </summary>
        /// <param name="worldRotation"></param>
        public void SetRotationFromPlanogramRelative(RotationValue worldRotation, PlanogramFixtureItem fixtureItem)
        {
            //get the parent relative rotation
            RotationValue parentRotation = GetPlanogramRelativeRotation(fixtureItem, /*incLocal*/false);

            //strip away the parent rotation to get the new local values.
            this.Angle = worldRotation.Angle - parentRotation.Angle;
            this.Slope = worldRotation.Slope - parentRotation.Slope;
            this.Roll = worldRotation.Roll - parentRotation.Roll;
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetPlanogramRelativeBoundingBox(PlanogramComponent component, PlanogramFixtureItem fixtureItem)
        {
            RectValue bounds = new RectValue();
            bounds.Width = component.Width;
            bounds.Height = component.Height;
            bounds.Depth = component.Depth;

            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            return relativeTransform.TransformBounds(bounds);
        }

        /// <summary>
        /// Returns static boundary information for this component placement.
        /// </summary>
        /// <param name="component">the associated component</param>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <returns>Boundary information</returns>
        public BoundaryInfo GetBoundaryInfo(PlanogramComponent component, PlanogramFixtureItem fixtureItem)
        {
            return new BoundaryInfo(
                component.Height, component.Width, component.Depth,
               GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true));
        }

        /// <summary>
        /// Converts the given local point to a planogram relative one.
        /// </summary>
        public static PointValue ToWorld(PointValue localPoint, PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureComponent.GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            //trasnform and return.
            PointValue worldPoint = localPoint.Transform(relativeTransform);
            return worldPoint;
        }

        /// <summary>
        /// Converts the given world point to one which is local
        /// to this subcomponent placement.
        /// </summary>
        public static PointValue ToLocal(PointValue worldPoint, PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureComponent.GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            //invert the transform.
            relativeTransform.Invert();

            return worldPoint.Transform(relativeTransform);
        }


        /// <summary>
        /// Returns a notched subcomponent adjacent to the given component.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        private PlanogramSubComponentPlacement GetAdjacentNotchedSubComponent(PlanogramComponent component, PlanogramFixtureItem fixtureItem)
        {
            //TODO: Implement as a proper mesh collision check 

            RectValue componentWorldBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem);
            RotationValue componentWorldRotation = GetPlanogramRelativeRotation(fixtureItem);

            PlanogramSubComponentPlacement returnSub = null;

            foreach (PlanogramSubComponentPlacement subPlacement in fixtureItem.GetPlanogramSubComponentPlacements())
            {
                //only assess the sub if it is not a child of the component
                if (component == subPlacement.Component) continue;

                PlanogramSubComponent sub = subPlacement.SubComponent;

                // only  if it has notch values and if the rotation is the same.
                if (sub.NotchHeight <= 0 || sub.NotchWidth <= 0
                 || !(sub.IsNotchPlacedOnFront || sub.IsNotchPlacedOnBack || sub.IsNotchPlacedOnLeft || sub.IsNotchPlacedOnRight))
                {
                    continue;
                }

                RectValue subWorldBounds = subPlacement.GetPlanogramRelativeBoundingBox();

                //Front Notches
                if (sub.IsNotchPlacedOnFront)
                {
                    //check if there is a collision against the front facing.
                    RectValue subFrontFaceBounds =
                    subPlacement.GetPlanogramRelativeTransform().TransformBounds(new RectValue(0, 0, sub.Depth, sub.Width, sub.Height, 1));

                    if (componentWorldBounds.IntersectsWith(subFrontFaceBounds))
                    {
                        //check that the z placements meet
                        //if ((subWorldBounds.Z + subWorldBounds.Depth) == componentWorldBounds.Z
                        //    && componentWorldBounds.IntersectsWith(subWorldBounds))
                        //{
                        returnSub = subPlacement;
                        break;
                        //}
                    }
                }

                //Back Notches
                if (sub.IsNotchPlacedOnBack)
                {
                    //check that the rotation of the component snaps it to the back of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*-180deg*/Convert.ToSingle(-3.1415926536);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check the z placement
                        if (subWorldBounds.Z == componentWorldBounds.Z
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }

                //Left Notches
                if (sub.IsNotchPlacedOnLeft)
                {
                    //check that the rotation of the component snaps it to the left of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*-90deg*/Convert.ToSingle(-1.5707963268);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check x placement
                        if (subWorldBounds.X == componentWorldBounds.X
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }

                //Right Notches
                if (sub.IsNotchPlacedOnRight)
                {
                    //check that the rotation of the component snaps it to the right of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*90deg*/Convert.ToSingle(1.5707963268);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check x placement
                        if ((subWorldBounds.X + subWorldBounds.Width) == componentWorldBounds.X
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }

            }


            return returnSub;
        }

        /// <summary>
        /// Returns the notch number of the given component.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public Int32? GetNotchNumber(PlanogramComponent component, PlanogramFixtureItem fixtureItem)
        {
            Int32? notchNumber = null;

            PlanogramSubComponentPlacement snappedToSub = GetAdjacentNotchedSubComponent(component, fixtureItem);
            if (snappedToSub != null)
            {
                RectValue componentBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem);

                PointValue componentWorldSnapPos =
                    new PointValue(componentBounds.X, componentBounds.Y + component.Height, componentBounds.Z);

                //localise the snap point relative to the sub.
                Single componentSnapPos = PlanogramSubComponentPlacement.ToLocal(componentWorldSnapPos, snappedToSub).Y;

                notchNumber = snappedToSub.SubComponent.GetNearestNotchNumber(componentSnapPos);

                LoadProperty<Int32?>(NotchNumberProperty, notchNumber);
            }

            return notchNumber;
        }

        /// <summary>
        /// Snaps the component to the given notch number of an
        /// the adjacent notched subcomponent
        /// </summary>
        /// <param name="component"></param>
        /// <param name="value"></param>
        public void SetNotchNumber(Int32 value, PlanogramComponent component, PlanogramFixtureItem fixtureItem)
        {
            PlanogramSubComponentPlacement subPlacement = GetAdjacentNotchedSubComponent(component, fixtureItem);
            if (subPlacement != null)
            {
                PlanogramSubComponent sub = subPlacement.SubComponent;

                Single? subRelativeNotchY = sub.GetNotchLocalYPosition(value);
                if (subRelativeNotchY.HasValue)
                {
                    RectValue componentBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem);

                    PointValue componentPosLocalToSub = PlanogramSubComponentPlacement.ToLocal(
                        new PointValue(componentBounds.X, componentBounds.Y, componentBounds.Z), subPlacement);

                    componentPosLocalToSub.Y = subRelativeNotchY.Value - componentBounds.Height;

                    //transform the point back to world space.
                    PointValue worldSpaceSnap = PlanogramSubComponentPlacement.ToWorld(componentPosLocalToSub, subPlacement);

                    this.Y = PlanogramFixtureItem.ToLocal(worldSpaceSnap, fixtureItem).Y;

                    LoadProperty<Int32?>(NotchNumberProperty, value);
                }
            }
        }

        /// <summary>
        /// Determines if the component is merchandisable (Not base or Backboard)
        /// </summary>
        /// <returns></returns>
        public Boolean IsMerchandisibleComponent()
        {
            var componentType = GetPlanogramComponent()?.ComponentType;
            return componentType != null && componentType != PlanogramComponentType.Base && componentType != PlanogramComponentType.Backboard;
        }

        #endregion

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            //Return these as component properties:
            Type type = typeof(PlanogramComponent);
            String typeFriendly = PlanogramComponent.FriendlyName;
            String detailsGroup = Message.PlanogramComponent_PropertyGroup_Details;


            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ComponentSequenceNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchNumberProperty, detailsGroup);

            //nb local placement values as we display the planogram relative metadata ones instead.
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AngleProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SlopeProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RollProperty, detailsGroup);



            //Metadata fields
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldXProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldYProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldAngleProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldSlopeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldRollProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOutsideOfFixtureAreaProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableLinearSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableAreaSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableVolumetricSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalLinearWhiteSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalAreaWhiteSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalVolumetricWhiteSpaceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaProductsPlacedProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNewProductsProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangesFromPreviousCountProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangeFromPreviousStarRatingProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaBlocksDroppedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedInventoryProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFacingsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFacingsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalUnitsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageUnitsProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinDosProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxDosProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageDosProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinCasesProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageCasesProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxCasesProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaSpaceToUnitsIndexProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMerchandisedWidthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMerchandisedHeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMerchandisedDepthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentCollisionsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalPositionCollisionsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsComponentSlopeWithNoRiserProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFrontFacingsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFrontFacingsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageLinearSpaceFilledProperty, detailsGroup);

        }

        /// <summary>
        /// Returns the value of the given field for this position.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field, PlanogramFixtureItem fixtureItem)
        {
            return field.GetValue(this);
        }

        #endregion

        #region Validation

        /// <summary>
        /// Returns a list of validation warnings for this fixture component.
        /// </summary>
        public List<PlanogramValidationWarning> GetBrokenValidationWarnings(PlanogramMetadataDetails metaDetails, PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();

            PlanogramComponent component = this.GetPlanogramComponent();

            // Collision validation. the component has at least one collision.
            if ((this.MetaTotalComponentCollisions.HasValue) ? this.MetaTotalComponentCollisions > 0 :
                this.EnumerateComponentCollisions(metaDetails, fixtureItem, component).FirstOrDefault() != null)
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentHasCollisions, fixtureItem, this));
            }

            // Slope with no riser validation.
            if ((this.MetaIsComponentSlopeWithNoRiser.HasValue) ? this.MetaIsComponentSlopeWithNoRiser.Value :
                this.IsSlopedWithNoRiser(component))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentSlopeWithNoRiser, fixtureItem, this));
            }

            // Component is overfilled.
            //if ((this.MetaPercentageLinearSpaceFilled.HasValue) ? this.MetaPercentageLinearSpaceFilled > 1 : this.IsOverfilled(metaDetails))
            if (this.IsOverfilled(metaDetails))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentIsOverfilled, fixtureItem, this));
            }

            // Component is outside of the fixture area
            if (this.MetaIsOutsideOfFixtureArea == true)
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea, fixtureItem, this));
            }

            return warnings;
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            //Calculate meta data
            if (this.Parent == null) return;
            Planogram parentPlan = this.Parent.Parent;
            if (parentPlan == null) return;
            PlanogramPositionList positionList = parentPlan.Positions;
            if (positionList == null) return;
            PlanogramMetadataDetails context = parentPlan.GetPlanogramMetadataDetails();

            if (positionList != null)
            {
                //Get all positions assigned to this fixture company
                List<PlanogramPosition> positions = positionList.Where(p => Object.Equals(p.PlanogramFixtureComponentId, this.Id)).ToList();

                //Get all of the assigned products and position details
                List<PlanogramPositionDetails> fixtureComponentPositionDetails = new List<PlanogramPositionDetails>(positions.Count);
                List<PlanogramProduct> products = new List<PlanogramProduct>(positions.Count);
                foreach (PlanogramPosition position in positions)
                {
                    PlanogramPositionDetails positionDetails;
                    if (context.PositionDetails.TryGetValue(position.Id, out positionDetails))
                    {
                        fixtureComponentPositionDetails.Add(positionDetails);
                    }

                    PlanogramProduct product = parentPlan.Products.FindById(position.PlanogramProductId);
                    if (product != null)
                    {
                        products.Add(product);
                    }
                }

                PlanogramFixtureItem fixtureItem = parentPlan.FixtureItems.FirstOrDefault(i => Object.Equals(i.PlanogramFixtureId, this.Parent.Id));

                //Get component
                PlanogramComponent component = parentPlan.Components.FindById(this.PlanogramComponentId);

                List<PlanogramSubComponentPlacement> subPlacements = this.GetPlanogramSubComponentPlacements().ToList();

                this.MetaAverageCases = positions.Any() ? positions.Average(p => p.MetaAchievedCases) : 0;
                this.MetaAverageDos = 0; //TODO
                this.MetaAverageUnits = positions.Any() ? Convert.ToInt32(positions.Average(p => p.TotalUnits)) : (Int32)0;
                this.MetaBlocksDropped = 0; //TODO
                this.MetaChangeFromPreviousStarRating = 0; //TODO
                this.MetaChangesFromPreviousCount = 0; //TODO
                this.MetaIsComponentSlopeWithNoRiser = IsSlopedWithNoRiser(component);
                this.MetaIsOverMerchandisedDepth = subPlacements.Any(s => context.IsMerchandisingGroupOverfilled(s.Id, AxisType.Z));
                this.MetaIsOverMerchandisedHeight = subPlacements.Any(s => context.IsMerchandisingGroupOverfilled(s.Id, AxisType.Y));
                this.MetaIsOverMerchandisedWidth = subPlacements.Any(s => context.IsMerchandisingGroupOverfilled(s.Id, AxisType.X));
                this.MetaMaxCases = positions.Any() ? positions.Max(p => p.MetaAchievedCases) : 0;
                this.MetaMaxDos = 0; //TODO
                this.MetaMinCases = positions.Any() ? positions.Min(p => p.MetaAchievedCases) : 0;
                this.MetaMinDos = 0; //TODO
                this.MetaNewProducts = 0; //TODO
                this.MetaNotAchievedInventory = products.Any() ? products.Count(p => p.MetaNotAchievedInventory == true) : (Int16)0;
                this.MetaProductsPlaced = products.Count;
                this.MetaSpaceToUnitsIndex = 0; //TODO
                this.MetaTotalFacings = PlanogramHelper.CalculateMetaTotalFacings(positions, products);
                this.MetaAverageFacings = Convert.ToInt16(positions.Count > 0 ? MetaTotalFacings / positions.Count : 0);
                this.MetaTotalUnits = positions.Sum(p => p.TotalUnits);
                this.MetaTotalFrontFacings = Convert.ToInt16(positions.Any() ? positions.Sum(p => p.MetaFrontFacingsWide) : 0);
                this.MetaAverageFrontFacings = Convert.ToSingle(positions.Any() ? Math.Round(((double)this.MetaTotalFrontFacings / positions.Count), 2, MidpointRounding.AwayFromZero) : 0);

                //Count all of the positions relating to this planogram fixture component that have a collision
                this.MetaTotalPositionCollisions = positions.Count(p => p.MetaIsPositionCollisions == true);

                this.MetaTotalComponentCollisions = EnumerateComponentCollisions(parentPlan.GetPlanogramMetadataDetails(), fixtureItem, component).Count();


                //Get the merch space of all of the subcomponents in this fixture component.
                IEnumerable<PlanogramMerchandisingSpace> merchandisingSpaces = context.GetFixtureComponentMerchandisingSpace(this, fixtureItem);

                this.MetaTotalMerchandisableLinearSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableLinear()), MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero);
                this.MetaTotalMerchandisableAreaSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableArea()), MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero);
                this.MetaTotalMerchandisableVolumetricSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableVolume()), MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero);

                //V830352 - Calculate the white space at a cumulative merchandising space level, not as a total for the planogram.
                // Components which are overfaced mask white space on other shelves.
                Single totalLinearWhiteSpace = 0, totalAreaWhiteSpace = 0, totalVolumetricWhiteSpace = 0;
                foreach (PlanogramMerchandisingSpace merchSpace in merchandisingSpaces)
                {
                    if (merchSpace.SubComponentPlacement != null)
                    {
                        //Get all positions in this merch space
                        IEnumerable<PlanogramPosition> merchSpacePositions = merchSpace.SubComponentPlacement.GetPlanogramPositions();

                        //Get position details
                        List<Tuple<PlanogramPosition,PlanogramPositionDetails>> posInfos = 
                            new List<Tuple<PlanogramPosition,PlanogramPositionDetails>>(merchSpacePositions.Count());


                        List<PlanogramPositionDetails> merchSpacePositionDetails = new List<PlanogramPositionDetails>();
                        foreach (PlanogramPosition pos in merchSpacePositions)
                        {
                            PlanogramPositionDetails posDetails = null;
                            if (context.PositionDetails.TryGetValue(pos.Id, out posDetails))
                            {
                                merchSpacePositionDetails.Add(posDetails);

                                posInfos.Add(new Tuple<PlanogramPosition,PlanogramPositionDetails>(pos, posDetails));
                            }
                        }


                        //Linear White Space:
                        Single occupiedLinear = 0;
                        AxisType linearAxis = merchSpace.GetMerchandisableLinearAxis();
                        foreach(var placementGroup in posInfos.GroupBy(t => t.Item1.GetSequence(linearAxis)))
                        {
                            occupiedLinear += 
                                placementGroup.Max(p=> (p.Item2.MainCoordinates.GetCoordinate(linearAxis) + p.Item2.TotalSize.GetSize(linearAxis)))
                                - placementGroup.Min(p=> p.Item2.MainCoordinates.GetCoordinate(linearAxis));
                        }
                        totalLinearWhiteSpace += PlanogramHelper.CalculateMetaTotalWhiteSpace((Single?)merchSpace.GetMerchandisableLinear(), occupiedLinear).Value;



                        // The total width of white space. That is, the difference between the sum of the widths
                        // of all the positions at the front of the plan and the total width of merchandising space of
                        // all of the subcomponents.
                        //totalLinearWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        //    (Single?)merchSpace.GetMerchandisableLinear(),
                        //    merchSpacePositionDetails.Sum(p => p.TotalSize.Width)).Value);

                        //TODO: CORRECT THESE TO MATCH LINEAR.


                        // The total area of white space. That is, the difference between the sum of all the positions
                        // that are at the front of the plan and the sum of the merchandisable areas of all the subcomponents.
                        totalAreaWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                            (Single?)merchSpace.GetMerchandisableArea(),
                            merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height)).Value);

                        // The total volume of white space. That is, the difference between the sum of the volumes 
                        // of all the positions and the sum of the merchandisable volumes of all the subcomponents.
                        totalVolumetricWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                            (Single?)merchSpace.GetMerchandisableVolume(),
                            merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height * p.TotalSize.Depth)).Value);
                    }
                }

                //Set cumulative values to be totals
                //these are percentages so are rounded to a higher precision.
                this.MetaTotalLinearWhiteSpace = (Single?)Math.Round(totalLinearWhiteSpace, 5, MidpointRounding.AwayFromZero);
                this.MetaTotalAreaWhiteSpace = (Single?)Math.Round(totalAreaWhiteSpace, 5, MidpointRounding.AwayFromZero);
                this.MetaTotalVolumetricWhiteSpace = (Single?)Math.Round(totalVolumetricWhiteSpace, 5, MidpointRounding.AwayFromZero);
            
                this.MetaPercentageLinearSpaceFilled = PlanogramHelper.CalculateMetaPercentageLinearSpaceFilled(
                    this.MetaTotalMerchandisableLinearSpace, fixtureComponentPositionDetails.Sum(p => p.TotalSize.Width));

                //Planogram relative placement:
                PointValue worldCoordinates = GetPlanogramRelativeCoordinates(fixtureItem);
                this.MetaWorldX = Convert.ToSingle(Math.Round(worldCoordinates.X, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));
                this.MetaWorldY = Convert.ToSingle(Math.Round(worldCoordinates.Y, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));
                this.MetaWorldZ = Convert.ToSingle(Math.Round(worldCoordinates.Z, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));
                RotationValue worldRotation = GetPlanogramRelativeRotation(fixtureItem);
                this.MetaWorldAngle = Convert.ToSingle(Math.Round(worldRotation.Angle, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));
                this.MetaWorldSlope = Convert.ToSingle(Math.Round(worldRotation.Slope, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));
                this.MetaWorldRoll = Convert.ToSingle(Math.Round(worldRotation.Roll, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero));

                //Area specific Meta
                this.MetaIsOutsideOfFixtureArea = IsEntirelyOutsideParentFixture();                
            }
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            this.MetaAverageCases = null;
            this.MetaAverageDos = null;
            this.MetaAverageFacings = null;
            this.MetaAverageUnits = null;
            this.MetaBlocksDropped = null;
            this.MetaChangeFromPreviousStarRating = null;
            this.MetaChangesFromPreviousCount = null;
            this.MetaIsComponentSlopeWithNoRiser = null;
            this.MetaIsOverMerchandisedDepth = null;
            this.MetaIsOverMerchandisedHeight = null;
            this.MetaIsOverMerchandisedWidth = null;
            this.MetaMaxCases = null;
            this.MetaMaxDos = null;
            this.MetaMinCases = null;
            this.MetaMinDos = null;
            this.MetaNewProducts = null;
            this.MetaNotAchievedInventory = null;
            this.MetaProductsPlaced = null;
            this.MetaSpaceToUnitsIndex = null;
            this.MetaTotalMerchandisableLinearSpace = null;
            this.MetaTotalMerchandisableAreaSpace = null;
            this.MetaTotalMerchandisableVolumetricSpace = null;
            this.MetaTotalComponentCollisions = null;
            this.MetaTotalPositionCollisions = null;
            this.MetaTotalFacings = null;
            this.MetaTotalLinearWhiteSpace = null;
            this.MetaTotalAreaWhiteSpace = null;
            this.MetaTotalVolumetricWhiteSpace = null;
            this.MetaTotalUnits = null;
            this.MetaTotalFrontFacings = null;
            this.MetaAverageFrontFacings = null;
            this.MetaPercentageLinearSpaceFilled = null;
            this.MetaWorldX = null;
            this.MetaWorldY = null;
            this.MetaWorldZ = null;
            this.MetaWorldAngle = null;
            this.MetaWorldSlope = null;
            this.MetaWorldRoll = null;
            this.MetaIsOutsideOfFixtureArea = null;  
        }

        /// <summary>
        ///     Returns true if this component is sloped but has no front riser.
        /// </summary>
        private Boolean IsSlopedWithNoRiser(PlanogramComponent component)
        {
            if (component == null) return false;

            // if we have no slope then return false as there is no error.
            if (this.Slope.EqualTo(0)) return false;

            Boolean allSubsHaveFrontRiser = component.SubComponents.All(p => p.IsRiserPlacedOnFront);

            return !allSubsHaveFrontRiser;
        }

        /// <summary>
        ///     Enumerates through collisions between this component and others on the plan.
        /// </summary>
        /// <param name="metadataDetails"></param>
        /// <param name="fixtureItem"></param>
        /// <returns></returns>
        private IEnumerable<PlanogramComponentPlacementId> EnumerateComponentCollisions(PlanogramMetadataDetails metadataDetails, PlanogramFixtureItem fixtureItem, PlanogramComponent component)
        {
            //if this is a base or backboard then ignore.
            // If another component does collide then this will be registered against that component directly.
            if (component == null || component.ComponentType == PlanogramComponentType.Base
                || component.ComponentType == PlanogramComponentType.Backboard)
                yield break;


            PlanogramComponentPlacementId thisId = new PlanogramComponentPlacementId(this, fixtureItem, metadataDetails.Planogram);

            var childPlacements = metadataDetails.SubPlacementInfosByComponent[thisId];


            //group all placement infos by component id and cycle through
            foreach (var componentPlacementIdGroup in metadataDetails.SubPlacementInfosByComponent)
            {
                //skip if this component.
                if (componentPlacementIdGroup.Key == thisId) continue;

                Boolean collisionFound = false;

                //cycle through all subs in the component
                foreach (var otherPlacement in componentPlacementIdGroup)
                {
                    //checking for collisions against each sub on this one.
                    foreach (var child in childPlacements)
                    {
                        if (child.WorldBoundary.CollidesWith(otherPlacement.WorldBoundary))
                        {
                            //returning the component id if a collision is found.
                            yield return new PlanogramComponentPlacementId(otherPlacement.Placement);

                            collisionFound = true;
                            break;
                        }
                    }

                    //break out if we have already found a collision with this component
                    // as we only need to return it once.
                    if (collisionFound) break;
                }
            }
        }

        /// <summary>
        ///     Check whether the component is overfilled or not.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Boolean IsOverfilled(PlanogramMetadataDetails context)
        {
            return
                this.GetPlanogramSubComponentPlacements()
                .Any(s => context.IsMerchandisingGroupOverfilled(s.Id));
        }

        /// <summary>
        /// Checks to see if a component is within it's parent fixture area.
        /// </summary>
        /// <returns>Returns true if the current component is entirely outside the fixture area, else false. Null when not on a fixture.</returns>
        public Boolean? IsEntirelyOutsideParentFixture()
        {
            PlanogramFixture parentFixture = this.Parent;
            PlanogramFixtureItem parentFixtureItem = parentFixture.Parent.FixtureItems.FirstOrDefault(f => f.PlanogramFixtureId.Equals(parentFixture.Id));

            if (parentFixtureItem == null) return null;

            IEnumerable<PlanogramSubComponentPlacement> myComponents = this.GetPlanogramSubComponentPlacements();

            WidthHeightDepthValue fixtureSpace = new WidthHeightDepthValue(parentFixture.Width, parentFixture.Height, parentFixture.Depth);
            PointValue fixturePoint = new PointValue(parentFixtureItem.X, parentFixtureItem.Y, parentFixtureItem.Z);
            RectValue fixtureBoundingBox = new RectValue(fixturePoint, fixtureSpace);


            //Component point needs to get world relative coords
            PointValue componentPoint = this.GetPlanogramRelativeCoordinates(parentFixtureItem);

            foreach (PlanogramSubComponentPlacement subCom in myComponents)
            {
                WidthHeightDepthValue componentSpace = new WidthHeightDepthValue(subCom.SubComponent.Width, subCom.SubComponent.Height, subCom.SubComponent.Depth);
                RectValue componentBoundingBox = new RectValue(componentPoint, componentSpace);

                // If doesn't intersect must be outside.
                if (!componentBoundingBox.IntersectsWith(fixtureBoundingBox)) return true;
            }

            return false;
        }

        #endregion

        #endregion
    }
}