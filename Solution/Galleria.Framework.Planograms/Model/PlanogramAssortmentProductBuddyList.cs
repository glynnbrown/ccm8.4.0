﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentProductBuddyList : ModelList<PlanogramAssortmentProductBuddyList, PlanogramAssortmentProductBuddy>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent objecta
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentProductBuddyList NewPlanogramAssortmentProductBuddyList()
        {
            var item = new PlanogramAssortmentProductBuddyList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
        
        #region Methods

        public void AddRange(IEnumerable<IPlanogramAssortmentProductBuddy> buddies)
        {
            base.AddRange(buddies.Select(b => PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy(b)));
        }

        public void RemoveAllBuddies()
        {
            base.Clear();
        }

        public void RemoveBuddiesBySourceGtin(String sourceProductGtin)
        {
            List<PlanogramAssortmentProductBuddy> itemsToRemove = Items.Where(item => item.ProductGtin.Equals(sourceProductGtin, StringComparison.InvariantCultureIgnoreCase)).ToList();
            base.RemoveList(itemsToRemove);
        }

        #endregion

        public Boolean HasBuddiesForSourceGtin(String sourceProductGtin)
        {
            return Items != null && Items.Any(p => p.ProductGtin == sourceProductGtin);
        }
    }
}

