﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class PlanogramImageList : ModelList<PlanogramImageList, PlanogramImage>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramImageList NewPlanogramImageList()
        {
            PlanogramImageList item = new PlanogramImageList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
