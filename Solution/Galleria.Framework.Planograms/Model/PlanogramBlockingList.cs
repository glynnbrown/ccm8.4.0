﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of Blockings contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramBlockingList : ModelList<PlanogramBlockingList, PlanogramBlocking>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingList NewPlanogramBlockingList()
        {
            PlanogramBlockingList item = new PlanogramBlockingList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
