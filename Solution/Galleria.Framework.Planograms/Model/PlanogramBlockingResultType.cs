﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 810)
// CCM-29057 : D.Pleasance
//		Created 
#endregion
#endregion

using System.Collections.Generic;
using System;
using Galleria.Framework.Planograms.Resources.Language;
namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramBlockingResultType
    {
        InsufficientComponentSpace = 0
    }    
}