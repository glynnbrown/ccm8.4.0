﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History: CCM830
// CCM-32563 : L.Ineson
//  Added RemoveFixtureAndDependants
// V8-32795 : L.Bailey
//  Dragging bays between plans mixes up products
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of fixtures contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramFixtureList : ModelList<PlanogramFixtureList, PlanogramFixture>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        public static PlanogramFixtureList NewPlanogramFixtureList()
        {
            PlanogramFixtureList item = new PlanogramFixtureList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void  Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixture Add()
        {
            PlanogramFixture item = PlanogramFixture.NewPlanogramFixture();
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixture Add(String name)
        {
            PlanogramFixture item = PlanogramFixture.NewPlanogramFixture(name);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixture Add(IPlanogramSettings settings)
        {
            PlanogramFixture item = PlanogramFixture.NewPlanogramFixture(settings);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixture Add(String name, IPlanogramSettings settings)
        {
            PlanogramFixture item = PlanogramFixture.NewPlanogramFixture(name, settings);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Removes this fixture and all of its dependants from the planogram.
        /// </summary>
        /// <param name="fixture"></param>
        public void RemoveFixtureAndDependants(PlanogramFixture fixture)
        {
            //if we have no parent then just return.
            if (this.Parent == null)
            {
                Remove(fixture);
                return;
            }

            //remove any linked fixture items
            List<Object> removedFixtureItemIds = new List<Object>();
            foreach (PlanogramFixtureItem fixtureItem in this.Parent.FixtureItems.ToArray())
            {
                if (Object.Equals(fixtureItem.PlanogramFixtureId, fixture.Id))
                {
                    removedFixtureItemIds.Add(fixtureItem.Id);
                    this.Parent.FixtureItems.Remove(fixtureItem);
                }
            }

            //remove positions            
            foreach (var pos in
                this.Parent.Positions.Where(p =>
                    p.PlanogramFixtureItemId != null
                    && removedFixtureItemIds.Contains(p.PlanogramFixtureItemId))
                    .ToList())
            {
                this.Parent.Positions.Remove(pos);
            }

            //remove annotations.
            this.Parent.Annotations.RemoveList(
                this.Parent.Annotations.Where(a=>
                    a.PlanogramFixtureItemId != null
                    && removedFixtureItemIds.Contains(a.PlanogramFixtureItemId))
                    .ToList());


            //remove all assemblies not referenced elsewhere
            List<Object> referencedAssemblyIds =
                 this.Parent.Fixtures.Where(f => f != fixture).SelectMany(f => f.Assemblies.Select(c => c.Id)).ToList();

            List<PlanogramAssembly> planAssemblies =
                fixture.Assemblies
                .Where(c => !referencedAssemblyIds.Contains(c.PlanogramAssemblyId))
                .Select(c => c.GetPlanogramAssembly()).ToList();
            foreach (PlanogramAssembly assembly in planAssemblies)
            {
                //remove the assembly and all of its dependants.
                this.Parent.Assemblies.RemoveAssemblyAndDependants(assembly);
            }

            //remove all components not referenced elsewhere
            List<Object> referencedComponentIds =
                this.Parent.Fixtures.Where(f=> f!= fixture).SelectMany(f => f.Components.Select(c => c.Id))
                .Union(this.Parent.Assemblies.SelectMany(a => a.Components.Select(c => c.Id)))
                .ToList();

            List<PlanogramComponent> planComponents =
                fixture.Components
                .Where(c => !referencedComponentIds.Contains(c.PlanogramComponentId))
                .Select(c => c.GetPlanogramComponent()).ToList();
            foreach (PlanogramComponent component in planComponents)
            {
                //remove the component and all of its dependants.
                this.Parent.Components.RemoveComponentAndDependants(component);
            }


            //finally remove this fixture
            Remove(fixture);
        }

        #endregion
    }
}
