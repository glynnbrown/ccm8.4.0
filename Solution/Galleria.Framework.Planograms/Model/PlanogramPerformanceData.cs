﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26271 : A.Kuszyk
//  Created.
// V8-26377 : L.Ineson
//  Added helper methods to find out metric name and whether it is use.
// V8-27647 : L.Luong
//  Added InventoryProperties and UpdateInventoryValues Method

#endregion

#region Version History: CCM801

// V8-28759 : J.Pickup
//  Imporved UpdateInventoryValues Method null checks
// V8-28886 : L.Ineson
//  Removed use of OnObjectGraphMember changed.

#endregion

#region Version History: CCM802

// V8-29030 : J.Pickup
//  Now implements IPlanogramPerformanceData. 
// V8-28811 : L.Luong
//  Added MetaP1Percentage to MetaP20Percentage

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29378 : N.Foster
//  Fixed divide by zero issues
// V8-29727 : L.Ineson
//  Added EnumeratePlanogramDisplayableFieldInfos
// V8-29682 : A.Probyn
//  Updated UpdateInventoryValues to take into account AggregationType

#endregion

#region Version History: CCM810

// V8-29546 : L.Ineson
//  Metadata value are set to 0 if the product is not placed.
// V8-29681 : A.Probyn
//  Fixed typo in EnumeratePlanogramDisplayableFieldInfos for decimal metric types being displayed as lengths.

#endregion

#region Version History: CCM811

// V8-30467 : A.Probyn
//  Updated UpdateInventoryValues to divide not sum performance data by 7 as its weekly average.
// V8-28911 : A.Silva
//  Added SetPerformanceData(Byte, Single?) to set the value of Performance data by its MetricId.

#endregion

#region Version History: CCM820
// V8-30992 : A.Probyn
//  Added MinimumInventoryUnits and updated UpdatedInventoryValues
// V8-31209 : J.Pickup
//  Now determines between regular and promotional when updating inventory values. (Code previously existed but Ibye removed it).
// V8-31316 : A.Kuszyk
//  Added GetPlanogramProduct method.
#endregion
#region Version History: CCM830
// V8-32264 : A.Probyn
//  Updated OnCalculateMetadata to be able to deal with values above AND below 0. 0 is the only problematic value as
//  we can't divide by 0 for mathmatical reasons.
// CCM-13956 : M.Pettit
//  Unwound changes made in V8-32264 to OnCalculateMetadata
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Enums;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing an item of Planogram Performance Data.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPerformanceData :
        ModelObject<PlanogramPerformanceData>, IPlanogramPerformanceData, IPerformanceDataExtended
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramPerformance Parent
        {
            get
            {
                var parentList = base.Parent as PlanogramPerformanceDataList;
                if (parentList == null) return null;

                return parentList.Parent as PlanogramPerformance;
            }
        }
        #endregion

        #region Properties

        #region PlanogramProductId
        /// <summary>
        /// PlanogramProductId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramProductIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramProductId);
        /// <summary>
        /// PlanogramProductId
        /// </summary>
        public Object PlanogramProductId
        {
            get { return GetProperty<Object>(PlanogramProductIdProperty); }
            set { SetProperty<Object>(PlanogramProductIdProperty, value); }
        }
        #endregion

        #region P1
        /// <summary>
        /// P1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P1Property =
            RegisterModelProperty<Single?>(c => c.P1, Message.PlanogramPerformanceData_P1);
        /// <summary>
        /// P1
        /// </summary>
        public Single? P1
        {
            get { return GetProperty<Single?>(P1Property); }
            set
            {
                if (ReadProperty<Single?>(P1Property) != value)
                {
                    SetProperty<Single?>(P1Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P2
        /// <summary>
        /// P2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P2Property =
            RegisterModelProperty<Single?>(c => c.P2, Message.PlanogramPerformanceData_P2);
        /// <summary>
        /// P2
        /// </summary>
        public Single? P2
        {
            get { return GetProperty<Single?>(P2Property); }
            set
            {
                if (ReadProperty<Single?>(P2Property) != value)
                {
                    SetProperty<Single?>(P2Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P3
        /// <summary>
        /// P3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P3Property =
            RegisterModelProperty<Single?>(c => c.P3, Message.PlanogramPerformanceData_P3);
        /// <summary>
        /// P3
        /// </summary>
        public Single? P3
        {
            get { return GetProperty<Single?>(P3Property); }
            set
            {
                if (ReadProperty<Single?>(P3Property) != value)
                {
                    SetProperty<Single?>(P3Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P4
        /// <summary>
        /// P4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P4Property =
            RegisterModelProperty<Single?>(c => c.P4, Message.PlanogramPerformanceData_P4);
        /// <summary>
        /// P4
        /// </summary>
        public Single? P4
        {
            get { return GetProperty<Single?>(P4Property); }
            set
            {
                if (ReadProperty<Single?>(P4Property) != value)
                {
                    SetProperty<Single?>(P4Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P5
        /// <summary>
        /// P5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P5Property =
            RegisterModelProperty<Single?>(c => c.P5, Message.PlanogramPerformanceData_P5);
        /// <summary>
        /// P5
        /// </summary>
        public Single? P5
        {
            get { return GetProperty<Single?>(P5Property); }
            set
            {
                if (ReadProperty<Single?>(P5Property) != value)
                {
                    SetProperty<Single?>(P5Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P6
        /// <summary>
        /// P6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P6Property =
            RegisterModelProperty<Single?>(c => c.P6, Message.PlanogramPerformanceData_P6);
        /// <summary>
        /// P6
        /// </summary>
        public Single? P6
        {
            get { return GetProperty<Single?>(P6Property); }
            set
            {
                if (ReadProperty<Single?>(P6Property) != value)
                {
                    SetProperty<Single?>(P6Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P7
        /// <summary>
        /// P7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P7Property =
            RegisterModelProperty<Single?>(c => c.P7, Message.PlanogramPerformanceData_P7);
        /// <summary>
        /// P7
        /// </summary>
        public Single? P7
        {
            get { return GetProperty<Single?>(P7Property); }
            set
            {
                if (ReadProperty<Single?>(P7Property) != value)
                {
                    SetProperty<Single?>(P7Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P8
        /// <summary>
        /// P8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P8Property =
            RegisterModelProperty<Single?>(c => c.P8, Message.PlanogramPerformanceData_P8);
        /// <summary>
        /// P8
        /// </summary>
        public Single? P8
        {
            get { return GetProperty<Single?>(P8Property); }
            set
            {
                if (ReadProperty<Single?>(P8Property) != value)
                {
                    SetProperty<Single?>(P8Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P9
        /// <summary>
        /// P9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P9Property =
            RegisterModelProperty<Single?>(c => c.P9, Message.PlanogramPerformanceData_P9);
        /// <summary>
        /// P9
        /// </summary>
        public Single? P9
        {
            get { return GetProperty<Single?>(P9Property); }
            set
            {
                if (ReadProperty<Single?>(P9Property) != value)
                {
                    SetProperty<Single?>(P9Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P10
        /// <summary>
        /// P10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P10Property =
            RegisterModelProperty<Single?>(c => c.P10, Message.PlanogramPerformanceData_P10);
        /// <summary>
        /// P10
        /// </summary>
        public Single? P10
        {
            get { return GetProperty<Single?>(P10Property); }
            set
            {
                if (ReadProperty<Single?>(P10Property) != value)
                {
                    SetProperty<Single?>(P10Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P11
        /// <summary>
        /// P11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P11Property =
            RegisterModelProperty<Single?>(c => c.P11, Message.PlanogramPerformanceData_P11);
        /// <summary>
        /// P11
        /// </summary>
        public Single? P11
        {
            get { return GetProperty<Single?>(P11Property); }
            set
            {
                if (ReadProperty<Single?>(P11Property) != value)
                {
                    SetProperty<Single?>(P11Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P12
        /// <summary>
        /// P12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P12Property =
            RegisterModelProperty<Single?>(c => c.P12, Message.PlanogramPerformanceData_P12);
        /// <summary>
        /// P12
        /// </summary>
        public Single? P12
        {
            get { return GetProperty<Single?>(P12Property); }
            set
            {
                if (ReadProperty<Single?>(P12Property) != value)
                {
                    SetProperty<Single?>(P12Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P13
        /// <summary>
        /// P13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P13Property =
            RegisterModelProperty<Single?>(c => c.P13, Message.PlanogramPerformanceData_P13);
        /// <summary>
        /// P13
        /// </summary>
        public Single? P13
        {
            get { return GetProperty<Single?>(P13Property); }
            set
            {
                if (ReadProperty<Single?>(P13Property) != value)
                {
                    SetProperty<Single?>(P13Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P14
        /// <summary>
        /// P14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P14Property =
            RegisterModelProperty<Single?>(c => c.P14, Message.PlanogramPerformanceData_P14);
        /// <summary>
        /// P14
        /// </summary>
        public Single? P14
        {
            get { return GetProperty<Single?>(P14Property); }
            set
            {
                if (ReadProperty<Single?>(P14Property) != value)
                {
                    SetProperty<Single?>(P14Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P15
        /// <summary>
        /// P15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P15Property =
            RegisterModelProperty<Single?>(c => c.P15, Message.PlanogramPerformanceData_P15);
        /// <summary>
        /// P15
        /// </summary>
        public Single? P15
        {
            get { return GetProperty<Single?>(P15Property); }
            set
            {
                if (ReadProperty<Single?>(P15Property) != value)
                {
                    SetProperty<Single?>(P15Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P16
        /// <summary>
        /// P16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P16Property =
            RegisterModelProperty<Single?>(c => c.P16, Message.PlanogramPerformanceData_P16);
        /// <summary>
        /// P16
        /// </summary>
        public Single? P16
        {
            get { return GetProperty<Single?>(P16Property); }
            set
            {
                if (ReadProperty<Single?>(P16Property) != value)
                {
                    SetProperty<Single?>(P16Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P17
        /// <summary>
        /// P17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P17Property =
            RegisterModelProperty<Single?>(c => c.P17, Message.PlanogramPerformanceData_P17);
        /// <summary>
        /// P17
        /// </summary>
        public Single? P17
        {
            get { return GetProperty<Single?>(P17Property); }
            set
            {
                if (ReadProperty<Single?>(P17Property) != value)
                {
                    SetProperty<Single?>(P17Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P18
        /// <summary>
        /// P18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P18Property =
            RegisterModelProperty<Single?>(c => c.P18, Message.PlanogramPerformanceData_P18);
        /// <summary>
        /// P18
        /// </summary>
        public Single? P18
        {
            get { return GetProperty<Single?>(P18Property); }
            set
            {
                if (ReadProperty<Single?>(P18Property) != value)
                {
                    SetProperty<Single?>(P18Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P19
        /// <summary>
        /// P19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P19Property =
            RegisterModelProperty<Single?>(c => c.P19, Message.PlanogramPerformanceData_P19);
        /// <summary>
        /// P19
        /// </summary>
        public Single? P19
        {
            get { return GetProperty<Single?>(P19Property); }
            set
            {
                if (ReadProperty<Single?>(P19Property) != value)
                {
                    SetProperty<Single?>(P19Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region P20
        /// <summary>
        /// P20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P20Property =
            RegisterModelProperty<Single?>(c => c.P20, Message.PlanogramPerformanceData_P20);
        /// <summary>
        /// P20
        /// </summary>
        public Single? P20
        {
            get { return GetProperty<Single?>(P20Property); }
            set
            {
                if (ReadProperty<Single?>(P20Property) != value)
                {
                    SetProperty<Single?>(P20Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region Calculated

        /// <summary>
        /// CP1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP1Property =
            RegisterModelProperty<Single?>(c => c.CP1, Message.PlanogramPerformanceData_CP1);
        /// <summary>
        /// CP1
        /// </summary>
        public Single? CP1
        {
            get { return GetProperty<Single?>(CP1Property); }
            set
            {
                if (ReadProperty<Single?>(CP1Property) != value)
                {
                    SetProperty<Single?>(CP1Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP2Property =
            RegisterModelProperty<Single?>(c => c.CP2, Message.PlanogramPerformanceData_CP2);
        /// <summary>
        /// CP2
        /// </summary>
        public Single? CP2
        {
            get { return GetProperty<Single?>(CP2Property); }
            set
            {
                if (ReadProperty<Single?>(CP2Property) != value)
                {
                    SetProperty<Single?>(CP2Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP3Property =
            RegisterModelProperty<Single?>(c => c.CP3, Message.PlanogramPerformanceData_CP3);
        /// <summary>
        /// CP3
        /// </summary>
        public Single? CP3
        {
            get { return GetProperty<Single?>(CP3Property); }
            set
            {
                if (ReadProperty<Single?>(CP3Property) != value)
                {
                    SetProperty<Single?>(CP3Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP4Property =
            RegisterModelProperty<Single?>(c => c.CP4, Message.PlanogramPerformanceData_CP4);
        /// <summary>
        /// CP4
        /// </summary>
        public Single? CP4
        {
            get { return GetProperty<Single?>(CP4Property); }
            set
            {
                if (ReadProperty<Single?>(CP4Property) != value)
                {
                    SetProperty<Single?>(CP4Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP5Property =
            RegisterModelProperty<Single?>(c => c.CP5, Message.PlanogramPerformanceData_CP5);
        /// <summary>
        /// CP5
        /// </summary>
        public Single? CP5
        {
            get { return GetProperty<Single?>(CP5Property); }
            set
            {
                if (ReadProperty<Single?>(CP5Property) != value)
                {
                    SetProperty<Single?>(CP5Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP6Property =
            RegisterModelProperty<Single?>(c => c.CP6, Message.PlanogramPerformanceData_CP6);
        /// <summary>
        /// CP6
        /// </summary>
        public Single? CP6
        {
            get { return GetProperty<Single?>(CP6Property); }
            set
            {
                if (ReadProperty<Single?>(CP6Property) != value)
                {
                    SetProperty<Single?>(CP6Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP7Property =
            RegisterModelProperty<Single?>(c => c.CP7, Message.PlanogramPerformanceData_CP7);
        /// <summary>
        /// CP7
        /// </summary>
        public Single? CP7
        {
            get { return GetProperty<Single?>(CP7Property); }
            set
            {
                if (ReadProperty<Single?>(CP7Property) != value)
                {
                    SetProperty<Single?>(CP7Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP8Property =
            RegisterModelProperty<Single?>(c => c.CP8, Message.PlanogramPerformanceData_CP8);
        /// <summary>
        /// CP8
        /// </summary>
        public Single? CP8
        {
            get { return GetProperty<Single?>(CP8Property); }
            set
            {
                if (ReadProperty<Single?>(CP8Property) != value)
                {
                    SetProperty<Single?>(CP8Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP9Property =
            RegisterModelProperty<Single?>(c => c.CP9, Message.PlanogramPerformanceData_CP9);
        /// <summary>
        /// CP9
        /// </summary>
        public Single? CP9
        {
            get { return GetProperty<Single?>(CP9Property); }
            set
            {
                if (ReadProperty<Single?>(CP9Property) != value)
                {
                    SetProperty<Single?>(CP9Property, value);

                    UpdateInventoryValues();
                }
            }
        }

        /// <summary>
        /// CP10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CP10Property =
            RegisterModelProperty<Single?>(c => c.CP10, Message.PlanogramPerformanceData_CP10);
        /// <summary>
        /// CP10
        /// </summary>
        public Single? CP10
        {
            get { return GetProperty<Single?>(CP10Property); }
            set
            {
                if (ReadProperty<Single?>(CP10Property) != value)
                {
                    SetProperty<Single?>(CP10Property, value);

                    UpdateInventoryValues();
                }
            }
        }
        #endregion

        #region UnitsSoldPerDay
        /// <summary>
        /// UnitsSoldPerDay property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> UnitsSoldPerDayProperty =
            RegisterModelProperty<Single?>(c => c.UnitsSoldPerDay, Message.PlanogramPerformanceData_UnitsSoldPerDay);
        /// <summary>
        /// UnitsSoldPerDay
        /// </summary>
        public Single? UnitsSoldPerDay
        {
            get { return GetProperty<Single?>(UnitsSoldPerDayProperty); }
            set { SetProperty<Single?>(UnitsSoldPerDayProperty, value); }
        }
        #endregion

        #region AchievedCasePacks
        /// <summary>
        /// AchievedCasePacks property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedCasePacksProperty =
            RegisterModelProperty<Single?>(c => c.AchievedCasePacks, Message.PlanogramPerformanceData_AchievedCasePacks);
        /// <summary>
        /// AchievedCasePacks
        /// </summary>
        public Single? AchievedCasePacks
        {
            get { return GetProperty<Single?>(AchievedCasePacksProperty); }
            set { SetProperty<Single?>(AchievedCasePacksProperty, value); }
        }
        #endregion

        #region AchievedDos
        /// <summary>
        /// AchievedDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedDosProperty =
            RegisterModelProperty<Single?>(c => c.AchievedDos, Message.PlanogramPerformanceData_AchievedDos);
        /// <summary>
        /// AchievedDos
        /// </summary>
        public Single? AchievedDos
        {
            get { return GetProperty<Single?>(AchievedDosProperty); }
            set { SetProperty<Single?>(AchievedDosProperty, value); }
        }
        #endregion

        #region AchievedShelfLife
        /// <summary>
        /// AchievedShelfLife property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedShelfLifeProperty =
            RegisterModelProperty<Single?>(c => c.AchievedShelfLife, Message.PlanogramPerformanceData_AchievedShelfLife);
        /// <summary>
        /// AchievedShelfLife
        /// </summary>
        public Single? AchievedShelfLife
        {
            get { return GetProperty<Single?>(AchievedShelfLifeProperty); }
            set { SetProperty<Single?>(AchievedShelfLifeProperty, value); }
        }
        #endregion

        #region AchievedDeliveries
        /// <summary>
        /// AchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedDeliveriesProperty =
            RegisterModelProperty<Single?>(c => c.AchievedDeliveries, Message.PlanogramPerformanceData_AchievedDeliveries);
        /// <summary>
        /// AchievedDeliveries
        /// </summary>
        public Single? AchievedDeliveries
        {
            get { return GetProperty<Single?>(AchievedDeliveriesProperty); }
            set { SetProperty<Single?>(AchievedDeliveriesProperty, value); }
        }
        #endregion

        #region MinimumInventoryUnits
        /// <summary>
        /// MinimumInventoryUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MinimumInventoryUnitsProperty =
            RegisterModelProperty<Single?>(c => c.MinimumInventoryUnits, Message.PlanogramPerformanceData_MinimumInventoryUnits);
        /// <summary>
        /// MinimumInventoryUnits
        /// </summary>
        public Single? MinimumInventoryUnits
        {
            get { return GetProperty<Single?>(MinimumInventoryUnitsProperty); }
            set { SetProperty<Single?>(MinimumInventoryUnitsProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaP1Percentage
        /// <summary>
        /// MetaP1Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP1PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP1Percentage, Message.PlanogramPerformanceData_MetaP1Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP1Percentage
        /// </summary>
        public Single? MetaP1Percentage
        {
            get { return this.GetProperty<Single?>(MetaP1PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP1PercentageProperty, value); }
        }
        #endregion

        #region MetaP2Percentage
        /// <summary>
        /// MetaP2Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP2PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP2Percentage, Message.PlanogramPerformanceData_MetaP2Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP2Percentage
        /// </summary>
        public Single? MetaP2Percentage
        {
            get { return GetProperty<Single?>(MetaP2PercentageProperty); }
            set { SetProperty<Single?>(MetaP2PercentageProperty, value); }
        }
        #endregion

        #region MetaP3Percentage
        /// <summary>
        /// MetaP3Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP3PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP3Percentage, Message.PlanogramPerformanceData_MetaP3Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP3Percentage
        /// </summary>
        public Single? MetaP3Percentage
        {
            get { return GetProperty<Single?>(MetaP3PercentageProperty); }
            set { SetProperty<Single?>(MetaP3PercentageProperty, value); }
        }
        #endregion

        #region MetaP4Percentage
        /// <summary>
        /// MetaP4Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP4PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP4Percentage, Message.PlanogramPerformanceData_MetaP4Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP4Percentage
        /// </summary>
        public Single? MetaP4Percentage
        {
            get { return GetProperty<Single?>(MetaP4PercentageProperty); }
            set { SetProperty<Single?>(MetaP4PercentageProperty, value); }
        }
        #endregion

        #region MetaP5Percentage
        /// <summary>
        /// MetaP5Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP5PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP5Percentage, Message.PlanogramPerformanceData_MetaP5Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP5Percentage
        /// </summary>
        public Single? MetaP5Percentage
        {
            get { return GetProperty<Single?>(MetaP5PercentageProperty); }
            set { SetProperty<Single?>(MetaP5PercentageProperty, value); }
        }
        #endregion

        #region MetaP6Percentage
        /// <summary>
        /// MetaP6Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP6PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP6Percentage, Message.PlanogramPerformanceData_MetaP6Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP6Percentage
        /// </summary>
        public Single? MetaP6Percentage
        {
            get { return GetProperty<Single?>(MetaP6PercentageProperty); }
            set { SetProperty<Single?>(MetaP6PercentageProperty, value); }
        }
        #endregion

        #region MetaP7Percentage
        /// <summary>
        /// MetaP7Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP7PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP7Percentage, Message.PlanogramPerformanceData_MetaP7Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP7Percentage
        /// </summary>
        public Single? MetaP7Percentage
        {
            get { return GetProperty<Single?>(MetaP7PercentageProperty); }
            set { SetProperty<Single?>(MetaP7PercentageProperty, value); }
        }
        #endregion

        #region MetaP8Percentage
        /// <summary>
        /// MetaP8Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP8PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP8Percentage, Message.PlanogramPerformanceData_MetaP8Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP8Percentage
        /// </summary>
        public Single? MetaP8Percentage
        {
            get { return GetProperty<Single?>(MetaP8PercentageProperty); }
            set { SetProperty<Single?>(MetaP8PercentageProperty, value); }
        }
        #endregion

        #region MetaP9Percentage
        /// <summary>
        /// MetaP9Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP9PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP9Percentage, Message.PlanogramPerformanceData_MetaP9Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP9Percentage
        /// </summary>
        public Single? MetaP9Percentage
        {
            get { return GetProperty<Single?>(MetaP9PercentageProperty); }
            set { SetProperty<Single?>(MetaP9PercentageProperty, value); }
        }
        #endregion

        #region MetaP10Percentage
        /// <summary>
        /// MetaP10Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP10PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP10Percentage, Message.PlanogramPerformanceData_MetaP10Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP10Percentage
        /// </summary>
        public Single? MetaP10Percentage
        {
            get { return GetProperty<Single?>(MetaP10PercentageProperty); }
            set { SetProperty<Single?>(MetaP10PercentageProperty, value); }
        }
        #endregion

        #region MetaP11Percentage
        /// <summary>
        /// MetaP11Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP11PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP11Percentage, Message.PlanogramPerformanceData_MetaP11Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP11Percentage
        /// </summary>
        public Single? MetaP11Percentage
        {
            get { return GetProperty<Single?>(MetaP11PercentageProperty); }
            set { SetProperty<Single?>(MetaP11PercentageProperty, value); }
        }
        #endregion

        #region MetaP12Percentage
        /// <summary>
        /// MetaP12Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP12PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP12Percentage, Message.PlanogramPerformanceData_MetaP12Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP12Percentage
        /// </summary>
        public Single? MetaP12Percentage
        {
            get { return GetProperty<Single?>(MetaP12PercentageProperty); }
            set { SetProperty<Single?>(MetaP12PercentageProperty, value); }
        }
        #endregion

        #region MetaP13Percentage
        /// <summary>
        /// MetaP13Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP13PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP13Percentage, Message.PlanogramPerformanceData_MetaP13Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP13Percentage
        /// </summary>
        public Single? MetaP13Percentage
        {
            get { return GetProperty<Single?>(MetaP13PercentageProperty); }
            set { SetProperty<Single?>(MetaP13PercentageProperty, value); }
        }
        #endregion

        #region MetaP14Percentage
        /// <summary>
        /// MetaP14Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP14PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP14Percentage, Message.PlanogramPerformanceData_MetaP14Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP14Percentage
        /// </summary>
        public Single? MetaP14Percentage
        {
            get { return GetProperty<Single?>(MetaP14PercentageProperty); }
            set { SetProperty<Single?>(MetaP14PercentageProperty, value); }
        }
        #endregion

        #region MetaP15Percentage
        /// <summary>
        /// MetaP15Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP15PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP15Percentage, Message.PlanogramPerformanceData_MetaP15Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP15Percentage
        /// </summary>
        public Single? MetaP15Percentage
        {
            get { return GetProperty<Single?>(MetaP15PercentageProperty); }
            set { SetProperty<Single?>(MetaP15PercentageProperty, value); }
        }
        #endregion

        #region MetaP16Percentage
        /// <summary>
        /// MetaP16Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP16PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP16Percentage, Message.PlanogramPerformanceData_MetaP16Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP16Percentage
        /// </summary>
        public Single? MetaP16Percentage
        {
            get { return GetProperty<Single?>(MetaP16PercentageProperty); }
            set { SetProperty<Single?>(MetaP16PercentageProperty, value); }
        }
        #endregion

        #region MetaP17Percentage
        /// <summary>
        /// MetaP17Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP17PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP17Percentage, Message.PlanogramPerformanceData_MetaP17Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP17Percentage
        /// </summary>
        public Single? MetaP17Percentage
        {
            get { return GetProperty<Single?>(MetaP17PercentageProperty); }
            set { SetProperty<Single?>(MetaP17PercentageProperty, value); }
        }
        #endregion

        #region MetaP18Percentage
        /// <summary>
        /// MetaP18Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP18PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP18Percentage, Message.PlanogramPerformanceData_MetaP18Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP18Percentage
        /// </summary>
        public Single? MetaP18Percentage
        {
            get { return GetProperty<Single?>(MetaP18PercentageProperty); }
            set { SetProperty<Single?>(MetaP18PercentageProperty, value); }
        }
        #endregion

        #region MetaP19Percentage
        /// <summary>
        /// MetaP19Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP19PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP19Percentage, Message.PlanogramPerformanceData_MetaP19Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP19Percentage
        /// </summary>
        public Single? MetaP19Percentage
        {
            get { return GetProperty<Single?>(MetaP19PercentageProperty); }
            set { SetProperty<Single?>(MetaP19PercentageProperty, value); }
        }
        #endregion

        #region MetaP20Percentage
        /// <summary>
        /// MetaP20Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP20PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP20Percentage, Message.PlanogramPerformanceData_MetaP20Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaP20Percentage
        /// </summary>
        public Single? MetaP20Percentage
        {
            get { return GetProperty<Single?>(MetaP20PercentageProperty); }
            set { SetProperty<Single?>(MetaP20PercentageProperty, value); }
        }
        #endregion

        #region Calculated Percentages
        #region MetaCP1Percentage
        /// <summary>
        /// MetaCP1Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP1PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP1Percentage, Message.PlanogramPerformanceData_MetaCP1Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP1Percentage
        /// </summary>
        public Single? MetaCP1Percentage
        {
            get { return this.GetProperty<Single?>(MetaCP1PercentageProperty); }
            set { this.SetProperty<Single?>(MetaCP1PercentageProperty, value); }
        }
        #endregion

        #region MetaCP2Percentage
        /// <summary>
        /// MetaCP2Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP2PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP2Percentage, Message.PlanogramPerformanceData_MetaCP2Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP2Percentage
        /// </summary>
        public Single? MetaCP2Percentage
        {
            get { return GetProperty<Single?>(MetaCP2PercentageProperty); }
            set { SetProperty<Single?>(MetaCP2PercentageProperty, value); }
        }
        #endregion

        #region MetaCP3Percentage
        /// <summary>
        /// MetaCP3Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP3PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP3Percentage, Message.PlanogramPerformanceData_MetaCP3Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP3Percentage
        /// </summary>
        public Single? MetaCP3Percentage
        {
            get { return GetProperty<Single?>(MetaCP3PercentageProperty); }
            set { SetProperty<Single?>(MetaCP3PercentageProperty, value); }
        }
        #endregion

        #region MetaCP4Percentage
        /// <summary>
        /// MetaCP4Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP4PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP4Percentage, Message.PlanogramPerformanceData_MetaCP4Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP4Percentage
        /// </summary>
        public Single? MetaCP4Percentage
        {
            get { return GetProperty<Single?>(MetaCP4PercentageProperty); }
            set { SetProperty<Single?>(MetaCP4PercentageProperty, value); }
        }
        #endregion

        #region MetaCP5Percentage
        /// <summary>
        /// MetaCP5Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP5PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP5Percentage, Message.PlanogramPerformanceData_MetaCP5Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP5Percentage
        /// </summary>
        public Single? MetaCP5Percentage
        {
            get { return GetProperty<Single?>(MetaCP5PercentageProperty); }
            set { SetProperty<Single?>(MetaCP5PercentageProperty, value); }
        }
        #endregion

        #region MetaCP6Percentage
        /// <summary>
        /// MetaCP6Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP6PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP6Percentage, Message.PlanogramPerformanceData_MetaCP6Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP6Percentage
        /// </summary>
        public Single? MetaCP6Percentage
        {
            get { return GetProperty<Single?>(MetaCP6PercentageProperty); }
            set { SetProperty<Single?>(MetaCP6PercentageProperty, value); }
        }
        #endregion

        #region MetaCP7Percentage
        /// <summary>
        /// MetaCP7Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP7PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP7Percentage, Message.PlanogramPerformanceData_MetaCP7Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP7Percentage
        /// </summary>
        public Single? MetaCP7Percentage
        {
            get { return GetProperty<Single?>(MetaCP7PercentageProperty); }
            set { SetProperty<Single?>(MetaCP7PercentageProperty, value); }
        }
        #endregion

        #region MetaCP8Percentage
        /// <summary>
        /// MetaCP8Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP8PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP8Percentage, Message.PlanogramPerformanceData_MetaCP8Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP8Percentage
        /// </summary>
        public Single? MetaCP8Percentage
        {
            get { return GetProperty<Single?>(MetaCP8PercentageProperty); }
            set { SetProperty<Single?>(MetaCP8PercentageProperty, value); }
        }
        #endregion

        #region MetaCP9Percentage
        /// <summary>
        /// MetaCP9Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP9PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP9Percentage, Message.PlanogramPerformanceData_MetaCP9Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP9Percentage
        /// </summary>
        public Single? MetaCP9Percentage
        {
            get { return GetProperty<Single?>(MetaCP9PercentageProperty); }
            set { SetProperty<Single?>(MetaCP9PercentageProperty, value); }
        }
        #endregion

        #region MetaCP10Percentage
        /// <summary>
        /// MetaCP10Percentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaCP10PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaCP10Percentage, Message.PlanogramPerformanceData_MetaCP10Percentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// MetaCP10Percentage
        /// </summary>
        public Single? MetaCP10Percentage
        {
            get { return GetProperty<Single?>(MetaCP10PercentageProperty); }
            set { SetProperty<Single?>(MetaCP10PercentageProperty, value); }
        }
        #endregion
        #endregion

        #region MetaP1Rank
        /// <summary>
        /// MetaP1Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP1RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP1Rank, Message.PlanogramPerformanceData_MetaP1Rank);
        /// <summary>
        /// MetaP1Rank
        /// </summary>
        public Int32? MetaP1Rank
        {
            get { return GetProperty<Int32?>(MetaP1RankProperty); }
            set { SetProperty<Int32?>(MetaP1RankProperty, value); }
        }
        #endregion

        #region MetaP2Rank
        /// <summary>
        /// MetaP2Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP2RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP2Rank, Message.PlanogramPerformanceData_MetaP2Rank);
        /// <summary>
        /// MetaP2Rank
        /// </summary>
        public Int32? MetaP2Rank
        {
            get { return GetProperty<Int32?>(MetaP2RankProperty); }
            set { SetProperty<Int32?>(MetaP2RankProperty, value); }
        }
        #endregion

        #region MetaP3Rank
        /// <summary>
        /// MetaP3Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP3RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP3Rank, Message.PlanogramPerformanceData_MetaP3Rank);
        /// <summary>
        /// MetaP3Rank
        /// </summary>
        public Int32? MetaP3Rank
        {
            get { return GetProperty<Int32?>(MetaP3RankProperty); }
            set { SetProperty<Int32?>(MetaP3RankProperty, value); }
        }
        #endregion

        #region MetaP4Rank
        /// <summary>
        /// MetaP4Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP4RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP4Rank, Message.PlanogramPerformanceData_MetaP4Rank);
        /// <summary>
        /// MetaP4Rank
        /// </summary>
        public Int32? MetaP4Rank
        {
            get { return GetProperty<Int32?>(MetaP4RankProperty); }
            set { SetProperty<Int32?>(MetaP4RankProperty, value); }
        }
        #endregion

        #region MetaP5Rank
        /// <summary>
        /// MetaP5Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP5RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP5Rank, Message.PlanogramPerformanceData_MetaP5Rank);
        /// <summary>
        /// MetaP5Rank
        /// </summary>
        public Int32? MetaP5Rank
        {
            get { return GetProperty<Int32?>(MetaP5RankProperty); }
            set { SetProperty<Int32?>(MetaP5RankProperty, value); }
        }
        #endregion

        #region MetaP6Rank
        /// <summary>
        /// MetaP6Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP6RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP6Rank, Message.PlanogramPerformanceData_MetaP6Rank);
        /// <summary>
        /// MetaP6Rank
        /// </summary>
        public Int32? MetaP6Rank
        {
            get { return GetProperty<Int32?>(MetaP6RankProperty); }
            set { SetProperty<Int32?>(MetaP6RankProperty, value); }
        }
        #endregion

        #region MetaP7Rank
        /// <summary>
        /// MetaP7Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP7RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP7Rank, Message.PlanogramPerformanceData_MetaP7Rank);
        /// <summary>
        /// MetaP7Rank
        /// </summary>
        public Int32? MetaP7Rank
        {
            get { return GetProperty<Int32?>(MetaP7RankProperty); }
            set { SetProperty<Int32?>(MetaP7RankProperty, value); }
        }
        #endregion

        #region MetaP8Rank
        /// <summary>
        /// MetaP8Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP8RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP8Rank, Message.PlanogramPerformanceData_MetaP8Rank);
        /// <summary>
        /// MetaP8Rank
        /// </summary>
        public Int32? MetaP8Rank
        {
            get { return GetProperty<Int32?>(MetaP8RankProperty); }
            set { SetProperty<Int32?>(MetaP8RankProperty, value); }
        }
        #endregion

        #region MetaP9Rank
        /// <summary>
        /// MetaP9Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP9RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP9Rank, Message.PlanogramPerformanceData_MetaP9Rank);
        /// <summary>
        /// MetaP9Rank
        /// </summary>
        public Int32? MetaP9Rank
        {
            get { return GetProperty<Int32?>(MetaP9RankProperty); }
            set { SetProperty<Int32?>(MetaP9RankProperty, value); }
        }
        #endregion

        #region MetaP10Rank
        /// <summary>
        /// MetaP10Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP10RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP10Rank, Message.PlanogramPerformanceData_MetaP10Rank);
        /// <summary>
        /// MetaP10Rank
        /// </summary>
        public Int32? MetaP10Rank
        {
            get { return GetProperty<Int32?>(MetaP10RankProperty); }
            set { SetProperty<Int32?>(MetaP10RankProperty, value); }
        }
        #endregion

        #region MetaP11Rank
        /// <summary>
        /// MetaP11Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP11RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP11Rank, Message.PlanogramPerformanceData_MetaP11Rank);
        /// <summary>
        /// MetaP11Rank
        /// </summary>
        public Int32? MetaP11Rank
        {
            get { return GetProperty<Int32?>(MetaP11RankProperty); }
            set { SetProperty<Int32?>(MetaP11RankProperty, value); }
        }
        #endregion

        #region MetaP12Rank
        /// <summary>
        /// MetaP12Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP12RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP12Rank, Message.PlanogramPerformanceData_MetaP12Rank);
        /// <summary>
        /// MetaP12Rank
        /// </summary>
        public Int32? MetaP12Rank
        {
            get { return GetProperty<Int32?>(MetaP12RankProperty); }
            set { SetProperty<Int32?>(MetaP12RankProperty, value); }
        }
        #endregion

        #region MetaP13Rank
        /// <summary>
        /// MetaP13Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP13RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP13Rank, Message.PlanogramPerformanceData_MetaP13Rank);
        /// <summary>
        /// MetaP13Rank
        /// </summary>
        public Int32? MetaP13Rank
        {
            get { return GetProperty<Int32?>(MetaP13RankProperty); }
            set { SetProperty<Int32?>(MetaP13RankProperty, value); }
        }
        #endregion

        #region MetaP14Rank
        /// <summary>
        /// MetaP14Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP14RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP14Rank, Message.PlanogramPerformanceData_MetaP14Rank);
        /// <summary>
        /// MetaP14Rank
        /// </summary>
        public Int32? MetaP14Rank
        {
            get { return GetProperty<Int32?>(MetaP14RankProperty); }
            set { SetProperty<Int32?>(MetaP14RankProperty, value); }
        }
        #endregion

        #region MetaP15Rank
        /// <summary>
        /// MetaP15Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP15RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP15Rank, Message.PlanogramPerformanceData_MetaP15Rank);
        /// <summary>
        /// MetaP15Rank
        /// </summary>
        public Int32? MetaP15Rank
        {
            get { return GetProperty<Int32?>(MetaP15RankProperty); }
            set { SetProperty<Int32?>(MetaP15RankProperty, value); }
        }
        #endregion

        #region MetaP16Rank
        /// <summary>
        /// MetaP16Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP16RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP16Rank, Message.PlanogramPerformanceData_MetaP16Rank);
        /// <summary>
        /// MetaP16Rank
        /// </summary>
        public Int32? MetaP16Rank
        {
            get { return GetProperty<Int32?>(MetaP16RankProperty); }
            set { SetProperty<Int32?>(MetaP16RankProperty, value); }
        }
        #endregion

        #region MetaP17Rank
        /// <summary>
        /// MetaP17Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP17RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP17Rank, Message.PlanogramPerformanceData_MetaP17Rank);
        /// <summary>
        /// MetaP17Rank
        /// </summary>
        public Int32? MetaP17Rank
        {
            get { return GetProperty<Int32?>(MetaP17RankProperty); }
            set { SetProperty<Int32?>(MetaP17RankProperty, value); }
        }
        #endregion

        #region MetaP18Rank
        /// <summary>
        /// MetaP18Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP18RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP18Rank, Message.PlanogramPerformanceData_MetaP18Rank);
        /// <summary>
        /// MetaP18Rank
        /// </summary>
        public Int32? MetaP18Rank
        {
            get { return GetProperty<Int32?>(MetaP18RankProperty); }
            set { SetProperty<Int32?>(MetaP18RankProperty, value); }
        }
        #endregion

        #region MetaP19Rank
        /// <summary>
        /// MetaP19Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP19RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP19Rank, Message.PlanogramPerformanceData_MetaP19Rank);
        /// <summary>
        /// MetaP19Rank
        /// </summary>
        public Int32? MetaP19Rank
        {
            get { return GetProperty<Int32?>(MetaP19RankProperty); }
            set { SetProperty<Int32?>(MetaP19RankProperty, value); }
        }
        #endregion

        #region MetaP20Rank
        /// <summary>
        /// MetaP20Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaP20RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaP20Rank, Message.PlanogramPerformanceData_MetaP20Rank);
        /// <summary>
        /// MetaP20Rank
        /// </summary>
        public Int32? MetaP20Rank
        {
            get { return GetProperty<Int32?>(MetaP20RankProperty); }
            set { SetProperty<Int32?>(MetaP20RankProperty, value); }
        }

        #endregion

        #region Calculated Ranks
        #region MetaCP1Rank
        /// <summary>
        /// MetaCP1Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP1RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP1Rank, Message.PlanogramPerformanceData_MetaCP1Rank);
        /// <summary>
        /// MetaCP1Rank
        /// </summary>
        public Int32? MetaCP1Rank
        {
            get { return GetProperty<Int32?>(MetaCP1RankProperty); }
            set { SetProperty<Int32?>(MetaCP1RankProperty, value); }
        }
        #endregion

        #region MetaCP2Rank
        /// <summary>
        /// MetaCP2Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP2RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP2Rank, Message.PlanogramPerformanceData_MetaCP2Rank);
        /// <summary>
        /// MetaCP2Rank
        /// </summary>
        public Int32? MetaCP2Rank
        {
            get { return GetProperty<Int32?>(MetaCP2RankProperty); }
            set { SetProperty<Int32?>(MetaCP2RankProperty, value); }
        }
        #endregion

        #region MetaCP3Rank
        /// <summary>
        /// MetaCP3Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP3RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP3Rank, Message.PlanogramPerformanceData_MetaCP3Rank);
        /// <summary>
        /// MetaCP3Rank
        /// </summary>
        public Int32? MetaCP3Rank
        {
            get { return GetProperty<Int32?>(MetaCP3RankProperty); }
            set { SetProperty<Int32?>(MetaCP3RankProperty, value); }
        }
        #endregion

        #region MetaCP4Rank
        /// <summary>
        /// MetaCP4Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP4RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP4Rank, Message.PlanogramPerformanceData_MetaCP4Rank);
        /// <summary>
        /// MetaCP4Rank
        /// </summary>
        public Int32? MetaCP4Rank
        {
            get { return GetProperty<Int32?>(MetaCP4RankProperty); }
            set { SetProperty<Int32?>(MetaCP4RankProperty, value); }
        }
        #endregion

        #region MetaCP5Rank
        /// <summary>
        /// MetaCP5Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP5RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP5Rank, Message.PlanogramPerformanceData_MetaCP5Rank);
        /// <summary>
        /// MetaCP5Rank
        /// </summary>
        public Int32? MetaCP5Rank
        {
            get { return GetProperty<Int32?>(MetaCP5RankProperty); }
            set { SetProperty<Int32?>(MetaCP5RankProperty, value); }
        }
        #endregion

        #region MetaCP6Rank
        /// <summary>
        /// MetaCP6Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP6RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP6Rank, Message.PlanogramPerformanceData_MetaCP6Rank);
        /// <summary>
        /// MetaCP6Rank
        /// </summary>
        public Int32? MetaCP6Rank
        {
            get { return GetProperty<Int32?>(MetaCP6RankProperty); }
            set { SetProperty<Int32?>(MetaCP6RankProperty, value); }
        }
        #endregion

        #region MetaCP7Rank
        /// <summary>
        /// MetaCP7Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP7RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP7Rank, Message.PlanogramPerformanceData_MetaCP7Rank);
        /// <summary>
        /// MetaCP7Rank
        /// </summary>
        public Int32? MetaCP7Rank
        {
            get { return GetProperty<Int32?>(MetaCP7RankProperty); }
            set { SetProperty<Int32?>(MetaCP7RankProperty, value); }
        }
        #endregion

        #region MetaCP8Rank
        /// <summary>
        /// MetaCP8Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP8RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP8Rank, Message.PlanogramPerformanceData_MetaCP8Rank);
        /// <summary>
        /// MetaCP8Rank
        /// </summary>
        public Int32? MetaCP8Rank
        {
            get { return GetProperty<Int32?>(MetaCP8RankProperty); }
            set { SetProperty<Int32?>(MetaCP8RankProperty, value); }
        }
        #endregion

        #region MetaCP9Rank
        /// <summary>
        /// MetaCP9Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP9RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP9Rank, Message.PlanogramPerformanceData_MetaCP9Rank);
        /// <summary>
        /// MetaCP9Rank
        /// </summary>
        public Int32? MetaCP9Rank
        {
            get { return GetProperty<Int32?>(MetaCP9RankProperty); }
            set { SetProperty<Int32?>(MetaCP9RankProperty, value); }
        }
        #endregion

        #region MetaCP10Rank
        /// <summary>
        /// MetaCP10Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCP10RankProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCP10Rank, Message.PlanogramPerformanceData_MetaCP10Rank);
        /// <summary>
        /// MetaCP10Rank
        /// </summary>
        public Int32? MetaCP10Rank
        {
            get { return GetProperty<Int32?>(MetaCP10RankProperty); }
            set { SetProperty<Int32?>(MetaCP10RankProperty, value); }
        }
        #endregion
        #endregion
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(PlanogramProductIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceData NewPlanogramPerformanceData()
        {
            var item = new PlanogramPerformanceData();
            item.Create(null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceData NewPlanogramPerformanceData(PlanogramProduct planogramProduct)
        {
            var item = new PlanogramPerformanceData();
            item.Create(planogramProduct);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramProduct product)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            if (product != null) this.LoadProperty<Object>(PlanogramProductIdProperty, product.Id);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramProductId
            Object planogramProductId = context.ResolveId<PlanogramProduct>(this.ReadProperty<Object>(PlanogramProductIdProperty));
            if (planogramProductId != null) this.LoadProperty<Object>(PlanogramProductIdProperty, planogramProductId);
        }

        /// <summary>
        /// Returns the name of the metric for the given id.
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        public String GetMetricName(Int32 metricId)
        {
            PlanogramPerformance performance = this.Parent;
            if (performance == null) return null;
            if (performance.Metrics == null) return null;

            var metric = performance.Metrics.FirstOrDefault(m => m.MetricId == metricId);
            if (metric == null) return null;

            return metric.Name;
        }

        /// <summary>
        /// Returns true if the metric of the given id 
        /// is in use
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        public Boolean IsMetricInUse(Int32 metricId)
        {
            PlanogramPerformance performance = this.Parent;
            if (performance == null) return false;
            if (performance.Metrics == null) return false;

            var metric = performance.Metrics.FirstOrDefault(m => m.MetricId == metricId);

            return (metric != null);
        }


        /// <summary>
        /// Calculates the Inventory values
        /// </summary>
        public void UpdateInventoryValues()
        {
            PlanogramPerformance performance = this.Parent;
            if (performance.Metrics == null || performance.Metrics.Count == 0) return;

            //get the product & positions for this data.
            Planogram planogram = performance.Parent;
            if (planogram == null) return;

            //Attempt to retrieve the correct metric based on the inventory type of the planogram.
            PlanogramPerformanceMetric metricForGivenInventoryType = getPerformanceMetricForGivenInventoryType(performance, planogram.Inventory);
            if (metricForGivenInventoryType == null) return;

            Object productId = this.PlanogramProductId;
            PlanogramProduct product = planogram.Products.FirstOrDefault(p => Object.Equals(p.Id, productId));
            if (product == null) return;

            IEnumerable<PlanogramPosition> planogramPositions = product.GetPlanogramPositions();
            Int32 totalUnits = planogramPositions.Sum(p => p.TotalUnits);
            
            Single? data = GetPerformanceData(metricForGivenInventoryType.MetricId);
            if (data.HasValue)
            {
                if (planogram.Inventory.DaysOfPerformance != 0)
                {
                    //UnitsSoldPerDay
                    Single unitsPerDay = 0;
                    //V8-29682 : Only factor in days of performance if the reg sales units aggregation is sum
                    if (metricForGivenInventoryType.AggregationType.Equals(AggregationType.Sum))
                    {
                        unitsPerDay = data.Value / planogram.Inventory.DaysOfPerformance;
                    }
                    else
                    {
                        //Use raw performance
                        unitsPerDay = data.Value / 7; //V8-30467 : Raw value is weekly average, so divide by 7.
                    }
                    this.UnitsSoldPerDay = (Single)(Math.Round(unitsPerDay, MathHelper.DefaultPrecision));

                    if (unitsPerDay != 0)
                    {
                        // Achieved Dos
                        this.AchievedDos = (Single)(Math.Round(
                            totalUnits / unitsPerDay,
                            MathHelper.DefaultPrecision));

                        // Achieved Deliveries
                        if (product.DeliveryFrequencyDays.HasValue && product.DeliveryFrequencyDays != 0)
                        {
                            this.AchievedDeliveries =
                                (Single)(Math.Round(
                                totalUnits / (unitsPerDay * product.DeliveryFrequencyDays.Value),
                                MathHelper.DefaultPrecision));
                        }
                        else
                        {
                            this.AchievedDeliveries = 0;
                        }

                        // Achieved Shelf Life
                        if (product.ShelfLife != 0)
                        {
                            this.AchievedShelfLife =
                                (Single)(Math.Round(
                                totalUnits / (unitsPerDay * (Single)product.ShelfLife),
                                MathHelper.DefaultPrecision));
                        }
                        else
                        {
                            this.AchievedShelfLife = 0;
                        }
                    }
                    else
                    {
                        this.AchievedDos = 0;
                        this.AchievedDeliveries = 0;
                        this.AchievedShelfLife = 0;
                    }
                }
                else
                {
                    this.UnitsSoldPerDay = 0;
                    this.AchievedDos = 0;
                    this.AchievedDeliveries = 0;
                    this.AchievedShelfLife = 0;
                }

            }
            
            //Minimum inventory units
            //Calculate unit values for the minimum inventory value types
            var inventoryProfileUnits = new[] { 
                (Single)(Math.Round(planogram.Inventory.MinCasePacks * 
                    product.CasePackUnits, MathHelper.DefaultPrecision)),
                (Single)(Math.Round(planogram.Inventory.MinDeliveries * 
                    ((this.UnitsSoldPerDay.HasValue ? this.UnitsSoldPerDay.Value : 0) * (product.DeliveryFrequencyDays.HasValue ? product.DeliveryFrequencyDays.Value : 0)), MathHelper.DefaultPrecision)), 
                (Single)(Math.Round(planogram.Inventory.MinDos * 
                    (this.UnitsSoldPerDay.HasValue ? this.UnitsSoldPerDay.Value : 0), MathHelper.DefaultPrecision)), 
                (Single)(Math.Round(planogram.Inventory.MinShelfLife * 
                    ((this.UnitsSoldPerDay.HasValue ? this.UnitsSoldPerDay.Value : 0) * product.ShelfLife), MathHelper.DefaultPrecision)), 
            };
            //Select the maximum value as this is the minimum amount to meet the inventory profiles requirement
            this.MinimumInventoryUnits = inventoryProfileUnits.Max();

            // Achieved Case Packs
            if (product.CasePackUnits != 0)
            {
                this.AchievedCasePacks = (Single)(Math.Round(
                    totalUnits / (Double)product.CasePackUnits,
                    MathHelper.DefaultPrecision));
            }
            else
            {
                this.AchievedCasePacks = 0;
            }
        }


        /// <summary>
        /// Gets the metric with the type that matches the gievn planogram inventory type
        /// </summary>
        /// <returns></returns>
        private PlanogramPerformanceMetric getPerformanceMetricForGivenInventoryType(PlanogramPerformance performance, PlanogramInventory planogramInventory)
        {
            switch (planogramInventory.InventoryMetricType)
            {
                case PlanogramInventoryMetricType.RegularSalesUnits:
                    return performance.Metrics.FirstOrDefault(m => m.SpecialType == MetricSpecialType.RegularSalesUnits);
                    break;
                case PlanogramInventoryMetricType.PromotionalSalesUnits:
                    return performance.Metrics.FirstOrDefault(m => m.SpecialType == MetricSpecialType.PromotionalSalesUnits);
                    break;
                default:
                    return null;
                    break;
            }
        }

        /// <summary>
        /// Gets the performance data depending on the metric
        /// </summary>
        /// <param name="metricId"></param>
        /// <returns></returns>
        public Single? GetPerformanceData(Byte metricId)
        {
            return GetValue((PerformanceSourceMetricColumnNumber)metricId);
        }

        /// <summary>
        ///     Set the performance data <paramref name="value"/> for the given <paramref name="metricId"/>.
        /// </summary>
        /// <param name="metricId">The Id of the metric to set the value for.</param>
        /// <param name="value">The value (or null) to set the metric with the given Id to.</param>
        public void SetPerformanceData(Byte metricId, Single? value)
        {
            SetValue(value, (PerformanceSourceMetricColumnNumber)metricId);
        }

        /// <summary>
        /// Sets all performance data values to null
        /// </summary>
        public void ClearAllValues()
        {
            foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
            {
                SetValue(null, column);
            }
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePropertyInfos(Boolean includeMetadata)
        {
            yield return P1Property;
            yield return P2Property;
            yield return P3Property;
            yield return P4Property;
            yield return P5Property;
            yield return P6Property;
            yield return P7Property;
            yield return P8Property;
            yield return P9Property;
            yield return P10Property;
            yield return P11Property;
            yield return P12Property;
            yield return P13Property;
            yield return P14Property;
            yield return P15Property;
            yield return P16Property;
            yield return P17Property;
            yield return P18Property;
            yield return P19Property;
            yield return P20Property;
            yield return CP1Property;
            yield return CP2Property;
            yield return CP3Property;
            yield return CP4Property;
            yield return CP5Property;
            yield return CP6Property;
            yield return CP7Property;
            yield return CP8Property;
            yield return CP9Property;
            yield return CP10Property;

            if (includeMetadata)
            {
                yield return UnitsSoldPerDayProperty;
                yield return AchievedCasePacksProperty;
                yield return AchievedDosProperty;
                yield return AchievedShelfLifeProperty;
                yield return AchievedDeliveriesProperty;
                yield return MinimumInventoryUnitsProperty;
                yield return MetaP1PercentageProperty;
                yield return MetaP2PercentageProperty;
                yield return MetaP3PercentageProperty;
                yield return MetaP4PercentageProperty;
                yield return MetaP5PercentageProperty;
                yield return MetaP6PercentageProperty;
                yield return MetaP7PercentageProperty;
                yield return MetaP8PercentageProperty;
                yield return MetaP9PercentageProperty;
                yield return MetaP10PercentageProperty;
                yield return MetaP11PercentageProperty;
                yield return MetaP12PercentageProperty;
                yield return MetaP13PercentageProperty;
                yield return MetaP14PercentageProperty;
                yield return MetaP15PercentageProperty;
                yield return MetaP16PercentageProperty;
                yield return MetaP17PercentageProperty;
                yield return MetaP18PercentageProperty;
                yield return MetaP19PercentageProperty;
                yield return MetaP20PercentageProperty;
                yield return MetaCP1PercentageProperty;
                yield return MetaCP2PercentageProperty;
                yield return MetaCP3PercentageProperty;
                yield return MetaCP4PercentageProperty;
                yield return MetaCP5PercentageProperty;
                yield return MetaCP6PercentageProperty;
                yield return MetaCP7PercentageProperty;
                yield return MetaCP8PercentageProperty;
                yield return MetaCP9PercentageProperty;
                yield return MetaCP10PercentageProperty;

                yield return MetaP1RankProperty;
                yield return MetaP2RankProperty;
                yield return MetaP3RankProperty;
                yield return MetaP4RankProperty;
                yield return MetaP5RankProperty;
                yield return MetaP6RankProperty;
                yield return MetaP7RankProperty;
                yield return MetaP8RankProperty;
                yield return MetaP9RankProperty;
                yield return MetaP10RankProperty;
                yield return MetaP11RankProperty;
                yield return MetaP12RankProperty;
                yield return MetaP13RankProperty;
                yield return MetaP14RankProperty;
                yield return MetaP15RankProperty;
                yield return MetaP16RankProperty;
                yield return MetaP17RankProperty;
                yield return MetaP18RankProperty;
                yield return MetaP19RankProperty;
                yield return MetaP20RankProperty;
                yield return MetaCP1RankProperty;
                yield return MetaCP2RankProperty;
                yield return MetaCP3RankProperty;
                yield return MetaCP4RankProperty;
                yield return MetaCP5RankProperty;
                yield return MetaCP6RankProperty;
                yield return MetaCP7RankProperty;
                yield return MetaCP8RankProperty;
                yield return MetaCP9RankProperty;
                yield return MetaCP10RankProperty;
            }
        }

        private static void GetMetricProperties(Int32 metricNumber, out IModelPropertyInfo propertyInfo, out IModelPropertyInfo metaPropertyInfo, out IModelPropertyInfo metaRankPropertyInfo)
        {
            //initialise the propertyinfos
            propertyInfo = null;
            metaPropertyInfo = null;
            metaRankPropertyInfo = null;

            switch (metricNumber)
            {
                //sandard metrics
                case 1:
                    propertyInfo = P1Property;
                    metaPropertyInfo = MetaP1PercentageProperty;
                    metaRankPropertyInfo = MetaP1RankProperty;
                    break;
                case 2:
                    propertyInfo = P2Property;
                    metaPropertyInfo = MetaP2PercentageProperty;
                    metaRankPropertyInfo = MetaP2RankProperty;
                    break;
                case 3:
                    propertyInfo = P3Property;
                    metaPropertyInfo = MetaP3PercentageProperty;
                    metaRankPropertyInfo = MetaP3RankProperty;
                    break;
                case 4:
                    propertyInfo = P4Property;
                    metaPropertyInfo = MetaP4PercentageProperty;
                    metaRankPropertyInfo = MetaP4RankProperty;
                    break;
                case 5:
                    propertyInfo = P5Property;
                    metaPropertyInfo = MetaP5PercentageProperty;
                    metaRankPropertyInfo = MetaP5RankProperty;
                    break;
                case 6:
                    propertyInfo = P6Property;
                    metaPropertyInfo = MetaP6PercentageProperty;
                    metaRankPropertyInfo = MetaP6RankProperty;
                    break;
                case 7:
                    propertyInfo = P7Property;
                    metaPropertyInfo = MetaP7PercentageProperty;
                    metaRankPropertyInfo = MetaP7RankProperty;
                    break;
                case 8:
                    propertyInfo = P8Property;
                    metaPropertyInfo = MetaP8PercentageProperty;
                    metaRankPropertyInfo = MetaP8RankProperty;
                    break;
                case 9:
                    propertyInfo = P9Property;
                    metaPropertyInfo = MetaP9PercentageProperty;
                    metaRankPropertyInfo = MetaP9RankProperty;
                    break;
                case 10:
                    propertyInfo = P10Property;
                    metaPropertyInfo = MetaP10PercentageProperty;
                    metaRankPropertyInfo = MetaP10RankProperty;
                    break;
                case 11:
                    propertyInfo = P11Property;
                    metaPropertyInfo = MetaP11PercentageProperty;
                    metaRankPropertyInfo = MetaP11RankProperty;
                    break;
                case 12:
                    propertyInfo = P12Property;
                    metaPropertyInfo = MetaP12PercentageProperty;
                    metaRankPropertyInfo = MetaP12RankProperty;
                    break;
                case 13:
                    propertyInfo = P13Property;
                    metaPropertyInfo = MetaP13PercentageProperty;
                    metaRankPropertyInfo = MetaP13RankProperty;
                    break;
                case 14:
                    propertyInfo = P14Property;
                    metaPropertyInfo = MetaP14PercentageProperty;
                    metaRankPropertyInfo = MetaP14RankProperty;
                    break;
                case 15:
                    propertyInfo = P15Property;
                    metaPropertyInfo = MetaP15PercentageProperty;
                    metaRankPropertyInfo = MetaP15RankProperty;
                    break;
                case 16:
                    propertyInfo = P16Property;
                    metaPropertyInfo = MetaP16PercentageProperty;
                    metaRankPropertyInfo = MetaP16RankProperty;
                    break;
                case 17:
                    propertyInfo = P17Property;
                    metaPropertyInfo = MetaP17PercentageProperty;
                    metaRankPropertyInfo = MetaP17RankProperty;
                    break;
                case 18:
                    propertyInfo = P18Property;
                    metaPropertyInfo = MetaP18PercentageProperty;
                    metaRankPropertyInfo = MetaP18RankProperty;
                    break;
                case 19:
                    propertyInfo = P19Property;
                    metaPropertyInfo = MetaP19PercentageProperty;
                    metaRankPropertyInfo = MetaP19RankProperty;
                    break;
                case 20:
                    propertyInfo = P20Property;
                    metaPropertyInfo = MetaP20PercentageProperty;
                    metaRankPropertyInfo = MetaP20RankProperty;
                    break;
                //calculated metrics
                case 21:
                    propertyInfo = CP1Property;
                    metaPropertyInfo = MetaCP1PercentageProperty;
                    metaRankPropertyInfo = MetaCP1RankProperty;
                    break;
                case 22:
                    propertyInfo = CP2Property;
                    metaPropertyInfo = MetaCP2PercentageProperty;
                    metaRankPropertyInfo = MetaCP2RankProperty;
                    break;
                case 23:
                    propertyInfo = CP3Property;
                    metaPropertyInfo = MetaCP3PercentageProperty;
                    metaRankPropertyInfo = MetaCP3RankProperty;
                    break;
                case 24:
                    propertyInfo = CP4Property;
                    metaPropertyInfo = MetaCP4PercentageProperty;
                    metaRankPropertyInfo = MetaCP4RankProperty;
                    break;
                case 25:
                    propertyInfo = CP5Property;
                    metaPropertyInfo = MetaCP5PercentageProperty;
                    metaRankPropertyInfo = MetaCP5RankProperty;
                    break;
                case 26:
                    propertyInfo = CP6Property;
                    metaPropertyInfo = MetaCP6PercentageProperty;
                    metaRankPropertyInfo = MetaCP6RankProperty;
                    break;
                case 27:
                    propertyInfo = CP7Property;
                    metaPropertyInfo = MetaCP7PercentageProperty;
                    metaRankPropertyInfo = MetaCP7RankProperty;
                    break;
                case 28:
                    propertyInfo = CP8Property;
                    metaPropertyInfo = MetaCP8PercentageProperty;
                    metaRankPropertyInfo = MetaCP8RankProperty;
                    break;
                case 29:
                    propertyInfo = CP9Property;
                    metaPropertyInfo = MetaCP9PercentageProperty;
                    metaRankPropertyInfo = MetaCP9RankProperty;
                    break;
                case 30:
                    propertyInfo = CP10Property;
                    metaPropertyInfo = MetaCP10PercentageProperty;
                    metaRankPropertyInfo = MetaCP10RankProperty;
                    break;
            }
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user based on the metrics of
        /// the planogram.
        /// </summary>
        public static IEnumerable<ObjectFieldInfo> EnumeratePlanogramDisplayableFieldInfos(PlanogramPerformance context, Boolean includeMetadata)
        {
            Type type = typeof(PlanogramProduct);
            String typeFriendly = PlanogramProduct.FriendlyName;
            String performanceDataGroup = Message.PlanogramProduct_PropertyGroup_Performance;

            if (context == null || context.Metrics == null)
            {
                foreach (IModelPropertyInfo propertyInfo in EnumerateDisplayablePropertyInfos(includeMetadata))
                {
                    yield return ObjectFieldInfo.NewObjectFieldInfo(
                      type, typeFriendly, PlanogramPerformance.PerformanceDataProperty.Name + "." + propertyInfo.Name,
                             propertyInfo.FriendlyName, propertyInfo.Type, propertyInfo.DisplayType, /*isSpecial*/false, performanceDataGroup);


                }
                yield break;
            }
            else
            {
                //cycle through setting metric names properly
                for (Int32 i = 1; i <= Constants.MaximumMetricsPerPerformanceSource; i++)
                {
                    IModelPropertyInfo propertyInfo = null;
                    IModelPropertyInfo metaPropertyInfo = null;
                    IModelPropertyInfo metaRankPropertyInfo = null;

                    //Get metric properties
                    GetMetricProperties(i, out propertyInfo, out metaPropertyInfo, out  metaRankPropertyInfo);

                    PlanogramPerformanceMetric metric = context.Metrics.FirstOrDefault(m => m.MetricId == i);
                    if (metric != null)
                    {
                        ModelPropertyDisplayType displayType = ModelPropertyDisplayType.None;
                        switch (metric.Type)
                        {
                            case MetricType.Currency:
                                displayType = ModelPropertyDisplayType.Currency;
                                break;

                            case MetricType.Decimal:
                                displayType = ModelPropertyDisplayType.None;
                                break;

                            case MetricType.Integer:
                                displayType = ModelPropertyDisplayType.None;
                                break;
                        }

                        yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                            PlanogramPerformance.PerformanceDataProperty.Name + "." + propertyInfo.Name,
                                   String.Format("{0} - {1}", propertyInfo.FriendlyName, metric.Name),
                                   propertyInfo.Type, displayType,  /*isSpecial*/false, performanceDataGroup);


                        if (includeMetadata)
                        {
                            ObjectFieldInfo percentField = 
                                 ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                                PlanogramPerformance.PerformanceDataProperty.Name + "." + metaPropertyInfo.Name,
                                 String.Format("{0} - {1}", metaPropertyInfo.FriendlyName, metric.Name),
                                   metaPropertyInfo.Type, metaPropertyInfo.DisplayType, /*isSpecial*/false, performanceDataGroup);
                            percentField.IsReadOnly = true;
                            yield return percentField;

                            ObjectFieldInfo rankField = 
                                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                           PlanogramPerformance.PerformanceDataProperty.Name + "." + metaRankPropertyInfo.Name,
                                String.Format("{0} - {1}", metaRankPropertyInfo.FriendlyName, metric.Name),
                                  metaRankPropertyInfo.Type, metaRankPropertyInfo.DisplayType, /*isSpecial*/false, performanceDataGroup);
                            rankField.IsReadOnly = true;
                            yield return rankField;
                        }

                    }
                    else
                    {
                        yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                         PlanogramPerformance.PerformanceDataProperty.Name + "." + propertyInfo.Name,
                                propertyInfo.FriendlyName, propertyInfo.Type, propertyInfo.DisplayType,
                            /*isSpecial*/false, performanceDataGroup);

                        if (includeMetadata)
                        {
                             ObjectFieldInfo percentField = 
                             ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                             PlanogramPerformance.PerformanceDataProperty.Name + "." + metaPropertyInfo.Name,
                                    metaPropertyInfo.FriendlyName, metaPropertyInfo.Type, metaPropertyInfo.DisplayType,
                                /*isSpecial*/false, performanceDataGroup);
                             percentField.IsReadOnly = true;
                             yield return percentField;

                            ObjectFieldInfo rankField = 
                            ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                             PlanogramPerformance.PerformanceDataProperty.Name + "." + metaRankPropertyInfo.Name,
                                    metaRankPropertyInfo.FriendlyName, metaRankPropertyInfo.Type, metaRankPropertyInfo.DisplayType,
                                /*isSpecial*/false, performanceDataGroup);
                            rankField.IsReadOnly = true;
                            yield return rankField;
                        }
                    }
                }

                if (includeMetadata)
                {
                    foreach (IModelPropertyInfo propertyInfo in
                        new IModelPropertyInfo[]
                    {
                        UnitsSoldPerDayProperty,
                        AchievedCasePacksProperty,
                        AchievedDosProperty,
                        AchievedShelfLifeProperty,
                        AchievedDeliveriesProperty,
                        MinimumInventoryUnitsProperty
                    })
                    {

                        yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                              PlanogramPerformance.PerformanceDataProperty.Name + "." + propertyInfo.Name,
                                     propertyInfo.FriendlyName, propertyInfo.Type, propertyInfo.DisplayType,
                            /*isSpecial*/false, performanceDataGroup);
                    }
                }


            }

        }

        /// <summary>
        /// Gets the <see cref="PlanogramProduct"/> that this instance relates to.
        /// </summary>
        /// <returns></returns>
        public PlanogramProduct GetPlanogramProduct()
        {
            if (Parent == null || Parent.Parent == null) return null;
            return Parent.Parent.Products.FindById(this.PlanogramProductId);
        }

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {

            PlanogramPerformanceDataList performanceDatas = this.Parent.PerformanceData;
            if (performanceDatas.Any())
            {
                Byte decimalPlaces = 4;
                PlanogramMetadataDetails metadataDetails = this.Parent.Parent.GetPlanogramMetadataDetails();
                Single?[] performanceTotals = metadataDetails.PlanogramPerformanceTotals;
                if (performanceTotals.Length != Constants.MaximumMetricsPerPerformanceSource) return;
                //Dictionary<Int32?, List<PlanogramPerformanceData>> peformanceRank = metadataDetails.CalculateProductRankByPerformance(this.Parent.Parent);
                //check if the product is placed.
                Boolean isProductPlaced = metadataDetails.FindPositionsByPlanogramProductId(this.PlanogramProductId).Any();

                if (isProductPlaced)
                {
                    foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
                    {
                        SetPlacedPercentage(decimalPlaces, performanceTotals, column);
                    }
                }
                else
                {
                    //if the product is not placed then just set values to 0.
                    foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
                    {
                        SetUnPlacedPercentage(decimalPlaces, performanceTotals, column);
                    }
                    
                }
            }
        }

        private void SetPlacedPercentage(byte decimalPlaces, float?[] performanceTotals, PerformanceSourceMetricColumnNumber column)
        {
            Single? value = this.GetValue(column);
            Int32 index = (Int32)column - 1;
            Single? percent = (performanceTotals[index].HasValue && performanceTotals[index] > 0 && value.HasValue) ? (Single?)Math.Round((Single)value / (Single)(performanceTotals[index].Value), decimalPlaces, MidpointRounding.AwayFromZero) : null;
            SetPercentageValue(percent, column);
        }

        private void SetUnPlacedPercentage(byte decimalPlaces, float?[] performanceTotals, PerformanceSourceMetricColumnNumber column)
        {
            Single? value = this.GetValue(column);
            Int32 index = (Int32)column - 1;
            Single? percent = (performanceTotals[index].HasValue && performanceTotals[index] > 0 && value.HasValue) ? (Single?)0 : null;
            SetPercentageValue(percent, column);
            Int32? rank = value.HasValue ? (Int32?)0 : null;
            SetRankValue(rank, column);

        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
            {
                SetPercentageValue(null, column);
                SetRankValue(null, column);
            }
        }

        #endregion

        #region IPeformanceDataExtended Methods

        public void SetValue(Single? value, PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    P1 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column2:
                    P2 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column3:
                    P3 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column4:
                    P4 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column5:
                    P5 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column6:
                    P6 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column7:
                    P7 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column8:
                    P8 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column9:
                    P9 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column10:
                    P10 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column11:
                    P11 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column12:
                    P12 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column13:
                    P13 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column14:
                    P14 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column15:
                    P15 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column16:
                    P16 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column17:
                    P17 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column18:
                    P18 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column19:
                    P19 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column20:
                    P20 = value;
                    break;

                case PerformanceSourceMetricColumnNumber.Column21:
                    CP1 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column22:
                    CP2 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column23:
                    CP3 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column24:
                    CP4 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column25:
                    CP5 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column26:
                    CP6 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column27:
                    CP7 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column28:
                    CP8 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column29:
                    CP9 = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column30:
                    CP10 = value;
                    break;
            }
        }

        public Single? GetValue(PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    return P1;
                case PerformanceSourceMetricColumnNumber.Column2:
                    return P2;
                case PerformanceSourceMetricColumnNumber.Column3:
                    return P3;
                case PerformanceSourceMetricColumnNumber.Column4:
                    return P4;
                case PerformanceSourceMetricColumnNumber.Column5:
                    return P5;
                case PerformanceSourceMetricColumnNumber.Column6:
                    return P6;
                case PerformanceSourceMetricColumnNumber.Column7:
                    return P7;
                case PerformanceSourceMetricColumnNumber.Column8:
                    return P8;
                case PerformanceSourceMetricColumnNumber.Column9:
                    return P9;
                case PerformanceSourceMetricColumnNumber.Column10:
                    return P10;
                case PerformanceSourceMetricColumnNumber.Column11:
                    return P11;
                case PerformanceSourceMetricColumnNumber.Column12:
                    return P12;
                case PerformanceSourceMetricColumnNumber.Column13:
                    return P13;
                case PerformanceSourceMetricColumnNumber.Column14:
                    return P14;
                case PerformanceSourceMetricColumnNumber.Column15:
                    return P15;
                case PerformanceSourceMetricColumnNumber.Column16:
                    return P16;
                case PerformanceSourceMetricColumnNumber.Column17:
                    return P17;
                case PerformanceSourceMetricColumnNumber.Column18:
                    return P18;
                case PerformanceSourceMetricColumnNumber.Column19:
                    return P19;
                case PerformanceSourceMetricColumnNumber.Column20:
                    return P20;

                case PerformanceSourceMetricColumnNumber.Column21:
                    return CP1;
                case PerformanceSourceMetricColumnNumber.Column22:
                    return CP2;
                case PerformanceSourceMetricColumnNumber.Column23:
                    return CP3;
                case PerformanceSourceMetricColumnNumber.Column24:
                    return CP4;
                case PerformanceSourceMetricColumnNumber.Column25:
                    return CP5;
                case PerformanceSourceMetricColumnNumber.Column26:
                    return CP6;
                case PerformanceSourceMetricColumnNumber.Column27:
                    return CP7;
                case PerformanceSourceMetricColumnNumber.Column28:
                    return CP8;
                case PerformanceSourceMetricColumnNumber.Column29:
                    return CP9;
                case PerformanceSourceMetricColumnNumber.Column30:
                    return CP10;
            }

            return null;
        }

        public void SetPercentageValue(Single? value, PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    MetaP1Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column2:
                    MetaP2Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column3:
                    MetaP3Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column4:
                    MetaP4Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column5:
                    MetaP5Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column6:
                    MetaP6Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column7:
                    MetaP7Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column8:
                    MetaP8Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column9:
                    MetaP9Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column10:
                    MetaP10Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column11:
                    MetaP11Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column12:
                    MetaP12Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column13:
                    MetaP13Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column14:
                    MetaP14Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column15:
                    MetaP15Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column16:
                    MetaP16Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column17:
                    MetaP17Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column18:
                    MetaP18Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column19:
                    MetaP19Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column20:
                    MetaP20Percentage  = value;
                    break;

                case PerformanceSourceMetricColumnNumber.Column21:
                    MetaCP1Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column22:
                    MetaCP2Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column23:
                    MetaCP3Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column24:
                    MetaCP4Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column25:
                    MetaCP5Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column26:
                    MetaCP6Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column27:
                    MetaCP7Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column28:
                    MetaCP8Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column29:
                    MetaCP9Percentage  = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column30:
                    MetaCP10Percentage  = value;
                    break;
            }
        }


        public Single? GetPercentageValue(PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    return MetaP1Percentage;
                case PerformanceSourceMetricColumnNumber.Column2:
                    return MetaP2Percentage;
                case PerformanceSourceMetricColumnNumber.Column3:
                    return MetaP3Percentage;
                case PerformanceSourceMetricColumnNumber.Column4:
                    return MetaP4Percentage;
                case PerformanceSourceMetricColumnNumber.Column5:
                    return MetaP5Percentage;
                case PerformanceSourceMetricColumnNumber.Column6:
                    return MetaP6Percentage;
                case PerformanceSourceMetricColumnNumber.Column7:
                    return MetaP7Percentage;
                case PerformanceSourceMetricColumnNumber.Column8:
                    return MetaP8Percentage;
                case PerformanceSourceMetricColumnNumber.Column9:
                    return MetaP9Percentage;
                case PerformanceSourceMetricColumnNumber.Column10:
                    return MetaP10Percentage;
                case PerformanceSourceMetricColumnNumber.Column11:
                    return MetaP11Percentage;
                case PerformanceSourceMetricColumnNumber.Column12:
                    return MetaP12Percentage;
                case PerformanceSourceMetricColumnNumber.Column13:
                    return MetaP13Percentage;
                case PerformanceSourceMetricColumnNumber.Column14:
                    return MetaP14Percentage;
                case PerformanceSourceMetricColumnNumber.Column15:
                    return MetaP15Percentage;
                case PerformanceSourceMetricColumnNumber.Column16:
                    return MetaP16Percentage;
                case PerformanceSourceMetricColumnNumber.Column17:
                    return MetaP17Percentage;
                case PerformanceSourceMetricColumnNumber.Column18:
                    return MetaP18Percentage;
                case PerformanceSourceMetricColumnNumber.Column19:
                    return MetaP19Percentage;
                case PerformanceSourceMetricColumnNumber.Column20:
                    return MetaP20Percentage;

                case PerformanceSourceMetricColumnNumber.Column21:
                    return MetaCP1Percentage;
                case PerformanceSourceMetricColumnNumber.Column22:
                    return MetaCP2Percentage;
                case PerformanceSourceMetricColumnNumber.Column23:
                    return MetaCP3Percentage;
                case PerformanceSourceMetricColumnNumber.Column24:
                    return MetaCP4Percentage;
                case PerformanceSourceMetricColumnNumber.Column25:
                    return MetaCP5Percentage;
                case PerformanceSourceMetricColumnNumber.Column26:
                    return MetaCP6Percentage;
                case PerformanceSourceMetricColumnNumber.Column27:
                    return MetaCP7Percentage;
                case PerformanceSourceMetricColumnNumber.Column28:
                    return MetaCP8Percentage;
                case PerformanceSourceMetricColumnNumber.Column29:
                    return MetaCP9Percentage;
                case PerformanceSourceMetricColumnNumber.Column30:
                    return MetaCP10Percentage;
            }

            return null;
        }

        public void SetRankValue(Int32? value, PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    MetaP1Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column2:
                    MetaP2Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column3:
                    MetaP3Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column4:
                    MetaP4Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column5:
                    MetaP5Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column6:
                    MetaP6Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column7:
                    MetaP7Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column8:
                    MetaP8Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column9:
                    MetaP9Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column10:
                    MetaP10Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column11:
                    MetaP11Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column12:
                    MetaP12Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column13:
                    MetaP13Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column14:
                    MetaP14Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column15:
                    MetaP15Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column16:
                    MetaP16Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column17:
                    MetaP17Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column18:
                    MetaP18Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column19:
                    MetaP19Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column20:
                    MetaP20Rank = value;
                    break;

                case PerformanceSourceMetricColumnNumber.Column21:
                    MetaCP1Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column22:
                    MetaCP2Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column23:
                    MetaCP3Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column24:
                    MetaCP4Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column25:
                    MetaCP5Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column26:
                    MetaCP6Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column27:
                    MetaCP7Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column28:
                    MetaCP8Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column29:
                    MetaCP9Rank = value;
                    break;
                case PerformanceSourceMetricColumnNumber.Column30:
                    MetaCP10Rank = value;
                    break;
            }
        }

        public Int32? GetRankValue(PerformanceSourceMetricColumnNumber column)
        {
            switch (column)
            {
                case PerformanceSourceMetricColumnNumber.Column1:
                    return MetaP1Rank;
                case PerformanceSourceMetricColumnNumber.Column2:
                    return MetaP2Rank;
                case PerformanceSourceMetricColumnNumber.Column3:
                    return MetaP3Rank;
                case PerformanceSourceMetricColumnNumber.Column4:
                    return MetaP4Rank;
                case PerformanceSourceMetricColumnNumber.Column5:
                    return MetaP5Rank;
                case PerformanceSourceMetricColumnNumber.Column6:
                    return MetaP6Rank;
                case PerformanceSourceMetricColumnNumber.Column7:
                    return MetaP7Rank;
                case PerformanceSourceMetricColumnNumber.Column8:
                    return MetaP8Rank;
                case PerformanceSourceMetricColumnNumber.Column9:
                    return MetaP9Rank;
                case PerformanceSourceMetricColumnNumber.Column10:
                    return MetaP10Rank;
                case PerformanceSourceMetricColumnNumber.Column11:
                    return MetaP11Rank;
                case PerformanceSourceMetricColumnNumber.Column12:
                    return MetaP12Rank;
                case PerformanceSourceMetricColumnNumber.Column13:
                    return MetaP13Rank;
                case PerformanceSourceMetricColumnNumber.Column14:
                    return MetaP14Rank;
                case PerformanceSourceMetricColumnNumber.Column15:
                    return MetaP15Rank;
                case PerformanceSourceMetricColumnNumber.Column16:
                    return MetaP16Rank;
                case PerformanceSourceMetricColumnNumber.Column17:
                    return MetaP17Rank;
                case PerformanceSourceMetricColumnNumber.Column18:
                    return MetaP18Rank;
                case PerformanceSourceMetricColumnNumber.Column19:
                    return MetaP19Rank;
                case PerformanceSourceMetricColumnNumber.Column20:
                    return MetaP20Rank;

                case PerformanceSourceMetricColumnNumber.Column21:
                    return MetaCP1Rank;
                case PerformanceSourceMetricColumnNumber.Column22:
                    return MetaCP2Rank;
                case PerformanceSourceMetricColumnNumber.Column23:
                    return MetaCP3Rank;
                case PerformanceSourceMetricColumnNumber.Column24:
                    return MetaCP4Rank;
                case PerformanceSourceMetricColumnNumber.Column25:
                    return MetaCP5Rank;
                case PerformanceSourceMetricColumnNumber.Column26:
                    return MetaCP6Rank;
                case PerformanceSourceMetricColumnNumber.Column27:
                    return MetaCP7Rank;
                case PerformanceSourceMetricColumnNumber.Column28:
                    return MetaCP8Rank;
                case PerformanceSourceMetricColumnNumber.Column29:
                    return MetaCP9Rank;
                case PerformanceSourceMetricColumnNumber.Column30:
                    return MetaCP10Rank;
            }

            return null;
        }

        /// <summary>
        /// Event method to resolve a field value for an ObjectFieldExpression object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            ObjectFieldInfo field = e.Parameter.FieldInfo;
            e.Result = field.GetValue(this);
        }
        #endregion
        #endregion
    }
}


