﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2015

//#region Version History: (CCM v8.0.3)
//// V8-29682 : A.Probyn
////   Created
//#endregion

//#endregion

//using System;
//using System.Collections.Generic;
//using Galleria.Framework.Planograms.Resources.Language;

//namespace Galleria.Framework.Planograms.Model
//{
//    /// <summary>
//    /// Indicates the value type of a PlanogramEventLog Event Type
//    /// </summary>
//    public enum AggregationType
//    {
//        Sum = 0,
//        Avg = 1,
//        Min = 2,
//        Max = 3,
//        Count = 4

//    }

//    /// <summary>
//    /// PlanogramPerformanceMetricAggregationTypeHelper Helper Class
//    /// </summary>
//    public static class PlanogramPerformanceMetricAggregationTypeHelper
//    {
//        /// <summary>
//        /// Dictionary with enum value as key and friendly name as value.
//        /// </summary>
//        public readonly static Dictionary<AggregationType, String> FriendlyNames = new Dictionary<AggregationType, String>
//        {
//            {AggregationType.Sum, Message.Enum_PlanogramPerformanceMetricAggregationType_Sum},
//            {AggregationType.Avg, Message.Enum_PlanogramPerformanceMetricAggregationType_Avg},
//            {AggregationType.Min, Message.Enum_PlanogramPerformanceMetricAggregationType_Min},
//            {AggregationType.Max, Message.Enum_PlanogramPerformanceMetricAggregationType_Max},
//            {AggregationType.Count, Message.Enum_PlanogramPerformanceMetricAggregationType_Count},
//        };
//    }
//}
