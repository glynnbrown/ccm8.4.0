﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-28242 : M.shelley
//  Created
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Added isset properties.
#endregion

#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class PlanogramAttributes : ModelObject<PlanogramAttributes>
    {
        #region Properties

        #region PlanogramId

        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramId);
        /// <summary>
        /// Gets/Sets the id of the planogram that this relates to.
        /// </summary>
        public Object PlanogramId
        {
            get { return this.GetProperty<Object>(PlanogramIdProperty); }
            set { this.SetProperty<Object>(PlanogramIdProperty, value); }
        }

        #endregion

        #region CategoryCode

        /// <summary>
        /// CategoryCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryCodeProperty =
            RegisterModelProperty<String>(c => c.CategoryCode);
        /// <summary>
        /// Gets/Sets the new category code value
        /// </summary>
        public String CategoryCode
        {
            get { return this.GetProperty<String>(CategoryCodeProperty); }
            set { this.SetProperty<String>(CategoryCodeProperty, value); }
        }

        #endregion

        #region CategoryCodeIsSet

        /// <summary>
        /// CategoryCodeIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CategoryCodeIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.CategoryCodeIsSet);

        /// <summary>
        /// Gets/Sets whether the category code should be updated.
        /// </summary>
        public Boolean CategoryCodeIsSet
        {
            get { return this.GetProperty<Boolean>(CategoryCodeIsSetProperty); }
            set { this.SetProperty<Boolean>(CategoryCodeIsSetProperty, value); }
        }

        #endregion

        #region CategoryName property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryNameProperty =
            RegisterModelProperty<String>(c => c.CategoryName);
        /// <summary>
        /// 
        /// </summary>
        public String CategoryName
        {
            get { return this.GetProperty<String>(CategoryNameProperty); }
            set { this.SetProperty<String>(CategoryNameProperty, value); }
        }

        #endregion

        #region CategoryNameIsSet

        /// <summary>
        /// CategoryNameIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CategoryNameIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.CategoryNameIsSet);

        /// <summary>
        /// Gets/Sets whether the category name should be updated.
        /// </summary>
        public Boolean CategoryNameIsSet
        {
            get { return this.GetProperty<Boolean>(CategoryNameIsSetProperty); }
            set { this.SetProperty<Boolean>(CategoryNameIsSetProperty, value); }
        }

        #endregion

        #region LocationCode

        /// <summary>
        /// LocationCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// Gets.Stes the new location code value.
        /// </summary>
        public String LocationCode
        {
            get { return this.GetProperty<String>(LocationCodeProperty); }
            set { this.SetProperty<String>(LocationCodeProperty, value); }
        }

        #endregion

        #region LocationCodeIsSet

        /// <summary>
        /// LocationCodeIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> LocationCodeIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.LocationCodeIsSet);

        /// <summary>
        /// Gets/Sets whether the location code should be updated.
        /// </summary>
        public Boolean LocationCodeIsSet
        {
            get { return this.GetProperty<Boolean>(LocationCodeIsSetProperty); }
            set { this.SetProperty<Boolean>(LocationCodeIsSetProperty, value); }
        }

        #endregion

        #region LocationName

        /// <summary>
        /// LocationName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationNameProperty =
            RegisterModelProperty<String>(c => c.LocationName);
        /// <summary>
        /// Gets/Sets the new location name value.
        /// </summary>
        public String LocationName
        {
            get { return this.GetProperty<String>(LocationNameProperty); }
            set { this.SetProperty<String>(LocationNameProperty, value); }
        }

        #endregion

        #region LocationNameIsSet

        /// <summary>
        /// LocationNameIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> LocationNameIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.LocationNameIsSet);

        /// <summary>
        /// Gets/Sets whether the location name should be updated.
        /// </summary>
        public Boolean LocationNameIsSet
        {
            get { return this.GetProperty<Boolean>(LocationNameIsSetProperty); }
            set { this.SetProperty<Boolean>(LocationNameIsSetProperty, value); }
        }

        #endregion

        #region ClusterSchemeName

        /// <summary>
        /// ClusterSchemeName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterSchemeNameProperty =
            RegisterModelProperty<String>(c => c.ClusterSchemeName);
        /// <summary>
        /// Gets/Sets the cluster scheme name.
        /// </summary>
        public String ClusterSchemeName
        {
            get { return this.GetProperty<String>(ClusterSchemeNameProperty); }
            set { this.SetProperty<String>(ClusterSchemeNameProperty, value); }
        }

        #endregion

        #region ClusterSchemeNameIsSet

        /// <summary>
        /// ClusterSchemeNameIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ClusterSchemeNameIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.ClusterSchemeNameIsSet);

        /// <summary>
        /// Gets/Sets whether the cluster scheme name should be updated.
        /// </summary>
        public Boolean ClusterSchemeNameIsSet
        {
            get { return this.GetProperty<Boolean>(ClusterSchemeNameIsSetProperty); }
            set { this.SetProperty<Boolean>(ClusterSchemeNameIsSetProperty, value); }
        }

        #endregion

        #region ClusterName

        /// <summary>
        /// ClusterName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterNameProperty =
            RegisterModelProperty<String>(c => c.ClusterName);
        /// <summary>
        /// Gets/Sets the new cluster name value
        /// </summary>
        public String ClusterName
        {
            get { return this.GetProperty<String>(ClusterNameProperty); }
            set { this.SetProperty<String>(ClusterNameProperty, value); }
        }

        #endregion

        #region ClusterNameIsSet

        /// <summary>
        /// ClusterNameIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ClusterNameIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.ClusterNameIsSet);

        /// <summary>
        /// Gets/Sets whether the cluster name should be updated.
        /// </summary>
        public Boolean ClusterNameIsSet
        {
            get { return this.GetProperty<Boolean>(ClusterNameIsSetProperty); }
            set { this.SetProperty<Boolean>(ClusterNameIsSetProperty, value); }
        }

        #endregion

        #region DateApproved

        /// <summary>
        /// DateApproved property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateApprovedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateApproved);
        /// <summary>
        /// Gets/Sets the new date approved value
        /// </summary>
        public DateTime? DateApproved
        {
            get { return this.GetProperty<DateTime?>(DateApprovedProperty); }
            set { this.SetProperty<DateTime?>(DateApprovedProperty, value); }
        }

        #endregion

        #region DateApprovedIsSet

        /// <summary>
        /// DateApprovedIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DateApprovedIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.DateApprovedIsSet);

        /// <summary>
        /// Gets/Sets whether the DateApproved should be updated.
        /// </summary>
        public Boolean DateApprovedIsSet
        {
            get { return this.GetProperty<Boolean>(DateApprovedIsSetProperty); }
            set { this.SetProperty<Boolean>(DateApprovedIsSetProperty, value); }
        }

        #endregion

        #region DateArchived

        /// <summary>
        /// DateArchived property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateArchivedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateArchived);
        /// <summary>
        /// Gets/Sets the new date archived value
        /// </summary>
        public DateTime? DateArchived
        {
            get { return this.GetProperty<DateTime?>(DateArchivedProperty); }
            set { this.SetProperty<DateTime?>(DateArchivedProperty, value); }
        }

        #endregion

        #region DateArchivedIsSet

        /// <summary>
        /// DateArchivedIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DateArchivedIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.DateArchivedIsSet);

        /// <summary>
        /// Gets/Sets whether the date archived should be updated.
        /// </summary>
        public Boolean DateArchivedIsSet
        {
            get { return this.GetProperty<Boolean>(DateArchivedIsSetProperty); }
            set { this.SetProperty<Boolean>(DateArchivedIsSetProperty, value); }
        }

        #endregion

        #region DateWip

        /// <summary>
        /// DateWip property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateWipProperty =
            RegisterModelProperty<DateTime?>(c => c.DateWip);
        /// <summary>
        /// Gets/Sets the new date wip value
        /// </summary>
        public DateTime? DateWip
        {
            get { return this.GetProperty<DateTime?>(DateWipProperty); }
            set { this.SetProperty<DateTime?>(DateWipProperty, value); }
        }

        #endregion

        #region DateWipIsSet

        /// <summary>
        /// DateWipIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DateWipIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.DateWipIsSet);

        /// <summary>
        /// Gets/Sets whether the DateWip should be updated.
        /// </summary>
        public Boolean DateWipIsSet
        {
            get { return this.GetProperty<Boolean>(DateWipIsSetProperty); }
            set { this.SetProperty<Boolean>(DateWipIsSetProperty, value); }
        }

        #endregion

        #region Status property

        /// <summary>
        /// Status property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramStatusType> StatusProperty =
            RegisterModelProperty<PlanogramStatusType>(c => c.Status);
        /// <summary>
        /// Gets/Sets the new status value
        /// </summary>
        public PlanogramStatusType Status
        {
            get { return this.GetProperty<PlanogramStatusType>(StatusProperty); }
            set { this.SetProperty<PlanogramStatusType>(StatusProperty, value); }
        }

        #endregion

        #region StatusIsSet

        /// <summary>
        /// StatusIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> StatusIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.StatusIsSet);

        /// <summary>
        /// Gets/Sets whether the Status should be updated.
        /// </summary>
        public Boolean StatusIsSet
        {
            get { return this.GetProperty<Boolean>(StatusIsSetProperty); }
            set { this.SetProperty<Boolean>(StatusIsSetProperty, value); }
        }

        #endregion

        #region PlanogramType

        /// <summary>
        /// PlanogramType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramType> PlanogramTypeProperty =
            RegisterModelProperty<PlanogramType>(c => c.PlanogramType);
        /// <summary>
        /// Gets/Sets the new planogram type value
        /// </summary>
        public PlanogramType PlanogramType
        {
            get { return this.GetProperty<PlanogramType>(PlanogramTypeProperty); }
            set { this.SetProperty<PlanogramType>(PlanogramTypeProperty, value); }
        }

        #endregion

        #region PlanogramTypeIsSet

        /// <summary>
        /// PlanogramTypeIsSet property defition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramTypeIsSetProperty =
           RegisterModelProperty<Boolean>(c => c.PlanogramTypeIsSet);

        /// <summary>
        /// Gets/Sets whether the PlanogramType should be updated.
        /// </summary>
        public Boolean PlanogramTypeIsSet
        {
            get { return this.GetProperty<Boolean>(PlanogramTypeIsSetProperty); }
            set { this.SetProperty<Boolean>(PlanogramTypeIsSetProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAttributes NewPlanogramAttributes(Object planogramId)
        {
            PlanogramAttributes item = new PlanogramAttributes();
            item.Create(planogramId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected void Create(Object planogramId)
        {
            this.LoadProperty<Object>(PlanogramIdProperty, planogramId);

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if any value is set.
        /// </summary>
        public Boolean IsAnyValueSet()
        {
            return CategoryCodeIsSet 
                || CategoryNameIsSet 
                || LocationCodeIsSet
                || LocationNameIsSet
                || ClusterSchemeNameIsSet
                || ClusterNameIsSet
                || DateApprovedIsSet
                || DateArchivedIsSet
                || DateWipIsSet
                || StatusIsSet
                || PlanogramTypeIsSet;
        }

        #endregion
    }
}
