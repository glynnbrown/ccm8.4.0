﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.
// V8-26815 : A.Silva ~ Added constuctor from source.
// V8-26954 : A.Silva ~ Addded ResultTypeProperty.
// V8-26952 : A.Kuszyk ~ Ensured lazy load properties have correct RelationshipType.
// V8-26812 : A.Silva ~ Addded ValidationType property.
// V8-27062 : A.Silva ~ Added implementation for Validation Calculations.
// V8-27237 : A.Silva   ~ Extended for ClearValidationData.
// V8-28553 : A.Kuszyk
//  Added business rule for unique list of metrics in group.
//  Added OnChildChanged event handler to update unique list rule when metrics are changed or added/removed.
#endregion
#region Version History: CCM811
// V8-28563 : A.Kuszyk
//  Changed UniqueList to framework version.
#endregion
#region Version History: CCM820
// V8-31483 : N.Foster
//  Improved performance of validation data calculation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     PlanogramValidationTemplateGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationTemplateGroup : ModelObject<PlanogramValidationTemplateGroup>, IValidationTemplateGroup
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramValidationTemplate Parent
        {
            get { return (PlanogramValidationTemplate)((PlanogramValidationTemplateGroupList)base.Parent).Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Threshold1
        /// <summary>
        /// Threshold1 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold1Property =
            RegisterModelProperty<Single>(c => c.Threshold1);
        /// <summary>
        /// Threshold1
        /// </summary>
        public Single Threshold1
        {
            get { return GetProperty<Single>(Threshold1Property); }
            set { SetProperty<Single>(Threshold1Property, value); }
        }
        #endregion

        #region Threshold2
        /// <summary>
        /// Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold2Property =
            RegisterModelProperty<Single>(c => c.Threshold2);
        /// <summary>
        /// Threshold2
        /// </summary>
        public Single Threshold2
        {
            get { return GetProperty<Single>(Threshold2Property); }
            set { SetProperty<Single>(Threshold2Property, value); }
        }
        #endregion

        #region ResultType

        /// <summary>
        ///		Metadata for the <see cref="ResultType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateResultType> ResultTypeProperty =
            RegisterModelProperty<PlanogramValidationTemplateResultType>(o => o.ResultType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ResultType"/> property.
        /// </summary>
        public PlanogramValidationTemplateResultType ResultType
        {
            get { return GetProperty(ResultTypeProperty); }
            set { SetProperty(ResultTypeProperty, value); }
        }

        #endregion

        #region Metrics

        /// <summary>
        /// Metrics property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateGroupMetricList> MetricsProperty =
            RegisterModelProperty<PlanogramValidationTemplateGroupMetricList>(c => c.Metrics, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Metrics
        /// </summary>
        public PlanogramValidationTemplateGroupMetricList Metrics
        {
            get
            {
                return this.GetPropertyLazy<PlanogramValidationTemplateGroupMetricList>(
                        MetricsProperty,
                        new PlanogramValidationTemplateGroupMetricList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                        true);
            }
        }

        #endregion

        #region ValidationType

        /// <summary>
        ///		Metadata for the <see cref="ValidationType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationType> ValidationTypeProperty =
            RegisterModelProperty<PlanogramValidationType>(o => o.ValidationType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ValidationType"/> property.
        /// </summary>
        public PlanogramValidationType ValidationType
        {
            get { return this.GetProperty<PlanogramValidationType>(ValidationTypeProperty); }
            set { this.SetProperty<PlanogramValidationType>(ValidationTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new PlanogramHelper.ValidationTypeConstraint(ValidationTypeProperty, Threshold1Property, Threshold2Property));
            BusinessRules.AddRule(new Dependency(Threshold1Property, ValidationTypeProperty));
            BusinessRules.AddRule(new Dependency(Threshold2Property, ValidationTypeProperty));
            BusinessRules.AddRule(new UniqueList<PlanogramValidationTemplateGroupMetric>(
                MetricsProperty, PlanogramValidationTemplateGroupMetric.FieldProperty, "Metrics", "a Validation Group"));
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroup NewPlanogramValidationTemplateGroup()
        {
            PlanogramValidationTemplateGroup item = new PlanogramValidationTemplateGroup();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroup NewPlanogramValidationTemplateGroup(String name)
        {
            PlanogramValidationTemplateGroup item = new PlanogramValidationTemplateGroup();
            item.Create(name);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroup NewPlanogramValidationTemplateGroup(IValidationTemplateGroup source)
        {
            PlanogramValidationTemplateGroup item = new PlanogramValidationTemplateGroup();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(MetricsProperty, PlanogramValidationTemplateGroupMetricList.NewPlanogramValidationTemplateGroupMetricList());
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String name)
        {
            LoadProperty<String>(NameProperty, name);
            LoadProperty(Threshold1Property, 5F);
            LoadProperty(Threshold2Property, 10F); 
            LoadProperty(ValidationTypeProperty, PlanogramValidationType.LessThan);
            this.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IValidationTemplateGroup source)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(NameProperty, source.Name);
            LoadProperty(Threshold1Property, source.Threshold1);
            LoadProperty(Threshold2Property, source.Threshold2);
            LoadProperty(MetricsProperty, PlanogramValidationTemplateGroupMetricList.NewPlanogramValidationTemplateGroupMetricList(source));
            LoadProperty(ValidationTypeProperty, source.ValidationType);

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramValidationTemplateGroup>(oldId, newId);
        }

        /// <summary>
        /// Called when a child object of this group is changed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);

            if (e.CollectionChangedArgs != null)
            {
                if (e.ChildObject is PlanogramValidationTemplateGroupMetricList)
                {
                    this.BusinessRules.CheckRules(MetricsProperty);
                }
            }
            else if (e.ChildObject is PlanogramValidationTemplateGroupMetric)
            {
                this.BusinessRules.CheckRules(MetricsProperty);
            }
        }

        #region Validation Data
        /// <summary>
        /// Calculates validation data for this instance
        /// </summary>
        protected override void CalculateValidationData()
        {
            // set the initial result as unknown
            PlanogramValidationTemplateResultType validationResult = PlanogramValidationTemplateResultType.Unknown;

            // calculate validation data for all metrics
            ((IModelList) Metrics).CalculateValidationData();

            // evaluate the metrics
            Single totalScore = 0;
            if (this.Metrics.Count > 0)
            {
                foreach (var metric in Metrics)
                {
                    switch (metric.ResultType)
                    {
                        case PlanogramValidationTemplateResultType.Green:
                            totalScore += metric.Score1;
                            break;
                        case PlanogramValidationTemplateResultType.Yellow:
                            totalScore += metric.Score2;
                            break;
                        case PlanogramValidationTemplateResultType.Red:
                            totalScore += metric.Score3;
                            break;
                        default:
                            break;
                    }
                }
                validationResult = ValidationType.GetResultType(totalScore, Threshold1, Threshold2);
            }

            // update the result
            this.ResultType = validationResult;
        }

        /// <summary>
        /// Clears validation data for this instance
        /// </summary>
        protected override void ClearValidationData()
        {
            // set the result type to unknown
            this.ResultType = PlanogramValidationTemplateResultType.Unknown;

            // clear the validation data on the metric
            ((IModelList)this.Metrics).ClearValidationData();
        }

        #endregion

        #endregion

        #region IValidationTemplateGroup Members

        IList<IValidationTemplateGroupMetric> IValidationTemplateGroup.Metrics
        {
            get { return Metrics.Select(metric => metric as IValidationTemplateGroupMetric).ToList(); }
        }

        IValidationTemplateGroupMetric IValidationTemplateGroup.AddNewMetric()
        {
            var newItem = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            Metrics.Add(newItem);
            return newItem;
        }

        void IValidationTemplateGroup.RemoveMetric(IValidationTemplateGroupMetric metric)
        {
            var removedItem = metric as PlanogramValidationTemplateGroupMetric;
            if (removedItem == null || !(Metrics.Contains(removedItem))) return;

            Metrics.Remove(removedItem);
        }

        #endregion
    }
}
