﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26954 : A.Silva ~ Created
// V8-26944 : A.Silva ~ Added TypeHelper.
// V8-27062 : A.Silva ~ Added internal extension method GetResultType to help mapping a set of Threshold values to their ResultType.
// V8-28060 : A.Kuszyk ~ Added FriendlyNames to helper.
// V8-27456 : A.Silva
//      Added code for GetResultType when EqualTo.

#endregion

#region Version History: CCM803
// V8-29596 : L.Luong
//  Added new methods to validated Boolean and String fields
#endregion

#region Version History: (CCM 810)
//  V8-29899 : M.Brumby
//      Handle nullable booleans
#endregion

#region Version History : CCM820
// V8-30844 : A.Kuszyk
//  Added LessThanOrEqualTo and GreaterThanOrEqualTo to GetResultType.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Possible values for Planogram Validation Results.
    /// </summary>
    public enum PlanogramValidationTemplateResultType
    {
        /// <summary>
        ///     Undeterminate result.
        /// </summary>
        Unknown,

        /// <summary>
        ///     The validation was a pass.
        /// </summary>
        Green,

        /// <summary>
        ///     The validation was a warning.
        /// </summary>
        Yellow,

        /// <summary>
        ///     The validation was a fail.
        /// </summary>
        Red
    }

    /// <summary>
    ///     This class provides support for the <see cref="PlanogramValidationTemplateResultType" /> enum.
    /// </summary>
    public static class PlanogramValidationTemplateResultTypeHelper
    {
        public static readonly Dictionary<PlanogramValidationTemplateResultType, String> FriendlyNames =
            new Dictionary<PlanogramValidationTemplateResultType, String>()
            {
                { PlanogramValidationTemplateResultType.Green, Message.Enum_PlanogramValidationTemplateResultType_Green },
                { PlanogramValidationTemplateResultType.Red, Message.Enum_PlanogramValidationTemplateResultType_Red },
                { PlanogramValidationTemplateResultType.Unknown, Message.Enum_PlanogramValidationTemplateResultType_Unknown },
                { PlanogramValidationTemplateResultType.Yellow, Message.Enum_PlanogramValidationTemplateResultType_Yellow },
            };

        /// <summary>
        ///     Calculates and returns the <see cref="PlanogramValidationTemplateResultType" /> for a field
        ///     <paramref name="value" /> and two given Threshold values (<paramref name="threshold1" />)
        /// </summary>
        /// <param name="validationType">The validation type that the values will be checked with.</param>
        /// <param name="value">The field string values to check against the threshold value.</param>
        /// <param name="threshold1">Criteria value which the list of values are comparing to.</param>
        /// <param name="threshold1">First threshold value.</param>
        /// <returns>
        ///     A value from the <see cref="PlanogramValidationTemplateResultType" /> enum calculated by comparing the count value that returns true for
        ///     the valiation type and the threshold value.
        /// </returns>
        internal static PlanogramValidationTemplateResultType GetResultType(this PlanogramValidationType validationType,
            Single value, Single threshold1, Single threshold2)
        {
            Boolean isGreaterThan = value > Math.Max(threshold1, threshold2);
            Boolean isLessThan = value < Math.Min(threshold1, threshold2);
            Boolean isLessThanOrEqualTo = value.LessOrEqualThan(Math.Min(threshold1, threshold2));
            Boolean isGreaterThanOrEqualTo = value.GreaterOrEqualThan(Math.Max(threshold1, threshold2));

            switch (validationType)
            {
                case PlanogramValidationType.GreaterThan:
                    if (isGreaterThan) return PlanogramValidationTemplateResultType.Green;
                    if (!isLessThan) return PlanogramValidationTemplateResultType.Yellow;
                    break;
                case PlanogramValidationType.LessThan:
                    if (isLessThan) return  PlanogramValidationTemplateResultType.Green;
                    if (!isGreaterThan) return PlanogramValidationTemplateResultType.Yellow;
                    break;
                case PlanogramValidationType.EqualTo:
                    if (value == threshold1) return PlanogramValidationTemplateResultType.Green;
                    if (Math.Abs(threshold1 - value) <= threshold2) return PlanogramValidationTemplateResultType.Yellow;
                    break;
                case PlanogramValidationType.LessThanOrEqualTo:
                    if (isLessThanOrEqualTo) return PlanogramValidationTemplateResultType.Green;
                    if (!isGreaterThan) return PlanogramValidationTemplateResultType.Yellow;
                    break;
                case PlanogramValidationType.GreaterThanOrEqualTo:
                    if(isGreaterThanOrEqualTo) return PlanogramValidationTemplateResultType.Green;
                    if (!isLessThan) return PlanogramValidationTemplateResultType.Yellow;
                    break;
            }
            return PlanogramValidationTemplateResultType.Red;
        }

        /// <summary>
        ///     Calculates and returns the <see cref="PlanogramValidationTemplateResultType" /> for a field
        ///     <paramref name="value" /> and two given Threshold values (<paramref name="threshold1" />)
        /// </summary>
        /// <param name="validationType">The validation type that the values will be checked with.</param>
        /// <param name="value">The field boolean values to check against the threshold value.</param>
        /// <param name="threshold1">First threshold value.</param>
        /// <returns>
        ///     A value from the <see cref="PlanogramValidationTemplateResultType" /> enum calculated by comparing the count value that returns true for
        ///     the valiation type and the threshold value.
        /// </returns>
        internal static PlanogramValidationTemplateResultType GetResultTypeString(this PlanogramValidationType validationType,
            List<Object> values, String criteria , Single threshold1)
        {
            Int32 count;
            criteria = criteria.ToLowerInvariant();
            switch (validationType)
            {
                case PlanogramValidationType.Contains:
                    count = values.Count(c => c.ToString().ToLowerInvariant().Contains(criteria));
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
                case PlanogramValidationType.DoesNotContain:
                    count = values.Count(c => !c.ToString().ToLowerInvariant().Contains(criteria));
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
                case PlanogramValidationType.EqualTo:
                    count = values.Count(c => c.ToString().ToLowerInvariant().Equals(criteria));
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
                case PlanogramValidationType.NotEqualTo:
                    count = values.Count(c => !c.ToString().ToLowerInvariant().Equals(criteria));
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
            }
            return PlanogramValidationTemplateResultType.Red;
        }

        /// <summary>
        ///     Calculates and returns the <see cref="PlanogramValidationTemplateResultType" /> for a field
        ///     <paramref name="value" /> and two given Threshold values (<paramref name="threshold1" /> and
        ///     <paramref name="threshold2" />)
        /// </summary>
        /// <param name="validationType">The validation type that the values will be checked with.</param>
        /// <param name="value">The field value to be compared against the two threshold values.</param>
        /// <param name="threshold1">First threshold value.</param>
        /// <param name="threshold2">Second threshold value</param>
        /// <returns>
        ///     A value from the <see cref="PlanogramValidationTemplateResultType" /> enum calculated by comparing the value
        ///     and the two threshold values.
        /// </returns>
        internal static PlanogramValidationTemplateResultType GetResultTypeBoolean(this PlanogramValidationType validationType,
            List<Object> values, Single threshold1)
        {
            Int32 count;
            switch (validationType)
            {
                case PlanogramValidationType.True:
                    count = values.Count(c => Convert.ToBoolean(c) == true);
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
                case PlanogramValidationType.False:
                    count = values.Count(c => Convert.ToBoolean(c) == false);
                    if (count >= threshold1) return PlanogramValidationTemplateResultType.Green;
                    break;
            }
            return PlanogramValidationTemplateResultType.Red;
        }
    }
}