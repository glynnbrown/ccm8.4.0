﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion


using System.Collections.Generic;
using System;
using Galleria.Framework.Planograms.Resources.Language;
namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramBlockingType
    {
        Initial = 0,
        PerformanceApplied = 1,
        Final = 2
    }

    public static class PlanogramBlockingTypeHelper
    {
        public static readonly Dictionary<PlanogramBlockingType, String> FriendlyNames =
           new Dictionary<PlanogramBlockingType, String>()
            {
                {PlanogramBlockingType.Initial, Message.Enum_PlanogramBlockingType_Initial},
                {PlanogramBlockingType.PerformanceApplied, Message.Enum_PlanogramBlockingType_PerformanceApplied},
                {PlanogramBlockingType.Final, Message.Enum_PlanogramBlockingType_Final},
            };
    }
}
