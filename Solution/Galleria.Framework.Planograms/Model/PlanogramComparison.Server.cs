﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts property.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType properties.

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparison
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        public PlanogramComparison() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto"/>.
        /// </summary>
        internal static PlanogramComparison Fetch(IDalContext dalContext, PlanogramComparisonDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparison>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region DataTransferObject support

        /// <summary>
        ///     Load the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonDto"/> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(DescriptionProperty, dto.Description);
            LoadProperty(DateLastComparedProperty, dto.DateLastCompared);
            LoadProperty(IgnoreNonPlacedProductsProperty, dto.IgnoreNonPlacedProducts);
            LoadProperty(DataOrderTypeProperty, (DataOrderType)dto.DataOrderType);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonResultDto"/> with the data in this instance.</returns>
        private PlanogramComparisonDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramComparisonDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramId = parent.Id,
                DateLastCompared = ReadProperty(DateLastComparedProperty),
                Name = ReadProperty(NameProperty),
                Description = ReadProperty(DescriptionProperty),
                IgnoreNonPlacedProducts = ReadProperty(IgnoreNonPlacedProductsProperty),
                DataOrderType = (Byte)ReadProperty(DataOrderTypeProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when performing a fetch.
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            try
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<IPlanogramComparisonDal>())
                    {
                        LoadDataTransferObject(dalContext, dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
            }
            catch (DtoDoesNotExistException)
            {
                Create();
            }
            MarkAsChild();
        }

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramComparisonDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramComparison>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}