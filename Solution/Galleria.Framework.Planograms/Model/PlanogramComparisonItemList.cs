﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31863 : A.Silva
//  Modified to accomadate changes in PlanogramComparisonItem.NewPlanogramComparisonItem.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Collection of <see cref="PlanogramComparisonItem"/> instances belonging to the parent <see cref="PlanogramComparisonResult"/>.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonItemList : ModelList<PlanogramComparisonItemList, PlanogramComparisonItem>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        public new PlanogramComparisonResult Parent { get { return (PlanogramComparisonResult)base.Parent; } }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparisonResultList"/>.
        /// </summary>
        public static PlanogramComparisonItemList NewPlanogramComparisonItemList()
        {
            var item = new PlanogramComparisonItemList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonResult"/> derived from a <see cref="Planogram"/>.
        /// </summary>
        /// <param name="comparisonKeys"></param>
        /// <param name="planItemType"></param>
        /// <param name="status">The status with which to add the compared item.</param>
        public List<PlanogramComparisonItem> AddRange(IEnumerable<String> comparisonKeys, PlanogramItemType planItemType, PlanogramComparisonItemStatusType status)
        {
            List<PlanogramComparisonItem> items =
                comparisonKeys.Select(key => PlanogramComparisonItem.NewPlanogramComparisonItem(key, planItemType, status)).ToList();
            AddRange(items);
            return items;
        }

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonResult"/> derived from a <see cref="Planogram"/>.
        /// </summary>
        /// <param name="comparisonKey"></param>
        /// <param name="planItemType"></param>
        /// <param name="status">The status with which to add the compared item.</param>
        public PlanogramComparisonItem Add(String comparisonKey, PlanogramItemType planItemType, PlanogramComparisonItemStatusType status)
        {
            PlanogramComparisonItem newItem = PlanogramComparisonItem.NewPlanogramComparisonItem(comparisonKey, planItemType, status);
            Add(newItem);
            return newItem;
        }

        #endregion
    }
}