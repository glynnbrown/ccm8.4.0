﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Defines a list of planograms within a package
    /// </summary>
    [Serializable]
    public partial class PlanogramList : ModelList<PlanogramList, Planogram>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Package Parent
        {
            get { return (Package)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramList NewPlanogramList()
        {
            PlanogramList item = new PlanogramList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Creates and adds a new planogram to this list
        /// </summary>
        /// <returns>A new planogram</returns>
        public Planogram Add()
        {
            Planogram planogram = Planogram.NewPlanogram();
            this.Add(planogram);
            return planogram;
        }
        #endregion

        #endregion
    }
}
