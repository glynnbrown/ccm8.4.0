﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentInventoryRule
    {
        #region Constructor
        private PlanogramAssortmentInventoryRule() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortmentInventoryRule Fetch(IDalContext dalContext, PlanogramAssortmentInventoryRuleDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortmentInventoryRule>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentInventoryRuleDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            LoadProperty<Single>(CasePackProperty, dto.CasePack);
            LoadProperty<Single>(DaysOfSupplyProperty, dto.DaysOfSupply);
            LoadProperty<Single>(ShelfLifeProperty, dto.ShelfLife);
            LoadProperty<Single>(ReplenishmentDaysProperty, dto.ReplenishmentDays);
            LoadProperty<Single>(WasteHurdleUnitsProperty, dto.WasteHurdleUnits);
            LoadProperty<Single>(WasteHurdleCasePackProperty, dto.WasteHurdleCasePack);
            LoadProperty<Int32>(MinUnitsProperty, dto.MinUnits);
            LoadProperty<Int32>(MinFacingsProperty, dto.MinFacings);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private PlanogramAssortmentInventoryRuleDto GetDataTransferObject(PlanogramAssortment parent)
        {
            return new PlanogramAssortmentInventoryRuleDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramAssortmentId = parent.Id,
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                CasePack = ReadProperty<Single>(CasePackProperty),
                DaysOfSupply = ReadProperty<Single>(DaysOfSupplyProperty),
                ShelfLife = ReadProperty<Single>(ShelfLifeProperty),
                ReplenishmentDays = ReadProperty<Single>(ReplenishmentDaysProperty),
                WasteHurdleUnits = ReadProperty<Single>(WasteHurdleUnitsProperty),
                WasteHurdleCasePack = ReadProperty<Single>(WasteHurdleCasePackProperty),
                MinUnits = ReadProperty<Int32>(MinUnitsProperty),
                MinFacings = ReadProperty<Int32>(MinFacingsProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentInventoryRuleDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentInventoryRuleDto>(
            (dc) =>
            {
                PlanogramAssortmentInventoryRuleDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortmentInventoryRule>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentInventoryRuleDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            batchContext.Delete<PlanogramAssortmentInventoryRuleDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
