﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Sub Component Fill Pattern Type Enum
    /// </summary>
    public enum PlanogramSubComponentFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }

    /// <summary>
    /// PlanogramSubComponentFillPatternType Helper Class
    /// </summary>
    public static class PlanogramSubComponentFillPatternTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentFillPatternType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentFillPatternType, String>()
            {
                {PlanogramSubComponentFillPatternType.Solid, Message.Enum_PlanogramSubComponentFillPatternType_Solid},
                {PlanogramSubComponentFillPatternType.Border, Message.Enum_PlanogramSubComponentFillPatternType_Border},
                {PlanogramSubComponentFillPatternType.DiagonalUp, Message.Enum_PlanogramSubComponentFillPatternType_DiagonalUp},
                {PlanogramSubComponentFillPatternType.DiagonalDown, Message.Enum_PlanogramSubComponentFillPatternType_DiagonalDown},
                {PlanogramSubComponentFillPatternType.Crosshatch, Message.Enum_PlanogramSubComponentFillPatternType_Crosshatch},
                {PlanogramSubComponentFillPatternType.Horizontal, Message.Enum_PlanogramSubComponentFillPatternType_Horizontal},
                {PlanogramSubComponentFillPatternType.Vertical, Message.Enum_PlanogramSubComponentFillPatternType_Vertical},
                {PlanogramSubComponentFillPatternType.Dotted, Message.Enum_PlanogramSubComponentFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentFillPatternType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentFillPatternType, String>()
            {
                {PlanogramSubComponentFillPatternType.Solid, Message.Enum_PlanogramSubComponentFillPatternType_Solid},
                {PlanogramSubComponentFillPatternType.Border, Message.Enum_PlanogramSubComponentFillPatternType_Border},
                {PlanogramSubComponentFillPatternType.DiagonalUp, Message.Enum_PlanogramSubComponentFillPatternType_DiagonalUp},
                {PlanogramSubComponentFillPatternType.DiagonalDown, Message.Enum_PlanogramSubComponentFillPatternType_DiagonalDown},
                {PlanogramSubComponentFillPatternType.Crosshatch, Message.Enum_PlanogramSubComponentFillPatternType_Crosshatch},
                {PlanogramSubComponentFillPatternType.Horizontal, Message.Enum_PlanogramSubComponentFillPatternType_Horizontal},
                {PlanogramSubComponentFillPatternType.Vertical, Message.Enum_PlanogramSubComponentFillPatternType_Vertical},
                {PlanogramSubComponentFillPatternType.Dotted, Message.Enum_PlanogramSubComponentFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentFillPatternType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentFillPatternType>()
            {
                {Message.Enum_PlanogramSubComponentFillPatternType_Solid.ToLowerInvariant(), PlanogramSubComponentFillPatternType.Solid},
                {Message.Enum_PlanogramSubComponentFillPatternType_Border.ToLowerInvariant(),PlanogramSubComponentFillPatternType.Border},
                {Message.Enum_PlanogramSubComponentFillPatternType_DiagonalUp.ToLowerInvariant(), PlanogramSubComponentFillPatternType.DiagonalUp},
                {Message.Enum_PlanogramSubComponentFillPatternType_DiagonalDown.ToLowerInvariant(),PlanogramSubComponentFillPatternType.DiagonalDown},
                {Message.Enum_PlanogramSubComponentFillPatternType_Crosshatch.ToLowerInvariant(),PlanogramSubComponentFillPatternType.Crosshatch},
                {Message.Enum_PlanogramSubComponentFillPatternType_Horizontal.ToLowerInvariant(),PlanogramSubComponentFillPatternType.Horizontal},
                {Message.Enum_PlanogramSubComponentFillPatternType_Vertical.ToLowerInvariant(), PlanogramSubComponentFillPatternType.Vertical},
                {Message.Enum_PlanogramSubComponentFillPatternType_Dotted.ToLowerInvariant(),PlanogramSubComponentFillPatternType.Dotted}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentFillPatternType? PlanogramSubComponentFillPatternTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentFillPatternType? returnValue = null;
            PlanogramSubComponentFillPatternType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}