﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25949 : Neil Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Exception that indicates that a package could not be locked
    /// </summary>
    [Serializable]
    public class PackageLockException : Exception
    {
        public PackageLockException() : base(Galleria.Framework.Planograms.Resources.Language.Message.PackageLockException) { }
        public PackageLockException(string message) : base(message) { }
        public PackageLockException(string message, Exception innerException) : base(message, innerException) { }
        protected PackageLockException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
