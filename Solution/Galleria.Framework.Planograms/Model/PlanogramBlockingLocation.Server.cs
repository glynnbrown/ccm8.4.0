﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created 
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingLocation
    {
        #region Constructor
        private PlanogramBlockingLocation() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramBlockingLocation Fetch(IDalContext dalContext, PlanogramBlockingLocationDto dto)
        {
            return DataPortal.FetchChild<PlanogramBlockingLocation>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramBlockingLocationDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(PlanogramBlockingGroupIdProperty, dto.PlanogramBlockingGroupId);
            this.LoadProperty<Object>(PlanogramBlockingDividerTopIdProperty, dto.PlanogramBlockingDividerTopId);
            this.LoadProperty<Object>(PlanogramBlockingDividerBottomIdProperty, dto.PlanogramBlockingDividerBottomId);
            this.LoadProperty<Object>(PlanogramBlockingDividerLeftIdProperty, dto.PlanogramBlockingDividerLeftId);
            this.LoadProperty<Object>(PlanogramBlockingDividerRightIdProperty, dto.PlanogramBlockingDividerRightId);
            this.LoadProperty<Single>(SpacePercentageProperty, dto.SpacePercentage);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramBlockingLocationDto GetDataTransferObject(PlanogramBlocking parent)
        {
            return new PlanogramBlockingLocationDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramBlockingId = parent.Id,
                PlanogramBlockingGroupId = this.ReadProperty<Object>(PlanogramBlockingGroupIdProperty),
                PlanogramBlockingDividerTopId = this.ReadProperty<Object>(PlanogramBlockingDividerTopIdProperty),
                PlanogramBlockingDividerBottomId = this.ReadProperty<Object>(PlanogramBlockingDividerBottomIdProperty),
                PlanogramBlockingDividerLeftId = this.ReadProperty<Object>(PlanogramBlockingDividerLeftIdProperty),
                PlanogramBlockingDividerRightId = this.ReadProperty<Object>(PlanogramBlockingDividerRightIdProperty),
                SpacePercentage = this.ReadProperty<Single>(SpacePercentageProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }


        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramBlockingLocationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramBlockingLocationDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramBlockingLocationDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramBlockingLocation>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramBlockingLocationDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this object
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            batchContext.Delete<PlanogramBlockingLocationDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
