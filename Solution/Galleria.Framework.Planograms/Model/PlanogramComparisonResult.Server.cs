﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonResult
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonResult() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto"/>.
        /// </summary>
        internal static PlanogramComparisonResult Fetch(IDalContext dalContext, PlanogramComparisonResultDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonResult>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonResultDto"/> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonResultDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
            LoadProperty(PlanogramNameProperty, dto.PlanogramName);
            LoadProperty(StatusProperty, (PlanogramComparisonResultStatusType)dto.Status);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonResultDto"/> with the data in this instance.</returns>
        private PlanogramComparisonResultDto GetDataTransferObject(PlanogramComparison parent)
        {
            return new PlanogramComparisonResultDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramComparisonId = parent.Id,
                ExtendedData = ReadProperty(ExtendedDataProperty),
                PlanogramName = ReadProperty(PlanogramNameProperty),
                Status = (Byte) ReadProperty(StatusProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonResultDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramComparisonResultDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramComparisonResult>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}