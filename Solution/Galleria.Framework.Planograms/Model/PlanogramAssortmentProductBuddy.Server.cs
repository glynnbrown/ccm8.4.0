﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentProductBuddy
    {
        #region Constructor
        private PlanogramAssortmentProductBuddy() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortmentProductBuddy Fetch(IDalContext dalContext, PlanogramAssortmentProductBuddyDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortmentProductBuddy>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentProductBuddyDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            LoadProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty, (PlanogramAssortmentProductBuddySourceType)dto.SourceType);
            LoadProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty, (PlanogramAssortmentProductBuddyTreatmentType)dto.TreatmentType);
            LoadProperty<Single>(TreatmentTypePercentageProperty, dto.TreatmentTypePercentage);
            LoadProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty, (PlanogramAssortmentProductBuddyProductAttributeType)dto.ProductAttributeType);
            LoadProperty<String>(S1ProductGtinProperty, dto.S1ProductGtin);
            LoadProperty<Single?>(S1PercentageProperty, dto.S1Percentage);
            LoadProperty<String>(S2ProductGtinProperty, dto.S2ProductGtin);
            LoadProperty<Single?>(S2PercentageProperty, dto.S2Percentage);
            LoadProperty<String>(S3ProductGtinProperty, dto.S3ProductGtin);
            LoadProperty<Single?>(S3PercentageProperty, dto.S3Percentage);
            LoadProperty<String>(S4ProductGtinProperty, dto.S4ProductGtin);
            LoadProperty<Single?>(S4PercentageProperty, dto.S4Percentage);
            LoadProperty<String>(S5ProductGtinProperty, dto.S5ProductGtin);
            LoadProperty<Single?>(S5PercentageProperty, dto.S5Percentage);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private PlanogramAssortmentProductBuddyDto GetDataTransferObject(PlanogramAssortment parent)
        {
            return new PlanogramAssortmentProductBuddyDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramAssortmentId = parent.Id,
                SourceType = (Byte)ReadProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty),
                TreatmentType = (Byte)ReadProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty),
                ProductAttributeType = (Byte)ReadProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty),
                TreatmentTypePercentage = ReadProperty<Single>(TreatmentTypePercentageProperty),
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                S1ProductGtin = ReadProperty<String>(S1ProductGtinProperty),
                S1Percentage = ReadProperty<Single?>(S1PercentageProperty),
                S2ProductGtin = ReadProperty<String>(S2ProductGtinProperty),
                S2Percentage = ReadProperty<Single?>(S2PercentageProperty),
                S3ProductGtin = ReadProperty<String>(S3ProductGtinProperty),
                S3Percentage = ReadProperty<Single?>(S3PercentageProperty),
                S4ProductGtin = ReadProperty<String>(S4ProductGtinProperty),
                S4Percentage = ReadProperty<Single?>(S4PercentageProperty),
                S5ProductGtin = ReadProperty<String>(S5ProductGtinProperty),
                S5Percentage = ReadProperty<Single?>(S5PercentageProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentProductBuddyDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentProductBuddyDto>(
            (dc) =>
            {
                PlanogramAssortmentProductBuddyDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortmentProductBuddy>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentProductBuddyDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            batchContext.Delete<PlanogramAssortmentProductBuddyDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
