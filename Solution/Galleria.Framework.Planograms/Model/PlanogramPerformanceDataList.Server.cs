﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramPerformanceDataList
    {
        #region Constructor
        private PlanogramPerformanceDataList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all Planogram Performance Data for a Planogram Performance item.
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            var dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramPerformanceDataDal>())
                {
                    var dtoList = dal.FetchByPlanogramPerformanceId(criteria.ParentId);
                    foreach (var dto in dtoList)
                    {
                        this.Add(PlanogramPerformanceData.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
