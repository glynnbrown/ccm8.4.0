﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramFixtureAssemblyList
    {
        #region Constructor
        private PlanogramFixtureAssemblyList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all assemblies for a fixture
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramFixtureAssemblyDal dal = dalContext.GetDal<IPlanogramFixtureAssemblyDal>())
                {
                    IEnumerable<PlanogramFixtureAssemblyDto> dtoList = dal.FetchByPlanogramFixtureId(criteria.ParentId);
                    foreach (PlanogramFixtureAssemblyDto dto in dtoList)
                    {
                        this.Add(PlanogramFixtureAssembly.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
