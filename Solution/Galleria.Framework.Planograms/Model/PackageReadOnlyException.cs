﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25949 : Neil Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public class PackageReadOnlyException : Exception
    {
        public PackageReadOnlyException() : base(Galleria.Framework.Planograms.Resources.Language.Message.PackageReadOnlyException) { }
        public PackageReadOnlyException(string message) : base(message) { }
        public PackageReadOnlyException(string message, Exception innerException) : base(message, innerException) { }
        protected PackageReadOnlyException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
