﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Changed LevelType to EntryType
// V8-28059 : A.Kuszyk
//  Added Debug option.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Indicates the value type of a PlanogramEventLog Level Type
    /// </summary>
    public enum PlanogramEventLogEntryType
    {
        Error,
        Warning,
        Information,
        Debug
    }

    /// <summary>
    /// PlanogramEventLogLevelTypeHelper Helper Class
    /// </summary>
    public static class PlanogramEventLogEntryTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public readonly static Dictionary<PlanogramEventLogEntryType, String> FriendlyNames = new Dictionary<PlanogramEventLogEntryType, String>
        {
            {PlanogramEventLogEntryType.Error, Message.Enum_PlanogramEventLogEntryType_Error},
            {PlanogramEventLogEntryType.Warning, Message.Enum_PlanogramEventLogEntryType_Warning},
            {PlanogramEventLogEntryType.Information, Message.Enum_PlanogramEventLogEntryType_Information},
            {PlanogramEventLogEntryType.Debug, Message.Enum_PlanogramEventLogEntryType_Detailed } 
        };
    }
}
