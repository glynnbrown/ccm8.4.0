﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27759 : A.Kuszyk
//   Created.
// V8-27765 : A.Kuszyk
//  Added TypesByOption helper.
// V8-28060 : A.Kuszyk
//  Added BlockingGroup option.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Indicates the type of planogram item that has been affected in an Event Log entry.
    /// </summary>
    /// <remarks>
    /// NOTE:
    /// When adding options to this enum, the TypesByOption dictionary on the enum helper must
    /// also be updated otherwise the Ids for these Planogram items will not be updated.
    /// 
    /// Space has also been left between the options for additional child objects as and when
    /// they are needed.
    /// </remarks>
    public enum PlanogramEventLogAffectedType
    {
        Planogram = 0,
        //Annotations = 0,
        //Assemblies = 10,
        Assortment = 20,
        AssortmentProduct = 21,
        Blocking = 30, 
        BlockingGroup = 31,
        Components = 40,
        ConsumerDecisionTree = 50,
        //CustomAttributes = 60,
        //FixtureItems = 70,
        //Fixtures = 80,
        //Images = 90,
        Metadata = 100,
        Performance = 110,
        Positions = 120,
        Products = 130,
        RenumberingStrategy = 140, 
        ValidationTemplate = 150
    }

    /// <summary>
    /// Friendly name helper for the PlanogramEventLogAffectType enum.
    /// </summary>
    public static class PlanogramEventLogAffectedTypeHelper
    {
        public static Dictionary<PlanogramEventLogAffectedType, String> FriendlyNames =
            new Dictionary<PlanogramEventLogAffectedType, String>()
            {
                { PlanogramEventLogAffectedType.Planogram, Message.Enum_PlanogramEventLogAffectedType_Planogram },
                //{ PlanogramEventLogAffectedType.Annotations, Message.Enum_PlanogramEventLogAffectedType_Annotations },
                //{ PlanogramEventLogAffectedType.Assemblies, Message.Enum_PlanogramEventLogAffectedType_Assemblies },
                { PlanogramEventLogAffectedType.Assortment, Message.Enum_PlanogramEventLogAffectedType_Assortment },
                { PlanogramEventLogAffectedType.AssortmentProduct, Message.Enum_PlanogramEventLogAffectedType_AssortmentProduct },
                { PlanogramEventLogAffectedType.Blocking, Message.Enum_PlanogramEventLogAffectedType_Blocking },
                { PlanogramEventLogAffectedType.BlockingGroup, Message.Enum_PlanogramEventLogAffectedType_BlockingGroup },
                { PlanogramEventLogAffectedType.Components, Message.Enum_PlanogramEventLogAffectedType_Components },
                { PlanogramEventLogAffectedType.ConsumerDecisionTree, Message.Enum_PlanogramEventLogAffectedType_ConsumerDecisionTree },
                //{ PlanogramEventLogAffectedType.CustomAttributes, Message.Enum_PlanogramEventLogAffectedType_CustomAttributes },
                //{ PlanogramEventLogAffectedType.FixtureItems, Message.Enum_PlanogramEventLogAffectedType_FixtureItems },
                //{ PlanogramEventLogAffectedType.Fixtures, Message.Enum_PlanogramEventLogAffectedType_Fixtures },
                //{ PlanogramEventLogAffectedType.Images, Message.Enum_PlanogramEventLogAffectedType_Images },
                { PlanogramEventLogAffectedType.Metadata, Message.Enum_PlanogramEventLogAffectedType_Metadata },
                { PlanogramEventLogAffectedType.Performance, Message.Enum_PlanogramEventLogAffectedType_Performance },
                { PlanogramEventLogAffectedType.Positions, Message.Enum_PlanogramEventLogAffectedType_Positions },
                { PlanogramEventLogAffectedType.Products, Message.Enum_PlanogramEventLogAffectedType_Products },
                { PlanogramEventLogAffectedType.RenumberingStrategy, Message.Enum_PlanogramEventLogAffectedType_RenumberingStrategy },
                { PlanogramEventLogAffectedType.ValidationTemplate, Message.Enum_PlanogramEventLogAffectedType_ValidationTemplate }
            };

        public static Dictionary<PlanogramEventLogAffectedType, Type> TypesByOption =
            new Dictionary<PlanogramEventLogAffectedType, Type>()
            {
                //{ PlanogramEventLogAffectedType.Annotations, typeof(PlanogramAnnotation)},
                //{ PlanogramEventLogAffectedType.Assemblies, typeof(PlanogramAssembly)},
                { PlanogramEventLogAffectedType.Assortment, typeof(PlanogramAssortment)},
                { PlanogramEventLogAffectedType.AssortmentProduct, typeof(PlanogramAssortmentProduct)},
                { PlanogramEventLogAffectedType.Blocking, typeof(PlanogramBlocking)},
                { PlanogramEventLogAffectedType.BlockingGroup, typeof(PlanogramBlockingGroup)},
                { PlanogramEventLogAffectedType.Components, typeof(PlanogramComponent)},
                { PlanogramEventLogAffectedType.ConsumerDecisionTree, typeof(PlanogramConsumerDecisionTree)},
                //{ PlanogramEventLogAffectedType.CustomAttributes, typeof(CustomAttributeData)},
                //{ PlanogramEventLogAffectedType.FixtureItems, typeof(PlanogramFixtureItem)},
                //{ PlanogramEventLogAffectedType.Fixtures, typeof(PlanogramFixture)},
                //{ PlanogramEventLogAffectedType.Images, typeof(PlanogramImage)},
                { PlanogramEventLogAffectedType.Metadata, null},
                { PlanogramEventLogAffectedType.Performance, typeof(PlanogramPerformance)},
                { PlanogramEventLogAffectedType.Positions, typeof(PlanogramPosition)},
                { PlanogramEventLogAffectedType.Products, typeof(PlanogramProduct)},
                { PlanogramEventLogAffectedType.RenumberingStrategy, typeof(PlanogramRenumberingStrategy)},
                { PlanogramEventLogAffectedType.ValidationTemplate, typeof(PlanogramValidationTemplate)},
            };
    }
}
