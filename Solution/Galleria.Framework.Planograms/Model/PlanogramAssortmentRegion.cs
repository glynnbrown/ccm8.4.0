﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added ToString override.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegion.
// V8-27267 : A.Kuszyk
//  Modified Create(region) to set Name using property so that validation rules will be checked on new object.
#endregion
#endregion

using System;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;
using Csla;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentRegion : ModelObject<PlanogramAssortmentRegion>, IPlanogramAssortmentRegion
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)((PlanogramAssortmentRegionList)base.Parent).Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PlanogramAssortmentRegion_Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Locations
        /// <summary>
        /// Locations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentRegionLocationList> LocationsProperty =
            RegisterModelProperty<PlanogramAssortmentRegionLocationList>(c => c.Locations, Message.PlanogramAssortmentRegion_Locations,RelationshipTypes.LazyLoad);

        /// <summary>
        /// Locations
        /// </summary>
        public PlanogramAssortmentRegionLocationList Locations
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentRegionLocationList>(
                    LocationsProperty,
                    new PlanogramAssortmentRegionLocationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Locations
        /// </summary>
        public PlanogramAssortmentRegionLocationList LocationsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentRegionLocationList>(
                    LocationsProperty,
                    new PlanogramAssortmentRegionLocationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Products
        /// <summary>
        /// Products property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentRegionProductList> ProductsProperty =
            RegisterModelProperty<PlanogramAssortmentRegionProductList>(c => c.Products, Message.PlanogramAssortmentRegion_Products, RelationshipTypes.LazyLoad);
        /// <summary>
        /// Products
        /// </summary>
        public PlanogramAssortmentRegionProductList Products
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentRegionProductList>(
                        ProductsProperty,
                        new PlanogramAssortmentRegionProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                        true);
            }
        }
        #endregion

        #region IPlanogramAssortmentRegion Members

        /// <summary>
        /// IPlanogramAssortmentRegion Locations member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegionLocation> IPlanogramAssortmentRegion.Locations
        {
            get { return Locations.Select(o => (IPlanogramAssortmentRegionLocation)o); }
        }

        /// <summary>
        /// IPlanogramAssortmentRegion Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegionProduct> IPlanogramAssortmentRegion.Products
        {
            get { return Products.Select(o => (IPlanogramAssortmentRegionProduct)o); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentRegion NewPlanogramAssortmentRegion()
        {
            var item = new PlanogramAssortmentRegion();
            item.Create();
            return item;
        }

        public static PlanogramAssortmentRegion NewPlanogramAssortmentRegion(IPlanogramAssortmentRegion region)
        {
            var item = new PlanogramAssortmentRegion();
            item.Create(region);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramAssortmentRegionLocationList>(LocationsProperty, PlanogramAssortmentRegionLocationList.NewPlanogramAssortmentRegionLocationList());
            this.LoadProperty<PlanogramAssortmentRegionProductList>(ProductsProperty, PlanogramAssortmentRegionProductList.NewPlanogramAssortmentRegionProductList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramAssortmentRegion region)
        {
            this.Create();
            Name = region.Name;
            Locations.AddRange(region.Locations);
            Products.AddRange(region.Products);
        }
        #endregion

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
