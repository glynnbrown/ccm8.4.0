﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonItem
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonItem() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto"/>.
        /// </summary>
        internal static PlanogramComparisonItem Fetch(IDalContext dalContext, PlanogramComparisonItemDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonItem>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonItemDto"/> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonItemDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ItemTypeProperty, (PlanogramItemType) dto.ItemType);
            LoadProperty(ItemIdProperty, dto.ItemId);
            LoadProperty(StatusProperty, (PlanogramComparisonItemStatusType)dto.Status);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonItemDto"/> with the data in this instance.</returns>
        private PlanogramComparisonItemDto GetDataTransferObject(PlanogramComparisonResult parent)
        {
            return new PlanogramComparisonItemDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramComparisonResultId = parent.Id,
                ItemType = (Byte) ReadProperty(ItemTypeProperty),
                ItemId = ReadProperty(ItemIdProperty),
                Status = (Byte) ReadProperty(StatusProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonItemDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparisonItem"/>.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramComparisonResult parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramComparisonItemDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramComparisonItem>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonItem"/>.
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramComparisonResult parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparisonItem"/>.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramComparisonResult parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}