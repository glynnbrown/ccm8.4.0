﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 802

// V8-28995 : A.Kuszyk
//  Created.
// V8-28999 : A.Silva
//      Added some extension methods to help working with PlanogramBlockingGroupPlacementType.

#endregion

#region Version History: CCM810

// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.

#endregion

#region Version History : CCM811
// V8-30114 : A.Kuszyk
//  Added ToNegative and ToPositive extension methods.
#endregion

#region Version History : CCM820
// V8-30795 : A.Kuszyk
//  Added GetIndex extension overload for IPlanogramSubComponentSequencedItem.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using System.Diagnostics;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramBlockingGroupPlacementType
    {
        LeftToRight = 0,
        RightToLeft = 1,
        BottomToTop = 2,
        TopToBottom = 3,
        BackToFront = 4,
        FrontToBack = 5,
    }

    public static class PlanogramBlockingGroupPlacementTypeHelper
    {
        public static readonly Dictionary<PlanogramBlockingGroupPlacementType, String> FriendlyNames =
           new Dictionary<PlanogramBlockingGroupPlacementType, String>()
            {
                {PlanogramBlockingGroupPlacementType.LeftToRight, Message.Enum_PlanogramBlockingGroupPlacementType_LeftToRight},
                {PlanogramBlockingGroupPlacementType.RightToLeft, Message.Enum_PlanogramBlockingGroupPlacementType_RightToLeft},
                {PlanogramBlockingGroupPlacementType.BottomToTop, Message.Enum_PlanogramBlockingGroupPlacementType_BottomToTop},
                {PlanogramBlockingGroupPlacementType.TopToBottom, Message.Enum_PlanogramBlockingGroupPlacementType_TopToBottom},
                {PlanogramBlockingGroupPlacementType.BackToFront, Message.Enum_PlanogramBlockingGroupPlacementType_BackToFront}, 
                {PlanogramBlockingGroupPlacementType.FrontToBack, Message.Enum_PlanogramBlockingGroupPlacementType_FrontToBack},
            };
    }

    public static class PlanogramBlockingGroupPlacementTypeExtensions
    {
        public static AxisType GetAxis(this PlanogramBlockingGroupPlacementType placementType)
        {
            if (placementType.IsX()) return AxisType.X;
            if (placementType.IsY()) return AxisType.Y;
            if (placementType.IsZ()) return AxisType.Z;
            throw new NotSupportedException();
        }

        public static Boolean IsX(this PlanogramBlockingGroupPlacementType placementType)
        {
            return 
                placementType == PlanogramBlockingGroupPlacementType.LeftToRight || 
                placementType == PlanogramBlockingGroupPlacementType.RightToLeft;
        }

        public static Boolean IsY(this PlanogramBlockingGroupPlacementType placementType)
        {
            return
                placementType == PlanogramBlockingGroupPlacementType.BottomToTop||
                placementType == PlanogramBlockingGroupPlacementType.TopToBottom;
        }

        public static Boolean IsZ(this PlanogramBlockingGroupPlacementType placementType)
        {
            return
                placementType == PlanogramBlockingGroupPlacementType.BackToFront||
                placementType == PlanogramBlockingGroupPlacementType.FrontToBack;
        }

        public static Single GetIndex(this PlanogramBlockingGroupPlacementType source, PointValue coordinates)
        {
            return source.IsX() ? coordinates.X 
                : (source.IsY() ? coordinates.Y 
                : (source.IsZ() ? coordinates.Z 
                : 0));
        }

        public static Single GetIndex(this PlanogramBlockingGroupPlacementType source, PlanogramPosition position)
        {
            return source.IsX() ? position.SequenceX
                : (source.IsY() ? position.SequenceY
                : (source.IsZ() ? position.SequenceZ
                : 0));
        }

        public static Single GetIndex(this PlanogramBlockingGroupPlacementType source, IPlanogramSubComponentSequencedItem position)
        {
            return source.IsX() ? position.SequenceX
                : (source.IsY() ? position.SequenceY
                : (source.IsZ() ? position.SequenceZ
                : 0));
        }

        /// <summary>
        /// Indicates if the placement type points in the positive direction in space.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Boolean IsPositive(this PlanogramBlockingGroupPlacementType source)
        {
            return source == PlanogramBlockingGroupPlacementType.LeftToRight||
                   source == PlanogramBlockingGroupPlacementType.BottomToTop ||
                   source == PlanogramBlockingGroupPlacementType.BackToFront;
        }

        /// <summary>
        /// Takes the direction of this item and returns the positive option. E.g. any X direction would return Left To Right.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PlanogramBlockingGroupPlacementType ToPositive(this PlanogramBlockingGroupPlacementType source)
        {
            if (source.IsX())
            {
                return PlanogramBlockingGroupPlacementType.LeftToRight;
            }
            else if (source.IsY())
            {
                return PlanogramBlockingGroupPlacementType.BottomToTop;
            }
            else if (source.IsZ())
            {
                return PlanogramBlockingGroupPlacementType.BackToFront;
            }
            else
            {
                Debug.Fail("source was not X, Y or Z");
                return PlanogramBlockingGroupPlacementType.BackToFront;
            }
        }

        /// <summary>
        /// Takes the direction of this item and returns the negative option. E.g. any X direction would return Right To Left.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PlanogramBlockingGroupPlacementType ToNegative(this PlanogramBlockingGroupPlacementType source)
        {
            if (source.IsX())
            {
                return PlanogramBlockingGroupPlacementType.RightToLeft;
            }
            else if (source.IsY())
            {
                return PlanogramBlockingGroupPlacementType.TopToBottom;
            }
            else if (source.IsZ())
            {
                return PlanogramBlockingGroupPlacementType.FrontToBack;
            }
            else
            {
                Debug.Fail("source was not X, Y or Z");
                return PlanogramBlockingGroupPlacementType.BackToFront;
            }
        }

        public static PlanogramBlockingGroupPlacementType Invert(this PlanogramBlockingGroupPlacementType source, Boolean invert)
        {
            switch(source)
            {
                case PlanogramBlockingGroupPlacementType.BackToFront:
                    return invert ? PlanogramBlockingGroupPlacementType.FrontToBack : source;
                case PlanogramBlockingGroupPlacementType.FrontToBack:
                    return invert ? PlanogramBlockingGroupPlacementType.BackToFront: source;
                case PlanogramBlockingGroupPlacementType.LeftToRight:
                    return invert ? PlanogramBlockingGroupPlacementType.RightToLeft: source;
                case PlanogramBlockingGroupPlacementType.RightToLeft:
                    return invert ? PlanogramBlockingGroupPlacementType.LeftToRight : source;
                case PlanogramBlockingGroupPlacementType.TopToBottom:
                    return invert ? PlanogramBlockingGroupPlacementType.BottomToTop : source;
                case PlanogramBlockingGroupPlacementType.BottomToTop:
                    return invert ? PlanogramBlockingGroupPlacementType.TopToBottom : source;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
