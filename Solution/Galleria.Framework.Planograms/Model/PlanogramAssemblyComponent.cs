﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)

// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26569 : L.Ineson
//  Added on methods to get world rotation and position
// V8-27058 : A.Probyn
//  Added new meta data calculations
//  Added ClearMetaData
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber property.
// V8-27477 : L.Ineson
//  Added more world space helper methods
// V8-27570 : A.Silva
//      Amended to include ComponentSequenceNumber to DisplayablePropertyInfos.
// V8-27605 : A.Silva
//      Added Meta data properties that PlanogramFixtureComponent has but were not included in PlanogramAssemblyComponent.
//      Added the relevant Meta data calculations.
// V8-27742 : A.Silva
//      Added IsOverfilled validation warning.
// V8-28078 : A.Probyn
//      Added defensive code to GetPlanogramRelativeCoordinates
// V8-27442 : L.Ineson
//      Added notch placement methods.

#endregion

#region Version History: CCM802

// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29044 : M.Shelley
//  Added NotchNumber property to allow saving to the database
// V8-29369 : L.Luong
//  Modified Meta data calculation so they are correct
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single

#endregion

#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29514 : L.Ineson
//	Removed PlanogramAssemblyComponent_MetaComponentCount
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-30210 : M.Brumby
//  Case pack metadata was being converted to Int16 erroneously

#endregion

#region Version History: CCM811

// V8-30352 : A.Probyn
//  Updated OnCalculateMetadata so that the white space is calculated at a cumulative merchandising space level, 
//  not as a total for the planogram. Components which are overfaced were masking white space on other shelves.
// V8-30398 : A.Silva
//  Amended MetaTotalFacings to use PlanogramHelper.CalculateMetaTotalFacings (that accounts for tray real facings).

#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
// V8-32588 : J.Pickup
//  Added Made MetaIsOutsideOfFixtureArea now takes into account the assembly as a world relative point, rather than just relative to the assembly.
// V8-32522 : A.Silva
//  Exposed IsEntirelyOutsideParentFixture as part of the IPlanogramFixtureComponent interface.
// CCM-13761 : L.Ineson
//  Added GetBoundaryInfo for improved collision detection.
// CCM-13565 : L.Ineson
//  EnumerateComponent collisions now checks against subcomponent bounds.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using System.Diagnostics;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramAssemblyComponent object
    /// (Child of  PlanogramAssembly)
    /// 
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAssemblyComponent : ModelObject<PlanogramAssemblyComponent>, IPlanogramFixtureComponent
    {
        #region Parent

        /// <summary>
        /// Returns the parent planogram assembly of this.
        /// </summary>
        public new PlanogramAssembly Parent
        {
            get
            {
                PlanogramAssemblyComponentList parentList = base.Parent as PlanogramAssemblyComponentList;
                if (parentList != null)
                {
                    return parentList.Parent as PlanogramAssembly;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region PlanogramComponentId
        public static readonly ModelPropertyInfo<Object> PlanogramComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramComponentId);
        /// <summary>
        /// The PlanogramAssemblyComponent PlanogramComponentId
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object PlanogramComponentId
        {
            get { return GetProperty<Object>(PlanogramComponentIdProperty); }
            set { SetProperty<Object>(PlanogramComponentIdProperty, value); }
        }
        #endregion

        #region ComponentSequenceNumber

        /// <summary>
        ///		Metadata for the <see cref="ComponentSequenceNumber"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int16?> ComponentSequenceNumberProperty =
            RegisterModelProperty<Int16?>(o => o.ComponentSequenceNumber, Message.PlanogramAssemblyComponent_ComponentSequenceNumber);

        /// <summary>
        ///		Gets or sets the sequence number (order) for this instance.
        /// </summary>
        public Int16? ComponentSequenceNumber
        {
            get { return this.GetProperty<Int16?>(ComponentSequenceNumberProperty); }
            set { this.SetProperty<Int16?>(ComponentSequenceNumberProperty, value); }
        }

        #endregion

        #region X
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);
        /// <summary>
        /// The PlanogramAssemblyComponent X
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);
        /// <summary>
        /// The PlanogramAssemblyComponent Y
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z);
        /// <summary>
        /// The PlanogramAssemblyComponent Z
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope);
        /// <summary>
        /// The PlanogramAssemblyComponent Slope
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle);
        /// <summary>
        /// The PlanogramAssemblyComponent Angle
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll);
        /// <summary>
        /// The PlanogramAssemblyComponent Roll
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region NotchNumber
        /// <summary>
        /// NotchNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> NotchNumberProperty =
            RegisterModelProperty<Int32?>(c => c.NotchNumber);

        public Int32? NotchNumber
        {
            get { return this.GetProperty<Int32?>(NotchNumberProperty); }
            set { this.SetProperty<Int32?>(NotchNumberProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsPlacedProperty, value); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
            set { this.SetProperty<Int32?>(MetaNewProductsProperty, value); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
            set { this.SetProperty<Int32?>(MetaChangesFromPreviousCountProperty, value); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
            set { this.SetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, value); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
            set { this.SetProperty<Int32?>(MetaBlocksDroppedProperty, value); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Int32?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFacings
        /// <summary>
        /// MetaAverageFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaAverageFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaAverageFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFacings
        /// </summary>
        public Int16? MetaAverageFacings
        {
            get { return this.GetProperty<Int16?>(MetaAverageFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaAverageFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }
        #endregion

        #region MetaAverageUnits
        /// <summary>
        /// MetaAverageUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaAverageUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaAverageUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageUnits
        /// </summary>
        public Int32? MetaAverageUnits
        {
            get { return this.GetProperty<Int32?>(MetaAverageUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaAverageUnitsProperty, value); }
        }
        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
            set { this.SetProperty<Single?>(MetaMinDosProperty, value); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
            set { this.SetProperty<Single?>(MetaMaxDosProperty, value); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
            set { this.SetProperty<Single?>(MetaAverageDosProperty, value); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
            set { this.SetProperty<Single?>(MetaMinCasesProperty, value); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
            set { this.SetProperty<Single?>(MetaAverageCasesProperty, value); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
            set { this.SetProperty<Single?>(MetaMaxCasesProperty, value); }
        }
        #endregion

        #region MetaSpaceToUnitsIndex
        /// <summary>
        /// MetaSpaceToUnitsIndex property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaSpaceToUnitsIndexProperty =
            RegisterModelProperty<Int16?>(c => c.MetaSpaceToUnitsIndex);
        /// <summary>
        /// Gets/Sets the meta data MetaSpaceToUnitsIndex
        /// </summary>
        public Int16? MetaSpaceToUnitsIndex
        {
            get { return this.GetProperty<Int16?>(MetaSpaceToUnitsIndexProperty); }
            set { this.SetProperty<Int16?>(MetaSpaceToUnitsIndexProperty, value); }
        }
        #endregion

        #region MetaIsComponentSlopeWithNoRiser
        /// <summary>
        /// MetaIsComponentSlopeWithNoRiser property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsComponentSlopeWithNoRiserProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsComponentSlopeWithNoRiser);
        /// <summary>
        /// Gets/Sets the meta data MetaIsComponentSlopeWithNoRiser
        /// </summary>
        public Boolean? MetaIsComponentSlopeWithNoRiser
        {
            get { return this.GetProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty); }
            set { this.SetProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedDepth
        /// <summary>
        /// MetaIsOverMerchandisedDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedDepthProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedDepth);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedDepth
        /// </summary>
        public Boolean? MetaIsOverMerchandisedDepth
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedHeight
        /// <summary>
        /// MetaIsOverMerchandisedHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedHeightProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedHeight);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedHeight
        /// </summary>
        public Boolean? MetaIsOverMerchandisedHeight
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty, value); }
        }
        #endregion

        #region MetaIsOverMerchandisedWidth
        /// <summary>
        /// MetaIsOverMerchandisedWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMerchandisedWidthProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMerchandisedWidth);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMerchandisedWidth
        /// </summary>
        public Boolean? MetaIsOverMerchandisedWidth
        {
            get { return this.GetProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty, value); }
        }
        #endregion

        #region MetaTotalComponentCollisions
        /// <summary>
        /// MetaTotalComponentCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentCollisions
        /// </summary>
        public Int32? MetaTotalComponentCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalPositionCollisions
        /// <summary>
        /// MetaTotalPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalPositionCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalPositionCollisions
        /// </summary>
        public Int32? MetaTotalPositionCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalPositionCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalPositionCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, Message.PlanogramAssemblyComponent_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaTotalFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, Message.PlanogramAssemblyComponent_MetaAverageFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
            set { this.SetProperty<Single?>(MetaAverageFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaPercentageLinearSpaceFilled
        /// <summary>
        /// MetaPercentageLinearSpaceFilled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageLinearSpaceFilledProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageLinearSpaceFilled, Message.PlanogramAssemblyComponent_MetaPercentageLinearSpaceFilled, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPercentageLinearSpaceFilled
        /// </summary>
        public Single? MetaPercentageLinearSpaceFilled
        {
            get { return this.GetProperty<Single?>(MetaPercentageLinearSpaceFilledProperty); }
            set { this.SetProperty<Single?>(MetaPercentageLinearSpaceFilledProperty, value); }
        }
        #endregion

        #region MetaWorldX
        /// <summary>
        /// MetaWorldX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldXProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldX, Message.PlanogramAssemblyComponent_MetaWorldX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldX
        /// </summary>
        public Single? MetaWorldX
        {
            get { return this.GetProperty<Single?>(MetaWorldXProperty); }
            set { this.SetProperty<Single?>(MetaWorldXProperty, value); }
        }
        #endregion

        #region MetaWorldY
        /// <summary>
        /// MetaWorldY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldYProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldY, Message.PlanogramAssemblyComponent_MetaWorldY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldY
        /// </summary>
        public Single? MetaWorldY
        {
            get { return this.GetProperty<Single?>(MetaWorldYProperty); }
            set { this.SetProperty<Single?>(MetaWorldYProperty, value); }
        }
        #endregion

        #region MetaWorldZ
        /// <summary>
        /// MetaWorldZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldZProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldZ, Message.PlanogramAssemblyComponent_MetaWorldZ, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldZ
        /// </summary>
        public Single? MetaWorldZ
        {
            get { return this.GetProperty<Single?>(MetaWorldZProperty); }
            set { this.SetProperty<Single?>(MetaWorldZProperty, value); }
        }
        #endregion

        #region MetaWorldAngle
        /// <summary>
        /// MetaWorldAngle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldAngleProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldAngle, Message.PlanogramAssemblyComponent_MetaWorldAngle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldAngle
        /// </summary>
        public Single? MetaWorldAngle
        {
            get { return this.GetProperty<Single?>(MetaWorldAngleProperty); }
            set { this.SetProperty<Single?>(MetaWorldAngleProperty, value); }
        }
        #endregion

        #region MetaWorldSlope
        /// <summary>
        /// MetaWorldSlope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldSlopeProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldSlope, Message.PlanogramAssemblyComponent_MetaWorldSlope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldSlope
        /// </summary>
        public Single? MetaWorldSlope
        {
            get { return this.GetProperty<Single?>(MetaWorldSlopeProperty); }
            set { this.SetProperty<Single?>(MetaWorldSlopeProperty, value); }
        }
        #endregion

        #region MetaWorldRoll
        /// <summary>
        /// MetaWorldRoll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldRollProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldRoll, Message.PlanogramAssemblyComponent_MetaWorldRoll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldRoll
        /// </summary>
        public Single? MetaWorldRoll
        {
            get { return this.GetProperty<Single?>(MetaWorldRollProperty); }
            set { this.SetProperty<Single?>(MetaWorldRollProperty, value); }
        }
        #endregion

        #region MetaIsOutsideOfFixtureArea
        /// <summary>
        /// MetaIsOutsideOfFixtureArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOutsideOfFixtureAreaProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOutsideOfFixtureArea, Message.PlanogramAssemblyComponent_MetaIsOutsideOfFixtureArea);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOutsideOfFixtureArea
        /// </summary>
        public Boolean? MetaIsOutsideOfFixtureArea
        {
            get { return this.GetProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty, value); }
        }
        #endregion

        #endregion

        #endregion

        #region Authorisation Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(PlanogramComponentIdProperty));
            BusinessRules.AddRule(new Required(XProperty));
            BusinessRules.AddRule(new Required(ZProperty));
            BusinessRules.AddRule(new Required(SlopeProperty));
            BusinessRules.AddRule(new Required(AngleProperty));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssemblyComponent NewPlanogramAssemblyComponent()
        {
            PlanogramAssemblyComponent item = new PlanogramAssemblyComponent();
            item.Create(null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssemblyComponent NewPlanogramAssemblyComponent(PlanogramComponent component)
        {
            PlanogramAssemblyComponent item = new PlanogramAssemblyComponent();
            item.Create(component);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramComponent component)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            if (component != null) this.LoadProperty<Object>(PlanogramComponentIdProperty, component.Id);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramAssemblyComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramComponentId
            Object planogramComponentId = context.ResolveId<PlanogramComponent>(this.ReadProperty<Object>(PlanogramComponentIdProperty));
            if (planogramComponentId != null) this.LoadProperty<Object>(PlanogramComponentIdProperty, planogramComponentId);
        }

        /// <summary>
        /// Returns the planogram component
        /// </summary>
        public PlanogramComponent GetPlanogramComponent()
        {
            PlanogramAssembly assembly = this.Parent;
            if (assembly == null) return null;

            Planogram planogram = assembly.Parent;
            if (planogram == null) return null;

            return planogram.Components.FindById(this.PlanogramComponentId);
        }

        /// <summary>
        /// Returns all sub component placements
        /// associated to this assembly component
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            PlanogramAssembly assembly = this.Parent;
            if (assembly == null) yield break;
            Planogram planogram = assembly.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.AssemblyComponent))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        /// <summary>
        /// Returns the value of the given field for this position.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field, 
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            return field.GetValue(this);
        }

        #region Placement Related

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTransform(
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, Boolean includeLocal)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //component
            if (includeLocal)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        this.X, this.Y, this.Z,
                        this.Angle, this.Slope, this.Roll));
            }

            //Assembly
            if (fixtureAssembly != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureAssembly.X, fixtureAssembly.Y, fixtureAssembly.Z,
                        fixtureAssembly.Angle, fixtureAssembly.Slope, fixtureAssembly.Roll));
            }

            //Fixture
            if (fixtureItem != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixtureItem.Angle, fixtureItem.Slope, fixtureItem.Roll));
            }


            return relativeTransform;
        }

        /// <summary>
        /// Returns the rotation for this component relative to the planogram
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private RotationValue GetPlanogramRelativeRotation(
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, Boolean includeLocal)
        {
            RotationValue rotation = new RotationValue();

            //Component
            if (includeLocal)
            {
                rotation.Angle += this.Angle;
                rotation.Slope += this.Slope;
                rotation.Roll += this.Roll;
            }

            //Assembly
            if (fixtureAssembly != null)
            {
                rotation.Angle += fixtureAssembly.Angle;
                rotation.Slope += fixtureAssembly.Slope;
                rotation.Roll += fixtureAssembly.Roll;
            }

            //Fixture
            if (fixtureItem != null)
            {
                rotation.Angle += fixtureItem.Angle;
                rotation.Slope += fixtureItem.Slope;
                rotation.Roll += fixtureItem.Roll;
            }

            return rotation;
        }

        /// <summary>
        /// Sets the position of this item based on
        /// the position of the provided item
        /// </summary>
        public void SetCoordinates(PlanogramFixtureComponent source)
        {
            this.X = source.X;
            this.Y = source.Y;
            this.Z = source.Z;
            this.Slope = source.Slope;
            this.Angle = source.Angle;
            this.Roll = source.Roll;
        }

        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates(
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            PointValue localCoordinates = new PointValue(this.X, this.Y, this.Z);
            if (fixtureItem == null || fixtureAssembly == null) return localCoordinates;

            //get the relative transform
            MatrixValue relativeTransform = 
                GetPlanogramRelativeTransform(fixtureItem, fixtureAssembly, /*incLocal*/false);

            //trasnform and return.
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            //get the inverted relative transform
            MatrixValue relativeTransform = 
                GetPlanogramRelativeTransform(fixtureItem,fixtureAssembly, /*incLocal*/false);

            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);

            this.X = newLocalCoordinate.X;
            this.Y = newLocalCoordinate.Y;
            this.Z = newLocalCoordinate.Z;
        }

        /// <summary>
        /// Returns the rotation of this component relative to the planogram as a whole.
        /// </summary>
        public RotationValue GetPlanogramRelativeRotation(
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            return GetPlanogramRelativeRotation(fixtureItem, fixtureAssembly, /*incLocal*/true);
        }

        /// <summary>
        /// Sets the local rotation values based on the given world rotation.
        /// </summary>
        /// <param name="worldRotation"></param>
        public void SetRotationFromPlanogramRelative(RotationValue worldRotation,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            //get the parent relative rotation
            RotationValue parentRotation =
                GetPlanogramRelativeRotation(fixtureItem, fixtureAssembly, /*incLocal*/false);

            //strip away the parent rotation to get the new local values.
            this.Angle = worldRotation.Angle - parentRotation.Angle;
            this.Slope = worldRotation.Slope - parentRotation.Slope;
            this.Roll = worldRotation.Roll - parentRotation.Roll;
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetPlanogramRelativeBoundingBox(
            PlanogramComponent component, PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            RectValue bounds = new RectValue();
            bounds.Width = component.Width;
            bounds.Height = component.Height;
            bounds.Depth = component.Depth;

            //create the relative transform.
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem,fixtureAssembly, /*incLocal*/true);
            
            return relativeTransform.TransformBounds(bounds);
        }

        /// <summary>
        /// Returns static boundary information for this component placement.
        /// </summary>
        /// <param name="component">the associated component</param>
        /// <param name="fixtureAssembly"the parent fixture assembly
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <returns>Boundary information</returns>
        public BoundaryInfo GetBoundaryInfo(PlanogramComponent component, PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            return new BoundaryInfo(
                component.Height, component.Width, component.Depth,
               GetPlanogramRelativeTransform(fixtureItem, fixtureAssembly, /*incLocal*/true));
        }

        /// <summary>
        /// Converts the given local point to a planogram relative one.
        /// </summary>
        public static PointValue ToWorld(PointValue localPoint,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, PlanogramAssemblyComponent assemblyComponent)
        {
            //get the relative transform
            MatrixValue relativeTransform =
                assemblyComponent.GetPlanogramRelativeTransform(fixtureItem, fixtureAssembly,/*incLocal*/true);

            //trasnform and return.
            PointValue worldPoint = localPoint.Transform(relativeTransform);
            return worldPoint;
        }

        /// <summary>
        /// Converts the given world point to one which is local
        /// to this subcomponent placement.
        /// </summary>
        public static PointValue ToLocal(PointValue worldPoint,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly, PlanogramAssemblyComponent assemblyComponent)
        {
            //get the relative transform
            MatrixValue relativeTransform = 
                assemblyComponent.GetPlanogramRelativeTransform(fixtureItem, fixtureAssembly,/*incLocal*/true);

            //invert the transform.
            relativeTransform.Invert();

            return worldPoint.Transform(relativeTransform);
        }


        /// <summary>
        /// Returns a notched subcomponent adjacent to the given component.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        private PlanogramSubComponentPlacement GetAdjacentNotchedSubComponent(PlanogramComponent component,
            PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem)
        {
            RectValue componentWorldBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem, fixtureAssembly);
            RotationValue componentWorldRotation = GetPlanogramRelativeRotation(fixtureItem, fixtureAssembly);

            PlanogramSubComponentPlacement returnSub = null;

            foreach (PlanogramSubComponentPlacement subPlacement in fixtureItem.GetPlanogramSubComponentPlacements())
            {
                //only assess the sub if it is not a child of the component
                if (component == subPlacement.Component) continue;

                PlanogramSubComponent sub = subPlacement.SubComponent;

                // only  if it has notch values and if the rotation is the same.
                if (sub.NotchHeight <= 0 || sub.NotchWidth <= 0
                 || !(sub.IsNotchPlacedOnFront || sub.IsNotchPlacedOnBack || sub.IsNotchPlacedOnLeft || sub.IsNotchPlacedOnRight))
                {
                    continue;
                }

                RectValue subWorldBounds = subPlacement.GetPlanogramRelativeBoundingBox();

                //Front Notches
                if (sub.IsNotchPlacedOnFront
                    && componentWorldRotation == subPlacement.GetPlanogramRelativeRotation())
                {
                    //check that the z placements meet
                    if ((subWorldBounds.Z + subWorldBounds.Depth) == componentWorldBounds.Z
                        && componentWorldBounds.IntersectsWith(subWorldBounds))
                    {
                        returnSub = subPlacement;
                        break;
                    }
                }

                //Back Notches
                if (sub.IsNotchPlacedOnBack)
                {
                    //check that the rotation of the component snaps it to the back of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*-180deg*/Convert.ToSingle(-3.1415926536);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check the z placement
                        if (subWorldBounds.Z == componentWorldBounds.Z
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }

                //Left Notches
                if (sub.IsNotchPlacedOnLeft)
                {
                    //check that the rotation of the component snaps it to the left of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*-90deg*/Convert.ToSingle(-1.5707963268);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check x placement
                        if (subWorldBounds.X == componentWorldBounds.X
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }

                //Right Notches
                if (sub.IsNotchPlacedOnRight)
                {
                    //check that the rotation of the component snaps it to the right of this sub.
                    RotationValue expectedRotation = subPlacement.GetPlanogramRelativeRotation();
                    expectedRotation.Angle += /*90deg*/Convert.ToSingle(1.5707963268);
                    if (componentWorldRotation == expectedRotation)
                    {
                        //check x placement
                        if ((subWorldBounds.X + subWorldBounds.Width) == componentWorldBounds.X
                            && componentWorldBounds.IntersectsWith(subWorldBounds))
                        {
                            returnSub = subPlacement;
                            break;
                        }
                    }
                }
            }

            return returnSub;
        }

        /// <summary>
        /// Returns the notch number of the given component.
        /// </summary>
        public Int32? GetNotchNumber(PlanogramComponent component, PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem)
        {
            Int32? notchNumber = null;

            PlanogramSubComponentPlacement snappedToSub = GetAdjacentNotchedSubComponent(component, fixtureAssembly, fixtureItem);
            if (snappedToSub != null)
            {
                RectValue componentBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem, fixtureAssembly);

                PointValue componentWorldSnapPos =
                    new PointValue(componentBounds.X, componentBounds.Y + component.Height, componentBounds.Z);

                //localise the snap point relative to the sub.
                Single componentSnapPos = PlanogramSubComponentPlacement.ToLocal(componentWorldSnapPos, snappedToSub).Y;

                notchNumber = snappedToSub.SubComponent.GetNearestNotchNumber(componentSnapPos);
                LoadProperty<Int32?>(NotchNumberProperty, notchNumber);
            }

            return notchNumber;
        }

        /// <summary>
        /// Snaps the component to the given notch number of an
        /// the adjacent notched subcomponent
        /// </summary>
        public void SetNotchNumber(Int32 value, PlanogramComponent component, PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem)
        {
            PlanogramSubComponentPlacement subPlacement = GetAdjacentNotchedSubComponent(component, fixtureAssembly, fixtureItem);
            if (subPlacement == null) return;

            PlanogramSubComponent sub = subPlacement.SubComponent;

            Single? subRelativeNotchY = sub.GetNotchLocalYPosition(value);
            if (subRelativeNotchY.HasValue)
            {
                RectValue componentBounds = GetPlanogramRelativeBoundingBox(component, fixtureItem, fixtureAssembly);

                PointValue componentPosLocalToSub = PlanogramSubComponentPlacement.ToLocal(
                    new PointValue(componentBounds.X, componentBounds.Y, componentBounds.Z), subPlacement);

                componentPosLocalToSub.Y = subRelativeNotchY.Value;

                //transform the point back to world space.
                PointValue worldSpaceSnap = PlanogramSubComponentPlacement.ToWorld(componentPosLocalToSub, subPlacement);

                //remove the component height as we always snap to the top
                worldSpaceSnap.Y -= component.Height;

                this.Y = PlanogramAssemblyComponent.ToLocal(worldSpaceSnap, fixtureItem, fixtureAssembly, this).Y;
                LoadProperty<Int32?>(NotchNumberProperty, value);
            }
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            if (this.Parent == null) return;
            Planogram parentPlan = this.Parent.Parent;
            if (parentPlan == null) return;
            PlanogramPositionList positionList = parentPlan.Positions;
            if (positionList == null) return;

            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();


            //Get all positions assigned to this fixture assembly
            List<PlanogramPosition> positions = positionList.Where(p => this.Id.Equals(p.PlanogramAssemblyComponentId)).ToList();
            List<PlanogramPositionDetails> assemblyComponentPositionDetails = new List<PlanogramPositionDetails>();

            //Get all products assigned to the positions
            List<PlanogramProduct> products = new List<PlanogramProduct>();
            foreach (PlanogramPosition position in positions)
            {
                PlanogramPositionDetails positionDetails;
                if (metadataDetails.PositionDetails.TryGetValue(position.Id, out positionDetails))
                {
                    assemblyComponentPositionDetails.Add(positionDetails);
                }

                PlanogramProduct product = parentPlan.Products.FindById(position.PlanogramProductId);
                if (product != null)
                {
                    products.Add(product);
                }
            }

            PlanogramFixtureAssembly fixtureAssembly = this.Parent.EnumeratePlanogramFixtureAssemblies().FirstOrDefault();
            PlanogramFixtureItem fixtureItem = (fixtureAssembly != null) ?
                parentPlan.FixtureItems.FirstOrDefault(i => Object.Equals(i.PlanogramFixtureId, fixtureAssembly.Parent.Id)) : null;

            //Get component
            PlanogramComponent component = parentPlan.Components.FindById(this.PlanogramComponentId);
            if (component == null) return;

            this.MetaProductsPlaced = products.Count;
            this.MetaNewProducts = 0; //TODO
            this.MetaChangesFromPreviousCount = 0; //TODO
            this.MetaChangeFromPreviousStarRating = 0; //TODO
            this.MetaBlocksDropped = 0; //TODO
            this.MetaNotAchievedInventory = products.Any() ? products.Count(p => p.MetaNotAchievedInventory == true) : (Int16)0;
            this.MetaTotalFacings = PlanogramHelper.CalculateMetaTotalFacings(positions, products);
            this.MetaAverageFacings = Convert.ToInt16(positions.Count > 0 ? MetaTotalFacings / positions.Count : 0);
            this.MetaTotalUnits = positions.Sum(p => p.TotalUnits);
            this.MetaAverageUnits = positions.Any() ? Convert.ToInt32(positions.Average(p => p.TotalUnits)) : (Int32)0;
            this.MetaMinDos = 0; //TODO
            this.MetaMaxDos = 0; //TODO
            this.MetaAverageDos = 0; //TODO
            this.MetaMinCases = positions.Any() ? positions.Min(p => p.MetaAchievedCases) : 0;
            this.MetaAverageCases = positions.Any() ? positions.Average(p => p.MetaAchievedCases) : 0;
            this.MetaMaxCases = positions.Any() ? positions.Max(p => p.MetaAchievedCases) : 0;
            this.MetaSpaceToUnitsIndex = 0; //TODO
            this.MetaTotalFrontFacings = Convert.ToInt16(positions.Any() ? positions.Sum(p => p.MetaFrontFacingsWide) : 0);
            this.MetaAverageFrontFacings = Convert.ToSingle(positions.Any() ? Math.Round(((double)this.MetaTotalFrontFacings / positions.Count), 2, MidpointRounding.AwayFromZero) : 0);
            this.MetaIsComponentSlopeWithNoRiser = IsSlopedWithNoRiser(component);
            this.MetaIsOverMerchandisedDepth = assemblyComponentPositionDetails.Any(p => p.TotalSize.Depth > component.Depth);
            this.MetaIsOverMerchandisedHeight = assemblyComponentPositionDetails.Any(p => p.TotalSize.Height > component.Height);
            this.MetaIsOverMerchandisedWidth = assemblyComponentPositionDetails.Any(p => p.TotalSize.Width > component.Width);

            //Count all of the positions relating to this planogram assembly component that have a collision
            this.MetaTotalPositionCollisions = positions.Count(p => p.MetaIsPositionCollisions == true);

            this.MetaTotalComponentCollisions = EnumerateComponentCollisions(parentPlan.GetPlanogramMetadataDetails(), fixtureItem, fixtureAssembly).Count();

            //Get the merch space of all of the subcomponents in this assembly component.
            IEnumerable<PlanogramMerchandisingSpace> merchandisingSpaces = metadataDetails.GetAssemblyComponentMerchandisingSpace(this, fixtureAssembly, fixtureItem);
            this.MetaTotalMerchandisableLinearSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableLinear()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableAreaSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableArea()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableVolumetricSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableVolume()), 2, MidpointRounding.AwayFromZero);

            //V830352 - Calculate the white space at a cumulative merchandising space level, not as a total for the planogram.
            // Components which are overfaced mask white space on other shelves.
            Single totalPlanLinearWhiteSpace = 0, totalPlanAreaWhiteSpace = 0, totalPlanVolumetricWhiteSpace = 0;
            Int32 numberOfMerchSpaces = merchandisingSpaces.Count();
            foreach (PlanogramMerchandisingSpace merchSpace in merchandisingSpaces)
            {
                if (merchSpace.SubComponentPlacement != null)
                {
                    //Get all positions in this merch space
                    IEnumerable<PlanogramPosition> merchSpacePositions = merchSpace.SubComponentPlacement.GetPlanogramPositions();

                    //Get position details
                    List<PlanogramPositionDetails> merchSpacePositionDetails = new List<PlanogramPositionDetails>();
                    foreach (PlanogramPosition pos in merchSpacePositions)
                    {
                        PlanogramPositionDetails posDetails;
                        if (metadataDetails.PositionDetails.TryGetValue(pos.Id, out posDetails))
                        {
                            merchSpacePositionDetails.Add(posDetails);
                        }
                    }

                    // The total width of white space. That is, the difference between the sum of the widths
                    // of all the positions at the front of the plan and the total width of merchandising space of
                    // all of the subcomponents.
                    totalPlanLinearWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableLinear(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width)).Value / numberOfMerchSpaces);

                    // The total area of white space. That is, the difference between the sum of all the positions
                    // that are at the front of the plan and the sum of the merchandisable areas of all the subcomponents.
                    totalPlanAreaWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableArea(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height)).Value / numberOfMerchSpaces);

                    // The total volume of white space. That is, the difference between the sum of the volumes 
                    // of all the positions and the sum of the merchandisable volumes of all the subcomponents.
                    totalPlanVolumetricWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableVolume(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height * p.TotalSize.Depth)).Value / numberOfMerchSpaces);
                }
            }

            //Set cumulative values to be totals
            this.MetaTotalLinearWhiteSpace = (Single?)Math.Round(totalPlanLinearWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalAreaWhiteSpace = (Single?)Math.Round(totalPlanAreaWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalVolumetricWhiteSpace = (Single?)Math.Round(totalPlanVolumetricWhiteSpace, 2, MidpointRounding.AwayFromZero);
            
            this.MetaPercentageLinearSpaceFilled = PlanogramHelper.CalculateMetaPercentageLinearSpaceFilled(
                this.MetaTotalMerchandisableLinearSpace, assemblyComponentPositionDetails.Sum(p => p.TotalSize.Width));

            //Planogram relative placement:
            PointValue worldCoordinates = GetPlanogramRelativeCoordinates(fixtureItem, fixtureAssembly);
            this.MetaWorldX = worldCoordinates.X;
            this.MetaWorldY = worldCoordinates.Y;
            this.MetaWorldZ = worldCoordinates.Z;
            RotationValue worldRotation = GetPlanogramRelativeRotation(fixtureItem, fixtureAssembly);
            this.MetaWorldAngle = worldRotation.Angle;
            this.MetaWorldSlope = worldRotation.Slope;
            this.MetaWorldRoll = worldRotation.Roll;

            this.MetaIsOutsideOfFixtureArea = this.IsEntirelyOutsideParentFixture();
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            this.MetaTotalMerchandisableLinearSpace = null;
            this.MetaTotalMerchandisableAreaSpace = null;
            this.MetaTotalMerchandisableVolumetricSpace = null;
            this.MetaTotalLinearWhiteSpace = null;
            this.MetaTotalAreaWhiteSpace = null;
            this.MetaTotalVolumetricWhiteSpace = null;
            this.MetaProductsPlaced = null;
            this.MetaNewProducts = null;
            this.MetaChangesFromPreviousCount = null;
            this.MetaChangeFromPreviousStarRating = null;
            this.MetaBlocksDropped = null;
            this.MetaNotAchievedInventory = null;
            this.MetaTotalFacings = null;
            this.MetaAverageFacings = null;
            this.MetaTotalUnits = null;
            this.MetaAverageUnits = null;
            this.MetaMinDos = null;
            this.MetaMaxDos = null;
            this.MetaAverageDos = null;
            this.MetaMinCases = null;
            this.MetaAverageCases = null;
            this.MetaMaxCases = null;
            this.MetaSpaceToUnitsIndex = null;
            this.MetaTotalFrontFacings = null;
            this.MetaAverageFrontFacings = null;
            this.MetaIsComponentSlopeWithNoRiser = null;
            this.MetaIsOverMerchandisedDepth = null;
            this.MetaIsOverMerchandisedHeight = null;
            this.MetaIsOverMerchandisedWidth = null;
            this.MetaTotalPositionCollisions = null;
            this.MetaTotalComponentCollisions = null;
            this.MetaPercentageLinearSpaceFilled = null;
            this.MetaWorldX = null;
            this.MetaWorldY = null;
            this.MetaWorldZ = null;
            this.MetaWorldAngle = null;
            this.MetaWorldSlope = null;
            this.MetaWorldRoll = null;
            this.MetaIsOutsideOfFixtureArea = null;
        }

        public Boolean? IsEntirelyOutsideParentFixture()
        {
            //Get fixture rect 
            Planogram myPlan = this.Parent.Parent;
            PlanogramFixtureAssembly planAssembly = null;

            foreach (PlanogramFixtureItem fixItem in myPlan.FixtureItems)
            {
                PlanogramFixture fixture = fixItem.GetPlanogramFixture();
                planAssembly = fixture.Assemblies.FirstOrDefault(a => a.PlanogramAssemblyId.Equals(this.Parent.Id));

                if (planAssembly != null) break;
            }

            if (planAssembly == null) return null;
            PlanogramFixture myDamnFixture = planAssembly.Parent;
            PlanogramFixtureItem parentFixtureItem = myDamnFixture.Parent.FixtureItems.FirstOrDefault(f => f.PlanogramFixtureId.Equals(myDamnFixture.Id));

            WidthHeightDepthValue fixtureCompSpace = new WidthHeightDepthValue(myDamnFixture.Width, myDamnFixture.Height, myDamnFixture.Depth);
            PointValue fixtureCompPoint = new PointValue(parentFixtureItem.X, parentFixtureItem.Y, parentFixtureItem.Z);
            RectValue fixtureCompBoundingBox = new RectValue(fixtureCompPoint, fixtureCompSpace);

            //Get assembly component rect 
            PlanogramAssemblyComponent assemComponent = this;
            PlanogramComponent planComp = assemComponent.GetPlanogramComponent();
            RectValue assemCompSpace = assemComponent.GetPlanogramRelativeBoundingBox(planComp, parentFixtureItem, planAssembly);

            //Check intersect
            if (!assemCompSpace.IntersectsWith(fixtureCompBoundingBox)) return true;

            return false;
        }

        /// <summary>
        /// Returns true if this component is sloped but has no riser.
        /// </summary>
        public Boolean IsSlopedWithNoRiser(PlanogramComponent component)
        {
            Debug.Assert(Object.Equals(component.Id, this.PlanogramComponentId), "Incorrect component");

            //if this has no slope return false
            if (this.Slope == 0) return false;

            Boolean allSubsHaveFrontRiser = component.SubComponents.All(p => p.IsRiserPlacedOnFront);

            return !allSubsHaveFrontRiser;
        }

        /// <summary>
        /// Enumerates through collisions between this component and others on the plan.
        /// </summary>
        /// <param name="metadataDetails"></param>
        /// <param name="fixtureItem"></param>
        /// <returns></returns>
        internal IEnumerable<PlanogramComponentPlacementId> EnumerateComponentCollisions(PlanogramMetadataDetails metadataDetails,
            PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            PlanogramComponentPlacementId thisId = new PlanogramComponentPlacementId(this, fixtureAssembly, fixtureItem, metadataDetails.Planogram);

            var childPlacements = metadataDetails.SubPlacementInfosByComponent[thisId];

            //group all placement infos by component id and cycle through
            foreach (var componentPlacementIdGroup in metadataDetails.SubPlacementInfosByComponent)
            {
                //skip if this component.
                if (componentPlacementIdGroup.Key == thisId) continue;

                Boolean collisionFound = false;

                //cycle through all subs in the component
                foreach (var otherPlacement in componentPlacementIdGroup)
                {
                    //checking for collisions against each sub on this one.
                    foreach (var child in childPlacements)
                    {
                        if (child.WorldBoundary.CollidesWith(otherPlacement.WorldBoundary))
                        {
                            //returning the component id if a collision is found.
                            yield return new PlanogramComponentPlacementId(otherPlacement.Placement);

                            collisionFound = true;
                            break;
                        }
                    }

                    //break out if we have already found a collision with this component
                    // as we only need to return it once.
                    if (collisionFound) break;
                }
            }
            
        }

        /// <summary>
        ///     Check whether the component is overfilled or not.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Boolean IsOverfilled(PlanogramMetadataDetails context)
        {
            return
                 this.GetPlanogramSubComponentPlacements()
                 .Any(s => context.IsMerchandisingGroupOverfilled(s.Id));
        }

        #endregion

        #endregion
    }
}