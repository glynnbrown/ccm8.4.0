﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-27439 : A.Kuszyk
//  Added Re-calculate blocking methods.
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of BlockingLocations contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramBlockingLocationList : ModelList<PlanogramBlockingLocationList, PlanogramBlockingLocation>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get { return (PlanogramBlocking)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingLocationList NewPlanogramBlockingLocationList()
        {
            PlanogramBlockingLocationList item = new PlanogramBlockingLocationList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingLocationList NewPlanogramBlockingLocationList(IEnumerable<IPlanogramBlockingLocation> sourceList, IResolveContext context)
        {
            PlanogramBlockingLocationList item = new PlanogramBlockingLocationList();
            item.Create(sourceList, context);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IEnumerable<IPlanogramBlockingLocation> sourceList, IResolveContext context)
        {
            foreach (var item in sourceList)
            {
                this.Add(PlanogramBlockingLocation.NewPlanogramBlockingLocation(item, context));
            }

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new blocking location for the given group
        /// </summary>
        /// <param name="group">The blocking group to add a new location for.</param>
        /// <returns>The new location</returns>
        public PlanogramBlockingLocation Add(PlanogramBlockingGroup group)
        {
            PlanogramBlockingLocation item = PlanogramBlockingLocation.NewPlanogramBlockingLocation(group);
            this.Add(item);
            return item;
        }

        #endregion
    }
}
