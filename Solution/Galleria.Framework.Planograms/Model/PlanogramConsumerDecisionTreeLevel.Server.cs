﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong
//	Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-30956 : N.Foster
//  Fixed issue where ids are not resolved at the right time
//  due to the way the BatchSaveContext works
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeLevel
    {
        #region Constructor
        private PlanogramConsumerDecisionTreeLevel() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramConsumerDecisionTreeLevel FetchPlanogramConsumerDecisionTreeLevel(
            IDalContext dalContext,
            PlanogramConsumerDecisionTreeLevelDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeLevelDto> treeLevelList)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTreeLevel>(dalContext, dto, treeLevelList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramConsumerDecisionTreeLevelDto GetDataTransferObject(PlanogramConsumerDecisionTreeLevel parent)
        {
            return new PlanogramConsumerDecisionTreeLevelDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramConsumerDecisionTreeId = this.ParentPlanogramConsumerDecisionTree.Id,
                Name = this.ReadProperty<String>(NameProperty),
                ParentLevelId = (parent != null) ? parent.Id : null,
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramConsumerDecisionTreeLevelDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeLevelDto> treeLevelList)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);

            //check if there is a child level to this
            PlanogramConsumerDecisionTreeLevelDto childLevelDto = treeLevelList.FirstOrDefault(l => Object.Equals(l.ParentLevelId,this.Id));
            if (childLevelDto != null)
            {
                this.ChildLevel = PlanogramConsumerDecisionTreeLevel.FetchPlanogramConsumerDecisionTreeLevel(dalContext, childLevelDto, treeLevelList);
                MarkClean();
            }

            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeLevelDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeLevelDto> treeLevelList)
        {
            this.LoadDataTransferObject(dalContext, dto, treeLevelList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeLevelDto>(
            (dc) =>
            {
                PlanogramConsumerDecisionTreeLevelDto dto = this.GetDataTransferObject(null);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTreeLevel>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeLevel parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeLevelDto>(
            (dc) =>
            {
                PlanogramConsumerDecisionTreeLevelDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTreeLevel>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeLevelDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(null);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeLevel parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeLevelDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            // delete the level
            batchContext.Delete<PlanogramConsumerDecisionTreeLevelDto>(this.GetDataTransferObject(null));

            //mark any child levels of this as deleted
            foreach (PlanogramConsumerDecisionTreeLevel child in this.ChildLevelList)
            {
                //forcibly mark the child as deleted
                ((Csla.Core.IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeLevel parent)
        {
            // delete the level
            batchContext.Delete<PlanogramConsumerDecisionTreeLevelDto>(this.GetDataTransferObject(parent));

            //mark any child levels of this as deleted
            foreach (PlanogramConsumerDecisionTreeLevel child in this.ChildLevelList)
            {
                //forcibly mark the child as deleted
                ((Csla.Core.IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #endregion
    }
}
