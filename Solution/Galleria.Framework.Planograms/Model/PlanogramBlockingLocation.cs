﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-27403 : A.Kuszyk
//  Ensured car park shelves won't be picked up as snappable.
// V8-27467 : A.Kuszyk
//  Adjusted the way snapping boundaries are selected.
// V8-27583 : A.Kuszyk
//  Added minimum threshold for fixture component overlaps.
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
// V8-27858 : A.Kuszyk
//  Added GetDesignViewPosition method.
// V8-27509 : A.Kuszyk
//  Re-factored GetMerchandisableFixtureComponents to GetMerchandisableFixtureAndAssemblyComponents
//  in order to pick-up assembly components.
#endregion
#region Version History: (CCM 810)
//  V8-29022 : M.Brumby
//      Modified UpdateHelperProperties so that the SpacePercentage is set instead
//      of loaded on the first instance to ensure initial overall blocking % are 
//      passed through to the UI.
#endregion
#region Version History: (CCM 820)
// V8-31393 : L.Ineson
//  Amended how notifications are passed around
#endregion
#region Version History : CCM830
// V8-32449 : A.Kuszyk
//  Removed rounding to 5 decimal places as this was causing accuracy issues elsewhere.
// V8-32539 : A.Kuszyk
//  Added GetOppositeDividerTo method.
// V8-32660 : A.Kuszyk
//  Added GetPlanogramBlockingDividers method.
// V8-32870 : A.Kuszyk
//  Implemented IPlanogramSequencableItem.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a Planogram Blocking Location object
    /// </summary>
    [Serializable]
    [DebuggerDisplay("XY: ({X}, {Y}), WH: ({Width}, {Height})")]
    public sealed partial class PlanogramBlockingLocation : ModelObject<PlanogramBlockingLocation>, IPlanogramBlockingLocation, IPlanogramSequencableItem
    {
        #region Constants
        private const Single _fixtureOverlapThreshold = 0.001f;
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get 
            {
                PlanogramBlockingLocationList parentList = base.Parent as PlanogramBlockingLocationList;
                if(parentList == null) return null;

                return parentList.Parent; 
            }
        }
        #endregion

        #region Properties

        #region PlanogramBlockingGroupId
        /// <summary>
        /// PlanogramBlockingGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramBlockingGroupIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramBlockingGroupId);
        /// <summary>
        /// The Location PlanogramBlockingGroupId
        /// </summary>
        public Object PlanogramBlockingGroupId
        {
            get { return GetProperty<Object>(PlanogramBlockingGroupIdProperty); }
            set 
            {
                Object currentValue = this.ReadProperty<Object>(PlanogramBlockingGroupIdProperty);
                if (value != currentValue)
                {
                    SetProperty<Object>(PlanogramBlockingGroupIdProperty, value);
                    
                    //notify both groups to update their space percentage values.
                    PlanogramBlocking parentBlocking = this.Parent;
                    if(parentBlocking != null)
                    {
                        if (currentValue != null)
                        {
                            PlanogramBlockingGroup oldGroup = parentBlocking.Groups.FindById(currentValue);
                            if (oldGroup != null) oldGroup.NotifyTotalSpacePercentageChanged();
                        }

                        PlanogramBlockingGroup newGroup = parentBlocking.Groups.FindById(value);
                        if (newGroup != null) newGroup.NotifyTotalSpacePercentageChanged();
                    }
                }
            }
        }
        #endregion

        #region PlanogramBlockingDividerTopId
        /// <summary>
        /// PlanogramBlockingDividerTopId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramBlockingDividerTopIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramBlockingDividerTopId);
        /// <summary>
        /// The Location PlanogramBlockingDividerTopId
        /// </summary>
        public Object PlanogramBlockingDividerTopId
        {
            get { return GetProperty<Object>(PlanogramBlockingDividerTopIdProperty); }
            set 
            { 
                SetProperty<Object>(PlanogramBlockingDividerTopIdProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region PlanogramBlockingDividerBottomId
        /// <summary>
        /// PlanogramBlockingDividerBottomId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramBlockingDividerBottomIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramBlockingDividerBottomId);
        /// <summary>
        /// The Location PlanogramBlockingDividerBottomId
        /// </summary>
        public Object PlanogramBlockingDividerBottomId
        {
            get { return GetProperty<Object>(PlanogramBlockingDividerBottomIdProperty); }
            set 
            { 
                SetProperty<Object>(PlanogramBlockingDividerBottomIdProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region PlanogramBlockingDividerLeftId
        /// <summary>
        /// PlanogramBlockingDividerLeftId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramBlockingDividerLeftIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramBlockingDividerLeftId);
        /// <summary>
        /// The Location PlanogramBlockingDividerLeftId
        /// </summary>
        public Object PlanogramBlockingDividerLeftId
        {
            get { return GetProperty<Object>(PlanogramBlockingDividerLeftIdProperty); }
            set 
            { 
                SetProperty<Object>(PlanogramBlockingDividerLeftIdProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region PlanogramBlockingDividerRightId
        /// <summary>
        /// PlanogramBlockingDividerRightId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramBlockingDividerRightIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramBlockingDividerRightId);
        /// <summary>
        /// The Location PlanogramBlockingDividerRightId
        /// </summary>
        public Object PlanogramBlockingDividerRightId
        {
            get { return GetProperty<Object>(PlanogramBlockingDividerRightIdProperty); }
            set 
            { 
                SetProperty<Object>(PlanogramBlockingDividerRightIdProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SpacePercentage

        /// <summary>
        /// SpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SpacePercentageProperty =
            RegisterModelProperty<Single>(c => c.SpacePercentage);

        /// <summary>
        /// Returns the percentage of space allocated to this location.
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single SpacePercentage
        {
            get {return GetProperty<Single>(SpacePercentageProperty);}
            private set
            {
                Single currentValue = this.ReadProperty<Single>(SpacePercentageProperty);
                if (currentValue != value)
                {
                    SetProperty<Single?>(SpacePercentageProperty, value);

                    //Notifiy the linked group to update its space percentage.
                    PlanogramBlockingGroup group = GetPlanogramBlockingGroup();
                    if (group != null)
                    {
                        group.NotifyTotalSpacePercentageChanged();
                    }
                }
            }
        }

        #endregion

        #region X (Calculated)

        public static readonly ModelPropertyInfo<Single?> XProperty =
             RegisterModelProperty<Single?>(c => c.X);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single X
        {
            get
            {
                Single? value = GetProperty<Single?>(XProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*isInitialLoad*/true);
                    return GetProperty<Single?>(XProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(XProperty, value); }
        }

        #endregion

        #region Y (Calculated)

        public static readonly ModelPropertyInfo<Single?> YProperty =
            RegisterModelProperty<Single?>(c => c.Y);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Y
        {
            get
            {
                Single? value = GetProperty<Single?>(YProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*isInitialLoad*/true);
                    return GetProperty<Single?>(YProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(YProperty, value); }
        }

        #endregion

        #region Width (Calculated)

        public static readonly ModelPropertyInfo<Single?> WidthProperty =
            RegisterModelProperty<Single?>(c => c.Width);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Width
        {
            get
            {
                Single? value = GetProperty<Single?>(WidthProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*isInitialLoad*/true);
                    return GetProperty<Single?>(WidthProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(WidthProperty, value); }
        }


        #endregion

        #region Height (Calculated)

        public static readonly ModelPropertyInfo<Single?> HeightProperty =
             RegisterModelProperty<Single?>(c => c.Height);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Height
        {
            get
            {
                Single? value = GetProperty<Single?>(HeightProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*isInitialLoad*/true);
                    return GetProperty<Single?>(HeightProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(HeightProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(PlanogramBlockingGroupIdProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingLocation NewPlanogramBlockingLocation()
        {
            PlanogramBlockingLocation item = new PlanogramBlockingLocation();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingLocation NewPlanogramBlockingLocation(PlanogramBlockingGroup group)
        {
            PlanogramBlockingLocation item = new PlanogramBlockingLocation();
            item.Create(group);
            return item;
        }


        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingLocation NewPlanogramBlockingLocation(IPlanogramBlockingLocation source, IResolveContext context)
        {
            PlanogramBlockingLocation item = new PlanogramBlockingLocation();
            item.Create(source, context);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramBlockingGroup group)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(PlanogramBlockingGroupIdProperty, group.Id);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramBlockingLocation source, IResolveContext context)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(PlanogramBlockingGroupIdProperty, context.ResolveId<PlanogramBlockingGroup>(source.PlanogramBlockingGroupId));
            this.LoadProperty<Object>(PlanogramBlockingDividerTopIdProperty, context.ResolveId<PlanogramBlockingDivider>(source.PlanogramBlockingDividerTopId));
            this.LoadProperty<Object>(PlanogramBlockingDividerBottomIdProperty, context.ResolveId<PlanogramBlockingDivider>(source.PlanogramBlockingDividerBottomId));
            this.LoadProperty<Object>(PlanogramBlockingDividerLeftIdProperty, context.ResolveId<PlanogramBlockingDivider>(source.PlanogramBlockingDividerLeftId));
            this.LoadProperty<Object>(PlanogramBlockingDividerRightIdProperty, context.ResolveId<PlanogramBlockingDivider>(source.PlanogramBlockingDividerRightId));
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the divider opposite to the given <paramref name="divider"/> for this location. E.g.
        /// If the <paramref name="divider"/> is the right divider, this method will return the left divider.
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public PlanogramBlockingDivider GetOppositeDividerTo(PlanogramBlockingDivider divider)
        {
            if(divider == null)
            {
                return null;
            }
            else if (divider.Id.Equals(PlanogramBlockingDividerLeftId))
            {
                return GetRightDivider();
            }
            else if (divider.Id.Equals(PlanogramBlockingDividerRightId))
            {
                return GetLeftDivider();
            }
            else if(divider.Id.Equals(PlanogramBlockingDividerTopId))
            {
                return GetBottomDivider();
            }
            else if (divider.Id.Equals(PlanogramBlockingDividerBottomId))
            {
                return GetTopDivider();
            }
            return null;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramBlockingLocation>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this context
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramBlockingGroupId
            Object planogramBlockingGroupId = context.ResolveId<PlanogramBlockingGroup>(this.ReadProperty<Object>(PlanogramBlockingGroupIdProperty));
            if (planogramBlockingGroupId != null) this.LoadProperty<Object>(PlanogramBlockingGroupIdProperty, planogramBlockingGroupId);

            // PlanogramBlockingDividerTopId
            Object planogramBlockingDividerTopId = context.ResolveId<PlanogramBlockingDivider>(this.ReadProperty<Object>(PlanogramBlockingDividerTopIdProperty));
            if (planogramBlockingDividerTopId != null) this.LoadProperty<Object>(PlanogramBlockingDividerTopIdProperty, planogramBlockingDividerTopId);

            // PlanogramBlockingDividerBottomId
            Object planogramBlockingDividerBottomId = context.ResolveId<PlanogramBlockingDivider>(this.ReadProperty<Object>(PlanogramBlockingDividerBottomIdProperty));
            if (planogramBlockingDividerBottomId != null) this.LoadProperty<Object>(PlanogramBlockingDividerBottomIdProperty, planogramBlockingDividerBottomId);

            // PlanogramBlockingDividerLeftId
            Object planogramBlockingDividerLeftId = context.ResolveId<PlanogramBlockingDivider>(this.ReadProperty<Object>(PlanogramBlockingDividerLeftIdProperty));
            if (planogramBlockingDividerLeftId != null) this.LoadProperty<Object>(PlanogramBlockingDividerLeftIdProperty, planogramBlockingDividerLeftId);

            // PlanogramBlockingDividerRightId
            Object planogramBlockingDividerRightId = context.ResolveId<PlanogramBlockingDivider>(this.ReadProperty<Object>(PlanogramBlockingDividerRightIdProperty));
            if (planogramBlockingDividerRightId != null) this.LoadProperty<Object>(PlanogramBlockingDividerRightIdProperty, planogramBlockingDividerRightId);
        }

        /// <summary>
        /// Notifies this location that the given divider has changed so that it may update itself
        /// if required.
        /// </summary>
        /// <param name="divider"></param>
        public void NotifyDividerMoved(PlanogramBlockingDivider divider)
        {
            if (IsAssociated(divider))
            {
                UpdateHelperProperties();
            }
        }


        /// <summary>
        /// Updates the helper properties of this location
        /// </summary>
        private void UpdateHelperProperties(Boolean isInitialLoad = false)
        {
            PlanogramBlockingDivider bottomDivider = GetBottomDivider();
            PlanogramBlockingDivider topDivider = GetTopDivider();
            PlanogramBlockingDivider leftDivider = GetLeftDivider();
            PlanogramBlockingDivider rightDivider = GetRightDivider();

            Single leftX = (leftDivider != null) ? leftDivider.X : 0;
            Single rightX = (rightDivider != null) ? rightDivider.X : 1;
            Single bottomY = (bottomDivider != null) ? bottomDivider.Y : 0;
            Single topY = (topDivider != null) ? topDivider.Y : 1;

            Single newX = leftX;
            Single newY = bottomY;
            Single newWidth = Convert.ToSingle(rightX - leftX);
            Single newHeight = Convert.ToSingle(topY - bottomY);
            Single newSpace = Convert.ToSingle(newWidth * newHeight);

            if (isInitialLoad)
            {
                LoadProperty(XProperty, newX);
                LoadProperty(YProperty, newY);
                LoadProperty(WidthProperty, newWidth);
                LoadProperty(HeightProperty, newHeight);

                //set the space percentage instead of loading it otherwise one of the first
                //two blocks created will display as 0%
                this.SpacePercentage = newSpace;
            }
            else
            {
                this.X = newX;
                this.Y = newY;
                this.Width = newWidth;
                this.Height = newHeight;
                this.SpacePercentage = newSpace;
            }
        }


        /// <summary>
        /// Returns the blocking group to which this location
        /// is associated.
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingGroup GetPlanogramBlockingGroup()
        {
            if (this.Parent == null) return null;
            return this.Parent.Groups.FirstOrDefault(g => Object.Equals(g.Id, this.PlanogramBlockingGroupId));
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// bottom of this block
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingDivider GetBottomDivider()
        {
            if (this.PlanogramBlockingDividerBottomId == null) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => Object.Equals(g.Id, this.PlanogramBlockingDividerBottomId));
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// left of this block
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingDivider GetLeftDivider()
        {
            if (this.PlanogramBlockingDividerLeftId == null) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => Object.Equals(g.Id,this.PlanogramBlockingDividerLeftId));
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// right of this block
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingDivider GetRightDivider()
        {
            if (this.PlanogramBlockingDividerRightId == null) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => Object.Equals(g.Id, this.PlanogramBlockingDividerRightId));
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// top of this block
        /// </summary>
        /// <returns></returns>
        public PlanogramBlockingDivider GetTopDivider()
        {
            if (this.PlanogramBlockingDividerTopId == null) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => Object.Equals(g.Id, this.PlanogramBlockingDividerTopId));
        }

        /// <summary>
        /// Returns true if this blocking is associated with the given divider 
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public Boolean IsAssociated(IPlanogramBlockingDivider divider)
        {
            if (divider == null) return false;

            PlanogramBlockingDivider d = divider as PlanogramBlockingDivider;
            if (d == null) return false;

            //return true if any of the ids here link to the divider given.
            return (Object.Equals(this.PlanogramBlockingDividerLeftId, d.Id)
                || Object.Equals(this.PlanogramBlockingDividerRightId, d.Id)
                || Object.Equals(this.PlanogramBlockingDividerTopId, d.Id)
                || Object.Equals(this.PlanogramBlockingDividerBottomId, d.Id));
        }

        /// <summary>
        /// Returns true if this location can be combined with the one given
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public Boolean CanCombineWith(PlanogramBlockingLocation location)
        {
            return BlockingHelper.CanCombine(location, this);
        }

        /// <summary>
        /// Combine this location with the one given.
        /// </summary>
        /// <param name="location"></param>
        public void CombineWith(PlanogramBlockingLocation location)
        {
            //check we have a parent blocking.
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null) return;

            //return out if we cant combine
            if (!CanCombineWith(location)) return;


            PlanogramBlockingGroup groupToRemove = GetPlanogramBlockingGroup();

            //update the group id of this block to the same and the loc
            // we are combining with.
            this.PlanogramBlockingGroupId = location.PlanogramBlockingGroupId;

            //remove the old group if it is now unused.
            if (groupToRemove.GetBlockingLocations().Count == 0)
            {
                parentBlocking.Groups.Remove(groupToRemove);
            }
        }

        /// <summary>
        /// Returns true if this location is adjacent.
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        public Boolean IsAdjacent(PlanogramBlockingLocation loc)
        {
            if (loc == this) return false;

            return (Object.Equals(loc.PlanogramBlockingDividerLeftId, this.PlanogramBlockingDividerRightId)
                        || Object.Equals(loc.PlanogramBlockingDividerRightId, this.PlanogramBlockingDividerLeftId)
                        || Object.Equals(loc.PlanogramBlockingDividerTopId, this.PlanogramBlockingDividerBottomId)
                        || Object.Equals(loc.PlanogramBlockingDividerBottomId, this.PlanogramBlockingDividerTopId));
        }

        /// <summary>
        /// Returns a list of locations that are adjacent to this one.
        /// </summary>
        /// <returns></returns>
        public List<PlanogramBlockingLocation> GetAdjacentLocations()
        {
            List<PlanogramBlockingLocation> adjacentLocations = new List<PlanogramBlockingLocation>();

            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking != null)
            {
                foreach (PlanogramBlockingLocation loc in parentBlocking.Locations)
                {
                    if (IsAdjacent(loc))
                    {
                        adjacentLocations.Add(loc);
                    }
                }
            }

            return adjacentLocations;
        }

        /// <summary>
        /// Gets the Design View Position for this Blocking Location.
        /// </summary>
        /// <returns></returns>
        public DesignViewPosition GetDesignViewPosition()
        {
            if (Parent == null || Parent.Parent == null) throw new InvalidOperationException("Parent cannot be null.");

            Single height;
            Single width;
            this.Parent.Parent.GetBlockingAreaSize(out height, out width);

            return new DesignViewPosition(this,width, height);
        }

        /// <summary>
        /// Returns a rectvalue that represents the bounds of this location, with a zero z component.
        /// </summary>
        /// <returns></returns>
        public RectValue ToRectValue()
        {
            return new RectValue(this.X, this.Y, 0f, this.Width, this.Height, 0f);
        }

        #region Get Blocking methods
        /// <summary>
        /// Expands this location to fill the space occupied by the given list of locations.
        /// </summary>
        /// <param name="locations">The locations whose space should be occupied.</param>
        /// <remarks>
        /// The space is filled by changing the dividers of this location to those that
        /// are furthest left, right, top and bottom in the given locations.
        /// </remarks>
        public void ExpandToFill(IEnumerable<PlanogramBlockingLocation> locations)
        {
            // Get minimum and maximum x and y values. If a null divider is present, just store null,
            // because this represents a boundary of the blocking scheme.
            Single? minX = locations.Any(l=>l.GetLeftDivider()==null) ? 0f : (Single?)locations.Select(l => l.GetLeftDivider().X).Min();
            Single? maxX = locations.Any(l => l.GetRightDivider() == null) ? 1f : (Single?)locations.Select(l => l.GetRightDivider().X).Max();
            Single? minY = locations.Any(l => l.GetBottomDivider() == null) ? 0f : (Single?)locations.Select(l => l.GetBottomDivider().Y).Min();
            Single? maxY = locations.Any(l => l.GetTopDivider() == null) ? 1f : (Single?)locations.Select(l => l.GetTopDivider().Y).Max();

            PlanogramBlockingDividerLeftId = minX==0f ? null : locations.
                Select(l => l.GetLeftDivider()).
                First(d => d.X == minX).
                Id;

            PlanogramBlockingDividerRightId = maxX == 1f ? null : locations.
                Select(l => l.GetRightDivider()).
                First(d => d.X == maxX).
                Id;

            PlanogramBlockingDividerTopId = maxY == 1f ? null : locations.
                Select(l => l.GetTopDivider()).
                First(d => d.Y == maxY).
                Id;

            PlanogramBlockingDividerBottomId = minY == 0f ? null : locations.
                Select(l => l.GetBottomDivider()).
                First(d => d.Y == minY).
                Id;
        }

        /// <summary>
        /// Gets ths Fixture Components that are located, partially or completely, within the bounds of this location.
        /// </summary>
        /// <returns>A collection of Fixture Components.</returns>
        public IEnumerable<IPlanogramFixtureComponent> GetMerchandisableFixtureAndAssemblyComponents()
        {
            if (Parent == null || Parent.Parent == null) throw new InvalidOperationException("Parent cannot be null");
            var returnList = new List<IPlanogramFixtureComponent>();

            // Get plan blocking area
            Single planWidth;
            Single planHeight;
            Single offset;
            Parent.Parent.GetBlockingAreaSize(out planHeight, out planWidth, out offset);
            
            // Look in all FixtureItems
            foreach (var fixtureItem in Parent.Parent.FixtureItems)
            {
                var fixture = fixtureItem.GetPlanogramFixture();
                if (fixture == null) continue;

                // Fixture components
                foreach (var fixtureComponent in fixture.Components)
                {
                    var component = fixtureComponent.GetPlanogramComponent();
                    if(component==null) continue;

                    DesignViewPosition designViewPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
                    if (component.IsMerchandisable && IsOverDesignViewPosition(designViewPosition, planHeight, planWidth, offset))
                    {
                        returnList.Add(fixtureComponent);
                    }
                }

                // Assembly components
                foreach (var fixtureAssembly in fixture.Assemblies)
                {
                    var assembly = fixtureAssembly.GetPlanogramAssembly();
                    if (assembly == null) continue;

                    foreach (var assemblyComponent in assembly.Components)
                    {
                        var component = assemblyComponent.GetPlanogramComponent();
                        if (component == null) continue;

                        DesignViewPosition designViewPosition = new DesignViewPosition(assemblyComponent, fixtureItem, fixtureAssembly, offset);
                        if (component.IsMerchandisable && IsOverDesignViewPosition(designViewPosition, planHeight, planWidth, offset))
                        {
                            returnList.Add(assemblyComponent);
                        }
                    }
                }
            }
            return returnList;
        }

        private Boolean IsOverDesignViewPosition(DesignViewPosition designViewPosition, Single planHeight, Single planWidth, Single offset)
        {
            if (planWidth.EqualTo(0) || planHeight.EqualTo(0)) return false;

            Single relX = designViewPosition.BoundX / planWidth;
            Single relWidth = designViewPosition.BoundWidth / planWidth;
            Single relY = designViewPosition.BoundY / planHeight;
            Single relHeight = designViewPosition.BoundHeight / planHeight;

            Single xOverlap = BlockingHelper.GetOverlap(X, Width, relX, relWidth);
            Single yOverlap = BlockingHelper.GetOverlap(Y, Height, relY, relHeight);

            return xOverlap.GreaterThan(_fixtureOverlapThreshold,4) && yOverlap.GreaterThan(_fixtureOverlapThreshold,4);
        }

        /// <summary>
        /// Enumerates the non-null dividers associated with this Location.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramBlockingDivider> GetPlanogramBlockingDividers()
        {
            if (PlanogramBlockingDividerLeftId != null) yield return GetLeftDivider();
            if (PlanogramBlockingDividerTopId != null) yield return GetTopDivider();
            if (PlanogramBlockingDividerRightId != null) yield return GetRightDivider();
            if (PlanogramBlockingDividerBottomId != null) yield return GetBottomDivider();
            yield break;
        }

        #endregion

        #region IPlanogramBlockingGroup Members

        IPlanogramBlockingGroup IPlanogramBlockingLocation.GetBlockingGroup()
        {
            return GetPlanogramBlockingGroup();
        }

        Boolean IPlanogramBlockingLocation.CanCombineWith(IPlanogramBlockingLocation location)
        {
            return CanCombineWith(location as PlanogramBlockingLocation);
        }

        void IPlanogramBlockingLocation.CombineWith(IPlanogramBlockingLocation location)
        {
            CombineWith(location as PlanogramBlockingLocation);
        }

        #endregion

        /// <summary>
        /// Returns the co-ordinates by which this location can be sequenced.
        /// </summary>
        /// <returns></returns>
        public PointValue GetSequencableCoords()
        {
            return new PointValue(this.X, this.Y, 0);
        }
        #endregion
    }
}
