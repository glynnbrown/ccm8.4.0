﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.1.0
// V8-29860
//  Created 
#endregion
#region Version History : CCM820
// V8-30864 : A.Kuszyk
//  Template option updated to SequenceTemplate and BlockingTemplate added.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum denoting the possible planogram types
    /// </summary>
    public enum PlanogramType
    {
        /// <summary>
        /// Denotes an ordinary planogram.
        /// </summary>
        Planogram = 0,
        /// <summary>
        /// Denotes a planogram used as a template for product sequence data.
        /// </summary>
        SequenceTemplate = 1,
        /// <summary>
        /// Denotes a reference planogram.
        /// </summary>
        ReferencePlanogram = 2,
        /// <summary>
        /// Denotes a planogram used as a template for blocking data.
        /// </summary>
        BlockingTemplate = 3,
    }

    /// <summary>
    /// PlanogramType Helper Class
    /// </summary>
    public static class PlanogramTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramType, String> FriendlyNames =
            new Dictionary<PlanogramType, String>()
            {
                {PlanogramType.Planogram, Message.Enum_PlanogramType_Planogram},
                {PlanogramType.SequenceTemplate, Message.Enum_PlanogramType_SequenceTemplate},
                {PlanogramType.ReferencePlanogram, Message.Enum_PlanogramType_ReferencePlanogram},
                {PlanogramType.BlockingTemplate, Message.Enum_PlanogramType_BlockingTemplate},
            };
    }
}
