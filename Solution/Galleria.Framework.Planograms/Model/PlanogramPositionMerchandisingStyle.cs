﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//  Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramPositionMerchandisingStyle
    {
        Default,
        Unit,
        Tray,
        Case,
        Display,
        Alternate,
        PointOfPurchase
    }

    /// <summary>
    /// PlanogramPositionMerchandisingStyle Helper Class
    /// </summary>
    public static class PlanogramPositionMerchandisingStyleHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramPositionMerchandisingStyle, String> FriendlyNames =
            new Dictionary<PlanogramPositionMerchandisingStyle, String>()
            {
                {PlanogramPositionMerchandisingStyle.Default, Message.Enum_PlanogramPositionMerchandisingStyle_Default},
                {PlanogramPositionMerchandisingStyle.Unit, Message.Enum_PlanogramPositionMerchandisingStyle_Unit},
                {PlanogramPositionMerchandisingStyle.Tray, Message.Enum_PlanogramPositionMerchandisingStyle_Tray},
                {PlanogramPositionMerchandisingStyle.Case, Message.Enum_PlanogramPositionMerchandisingStyle_Case},
                {PlanogramPositionMerchandisingStyle.Display, Message.Enum_PlanogramPositionMerchandisingStyle_Display},
                {PlanogramPositionMerchandisingStyle.Alternate, Message.Enum_PlanogramPositionMerchandisingStyle_Alternate},
                {PlanogramPositionMerchandisingStyle.PointOfPurchase, Message.Enum_PlanogramPositionMerchandisingStyle_PointOfPurchase},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramPositionMerchandisingStyle, String> FriendlyDescriptions =
            new Dictionary<PlanogramPositionMerchandisingStyle, String>()
            {
                {PlanogramPositionMerchandisingStyle.Default, Message.Enum_PlanogramPositionMerchandisingStyle_Default},
                {PlanogramPositionMerchandisingStyle.Unit, Message.Enum_PlanogramPositionMerchandisingStyle_Unit},
                {PlanogramPositionMerchandisingStyle.Tray, Message.Enum_PlanogramPositionMerchandisingStyle_Tray},
                {PlanogramPositionMerchandisingStyle.Case, Message.Enum_PlanogramPositionMerchandisingStyle_Case},
                {PlanogramPositionMerchandisingStyle.Display, Message.Enum_PlanogramPositionMerchandisingStyle_Display},
                {PlanogramPositionMerchandisingStyle.Alternate, Message.Enum_PlanogramPositionMerchandisingStyle_Alternate},
                {PlanogramPositionMerchandisingStyle.PointOfPurchase, Message.Enum_PlanogramPositionMerchandisingStyle_PointOfPurchase},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramPositionMerchandisingStyle> EnumFromFriendlyName =
            new Dictionary<String, PlanogramPositionMerchandisingStyle>()
            {
                {Message.Enum_PlanogramPositionMerchandisingStyle_Default.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Default},
                {Message.Enum_PlanogramPositionMerchandisingStyle_Unit.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Unit},
                {Message.Enum_PlanogramPositionMerchandisingStyle_Tray.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Tray},
                {Message.Enum_PlanogramPositionMerchandisingStyle_Case.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Case},
                {Message.Enum_PlanogramPositionMerchandisingStyle_Display.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Display},
                {Message.Enum_PlanogramPositionMerchandisingStyle_Alternate.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.Alternate},
                {Message.Enum_PlanogramPositionMerchandisingStyle_PointOfPurchase.ToLowerInvariant(), PlanogramPositionMerchandisingStyle.PointOfPurchase},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramPositionMerchandisingStyle? PlanogramPositionMerchandisingStyleGetEnum(String friendlyName)
        {
            PlanogramPositionMerchandisingStyle? returnValue = null;
            PlanogramPositionMerchandisingStyle enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
