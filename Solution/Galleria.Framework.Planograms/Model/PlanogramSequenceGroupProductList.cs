﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     A list of <see cref="PlanogramSequenceGroupProduct"/> instances that belong to the same <see cref="PlanogramSequenceGroup"/>.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSequenceGroupProductList : ModelList<PlanogramSequenceGroupProductList, PlanogramSequenceGroupProduct>
    {
        #region Parent

        /// <summary>
        ///     Gets this instances parent <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        public new PlanogramSequenceGroup Parent
        {
            get { return (PlanogramSequenceGroup) base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceGroupProductList"/>.</returns>
        public static PlanogramSequenceGroupProductList NewPlanogramSequenceGroupProductList()
        {
            var item = new PlanogramSequenceGroupProductList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Invoked when creating a new instance of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}