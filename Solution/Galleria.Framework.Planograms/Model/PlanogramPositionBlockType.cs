﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the block parts that make up a plan position.
    /// </summary>
    public enum PlanogramPositionBlockType : byte
    {
        Main = 0,
        X = 1,
        Y = 2,
        Z = 3
    }
}
