﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public sealed partial class PlanogramSequenceGroupProductList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to obtain new instances of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        private PlanogramSequenceGroupProductList() {}

        #endregion

        /// <summary>
        ///     Invoked by CSLA via reflection when returning all the <see cref="PlanogramSequenceGroupProduct"/> items in the <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        /// <param name="criteria"><see cref="ModelList{T,T2}.FetchByParentIdCriteria"/> containing the parent Id of the items to be fetched.</param>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramSequenceGroupProductDal>())
            {
                IEnumerable<PlanogramSequenceGroupProductDto> dtos = dal.FetchByPlanogramSequenceGroupId(criteria.ParentId);
                foreach (PlanogramSequenceGroupProductDto dto in dtos)
                {
                    Add(PlanogramSequenceGroupProduct.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }
    }
}