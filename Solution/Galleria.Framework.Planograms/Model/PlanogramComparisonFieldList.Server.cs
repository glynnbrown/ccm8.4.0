﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonFieldList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonFieldList() {}

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonFieldList" />.
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonFieldDal>())
                {
                    IEnumerable<PlanogramComparisonFieldDto> dtoList = dal.FetchByPlanogramComparisonId(criteria.ParentId);
                    foreach (PlanogramComparisonFieldDto dto in dtoList) { Add(PlanogramComparisonField.Fetch(dalContext, dto)); }
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}