﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550: A.Probyn
//	Copied from SA.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductBuddySourceType
    {
        Attribute,
        Manual
    }

    public static class PlanogramAssortmentProductBuddySourceTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductBuddySourceType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentProductBuddySourceType, String>()
            {
                {PlanogramAssortmentProductBuddySourceType.Attribute, Message.Enum_PlanogramAssortmentProductBuddySourceType_Attribute},
                {PlanogramAssortmentProductBuddySourceType.Manual, Message.Enum_PlanogramAssortmentProductBuddySourceType_Manual}
            };

        /// <summary>
        /// Returns the matching enum value from the specified friendlyname
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramAssortmentProductBuddySourceType? ParseFromFriendlyName(String friendlyName, Boolean closestMatch = false)
        {
            String cleanFriendlyName = friendlyName.ToLowerInvariant().Trim();

            foreach (KeyValuePair<PlanogramAssortmentProductBuddySourceType, String> keyPair in PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.ToLowerInvariant().Equals(cleanFriendlyName))
                {
                    return keyPair.Key;
                }
            }

            if (closestMatch)
            {
                foreach (KeyValuePair<PlanogramAssortmentProductBuddySourceType, String> keyPair in PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames)
                {
                    if (keyPair.Value.ToLowerInvariant().Contains(cleanFriendlyName))
                    {
                        return keyPair.Key;
                    }
                }
            }
            return null;
        }

    }
}
