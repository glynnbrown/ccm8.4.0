﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-24974 : L.Hodson
//      Added FrontStacked and BackStacked
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramSubComponentZMerchStrategyType
    {
        Manual = 0,
        Front = 1,
        FrontStacked = 2,
        Back = 3,
        BackStacked = 4,
        Even = 5,
    }

    /// <summary>
    /// PlanogramSubComponentZMerchStrategyType Helper Class
    /// </summary>
    public static class PlanogramSubComponentZMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentZMerchStrategyType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentZMerchStrategyType, String>()
            {
                {PlanogramSubComponentZMerchStrategyType.Manual, Message.Enum_PlanogramSubComponentZMerchStrategyType_Manual},
                {PlanogramSubComponentZMerchStrategyType.Front, Message.Enum_PlanogramSubComponentZMerchStrategyType_Front},
                {PlanogramSubComponentZMerchStrategyType.FrontStacked, Message.Enum_PlanogramSubComponentZMerchStrategyType_FrontStacked},
                {PlanogramSubComponentZMerchStrategyType.Back, Message.Enum_PlanogramSubComponentZMerchStrategyType_Back},
                {PlanogramSubComponentZMerchStrategyType.BackStacked, Message.Enum_PlanogramSubComponentZMerchStrategyType_BackStacked},
                {PlanogramSubComponentZMerchStrategyType.Even, Message.Enum_PlanogramSubComponentZMerchStrategyType_Even},
            };


        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentZMerchStrategyType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentZMerchStrategyType>()
            {
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_Manual.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.Manual},
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_Front.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.Front},
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_FrontStacked.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.FrontStacked},
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_Back.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.Back},
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_BackStacked.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.BackStacked},
                {Message.Enum_PlanogramSubComponentZMerchStrategyType_Even.ToLowerInvariant(), PlanogramSubComponentZMerchStrategyType.Even},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentZMerchStrategyType? PlanogramSubComponentZMerchStrategyTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentZMerchStrategyType? returnValue = null;
            PlanogramSubComponentZMerchStrategyType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
