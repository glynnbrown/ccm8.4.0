﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31862 : A.Silva
//  Refactored the comparison code to be encapsulated in ComparisonContext.
// V8-31885 : A.Silva
//  Added event to notify when all results are updated.
// V8-31913 : A.Silva
//  Added IgnoreNonPlacedProducts property.
// V8-31947 : A.Silva
//  Added GetComparisonStatus to agreggate the comparison statuses for each plan item across planograms.
// V8-32005 : A.Silva
//  Added Name, Description and DataOrderType properties.
// V8-31945 : A.Silva
//  Added implementation of IPlanogramComparisonSettings.
// V8-32732 : A.Silva
//  Amended an incorrect status when the item was missing from the master plan (now correctly marked as Not Found).
// V8-32926 : A.Silva
//  Added GetOrderedFieldsOfType to return fields in the correct order.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Represents the comparison of other planograms with the parent planogram.
    /// </summary>
    [Serializable, DefaultNewMethod("NewPlanogramComparison")]
    public sealed partial class PlanogramComparison : ModelObject<PlanogramComparison>, IPlanogramComparisonSettings
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="Planogram" />.
        /// </summary>
        public new Planogram Parent { get { return (Planogram) base.Parent; } }

        #endregion

        #region Name

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Name" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty = RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        ///     Get/Set the name used to identify this comparison to the user.
        /// </summary>
        public String Name { get { return GetProperty(NameProperty); } set { SetProperty(NameProperty, value); } }

        #endregion

        #region Description

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Description" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty = RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        ///     Get/Set the description used for this comparison.
        /// </summary>
        public String Description { get { return GetProperty(DescriptionProperty); } set { SetProperty(DescriptionProperty, value); } }

        #endregion

        #region DateLastCompared

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="DateLastCompared" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLastComparedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateLastCompared);

        /// <summary>
        ///     Get the date that this instance run a comparison last.
        /// </summary>
        public DateTime? DateLastCompared
        {
            get { return GetProperty(DateLastComparedProperty); }
            set { SetProperty(DateLastComparedProperty, value); }
        }

        #endregion

        #region IgnoreNonPlacedProducts

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="IgnoreNonPlacedProducts" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IgnoreNonPlacedProductsProperty =
            RegisterModelProperty<Boolean>(c => c.IgnoreNonPlacedProducts);

        /// <summary>
        ///     Get or set whether non placed products are ignored in the compared planograms.
        /// </summary>
        public Boolean IgnoreNonPlacedProducts
        {
            get { return GetProperty(IgnoreNonPlacedProductsProperty); }
            set { SetProperty(IgnoreNonPlacedProductsProperty, value); }
        }

        #endregion

        #region DataOrderType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="DataOrderType" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DataOrderType> DataOrderTypeProperty = RegisterModelProperty<DataOrderType>(c => c.DataOrderType);

        /// <summary>
        ///     Get/Set the data order type this comparison will be displayed in to the user.
        /// </summary>
        public DataOrderType DataOrderType { get { return GetProperty(DataOrderTypeProperty); } set { SetProperty(DataOrderTypeProperty, value); } }

        #endregion

        #region Results

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Results" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComparisonResultList> ResultsProperty =
            RegisterModelProperty<PlanogramComparisonResultList>(c => c.Results, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     [<c>Lazy</c>] Get the list of <see cref="PlanogramComparisonResult" /> for this instance.
        /// </summary>
        public PlanogramComparisonResultList Results
        {
            get
            {
                return GetPropertyLazy(ResultsProperty,
                                       new ModelList<PlanogramComparisonResultList, PlanogramComparisonResult>.FetchByParentIdCriteria(
                                           DalFactoryName,
                                           Id),
                                       true);
            }
        }

        #endregion

        #region ComparisonFields

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="ComparisonFields" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComparisonFieldList> ComparisonFieldsProperty =
            RegisterModelProperty<PlanogramComparisonFieldList>(c => c.ComparisonFields, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     [<c>Lazy</c>] Get the list of <see cref="PlanogramComparisonField" /> for this instance's Product comparisons.
        /// </summary>
        public PlanogramComparisonFieldList ComparisonFields
        {
            get
            {
                return GetPropertyLazy(ComparisonFieldsProperty,
                                       new ModelList<PlanogramComparisonFieldList, PlanogramComparisonField>.FetchByParentIdCriteria(DalFactoryName,
                                                                                                                                     Id),
                                       true);
            }
        }

        /// <summary>
        ///     IPlanogramComparisonSettings implementation of Comparison Fields.
        /// </summary>
        IEnumerable<IPlanogramComparisonSettingsField> IPlanogramComparisonSettings.ComparisonFields
        {
            get { return ComparisonFields; }
        } 

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new UniqueList<PlanogramComparisonResult>(ResultsProperty, PlanogramComparisonResult.PlanogramNameProperty));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules() {}

        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria

        /// <summary>
        ///     Criteria for the FetchByPlanogramId factory method.
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> definition for the <see cref="DalFactoryName" /> property.
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty = RegisterProperty<String>(c => c.DalFactoryName);

            /// <summary>
            ///     Get the dal factory name.
            /// </summary>
            public String DalFactoryName { get { return ReadProperty(DalFactoryNameProperty); } }

            #endregion

            #region PlanogramId

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> definition for the <see cref="PlanogramId" /> property.
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty = RegisterProperty<Object>(c => c.PlanogramId);

            /// <summary>
            ///     Get the ID corresponding to the parent Planogram.
            /// </summary>
            public Object PlanogramId { get { return ReadProperty(PlanogramIdProperty); } }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initialize a new instance of <see cref="FetchByPlanogramIdCriteria" />.
            /// </summary>
            /// <param name="dalFactoryName">Name of the dal factory in use.</param>
            /// <param name="planogramId">ID corresponding to the parent Planogram.</param>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                LoadProperty(DalFactoryNameProperty, dalFactoryName);
                LoadProperty(PlanogramIdProperty, planogramId);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparison" />.
        /// </summary>
        public static PlanogramComparison NewPlanogramComparison()
        {
            var item = new PlanogramComparison();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(DateLastComparedProperty, null);
            LoadProperty(ResultsProperty, PlanogramComparisonResultList.NewPlanogramComparisonResultList());
            LoadProperty(ComparisonFieldsProperty, PlanogramComparisonFieldList.NewPlanogramComparisonFieldList());
            LoadProperty(IgnoreNonPlacedProductsProperty, true);
            LoadProperty(DataOrderTypeProperty, DataOrderType.ByPlanogram);
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Loads the data contained in the <paramref name="source"/> instance implementing <see cref="IPlanogramComparisonSettings"/> into the current instance.
        /// </summary>
        /// <param name="source">Instance containing the data to load, and that implements <see cref="IPlanogramComparisonSettings"/>.</param>
        public void Load(IPlanogramComparisonSettings source)
        {
            Name = source.Name;
            Description = source.Description;
            DateLastCompared = source.DateLastCompared;
            IgnoreNonPlacedProducts = source.IgnoreNonPlacedProducts;
            DataOrderType = source.DataOrderType;
            ResetComparisonFields();
            AddComparisonFields(source.ComparisonFields);
        }

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="comparisonFields">The collection of <see cref="IPlanogramComparisonSettingsField"/> to add.</param>
        public void AddComparisonFields(IEnumerable<IPlanogramComparisonSettingsField> comparisonFields)
        {
            ComparisonFields.AddRange(comparisonFields);
        }

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="comparisonFields">The collection of <see cref="IPlanogramComparisonSettingsField"/> to add.</param>
        public void AddComparisonFields(PlanogramItemType itemType, IEnumerable<IPlanogramComparisonSettingsField> comparisonFields)
        {
            ComparisonFields.AddRange(itemType, comparisonFields);
        }

        /// <summary>
        ///     Add a collection of <see cref="IPlanogramComparisonSettingsField" /> derived from an enumeration of
        ///     <see cref="ObjectFieldInfo" />.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="fieldInfos">The enumeration of <see cref="ObjectFieldInfo" /> from which to derive the new items.</param>
        public void AddComparisonFields(PlanogramItemType itemType, Dictionary<ObjectFieldInfo, Boolean> fieldInfos)
        {
            ComparisonFields.AddRange(itemType, fieldInfos);
        }

        /// <summary>
        ///     Resets the internal collection, clearing all existing comparison fields in it.
        /// </summary>
        public void ResetComparisonFields()
        {
            ComparisonFields.Clear();
        }

        /// <summary>
        ///     Performs all field comparisons and updates the results.
        /// </summary>
        public void UpdateResults(IEnumerable<Planogram> targetPlanograms)
        {
            //  Clear any previous results.
            Results.Clear();

            var helper = new PlanogramComparerHelper(Parent);

            //  Obtain the results per target planogram.
            foreach (Planogram target in targetPlanograms.Where(planogram => !Equals(planogram, Parent)))
                helper.CompareTarget(target);

            DateLastCompared = DateTime.UtcNow;
        }

        /// <summary>
        ///     Get the aggregated comparison status for the given <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The comparison ID for which to calculate the aggregated comparison status.</param>
        /// <returns>The most generic change type in all the comparison statuses linked to this comparison ID.</returns>
        private PlanogramItemComparisonStatusType GetComparisonStatus(String key)
        {
            List<PlanogramComparisonItemStatusType> statuses = Results.SelectMany(result => result.ComparedItems)
                                                                      .Where(item => item.ItemId == key)
                                                                      .Select(item => item.Status)
                                                                      .Distinct()
                                                                      .ToList();

            //  If the item is new for at least one compared planogram, it is not found in the master planogram.
            if (statuses.Any(type => type == PlanogramComparisonItemStatusType.New) || Results.Count == 0) return PlanogramItemComparisonStatusType.NotFound;

            //  If the item has been changed in at least one compared planogram, the general status is changed in the master planogram.
            if (statuses.Any(type => type == PlanogramComparisonItemStatusType.Changed)) return PlanogramItemComparisonStatusType.Changed;

            //  IF the item has no changes in any compared planogram and was not found in at least one, the item is new in the master planogram/
            if (statuses.Any(type => type == PlanogramComparisonItemStatusType.NotFound)) return PlanogramItemComparisonStatusType.New;

            //  There must be no changes at all.
            return PlanogramItemComparisonStatusType.Unchanged;
        }

        /// <summary>
        ///     Get the aggregated comparison status for the given <paramref name="planItem"/>.
        /// </summary>
        /// <param name="planItem">The <see cref="PlanogramProduct"/> for which to calculate the aggregated comparison status.</param>
        /// <returns>The most generic change type in all the comparison statuses linked to this item.</returns>
        public PlanogramItemComparisonStatusType GetComparisonStatus(PlanogramProduct planItem)
        {
            return GetComparisonStatus(PlanogramComparerHelper.CreateComparisonKey(planItem));
        }

        /// <summary>
        ///     Get the aggregated comparison status for the given <paramref name="planItem"/>.
        /// </summary>
        /// <param name="planItem">The <see cref="PlanogramPosition"/> for which to calculate the aggregated comparison status.</param>
        /// <returns>The most generic change type in all the comparison statuses linked to this item.</returns>
        public PlanogramItemComparisonStatusType GetComparisonStatus(PlanogramPosition planItem)
        {
            return GetComparisonStatus(PlanogramComparerHelper.CreateComparisonKey(planItem));
        }

        /// <summary>
        ///     Enumerate <see cref="PlanogramComparisonField"/> instances that match the given <paramref name="planItemType"/> in ascending <see cref="Number"/> order.
        /// </summary>
        /// <param name="planItemType"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramComparisonField> GetOrderedFieldsOfType(PlanogramItemType planItemType)
        {
            return ComparisonFields.OfItemType(planItemType).OrderBy(field => field.Number);
        }

        #region Metadata

        /// <summary>
        ///     Override of base calculate metadata to make
        ///     sure items are loaded.
        /// </summary>
        protected override void CalculateMetadata()
        {
            base.CalculateMetadata(new Object[] {Results});
        }

        /// <summary>
        ///     Override of the clear metadata
        /// </summary>
        protected override void ClearMetadata()
        {
            base.ClearMetadata(new Object[] {Results});
        }

        #endregion

        #endregion
    }

    /// <summary>
    ///     Denotes the different ways the comparison values can be grouped.
    /// </summary>
    public enum DataOrderType
    {
        ByPlanogram = 0,
        ByAttribute = 1
    }

    public static class DataOrderTypeHelper
    {
        public static readonly Dictionary<DataOrderType, String> FriendlyNames =
            new Dictionary<DataOrderType, String>
            {
                {DataOrderType.ByPlanogram, Message.Enum_DataOrderType_ByPlanogram},
                {DataOrderType.ByAttribute, Message.Enum_DataOrderType_ByAttribute},
            };
    }
}