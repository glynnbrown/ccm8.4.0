﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// CCM-26891 : L.Ineson
//	Created 
// V8-26986 : A.Kuszyk
//  Added Get Blocking methods.
// V8-27399 : A.Kuszyk
//  Ensure that combining merged blocks combines all relevant locations.
// V8-27467 : A.Kuszyk
//  Adjusted the way snapping boundaries are selected.
// V8-27510 : A.Kuszyk
//  Amended SnapHorizontalDivider() to clean up any anomalous locations and dividers left after Dividers.Remove().
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
// V8-27439 : A.Kuszyk
//  Added GetLocationsBetweenDividers method.
// V8-27496 : L.Luong
//  Added ClearAll method
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.

#endregion

#region Version History: CCM802

// V8-28989 : A.Kuszyk
//  Re-factored GetLocationsBetweenDividers().
// V8-29000 : A.Silva
//  Added BlockingChangeCompleted event to notify when Dividers are added, removed or moved AFTER all other dividers have been updated too.

#endregion

#region Version History: CCM810

// V8-29597 : N.Foster
//  Moved blocking calculation methods to framework
// V8-29057 : D.Pleasance
//  Amended ApplyToFixtures() to return list of known blocking results, handles throwing of exception 'Block merging has been disabled' more gracefully.

#endregion

#region Version History: CCM811

// V8-29184 : M.Brumby
//  Added method to GetPlanogramBlockingGroupMerchandisingVolumePercentages
// V8-30423 : A.Silva
//  Amended GetPlanogramBlockingGroupMerchandisingVolumePercentages to account for 0 total merchandising volume.
// V8-29807 : A.Kuszyk
//  Added OnChildChanged to check for duplicate blocking group names and added a business rule to enforce this behaviour.
#endregion

#region Version History : CCM820

// V8-30536 : A.Kuszyk
//  Re-factored GetPlanogramBlockingGroupMerchandisingVolumePercentages to return a dictionary keyed by PlanogramBlockingGroup id.
// V8-30686 : A.Kuszyk
//  Removed exceptions from GetLocationsBetweenDividers and replace with Debug.Fails and safe returns.
// V8-30999 : A.Kuszyk
//  Removed vertical threshold for snapping dividers.
// V8-31162 : A.Kuszyk
//  Added business rules for unique colours.
// V8-31172 : A.Kuszyk
//  Added GetLocationSpacesByLocation.
// V8-31207 : A.Silva
//  Amended GetNormalisedPsis so that when all PSIs are zeroes we get a valid result, not just NaN.
// V8-31495 : D.Pleasance
//  Amended ApplyPerformanceData() to Calculate Psis for all RANGED products.

#endregion

#region Version History : CCM830
// V8-31804 : A.Kuszyk
//  Added GetProductStacksByColour.
// V8-32035 : D.Pleasance
//  Added GetPositionsSequenceGroup().
// V8-32018 : A.Kuszyk
//  Re-factored ApplyToFixtures methods to improve response when there is insufficient component space.
// V8-32449 : A.Kuszyk
//  Amended ApplyPerformanceData to take into account product size and inventory. Also made adjustments to ensure 
//  that 4 dp is used when comparing singles.
// V8-32539 : A.Kuszyk
//  Updated ApplyToFixtures to allow merging of groups.
// V8-32660 : A.Kuszyk
//  Implemented changes for the CanOptimise flag.
// V8-32752 : A.Kuszyk
// Fixed issue in DoGroupsHaveEnoughBoundaries.
// V8-32944 : A.Kuszyk
//  Changed precision of location comparison in GetLocationsBetweenDividers.
// V8-32982 : A.Kuszyk
//  Added EnumerateGroupsInOptimisationOrder.
// V8-32686 : A.Kuszyk
//  Added changes to enforce group and divider limitations.
// CCM-18452 : A.Kuszyk
//  Fixed issue with EnumerateGroupsInOptimisationOrder.
// CCM-18455 : A.Kuszyk
//  Ensured that sequence group colours are unique when merging blocks.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;
using System.Collections.Specialized;
using Galleria.Framework.Rules;
using Galleria.Framework.Enums;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a Planogram Blocking object
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewPlanogramBlocking", "Type")]
    public sealed partial class PlanogramBlocking : ModelObject<PlanogramBlocking>, IPlanogramBlocking 
    {
        #region Fields

        /// <summary>
        /// Contains state information for edits carried out by the <see cref="ApplyToFixtures"/> method.
        /// </summary>
        private Stack<DividerStates> _snappingEditStates = null;

        public const Int32 Precision = 4;
        
        #endregion

        #region Nested Types

        /// <summary>
        /// Represents a group of dividers to re-calculate postions for, between the boundaries of its band or
        /// locked dividers.
        /// </summary>
        [Serializable]
        private sealed class PerformanceCalculationDividers
        {
            /// <summary>
            /// The dividers to be recalculated for this group.
            /// </summary>
            public IEnumerable<PlanogramBlockingDivider> Dividers { get; private set; }
            
            /// <summary>
            /// The starting divider for this group.
            /// </summary>
            public PlanogramBlockingDivider Floor { get; private set; }
            
            /// <summary>
            /// The ending divider for this group.
            /// </summary>
            public PlanogramBlockingDivider Ceiling { get; private set; }
            
            /// <summary>
            /// The position to start calculating from.
            /// </summary>
            public Single StartingBound { get; private set; }
            
            /// <summary>
            /// The position to calculate up to.
            /// </summary>
            public Single EndingBound { get; private set; }

            public PerformanceCalculationDividers(
                IEnumerable<PlanogramBlockingDivider> dividers, PlanogramBlockingDivider floor, PlanogramBlockingDivider ceiling)
            {
                Dividers = dividers.ToList();
                Floor = floor;
                Ceiling = ceiling;
                StartingBound = Floor == null ? 0f : Floor.GetNormalCoordinate();
                EndingBound = Ceiling == null ? 1f : Ceiling.GetNormalCoordinate();
            }

            /// <summary>
            /// Arranges the given <paramref name="dividers"/> into groups that lie between the <paramref name="lockedDividers"/>,
            /// which are not included in any of the groups.
            /// </summary>
            /// <param name="dividers"></param>
            /// <param name="lockedDividers"></param>
            /// <returns></returns>
            public static IEnumerable<PerformanceCalculationDividers> GetPerformanceCalculationDividers(
                IEnumerable<PlanogramBlockingDivider> dividers, IEnumerable<PlanogramBlockingDivider> lockedDividers)
            {
                PlanogramBlockingDivider currentFloorDivider = null;
                PlanogramBlockingDivider currentCeilingDivider = null;
                List<PlanogramBlockingDivider> currentDividers = new List<PlanogramBlockingDivider>();
                List<PerformanceCalculationDividers> performanceCalculationDividers = new List<PerformanceCalculationDividers>();

                List<PlanogramBlockingDivider> dividersOrderedByNormalCoord = dividers.OrderBy(d => d.GetNormalCoordinate()).ToList();
                foreach (PlanogramBlockingDivider divider in dividersOrderedByNormalCoord)
                {
                    // Determine if we have reached the end of a group. This either happens if this divider is locked
                    // or if we have reached the end of the dividers.
                    Boolean dividerIsLocked = lockedDividers.Contains(divider);
                    Boolean endOfCurrentGroup =
                        dividersOrderedByNormalCoord.IndexOf(divider) == (dividersOrderedByNormalCoord.Count - 1) ||
                        dividerIsLocked;

                    // If the divider is not locked, it needs to be added to the group (locked dividers don't get added,
                    // because we don't want them to be considered in the performance calculation.
                    if (!dividerIsLocked) currentDividers.Add(divider);
                    
                    // If we're not yet at the end of the group, then we carry on looking through the dividers.
                    if (!endOfCurrentGroup) continue;

                    // If we don't yet have any dividers in the current group, then this divider is locked
                    // and is the first one we've seen. As such, we just need to skip it and set it to be the
                    // current floor.
                    if (!currentDividers.Any() && dividerIsLocked)
                    {
                        currentFloorDivider = divider;
                        continue;
                    }

                    // Now, we need to determine our floor and ceiling dividers. If we already have them (from
                    // locked dividers) then we've nothing to do. However, if they're null then we need to find 
                    // the correct dividers.
                    if (dividerIsLocked) currentCeilingDivider = divider;
                    if (currentFloorDivider == null) currentFloorDivider = currentDividers.First().GetParallelParentDivider(getTop: false);
                    if (currentCeilingDivider == null) currentCeilingDivider = currentDividers.First().GetParallelParentDivider(getTop: true);

                    // When we're ready, instantiate a new PerformanceCalculationDividers group and add it to the return list.
                    performanceCalculationDividers.Add(
                        new PerformanceCalculationDividers(currentDividers, currentFloorDivider, currentCeilingDivider));

                    // Then, we also clear off the current group variables so that they are ready for the next group.
                    // If this divider is locked, then it will form the floor of our next group.
                    currentDividers.Clear();
                    currentFloorDivider = dividerIsLocked ? divider : null;
                    currentCeilingDivider = null;
                }

                return performanceCalculationDividers;
            }
        }

        /// <summary>
        /// A simple class representing a group of <see cref="PlanogramBlockingGroup"/>s that
        /// can be merged following the removal of a <see cref="PlanogramBlockingDivider"/>.
        /// </summary>
        [Serializable]
        private sealed class MergableDividerGroups
        {
            #region Properties
            /// <summary>
            /// The <see cref="PlanogramBlockingDivider"/> that can be removed.
            /// </summary>
            public PlanogramBlockingDivider Divider { get; private set; }

            /// <summary>
            /// The <see cref="PlanogramBlockingGroup"/>s that can be merged following the removal of <see cref="Divider"/>.
            /// </summary>
            public IEnumerable<PlanogramBlockingGroup> Groups { get; private set; }

            /// <summary>
            /// The <see cref="PlanogramSequenceGroup"/> that can be used for the <see cref="PlanogramBlockingGroup"/> that
            /// survives the merging of <see cref="Groups"/>.
            /// </summary>
            public PlanogramSequenceGroup MatchingSequenceGroup { get; private set; }

            /// <summary>
            /// The amount by which <see cref="MatchingSequenceGroup"/> deviates from the <see cref="PlanogramSequenceGroup"/>s 
            /// of <see cref="Groups"/>. This is simply the number of additional products that appear in <see cref="MatchingSequenceGroup"/>.
            /// </summary>
            public Single? SequenceGroupDeviation { get; private set; } 
            #endregion

            #region Constructor
            public MergableDividerGroups(PlanogramBlockingDivider divider, IEnumerable<PlanogramBlockingGroup> groups)
            {
                Divider = divider;
                Groups = groups.ToList();

                ILookup<Single, PlanogramSequenceGroup> sequenceGroupsByDeviation = divider.Parent.Parent.Sequence.Groups
                    .Where(g => IsSequenceGroupAMatchFor(g, groups))
                    .ToLookup(g => GetDeviationFor(g, groups));

                if (!sequenceGroupsByDeviation.Any()) return;

                SequenceGroupDeviation = sequenceGroupsByDeviation.Min(g => g.Key);
                MatchingSequenceGroup = sequenceGroupsByDeviation[SequenceGroupDeviation.Value].FirstOrDefault();
            } 
            #endregion

            #region Methods

            /// <summary>
            /// Determines if the <paramref name="sequenceGroup"/> contains matching products for the <paramref name="blockingGroups"/>.
            /// </summary>
            /// <param name="sequenceGroup"></param>
            /// <param name="blockingGroups"></param>
            /// <returns></returns>
            private Boolean IsSequenceGroupAMatchFor(
                PlanogramSequenceGroup sequenceGroup, IEnumerable<PlanogramBlockingGroup> blockingGroups)
            {
                IEnumerable<String> blockingGroupsGtins = GetBlockingGroupGtins(blockingGroups);
                IEnumerable<String> sequenceGroupGtins = GetSequenceGroupGtins(sequenceGroup);
                foreach (String gtin in blockingGroupsGtins)
                {
                    if (sequenceGroupGtins.Contains(gtin)) continue;
                    return false;
                }
                return true;
            }

            private static IEnumerable<String> GetSequenceGroupGtins(PlanogramSequenceGroup sequenceGroup)
            {
                IEnumerable<String> sequenceGroupGtins = sequenceGroup.Products.Select(p => p.Gtin).ToList();
                return sequenceGroupGtins;
            }

            private static IEnumerable<String> GetBlockingGroupGtins(IEnumerable<PlanogramBlockingGroup> blockingGroups)
            {
                IEnumerable<String> blockingGroupsGtins = blockingGroups.SelectMany(g =>
                {
                    var sg = g.GetPlanogramSequenceGroup();
                    if (sg == null) return new List<String>();
                    return sg.Products.Select(p => p.Gtin);
                });
                return blockingGroupsGtins;
            }

            private Single GetDeviationFor(PlanogramSequenceGroup sequenceGroup, IEnumerable<PlanogramBlockingGroup> blockingGroups)
            {
                return GetSequenceGroupGtins(sequenceGroup).Count() - GetBlockingGroupGtins(blockingGroups).Count();
            }

            /// <summary>
            /// Creates a new sequence group, if appropriate, using the gtins from all the sequence groups
            /// associated with <see cref="Groups"/>, arranging them in a default sequence order.
            /// </summary>
            public void CreateMatchingSequenceGroup()
            {
                // Find the order that the sequence products in each of the original sequence groups should appear
                // in the new sequence group by sequencing the blocking groups using default directions and then
                // combining their sequence group products in sequence order.
                IEnumerable<String> gtinsInSequence = PlanogramBlockingGroup.NewPlanogramBlockingGroup()
                    .EnumerateSequencableItemsInSequence<PlanogramBlockingGroup>(Groups)
                    .SelectMany(g =>
                        {
                            var sg = g.Item.GetPlanogramSequenceGroup();
                            if (sg == null) return new List<PlanogramSequenceGroupProduct>();
                            return sg.Products.OrderBy(p => p.SequenceNumber).ToList();
                        })
                    .Select(p => p.Gtin);

                // If we didn't find any products, there were probably no sequence groups for the blocking groups
                // in the first place. As such, we won't add a new blank sequence group, we'll just leave the sequence
                // as is.
                if (!gtinsInSequence.Any()) return;

                // Ensure that we use the first instance of the gtins in sequence;
                List<String> uniqueGtinsInSequence = new List<String>();
                foreach (String gtin in gtinsInSequence)
                {
                    if (uniqueGtinsInSequence.Contains(gtin)) continue;
                    uniqueGtinsInSequence.Add(gtin);
                }

                // Construct a new sequence group containing all of the products from the original groups.
                MatchingSequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                Int32 sequenceNumber = 0;
                foreach (String gtin in uniqueGtinsInSequence)
                {
                    PlanogramSequenceGroupProduct sequenceProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                    sequenceProduct.Gtin = gtin;
                    sequenceProduct.SequenceNumber = sequenceNumber++;
                    MatchingSequenceGroup.Products.Add(sequenceProduct);
                }

                // Add the group the sequence, which assigns it a unique colour.
                this.Divider.Parent.Parent.Sequence.Groups.Add(MatchingSequenceGroup);
            } 
            #endregion
        }

        /// <summary>
        /// A simple context object for the <see cref="ApplyToFixtures"/> and <see cref="ApplyPerformanceData"/>
        /// methods, encapsulating the data that both methods need.
        /// </summary>
        [Serializable]
        internal sealed class ApplyBlockingContext
        {
            #region Properties
            public Single BlockingWidth { get; private set; }
            public Single BlockingHeight { get; private set; }
            public Single BlockingHeightOffset { get; private set; }
            /// <summary>
            /// A dictionary of divider groupings keyed by divider level number. For each level number, there 
            /// are a collection of groups of dividers representing the dividers of that level grouped into bands
            /// sharing the same bounding edges.
            /// </summary>
            public Dictionary<Byte, IEnumerable<IEnumerable<PlanogramBlockingDivider>>> DividerGroupings { get; private set; }
            public IEnumerable<PlanogramBlockingDivider> VerticalDividers { get; private set; }
            public HashSet<PlanogramBlockingDivider> SnappedDividers { get; private set; }
            public IEnumerable<Single> FixtureBoundaries { get; private set; }
            public Dictionary<PlanogramBlockingDivider, String> OriginalDividerPositions { get; private set; } 
            #endregion

            #region Constructor
            public ApplyBlockingContext(PlanogramBlocking blocking)
            {
                SnappedDividers = new HashSet<PlanogramBlockingDivider>();
                VerticalDividers = blocking.Dividers.Where(d => d.Type == PlanogramBlockingDividerType.Vertical).ToList();
                FixtureBoundaries = blocking.Parent.FixtureItems.GetRelativeFixtureBoundaries();

                // Get Planogram size.            
                Single blockingWidth, blockingHeight, blockingOffset;
                blocking.Parent.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out blockingOffset);
                BlockingWidth = blockingWidth;
                BlockingHeight = blockingHeight;
                BlockingHeightOffset = blockingOffset;

                DividerGroupings = new Dictionary<Byte, IEnumerable<IEnumerable<PlanogramBlockingDivider>>>();
                foreach (IGrouping<Byte, PlanogramBlockingDivider> levelDividers in blocking.Dividers.ToLookup(d => d.Level))
                {
                    DividerGroupings.Add(levelDividers.Key, GroupLevelDividers(levelDividers));
                }

                OriginalDividerPositions = blocking.Dividers.ToDictionary(d => d, d => String.Format("Type: {0}, X: {1}, Y: {2}", d.Type, d.X, d.Y));
            } 
            #endregion

            #region Methods
            /// <summary>
            /// Groups the given dividers based on the parent band that they lie in.
            /// </summary>
            /// <param name="dividers">The dividers to group.</param>
            /// <returns>An empty list of dividers is empty.</returns>
            /// <remarks>
            /// Dividers are grouped by their starting position and the Id of the divider that represents the parent above.
            /// </remarks>
            /// <exception cref="ArgumentException">Thrown if not all of the dividers are of the same level.</exception>
            internal static IEnumerable<IEnumerable<PlanogramBlockingDivider>> GroupLevelDividers(
                IEnumerable<PlanogramBlockingDivider> dividers)
            {
                if (dividers.Select(d => d.Level).Distinct().Count() > 1)
                    throw new ArgumentException("dividers must all be of the same level");
                if (!dividers.Any()) return new List<List<PlanogramBlockingDivider>>();

                Boolean isHorizontal = dividers.First().Type == PlanogramBlockingDividerType.Horizontal;
                return dividers.GroupBy(d =>
                {
                    PlanogramBlockingDivider ceilingDivider = d.GetParallelParentDivider(true);
                    Int32? ceilingDividerId = ceilingDivider == null ? null : ceilingDivider.Id as Int32?;
                    return new Tuple<Single, Int32?>(isHorizontal ? d.X : d.Y, ceilingDividerId);
                });
            } 
            #endregion
        }

        /// <summary>
        /// Holds state information for a <see cref="PlanogramBlocking"/> instances, storing information
        /// about dividers and their co-ordinates.
        /// </summary>
        [Serializable]
        private sealed class DividerStates : IEnumerable<DividerState>
        {
            #region Fields
            private List<DividerState> _states; 
            #endregion

            #region Properties
            /// <summary>
            /// Indicates whether or not this collection has been commited.
            /// </summary>
            public Boolean Committed { get; private set; } 
            #endregion

            #region Constructors
            public DividerStates(PlanogramBlocking blocking)
            {
                _states = blocking.Dividers.Select(d => new DividerState(d)).ToList();
                Committed = false;
            } 
            #endregion

            #region Methods
            /// <summary>
            /// Commits all the states in this collection.
            /// </summary>
            public void Commit()
            {
                foreach (DividerState state in _states)
                {
                    state.Commit();
                }
                Committed = true;
            }

            public IEnumerator<DividerState> GetEnumerator()
            {
                return _states.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return _states.GetEnumerator();
            } 
            #endregion
        }

        /// <summary>
        /// Holds state information about a particular <see cref="PlanogramBlockingDivider"/> by interrogating
        /// its X/Y co-ordinated based on its orientation.
        /// </summary>
        [Serializable]
        private sealed class DividerState
        {
            #region Properties
            /// <summary>
            /// The <see cref="PlanogramBlockingDivider"/> that this state refers to.
            /// </summary>
            public PlanogramBlockingDivider Divider { get; private set; }

            /// <summary>
            /// The X co-ordinate stored in this state for <see cref="Divider"/>.
            /// </summary>
            public Single X { get; private set; }

            /// <summary>
            /// The Y co-ordinate stored in this state for <see cref="Divider"/>.
            /// </summary>
            public Single Y { get; private set; }

            /// <summary>
            /// The Length stored in this state for <see cref="Divider"/>.
            /// </summary>
            public Single Length { get; private set; }

            /// <summary>
            /// Indicates whether or not this state has been committed.
            /// </summary>
            public Boolean Committed { get; private set; } 
            #endregion

            #region Constructors
            public DividerState(PlanogramBlockingDivider divider)
            {
                Divider = divider;
                X = divider.X;
                Y = divider.Y;
                Length = divider.Length;
                Committed = false;
            } 
            #endregion

            #region Methods
            /// <summary>
            /// Commits this state to the <see cref="Divider"/>.
            /// </summary>
            public void Commit()
            {
                Divider.SetState(X, Y, Length);
                Committed = true;
            } 
            #endregion
        }

        /// <summary>
        /// Represents a <see cref="PlanogramBlockingDivider"/> that is constrained to particular position
        /// </summary>
        private sealed class DividerConstraint
        {
            /// <summary>
            /// The <see cref="PlanogramBlockingDivider"/> that this constraint applies to.
            /// </summary>
            public PlanogramBlockingDivider Divider { get; }

            /// <summary>
            /// The co-ordinate in the <see cref="Divider"/>'s normal axis that it is constrained to.
            /// </summary>
            public Single Constraint { get; }

            private DividerConstraint(PlanogramBlockingDivider divider, Single constraint)
            {
                Constraint = constraint;
                Divider = divider;
            }

            /// <summary>
            /// Gets the <see cref="DividerConstraint"/>s for the given <paramref name="blocking"/> by checking
            /// the current co-ordinates of the dividers against their limitations and <paramref name="originalCoordsByDivider"/>.
            /// </summary>
            /// <param name="blocking"></param>
            /// <param name="originalCoordsByDivider"></param>
            /// <returns></returns>
            public static IEnumerable<DividerConstraint> GetDividerConstraints(
                PlanogramBlocking blocking, 
                Dictionary<PlanogramBlockingDivider, Single> originalCoordsByDivider)
            {
                List<DividerConstraint> dividerConstraints = new List<DividerConstraint>();

                foreach (PlanogramBlockingDivider divider in blocking.Dividers.Where(d => d.IsLimited))
                {
                    DividerConstraint dividerConstraint;
                    Single originalCoordinate;
                    if (!originalCoordsByDivider.TryGetValue(divider, out originalCoordinate)) continue;

                    if (divider.LimitedPercentage.EqualTo(0))
                    {
                        // If the limited percentage is zero, this means that the divider isn't allowed to move at all,
                        // so we set the constraint coordinate to be the original coordinate.
                        dividerConstraint = new DividerConstraint(divider, originalCoordinate);
                    }
                    else if (divider.GetNormalCoordinate().GreaterThan(originalCoordinate + divider.LimitedPercentage))
                    {
                        // If the divider is above its original coordinate plus its limited percentage, we need to constrain
                        // it to the original coord plus the limited percentage as an upper bound.
                        dividerConstraint = new DividerConstraint(divider, originalCoordinate + divider.LimitedPercentage);
                    }
                    else if (divider.GetNormalCoordinate().LessThan(originalCoordinate - divider.LimitedPercentage))
                    {
                        // If the divider is below its original coordinate minus its limited percentage, we need to constrain
                        // it to the original coordinate minus the limited percentage as a lower bound.
                        dividerConstraint = new DividerConstraint(divider, originalCoordinate - divider.LimitedPercentage);
                    }
                    else
                    {
                        // If the current position is between the limited percentage bounds then we don't need to constrain
                        // this divider, because it is complying with its constraints.
                        continue;
                    }

                    dividerConstraints.Add(dividerConstraint);
                }

                return dividerConstraints;
            }

            /// <summary>
            /// Gets the co-ordinater change required to get the <see cref="Divider"/> from its
            /// current normal co-ordinate to the <see cref="Constraint"/>.
            /// </summary>
            /// <returns></returns>
            internal Single GetDelta()
            {
                return Constraint - Divider.GetNormalCoordinate();
            }
        }

        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramBlockingList)base.Parent).Parent; }
        }
        #endregion

        #region Events

        /// <summary>
        ///     Occurs when the blocking has completed a change, including any chained updates.
        /// </summary>
        public event EventHandler BlockingChangeCompleted;

        /// <summary>
        ///     Raises the <see cref="BlockingChangeCompleted"/> event for any subscribers.
        /// </summary>
        internal void OnBlockingChangeCompleted()
        {
            if (BlockingChangeCompleted != null) BlockingChangeCompleted.Invoke(this, new EventArgs());
        }

        #endregion

        #region Properties

        #region Type
        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingType> TypeProperty =
            RegisterModelProperty<PlanogramBlockingType>(c => c.Type);
        /// <summary>
        /// The type of blocking this is
        /// </summary>
        public PlanogramBlockingType Type
        {
            get { return GetProperty<PlanogramBlockingType>(TypeProperty); }
            set { SetProperty<PlanogramBlockingType>(TypeProperty, value); }
        }
        #endregion

        #region Groups
        /// <summary>
        /// Groups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupList> GroupsProperty =
            RegisterModelProperty<PlanogramBlockingGroupList>(c => c.Groups, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The list of groups
        /// </summary>
        public PlanogramBlockingGroupList Groups
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingGroupList>(
                    GroupsProperty,
                    new PlanogramBlockingGroupList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of groups
        /// </summary>
        public PlanogramBlockingGroupList GroupsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingGroupList>(
                    GroupsProperty,
                    new PlanogramBlockingGroupList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of groups
        /// </summary>
        IEnumerable<IPlanogramBlockingGroup> IPlanogramBlocking.Groups
        {
            get { return this.Groups; }
        }

        #endregion

        #region Dividers
        /// <summary>
        /// Dividers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingDividerList> DividersProperty =
            RegisterModelProperty<PlanogramBlockingDividerList>(c => c.Dividers, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The list of dividers
        /// </summary>
        public PlanogramBlockingDividerList Dividers
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingDividerList>(
                    DividersProperty,
                    new PlanogramBlockingDividerList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of dividers
        /// </summary>
        public PlanogramBlockingDividerList DividersAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingDividerList>(
                    DividersProperty,
                    new PlanogramBlockingDividerList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of dividers
        /// </summary>
        IEnumerable<IPlanogramBlockingDivider> IPlanogramBlocking.Dividers
        {
            get { return this.Dividers; }
        }

        #endregion

        #region Locations
        /// <summary>
        /// Locations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingLocationList> LocationsProperty =
            RegisterModelProperty<PlanogramBlockingLocationList>(c => c.Locations, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The list of locations
        /// </summary>
        public PlanogramBlockingLocationList Locations
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingLocationList>(
                    LocationsProperty,
                    new PlanogramBlockingLocationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of locations
        /// </summary>
        public PlanogramBlockingLocationList LocationsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingLocationList>(
                    LocationsProperty,
                    new PlanogramBlockingLocationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of locations
        /// </summary>
        IEnumerable<IPlanogramBlockingLocation> IPlanogramBlocking.Locations
        {
            get { return this.Locations; }
        }

        #endregion

        /// <summary>
        /// The current edit level, which is the number of states currently held, in the snapping edit state stack.
        /// </summary>
        private Int32 SnappingEditLevel
        {
            get
            {
                return
                    _snappingEditStates == null ?
                    0 :
                    _snappingEditStates.Count;
            }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new UniqueList<PlanogramBlockingGroup>(GroupsProperty, PlanogramBlockingGroup.NameProperty));
            BusinessRules.AddRule(new UniqueList<PlanogramBlockingGroup>(GroupsProperty, PlanogramBlockingGroup.ColourProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlocking NewPlanogramBlocking(PlanogramBlockingType type)
        {
            PlanogramBlocking item = new PlanogramBlocking();
            item.Create(type);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="type">The type of blocking</param>
        /// <param name="source">The source to copy values from</param>
        /// <returns></returns>
        public static PlanogramBlocking NewPlanogramBlocking(PlanogramBlockingType type, IPlanogramBlocking source)
        {
            PlanogramBlocking item = new PlanogramBlocking();
            item.Create(type, source);
            return item;
        }

        #endregion  

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramBlockingType type)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramBlockingType>(TypeProperty, type);
            this.LoadProperty<PlanogramBlockingGroupList>(GroupsProperty, PlanogramBlockingGroupList.NewPlanogramBlockingGroupList());
            this.LoadProperty<PlanogramBlockingDividerList>(DividersProperty, PlanogramBlockingDividerList.NewPlanogramBlockingDividerList());
            this.LoadProperty<PlanogramBlockingLocationList>(LocationsProperty, PlanogramBlockingLocationList.NewPlanogramBlockingLocationList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramBlockingType type, IPlanogramBlocking source)
        {
            IResolveContext context = new CopyContext();

            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramBlockingType>(TypeProperty, type);
            this.LoadProperty<PlanogramBlockingGroupList>(GroupsProperty, PlanogramBlockingGroupList.NewPlanogramBlockingGroupList(source.Groups, context));
            this.LoadProperty<PlanogramBlockingDividerList>(DividersProperty, PlanogramBlockingDividerList.NewPlanogramBlockingDividerList(source.Dividers, context));
            this.LoadProperty<PlanogramBlockingLocationList>(LocationsProperty, PlanogramBlockingLocationList.NewPlanogramBlockingLocationList(source.Locations, context));
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Enumerates the <see cref="Groups"/> in the order they should be processed for optimisation.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Note that at present, the blocks are ordered by the minimum rank of their products.
        /// </remarks>
        public IOrderedEnumerable<PlanogramBlockingGroup> EnumerateGroupsInOptimisationOrder()
        {
            return Groups
                .OrderBy(g => 
                {
                    PlanogramSequenceGroup sequenceGroup = g.GetPlanogramSequenceGroup();
                    if(sequenceGroup == null || !sequenceGroup.Products.Any()) return Int32.MaxValue;
                    return sequenceGroup.Products
                        .Min(p => 
                        {
                            PlanogramAssortmentProduct assortmentProduct = Parent.Assortment.Products
                                .FirstOrDefault(ap => ap.Gtin.Equals(p.Gtin));
                            if(assortmentProduct == null) return Int32.MaxValue;
                            return assortmentProduct.Rank;
                        });
                })
                .ThenBy(g => g.Colour);
        }

        /// <summary>
        /// Get the design view relative spaces of the locations and arrange them in a dictionary.
        /// </summary>
        public Dictionary<PlanogramBlockingLocation, RectValue> GetLocationSpacesByLocation(Single blockingWidth, Single blockingHeight)
        {
            return Locations.ToDictionary(
                l => l,
                l => new RectValue(l.X * blockingWidth, l.Y * blockingHeight, 0f, l.Width * blockingWidth, l.Height * blockingHeight, 0f));
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramBlocking>(oldId, newId);
        }

        /// <summary>
        /// Returns the next available group colour
        /// </summary>
        /// <returns></returns>
        public Int32 GetNextBlockingGroupColour()
        {
            return BlockingHelper.GetNextBlockingGroupColour(this);
        }

        /// <summary>
        /// Called when a child object of this Blocking model is changed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);
            
            // Check modified blocking groups for duplicate names.
            if (e.ChildObject is PlanogramBlockingGroup)
            {
                BusinessRules.CheckRules(GroupsProperty);
                if (e.PropertyChangedArgs.PropertyName.Equals(PlanogramBlockingGroup.NameProperty.Name))
                {
                    EnsureGroupNamesAreUnique(e.ChildObject);
                }
                else if (e.PropertyChangedArgs.PropertyName.Equals(PlanogramBlockingGroup.ColourProperty.Name))
                {
                    EnsureGroupColoursAreUnique(e.ChildObject);
                }
            }

            // Check newly added blocking groups for duplicate names.
            if (e.ChildObject is PlanogramBlockingGroupList)
            {
                BusinessRules.CheckRules(GroupsProperty);
                if (e.CollectionChangedArgs != null && e.CollectionChangedArgs.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (Object newItem in e.CollectionChangedArgs.NewItems)
                    {
                        EnsureGroupNamesAreUnique(newItem);
                        EnsureGroupColoursAreUnique(newItem);
                    }
                }
            }
        }

        Boolean _suppressColourChangeChecks = false;
        private void EnsureGroupColoursAreUnique(Object blockingGroup)
        {
            if(_suppressColourChangeChecks) return;
            PlanogramBlockingGroup objectAsBlockingGroup = blockingGroup as PlanogramBlockingGroup;
            if (objectAsBlockingGroup == null) return;

            if (Groups.Select(g => g.Colour).Distinct().Count() < Groups.Count)
            {
                IEnumerable<PlanogramBlockingGroup> groupsWithMatchingColour = 
                    Groups.Where(g => g.Colour.Equals(objectAsBlockingGroup.Colour));
                if (groupsWithMatchingColour.Count() > 1)
                {
                    // We want to leave the group that changed as is and get unique colours for all
                    // the other offending groups.
                    _suppressColourChangeChecks = true;
                    foreach (PlanogramBlockingGroup offendingGroup in groupsWithMatchingColour.Where(g => g != objectAsBlockingGroup))
                    {
                        offendingGroup.Colour = BlockingHelper.GetNextBlockingGroupColour(this, Parent.Sequence);
                    }
                    _suppressColourChangeChecks = false;
                }
            }
        }

        /// <summary>
        /// Checks all the group names are unique as a result of the name of the given blocking group
        /// and makes them unique if they aren't.
        /// </summary>
        /// <param name="blockingGroup">An object containing the blocking group to be checked.</param>
        private void EnsureGroupNamesAreUnique(Object blockingGroup)
        {
            PlanogramBlockingGroup objectAsBlockingGroup = blockingGroup as PlanogramBlockingGroup;
            if (objectAsBlockingGroup == null) return;

            // If there are fewer unique names than groups...
            if (Groups.Select(g => g.Name).Distinct().Count() < Groups.Count)
            {
                // If this name change is the culprit
                if (Groups.Where(g => g.Name.Equals(objectAsBlockingGroup.Name)).Count() > 1)
                {
                    Int32 countDuplication = 0;
                    String newName = CheckSelectedBlockGroupNameDuplication(objectAsBlockingGroup.Name.Trim(), countDuplication);
                    objectAsBlockingGroup.Name = newName;
                }
            }
        }

        /// <summary>
        /// Recursively checks the given group name, appending the number of times it appears within <see cref="Groups"/> minus one
        /// to the end of the name.
        /// </summary>
        /// <param name="groupName">The group name to check.</param>
        /// <param name="count">The current count of <paramref name="groupName"/></param>
        /// <returns>The amended group name.</returns>
        private String CheckSelectedBlockGroupNameDuplication(String groupName, Int32 count)
        {
            String name = String.Format("{0} ({1})", groupName, count);
            if (Groups.Any(p => p.Name.ToLowerInvariant().Trim() == name.ToLowerInvariant().Trim()))
            {
                count++;
                return CheckSelectedBlockGroupNameDuplication(groupName, count);
            }
            else
            {
                return name;
            }
        }

        /// <summary>
        /// Gets stacks of products arranged in the sequence order that their positions appear on the plan, keyed by the
        /// sequence group/blocking group colour that they are related to. Whilst building the dictionary, position placements
        /// in the <paramref name="merchandisingGroups"/> have their sequence number and colour properties set to match.
        /// </summary>
        /// <param name="merchandisingGroups"></param>
        /// <returns></returns>
        public Dictionary<Int32, Stack<PlanogramProduct>> GetAndUpdateProductStacksByColour(IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups)
        {
            Dictionary<Int32, Stack<PlanogramProduct>> productStacksByColour = new Dictionary<Int32, Stack<PlanogramProduct>>();
            if (Parent == null) return productStacksByColour;
            Dictionary<String, PlanogramProduct> productsByGtin = Parent.Products.ToDictionary(p => p.Gtin);
            Dictionary<Int32, List<PlanogramPositionPlacement>> positionsBySequenceColour = GetPositionsBySequenceColour(merchandisingGroups);

            foreach (PlanogramBlockingGroup blockingGroup in Groups)
            {
                List<PlanogramPositionPlacement> positionPlacements;
                if (!positionsBySequenceColour.TryGetValue(blockingGroup.Colour, out positionPlacements)) continue;

                PlanogramSequenceGroup sequencingGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                foreach (PlanogramPositionPlacement positionPlacement in positionPlacements)
                {
                    sequencingGroup.Products.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(positionPlacement.Product));
                }

                // Sequence the group forcing it to use only the positions that we have supplied.
                sequencingGroup.SequenceProducts(blockingGroup, positionPlacements: positionPlacements);

                // Ensure that the positions' sequence information is up-to-date
                foreach (PlanogramPositionPlacement positionPlacement in positionPlacements)
                {
                    positionPlacement.BeginEdit();
                    positionPlacement.Position.SetSequenceGroup(
                        sequencingGroup.Products.FirstOrDefault(p => p.Gtin.Equals(positionPlacement.Product.Gtin)));
                    positionPlacement.ApplyEdit();
                }

                // Stack the sequenced products in reverse sequence order.
                Stack<PlanogramProduct> productStack = new Stack<PlanogramProduct>();
                foreach (PlanogramSequenceGroupProduct sequenceProduct in
                    sequencingGroup.Products.OrderByDescending(p => p.SequenceNumber))
                {
                    PlanogramProduct product;
                    if (!productsByGtin.TryGetValue(sequenceProduct.Gtin, out product)) continue;
                    productStack.Push(product);
                }

                productStacksByColour.Add(blockingGroup.Colour, productStack);
            }

            return productStacksByColour;
        }

        private Dictionary<Int32, List<PlanogramPositionPlacement>> GetPositionsBySequenceColour(
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups)
        {
            Dictionary<Int32, List<PlanogramPositionPlacement>> positionsBySequenceColour =
                new Dictionary<Int32, List<PlanogramPositionPlacement>>();
            if (Parent == null) return positionsBySequenceColour;

            //Pre-Populate sequence colours so that we don't loose any if they have no positions
            positionsBySequenceColour = Parent.Sequence.Groups.ToDictionary(g => g.Colour, k => new List<PlanogramPositionPlacement>());

            Dictionary<String, List<PlanogramSequenceGroup>> sequenceGroupsByGtin = new Dictionary<String, List<PlanogramSequenceGroup>>();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in Parent.Sequence.Groups.SelectMany(g => g.Products))
            {
                List<PlanogramSequenceGroup> sequenceGroups;
                if (!sequenceGroupsByGtin.TryGetValue(sequenceProduct.Gtin, out sequenceGroups))
                {
                    sequenceGroups = new List<PlanogramSequenceGroup>();
                    sequenceGroupsByGtin.Add(sequenceProduct.Gtin, sequenceGroups);
                }
                sequenceGroups.Add(sequenceProduct.Parent);
            }

            foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroups.SelectMany(m => m.PositionPlacements))
            {
                List<PlanogramSequenceGroup> sequenceGroups;
                if (!sequenceGroupsByGtin.TryGetValue(positionPlacement.Product.Gtin, out sequenceGroups)) continue;

                // Determine which sequence group this position belongs to.
                PlanogramSequenceGroup sequenceGroup;
                if (sequenceGroups.Count == 1)
                {
                    sequenceGroup = sequenceGroups.First();
                }
                else
                {
                    // Work out which sequence group this position belongs to.
                    sequenceGroup = sequenceGroups.FirstOrDefault(g => g.Colour.Equals(positionPlacement.Position.SequenceColour));
                    if (sequenceGroup == null)
                    {
                        sequenceGroup = GetPositionsSequenceGroup(positionPlacement, sequenceGroups);
                    }
                }

                List<PlanogramPositionPlacement> positionPlacements;
                if (!positionsBySequenceColour.TryGetValue(sequenceGroup.Colour, out positionPlacements))
                {
                    positionPlacements = new List<PlanogramPositionPlacement>();
                    positionsBySequenceColour.Add(sequenceGroup.Colour, positionPlacements);
                }

                positionPlacements.Add(positionPlacement);
            }

            return positionsBySequenceColour;
        }

        /// <summary>
        /// Gets the most appropriate sequence group that the product relates too 
        /// Firstly attempts to find a matching sequence group (If there is only 1 this wins)
        /// If there are more than 1 sequence group to be considered we take the one with fewest sequence products then by sequence group name.
        /// </summary>
        /// <param name="positionPlacement">The position placement to lookup</param>
        /// <param name="sequenceGroups">The initial matching sequence groups containing the product</param>
        /// <returns></returns>
        private static PlanogramSequenceGroup GetPositionsSequenceGroup(PlanogramPositionPlacement positionPlacement, List<PlanogramSequenceGroup> sequenceGroups)
        {
            // Get all of the sub component positions
            List<PlanogramPosition> subComponentPositions = positionPlacement.SubComponentPlacement.GetPlanogramPositions().ToList();
            AxisType facingAxis = positionPlacement.SubComponentPlacement.GetFacingAxis();
            Int16 placementSequence = positionPlacement.Position.GetSequence(facingAxis);

            List<PlanogramSequenceGroup> sequenceGroupLookups = sequenceGroups;
            Int16 placementSequenceRange = 1;
            PlanogramSequenceGroup sequenceGroup = null;

            while (true)
            {
                // Get pre \ pos positions within sequence range
                List<PlanogramPosition> prePlanogramPositions = subComponentPositions.Where(p => p.GetSequence(facingAxis) < placementSequence && p.GetSequence(facingAxis) >= placementSequence - placementSequenceRange).OrderBy(p => p.GetSequence(facingAxis)).ToList();
                List<PlanogramPosition> postPlanogramPositions = subComponentPositions.Where(p => p.GetSequence(facingAxis) > placementSequence && p.GetSequence(facingAxis) <= placementSequence + placementSequenceRange).OrderBy(p => p.GetSequence(facingAxis)).ToList();

                List<String> gtinSequenceSearch = new List<String>(); // The list of product gtins in sequence order
                Boolean containsMaxItemsWithinRange = false; // if the count doesnt match the range we have searched as far as we could
                List<PlanogramSequenceGroup> availableSequenceGroups = new List<PlanogramSequenceGroup>(); // available sequence groups within range
                if (prePlanogramPositions.Any())
                {
                    gtinSequenceSearch.AddRange(prePlanogramPositions.Select(p => p.GetPlanogramProduct().Gtin));
                    containsMaxItemsWithinRange = prePlanogramPositions.Count == placementSequenceRange;
                }
                gtinSequenceSearch.Add(positionPlacement.Product.Gtin);

                if (postPlanogramPositions.Any())
                {
                    gtinSequenceSearch.AddRange(postPlanogramPositions.Select(p => p.GetPlanogramProduct().Gtin));
                    if (!containsMaxItemsWithinRange) { containsMaxItemsWithinRange = postPlanogramPositions.Count == placementSequenceRange; }
                }

                if (containsMaxItemsWithinRange)
                {
                    placementSequenceRange++;

                    // Find a sequence group that contains the products in this sequence
                    foreach (PlanogramSequenceGroup planogramSequenceGroup in sequenceGroups)
                    {
                        IEnumerable<PlanogramSequenceGroupProduct> planogramSequenceGroupProducts = planogramSequenceGroup.Products.Where(p => gtinSequenceSearch.Contains(p.Gtin));
                        if (planogramSequenceGroupProducts.Count() == gtinSequenceSearch.Count)
                        {
                            Int16 sequenceOrder = 0;
                            Boolean matchingSequenceGroup = true;
                            foreach (String productGtin in planogramSequenceGroupProducts.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin))
                            {
                                if (productGtin != gtinSequenceSearch[sequenceOrder++])
                                {
                                    matchingSequenceGroup = false;
                                    break;
                                }
                            }
                            if (matchingSequenceGroup) { availableSequenceGroups.Add(planogramSequenceGroup); } // there are sequence groups that contain products in this sequence
                        }
                    }
                }

                if (availableSequenceGroups.Any())
                {
                    if (availableSequenceGroups.Count == 1) // have we found a matching sequence group
                    {
                        sequenceGroup = availableSequenceGroups.First();
                        break;
                    }
                    sequenceGroupLookups = availableSequenceGroups.ToList(); // remember previous selections
                }
                else
                {
                    // no more selections can be found, find most appropriate from the sequence groups we were able to lookup
                    // The sequence group with the fewest products then by name
                    sequenceGroup = sequenceGroupLookups.OrderBy(p => p.Products.Count).ThenBy(p => p.Name).First();
                    break;
                }
            }
            return sequenceGroup;
        }

        #region Apply Blocking Methods

        #region Apply To Fixtures
        
        /// <summary>
        /// Applies the given Planogram Blocking to its Parent's fixtures.
        /// </summary>
        /// <param name="planogramBlocking">The Planogram Blocking to apply to fixtures.</param>
        public List<PlanogramBlockingResultType> ApplyToFixtures()
        {
            List<PlanogramBlockingResultType> planogramBlockingResults = new List<PlanogramBlockingResultType>();
            if (this.Parent == null || !this.Dividers.Any()) return planogramBlockingResults;

            // Prepare a context object to store all our frequently used values.
            ApplyBlockingContext context = new ApplyBlockingContext(this);

            // Iterate down through the divider hierarchy, snapping each level in turn.
            Byte level = PlanogramBlockingDivider.RootLevelNumber;
            while (true)
            {
                // Get dividers at this level and break if none are found.
                IEnumerable<PlanogramBlockingDivider> levelDividers = this.Dividers.Where(d => d.Level == level).ToList();
                if (!levelDividers.Any()) break;

                Debug.WriteLine("--------------------");
                Debug.WriteLine(String.Format("Processing divider level {0}", level));

                // Group dividers into their parent bands. This ensures that they are sorted into asc/desc order
                // based on the available snapping boundaries in their band, which is fixed since we are
                // increase level number on each iteration.
                IEnumerable<IEnumerable<PlanogramBlockingDivider>> dividerGroups;
                if (!context.DividerGroupings.TryGetValue(level, out dividerGroups)) continue;
                foreach (IEnumerable<PlanogramBlockingDivider> dividerGroup in dividerGroups)
                {
                    // Get the snappable boundaries count for this level.
                    Int32 boundariesCount = GetFlattenedBoundaries(dividerGroup, context).Count();

                    // Order dividers list based on whether or not there are enough snappable boundaries 
                    // for all dividers.
                    IEnumerable<PlanogramBlockingDivider> orderedDividersToSnap = OrderDividersToSnap(dividerGroup, boundariesCount);

                    // Snap the group of dividers and log a result if unsuccessful.
                    Boolean success = SnapDividers(orderedDividersToSnap, boundariesCount, context);
                    if (!success) planogramBlockingResults.Add(PlanogramBlockingResultType.InsufficientComponentSpace);
                }

                level++;
            }

            return planogramBlockingResults;
        }

        /// <summary>
        /// Snaps a group of horizontal or vertical dividers ensuring that, if there is sufficient fixture boundary space, all
        /// dividers are snapped.
        /// </summary>
        /// <param name="snappedDividers">A collection of dividers indicating which ones have been snapped. This collection will be modified by this method.</param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <param name="blockingOffset"></param>
        /// <param name="fixtureBoundaries"></param>
        /// <param name="verticalDividers"></param>
        /// <param name="dividersToSnap">The dividers to snap.</param>
        /// <param name="snappableBoundariesCount">The number of snappable boundaries first available to this group of dividers.</param>
        /// <returns></returns>
        private Boolean SnapDividers(
            IEnumerable<PlanogramBlockingDivider> dividersToSnap,
            Int32 snappableBoundariesCount,
            ApplyBlockingContext context)
        {
            Boolean success = true;
            Int32 dividersToSnapCount = dividersToSnap.Count();

            // Get the original flattened collection of boundaries that could be snapped to, in case we need to force
            // the dividers to snap somewhere.
            IEnumerable<Single> flattenedBoundaries = GetFlattenedBoundaries(dividersToSnap, context).ToList();

            // Begin a snapping edit, in case we need to revert to the starting state when merging blocks.
            Int32 startingEditLevel = SnappingEditLevel;
            BeginSnappingEdit();

            foreach (PlanogramBlockingDivider dividerToSnap in dividersToSnap)
            {
                Debug.WriteLine(String.Format("Snapping divider originally at - {0}", context.OriginalDividerPositions[dividerToSnap]));

                if (dividerToSnap.Type == PlanogramBlockingDividerType.Horizontal)
                {
                    // Try to move the divider to the most appropriate boundary.
                    Boolean dividerCouldBeSnapped = TryMoveDividerToSnappableBoundaries(dividerToSnap, context);
                    if (dividerCouldBeSnapped)
                    {
                        Debug.WriteLine(String.Format("Divider was snapped successfully to - X: {0}, Y: {1}", dividerToSnap.X, dividerToSnap.Y));
                        continue;
                    }

                    Debug.WriteLine("Divider was not snapped successfully");

                    // First, revert the blocking to the state it was in at the start of the method.
                    while (SnappingEditLevel > startingEditLevel) CancelSnappingEdit();

                    // Now, Before trying anything else, we try to force the dividers to snap using the
                    // available boundaries.
                    Int32 preForceEditLevel = SnappingEditLevel;
                    BeginSnappingEdit();
                    success = ForceDividersToSnap(dividersToSnap, flattenedBoundaries, context);

                    // If this worked, then we're good to go. However, if it didn't we need to try
                    // merging blocks.
                    if (success) return true;

                    // If forcing the dividers to snap didn't work, revert the state to before we started.
                    while (SnappingEditLevel > preForceEditLevel) CancelSnappingEdit();

                    // We also need all the groups that could be merged for each divider.
                    List<MergableDividerGroups> mergableDividerGroups = new List<MergableDividerGroups>();
                    foreach (PlanogramBlockingDivider divider in dividersToSnap)
                    {
                        List<PlanogramBlockingGroup> dividerGroups = divider.GetLocations()
                            .Select(l => l.GetPlanogramBlockingGroup())
                            .Where(g => g != null)
                            .Distinct()
                            .ToList();
                        if(!dividerGroups.Any() || !dividerGroups.All(g => g.CanMerge)) continue;
                        mergableDividerGroups.Add(new MergableDividerGroups(divider, dividerGroups));
                    }

                    // If there aren't any mergable groups for these dividers, then there's nothing more we can do.
                    // Return false to indicate that the dividers couldn't be snapped.
                    if (!mergableDividerGroups.Any()) return false;

                    // Find the best group of groups.
                    MergableDividerGroups bestMergableGroups;
                    if (mergableDividerGroups.Any(m => m.MatchingSequenceGroup != null))
                    {
                        // If there are mergable groups with matching sequence groups, then we take the 
                        // mergable group with the best matching sequence group (i.e. the one with the 
                        // smallest deviation).
                        bestMergableGroups = mergableDividerGroups
                            .Where(m => m.MatchingSequenceGroup != null && m.SequenceGroupDeviation.HasValue)
                            .OrderBy(m => m.SequenceGroupDeviation.Value)
                            .First();
                    }
                    else
                    {
                        // If there are no mergable groups with matching sequence groups, then we
                        // take the mergable group with the least number of groups in (limiting the
                        // number of blocks that we end up dropping) and of those, the one with the 
                        // smallest overall size.
                        bestMergableGroups = mergableDividerGroups
                            .OrderBy(m => m.Groups.Count())
                            .ThenBy(m => m.Groups.Sum(g => g.TotalSpacePercentage))
                            .First();

                        // Now, this mergable group doesn't have a matching sequence group, which 
                        // means we need to create one containing all the products that were in the
                        // original sequence groups.
                        bestMergableGroups.CreateMatchingSequenceGroup();
                    }
                    PlanogramBlockingDivider dividerToRemove = bestMergableGroups.Divider;
                    Dividers.Remove(dividerToRemove);

                    // Find the remaining blocking group and change the colour to that of the matching
                    // sequence group, if one was found.
                    PlanogramBlockingGroup survivingGroup = bestMergableGroups.Groups.FirstOrDefault(g => !g.IsDeleted);
                    if(survivingGroup != null && bestMergableGroups.MatchingSequenceGroup != null) 
                    {
                        if (bestMergableGroups.SequenceGroupDeviation.HasValue)
                        {
                            // If there is a score, this suggests that we found a sequence group in 
                            // the sequence to use. As such, we change the name and colour of the blocking
                            // group to match.
                            survivingGroup.Colour = bestMergableGroups.MatchingSequenceGroup.Colour;
                            survivingGroup.Name = bestMergableGroups.MatchingSequenceGroup.Name;
                        }
                        else
                        {
                            // If there was no score, this indicates that we had to manually create
                            // the matching sequence group. We need to set the name and colour to match
                            // the surviving group, but we also need to ensure that the group colour is 
                            // unique.
                            Int32 uniqueColour = BlockingHelper.GetNextBlockingGroupColour(this, Parent?.Sequence);
                            bestMergableGroups.MatchingSequenceGroup.Colour = uniqueColour;
                            survivingGroup.Colour = uniqueColour;
                            bestMergableGroups.MatchingSequenceGroup.Name = survivingGroup.Name;
                        }
                    }
                        
                    // Finally, try to call this method again in an attempt to snap the remaining dividers again.
                    return SnapDividers(dividersToSnap.Except(new[] { dividerToRemove }), snappableBoundariesCount, context);
                }
                else
                {
                    if (!dividerToSnap.IsSnapped) continue;

                    // If root level, snap to fixture boundaries.
                    if (dividerToSnap.Level == PlanogramBlockingDivider.RootLevelNumber && context.FixtureBoundaries.Any())
                    {
                        // Check that the new X wouldn't position the dividerToSnap at the same location as
                        // another dividerToSnap.
                        Single delta = GetClosestValue(dividerToSnap.X, context.FixtureBoundaries) - dividerToSnap.X;
                        Boolean anySnappedVerticalDividersOccupyNewPosition = context.SnappedDividers
                            .Where(d => d.Type == PlanogramBlockingDividerType.Vertical)
                            .Any(d => d.X.EqualTo(dividerToSnap.X + delta));
                        if (!anySnappedVerticalDividersOccupyNewPosition)
                        {
                            MoveDivider(this, dividerToSnap, delta, context);
                            Debug.WriteLine(String.Format("Divider was snapped successfully to - X: {0}, Y: {1}", dividerToSnap.X, dividerToSnap.Y));
                        }
                    }
                    else
                    {
                        // Try to move dividerToSnap, but don't check for success, because if the dividerToSnap 
                        // wasn't snapped, we leave it where it was.
                        Boolean dividerCouldBeSnapped = TryMoveDividerToSnappableBoundaries(dividerToSnap, context);
                        if (dividerCouldBeSnapped)
                        {
                            Debug.WriteLine(String.Format("Divider was snapped successfully to - X: {0}, Y: {1}", dividerToSnap.X, dividerToSnap.Y));
                            continue;
                        }
                        Debug.WriteLine("Divider was not snapped successfully, but is vertical so ignoring.");
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Manually snaps the given dividers to the available boundaries.
        /// </summary>
        /// <param name="dividersToSnap">The dividers to manually snap.</param>
        /// <param name="snappedDividers"></param>
        /// <param name="flattenedBoundaries">The snappable boundaries that are available.</param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <param name="blockingOffset"></param>
        /// <returns></returns>
        private Boolean ForceDividersToSnap(
            IEnumerable<PlanogramBlockingDivider> dividersToSnap, 
            IEnumerable<Single> originalFlattenedBoundaries, 
            ApplyBlockingContext context)
        {
            Int32 dividersToSnapCount = dividersToSnap.Count();
            if (originalFlattenedBoundaries.Count() < dividersToSnapCount) return false;

            // Ensure that any dividers that were previously snapped are un-snapped.
            foreach (PlanogramBlockingDivider divider in dividersToSnap)
	        {
                if (context.SnappedDividers.Contains(divider)) context.SnappedDividers.Remove(divider);
	        }
            Debug.WriteLine("Forcing dividers to snap...");

            // Arrange the dividers to snap in ascending order - we'll do the same with boundaries
            // to make sure that they are snapped in turn.
            IEnumerable<PlanogramBlockingDivider> dividersInAscendingOrder = dividersToSnap
                .OrderBy(d => d.Type == PlanogramBlockingDividerType.Horizontal ? d.Y : d.X);

            Int32 dividerIndex = 0;
            Int32 boundariesIndex = 0;
            IEnumerable<Single> flattenedBoundaries = null;
            while(true)
            {
                // Break out of the loop if we have reached the end of the dividers.
                if (dividerIndex >= dividersToSnapCount) break;
                
                // Get the divider to snap and the boundaries that it covers. We re-get the boundaries
                // each time, because, as the dividers are snapped and move, more boundaries may become usable.
                PlanogramBlockingDivider dividerToSnap = dividersInAscendingOrder.ElementAt(dividerIndex);
                Single dividerToSnapPosition =
                    dividerToSnap.Type == PlanogramBlockingDividerType.Horizontal ?
                    dividerToSnap.Y :
                    dividerToSnap.X;
                flattenedBoundaries = 
                    flattenedBoundaries == null ?
                    flattenedBoundaries = originalFlattenedBoundaries :
                    flattenedBoundaries = GetFlattenedBoundaries(dividersToSnap, context, excludeSnappedDividers:false);

                // Order the dividers and boundaries in ascending order so that the "bottom" divider gets snapped
                // to the "bottom" boundary and so on, and then get the boundary we're supposed to be on at the moment.
                IEnumerable<Single> boundariesInAscendingOrder = flattenedBoundaries.OrderBy(b => b).ToList();
                if (boundariesIndex >= boundariesInAscendingOrder.Count()) break;
                Single boundary = boundariesInAscendingOrder.ElementAt(boundariesIndex);
                
                BeginSnappingEdit();

                Debug.WriteLine(String.Format("Forcing the divider originally at - {0}", context.OriginalDividerPositions[dividerToSnap]));

                MoveDivider(this, dividerToSnap, boundary - dividerToSnapPosition, context);

                if (IsDividerPositionValid(dividerToSnap, context))
                {
                    Debug.WriteLine(String.Format("Divider was successfully moved to - X: {0}, Y: {1}", dividerToSnap.X, dividerToSnap.Y));
                    ApplySnappingEdit();
                    dividerIndex++;
                }
                else
                {
                    Debug.WriteLine("Divider move was unsuccessful, cancelling and trying again");
                    CancelSnappingEdit();
                    context.SnappedDividers.Remove(dividerToSnap);
                }
                
                // Always move onto the "next" boundary at the end of each pass. This allows us to skip boundaries.
                boundariesIndex++;
            }

            return dividerIndex >= dividersToSnapCount;
        }

        /// <summary>
        /// Moves the given divider by the amount specified, adjusting the surrounding dividers
        /// so that they still retain their original relative positions.
        /// </summary>
        /// <param name="planogramBlocking">The blocking upon which the divider exists.</param>
        /// <param name="divider">The divider to move.</param>
        /// <param name="snappedDividers">A list of currently snapped dividers.</param>
        /// <param name="delta">The the amount by which the divider should be moved.</param>
        /// <remarks>
        /// This method moves the given divider whilst attempting to retain the aesthetics of the
        /// original divider layout.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// Thrown if the given divider does not appear in the Dividers list on the given planogramBlocking.
        /// </exception>
        internal static void MoveDivider(
            PlanogramBlocking planogramBlocking,
            PlanogramBlockingDivider divider,
            Single delta,
            ApplyBlockingContext context)
        {
            if (!planogramBlocking.Dividers.Contains(divider)) throw new ArgumentException("planogramBlocking.Dividers must contain divider.");

            var dividerIsHorizontal = divider.Type == PlanogramBlockingDividerType.Horizontal;
            Single newPosition = dividerIsHorizontal ? divider.Y + delta : divider.X + delta;
            Single oldPosition = dividerIsHorizontal ? divider.Y : divider.X;

            if (!Constants.StretchLocationsWhenMovingDividers)
            {
                divider.MoveTo(newPosition);
                return;
            }

            // Get the dividers that lie at each end of the divider.
            PlanogramBlockingDivider startDivider;
            PlanogramBlockingDivider endDivider;
            if (dividerIsHorizontal)
            {
                startDivider = planogramBlocking.Dividers.FirstOrDefault(d =>
                    d.Type == PlanogramBlockingDividerType.Vertical &&
                    d.X.EqualTo(divider.X, Precision) && 
                    d.Y.LessOrEqualThan(divider.Y, Precision) && 
                    (d.Y + d.Length).GreaterOrEqualThan(d.Y, Precision));
                endDivider = planogramBlocking.Dividers.FirstOrDefault(d =>
                    d.Type == PlanogramBlockingDividerType.Vertical &&
                    d.X.EqualTo(divider.X + divider.Length, Precision) &&
                    d.Y.LessOrEqualThan(divider.Y, Precision) &&
                    (d.Y + d.Length).GreaterOrEqualThan(d.Y, Precision));
            }
            else
            {
                startDivider = planogramBlocking.Dividers.FirstOrDefault(d =>
                    d.Type == PlanogramBlockingDividerType.Horizontal &&
                    d.Y.EqualTo(divider.Y, Precision) &&
                    d.X.LessOrEqualThan(divider.X, Precision) &&
                    (d.X + d.Length).GreaterOrEqualThan(d.X, Precision));
                endDivider = planogramBlocking.Dividers.FirstOrDefault(d =>
                    d.Type == PlanogramBlockingDividerType.Horizontal &&
                    d.Y.EqualTo(divider.Y + divider.Length, Precision) &&
                    d.X.LessOrEqualThan(divider.X, Precision) &&
                    (d.X + d.Length).GreaterOrEqualThan(d.X, Precision));
            }

            // Select which divider to use for the search area (the shortest one).
            // The search area is the area in which any dividers of the same orientation will need to be moved
            // as a result of moving this divider.
            PlanogramBlockingDivider parentAreaDivider;
            if (endDivider == null && startDivider == null) parentAreaDivider = null;
            else if (endDivider == null) parentAreaDivider = startDivider;
            else if (startDivider == null) parentAreaDivider = endDivider;
            else parentAreaDivider = startDivider.Length.GreaterOrEqualThan(endDivider.Length) ? startDivider : endDivider;

            // Determine the co-ordinates of the search area given the parent area divider to use.
            var x1 = dividerIsHorizontal ? (divider.X) : (parentAreaDivider == null ? 0f : parentAreaDivider.X);
            var x2 = dividerIsHorizontal ? (divider.X + divider.Length) : (parentAreaDivider == null ? 1f : parentAreaDivider.X + parentAreaDivider.Length);
            var y1 = dividerIsHorizontal ? (parentAreaDivider == null ? 0f : parentAreaDivider.Y) : (divider.Y);
            var y2 = dividerIsHorizontal ? (parentAreaDivider == null ? 1f : parentAreaDivider.Y + parentAreaDivider.Length) : (divider.Y + divider.Length);

            // Get dividers of the same type that lie within the search area. Filter out dividers
            // of a different type, this divider.
            var dividersAffectedByMove = planogramBlocking.Dividers.
                Where(d => d.Type == divider.Type && d != divider).
                Where(d =>
                    dividerIsHorizontal ?
                    (d.X.GreaterOrEqualThan(x1, Precision) && (d.X + d.Length).LessOrEqualThan(x2, Precision) && d.Y.GreaterOrEqualThan(y1, Precision) && d.Y.LessOrEqualThan(y2, Precision)) :
                    (d.Y.GreaterOrEqualThan(y1, Precision) && (d.Y + d.Length).LessOrEqualThan(y2, Precision) && d.X.GreaterOrEqualThan(x1, Precision) && d.X.LessOrEqualThan(x2, Precision))
                    ).
                Distinct().
                ToList();

            // Filter out dividers that have already been snapped and any dividers on the other side
            // of these dividers.
            foreach (var snappedDivider in context.SnappedDividers.Where(d => dividersAffectedByMove.Contains(d)).ToList())
            {
                var snappedDividerPos = dividerIsHorizontal ? snappedDivider.Y : snappedDivider.X;
                dividersAffectedByMove.
                    Where(d =>
                    {
                        var dPos = d.Type == PlanogramBlockingDividerType.Horizontal ? d.Y : d.X;
                        return
                            snappedDividerPos.GreaterThan(oldPosition, Precision) ?
                            dPos.GreaterThan(snappedDividerPos, Precision) :
                            dPos.LessThan(snappedDividerPos, Precision);
                    }).
                    ToList().
                    ForEach(d => dividersAffectedByMove.Remove(d));
                dividersAffectedByMove.Remove(snappedDivider);
            }

            // Create dictionary of dividers against their proportional positions with
            // respect to the length and position of the parent area divider.
            var parentDividerStart = parentAreaDivider == null ? 0f : (dividerIsHorizontal ? parentAreaDivider.Y : parentAreaDivider.X);
            var parentDividerEnd = parentAreaDivider == null ? 1f : parentAreaDivider.Length + parentDividerStart;
            var affectedDividerPositions = new Dictionary<PlanogramBlockingDivider, Single>();
            foreach (var affectedDivider in dividersAffectedByMove)
            {
                Single proportionalPosition = 0;
                if (dividerIsHorizontal)
                {
                    if (affectedDivider.Y.GreaterThan(divider.Y, Precision))
                    {
                        if (!(parentDividerEnd - divider.Y).EqualTo(0, Precision))
                            proportionalPosition = (affectedDivider.Y - divider.Y) / (parentDividerEnd - divider.Y);
                    }
                    else
                    {
                        if (!(divider.Y - parentDividerStart).EqualTo(0, Precision))
                            proportionalPosition = (affectedDivider.Y - parentDividerStart) / (divider.Y - parentDividerStart);
                    }
                }
                else
                {
                    if (affectedDivider.X.GreaterThan(divider.X, Precision))
                    {
                        if (!(parentDividerEnd - divider.X).EqualTo(0, Precision))
                            proportionalPosition = (affectedDivider.X - divider.X) / (parentDividerEnd - divider.X);

                    }
                    else
                    {
                        if (!(divider.X - parentDividerStart).EqualTo(0, Precision))
                            proportionalPosition = (affectedDivider.X - parentDividerStart) / (divider.X - parentDividerStart);
                    }
                }
                affectedDividerPositions.Add(affectedDivider, proportionalPosition);
            }

            // Create list of dividers to move and order dependant on which way the divider is going to move.
            var dividersToMove = dividersAffectedByMove.Union(new PlanogramBlockingDivider[] { divider });
            dividersToMove =
                delta > 0 ?
                dividersToMove.OrderByDescending(d => dividerIsHorizontal ? d.Y : d.X) :
                dividersToMove.OrderBy(d => dividerIsHorizontal ? d.Y : d.X);

            // Keep track of divider that have been snapped, so that they aren't adjusted here.
            if (!context.SnappedDividers.Contains(divider)) context.SnappedDividers.Add(divider);

            // Move dividers.
            foreach (var dividerToMove in dividersToMove)
            {
                if (dividerToMove == divider)
                {
                    divider.MoveTo(newPosition);
                }
                else
                {
                    var dividerToMovePosition = dividerIsHorizontal ? dividerToMove.Y : dividerToMove.X;
                    var affectedDividerNewPosition =
                        dividerToMovePosition.GreaterThan(oldPosition, Precision) ?
                        newPosition + (affectedDividerPositions[dividerToMove] * (parentDividerEnd - newPosition)) :
                        parentDividerStart + (affectedDividerPositions[dividerToMove] * (newPosition - parentDividerStart));
                    dividerToMove.MoveTo(affectedDividerNewPosition);
                }
            }
        }

        /// <summary>
        /// Attempts to move the divider to the nearest boundary in those provided, move down through the precendence order.
        /// </summary>
        /// <param name="planogramBlocking">The blocking upon which the divider exists.</param>
        /// <param name="divider">The divider to snap.</param>
        /// <param name="snappedDividers">A list of currently snapped dividers.</param>
        /// <returns>True if the divider was moved. False if it wasn't.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if the given divider does not appear in the Dividers list on the given planogramBlocking.
        /// </exception>
        private Boolean TryMoveDividerToSnappableBoundaries(PlanogramBlockingDivider divider, ApplyBlockingContext context)
        {
            if (!Dividers.Contains(divider)) Debug.Fail("planogramBlocking.Dividers must contain divider.");
            PlanogramBlockingDividerSnappingPositions snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(
                    divider, context.BlockingWidth, context.BlockingHeight, context.BlockingHeightOffset, context.SnappedDividers);
            List<Single> availableBoundaries = snappableBoundaries.GetFirstList();
            
            // Prepare a list of possible deltas. These will be deltas which we could snap to, but they're not our 
            // first choice.
            List<Single> possibleDeltas = new List<Single>();

            while (true)
            {
                // If we've run out of possible boundaries to try, then break and delete divider.
                if (availableBoundaries.Count == 0)
                {
                    // V8-27584
                    // If IsSnapped and no available boundaries, then this means that there were either no edges,
                    // or none of the edges could be snapped to. At this point, because IsSnapped == true, just
                    // return without trying sub-component dividers or peg holes.
                    if (divider.Type == PlanogramBlockingDividerType.Vertical && divider.IsSnapped)
                    {
                        return false;
                    }

                    // Get the next list of snappable boundaries.
                    availableBoundaries = snappableBoundaries.GetNextList();
                    if (availableBoundaries == null)
                    {
                        // At this stage, we've run out of boundaries to try. However, we may have noted some 
                        // possible boundaries along the way. If we have, then we'll snap to the closest one of these.
                        // If not, then there's nothing more we can do.
                        if (possibleDeltas.Any())
                        {
                            Single minDelta = possibleDeltas.Min();
                            MoveDivider(this, divider, minDelta, context);
                            ApplySnappingEdit();

                            // We know that this is a plausible move, because the delta was flagged as a possible during 
                            // the pass through the closest boundaries. It may not be optimal, because lower levels might
                            // be left without anywhere to snap, but its still a valid move.
                            return true;
                        }

                        // We get here if there were no possible boundaries picked up, or if there
                        // was a problem using one.
                        if (snappableBoundaries.CanIgnoreSnappingPositions)
                        {
                            context.SnappedDividers.Add(divider);
                            return true;
                        }
                        return false;
                    }
                    if (availableBoundaries.Count == 0) continue;
                }

                // Find closest boundary and move divider.
                Single oldPosition = divider.Type == PlanogramBlockingDividerType.Horizontal ? divider.Y : divider.X;
                Single boundary = GetClosestValue(oldPosition, availableBoundaries);
                Single delta = boundary - oldPosition;
                BeginSnappingEdit();
                MoveDivider(this, divider, delta, context);

                Boolean locationsContainMerchComp = DividerLocationsContainMerchComponents(divider, context);
                Boolean distanceToNeighboursMoreThanMin = divider.GetMinDistanceToNeighbour().GreaterThan(Constants.MinDistanceToNeighbour, 2);
                Boolean groupsHaveEnoughBoundaries = DoGroupsHaveEnoughBoundaries(divider, context);

                if (locationsContainMerchComp && distanceToNeighboursMoreThanMin && groupsHaveEnoughBoundaries)
                {
                    ApplySnappingEdit();
                    return true;
                }

                // Provided the locations contained merchandisable components and the distance to the neighbouring dividers
                // was greater than the min, we might still be able to snap to this boundary, but only as a last resort.
                if (locationsContainMerchComp && distanceToNeighboursMoreThanMin)
                {
                    possibleDeltas.Add(delta);
                }

                // Un-do the move, remove the boundary position and loop to try again.
                CancelSnappingEdit();
                context.SnappedDividers.Remove(divider);
                availableBoundaries.Remove(boundary);
            }
        }

        private Boolean IsDividerPositionValid(PlanogramBlockingDivider divider, ApplyBlockingContext context)
        {
            // If divider is last one before top or bottom, ensure that location between
            // divider and edge contains merchandisable components.
            Boolean locationsContainMerchComp = DividerLocationsContainMerchComponents(divider, context);

            // Check that we haven't moved too close to another divider
            Boolean distanceToNeighboursMoreThanMin =
                divider.GetMinDistanceToNeighbour().GreaterThan(Constants.MinDistanceToNeighbour, 2);

            Boolean groupsHaveEnoughBoundaries = DoGroupsHaveEnoughBoundaries(divider, context);

            return locationsContainMerchComp && distanceToNeighboursMoreThanMin && groupsHaveEnoughBoundaries;
        }

        /// <summary>
        /// Checks that all the divider groupings (the bands of dividers grouped by level) of a level lower than
        /// <paramref name="divider"/> have enough boundaries to allow snapping.
        /// </summary>
        /// <param name="divider"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private static Boolean DoGroupsHaveEnoughBoundaries(PlanogramBlockingDivider divider, ApplyBlockingContext context)
        {
            foreach (KeyValuePair<Byte, IEnumerable<IEnumerable<PlanogramBlockingDivider>>> dividerGrouping in context.DividerGroupings)
            {
                Byte level = dividerGrouping.Key;
                IEnumerable<IEnumerable<PlanogramBlockingDivider>> dividerGroups = dividerGrouping.Value;

                // We're only interested in check levels below the divider we're validating.
                if (level <= divider.Level) continue;

                foreach (IEnumerable<PlanogramBlockingDivider> dividerGroup in dividerGroups)
                {
                    // We're only interested in checking horizontal groups.
                    if (!dividerGroup.Any() || dividerGroup.First().Type != PlanogramBlockingDividerType.Horizontal) continue;

                    IEnumerable<Single> availableBoundaries = GetFlattenedBoundaries(dividerGroup, context, removeBoundaries: false);
                    
                    Int32 boundaryCheckCount = 0;
                    Int32 groupCheckCount = dividerGroup.Where(d => !context.SnappedDividers.Contains(d)).Count();

                    // Remove any boundaries that aren't possible.
                    if (divider.GetOverlap(dividerGroup.First()).GreaterThan(0))
                    {
                        if (dividerGroup.First().Y.LessThan(divider.Y))
                        {
                            boundaryCheckCount = availableBoundaries.Where(b => b.LessThan(divider.Y)).Count();
                        }
                        else
                        {
                            boundaryCheckCount = availableBoundaries.Where(b => b.GreaterThan(divider.Y)).Count();

                            //When checking up assume we have 1 more divider position as the divider
                            //being moved will have as merch space for the first blocking location
                            boundaryCheckCount++;
                        }
                    }
                    else
                    {
                        boundaryCheckCount = availableBoundaries.Count();                        
                    }

                    if (boundaryCheckCount <= groupCheckCount)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static IEnumerable<PlanogramBlockingDivider> OrderDividersToSnap(
                    IEnumerable<PlanogramBlockingDivider> dividerGroup, Int32 boundariesCount)
        {
            IEnumerable<PlanogramBlockingDivider> dividersToSnap;
            if (boundariesCount > dividerGroup.Count())
            {
                // If more boundaries than dividers, arrange by smallest block first. This ensures
                // that the largest blocks are snapped last since they probably cover the largest
                // number of snappable locations.
                dividersToSnap = dividerGroup
                    .Where(d => !d.IsDeleted)
                    .OrderBy(d => d.GetMaxDistanceToNeighbour())
                    .ThenBy(d => d.GetMinDistanceToNeighbour())
                    .ThenBy(d => d.Type == PlanogramBlockingDividerType.Horizontal ? d.Y : d.X);
            }
            else
            {
                // If fewer boundaries than dividers, arrange by largest block first. This ensures that 
                // the smallest blocks are snapped last, which means that the smallest blocks are
                // more likely to be merged than the large blocks.
                dividersToSnap = dividerGroup
                    .Where(d => !d.IsDeleted)
                    .OrderByDescending(d => d.GetMaxDistanceToNeighbour())
                    .ThenByDescending(d => d.GetMinDistanceToNeighbour())
                    .ThenBy(d => d.Type == PlanogramBlockingDividerType.Horizontal ? d.Y : d.X);
            }
            return dividersToSnap;
        }

        /// <summary>
        /// Gets the locations which are situated between the two given dividers.
        /// </summary>
        /// <param name="fromDivider">The starting divider.</param>
        /// <param name="toDivider">The ending divider.</param>
        /// <returns>The locations between the two dividers.</returns>
        public IEnumerable<PlanogramBlockingLocation> GetLocationsBetweenDividers(
            PlanogramBlockingDivider fromDivider,
            PlanogramBlockingDivider toDivider)
        {
            // If both dividers are null, just return all the locations.
            if (fromDivider == null && toDivider == null) return Locations;
            
            // The dividers must be of the same type.
            if (fromDivider != null && toDivider != null && fromDivider.Type != toDivider.Type)
            {
                System.Diagnostics.Debug.Fail("fromDivider and toDivider must be of the same type");
                return new List<PlanogramBlockingLocation>();
            }

            // The dividers must overlap.
            if (fromDivider != null && fromDivider.GetOverlap(toDivider).EqualTo(0) ||
                toDivider != null && toDivider.GetOverlap(fromDivider).EqualTo(0))
            {
                System.Diagnostics.Debug.Fail("fromDivider and toDivider must overlap");
                return new List<PlanogramBlockingLocation>();
            }

            // One divider must be within the other
            Single fromDividerOverlap = fromDivider == null ? 0f : fromDivider.GetOverlap(toDivider);
            Single toDividerOverlap = toDivider == null ? 0f : toDivider.GetOverlap(fromDivider);
            if(!fromDividerOverlap.EqualTo(1) && !toDividerOverlap.EqualTo(1))
            {
                System.Diagnostics.Debug.Fail("one divider must be entirely within the other");
                return new List<PlanogramBlockingLocation>();
            }

            // In order to work out which locations lie between the two dividers, we need to look inside
            // the search area described by the extrusion of the small divider to the large divider. Once 
            // we have this area, we can check the co-ordinates and size of every location in the blocking
            // to see if it fits wholly inside (we're not interested in any locations that only partially
            // fit inside).

            // Identify the small and large divider.
            PlanogramBlockingDivider smallDivider; 
            PlanogramBlockingDivider largeDivider; 
            Boolean dividersAreHorizontal;
            if(fromDivider == null)
            {
                smallDivider = toDividerOverlap.EqualTo(1) ? toDivider : fromDivider;
                largeDivider = toDividerOverlap.EqualTo(1) ? fromDivider : toDivider;
                dividersAreHorizontal = toDivider.Type == PlanogramBlockingDividerType.Horizontal;
            }
            else
            {
                smallDivider = fromDividerOverlap.EqualTo(1) ? fromDivider : toDivider;
                largeDivider = fromDividerOverlap.EqualTo(1) ? toDivider : fromDivider;
                dividersAreHorizontal = fromDivider.Type == PlanogramBlockingDividerType.Horizontal;
            }

            // At this point, smallDivider should not be null. If it is, something has gone wrong.
            if (smallDivider == null)
            {
                System.Diagnostics.Debug.Fail("the smallest divider out of the arguments given was null");
                return new List<PlanogramBlockingLocation>();
            }

            // The small divider should always be non-null, but if the large divider is null
            // we need to identify its location. This can be inferred from whether the from or 
            // to divider was null.
            RectValue largeDividerBounds;
            if (largeDivider == null)
            {
                if (fromDivider == null)
                {
                    largeDividerBounds = new RectValue(
                        0f, // start at x = 0
                        0f, // start at y = 0
                        0f, // ignore z
                        dividersAreHorizontal ? 1f : 0f, // entire width if horizontal
                        dividersAreHorizontal ? 0f : 1f, // entire height if vertical
                        0f); // ignore z
                }
                else // toDivider must have been null.
                {
                    largeDividerBounds = new RectValue(
                        dividersAreHorizontal ? 0f : 1f, // start at x=0 if horizontal, or 1 if vertical
                        dividersAreHorizontal ? 1f : 0f, // start at y = 1 if horizontal, or 1 if vertical
                        0f, // ignore z
                        dividersAreHorizontal ? 1f : 0f, // entire width if horizontal
                        dividersAreHorizontal ? 0f : 1f, // entire height if vertical
                        0f); // ignore z
                }
            }
            else
            {
                largeDividerBounds = new RectValue(
                    largeDivider.X,
                    largeDivider.Y,
                    0f,
                    dividersAreHorizontal ? largeDivider.Length : 0f,
                    dividersAreHorizontal ? 0f : largeDivider.Length,
                    0f);
            }

            // Now, we can identify the coordinates of the search area based on the known co-ords
            // of the small divider and what we've worked out for the large divider.
            RectValue searchArea;
            if(dividersAreHorizontal)
            {
                searchArea = new RectValue(
                    smallDivider.X, // x1 is the small divider x.
                    Math.Min(smallDivider.Y, largeDividerBounds.Y), // y1 is the minimum y value.
                    0f, // ignore z.
                    smallDivider.Length, // the width is the width of the small divider.
                    Math.Abs(smallDivider.Y - largeDividerBounds.Y), // the height is the difference between the divider y values.
                    0f); // ignore z.
            }
            else
            {
                searchArea = new RectValue(
                    Math.Min(smallDivider.X, largeDividerBounds.X), // x1 is the min x co-ordinate of the two dividers.
                    smallDivider.Y, // y1 is the y co-ord of the small divider.
                    0f, // ignore z.
                    Math.Abs(smallDivider.X - largeDividerBounds.X), // width is the difference between the divider x co-ordinates.
                    smallDivider.Length, // height is the length of the small divider.
                    0f); // ignore z.
            }

            // Return the locations that intersect fully with the search area.
            List<PlanogramBlockingLocation> returnLocations = new List<PlanogramBlockingLocation>();
            foreach(PlanogramBlockingLocation l in Locations)
            {
                RectValue? intersect = l.ToRectValue().Intersect(searchArea);
                if (!intersect.HasValue) continue;
                // Check that the intersection matches the location. If it does, then
                // it falls within the search area. We only check to 2dp, because at this
                // level of accuracy, the location definitely falls within the search area.
                if (intersect.Value.Equals(l.ToRectValue(), 2)) returnLocations.Add(l);
            }
            return returnLocations;
        }

        private static IEnumerable<Single> GetFlattenedBoundaries(
            IEnumerable<PlanogramBlockingDivider> dividerGroup, 
            ApplyBlockingContext context, 
            Boolean removeBoundaries = true,
            Boolean excludeSnappedDividers = true)
        {
            return dividerGroup
                .SelectMany(d => 
                    new PlanogramBlockingDividerSnappingPositions(
                        d, 
                        context.BlockingWidth, 
                        context.BlockingHeight, 
                        context.BlockingHeightOffset, 
                        excludeSnappedDividers ? context.SnappedDividers : new HashSet<PlanogramBlockingDivider>(), 
                        removeBoundaries)
                            .GetFlattenedBoundaries())
                .Distinct();
        }

        /// <summary>
        /// Returns the closest value from the list of specified values
        /// </summary>
        internal static Single GetClosestValue(Single value, IEnumerable<Single> possibleValues)
        {
            var dictionary = new Dictionary<Single, Single>(); // Distance, value
            foreach (var possibleValue in possibleValues)
            {
                dictionary.Add(value - possibleValue, possibleValue);
            }
            var minKey = dictionary.Keys.Select(v => Math.Abs(v)).Min();
            var actualKey = dictionary.Keys.First(k => Math.Abs(k).EqualTo(minKey));
            return dictionary[actualKey];
        }

        /// <summary>
        /// Begins a snapping edit by storing a snapshot of the state of the dividers.
        /// </summary>
        private void BeginSnappingEdit()
        {
            if (_snappingEditStates == null) _snappingEditStates = new Stack<DividerStates>();

            _snappingEditStates.Push(new DividerStates(this));
        }

        /// <summary>
        /// "Applies" the edit, by not committing the most recent snapshot.
        /// </summary>
        private void ApplySnappingEdit()
        {
            if (_snappingEditStates == null || !_snappingEditStates.Any())
            {
                Debug.Fail("BeginSnappingEdit should be called before ApplySnappingEdit");
                return;
            }

            // Don't do anything, because the last state is the state when we called Begin.
            // We want to apply the changes since then, so we apply by not committing the last state.
            // This is correct, because we don't clone dividers when we begin an edit, we just
            // store a snapshot of their states.
        }

        /// <summary>
        /// Cancels the most recent edit, by committing its state back to the dividers.
        /// </summary>
        private void CancelSnappingEdit()
        {
            if (_snappingEditStates == null || !_snappingEditStates.Any())
            {
                Debug.Fail("BeginSnappingEdit should be called before CancelSnappingEdit");
                return;
            }

            DividerStates state = _snappingEditStates.Pop();

            state.Commit();
        }
        #endregion

        #region Apply Performance Data
        /// <summary>
        /// Applies Performance Data to the given Blocking in order to re-scale the blocks according
        /// to performance.
        /// </summary>
        /// <param name="performanceRatio">
        /// The weighting that should be given to the performance data of products vs. the score for their size and inventory.
        /// The size/inventory will be given a weighting of 1 - <paramref name="performanceRatio"/>.
        /// </param>
        /// <exception cref="GetBlockingException">Thrown if any of the Blocking Groups don't have Products associated with them.</exception>
        public void ApplyPerformanceData(IPlanogramMetricProfile metricProfile, Single performanceRatio = 1f)
        {
            ApplyBlockingContext context = new ApplyBlockingContext(this);

            // Calculate Psis for all products, normalised against the total of all the performance values.
            IEnumerable<PlanogramProduct> products = this.Groups.SelectMany(g => g.EnumerateRangedPlanogramProducts()).Distinct().ToList();
            PlanogramPerformanceDataTotals performanceDataTotals = new PlanogramPerformanceDataTotals(this.Parent.Performance.PerformanceData);
            Dictionary<PlanogramProduct, Double> normalisedProductPsis = GetNormalisedPsis(metricProfile, products, performanceDataTotals);
            Dictionary<PlanogramProduct, Double> normalisedProductInventoryScores = GetNormalisedInventoryScores(products);
            Dictionary<PlanogramProduct, Double> normalisedProductScores =
                GetCombinedProductScores(normalisedProductPsis, normalisedProductInventoryScores, performanceRatio);

            Dictionary<PlanogramBlockingGroup, Double> scoresByGroup = GetScoresByGroup(normalisedProductScores);

            // Get the dividers that cannot be optimised.
            HashSet<PlanogramBlockingDivider> lockedDividers = new HashSet<PlanogramBlockingDivider>(Groups
                .Where(g => g.IsLimited && g.LimitedPercentage.EqualTo(0))
                .SelectMany(g => g.GetBlockingLocations())
                .SelectMany(l => l.GetPlanogramBlockingDividers())
                .Union(Dividers.Where(d => d.IsLimited && d.LimitedPercentage.EqualTo(0)))
                .Distinct());

            // Ensure that these locked dividers are not moved by adding them to the
            // snapped dividers collection.
            foreach(PlanogramBlockingDivider divider in lockedDividers)
            {
                context.SnappedDividers.Add(divider);
            }

            // Store the original coordinates of the dividers.
            Dictionary<PlanogramBlockingDivider, Single> originalCoordinatesByDivider = Dividers
                .ToDictionary(d => d, d => d.GetNormalCoordinate());

            // Try to apply performance data to dividers. If there are constraints that
            // have not been met, then lock those dividers to their constraints and try again.
            IEnumerable<DividerConstraint> constrainedDividers = null;
            while (constrainedDividers == null || constrainedDividers.Any())
            {
                constrainedDividers = TryApplyingPerformanceData(
                    metricProfile,
                    context,
                    scoresByGroup,
                    lockedDividers,
                    originalCoordinatesByDivider);

                // Re-set the snapped dividers collection to only contain the dividers
                // that were originally locked.
                context.SnappedDividers.Clear();
                foreach (PlanogramBlockingDivider divider in lockedDividers)
                {
                    context.SnappedDividers.Add(divider);
                }

                // Now add any dividers that need to be constrained to the locked dividers collection
                foreach (DividerConstraint constrainedDivider in constrainedDividers)
                {
                    MoveDivider(this, constrainedDivider.Divider, constrainedDivider.GetDelta(), context);

                    // Add the divider to the locked dividers collection so that it is locked to its
                    // current position on the next pass.
                    if (!lockedDividers.Contains(constrainedDivider.Divider))
                    {
                        lockedDividers.Add(constrainedDivider.Divider);
                    }

                    // Also add the divider to the snapped dividers collection, so that it isn't
                    // moved in future.
                    if(!context.SnappedDividers.Contains(constrainedDivider.Divider))
                    {
                        context.SnappedDividers.Add(constrainedDivider.Divider);
                    }
                }

                //If all of the dividers have been constrained with no room for manouvre then
                // break out otherwise we get stuck in an infinate loop.
                if (constrainedDividers.All(c => c.Divider.LimitedPercentage.EqualTo(0))) break;

            }
        }

        /// <summary>
        /// Aggregates the product scores for each <see cref="PlanogramBlockingGroup"/> in <see cref="Groups"/>
        /// and returns them keyed by group.
        /// </summary>
        /// <param name="normalisedProductScores"></param>
        /// <returns></returns>
        private Dictionary<PlanogramBlockingGroup, Double> GetScoresByGroup(
            Dictionary<PlanogramProduct, Double> normalisedProductScores)
        {
            // Sum the product scores for each of the groups to give a performance score
            // to each of the groups.
            Dictionary<PlanogramBlockingGroup, Double> scoresByGroup = new Dictionary<PlanogramBlockingGroup, Double>();
            foreach(PlanogramBlockingGroup blockingGroup in Groups)
            {
                Double groupScore = blockingGroup.EnumerateRangedPlanogramProducts().Sum(p =>
                {
                    Double productScore;
                    if(normalisedProductScores.TryGetValue(p, out productScore))
                    {
                        return productScore;
                    }
                    return 0D;
                });

                scoresByGroup.Add(blockingGroup, groupScore);
            }

            // Now, check the scores for each group and manually adjust them if they do not comply
            // with the block's limited percentage.
            foreach(PlanogramBlockingGroup blockingGroup in Groups.Where(g => g.IsLimited))
            {
                Double totalScore = scoresByGroup.Values.Sum();
                if (totalScore.EqualTo(0)) continue;
                Double groupScore;
                if (!scoresByGroup.TryGetValue(blockingGroup, out groupScore)) continue;

                Double performancePercentage = groupScore / totalScore;
                if (performancePercentage.GreaterThan(blockingGroup.TotalSpacePercentage + blockingGroup.LimitedPercentage))
                {
                    scoresByGroup[blockingGroup] = AdjustScoreForPercentage(
                        totalScore, groupScore, blockingGroup.TotalSpacePercentage + blockingGroup.LimitedPercentage);
                }
                if (performancePercentage.LessThan(blockingGroup.TotalSpacePercentage - blockingGroup.LimitedPercentage))
                {
                    scoresByGroup[blockingGroup] = AdjustScoreForPercentage(
                        totalScore, groupScore, blockingGroup.TotalSpacePercentage - blockingGroup.LimitedPercentage);
                }
                else
                {
                    continue;
                }
            }

            return scoresByGroup;
        }

        /// <summary>
        /// Adjusts the current <paramref name="groupScore"/> to meet the <paramref name="newPercentage"/> of the
        /// score total.
        /// </summary>
        /// <param name="totalScore">The current total score, of which <paramref name="groupScore"/> is a portion.</param>
        /// <param name="groupScore">The current, aggregated product score for the group.</param>
        /// <param name="newPercentage">The new percentage that the returned score should represent in the new total.</param>
        /// <returns>The new score for the group such that it satisfies the <paramref name="newPercentage"/>.</returns>
        private static Double AdjustScoreForPercentage(Double totalScore, Double groupScore, Double newPercentage)
        {
            return (newPercentage * (totalScore - groupScore)) / (1 - newPercentage);
        }

        /// <summary>
        /// Applies performance scores to the blocking, returning a collection of constraints against
        /// dividers that have been broken.
        /// </summary>
        /// <param name="metricProfile"></param>
        /// <param name="context"></param>
        /// <param name="scoresByGroup"></param>
        /// <param name="lockedDividers"></param>
        /// <param name="originalCoordinatesByDivider"></param>
        /// <returns></returns>
        private IEnumerable<DividerConstraint> TryApplyingPerformanceData(
            IPlanogramMetricProfile metricProfile, 
            ApplyBlockingContext context, 
            Dictionary<PlanogramBlockingGroup, Double> scoresByGroup, 
            IEnumerable<PlanogramBlockingDivider> lockedDividers,
            Dictionary<PlanogramBlockingDivider, Single> originalCoordinatesByDivider)
        {
            Byte level = PlanogramBlockingDivider.RootLevelNumber;

            // Calculate new positions and move dividers.
            while (true)
            {
                IEnumerable<PlanogramBlockingDivider> levelDividers = this.Dividers.Where(d => d.Level == level);
                if (!levelDividers.Any()) break;

                // Group dividers by adjacent dividers, i.e. in bands.
                IEnumerable<IEnumerable<PlanogramBlockingDivider>> dividerGroups;
                if (!context.DividerGroupings.TryGetValue(level, out dividerGroups)) continue;
                foreach (PerformanceCalculationDividers performanceCalculationDividers in
                    dividerGroups.SelectMany(g => 
                        PerformanceCalculationDividers.GetPerformanceCalculationDividers(g, lockedDividers)))
                {
                    // Calculate normalised positions within the available bounds.
                    BlockingDividerSpaceCalculation dividerCalculations = new BlockingDividerSpaceCalculation(
                        performanceCalculationDividers.Dividers,
                        performanceCalculationDividers.StartingBound,
                        performanceCalculationDividers.EndingBound);

                    IEnumerable<PlanogramBlockingDividerSpaceCalculationResult> calculationResults =
                        dividerCalculations.CalculateNormalisedPositions(metricProfile, scoresByGroup);

                    // Re-position the dividers based on the deltas calculated.
                    foreach (PlanogramBlockingDividerSpaceCalculationResult result in calculationResults)
                    {
                        MoveDivider(this, result.Divider, result.Delta, context);
                    }
                }

                level++;
            }

            return DividerConstraint.GetDividerConstraints(this, originalCoordinatesByDivider);
        }

        /// <summary>
        /// Gets normalised scores for the <paramref name="products"/> based on their dimensions and assortment inventory.
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        private Dictionary<PlanogramProduct, Double> GetNormalisedInventoryScores(IEnumerable<PlanogramProduct> products)
        {
            Dictionary<PlanogramProduct, Double> unnormalisedInventoryScores = GetUnNormalisedInventoryScores(products);

            Dictionary<PlanogramProduct, Double> normalisedInventoryScores = new Dictionary<PlanogramProduct, Double>();
            Double totalInventoryScore = unnormalisedInventoryScores.Values.Sum();
            if (totalInventoryScore.EqualTo(0)) return normalisedInventoryScores;
            foreach (KeyValuePair<PlanogramProduct, Double> unnormalisedInventoryScore in unnormalisedInventoryScores)
            {
                normalisedInventoryScores.Add(unnormalisedInventoryScore.Key, unnormalisedInventoryScore.Value / totalInventoryScore);
            }
            return normalisedInventoryScores;
        }

        /// <summary>
        /// Gets un-normalised scores for products based on their size and inventory by multiplying their volume by their 
        /// assortment units.
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        private Dictionary<PlanogramProduct, Double> GetUnNormalisedInventoryScores(IEnumerable<PlanogramProduct> products)
        {
            Dictionary<PlanogramProduct, Double> unnormalisedInventoryScores = new Dictionary<PlanogramProduct, Double>();
            Dictionary<String, PlanogramAssortmentProduct> assortmentProductsByGtin = Parent.Assortment.Products.ToDictionary(p => p.Gtin);
            foreach (PlanogramProduct product in products)
            {
                PlanogramAssortmentProduct assortmentProduct;
                if (!assortmentProductsByGtin.TryGetValue(product.Gtin, out assortmentProduct)) continue;
                unnormalisedInventoryScores.Add(
                    product,
                    product.Width * product.Height * product.Depth * assortmentProduct.Units);
            }
            return unnormalisedInventoryScores;
        }

        /// <summary>
        /// Combines the <paramref name="normalisedProductPsis"/> with the <paramref name="normalisedProductInventoryScores"/>
        /// using <paramref name="performanceRatio"/> as the ratio of PSI values to Inventory Scores.
        /// </summary>
        /// <param name="normalisedProductPsis"></param>
        /// <param name="normalisedProductInventoryScores"></param>
        /// <param name="performanceRatio"></param>
        /// <returns></returns>
        private Dictionary<PlanogramProduct, Double> GetCombinedProductScores(
            Dictionary<PlanogramProduct, Double> normalisedProductPsis, 
            Dictionary<PlanogramProduct, Double> normalisedProductInventoryScores, 
            Single performanceRatio)
        {
            Dictionary<PlanogramProduct, Double> combinedProductScores = new Dictionary<PlanogramProduct, Double>();

            foreach (KeyValuePair<PlanogramProduct, Double> productPsi in normalisedProductPsis)
            {
                PlanogramProduct product = productPsi.Key;
                Double psi = productPsi.Value;

                Double inventoryScore = 0d;
                normalisedProductInventoryScores.TryGetValue(product, out inventoryScore);

                combinedProductScores.Add(
                    product,
                    (performanceRatio * psi) + ((1 - performanceRatio) * inventoryScore));
            }

            return combinedProductScores;
        }

        /// <summary>
        /// Calculates the un-normalised Psi value for the given product.
        /// </summary>
        /// <param name="product">The Planogram Product for which the Psi should be calculated.</param>
        /// <param name="metricProfile">The profile of the metrics to use in the calculation.</param>
        /// <param name="performanceDataTotals">The pre-calculated totals of the performance data.</param>
        /// <returns>The product's un-normalised Psi, or null if no corresponding performance data is available.</returns>
        /// <exception cref="ArgumentException">Thrown if the product's parent is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if product is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if metricProfile is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if performanceDataTotals is null.</exception>
        internal static Double? GetUnNormalisedPsi(
            IPlanogramMetricProfile metricProfile,
            PlanogramProduct product,
            PlanogramPerformanceDataTotals performanceDataTotals)
        {
            if (product == null) throw new ArgumentNullException("product cannot be null");
            if (metricProfile == null) throw new ArgumentNullException("metricProfile cannot be null");
            if (performanceDataTotals == null) throw new ArgumentNullException("performanceDataTotals cannot be null");
            if (product.Parent == null) throw new ArgumentException("Parent cannot be null");
            PlanogramPerformanceData performanceData = product.Parent.Performance.PerformanceData.
                FirstOrDefault(d => d.PlanogramProductId.Equals(product.Id));
            if (performanceData == null) return null;

            Double unnormalisedPsi = 0;
            PlanogramPerformanceMetric metric;
            IEnumerable<PlanogramPerformanceMetric> planogramMetrics = product.Parent.Performance.Metrics;

            

            // Metric 1
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 1);
            if (metricProfile.Metric1Ratio != null && performanceData.P1 != null && performanceDataTotals.P1 != null && performanceDataTotals.P1 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric1Ratio * ((Single)performanceData.P1 / (Single)performanceDataTotals.P1);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric1Ratio * (1 - ((Single)performanceData.P1 / (Single)performanceDataTotals.P1));
                }
            }

            // Metric 2
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 2);
            if (metricProfile.Metric2Ratio != null && performanceData.P2 != null && performanceDataTotals.P2 != null && performanceDataTotals.P2 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric2Ratio * ((Single)performanceData.P2 / (Single)performanceDataTotals.P2);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric2Ratio * (1 - ((Single)performanceData.P2 / (Single)performanceDataTotals.P2));
                }
            }

            // Metric 3
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 3);
            if (metricProfile.Metric3Ratio != null && performanceData.P3 != null && performanceDataTotals.P3 != null && performanceDataTotals.P3 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric3Ratio * ((Single)performanceData.P3 / (Single)performanceDataTotals.P3);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric3Ratio * (1 - ((Single)performanceData.P3 / (Single)performanceDataTotals.P3));
                }
            }

            // Metric 4
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 4);
            if (metricProfile.Metric4Ratio != null && performanceData.P4 != null && performanceDataTotals.P4 != null && performanceDataTotals.P4 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric4Ratio * ((Single)performanceData.P4 / (Single)performanceDataTotals.P4);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric4Ratio * (1 - ((Single)performanceData.P4 / (Single)performanceDataTotals.P4));
                }
            }

            // Metric 5
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 5);
            if (metricProfile.Metric5Ratio != null && performanceData.P5 != null && performanceDataTotals.P5 != null && performanceDataTotals.P5 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric5Ratio * ((Single)performanceData.P5 / (Single)performanceDataTotals.P5);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric5Ratio * (1 - ((Single)performanceData.P5 / (Single)performanceDataTotals.P5));
                }
            }

            // Metric 6
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 6);
            if (metricProfile.Metric6Ratio != null && performanceData.P6 != null && performanceDataTotals.P6 != null && performanceDataTotals.P6 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric6Ratio * ((Single)performanceData.P6 / (Single)performanceDataTotals.P6);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric6Ratio * (1 - ((Single)performanceData.P6 / (Single)performanceDataTotals.P6));
                }
            }

            // Metric 7
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 7);
            if (metricProfile.Metric7Ratio != null && performanceData.P7 != null && performanceDataTotals.P7 != null && performanceDataTotals.P7 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric7Ratio * ((Single)performanceData.P7 / (Single)performanceDataTotals.P7);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric7Ratio * (1 - ((Single)performanceData.P7 / (Single)performanceDataTotals.P7));
                }
            }

            // Metric 8
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 8);
            if (metricProfile.Metric8Ratio != null && performanceData.P8 != null && performanceDataTotals.P8 != null && performanceDataTotals.P8 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric8Ratio * ((Single)performanceData.P8 / (Single)performanceDataTotals.P8);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric8Ratio * (1 - ((Single)performanceData.P8 / (Single)performanceDataTotals.P8));
                }
            }

            // Metric 9
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 9);
            if (metricProfile.Metric9Ratio != null && performanceData.P9 != null && performanceDataTotals.P9 != null && performanceDataTotals.P9 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric9Ratio * ((Single)performanceData.P9 / (Single)performanceDataTotals.P9);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric9Ratio * (1 - ((Single)performanceData.P9 / (Single)performanceDataTotals.P9));
                }
            }

            // Metric 10
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 10);
            if (metricProfile.Metric10Ratio != null && performanceData.P10 != null && performanceDataTotals.P10 != null && performanceDataTotals.P10 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric10Ratio * ((Single)performanceData.P10 / (Single)performanceDataTotals.P10);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric10Ratio * (1 - ((Single)performanceData.P10 / (Single)performanceDataTotals.P10));
                }
            }

            // Metric 11
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 11);
            if (metricProfile.Metric11Ratio != null && performanceData.P11 != null && performanceDataTotals.P11 != null && performanceDataTotals.P11 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric11Ratio * ((Single)performanceData.P11 / (Single)performanceDataTotals.P11);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric11Ratio * (1 - ((Single)performanceData.P11 / (Single)performanceDataTotals.P11));
                }
            }

            // Metric 12
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 12);
            if (metricProfile.Metric12Ratio != null && performanceData.P12 != null && performanceDataTotals.P12 != null && performanceDataTotals.P12 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric12Ratio * ((Single)performanceData.P12 / (Single)performanceDataTotals.P12);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric12Ratio * (1 - ((Single)performanceData.P12 / (Single)performanceDataTotals.P12));
                }
            }

            // Metric 13
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 13);
            if (metricProfile.Metric13Ratio != null && performanceData.P13 != null && performanceDataTotals.P13 != null && performanceDataTotals.P13 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric13Ratio * ((Single)performanceData.P13 / (Single)performanceDataTotals.P13);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric13Ratio * (1 - ((Single)performanceData.P13 / (Single)performanceDataTotals.P13));
                }
            }

            // Metric 14
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 14);
            if (metricProfile.Metric14Ratio != null && performanceData.P14 != null && performanceDataTotals.P14 != null && performanceDataTotals.P14 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric14Ratio * ((Single)performanceData.P14 / (Single)performanceDataTotals.P14);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric14Ratio * (1 - ((Single)performanceData.P14 / (Single)performanceDataTotals.P14));
                }
            }

            // Metric 15
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 15);
            if (metricProfile.Metric15Ratio != null && performanceData.P15 != null && performanceDataTotals.P15 != null && performanceDataTotals.P15 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric15Ratio * ((Single)performanceData.P15 / (Single)performanceDataTotals.P15);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric15Ratio * (1 - ((Single)performanceData.P15 / (Single)performanceDataTotals.P15));
                }
            }

            // Metric 16
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 16);
            if (metricProfile.Metric16Ratio != null && performanceData.P16 != null && performanceDataTotals.P16 != null && performanceDataTotals.P16 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric16Ratio * ((Single)performanceData.P16 / (Single)performanceDataTotals.P16);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric16Ratio * (1 - ((Single)performanceData.P16 / (Single)performanceDataTotals.P16));
                }
            }

            // Metric 17
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 17);
            if (metricProfile.Metric17Ratio != null && performanceData.P17 != null && performanceDataTotals.P17 != null && performanceDataTotals.P17 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric17Ratio * ((Single)performanceData.P17 / (Single)performanceDataTotals.P17);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric17Ratio * (1 - ((Single)performanceData.P17 / (Single)performanceDataTotals.P17));
                }
            }

            // Metric 18
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 18);
            if (metricProfile.Metric18Ratio != null && performanceData.P18 != null && performanceDataTotals.P18 != null && performanceDataTotals.P18 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric18Ratio * ((Single)performanceData.P18 / (Single)performanceDataTotals.P18);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric18Ratio * (1 - ((Single)performanceData.P18 / (Single)performanceDataTotals.P18));
                }
            }

            // Metric 19
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 19);
            if (metricProfile.Metric19Ratio != null && performanceData.P19 != null && performanceDataTotals.P19 != null && performanceDataTotals.P19 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric19Ratio * ((Single)performanceData.P19 / (Single)performanceDataTotals.P19);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric19Ratio * (1 - ((Single)performanceData.P19 / (Single)performanceDataTotals.P19));
                }
            }

            // Metric 20
            metric = planogramMetrics.FirstOrDefault(m => m.MetricId == 20);
            if (metricProfile.Metric20Ratio != null && performanceData.P20 != null && performanceDataTotals.P20 != null && performanceDataTotals.P20 > 0 && metric != null)
            {
                if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                {
                    unnormalisedPsi += (Single)metricProfile.Metric20Ratio * ((Single)performanceData.P20 / (Single)performanceDataTotals.P20);
                }
                else
                {
                    unnormalisedPsi += (Single)metricProfile.Metric20Ratio * (1 - ((Single)performanceData.P20 / (Single)performanceDataTotals.P20));
                }
            }

            return unnormalisedPsi;
        }

        /// <summary>
        /// Calculates normalised Psis for the given products.
        /// </summary>
        /// <param name="metricProfile">The metric profile to use for the calculations.</param>
        /// <param name="products">The products to calculate the Psis for.</param>
        /// <param name="performanceDataTotals">Pre-calculated performance data totals.</param>
        /// <returns>A dictionary of products and their Psi values.</returns>
        internal static Dictionary<PlanogramProduct, Double> GetNormalisedPsis(
            IPlanogramMetricProfile metricProfile,
            IEnumerable<PlanogramProduct> products,
            PlanogramPerformanceDataTotals performanceDataTotals)
        {
            Dictionary<PlanogramProduct, Double?> unnormalisedPsis =
                products.ToDictionary(p => p, p => GetUnNormalisedPsi(metricProfile, p, performanceDataTotals));

            //  Get the sum of the PSI values so that we can determine the percentage of the total each PSI represents.
            //  NB If all the PSI values are zero, it will result in a total of zero, which will later cause a 0 / 0 operation, and thus a NaN value.
            //  This would make the divider invalid, and the planogram would not be allowed to be saved. In this case, just return the uncorrected values,
            //  which are zeros, anyway.
            Double totalPsiValue = unnormalisedPsis.Values.Select(v => v ?? 0.0D).Sum();
            if (totalPsiValue.EqualTo(0)) totalPsiValue = 1;

            // Build a return dictionary of products and their normalised psi values.
            return unnormalisedPsis.Where(productAndPsi => productAndPsi.Value != null)
                                   .ToDictionary(productAndPsi => productAndPsi.Key, productAndPsi => (productAndPsi.Value ?? 0.0D) / totalPsiValue);
        }
        #endregion

        /// <summary>
        /// Gets a list of comma seperated group names
        /// </summary>
        internal static String GetCommaSeparatedGroupNames(IEnumerable<PlanogramBlockingGroup> groups)
        {
            var groupNames = new StringBuilder();

            // Append comma between each names.
            foreach (var group in groups)
            {
                groupNames.AppendFormat("{0}, ", group.Name);
            }

            // Remove trailing comma and space.
            groupNames.Remove(groupNames.Length - 2, 2);

            return groupNames.ToString();
        }

        /// <summary>
        /// Combines the given groups into a new group, which is added to the list.
        /// </summary>
        /// <param name="groups">The groups to combine.</param>
        /// <param name="planogramBlocking">The Planogram Blocking into which the Groups should be combined.</param>
        /// <param name="blockColour">The colour value that the new block should have.</param>
        /// <returns>The new group that combines those provided.</returns>
        /// <exception cref="ArgumentNullException">Thrown if groups is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if planogramBlocking is null.</exception>
        /// <exception cref="ArgumentException">Thrown if any of the groups have CanMerge equal to false.</exception>
        /// <exception cref="ArgumentException">
        /// Thrown if any of the groups are restricted by component type and multiple component types exist.
        /// </exception>
        [Obsolete("V8-29027: Blocks should not be combined for now", true)]
        private static PlanogramBlockingGroup CombineGroups(IEnumerable<PlanogramBlockingGroup> groups, PlanogramBlocking planogramBlocking, Int32 blockColour)
        {
            // V8-29027
            // The plan should fail if it needs to merge a group.
            throw new NotImplementedException();
            //    if (planogramBlocking == null) throw new ArgumentNullException("planogramBlocking cannot be null");
            //    if (groups == null) throw new ArgumentNullException("groups cannot be null");

            //    // If any of the groups can't be merged then throw a GetBlockingException, which
            //    // will be caught and logged.
            //    if (groups.Any(g => !g.CanMerge))
            //        throw new GetBlockingException(
            //            String.Format(Message.GetBlocking_CombineGroupsCantBeMergedError, GetCommaSeparatedGroupNames(groups.Where(g => !g.CanMerge))),
            //            PlanogramEventLogEntryType.Error);

            //    // If any groups are restricted by component type, then ensure that only one component type is present.
            //    var differentComponentTypes = planogramBlocking.Locations.
            //        SelectMany(l => l.GetMerchandisableFixtureAndAssemblyComponents()).
            //        Select(c => c.GetPlanogramComponent().ComponentType).
            //        Distinct();
            //    if (groups.Any(g => g.IsRestrictedByComponentType) && differentComponentTypes.Count() > 1)
            //        throw new GetBlockingException(
            //                String.Format(Message.GetBlocking_CombineGroupsWereRestrictedByComponentTypeError, GetCommaSeparatedGroupNames(groups.Where(g => !g.IsRestrictedByComponentType))),
            //                PlanogramEventLogEntryType.Error);

            //    var newGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
            //    planogramBlocking.Groups.Add(newGroup);

            //    // If any of the groups assign products on a dynamic basis, then we need
            //    // re-create all of the groups' criteria in the new group.
            //    if (groups.Any(g => g.ProductAssignType == PlanogramBlockingProductAssignType.Dynamic))
            //    {
            //        newGroup.ProductAssignType = PlanogramBlockingProductAssignType.Dynamic;
            //        var fields = new List<PlanogramBlockingGroupField>();

            //        foreach (var group in groups)
            //        {
            //            // If the group dynamically assigns products, just use its fields.
            //            if (group.ProductAssignType == PlanogramBlockingProductAssignType.Dynamic)
            //            {
            //                fields.AddRange(group.Fields.Select(f => PlanogramBlockingGroupField.NewPlanogramBlockingGroupField(f)));
            //            }
            //            else // If the group has a static list of products, create fields that will
            //            // select the products from their Gtin values.
            //            {
            //                foreach (var product in group.Products)
            //                {
            //                    var field = PlanogramBlockingGroupField.NewPlanogramBlockingGroupField();
            //                    field.Name = PlanogramProduct.GtinProperty.Name;
            //                    field.FieldType = PlanogramBlockingGroupFieldType.Text;
            //                    field.TextValue = product.Gtin;
            //                    fields.Add(field);
            //                }
            //            }
            //        }

            //        newGroup.Fields.AddRange(fields.Distinct(new PlanogramBlockingGroupFieldComparer()));
            //    }
            //    else // We can just combine the product lists.
            //    {
            //        newGroup.ProductAssignType = PlanogramBlockingProductAssignType.Static;
            //        newGroup.Products.AddRange(groups.
            //            SelectMany(g => g.Products).
            //            Distinct(new PlanogramBlockingGroupProductComparer()).
            //            Select(p => PlanogramBlockingGroupProduct.NewPlanogramBlockingGroupProduct(p)));
            //    }

            //    // Combine the rest of the group properties.
            //    var newName = BuildNewGroupName(groups);
            //    newGroup.Name =
            //        planogramBlocking.Groups.Any(g => g.Name.Equals(newName, StringComparison.InvariantCultureIgnoreCase)) ?
            //        Guid.NewGuid().ToString() :
            //        newName;
            //    newGroup.CanMerge = true;
            //    newGroup.CanCompromiseSequence = groups.Any(g => g.CanCompromiseSequence);
            //    newGroup.CanOptimise = groups.Any(g => g.CanOptimise);
            //    newGroup.Colour = blockColour;
            //    newGroup.FillPatternType = PlanogramBlockingFillPatternType.Solid;
            //    newGroup.IsRestrictedByComponentType = false;

            //    return newGroup;
        }

        /// <summary>
        /// Returns a collection of locations that would be affected by a divider removal and the subsequent
        /// re-grouping operation.
        /// </summary>
        /// <param name="divider">The divider that would be removed.</param>
        /// <returns>A collection of affected locations.</returns>
        /// <exception cref="ArgumentNullException">Thrown if divider is null.</exception>
        /// <exception cref="ArgumentException">Thrown if divider.Parent is null.</exception>
        /// <example>
        /// We need to pick up all locations affected here, not just those that border
        /// the divider in question. For example:
        ///  ____________
        /// |    | D | E |       A, B and C need to be merged across the divider.
        /// |    |___|___|       D and E also need to be considered, because merging
        /// | C  |  B    |       A, B and C will create a location that overlaps D and E.
        /// |============| o----+
        /// |     A      |      |
        /// |            |      +---o Divider to be removed
        /// </example>
        internal static IEnumerable<PlanogramBlockingLocation> GetLocationsAffectedByDividerRemoval(PlanogramBlockingDivider divider)
        {
            if (divider == null) throw new ArgumentNullException("divider");
            if (divider.Parent == null) throw new ArgumentException("divider.Parent cannot be null");
            var affectedLocations = divider.Parent.Locations.Where(l => l.IsAssociated(divider)).ToList();
            if (affectedLocations.Count() > 2)
            {
                // Get left and right dividers that aren't positioned on the edges of this divider.
                var significantDividers = affectedLocations.
                    SelectMany(l => new PlanogramBlockingDivider[] { l.GetLeftDivider(), l.GetRightDivider() }).
                    Where(d => d != null && d.X.GreaterThan(divider.X) && d.X.LessThan(divider.X + divider.Length));

                // Get all the locations associated with these dividers.
                var locationsToAddToAffected = new List<PlanogramBlockingLocation>();
                foreach (var significantDivider in significantDividers)
                {
                    var additionalLocations = divider.Parent.Locations.Where(l => l.IsAssociated(significantDivider));
                    locationsToAddToAffected.AddRange(additionalLocations);
                }

                // Add new locations to the affected list.
                affectedLocations.AddRange(locationsToAddToAffected.
                    Where(l => !affectedLocations.Contains(l)).
                    Distinct());
            }
            return affectedLocations;
        }

        /// <summary>
        /// Checks the locations around <paramref name="divider"/> to see if the contain merchandisable components,
        /// if appropriate.
        /// </summary>
        /// <param name="divider">The divider whose locations will be evaluated.</param>
        /// <remarks>
        /// In this method, we need to check locations around the divider to make sure that the contain
        /// merchandisable components. However, we're not interested in locations that are between this
        /// divider and non-snapped dividers, because these dividers may yet snap ensuring that the location
        /// does contain merchandisable components. As such, we just want to check locations that are either
        /// between the divider and the edge of the blocking area, or between this divider and other snapped
        /// dividers.
        /// </remarks>
        private static Boolean DividerLocationsContainMerchComponents(PlanogramBlockingDivider divider, ApplyBlockingContext context)
        {
            foreach (PlanogramBlockingLocation location in divider.GetLocations())
            {
                PlanogramBlockingDivider oppositeDivider = location.GetOppositeDividerTo(divider);

                // If the divider is not an edge of the blocking area or already snapped, then we won't check it.
                if(!(oppositeDivider == null || context.SnappedDividers.Contains(oppositeDivider))) continue;

                // If this location does not contain merchandisable components, return false, because
                // the divider's position is not valid.
                if(!location.GetMerchandisableFixtureAndAssemblyComponents().Any()) return false;
            }

            return true;
        }

        /// <summary>
        /// Combines the names of the given groups into one, capping the new name at 50 characters.
        /// </summary>
        /// <param name="groups">The groups whose names should be combined.</param>
        /// <returns>A new group name.</returns>
        private static String BuildNewGroupName(IEnumerable<PlanogramBlockingGroup> groups)
        {
            var stringBuilder = new StringBuilder();
            var groupCount = groups.Count();
            for (Int32 i = 0; i < groupCount; i++)
            {
                if (i == 0)
                {
                    stringBuilder.AppendFormat(Message.GetBlockingByName_v1_GroupCombinedName_First, groups.ElementAt(i).Name);
                }
                else if (i == groupCount - 1)
                {
                    if (groupCount > 2)
                    {
                        stringBuilder.AppendFormat(Message.GetBlockingByName_v1_GroupCombinedName_Last, groups.ElementAt(i).Name);
                    }
                    else
                    {
                        stringBuilder.Append(groups.ElementAt(i).Name);
                    }
                }
                else if (i == groupCount - 2)
                {
                    stringBuilder.AppendFormat(Message.GetBlockingByName_v1_GroupCombinedName_MiddleSingle, groups.ElementAt(i).Name);
                }
                else
                {
                    stringBuilder.AppendFormat(Message.GetBlockingByName_v1_GroupCombinedName_MiddleMulti, groups.ElementAt(i).Name);
                }
            }
            if (stringBuilder.Length > 50) // Max length of name
            {
                // Remove last three characters and replace with ...
                var removalIndex = 50 - Message.GetBlockingByName_v1_GroupCombinedName_Etc.Length;
                stringBuilder.Remove(removalIndex, stringBuilder.Length - removalIndex);
                stringBuilder.Append(Message.GetBlockingByName_v1_GroupCombinedName_Etc);
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Returns a dictionary for the percentage of merchandisable space each blocking group
        /// can use.
        /// </summary>
        /// <param name="merchandisingGroups"></param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <param name="blockingHeightOffset"></param>
        /// <returns></returns>
        public Dictionary<Object, Double> GetMerchandisingVolumePercentagesByGroupId(
            IEnumerable<PlanogramMerchandisingGroup> merchandisingGroups,
            Single blockingWidth,
            Single blockingHeight,
            Single blockingHeightOffset)
        {
            var blocks = new PlanogramMerchandisingBlocks(this,
                                                          Parent.Sequence,
                                                          merchandisingGroups,
                                                          blockingWidth,
                                                          blockingHeight,
                                                          blockingHeightOffset,
                                                          true);
            var groupTotals = new Dictionary<Object, Double>();
            Double total = 0;
            foreach (PlanogramMerchandisingBlock block in blocks)
            {
                List<RectValue> placementBounds = block
                    .BlockPlacements.Select(place =>
                                            place.MerchandisingGroup.SubComponentPlacements.First()
                                                 .GetPlanogramRelativeTransform()
                                                 .TransformBounds(place.Bounds))
                    .ToList();

                //get the volume for this merch block
                Double blockTotal = GetTotalRectValueVolume(placementBounds);

                //keep the overall total going
                total += blockTotal;
                Double blockingGroupTotal;

                //Add the total fo rthis merch block to its blocking group total
                if (groupTotals.TryGetValue(block.BlockingGroup.Id, out blockingGroupTotal))
                {
                    groupTotals[block.BlockingGroup.Id] += blockTotal;
                }
                else
                {
                    groupTotals[block.BlockingGroup.Id] = blockTotal;
                }

            }

            var groupPercentages = new Dictionary<Object, Double>();
            foreach (KeyValuePair<Object, Double> v in groupTotals)
            {
                groupPercentages[v.Key] = total.GreaterThan(0) ? v.Value/total : 0;
            }

            //totals are now percentages
            return groupPercentages;
        }

        /// <summary>
        /// Gets the volume contained by a list of rect values taking into account
        /// overlapping space
        /// </summary>
        /// <param name="rects"></param>
        /// <returns></returns>
        private double GetTotalRectValueVolume(List<RectValue> rects)
        {
            Double output = 0;
            List<RectValue> checkedRects = new List<RectValue>();
            //calculate the volumes one rectvalue at a time adding in a new
            //rectvalue at each pass so that we never check against ourself.            
            foreach (RectValue rect in rects)
            {
                output += rect.Height * rect.Width * rect.Depth;
                List<RectValue> inersectRects = new List<RectValue>();
                foreach (RectValue checkRect in checkedRects)
                {
                    RectValue? intersect = checkRect.Intersect(rect);
                    if (intersect.HasValue)
                    {
                        inersectRects.Add(intersect.Value);
                    }
                }

                //remove any overlapping volumes
                //this keeps going till even the
                //overlaps have no overlaps.
                output -= GetTotalRectValueVolume(inersectRects);

                checkedRects.Add(rect);
            }

            return output;
        }
        #endregion

        #region IPlanogramBlocking Members

        IPlanogramBlockingDivider IPlanogramBlocking.AddNewDivider(Single x, Single y, PlanogramBlockingDividerType type)
        {
            return this.Dividers.Add(x, y, type);
        }

        void IPlanogramBlocking.RemoveDivider(IPlanogramBlockingDivider divider)
        {
            this.Dividers.Remove(divider as PlanogramBlockingDivider);
        }

        #endregion

        #endregion
    }
}