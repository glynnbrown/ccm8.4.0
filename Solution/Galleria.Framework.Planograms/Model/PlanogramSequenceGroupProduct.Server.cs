﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28996 : A.Silva
//		Created
#endregion

#region Version History : CCM 830
// V8-32504 : A.Kuszyk
//  Added sub group id property.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public sealed partial class PlanogramSequenceGroupProduct
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to obtain new instances of <see cref="PlanogramSequenceGroupProduct"/>.
        /// </summary>
        private PlanogramSequenceGroupProduct() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Invokes the data portal to retrieve the <see cref="PlanogramSequenceGroupProduct"/> item corresponding to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal static PlanogramSequenceGroupProduct Fetch(IDalContext dalContext, PlanogramSequenceGroupProductDto dto)
        {
            return DataPortal.FetchChild<PlanogramSequenceGroupProduct>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads this instance properties from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramSequenceGroupProductDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(GtinProperty, dto.Gtin);
            LoadProperty(PlanogramSequenceGroupSubGroupIdProperty, dto.PlanogramSequenceGroupSubGroupId);
            LoadProperty(SequenceNumberProperty, dto.SequenceNumber);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Creates a data transfer object for this instance.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private PlanogramSequenceGroupProductDto GetDataTransferObject(PlanogramSequenceGroup parent)
        {
            return new PlanogramSequenceGroupProductDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramSequenceGroupId = parent.Id,
                PlanogramSequenceGroupSubGroupId = ReadProperty(PlanogramSequenceGroupSubGroupIdProperty),
                Gtin = ReadProperty(GtinProperty),
                SequenceNumber = ReadProperty(SequenceNumberProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked via reflection by CSLA to fetch the child item.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void Child_Fetch(IDalContext dalContext, PlanogramSequenceGroupProductDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Invoked via reflection by CSLA to insert the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    this.ResolveIds(dc);
                    PlanogramSequenceGroupProductDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramSequenceGroupProduct>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Invoked via reflection by CSLA to update the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Update(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            if (IsSelfDirty)
            {
                batchContext.Update(dc => GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Invoked via reflection by CSLA to delete the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}