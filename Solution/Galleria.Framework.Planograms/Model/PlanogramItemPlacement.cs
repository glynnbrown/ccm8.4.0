﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.3)
// V8-31541 : L.Ineson
//  Created
//V8-32421 : L.Ineson
//  Moved resolve field methods to here and added resolve for
//  common child values
//V8-32384 : L.Ineson
//  Removed percentage multiply.
// V8-32594 : M.Brumby
//  Renamed input property to prevent looping
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.ViewModel;
using System.Diagnostics;
using Galleria.Framework.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Class to represent a single item within the planogram visual tree structure
    /// to bring together information about its parents and children.
    /// </summary>
    public class PlanogramItemPlacement
    {
        #region Fields
        private HashCode _hashCode;
        private PlanogramAnnotation _annotation;
        private PlanogramPosition _position;
        private PlanogramProduct _product;
        private PlanogramSubComponent _subComponent;
        private PlanogramComponent _component;
        private PlanogramFixtureComponent _fixtureComponent;
        private PlanogramAssemblyComponent _assemblyComponent;
        private PlanogramAssembly _assembly;
        private PlanogramFixtureAssembly _fixtureAssembly;
        private PlanogramFixture _fixture;
        private PlanogramFixtureItem _fixtureItem;
        private Planogram _planogram;
        private PlanogramItemType _itemType;
        #endregion

        #region Properties

        #region Planogram
        public Planogram Planogram
        {
            get { return _planogram; }
            private set { _planogram = value; }
        }
        #endregion

        #region FixtureItem
        public PlanogramFixtureItem FixtureItem
        {
            get { return _fixtureItem; }
            private set { _fixtureItem = value; }
        }
        #endregion

        #region Fixture
        public PlanogramFixture Fixture
        {
            get { return _fixture; }
            private set { _fixture = value; }
        }
        #endregion

        #region Assembly
        public PlanogramAssembly Assembly
        {
            get { return _assembly; }
            private set { _assembly = value; }
        }
        #endregion

        #region FixtureAssembly
        public PlanogramFixtureAssembly FixtureAssembly
        {
            get { return _fixtureAssembly; }
            private set { _fixtureAssembly = value; }
        }
        #endregion

        #region AssemblyComponent
        public PlanogramAssemblyComponent AssemblyComponent
        {
            get { return _assemblyComponent; }
            private set { _assemblyComponent = value; }
        }
        #endregion

        #region FixtureComponent
        public PlanogramFixtureComponent FixtureComponent
        {
            get { return _fixtureComponent; }
            private set { _fixtureComponent = value; }
        }
        #endregion

        #region Component

        public PlanogramComponent Component
        {
            get { return _component; }
            private set { _component = value; }
        }

        #endregion

        #region SubComponent

        public PlanogramSubComponent SubComponent
        {
            get { return _subComponent; }
            private set { _subComponent = value; }
        }

        #endregion

        #region Product

        public PlanogramProduct Product
        {
            get { return _product; }
            private set { _product = value; }
        }

        #endregion

        #region Position
        public PlanogramPosition Position
        {
            get { return _position; }
            private set { _position = value; }
        }
        #endregion

        #region Annotation
        public PlanogramAnnotation Annotation
        {
            get { return _annotation; }
            private set { _annotation = value; }
        }
        #endregion

        #region ItemType
        public PlanogramItemType ItemType
        {
            get { return _itemType; }
            private set { _itemType = value; }
        }
        #endregion

        #endregion

        #region Constructor
        private PlanogramItemPlacement() { } //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new item of this type.
        /// </summary>
        public static PlanogramItemPlacement NewPlanogramItemPlacement(
            PlanogramPosition position = null,
            PlanogramProduct product = null,
            PlanogramSubComponent subComponent = null,
            PlanogramComponent component = null,
            PlanogramFixtureComponent fixtureComponent = null,
            PlanogramAssemblyComponent assemblyComponent = null,
            PlanogramAssembly assembly = null,
            PlanogramFixtureAssembly fixtureAssembly = null,
            PlanogramFixture fixture = null,
            PlanogramFixtureItem fixtureItem = null,
            Planogram planogram = null,
            PlanogramAnnotation annotation = null)
        {
            PlanogramItemPlacement item = new PlanogramItemPlacement();
            item.Product = product;
            item.Position = position;
            item.SubComponent = subComponent;
            item.Component = component;
            item.FixtureComponent = fixtureComponent;
            item.AssemblyComponent = assemblyComponent;
            item.Assembly = assembly;
            item.FixtureAssembly = fixtureAssembly;
            item.Fixture = fixture;
            item.FixtureItem = fixtureItem;
            item.Planogram = planogram;
            item.Annotation = annotation;

            item.ItemType = item.GetItemType();
            item.GenerateHashCode();

            return item;
        }

        /// <summary>
        /// Creates a new item of this type.
        /// </summary>
        public static PlanogramItemPlacement NewPlanogramItemPlacement(PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramItemPlacement item = new PlanogramItemPlacement();
            item.SubComponent = subComponentPlacement.SubComponent;
            item.Component = subComponentPlacement.Component;
            item.FixtureComponent = subComponentPlacement.FixtureComponent;
            item.AssemblyComponent = subComponentPlacement.AssemblyComponent;
            item.Assembly = subComponentPlacement.Assembly;
            item.FixtureAssembly = subComponentPlacement.FixtureAssembly;
            item.Fixture = subComponentPlacement.Fixture;
            item.FixtureItem = subComponentPlacement.FixtureItem;
            item.Planogram = subComponentPlacement.Planogram;

            item.ItemType = item.GetItemType();
            item.GenerateHashCode();

            return item;
        }

        /// <summary>
        /// Creates a new item of this type.
        /// </summary>
        public static PlanogramItemPlacement NewPlanogramItemPlacement(
            PlanogramPosition position, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramItemPlacement item = new PlanogramItemPlacement();
            item.Product = product;
            item.Position = position;
            item.SubComponent = subComponentPlacement.SubComponent;
            item.Component = subComponentPlacement.Component;
            item.FixtureComponent = subComponentPlacement.FixtureComponent;
            item.AssemblyComponent = subComponentPlacement.AssemblyComponent;
            item.Assembly = subComponentPlacement.Assembly;
            item.FixtureAssembly = subComponentPlacement.FixtureAssembly;
            item.Fixture = subComponentPlacement.Fixture;
            item.FixtureItem = subComponentPlacement.FixtureItem;
            item.Planogram = subComponentPlacement.Planogram;

            item.ItemType = item.GetItemType();
            item.GenerateHashCode();

            return item;
        }

        /// <summary>
        /// Creates a new item of this type.
        /// </summary>
        public static PlanogramItemPlacement NewPlanogramItemPlacement(PlanogramPosition position)
        {
            return PlanogramItemPlacement.NewPlanogramItemPlacement(
                position,
                position.GetPlanogramProduct(),
                position.GetPlanogramSubComponentPlacement());
        }

        /// <summary>
        /// Creates a new item of this type.
        /// </summary>
        public static PlanogramItemPlacement NewPlanogramItemPlacement(Planogram planogramItem)
        {
            return PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: planogramItem);
        }

        #endregion

        #region Methods

        #region GetItem

        /// <summary>
        /// Gets the type of item this represents.
        /// </summary>
        private PlanogramItemType GetItemType()
        {
            if (Annotation != null)
                return PlanogramItemType.Annotation;

            if (Product != null && Position == null)
                return PlanogramItemType.Product;

            if (Position != null)
                return PlanogramItemType.Position;

            if (SubComponent != null)
                return PlanogramItemType.SubComponent;

            if (Component != null)
                return PlanogramItemType.Component;

            if (Assembly != null)
                return PlanogramItemType.Assembly;

            if (Fixture != null)
                return PlanogramItemType.Fixture;


            return PlanogramItemType.Planogram;
        }

        /// <summary>
        /// Returns the value from this item at the given level.
        /// </summary>
        public Object GetModelObjectOfType(PlanogramItemType itemType)
        {
            switch (itemType)
            {
                case PlanogramItemType.Annotation:
                    return this.Annotation;

                case PlanogramItemType.Assembly:
                    return this.Assembly;

                case PlanogramItemType.Component:
                    return this.Component;

                case PlanogramItemType.Fixture:
                    return this.Fixture;

                case PlanogramItemType.Planogram:
                    return this.Planogram;

                case PlanogramItemType.Position:
                    return this.Position;

                case PlanogramItemType.Product:
                    return this.Product;

                case PlanogramItemType.SubComponent:
                    return this.SubComponent;

                default:
                    return null;
            }
        }

        /// <summary>
        /// Returns a list of items from this placement for the given type.
        /// This could be a single parent or a list of children underneath.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        public List<PlanogramItemPlacement> GetPlanogramItemPlacementOfType(PlanogramItemType itemType)
        {
            return GetItemsOfType(this, itemType);
        }

        public static List<PlanogramItemPlacement> GetItemsOfType(Planogram sourceItem, PlanogramItemType itemType)
        {
            return PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem).GetPlanogramItemPlacementOfType(itemType);
        }

        public static List<PlanogramItemPlacement> GetItemsOfType(PlanogramItemPlacement sourceItem, PlanogramItemType itemType)
        {
            List<PlanogramItemPlacement> items = new List<PlanogramItemPlacement>();

            Planogram plan = sourceItem.Planogram;

            switch (sourceItem.ItemType)
            {
                #region Planogram
                case PlanogramItemType.Planogram:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Product:
                            foreach (PlanogramProduct p in plan.Products)
                            {
                                items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(product: p, planogram: plan));
                            }
                            break;

                        case PlanogramItemType.Position:
                        case PlanogramItemType.Fixture:
                        case PlanogramItemType.Assembly:
                        case PlanogramItemType.Component:
                            foreach (PlanogramSubComponentPlacement subPlacement in plan.GetPlanogramSubComponentPlacements())
                            {
                                items.AddRange(EnumerateItemsOfType(subPlacement, itemType));
                            }
                            break;
                    }
                    break;
                #endregion

                #region Fixture
                case PlanogramItemType.Fixture:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;
                        case PlanogramItemType.Fixture:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Position:
                        case PlanogramItemType.Product:
                        case PlanogramItemType.SubComponent:
                        case PlanogramItemType.Component:
                        case PlanogramItemType.Assembly:
                            foreach (PlanogramSubComponentPlacement subPlacement in sourceItem.FixtureItem.GetPlanogramSubComponentPlacements())
                            {
                                items.AddRange(EnumerateItemsOfType(subPlacement, itemType));
                            }
                            break;
                    }
                    break;
                #endregion

                #region Assembly
                case PlanogramItemType.Assembly:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;
                        case PlanogramItemType.Fixture:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                            planogram: sourceItem.Planogram,
                            fixtureItem: sourceItem.FixtureItem,
                            fixture: sourceItem.Fixture));
                            break;

                        case PlanogramItemType.Assembly:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Position:
                        case PlanogramItemType.Product:
                        case PlanogramItemType.SubComponent:
                        case PlanogramItemType.Component:
                            foreach (PlanogramSubComponentPlacement subPlacement in plan.GetPlanogramSubComponentPlacements())
                            {
                                if (subPlacement.Assembly != sourceItem.Assembly) continue;
                                items.AddRange(EnumerateItemsOfType(subPlacement, itemType));
                            }
                            break;
                    }
                    break;
                #endregion

                #region Component
                case PlanogramItemType.Component:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;

                        case PlanogramItemType.Fixture:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                            planogram: sourceItem.Planogram,
                            fixtureItem: sourceItem.FixtureItem,
                            fixture: sourceItem.Fixture));
                            break;

                        case PlanogramItemType.Assembly:
                            if (sourceItem.Assembly != null)
                            {
                                items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent));
                            }
                            break;

                        case PlanogramItemType.Component:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Position:
                        case PlanogramItemType.Product:
                        case PlanogramItemType.SubComponent:
                            foreach (PlanogramSubComponentPlacement subPlacement in plan.GetPlanogramSubComponentPlacements())
                            {
                                if (subPlacement.Component != sourceItem.Component) continue;
                                items.AddRange(EnumerateItemsOfType(subPlacement, itemType));
                            }
                            break;
                    }
                    break;
                #endregion

                #region SubComponent
                case PlanogramItemType.SubComponent:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;

                        case PlanogramItemType.Fixture:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                            planogram: sourceItem.Planogram,
                            fixtureItem: sourceItem.FixtureItem,
                            fixture: sourceItem.Fixture));
                            break;

                        case PlanogramItemType.Assembly:
                            if (sourceItem.Assembly != null)
                            {
                                items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent));
                            }
                            break;

                        case PlanogramItemType.Component:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent,
                                fixtureComponent: sourceItem.FixtureComponent,
                                component: sourceItem.Component));
                            break;

                        case PlanogramItemType.SubComponent:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Position:
                        case PlanogramItemType.Product:
                            foreach (PlanogramSubComponentPlacement subPlacement in sourceItem.SubComponent.GetPlanogramSubComponentPlacements())
                            {
                                items.AddRange(EnumerateItemsOfType(subPlacement, itemType));
                            }
                            break;
                    }
                    break;
                #endregion

                #region Position
                case PlanogramItemType.Position:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;

                        case PlanogramItemType.Fixture:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                            planogram: sourceItem.Planogram,
                            fixtureItem: sourceItem.FixtureItem,
                            fixture: sourceItem.Fixture));
                            break;

                        case PlanogramItemType.Assembly:
                            if (sourceItem.Assembly != null)
                            {
                                items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent));
                            }
                            break;

                        case PlanogramItemType.Component:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent,
                                fixtureComponent: sourceItem.FixtureComponent,
                                component: sourceItem.Component));
                            break;

                        case PlanogramItemType.SubComponent:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(
                                planogram: sourceItem.Planogram,
                                fixtureItem: sourceItem.FixtureItem,
                                fixture: sourceItem.Fixture,
                                assembly: sourceItem.Assembly,
                                assemblyComponent: sourceItem.AssemblyComponent,
                                fixtureComponent: sourceItem.FixtureComponent,
                                component: sourceItem.Component,
                                subComponent: sourceItem.SubComponent));
                            break;

                        case PlanogramItemType.Position:
                            items.Add(sourceItem);
                            break;

                        case PlanogramItemType.Product:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(product: sourceItem.Product, planogram: sourceItem.Planogram));
                            break;
                    }
                    break;
                #endregion

                #region Product
                case PlanogramItemType.Product:
                    switch (itemType)
                    {
                        case PlanogramItemType.Planogram:
                            items.Add(PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: sourceItem.Planogram));
                            break;

                        case PlanogramItemType.Product:
                            items.Add(sourceItem);
                            break;
                    }
                    break;
                #endregion
            }

            return items.Distinct().ToList();
        }

        private static IEnumerable<PlanogramItemPlacement> EnumerateItemsOfType(PlanogramSubComponentPlacement placement, PlanogramItemType itemType)
        {
            switch (itemType)
            {
                case PlanogramItemType.Product:
                    foreach (PlanogramPosition pos in placement.GetPlanogramPositions())
                    {
                        yield return PlanogramItemPlacement.NewPlanogramItemPlacement(product: pos.GetPlanogramProduct(), planogram: placement.Planogram);
                    }
                    break;

                case PlanogramItemType.Position:
                    foreach (PlanogramPosition pos in placement.GetPlanogramPositions())
                    {
                        yield return PlanogramItemPlacement.NewPlanogramItemPlacement(pos, pos.GetPlanogramProduct(), placement);
                    }
                    break;

                case PlanogramItemType.SubComponent:
                    yield return PlanogramItemPlacement.NewPlanogramItemPlacement(subComponentPlacement: placement);
                    break;

                case PlanogramItemType.Component:
                    yield return PlanogramItemPlacement.NewPlanogramItemPlacement(
                    component: placement.Component,
                    fixtureComponent: placement.FixtureComponent,
                    assemblyComponent: placement.AssemblyComponent,
                    assembly: placement.Assembly,
                    fixtureAssembly: placement.FixtureAssembly,
                    fixture: placement.Fixture,
                    fixtureItem: placement.FixtureItem,
                    planogram: placement.Planogram);
                    break;

                case PlanogramItemType.Assembly:
                    if (placement.Assembly == null) yield break;
                    else yield return PlanogramItemPlacement.NewPlanogramItemPlacement(
                    assembly: placement.Assembly,
                    fixtureAssembly: placement.FixtureAssembly,
                    fixture: placement.Fixture,
                    fixtureItem: placement.FixtureItem,
                    planogram: placement.Planogram);
                    break;

                case PlanogramItemType.Fixture:
                    yield return PlanogramItemPlacement.NewPlanogramItemPlacement(
                    fixture: placement.Fixture,
                    fixtureItem: placement.FixtureItem,
                    planogram: placement.Planogram);
                    break;

                case PlanogramItemType.Planogram:
                    yield return PlanogramItemPlacement.NewPlanogramItemPlacement(planogram: placement.Planogram);
                    break;
            }
        }
        
        #endregion

        #region Resolve ObjectFieldInfo

        /// <summary>
        /// Returns the value of the field for this placement.
        /// </summary>
        /// <param name="fieldInfo">the field to get value for</param>
        /// <returns>The resolved value.</returns>
        public Object ResolveField(ObjectFieldInfo fieldInfo)
        {
            return ResolveField(fieldInfo, this);
        }

        /// <summary>
        /// Returns the value of the field for the given target PlanogramItemPlacement.
        /// </summary>
        /// <param name="fieldInfo">the field to get value for</param>
        /// <param name="target">the target to get the value from.</param>
        /// <returns>The resolved value.</returns>
        public static Object ResolveField(ObjectFieldInfo fieldInfo, PlanogramItemPlacement target)
        {
            //Position:
            if (fieldInfo.OwnerType == typeof(PlanogramPosition))
            {
                if (target.Position != null)
                    return target.Position.GetFieldValue(fieldInfo);

                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.Position));
            }

            //Product:
            else if (fieldInfo.OwnerType == typeof(PlanogramProduct))
            {
                if (target.Product != null)
                    return target.Product.GetFieldValue(fieldInfo);

                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.Product));
            }

            //SubComponent:
            else if (fieldInfo.OwnerType == typeof(PlanogramSubComponent))
            {
                if (target.SubComponent != null)
                    return target.SubComponent.GetFieldValue(fieldInfo);

                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.SubComponent));
            }

            //Component:
            else if (fieldInfo.OwnerType == typeof(PlanogramComponent)
                || fieldInfo.OwnerType == typeof(PlanogramFixtureComponent)
                || fieldInfo.OwnerType == typeof(PlanogramAssemblyComponent))
            {
                if (target.Component != null)
                {
                    if (typeof(PlanogramFixtureComponent).GetProperty(fieldInfo.PropertyName) != null)
                    {
                        //nb: if the property is on fixture component, it will also be on assembly component
                        if (target.FixtureComponent != null)
                        {
                            return target.FixtureComponent.GetFieldValue(fieldInfo, target.FixtureItem);
                        }
                        else if (target.AssemblyComponent != null)
                        {
                            return target.AssemblyComponent.GetFieldValue(fieldInfo, target.FixtureItem, target.FixtureAssembly);
                        }
                        return null;
                    }
                    else
                    {
                        return target.Component.GetFieldValue(fieldInfo);
                    }
                }
                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.Component));
            }

            //Fixture:
            else if (fieldInfo.OwnerType == typeof(PlanogramFixture)
                || fieldInfo.OwnerType == typeof(PlanogramFixtureItem))
            {
                if (typeof(PlanogramFixtureItem).GetProperty(fieldInfo.PropertyName) != null)
                {
                    if (target.FixtureItem != null)
                        return target.FixtureItem.GetFieldValue(fieldInfo);
                }
                else
                {
                    if (target.Fixture != null)
                        return target.Fixture.GetFieldValue(fieldInfo);
                }
                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.Fixture));
            }

            //Planogram:
            else if (fieldInfo.OwnerType == typeof(Planogram))
            {
                if (target.Planogram == null) return null;
                return target.Planogram.GetFieldValue(fieldInfo);
            }

            //Assembly:
            else if (fieldInfo.OwnerType == typeof(PlanogramAssembly)
                || fieldInfo.OwnerType == typeof(PlanogramFixtureAssembly))
            {
                if (typeof(PlanogramFixtureAssembly).GetProperty(fieldInfo.PropertyName) != null)
                {
                    if (target.FixtureAssembly != null)
                        return target.FixtureAssembly.GetFieldValue(fieldInfo, target.FixtureItem);
                }
                else
                {
                    if (target.Assembly != null)
                        return target.Assembly.GetFieldValue(fieldInfo);
                }
                return ResolveCommonFieldValue(fieldInfo, target.GetPlanogramItemPlacementOfType(PlanogramItemType.Assembly));
            }


            Debug.Fail("Field info owner type not handled");
            return null;
        }

        /// <summary>
        /// Returns the value of the field across the given placemnet
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <param name="targetItems"></param>
        /// <returns></returns>
        private static Object ResolveCommonFieldValue(ObjectFieldInfo fieldInfo, IEnumerable<PlanogramItemPlacement> targetItems)
        {
            if (targetItems == null || !targetItems.Any()) return null;

            Object value = ResolveField(fieldInfo, targetItems.First());
            if (targetItems.Count() == 1) return value;

            for (Int32 i = 1; i < targetItems.Count(); i++)
            {
                if (ResolveField(fieldInfo, targetItems.ElementAt(i)) != value)
                {
                    return null;
                }
            }

            return value;
        }

        /// <summary>
        /// Returns the value of the resolved formula for the given target placement.
        /// </summary>
        /// <param name="fieldText">The formula to resolve.</param>
        /// <param name="masterFieldList">List of all available field infos.</param>
         /// <returns>The resolved value.</returns>
        public Object ResolveFieldExpression(String fieldText, IEnumerable<ObjectFieldInfo> masterFieldList)
        {
            ObjectFieldExpression expression = new ObjectFieldExpression(fieldText);
            expression.AddDynamicFieldParameters(masterFieldList);

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;
            expression.GettingAggregationValues += OnFieldExpressionGettingAggregationValues;

            //evaluate
            Object returnValue = expression.Evaluate(this);

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;
            expression.GettingAggregationValues -= OnFieldExpressionGettingAggregationValues;

            return returnValue;
        }

        /// <summary>
        /// Event handler called by ObjectFieldExpression when resolving a single field.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            PlanogramItemPlacement targetPlacement = e.TargetContext as PlanogramItemPlacement;
            if (targetPlacement == null)
            {
                e.Result = null;
                return;
            }
            e.Result = targetPlacement.ResolveField(e.Parameter.FieldInfo);
        }

        /// <summary>
        /// Event handler called by ObjectFieldExpression when getting values to resolve an aggregation function.
        /// </summary>
        private void OnFieldExpressionGettingAggregationValues(object sender, ObjectFieldExpressionAggregationValuesArgs e)
        {
            PlanogramItemPlacement targetItem = (PlanogramItemPlacement)e.ExpressionTarget;

            if (String.IsNullOrWhiteSpace(e.Formula))
            {
                e.Values = new List<Object> { targetItem };
                return;
            }

            //TODO: We should be using the passed in field list here...
            var fields = ObjectFieldInfo.ExtractFieldsFromText(e.Formula, PlanogramFieldHelper.EnumerateAllFields());

            //create the new formula
            ObjectFieldExpression expression = new ObjectFieldExpression(e.Formula);
            fields.ForEach(f => expression.AddParameter(f));

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;
            expression.GettingAggregationValues += OnFieldExpressionGettingAggregationValues;


            //determine the data level
            PlanogramItemType aggregationLevel = PlanogramFieldHelper.GetFieldDataLevel(fields);

            //get items of the same type from the planogram.
            List<PlanogramItemPlacement> resolveItems = targetItem.GetPlanogramItemPlacementOfType(aggregationLevel);

            //resolve values for each item
            List<Object> values = new List<Object>(resolveItems.Count);
            foreach (PlanogramItemPlacement p in resolveItems)
            {
                values.Add(expression.Evaluate(p));
            }


            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;
            expression.GettingAggregationValues -= OnFieldExpressionGettingAggregationValues;

            e.Values = values;
        }

        #endregion

        #region Equals Override

        public override bool Equals(object obj)
        {
            PlanogramItemPlacement other = obj as PlanogramItemPlacement;
            if (other == null) return false;
            if (!AreIdsEqual(other.Planogram, this.Planogram)) return false;
            if (!AreIdsEqual(other.FixtureItem, this.FixtureItem)) return false;
            if (!AreIdsEqual(other.Fixture, this.Fixture)) return false;
            if (!AreIdsEqual(other.FixtureAssembly, this.FixtureAssembly)) return false;
            if (!AreIdsEqual(other.Assembly, this.Assembly)) return false;
            if (!AreIdsEqual(other.AssemblyComponent, this.AssemblyComponent)) return false;
            if (!AreIdsEqual(other.FixtureComponent, this.FixtureComponent)) return false;
            if (!AreIdsEqual(other.Component, this.Component)) return false;
            if (!AreIdsEqual(other.SubComponent, this.SubComponent)) return false;
            if (!AreIdsEqual(other.Product, this.Product)) return false;
            if (!AreIdsEqual(other.Position, this.Position)) return false;
            if (!AreIdsEqual(other.Annotation, this.Annotation)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return _hashCode;
        }

        private void GenerateHashCode()
        {
            _hashCode =
            HashCode.Start
            .Hash(this.Planogram != null ? this.Planogram.Id : null)
            .Hash(this.SubComponent != null ? this.SubComponent.Id : null)
            .Hash(this.Component != null ? this.Component.Id : null)
            .Hash(this.AssemblyComponent != null ? this.AssemblyComponent.Id : null)
            .Hash(this.Assembly != null ? this.Assembly.Id : null)
            .Hash(this.FixtureAssembly != null ? this.FixtureAssembly.Id : null)
            .Hash(this.Fixture != null ? this.Fixture.Id : null)
            .Hash(this.FixtureComponent != null ? this.FixtureComponent.Id : null)
            .Hash(this.FixtureItem != null ? this.FixtureItem.Id : null)
            .Hash(this.Product != null ? this.Product.Id : null)
            .Hash(this.Position != null ? this.Position.Id : null)
            .Hash(this.Annotation != null ? this.Annotation.Id : null);
        }

        /// <summary>
        /// Asserts if two object references are equal
        /// </summary>
        private static Boolean AreIdsEqual<T>(ModelObject<T> one, ModelObject<T> two)
            where T : ModelObject<T>
        {
            if (one == null && two != null) return false;
            if (one != null && two == null) return false;
            if (one != null && two != null && one.Id != two.Id) return false;
            return true;
        }

        #endregion

        #endregion

    }

    /// <summary>
    /// Denotes the different item types in a planogram,
    /// ordered by granularity.
    /// </summary>
    public enum PlanogramItemType
    {
        Annotation = 0,
        Position = 1,
        Product = 2,
        SubComponent = 3,
        Component = 4,
        Assembly = 5,
        Fixture = 6,
        Planogram = 7,
        Unknown = 8
    }
}
