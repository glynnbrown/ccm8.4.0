﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24971 : L.Ineson
//  Changes to structure.
// V8-25543 : L.Ineson
//  Changed all orientation type fields to use PlanogramPositionOrientationType
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-27058 : A.Probyn
//  Added new meta data properties & calculations
//  Added ClearMetadata
//  Added NewPlanogramPosition with no parameter unit test data creation to work.
// V8-27474 : A.Silva
//  Added PositionSequenceNumber property.
// V8-27570 : A.Silva
//  Amended to include PositionSequenceNumber to DisplayablePropertyInfos.
// V8-27742 : A.Silva
//  Added IsOutsideMerchandisingSpace, IsPegOverfilled,
//  IsMaxStackExceeded, IsMaxCapExceeded, IsMinDeepNotAchieved, IsMaxDeepExceeded, 
//  IsCannotBreakTrayBackIgnored, IsCannotBreakTrayTopIgnored calculations.
// V8-27926 : A.Silva
//  Amended IsMaxCapExceeded to warn if MaxCap = 0 and it is exceeded.
// V8-27924 : A.Silva
//  Added IsMaxTopCapExceeded, refactored IsMaxCapExeeded to IsMaxRightCapExceeded.
// V8-27823 : L.Ineson
//  Facings properties are now Int16
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
// V8-25976 : L.ineson
//  Added GetBrokenValidationWarnings method.
// V8-27780 : A.Kuszyk
//  Amended FillHigh to take into account Y block and fix bug related to single facings high.
// V8-27814 : A.Kuszyk
//  Re-factored FillHigh to account for nested products.
// V8-27959 : A.Silva
//  Amended IsOutsideMerchandisingSpace so that hanging positions are not outside if wider than the component,
//  but only if they are not hanging directly under it. (Can hang wide produts under a rod, but they need to be directly under it).
// V8-28521 : A.Kuszyk
//  Added MaxStack constraint to FillHigh.
// V8-28422 : L.Ineson
//  HasCollisions no longer returns true for positions in chests.

#endregion

#region Version History: CCM801

// V8-28767 : A.Kuszyk
//  Added cap increase and decreases to Increase/Decrease Facings methods. Ensured that all cap facings are set to zero
//  if high/wide/deep is set to zero.
// V8-28676 : A.Silva
//  Added overload for GetImageDataAsync that includes a call back to invoke when the image is finally retrieved.
// V8-28837 : A.Kuszyk
//  Added GetSequence and GetCoordinate helper methods.
// V8-28560 : A.Kuszyk
//  Updated FillHigh and FillDeep to check for MaxFillHigh and MaxFillDeep Product constraints.
// V8-28939 : A.Silva
//  Removed old unused overload for GetImageDataAsync.
// V8-28951 : A.Silva
//  Added check for null products to GetImageDataAsync as defensive coding.

#endregion

#region Version History: CCM802

//V8-29028 : L.Ineson
//  Added new squeeze properties.
// V8-29054 : M.Pettit
//  Added Meta validation properties
// CCM-28766 : J.Pickup
//  IsManuallyPlaced property introuced
// CCM-28816 : D.Pleasance
//  Amended HasCollisions, so that if the bounds of a position collides with a different position from an alternate component this is flagged.
// V8-29105 : A.Silva
//  Added SetSequence helper method to set the correct sequence value for the given axis.

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29034 : J.Pickup
//  Added a little tollerance to FillDeep and FillWide in order to account for a rounding issue in a float to double conversion.
// V8-29137 : A.Kuszyk
//  Added HasMatchingNormalSequences helper method.
// V8-28766 : N.Foster
//  Stubbed out autofill methods

#endregion

#region Version History: CCM810

// V8-28766 : J.Pickup
//  Introduced Increase/Decrease Units region with lots of logic, Includes handling of TrayProduct units - within reason.
//  Also helpers: getTrayDimensionValueForProductsFromPositionRotation(), TransformPositionIntoTray().
//  And RemoveAllCaps(), RemoveCapsX(), RemoveCapsY(), RemoveCapsZ().
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-28766 : J.Pickup
//  Introduced SetMinimumUnits() and UpdateBreakOutOfTrayCappingsOnTrayPositions().
//  Improvements to TransformPositionIntoTray(), and now caps also include other capping dimensions when Increase/Decrease Units() called.
// V8-28766 : J.Pickup
//  re-implemented IncreaseUnits() & DecreaseUnits().
// V8-29844 : L.Ineson
//  Added more metadata
// V8-28904 : M.Pettit
//  PegIsOverfilled warnings changed so only valid for positions on Hang-type subcomponents
// V8-29957 : J.Pickup
//  Increase units z now checks if the position is placed as a pegged position, and takes into account the merchandisable depth.
// V8-30011 : L.Ineson
//  Added addition linear, area and volumetric metadata.
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-30071 : M.Brumby
//  fixed MetaAchievedCases to caclulate as a Single and save as 2d.p.
// V8-29391 : M.Shelley
//  Modified the IsMerchStyleSettingsInvalid method so that if the selected merchandising style is "Unit" and
//  the product display dimensions are 0, then a validation warning is given.
// V8-30108 : J.Pickup
//  InreaseUnitsX&Y now has restrictions on hangfrombottom merch types.
// V8-30100 : M.Brumby
//  Check caps when determining if we can decrease units on an axis.
// V8-30110 : L.Ineson
//  Corrected orientation type field resolve.
// V8-30034 : A.Silva
//  Refactored Increase/Decrease Units to allow for each component deciding the base axes order.
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30146 : A.Silva
//  Amended IncreaseUnitsX, IncreaseUnitsY and IncreaseUnitsZ so that invalid values for Tray Size do not cause exceptions and incorrect behavior.
//  Refactored some of the methods around increase/decrease units.
// V8-30198 : L.Ineson
//  Corrected some rounding issues.
//  Made sure that top cap wide gets set based on facings wide when filling high.
// V8-30181 : L.Ineson
//  Stopped top caps being placed on hang and hang below merch types.
// V8-30118 : A.Silva
//  Amended change to Tray instead of Default as Merchandising Style for the position 
//      when Increase/Decrease Units is called and a tray needs to be breaken down or re-formed.
//  Amended Increase for trays that would make them become a single unit when increasing only in one axis.

#endregion

#region Version History : CCM811

// V8-30406 : A.Silva
//  Amended Increase/Decrease Cap Facing Units so that caps that cannot be placed do not get increased to max cap value.
// V8-30398 : A.Silva
//  Amended MetaTotalFrontFacingsWide to have a value for ANY FRONT orientation (not just non rotated).
// V8-30586 : A.Silva
//  Tidied up temp class.

#endregion

#region Version History : CCM 820
// V8-31004 : D.Pleasance
//	Added interface implementation, IPlanogramSubComponentSequencedItem.
// V8-31164 : D.Pleasance
//  Added SequenceColour \ SequenceNumber
// V8-31269 : L.Ineson
//  Added method for image synchronous fetch.
// V8-31471 : L.Ineson
//  Added check to make sure that x caps aren't placed on hanging components.
// V8-31544 : D.Pleasance, A.Silva
//  Amended IncreaseUnitsX, IncreaseUnitsY, so that when the first cap is added correct squeeze values are applied to determine if a cap would be bigger than the main position facing
#endregion

#region Version History : CCM 830
// V8-31672 : L.Ineson
//  Amend caps methods no longer clean up for 0 values as this breaks autofill. Added new method to do this instead.
// V8-31698 : A.Heathcote
//  Removed the following, in order to remove them from Labels and Datasheets:  
//                      yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitsDeepProperty, detailsGroup);
//                      yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitsHighProperty, detailsGroup);
//                      yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitsWideProperty, detailsGroup);
// V8-31560 : D.Pleasance
//  Amended DecreaseUnits overload that checks all axis directions for decrease. Previous was only ever checking one direction and always returning true even if a decrease couldnt happen.
//V8-31828 : L.Ineson
//  Added methods for squeeze logic
//  IncreaseUnitsY is not longer prevented from adding tops caps if the max stack has been reached.
// V8-31804 : A.Kuszyk
//  Added GetOptimisationClone().
// V8-31907 : L.Ineson
//  Amended Increase & Decrease X,Y and Z methods to consider nested unit sizes.
//  Amended the update caps methods to allow the recalc to be forced.
//  Renamed update caps methods to better reflect what they actually do.
// V8-31906 : L.Ineson
//  Stopped trays being decreased to 0 facings high by autofill.
// V8-31915 : L.Ineson
//  SetMinimumUnits will now apply min deep if over 0.
// V8-31948 : A.Kuszyk
//  Amended DecreaseUnits to decrease in the reverese axis order of IncreaseUnits.
// V8-31957 : L.Ineson
//  A couple more auto fill squeezing caps corrections
//V8-31619 : L.Ineson
//  Added new overload for recalculate units and made sure that it is always called by metadata.
// V8-31804 : A.Kuszyk
//  Correct incorrect inequality sign in IncreaseUnitsY related to capping.
// V8-31884 : M.Brumby
//  Made IsMaxStackExeeded and IsMaxDeepExceeded internally available
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32562 : L.Ineson
//  Positions are no longer flagged as outside of merch space if the merch space has a 0 dimension.
// V8-31626 : A.Heathcote
//  Made improvments to IsOutsideOfBlockSpace (mostly re-written) and included a percentageSpaceAllowence (See Summery)
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName.
// V8-32787 : A.Silva
//  Added Assortment rule enforcement to Increase/Decrease/Set units.
// V8-32953 : L.Ineson
// GetImage methods now accept a userstate parameter and output the actual type of image returned.
// V8-32962 : A.Kuszyk
//  Changed CSLA properties to use ReadProperty instead of GetProperty, as this bypasses
//  property level security (which we don't use) and yields a performance increase. Also implemented
//  a cache for PlanogramPositionDetails to improve performance.
// CCM-13761 : L.Ineson
//  Added GetBoundaryInfo for improved collision detection.
// CCM-13565 : L.Ineson
//  EnumerateComponent collisions now checks against subcomponent rather than component bounds.
#endregion

#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising.Tray;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramPosition object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPosition : ModelObject<PlanogramPosition>, IPlanogramSubComponentSequencedItem
    {
        #region Constants
        const String AppliedMerchandisingStyleFieldName = "AppliedMerchandisingStyle";
        const String AppliedMerchandisingStyleXFieldName = "AppliedMerchandisingStyleX";
        const String AppliedMerchandisingStyleYFieldName = "AppliedMerchandisingStyleY";
        const String AppliedMerchandisingStyleZFieldName = "AppliedMerchandisingStyleZ";
        const String AppliedOrientationTypeFieldName = "AppliedOrientationType";
        const String AppliedOrientationTypeXFieldName = "AppliedOrientationTypeX";
        const String AppliedOrientationTypeYFieldName = "AppliedOrientationTypeY";
        const String AppliedOrientationTypeZFieldName = "AppliedOrientationTypeZ";

        /// <summary>
        /// percentageSpaceAllowence is used in the caluculation of IsOutsideOfBlockSpace,
        /// and represents the percentage of the position that has to be in blockspace for it to be considered
        /// "In Block Space". 
        /// So as it stands (0.5) it has to be more than half outside of blockspace to be considered Outside Of BlockSpace.
        /// 50% (0.5) was decided by Steve Richards.
        /// </summary>
        private const Double percentageSpaceAllowence = 0.5;
        #endregion

        #region Fields
        private Object _positionDetailsLock = new Object(); // object used for locking
        private PlanogramPositionDetails _positionDetails; // caches the position details
        #endregion

        #region Constructors
        static PlanogramPosition()
        {
            FriendlyName = Message.PlanogramPosition_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return base.Parent == null ? null : ((PlanogramPositionList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region PlanogramFixtureItemId
        /// <summary>
        /// PlanogramFixtureId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureItemIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureItemId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureItemId
        /// </summary>
        public Object PlanogramFixtureItemId
        {
            get { return this.ReadProperty<Object>(PlanogramFixtureItemIdProperty); }
            set { this.SetProperty<Object>(PlanogramFixtureItemIdProperty, value); }
        }
        #endregion

        #region PlanogramFixtureAssemblyId
        /// <summary>
        /// PlanogramFixtureAssemblyId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureAssemblyIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureAssemblyId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureAssemblyId
        /// </summary>
        public Object PlanogramFixtureAssemblyId
        {
            get { return this.ReadProperty<Object>(PlanogramFixtureAssemblyIdProperty); }
            set { this.SetProperty<Object>(PlanogramFixtureAssemblyIdProperty, value); }
        }
        #endregion

        #region PlanogramAssemblyComponentId
        /// <summary>
        /// PlanogramAssemblyComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramAssemblyComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramAssemblyComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramAssemblyComponentId
        /// </summary>
        public Object PlanogramAssemblyComponentId
        {
            get { return ReadProperty<Object>(PlanogramAssemblyComponentIdProperty); }
            set { SetProperty<Object>(PlanogramAssemblyComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramFixtureComponentId
        /// <summary>
        /// PlanogramFixtureComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureComponentId
        /// </summary>
        public Object PlanogramFixtureComponentId
        {
            get { return ReadProperty<Object>(PlanogramFixtureComponentIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramSubComponentId
        /// <summary>
        /// PlanogramSubComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramSubComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramSubComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramSubComponentId
        /// </summary>
        public Object PlanogramSubComponentId
        {
            get { return ReadProperty<Object>(PlanogramSubComponentIdProperty); }
            set { SetProperty<Object>(PlanogramSubComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramProductId
        /// <summary>
        /// PlanogramProductId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramProductIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramProductId);
        /// <summary>
        /// The PlanogramPosition PlanogramProductId
        /// </summary>
        public Object PlanogramProductId
        {
            get { return this.ReadProperty<Object>(PlanogramProductIdProperty); }
            set { this.SetProperty<Object>(PlanogramProductIdProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.Generic_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramPosition X
        /// </summary>
        public Single X
        {
            get { return ReadProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.Generic_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramPosition Y
        /// </summary>
        public Single Y
        {
            get { return ReadProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.Generic_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramPosition Z
        /// </summary>
        public Single Z
        {
            get { return ReadProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, Message.Generic_Slope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramPosition Slope
        /// </summary>
        public Single Slope
        {
            get { return ReadProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle, Message.Generic_Angle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramPosition Angle
        /// </summary>
        public Single Angle
        {
            get { return ReadProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, Message.Generic_Roll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramPosition Roll
        /// </summary>
        public Single Roll
        {
            get { return ReadProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region Main Block

        #region FacingsHigh
        /// <summary>
        /// FacingsHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsHighProperty =
            RegisterModelProperty<Int16>(c => c.FacingsHigh, Message.PlanogramPosition_FacingsHigh);
        /// <summary>
        /// Gets/Sets the FacingsHigh value
        /// </summary>
        public Int16 FacingsHigh
        {
            get { return ReadProperty<Int16>(FacingsHighProperty); }
            set { SetProperty<Int16>(FacingsHighProperty, value); }
        }
        #endregion

        #region FacingsWide
        /// <summary>
        /// FacingsWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsWideProperty =
            RegisterModelProperty<Int16>(c => c.FacingsWide, Message.PlanogramPosition_FacingsWide);
        /// <summary>
        /// Gets/Sets the FacingsWide value
        /// </summary>
        public Int16 FacingsWide
        {
            get { return ReadProperty<Int16>(FacingsWideProperty); }
            set { SetProperty<Int16>(FacingsWideProperty, value); }
        }
        #endregion

        #region FacingsDeep
        /// <summary>
        /// FacingsDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsDeepProperty =
            RegisterModelProperty<Int16>(c => c.FacingsDeep, Message.PlanogramPosition_FacingsDeep);
        /// <summary>
        /// Gets/Sets the FacingsDeep value
        /// </summary>
        public Int16 FacingsDeep
        {
            get { return ReadProperty<Int16>(FacingsDeepProperty); }
            set { SetProperty<Int16>(FacingsDeepProperty, value); }
        }
        #endregion

        #region OrientationType

        public static readonly ModelPropertyInfo<PlanogramPositionOrientationType> OrientationTypeProperty =
            RegisterModelProperty<PlanogramPositionOrientationType>(c => c.OrientationType, Message.PlanogramProduct_OrientationType);
        /// <summary>
        /// Gets/Sets the orientation type of this position.
        /// </summary>
        public PlanogramPositionOrientationType OrientationType
        {
            get { return ReadProperty<PlanogramPositionOrientationType>(OrientationTypeProperty); }
            set { SetProperty<PlanogramPositionOrientationType>(OrientationTypeProperty, value); }
        }

        #endregion

        #region MerchandisingStyle

        public static readonly ModelPropertyInfo<PlanogramPositionMerchandisingStyle> MerchandisingStyleProperty =
            RegisterModelProperty<PlanogramPositionMerchandisingStyle>(c => c.MerchandisingStyle, Message.PlanogramProduct_MerchandisingStyle);
        /// <summary>
        /// The merchandising style to be used for this position
        /// </summary>
        public PlanogramPositionMerchandisingStyle MerchandisingStyle
        {
            get { return ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleProperty); }
            set { SetProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleProperty, value); }
        }

        #endregion

        #region VerticalSqueeze

        /// <summary>
        /// VerticalSqueeze property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> VerticalSqueezeProperty =
            RegisterModelProperty<Single>(o => o.VerticalSqueeze, Message.PlanogramPosition_VerticalSqueeze, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the main block facings height
        /// </summary>
        public Single VerticalSqueeze
        {
            get { return this.ReadProperty<Single>(VerticalSqueezeProperty); }
            set { this.SetProperty<Single>(VerticalSqueezeProperty, value); }
        }

        #endregion

        #region HorizontalSqueeze

        /// <summary>
        /// HorizontalSqueeze property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HorizontalSqueezeProperty =
           RegisterModelProperty<Single>(o => o.HorizontalSqueeze, Message.PlanogramPosition_HorizontalSqueeze, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the main block facings width
        /// </summary>
        public Single HorizontalSqueeze
        {
            get { return this.ReadProperty<Single>(HorizontalSqueezeProperty); }
            set { this.SetProperty<Single>(HorizontalSqueezeProperty, value); }
        }

        #endregion

        #region DepthSqueeze

        /// <summary>
        /// DepthSqueeze property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthSqueezeProperty =
           RegisterModelProperty<Single>(o => o.DepthSqueeze, Message.PlanogramPosition_DepthSqueeze, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the main block facings depth
        /// </summary>
        public Single DepthSqueeze
        {
            get { return this.ReadProperty<Single>(DepthSqueezeProperty); }
            set { this.SetProperty<Single>(DepthSqueezeProperty, value); }
        }

        #endregion

        #endregion

        #region X Block

        #region FacingsXHigh
        /// <summary>
        /// FacingsXHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsXHighProperty =
            RegisterModelProperty<Int16>(c => c.FacingsXHigh, Message.PlanogramPosition_FacingsXHigh);
        /// <summary>
        /// Gets/Sets the FacingsXHigh value
        /// </summary>
        public Int16 FacingsXHigh
        {
            get { return ReadProperty<Int16>(FacingsXHighProperty); }
            set { SetProperty<Int16>(FacingsXHighProperty, value); }
        }
        #endregion

        #region FacingsXWide
        /// <summary>
        /// FacingsXWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsXWideProperty =
            RegisterModelProperty<Int16>(c => c.FacingsXWide, Message.PlanogramPosition_FacingsXWide);
        /// <summary>
        /// Gets/Sets the FacingsXWide value
        /// </summary>
        public Int16 FacingsXWide
        {
            get { return ReadProperty<Int16>(FacingsXWideProperty); }
            set { SetProperty<Int16>(FacingsXWideProperty, value); }
        }
        #endregion

        #region FacingsXDeep
        /// <summary>
        /// FacingsXDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsXDeepProperty =
            RegisterModelProperty<Int16>(c => c.FacingsXDeep, Message.PlanogramPosition_FacingsXDeep);
        /// <summary>
        /// Gets/Sets the FacingsXDeep value
        /// </summary>
        public Int16 FacingsXDeep
        {
            get { return ReadProperty<Int16>(FacingsXDeepProperty); }
            set { SetProperty<Int16>(FacingsXDeepProperty, value); }
        }
        #endregion

        #region MerchandisingStyleX
        /// <summary>
        /// MerchandisingStyleX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionMerchandisingStyle> MerchandisingStyleXProperty =
            RegisterModelProperty<PlanogramPositionMerchandisingStyle>(c => c.MerchandisingStyleX, Message.PlanogramPosition_MerchandisingStyleX);
        /// <summary>
        /// Gets/Sets the MerchandisingStyleX value
        /// </summary>
        public PlanogramPositionMerchandisingStyle MerchandisingStyleX
        {
            get { return ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleXProperty); }
            set { SetProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleXProperty, value); }
        }
        #endregion

        #region OrientationTypeX
        /// <summary>
        /// OrientationTypeX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionOrientationType> OrientationTypeXProperty =
            RegisterModelProperty<PlanogramPositionOrientationType>(c => c.OrientationTypeX, Message.PlanogramPosition_OrientationTypeX);
        /// <summary>
        /// Gets/Sets the OrientationTypeX value
        /// </summary>
        public PlanogramPositionOrientationType OrientationTypeX
        {
            get { return ReadProperty<PlanogramPositionOrientationType>(OrientationTypeXProperty); }
            set { SetProperty<PlanogramPositionOrientationType>(OrientationTypeXProperty, value); }
        }
        #endregion

        #region IsXPlacedLeft
        /// <summary>
        /// PlacementTypeX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsXPlacedLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsXPlacedLeft, Message.PlanogramPosition_IsXPlacedLeft);
        /// <summary>
        /// Gets/Sets the PlacementTypeX value
        /// </summary>
        public Boolean IsXPlacedLeft
        {
            get { return ReadProperty<Boolean>(IsXPlacedLeftProperty); }
            set { SetProperty<Boolean>(IsXPlacedLeftProperty, value); }
        }
        #endregion

        #region VerticalSqueezeX

        /// <summary>
        /// VerticalSqueezeX property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> VerticalSqueezeXProperty =
            RegisterModelProperty<Single>(o => o.VerticalSqueezeX, Message.PlanogramPosition_VerticalSqueezeX, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the X block facings height
        /// </summary>
        public Single VerticalSqueezeX
        {
            get { return this.ReadProperty<Single>(VerticalSqueezeXProperty); }
            set { this.SetProperty<Single>(VerticalSqueezeXProperty, value); }
        }

        #endregion

        #region HorizontalSqueezeX

        /// <summary>
        /// HorizontalSqueezeX property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HorizontalSqueezeXProperty =
           RegisterModelProperty<Single>(o => o.HorizontalSqueezeX, Message.PlanogramPosition_HorizontalSqueezeX, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the X block facings width
        /// </summary>
        public Single HorizontalSqueezeX
        {
            get { return this.ReadProperty<Single>(HorizontalSqueezeXProperty); }
            set { this.SetProperty<Single>(HorizontalSqueezeXProperty, value); }
        }

        #endregion

        #region DepthSqueezeX

        /// <summary>
        /// DepthSqueezeX property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthSqueezeXProperty =
           RegisterModelProperty<Single>(o => o.DepthSqueezeX, Message.PlanogramPosition_DepthSqueezeX, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the X block facings depth
        /// </summary>
        public Single DepthSqueezeX
        {
            get { return this.ReadProperty<Single>(DepthSqueezeXProperty); }
            set { this.SetProperty<Single>(DepthSqueezeXProperty, value); }
        }

        #endregion

        #endregion

        #region Y Block

        #region FacingsYHigh
        /// <summary>
        /// FacingsYHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsYHighProperty =
            RegisterModelProperty<Int16>(c => c.FacingsYHigh, Message.PlanogramPosition_FacingsYHigh);
        /// <summary>
        /// Gets/Sets the FacingsYHigh value
        /// </summary>
        public Int16 FacingsYHigh
        {
            get { return ReadProperty<Int16>(FacingsYHighProperty); }
            set { SetProperty<Int16>(FacingsYHighProperty, value); }
        }
        #endregion

        #region FacingsYWide
        /// <summary>
        /// FacingsYWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsYWideProperty =
            RegisterModelProperty<Int16>(c => c.FacingsYWide, Message.PlanogramPosition_FacingsYWide);
        /// <summary>
        /// Gets/Sets the FacingsYWide value
        /// </summary>
        public Int16 FacingsYWide
        {
            get { return ReadProperty<Int16>(FacingsYWideProperty); }
            set { SetProperty<Int16>(FacingsYWideProperty, value); }
        }
        #endregion

        #region FacingsYDeep
        /// <summary>
        /// FacingsYDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsYDeepProperty =
            RegisterModelProperty<Int16>(c => c.FacingsYDeep, Message.PlanogramPosition_FacingsYDeep);
        /// <summary>
        /// Gets/Sets the FacingsYDeep value
        /// </summary>
        public Int16 FacingsYDeep
        {
            get { return ReadProperty<Int16>(FacingsYDeepProperty); }
            set { SetProperty<Int16>(FacingsYDeepProperty, value); }
        }
        #endregion

        #region MerchandisingStyleY
        /// <summary>
        /// MerchandisingStyleY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionMerchandisingStyle> MerchandisingStyleYProperty =
            RegisterModelProperty<PlanogramPositionMerchandisingStyle>(c => c.MerchandisingStyleY, Message.PlanogramPosition_MerchandisingStyleY);
        /// <summary>
        /// Gets/Sets the MerchandisingStyleY value
        /// </summary>
        public PlanogramPositionMerchandisingStyle MerchandisingStyleY
        {
            get { return ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleYProperty); }
            set { SetProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleYProperty, value); }
        }
        #endregion

        #region OrientationTypeY
        /// <summary>
        /// OrientationTypeY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionOrientationType> OrientationTypeYProperty =
            RegisterModelProperty<PlanogramPositionOrientationType>(c => c.OrientationTypeY, Message.PlanogramPosition_OrientationTypeY);
        /// <summary>
        /// Gets/Sets the OrientationTypeY value
        /// </summary>
        public PlanogramPositionOrientationType OrientationTypeY
        {
            get { return ReadProperty<PlanogramPositionOrientationType>(OrientationTypeYProperty); }
            set { SetProperty<PlanogramPositionOrientationType>(OrientationTypeYProperty, value); }
        }
        #endregion

        #region IsYPlacedBottom
        /// <summary>
        /// PlacementTypeY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsYPlacedBottomProperty =
            RegisterModelProperty<Boolean>(c => c.IsYPlacedBottom, Message.PlanogramPosition_IsYPlacedBottom);
        /// <summary>
        /// Gets/Sets the PlacementTypeY value
        /// </summary>
        public Boolean IsYPlacedBottom
        {
            get { return ReadProperty<Boolean>(IsYPlacedBottomProperty); }
            set { SetProperty<Boolean>(IsYPlacedBottomProperty, value); }
        }
        #endregion

        #region VerticalSqueezeY

        /// <summary>
        /// VerticalSqueezeY property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> VerticalSqueezeYProperty =
            RegisterModelProperty<Single>(o => o.VerticalSqueezeY, Message.PlanogramPosition_VerticalSqueezeY, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the main block facings height
        /// </summary>
        public Single VerticalSqueezeY
        {
            get { return this.ReadProperty<Single>(VerticalSqueezeYProperty); }
            set { this.SetProperty<Single>(VerticalSqueezeYProperty, value); }
        }

        #endregion

        #region HorizontalSqueezeY

        /// <summary>
        /// HorizontalSqueezeY property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HorizontalSqueezeYProperty =
           RegisterModelProperty<Single>(o => o.HorizontalSqueezeY, Message.PlanogramPosition_HorizontalSqueezeY, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the Y block facings width
        /// </summary>
        public Single HorizontalSqueezeY
        {
            get { return this.ReadProperty<Single>(HorizontalSqueezeYProperty); }
            set { this.SetProperty<Single>(HorizontalSqueezeYProperty, value); }
        }

        #endregion

        #region DepthSqueezeY

        /// <summary>
        /// DepthSqueezeY property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthSqueezeYProperty =
           RegisterModelProperty<Single>(o => o.DepthSqueezeY, Message.PlanogramPosition_DepthSqueezeY, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the Y block facings depth
        /// </summary>
        public Single DepthSqueezeY
        {
            get { return this.ReadProperty<Single>(DepthSqueezeYProperty); }
            set { this.SetProperty<Single>(DepthSqueezeYProperty, value); }
        }

        #endregion

        #endregion

        #region Z Block

        #region FacingsZHigh
        /// <summary>
        /// FacingsZHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsZHighProperty =
            RegisterModelProperty<Int16>(c => c.FacingsZHigh, Message.PlanogramPosition_FacingsZHigh);
        /// <summary>
        /// Gets/Sets the FacingsZHigh value
        /// </summary>
        public Int16 FacingsZHigh
        {
            get { return ReadProperty<Int16>(FacingsZHighProperty); }
            set { SetProperty<Int16>(FacingsZHighProperty, value); }
        }
        #endregion

        #region FacingsZWide
        /// <summary>
        /// FacingsZWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsZWideProperty =
            RegisterModelProperty<Int16>(c => c.FacingsZWide, Message.PlanogramPosition_FacingsZWide);
        /// <summary>
        /// Gets/Sets the FacingsZWide value
        /// </summary>
        public Int16 FacingsZWide
        {
            get { return ReadProperty<Int16>(FacingsZWideProperty); }
            set { SetProperty<Int16>(FacingsZWideProperty, value); }
        }
        #endregion

        #region FacingsZDeep
        /// <summary>
        /// FacingsZDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> FacingsZDeepProperty =
            RegisterModelProperty<Int16>(c => c.FacingsZDeep, Message.PlanogramPosition_FacingsZDeep);
        /// <summary>
        /// Gets/Sets the FacingsZDeep value
        /// </summary>
        public Int16 FacingsZDeep
        {
            get { return ReadProperty<Int16>(FacingsZDeepProperty); }
            set { SetProperty<Int16>(FacingsZDeepProperty, value); }
        }
        #endregion

        #region MerchandisingStyleZ
        /// <summary>
        /// MerchandisingStyleZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionMerchandisingStyle> MerchandisingStyleZProperty =
            RegisterModelProperty<PlanogramPositionMerchandisingStyle>(c => c.MerchandisingStyleZ, Message.PlanogramPosition_MerchandisingStyleZ);
        /// <summary>
        /// Gets/Sets the MerchandisingStyleZ value
        /// </summary>
        public PlanogramPositionMerchandisingStyle MerchandisingStyleZ
        {
            get { return ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleZProperty); }
            set { SetProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleZProperty, value); }
        }
        #endregion

        #region OrientationTypeZ
        /// <summary>
        /// OrientationTypeZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionOrientationType> OrientationTypeZProperty =
            RegisterModelProperty<PlanogramPositionOrientationType>(c => c.OrientationTypeZ, Message.PlanogramPosition_OrientationTypeZ);
        /// <summary>
        /// Gets/Sets the OrientationTypeZ value
        /// </summary>
        public PlanogramPositionOrientationType OrientationTypeZ
        {
            get { return ReadProperty<PlanogramPositionOrientationType>(OrientationTypeZProperty); }
            set { SetProperty<PlanogramPositionOrientationType>(OrientationTypeZProperty, value); }
        }
        #endregion

        #region IsZPlacedFront
        /// <summary>
        /// PlacementTypeZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsZPlacedFrontProperty =
            RegisterModelProperty<Boolean>(c => c.IsZPlacedFront, Message.PlanogramPosition_IsZPlacedFront);
        /// <summary>
        /// Gets/Sets the PlacementTypeZ value
        /// </summary>
        public Boolean IsZPlacedFront
        {
            get { return ReadProperty<Boolean>(IsZPlacedFrontProperty); }
            set { SetProperty<Boolean>(IsZPlacedFrontProperty, value); }
        }
        #endregion

        #region VerticalSqueezeZ

        /// <summary>
        /// VerticalSqueezeZ property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> VerticalSqueezeZProperty =
            RegisterModelProperty<Single>(o => o.VerticalSqueezeZ, Message.PlanogramPosition_VerticalSqueezeZ, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the Z block facings height
        /// </summary>
        public Single VerticalSqueezeZ
        {
            get { return this.ReadProperty<Single>(VerticalSqueezeZProperty); }
            set { this.SetProperty<Single>(VerticalSqueezeZProperty, value); }
        }

        #endregion

        #region HorizontalSqueezeZ

        /// <summary>
        /// HorizontalSqueezeZ property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HorizontalSqueezeZProperty =
           RegisterModelProperty<Single>(o => o.HorizontalSqueezeZ, Message.PlanogramPosition_HorizontalSqueezeZ, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the Z block facings width
        /// </summary>
        public Single HorizontalSqueezeZ
        {
            get { return this.ReadProperty<Single>(HorizontalSqueezeZProperty); }
            set { this.SetProperty<Single>(HorizontalSqueezeZProperty, value); }
        }

        #endregion

        #region DepthSqueezeZ

        /// <summary>
        /// DepthSqueezeZ property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthSqueezeZProperty =
           RegisterModelProperty<Single>(o => o.DepthSqueezeZ, Message.PlanogramPosition_DepthSqueezeZ, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Get/Sets the squeeze that should be applied to the Z block facings depth
        /// </summary>
        public Single DepthSqueezeZ
        {
            get { return this.ReadProperty<Single>(DepthSqueezeZProperty); }
            set { this.SetProperty<Single>(DepthSqueezeZProperty, value); }
        }

        #endregion

        #endregion

        #region Sequence

        #region Sequence
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>.
        /// This property is used in conjunction with <see cref="SequenceX"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> SequenceProperty =
            RegisterModelProperty<Int16>(c => c.Sequence);
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>.
        /// This property is used in conjunction with <see cref="SequenceX"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public Int16 Sequence
        {
            get { return ReadProperty<Int16>(SequenceProperty); }
            set { SetProperty<Int16>(SequenceProperty, value); }
        }
        #endregion

        #region SequenceX
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the X direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> SequenceXProperty =
            RegisterModelProperty<Int16>(c => c.SequenceX);
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the X direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public Int16 SequenceX
        {
            get { return ReadProperty<Int16>(SequenceXProperty); }
            set { SetProperty<Int16>(SequenceXProperty, value); }
        }
        #endregion

        #region SequenceY
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the Y direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceX"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> SequenceYProperty =
            RegisterModelProperty<Int16>(c => c.SequenceY);
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the Y direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceX"/> and <see cref="SequenceZ"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public Int16 SequenceY
        {
            get { return ReadProperty<Int16>(SequenceYProperty); }
            set { SetProperty<Int16>(SequenceYProperty, value); }
        }
        #endregion

        #region SequenceZ
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the Z direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceX"/> and <see cref="SequenceY"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> SequenceZProperty =
            RegisterModelProperty<Int16>(c => c.SequenceZ);
        /// <summary>
        /// The order that this <see cref="PlanogramPosition"/> is layed out in by <see cref="PlanogramMerchandisingGroup"/>, in the Z direction.
        /// This property is used in conjunction with <see cref="Sequence"/>, <see cref="SequenceX"/> and <see cref="SequenceY"/>.
        /// Note that this property is not related to <see cref="PlanogramSequence"/> and has nothing to do with
        /// automation or optimisation sequence.
        /// </summary>
        public Int16 SequenceZ
        {
            get { return ReadProperty<Int16>(SequenceZProperty); }
            set { SetProperty<Int16>(SequenceZProperty, value); }
        }
        #endregion

        #endregion

        #region Units

        #region UnitsHigh
        /// <summary>
        /// UnitsHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> UnitsHighProperty =
            RegisterModelProperty<Int16>(c => c.UnitsHigh, Message.PlanogramPosition_UnitsHigh);
        /// <summary>
        /// The PlanogramPosition UnitsHigh
        /// </summary>
        public Int16 UnitsHigh
        {
            get { return ReadProperty<Int16>(UnitsHighProperty); }
            set { SetProperty<Int16>(UnitsHighProperty, value); }
        }
        #endregion

        #region UnitsWide
        /// <summary>
        /// UnitsWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> UnitsWideProperty =
            RegisterModelProperty<Int16>(c => c.UnitsWide, Message.PlanogramPosition_UnitsWide);
        /// <summary>
        /// The PlanogramPosition UnitsWide
        /// </summary>
        public Int16 UnitsWide
        {
            get { return ReadProperty<Int16>(UnitsWideProperty); }
            set { SetProperty<Int16>(UnitsWideProperty, value); }
        }
        #endregion

        #region UnitsDeep
        /// <summary>
        /// UnitsDeep property
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> UnitsDeepProperty =
            RegisterModelProperty<Int16>(c => c.UnitsDeep, Message.PlanogramPosition_UnitsDeep);
        /// <summary>
        /// The PlanogramPosition UnitsDeep
        /// </summary>
        public Int16 UnitsDeep
        {
            get { return ReadProperty<Int16>(UnitsDeepProperty); }
            set { SetProperty<Int16>(UnitsDeepProperty, value); }
        }
        #endregion

        #region TotalUnits
        /// <summary>
        /// TotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TotalUnitsProperty =
            RegisterModelProperty<Int32>(c => c.TotalUnits, Message.PlanogramPosition_TotalUnits);
        public Int32 TotalUnits
        {
            get { return ReadProperty<Int32>(TotalUnitsProperty); }
            set
            {
                SetProperty<Int32>(TotalUnitsProperty, value);

                //tell any linked performance data to update.
                PlanogramPerformanceData perfData = GetPlanogramPerformanceData();
                if (perfData != null) perfData.UpdateInventoryValues();
            }
        }
        #endregion

        #endregion

        #region Meta Data Properties

        #region MetaAchievedCases
        /// <summary>
        /// MetaAchievedCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAchievedCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAchievedCases, Message.PlanogramPosition_MetaAchievedCases);
        /// <summary>
        /// Gets/Sets the meta data achieved cases
        /// </summary>
        public Single? MetaAchievedCases
        {
            get { return this.ReadProperty<Single?>(MetaAchievedCasesProperty); }
            set { this.SetProperty<Single?>(MetaAchievedCasesProperty, value); }
        }
        #endregion

        #region MetaIsPositionCollisions
        /// <summary>
        /// MetaIsPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsPositionCollisionsProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsPositionCollisions, Message.PlanogramPosition_MetaIsPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data is positions collisions
        /// </summary>
        public Boolean? MetaIsPositionCollisions
        {
            get { return this.ReadProperty<Boolean?>(MetaIsPositionCollisionsProperty); }
            set { this.SetProperty<Boolean?>(MetaIsPositionCollisionsProperty, value); }
        }
        #endregion

        #region MetaFrontFacingsWide
        /// <summary>
        /// MetaFrontFacingsWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaFrontFacingsWideProperty =
            RegisterModelProperty<Int16?>(c => c.MetaFrontFacingsWide, Message.PlanogramPosition_MetaFrontFacingsWide);
        /// <summary>
        /// Gets/Sets the meta data MetaFrontFacingsWide
        /// </summary>
        public Int16? MetaFrontFacingsWide
        {
            get { return this.ReadProperty<Int16?>(MetaFrontFacingsWideProperty); }
            set { this.SetProperty<Int16?>(MetaFrontFacingsWideProperty, value); }
        }
        #endregion

        #region MetaIsUnderMinDeep
        /// <summary>
        /// MetaIsUnderMinDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsUnderMinDeepProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsUnderMinDeep, Message.PlanogramPosition_MetaIsUnderMinDeep);
        /// <summary>
        /// Gets/Sets the meta data MetaIsUnderMinDeep
        /// </summary>
        public Boolean? MetaIsUnderMinDeep
        {
            get { return this.ReadProperty<Boolean?>(MetaIsUnderMinDeepProperty); }
            set { this.SetProperty<Boolean?>(MetaIsUnderMinDeepProperty, value); }
        }
        #endregion

        #region MetaIsOverMaxDeep
        /// <summary>
        /// MetaIsOverMaxDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMaxDeepProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMaxDeep, Message.PlanogramPosition_MetaIsOverMaxDeep);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMaxDeep
        /// </summary>
        public Boolean? MetaIsOverMaxDeep
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOverMaxDeepProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMaxDeepProperty, value); }
        }
        #endregion

        #region MetaIsOverMaxRightCap
        /// <summary>
        /// MetaIsOverMaxRightCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMaxRightCapProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMaxRightCap, Message.PlanogramPosition_MetaIsOverMaxRightCap);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMaxRightCap
        /// </summary>
        public Boolean? MetaIsOverMaxRightCap
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOverMaxRightCapProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMaxRightCapProperty, value); }
        }
        #endregion

        #region MetaIsOverMaxStack
        /// <summary>
        /// MetaIsOverMaxStack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMaxStackProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMaxStack, Message.PlanogramPosition_MetaIsOverMaxStack);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMaxStack
        /// </summary>
        public Boolean? MetaIsOverMaxStack
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOverMaxStackProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMaxStackProperty, value); }
        }
        #endregion

        #region MetaIsOverMaxTopCap
        /// <summary>
        /// MetaIsOverMaxTopCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverMaxTopCapProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverMaxTopCap, Message.PlanogramPosition_MetaIsOverMaxTopCap);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOverMaxTopCap
        /// </summary>
        public Boolean? MetaIsOverMaxTopCap
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOverMaxTopCapProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverMaxTopCapProperty, value); }
        }
        #endregion

        #region MetaIsInvalidMerchandisingStyle
        /// <summary>
        /// MetaIsInvalidMerchandisingStyle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsInvalidMerchandisingStyleProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsInvalidMerchandisingStyle, Message.PlanogramPosition_MetaIsInvalidMerchandisingStyle);
        /// <summary>
        /// Gets/Sets the meta data MetaIsInvalidMerchandisingStyle
        /// </summary>
        public Boolean? MetaIsInvalidMerchandisingStyle
        {
            get { return this.ReadProperty<Boolean?>(MetaIsInvalidMerchandisingStyleProperty); }
            set { this.SetProperty<Boolean?>(MetaIsInvalidMerchandisingStyleProperty, value); }
        }
        #endregion

        #region MetaIsHangingTray
        /// <summary>
        /// MetaIsHangingTray property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsHangingTrayProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsHangingTray, Message.PlanogramPosition_MetaIsHangingTray);
        /// <summary>
        /// Gets/Sets the meta data MetaIsHangingTray
        /// </summary>
        public Boolean? MetaIsHangingTray
        {
            get { return this.ReadProperty<Boolean?>(MetaIsHangingTrayProperty); }
            set { this.SetProperty<Boolean?>(MetaIsHangingTrayProperty, value); }
        }
        #endregion

        #region MetaIsPegOverfilled
        /// <summary>
        /// MetaIsPegOverfilled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsPegOverfilledProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsPegOverfilled, Message.PlanogramPosition_MetaIsPegOverfilled);
        /// <summary>
        /// Gets/Sets the meta data MetaIsPegOverfilled
        /// </summary>
        public Boolean? MetaIsPegOverfilled
        {
            get { return this.ReadProperty<Boolean?>(MetaIsPegOverfilledProperty); }
            set { this.SetProperty<Boolean?>(MetaIsPegOverfilledProperty, value); }
        }
        #endregion

        #region MetaIsOutsideMerchandisingSpace
        /// <summary>
        /// MetaIsOutsideMerchandisingSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOutsideMerchandisingSpaceProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOutsideMerchandisingSpace, Message.PlanogramPosition_MetaIsOutsideMerchandisingSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOutsideMerchandisingSpace
        /// </summary>
        public Boolean? MetaIsOutsideMerchandisingSpace
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOutsideMerchandisingSpaceProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOutsideMerchandisingSpaceProperty, value); }
        }
        #endregion

        #region MetaIsOutsideOfBlockSpace
        /// <summary>
        /// MetaIsOutsideOfBlockSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOutsideOfBlockSpaceProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOutsideOfBlockSpace, Message.PlanogramPosition_MetaIsOutsideOfBlockSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaIsOutsideOfBlockSpace
        /// </summary>
        public Boolean? MetaIsOutsideOfBlockSpace
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOutsideOfBlockSpaceProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOutsideOfBlockSpaceProperty, value); }
        }
        #endregion

        #region MetaHeight
        /// <summary>
        /// MetaHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaHeightProperty =
            RegisterModelProperty<Single?>(c => c.MetaHeight, Message.PlanogramPosition_MetaHeight,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaHeight
        /// </summary>
        public Single? MetaHeight
        {
            get { return this.ReadProperty<Single?>(MetaHeightProperty); }
            set { this.SetProperty<Single?>(MetaHeightProperty, value); }
        }
        #endregion

        #region MetaWidth
        /// <summary>
        /// MetaWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWidthProperty =
            RegisterModelProperty<Single?>(c => c.MetaWidth, Message.PlanogramPosition_MetaWidth,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWidth
        /// </summary>
        public Single? MetaWidth
        {
            get { return this.ReadProperty<Single?>(MetaWidthProperty); }
            set { this.SetProperty<Single?>(MetaWidthProperty, value); }
        }
        #endregion

        #region MetaDepth
        /// <summary>
        /// MetaDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaDepthProperty =
            RegisterModelProperty<Single?>(c => c.MetaDepth, Message.PlanogramPosition_MetaDepth,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaDepth
        /// </summary>
        public Single? MetaDepth
        {
            get { return this.ReadProperty<Single?>(MetaDepthProperty); }
            set { this.SetProperty<Single?>(MetaDepthProperty, value); }
        }
        #endregion

        #region MetaWorldX
        /// <summary>
        /// MetaWorldX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldXProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldX, Message.PlanogramPosition_MetaWorldX,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldX
        /// </summary>
        public Single? MetaWorldX
        {
            get { return this.ReadProperty<Single?>(MetaWorldXProperty); }
            set { this.SetProperty<Single?>(MetaWorldXProperty, value); }
        }
        #endregion

        #region MetaWorldY
        /// <summary>
        /// MetaWorldY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldYProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldY, Message.PlanogramPosition_MetaWorldY,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldY
        /// </summary>
        public Single? MetaWorldY
        {
            get { return this.ReadProperty<Single?>(MetaWorldYProperty); }
            set { this.SetProperty<Single?>(MetaWorldYProperty, value); }
        }
        #endregion

        #region MetaWorldZ
        /// <summary>
        /// MetaWorldZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaWorldZProperty =
            RegisterModelProperty<Single?>(c => c.MetaWorldZ, Message.PlanogramPosition_MetaWorldZ,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaWorldZ
        /// </summary>
        public Single? MetaWorldZ
        {
            get { return this.ReadProperty<Single?>(MetaWorldZProperty); }
            set { this.SetProperty<Single?>(MetaWorldZProperty, value); }
        }
        #endregion

        #region MetaTotalLinearSpace
        /// <summary>
        /// MetaTotalLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearSpace, Message.PlanogramPosition_MetaTotalLinearSpace,
            ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalLinearSpace
        /// </summary>
        public Single? MetaTotalLinearSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaSpace
        /// <summary>
        /// MetaTotalAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaSpace, Message.PlanogramPosition_MetaTotalAreaSpace,
            ModelPropertyDisplayType.Area);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaSpace
        /// </summary>
        public Single? MetaTotalAreaSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricSpace
        /// <summary>
        /// MetaTotalVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricSpace, Message.PlanogramPosition_MetaTotalVolumetricSpace,
            ModelPropertyDisplayType.Volume);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalVolumetricSpace
        /// </summary>
        public Single? MetaTotalVolumetricSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaPlanogramLinearSpacePercentage
        /// <summary>
        /// MetaPlanogramLinearSpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPlanogramLinearSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramLinearSpacePercentage, Message.PlanogramPosition_MetaPlanogramLinearSpacePercentage,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPlanogramLinearSpacePercentage
        /// </summary>
        public Single? MetaPlanogramLinearSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty, value); }
        }
        #endregion

        #region MetaPlanogramAreaSpacePercentage
        /// <summary>
        /// MetaPlanogramAreaSpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPlanogramAreaSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramAreaSpacePercentage, Message.PlanogramPosition_MetaPlanogramAreaSpacePercentage,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPlanogramAreaSpacePercentage
        /// </summary>
        public Single? MetaPlanogramAreaSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty, value); }
        }
        #endregion

        #region MetaPlanogramVolumetricSpacePercentage
        /// <summary>
        /// MetaPlanogramVolumetricSpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPlanogramVolumetricSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramVolumetricSpacePercentage, Message.PlanogramPosition_MetaPlanogramVolumetricSpacePercentage,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPlanogramVolumetricSpacePercentage
        /// </summary>
        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty, value); }
        }
        #endregion

        #region MetaComparisonStatus

        /// <summary>
        ///     Registered <see cref="ModelPropertyInfo{Single}"/> for the <see cref="MetaComparisonStatus"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramItemComparisonStatusType> MetaComparisonStatusProperty =
            RegisterModelProperty<PlanogramItemComparisonStatusType>(c => c.MetaComparisonStatus, Message.PlanogramPosition_MetaComparisonStatus,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaPlanogramVolumetricSpacePercentage
        /// </summary>
        public PlanogramItemComparisonStatusType MetaComparisonStatus
        {
            get { return ReadProperty(MetaComparisonStatusProperty); }
            set { SetProperty(MetaComparisonStatusProperty, value); }
        }
        #endregion

        #region MetaSequenceSubGroupName

        /// <summary>
        /// The name of the <see cref="PlanogramSequenceGroupSubGroup"/> that this position is a member
        /// of, or null if it is not in a sub-group.
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetaSequenceSubGroupNameProperty = RegisterModelProperty<String>(
            c => c.MetaSequenceSubGroupName,
            Message.PlanogramPosition_MetaSequenceSubGroupName);
        /// <summary>
        /// The name of the <see cref="PlanogramSequenceGroupSubGroup"/> that this position is a member
        /// of, or null if it is not in a sub-group.
        /// </summary>
        public String MetaSequenceSubGroupName
        {
            get { return ReadProperty(MetaSequenceSubGroupNameProperty); }
            set { SetProperty(MetaSequenceSubGroupNameProperty, value); }
        }
        #endregion

        #region MetaSequenceGroupName

        /// <summary>
        /// The name of the <see cref="PlanogramSequenceGroup"/> that this position is a member
        /// of, or null if it is not in a sub-group.
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetaSequenceGroupNameProperty = RegisterModelProperty<String>(
            c => c.MetaSequenceGroupName,
            Message.PlanogramPosition_MetaSequenceGroupName);
        /// <summary>
        /// The name of the <see cref="PlanogramSequenceGroupGroup"/> that this position is a member
        /// of, or null if it is not in a sub-group.
        /// </summary>
        public String MetaSequenceGroupName
        {
            get { return ReadProperty(MetaSequenceGroupNameProperty); }
            set { SetProperty(MetaSequenceGroupNameProperty, value); }
        }
        #endregion

        #region MetaPegRowNumber
        /// <summary>
        /// MetaPegRowNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaPegRowNumberProperty =
            RegisterModelProperty<Int32?>(c => c.MetaPegRowNumber, Message.PlanogramPosition_MetaPegRowNumber);
        /// <summary>
        ///The row number that the position is snapped to starting from the top of the component.
        /// </summary>
        public Int32? MetaPegRowNumber
        {
            get { return ReadProperty(MetaPegRowNumberProperty); }
            set { SetProperty(MetaPegRowNumberProperty, value); }
        }
        #endregion

        #region MetaPegColumnNumber
        /// <summary>
        /// MetaPegColumnNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaPegColumnNumberProperty =
            RegisterModelProperty<Int32?>(c => c.MetaPegColumnNumber, Message.PlanogramPosition_MetaPegColumnNumber);
        /// <summary>
        ///The column number that the position is peg snapped to, starting from the left
        /// </summary>
        public Int32? MetaPegColumnNumber
        {
            get { return ReadProperty(MetaPegColumnNumberProperty); }
            set { SetProperty(MetaPegColumnNumberProperty, value); }
        }
        #endregion

        #endregion

        #region PositionSequenceNumber

        /// <summary>
        ///	Gets or sets the renumbering sequence number (order) for this instance. This sequence property
        ///	has nothing to do with merchandising layout (see <see cref="SequenceX"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>)
        ///	and nothing to do with automation or optimisation sequence (see <see cref="SequenceNumber"/> and <see cref="SequenceColour"/>).
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> PositionSequenceNumberProperty =
            RegisterModelProperty<Int16?>(o => o.PositionSequenceNumber, Message.PlanogramPosition_PositionSequenceNumber);

        /// <summary>
        ///	Gets or sets the renumbering sequence number (order) for this instance. This sequence property
        ///	has nothing to do with merchandising layout (see <see cref="SequenceX"/>, <see cref="SequenceY"/> and <see cref="SequenceZ"/>)
        ///	and nothing to do with automation or optimisation sequence (see <see cref="SequenceNumber"/> and <see cref="SequenceColour"/>).
        /// </summary>
        public Int16? PositionSequenceNumber
        {
            get { return this.ReadProperty<Int16?>(PositionSequenceNumberProperty); }
            set { this.SetProperty<Int16?>(PositionSequenceNumberProperty, value); }
        }

        #endregion

        #region IsManuallyPlaced

        /// <summary>
        ///		Metadata for the <see cref="IsManuallyPlaced"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsManuallyPlacedProperty =
            RegisterModelProperty<Boolean>(o => o.IsManuallyPlaced, Message.PlanogramPosition_IsManuallyPlaced);

        /// <summary>
        ///		Gets or sets wether the posistion is specified as having been a manually placed product
        /// </summary>
        public Boolean IsManuallyPlaced
        {
            get { return this.ReadProperty<Boolean>(IsManuallyPlacedProperty); }
            set { this.SetProperty<Boolean>(IsManuallyPlacedProperty, value); }
        }

        #endregion

        #region SequenceColour
        /// <summary>
        /// The colour of the <see cref="PlanogramSequenceGroup"/> that this <see cref="PlanogramPosition"/> is
        /// sequenced in. Its position in the <see cref="PlanogramSequenceGroup"/> is denoted by <see cref="SequenceNumber"/>.
        /// Note: this property has nothing to do with other sequence properties relating to merchandising layout and renumbering.
        /// This property refers specifically to <see cref="PlanogramSequence"/> and is used for automation and optimisation.
        ///</summary>
        public static readonly ModelPropertyInfo<Int32?> SequenceColourProperty =
            RegisterModelProperty<Int32?>(c => c.SequenceColour, Message.PlanogramPosition_SequenceColour,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The colour of the <see cref="PlanogramSequenceGroup"/> that this <see cref="PlanogramPosition"/> is
        /// sequenced in. Its position in the <see cref="PlanogramSequenceGroup"/> is denoted by <see cref="SequenceNumber"/>.
        /// Note: this property has nothing to do with other sequence properties relating to merchandising layout and renumbering.
        /// This property refers specifically to <see cref="PlanogramSequence"/> and is used for automation and optimisation.
        ///</summary>
        public Int32? SequenceColour
        {
            get { return ReadProperty<Int32?>(SequenceColourProperty); }
            internal set { SetProperty<Int32?>(SequenceColourProperty, value); }
        }
        #endregion

        #region SequenceNumber

        /// <summary>
        /// The sequence number that this <see cref="PlanogramPosition"/> has in the <see cref="PlanogramSequenceGroup"/>
        /// matching <see cref="SequenceColour"/>. Not to be confused with other sequence properties that relate to merchandising
        /// layout or re-numbering. This property refers to the <see cref="PlanogramSequence"/> used for automation and optimisation.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> SequenceNumberProperty =
            RegisterModelProperty<Int32?>(c => c.SequenceNumber, Message.PlanogramPosition_SequenceNumber);

        /// <summary>
        /// The sequence number that this <see cref="PlanogramPosition"/> has in the <see cref="PlanogramSequenceGroup"/>
        /// matching <see cref="SequenceColour"/>. Not to be confused with other sequence properties that relate to merchandising
        /// layout or re-numbering. This property refers to the <see cref="PlanogramSequence"/> used for automation and optimisation.
        /// </summary>
        public Int32? SequenceNumber
        {
            get { return ReadProperty<Int32?>(SequenceNumberProperty); }
            internal set { SetProperty<Int32?>(SequenceNumberProperty, value); }
        }


        #endregion

        #endregion

        #region Constants

        const Int32 RoundToDigits = 4;

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPosition NewPlanogramPosition()
        {
            PlanogramPosition item = new PlanogramPosition();
            item.Create(1, null, null, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPosition NewPlanogramPosition(Int16 sequence)
        {
            PlanogramPosition item = new PlanogramPosition();
            item.Create(sequence, null, null, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPosition NewPlanogramPosition(Int16 sequence,
            PlanogramProduct product)
        {
            PlanogramPosition item = new PlanogramPosition();
            item.Create(sequence, product, null, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPosition NewPlanogramPosition(Int16 sequence,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramPosition item = new PlanogramPosition();
            item.Create(sequence, product, subComponentPlacement, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPosition NewPlanogramPosition(Int16 sequence,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            Int32 sequenceNumber,
            Int32 sequenceColour)
        {
            PlanogramPosition item = new PlanogramPosition();
            item.Create(sequence, product, subComponentPlacement, sequenceNumber, sequenceColour);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        private void Create(Int16 sequence, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, Int32? sequenceNumber, Int32? sequenceColour)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            if (product != null)
            {
                this.LoadProperty<Object>(PlanogramProductIdProperty, product.Id);
                this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeProperty, PlanogramPositionOrientationType.Default);
            }
            if (subComponentPlacement != null)
            {
                if (subComponentPlacement.SubComponent != null)
                {
                    this.LoadProperty<Object>(PlanogramSubComponentIdProperty, subComponentPlacement.SubComponent.Id);

                    //if the product is to be hung but is in a tray then force the position to place as a unit.
                    if (subComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang
                        || subComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                    {
                        if (product != null && product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray)
                        {
                            this.LoadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleProperty, PlanogramPositionMerchandisingStyle.Unit);
                        }
                    }
                }
                if (subComponentPlacement.FixtureItem != null) this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, subComponentPlacement.FixtureItem.Id);
                if (subComponentPlacement.FixtureAssembly != null) this.LoadProperty<Object>(PlanogramFixtureAssemblyIdProperty, subComponentPlacement.FixtureAssembly.Id);
                if (subComponentPlacement.FixtureComponent != null) this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, subComponentPlacement.FixtureComponent.Id);
                if (subComponentPlacement.AssemblyComponent != null) this.LoadProperty<Object>(PlanogramAssemblyComponentIdProperty, subComponentPlacement.AssemblyComponent.Id);
            }
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeXProperty, PlanogramPositionOrientationType.Right0);
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeYProperty, PlanogramPositionOrientationType.Top0);
            this.LoadProperty<Int16>(SequenceProperty, sequence);
            this.LoadProperty<Int16>(SequenceXProperty, 1);
            this.LoadProperty<Int16>(SequenceYProperty, 1);
            this.LoadProperty<Int16>(SequenceZProperty, 1);
            this.LoadProperty<Int16>(FacingsHighProperty, 1);
            this.LoadProperty<Int16>(FacingsWideProperty, 1);
            this.LoadProperty<Int16>(FacingsDeepProperty, 1);
            this.LoadProperty<Int16>(UnitsHighProperty, 1);
            this.LoadProperty<Int16>(UnitsWideProperty, 1);
            this.LoadProperty<Int16>(UnitsDeepProperty, 1);
            this.LoadProperty<Int32>(TotalUnitsProperty, 1);
            this.LoadProperty<Single>(VerticalSqueezeProperty, 1);
            this.LoadProperty<Single>(HorizontalSqueezeProperty, 1);
            this.LoadProperty<Single>(DepthSqueezeProperty, 1);
            this.LoadProperty<Single>(VerticalSqueezeXProperty, 1);
            this.LoadProperty<Single>(HorizontalSqueezeXProperty, 1);
            this.LoadProperty<Single>(DepthSqueezeXProperty, 1);
            this.LoadProperty<Single>(VerticalSqueezeYProperty, 1);
            this.LoadProperty<Single>(HorizontalSqueezeYProperty, 1);
            this.LoadProperty<Single>(DepthSqueezeYProperty, 1);
            this.LoadProperty<Single>(VerticalSqueezeZProperty, 1);
            this.LoadProperty<Single>(HorizontalSqueezeZProperty, 1);
            this.LoadProperty<Single>(DepthSqueezeZProperty, 1);
            this.LoadProperty<Boolean>(IsManuallyPlacedProperty, false);

            if (sequenceNumber != null)
            {
                this.LoadProperty<Int32?>(SequenceNumberProperty, sequenceNumber);
            }
            if (sequenceColour != null)
            {
                this.LoadProperty<Int32?>(SequenceColourProperty, sequenceColour);
            }

            this.LoadProperty<PlanogramItemComparisonStatusType>(MetaComparisonStatusProperty, PlanogramItemComparisonStatusType.Unchanged);

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Event Handlers
        protected override void OnPropertyChanging(Csla.Core.IPropertyInfo propertyInfo)
        {
            base.OnPropertyChanging(propertyInfo);

            // clear the position details cache if required
            if ((propertyInfo.Name != PlanogramPosition.XProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.YProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.ZProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.SequenceProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.SequenceXProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.SequenceXProperty.Name) &&
                (propertyInfo.Name != PlanogramPosition.SequenceXProperty.Name))
            {
                if (_positionDetails != null)
                {
                    lock (_positionDetailsLock)
                    {
                        if (_positionDetails != null)
                        {
                            _positionDetails = null;
                        }
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramPosition>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }
        #endregion

        #region ResolveIds
        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramFixtureItemId
            Object planogramFixtureItemId = context.ResolveId<PlanogramFixtureItem>(this.ReadProperty<Object>(PlanogramFixtureItemIdProperty));
            if (planogramFixtureItemId != null) this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, planogramFixtureItemId);

            // PlanogramFixtureAssemblyId
            Object planogramFixtureAssemblyId = context.ResolveId<PlanogramFixtureAssembly>(this.ReadProperty<Object>(PlanogramFixtureAssemblyIdProperty));
            if (planogramFixtureAssemblyId != null) this.LoadProperty<Object>(PlanogramFixtureAssemblyIdProperty, planogramFixtureAssemblyId);

            // PlanogramFixtureComponentId
            Object planogramFixtureComponentId = context.ResolveId<PlanogramFixtureComponent>(this.ReadProperty<Object>(PlanogramFixtureComponentIdProperty));
            if (planogramFixtureComponentId != null) this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, planogramFixtureComponentId);

            // PlanogramAssemblyComponentId
            Object planogramAssemblyComponentId = context.ResolveId<PlanogramAssemblyComponent>(this.ReadProperty<Object>(PlanogramAssemblyComponentIdProperty));
            if (planogramAssemblyComponentId != null) this.LoadProperty<Object>(PlanogramAssemblyComponentIdProperty, planogramAssemblyComponentId);

            // PlanogramSubComponentId
            Object planogramSubComponentId = context.ResolveId<PlanogramSubComponent>(this.ReadProperty<Object>(PlanogramSubComponentIdProperty));
            if (planogramSubComponentId != null) this.LoadProperty<Object>(PlanogramSubComponentIdProperty, planogramSubComponentId);

            // PlanogramProductId
            Object planogramProductId = context.ResolveId<PlanogramProduct>(this.ReadProperty<Object>(PlanogramProductIdProperty));
            if (planogramProductId != null) this.LoadProperty<Object>(PlanogramProductIdProperty, planogramProductId);
        }
        #endregion

        #region Transformation Methods

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// </summary>
        /// <param name="subComponentPlacement">the placement.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTranform(PlanogramSubComponentPlacement subComponentPlacement,
            Boolean includeLocal)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;
            if (subComponentPlacement == null) return relativeTransform;

            //Position
            if (includeLocal)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        this.X, this.Y, this.Z,
                        this.Angle, this.Slope, this.Roll));
            }


            //SubComponent
            PlanogramSubComponent sub = subComponentPlacement.SubComponent;
            if (sub != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        sub.X, sub.Y, sub.Z, sub.Angle, sub.Slope, sub.Roll));
            }

            if (subComponentPlacement.FixtureComponent != null)
            {
                //Fixture Component
                PlanogramFixtureComponent fc = subComponentPlacement.FixtureComponent;

                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fc.X, fc.Y, fc.Z, fc.Angle, fc.Slope, fc.Roll));

            }
            else if (subComponentPlacement.AssemblyComponent != null)
            {
                //Assembly Component
                PlanogramAssemblyComponent ac = subComponentPlacement.AssemblyComponent;

                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        ac.X, ac.Y, ac.Z, ac.Angle, ac.Slope, ac.Roll));

                //Assembly
                if (subComponentPlacement.FixtureAssembly != null)
                {
                    PlanogramFixtureAssembly fa = subComponentPlacement.FixtureAssembly;

                    relativeTransform.Append(
                        MatrixValue.CreateTransformationMatrix(
                            fa.X, fa.Y, fa.Z, fa.Angle, fa.Slope, fa.Roll));
                }

            }

            //Fixture
            if (subComponentPlacement.FixtureItem != null)
            {
                PlanogramFixtureItem fixtureItem = subComponentPlacement.FixtureItem;

                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixtureItem.Angle, fixtureItem.Slope, fixtureItem.Roll));
            }


            return relativeTransform;
        }

        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates(PlanogramSubComponentPlacement subComponentPlacement)
        {
            // get the local coordinates
            PointValue localCoordinates = new PointValue(this.X, this.Y, this.Z);

            // get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTranform(subComponentPlacement, /*incLocal*/false);

            // transform and return
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            //get the inverted relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTranform(subComponentPlacement, /*incLocal*/false);
            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);

            this.X = newLocalCoordinate.X;
            this.Y = newLocalCoordinate.Y;
            this.Z = newLocalCoordinate.Z;
        }

        /// <summary>
        /// Returns the planogram relative bounding box for this position.
        /// </summary>
        public RectValue GetPlanogramRelativeBoundingBox(PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramPositionDetails details)
        {
            RectValue bounds = new RectValue();
            bounds.Width = details.TotalSize.Width;
            bounds.Height = details.TotalSize.Height;
            bounds.Depth = details.TotalSize.Depth;

            MatrixValue relativeTransform = GetPlanogramRelativeTranform(subComponentPlacement, /*incLocal*/true);

            return relativeTransform.TransformBounds(bounds);
        }

        /// <summary>
        /// Returns static boundary information for this position placement.
        /// </summary>
        /// <param name="subComponentPlacement">the parent subcomponent placement</param>
        /// <param name="details">The current position details.</param>
        /// <returns>Boundary information</returns>
        public BoundaryInfo GetBoundaryInfo(PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramPositionDetails details)
        {
            return new BoundaryInfo(
                details.TotalSize.Height, details.TotalSize.Width, details.TotalSize.Depth,
                GetPlanogramRelativeTranform(subComponentPlacement, /*incLocal*/true));
        }

        #endregion

        #region Planogram Stuff

        /// <summary>
        /// Returns the planogram product linked to this position.
        /// </summary>
        /// <returns></returns>
        public PlanogramProduct GetPlanogramProduct()
        {
            if (this.PlanogramProductId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.Products.FindById(this.PlanogramProductId);
        }

        /// <summary>
        /// Returns the performance data for the product linked to this position.
        /// </summary>
        /// <returns></returns>
        public PlanogramPerformanceData GetPlanogramPerformanceData()
        {
            Object productId = this.PlanogramProductId;
            if (productId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            if (planogram.Performance == null) return null;

            return
                planogram.Performance.PerformanceData.FirstOrDefault(p => Object.Equals(p.PlanogramProductId, productId));
        }

        /// <summary>
        /// Returns the planogram subcomponent linked to this position.
        /// </summary>
        public PlanogramSubComponent GetPlanogramSubComponent()
        {
            if (this.PlanogramSubComponentId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.GetPlanogramSubComponent(this.PlanogramSubComponentId);
        }

        /// <summary>
        /// Returns the planogram sub component placement for this position
        /// </summary>
        public PlanogramSubComponentPlacement GetPlanogramSubComponentPlacement()
        {
            return PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(this);
        }

        /// <summary>
        /// Returns the actual merch style applied to this position
        /// </summary>
        public PlanogramProductMerchandisingStyle GetAppliedMerchandisingStyle(PlanogramPositionBlockType block)
        {
            PlanogramProduct product = this.GetPlanogramProduct();
            return this.GetAppliedMerchandisingStyle(product, block);
        }

        /// <summary>
        /// Returns the actual merch style applied to this position
        /// </summary>
        internal PlanogramProductMerchandisingStyle GetAppliedMerchandisingStyle(PlanogramProduct product,
            PlanogramPositionBlockType block)
        {
            Debug.Assert(product != null, "Product should not be null");
            if (product == null) return PlanogramProductMerchandisingStyle.Unit;

            PlanogramPositionMerchandisingStyle blockStyle;
            switch (block)
            {
                default:
                case PlanogramPositionBlockType.Main:
                    blockStyle = this.MerchandisingStyle;
                    break;
                case PlanogramPositionBlockType.X:
                    blockStyle = this.MerchandisingStyleX;
                    break;
                case PlanogramPositionBlockType.Y:
                    blockStyle = this.MerchandisingStyleY;
                    break;
                case PlanogramPositionBlockType.Z:
                    blockStyle = this.MerchandisingStyleZ;
                    break;
            }

            switch (blockStyle)
            {
                case PlanogramPositionMerchandisingStyle.Default:
                    return product.MerchandisingStyle;
                case PlanogramPositionMerchandisingStyle.Alternate:
                    return PlanogramProductMerchandisingStyle.Alternate;
                case PlanogramPositionMerchandisingStyle.Case:
                    return PlanogramProductMerchandisingStyle.Case;
                case PlanogramPositionMerchandisingStyle.Display:
                    return PlanogramProductMerchandisingStyle.Display;
                case PlanogramPositionMerchandisingStyle.PointOfPurchase:
                    return PlanogramProductMerchandisingStyle.PointOfPurchase;
                case PlanogramPositionMerchandisingStyle.Tray:
                    return PlanogramProductMerchandisingStyle.Tray;
                case PlanogramPositionMerchandisingStyle.Unit:
                    return PlanogramProductMerchandisingStyle.Unit;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the actual orientation type applied to this position
        /// </summary>
        public PlanogramProductOrientationType GetAppliedOrientationType(PlanogramPositionBlockType block)
        {
            PlanogramProduct product = this.GetPlanogramProduct();
            return this.GetAppliedOrientationType(product, block);
        }

        /// <summary>
        /// Returns the actual orientation type applied to this position
        /// </summary>
        public PlanogramProductOrientationType GetAppliedOrientationType(PlanogramProduct product,
            PlanogramPositionBlockType block)
        {
            Debug.Assert(product != null, "Product should not be null");
            if (product == null) return PlanogramProductOrientationType.Front0;

            //if the product is set as front only then always return that.
            if (product.IsFrontOnly)
            {
                return PlanogramProductOrientationType.Front0;
            }
            else
            {
                //otherwise determine the value from the position

                PlanogramPositionOrientationType blockType;
                switch (block)
                {
                    default:
                    case PlanogramPositionBlockType.Main:
                        blockType = this.OrientationType;
                        break;
                    case PlanogramPositionBlockType.X:
                        blockType = this.OrientationTypeX;
                        break;
                    case PlanogramPositionBlockType.Y:
                        blockType = this.OrientationTypeY;
                        break;
                    case PlanogramPositionBlockType.Z:
                        blockType = this.OrientationTypeZ;
                        break;
                }

                switch (blockType)
                {
                    case PlanogramPositionOrientationType.Default:
                        return product.OrientationType;
                    case PlanogramPositionOrientationType.Front0:
                        return PlanogramProductOrientationType.Front0;
                    case PlanogramPositionOrientationType.Front90:
                        return PlanogramProductOrientationType.Front90;
                    case PlanogramPositionOrientationType.Top0:
                        return PlanogramProductOrientationType.Top0;
                    case PlanogramPositionOrientationType.Top90:
                        return PlanogramProductOrientationType.Top90;
                    case PlanogramPositionOrientationType.Right0:
                        return PlanogramProductOrientationType.Right0;
                    case PlanogramPositionOrientationType.Right90:
                        return PlanogramProductOrientationType.Right90;
                    case PlanogramPositionOrientationType.Left0:
                        return PlanogramProductOrientationType.Left0;
                    case PlanogramPositionOrientationType.Left90:
                        return PlanogramProductOrientationType.Left90;
                    case PlanogramPositionOrientationType.Back0:
                        return PlanogramProductOrientationType.Back0;
                    case PlanogramPositionOrientationType.Back90:
                        return PlanogramProductOrientationType.Back90;
                    case PlanogramPositionOrientationType.Bottom0:
                        return PlanogramProductOrientationType.Bottom0;
                    case PlanogramPositionOrientationType.Bottom90:
                        return PlanogramProductOrientationType.Bottom90;
                    case PlanogramPositionOrientationType.Front180:
                        return PlanogramProductOrientationType.Front180;
                    case PlanogramPositionOrientationType.Front270:
                        return PlanogramProductOrientationType.Front270;
                    case PlanogramPositionOrientationType.Top180:
                        return PlanogramProductOrientationType.Top180;
                    case PlanogramPositionOrientationType.Top270:
                        return PlanogramProductOrientationType.Top270;
                    case PlanogramPositionOrientationType.Right180:
                        return PlanogramProductOrientationType.Right180;
                    case PlanogramPositionOrientationType.Right270:
                        return PlanogramProductOrientationType.Right270;
                    case PlanogramPositionOrientationType.Left180:
                        return PlanogramProductOrientationType.Left180;
                    case PlanogramPositionOrientationType.Left270:
                        return PlanogramProductOrientationType.Left270;
                    case PlanogramPositionOrientationType.Back180:
                        return PlanogramProductOrientationType.Back180;
                    case PlanogramPositionOrientationType.Back270:
                        return PlanogramProductOrientationType.Back270;
                    case PlanogramPositionOrientationType.Bottom180:
                        return PlanogramProductOrientationType.Bottom180;
                    case PlanogramPositionOrientationType.Bottom270:
                        return PlanogramProductOrientationType.Bottom270;

                    default:
                        throw new NotImplementedException();

                }
            }
        }


        /// <summary>
        /// Returns the row number that the top 
        /// facing of this position is snapped to 
        /// </summary>
        public Int32? GetPegRowNum(Single positionHeight, PlanogramProduct product, PlanogramSubComponent subComponent, PlanogramLengthUnitOfMeasureType uom)
        {
            Single posY = this.Y;
            Single productPegY = product.PegY;
            Single productPegProngOffsetY = product.PegProngOffsetY;


            Boolean hasRow1Holes = (subComponent.MerchConstraintRow1SpacingY > 0 && subComponent.MerchConstraintRow1Width > 0 && subComponent.MerchConstraintRow1Height > 0);
            Boolean hasRow2Holes = (subComponent.MerchConstraintRow2SpacingY > 0 && subComponent.MerchConstraintRow2Width > 0 && subComponent.MerchConstraintRow2Height > 0);

            if (hasRow1Holes && !hasRow2Holes)
            {
                return 1;
            }
            else if (!hasRow1Holes && hasRow2Holes)
            {
                return 2;
            }
            else
            {
                Int32? rowNum = null;

                if ((subComponent.MerchConstraintRow1Width > 0 && subComponent.MerchConstraintRow1Height > 0)
                        || (subComponent.MerchConstraintRow2Width > 0 && subComponent.MerchConstraintRow2Height > 0))
                {
                    List<Single> rowYVals = subComponent.EnumeratePegholeYCoords().Distinct().OrderByDescending(y => y).ToList();
                    if (!rowYVals.Any()) return null;

                    Single pegY = (productPegY > 0) ? productPegY : PlanogramPositionDetails.GetDefaultPegYOffset(productPegY, uom);
                    Single snapY = posY + productPegProngOffsetY + positionHeight - productPegY;

                    Single nearestRowVal = rowYVals.OrderBy(r => Math.Abs(r - snapY)).First();

                    Int32 yHoleNum = rowYVals.IndexOf(nearestRowVal);
                    if (yHoleNum % 2 == 0) rowNum = 1;
                    else rowNum = 2;

                }

                return rowNum;
            }
        }

        /// <summary>
        /// Returns true if the given block has values.
        /// </summary>
        public Boolean IsBlockActive(PlanogramPositionBlockType block)
        {
            switch (block)
            {
                case PlanogramPositionBlockType.Main:
                    return (this.FacingsHigh > 0 && this.FacingsWide > 0 && this.FacingsDeep > 0);

                case PlanogramPositionBlockType.X:
                    return (this.FacingsXHigh > 0 && this.FacingsXWide > 0 && this.FacingsXDeep > 0);

                case PlanogramPositionBlockType.Y:
                    return (this.FacingsYHigh > 0 && this.FacingsYWide > 0 && this.FacingsYDeep > 0);

                case PlanogramPositionBlockType.Z:
                    return (this.FacingsZHigh > 0 && this.FacingsZWide > 0 && this.FacingsZDeep > 0);

                default:
                    Debug.Fail("case not handled");
                    return false;
            }
        }

        /// <summary>
        ///     Gets asynchronously the image data bytes for the given <paramref name="product"/> and other criteria.
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> from which to get an image for.</param>
        /// <param name="blockType">The position's block type used to determine the applied merchandising style.</param>
        /// <param name="faceType">The image's desired facing type.</param>
        /// <param name="imageUpdateCallBack">The call back invoked when the image is actually retrieved if not done immediatelly.</param>
        /// <param name="imageType">The type of image returned as this may be different to the one requested</param>
        public PlanogramImage GetImageAsync(PlanogramProduct product, PlanogramPositionBlockType blockType, PlanogramProductFaceType faceType,
            Action<Object> imageUpdateCallBack, Object userState,
            out PlanogramImageType imageType)
        {
            imageType = PlanogramImageType.None;
            if (product == null) return null;

            return product.GetMerchStyleImageAsync(
                GetAppliedMerchandisingStyle(product, blockType),
                faceType, imageUpdateCallBack, userState, out imageType);
        }

        /// <summary>
        /// Returns the image data requested
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> from which to get an image for.</param>
        /// <param name="blockType">The position's block type used to determine the applied merchandising style.</param>
        /// <param name="faceType">The image's desired facing type.</param>
        /// <param name="imageType">The type of image returned as this may be different to the one requested</param>
        public PlanogramImage GetImage(PlanogramProduct product, PlanogramPositionBlockType blockType, PlanogramProductFaceType faceType,
            out PlanogramImageType imageType)
        {
            imageType = PlanogramImageType.None;
            if (product == null) return null;

            return product.GetMerchStyleImage(GetAppliedMerchandisingStyle(product, blockType), faceType, out imageType);
        }

        #region GetPositionDetails

        /// <summary>
        /// Returns calculated details for this position
        /// </summary>
        public PlanogramPositionDetails GetPositionDetails()
        {
            if (_positionDetails == null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails == null)
                    {
                        _positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(
                            this, this.GetPlanogramProduct(), this.GetPlanogramSubComponentPlacement());
                    }
                }
            }
            return _positionDetails;
        }

        /// <summary>
        /// Returns calculated details for this position
        /// </summary>
        public PlanogramPositionDetails GetPositionDetails(
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            if (_positionDetails == null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails == null)
                    {
                        _positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(
                            this, this.GetPlanogramProduct(), subComponentPlacement);
                    }
                }
            }
            return _positionDetails;
        }

        /// <summary>
        /// Returns calculated details for this position
        /// </summary>
        public PlanogramPositionDetails GetPositionDetails(
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            if (_positionDetails == null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails == null)
                    {
                        _positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(
                            this, product, subComponentPlacement);
                    }
                }
            }
            return _positionDetails;
        }

        /// <summary>
        /// Returns calculated details for this position
        /// </summary>
        public PlanogramPositionDetails GetPositionDetails(
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (_positionDetails == null)
            {
                lock (_positionDetailsLock)
                {
                    if (_positionDetails == null)
                    {
                        _positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(
                            this, product, subComponentPlacement, merchandisingGroup);
                    }
                }
            }
            return _positionDetails;
        }

        #endregion

        /// <summary>
        /// Returns a tuple per position facing. 
        /// Val1 = x start, Val2 = facing width.
        /// </summary>
        public List<Tuple<Single, Single>> GetFacingsXAndWidth(PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            List<Tuple<Single, Single>> returnList = new List<Tuple<Single, Single>>();

            PlanogramPositionDetails renderInfo = this.GetPositionDetails(product, subComponentPlacement, null);
            Single unitSizeX;
            Single curX = 0;

            //left x block.
            if (renderInfo.HasXBlock && this.IsXPlacedLeft)
            {
                unitSizeX = renderInfo.XRotatedUnitSize.Width;
                for (Int32 i = 0; i < this.FacingsXWide; i++)
                {
                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                    curX += unitSizeX;

                    //add spacing.
                    if (i <= this.FacingsXWide - 1)
                    {
                        curX += renderInfo.FacingSize.Width;
                    }
                }
            }

            //main
            unitSizeX = renderInfo.MainRotatedUnitSize.Width;
            for (Int32 i = 0; i < this.FacingsWide; i++)
            {
                returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                curX += unitSizeX;

                //add spacing.
                if (i < this.FacingsWide - 1)
                {
                    curX += renderInfo.FacingSize.Width;
                }
            }

            //right x block
            if (renderInfo.HasXBlock && !this.IsXPlacedLeft)
            {
                //add another spacing
                curX += renderInfo.FacingSize.Width;

                unitSizeX = renderInfo.XRotatedUnitSize.Width;
                for (Int32 i = 0; i < this.FacingsXWide; i++)
                {
                    returnList.Add(new Tuple<Single, Single>(curX, unitSizeX));
                    curX += unitSizeX;

                    //add spacing.
                    if (i < this.FacingsXWide - 1)
                    {
                        curX += renderInfo.FacingSize.Width;
                    }
                }
            }

            return returnList;
        }

        /// <summary>
        /// Sets the position to fill the given height
        /// </summary>
        public void FillHigh(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup, Single targetHeight)
        {
            if (product == null || subComponentPlacement == null) return;

            PlanogramPosition positionCopy = this.Copy();
            positionCopy.FacingsHigh = 1;

            // Fill the main block up until the target height is exceeded and then 
            // set the actual facings high to one less.
            PlanogramPositionDetails positionCopyDetails = positionCopy.GetPositionDetails(product,
                subComponentPlacement, merchandisingGroup);
            while (positionCopyDetails.MainTotalSize.Height.LessOrEqualThan(targetHeight))
            {
                positionCopy.FacingsHigh++;
                positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
            }
            positionCopy.FacingsHigh = Convert.ToInt16(Math.Max(positionCopy.FacingsHigh - 1, 1));
            Int16 facingsHigh = positionCopy.FacingsHigh;
            // Ensure that we don't exceed the MaxStack and MaxNestingHigh constraints.
            if (product.MaxStack > 0)
            {
                facingsHigh = Math.Min(facingsHigh, product.MaxStack);
            }
            this.FacingsHigh = facingsHigh;
            positionCopy.FacingsHigh = facingsHigh;
            // Now fill the X block until the target height is exceeded and set the
            // actuals facings high to one less.
            if (positionCopyDetails.HasXBlock)
            {
                positionCopy.FacingsXHigh = 0;
                positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                while (positionCopyDetails.XTotalSize.Height.LessOrEqualThan(targetHeight))
                {
                    positionCopy.FacingsXHigh++;
                    positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement,
                        merchandisingGroup);
                }
                positionCopy.FacingsXHigh = Convert.ToInt16(Math.Max(positionCopy.FacingsXHigh - 1, 1));
                this.FacingsXHigh = product.MaxStack > 0
                    ? Math.Min(positionCopy.FacingsXHigh, product.MaxStack)
                    : positionCopy.FacingsHigh;
            }

            // Now fill the Z block until the target height is exceeded and set the
            // actuals facings high to one less.
            if (positionCopyDetails.HasZBlock)
            {
                positionCopy.FacingsZHigh = 0;
                positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                while (positionCopyDetails.ZTotalSize.Height.LessOrEqualThan(targetHeight))
                {
                    positionCopy.FacingsZHigh++;
                    positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement,
                        merchandisingGroup);
                }
                positionCopy.FacingsZHigh = Convert.ToInt16(Math.Max(positionCopy.FacingsZHigh - 1, 1));
                this.FacingsZHigh = positionCopy.FacingsZHigh;
            }

            // Now fill the Y block until the target height is exceeded and set the
            // actuals facings high to one less.
            if (positionCopyDetails.HasYBlock)
            {
                positionCopy.FacingsYHigh = 0;
                positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                while (positionCopyDetails.TotalSize.Height.LessOrEqualThan(targetHeight))
                {
                    positionCopy.FacingsYHigh++;
                    positionCopyDetails = positionCopy.GetPositionDetails(product, subComponentPlacement,
                        merchandisingGroup);
                }
                positionCopy.FacingsYHigh = Convert.ToInt16(Math.Max(positionCopy.FacingsYHigh - 1, 0));
                this.FacingsYHigh = positionCopy.FacingsYHigh;
            }
        }

        /// <summary>
        /// Sets the position to fill the given width
        /// </summary>
        public void FillWide(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup, Single targetWidth)
        {
            PlanogramPositionDetails renderInfo = this.GetPositionDetails(product, subComponentPlacement,
                merchandisingGroup);
            Double unitWidth = renderInfo.MainRotatedUnitSize.Width;
            //Add a little tollerance for when the measurements due to the individual float to Double conversion. 
            Double roundedUnitWidth = Math.Round(unitWidth, PlanogramPosition.RoundToDigits);
            Double roundedTargetWidth = Math.Round(targetWidth, PlanogramPosition.RoundToDigits);

            Byte posWide = 1;

            Int32 maxWide = (Int32)(roundedTargetWidth / roundedUnitWidth);
            if (maxWide > 1)
            {
                posWide = Convert.ToByte(maxWide);

                PlanogramPosition preview = this.Copy();

                //Main Block
                preview.FacingsWide = posWide;
                PlanogramPositionDetails previewInfo = preview.GetPositionDetails(product, subComponentPlacement,
                    merchandisingGroup);
                while (previewInfo.TotalSize.Width > targetWidth && posWide > 1)
                {
                    posWide--;
                    preview.FacingsWide = posWide;

                    previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
                this.FacingsWide = posWide;


                //Y Block
                if (previewInfo.HasYBlock)
                {
                    maxWide = (Int32)(targetWidth / previewInfo.YRotatedUnitSize.Width);
                    if (maxWide > 1)
                    {
                        posWide = Convert.ToByte(maxWide);
                        preview.FacingsYWide = posWide;

                        while (previewInfo.TotalSize.Width > targetWidth && posWide > 1)
                        {
                            posWide--;

                            preview.FacingsYWide = posWide;

                            previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                        }
                        this.FacingsYWide = posWide;
                    }
                }

                //Z Block
                if (previewInfo.HasZBlock)
                {
                    maxWide = (Int32)(targetWidth / previewInfo.ZRotatedUnitSize.Width);
                    if (maxWide > 1)
                    {
                        posWide = Convert.ToByte(maxWide);
                        preview.FacingsZWide = posWide;

                        while (previewInfo.TotalSize.Width > targetWidth && posWide > 1)
                        {
                            posWide--;

                            preview.FacingsZWide = posWide;

                            previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                        }
                        this.FacingsZWide = posWide;
                    }
                }
            }
        }

        /// <summary>
        /// Sets the position to fill the given depth
        /// </summary>
        public void FillDeep(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup, Single targetDepth)
        {
            PlanogramPositionDetails renderInfo = this.GetPositionDetails(product, subComponentPlacement,
                merchandisingGroup);
            Double unitDepth = renderInfo.MainRotatedUnitSize.Depth;
            //Add a little tollerance for when the measurements due to the individual float to Double conversion. 
            Double roundedUnitDepth = Math.Round(unitDepth, PlanogramPosition.RoundToDigits);
            Double roundedTargetDepth = Math.Round(targetDepth, PlanogramPosition.RoundToDigits);

            Byte posDeep = 1;

            Int32 maxDeep = (Int32)(roundedTargetDepth / roundedUnitDepth);
            if (product.MaxDeep > 0)
            {
                maxDeep = Math.Min(maxDeep, product.MaxDeep);
            }
            if (maxDeep > 1)
            {
                posDeep = Convert.ToByte(maxDeep);

                PlanogramPosition preview = this.Copy();
                preview.FacingsDeep = posDeep;
                PlanogramPositionDetails previewInfo = preview.GetPositionDetails(product, subComponentPlacement,
                    merchandisingGroup);

                //MainBlock
                while (previewInfo.TotalSize.Depth > targetDepth && posDeep > 1)
                {
                    posDeep--;

                    preview.FacingsDeep = posDeep;

                    previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
                this.FacingsDeep = posDeep;


                //X Block
                if (previewInfo.HasXBlock)
                {
                    maxDeep = (Int32)(targetDepth / previewInfo.XRotatedUnitSize.Depth);
                    if (maxDeep > 1)
                    {
                        posDeep = Convert.ToByte(maxDeep);
                        preview.FacingsXDeep = posDeep;

                        while (previewInfo.TotalSize.Depth > targetDepth && posDeep > 1)
                        {
                            posDeep--;

                            preview.FacingsXDeep = posDeep;

                            previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                        }
                        this.FacingsXDeep = posDeep;
                    }
                }

                //Y Block
                if (previewInfo.HasYBlock)
                {
                    maxDeep = (Int32)(targetDepth / previewInfo.YRotatedUnitSize.Depth);
                    if (maxDeep > 1)
                    {
                        posDeep = Convert.ToByte(maxDeep);
                        preview.FacingsYDeep = posDeep;

                        while (previewInfo.TotalSize.Depth > targetDepth && posDeep > 1)
                        {
                            posDeep--;

                            preview.FacingsYDeep = posDeep;

                            previewInfo = preview.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                        }
                        this.FacingsYDeep = posDeep;
                    }
                }
            }
        }

        /// <summary>
        /// Recalculates the total units for this position
        /// </summary>
        public void RecalculateUnits(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement)
        {
            RecalculateUnits(this.GetPositionDetails(product, subComponentPlacement, null));
        }

        /// <summary>
        /// Recalculates the total units for this position
        /// </summary>
        public void RecalculateUnits(PlanogramPositionDetails positionDetails)
        {
            this.TotalUnits = positionDetails.TotalUnitCount;
            this.UnitsWide = positionDetails.TotalUnits.Wide;
            this.UnitsHigh = positionDetails.TotalUnits.High;
            this.UnitsDeep = positionDetails.TotalUnits.Deep;
        }

        /// <summary>
        /// Resets all values of this position so that
        /// squeeze is not applied at all.
        /// </summary>
        public void ResetSqueezeValues()
        {
            this.VerticalSqueeze = 1;
            this.HorizontalSqueeze = 1;
            this.DepthSqueeze = 1;

            this.VerticalSqueezeX = 1;
            this.HorizontalSqueezeX = 1;
            this.DepthSqueezeX = 1;

            this.VerticalSqueezeY = 1;
            this.HorizontalSqueezeY = 1;
            this.DepthSqueezeY = 1;

            this.VerticalSqueezeZ = 1;
            this.HorizontalSqueezeZ = 1;
            this.DepthSqueezeZ = 1;
        }

        /// <summary>
        /// Returns true if the sequences normal to the direction indicated match between this 
        /// position and the sequence values provided.
        /// </summary>
        /// <param name="axis">
        /// The direction normal to which sequences should be evaluated. E.g. providing X will compare SequenceY and SequenceZ.
        /// </param>
        /// <returns></returns>
        public Boolean HasMatchingNormalSequences(AxisType axis, Int16 sequenceX, Int16 sequenceY, Int16 sequenceZ)
        {
            switch (axis)
            {
                case AxisType.X:
                    return this.SequenceY == sequenceY && this.SequenceZ == sequenceZ;

                case AxisType.Y:
                    return this.SequenceX == sequenceX && this.SequenceZ == sequenceZ;

                case AxisType.Z:
                    return this.SequenceY == sequenceY && this.SequenceX == sequenceX;

                default:
                    throw new NotSupportedException("axis");
            }
        }

        #endregion

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <param name="appliedMerch">True by default, if on then applied readonly merch and
        /// orientation values are returned, otherwise editable actual are.</param>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean appliedMerch = true)
        {
            Type type = typeof(PlanogramPosition);
            String typeFriendly = PlanogramPosition.FriendlyName;


            String detailsGroup = Message.PlanogramPosition_PropertyGroup_Details;
            String metaGroup = Message.PlanogramPosition_PropertyGroup_Metadata;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsDeepProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsHighProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsWideProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsXDeepProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsXHighProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsXWideProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsYDeepProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsYHighProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsYWideProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsZDeepProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsZHighProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsZWideProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsXPlacedLeftProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsYPlacedBottomProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsZPlacedFrontProperty, detailsGroup);

            if (appliedMerch)
            {
                //AppliedMerchandisingStyle
                ObjectFieldInfo appliedMerchStyle = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleProperty, detailsGroup);
                appliedMerchStyle.PropertyName = AppliedMerchandisingStyleFieldName;
                appliedMerchStyle.PropertyType = typeof(PlanogramProductMerchandisingStyle);
                appliedMerchStyle.IsReadOnly = true;
                yield return appliedMerchStyle;

                //AppliedMerchandisingStyleX
                ObjectFieldInfo appliedMerchStyleX = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleXProperty, detailsGroup);
                appliedMerchStyleX.PropertyName = AppliedMerchandisingStyleXFieldName;
                appliedMerchStyleX.PropertyType = typeof(PlanogramProductMerchandisingStyle);
                appliedMerchStyleX.IsReadOnly = true;
                yield return appliedMerchStyleX;

                //AppliedMerchandisingStyleY
                ObjectFieldInfo appliedMerchStyleY = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleYProperty, detailsGroup);
                appliedMerchStyleY.PropertyName = AppliedMerchandisingStyleYFieldName;
                appliedMerchStyleY.PropertyType = typeof(PlanogramProductMerchandisingStyle);
                appliedMerchStyleY.IsReadOnly = true;
                yield return appliedMerchStyleY;

                //AppliedMerchandisingStyleZ
                ObjectFieldInfo appliedMerchStyleZ = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleZProperty, detailsGroup);
                appliedMerchStyleZ.PropertyName = AppliedMerchandisingStyleZFieldName;
                appliedMerchStyleZ.PropertyType = typeof(PlanogramProductMerchandisingStyle);
                appliedMerchStyleZ.IsReadOnly = true;
                yield return appliedMerchStyleZ;

                //AppliedOrientationType
                ObjectFieldInfo appliedOrientationType = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeProperty, detailsGroup);
                appliedOrientationType.PropertyName = AppliedOrientationTypeFieldName;
                appliedOrientationType.PropertyType = typeof(PlanogramProductOrientationType);
                appliedOrientationType.IsReadOnly = true;
                yield return appliedOrientationType;

                //AppliedOrientationTypeX
                ObjectFieldInfo appliedOrientationTypeX = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeXProperty, detailsGroup);
                appliedOrientationTypeX.PropertyName = AppliedOrientationTypeXFieldName;
                appliedOrientationTypeX.PropertyType = typeof(PlanogramProductOrientationType);
                appliedOrientationTypeX.IsReadOnly = true;
                yield return appliedOrientationTypeX;

                //AppliedOrientationTypeY
                ObjectFieldInfo appliedOrientationTypeY = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeYProperty, detailsGroup);
                appliedOrientationTypeY.PropertyName = AppliedOrientationTypeYFieldName;
                appliedOrientationTypeY.PropertyType = typeof(PlanogramProductOrientationType);
                appliedOrientationTypeY.IsReadOnly = true;
                yield return appliedOrientationTypeY;

                //AppliedOrientationTypeZ
                ObjectFieldInfo appliedOrientationTypeZ = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeZProperty, detailsGroup);
                appliedOrientationTypeZ.PropertyName = AppliedOrientationTypeZFieldName;
                appliedOrientationTypeZ.PropertyType = typeof(PlanogramProductOrientationType);
                appliedOrientationTypeZ.IsReadOnly = true;
                yield return appliedOrientationTypeZ;
            }
            else
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleXProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleYProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleZProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeXProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeYProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeZProperty, detailsGroup);
            }

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PositionSequenceNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TotalUnitsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SequenceNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SequenceColourProperty, detailsGroup);


            //dont return these as we are displaying plan relative ones instead.
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsManuallyPlacedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HorizontalSqueezeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VerticalSqueezeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthSqueezeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HorizontalSqueezeXProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VerticalSqueezeXProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthSqueezeXProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HorizontalSqueezeYProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VerticalSqueezeYProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthSqueezeYProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HorizontalSqueezeZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VerticalSqueezeZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthSqueezeZProperty, detailsGroup);

            //Metadata fields
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldXProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldYProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWorldZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaHeightProperty, detailsGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaWidthProperty, detailsGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaDepthProperty, detailsGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAchievedCasesProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaFrontFacingsWideProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsPositionCollisionsProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsUnderMinDeepProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMaxDeepProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMaxRightCapProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMaxStackProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverMaxTopCapProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsInvalidMerchandisingStyleProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsHangingTrayProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsPegOverfilledProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOutsideMerchandisingSpaceProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOutsideOfBlockSpaceProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalLinearSpaceProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalAreaSpaceProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalVolumetricSpaceProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramLinearSpacePercentageProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramAreaSpacePercentageProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramVolumetricSpacePercentageProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaComparisonStatusProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaSequenceSubGroupNameProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaSequenceGroupNameProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPegRowNumberProperty, metaGroup, /*isReadOnly*/true);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPegColumnNumberProperty, metaGroup, /*isReadOnly*/true);

            //following shouldnt be available to user but is useful when debugging.
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramPosition.SequenceProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramPosition.SequenceXProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramPosition.SequenceYProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramPosition.SequenceZProperty, detailsGroup);
        }

        /// <summary>
        /// Returns the value of the given field for this position.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            Debug.Assert(field.OwnerType == typeof(PlanogramPosition), "Field is not for this object type");

            switch (field.PropertyName)
            {
                case AppliedMerchandisingStyleFieldName: return GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Main);
                case AppliedMerchandisingStyleXFieldName: return GetAppliedMerchandisingStyle(PlanogramPositionBlockType.X);
                case AppliedMerchandisingStyleYFieldName: return GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Y);
                case AppliedMerchandisingStyleZFieldName: return GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Z);
                case AppliedOrientationTypeFieldName: return GetAppliedOrientationType(PlanogramPositionBlockType.Main);
                case AppliedOrientationTypeXFieldName: return GetAppliedOrientationType(PlanogramPositionBlockType.X);
                case AppliedOrientationTypeYFieldName: return GetAppliedOrientationType(PlanogramPositionBlockType.Y);
                case AppliedOrientationTypeZFieldName: return GetAppliedOrientationType(PlanogramPositionBlockType.Z);

                default:
                    return field.GetValue(this);
            }
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            Planogram parentPlan = this.Parent;
            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();
            PlanogramProduct product = parentPlan.Products.FindById(this.PlanogramProductId);

            PlanogramPositionDetails positionDetails = metadataDetails.FindPositionDetailsByPlanogramPositionId(this.Id);
            if (positionDetails == null) return;

            PlanogramSubComponentPlacement placement = metadataDetails.FindSubComponentPlacementForPosition(this);
            if (placement == null) return;

            PlanogramSubComponent subComponent = placement.SubComponent;
            if (subComponent == null) return;

            //make sure that total units is updated first
            RecalculateUnits(positionDetails);

            //Get info about the merch group this belongs to:
            PlanogramMetadataDetails.PlanogramMerchandisingGroupInfo parentMerchGroupInfo =
                metadataDetails.GetMerchandisingGroupInfo(placement.Id);


            //Set meta data properties based on child meta data
            this.MetaAchievedCases = product.CasePackUnits == 0 ? 0 : Convert.ToSingle(Math.Round(Convert.ToSingle(this.TotalUnits) / product.CasePackUnits, 2, MidpointRounding.AwayFromZero));

            //Calculate front facings - If main block is front facing with no orientation
            this.MetaFrontFacingsWide = CalculateMetaFrontFacingsWide(product);

            this.MetaIsPositionCollisions = HasCollisions(metadataDetails, placement);

            this.MetaIsUnderMinDeep = IsMinDeepNotAchieved(product);
            this.MetaIsOverMaxDeep = IsMaxDeepExceeded(product);
            this.MetaIsOverMaxRightCap = IsMaxRightCapExceeded(product);
            this.MetaIsOverMaxStack = IsMaxStackExceeded(product);
            this.MetaIsOverMaxTopCap = IsMaxTopCapExceeded(product);
            this.MetaIsInvalidMerchandisingStyle = IsMerchStyleSettingsInvalid(product);
            this.MetaIsHangingTray = IsTrayOnHangingSubComponent(product, subComponent);
            this.MetaIsPegOverfilled = IsPegOverfilled(positionDetails, product, subComponent);
            this.MetaIsOutsideMerchandisingSpace = IsOutsideMerchandisingSpace(placement, parentMerchGroupInfo, positionDetails);
            this.MetaIsOutsideOfBlockSpace = IsOutsideOfBlockSpace(product, metadataDetails);
            this.MetaHeight = positionDetails.TotalSize.Height;
            this.MetaWidth = positionDetails.TotalSize.Width;
            this.MetaDepth = positionDetails.TotalSize.Depth;
            this.MetaTotalLinearSpace = positionDetails.TotalSpace.Width;
            this.MetaTotalAreaSpace = positionDetails.TotalSpace.Width * positionDetails.TotalSpace.Height;
            this.MetaTotalVolumetricSpace = positionDetails.TotalSpace.Width * positionDetails.TotalSpace.Height * positionDetails.TotalSize.Depth;

            PointValue worldCoordinates = GetPlanogramRelativeCoordinates(placement); //TODO: Use boundary info instead!
            this.MetaWorldX = Convert.ToSingle(Math.Round(worldCoordinates.X, MathHelper.DefaultPrecision));
            this.MetaWorldY = Convert.ToSingle(Math.Round(worldCoordinates.Y, MathHelper.DefaultPrecision));
            this.MetaWorldZ = Convert.ToSingle(Math.Round(worldCoordinates.Z, MathHelper.DefaultPrecision));

            if (Parent.Comparison.Results.Any()) this.MetaComparisonStatus = Parent.Comparison.GetComparisonStatus(this);

            this.MetaSequenceSubGroupName = GetMetaSequenceSubGroupName(product);
            this.MetaSequenceGroupName = GetMetaSequenceGroupName(product);

            //if this is a pegged position, calculate the row and column number that it snaps to.
            this.MetaPegRowNumber = null;
            this.MetaPegColumnNumber = null;
            if (placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                Int32? rowNum = GetPegRowNum(positionDetails.TotalSize.Height, product, placement.SubComponent, parentPlan.LengthUnitsOfMeasure);
                if (rowNum.HasValue)
                {
                    Single pegX, pegY;
                    PlanogramPositionDetails.GetProductPegXPegY(product, positionDetails, parentPlan, out pegX, out pegY);

                    Single snapY = this.Y + positionDetails.TotalSize.Height - pegY + product.PegProngOffsetY;


                    Int32 yNum = 1;
                    foreach (Single y in placement.SubComponent.EnumeratePegholeYCoords().Distinct().OrderByDescending(y=> y))
                    {
                        if (snapY.EqualTo(y)) break;
                        yNum++;
                    }
                    this.MetaPegRowNumber = yNum;


                    if (rowNum.Value == 1 && placement.SubComponent.MerchConstraintRow1SpacingX == 0
                        || (rowNum == 2 && placement.SubComponent.MerchConstraintRow2SpacingX == 0))
                    {
                        this.MetaPegColumnNumber = null;
                    }
                    else
                    {
                        Single snapX = this.X + pegX - product.PegProngOffsetX;
                        Int32 colNum = 1;
                        foreach (Single x in placement.SubComponent.EnumeratePegholeXCoords(rowNum.Value))
                        {
                            if (snapX.EqualTo(x)) break;
                            colNum++;
                        }
                        this.MetaPegColumnNumber = colNum;
                    }
                }

            }

        }

        private string GetMetaSequenceGroupName(PlanogramProduct product)
        {
            if (SequenceColour.HasValue)
            {
                PlanogramSequenceGroup sequenceGroup = Parent.Sequence.Groups
                    .FirstOrDefault(g => g.Colour == SequenceColour.Value);
                if (sequenceGroup != null)
                {
                    return sequenceGroup.Name;
                }
            }
            return null;
        }

        private String GetMetaSequenceSubGroupName(PlanogramProduct product)
        {
            if (SequenceColour.HasValue)
            {
                PlanogramSequenceGroup sequenceGroup = Parent.Sequence.Groups
                    .FirstOrDefault(g => g.Colour == SequenceColour.Value);
                if (sequenceGroup != null)
                {
                    PlanogramSequenceGroupProduct sequenceProduct = sequenceGroup.Products
                        .FirstOrDefault(p => p.Gtin == product.Gtin);
                    if (sequenceProduct != null)
                    {
                        PlanogramSequenceGroupSubGroup subGroup = sequenceProduct.GetSubGroup();
                        if (subGroup != null)
                        {
                            return subGroup.Name;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Calculates product meta data that relies on planogram values
        /// having being calculated first.
        /// </summary>
        internal void CalculatePostPlanogramMetadata()
        {
            Planogram parentPlan = this.Parent;
            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();
            PlanogramProduct product = parentPlan.Products.FindById(this.PlanogramProductId);

            PlanogramPositionDetails positionDetails = metadataDetails.FindPositionDetailsByPlanogramPositionId(this.Id);
            if (positionDetails == null) return;


            //PlanogramLinearSpacePercentage
            if (parentPlan.MetaTotalMerchandisableLinearSpace.HasValue)
            {
                Single percentage = parentPlan.MetaTotalMerchandisableLinearSpace > 0 ? (this.MetaTotalLinearSpace.Value / parentPlan.MetaTotalMerchandisableLinearSpace.Value) : 0;
                this.MetaPlanogramLinearSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramLinearSpacePercentage = null;
            }

            // PlanogramAreaSpacePercentage
            if (parentPlan.MetaTotalMerchandisableAreaSpace.HasValue)
            {
                Single percentage = parentPlan.MetaTotalMerchandisableAreaSpace > 0 ? (this.MetaTotalAreaSpace.Value / parentPlan.MetaTotalMerchandisableAreaSpace.Value) : 0;
                this.MetaPlanogramAreaSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramAreaSpacePercentage = null;
            }

            // PlanogramVolumetricSpacePercentage
            if (parentPlan.MetaTotalMerchandisableVolumetricSpace.HasValue)
            {
                Single percentage =
                    parentPlan.MetaTotalMerchandisableVolumetricSpace > 0 ?
                    (this.MetaTotalVolumetricSpace.Value / parentPlan.MetaTotalMerchandisableVolumetricSpace.Value) : 0;

                this.MetaPlanogramVolumetricSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramVolumetricSpacePercentage = null;
            }
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            //Set meta data properties based on child meta data
            this.MetaAchievedCases = null;
            this.MetaFrontFacingsWide = null;
            this.MetaIsPositionCollisions = null;
            this.MetaIsUnderMinDeep = null;
            this.MetaIsOverMaxDeep = null;
            this.MetaIsOverMaxRightCap = null;
            this.MetaIsOverMaxStack = null;
            this.MetaIsOverMaxTopCap = null;
            this.MetaIsInvalidMerchandisingStyle = null;
            this.MetaIsHangingTray = null;
            this.MetaIsPegOverfilled = null;
            this.MetaIsOutsideMerchandisingSpace = null;
            this.MetaIsOutsideOfBlockSpace = null;
            this.MetaHeight = null;
            this.MetaWidth = null;
            this.MetaDepth = null;
            this.MetaWorldX = null;
            this.MetaWorldY = null;
            this.MetaWorldZ = null;
            this.MetaTotalLinearSpace = null;
            this.MetaTotalAreaSpace = null;
            this.MetaTotalVolumetricSpace = null;
            this.MetaPlanogramLinearSpacePercentage = null;
            this.MetaPlanogramAreaSpacePercentage = null;
            this.MetaPlanogramVolumetricSpacePercentage = null;
            this.MetaSequenceSubGroupName = null;
            this.MetaSequenceGroupName = null;
            this.MetaPegColumnNumber = null;
            this.MetaPegRowNumber = null;
        }

        /// <summary>
        /// Returns true if any objects collide with this instance.
        /// </summary>
        /// <param name="metaDetails"></param>
        /// <returns></returns>
        internal Boolean HasCollisions(PlanogramMetadataDetails metaDetails)
        {
            return HasCollisions(metaDetails, this.GetPlanogramSubComponentPlacement());
        }

        /// <summary>
        ///   Returns true if any objects collide with this instance.
        /// </summary>
        /// <returns></returns>
        private Boolean HasCollisions(PlanogramMetadataDetails metaDetails, PlanogramSubComponentPlacement parentSubComponentPlacement)
        {
            PlanogramSubComponent parentSubComponent = parentSubComponentPlacement.SubComponent;

            BoundaryInfo thisBounds = null;
            PlanogramPositionPlacementId placementId = new PlanogramPositionPlacementId(this);
            metaDetails.PositionPlacementIdToWorldSpaceBounds.TryGetValue(placementId, out thisBounds);
            if (thisBounds == null) return false;

            // Check against other positions if overlapping is not allowed.
            //need to consider x, y and z of the product position plus the  stacking, capping, orientations applied
            foreach (var positionValue in metaDetails.PositionPlacementIdToWorldSpaceBounds)
            {
                BoundaryInfo otherPosBounds = positionValue.Value;

                //don't check collision against self.
                if (Object.Equals(positionValue.Key.PositionId, this.Id)) continue;

                if (Object.Equals(placementId.SubComponentId, positionValue.Key.SubComponentId))
                {
                    //position on same component - validation only if overlap is not allowed.
                    if (!parentSubComponent.IsProductOverlapAllowed && thisBounds.CollidesWith(otherPosBounds)) return true;
                }
                else
                {
                    //return true if the bounds collide with different position from alternate component
                    if (thisBounds.CollidesWith(otherPosBounds)) return true;
                }
            }

            PlanogramComponentPlacementId parentComponentKey = new PlanogramComponentPlacementId(parentSubComponentPlacement);

            //cycle through component groups
            foreach (var componentPlacementIdGroup in metaDetails.SubPlacementInfosByComponent)
            {
                //and against each sub on the component
                foreach (var subPlacement in componentPlacementIdGroup)
                {
                    //check if we have a collision
                    if (!thisBounds.CollidesWith(subPlacement.WorldBoundary)) continue;

                    //if has collided with its parent component
                    if (Object.Equals(componentPlacementIdGroup.Key, parentComponentKey))
                    {
                        //[V8-28422] check that the position is not colliding with its own chest parent.
                        if (parentSubComponentPlacement.Component.ComponentType == PlanogramComponentType.Chest)
                        {
                            //return true if we have hit one of the chest walls.
                            if (parentSubComponent.FaceThicknessLeft > 0 && this.X < parentSubComponent.FaceThicknessLeft) return true;
                            if (parentSubComponent.FaceThicknessRight > 0 && ((this.X + thisBounds.WorldRelativeBounds.Width) > (parentSubComponent.Width - parentSubComponent.FaceThicknessRight))) return true;
                            if (parentSubComponent.FaceThicknessBack > 0 && this.Z < parentSubComponent.FaceThicknessBack) return true;
                            if (parentSubComponent.FaceThicknessFront > 0 && ((this.Z + thisBounds.WorldRelativeBounds.Depth) > (parentSubComponent.Depth - parentSubComponent.FaceThicknessFront))) return true;
                            if (parentSubComponent.FaceThicknessBottom > 0 && this.Y < parentSubComponent.FaceThicknessBottom) return true;
                        }

                        //no wall hits so just break.
                        break;
                    }

                    return true;
                }
            }


            return false;
        }

        /// <summary>
        ///     Calculate the <c>Front</c> facings of the main block for this instance.
        /// </summary>
        /// <param name="product"></param>
        /// <returns>The front wide facings for this instance.</returns>
        /// <remarks></remarks>
        private Int16 CalculateMetaFrontFacingsWide(PlanogramProduct product)
        {
            Int16 facingsWide = (OrientationType == PlanogramPositionOrientationType.Front0 ||
                                 OrientationType == PlanogramPositionOrientationType.Front90 ||
                                 OrientationType == PlanogramPositionOrientationType.Front180 ||
                                 OrientationType == PlanogramPositionOrientationType.Front270)
                                    ? FacingsWide
                                    : (Byte)0;
            if (GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main) == PlanogramProductMerchandisingStyle.Tray)
            {
                facingsWide *= product
                    .GetRotatedTraySize(GetAppliedOrientationType(product, PlanogramPositionBlockType.Main).ToPositionOrientationType())
                    .Wide;
            }
            return facingsWide;
        }

        #endregion

        #region Validation

        /// <summary>
        /// Returns a list of validation warnings for this position.
        /// </summary>
        public List<PlanogramValidationWarning> GetBrokenValidationWarnings(PlanogramMetadataDetails metaDetails)
        {
            List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();

            PlanogramProduct product = GetPlanogramProduct();
            PlanogramSubComponent component = GetPlanogramSubComponent();
            PlanogramSubComponentPlacement subPlacement = metaDetails.FindSubComponentPlacementForPosition(this);
            if (subPlacement == null) return warnings;

            PlanogramPositionDetails posDetails = metaDetails.FindPositionDetailsByPlanogramPositionId(this.Id);

            //Get info about the merch group this belongs to:
            PlanogramMetadataDetails.PlanogramMerchandisingGroupInfo parentMerchGroupInfo =
                metaDetails.GetMerchandisingGroupInfo(subPlacement.Id);


            #region Collisions

            // Collision validation. The position has at least one collision.
            if ((this.MetaIsPositionCollisions.HasValue) ? this.MetaIsPositionCollisions.Value : this.HasCollisions(metaDetails, subPlacement))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionHasCollisions, this));
            }

            #endregion

            #region Overfill

            // Position is outside merchandising space.
            if ((this.MetaIsOutsideMerchandisingSpace.HasValue) ?
                this.MetaIsOutsideMerchandisingSpace.Value : IsOutsideMerchandisingSpace(subPlacement, parentMerchGroupInfo, posDetails))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace, this));
            }

            // Peg is overfilled.
            if ((this.MetaIsPegOverfilled.HasValue) ? this.MetaIsPegOverfilled.Value : IsPegOverfilled(posDetails, product, subPlacement.SubComponent))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PegIsOverfilled, this));
            }

            #endregion

            #region Product Settings

            //No dimensions set for merch style
            if ((this.MetaIsInvalidMerchandisingStyle.HasValue) ? this.MetaIsInvalidMerchandisingStyle.Value : IsMerchStyleSettingsInvalid(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings, this));
            }

            //tray product on a hanging sub.
            if ((this.MetaIsHangingTray.HasValue) ? this.MetaIsHangingTray.Value : IsTrayOnHangingSubComponent(product, subPlacement.SubComponent))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionIsHangingTray, this));
            }

            //Hanging without peg
            if (IsHangingWithoutPeg(product, subPlacement.SubComponent))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionIsHangingWithoutPeg, this));
            }

            // Product Max Stack Exceeded.
            if ((this.MetaIsOverMaxStack.HasValue) ? this.MetaIsOverMaxStack.Value : IsMaxStackExceeded(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionExceedsMaxStack, this));
            }

            // Max right cap exceeded.
            if ((this.MetaIsOverMaxRightCap.HasValue) ? this.MetaIsOverMaxRightCap.Value : IsMaxRightCapExceeded(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionExceedsMaxRightCap, this));
            }

            // Max top cap exceeded.
            if ((this.MetaIsOverMaxTopCap.HasValue) ? this.MetaIsOverMaxTopCap.Value : IsMaxTopCapExceeded(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionExceedsMaxTopCap, this));
            }

            // Min deep not achieved.
            if ((this.MetaIsUnderMinDeep.HasValue) ? this.MetaIsUnderMinDeep.Value : IsMinDeepNotAchieved(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep, this));
            }

            // Max deep exceeded.
            if ((this.MetaIsOverMaxDeep.HasValue) ? this.MetaIsOverMaxDeep.Value : IsMaxDeepExceeded(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionExceedsMaxDeep, this));
            }

            // Cannot break tray back is ignored.
            if (IsCannotBreakTrayBackIgnored(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionCannotBreakTrayBack, this));
            }

            // Cannot break tray top is ignored
            if (IsCannotBreakTrayTopIgnored(product))
            {
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionCannotBreakTrayTop, this));
            }

            //Position peg holes don't align to pegboard peg holes
            if(ArePositionPegHolesValid(product, component)){
                warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionPegHolesAreInvalidRule, this));
            }

            // Cannot break tray up is ignored
            //if (this.IsCannotBreakTrayUpIgnored(product))
            //{
            //    warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionCannotBreakTrayUp, this));
            //}

            // Cannot break tray down is ignored
            //if (this.IsCannotBreakTrayDownIgnored(product))
            //{
            //    warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.PositionCannotBreakTrayDown, this));
            //}


            #endregion

            return warnings;
        }

        /// <summary>
        /// Returns whether this positions is outside 
        /// its placement merchandising space bounds.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Boolean IsOutsideMerchandisingSpace(
            PlanogramSubComponentPlacement parentSubPlacement,
            PlanogramMetadataDetails.PlanogramMerchandisingGroupInfo parentMerchGroupInfo,
            PlanogramPositionDetails positionDetails)
        {
            if (parentMerchGroupInfo == null || positionDetails == null) return false;


            //we do this in local space so that rotation does not skew the result.
            RectValue? merchandisingSpaceBounds = parentMerchGroupInfo.MerchandisingSpaceBounds;
            if (!merchandisingSpaceBounds.HasValue
                || merchandisingSpaceBounds.Value.Height.LessOrEqualThan(0)
                || merchandisingSpaceBounds.Value.Width.LessOrEqualThan(0)
                || merchandisingSpaceBounds.Value.Depth.LessOrEqualThan(0)) return false;

            //Get the bounding info for this position relative to the merch space.
            RectValue positionSpaceBounds = new RectValue(
                this.X + parentMerchGroupInfo.GetSubPlacementXOffset(parentSubPlacement.Id), this.Y, this.Z,
                 positionDetails.TotalSize.Width, positionDetails.TotalSize.Height, positionDetails.TotalSize.Depth);


            Boolean isHangingPosition = parentSubPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang
                || parentSubPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom;

            //  Return whether the position is fully inside the merchandising area of its component,
            //  OR if the position is hanging just ensure it is really hanging from the component (partially inside the merchandising area).
            return isHangingPosition
                       ? !merchandisingSpaceBounds.Value.IntersectsWith(positionSpaceBounds)
                       : !merchandisingSpaceBounds.Value.Contains(positionSpaceBounds);
        }

        /// <summary>
        ///     Returns whether this position's peg is overfilled for the product.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        ///     If the PegDepth is 0, it is not checked against and is considered as not set.
        ///     If position is not on a Hang-type sub-component, it is not checked against and is considered not set. 
        /// </remarks>
        private static Boolean IsPegOverfilled(PlanogramPositionDetails positionDetails, PlanogramProduct product, PlanogramSubComponent subComponent)
        {
            if (product.PegDepth.EqualTo(0)) return false;
            if (subComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang) return false;
            return positionDetails.TotalSize.Depth.GreaterThan(product.PegDepth);
        }

        /// <summary>
        ///     Returns whether the position is exceeding the max stack value for the product.
        /// </summary>
        /// <returns></returns>
        /// <remarks>If the MaxStack value is 0, it is not checked against and is considered as not set.</remarks>
        internal Boolean IsMaxStackExceeded(PlanogramProduct product)
        {
            return product.MaxStack != 0 && this.FacingsHigh > product.MaxStack;
        }

        /// <summary>
        ///     Determines whether any of the max right cap value has been exceeded.
        /// </summary>
        /// <returns></returns>
        /// <remarks>If a MaxCap value is 0, no excess cap is allowed.</remarks>
        internal Boolean IsMaxRightCapExceeded(PlanogramProduct product)
        {
            Byte maxRightCap = product.MaxRightCap;
            Int16 actualRightCap = this.FacingsXDeep > 0 && this.FacingsXHigh > 0
                ? this.FacingsXWide
                : (Int16)0;

            return actualRightCap > maxRightCap;
        }

        /// <summary>
        ///     Determines whether any of the max right cap value has been exceeded.
        /// </summary>
        /// <returns></returns>
        /// <remarks>If a MaxCap value is 0, no excess cap is allowed.</remarks>
        internal Boolean IsMaxTopCapExceeded(PlanogramProduct product)
        {
            Int16 actualTopCap = this.FacingsYDeep > 0 && this.FacingsYWide > 0
                ? this.FacingsYHigh
                : (Int16)0;

            return actualTopCap > product.MaxTopCap;
        }

        /// <summary>
        ///     Determines whether the min deep value has been achieved or not.
        /// </summary>
        /// <returns></returns>
        /// <remarks>If a MinDeep value is 0, it is not checked against, and is considered as not set.</remarks>
        private Boolean IsMinDeepNotAchieved(PlanogramProduct product)
        {
            return product.MinDeep != 0 && product.MinDeep > this.FacingsDeep;
        }

        /// <summary>
        ///     Determines whether the max deep value has been exceeded or not.
        /// </summary>
        /// <returns></returns>
        /// <remarks>If a MaxDeep value is 0, it is not checked against, and is considered as not set.</remarks>
        internal Boolean IsMaxDeepExceeded(PlanogramProduct product)
        {
            return product.MaxDeep != 0 && product.MaxDeep < this.FacingsDeep;
        }

        /// <summary>
        ///     Determines whether cannot break tray back has been ignored or not.
        /// </summary>
        /// <returns></returns>
        private Boolean IsCannotBreakTrayBackIgnored(PlanogramProduct product)
        {
            return product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray
                && !product.CanBreakTrayBack
                && this.MerchandisingStyleZ != PlanogramPositionMerchandisingStyle.Tray
                && this.MerchandisingStyleZ != PlanogramPositionMerchandisingStyle.Default
                && this.GetZBlockUnits() > 0;
        }

        /// <summary>
        ///     Determines whether cannot break tray top has been ignored or not.
        /// </summary>
        /// <returns></returns>
        private Boolean IsCannotBreakTrayTopIgnored(PlanogramProduct product)
        {
            return product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray
                && !product.CanBreakTrayTop
                && this.MerchandisingStyleY != PlanogramPositionMerchandisingStyle.Tray
                && this.MerchandisingStyleY != PlanogramPositionMerchandisingStyle.Default
                && this.GetYBlockUnits() > 0;
        }


        public Boolean ArePositionPegHolesValid(PlanogramProduct product, PlanogramSubComponent component)
        {

           switch(product.NumberOfPegHoles)
            {
                case 2:
                    if (product.PegX == 0 && product.PegX2 == 0)
                    {
                        return false;
                    }
                    else 
                    {
                        if (((product.PegX2 - product.PegX) == component.MerchConstraintRow1SpacingX)  && (product.PegY == product.PegY2))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                 
                case 3:
                    if (product.PegX == 0 && product.PegX2 == 0 && product.PegX3 == 0)
                    {
                        return false;
                    }
                    else
                    {
                        if ((((product.PegX2 - product.PegX) == component.MerchConstraintRow1SpacingX) && ((product.PegX3 - product.PegX2) == component.MerchConstraintRow1SpacingX))
                            && ((product.PegY == product.PegY2) && (product.PegY2 == product.PegY3)))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
               }

            return false;
       }

        /// <summary>
        ///     Determines whether cannot break tray up has been ignored or not.
        /// </summary>
        /// <returns></returns>
        private Boolean IsCannotBreakTrayUpIgnored(PlanogramProduct product)
        {
            return product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray
                && !product.CanBreakTrayUp
                && this.MerchandisingStyleX != PlanogramPositionMerchandisingStyle.Tray
                && this.MerchandisingStyleX != PlanogramPositionMerchandisingStyle.Default
                && this.GetXBlockUnits() > 0
                && this.IsXPlacedLeft;
        }

        /// <summary>
        ///     Determines whether cannot break tray down has been ignored or not.
        /// </summary>
        /// <returns></returns>
        private Boolean IsCannotBreakTrayDownIgnored(PlanogramProduct product)
        {
            return product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray
                && !product.CanBreakTrayDown
                && this.MerchandisingStyleX != PlanogramPositionMerchandisingStyle.Tray
                && this.MerchandisingStyleX != PlanogramPositionMerchandisingStyle.Default
                && this.GetXBlockUnits() > 0
                && !this.IsXPlacedLeft;
        }

        /// <summary>
        /// Returns true if the applied merch styles have
        /// invalid dimensions or settings
        /// </summary>
        /// <returns></returns>
        private Boolean IsMerchStyleSettingsInvalid(PlanogramProduct product)
        {
            foreach (PlanogramPositionBlockType block in Enum.GetValues(typeof(PlanogramPositionBlockType)))
            {
                //don't check if the block is not in use.
                if (!this.IsBlockActive(block)) continue;

                PlanogramProductMerchandisingStyle appliedStyle = this.GetAppliedMerchandisingStyle(product, block);

                switch (appliedStyle)
                {
                    case PlanogramProductMerchandisingStyle.Unit:
                        // A product should not have 0 width, height or depth and the UI does not allow it,
                        // but should check as the values in the data base could be 0

                        if (product.Height == 0 || product.Width == 0 || product.Depth == 0) return true;

                        break;

                    case PlanogramProductMerchandisingStyle.Tray:
                        if (product.TrayHeight == 0 || product.TrayWidth == 0 || product.TrayDepth == 0
                            || product.TrayHigh == 0 || product.TrayWide == 0 || product.TrayDeep == 0)
                            return true;
                        break;

                    case PlanogramProductMerchandisingStyle.PointOfPurchase:
                        if (product.PointOfPurchaseHeight == 0 || product.PointOfPurchaseWidth == 0 || product.PointOfPurchaseDepth == 0) return true;
                        break;

                    case PlanogramProductMerchandisingStyle.Display:
                        if (product.DisplayHeight == 0 || product.DisplayWidth == 0 || product.DisplayDepth == 0) return true;
                        break;

                    case PlanogramProductMerchandisingStyle.Case:
                        if (product.CaseHeight == 0 || product.CaseWidth == 0 || product.CaseDepth == 0
                            || product.CaseWide == 0 || product.CaseDeep == 0 || product.CaseHigh == 0)
                            return true;
                        break;

                    case PlanogramProductMerchandisingStyle.Alternate:
                        if (product.AlternateHeight == 0 || product.AlternateWidth == 0 || product.AlternateDepth == 0) return true;
                        break;

                    default:
                        Debug.Fail("Case not handled");
                        break;

                }

            }

            return false;
        }

        /// <summary>
        /// Returns true if we are a tray product on a hanging subcomponent
        /// </summary>
        private Boolean IsTrayOnHangingSubComponent(PlanogramProduct product, PlanogramSubComponent sub)
        {
            if (sub.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang
                && sub.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom)
            {
                //can only break this rule if we are on a hanging type.
                return false;
            }

            foreach (PlanogramPositionBlockType block in Enum.GetValues(typeof(PlanogramPositionBlockType)))
            {
                //don't check if the block is not in use.
                if (!this.IsBlockActive(block)) continue;

                if (this.GetAppliedMerchandisingStyle(product, block) == PlanogramProductMerchandisingStyle.Tray) return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the position is currently hanging
        /// but the product has no peg details specified.
        /// </summary>
        private static Boolean IsHangingWithoutPeg(PlanogramProduct product, PlanogramSubComponent sub)
        {
            if (sub.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang
                && sub.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom)
            {
                //can only break this rule if we are on a hanging type.
                return false;
            }

            if (product.NumberOfPegHoles == 0) return true;
            if (product.PegDepth == 0) return true;

            return false;
        }

        /// <summary>
        /// Returns true if the position sits outside of its blocking area.
        /// </summary>
        private Boolean IsOutsideOfBlockSpace(PlanogramProduct product, PlanogramMetadataDetails context)
        {
            if (this.SequenceColour == null) return false;
            Int32? colourToMatch = this.SequenceColour;
            PlanogramBlocking blocking = null;
            PlanogramBlockingList blockingList = this.Parent.Blocking;
            Single totalAreaOfIntersectionPercentage = new Single();

            blocking = blockingList.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final);
            if (blocking == null)
            {
                blocking = blockingList.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied);
            }
            if (blocking == null)
            {
                blocking = blockingList.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial);
            }
            if (blocking == null) return false;

            PlanogramBlockingGroup blockingGroup = blocking.Groups.FirstOrDefault(g => g.Colour.Equals(colourToMatch));
            if (blockingGroup == null) return false;

            RectValue designBounds = context.GetDesignViewBoundsByPositionId(this.Id);
            List<RectValue> locationList = new List<RectValue>();
            foreach (PlanogramBlockingLocation location in blockingGroup.GetBlockingLocations())
            {
                //Get the 2D bounding rectangle of the location
                RectValue locationRect = new RectValue(
                location.X * context.PlanogramDesignWidth,
                location.Y * context.PlanogramDesignHeight,
                0,
                location.Width * context.PlanogramDesignWidth,
                location.Height * context.PlanogramDesignHeight,
                0);

                locationList.Add(locationRect);
            }

            foreach (RectValue locationRect in locationList)
            {
                RectValue? intersecting = designBounds.Intersect(locationRect);
                if (intersecting == null) continue;
                Single areaOfIntersecting = intersecting.Value.Height * intersecting.Value.Width;
                Single totalDesignArea = designBounds.Height * designBounds.Width;
                if (totalDesignArea == 0) continue;
                Single percentageOfIntersecting = areaOfIntersecting / totalDesignArea;
                totalAreaOfIntersectionPercentage += percentageOfIntersecting;
            }

            if (totalAreaOfIntersectionPercentage.GreaterOrEqualThan((Single)percentageSpaceAllowence))
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Position Options

        #region Units

        #region IncreaseUnits

        /// <summary>
        ///     Increases the units of this position in all three dimensions where possible one axis at a time, based on a
        ///     prioritised order.
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement.</param>
        /// <param name="merchandisingGroup">[Optional] The merch group to use when working with the position.</param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns>True if units was successfully increased in at least one dimension.</returns>
        /// <remarks>If a merchgroup is provided then this method does not handle the applying the edit.</remarks>
        public Boolean IncreaseUnits(PlanogramProduct product,
                                     PlanogramSubComponentPlacement subComponentPlacement,
                                     PlanogramMerchandisingGroup merchandisingGroup,
                                     IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            //  If a Merchandising Group was not given, create an internal one.
            //  Make sure to apply changes if the Merchandising Group is internal.
            Boolean isInternalMerchGroup = merchandisingGroup == null;
            if (isInternalMerchGroup)
            {
                // We are only allowed to apply an edit when the merchgroup is null. 
                merchandisingGroup =
                    PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement.GetCombinedWithList());
            }

            try
            {
                //  Get the stack containing the axes to be increased in the order they should be attempted in.
                Stack<AxisType> increaseAxesStack =
                    subComponentPlacement.GetIncreaseAxesStack(merchandisingGroup.LookupAxesStackedValue());
                while (increaseAxesStack.Count > 0)
                {
                    // Re-get the position instance here, because apply edit could have been called on the merch group.
                    PlanogramPosition positionInstanceToEdit =
                        merchandisingGroup.PositionPlacements.First(pp => pp.Position.Id.Equals(Id)).Position;

                    Boolean isLast = increaseAxesStack.Count == 1;
                    AxisType currentAxis = increaseAxesStack.Pop();

                    //  If not the last axis but overfilled, just skip it.
                    if (!isLast &&
                        merchandisingGroup.IsOverfilled(currentAxis)) continue;

                    //  If it is not possible to increase units in the current axis, continue on to the next.
                    Int32 startingUnitCount = positionInstanceToEdit.GetTotalUnitCount();
                    if (!positionInstanceToEdit.IncreaseUnits(currentAxis, product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer))
                        continue;

                    //  If the merch group has peg holes, process the group so that every placement is on each available peg.
                    if (merchandisingGroup.HasRow1PegHoles ||
                        merchandisingGroup.HasRow2PegHoles)
                        merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                    //  If this is the last axis, OR if the increase did not cause overfill return success.
                    if (isLast || !merchandisingGroup.IsOverfilled(currentAxis))
                    {
                        if (isInternalMerchGroup) merchandisingGroup.ApplyEdit();

                        if (positionInstanceToEdit.GetTotalUnitCount() < startingUnitCount) continue;

                        //  Return success.
                        return true;
                    }

                    // Could not increase units, undo and try the next axis if there is one.
                    positionInstanceToEdit.DecreaseUnits(currentAxis, product, subComponentPlacement, merchandisingGroup);
                }

                //  Return failure.
                return false;
            }
            finally
            {
                //  Dispose of the Merchandising Group if it is internal.
                //  This line will be called always, even if there is an exception thrown.
                if (isInternalMerchGroup) merchandisingGroup.Dispose();
            }
        }

        /// <summary>
        ///     Increase units for this instance on the given <paramref name="axis" />.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType" /> on which units will be increased.</param>
        /// <param name="product">The <see cref="PlanogramProduct" /> that is asociated with this instance.</param>
        /// <param name="subComponentPlacement">The <see cref="PlanogramSubComponentPlacement" /> that contains this instance.</param>
        /// <param name="merchandisingGroup">The <see cref="PlanogramMerchandisingGroup" /> to use when increasing units.</param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns><c>True</c> if the increase was succesful; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     Even if the increase is successful, it might not be valid as the last axis is always increased, even if
        ///     overfilling.
        /// </remarks>
        public Boolean IncreaseUnits(
            AxisType axis,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup,
            IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            Boolean success;
            switch (axis)
            {
                case AxisType.X:
                    success = IncreaseUnitsX(product, subComponentPlacement, merchandisingGroup);
                    break;
                case AxisType.Y:
                    success = IncreaseUnitsY(product, subComponentPlacement, merchandisingGroup);
                    break;
                case AxisType.Z:
                    success = IncreaseUnitsZ(product, subComponentPlacement, merchandisingGroup);
                    break;
                default:
                    Debug.Fail("Unkown Axis Type when processing PlanogramPosition.IncreaseUnits");
                    success = false;
                    break;
            }
            if (success) EnforceRules(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, axis);
            return success;
        }

        /// <summary>
        ///     Increases the units of this position in the x axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The merch group being processed</param>
        /// <returns>True if units was successfully increased, else false</returns>
        private Boolean IncreaseUnitsX(PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            //Tray Products get their units increased based on whether they can break up or down.
            //Anything else has generic behaviour of increasing main positions facings wide by 1.

            if (product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray))
                return IncreaseTrayUnitsX(product);

            // Do not exceed 1 high on hang from bottom merch style.
            if (subComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
            {
                if ((FacingsWide + 1) >= 2) return false;
            }

            //Set the flag to indicate if top caps are allowed - 
            // must have max top cap > 0 and not be on a hanging sub.
            Boolean canPlaceXCap =
                product.MaxRightCap != 0
                && merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang
                && merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom;

            // Generic behaviour is to increase facings/caps based on whatever would be the smallest increase of space in that axis while still increasing units.

            #region Add first cap if doens't exist and will fit.

            // First thing to check is that if we have a facing but no cap, then the next step is to check whether we can just add a cap.
            if (FacingsWide != 0 && FacingsXWide == 0 && canPlaceXCap)
            {
                // Apply a single cap so that details can calculate size.
                FacingsXWide = 1;

                // Need to apply the squeeze values from the main position as the value created for XRotatedUnitSize will be incorrect if squeeze has been applied
                DepthSqueezeX = DepthSqueeze;
                VerticalSqueezeX = VerticalSqueeze;

                // Need to set the depth and height to a correct value that doesn't overflow. 
                // If it cannot be placed without overflowing remove the cap and we just add a facing later.
                PlanogramPositionDetails detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                this.FacingsXDeep = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Z, ref detailsForCap);
                this.FacingsXHigh = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Y, ref detailsForCap);


                // If a cap would be bigger than the main position facing or having one cap would overflow then don't add the cap.
                detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                //get the width of the extra full facing - this considers that it may be smaller if nested.
                Single additionalFullFacingWidth = (detailsForCap.MainRotatedNestingSize.Width.GreaterThan(0) ? detailsForCap.MainRotatedNestingSize.Width : detailsForCap.MainRotatedUnitSize.Width);

                if (detailsForCap.XRotatedUnitSize.Width >= additionalFullFacingWidth
                    || FacingsXDeep == 0
                    || FacingsXHigh == 0)
                {
                    // Cap is bigger than a single facing or it overflows so we actually never want the cap.
                    FacingsXDeep = 0;
                    FacingsXHigh = 0;
                    FacingsXWide = 0;
                }
                else
                {
                    // We keep the cap and are done.
                    // Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                    detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref detailsForCap, alwaysRecalc: true);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref detailsForCap);
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref detailsForCap);

                    return true;
                }
            }

            #endregion

            // At This point we know we don't want a first cap adding.
            // But do we want to increase the cap high or make into a full facing - Depends if adding another cap would use more space than a full facing.
            PlanogramPositionDetails details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
            Int32 absoluteStartUnits = details.TotalUnitCount;

            // We always increase until our new unit count is greater than our original unit count.
            while (details.TotalUnitCount <= absoluteStartUnits)
            {
                #region Increase Caps if would be less than one full facing

                //get the width of the extra full facing - this considers that it may be smaller if nested.
                Single additionalFullFacingWidth = (FacingsXWide != 0) ?
                    (details.MainRotatedNestingSize.Width.GreaterThan(0) ? details.MainRotatedNestingSize.Width : details.MainRotatedUnitSize.Width)
                    : details.MainRotatedUnitSize.Width;

                //and the expected width of the cap if another facing were added.
                Single anticipatedXSize = details.GetAnticipatedBlockSize(PlanogramPositionBlockType.X, AxisType.X, FacingsXWide + 1);

                if (canPlaceXCap
                    && product.MaxRightCap >= (FacingsXWide + 1)
                    && anticipatedXSize < additionalFullFacingWidth)
                {
                    // Check whether we are less than the original units (musn't be).
                    details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                    //We keep increasing 'units' until we are greater than our original value.
                    while ((details.TotalUnitCount > absoluteStartUnits) == false)
                    {
                        anticipatedXSize = details.GetAnticipatedBlockSize(PlanogramPositionBlockType.X, AxisType.X, FacingsXWide + 1);

                        // We should keep adding a cap while we are less than a facing high
                        if ((FacingsXWide + 1) <= product.MaxRightCap/*is not over max cap*/
                            && (anticipatedXSize < additionalFullFacingWidth)/*is still smaller than a full facing*/)
                        {
                            FacingsXWide++;
                            this.FacingsXDeep = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Z, ref details);
                            this.FacingsXHigh = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Y, ref details);

                            details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                            // If we are to big then remove cap and revert to just increasing the facing.
                            if (details.XRotatedUnitSize.Depth > details.MainTotalSize.Depth
                               || details.XRotatedUnitSize.Height > details.MainTotalSize.Height
                               || (details.XTotalSize.Width > additionalFullFacingWidth))
                            {
                                FacingsXWide = 0;
                                FacingsWide++;
                            }
                        }
                        else
                        {
                            // Turn the caps into a full facing
                            FacingsXWide = 0;
                            FacingsWide++;
                        }

                        //Update.
                        details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    }

                    //Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details, alwaysRecalc: true);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details);
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details);

                    //Generic increase units was possilble.
                    details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                    if (details.TotalUnitCount >= absoluteStartUnits)
                    {
                        // We have made an overall increase in units.
                        return true;
                    }

                    //If we have still not had an overall increase in units then continue
                    continue;
                }

                #endregion

                #region Just increase a single facing high

                //It is possible to reach this point still with a capping too many...

                FacingsXWide = 0;
                FacingsWide++;

                // Cannot exceed max stack
                //This is commented out in 820CP03?
                //if (IsMaxStackExceeded(product))
                //{
                //    FacingsWide--;
                //    return false;
                //}

                details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                //Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details, alwaysRecalc: true);
                UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details);
                UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details);

                details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                #endregion
            }

            // Once we have broken out of the while loop we know overall units has increased.
            return true;
        }

        private Boolean IncreaseTrayUnitsX(PlanogramProduct product)
        {
            //  Get the units on each axis, accounting for any rotation.
            WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(OrientationType);

            // Enforce the tray product placed into a tray correctly. Aka if enough high/wide/deep then should be in one. 
            TryMakeTray(product, rotatedTraySize);

            // If we can't break down or up then simply increase facing wide (current position must be a tray).
            if (!product.CanBreakTrayDown &&
                !product.CanBreakTrayUp)
            {
                MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                AddFacings(AxisType.X, 1);

                //Increase the y & z Caps also:
                UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);
                return true;
            }


            // If is smaller than a tray and we cannot become a tray just increase facing wide.
            if (!IsAppliedMerchandisingStyleAnyOf(product, PlanogramProductMerchandisingStyle.Tray))
            {
                // Must reach tray high and deep before increasing wide could make a tray.
                if (AxisType.X.GetNormals().Any(type => rotatedTraySize.GetSize(type) > GetFacings(type)))
                {
                    AddFacings(AxisType.X, 1);
                    return true;
                }
            }


            // If we can't break down but can break up we increase caps until caps wide == number in a tray at which point
            // We switch the caps to a single tray facing AND we switch to a tray type.
            if (product.CanBreakTrayUp &&
                !product.CanBreakTrayDown)
            {
                // If we are unable to break the tray down then enforce the main block as a tray:
                MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                if (GetFacingCapUnits(AxisType.X) + 1 < rotatedTraySize.GetSize(AxisType.X))
                {
                    FacingsXWide++;
                    FacingsXHigh = Convert.ToInt16(rotatedTraySize.High * FacingsHigh);
                    FacingsXDeep = Convert.ToInt16(rotatedTraySize.Deep * FacingsDeep);
                    MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                    OrientationTypeX = product.OrientationType.ToPositionOrientationType();
                    return true;
                }
                MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                SetFacingCapUnits(AxisType.X, 0);

                if (GetFacings(AxisType.X) == 0)
                {
                    SetFacings(AxisType.X, 1);
                    return true;
                }

                AddFacings(AxisType.X, 1);

                // Increase the y & z Caps also:
                UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                return true;
            }

            // When we cannot break up but just down max out at one facing wide:
            if (product.CanBreakTrayDown &&
                !product.CanBreakTrayUp)
            {
                // If we have a total units wide less than the product.tray wide we should just increase normal unit facings wide 
                // (When we are less than a single tray).
                if (MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit &&
                    GetFacings(AxisType.X) < rotatedTraySize.GetSize(AxisType.X))
                {
                    // If should unit facings should become a single facing
                    if ((GetFacings(AxisType.X) + 1) == rotatedTraySize.GetSize(AxisType.X))
                    {
                        SetFacings(AxisType.X, 1);
                        // We want to update, unless that would leave us without a facing in which case we leave alone
                        foreach (AxisType axisType in AxisType.X.GetNormals())
                        {
                            Int32 targetFacings = GetFacings(axisType) / rotatedTraySize.GetSize(axisType);
                            if (targetFacings >= 1) SetFacings(axisType, targetFacings);
                        }

                        MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                        return true;
                    }
                    // Just increase facings wide, should stay as a unit.
                    AddFacings(AxisType.X, 1);
                    return true;
                }
                // We reach here when we have at least one full main tray. 
                // As we cannot break tray up then we have to go and increase facings wide only.
                AddFacings(AxisType.X, 1);

                //Increase the Y & Z Caps also:
                UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                return true;
            }

            // If we can do both?
            if (product.CanBreakTrayDown &&
                product.CanBreakTrayUp)
            {
                // If merch style is a unit then we have less than one single tray:
                if (MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
                {
                    // We increase until we make a single tray
                    if (GetFacings(AxisType.X) + 1 == rotatedTraySize.GetSize(AxisType.X))
                    {
                        SetFacings(AxisType.X, 1);
                        // We want to update, unless that would leave us without a facing in which case we leave alone
                        Int16 trayFacingsHigh = Convert.ToInt16(FacingsHigh / rotatedTraySize.High);
                        if (trayFacingsHigh >= 1)
                        {
                            FacingsYHigh = Convert.ToInt16(FacingsHigh - trayFacingsHigh * rotatedTraySize.High);

                            FacingsHigh = trayFacingsHigh;
                        }
                        Int16 trayFacingsDeep = Convert.ToInt16(FacingsDeep / rotatedTraySize.Deep);
                        if (trayFacingsDeep >= 1)
                        {
                            FacingsZDeep = Convert.ToInt16(FacingsDeep - trayFacingsDeep * rotatedTraySize.Deep);

                            FacingsDeep = trayFacingsDeep;
                        }

                        MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                        OrientationTypeX = product.OrientationType.ToPositionOrientationType();
                        MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                        OrientationTypeY = product.OrientationType.ToPositionOrientationType();
                        MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                        OrientationTypeZ = product.OrientationType.ToPositionOrientationType();

                        FacingsYWide = Convert.ToInt16(FacingsWide * rotatedTraySize.Wide);
                        FacingsYDeep = Convert.ToInt16(FacingsDeep * rotatedTraySize.Deep);
                        FacingsZWide = Convert.ToInt16(FacingsWide * rotatedTraySize.Wide);
                        FacingsZHigh = Convert.ToInt16(FacingsHigh * rotatedTraySize.High);

                        MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

                        // Update the caps
                        UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);
                        return true;
                    }
                    // Just increase facings wide, should stay as a unit.
                    AddFacings(AxisType.X, 1);
                    return true;
                }
                if ((FacingsXWide + 1) == rotatedTraySize.Wide)
                {
                    FacingsWide++;
                    MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                    SetFacingCapUnits(AxisType.X, 0);

                    // Increase the y & z Caps also:
                    UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                    return true;
                }
                FacingsXWide++;
                FacingsXHigh = Convert.ToInt16(rotatedTraySize.High * FacingsHigh);
                FacingsXDeep = Convert.ToInt16(rotatedTraySize.Deep * FacingsDeep);
                MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                OrientationTypeX = product.OrientationType.ToPositionOrientationType();

                //Increase the Y & Z Caps also:
                UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Increases the units of this position in the y axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The merch group being processed</param>
        /// <returns>True if units was successfully increased, else false</returns>
        private Boolean IncreaseUnitsY(PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (TrayPositionHelper.YAxisManager.IncreaseUnits(product, this, subComponentPlacement,
                merchandisingGroup)) return true;

            #region Generic Behaviour

            //Set the flag to indicate if top caps are allowed - 
            // must have max top cap > 0 and not be on a hanging sub.
            Boolean canPlaceTopCap =
                product.MaxTopCap != 0
                && merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang
                && merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom;

            // Do not exceed 1 high on hang from bottom merch style.
            if (merchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom)
                if ((FacingsHigh + 1) >= 2) return false;

            // Generic behaviour is to increase facings/caps based on whatever would be the smallest increase of space in that axis while still increasing units.

            #region Add first cap if doens't exist and will fit.

            // First thing to check is that if we have a facing but no cap, then the next step is to check whether we can just add a cap.
            if (canPlaceTopCap && FacingsHigh != 0 && FacingsYHigh == 0)
            {
                //Apply a single cap so that details can calculate size.
                FacingsYHigh = 1;
                //FacingsYDeep = 1;
                //FacingsYWide = 1;

                // Need to apply the squeeze values from the main position as the value created for YRotatedUnitSize will be incorrect 
                // if squeeze has been applied which results in no FacingsYWide value.
                HorizontalSqueezeY = HorizontalSqueeze;
                DepthSqueezeY = DepthSqueeze;

                // Need to set the depth and wide to a correct value that doesn't overflow. 
                // If it cannot be placed without overflowing remove the cap and we just add a facing later.
                PlanogramPositionDetails detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                this.FacingsYDeep = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.Z, ref detailsForCap);
                this.FacingsYWide = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.X, ref detailsForCap);

                // If a cap would be bigger than the main position facing or having one cap would overflow then don't add the cap.
                detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                //get the height of the extra full facing - this considers that it may be smaller if nested.
                Single additionalFullFacingHeight = (detailsForCap.MainRotatedNestingSize.Height.GreaterThan(0) ? detailsForCap.MainRotatedNestingSize.Height : detailsForCap.MainRotatedUnitSize.Height);

                if (((product.MaxStack == 0 || FacingsHigh < product.MaxStack) && detailsForCap.YRotatedUnitSize.Height >= additionalFullFacingHeight)
                    || FacingsYDeep == 0
                    || FacingsYWide == 0)
                {
                    // Cap is bigger than a single facing or it overflows so we actually never want the cap.
                    FacingsYDeep = 0;
                    FacingsYHigh = 0;
                    FacingsYWide = 0;
                }
                else
                {
                    // We keep the cap and are done.
                    // Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                    detailsForCap = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref detailsForCap, alwaysRecalc: true);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref detailsForCap);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref detailsForCap);

                    return true;
                }
            }

            #endregion

            // At This point we know we don't want a first cap adding.
            // But do we want to increase the cap high or make into a full facing - Depends if adding another cap would use more space than a full facing.
            PlanogramPositionDetails details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
            Int32 absoluteStartUnits = details.TotalUnitCount;

            Int16 facingsYDeep = FacingsYDeep;
            Int16 facingsYWide = FacingsYWide;

            // We always increase until our new unit count is greater than our original unit count.
            while (details.TotalUnitCount <= absoluteStartUnits)
            {
                #region Increase Caps if would be less than one full facing

                //get the height of the extra full facing - this considers that it may be smaller if nested.
                Single additionalFullFacingHeight = (FacingsYHigh != 0) ?
                    (details.MainRotatedNestingSize.Height.GreaterThan(0) ? details.MainRotatedNestingSize.Height : details.MainRotatedUnitSize.Height)
                    : details.MainRotatedUnitSize.Height;

                //and the expected height of the y block if another facing were added.
                Single anticipatedYSize = details.GetAnticipatedBlockSize(PlanogramPositionBlockType.Y, AxisType.Y, FacingsYHigh + 1);


                if (canPlaceTopCap
                   && (FacingsHigh >= product.MaxStack || (anticipatedYSize < additionalFullFacingHeight))
                   && product.MaxTopCap >= (FacingsYHigh + 1))
                {
                    if (FacingsYHigh == 0)
                    {
                        // Since we are now attempting to increase top caps we need to set original facing y deep \ y wide to save having to recalculate this
                        FacingsYHigh = 1;
                        FacingsYDeep = facingsYDeep;
                        FacingsYWide = facingsYWide;
                    }

                    // Check whether we are less than the original units (musn't be).
                    PlanogramPositionDetails detailsAfterChange = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                    //We keep increasing 'units' until we are greater than our original value.
                    while ((detailsAfterChange.TotalUnitCount > absoluteStartUnits) == false)
                    {
                        // We should keep adding a cap while we are less than a facing high or if the max stack has been met
                        anticipatedYSize = details.GetAnticipatedBlockSize(PlanogramPositionBlockType.Y, AxisType.Y, FacingsYHigh + 1);

                        Boolean shouldAddUnit =
                            ((product.MaxStack == 0 || this.FacingsHigh < product.MaxStack)
                            && (anticipatedYSize.GreaterOrEqualThan(additionalFullFacingHeight)));


                        if (!shouldAddUnit && (FacingsYHigh + 1) <= product.MaxTopCap)
                        {
                            FacingsYHigh++;

                            detailsAfterChange = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                            // If we are to big then remove cap and revert to just increasing the facing.
                            if (detailsAfterChange.YRotatedUnitSize.Depth > detailsAfterChange.MainTotalSize.Depth
                                || detailsAfterChange.YRotatedUnitSize.Width > detailsAfterChange.MainTotalSize.Width
                                || ((product.MaxStack == 0 || this.FacingsHigh < product.MaxStack) &&
                                    (detailsAfterChange.YTotalSize.Height > additionalFullFacingHeight))
                                )
                            {
                                FacingsYHigh = 0;
                                FacingsHigh++;
                            }
                        }
                        else
                        {
                            // Turn the caps into a full facing
                            FacingsYHigh = 0;
                            FacingsHigh++;
                        }

                        //Update.
                        detailsAfterChange = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    }

                    //Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref detailsAfterChange, alwaysRecalc: true);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref detailsAfterChange);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref detailsAfterChange);

                    //Generic increase units was possilble.
                    detailsAfterChange = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    if (detailsAfterChange.TotalUnitCount >= absoluteStartUnits)
                    {
                        // We have made an overall increase in units.
                        return true;
                    }
                }

                #endregion

                #region Just increase a single facing high

                else
                {
                    //It is possible to reach this point still with a capping too many...
                    Int16 prevFacingsYHigh = this.FacingsYHigh;

                    FacingsYHigh = 0;
                    FacingsHigh++;

                    // Cannot exceed max stack
                    if (IsMaxStackExceeded(product))
                    {
                        this.FacingsHigh--;
                        this.FacingsYHigh = prevFacingsYHigh;
                        return false;
                    }

                    details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                    //Once we have increased units in the direction we are increasing in we will want to ensure any caps on the other axis are updated to match the new size.
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details, alwaysRecalc: true);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details);


                    details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                }

                #endregion
            }

            // Once we have broken out of the while loop we know overall units has increased.
            return true;

            #endregion
        }

        /// <summary>
        ///     Increases the units of this position in the z axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">[Optional] The merch group to use when working with the position.</param>
        /// <returns>True if units was successfully increased, else false</returns>
        private Boolean IncreaseUnitsZ(PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup)
        {
            if (TrayPositionHelper.ZAxisManager.IncreaseUnits(product, this, subComponentPlacement,
               merchandisingGroup)) return true;

            #region Generic Behaviour

            #region Peg Depth

            //If product has a peg depth and we are hanging then we use the peg depth to determine the maximum depth else we use merch depth.
            if (product.NumberOfPegHoles > 0 &&
                product.PegDepth > 0
                &&
                subComponentPlacement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
            {
                // As we are on a peg we cannot increase past the depth of the peg.
                PlanogramPositionDetails startingDetailsOfThisPosition =
                    PlanogramPositionDetails.NewPlanogramPositionDetails(this,
                        product,
                        subComponentPlacement,
                        merchandisingGroup);
                if ((FacingsDeep + 1) * startingDetailsOfThisPosition.MainRotatedUnitSize.Depth > product.PegDepth)
                    return false;
            }
            else if (subComponentPlacement.SubComponent.MerchandisableDepth != 0)
            {
                // The peg depth either doesn't exist or doesn't apply AND we have specified a merch depth.
                PlanogramPositionDetails startingDetailsOfThisPosition =
                    PlanogramPositionDetails.NewPlanogramPositionDetails(this,
                        product,
                        subComponentPlacement,
                        merchandisingGroup);
                if ((FacingsDeep + 1) * startingDetailsOfThisPosition.MainRotatedUnitSize.Depth >
                    subComponentPlacement.SubComponent.MerchandisableDepth)
                    return false;
            }

            #endregion

            // Don't exceed max deep!
            if ((product.MaxDeep != 0 && (FacingsDeep + 1) >= (product.MaxDeep + 1))) return false;

            FacingsDeep++;

            //Once we have increased units we need to ensure any caps on the other axis are updated to match the new size.
            PlanogramPositionDetails details = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
            UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details, alwaysRecalc: true);
            UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details);
            UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details);

            return true;

            #endregion
        }

        #endregion

        #region DecreaseUnits

        /// <summary>
        /// Decreases the units of this position in all three dimensions.
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">[Optional] The merch group to use when working with the position.</param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns>True if units was successfully decreased in at least one dimension.</returns>
        public Boolean DecreaseUnits(PlanogramProduct product,
                                     PlanogramSubComponentPlacement subComponentPlacement,
                                     PlanogramMerchandisingGroup merchandisingGroup,
                                     IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            //  If a Merchandising Group was not given, create an internal one.
            //  Make sure to apply changes if the Merchandising Group is internal.
            Boolean isInternalMerchGroup = false;
            if (merchandisingGroup == null)
            {
                // We are only allowed to apply an edit when the merchgroup is null. 
                isInternalMerchGroup = true;
                merchandisingGroup =
                    PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(
                        subComponentPlacement.GetCombinedWithList());
            }

            try
            {
                PlanogramPosition positionInstanceToEdit =
                    merchandisingGroup.PositionPlacements.First(pp => pp.Position.Id.Equals(this.Id)).Position;

                //  Get the stack containing the axes to be increased in the order they should be attempted in.
                //  NB Do not reverse the stack before using it as the priority is reversed for DecreaseUnits and the 
                //  IEnumerable will be reversed when it is added to the stack.
                var decreaseAxesStack =
                    new Stack<AxisType>(
                        subComponentPlacement.GetIncreaseAxesStack(merchandisingGroup.LookupAxesStackedValue()));
                while (decreaseAxesStack.Count > 0)
                {
                    AxisType currentAxis = decreaseAxesStack.Pop();

                    //  If there are not caps and minimum facings already, there is nothing to decrease on the current axis.
                    if (positionInstanceToEdit.GetFacingCapUnits(currentAxis) <= 0 &&
                        positionInstanceToEdit.GetFacings(currentAxis) <= 1) continue;

                    Int32 startingTotalUnitCount = positionInstanceToEdit.GetTotalUnitCount();

                    Boolean axisDecreased = positionInstanceToEdit.DecreaseUnits(currentAxis, product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer);

                    if (isInternalMerchGroup) merchandisingGroup.ApplyEdit();

                    if (positionInstanceToEdit.GetTotalUnitCount() >= startingTotalUnitCount) return false;

                    if (axisDecreased) return true;
                }

                //  Return failure.
                return false;
            }
            finally
            {
                //  Dispose of the Merchandising Group if it is internal.
                //  This line will be called always, even if there is an exception thrown.
                if (isInternalMerchGroup) merchandisingGroup.Dispose();
            }
        }

        /// <summary>
        /// Decrease units for this instance on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="product"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup">The merch group to which the placement belongs</param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns></returns>
        public Boolean DecreaseUnits(
            AxisType axis,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup,
            IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            Boolean success;
            switch (axis)
            {
                case AxisType.X:
                    success = DecreaseUnitsX(product, subComponentPlacement, merchandisingGroup);
                    break;
                case AxisType.Y:
                    success = DecreaseUnitsY(product, subComponentPlacement, merchandisingGroup);
                    break;
                case AxisType.Z:
                    success = DecreaseUnitsZ(product, subComponentPlacement, merchandisingGroup);
                    break;
                default:
                    Debug.Fail("Unkown Axis Type when processing PlanogramPosition.DecreaseUnits");
                    success = false;
                    break;
            }
            if (success) EnforceRules(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, axis);
            return success;
        }

        /// <summary>
        /// Decrease the units of this position in the x axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The merch group to which the placement belongs</param>
        /// <returns>True if successful, else false</returns>
        private Boolean DecreaseUnitsX(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            // This method needs to decrease the 'units' X based on the type of products being handled. 
            // Currently handles tray products, and generic units.

            #region TrayProduct Rules

            if (product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray))
            {
                // At this point we know if the Product is a tray product but how is the actual position merchandised?
                Boolean isPositionCurrentlyAUnit = (this.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit);

                // We should get the rotated tray wide dimensions.
                WideHighDeepValue rotatedTraySize = product.GetRotatedTraySize(OrientationType);

                // If we are a single unit and a tray then we can never decrease further.
                if (isPositionCurrentlyAUnit && this.FacingsWide <= 1) return false;

                // If we are a single tray with single facing wide and cannot face down or up then we can never decrease further.
                if (!isPositionCurrentlyAUnit &&
                    !product.CanBreakTrayDown &&
                    this.FacingsWide == 1 &&
                    !product.CanBreakTrayUp) return false;

                // If position is currently a unit then we must be broken down further than a single tray 
                // and therefore can only reduce when we have more that one facing.
                if (isPositionCurrentlyAUnit && this.FacingsWide > 1)
                {
                    this.FacingsWide--;
                    TryMakeTray(product, rotatedTraySize);
                    return true;
                }


                // If we have at least one full tray then decrease cap by one unless there are no caps in which case
                // We remove an entire tray facing and add in the new caps.
                if (!isPositionCurrentlyAUnit &&
                    this.FacingsXWide != 0)
                {
                    this.FacingsXWide--;
                    TryMakeTray(product, rotatedTraySize);

                    //Update the y & z Caps also:
                    UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                    return true;
                }
                else
                {
                    // Evaulate here whether we can break down & up. 
                    if (FacingsWide > 1)
                    {
                        if (product.CanBreakTrayUp &&
                            rotatedTraySize.Wide > 1)
                        {
                            // Remove a facing and add missing x facings.
                            this.FacingsWide--;
                            this.FacingsXWide = Convert.ToInt16(rotatedTraySize.Wide - 1);
                            this.FacingsXHigh = Convert.ToInt16(rotatedTraySize.High * FacingsHigh);
                            this.FacingsXDeep = Convert.ToInt16(rotatedTraySize.Deep * FacingsDeep);
                            TryMakeTray(product, rotatedTraySize);

                            //Update the y & z Caps also:
                            UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                            return true;
                        }
                        else
                        {
                            //We can only remove entire facings in which case do so and ensure there are no caps.
                            this.FacingsWide--;
                            SetFacingCapUnits(AxisType.X, 0);
                            TryMakeTray(product, rotatedTraySize);

                            //Update the y & z Caps also:
                            UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);
                            return true;
                        }
                    }
                    else if (FacingsWide == 1 &&
                             product.CanBreakTrayDown &&
                             rotatedTraySize.Wide > 1)
                    {
                        //If we have just one tray and can break down then we are no longer a tray but must become a unit 
                        // - we need to adjust facing values acordingly.
                        this.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                        this.FacingsHigh = Convert.ToInt16(rotatedTraySize.High * FacingsHigh);
                        this.FacingsDeep = Convert.ToInt16(rotatedTraySize.Deep * FacingsDeep);
                        this.FacingsWide = Convert.ToInt16(rotatedTraySize.Wide - 1);
                        TryMakeTray(product, rotatedTraySize);

                        // Update the y & z Caps also:
                        // Because we are a unit at this point we shouldn't have any caps. 
                        // If we do go integrate them into the main position body. 
                        UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType.X, rotatedTraySize);

                        return true;
                    }
                }

                //If any of the above fails for trays then we should return false
                return false;
            }

            #endregion

            #region Generic Behaviour

            else
            {
                PlanogramPositionDetails posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                // If has caps then we should reduce the cap layer.
                if (FacingsXHigh != 0 &&
                    FacingsXWide != 0 &&
                    FacingsXDeep != 0)
                {
                    FacingsXWide--;

                    //Update details and update caps
                    posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails);

                    return true;
                }

                // If isn't the last facing then we can still decrease
                if (FacingsWide != 1)
                {
                    //Remove an entire facing
                    FacingsWide--;

                    //Replace right caps if this position can have them
                    if (product.MaxRightCap != 0 &&
                        merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang &&
                        merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom)
                    {
                        //Add dummy cap to calc size
                        FacingsXHigh = 1;
                        FacingsXDeep = 1;
                        FacingsXWide = 1;

                        //squeeze them to match the current size so that the width and depth are calculated correctly.
                        SqueezeHeightTo(posDetails.MainTotalSize.Height, product, subComponentPlacement, merchandisingGroup);
                        SqueezeDepthTo(posDetails.MainTotalSize.Depth, product, subComponentPlacement, merchandisingGroup);

                        // Can we fit caps into the space. If so reduce a facing and add caps
                        posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                        //Calculate the correct number wide.
                        this.FacingsXWide = CalculateCapFacingsPerSingleMainUnit(AxisType.X, ref posDetails);

                        //CalculateCapFacings(
                        //(posDetails.MainRotatedNestingSize.Width.GreaterThan(0)) ? posDetails.MainRotatedNestingSize.Width : posDetails.MainRotatedUnitSize.Width,
                        //posDetails.XRotatedUnitSize.Width, posDetails.XRotatedNestingSize.Width, 0);

                        if (FacingsXWide != 0 &&
                            (posDetails.XRotatedUnitSize.Depth > posDetails.MainTotalSize.Depth || posDetails.XRotatedUnitSize.Height > posDetails.MainTotalSize.Height))
                        {
                            // Caps must've fit in but they overflow in the other axis...
                            FacingsXHigh = 0;
                            FacingsXDeep = 0;
                            FacingsXWide = 0;
                        }

                        // If the capping high is equivalent to our facing high then remove another cap high.
                        if (posDetails.MainRotatedUnitSize.Width.LessOrEqualThan(posDetails.GetAnticipatedBlockSize(PlanogramPositionBlockType.X, AxisType.X, this.FacingsXWide)))
                            FacingsXWide--;

                        // Capping can never exceed our max cap. 
                        if (FacingsXWide > product.MaxRightCap)
                            FacingsXWide = product.MaxRightCap;
                    }


                    //now update caps and return true.
                    posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                    return true;
                }

                // Update details and update caps
                posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
                UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails);

                // Cannot decrease if last facing
                return false;
            }

            #endregion
        }

        /// <summary>
        /// Decrease the units of this position in the y axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The merch group to which the placement belongs</param>
        /// <returns>True if successful, else false</returns>
        private Boolean DecreaseUnitsY(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            // This method needs to decrease the 'units' Y based on the type of products being handled. 
            // i.e taking into account tray products, nested products, etc. 

            if (TrayPositionHelper.YAxisManager.DecreaseUnits(product, this, subComponentPlacement,
                merchandisingGroup)) return true;

            #region Generic Behaviour

            PlanogramPositionDetails posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

            // If has caps then we should reduce the cap layer.
            if (FacingsYHigh != 0 &&
                FacingsYWide != 0 &&
                FacingsYDeep != 0)
            {
                FacingsYHigh--;

                //Update details and update caps
                posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
                UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails);

                return true;
            }

            // If isn't the last facing then we can still decrease
            else if (FacingsHigh != 1)
            {
                //Remove an entire facing
                FacingsHigh--;

                //Replace top caps if this position allows it.
                if (product.MaxTopCap != 0 &&
                    merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.Hang &&
                    merchandisingGroup.MerchandisingType != PlanogramSubComponentMerchandisingType.HangFromBottom)
                {
                    //Add dummy cap to calc size
                    FacingsYHigh = 1;
                    FacingsYDeep = 1;
                    FacingsYWide = 1;

                    //squeeze them to match the current size so that the width and depth are calculated correctly.
                    SqueezeWidthTo(posDetails.MainTotalSize.Width, product, subComponentPlacement, merchandisingGroup);
                    SqueezeDepthTo(posDetails.MainTotalSize.Depth, product, subComponentPlacement, merchandisingGroup);

                    // Can we fit caps into the space. If so reduce a facing and add caps
                    posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);

                    //Calculate the correct number high, wide and deep
                    if (posDetails.YRotatedUnitSize.Height != 0)
                    {
                        this.FacingsYHigh = CalculateCapFacingsPerSingleMainUnit(AxisType.Y, ref posDetails);
                        //CalculateCapFacings(
                        //(posDetails.MainRotatedNestingSize.Height.GreaterThan(0)) ? posDetails.MainRotatedNestingSize.Height : posDetails.MainRotatedUnitSize.Height,
                        //posDetails.YRotatedUnitSize.Height, posDetails.YRotatedNestingSize.Height, 0);
                    }

                    //check if the caps actually fit on all axis
                    if (this.FacingsYHigh > 0)
                    {
                        if ((posDetails.YRotatedUnitSize.Depth > posDetails.MainTotalSize.Depth || posDetails.YRotatedUnitSize.Width > posDetails.MainTotalSize.Width))
                        {
                            // Caps must've fit in but they overflow in the other axis...
                            FacingsYHigh = 0;
                            FacingsYDeep = 0;
                            FacingsYWide = 0;
                        }
                        else
                        {
                            // If the capping high is equivalent to our facing high then remove another cap high.
                            if (posDetails.MainRotatedUnitSize.Height.LessOrEqualThan(posDetails.GetAnticipatedBlockSize(PlanogramPositionBlockType.Y, AxisType.Y, this.FacingsYHigh)))
                                FacingsYHigh--;
                        }
                    }

                    // Capping can never exceed our max cap. 
                    if (this.FacingsYHigh > product.MaxTopCap)
                        this.FacingsYHigh = product.MaxTopCap;
                }

                //now tell the caps to update so they are all correct.
                posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
                UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
                return true;
            }

            //Update details and update caps
            posDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
            UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref posDetails, alwaysRecalc: true);
            UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref posDetails);
            UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref posDetails);

            // Cannot decrease if last facing
            return false;

            #endregion
        }

        /// <summary>
        /// Decrease the units of this position in the z axis only
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The merch group to which the placement belongs</param>
        /// <returns>True if successful, else false</returns>
        private Boolean DecreaseUnitsZ(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            //Tray Products not yet handled in Y axis.
            //Anything else has generic behaviour of decreasing main positions facings high by 1.

            if (TrayPositionHelper.ZAxisManager.DecreaseUnits(product, this, subComponentPlacement,
               merchandisingGroup)) return true;

            #region Generic behaviour

            else
            {
                //Never decrease less than one
                if (FacingsDeep == 1) return false;

                // Don't go bellow min deep!
                if ((FacingsDeep - 1) <= (product.MinDeep - 1)) return false;

                //Reduce by one facing high
                this.FacingsDeep--;

                PlanogramPositionDetails positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
                UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref positionDetails, alwaysRecalc: true);
                UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref positionDetails);
                UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref positionDetails);

                return true;
            }

            #endregion
        }

        #endregion

        #region IncreaseCaps

        /// <summary>
        ///     Try increasing cap units on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The axis to increase cap units on for this instance.</param>
        /// <param name="product">The product to increase cap units of.</param>
        /// <param name="subComponentPlacement">The sub component placement this position is on.</param>
        /// <param name="merchandisingGroup">The merchandising group this position belongs to.</param>
        /// <returns><c>True</c> if cap units could be increased. <c>False</c> otherwise.</returns>
        public Boolean TryIncreaseCapUnits(AxisType axis, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            //  Do not increase units for products merchandised as Tray.
            if (product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray) return false;

            //  Only add cap units for stacked merchandising types.
            if (subComponentPlacement.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.Stack)
                return false;

            //  Do not exceed maximum cap units value for the product.
            Int32 capUnits = GetFacingCapUnits(axis);
            if (product.GetMaxCapUnits(axis) <= capUnits) return false;

            SetDefaultCapOrientation(axis, product);

            //  Increase facing cap units on the correct axis.
            SetFacingCapUnits(axis, ++capUnits);
            SetMaxNormalFacingCapUnits(axis, product, subComponentPlacement, merchandisingGroup);

            //  If there are no caps here, something went wrong and caps cannot be increased.
            return GetFacingCapUnits(axis) != 0;
        }

        #endregion

        #region DecreaseCaps

        /// <summary>
        ///     Try decreasing cap units on the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The axis to decrease cap units on for this instance.</param>
        /// <param name="product">The product to decrease cap units of.</param>
        /// <param name="subComponentPlacement">The sub component placement this position is on.</param>
        /// <param name="merchandisingGroup">The merchandising group this position belongs to.</param>
        /// <returns><c>True</c> if cap units could be decreased. <c>False</c> otherwise.</returns>
        public Boolean TryDecreaseCapUnits(AxisType axis, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            //  Do not decrease units for products merchandised as Tray.
            if (product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray) return false;

            //  Only remove cap units for stacked merchandising types.
            if (subComponentPlacement.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.Stack)
                return false;

            //  Do not go below 0 cap units value for the product.
            Int32 capUnits = GetFacingCapUnits(axis);
            if (capUnits <= 0) return false;

            SetDefaultCapOrientation(axis, product);

            //  Increase facing cap units on the correct axis.
            SetFacingCapUnits(axis, --capUnits);
            SetMaxNormalFacingCapUnits(axis, product, subComponentPlacement, merchandisingGroup);

            return true;
        }

        #endregion

        #region SetUnits

        /// <summary>
        /// Changes the units of this position to
        /// match the specified target
        /// </summary>
        /// <param name="product">The planogam product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="targetUnits">The target inventory</param>
        /// <param name="targetType">The target type</param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <param name="merchandisingGroup">[Optional] An existing <see cref="PlanogramMerchandisingGroup"/> that will be used to set units in.</param>
        public void SetUnits(PlanogramProduct product,
                             PlanogramSubComponentPlacement subComponentPlacement,
                             PlanogramMerchandisingGroup merchandisingGroup,
                             Int32 targetUnits,
                             PlanogramPositionInventoryTargetType targetType,
                             AssortmentRuleEnforcer assortmentRuleEnforcer,
                             PlanogramMerchandisingInventoryChangeType inventoryChangeType = PlanogramMerchandisingInventoryChangeType.ByUnits)
        {
            PlanogramPositionDetails positionDetails = this.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
            Int32 currentUnits = positionDetails.TotalUnitCount;
            Int32 previousUnits = currentUnits;

            SetUnits(
                product,
                subComponentPlacement,
                merchandisingGroup,
                targetUnits,
                assortmentRuleEnforcer,
                ref currentUnits,
                ref previousUnits,
                positionDetails,
                inventoryChangeType);

            // set a flag that indicates
            // we need to revert inventory
            // to its previous
            Boolean revertToPrevious = ShouldRevertToPrevious(targetUnits, targetType, ref currentUnits, ref previousUnits);

            // revert the position to its previous
            // inventory if required
            if (revertToPrevious)
            {
                RevertToPreviousUnits(
                    product,
                    subComponentPlacement,
                    merchandisingGroup,
                    assortmentRuleEnforcer,
                    currentUnits,
                    previousUnits,
                    inventoryChangeType);
            }
        }

        private void SetUnits(
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup,
            Int32 targetUnits,
            AssortmentRuleEnforcer assortmentRuleEnforcer,
            ref Int32 currentUnits,
            ref Int32 previousUnits,
            PlanogramPositionDetails positionDetails,
            PlanogramMerchandisingInventoryChangeType inventoryChangeType)
        {
            // get the current position details

            // loop until our previous and current units
            // straddle the target inventory
            while (((previousUnits < targetUnits) && (currentUnits < targetUnits)) ||
                   ((previousUnits > targetUnits) && (currentUnits > targetUnits)))
            {
                if (currentUnits == targetUnits) break;

                // increase or decrease the inventory
                // for this position based on the current inventory
                if (currentUnits > targetUnits)
                {
                    if (!PerformDecreasingInventoryChange(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, inventoryChangeType))
                    {
                        break;
                    }
                }
                else if (currentUnits < targetUnits)
                {
                    if (!PerformIncreasingInventoryChange(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, inventoryChangeType))
                    {
                        break;
                    }
                }

                // get the current position details again
                positionDetails = this.GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                if (previousUnits == positionDetails.TotalUnitCount) break;

                previousUnits = currentUnits;
                currentUnits = positionDetails.TotalUnitCount;
            }
        }

        private Boolean PerformDecreasingInventoryChange(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, AssortmentRuleEnforcer assortmentRuleEnforcer, PlanogramMerchandisingInventoryChangeType inventoryChangeType)
        {
            if (inventoryChangeType == PlanogramMerchandisingInventoryChangeType.ByUnits)
            {
                if (!this.DecreaseUnits(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer))
                {
                    return false;
                }
            }
            else
            {
                if (!this.DecreaseFacings(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer))
                {
                    return false;
                }
            }
            return true;
        }

        private Boolean PerformIncreasingInventoryChange(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, AssortmentRuleEnforcer assortmentRuleEnforcer, PlanogramMerchandisingInventoryChangeType inventoryChangeType)
        {
            if (inventoryChangeType == PlanogramMerchandisingInventoryChangeType.ByUnits)
            {
                if (!this.IncreaseUnits(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer))
                {
                    return false;
                }
            }
            else
            {
                if (!this.IncreaseFacings(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer))
                {
                    return false;
                }
            }
            return true;
        }

        private static Boolean ShouldRevertToPrevious(
            Int32 targetUnits,
            PlanogramPositionInventoryTargetType targetType,
            ref Int32 currentUnits,
            ref Int32 previousUnits)
        {
            Boolean revertToPrevious = true;

            // now, based on the target type
            // determine the final action to
            // take against this position
            switch (targetType)
            {
                #region ClosestOrEqualTo

                // we are trying to locate the position option
                // that gets us closest to the target inventory
                case PlanogramPositionInventoryTargetType.ClosestOrEqualTo:

                    if (currentUnits == targetUnits)
                    {
                        // current units match target units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if (currentUnits == previousUnits)
                    {
                        // current units equals previous units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if (Math.Abs(currentUnits - targetUnits) <= Math.Abs(previousUnits - targetUnits))
                    {
                        // current units are closer to target units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    break;

                #endregion

                #region GreaterThanOrEqualTo

                // we are trying to locate the position option that
                // gets us equal to our target units or is above it
                case PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo:

                    if (currentUnits == targetUnits)
                    {
                        // current units equals target units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if (currentUnits == previousUnits)
                    {
                        // current units equals previous units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if ((currentUnits > targetUnits) &&
                             (previousUnits < targetUnits))
                    {
                        // current units is above target units
                        // and previous units is below so keep
                        // the current position details
                        revertToPrevious = false;
                    }
                    else if ((currentUnits > targetUnits) &&
                             (previousUnits > targetUnits) &&
                             ((currentUnits - targetUnits) < (previousUnits - targetUnits)))
                    {
                        // both current and previous are above target
                        // but current is closer to target, so keep
                        // the current position details
                        revertToPrevious = false;
                    }
                    break;

                #endregion

                #region LessThanOrEqualTo

                case PlanogramPositionInventoryTargetType.LessThanOrEqualTo:

                    if (currentUnits == targetUnits)
                    {
                        // current units equals target units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if (currentUnits == previousUnits)
                    {
                        // current units equals previous units
                        // so keep the current position details
                        revertToPrevious = false;
                    }
                    else if ((currentUnits < targetUnits) &&
                             (previousUnits > targetUnits))
                    {
                        // current units is below target units
                        // and previous units is above so keep
                        // the current position details
                        revertToPrevious = false;
                    }
                    else if ((currentUnits < targetUnits) &&
                             (previousUnits < targetUnits) &&
                             ((targetUnits - currentUnits) < (targetUnits - previousUnits)))
                    {
                        // both current and previous are below target
                        // but current is closer to target, so keep
                        // the current position details
                        revertToPrevious = false;
                    }
                    break;

                    #endregion
            }

            return revertToPrevious;
        }

        private void RevertToPreviousUnits(
            PlanogramProduct product,
            PlanogramSubComponentPlacement subComponentPlacement,
            PlanogramMerchandisingGroup merchandisingGroup,
            AssortmentRuleEnforcer assortmentRuleEnforcer,
            Int32 currentUnits,
            Int32 previousUnits,
            PlanogramMerchandisingInventoryChangeType inventoryChangeType)
        {
            if (currentUnits > previousUnits)
            {
                PerformDecreasingInventoryChange(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, inventoryChangeType);
            }
            else if (previousUnits > currentUnits)
            {
                PerformIncreasingInventoryChange(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer, inventoryChangeType);
            }
        }

        /// <summary>
        /// Sets the position to its its absolute minimum number of 'units' while adhering to the rules of the positon.
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> currently placed in this <c>Position</c>.</param>
        /// <param name="shouldMinimiseX">Should X facings be set to minimum</param>
        /// <param name="shouldMinimiseY">Should Y facings be set to minimum</param>
        /// <param name="shouldMinimiseZ">Should Z facings be set to minimum</param>
        /// <param name="shouldRemoveCaps">Should caps be removed as part of setting the facings to their minimum values</param>
        /// <remarks>
        /// Still reduces caps when RemoveCaps is false - may still result in no caps if cannot fit.
        /// </remarks>
        public void SetMinimumUnits(PlanogramProduct product, Boolean shouldMinimiseX = true, Boolean shouldMinimiseY = true, Boolean shouldMinimiseZ = true, Boolean shouldRemoveCaps = true)
        {
            if (product == null) return;

            //  Get the axes to be reduced.
            var reducingAxes = new List<AxisType>();
            if (shouldMinimiseX) reducingAxes.Add(AxisType.X);
            if (shouldMinimiseY) reducingAxes.Add(AxisType.Y);
            if (shouldMinimiseZ) reducingAxes.Add(AxisType.Z);

            // Must have at least one axis to proceed.
            if (!reducingAxes.Any()) return;

            //  If the position is being merchandised as a tray,
            //  the product is a tray,
            //  and it allows breaking down...
            if (IsAppliedMerchandisingStyleAnyOf(product, PlanogramProductMerchandisingStyle.Tray) &&
                product.IsMerchandisingStyleAnyOf(PlanogramProductMerchandisingStyle.Tray) &&
                product.CanBreakTrayDown)
            {
                //  Explode the tray into its units.
                MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                WideHighDeepValue trayFacings = product.GetRotatedTraySize(OrientationType);

                //  Trays as units cannot have caps.
                foreach (AxisType axisType in Enum.GetValues(typeof(AxisType)).Cast<AxisType>())
                {
                    SetFacings(axisType, GetFacings(axisType) * trayFacings.GetSize(axisType));
                    AddFacings(axisType, (Int16)GetFacingCapUnits(axisType));
                    SetFacingCapUnits(axisType, 0);
                }
            }

            //  Reduce units on each axis that was requested.

            foreach (AxisType axis in reducingAxes)
            {
                SetFacings(axis, axis != AxisType.Z ? 1 : Math.Max((Int32)product.MinDeep, 1));
                SetFacingCapUnitsMain(axis, 1);
                if (shouldRemoveCaps) SetFacingCapUnits(axis, 0);
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        ///     Returns the number of units in the XBlock
        /// </summary>
        private Int32 GetXBlockUnits()
        {
            return this.FacingsXHigh * this.FacingsXWide * this.FacingsXDeep;
        }

        /// <summary>
        ///     Returns the number of units in the YBlock.
        /// </summary>
        private Int32 GetYBlockUnits()
        {
            return this.FacingsYHigh * this.FacingsYWide * this.FacingsYDeep;
        }

        /// <summary>
        ///     Returns the number of units in the ZBlock.
        /// </summary>
        private Int32 GetZBlockUnits()
        {
            return this.FacingsZHigh * this.FacingsZWide * this.FacingsZDeep;
        }

        /// <summary>
        ///     Get the <c>Cap Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to return the <c>Cap Facings</c> count for.</param>
        /// <returns>The number of cap facings set for this instance on the given <paramref name="axis"/>.</returns>
        private Int32 GetFacingCapUnits(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return FacingsXWide;
                case AxisType.Y:
                    return FacingsYHigh;
                case AxisType.Z:
                    return FacingsZDeep;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.GetFacingCapUnits");
                    return 0;
            }
        }

        /// <summary>
        ///     Set the <c>Cap Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to set the <c>Cap Facings</c> count for.</param>
        /// <param name="value">The amount of <C>Cap Facings</C> to add on the given <paramref name="axis"/>.</param>
        /// <remarks>
        ///     If <paramref name="value"/> is ZERO, all facing cap units values for the axis will be set to ZERO.
        /// <para></para>
        ///     If <paramref name="value"/> is not ZERO, all other facing cap units for the axis will be set AT LEAST to 1 (they will not be adjusted if they are already not ZERO).</remarks>
        internal void SetFacingCapUnits(AxisType axis, Int32 value)
        {
            //  Facing cap units should not be lower than 0.
            if (value < 0) value = 0;

            switch (axis)
            {
                case AxisType.X:
                    FacingsXWide = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        FacingsXHigh = 0;
                        FacingsXDeep = 0;
                    }
                    else
                    {
                        if (FacingsXHigh == 0) FacingsXHigh = 1;
                        if (FacingsXDeep == 0) FacingsXDeep = 1;
                    }
                    break;
                case AxisType.Y:
                    FacingsYHigh = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        FacingsYWide = 0;
                        FacingsYDeep = 0;
                    }
                    else
                    {
                        if (FacingsYWide == 0) FacingsYWide = 1;
                        if (FacingsYDeep == 0) FacingsYDeep = 1;
                    }
                    break;
                case AxisType.Z:
                    FacingsZDeep = Convert.ToInt16(value);
                    if (value == 0)
                    {
                        FacingsZHigh = 0;
                        FacingsZWide = 0;
                    }
                    else
                    {
                        if (FacingsZHigh == 0) FacingsZHigh = 1;
                        if (FacingsZWide == 0) FacingsZWide = 1;
                    }
                    break;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.SetFacingCapUnits");
                    break;
            }
        }

        /// <summary>
        ///     Check whether this instance has valid caps on the given <paramref name="axis" />.
        /// </summary>
        /// <param name="axis">The axis on which to check for valid caps.</param>
        /// <returns><c>True</c> if there are valid caps on the axis; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     Valid caps implies that the axis has cap units specified in all <c>Wide</c>, <c>High</c> and <c>Deep</c>
        ///     dimensions.
        /// </remarks>
        private Boolean HasFacingCaps(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return FacingsXWide > 0 && FacingsXHigh > 0 && FacingsXDeep > 0;
                case AxisType.Y:
                    return FacingsYWide > 0 && FacingsYHigh > 0 && FacingsYDeep > 0;
                case AxisType.Z:
                    return FacingsZWide > 0 && FacingsZHigh > 0 && FacingsZDeep > 0;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.HasFacingCaps");
                    return false;
            }
        }

        /// <summary>
        ///     Set to the default Orientation Type for the given <paramref name="axis"/> and <paramref name="product"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to set to the default orientation type.</param>
        /// <param name="product">The <see cref="PlanogramProduct"/> to get the default orientation for.</param>
        private void SetDefaultCapOrientation(AxisType axis, PlanogramProduct product)
        {
            switch (axis)
            {
                case AxisType.X:
                    OrientationTypeX = ProductOrientationHelper.GetRightCapOrientation(PlanogramProductOrientationTypeHelper.GetOrientationTypeFromEquivalent(OrientationType, product)).ToPositionOrientationType();
                    break;
                case AxisType.Y:
                    OrientationTypeY = ProductOrientationHelper.GetTopCapOrientation(PlanogramProductOrientationTypeHelper.GetOrientationTypeFromEquivalent(OrientationType, product)).ToPositionOrientationType();
                    break;
                case AxisType.Z:
                    Debug.Fail("There is no default cap orientation value for axis Z.");
                    break;
                default:
                    Debug.Fail("Unknown axis when calling SetDefaultCapOrientation(axis, product).");
                    break;
            }
        }

        /// <summary>
        ///     Sets the normal axes of a given <paramref name="axis"/> facing cap units to the specified <paramref name="value"/>.
        /// </summary>
        /// <param name="axis">The axis which normal axes need cap units set.</param>
        /// <param name="value">The units to set the normal axes cap units to.</param>
        /// <remarks>Any facing that has no cap units will be left alone.</remarks>
        private void SetFacingCapUnitsMain(AxisType axis, Int16 value)
        {
            switch (axis)
            {
                case AxisType.X:
                    if (FacingsYHigh > 0) FacingsYWide = value;
                    if (FacingsZDeep > 0) FacingsZWide = value;
                    break;
                case AxisType.Y:
                    if (FacingsXWide > 0) FacingsXHigh = value;
                    if (FacingsZDeep > 0) FacingsZHigh = value;
                    break;
                case AxisType.Z:
                    if (FacingsYHigh > 0) FacingsYDeep = value;
                    if (FacingsXWide > 0) FacingsXDeep = value;
                    break;
                default:
                    Debug.Fail("Unknown axis when processing SetFacingCapUnitsMain(axis, value)");
                    break;
            }
        }

        private void SetNormalFacingCapUnits(AxisType blockAxis, AxisType normal, Int16 value)
        {
            //  If any of the cap units is zero, there are no cap units.
            if (value == 0)
            {
                SetFacingCapUnits(blockAxis, value);
                return;
            }

            switch (blockAxis)
            {
                case AxisType.X:
                    if (normal == AxisType.Y) FacingsXHigh = value;
                    if (normal == AxisType.Z) FacingsXDeep = value;
                    break;
                case AxisType.Y:
                    if (normal == AxisType.X) FacingsYWide = value;
                    if (normal == AxisType.Z) FacingsYDeep = value;
                    break;
                case AxisType.Z:
                    if (normal == AxisType.X) FacingsZWide = value;
                    if (normal == AxisType.Y) FacingsZHigh = value;
                    break;
                default:
                    Debug.Fail("Unknown axis when processing SetNormalFacingCapUnits(blockAxis, normal, value)");
                    break;
            }
        }

        private void SetMaxNormalFacingCapUnits(AxisType axis, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            //  If there are no facings on the main axis, then enforce no caps on the other axes.
            if (GetFacingCapUnits(axis) == 0)
            {
                SetFacingCapUnits(axis, 0);
                return;
            }

            //Get some detailed information about our positon now we know there is at least on cap mae avaiable.
            PlanogramPositionDetails positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(this, product, subComponentPlacement, merchandisingGroup);
            WidthHeightDepthValue totalSize = positionDetails.MainTotalSize;

            WidthHeightDepthValue rotatedUnitSize = positionDetails.GetBlockRotatedUnitSize(axis);

            foreach (AxisType normal in axis.GetNormals())
            {
                //Now we must consider the other dimensions. Should fill the main position body, and maybe the capping too. 
                //How many deep/high can we fit given the capping rotation on the position size?
                Single availableSpace = rotatedUnitSize.GetSize(normal);
                if (!availableSpace.GreaterThan(0)) continue;

                Int16 maxUnits = Convert.ToInt16(Math.Floor(Math.Round(totalSize.GetSize(normal) / availableSpace, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));
                SetNormalFacingCapUnits(axis, normal, maxUnits);

                //  If the facings fall back to 0, then no more facings should be attempted.
                if (maxUnits == 0) break;
            }
        }

        #endregion

        #endregion

        #region Facings

        #region IncreaseFacings

        /// <summary>
        /// Increases the facings of this position
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <returns>True if facings was successfully increased, else false</returns>
        public Boolean IncreaseFacings(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return this.IncreaseFacings(product, subComponentPlacement, null, assortmentRuleEnforcer);
        }

        /// <summary>
        /// Increases the facings of this position
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The planogram merchandising group</param>
        /// <returns>True if facings was successfully increased, else false</returns>
        public Boolean IncreaseFacings(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            // The position details will only be calculated if required.
            PlanogramPositionDetails details = null;

            // NF - TODO - Needs to take into account
            // stuff such as MaxDeep, etc.
            switch (subComponentPlacement.GetFacingAxis())
            {
                case AxisType.X:
                    this.FacingsWide++;
                    UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details);
                    break;

                case AxisType.Y:
                    this.FacingsHigh++;
                    UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details);
                    break;

                case AxisType.Z:
                    this.FacingsDeep++;
                    UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details);
                    break;
            }
            return EnforceRules(
                product,
                subComponentPlacement,
                merchandisingGroup,
                assortmentRuleEnforcer,
                subComponentPlacement.GetFacingAxis(),
                PlanogramMerchandisingInventoryChangeType.ByFacings);
        }

        #endregion

        #region DecreaseFacings

        /// <summary>
        /// Decreases the facings of this position
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <returns>True if successful, else false</returns>
        public Boolean DecreaseFacings(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            return this.DecreaseFacings(product, subComponentPlacement, null, assortmentRuleEnforcer);
        }

        /// <summary>
        /// Decreases the facings of this position
        /// </summary>
        /// <param name="product">The planogram product</param>
        /// <param name="subComponentPlacement">The planogram sub component placement</param>
        /// <param name="merchandisingGroup">The planogram merchandising group</param>
        /// <returns>True if successful, else false</returns>
        public Boolean DecreaseFacings(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, IAssortmentRuleEnforcer assortmentRuleEnforcer = null)
        {
            // The position details will only be calculated if required.
            PlanogramPositionDetails details = null;
            Boolean success = false;

            switch (subComponentPlacement.GetFacingAxis())
            {
                case AxisType.X:
                    if (this.FacingsWide > 1)
                    {
                        this.FacingsWide--;
                        UpdateCapsToMainBlockWidth(product, subComponentPlacement, merchandisingGroup, ref details);
                        success = true;
                    }
                    break;
                case AxisType.Y:
                    if (this.FacingsHigh > 1)
                    {
                        this.FacingsHigh--;
                        UpdateCapsToMainBlockHeight(product, subComponentPlacement, merchandisingGroup, ref details);
                        success = true;
                    }
                    break;
                case AxisType.Z:
                    if (this.FacingsDeep > 1)
                    {
                        this.FacingsDeep--;
                        UpdateCapsToMainBlockDepth(product, subComponentPlacement, merchandisingGroup, ref details);
                        success = true;
                    }
                    break;
            }
            if (success)
            {
                return EnforceRules(
                    product,
                    subComponentPlacement,
                    merchandisingGroup,
                    assortmentRuleEnforcer,
                    subComponentPlacement.GetFacingAxis(),
                    PlanogramMerchandisingInventoryChangeType.ByFacings);
            }
            return success;
        }

        #endregion

        #region Increase/Decrease cap related

        /// <summary>
        /// Increases or decreases this positions cap facings according to the depth of the main block.
        /// </summary>
        public void UpdateCapsToMainBlockDepth(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, ref PlanogramPositionDetails details)
        {
            this.UpdateCapsToMainBlockDepth(product, subComponentPlacement, null, ref details);
        }

        /// <summary>
        /// Increases or decreases this positions cap facings according to the depth of the main block.
        /// </summary>
        private void UpdateCapsToMainBlockDepth(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, ref PlanogramPositionDetails details, Boolean alwaysRecalc = false)
        {
            // Increase/decrease right/left caps.
            if (FacingsXDeep != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsXDeep = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Z, ref details);
            }

            // Increase/decrease top/bottom caps.
            if (FacingsYDeep != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsYDeep = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.Z, ref details);
            }
        }

        /// <summary>
        /// Increases or decreases this positions cap facings according to the height of the main block.
        /// </summary>
        public void UpdateCapsToMainBlockHeight(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, ref PlanogramPositionDetails details)
        {
            this.UpdateCapsToMainBlockHeight(product, subComponentPlacement, null, ref details);
        }

        /// <summary>
        /// Increases or decreases this positions cap facings according to the height of the main block.
        /// </summary>
        private void UpdateCapsToMainBlockHeight(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, ref PlanogramPositionDetails details, Boolean alwaysRecalc = false)
        {
            // Increase/decrease right/left caps.
            if (FacingsXHigh != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsXHigh = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Y, ref details);
            }

            // Increase/decrease front/back caps.
            if (FacingsZHigh != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsZHigh = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Z, AxisType.Y, ref details);
            }
        }

        /// <summary>
        /// Increases or decreases this positions cap facings according to the width of the main block.
        /// </summary>
        public void UpdateCapsToMainBlockWidth(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, ref PlanogramPositionDetails details)
        {
            this.UpdateCapsToMainBlockWidth(product, subComponentPlacement, null, ref details);
        }

        /// <summary>
        /// Increases or decreases this positions cap facings according to the width of the main block.
        /// </summary>
        private void UpdateCapsToMainBlockWidth(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup, ref PlanogramPositionDetails details, Boolean alwaysRecalc = false)
        {
            // Increase/decrease top/bottom caps.
            if (FacingsYWide != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsYWide = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.X, ref details);
            }

            // Increase/decrease front/back caps.
            if (FacingsZWide != 0 || alwaysRecalc)
            {
                details = details ?? GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                this.FacingsZWide = CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Z, AxisType.X, ref details);
            }
        }

        /// <summary>
        /// Updates the given cap type facings in the specified axis to its optimal value
        /// according to the current size of the main block.
        /// This takes into account nesting and squeeze.
        /// </summary>
        /// <param name="capType">the cap type to update</param>
        /// <param name="axis">the axis to update in</param>
        /// <param name="details">the current details info.</param>
        /// <returns></returns>
        public Int16 CalculateCapFacingsToMainBlock(PlanogramPositionBlockType capType, AxisType axis, ref PlanogramPositionDetails details)
        {
            Single mainFacingsTotalSize = details.MainTotalSize.GetSize(axis);
            Single capFacingsUnitSize;
            Single capFacingsNestedUnitSize;
            Single capFacingSeparation = details.FacingSize.GetSize(axis);


            switch (capType)
            {
                case PlanogramPositionBlockType.X:
                    capFacingsUnitSize = details.XRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.XRotatedNestingSize.GetSize(axis);
                    break;

                case PlanogramPositionBlockType.Y:
                    capFacingsUnitSize = details.YRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.YRotatedNestingSize.GetSize(axis);
                    break;

                case PlanogramPositionBlockType.Z:
                    capFacingsUnitSize = details.ZRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.ZRotatedNestingSize.GetSize(axis);
                    break;

                default:
                    return 0;
            }

            //if unit has no size then return 0.
            if (capFacingsUnitSize.EqualTo(0)) return 0;

            Int16 normalFacings = Convert.ToInt16(Math.Floor(Math.Round((mainFacingsTotalSize + capFacingSeparation) / (capFacingsUnitSize + capFacingSeparation), MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));


            //Nested:
            if (capFacingsNestedUnitSize.GreaterThan(0))
            {
                //if we could not fit a full facing then just return out 0.
                if (normalFacings == 0) return normalFacings;

                //if we are nesting and can fit at least 1 full facings on,
                // then we need to consider nested size.

                // = single full facing + number of nested facings
                return Convert.ToInt16(1 + Math.Floor(Math.Round((mainFacingsTotalSize + capFacingSeparation - capFacingsUnitSize) / capFacingsNestedUnitSize, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));
            }


            //Squeeze cap type specific checks:
            if ((axis == AxisType.X && this.HorizontalSqueeze.LessThan(1)) ||
                (axis == AxisType.Y && this.VerticalSqueeze.LessThan(1)) ||
                (axis == AxisType.Z && this.DepthSqueeze.LessThan(1)))
            {
                switch (capType)
                {
                    #region Left/Right Cap:

                    case PlanogramPositionBlockType.X:
                        {
                            //Horiz squeeze - not affected by main block size.

                            if (axis == AxisType.Y)
                            {
                                if (this.VerticalSqueeze.LessThan(1) &&
                                    details.XRotatedMaxSqueeze.Height.LessThan(1))
                                {
                                    //check if it is possible to squeeze to the width of the main units.
                                    if ((details.XRotatedMaxSqueeze.Height * details.XRotatedUnitSize.Height).LessOrEqualThan(details.MainRotatedUnitSize.Height))
                                        return this.FacingsHigh;
                                }
                            }
                            else if (axis == AxisType.Z)
                            {
                                if (this.DepthSqueeze.LessThan(1) &&
                                    details.XRotatedMaxSqueeze.Depth.LessThan(1))
                                {
                                    //check if it is possible to squeeze to the width of the main units.
                                    if ((details.XRotatedMaxSqueeze.Depth * details.XRotatedUnitSize.Depth).LessOrEqualThan(details.MainRotatedUnitSize.Depth))
                                        return this.FacingsDeep;
                                }
                            }
                        }
                        break;

                    #endregion

                    #region Top/Bottom Cap:

                    case PlanogramPositionBlockType.Y:
                        {
                            //width squeeze
                            if (axis == AxisType.X)
                            {
                                if (this.HorizontalSqueeze.LessThan(1) &&
                                    details.YRotatedMaxSqueeze.Width.LessThan(1))
                                {
                                    //check if it is possible to squeeze to the width of the main units.
                                    if ((details.YRotatedMaxSqueeze.Width * details.YRotatedUnitSize.Width).LessOrEqualThan(details.MainRotatedUnitSize.Width))
                                        return this.FacingsWide;
                                }
                            }

                            //Vertical squeeze - not affected by main block size.

                            //depth squeeze
                            else if (axis == AxisType.Z)
                            {
                                if (this.DepthSqueeze.LessThan(1) &&
                                    details.YRotatedMaxSqueeze.Depth.LessThan(1))
                                {
                                    //check if it is possible to squeeze to the width of the main units.
                                    if ((details.YRotatedMaxSqueeze.Depth * details.YRotatedUnitSize.Depth).LessOrEqualThan(details.MainRotatedUnitSize.Depth))
                                        return this.FacingsDeep;
                                }
                            }
                        }
                        break;

                    #endregion

                    #region Front/Back Cap:

                    case PlanogramPositionBlockType.Z:
                        {
                            //width squeeze
                            if (axis == AxisType.X)
                            {
                                if (this.HorizontalSqueeze.LessThan(1) &&
                                    details.ZRotatedMaxSqueeze.Width.LessThan(1))
                                {
                                    //check if it is possible to squeeze to the width of the main units.
                                    if ((details.ZRotatedMaxSqueeze.Width * details.ZRotatedUnitSize.Width).LessOrEqualThan(details.MainRotatedUnitSize.Width))
                                        return this.FacingsWide;
                                }
                                else if (axis == AxisType.Y)
                                {
                                    if (this.VerticalSqueeze.LessThan(1) &&
                                        details.ZRotatedMaxSqueeze.Height.LessThan(1))
                                    {
                                        //check if it is possible to squeeze to the width of the main units.
                                        if ((details.ZRotatedMaxSqueeze.Height * details.ZRotatedUnitSize.Height).LessOrEqualThan(details.MainRotatedUnitSize.Height))
                                            return this.FacingsHigh;
                                    }
                                }
                            }
                        }
                        break;

                        #endregion
                }
            }


            //just return as normal facings:
            return normalFacings;
        }

        /// <summary>
        /// Calcuates how many caps are equal to a single main facing on the given axis.
        /// If nesting is applied, the main nested unit size will be used.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        private Int16 CalculateCapFacingsPerSingleMainUnit(AxisType axis, ref PlanogramPositionDetails details)
        {
            Single mainFacingsTotalSize = (details.MainRotatedNestingSize.GetSize(axis).GreaterThan(0)) ? details.MainRotatedNestingSize.GetSize(axis) : details.MainRotatedUnitSize.GetSize(axis);

            Single capFacingsUnitSize;
            Single capFacingsNestedUnitSize;
            Single capFacingSeparation = details.FacingSize.GetSize(axis);


            switch (axis)
            {
                case AxisType.X:
                    capFacingsUnitSize = details.XRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.XRotatedNestingSize.GetSize(axis);
                    break;

                case AxisType.Y:
                    capFacingsUnitSize = details.YRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.YRotatedNestingSize.GetSize(axis);
                    break;

                case AxisType.Z:
                    capFacingsUnitSize = details.ZRotatedUnitSize.GetSize(axis);
                    capFacingsNestedUnitSize = details.ZRotatedNestingSize.GetSize(axis);
                    break;

                default:
                    return 0;
            }

            // Calculate the number of caps that could fit inside the main facings size.
            // Let n = the number of caps we can fit into the main block (we need to return the floor of this).
            // Let a = capFacingsUnitSize.
            // Let b = capFacingSeparation.
            // Let x = mainFacingsTotalSize.
            // na + (n - 1)b = x
            // n = (x + b) / (a + b)
            if (capFacingsUnitSize.EqualTo(0)) return 0;

            Int16 normalFacings = Convert.ToInt16(Math.Floor(Math.Round((mainFacingsTotalSize + capFacingSeparation) / (capFacingsUnitSize + capFacingSeparation), MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));

            if (capFacingsNestedUnitSize.LessOrEqualThan(0) ||
                normalFacings == 0)
                return normalFacings;


            //if we are nesting and can fit at least 1 full facings on,
            // then we need to consider nested size.
            if (capFacingsNestedUnitSize.GreaterThan(0))
            {
                // = single full facing + number of nested facings
                return Convert.ToInt16(1 + Math.Floor(Math.Round((mainFacingsTotalSize + capFacingSeparation - capFacingsUnitSize) / capFacingsNestedUnitSize, MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));
            }

            return normalFacings;
        }


        ///// <summary>
        ///// Calculates the number of cap facings that fit within the size specified.
        ///// </summary>
        ///// <param name="mainFacingsTotalSize">The size in the desired direction of the main block</param>
        ///// <param name="capFacingsUnitSize">The size in the desired direction of a unit in the cap being evaluated.</param>
        ///// <param name="capFacingsNestedUnitSize">The nested size in the desired direction of a unit in the cap being evaluated</param>
        ///// <param name="capFacingSeparation">The size of the gap between cap facings.</param>
        ///// <returns>The maximum number of whole caps that can fit within the main block.</returns>
        //private Int16 CalculateCapFacings(
        //    Single mainFacingsTotalSize,
        //    Single capFacingsUnitSize,
        //    Single capFacingsNestedUnitSize,
        //    Single capFacingSeparation)
        //{
        //    // Calculate the number of caps that could fit inside the main facings size.
        //    // Let n = the number of caps we can fit into the main block (we need to return the floor of this).
        //    // Let a = capFacingsUnitSize.
        //    // Let b = capFacingSeparation.
        //    // Let x = mainFacingsTotalSize.
        //    // na + (n - 1)b = x
        //    // n = (x + b) / (a + b)
        //    if (capFacingsUnitSize.EqualTo(0)) return 0;

        //    Int16 normalFacings =
        //        Convert.ToInt16(Math.Floor(
        //        Math.Round((mainFacingsTotalSize + capFacingSeparation) / (capFacingsUnitSize + capFacingSeparation),
        //        MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));

        //    if (capFacingsNestedUnitSize.LessOrEqualThan(0) || normalFacings == 0)
        //        return normalFacings;


        //    //if we are nesting and can fit at least 1 full facings on,
        //    // then we need to consider nested size.
        //    if (capFacingsNestedUnitSize.GreaterThan(0))
        //    {
        //        // = single full facing + number of nested facings
        //        return Convert.ToInt16(1 +
        //            Math.Floor(
        //                Math.Round((mainFacingsTotalSize + capFacingSeparation - capFacingsUnitSize) / capFacingsNestedUnitSize,
        //                MathHelper.DefaultPrecision, MidpointRounding.AwayFromZero)));
        //    }

        //    return normalFacings;
        //}

        /// <summary>
        /// Remove all caps along all axis.
        /// </summary>
        /// <remarks>Currently just sets all cap facings = 0.</remarks>
        public void RemoveAllCaps()
        {
            RemoveAllCapsForX();
            RemoveAllCapsForY();
            RemoveAllCapsForZ();
        }

        /// <summary>
        /// Remove all caps along x axis.
        /// </summary>
        /// <remarks>Currently just sets all cap facings x = 0.</remarks>
        private void RemoveAllCapsForX()
        {
            FacingsXWide = 0;
            FacingsXHigh = 0;
            FacingsXDeep = 0;
        }

        /// <summary>
        /// Remove all caps along y axis.
        /// </summary>
        /// <remarks>Currently just sets all cap facings y = 0.</remarks>
        private void RemoveAllCapsForY()
        {
            FacingsYWide = 0;
            FacingsYHigh = 0;
            FacingsYDeep = 0;
        }

        /// <summary>
        /// Remove all caps along z axis.
        /// </summary>
        /// <remarks>Currently just sets all facings x = 0.</remarks>
        private void RemoveAllCapsForZ()
        {
            FacingsZWide = 0;
            FacingsZHigh = 0;
            FacingsZDeep = 0;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Returns the number of facings for this position
        /// along the facing axis for the sub component placement
        /// </summary>
        public Int32 GetFacings(PlanogramSubComponentPlacement subComponentPlacement)
        {
            switch (subComponentPlacement.GetFacingAxis())
            {
                case AxisType.X:
                    return this.FacingsWide;
                case AxisType.Y:
                    return this.FacingsHigh;
                case AxisType.Z:
                    return this.FacingsDeep;
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        ///     Get the <c>Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to return the <c>Facings</c> count for.</param>
        /// <returns>The number of facings set for this instance on the given <paramref name="axis"/>.</returns>
        private Int32 GetFacings(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return FacingsWide;
                case AxisType.Y:
                    return FacingsHigh;
                case AxisType.Z:
                    return FacingsDeep;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.GetFacings");
                    return 1;
            }
        }

        /// <summary>
        ///     Set the <c>Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to set the <c>Facings</c> count for.</param>
        /// <param name="value">The amount of <C>Facings</C> to add on the given <paramref name="axis"/>.</param>
        private void SetFacings(AxisType axis, Int32 value)
        {
            //  Facings should not be lower than 1.
            if (value < 1) value = 1;

            switch (axis)
            {
                case AxisType.X:
                    FacingsWide = Convert.ToInt16(value);
                    break;
                case AxisType.Y:
                    FacingsHigh = Convert.ToInt16(value);
                    break;
                case AxisType.Z:
                    FacingsDeep = Convert.ToInt16(value);
                    break;
                default:
                    Debug.Fail("Unknown axis when processing PlanogramPosition.SetFacings");
                    break;
            }
        }

        public void SetFacings(WideHighDeepValue facings)
        {
            FacingsWide = facings.Wide;
            FacingsHigh = facings.High;
            FacingsDeep = facings.Deep;
        }

        /// <summary>
        ///     Add to the <c>Facings</c> count for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> to add to the <c>Facings</c> count for.</param>
        /// <param name="value">The amount of <C>Facings</C> to add on the given <paramref name="axis"/>.</param>
        private void AddFacings(AxisType axis, Int32 value)
        {
            SetFacings(axis, GetFacings(axis) + Convert.ToInt16(value));
        }

        /// <summary>
        /// Where a cap hig, wide or deep is 0 this tidies up any other populated values
        /// back to 0 too.
        /// </summary>
        public void CleanUpUnusedCapValues()
        {
            if (this.FacingsXHigh == 0 ||
                this.FacingsXWide == 0 ||
                this.FacingsXDeep == 0)
            {
                this.FacingsXHigh = 0;
                this.FacingsXWide = 0;
                this.FacingsXDeep = 0;
            }
            if (this.FacingsYHigh == 0 ||
                this.FacingsYWide == 0 ||
                this.FacingsYDeep == 0)
            {
                this.FacingsYHigh = 0;
                this.FacingsYWide = 0;
                this.FacingsYDeep = 0;
            }
            if (this.FacingsZHigh == 0 ||
                this.FacingsZWide == 0 ||
                this.FacingsZDeep == 0)
            {
                this.FacingsZHigh = 0;
                this.FacingsZWide = 0;
                this.FacingsZDeep = 0;
            }
        }

        #endregion

        /// <summary>
        /// Adjusts the position X cap facing by 1 wide
        /// </summary>
        /// <param name="posModel">The position model to adjust</param>
        /// <param name="posDetails">The details of the position to be adjusted.</param>
        /// <param name="isAdjustRight">Flag to indicate if the adjustment is to be made
        /// to the right or the left of the main block.</param>
        public static void AdjustXCapWide(PlanogramPosition posModel, PlanogramPositionDetails posDetails, Boolean isAdjustRight)
        {
            if (posModel.FacingsXHigh == 0) posModel.FacingsXHigh = 1;
            if (posModel.FacingsXDeep == 0) posModel.FacingsXDeep = 1;

            //Adjust the position caps moving right:
            if (isAdjustRight)
            {
                //if we have no caps, add a right one.
                if (posModel.FacingsXWide == 0)
                {
                    posModel.FacingsXWide = 1;
                    posModel.IsXPlacedLeft = false;

                    //recalculate high and deep but ensure that at least 1 is always placed.
                    posModel.FacingsXHigh = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Y, ref posDetails));
                    posModel.FacingsXDeep = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Z, ref posDetails));
                }

                //if the cap is left then reduce by one.
                else if (posModel.IsXPlacedLeft)
                {
                    posModel.FacingsXWide -= 1;
                }

                //if the cap is right already then increase by one.
                else
                {
                    posModel.FacingsXWide += 1;
                }

            }


            //Adjust the position caps moving left:
            else
            {
                //if we have no caps, add a left one.
                if (posModel.FacingsXWide == 0)
                {
                    posModel.FacingsXWide = 1;
                    posModel.IsXPlacedLeft = true;

                    //recalculate high and deep but ensure that at least 1 is always placed.
                    posModel.FacingsXHigh = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Y, ref posDetails));
                    posModel.FacingsXDeep = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.X, AxisType.Z, ref posDetails));
                }

                //if the cap is left already, then increase.
                else if (posModel.IsXPlacedLeft)
                {
                    posModel.FacingsXWide += 1;
                }

                //if the cap is right then decrease.
                else
                {
                    posModel.FacingsXWide -= 1;
                }

            }
        }

        /// <summary>
        /// Adjusts the position Y cap facing by 1 high
        /// </summary>
        /// <param name="isAdjustUp">The adjust direction is up</param>
        public static void AdjustYCapHigh(PlanogramPosition posModel, PlanogramPositionDetails posDetails, Boolean isAdjustUp)
        {
            if (posModel.FacingsYWide == 0) posModel.FacingsYWide = 1;
            if (posModel.FacingsYDeep == 0) posModel.FacingsYDeep = 1;

            //Adjust the position caps upwards:
            if (isAdjustUp)
            {
                //if we have no caps, add a top one.
                if (posModel.FacingsYHigh == 0)
                {
                    posModel.FacingsYHigh = 1;
                    posModel.IsYPlacedBottom = false;

                    //recalculate wide and deep but ensure that at least 1 is always placed.
                    posModel.FacingsYWide = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.X, ref posDetails));
                    posModel.FacingsYDeep = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.Z, ref posDetails));
                }

                //if the cap is bottom then reduce by one.
                else if (posModel.IsYPlacedBottom)
                {
                    posModel.FacingsYHigh -= 1;
                }

                //if the cap is top already then increase by one.
                else
                {
                    posModel.FacingsYHigh += 1;
                }
            }


            //Adjust the position caps downwards:
            else
            {
                //if we have no caps, add a bottom one.
                if (posModel.FacingsYHigh == 0)
                {
                    posModel.FacingsYHigh = 1;
                    posModel.IsYPlacedBottom = true;

                    //recalculate wide and deep but ensure that at least 1 is always placed.
                    posModel.FacingsYWide = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.X, ref posDetails));
                    posModel.FacingsYDeep = Math.Max((Int16)1, posModel.CalculateCapFacingsToMainBlock(PlanogramPositionBlockType.Y, AxisType.Z, ref posDetails));
                }

                //if the cap is bottom already, then increase.
                else if (posModel.IsYPlacedBottom)
                {
                    posModel.FacingsYHigh += 1;
                }

                //if the cap is top then decrease.
                else
                {
                    posModel.FacingsYHigh -= 1;
                }

            }
        }

        #endregion

        #region Squeeze

        /// <summary>
        /// Squeezes the width of this position to the target given
        /// or the minimum allowed amount.
        /// No change will be made if already below the target.
        /// </summary>
        /// <param name="targetWidth"></param>
        /// <returns></returns>
        public Boolean SqueezeWidthTo(Single targetWidth, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            PlanogramPositionDetails posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

            //if the width is already ok then just return out.
            if (posDetails.TotalSize.Width.LessOrEqualThan(targetWidth)) return true;

            //if the target with is below the min then just set to min.
            if (targetWidth.LessThan(posDetails.MinimumSize.Width))
            {
                SqueezeWidthTo(posDetails.MinimumSize.Width, product, subComponentPlacement, merchandisingGroup);
                return false;
            }

            //otherwise apply a specific squeeze to the facings.
            Boolean canSqueezeMain = posDetails.MainMinimumSize.Width < posDetails.MainTotalSize.Width;
            Boolean canSqueezeX = posDetails.XMinimumSize.Width < posDetails.XTotalSize.Width;
            Boolean canSqueezeY = posDetails.YMinimumSize.Width < posDetails.YTotalSize.Width;
            Boolean canSqueezeZ = posDetails.ZMinimumSize.Width < posDetails.ZTotalSize.Width;

            //check x & main first
            Single curBlockWidth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.XCoordinates, posDetails.XTotalSize)).Width;

            if (curBlockWidth > targetWidth)
            {
                //if only the main can be squeezed
                if (canSqueezeMain && !canSqueezeX)
                {
                    Single reductionPerFacing = (curBlockWidth - targetWidth) / this.FacingsWide;
                    Single targetUnitWidth = posDetails.MainRotatedUnitSize.Width - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, targetUnitWidth, null);
                }

                // else if only x can be squeeze
                else if (canSqueezeX && !canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockWidth - targetWidth) / this.FacingsXWide;
                    Single targetUnitWidth = posDetails.XRotatedUnitSize.Width - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.X, null, targetUnitWidth, null);
                }

                //or if both can squeeze
                else if (canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockWidth - targetWidth) / (this.FacingsWide + this.FacingsXWide);

                    //if reduction would put either below min just do it.
                    if ((posDetails.MainTotalSize.Width - (reductionPerFacing * this.FacingsWide)) < posDetails.MainMinimumSize.Width)
                    {
                        this.HorizontalSqueeze = posDetails.MainRotatedMaxSqueeze.Width;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to x
                        curBlockWidth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.XCoordinates, posDetails.XTotalSize)).Width;

                        reductionPerFacing = (curBlockWidth - targetWidth) / this.FacingsXWide;

                        Single targetUnitWidth = posDetails.XRotatedUnitSize.Width - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.X, null, targetUnitWidth, null);
                    }
                    else if ((posDetails.XTotalSize.Width - (reductionPerFacing * this.FacingsXWide)) < posDetails.XMinimumSize.Width)
                    {
                        this.HorizontalSqueezeX = posDetails.XRotatedMaxSqueeze.Width;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to main
                        curBlockWidth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.XCoordinates, posDetails.XTotalSize)).Width;

                        reductionPerFacing = (curBlockWidth - targetWidth) / this.FacingsWide;

                        Single targetUnitWidth = posDetails.MainRotatedUnitSize.Width - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, targetUnitWidth, null);
                    }
                    else
                    {
                        //just apply targets to both
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, posDetails.MainRotatedUnitSize.Width - reductionPerFacing, null);
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.X, null, posDetails.XRotatedUnitSize.Width - reductionPerFacing, null);
                    }
                }
                posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
            }

            //now check the y
            if (canSqueezeY)
            {
                Single yOverhang = (posDetails.YCoordinates.X + posDetails.YTotalSize.Width) - targetWidth;
                if (yOverhang > 0)
                {
                    Single reductionPerFacing = yOverhang / this.FacingsYWide;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, null, posDetails.YRotatedUnitSize.Width - reductionPerFacing, null);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }


            //now check the Z
            if (canSqueezeZ)
            {
                Single zOverhang = (posDetails.ZCoordinates.X + posDetails.ZTotalSize.Width) - targetWidth;
                if (zOverhang > 0)
                {
                    Single reductionPerFacing = zOverhang / this.FacingsZWide;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Z, null, posDetails.ZRotatedUnitSize.Width - reductionPerFacing, null);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }

            //check whether we made our target
            return posDetails.TotalSize.Width.LessOrEqualThan(targetWidth);
        }

        /// <summary>
        /// Squeezes the height of this position to the target given
        /// </summary>
        public Boolean SqueezeHeightTo(Single targetHeight, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            PlanogramPositionDetails posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

            //if the height is already ok then just return out.
            if (posDetails.TotalSize.Height.LessOrEqualThan(targetHeight)) return true;

            //if the target is below the min then just set to min.
            if (targetHeight.LessThan(posDetails.MinimumSize.Height))
            {
                SqueezeHeightTo(posDetails.MinimumSize.Height, product, subComponentPlacement, merchandisingGroup);
                return false;
            }

            //otherwise apply a specific squeeze to the facings.
            Boolean canSqueezeMain = posDetails.MainMinimumSize.Height < posDetails.MainTotalSize.Height;
            Boolean canSqueezeX = posDetails.XMinimumSize.Height < posDetails.XTotalSize.Height;
            Boolean canSqueezeY = posDetails.YMinimumSize.Height < posDetails.YTotalSize.Height;
            Boolean canSqueezeZ = posDetails.ZMinimumSize.Height < posDetails.ZTotalSize.Height;

            //check main & y first
            Single curBlockHeight = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.YCoordinates, posDetails.YTotalSize)).Height;

            if (curBlockHeight > targetHeight)
            {
                //if only the main can be squeezed
                if (canSqueezeMain && !canSqueezeY)
                {
                    Single reductionPerFacing = (curBlockHeight - targetHeight) / this.FacingsHigh;
                    Single targetUnitHeight = posDetails.MainRotatedUnitSize.Height - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, targetUnitHeight, null, null);
                }

                // else if only y can be squeezed
                else if (canSqueezeY && !canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockHeight - targetHeight) / this.FacingsYHigh;
                    Single targetUnitHeight = posDetails.YRotatedUnitSize.Height - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, targetUnitHeight, null, null);
                }

                //or if both can squeeze
                else if (canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockHeight - targetHeight) / (this.FacingsHigh + this.FacingsYHigh);

                    //if reduction would put either below min just do it.
                    if ((posDetails.MainTotalSize.Height - (reductionPerFacing * this.FacingsHigh)) < posDetails.MainMinimumSize.Height)
                    {
                        this.VerticalSqueeze = posDetails.MainRotatedMaxSqueeze.Height;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to y
                        curBlockHeight = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.YCoordinates, posDetails.YTotalSize)).Height;

                        reductionPerFacing = (curBlockHeight - targetHeight) / this.FacingsYHigh;

                        Single targetUnitHeight = posDetails.YRotatedUnitSize.Height - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, targetUnitHeight, null, null);
                    }
                    else if ((posDetails.YTotalSize.Height - (reductionPerFacing * this.FacingsYHigh)) < posDetails.YMinimumSize.Height)
                    {
                        this.VerticalSqueezeY = posDetails.YRotatedMaxSqueeze.Height;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to main
                        curBlockHeight = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.YCoordinates, posDetails.YTotalSize)).Height;

                        reductionPerFacing = (curBlockHeight - targetHeight) / this.FacingsHigh;

                        Single targetUnitHeight = posDetails.MainRotatedUnitSize.Height - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, targetUnitHeight, null, null);
                    }
                    else
                    {
                        //just apply targets to both
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, posDetails.MainRotatedUnitSize.Height - reductionPerFacing, null, null);
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, posDetails.YRotatedUnitSize.Height - reductionPerFacing, null, null);
                    }
                }

                posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
            }


            //now check the X
            if (canSqueezeX)
            {
                Single xOverhang = (posDetails.XCoordinates.Y + posDetails.XTotalSize.Height) - targetHeight;
                if (xOverhang > 0)
                {
                    Single reductionPerFacing = xOverhang / this.FacingsXHigh;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.X, posDetails.XRotatedUnitSize.Height - reductionPerFacing, null, null);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }


            //now check the Z
            if (canSqueezeZ)
            {
                Single zOverhang = (posDetails.ZCoordinates.Y + posDetails.ZTotalSize.Height) - targetHeight;
                if (zOverhang > 0)
                {
                    Single reductionPerFacing = zOverhang / this.FacingsZHigh;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Z, posDetails.ZRotatedUnitSize.Height - reductionPerFacing, null, null);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }


            //check whether we made our target
            return posDetails.TotalSize.Height.LessOrEqualThan(targetHeight);
        }

        /// <summary>
        /// Squeezes the depth of this position to the target given
        /// </summary>
        public Boolean SqueezeDepthTo(Single targetDepth, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, PlanogramMerchandisingGroup merchandisingGroup)
        {
            PlanogramPositionDetails posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

            //if the depth is already ok then just return out.
            if (posDetails.TotalSize.Depth.LessOrEqualThan(targetDepth)) return true;

            //if the target is below the min then just set to min.
            if (targetDepth.LessThan(posDetails.MinimumSize.Depth))
            {
                SqueezeDepthTo(posDetails.MinimumSize.Depth, product, subComponentPlacement, merchandisingGroup);
                return false;
            }

            //otherwise apply a specific squeeze to the facings.
            Boolean canSqueezeMain = posDetails.MainMinimumSize.Depth < posDetails.MainTotalSize.Depth;
            Boolean canSqueezeX = posDetails.XMinimumSize.Depth < posDetails.XTotalSize.Depth;
            Boolean canSqueezeY = posDetails.YMinimumSize.Depth < posDetails.YTotalSize.Depth;
            Boolean canSqueezeZ = posDetails.ZMinimumSize.Depth < posDetails.ZTotalSize.Depth;

            //check the main and z first
            Single curBlockDepth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.ZCoordinates, posDetails.ZTotalSize)).Depth;

            if (curBlockDepth > targetDepth)
            {
                //if only the main can be squeezed
                if (canSqueezeMain && !canSqueezeZ)
                {
                    Single reductionPerFacing = (curBlockDepth - targetDepth) / this.FacingsDeep;
                    Single targetUnitDepth = posDetails.MainRotatedUnitSize.Depth - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, null, targetUnitDepth);
                }

                // else if only z can be squeezed
                else if (canSqueezeZ && !canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockDepth - targetDepth) / this.FacingsZDeep;
                    Single targetUnitDepth = posDetails.ZRotatedUnitSize.Depth - reductionPerFacing;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, null, null, targetUnitDepth);
                }


                //or if both can squeeze
                else if (canSqueezeMain)
                {
                    Single reductionPerFacing = (curBlockDepth - targetDepth) / (this.FacingsDeep + this.FacingsZDeep);

                    //if reduction would put either below min just do it.
                    if ((posDetails.MainTotalSize.Depth - (reductionPerFacing * this.FacingsDeep)) < posDetails.MainMinimumSize.Depth)
                    {
                        this.DepthSqueeze = posDetails.MainRotatedMaxSqueeze.Depth;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to y
                        curBlockDepth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.ZCoordinates, posDetails.ZTotalSize)).Depth;

                        reductionPerFacing = (curBlockDepth - targetDepth) / this.FacingsZDeep;

                        Single targetUnitDepth = posDetails.ZRotatedUnitSize.Depth - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Z, null, null, targetUnitDepth);
                    }
                    else if ((posDetails.ZTotalSize.Depth - (reductionPerFacing * this.FacingsZDeep)) < posDetails.ZMinimumSize.Depth)
                    {
                        this.DepthSqueezeZ = posDetails.ZRotatedMaxSqueeze.Depth;
                        posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);

                        //apply the rest to main
                        curBlockDepth = new RectValue(posDetails.MainCoordinates, posDetails.MainTotalSize).Union(new RectValue(posDetails.ZCoordinates, posDetails.ZTotalSize)).Depth;

                        reductionPerFacing = (curBlockDepth - targetDepth) / this.FacingsDeep;

                        Single targetUnitDepth = posDetails.MainRotatedUnitSize.Depth - reductionPerFacing;
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, null, targetUnitDepth);
                    }
                    else
                    {
                        //just apply targets to both
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Main, null, null, posDetails.MainRotatedUnitSize.Depth - reductionPerFacing);
                        SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Z, null, null, posDetails.ZRotatedUnitSize.Depth - reductionPerFacing);
                    }
                }

                posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
            }

            //now check the X
            if (canSqueezeX)
            {
                Single xOverhang = (posDetails.XCoordinates.Z + posDetails.XTotalSize.Depth) - targetDepth;
                if (xOverhang > 0)
                {
                    Single reductionPerFacing = xOverhang / this.FacingsXDeep;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.X, null, null, posDetails.XRotatedUnitSize.Depth - reductionPerFacing);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }

            //now check the y
            if (canSqueezeY)
            {
                Single yOverhang = (posDetails.YCoordinates.Z + posDetails.YTotalSize.Depth) - targetDepth;
                if (yOverhang > 0)
                {
                    Single reductionPerFacing = yOverhang / this.FacingsYDeep;
                    SqueezeBlockUnitsTo(posDetails, PlanogramPositionBlockType.Y, null, null, posDetails.YRotatedUnitSize.Depth - reductionPerFacing);
                    posDetails = GetPositionDetails(product, subComponentPlacement, merchandisingGroup);
                }
            }

            //check whether we made our target
            return MathHelper.LessOrEqualThan(posDetails.TotalSize.Depth, targetDepth);
        }

        /// <summary>
        /// Applies a squeeze to the requested block.
        /// </summary>
        private void SqueezeBlockUnitsTo(PlanogramPositionDetails posDetails, PlanogramPositionBlockType block, Single? targetUnitHeight, Single? targetUnitWidth, Single? targetUnitDepth)
        {
            switch (block)
            {
                case PlanogramPositionBlockType.Main:
                    if (targetUnitHeight.HasValue)
                        this.VerticalSqueeze = Math.Max(posDetails.MainRotatedMaxSqueeze.Height, targetUnitHeight.Value / posDetails.MainRotatedUnitSize.Height);
                    if (targetUnitWidth.HasValue)
                        this.HorizontalSqueeze = Math.Max(posDetails.MainRotatedMaxSqueeze.Width, targetUnitWidth.Value / posDetails.MainRotatedUnitSize.Width);
                    if (targetUnitDepth.HasValue)
                        this.DepthSqueeze = Math.Max(posDetails.MainRotatedMaxSqueeze.Depth, targetUnitDepth.Value / posDetails.MainRotatedUnitSize.Depth);
                    break;

                case PlanogramPositionBlockType.X:
                    if (targetUnitHeight.HasValue)
                        this.VerticalSqueezeX = Math.Max(posDetails.XRotatedMaxSqueeze.Height, targetUnitHeight.Value / posDetails.XRotatedUnitSize.Height);
                    if (targetUnitWidth.HasValue)
                        this.HorizontalSqueezeX = Math.Max(posDetails.XRotatedMaxSqueeze.Width, targetUnitWidth.Value / posDetails.XRotatedUnitSize.Width);
                    if (targetUnitDepth.HasValue)
                        this.DepthSqueezeX = Math.Max(posDetails.XRotatedMaxSqueeze.Depth, targetUnitDepth.Value / posDetails.XRotatedUnitSize.Depth);
                    break;

                case PlanogramPositionBlockType.Y:
                    if (targetUnitHeight.HasValue)
                        this.VerticalSqueezeY = Math.Max(posDetails.YRotatedMaxSqueeze.Height, targetUnitHeight.Value / posDetails.YRotatedUnitSize.Height);
                    if (targetUnitWidth.HasValue)
                        this.HorizontalSqueezeY = Math.Max(posDetails.YRotatedMaxSqueeze.Width, targetUnitWidth.Value / posDetails.YRotatedUnitSize.Width);
                    if (targetUnitDepth.HasValue)
                        this.DepthSqueezeY = Math.Max(posDetails.YRotatedMaxSqueeze.Depth, targetUnitDepth.Value / posDetails.YRotatedUnitSize.Depth);
                    break;

                case PlanogramPositionBlockType.Z:
                    if (targetUnitHeight.HasValue)
                        this.VerticalSqueezeZ = Math.Max(posDetails.ZRotatedMaxSqueeze.Height, targetUnitHeight.Value / posDetails.ZRotatedUnitSize.Height);
                    if (targetUnitWidth.HasValue)
                        this.HorizontalSqueezeZ = Math.Max(posDetails.ZRotatedMaxSqueeze.Width, targetUnitWidth.Value / posDetails.ZRotatedUnitSize.Width);
                    if (targetUnitDepth.HasValue)
                        this.DepthSqueezeZ = Math.Max(posDetails.ZRotatedMaxSqueeze.Depth, targetUnitDepth.Value / posDetails.ZRotatedUnitSize.Depth);
                    break;
            }
        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Sets the <see cref="SequenceColour"/> and <see cref="SequenceNumber"/> properties using the 
        /// colour of the <paramref name="sequenceGroup"/> the the sequence number of the product within
        /// that group that matches this position's product.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        public void SetSequenceGroup(PlanogramSequenceGroupProduct sequenceProduct)
        {
            if (sequenceProduct == null) return;
            SequenceColour = sequenceProduct.Parent.Colour;
            SequenceNumber = sequenceProduct.SequenceNumber;
        }

        /// <summary>
        /// Copies the <see cref="SequenceNumber"/> and <see cref="SequenceColour"/> values from the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source"></param>
        public void CopySequenceValues(PlanogramPosition source)
        {
            SequenceNumber = source.SequenceNumber;
            SequenceColour = source.SequenceColour;
        }

        /// <summary>
        /// Gets the sequence value in the direction specified.
        /// </summary>
        /// <param name="axis">The direction in which to return the sequence.</param>
        /// <returns>The sequence value for the given direction.</returns>
        public Int16 GetSequence(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return this.SequenceX;
                case AxisType.Y:
                    return this.SequenceY;
                case AxisType.Z:
                    return this.SequenceZ;
                default:
                    throw new ArgumentOutOfRangeException("axis");
            }
        }

        /// <summary>
        /// Takes a position and places its units within a tray. If is already a tray then checks the caps.
        /// </summary>
        /// /// <param name="product">The product the position represents</param>
        /// <param name="unitsInTray">The number of units that make up a tray in each axis.</param>
        /// <returns>True when could transform, false when no changes could be made</returns>
        internal Boolean TryMakeTray(PlanogramProduct product, WideHighDeepValue unitsInTray)
        {
            Int16 trayWideRotated = unitsInTray.Wide;
            Int16 trayHighRotated = unitsInTray.High;
            Int16 trayDeepRotated = unitsInTray.Deep;
            // 1. If we are already a tray we should check if the cappings can become a tray.
            if (IsAppliedMerchandisingStyleAnyOf(product, PlanogramProductMerchandisingStyle.Tray))
            {
                #region Check Top Caps

                //Check Top Caps
                Boolean isYCapWideEnoughToBecomeAFullTray = ((trayWideRotated * FacingsWide) <= FacingsYWide);
                Boolean isYCapHighEnoughToBecomeAFullTray = (trayHighRotated <= FacingsYHigh);
                Boolean isYCapDeepEnoughToBecomeAFullTray = ((trayDeepRotated * FacingsDeep) <= FacingsYDeep);

                //If we have enough to make a full tray from the y caps
                if (isYCapWideEnoughToBecomeAFullTray &&
                    isYCapHighEnoughToBecomeAFullTray &&
                    isYCapDeepEnoughToBecomeAFullTray)
                {
                    //We increase facings / decrease caps.
                    Int16 startUnitsYHigh = FacingsYHigh;
                    if (trayHighRotated != 0)
                    {
                        Int16 amountOfFacingsHighWeCanIncreaseBy = Convert.ToInt16(startUnitsYHigh / trayHighRotated);
                        this.FacingsHigh += amountOfFacingsHighWeCanIncreaseBy;
                        this.FacingsYHigh -= Convert.ToInt16(amountOfFacingsHighWeCanIncreaseBy * trayHighRotated);
                    }
                }

                #endregion

                #region Check Right Caps

                //Check Right Caps
                Boolean isXCapWideEnoughToBecomeAFullTray = (trayWideRotated <= FacingsXWide);
                Boolean isXCapHighEnoughToBecomeAFullTray = ((trayHighRotated * FacingsHigh) <= FacingsXHigh);
                Boolean isXCapDeepEnoughToBecomeAFullTray = ((trayDeepRotated * FacingsDeep) <= FacingsXDeep);

                //If we have enough to make a full tray from the y caps
                if (isXCapWideEnoughToBecomeAFullTray &&
                    isXCapHighEnoughToBecomeAFullTray &&
                    isXCapDeepEnoughToBecomeAFullTray)
                {
                    //We increase facings / decrease caps.
                    Int16 startUnitsXWide = FacingsXWide;
                    if (trayWideRotated != 0)
                    {
                        Int16 amountOfFacingsWideWeCanIncreaseBy = Convert.ToInt16(startUnitsXWide / trayWideRotated);
                        this.FacingsWide += amountOfFacingsWideWeCanIncreaseBy;
                        this.FacingsXWide -= Convert.ToInt16(amountOfFacingsWideWeCanIncreaseBy * trayWideRotated);
                    }
                }

                #endregion

                #region Check Back Caps

                //Check Back Caps
                Boolean isZCapWideEnoughToBecomeAFullTray = ((trayWideRotated * FacingsWide) <= FacingsZWide);
                Boolean isZCapHighEnoughToBecomeAFullTray = ((trayHighRotated * FacingsHigh) <= FacingsZHigh);
                Boolean isZCapDeepEnoughToBecomeAFullTray = (trayDeepRotated <= FacingsZDeep);

                //If we have enough to make a full tray from the y caps
                if (isZCapWideEnoughToBecomeAFullTray &&
                    isZCapHighEnoughToBecomeAFullTray &&
                    isZCapDeepEnoughToBecomeAFullTray)
                {
                    //We increase facings / decrease caps.
                    Int16 startUnitsZDeep = FacingsZDeep;
                    if (trayDeepRotated != 0)
                    {
                        Int16 amountOfFacingsDeepWeCanIncreaseBy = Convert.ToInt16(startUnitsZDeep / trayDeepRotated);
                        this.FacingsDeep += amountOfFacingsDeepWeCanIncreaseBy;
                        this.FacingsZDeep -= Convert.ToInt16(amountOfFacingsDeepWeCanIncreaseBy * trayDeepRotated);
                    }
                }

                #endregion

                //Return success.
                return true;
            }

            // 2. If it's not allready a tray, because we are an automated process we expect that the position should become a tray where possible.
            if (this.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
            {
                #region Check that meets break up/down/back/top rules already

                //If it cannot break down don't allow it to be a unit.
                if (FacingsWide <= trayWideRotated &&
                    !product.CanBreakTrayDown)
                {
                    this.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                    Int16 calculatedTrayUnitFacings = Convert.ToInt16(Math.Floor((float)(FacingsWide / trayWideRotated)));
                    this.FacingsXWide = Convert.ToInt16(FacingsWide - calculatedTrayUnitFacings);
                    this.FacingsWide = calculatedTrayUnitFacings;
                }

                #endregion

                #region Convert Units to Tray

                Boolean isWideEnoughToBecomeAFullTray = (trayWideRotated <= FacingsWide);
                Boolean isHighEnoughToBecomeAFullTray = (trayHighRotated <= FacingsHigh);
                Boolean isDeepEnoughToBecomeAFullTray = (trayDeepRotated <= FacingsDeep);

                //Can only convert into a tray when there are enough units in all dimensions. 
                if (!isHighEnoughToBecomeAFullTray ||
                    !isWideEnoughToBecomeAFullTray ||
                    !isDeepEnoughToBecomeAFullTray)
                    return false;

                //Store the original units
                Int16 startUnitsWide = FacingsWide;
                Int16 startUnitsHigh = FacingsHigh;
                Int16 startUnitsDeep = FacingsDeep;

                //Update main facings now that It is a tray not a unit
                this.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                this.FacingsWide = Convert.ToInt16(Math.Floor((float)(startUnitsWide / trayWideRotated)));
                this.FacingsHigh = Convert.ToInt16(Math.Floor((float)(startUnitsHigh / trayHighRotated)));
                this.FacingsDeep = Convert.ToInt16(Math.Floor((float)(startUnitsDeep / trayDeepRotated)));

                //We need to update all cappings to be the units that are left over after placing the rest within a tray
                this.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
                OrientationTypeZ = product.OrientationType.ToPositionOrientationType();
                this.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                OrientationTypeX = product.OrientationType.ToPositionOrientationType();
                this.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                OrientationTypeY = product.OrientationType.ToPositionOrientationType();

                //Store the calculated values. (For performance reasons).
                Int16 amountOfUnitsThatFitWide = Convert.ToInt16(FacingsWide * trayWideRotated);
                Int16 amountOfUnitsThatFitHigh = Convert.ToInt16(FacingsHigh * trayHighRotated);
                Int16 amountOfUnitsThatFitDeep = Convert.ToInt16(FacingsDeep * trayDeepRotated);

                this.FacingsXWide = Convert.ToInt16(startUnitsWide - (FacingsWide * trayWideRotated));
                this.FacingsXHigh = amountOfUnitsThatFitHigh;
                this.FacingsXDeep = amountOfUnitsThatFitDeep;

                this.FacingsYWide = amountOfUnitsThatFitWide;
                this.FacingsYHigh = Convert.ToInt16(startUnitsHigh - (FacingsHigh * trayHighRotated));
                this.FacingsYDeep = amountOfUnitsThatFitDeep;

                this.FacingsZWide = amountOfUnitsThatFitWide;
                this.FacingsZHigh = amountOfUnitsThatFitHigh;
                this.FacingsZDeep = Convert.ToInt16(startUnitsDeep - (FacingsDeep * trayDeepRotated));

                //Last thing to do is ensure if we reached 0 facings then we increase to one so it appears at least in one dimension.
                //We can never allow facings less than one, though this will occur on first fill to a tray. So Check.
                if (this.FacingsWide == 0) FacingsWide++;
                if (this.FacingsHigh == 0) FacingsHigh++;
                if (this.FacingsDeep == 0) FacingsDeep++;

                //Return success.
                return true;

                #endregion
            }

            // 3. If it is another merch style then we can't currently transform into tray equivalent
            //  - Haven't tested the effects of just letting step 2 run when not a 'unit', so for now return false.
            return false;
        }

        /// <summary>
        /// Sets the existing cappings to fill the remaining dimension of the positions body and then to include dimensions of other cappings on the other axis.
        /// </summary>
        /// <param name="axis">The capping facing axis the caps are to be updated. i.e. facingsx is axistype.x</param>
        /// <param name="unitsInTray">The number of units that make up a tray in each axis.</param>
        /// <remarks>Useful to ensure capps are increased along with the facings. </remarks>
        private void UpdateBreakOutOfTrayCappingsOnTrayPositions(AxisType axis, WideHighDeepValue unitsInTray)
        {
            //If we are a unit on a tray product we shoudln't have any caps!
            //If we do they require integrating into the main body.
            //Note: This behaviour >may< change if in the future we choose to implement tray capping features.
            if (MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit)
            {
                foreach (AxisType axisWithCaps in axis.GetNormals().Where(HasFacingCaps))
                {
                    SetFacings(axisWithCaps, (Int16)(GetFacings(axis) + GetFacingCapUnits(axisWithCaps)));
                    SetFacingCapUnits(axisWithCaps, 0);
                }
            }
            else
            {
                //  Ensure the existing cap units meet the dimensions of the entire position.
                Int16 units = unitsInTray.GetSize(axis);

                //  Calculate the cap units limit from the main units, and including any existing cap units.
                Int16 capUnitsLimit = Convert.ToInt16(units * GetFacings(axis));
                SetFacingCapUnitsMain(axis, capUnitsLimit);
            }
        }

        internal Boolean IsAppliedMerchandisingStyleAnyOf(PlanogramProduct product, params PlanogramProductMerchandisingStyle[] merchandisingStyles)
        {
            PlanogramProductMerchandisingStyle appliedMerchandisingStyle = GetAppliedMerchandisingStyle(product, PlanogramPositionBlockType.Main);
            return merchandisingStyles.Any(o => o.Equals(appliedMerchandisingStyle));
        }

        /// <summary>
        ///     Enforce any Assortment rules appropriate for this position.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="subComponentPlacement"></param>
        /// <param name="merchandisingGroup"></param>
        /// <param name="assortmentRuleEnforcer"></param>
        /// <returns></returns>
        internal Boolean EnforceRules(PlanogramProduct product,
                                      PlanogramSubComponentPlacement subComponentPlacement,
                                      PlanogramMerchandisingGroup merchandisingGroup,
                                      IAssortmentRuleEnforcer assortmentRuleEnforcer,
                                      AxisType? changeAxis = null,
                                      PlanogramMerchandisingInventoryChangeType inventoryChangeType = PlanogramMerchandisingInventoryChangeType.ByUnits)
        {
            //  Enforce Assortment Rules if there are any.
            if (assortmentRuleEnforcer == null) return true;

            PlanogramAssortmentProduct assortmentProduct = product.GetPlanogramAssortmentProduct();
            foreach (IBrokenAssortmentRule brokenRule in assortmentRuleEnforcer.GetBrokenRulesFor(assortmentProduct, merchandisingGroup))
            {
                switch (brokenRule.Type)
                {
                    case BrokenRuleType.ProductMaxUnits:
                        if(changeAxis.HasValue)
                        {
                            if(inventoryChangeType == PlanogramMerchandisingInventoryChangeType.ByUnits)
                            {
                                DecreaseUnits(changeAxis.Value, product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer);
                            }
                            else
                            {
                                DecreaseFacings(product, subComponentPlacement, merchandisingGroup, assortmentRuleEnforcer);
                            }
                        }
                        else
                        {
                            SetUnits(product,
                                     subComponentPlacement,
                                     merchandisingGroup,
                                     assortmentProduct.MaxListUnits ?? 1,
                                     PlanogramPositionInventoryTargetType.LessThanOrEqualTo,
                                     null);
                        }
                        return false;
                    case BrokenRuleType.ProductMaxFacings:
                        SetFacings(AxisType.X, assortmentProduct.MaxListFacings ?? 1);
                        return false;
                    case BrokenRuleType.ProductPreserveUnits:
                        SetUnits(product,
                                 subComponentPlacement,
                                 merchandisingGroup,
                                 assortmentProduct.PreserveListUnits ?? 1,
                                 PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo,
                                 null);
                        return false;
                    case BrokenRuleType.ProductPreserveFacings:
                        SetFacings(AxisType.X, assortmentProduct.PreserveListFacings ?? 1);
                        return false;
                    case BrokenRuleType.ProductMinimumHurdleUnits:
                        SetUnits(product,
                                 subComponentPlacement,
                                 merchandisingGroup,
                                 assortmentProduct.MinListUnits ?? 1,
                                 PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo,
                                 null);
                        return false;
                    case BrokenRuleType.ProductMinimumHurdleFacings:
                        SetFacings(AxisType.X, assortmentProduct.MinListFacings ?? 1);
                        return false;
                    case BrokenRuleType.ProductExactUnits:
                        SetUnits(product,
                                 subComponentPlacement,
                                 merchandisingGroup,
                                 assortmentProduct.ExactListUnits ?? 1,
                                 PlanogramPositionInventoryTargetType.ClosestOrEqualTo,
                                 null);
                        return false;
                    case BrokenRuleType.ProductExactFacings:
                        SetFacings(AxisType.X, assortmentProduct.ExactListFacings ?? 1);
                        return false;
                    default:
                        Debug.Fail("Unknown Broken Rule Type.");
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        ///     Get the current total unit count for this instance of <see cref="PlanogramPosition"/>.
        /// </summary>
        /// <returns></returns>
        public Int32 GetTotalUnitCount()
        {
            return FacingsDeep * FacingsHigh * FacingsWide +
                   FacingsXDeep * FacingsXHigh * FacingsXWide +
                   FacingsYDeep * FacingsYHigh * FacingsYWide +
                   FacingsZDeep * FacingsZHigh * FacingsZWide;
        }

        #endregion

        #region IPlanogramSubComponentSequencedItem Members

        /// <summary>
        /// Gets the <see cref="PlanogramSubComponentPlacement"/> that this item is sequenced on.
        /// </summary>
        /// <returns></returns>
        PlanogramSubComponentPlacement IPlanogramSubComponentSequencedItem.GetParentSubComponentPlacement()
        {
            return PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(this);
        }

        #endregion

        /// <summary>
        /// Gets a simple clone of this positions by instantiating a new <see cref="PlanogramPosition"/> and loading
        /// in the data from this instance's dto. Although this copies the property data, it avoids checking business rules
        /// on the new instance, which may have inaccurate validation flags, etc. This method is specifically for use
        /// as a performance enhancement in engine merchandising operations.
        /// </summary>
        /// <returns></returns>
        internal PlanogramPosition GetOptimisationClone()
        {
            PlanogramPosition clone = new PlanogramPosition();
            clone.LoadDataTransferObject(null, GetDataTransferObject(Parent));
            return clone;
        }

        #endregion
    }

    //TODO: Review and move to framework if approved.
    public static partial class WideHighDeepValueExtensions
    {
        /// <summary>
        ///     Get the size on the <paramref name="source"/> for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="source">The <see cref="WideHighDeepValue"/> to obtain the axis size from.</param>
        /// <param name="axis">The <see cref="AxisType"/> to get the size for.</param>
        /// <returns>The size that corresponds to the given <paramref name="axis"/>.</returns>
        public static Int16 GetSize(this WideHighDeepValue source, AxisType axis)
        {
            Int16 units = 0;
            switch (axis)
            {
                case AxisType.X:
                    units = source.Wide;
                    break;
                case AxisType.Y:
                    units = source.High;
                    break;
                case AxisType.Z:
                    units = source.Deep;
                    break;
                default:
                    Debug.Fail("Unknown axis when processing WideHighDeepValue.GetSize");
                    return units;
            }
            return units;
        }
    }
}