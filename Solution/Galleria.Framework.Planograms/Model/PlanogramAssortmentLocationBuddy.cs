﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A class representing an Assortment Location Buddy within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    public partial class PlanogramAssortmentLocationBuddy : ModelObject<PlanogramAssortmentLocationBuddy>, IPlanogramAssortmentLocationBuddy
    {
        #region Static Constructor
        static PlanogramAssortmentLocationBuddy()
        {
        }
        #endregion

        #region Properties
        
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// The Location Code
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
            set { SetProperty<String>(LocationCodeProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentLocationBuddyTreatmentType> TreatmentTypeProperty =
        RegisterModelProperty<PlanogramAssortmentLocationBuddyTreatmentType>(c => c.TreatmentType);
        public PlanogramAssortmentLocationBuddyTreatmentType TreatmentType
        {
            get { return GetProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty, value); }
        }
        
        #region S1

        #region S1LocationCode

        private static readonly ModelPropertyInfo<String> S1LocationCodeProperty =
            RegisterModelProperty<String>(c => c.S1LocationCode);
        /// <summary>
        /// The LocationCode of the source location 1
        /// </summary>
        public String S1LocationCode
        {
            get { return GetProperty<String>(S1LocationCodeProperty); }
            set { SetProperty<String>(S1LocationCodeProperty, value); }
        }
        
        #endregion

        public static readonly ModelPropertyInfo<Single?> S1PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S1Percentage);
        public Single? S1Percentage
        {
            get { return GetProperty<Single?>(S1PercentageProperty); }
            set { SetProperty<Single?>(S1PercentageProperty, value); }
        }

        #endregion

        #region S2

        #region S2LocationCode

        private static readonly ModelPropertyInfo<String> S2LocationCodeProperty =
            RegisterModelProperty<String>(c => c.S2LocationCode);
        /// <summary>
        /// The LocationCode of the source location 2
        /// </summary>
        public String S2LocationCode
        {
            get { return GetProperty<String>(S2LocationCodeProperty); }
            set { SetProperty<String>(S2LocationCodeProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S2PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S2Percentage);
        public Single? S2Percentage
        {
            get { return GetProperty<Single?>(S2PercentageProperty); }
            set { SetProperty<Single?>(S2PercentageProperty, value); }
        }

        #endregion

        #region S3

        #region S3LocationCode

        private static readonly ModelPropertyInfo<String> S3LocationCodeProperty =
            RegisterModelProperty<String>(c => c.S3LocationCode);
        /// <summary>
        /// The LocationCode of the source location 3
        /// </summary>
        public String S3LocationCode
        {
            get { return GetProperty<String>(S3LocationCodeProperty); }
            set { SetProperty<String>(S3LocationCodeProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S3PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S3Percentage);
        public Single? S3Percentage
        {
            get { return GetProperty<Single?>(S3PercentageProperty); }
            set { SetProperty<Single?>(S3PercentageProperty, value); }
        }

        #endregion

        #region S4

        #region S4LocationCode

        private static readonly ModelPropertyInfo<String> S4LocationCodeProperty =
            RegisterModelProperty<String>(c => c.S4LocationCode);
        /// <summary>
        /// The LocationCode of the source location 4
        /// </summary>
        public String S4LocationCode
        {
            get { return GetProperty<String>(S4LocationCodeProperty); }
            set { SetProperty<String>(S4LocationCodeProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S4PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S4Percentage);
        public Single? S4Percentage
        {
            get { return GetProperty<Single?>(S4PercentageProperty); }
            set { SetProperty<Single?>(S4PercentageProperty, value); }
        }

        #endregion

        #region S5

        #region S5LocationCode

        private static readonly ModelPropertyInfo<String> S5LocationCodeProperty =
            RegisterModelProperty<String>(c => c.S5LocationCode);
        /// <summary>
        /// The LocationCode of the source location 5
        /// </summary>
        public String S5LocationCode
        {
            get { return GetProperty<String>(S5LocationCodeProperty); }
            set { SetProperty<String>(S5LocationCodeProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S5PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S5Percentage);
        public Single? S5Percentage
        {
            get { return GetProperty<Single?>(S5PercentageProperty); }
            set { SetProperty<Single?>(S5PercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {

            base.AddBusinessRules();

            BusinessRules.AddRule(new NullableMaxValue<Single>(S1PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S2PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S3PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S4PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S5PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMinValue<Single>(S1PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S2PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S3PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S4PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S5PercentageProperty, 0.01f));
            BusinessRules.AddRule(new MaxLength(S1PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S2PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S3PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S4PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S5PercentageProperty, 4));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramAssortmentLocationBuddy NewPlanogramAssortmentLocationBuddy()
        {
            PlanogramAssortmentLocationBuddy item = new PlanogramAssortmentLocationBuddy();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramAssortmentLocationBuddy NewPlanogramAssortmentLocationBuddy(String locationCode)
        {
            PlanogramAssortmentLocationBuddy item = new PlanogramAssortmentLocationBuddy();
            item.Create(locationCode);
            return item;
        }

        public static PlanogramAssortmentLocationBuddy NewPlanogramAssortmentLocationBuddy(IPlanogramAssortmentLocationBuddy buddy)
        {
            var item = new PlanogramAssortmentLocationBuddy();
            item.Create(buddy);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(String locationCode)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(LocationCodeProperty, locationCode);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(IPlanogramAssortmentLocationBuddy buddy)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(LocationCodeProperty, buddy.LocationCode);
            LoadProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty, buddy.TreatmentType);
            LoadProperty<String>(S1LocationCodeProperty, buddy.S1LocationCode);
            LoadProperty<Single?>(S1PercentageProperty, buddy.S1Percentage);
            LoadProperty<String>(S2LocationCodeProperty, buddy.S2LocationCode);
            LoadProperty<Single?>(S2PercentageProperty, buddy.S2Percentage);
            LoadProperty<String>(S3LocationCodeProperty, buddy.S3LocationCode);
            LoadProperty<Single?>(S3PercentageProperty, buddy.S3Percentage);
            LoadProperty<String>(S4LocationCodeProperty, buddy.S4LocationCode);
            LoadProperty<Single?>(S4PercentageProperty, buddy.S4Percentage);
            LoadProperty<String>(S5LocationCodeProperty, buddy.S5LocationCode);
            LoadProperty<Single?>(S5PercentageProperty, buddy.S5Percentage);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        public Int32 GetNumberOfSourceLocations()
        {
            Int32 numOfSourceLocations = 0;

            if (String.IsNullOrEmpty(this.S1LocationCode))
            {
                return numOfSourceLocations;
            }
            numOfSourceLocations++;

            if (String.IsNullOrEmpty(this.S2LocationCode))
            {
                return numOfSourceLocations;
            }
            numOfSourceLocations++;

            if (String.IsNullOrEmpty(this.S3LocationCode))
            {
                return numOfSourceLocations;
            }
            numOfSourceLocations++;

            if (String.IsNullOrEmpty(this.S4LocationCode))
            {
                return numOfSourceLocations;
            }
            numOfSourceLocations++;

            if (String.IsNullOrEmpty(this.S5LocationCode))
            {
                return numOfSourceLocations;
            }
            numOfSourceLocations++;

            return numOfSourceLocations;
        }

        #endregion
    }
}
