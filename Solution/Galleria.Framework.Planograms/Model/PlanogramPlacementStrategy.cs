﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enumeration that defines the placement
    /// </summary>
    public enum PlanogramPlacementStrategy : byte
    {
        Min = 0,
        Max = 1,
        Even = 2,
        Manual = 3
    }
}
