﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History: CCM830
// CCM-32558 : L.Ineson
//  Made sure that adding components to an assembly also updated linked annotations.
//V8-31845 : L.Ineson
//  Corrected how set of coordintes when creating a new assembly.
// CCM-13587 : D.Pleasance
//  Amended Add() to update Positions PlanogramFixtureItemId to the fixture that contains the assembly.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Planograms.Resources.Language;
using System.Collections.Generic;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of assembly items contained within a fixture
    /// </summary>
    [Serializable]
    public partial class PlanogramFixtureAssemblyList : ModelList<PlanogramFixtureAssemblyList, PlanogramFixtureAssembly>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramFixture Parent
        {
            get { return (PlanogramFixture)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureAssemblyList NewPlanogramFixtureAssemblyList()
        {
            PlanogramFixtureAssemblyList item = new PlanogramFixtureAssemblyList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new assembly to the fixture
        /// </summary>
        public PlanogramFixtureAssembly Add()
        {
            PlanogramFixture fixture = this.Parent;
            Planogram plan = fixture.Parent;
            Int32 assemblyNo = plan.Assemblies.Count;

            PlanogramAssembly assembly = PlanogramAssembly.NewPlanogramAssembly(String.Format(Message.PlanogramAssembly_Name_New, assemblyNo));
            plan.Assemblies.Add(assembly);

            PlanogramFixtureAssembly fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly.PlanogramAssemblyId = assembly.Id;
            fixture.Assemblies.Add(fixtureAssembly);

            return fixtureAssembly;
        }

        /// <summary>
        /// Adds a new assembly to the fixture created from the given components
        /// </summary>
        /// <param name="components">The components to be included in the new assembly.</param>
        public PlanogramFixtureAssembly Add(IEnumerable<Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>> components, PlanogramFixtureItem assemblyFixtureItem)
        {
            if (!components.Any()) return null;

            PlanogramFixture fixture = this.Parent;
            Planogram plan = fixture.Parent;
            Int32 assemblyNo = plan.Assemblies.Count +1;

            PlanogramAssembly assembly = PlanogramAssembly.NewPlanogramAssembly(String.Format(Message.PlanogramAssembly_Name_New, assemblyNo));
            plan.Assemblies.Add(assembly);

            PlanogramFixtureAssembly fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly.PlanogramAssemblyId = assembly.Id;
            fixtureAssembly.X = components.Min(c => c.Item1.X);
            fixtureAssembly.Y = components.Min(c => c.Item1.Y);
            fixtureAssembly.Z = components.Min(c => c.Item1.Z);
            fixture.Assemblies.Add(fixtureAssembly);


            foreach(Tuple<PlanogramFixtureComponent, PlanogramFixtureItem> componentPlacement in components)
            {
                PlanogramFixtureComponent fixtureComponent = componentPlacement.Item1;
                PointValue currentWorldCoordinate = fixtureComponent.GetPlanogramRelativeCoordinates(componentPlacement.Item2);

                //get all positions related to the component.
                List<PlanogramPosition> positions = 
                    fixtureComponent.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions()).ToList();

                List<PlanogramAnnotation> annotations = 
                    fixtureComponent.Parent.Parent.Annotations.Where(a=> Object.Equals(a.PlanogramFixtureComponentId, fixtureComponent.Id)).ToList();

                //add the new assembly component
                PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(fixtureComponent.GetPlanogramComponent());
                assembly.Components.Add(assemblyComponent);

                //update the component coordinates so that they have not changed.
                assemblyComponent.SetCoordinatesFromPlanogramRelative(currentWorldCoordinate, assemblyFixtureItem, fixtureAssembly);
                
                //reassociate positions
                foreach (PlanogramPosition pos in positions)
                {
                    pos.PlanogramFixtureItemId = assemblyFixtureItem.Id;
                    pos.PlanogramFixtureAssemblyId = fixtureAssembly.Id;
                    pos.PlanogramAssemblyComponentId = assemblyComponent.Id;
                    pos.PlanogramFixtureComponentId = null;
                }

                //reassociate annotations
                foreach (PlanogramAnnotation annotation in annotations)
                {
                    annotation.PlanogramFixtureItemId = assemblyFixtureItem.Id;
                    annotation.PlanogramFixtureAssemblyId = fixtureAssembly.Id;
                    annotation.PlanogramAssemblyComponentId = assemblyComponent.Id;
                    annotation.PlanogramFixtureComponentId = null;
                }

                //remove from the old parent
                fixtureComponent.Parent.Components.Remove(fixtureComponent);
            }


            return fixtureAssembly;
        }

        #endregion
    }
}