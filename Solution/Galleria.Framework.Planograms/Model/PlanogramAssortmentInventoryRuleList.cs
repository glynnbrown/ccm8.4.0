﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentInventoryRuleList : ModelList<PlanogramAssortmentInventoryRuleList, PlanogramAssortmentInventoryRule>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent objecta
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentInventoryRuleList NewPlanogramAssortmentInventoryRuleList()
        {
            var item = new PlanogramAssortmentInventoryRuleList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion


        #region Methods

        public void AddRange(IEnumerable<IPlanogramAssortmentInventoryRule> inventoryRules)
        {
            base.AddRange(inventoryRules.Select(r => PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(r)));
        }

        #endregion

        public void RemoveByGtin(String gtin)
        {
            List<PlanogramAssortmentInventoryRule> rulesForGtin = this.Where(p => p.ProductGtin == gtin).ToList();
            RemoveList(rulesForGtin);
        }
    }
}
