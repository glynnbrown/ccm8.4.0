﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27738 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A simple enum that defines how we
    /// want to achieve inventory for a position
    /// </summary>
    public enum PlanogramPositionInventoryTargetType : byte
    {
        ClosestOrEqualTo = 0,
        GreaterThanOrEqualTo = 1,
        LessThanOrEqualTo = 2
    }
}
