﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Sub Component Type Enum
    /// </summary>
    public enum PlanogramSubComponentType
    {
        None= 0, 
        Shelf = 1, 
        Bar = 2,
        Peg = 3, 
        Chest = 4,
        //Backboard = 5
    }

    /// <summary>
    /// PlanogramSubComponentType Helper Class
    /// </summary>
    public static class PlanogramSubComponentTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentType, String>()
            {
                {PlanogramSubComponentType.None, Message.Enum_PlanogramSubComponentType_None},
                {PlanogramSubComponentType.Shelf, Message.Enum_PlanogramSubComponentType_Shelf},
                {PlanogramSubComponentType.Bar, Message.Enum_PlanogramSubComponentType_Bar},
                {PlanogramSubComponentType.Peg, Message.Enum_PlanogramSubComponentType_Peg},
                {PlanogramSubComponentType.Chest, Message.Enum_PlanogramSubComponentType_Chest},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentType, String>()
            {
                {PlanogramSubComponentType.None, Message.Enum_PlanogramSubComponentType_None},
                {PlanogramSubComponentType.Shelf, Message.Enum_PlanogramSubComponentType_Shelf},
                {PlanogramSubComponentType.Bar, Message.Enum_PlanogramSubComponentType_Bar},
                {PlanogramSubComponentType.Peg, Message.Enum_PlanogramSubComponentType_Peg},
                {PlanogramSubComponentType.Chest, Message.Enum_PlanogramSubComponentType_Chest},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentType>()
            {
                {Message.Enum_PlanogramSubComponentType_None.ToLowerInvariant(), PlanogramSubComponentType.None},
                {Message.Enum_PlanogramSubComponentType_Shelf.ToLowerInvariant(), PlanogramSubComponentType.Shelf},
                {Message.Enum_PlanogramSubComponentType_Bar.ToLowerInvariant(), PlanogramSubComponentType.Bar},
                {Message.Enum_PlanogramSubComponentType_Peg.ToLowerInvariant(), PlanogramSubComponentType.Peg},
                {Message.Enum_PlanogramSubComponentType_Chest.ToLowerInvariant(), PlanogramSubComponentType.Chest},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentType? PlanogramSubComponentTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentType? returnValue = null;
            PlanogramSubComponentType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}