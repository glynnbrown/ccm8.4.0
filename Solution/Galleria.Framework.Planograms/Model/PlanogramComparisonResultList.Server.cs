﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonResultList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonResultList() { }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonResultList"/>.
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramComparisonResultDal>())
            {
                IEnumerable<PlanogramComparisonResultDto> dtoList = dal.FetchByPlanogramComparisonId(criteria.ParentId);
                foreach (PlanogramComparisonResultDto dto in dtoList)
                {
                    Add(PlanogramComparisonResult.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}