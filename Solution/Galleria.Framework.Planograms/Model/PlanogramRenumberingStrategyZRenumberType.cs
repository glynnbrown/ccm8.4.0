﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Types of Z (depth) renumber strategies.
    /// </summary>
    [Serializable]
    public enum PlanogramRenumberingStrategyZRenumberType
    {
        FrontToBack,
        BackToFront
    }

    public static class PlanogramRenumberingStrategyZRenumberTypeHelper
    {
        /// <summary>
        ///     Maps each value to its friendly name.
        /// </summary>
        public static readonly Dictionary<PlanogramRenumberingStrategyZRenumberType, String> FriendlyNames =
            new Dictionary<PlanogramRenumberingStrategyZRenumberType, String>
            {
                {PlanogramRenumberingStrategyZRenumberType.FrontToBack, "Front to Back"},
                {PlanogramRenumberingStrategyZRenumberType.BackToFront, "Back to Front"}
            };
    }
}
