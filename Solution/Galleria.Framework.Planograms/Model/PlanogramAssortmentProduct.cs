﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added helper properties for UI.
// V8-26799 : A.Kuszyk
//  Removed LocationCode property.
#endregion
#region Version History: CCM820
// V8-30762 : I.George
// Added MetaData field MetaIsProductPlacedOnPlanogram
// V8-30705 : A.Probyn
//  Added IsAvailableToPlace helper property
//  Added DelistedDueToAssortmentRule
//  Added EnforceNonForceAssortmentRuleUnits method
#endregion
#region Version History: CCM830
// V8-32025 : A.Probyn
//  Fixed bug in IsAvailableToPlace where local lines were only looking at first location.
// V8-32396 : A.Probyn
//  Updated GTIN references to Gtin
// CCM-14014 : M.Pettit
//  Added EnumerateDisplayableFieldInfos
#endregion
#endregion

using System;
using System.Linq;
using System.Globalization;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentProduct : ModelObject<PlanogramAssortmentProduct>, IPlanogramAssortmentProduct, IPlanogramProductInfo
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)((PlanogramAssortmentProductList)base.Parent).Parent; }
        }
        #endregion

        #region Gtin
        /// <summary>
        /// Gtin property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin, Message.PlanogramAssortmentProduct_Gtin);
        /// <summary>
        /// Gtin
        /// </summary>
        public String Gtin
        {
            get { return GetProperty<String>(GtinProperty); }
            set { SetProperty<String>(GtinProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c=>c.Name,Message.PlanogramAssortmentProduct_Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region IsRanged
        /// <summary>
        /// IsRanged property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRangedProperty =
            RegisterModelProperty<Boolean>(c=>c.IsRanged,Message.PlanogramAssortmentProduct_IsRanged);
        /// <summary>
        /// IsRanged
        /// </summary>
        public Boolean IsRanged
        {
            get { return GetProperty<Boolean>(IsRangedProperty); }
            set { SetProperty<Boolean>(IsRangedProperty, value); }
        }
        #endregion

        #region Rank
        /// <summary>
        /// Rank property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> RankProperty =
            RegisterModelProperty<Int16>(c=>c.Rank, Message.PlanogramAssortmentProduct_Rank);
        /// <summary>
        /// Rank
        /// </summary>
        public Int16 Rank
        {
            get { return GetProperty<Int16>(RankProperty); }
            set { SetProperty<Int16>(RankProperty, value); }
        }
        #endregion

        #region Segmentation
        /// <summary>
        /// Segmentation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SegmentationProperty =
            RegisterModelProperty<String>(c=>c.Segmentation, Message.PlanogramAssortmentProduct_Segmentation);
        /// <summary>
        /// Segmentation
        /// </summary>
        public String Segmentation
        {
            get { return GetProperty<String>(SegmentationProperty); }
            set { SetProperty<String>(SegmentationProperty, value); }
        }
        #endregion

        #region Facings
        /// <summary>
        /// Facings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> FacingsProperty =
            RegisterModelProperty<Byte>(c=>c.Facings, Message.PlanogramAssortmentProduct_Facings);
        /// <summary>
        /// Facings
        /// </summary>
        public Byte Facings
        {
            get { return GetProperty<Byte>(FacingsProperty); }
            set { SetProperty<Byte>(FacingsProperty, value); }
        }
        #endregion

        #region Units
        /// <summary>
        /// Units property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> UnitsProperty =
            RegisterModelProperty<Int16>(c=>c.Units, Message.PlanogramAssortmentProduct_Units);
        /// <summary>
        /// Units
        /// </summary>
        public Int16 Units
        {
            get { return GetProperty<Int16>(UnitsProperty); }
            set { SetProperty<Int16>(UnitsProperty, value); }
        }
        #endregion

        #region ProductTreatmentType
        /// <summary>
        /// ProductTreatmentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentProductTreatmentType> ProductTreatmentTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductTreatmentType>(c=>c.ProductTreatmentType,Message.PlanogramAssortmentProduct_ProductTreatmentType);
        /// <summary>
        /// ProductTreatmentType
        /// </summary>
        public PlanogramAssortmentProductTreatmentType ProductTreatmentType
        {
            get { return GetProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty, value); }
        }
        #endregion

        #region ProductLocalizationType
        /// <summary>
        /// ProductLocalizationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentProductLocalizationType> ProductLocalizationTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductLocalizationType>(c=>c.ProductLocalizationType, Message.PlanogramAssortmentProduct_ProductLocalizationType);
        /// <summary>
        /// ProductLocalizationType
        /// </summary>
        public PlanogramAssortmentProductLocalizationType ProductLocalizationType
        {
            get { return GetProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty, value); }
        }
        #endregion

        #region Comments
        /// <summary>
        /// Comments property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CommentsProperty =
            RegisterModelProperty<String>(c=>c.Comments,Message.PlanogramAssortmentProduct_Comments);
        /// <summary>
        /// Comments
        /// </summary>
        public String Comments
        {
            get { return GetProperty<String>(CommentsProperty); }
            set { SetProperty<String>(CommentsProperty, value); }
        }
        #endregion

        #region ExactListFacings
        /// <summary>
        /// ExactListFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> ExactListFacingsProperty =
            RegisterModelProperty<Byte?>(c=>c.ExactListFacings, Message.PlanogramAssortmentProduct_ExactListFacings);
        /// <summary>
        /// ExactListFacings
        /// </summary>
        public Byte? ExactListFacings
        {
            get { return GetProperty<Byte?>(ExactListFacingsProperty); }
            set { SetProperty<Byte?>(ExactListFacingsProperty, value); }
        }
        #endregion

        #region ExactListUnits
        /// <summary>
        /// ExactListUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> ExactListUnitsProperty =
            RegisterModelProperty<Int16?>(c=>c.ExactListUnits, Message.PlanogramAssortmentProduct_ExactListUnits);
        /// <summary>
        /// ExactListUnits
        /// </summary>
        public Int16? ExactListUnits
        {
            get { return GetProperty<Int16?>(ExactListUnitsProperty); }
            set { SetProperty<Int16?>(ExactListUnitsProperty, value); }
        }
        #endregion

        #region PreserveListFacings
        /// <summary>
        /// PreserveListFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> PreserveListFacingsProperty =
            RegisterModelProperty<Byte?>(c=>c.PreserveListFacings,Message.PlanogramAssortmentProduct_PreserveListFacings);
        /// <summary>
        /// PreserveListFacings
        /// </summary>
        public Byte? PreserveListFacings
        {
            get { return GetProperty<Byte?>(PreserveListFacingsProperty); }
            set { SetProperty<Byte?>(PreserveListFacingsProperty, value); }
        }
        #endregion

        #region PreserveListUnits
        /// <summary>
        /// PreserveListUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> PreserveListUnitsProperty =
            RegisterModelProperty<Int16?>(c=>c.PreserveListUnits,Message.PlanogramAssortmentProduct_PreserveListUnits);
        /// <summary>
        /// PreserveListUnits
        /// </summary>
        public Int16? PreserveListUnits
        {
            get { return GetProperty<Int16?>(PreserveListUnitsProperty); }
            set { SetProperty<Int16?>(PreserveListUnitsProperty, value); }
        }
        #endregion

        #region MaxListFacings
        /// <summary>
        /// MaxListFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> MaxListFacingsProperty =
            RegisterModelProperty<Byte?>(c=>c.MaxListFacings,Message.PlanogramAssortmentProduct_MaxListFacings);
        /// <summary>
        /// MaxListFacings
        /// </summary>
        public Byte? MaxListFacings
        {
            get { return GetProperty<Byte?>(MaxListFacingsProperty); }
            set { SetProperty<Byte?>(MaxListFacingsProperty, value); }
        }
        #endregion

        #region MaxListUnits
        /// <summary>
        /// MaxListUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MaxListUnitsProperty =
            RegisterModelProperty<Int16?>(c=>c.MaxListUnits,Message.PlanogramAssortmentProduct_MaxListUnits);
        /// <summary>
        /// MaxListUnits
        /// </summary>
        public Int16? MaxListUnits
        {
            get { return GetProperty<Int16?>(MaxListUnitsProperty); }
            set { SetProperty<Int16?>(MaxListUnitsProperty, value); }
        }
        #endregion

        #region MinListFacings
        /// <summary>
        /// MinListFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> MinListFacingsProperty =
            RegisterModelProperty<Byte?>(c=>c.MinListFacings,Message.PlanogramAssortmentProduct_MinListFacings);
        /// <summary>
        /// MinListFacings
        /// </summary>
        public Byte? MinListFacings
        {
            get { return GetProperty<Byte?>(MinListFacingsProperty); }
            set { SetProperty<Byte?>(MinListFacingsProperty, value); }
        }
        #endregion

        #region MinListUnits
        /// <summary>
        /// MinListUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MinListUnitsProperty =
            RegisterModelProperty<Int16?>(c=>c.MinListUnits, Message.PlanogramAssortmentProduct_MinListUnits);
        /// <summary>
        /// MinListUnits
        /// </summary>
        public Int16? MinListUnits
        {
            get { return GetProperty<Int16?>(MinListUnitsProperty); }
            set { SetProperty<Int16?>(MinListUnitsProperty, value); }
        }
        #endregion

        #region FamilyRuleName
        /// <summary>
        /// FamilyRuleName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FamilyRuleNameProperty =
            RegisterModelProperty<String>(c=>c.FamilyRuleName,Message.PlanogramAssortmentProduct_FamilyRuleName);
        /// <summary>
        /// FamilyRuleName
        /// </summary>
        public String FamilyRuleName
        {
            get { return GetProperty<String>(FamilyRuleNameProperty); }
            set { SetProperty<String>(FamilyRuleNameProperty, value); }
        }
        #endregion

        #region FamilyRuleType
        /// <summary>
        /// FamilyRuleType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentProductFamilyRuleType> FamilyRuleTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductFamilyRuleType>(c=>c.FamilyRuleType,Message.PlanogramAssortmentProduct_FamilyRuleType);
        /// <summary>
        /// FamilyRuleType
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType FamilyRuleType
        {
            get { return GetProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty, value); }
        }
        #endregion

        #region FamilyRuleValue
        /// <summary>
        /// FamilyRuleValue property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> FamilyRuleValueProperty =
            RegisterModelProperty<Byte?>(c=>c.FamilyRuleValue, Message.PlanogramAssortmentProduct_FamilyRuleValue);
        /// <summary>
        /// FamilyRuleValue
        /// </summary>
        public Byte? FamilyRuleValue
        {
            get { return GetProperty<Byte?>(FamilyRuleValueProperty); }
            set { SetProperty<Byte?>(FamilyRuleValueProperty, value); }
        }
        #endregion

        #region IsPrimaryRegionalProduct
        /// <summary>
        /// IsPrimaryRegionalProduct property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPrimaryRegionalProductProperty =
            RegisterModelProperty<Boolean>(c=>c.IsPrimaryRegionalProduct, Message.PlanogramAssortmentProduct_IsPrimaryRegionalProduct);
        /// <summary>
        /// IsPrimaryRegionalProduct
        /// </summary>
        public Boolean IsPrimaryRegionalProduct
        {
            get { return GetProperty<Boolean>(IsPrimaryRegionalProductProperty); }
            set { SetProperty<Boolean>(IsPrimaryRegionalProductProperty, value); }
        }
        #endregion

        #region MetaIsProductPlacedOnPlanogram
        /// <summary>
        /// MetaIsProductPlacedOnPlanogram property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaIsProductPlacedOnPlanogramProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsProductPlacedOnPlanogram, Message.PlanogramAssortmentProduct_MetaIsProductPlacedOnPlanogram);
        /// <summary>
        /// MetaIsProductPlacedOnPlanogram
        /// </summary>
        public Boolean? MetaIsProductPlacedOnPlanogram
        {
            get { return GetProperty<Boolean?>(MetaIsProductPlacedOnPlanogramProperty); }
            set { SetProperty<Boolean?>(MetaIsProductPlacedOnPlanogramProperty, value); }
        }
        #endregion

        #region DelistedDueToAssortmentRule
        /// <summary>
        /// DelistedDueToAssortmentRule property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DelistedDueToAssortmentRuleProperty =
            RegisterModelProperty<Boolean>(c => c.DelistedDueToAssortmentRule, Message.PlanogramAssortmentProduct_DelistedDueToAssortmentRule);
        /// <summary>
        /// DelistedDueToAssortmentRule
        /// </summary>
        public Boolean DelistedDueToAssortmentRule
        {
            get { return GetProperty<Boolean>(DelistedDueToAssortmentRuleProperty); }
            set { SetProperty<Boolean>(DelistedDueToAssortmentRuleProperty, value); }
        }
        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns the rule max value that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public Int16? RuleMaximumValue
        {
            get
            {
                if (this.MaxListFacings != null) return (Int16)this.MaxListFacings;
                if (this.MaxListUnits != null) return (Int16)this.MaxListUnits;
                return null;
            }
        }

        /// <summary>
        /// Returns the rule value type (Units or Facings) that has been applied to the assortment products max list if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleValueType RuleMaximumValueType
        {
            get
            {
                if (this.MaxListFacings != null)
                {
                    return PlanogramAssortmentProductRuleValueType.Facings;
                }

                if (this.MaxListUnits != null)
                {
                    return PlanogramAssortmentProductRuleValueType.Units;
                }
                return PlanogramAssortmentProductRuleValueType.Units;
            }
        }

        /// <summary>
        /// Returns the rule type that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleType RuleType
        {
            get
            {
                if ((this.ExactListFacings != null) || (this.ExactListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.Force;
                }
                if ((this.PreserveListFacings != null) || (this.PreserveListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.Preserve;
                }
                if ((this.MinListFacings != null) || (this.MinListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.MinimumHurdle;
                }
                if (this.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Core)
                {
                    return PlanogramAssortmentProductRuleType.Core;
                }
                return PlanogramAssortmentProductRuleType.None;
            }
        }

        /// <summary>
        /// Returns the rule value that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public Int16? RuleValue
        {
            get
            {
                if (this.ExactListFacings != null) return (Int16)this.ExactListFacings;
                if (this.ExactListUnits != null) return (Int16)this.ExactListUnits;
                if (this.PreserveListFacings != null) return (Int16)this.PreserveListFacings;
                if (this.PreserveListUnits != null) return (Int16)this.PreserveListUnits;
                if (this.MinListFacings != null) return (Int16)this.MinListFacings;
                if (this.MinListUnits != null) return (Int16)this.MinListUnits;
                return null;
            }
        }

        /// <summary>
        /// Returns the rule value type (Units or Facings) that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleValueType RuleValueType
        {
            get
            {
                if ((this.ExactListFacings != null) || (this.PreserveListFacings != null) || (this.MinListFacings != null))
                {
                    return PlanogramAssortmentProductRuleValueType.Facings;
                }

                if ((this.ExactListUnits != null) || (this.PreserveListUnits != null) || (this.MinListUnits != null))
                {
                    return PlanogramAssortmentProductRuleValueType.Units;
                }
                return PlanogramAssortmentProductRuleValueType.Units;
            }
        }

        public Boolean HasMaximum
        {
            get { return ((this.MaxListUnits != null) || (this.MaxListFacings != null)); }
        }

        /// <summary>
        /// Friendly description of the product rules applied to this product
        /// </summary>
        public String RuleSummary
        {
            get
            {
                String value = PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[this.RuleType];

                if (this.RuleType == PlanogramAssortmentProductRuleType.Force && this.RuleValue == 0)
                {
                    value = Message.PlanogramAssortmentProduct_RuleSummary_Delisted;
                }
                else if (this.RuleType == PlanogramAssortmentProductRuleType.Core)
                {
                    value = PlanogramAssortmentProductTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductTreatmentType.Core];
                }
                else
                {
                    //Get basic rule as friendly text
                    if (this.RuleType != PlanogramAssortmentProductRuleType.None)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2}",
                            value,
                            this.RuleValue,
                            PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[this.RuleValueType]);
                    }

                    //add any maximums set
                    if (this.MaxListFacings != null)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2} {3}",
                            value,
                            Message.PlanogramAssortmentProduct_RuleSummary_Max,
                            this.MaxListFacings,
                            PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleValueType.Facings]);
                    }
                    else if (this.MaxListUnits != null)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2} {3}",
                           value,
                           Message.PlanogramAssortmentProduct_RuleSummary_Max,
                           this.MaxListUnits,
                           PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleValueType.Units]);
                    }
                }

                return value;
            }
        }
        
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(GtinProperty));
            BusinessRules.AddRule(new MaxLength(GtinProperty, 14));
            //BusinessRules.AddRule(new Required(LocationCodeProperty));
            //BusinessRules.AddRule(new MaxLength(LocationCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(NameProperty,100));
            BusinessRules.AddRule(new MaxLength(SegmentationProperty,300));
            BusinessRules.AddRule(new MaxLength(CommentsProperty,100));
            BusinessRules.AddRule(new MaxLength(FamilyRuleNameProperty,100));

            BusinessRules.AddRule(new Required(IsRangedProperty));

            //Facings and Units Rules
            BusinessRules.AddRule(new NullableMinValue<Byte>(PreserveListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Byte>(MinListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Byte>(MaxListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(PreserveListUnitsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(MinListUnitsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(MaxListUnitsProperty, 1));

            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentProduct NewPlanogramAssortmentProduct()
        {
            var item = new PlanogramAssortmentProduct();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new assortment product from the given product info.
        /// </summary>
        public static PlanogramAssortmentProduct NewPlanogramAssortmentProduct(IPlanogramProductInfo productInfo)
        {
            var item = new PlanogramAssortmentProduct();
            item.Create(productInfo.Gtin, productInfo.Name);
            return item;
        }

        /// <summary>
        /// Creates a new assortment product from the given gtin and name
        /// </summary>
        public static PlanogramAssortmentProduct NewPlanogramAssortmentProduct(String gtin, String name)
        {
            var item = new PlanogramAssortmentProduct();
            item.Create(gtin, name);
            return item;
        }

        /// <summary>
        /// Creates a new assortment product from the given existing assortment product
        /// </summary>
        public static PlanogramAssortmentProduct NewPlanogramAssortmentProduct(IPlanogramAssortmentProduct assortmentProduct)
        {
            var item = new PlanogramAssortmentProduct();
            item.Create(assortmentProduct);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String gtin, String name)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<String>(GtinProperty, gtin);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramAssortmentProduct assortmentProduct)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, assortmentProduct.Gtin);
            this.LoadProperty<String>(NameProperty, assortmentProduct.Name);
            this.LoadProperty<Boolean>(IsRangedProperty, assortmentProduct.IsRanged);
            this.LoadProperty<Int16>(RankProperty, assortmentProduct.Rank);
            this.LoadProperty<String>(SegmentationProperty, assortmentProduct.Segmentation);
            this.LoadProperty<Byte>(FacingsProperty, assortmentProduct.Facings);
            this.LoadProperty<Int16>(UnitsProperty, assortmentProduct.Units);
            this.LoadProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty, assortmentProduct.ProductTreatmentType);
            this.LoadProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty, assortmentProduct.ProductLocalizationType);
            this.LoadProperty<String>(CommentsProperty, assortmentProduct.Comments);
            this.LoadProperty<Byte?>(ExactListFacingsProperty, assortmentProduct.ExactListFacings);
            this.LoadProperty<Int16?>(ExactListUnitsProperty, assortmentProduct.ExactListUnits);
            this.LoadProperty<Byte?>(PreserveListFacingsProperty, assortmentProduct.PreserveListFacings);
            this.LoadProperty<Int16?>(PreserveListUnitsProperty, assortmentProduct.PreserveListUnits);
            this.LoadProperty<Byte?>(MaxListFacingsProperty, assortmentProduct.MaxListFacings);
            this.LoadProperty<Int16?>(MaxListUnitsProperty, assortmentProduct.MaxListUnits);
            this.LoadProperty<Byte?>(MinListFacingsProperty, assortmentProduct.MinListFacings);
            this.LoadProperty<Int16?>(MinListUnitsProperty, assortmentProduct.MinListUnits);
            this.LoadProperty<String>(FamilyRuleNameProperty, assortmentProduct.FamilyRuleName);
            this.LoadProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty, assortmentProduct.FamilyRuleType);
            this.LoadProperty<Byte?>(FamilyRuleValueProperty, assortmentProduct.FamilyRuleValue);
            this.LoadProperty<Boolean>(IsPrimaryRegionalProductProperty, assortmentProduct.IsPrimaryRegionalProduct);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        public void ClearRule()
        {
            this.ExactListFacings = null;
            this.ExactListUnits = null;
            this.PreserveListFacings = null;
            this.PreserveListUnits = null;
            this.MinListFacings = null;
            this.MinListUnits = null;
            this.MaxListFacings = null;
            this.MaxListUnits = null;
            this.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
        }

        internal static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePropertyInfos()
        {
            //yield return NameProperty; //don't include as this always attaches from planogram product
            //yield return GTINProperty; //don't include as this always attaches from planogram product
            yield return IsRangedProperty;
            yield return RankProperty;
            yield return SegmentationProperty;
            yield return FacingsProperty;
            yield return UnitsProperty;
            yield return ProductTreatmentTypeProperty;
            yield return ProductLocalizationTypeProperty;
            yield return CommentsProperty;
            yield return ExactListFacingsProperty;
            yield return ExactListUnitsProperty;
            yield return PreserveListFacingsProperty;
            yield return PreserveListUnitsProperty;
            yield return MaxListFacingsProperty;
            yield return MaxListUnitsProperty;
            yield return MinListFacingsProperty;
            yield return MinListUnitsProperty;
            yield return FamilyRuleNameProperty;
            yield return FamilyRuleTypeProperty;
            yield return FamilyRuleValueProperty;
            yield return MetaIsProductPlacedOnPlanogramProperty;
            yield return DelistedDueToAssortmentRuleProperty;
        }

        #region FieldInfos
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(PlanogramAssortmentProduct);
            String typeFriendly = PlanogramAssortmentProduct.FriendlyName;

            String assortmentProductGroup = "Assortment Product"; // Message.AssortmentProduct_AssortmentProduct_Details;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, GtinProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRangedProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RankProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SegmentationProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductTreatmentTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductLocalizationTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ExactListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ExactListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PreserveListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PreserveListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleNameProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleValueProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsPrimaryRegionalProductProperty, assortmentProductGroup);
        }
        #endregion

        protected override void OnCalculateMetadata()
        {
            PlanogramMetadataDetails metadataDetails = this.Parent.Parent.GetPlanogramMetadataDetails();
            PlanogramProduct product = metadataDetails.FindPlanogramProductByGtin(this.Gtin);

            this.MetaIsProductPlacedOnPlanogram = (product != null && metadataDetails.FindPositionsByPlanogramProductId(product.Id).Any());
        }

        protected override void OnClearMetadata()
        {
            this.MetaIsProductPlacedOnPlanogram = null;
        }

        /// <summary>
        /// Helper method to simply calculate if this product is available to place in the location specification
        /// </summary>
        /// <param name="locationCode"></param>
        /// <returns></returns>
        public Boolean IsAvailableToPlace(String locationCode)
        {
            return
                //Not a force delist
                   !(this.RuleType == PlanogramAssortmentProductRuleType.Force && this.RuleValue == 0)
                   &&
                //Not a local line product or is local to this location
                   (this.Parent.LocalProducts.FirstOrDefault(p => p.ProductGtin.Equals(this.Gtin)) == null /*Not a local line product*/
                   || (this.Parent.LocalProducts.Any(p => p.ProductGtin.Equals(this.Gtin)) /* If product has local lines, check if any match location code*/
                        && this.Parent.LocalProducts.Where(p => p.ProductGtin.Equals(this.Gtin)).Any(p => p.LocationCode.Equals(locationCode)))
                   || String.IsNullOrEmpty(locationCode)) /* Planogram is not assigned a location */
                   &&
                //Not a regional product
                   !this.IsPrimaryRegionalProduct;
        }

        /// <summary>
        /// Helper method to enforce any assortment rules against a proposed value for the assortment product
        /// </summary>
        /// <param name="proposedValue"></param>
        /// <returns></returns>
        public Int16? EnforceNonForceAssortmentRuleUnits(Int16? proposedValue)
        {
            Int16? value = proposedValue;

            //If any rule is setup that means atleast 1 unit must be placed, and 0 are
            if ((this.RuleType == PlanogramAssortmentProductRuleType.Core 
                || this.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Distribution
                || this.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Inheritance
                || this.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Core)
                && (!proposedValue.HasValue || (proposedValue.HasValue && proposedValue.Value <= 0)))
            {
                //Set minimum of 1 unit
                value = 1;
            }

            //Check any minimum product rules
            if ((this.RuleType == PlanogramAssortmentProductRuleType.MinimumHurdle
                || this.RuleType == PlanogramAssortmentProductRuleType.Preserve)
                && this.RuleValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                if (proposedValue.HasValue && proposedValue.Value <= this.RuleValue)
                {
                    value = this.RuleValue;
                }
            }

            //Check any maximum product rules
            if(this.HasMaximum && this.RuleMaximumValueType == PlanogramAssortmentProductRuleValueType.Units)
            {
                if (proposedValue.HasValue && proposedValue.Value > this.RuleMaximumValue)
                {
                    value = this.RuleMaximumValue;
                }
            }

            return value;
        }

        /// <summary>
        /// Gets the planogram product related to this assortment product.
        /// </summary>
        public PlanogramProduct GetPlanogramProduct()
        {
            PlanogramAssortment assortment = this.Parent;
            if (assortment == null) return null;

            Planogram plan = assortment.Parent;
            if (plan == null) return null;

            return plan.Products.FirstOrDefault(p => p.Gtin == this.Gtin);
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return String.Format("{0} {1}", this.Gtin, this.Name);
        }

        #endregion
    }
}
