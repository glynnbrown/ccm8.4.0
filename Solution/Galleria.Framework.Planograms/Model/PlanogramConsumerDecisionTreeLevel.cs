﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created
// V8-27132 : A.Kuszyk
//  Implemented common interface.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramConsumerDecisionTreeLevel Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeLevel : ModelObject<PlanogramConsumerDecisionTreeLevel>, IPlanogramConsumerDecisionTreeLevel
    {
        #region Parent

        /// <summary>
        /// Returns the parent level or null if this is the root
        /// </summary>
        public PlanogramConsumerDecisionTreeLevel ParentLevel
        {
            get
            {
                PlanogramConsumerDecisionTreeLevelList listParent = base.Parent as PlanogramConsumerDecisionTreeLevelList;
                if (listParent != null)
                {
                    return listParent.ParentLevel;
                }
                return null;
                //return base.Parent as ConsumerDecisionTreeLevel; 
            }
        }

        /// <summary>
        /// returns the parent planogram consumer decision tree
        /// </summary>
        public PlanogramConsumerDecisionTree ParentPlanogramConsumerDecisionTree
        {
            get
            {
                PlanogramConsumerDecisionTree parentCDT = base.Parent as PlanogramConsumerDecisionTree;
                if (parentCDT == null)
                {
                    PlanogramConsumerDecisionTreeLevel currentParent = this.ParentLevel;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentLevel != null)
                        {
                            currentParent = currentParent.ParentLevel;
                        }
                        else
                        {
                            return currentParent.Parent as PlanogramConsumerDecisionTree;
                        }
                    }
                }
                else
                {
                    return parentCDT;
                }

                return null;
                //if (this.ParentLevel != null)
                //{
                //    return this.ParentLevel.ParentConsumerDecisionTree;
                //}
                //else
                //{
                //    return base.Parent as ConsumerDecisionTree;
                //}
            }
        }

        #endregion

        #region Constants
        private const Int16 _maxNamePropertyLenght = 100;
        #endregion

        #region Properties

        #region Parent Property
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramConsumerDecisionTree Parent
        {
            get { return (PlanogramConsumerDecisionTree)base.Parent; }
        }
        #endregion

        #region Name

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Parent Level

        public static readonly ModelPropertyInfo<Object> ParentLevelIdProperty =
        RegisterModelProperty<Object>(c => c.ParentLevelId);
        /// <summary>
        /// The ParentNodeId of the level this belongs to
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object ParentLevelId
        {
            get { return GetProperty<Object>(ParentLevelIdProperty); }
            set { SetProperty<Object>(ParentLevelIdProperty, value); }
        }

        #endregion

        #region Child Level

        private static readonly ModelPropertyInfo<PlanogramConsumerDecisionTreeLevelList> ChildLevelListProperty =
            RegisterModelProperty<PlanogramConsumerDecisionTreeLevelList>(c => c.ChildLevelList);
        /// <summary>
        /// The Child level list: see GFS-13403
        /// </summary>
        private PlanogramConsumerDecisionTreeLevelList ChildLevelList
        {
            get
            {
                PlanogramConsumerDecisionTreeLevelList returnList = GetProperty<PlanogramConsumerDecisionTreeLevelList>(ChildLevelListProperty);
                if (returnList == null)
                {
                    returnList = PlanogramConsumerDecisionTreeLevelList.NewPlanogramConsumerDecisionTreeLevelList();
                    LoadProperty<PlanogramConsumerDecisionTreeLevelList>(ChildLevelListProperty, returnList);
                }
                return returnList;
            }
        }
        public const String ChildLevelProperty = "ChildLevel";
        /// <summary>
        /// The child level of this, may be null
        /// </summary>
        public PlanogramConsumerDecisionTreeLevel ChildLevel
        {
            get { return this.ChildLevelList.FirstOrDefault(); }
            set
            {
                //remove the existing level
                PlanogramConsumerDecisionTreeLevel existingLevel = this.ChildLevelList.FirstOrDefault();
                if (existingLevel != null)
                {
                    this.ChildLevelList.Remove(existingLevel);
                }

                if (value != null)
                {
                    //add the new level
                    this.ChildLevelList.Add(value);
                }

                //fire the on property changed
                OnPropertyChanged(ChildLevelProperty);
            }
        }


        #endregion

        #region IsRoot

        /// <summary>
        /// Returns true if this is the root level
        /// </summary>
        public Boolean IsRoot
        {
            get { return this.ParentLevel == null; }
        }

        #endregion

        #region Level Number

        /// <summary>
        /// Returns the number of this level, assuming that levels are numbered from top (root) to bottom.
        /// </summary>
        public Int32 LevelNumber
        {
            get
            {
                if (IsRoot)
                {
                    return 1;
                }
                return 1 + ParentLevel.LevelNumber;
            }
        }
        #endregion

        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, _maxNamePropertyLenght));

        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramConsumerDecisionTreeLevel NewPlanogramConsumerDecisionTreeLevel()
        {
            PlanogramConsumerDecisionTreeLevel item = new PlanogramConsumerDecisionTreeLevel();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramConsumerDecisionTreeLevel NewPlanogramConsumerDecisionTreeLevel(String name)
        {
            PlanogramConsumerDecisionTreeLevel item = new PlanogramConsumerDecisionTreeLevel();
            item.Create(name);
            return item;
        }

        /// <summary>
        /// Creates a new level, copying all values from the level provided
        /// </summary>
        /// <returns>A new level</returns>
        public static PlanogramConsumerDecisionTreeLevel NewPlanogramConsumerDecisionTreeLevel(IPlanogramConsumerDecisionTreeLevel level)
        {
            if (level == null) throw new ArgumentNullException("level cannot be null");
            PlanogramConsumerDecisionTreeLevel item = new PlanogramConsumerDecisionTreeLevel();
            item.Create(level);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(IPlanogramConsumerDecisionTreeLevel level)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(NameProperty,level.Name);
            if (level.ChildLevel != null)
            {
                ChildLevelList.Add(PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel(level.ChildLevel));
            }
            MarkAsChild();
            this.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(String name)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(NameProperty, name);

            MarkAsChild();
            this.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            //give this a temp id
            LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            var oldId = this.Id;
            var newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramConsumerDecisionTreeLevel>(oldId, newId);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Replaces this level with that given.
        /// </summary>
        /// <param name="replacementLevel">The level to replace with.</param>
        public void ReplaceConsumerDecisionTreeLevel(IPlanogramConsumerDecisionTreeLevel replacementLevel)
        {
            ChildLevelList.Clear();
            if (replacementLevel.ChildLevel != null)
            {
                ChildLevelList.Add(NewPlanogramConsumerDecisionTreeLevel(replacementLevel.ChildLevel));
            }
            Name = replacementLevel.Name;
        }

        /// <summary>
        /// Creates a dictionary of level id links, will create levels if required
        /// </summary>
        internal Dictionary<Int32, Int32> MergeFrom(PlanogramConsumerDecisionTreeLevel sourceLevel)
        {
            Dictionary<Int32, Int32> matchedLevelIds = new Dictionary<Int32, Int32>();

            //Add the level ids to the dictionary
            matchedLevelIds.Add((Int32)sourceLevel.Id, (Int32)this.Id);
            String testChildLevelName = String.Empty;

            if (sourceLevel.ChildLevel != null)
            {
                PlanogramConsumerDecisionTreeLevel sourceChildLevel = sourceLevel.ChildLevel;
                if (this.ChildLevel == null)
                {
                    this.ChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
                    testChildLevelName = String.Format("{0} - {1}", sourceLevel.ParentPlanogramConsumerDecisionTree.Name, sourceChildLevel.Name);
                }
                else
                {
                    //if exists, and name already been changed to mixed level, use the name
                    if (this.ChildLevel.Name == Message.PlanogramConsumerDecisionTree_ChildNameTooLong_ReplacementName)
                    {
                        testChildLevelName = this.ChildLevel.Name;
                    }
                    else
                    {
                        testChildLevelName = String.Format("{0}\n{1} - {2}", this.ChildLevel.Name, sourceLevel.ParentPlanogramConsumerDecisionTree.Name, sourceChildLevel.Name);
                    }
                }

                //validate child level name lenght
                if (testChildLevelName.Length > _maxNamePropertyLenght)
                {
                    testChildLevelName = Message.PlanogramConsumerDecisionTree_ChildNameTooLong_ReplacementName;
                }
                this.ChildLevel.Name = testChildLevelName;
                matchedLevelIds = matchedLevelIds.Union(this.ChildLevel.MergeFrom(sourceChildLevel)).ToDictionary(p => p.Key, p => p.Value);
            }
            return matchedLevelIds;
        }

        public void RemoveLevel(PlanogramConsumerDecisionTreeLevel level)
        {
            ChildLevelList.Remove(level);
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeLevel members
        IPlanogramConsumerDecisionTreeLevel IPlanogramConsumerDecisionTreeLevel.ChildLevel
        {
            get { return ChildLevel as IPlanogramConsumerDecisionTreeLevel; }
        }
        #endregion
    }
}
