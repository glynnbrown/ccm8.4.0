﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27919 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A simple model used to represent a mapping between 2 fields within a planogram.
    /// </summary>
    [Serializable]
    public sealed class PlanogramFieldMapping : ModelReadOnlyObject<PlanogramFieldMapping>
    {
        #region Properties

        #region Type
        /// <summary>
        /// Type property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFieldMappingType> TypeProperty =
            RegisterModelProperty<PlanogramFieldMappingType>(c => c.Type);
        /// <summary>
        /// Gets/Sets the mapping type.
        /// </summary>
        public PlanogramFieldMappingType Type
        {
            get { return this.GetProperty<PlanogramFieldMappingType>(TypeProperty); }
        }
        #endregion

        #region Target
        /// <summary>
        /// Target property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TargetProperty =
            RegisterModelProperty<String>(c => c.Target);
        /// <summary>
        /// Gets or sets the target field
        /// </summary>
        public String Target
        {
            get { return this.GetProperty<String>(TargetProperty); }
        }
        #endregion

        #region Source
        /// <summary>
        /// Source property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceProperty =
            RegisterModelProperty<String>(c => c.Source);
        /// <summary>
        /// Gets or sets the source field
        /// </summary>
        public String Source
        {
            get { return this.GetProperty<String>(SourceProperty); }
        }
        #endregion

        #endregion

        #region Constructor
        private PlanogramFieldMapping() { } //force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramFieldMapping NewPlanogramFieldMapping(PlanogramFieldMappingType type, String target, String source)
        {
            PlanogramFieldMapping item = new PlanogramFieldMapping();
            item.Create(type, target, source);
            return item;
        }
        #endregion

        #region Data Access

        #region Data Transfer object
        public PlanogramFieldMappingDto GetDataTransferObject()
        {
            return new PlanogramFieldMappingDto()
            {
                Type = (Byte)ReadProperty<PlanogramFieldMappingType>(TypeProperty),
                Target = ReadProperty<String>(TargetProperty),
                Source = ReadProperty<String>(SourceProperty),
            };
        }
        #endregion

        #region Create
        /// <summary>
        /// Called when creating a new item.
        /// </summary>
        private void Create(PlanogramFieldMappingType type, String target, String source)
        {
            this.LoadProperty<PlanogramFieldMappingType>(TypeProperty, type);
            this.LoadProperty<String>(TargetProperty, target);
            this.LoadProperty<String>(SourceProperty, source);
        }
        #endregion

        #endregion
    }

    /// <summary>
    /// Denotes the available field mapping types.
    /// </summary>
    public enum PlanogramFieldMappingType
    {
        Planogram = 0,
        Fixture = 1,
        Component = 2,
        Product = 3,
        Performance = 4
    }
}
