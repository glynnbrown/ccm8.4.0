﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 802
// V8-28995 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Core;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A rule to ensure that there are no duplicate placement types in the same axis in the Primary,
    /// Secondary and Tertiary Placement type properties.
    /// </summary>
    public class PlanogramBlockingGroupPlacementTypeRule : Csla.Rules.BusinessRule
    {
        /// <summary>
        /// Instantiates a rule from the given three placement type properties.
        /// </summary>
        /// <param name="primaryProperty">The placement type being evaluated.</param>
        /// <param name="secondaryProperty">One of the other placement type properties.</param>
        /// <param name="tertiaryProperty">One of the other placement type properties.</param>
        public PlanogramBlockingGroupPlacementTypeRule(IPropertyInfo primaryProperty, IPropertyInfo secondaryProperty, IPropertyInfo tertiaryProperty)
            : base(primaryProperty)
        {
            InputProperties = new List<IPropertyInfo>()
                {
                    primaryProperty, secondaryProperty, tertiaryProperty
                };
        }

        protected override void Execute(Csla.Rules.RuleContext context)
        {
            // Get the Object values of all the properties.
            Object primaryValueObject = context.InputPropertyValues[InputProperties[0]];
            Object secondaryValueObject = context.InputPropertyValues[InputProperties[1]];
            Object tertiaryValueObject = context.InputPropertyValues[InputProperties[2]];

            // Check that the property values are of the right type.
            if (!(primaryValueObject is PlanogramBlockingGroupPlacementType) ||
                !(secondaryValueObject is PlanogramBlockingGroupPlacementType) ||
                !(tertiaryValueObject is PlanogramBlockingGroupPlacementType))
            {
                context.AddErrorResult(Message.PlanogramBlockingGroupPlacementTypeRule_InvalidFormat);
                return;
            }

            // Cast the property values into an enum.
            PlanogramBlockingGroupPlacementType primaryValue = (PlanogramBlockingGroupPlacementType)primaryValueObject;
            PlanogramBlockingGroupPlacementType secondaryValue = (PlanogramBlockingGroupPlacementType)secondaryValueObject;
            PlanogramBlockingGroupPlacementType tertiaryValue = (PlanogramBlockingGroupPlacementType)tertiaryValueObject;

            // Check the value for duplicates.
            if (primaryValue.IsX() && (secondaryValue.IsX() || tertiaryValue.IsX()))
            {
                context.AddErrorResult(
                    String.Format(Message.PlanogramBlockingGroupPlacementTypeRule_Error, 
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.LeftToRight],
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.RightToLeft]));
                return;
            }
            else if (primaryValue.IsY() && (secondaryValue.IsY() || tertiaryValue.IsY()))
            {
                context.AddErrorResult(
                    String.Format(Message.PlanogramBlockingGroupPlacementTypeRule_Error, 
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.BottomToTop],
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.TopToBottom]));
                return;
            }
            else if (primaryValue.IsZ() && (secondaryValue.IsZ() || tertiaryValue.IsZ()))
            {
                context.AddErrorResult(
                    String.Format(Message.PlanogramBlockingGroupPlacementTypeRule_Error,
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.BackToFront],
                    PlanogramBlockingGroupPlacementTypeHelper.FriendlyNames[PlanogramBlockingGroupPlacementType.FrontToBack]));
                return;
            }

            context.AddSuccessResult(stopProcessing: false);
        }
    }
}
