﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added helper class.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn
//  Added delist type
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductFamilyRuleType
    {
        None = 0,
        MinimumProductCount = 1,
        MaximumProductCount = 2,
        DependencyList = 3,
        Delist = 4
    }

    public static class PlanogramAssortmentProductFamilyRuleTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductFamilyRuleType, String> FriendlyNames =
            new Dictionary<PlanogramAssortmentProductFamilyRuleType, String>()
            {
                {PlanogramAssortmentProductFamilyRuleType.None, Message.Enum_Generic_None},
                {PlanogramAssortmentProductFamilyRuleType.MaximumProductCount, Message.Enum_PlanogramAssortmentProductFamilyRuleType_MaximumProductCount}, 
                {PlanogramAssortmentProductFamilyRuleType.MinimumProductCount, Message.Enum_PlanogramAssortmentProductFamilyRuleType_MinimumProductCount}, 
                {PlanogramAssortmentProductFamilyRuleType.DependencyList, Message.Enum_PlanogramAssortmentProductFamilyRuleType_DependencyList}, 
                {PlanogramAssortmentProductFamilyRuleType.Delist, Message.Enum_PlanogramAssortmentProductFamilyRuleType_Delist}
            };
    }
}
