﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes alignment options for a <see cref="PlanogramSequenceGroupSubGroup"/>.
    /// </summary>
    public enum PlanogramSequenceGroupSubGroupAlignmentType
    {
        Bottom = 0,
        Middle = 1,
        Top = 2,
    }
}
