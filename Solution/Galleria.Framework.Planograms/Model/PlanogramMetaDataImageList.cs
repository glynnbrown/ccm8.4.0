﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27058 : A.Probyn
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class PlanogramMetadataImageList : ModelList<PlanogramMetadataImageList, PlanogramMetadataImage>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramMetadataImageList NewPlanogramMetaDataImageList()
        {
            PlanogramMetadataImageList item = new PlanogramMetadataImageList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
