﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeNodeProductList
    {
        #region Constructor
        private PlanogramConsumerDecisionTreeNodeProductList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static PlanogramConsumerDecisionTreeNodeProductList FetchListByPlanogramConsumerDecisionTreeNodeId(
            IDalContext dalContext, Object parentNodeId)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTreeNodeProductList>(dalContext, parentNodeId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Object parentId)
        {
            RaiseListChangedEvents = false;

            using (IPlanogramConsumerDecisionTreeNodeProductDal dal =
               dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
            {
                IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> dtoList =
                    dal.FetchByPlanogramConsumerDecisionTreeNodeId(parentId);

                foreach (PlanogramConsumerDecisionTreeNodeProductDto childDto in dtoList)
                {
                    this.Add(PlanogramConsumerDecisionTreeNodeProduct.FetchPlanogramConsumerDecisionTreeNodeProduct(dalContext, childDto));
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
