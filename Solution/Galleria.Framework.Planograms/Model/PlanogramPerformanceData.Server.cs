﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-28811 : L.Luong
//  Added MetaP1Percentage to MetaP20Percentage
#endregion
#region Version History: CCM811
// V8-30992 : A.Probyn
//  Added MinimumInventoryUnits
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramPerformanceData
    {
        #region Constructor
        private PlanogramPerformanceData() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramPerformanceData Fetch(IDalContext dalContext, PlanogramPerformanceDataDto dto)
        {
            return DataPortal.FetchChild<PlanogramPerformanceData>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramPerformanceDataDto dto)
        {
            this.LoadProperty<Object>(IdProperty,dto.Id);
            this.LoadProperty<Object>(PlanogramProductIdProperty, dto.PlanogramProductId);
            this.LoadProperty<Single?>(P1Property,dto.P1);
            this.LoadProperty<Single?>(P2Property,dto.P2);
            this.LoadProperty<Single?>(P3Property,dto.P3);
            this.LoadProperty<Single?>(P4Property,dto.P4);
            this.LoadProperty<Single?>(P5Property,dto.P5);
            this.LoadProperty<Single?>(P6Property,dto.P6);
            this.LoadProperty<Single?>(P7Property,dto.P7);
            this.LoadProperty<Single?>(P8Property,dto.P8);
            this.LoadProperty<Single?>(P9Property,dto.P9);
            this.LoadProperty<Single?>(P10Property,dto.P10);
            this.LoadProperty<Single?>(P11Property,dto.P11);
            this.LoadProperty<Single?>(P12Property,dto.P12);
            this.LoadProperty<Single?>(P13Property,dto.P13);
            this.LoadProperty<Single?>(P14Property,dto.P14);
            this.LoadProperty<Single?>(P15Property,dto.P15);
            this.LoadProperty<Single?>(P16Property,dto.P16);
            this.LoadProperty<Single?>(P17Property,dto.P17);
            this.LoadProperty<Single?>(P18Property,dto.P18);
            this.LoadProperty<Single?>(P19Property,dto.P19);
            this.LoadProperty<Single?>(P20Property,dto.P20);
            this.LoadProperty<Single?>(CP1Property, dto.CP1);
            this.LoadProperty<Single?>(CP2Property, dto.CP2);
            this.LoadProperty<Single?>(CP3Property, dto.CP3);
            this.LoadProperty<Single?>(CP4Property, dto.CP4);
            this.LoadProperty<Single?>(CP5Property, dto.CP5);
            this.LoadProperty<Single?>(CP6Property, dto.CP6);
            this.LoadProperty<Single?>(CP7Property, dto.CP7);
            this.LoadProperty<Single?>(CP8Property, dto.CP8);
            this.LoadProperty<Single?>(CP9Property, dto.CP9);
            this.LoadProperty<Single?>(CP10Property, dto.CP10);
            this.LoadProperty<Single?>(UnitsSoldPerDayProperty, dto.UnitsSoldPerDay);
            this.LoadProperty<Single?>(AchievedCasePacksProperty, dto.AchievedCasePacks);
            this.LoadProperty<Single?>(AchievedDosProperty, dto.AchievedDos);
            this.LoadProperty<Single?>(AchievedShelfLifeProperty, dto.AchievedShelfLife);
            this.LoadProperty<Single?>(AchievedDeliveriesProperty, dto.AchievedDeliveries);
            this.LoadProperty<Single?>(MinimumInventoryUnitsProperty, dto.MinimumInventoryUnits);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Single?>(MetaP1PercentageProperty, dto.MetaP1Percentage);
            this.LoadProperty<Single?>(MetaP2PercentageProperty, dto.MetaP2Percentage);
            this.LoadProperty<Single?>(MetaP3PercentageProperty, dto.MetaP3Percentage);
            this.LoadProperty<Single?>(MetaP4PercentageProperty, dto.MetaP4Percentage);
            this.LoadProperty<Single?>(MetaP5PercentageProperty, dto.MetaP5Percentage);
            this.LoadProperty<Single?>(MetaP6PercentageProperty, dto.MetaP6Percentage);
            this.LoadProperty<Single?>(MetaP7PercentageProperty, dto.MetaP7Percentage);
            this.LoadProperty<Single?>(MetaP8PercentageProperty, dto.MetaP8Percentage);
            this.LoadProperty<Single?>(MetaP9PercentageProperty, dto.MetaP9Percentage);
            this.LoadProperty<Single?>(MetaP10PercentageProperty, dto.MetaP10Percentage);
            this.LoadProperty<Single?>(MetaP11PercentageProperty, dto.MetaP11Percentage);
            this.LoadProperty<Single?>(MetaP12PercentageProperty, dto.MetaP12Percentage);
            this.LoadProperty<Single?>(MetaP13PercentageProperty, dto.MetaP13Percentage);
            this.LoadProperty<Single?>(MetaP14PercentageProperty, dto.MetaP14Percentage);
            this.LoadProperty<Single?>(MetaP15PercentageProperty, dto.MetaP15Percentage);
            this.LoadProperty<Single?>(MetaP16PercentageProperty, dto.MetaP16Percentage);
            this.LoadProperty<Single?>(MetaP17PercentageProperty, dto.MetaP17Percentage);
            this.LoadProperty<Single?>(MetaP18PercentageProperty, dto.MetaP18Percentage);
            this.LoadProperty<Single?>(MetaP19PercentageProperty, dto.MetaP19Percentage);
            this.LoadProperty<Single?>(MetaP20PercentageProperty, dto.MetaP20Percentage);
            this.LoadProperty<Single?>(MetaCP1PercentageProperty, dto.MetaCP1Percentage);
            this.LoadProperty<Single?>(MetaCP2PercentageProperty, dto.MetaCP2Percentage);
            this.LoadProperty<Single?>(MetaCP3PercentageProperty, dto.MetaCP3Percentage);
            this.LoadProperty<Single?>(MetaCP4PercentageProperty, dto.MetaCP4Percentage);
            this.LoadProperty<Single?>(MetaCP5PercentageProperty, dto.MetaCP5Percentage);
            this.LoadProperty<Single?>(MetaCP6PercentageProperty, dto.MetaCP6Percentage);
            this.LoadProperty<Single?>(MetaCP7PercentageProperty, dto.MetaCP7Percentage);
            this.LoadProperty<Single?>(MetaCP8PercentageProperty, dto.MetaCP8Percentage);
            this.LoadProperty<Single?>(MetaCP9PercentageProperty, dto.MetaCP9Percentage);
            this.LoadProperty<Single?>(MetaCP10PercentageProperty, dto.MetaCP10Percentage);
            this.LoadProperty<Int32?>(MetaP1RankProperty, dto.MetaP1Rank);
            this.LoadProperty<Int32?>(MetaP2RankProperty, dto.MetaP2Rank);
            this.LoadProperty<Int32?>(MetaP3RankProperty, dto.MetaP3Rank);
            this.LoadProperty<Int32?>(MetaP4RankProperty, dto.MetaP4Rank);
            this.LoadProperty<Int32?>(MetaP5RankProperty, dto.MetaP5Rank);
            this.LoadProperty<Int32?>(MetaP6RankProperty, dto.MetaP6Rank);
            this.LoadProperty<Int32?>(MetaP7RankProperty, dto.MetaP7Rank);
            this.LoadProperty<Int32?>(MetaP8RankProperty, dto.MetaP8Rank);
            this.LoadProperty<Int32?>(MetaP9RankProperty, dto.MetaP9Rank);
            this.LoadProperty<Int32?>(MetaP10RankProperty, dto.MetaP10Rank);
            this.LoadProperty<Int32?>(MetaP11RankProperty, dto.MetaP11Rank);
            this.LoadProperty<Int32?>(MetaP12RankProperty, dto.MetaP12Rank);
            this.LoadProperty<Int32?>(MetaP13RankProperty, dto.MetaP13Rank);
            this.LoadProperty<Int32?>(MetaP14RankProperty, dto.MetaP14Rank);
            this.LoadProperty<Int32?>(MetaP15RankProperty, dto.MetaP15Rank);
            this.LoadProperty<Int32?>(MetaP16RankProperty, dto.MetaP16Rank);
            this.LoadProperty<Int32?>(MetaP17RankProperty, dto.MetaP17Rank);
            this.LoadProperty<Int32?>(MetaP18RankProperty, dto.MetaP18Rank);
            this.LoadProperty<Int32?>(MetaP19RankProperty, dto.MetaP19Rank);
            this.LoadProperty<Int32?>(MetaP20RankProperty, dto.MetaP20Rank);
            this.LoadProperty<Int32?>(MetaCP1RankProperty, dto.MetaCP1Rank);
            this.LoadProperty<Int32?>(MetaCP2RankProperty, dto.MetaCP2Rank);
            this.LoadProperty<Int32?>(MetaCP3RankProperty, dto.MetaCP3Rank);
            this.LoadProperty<Int32?>(MetaCP4RankProperty, dto.MetaCP4Rank);
            this.LoadProperty<Int32?>(MetaCP5RankProperty, dto.MetaCP5Rank);
            this.LoadProperty<Int32?>(MetaCP6RankProperty, dto.MetaCP6Rank);
            this.LoadProperty<Int32?>(MetaCP7RankProperty, dto.MetaCP7Rank);
            this.LoadProperty<Int32?>(MetaCP8RankProperty, dto.MetaCP8Rank);
            this.LoadProperty<Int32?>(MetaCP9RankProperty, dto.MetaCP9Rank);
            this.LoadProperty<Int32?>(MetaCP10RankProperty, dto.MetaCP10Rank);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramPerformanceDataDto GetDataTransferObject(PlanogramPerformance parent)
        {
            return new PlanogramPerformanceDataDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramPerformanceId = parent.Id,
                PlanogramProductId = this.ReadProperty<Object>(PlanogramProductIdProperty),
                P1 = this.ReadProperty<Single?>(P1Property),
                P2 = this.ReadProperty<Single?>(P2Property),
                P3 = this.ReadProperty<Single?>(P3Property),
                P4 = this.ReadProperty<Single?>(P4Property),
                P5 = this.ReadProperty<Single?>(P5Property),
                P6 = this.ReadProperty<Single?>(P6Property),
                P7 = this.ReadProperty<Single?>(P7Property),
                P8 = this.ReadProperty<Single?>(P8Property),
                P9 = this.ReadProperty<Single?>(P9Property),
                P10 = this.ReadProperty<Single?>(P10Property),
                P11 = this.ReadProperty<Single?>(P11Property),
                P12 = this.ReadProperty<Single?>(P12Property),
                P13 = this.ReadProperty<Single?>(P13Property),
                P14 = this.ReadProperty<Single?>(P14Property),
                P15 = this.ReadProperty<Single?>(P15Property),
                P16 = this.ReadProperty<Single?>(P16Property),
                P17 = this.ReadProperty<Single?>(P17Property),
                P18 = this.ReadProperty<Single?>(P18Property),
                P19 = this.ReadProperty<Single?>(P19Property),
                P20 = this.ReadProperty<Single?>(P20Property),
                CP1 = this.ReadProperty<Single?>(CP1Property),
                CP2 = this.ReadProperty<Single?>(CP2Property),
                CP3 = this.ReadProperty<Single?>(CP3Property),
                CP4 = this.ReadProperty<Single?>(CP4Property),
                CP5 = this.ReadProperty<Single?>(CP5Property),
                CP6 = this.ReadProperty<Single?>(CP6Property),
                CP7 = this.ReadProperty<Single?>(CP7Property),
                CP8 = this.ReadProperty<Single?>(CP8Property),
                CP9 = this.ReadProperty<Single?>(CP9Property),
                CP10 = this.ReadProperty<Single?>(CP10Property),
                UnitsSoldPerDay = this.ReadProperty<Single?>(UnitsSoldPerDayProperty),
                AchievedCasePacks = this.ReadProperty<Single?>(AchievedCasePacksProperty),
                AchievedDos = this.ReadProperty<Single?>(AchievedDosProperty),
                AchievedShelfLife = this.ReadProperty<Single?>(AchievedShelfLifeProperty),
                AchievedDeliveries = this.ReadProperty<Single?>(AchievedDeliveriesProperty),
                MinimumInventoryUnits = this.ReadProperty<Single?>(MinimumInventoryUnitsProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                MetaP1Percentage = this.ReadProperty<Single?>(MetaP1PercentageProperty),
                MetaP2Percentage = this.ReadProperty<Single?>(MetaP2PercentageProperty),
                MetaP3Percentage = this.ReadProperty<Single?>(MetaP3PercentageProperty),
                MetaP4Percentage = this.ReadProperty<Single?>(MetaP4PercentageProperty),
                MetaP5Percentage = this.ReadProperty<Single?>(MetaP5PercentageProperty),
                MetaP6Percentage = this.ReadProperty<Single?>(MetaP6PercentageProperty),
                MetaP7Percentage = this.ReadProperty<Single?>(MetaP7PercentageProperty),
                MetaP8Percentage = this.ReadProperty<Single?>(MetaP8PercentageProperty),
                MetaP9Percentage = this.ReadProperty<Single?>(MetaP9PercentageProperty),
                MetaP10Percentage = this.ReadProperty<Single?>(MetaP10PercentageProperty),
                MetaP11Percentage = this.ReadProperty<Single?>(MetaP11PercentageProperty),
                MetaP12Percentage = this.ReadProperty<Single?>(MetaP12PercentageProperty),
                MetaP13Percentage = this.ReadProperty<Single?>(MetaP13PercentageProperty),
                MetaP14Percentage = this.ReadProperty<Single?>(MetaP14PercentageProperty),
                MetaP15Percentage = this.ReadProperty<Single?>(MetaP15PercentageProperty),
                MetaP16Percentage = this.ReadProperty<Single?>(MetaP16PercentageProperty),
                MetaP17Percentage = this.ReadProperty<Single?>(MetaP17PercentageProperty),
                MetaP18Percentage = this.ReadProperty<Single?>(MetaP18PercentageProperty),
                MetaP19Percentage = this.ReadProperty<Single?>(MetaP19PercentageProperty),
                MetaP20Percentage = this.ReadProperty<Single?>(MetaP20PercentageProperty),
                MetaCP1Percentage = this.ReadProperty<Single?>(MetaCP1PercentageProperty),
                MetaCP2Percentage = this.ReadProperty<Single?>(MetaCP2PercentageProperty),
                MetaCP3Percentage = this.ReadProperty<Single?>(MetaCP3PercentageProperty),
                MetaCP4Percentage = this.ReadProperty<Single?>(MetaCP4PercentageProperty),
                MetaCP5Percentage = this.ReadProperty<Single?>(MetaCP5PercentageProperty),
                MetaCP6Percentage = this.ReadProperty<Single?>(MetaCP6PercentageProperty),
                MetaCP7Percentage = this.ReadProperty<Single?>(MetaCP7PercentageProperty),
                MetaCP8Percentage = this.ReadProperty<Single?>(MetaCP8PercentageProperty),
                MetaCP9Percentage = this.ReadProperty<Single?>(MetaCP9PercentageProperty),
                MetaCP10Percentage = this.ReadProperty<Single?>(MetaCP10PercentageProperty),
                MetaP1Rank = this.ReadProperty<Int32?>(MetaP1RankProperty),
                MetaP2Rank = this.ReadProperty<Int32?>(MetaP2RankProperty),
                MetaP3Rank = this.ReadProperty<Int32?>(MetaP3RankProperty),
                MetaP4Rank = this.ReadProperty<Int32?>(MetaP4RankProperty),
                MetaP5Rank = this.ReadProperty<Int32?>(MetaP5RankProperty),
                MetaP6Rank = this.ReadProperty<Int32?>(MetaP6RankProperty),
                MetaP7Rank = this.ReadProperty<Int32?>(MetaP7RankProperty),
                MetaP8Rank = this.ReadProperty<Int32?>(MetaP8RankProperty),
                MetaP9Rank = this.ReadProperty<Int32?>(MetaP9RankProperty),
                MetaP10Rank = this.ReadProperty<Int32?>(MetaP10RankProperty),
                MetaP11Rank = this.ReadProperty<Int32?>(MetaP11RankProperty),
                MetaP12Rank = this.ReadProperty<Int32?>(MetaP12RankProperty),
                MetaP13Rank = this.ReadProperty<Int32?>(MetaP13RankProperty),
                MetaP14Rank = this.ReadProperty<Int32?>(MetaP14RankProperty),
                MetaP15Rank = this.ReadProperty<Int32?>(MetaP15RankProperty),
                MetaP16Rank = this.ReadProperty<Int32?>(MetaP16RankProperty),
                MetaP17Rank = this.ReadProperty<Int32?>(MetaP17RankProperty),
                MetaP18Rank = this.ReadProperty<Int32?>(MetaP18RankProperty),
                MetaP19Rank = this.ReadProperty<Int32?>(MetaP19RankProperty),
                MetaP20Rank = this.ReadProperty<Int32?>(MetaP20RankProperty),
                MetaCP1Rank = this.ReadProperty<Int32?>(MetaCP1RankProperty),
                MetaCP2Rank = this.ReadProperty<Int32?>(MetaCP2RankProperty),
                MetaCP3Rank = this.ReadProperty<Int32?>(MetaCP3RankProperty),
                MetaCP4Rank = this.ReadProperty<Int32?>(MetaCP4RankProperty),
                MetaCP5Rank = this.ReadProperty<Int32?>(MetaCP5RankProperty),
                MetaCP6Rank = this.ReadProperty<Int32?>(MetaCP6RankProperty),
                MetaCP7Rank = this.ReadProperty<Int32?>(MetaCP7RankProperty),
                MetaCP8Rank = this.ReadProperty<Int32?>(MetaCP8RankProperty),
                MetaCP9Rank = this.ReadProperty<Int32?>(MetaCP9RankProperty),
                MetaCP10Rank = this.ReadProperty<Int32?>(MetaCP10RankProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramPerformanceDataDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramPerformanceDataDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramPerformanceDataDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramPerformanceData>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramPerformanceDataDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            batchContext.Delete<PlanogramPerformanceDataDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
