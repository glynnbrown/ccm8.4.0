﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductLocalizationType
    {
        None = 0,
        Regional = 1,
        Local = 2
    }

    public static class PlanogramAssortmentProductLocalizationTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductLocalizationType, string> FriendlyNames =
            new Dictionary<PlanogramAssortmentProductLocalizationType, string>()
            {
                {PlanogramAssortmentProductLocalizationType.None, Message.Enum_PlanogramAssortmentProductLocalizationType_None},
                {PlanogramAssortmentProductLocalizationType.Regional, Message.Enum_PlanogramAssortmentProductLocalizationType_Regional},
                {PlanogramAssortmentProductLocalizationType.Local, Message.Enum_PlanogramAssortmentProductLocalizationType_Local }
            };
    }

}
