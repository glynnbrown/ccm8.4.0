﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegionLocation.
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentRegionLocation : ModelObject<PlanogramAssortmentRegionLocation>, IPlanogramAssortmentRegionLocation
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortmentRegion Parent
        {
            get { return (PlanogramAssortmentRegion)((PlanogramAssortmentRegionLocationList)base.Parent).Parent; }
        }
        #endregion

        #region LocationCode
        /// <summary>
        /// LocationCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode,Message.PlanogramAssortmentRegionLocation_Code);
        /// <summary>
        /// LocationCode
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
            set { SetProperty<String>(LocationCodeProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(LocationCodeProperty));
            BusinessRules.AddRule(new MaxLength(LocationCodeProperty, 50));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentRegionLocation NewPlanogramAssortmentRegionLocation()
        {
            var item = new PlanogramAssortmentRegionLocation();
            item.Create();
            return item;
        }

        public static PlanogramAssortmentRegionLocation NewPlanogramAssortmentRegionLocation(IPlanogramAssortmentRegionLocation regionLocation)
        {
            var item = new PlanogramAssortmentRegionLocation();
            item.Create(regionLocation);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        private void Create(IPlanogramAssortmentRegionLocation regionLocation)
        {
            LoadProperty<String>(LocationCodeProperty, regionLocation.LocationCode);
            this.Create();
        }
        #endregion

        #endregion
    }
}
