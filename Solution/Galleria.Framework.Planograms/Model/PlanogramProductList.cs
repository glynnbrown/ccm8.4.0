﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-27496 : L.Luong
//  Added ClearImages method
#endregion

#region Version History: CCM830

// V8-32132 : A.Silva
//  Added AppendNew method to append a second Planogram Product List's new items as copies.
// V8-32579 : M.Brumby
//  Removing a product now also removes any associated assortment product record
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of products contained within a planogram
    /// </summary>
    [Serializable]
    public partial class PlanogramProductList : ModelList<PlanogramProductList, PlanogramProduct>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramProductList NewPlanogramProductList()
        {
            PlanogramProductList item = new PlanogramProductList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when a product is being removed from this list
        /// </summary>
        protected override void RemoveItem(Int32 index)
        {
            // remove all corresponding positions
            // for this product
            this.Parent.Positions.RemoveList(
                this[index].GetPlanogramPositions());

            // remove all corresponding assortment products
            if (this.Parent.Assortment != null &&
                this.Parent.Assortment.Products != null)
            {
                PlanogramAssortmentProduct asmtProduct = this[index].GetPlanogramAssortmentProduct();
                if (asmtProduct != null)
                {
                    this.Parent.Assortment.Products.Remove(asmtProduct);
                }
            }

            // now perform the removal of the product
            base.RemoveItem(index);
        }

        public void ClearImages()
        {
            foreach (PlanogramProduct product in this) 
            {
                product.PlanogramImageIdAlternateBack = null;
                product.PlanogramImageIdAlternateBottom = null;
                product.PlanogramImageIdAlternateFront = null;
                product.PlanogramImageIdAlternateLeft = null;
                product.PlanogramImageIdAlternateRight = null;
                product.PlanogramImageIdAlternateTop = null;
                product.PlanogramImageIdBack = null;
                product.PlanogramImageIdBottom = null;
                product.PlanogramImageIdCaseBack = null;
                product.PlanogramImageIdCaseBottom = null;
                product.PlanogramImageIdCaseFront = null;
                product.PlanogramImageIdCaseLeft = null;
                product.PlanogramImageIdCaseRight = null;
                product.PlanogramImageIdCaseTop = null;
                product.PlanogramImageIdDisplayBack = null;
                product.PlanogramImageIdDisplayBottom = null;
                product.PlanogramImageIdDisplayFront = null;
                product.PlanogramImageIdDisplayLeft = null;
                product.PlanogramImageIdDisplayRight = null;
                product.PlanogramImageIdDisplayTop = null;
                product.PlanogramImageIdFront = null;
                product.PlanogramImageIdLeft = null;
                product.PlanogramImageIdPointOfPurchaseBack = null;
                product.PlanogramImageIdPointOfPurchaseBottom = null;
                product.PlanogramImageIdPointOfPurchaseFront = null;
                product.PlanogramImageIdPointOfPurchaseLeft = null;
                product.PlanogramImageIdPointOfPurchaseRight = null;
                product.PlanogramImageIdPointOfPurchaseTop = null;
                product.PlanogramImageIdRight = null;
                product.PlanogramImageIdTop = null;
                product.PlanogramImageIdTrayBack = null;
                product.PlanogramImageIdTrayBottom = null;
                product.PlanogramImageIdTrayFront = null;
                product.PlanogramImageIdTrayLeft = null;
                product.PlanogramImageIdTrayRight = null;
                product.PlanogramImageIdTrayTop = null;
            }
        }

        /// <summary>
        ///     Append any new items from the <paramref name="sourceItems"/> collection.
        /// </summary>
        /// <param name="sourceItems">Collection of <see cref="PlanogramProduct"/> items to add missing ones from.</param>
        /// <remarks>Uniqueness is determined by GTIN. Only copies of the source items are added to this instance.</remarks>
        public void AppendNew(PlanogramProductList sourceItems)
        {
            //  Look up existing GTINs in this list.
            List<String> existingGtins = Items.Select(item => item.Gtin).ToList();

            //  Find missing GTINs in the source products. These are the new products that need copying into this instance.
            IEnumerable<PlanogramProduct> newItems = sourceItems.Where(item => !existingGtins.Contains(item.Gtin));

            //  Add copies of the new products.
            AddRange(newItems.Select(item => item.Copy()));
        }

        #endregion
    }
}