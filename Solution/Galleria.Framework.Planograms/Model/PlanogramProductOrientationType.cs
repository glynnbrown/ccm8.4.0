﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#region Version History: (CCM 8.2)
// V8-25410 : I.George
//  changed the way the orientation values gets displayed
#endregion
#region Version History: (CCM 810)
// V8-28766 : J.Pickup
//  added GetPlanogramProductOrientationTypeFromPositionOrientatonTypeEquivalent().
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum for the Product Orientation type property
    /// </summary>
    public enum PlanogramProductOrientationType
    {
        Front0 = 0,
        Front90 = 1,
        Top0 = 2,
        Top90 = 3,
        Right0 = 4,
        Right90 = 5,
        Left0 = 6,
        Left90 = 7,
        Back0 = 8,
        Back90 = 9,
        Bottom0 = 10,
        Bottom90 = 11,
        Front180 = 12,
        Front270 = 13,
        Top180 = 14,
        Top270 = 15,
        Right180 = 16,
        Right270 = 17,
        Left180 = 18,
        Left270 = 19,
        Back180 = 20,
        Back270 = 21,
        Bottom180 = 22,
        Bottom270 = 23
    }

    /// <summary>
    /// PlanogramProductOrientationType Helper Class
    /// </summary>
    public static class PlanogramProductOrientationTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductOrientationType, String> FriendlyNames =
            new Dictionary<PlanogramProductOrientationType, String>()
            {
                {PlanogramProductOrientationType.Front0, Message.Enum_PlanogramProductOrientationType_Front0},
                {PlanogramProductOrientationType.Front90, Message.Enum_PlanogramProductOrientationType_Front90},
                {PlanogramProductOrientationType.Front180, Message.Enum_PlanogramProductOrientationType_Front180},
                {PlanogramProductOrientationType.Front270, Message.Enum_PlanogramProductOrientationType_Front270},
                {PlanogramProductOrientationType.Top0, Message.Enum_PlanogramProductOrientationType_Top0},
                {PlanogramProductOrientationType.Top90, Message.Enum_PlanogramProductOrientationType_Top90},
                {PlanogramProductOrientationType.Top180, Message.Enum_PlanogramProductOrientationType_Top180},
                {PlanogramProductOrientationType.Top270, Message.Enum_PlanogramProductOrientationType_Top270},
                {PlanogramProductOrientationType.Right0, Message.Enum_PlanogramProductOrientationType_Right0},
                {PlanogramProductOrientationType.Right90, Message.Enum_PlanogramProductOrientationType_Right90},
                {PlanogramProductOrientationType.Right180, Message.Enum_PlanogramProductOrientationType_Right180},
                {PlanogramProductOrientationType.Right270, Message.Enum_PlanogramProductOrientationType_Right270},
                {PlanogramProductOrientationType.Left0, Message.Enum_PlanogramProductOrientationType_Left0},
                {PlanogramProductOrientationType.Left90, Message.Enum_PlanogramProductOrientationType_Left90},
                {PlanogramProductOrientationType.Left180, Message.Enum_PlanogramProductOrientationType_Left180},
                {PlanogramProductOrientationType.Left270, Message.Enum_PlanogramProductOrientationType_Left270},
                {PlanogramProductOrientationType.Back0, Message.Enum_PlanogramProductOrientationType_Back0},
                {PlanogramProductOrientationType.Back90, Message.Enum_PlanogramProductOrientationType_Back90},
                {PlanogramProductOrientationType.Back180, Message.Enum_PlanogramProductOrientationType_Back180},
                {PlanogramProductOrientationType.Back270, Message.Enum_PlanogramProductOrientationType_Back270},
                {PlanogramProductOrientationType.Bottom0, Message.Enum_PlanogramProductOrientationType_Bottom0},
                {PlanogramProductOrientationType.Bottom90, Message.Enum_PlanogramProductOrientationType_Bottom90},
                {PlanogramProductOrientationType.Bottom180, Message.Enum_PlanogramProductOrientationType_Bottom180},
                {PlanogramProductOrientationType.Bottom270, Message.Enum_PlanogramProductOrientationType_Bottom270},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductOrientationType, String> FriendlyDescriptions =
            new Dictionary<PlanogramProductOrientationType, String>()
            {
                {PlanogramProductOrientationType.Front0, Message.Enum_PlanogramProductOrientationType_Front0},
                {PlanogramProductOrientationType.Front90, Message.Enum_PlanogramProductOrientationType_Front90},
                {PlanogramProductOrientationType.Front180, Message.Enum_PlanogramProductOrientationType_Front180},
                {PlanogramProductOrientationType.Front270, Message.Enum_PlanogramProductOrientationType_Front270},
                {PlanogramProductOrientationType.Top0, Message.Enum_PlanogramProductOrientationType_Top0},
                {PlanogramProductOrientationType.Top90, Message.Enum_PlanogramProductOrientationType_Top90},
                {PlanogramProductOrientationType.Top180, Message.Enum_PlanogramProductOrientationType_Top180},
                {PlanogramProductOrientationType.Top270, Message.Enum_PlanogramProductOrientationType_Top270},
                {PlanogramProductOrientationType.Right0, Message.Enum_PlanogramProductOrientationType_Right0},
                {PlanogramProductOrientationType.Right90, Message.Enum_PlanogramProductOrientationType_Right90},
                {PlanogramProductOrientationType.Right180, Message.Enum_PlanogramProductOrientationType_Right180},
                {PlanogramProductOrientationType.Right270, Message.Enum_PlanogramProductOrientationType_Right270},
                {PlanogramProductOrientationType.Left0, Message.Enum_PlanogramProductOrientationType_Left0},
                {PlanogramProductOrientationType.Left90, Message.Enum_PlanogramProductOrientationType_Left90},
                {PlanogramProductOrientationType.Left180, Message.Enum_PlanogramProductOrientationType_Left180},
                {PlanogramProductOrientationType.Left270, Message.Enum_PlanogramProductOrientationType_Left270},
                {PlanogramProductOrientationType.Back0, Message.Enum_PlanogramProductOrientationType_Back0},
                {PlanogramProductOrientationType.Back90, Message.Enum_PlanogramProductOrientationType_Back90},
                {PlanogramProductOrientationType.Back180, Message.Enum_PlanogramProductOrientationType_Back180},
                {PlanogramProductOrientationType.Back270, Message.Enum_PlanogramProductOrientationType_Back270},
                {PlanogramProductOrientationType.Bottom0, Message.Enum_PlanogramProductOrientationType_Bottom0},
                {PlanogramProductOrientationType.Bottom90, Message.Enum_PlanogramProductOrientationType_Bottom90},
                {PlanogramProductOrientationType.Bottom180, Message.Enum_PlanogramProductOrientationType_Bottom180},
                {PlanogramProductOrientationType.Bottom270, Message.Enum_PlanogramProductOrientationType_Bottom270},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductOrientationType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductOrientationType>()
            {
                {Message.Enum_PlanogramProductOrientationType_Front0.ToLowerInvariant(), PlanogramProductOrientationType.Front0},
                {Message.Enum_PlanogramProductOrientationType_Front90.ToLowerInvariant(), PlanogramProductOrientationType.Front90},
                {Message.Enum_PlanogramProductOrientationType_Front180.ToLowerInvariant(), PlanogramProductOrientationType.Front180},
                {Message.Enum_PlanogramProductOrientationType_Front270.ToLowerInvariant(), PlanogramProductOrientationType.Front270},
                {Message.Enum_PlanogramProductOrientationType_Top0.ToLowerInvariant(), PlanogramProductOrientationType.Top0},
                {Message.Enum_PlanogramProductOrientationType_Top90.ToLowerInvariant(), PlanogramProductOrientationType.Top90},
                {Message.Enum_PlanogramProductOrientationType_Top180.ToLowerInvariant(), PlanogramProductOrientationType.Top180},
                {Message.Enum_PlanogramProductOrientationType_Top270.ToLowerInvariant(), PlanogramProductOrientationType.Top270},
                {Message.Enum_PlanogramProductOrientationType_Right0.ToLowerInvariant(), PlanogramProductOrientationType.Right0},
                {Message.Enum_PlanogramProductOrientationType_Right90.ToLowerInvariant(), PlanogramProductOrientationType.Right90},
                {Message.Enum_PlanogramProductOrientationType_Right180.ToLowerInvariant(), PlanogramProductOrientationType.Right180},
                {Message.Enum_PlanogramProductOrientationType_Right270.ToLowerInvariant(), PlanogramProductOrientationType.Right270},
                {Message.Enum_PlanogramProductOrientationType_Left0.ToLowerInvariant(), PlanogramProductOrientationType.Left0},
                {Message.Enum_PlanogramProductOrientationType_Left90.ToLowerInvariant(), PlanogramProductOrientationType.Left90},
                {Message.Enum_PlanogramProductOrientationType_Left180.ToLowerInvariant(), PlanogramProductOrientationType.Left180},
                {Message.Enum_PlanogramProductOrientationType_Left270.ToLowerInvariant(), PlanogramProductOrientationType.Left270},
                {Message.Enum_PlanogramProductOrientationType_Back0.ToLowerInvariant(), PlanogramProductOrientationType.Back0},
                {Message.Enum_PlanogramProductOrientationType_Back90.ToLowerInvariant(), PlanogramProductOrientationType.Back90},
                {Message.Enum_PlanogramProductOrientationType_Back180.ToLowerInvariant(), PlanogramProductOrientationType.Back180},
                {Message.Enum_PlanogramProductOrientationType_Back270.ToLowerInvariant(), PlanogramProductOrientationType.Back270},
                {Message.Enum_PlanogramProductOrientationType_Bottom0.ToLowerInvariant(), PlanogramProductOrientationType.Bottom0},
                {Message.Enum_PlanogramProductOrientationType_Bottom90.ToLowerInvariant(), PlanogramProductOrientationType.Bottom90},
                {Message.Enum_PlanogramProductOrientationType_Bottom180.ToLowerInvariant(), PlanogramProductOrientationType.Bottom180},
                {Message.Enum_PlanogramProductOrientationType_Bottom270.ToLowerInvariant(), PlanogramProductOrientationType.Bottom270},

            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramProductOrientationType? PlanogramProductOrientationTypeGetEnum(String friendlyName)
        {
            PlanogramProductOrientationType? returnValue = null;
            PlanogramProductOrientationType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }

        /// <summary>
        /// Gets the equivalent PlanogramProductOrientation for the given PlanogramPositionOrientation.
        /// </summary>
        /// <param name="orientationToEvaulate">Position orientation to be evaluated.</param>
        /// <returns>PlanogramProductOrientation.</returns>
        /// <remarks>Useful for when the position orientation is marked default.</remarks>
        public static PlanogramProductOrientationType GetOrientationTypeFromEquivalent(
            PlanogramPositionOrientationType orientationToEvaulate, PlanogramProduct productToWhichThePositionBelongs)
        {
            switch (orientationToEvaulate)
            {
                case PlanogramPositionOrientationType.Front0:
                    return PlanogramProductOrientationType.Front0;
                case PlanogramPositionOrientationType.Front180:
                    return PlanogramProductOrientationType.Front180;
                case PlanogramPositionOrientationType.Front270:
                    return PlanogramProductOrientationType.Front270;
                case PlanogramPositionOrientationType.Front90:
                    return PlanogramProductOrientationType.Front90;
                case PlanogramPositionOrientationType.Top0:
                    return PlanogramProductOrientationType.Top0;
                case PlanogramPositionOrientationType.Top180:
                    return PlanogramProductOrientationType.Top180;
                case PlanogramPositionOrientationType.Top270:
                    return PlanogramProductOrientationType.Top270;
                case PlanogramPositionOrientationType.Top90:
                    return PlanogramProductOrientationType.Top90;
                case PlanogramPositionOrientationType.Right0:
                    return PlanogramProductOrientationType.Right0;
                case PlanogramPositionOrientationType.Right180:
                    return PlanogramProductOrientationType.Right180;
                case PlanogramPositionOrientationType.Right270:
                    return PlanogramProductOrientationType.Right270;
                case PlanogramPositionOrientationType.Right90:
                    return PlanogramProductOrientationType.Right90;
                case PlanogramPositionOrientationType.Left0:
                    return PlanogramProductOrientationType.Left0;
                case PlanogramPositionOrientationType.Left180:
                    return PlanogramProductOrientationType.Left180;
                case PlanogramPositionOrientationType.Left270:
                    return PlanogramProductOrientationType.Left270;
                case PlanogramPositionOrientationType.Left90:
                    return PlanogramProductOrientationType.Left90;
                case PlanogramPositionOrientationType.Back0:
                    return PlanogramProductOrientationType.Back0;
                case PlanogramPositionOrientationType.Back180:
                    return PlanogramProductOrientationType.Back180;
                case PlanogramPositionOrientationType.Back270:
                    return PlanogramProductOrientationType.Back270;
                case PlanogramPositionOrientationType.Back90:
                    return PlanogramProductOrientationType.Back90;
                case PlanogramPositionOrientationType.Bottom0:
                    return PlanogramProductOrientationType.Bottom0;
                case PlanogramPositionOrientationType.Bottom180:
                    return PlanogramProductOrientationType.Bottom180;
                case PlanogramPositionOrientationType.Bottom270:
                    return PlanogramProductOrientationType.Bottom270;
                case PlanogramPositionOrientationType.Bottom90:
                    return PlanogramProductOrientationType.Bottom90;

                //If is default then we require knowing what the products orientation is.
                case PlanogramPositionOrientationType.Default:
                    return productToWhichThePositionBelongs.OrientationType;

                default: throw new ArgumentOutOfRangeException("Orientaton not recognised");
            }
        }
    }
}
