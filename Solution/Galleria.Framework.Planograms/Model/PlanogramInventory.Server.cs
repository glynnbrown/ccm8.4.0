﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27647 : L.Luong
//	Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramInventory
    {
        #region Constructor
        private PlanogramInventory() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramInventory Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeDto dto)
        {
            return DataPortal.FetchChild<PlanogramInventory>(dalContext, dto);
        }

        public static PlanogramInventory Fetch(IDalContext dalContext, Object planogramId)
        {
            return DataPortal.FetchChild<PlanogramInventory>(new FetchByPlanogramIdCriteria(null, planogramId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramInventoryDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Single>(DaysOfPerformanceProperty, dto.DaysOfPerformance);
            this.LoadProperty<Single>(MinCasePacksProperty, dto.MinCasePacks);
            this.LoadProperty<Single>(MinDosProperty, dto.MinDos);
            this.LoadProperty<Single>(MinShelfLifeProperty, dto.MinShelfLife);
            this.LoadProperty<Single>(MinDeliveriesProperty, dto.MinDeliveries);
            this.LoadProperty<Boolean>(IsCasePacksValidatedProperty, dto.IsCasePacksValidated);
            this.LoadProperty<Boolean>(IsDosValidatedProperty, dto.IsDosValidated);
            this.LoadProperty<Boolean>(IsShelfLifeValidatedProperty, dto.IsShelfLifeValidated);
            this.LoadProperty<Boolean>(IsDeliveriesValidatedProperty, dto.IsDeliveriesValidated);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<PlanogramInventoryMetricType>(InventoryMetricTypeProperty, (PlanogramInventoryMetricType)dto.InventoryMetricType);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramInventoryDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramInventoryDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                DaysOfPerformance = this.ReadProperty<Single>(DaysOfPerformanceProperty),
                MinCasePacks = this.ReadProperty<Single>(MinCasePacksProperty),
                MinDos = this.ReadProperty<Single>(MinDosProperty),
                MinShelfLife = this.ReadProperty<Single>(MinShelfLifeProperty),
                MinDeliveries = this.ReadProperty<Single>(MinDeliveriesProperty),
                IsCasePacksValidated = this.ReadProperty<Boolean>(IsCasePacksValidatedProperty),
                IsDosValidated = this.ReadProperty<Boolean>(IsDosValidatedProperty),
                IsShelfLifeValidated = this.ReadProperty<Boolean>(IsShelfLifeValidatedProperty),
                IsDeliveriesValidated = this.ReadProperty<Boolean>(IsDeliveriesValidatedProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                InventoryMetricType = (Byte)this.ReadProperty<PlanogramInventoryMetricType>(InventoryMetricTypeProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPlanogramInventoryDal dal = dalContext.GetDal<IPlanogramInventoryDal>())
                    {
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    this.Create();
                }
            }
            this.MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramInventoryDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramInventoryDto>(
            (dc) =>
            {
                PlanogramInventoryDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramInventory>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramInventoryDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramInventoryDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
