﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    public enum PackageUnlockResult : byte
    {
        Failed = 0, // indicates the package unlock failed
        Success = 1 // indicates the package unlock was successful
    }
}
