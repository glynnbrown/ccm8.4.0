﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Collection of <see cref="PlanogramComparisonFieldValue"/> instances belonging to the parent <see cref="PlanogramComparisonItem"/>.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonFieldValueList : ModelList<PlanogramComparisonFieldValueList, PlanogramComparisonFieldValue>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparisonItem"/>.
        /// </summary>
        public new PlanogramComparisonItem Parent { get { return (PlanogramComparisonItem)base.Parent; } }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparisonResultList"/>.
        /// </summary>
        public static PlanogramComparisonFieldValueList NewPlanogramComparisonFieldValueList()
        {
            var item = new PlanogramComparisonFieldValueList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Add a collection of <see cref="PlanogramComparisonFieldValue"/> derived from an enumeration of Field Placeholders, with null values./>.
        /// </summary>
        /// <param name="range">The enumeration of field placeholders from which to derive the new items.</param>
        public void AddRange(IEnumerable<String> range)
        {
            base.AddRange(range.Select(s => PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(s, null)));
        }

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonFieldValue"/> derived from a field placeholder, with a null value.
        /// </summary>
        /// <param name="item">The field placeholder from which to derive the new item.</param>
        public PlanogramComparisonFieldValue Add(String item)
        {
            PlanogramComparisonFieldValue newItem = PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue(item, null);
            Add(newItem);
            return newItem;
        }

        #endregion
    }
}