﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    public enum PackageLockResult : byte
    {
        Failed = 0, // indicates the lock could not be obtained
        Success = 1, // indicates the lock was obtained successfully
        PreviousLock = 2 // indicates that the user already holds a lock
    }
}
