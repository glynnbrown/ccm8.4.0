﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong
//	Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTree
    {
        #region Constructor
        private PlanogramConsumerDecisionTree() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramConsumerDecisionTree Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeDto dto)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTree>(dalContext, dto);
        }

        public static PlanogramConsumerDecisionTree Fetch(IDalContext dalContext, Object planogramId)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTree>(new FetchByPlanogramIdCriteria(null, planogramId));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Ensures that all level ids are correctly synched with product groups
        /// </summary>
        private void UpdateChildGroupToLevelIds()
        {
            //only proceed if levels exist
            List<PlanogramConsumerDecisionTreeLevel> childLevelList = this.FetchAllLevels().ToList();
            if (childLevelList.Count > 0)
            {
                //get the resolved id dict
                //Dictionary<Guid, Int32> resolvedIdDict =
                //  ConsumerDecisionTreeLevel.ConstructResolvedIdDictionary(childLevelList);

                //get the list of groups to be ammended
                IEnumerable<PlanogramConsumerDecisionTreeNode> nodeList = this.FetchAllNodes();

                //resolve
                //ConsumerDecisionTreeNode.ResolveConsumerDecisionTreeLevelReferences(nodeList, resolvedIdDict);
            }

        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private PlanogramConsumerDecisionTreeDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramConsumerDecisionTreeDto
            {
                Id = this.ReadProperty<Object>(IdProperty),
                Name = this.ReadProperty<String>(NameProperty),
                PlanogramId = parent.Id,
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <param name="childNodeList">list of child nodes for this hierarchy</param>
        private void LoadDataTransferObject(
            IDalContext dalContext,
            PlanogramConsumerDecisionTreeDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeLevelDto> childPlanogramConsumerDecisionTreeLevelList,
            IEnumerable<PlanogramConsumerDecisionTreeNodeDto> childNodeList)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);

            //get the root level dto and load it

            PlanogramConsumerDecisionTreeLevelDto rootLevelDto = childPlanogramConsumerDecisionTreeLevelList.FirstOrDefault(l => (l.ParentLevelId == null || l.ParentLevelId.Equals(0)));
            if (rootLevelDto != null)
            {
                this.LoadProperty<PlanogramConsumerDecisionTreeLevel>(RootLevelProperty,
                    PlanogramConsumerDecisionTreeLevel.FetchPlanogramConsumerDecisionTreeLevel(dalContext, rootLevelDto, childPlanogramConsumerDecisionTreeLevelList));
            }

            //get the root group dto and load
            PlanogramConsumerDecisionTreeNodeDto rootNodeDto = childNodeList.FirstOrDefault(l => (l.ParentNodeId == null || l.ParentNodeId.Equals(0)));
            if (rootNodeDto != null)
            {
                this.LoadProperty<PlanogramConsumerDecisionTreeNode>(RootNodeProperty,
                    PlanogramConsumerDecisionTreeNode.FetchPlanogramConsumerDecisionTreeNode(dalContext, rootNodeDto, childNodeList));
            }
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            Object treeId = criteria.PlanogramId;

            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);

            try
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // fetch the structure dto
                    PlanogramConsumerDecisionTreeDto dto;
                    using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeDal>())
                    {
                        dto = dal.FetchByPlanogramId(treeId);
                    }

                    //now use the hierarchy id to get all the levels in one go
                    IEnumerable<PlanogramConsumerDecisionTreeLevelDto> levelDtoList;
                    using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeLevelDal>())
                    {
                        levelDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(dto.Id);
                    }


                    //and get all the nodes in one go
                    IEnumerable<PlanogramConsumerDecisionTreeNodeDto> nodeDtoList;
                    using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
                    {
                        nodeDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(dto.Id);
                    }

                    //load the hierarchy
                    LoadDataTransferObject(dalContext, dto, levelDtoList, nodeDtoList);
                }
            }
            catch (DtoDoesNotExistException)
            {
                this.Create();
            }
            this.MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeDto dto)
        {
            //now use the hierarchy id to get all the levels in one go
            IEnumerable<PlanogramConsumerDecisionTreeLevelDto> levelDtoList;
            using (IPlanogramConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeLevelDal>())
            {
                levelDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(dto.Id);
            }

            //and get all the nodes in one go
            IEnumerable<PlanogramConsumerDecisionTreeNodeDto> nodeDtoList;
            using (IPlanogramConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
            {
                nodeDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(dto.Id);
            }

            this.LoadDataTransferObject(dalContext, dto, levelDtoList, nodeDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeDto>(
            (dc) =>
            {
                PlanogramConsumerDecisionTreeDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTree>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramConsumerDecisionTreeDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
