using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{

    public sealed partial class ProductAttributeComparisonResultList
    {
        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private ProductAttributeComparisonResultList() { }

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="ProductAttributeComparisonResultList" />.
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IProductAttributeComparisonResultDal>())
                {
                    IEnumerable<ProductAttributeComparisonResultDto> dtoList = dal.FetchByPlanogramId(criteria.ParentId);
                    foreach (ProductAttributeComparisonResultDto dto in dtoList.OrderBy(g => g.ComparedProductAttribute))
                    {
                        Add(ProductAttributeComparisonResult.Fetch(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }
    }
}