﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29622 : L.Ineson
//  Created
// V8-29137 : A.Kuszyk
//  Added static helper class.
#endregion

#region Version History: CCM830

// CCM-13871 : A.Silva
//  Added methods GetAxis, IsPositive and GetNextSequence.

#endregion

#endregion

using System;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the different types of anchor direction
    /// </summary>
    public enum PlanogramPositionAnchorDirection
    {
        InFront = 0,
        Behind = 1,
        Above = 2,
        Below = 3,
        ToLeft = 4,
        ToRight = 5
    }

    public static class PlanogramPositionAnchorDirectionHelper
    {
        public static PlanogramPositionAnchorDirection GetDirection(AxisType axis, Boolean positiveDirection)
        {
            switch(axis)
            {
                case AxisType.X:
                    return positiveDirection ? PlanogramPositionAnchorDirection.ToRight : PlanogramPositionAnchorDirection.ToLeft;
                case AxisType.Y:
                    return positiveDirection ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below;
                case AxisType.Z:
                    return positiveDirection ? PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;
                default: throw new NotSupportedException();
            }
        }

        /// <summary>
        ///     Determines the axis that that given <paramref name="anchorDirection"/> is on, regardless of its direction.
        /// </summary>
        /// <param name="anchorDirection">The <see cref="PlanogramPositionAnchorDirection"/> to check for direction type.</param>
        /// <returns>The <see cref="AxisType"/> of the <paramref name="anchorDirection"/>.</returns>
        public static AxisType GetAxis(this PlanogramPositionAnchorDirection anchorDirection)
        {
            switch (anchorDirection)
            {
                case PlanogramPositionAnchorDirection.InFront:
                case PlanogramPositionAnchorDirection.Behind:
                    return AxisType.Z;
                case PlanogramPositionAnchorDirection.Above:
                case PlanogramPositionAnchorDirection.Below:
                    return AxisType.Y;
                case PlanogramPositionAnchorDirection.ToLeft:
                case PlanogramPositionAnchorDirection.ToRight:
                    return AxisType.X;
                default:
                    return AxisType.X;
            }
        }

        /// <summary>
        ///     Determines whether the provided <paramref name="anchorDirection"/> is <c>positive</c> or <c>negative</c>, 
        /// that is whether the axis sequence would progress to larger numbers or to smaller ones.
        /// </summary>
        /// <param name="anchorDirection">The <see cref="PlanogramPositionAnchorDirection"/> to check for direction type.</param>
        /// <returns><c>True</c> if the direction goes up, or <c>false</c> if it goes down.</returns>
        private static Boolean IsPositive(this PlanogramPositionAnchorDirection anchorDirection)
        {
            switch (anchorDirection)
            {
                case PlanogramPositionAnchorDirection.Below:
                case PlanogramPositionAnchorDirection.Behind:
                case PlanogramPositionAnchorDirection.ToLeft:
                    return false;
                case PlanogramPositionAnchorDirection.Above:
                case PlanogramPositionAnchorDirection.InFront:
                case PlanogramPositionAnchorDirection.ToRight:
                    return true;
                default:
                    return true;
            }
        }
        
        /// <summary>
        ///     Determines the next sequence on the axis and direction 
        ///     indicated by <paramref name="anchorDirection"/> 
        ///     for the given <paramref name="position"/>.
        /// </summary>
        /// <param name="anchorDirection">The direction to get the next sequence number from.</param>
        /// <param name="position">The position to get the next sequence number for.</param>
        /// <returns>An <see cref="Int16"/> value that represents the next sequence
        ///     on the axis and direction given by <paramref name="anchorDirection"/> and
        ///     for the given <paramref name="position"/>.</returns>
        public static Int16 GetNextSequence(this PlanogramPositionAnchorDirection anchorDirection, PlanogramPosition position)
        {
            return (Int16)(position.GetSequence(anchorDirection.GetAxis()) + (anchorDirection.IsPositive() ? 1 : -1));
        }

    }
}