﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created

#endregion

#endregion

using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Represents a list containing PlanogramConsumerDecisionTreeNode objects
    /// (Child of PlanogramConsumerDecisionTreeNode)
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeNodeList : ModelList<PlanogramConsumerDecisionTreeNodeList, PlanogramConsumerDecisionTreeNode>
    {
        #region Parent

        /// <summary>
        /// Returns the parent node
        /// </summary>
        public PlanogramConsumerDecisionTreeNode ParentNode
        {
            get { return base.Parent as PlanogramConsumerDecisionTreeNode; }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the total number of products
        /// contained within the nodes in this node list
        /// </summary>
        public Int32 TotalProductCount
        {
            get
            {
                Int32 totalProds = 0;
                foreach (PlanogramConsumerDecisionTreeNode node in this)
                {
                    totalProds += node.TotalProductCount;
                }
                return totalProds;
            }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramConsumerDecisionTreeNodeList NewPlanogramConsumerDecisionTreeNodeList()
        {
            PlanogramConsumerDecisionTreeNodeList item = new PlanogramConsumerDecisionTreeNodeList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramConsumerDecisionTreeNodeList NewPlanogramConsumerDecisionTreeNodeList
            (PlanogramConsumerDecisionTreeNodeList nodeList, PlanogramConsumerDecisionTreeNode rootNode)
        {
            PlanogramConsumerDecisionTreeNodeList item = new PlanogramConsumerDecisionTreeNodeList();
            item.Create(nodeList, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramConsumerDecisionTreeNodeList nodeList, PlanogramConsumerDecisionTreeNode rootNode)
        {
            foreach (PlanogramConsumerDecisionTreeNode node in nodeList)
            {
                this.Add(PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(node, rootNode));
            }

            MarkAsChild();
        }

        #endregion

        #endregion
    }
}
