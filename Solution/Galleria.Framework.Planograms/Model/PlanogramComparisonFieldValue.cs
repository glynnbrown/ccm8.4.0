﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Represents the result for an item comparisons agains the same item in the target planogram.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonFieldValue : ModelObject<PlanogramComparisonFieldValue>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparisonItem"/>.
        /// </summary>
        public new PlanogramComparisonItem Parent
        {
            get { return ((PlanogramComparisonFieldValueList)base.Parent).Parent; }
        }

        #endregion

        #region FieldPlaceholder

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="FieldPlaceholder"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldPlaceholderProperty =
            RegisterModelProperty<String>(c => c.FieldPlaceholder);

        /// <summary>
        ///     Get the field placeholder for the field compared in this instance.
        /// </summary>
        public String FieldPlaceholder
        {
            get { return GetProperty(FieldPlaceholderProperty); }
            set { SetProperty(FieldPlaceholderProperty, value); }
        }

        #endregion

        #region Value

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="Value"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValueProperty =
            RegisterModelProperty<String>(c => c.Value);

        /// <summary>
        ///     Get the value for the field in this instance.
        /// </summary>
        public String Value
        {
            get { return GetProperty(ValueProperty); }
            set { SetProperty(ValueProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(FieldPlaceholderProperty));
            BusinessRules.AddRule(new Required(ValueProperty));
            BusinessRules.AddRule(new MaxLength(FieldPlaceholderProperty, 1000));
            BusinessRules.AddRule(new MaxLength(ValueProperty, 100));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonFieldValue"/>.
        /// </summary>
        public static PlanogramComparisonFieldValue NewPlanogramComparisonFieldValue()
        {
            var item = new PlanogramComparisonFieldValue();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonFieldValue"/> storing the <paramref name="value"/> of a given <paramref name="fieldPlaceholder"/>.
        /// </summary>
        /// <param name="fieldPlaceholder">The field to register a value for.</param>
        /// <param name="value">The value for the field.</param>
        public static PlanogramComparisonFieldValue NewPlanogramComparisonFieldValue(String fieldPlaceholder, String value)
        {
            var item = new PlanogramComparisonFieldValue();
            item.Create(fieldPlaceholder, value);
            return item;
        }

        #endregion

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Initialize all default property values for this instance using the given <paramref name="fieldPlaceholder"/> and <paramref name="value"/>.
        /// </summary>
        private void Create(String fieldPlaceholder, String value)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(FieldPlaceholderProperty, fieldPlaceholder);
            LoadProperty(ValueProperty, value);
            MarkAsChild();
            base.Create();
        }

        #endregion
    }
}