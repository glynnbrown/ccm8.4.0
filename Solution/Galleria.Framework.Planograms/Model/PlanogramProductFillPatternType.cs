﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : A.Kuszyk
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramProductFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }

    /// <summary>
    /// ProductFillPatternType Helper Class
    /// </summary>
    public static class PlanogramProductFillPatternTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductFillPatternType, String> FriendlyNames =
            new Dictionary<PlanogramProductFillPatternType, String>()
            {
                {PlanogramProductFillPatternType.Solid, Message.Enum_PlanogramProductFillPatternType_Solid},
                {PlanogramProductFillPatternType.Border, Message.Enum_PlanogramProductFillPatternType_Border},
                {PlanogramProductFillPatternType.DiagonalUp, Message.Enum_PlanogramProductFillPatternType_DiagonalUp},
                {PlanogramProductFillPatternType.DiagonalDown, Message.Enum_PlanogramProductFillPatternType_DiagonalDown},
                {PlanogramProductFillPatternType.Crosshatch, Message.Enum_PlanogramProductFillPatternType_Crosshatch},
                {PlanogramProductFillPatternType.Horizontal, Message.Enum_PlanogramProductFillPatternType_Horizontal},
                {PlanogramProductFillPatternType.Vertical, Message.Enum_PlanogramProductFillPatternType_Vertical},
                {PlanogramProductFillPatternType.Dotted, Message.Enum_PlanogramProductFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductFillPatternType, String> FriendlyDescriptions =
            new Dictionary<PlanogramProductFillPatternType, String>()
            {
                {PlanogramProductFillPatternType.Solid, Message.Enum_PlanogramProductFillPatternType_Solid},
                {PlanogramProductFillPatternType.Border, Message.Enum_PlanogramProductFillPatternType_Border},
                {PlanogramProductFillPatternType.DiagonalUp, Message.Enum_PlanogramProductFillPatternType_DiagonalUp},
                {PlanogramProductFillPatternType.DiagonalDown, Message.Enum_PlanogramProductFillPatternType_DiagonalDown},
                {PlanogramProductFillPatternType.Crosshatch, Message.Enum_PlanogramProductFillPatternType_Crosshatch},
                {PlanogramProductFillPatternType.Horizontal, Message.Enum_PlanogramProductFillPatternType_Horizontal},
                {PlanogramProductFillPatternType.Vertical, Message.Enum_PlanogramProductFillPatternType_Vertical},
                {PlanogramProductFillPatternType.Dotted, Message.Enum_PlanogramProductFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductFillPatternType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductFillPatternType>()
            {
                {Message.Enum_PlanogramProductFillPatternType_Solid.ToLowerInvariant(), PlanogramProductFillPatternType.Solid},
                {Message.Enum_PlanogramProductFillPatternType_Border.ToLowerInvariant(), PlanogramProductFillPatternType.Border},
                {Message.Enum_PlanogramProductFillPatternType_DiagonalUp.ToLowerInvariant(), PlanogramProductFillPatternType.DiagonalUp},
                {Message.Enum_PlanogramProductFillPatternType_DiagonalDown.ToLowerInvariant(), PlanogramProductFillPatternType.DiagonalDown},
                {Message.Enum_PlanogramProductFillPatternType_Crosshatch.ToLowerInvariant(),PlanogramProductFillPatternType.Crosshatch},
                {Message.Enum_PlanogramProductFillPatternType_Horizontal.ToLowerInvariant(), PlanogramProductFillPatternType.Horizontal},
                {Message.Enum_PlanogramProductFillPatternType_Vertical.ToLowerInvariant(), PlanogramProductFillPatternType.Vertical},
                {Message.Enum_PlanogramProductFillPatternType_Dotted.ToLowerInvariant(), PlanogramProductFillPatternType.Dotted}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        public static PlanogramProductFillPatternType? PlanogramProductFillPatternTypeGetEnum(String friendlyName)
        {
            PlanogramProductFillPatternType? returnValue = null;
            PlanogramProductFillPatternType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}