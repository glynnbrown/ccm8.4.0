﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#region Version History: (CCM 8.11)
// V8-30576 : N.Haywood
//  Added textbox annotation type
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available types of annotation
    /// </summary>
    public enum PlanogramAnnotationType
    {
        Informational,
        Preserve,
        TextBox
    }

    /// <summary>
    /// PlanogramAnnotationType Helper Class
    /// </summary>
    public static class PlanogramAnnotationTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAnnotationType, String> FriendlyNames =
            new Dictionary<PlanogramAnnotationType, String>()
            {
                {PlanogramAnnotationType.Informational, Message.Enum_PlanogramAnnotationType_Informational},
                {PlanogramAnnotationType.Preserve, Message.Enum_PlanogramAnnotationType_Preserve},
                {PlanogramAnnotationType.TextBox, Message.Enum_PlanogramAnnotationType_TextBox},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAnnotationType, String> FriendlyDescriptions =
            new Dictionary<PlanogramAnnotationType, String>()
            {
                {PlanogramAnnotationType.Informational, Message.Enum_PlanogramAnnotationType_Informational},
                {PlanogramAnnotationType.Preserve, Message.Enum_PlanogramAnnotationType_Preserve},
                {PlanogramAnnotationType.TextBox, Message.Enum_PlanogramAnnotationType_TextBox},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramAnnotationType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramAnnotationType>()
            {
                {Message.Enum_PlanogramAnnotationType_Informational.ToLowerInvariant(), PlanogramAnnotationType.Informational},
                {Message.Enum_PlanogramAnnotationType_Preserve.ToLowerInvariant(), PlanogramAnnotationType.Preserve},
                {Message.Enum_PlanogramAnnotationType_TextBox.ToLowerInvariant(), PlanogramAnnotationType.TextBox},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramAnnotationType? PlanogramAnnotationTypeGetEnum(String friendlyName)
        {
            PlanogramAnnotationType? returnValue = null;
            PlanogramAnnotationType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
