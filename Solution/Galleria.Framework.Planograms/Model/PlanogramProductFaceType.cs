﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum denoting the available face types
    /// </summary>
    public enum PlanogramProductFaceType : byte
    {
        Front = 0,
        Back = 1,
        Top = 2,
        Bottom = 3,
        Left = 4,
        Right = 5
    }
}
