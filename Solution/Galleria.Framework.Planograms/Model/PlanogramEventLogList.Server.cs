﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramEventLogList
    {
        #region Constructor
        private PlanogramEventLogList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all Event Logs for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramEventLogDal dal = dalContext.GetDal<IPlanogramEventLogDal>())
                {
                    IEnumerable<PlanogramEventLogDto> dtoList = dal.FetchByPlanogramId(criteria.ParentId);
                    foreach (PlanogramEventLogDto dto in dtoList)
                    {
                        this.Add(PlanogramEventLog.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
