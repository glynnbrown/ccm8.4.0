﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes values available for the Product StatusType property
    /// </summary>
    public enum PlanogramProductStatusType   
    {
        Active,
        SellThrough, 
        Promo
    }

    /// <summary>
    /// PlanogramProductStatusType Helper Class
    /// </summary>
    public static class PlanogramProductStatusTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductStatusType, String> FriendlyNames =
            new Dictionary<PlanogramProductStatusType, String>()
            {
                {PlanogramProductStatusType.Active, Message.Enum_PlanogramProductStatusType_Active},
                {PlanogramProductStatusType.SellThrough, Message.Enum_PlanogramProductStatusType_SellThrough},
                {PlanogramProductStatusType.Promo, Message.Enum_PlanogramProductStatusType_Promo},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductStatusType, String> FriendlyDescriptions =
            new Dictionary<PlanogramProductStatusType, String>()
            {
                {PlanogramProductStatusType.Active, Message.Enum_PlanogramProductStatusType_Active},
                {PlanogramProductStatusType.SellThrough, Message.Enum_PlanogramProductStatusType_SellThrough},
                {PlanogramProductStatusType.Promo, Message.Enum_PlanogramProductStatusType_Promo},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductStatusType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductStatusType>()
            {
                {Message.Enum_PlanogramProductStatusType_Active.ToLowerInvariant(), PlanogramProductStatusType.Active},
                {Message.Enum_PlanogramProductStatusType_SellThrough.ToLowerInvariant(), PlanogramProductStatusType.SellThrough},
                {Message.Enum_PlanogramProductStatusType_Promo.ToLowerInvariant(), PlanogramProductStatusType.Promo},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramProductStatusType? PlanogramProductStatusTypeGetEnum(String friendlyName)
        {
            PlanogramProductStatusType? returnValue = null;
            PlanogramProductStatusType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
