﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25479 : A.Kuszyk
//      Business rules added.
// V8-26569 : L.Ineson
//  Added methods to get world space coordinates
// V8-26760 : L.Ineson
//  Added friendly names and display types.
#endregion
#region Version History: CCM810
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
#endregion
#region Version History: (CCM 8.11)
// V8-30576 : N.Haywood
//  Added textbox annotation type
#endregion
#region Version History: (CCM 8.3)
// V8-32523 : L.ineson
//  Added factory method for new component annotation.
// V8-32591 : L.Ineson
//  Added pass through for plan settings.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a Planogram Annotation object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAnnotation : ModelObject<PlanogramAnnotation>
    {
        #region Constants
        const String WorldXFieldName = "WorldX";
        const String WorldYFieldName = "WorldY";
        const String WorldZFieldName = "WorldZ";
        #endregion

        #region Constructors
        static PlanogramAnnotation()
        {
            FriendlyName = Message.PlanogramAnnotation_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramAnnotationList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region PlanogramFixtureItemId
        /// <summary>
        /// PlanogramFixtureItemId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureItemIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureItemId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureItemId
        /// </summary>
        public Object PlanogramFixtureItemId
        {
            get { return GetProperty<Object>(PlanogramFixtureItemIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureItemIdProperty, value); }
        }
        #endregion

        #region PlanogramFixtureAssemblyId
        /// <summary>
        /// PlanogramFixtureAssemblyId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureAssemblyIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureAssemblyId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureAssemblyId
        /// </summary>
        public Object PlanogramFixtureAssemblyId
        {
            get { return GetProperty<Object>(PlanogramFixtureAssemblyIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureAssemblyIdProperty, value); }
        }
        #endregion

        #region PlanogramFixtureComponentId
        /// <summary>
        /// PlanogramFixtureComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramFixtureComponentId
        /// </summary>
        public Object PlanogramFixtureComponentId
        {
            get { return GetProperty<Object>(PlanogramFixtureComponentIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramAssemblyComponentId
        /// <summary>
        /// PlanogramAssemblyComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramAssemblyComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramAssemblyComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramAssemblyComponentId
        /// </summary>
        public Object PlanogramAssemblyComponentId
        {
            get { return GetProperty<Object>(PlanogramAssemblyComponentIdProperty); }
            set { SetProperty<Object>(PlanogramAssemblyComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramSubComponentId
        /// <summary>
        /// PlanogramSubComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramSubComponentIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramSubComponentId);
        /// <summary>
        /// The PlanogramPosition PlanogramSubComponentId
        /// </summary>
        public Object PlanogramSubComponentId
        {
            get { return GetProperty<Object>(PlanogramSubComponentIdProperty); }
            set { SetProperty<Object>(PlanogramSubComponentIdProperty, value); }
        }
        #endregion

        #region PlanogramPositionId
        /// <summary>
        /// PlanogramPositionId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramPositionIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramPositionId);
        /// <summary>
        /// The PlanogramPosition PlanogramPositionId
        /// </summary>
        public Object PlanogramPositionId
        {
            get { return GetProperty<Object>(PlanogramPositionIdProperty); }
            set { SetProperty<Object>(PlanogramPositionIdProperty, value); }
        }
        #endregion

        #region AnnotationType
        /// <summary>
        /// AnnotationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAnnotationType> AnnotationTypeProperty =
            RegisterModelProperty<PlanogramAnnotationType>(c => c.AnnotationType);
        /// <summary>
        /// The type of textbox this is
        /// </summary>
        public PlanogramAnnotationType AnnotationType
        {
            get { return GetProperty<PlanogramAnnotationType>(AnnotationTypeProperty); }
            set { SetProperty<PlanogramAnnotationType>(AnnotationTypeProperty, value); }
        }
        #endregion

        #region Text
        /// <summary>
        /// Text property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextProperty =
            RegisterModelProperty<String>(c => c.Text, Message.PlanogramAnnotation_Text);
        /// <summary>
        /// Annotation Text
        /// </summary>
        public String Text
        {
            get { return GetProperty<String>(TextProperty); }
            set { SetProperty<String>(TextProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> XProperty =
            RegisterModelProperty<Single?>(c => c.X, Message.Generic_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation X
        /// </summary>
        public Single? X
        {
            get { return GetProperty<Single?>(XProperty); }
            set { SetProperty<Single?>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> YProperty =
            RegisterModelProperty<Single?>(c => c.Y, Message.Generic_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Y
        /// </summary>
        public Single? Y
        {
            get { return GetProperty<Single?>(YProperty); }
            set { SetProperty<Single?>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> ZProperty =
            RegisterModelProperty<Single?>(c => c.Z, Message.Generic_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Z
        /// </summary>
        public Single? Z
        {
            get { return GetProperty<Single?>(ZProperty); }
            set { SetProperty<Single?>(ZProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.Generic_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Height
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.Generic_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Width
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.Generic_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Depth
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region BorderColour
        /// <summary>
        /// BorderColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BorderColourProperty=
            RegisterModelProperty<Int32>(c => c.BorderColour, Message.PlanogramAnnotation_BorderColour);
        /// <summary>
        /// The colour of the annotation border
        /// </summary>
        public Int32 BorderColour
        {
            get { return GetProperty<Int32>(BorderColourProperty); }
            set { SetProperty<Int32>(BorderColourProperty, value); }
        }
        #endregion

        #region BorderThickness
        /// <summary>
        /// BorderThickness property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BorderThicknessProperty=
            RegisterModelProperty<Single>(c => c.BorderThickness, Message.PlanogramAnnotation_BorderThickness);
        /// <summary>
        /// The thickness of the annotation border
        /// </summary>
        public Single BorderThickness
        {
            get { return GetProperty<Single>(BorderThicknessProperty); }
            set { SetProperty<Single>(BorderThicknessProperty, value); }
        }
        #endregion

        #region BackgroundColour
        /// <summary>
        /// BackgroundColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BackgroundColourProperty =
            RegisterModelProperty<Int32>(c => c.BackgroundColour, Message.PlanogramAnnotation_BackgroundColour);
        /// <summary>
        /// The colour of the annotation background
        /// </summary>
        public Int32 BackgroundColour
        {
            get { return GetProperty<Int32>(BackgroundColourProperty); }
            set { SetProperty<Int32>(BackgroundColourProperty, value); }
        }
        #endregion

        #region FontColour
        /// <summary>
        /// FontColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FontColourProperty =
            RegisterModelProperty<Int32>(c => c.FontColour, Message.PlanogramAnnotation_FontColour);
        /// <summary>
        /// The annotation text foreground colour
        /// </summary>
        public Int32 FontColour
        {
            get { return GetProperty<Int32>(FontColourProperty); }
            set { SetProperty<Int32>(FontColourProperty, value); }
        }
        #endregion

        #region FontSize
        /// <summary>
        /// FontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FontSizeProperty =
            RegisterModelProperty<Single>(c => c.FontSize, Message.PlanogramAnnotation_FontSize);
        /// <summary>
        /// The size of the annotation text
        /// </summary>
        public Single FontSize
        {
            get { return GetProperty<Single>(FontSizeProperty); }
            set { SetProperty<Single>(FontSizeProperty, value); }
        }
        #endregion

        #region FontName
        /// <summary>
        /// FontName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FontNameProperty =
            RegisterModelProperty<String>(c => c.FontName, Message.PlanogramAnnotation_FontName);
        /// <summary>
        /// The name of the font family to render text as
        /// </summary>
        public String FontName
        {
            get { return GetProperty<String>(FontNameProperty); }
            set { SetProperty<String>(FontNameProperty, value); }
        }
        #endregion

        #region CanReduceFontToFit
        /// <summary>
        /// CanReduceFontToFit property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanReduceFontToFitProperty =
            RegisterModelProperty<Boolean>(c => c.CanReduceFontToFit, Message.PlanogramAnnotation_CanReduceFontToFit);
        /// <summary>
        /// Indicates if the font size can be reduced if too large
        /// </summary>
        public Boolean CanReduceFontToFit
        {
            get { return GetProperty<Boolean>(CanReduceFontToFitProperty); }
            set { SetProperty<Boolean>(CanReduceFontToFitProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(TextProperty));
            BusinessRules.AddRule(new MaxLength(TextProperty, 255));
            BusinessRules.AddRule(new MaxLength(FontNameProperty, 255));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAnnotation NewPlanogramAnnotation()
        {
            PlanogramAnnotation item = new PlanogramAnnotation();
            item.Create(PlanogramSettings.NewPlanogramSettings());
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAnnotation NewPlanogramAnnotation(IPlanogramSettings settings)
        {
            PlanogramAnnotation item = new PlanogramAnnotation();
            item.Create(settings);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAnnotation NewPlanogramAnnotation(PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            PlanogramAnnotation item = new PlanogramAnnotation();
            item.Create(fixtureItem, settings);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAnnotation NewPlanogramAnnotation(PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            PlanogramAnnotation item = new PlanogramAnnotation();
            item.Create(fixtureComponent, fixtureItem, settings);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected void Create(IPlanogramSettings settings)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Single?>(XProperty, 0);
            this.LoadProperty<Single?>(YProperty, 0);
            this.LoadProperty<Single?>(ZProperty, 0);
            this.LoadProperty<String>(TextProperty, Message.Generic_New);
            this.LoadProperty<PlanogramAnnotationType>(AnnotationTypeProperty, PlanogramAnnotationType.TextBox);
            
            this.LoadProperty<String>(FontNameProperty, settings.TextBoxFont);
            this.LoadProperty<Single>(FontSizeProperty, settings.TextBoxFontSize);
            this.LoadProperty<Int32>(FontColourProperty, settings.TextBoxFontColour);
            this.LoadProperty<Boolean>(CanReduceFontToFitProperty, settings.TextBoxCanReduceToFit);
            this.LoadProperty<Int32>(BackgroundColourProperty, settings.TextBoxBackgroundColour);
            this.LoadProperty<Int32>(BorderColourProperty, settings.TextBoxBorderColour);
            this.LoadProperty<Single>(BorderThicknessProperty, settings.TextBoxBorderThickness);

            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected void Create(PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, fixtureComponent.Id);
            this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, fixtureItem.Id);
            Create(settings);
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected void Create(PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, fixtureItem.Id);
            Create(settings);
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramAnnotation>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when ids need resolving
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramFixtureItemId
            Object planogramFixtureItemId = context.ResolveId<PlanogramFixtureItem>(this.ReadProperty<Object>(PlanogramFixtureItemIdProperty));
            if (planogramFixtureItemId != null) this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, planogramFixtureItemId);

            // PlanogramFixtureAssemblyId
            Object planogramFixtureAssemblyId = context.ResolveId<PlanogramFixtureAssembly>(this.ReadProperty<Object>(PlanogramFixtureAssemblyIdProperty));
            if (planogramFixtureAssemblyId != null) this.LoadProperty<Object>(PlanogramFixtureAssemblyIdProperty, planogramFixtureAssemblyId);

            // PlanogramFixtureComponentId
            Object planogramFixtureComponentId = context.ResolveId<PlanogramFixtureComponent>(this.ReadProperty<Object>(PlanogramFixtureComponentIdProperty));
            if (planogramFixtureComponentId != null) this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, planogramFixtureComponentId);

            // PlanogramAssemblyComponentId
            Object planogramAssemblyComponentId = context.ResolveId<PlanogramAssemblyComponent>(this.ReadProperty<Object>(PlanogramAssemblyComponentIdProperty));
            if (planogramAssemblyComponentId != null) this.LoadProperty<Object>(PlanogramAssemblyComponentIdProperty, planogramAssemblyComponentId);

            // PlanogramSubComponentId
            Object planogramSubComponentId = context.ResolveId<PlanogramSubComponent>(this.ReadProperty<Object>(PlanogramSubComponentIdProperty));
            if (planogramSubComponentId != null) this.LoadProperty<Object>(PlanogramSubComponentIdProperty, planogramSubComponentId);

            // PlanogramPositionId
            Object planogramPositionId = context.ResolveId<PlanogramPosition>(this.ReadProperty<Object>(PlanogramPositionIdProperty));
            if (planogramPositionId != null) this.LoadProperty<Object>(PlanogramPositionIdProperty, planogramPositionId);
        }

        /// <summary>
        /// Returns the fixture item that this annotation is associated with.
        /// </summary>
        public PlanogramFixtureItem GetPlanogramFixtureItem()
        {
            if (this.PlanogramFixtureItemId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;

            return planogram.FixtureItems.FindById(this.PlanogramFixtureItemId);
        }

        /// <summary>
        /// Returns the fixture assembly that this annotation is associated with.
        /// </summary>
        public PlanogramFixtureAssembly GetPlanogramFixtureAssembly()
        {
            if (this.PlanogramFixtureAssemblyId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;

            return planogram.GetPlanogramFixtureAssembly(this.PlanogramFixtureAssemblyId);
        }

        /// <summary>
        /// Returns the assembly component that this annotation is associated with.
        /// </summary>
        public PlanogramAssemblyComponent GetPlanogramAssemblyComponent()
        {
            if (this.PlanogramAssemblyComponentId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;

            return planogram.GetPlanogramAssemblyComponent(this.PlanogramAssemblyComponentId);
        }

        /// <summary>
        /// Returns the fixture component that this annotation is associated with.
        /// </summary>
        public PlanogramFixtureComponent GetPlanogramFixtureComponent()
        {
            if (this.PlanogramFixtureComponentId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;

            return planogram.GetPlanogramFixtureComponent(this.PlanogramFixtureComponentId);
        }

        /// <summary>
        /// Returns the planogram subcomponent linked to this annotation.
        /// </summary>
        public PlanogramSubComponent GetPlanogramSubComponent()
        {
            if (this.PlanogramSubComponentId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.GetPlanogramSubComponent(this.PlanogramSubComponentId);
        }

        /// <summary>
        /// Returns the planogram subcomponent linked to this annotation.
        /// </summary>
        public PlanogramPosition GetPlanogramPosition()
        {
            if (this.PlanogramPositionId == null) return null;
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.Positions.FindById(this.PlanogramPositionId);
        }

        #region Placement Related

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// </summary>
        /// <param name="subComponentPlacement">the placement.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTransform(Boolean includeLocal)
        {
            PlanogramFixtureItem fixtureItem = GetPlanogramFixtureItem();
            PlanogramFixtureAssembly fixtureAssembly = GetPlanogramFixtureAssembly();
            PlanogramAssemblyComponent assemblyComponent = GetPlanogramAssemblyComponent();
            PlanogramFixtureComponent fixtureComponent = GetPlanogramFixtureComponent();
            PlanogramSubComponent subComponent = GetPlanogramSubComponent();
            PlanogramPosition position = GetPlanogramPosition();


            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //Annotation
            if (includeLocal)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                    (this.X.HasValue)? this.X.Value : 0, 
                    (this.Y.HasValue)?this.Y.Value : 0,
                    (this.Z.HasValue) ? this.Z.Value : 0, 
                        0, 0, 0));
            }

            //Position
            if (position != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        position.X, position.Y, position.Z,
                        position.Angle, position.Slope, position.Roll));
            }


            //SubComponent
            if (subComponent != null)
            {
                relativeTransform.Append(
                   MatrixValue.CreateTransformationMatrix(
                       subComponent.X, subComponent.Y, subComponent.Z, 
                       subComponent.Angle, subComponent.Slope, subComponent.Roll));
            }

            if (fixtureComponent != null)
            {
                //Fixture Component
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureComponent.X, fixtureComponent.Y, fixtureComponent.Z,
                        fixtureComponent.Angle, fixtureComponent.Slope, fixtureComponent.Roll));

            }
            else if (assemblyComponent != null)
            {
                //Assembly Component
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        assemblyComponent.X, assemblyComponent.Y, assemblyComponent.Z,
                        assemblyComponent.Angle, assemblyComponent.Slope, assemblyComponent.Roll));

                //Assembly
                if (fixtureAssembly != null)
                {
                    relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureAssembly.X, fixtureAssembly.Y, fixtureAssembly.Z,
                        fixtureAssembly.Angle, fixtureAssembly.Slope, fixtureAssembly.Roll));
                }

            }

            //Fixture
            if (fixtureItem != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixtureItem.Angle, fixtureItem.Slope, fixtureItem.Roll));
            }


            return relativeTransform;
        }


        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates()
        {
            //return out if the coordinates have not been set for this.
            if (!this.X.HasValue || !this.Y.HasValue || !this.Z.HasValue) return new PointValue();
            
            //return out if we are not attached to a plan
            Planogram plan = this.Parent;
            if (plan == null) return new PointValue();

            PointValue localCoordinates = new PointValue(this.X.Value, this.Y.Value, this.Z.Value);

            //get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(/*incLocal*/false);

            //trasnform and return.
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate)
        {
            //return out if we are not attached to a plan
            Planogram plan = this.Parent;
            if (plan == null) return;

            //get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(/*incLocal*/false);

            //invert it.
            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);

            this.X = newLocalCoordinate.X;
            this.Y = newLocalCoordinate.Y;
            this.Z = newLocalCoordinate.Z;
        }

        #endregion


        /// <summary>
        /// Associates this annotation with the given fixture item and fixture component.
        /// </summary>
        public void Associate(PlanogramFixtureItem fixtureItem)
        {
            this.PlanogramFixtureItemId = fixtureItem.Id;
            this.PlanogramFixtureComponentId = null;
            this.PlanogramFixtureAssemblyId = null;
            this.PlanogramAssemblyComponentId = null;
            this.PlanogramPositionId = null;
            this.PlanogramSubComponentId = null;
        }

        /// <summary>
        /// Associates this annotation with the given fixture item and fixture component.
        /// </summary>
        public void Associate(PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent)
        {
            this.PlanogramFixtureItemId = fixtureItem.Id;
            this.PlanogramFixtureComponentId = fixtureComponent.Id;
            this.PlanogramFixtureAssemblyId = null;
            this.PlanogramAssemblyComponentId = null;
            this.PlanogramPositionId = null;
            this.PlanogramSubComponentId = null;
        }

        /// <summary>
        /// Returns true if this annotation belongs to the given items
        /// </summary>
        public Boolean IsAssociated(PlanogramFixtureItem fixtureItem)
        {
            if (!Object.Equals(this.PlanogramFixtureItemId, fixtureItem.Id)) return false;
            if (this.PlanogramFixtureComponentId != null) return false;
            if (this.PlanogramFixtureAssemblyId != null) return false;
            if (this.PlanogramAssemblyComponentId != null) return false;
            if (this.PlanogramPositionId != null) return false;
            if (this.PlanogramSubComponentId != null) return false;
            return true;
        }

        /// <summary>
        /// Returns true if this annotation belongs to the given items
        /// </summary>
        public Boolean IsAssociated(PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent)
        {
            if (!Object.Equals(this.PlanogramFixtureComponentId, fixtureComponent.Id)) return false;
            if (!Object.Equals(this.PlanogramFixtureItemId, fixtureItem.Id)) return false;
            if (this.PlanogramFixtureAssemblyId != null) return false;
            if (this.PlanogramAssemblyComponentId != null) return false;
            if (this.PlanogramPositionId != null) return false;
            if (this.PlanogramSubComponentId != null) return false;
            return true;
        }

        /// <summary>
        /// Returns true if this annotation belongs to the given items
        /// </summary>
        public Boolean IsAssociated(PlanogramFixtureItem fixtureItem, PlanogramFixtureComponent fixtureComponent, PlanogramSubComponent subComponent)
        {
            if (!Object.Equals(this.PlanogramFixtureComponentId, fixtureComponent.Id)) return false;
            if (!Object.Equals(this.PlanogramFixtureItemId, fixtureItem.Id)) return false;
            if (!Object.Equals(this.PlanogramSubComponentId, subComponent.Id)) return false;
            if (this.PlanogramFixtureAssemblyId != null) return false;
            if (this.PlanogramAssemblyComponentId != null) return false;
            if (this.PlanogramPositionId != null) return false;
            
            return true;
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(PlanogramAnnotation);
            String typeFriendly = PlanogramAnnotation.FriendlyName;

            String detailsGroup = Message.PlanogramProduct_PropertyGroup_Details;


            //WorldX
            ObjectFieldInfo worldX = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            worldX.PropertyName = WorldXFieldName;
            yield return worldX;

            //WorldY
            ObjectFieldInfo worldY = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            worldY.PropertyName = WorldYFieldName;
            yield return worldY;

            //WorldZ
            ObjectFieldInfo worldZ = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);
            worldZ.PropertyName = WorldZFieldName;
            yield return worldZ;

          

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TextProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BackgroundColourProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BorderColourProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FontColourProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FontNameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FontSizeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanReduceFontToFitProperty, detailsGroup);
        }

        #endregion
    }
}