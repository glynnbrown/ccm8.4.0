﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created

#endregion

#endregion

using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramConsumerDecisionTreeLevelList Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeLevelList : ModelList<PlanogramConsumerDecisionTreeLevelList, PlanogramConsumerDecisionTreeLevel>
    {
        #region Parent

        /// <summary>
        /// Returns the parent product level
        /// </summary>
        public PlanogramConsumerDecisionTreeLevel ParentLevel
        {
            get { return base.Parent as PlanogramConsumerDecisionTreeLevel; }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static PlanogramConsumerDecisionTreeLevelList NewPlanogramConsumerDecisionTreeLevelList()
        {
            PlanogramConsumerDecisionTreeLevelList item = new PlanogramConsumerDecisionTreeLevelList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
