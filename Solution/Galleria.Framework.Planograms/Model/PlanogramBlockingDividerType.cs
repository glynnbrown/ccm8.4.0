﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the types of blocking divider
    /// </summary>
    public enum PlanogramBlockingDividerType
    {
        Vertical,
        Horizontal
    }


}
