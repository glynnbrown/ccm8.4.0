﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#region Version History: (CCM 8.1.0)

// V8-30101 : A.Probyn ~ Updated with language resource references.

#endregion


#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Types of Y (vertical) renumber strategies.
    /// </summary>
    [Serializable]
    public enum PlanogramRenumberingStrategyYRenumberType
    {
        TopToBottom,
        BottomToTop
    }

    public static class PlanogramRenumberingStrategyYRenumberTypeHelper
    {
        /// <summary>
        ///     Maps each value to its friendly name.
        /// </summary>
        public static readonly Dictionary<PlanogramRenumberingStrategyYRenumberType, String> FriendlyNames =
            new Dictionary<PlanogramRenumberingStrategyYRenumberType, String>
            {
                {PlanogramRenumberingStrategyYRenumberType.TopToBottom,  Message.Enum_PlanogramRenumberingStrategyYRenumberType_TopToBottom},
                {PlanogramRenumberingStrategyYRenumberType.BottomToTop, Message.Enum_PlanogramRenumberingStrategyYRenumberType_BottomToTop}
            };
    }
}
