﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonFieldValueList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonFieldValueList() { }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonFieldValueList"/>.
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramComparisonFieldValueDal>())
            {
                IEnumerable<PlanogramComparisonFieldValueDto> dtoList = dal.FetchByPlanogramComparisonItemId(criteria.ParentId);
                foreach (PlanogramComparisonFieldValueDto dto in dtoList)
                {
                    Add(PlanogramComparisonFieldValue.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}
