﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24953 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;


namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available weight unit of measure types
    /// </summary>
    public enum PlanogramWeightUnitOfMeasureType
    {
        Unknown = 0,
        Kilograms = 1,
        Pounds = 2
    }

    /// <summary>
    /// PlanogramWeightUnitOfMeasureType Helper Class
    /// </summary>
    public static class PlanogramWeightUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramWeightUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<PlanogramWeightUnitOfMeasureType, String>()
            {
                {PlanogramWeightUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramWeightUnitOfMeasureType.Kilograms, Message.Enum_PlanogramWeightUnitOfMeasureType_Kilograms},
                {PlanogramWeightUnitOfMeasureType.Pounds, Message.Enum_PlanogramWeightUnitOfMeasureType_Pounds},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramWeightUnitOfMeasureType, String> Abbreviations =
            new Dictionary<PlanogramWeightUnitOfMeasureType, String>()
            {
                {PlanogramWeightUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramWeightUnitOfMeasureType.Kilograms, Message.Enum_PlanogramWeightUnitOfMeasureType_Kilograms_Abbrev},
                {PlanogramWeightUnitOfMeasureType.Pounds, Message.Enum_PlanogramWeightUnitOfMeasureType_Pounds_Abbrev},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramWeightUnitOfMeasureType, String> AvailableValues =
            new Dictionary<PlanogramWeightUnitOfMeasureType, String>()
            {
                //{PlanogramWeightUnitOfMeasureType.Unknown, String.Empty},//removed so that this isn't selectable in ui.
                {PlanogramWeightUnitOfMeasureType.Kilograms, Message.Enum_PlanogramWeightUnitOfMeasureType_Kilograms},
                {PlanogramWeightUnitOfMeasureType.Pounds, Message.Enum_PlanogramWeightUnitOfMeasureType_Pounds},
            };
    
    }
}
