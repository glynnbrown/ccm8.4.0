﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
// V8-26773 : N.Foster
//  Added common performance source interfaces
#endregion

#region Version History: CCM811

// V8-28911 : A.Silva
//  Added OnChildChanged to ensure updates the children update the validity state of the Parent.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Core;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of Planogram Performance Metrics, contained within a Planogram Performance item.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPerformanceMetricList : ModelList<PlanogramPerformanceMetricList, PlanogramPerformanceMetric>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramPerformance Parent
        {
            get { return (PlanogramPerformance)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceMetricList NewPlanogramPerformanceMetricList()
        {
            var item = new PlanogramPerformanceMetricList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the parent's business rules are checked when the collection is changed.
        /// </summary>
        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            Parent.CheckBusinessRules(PlanogramPerformance.MetricsProperty);
            base.OnBulkCollectionChanged(e);
        }

        /// <summary>
        ///     Whenever a child object is changed, if the property may affect validity, recheck business rules.
        /// </summary>
        protected override void OnChildChanged(ChildChangedEventArgs e)
        {
            if (!String.Equals(e.PropertyChangedArgs.PropertyName, PlanogramPerformanceMetric.NameProperty.Name) && !String.Equals(e.PropertyChangedArgs.PropertyName, PlanogramPerformanceMetric.MetricIdProperty.Name)) return;
            Parent.CheckBusinessRules(PlanogramPerformance.MetricsProperty);
            base.OnChildChanged(e);
        }

        /// <summary>
        /// Adds a number of metrics to this collection
        /// </summary>
        public void AddRange(IEnumerable<IPlanogramPerformanceMetric> range)
        {
            List<PlanogramPerformanceMetric> metrics = new List<PlanogramPerformanceMetric>();
            foreach (IPlanogramPerformanceMetric metric in range)
            {
                metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(metric));
            }
            base.AddRange(metrics);
        }
        #endregion
    }
}
