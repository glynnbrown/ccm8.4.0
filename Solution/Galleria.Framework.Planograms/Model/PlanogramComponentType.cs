﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramComponentType
    {
        Custom= 0, 
        Shelf = 1, 
        Bar = 2,
        Peg = 3, 
        Chest = 4,
        Backboard = 5,
        Panel = 6,
        Base = 7,
        Rod = 8,
        ClipStrip = 9,
        Pallet = 10,
        SlotWall = 11
    }

    /// <summary>
    /// PlanogramComponentType Helper Class
    /// </summary>
    public static class PlanogramComponentTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramComponentType, String> FriendlyNames =
            new Dictionary<PlanogramComponentType, String>()
            {
                {PlanogramComponentType.Custom, Message.Enum_PlanogramComponentType_Custom},
                {PlanogramComponentType.Shelf, Message.Enum_PlanogramComponentType_Shelf},
                {PlanogramComponentType.Bar, Message.Enum_PlanogramComponentType_Bar},
                {PlanogramComponentType.Peg, Message.Enum_PlanogramComponentType_Peg},
                {PlanogramComponentType.Chest, Message.Enum_PlanogramComponentType_Chest},
                {PlanogramComponentType.Backboard, Message.Enum_PlanogramComponentType_Backboard},
                {PlanogramComponentType.Panel, Message.Enum_PlanogramComponentType_Panel},
                {PlanogramComponentType.Base, Message.Enum_PlanogramComponentType_Base},
                {PlanogramComponentType.Rod, Message.Enum_PlanogramComponentType_Rod},
                {PlanogramComponentType.ClipStrip, Message.Enum_PlanogramComponentType_ClipStrip},
                {PlanogramComponentType.Pallet, Message.Enum_PlanogramComponentType_Pallet},
                {PlanogramComponentType.SlotWall, Message.Enum_PlanogramComponentType_Slotwall},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramComponentType, String> FriendlyDescriptions =
            new Dictionary<PlanogramComponentType, String>()
            {
                {PlanogramComponentType.Custom, Message.Enum_PlanogramComponentType_Custom},
                {PlanogramComponentType.Shelf, Message.Enum_PlanogramComponentType_Shelf},
                {PlanogramComponentType.Bar, Message.Enum_PlanogramComponentType_Bar},
                {PlanogramComponentType.Peg, Message.Enum_PlanogramComponentType_Peg},
                {PlanogramComponentType.Chest, Message.Enum_PlanogramComponentType_Chest},
                {PlanogramComponentType.Backboard, Message.Enum_PlanogramComponentType_Backboard},
                {PlanogramComponentType.Panel, Message.Enum_PlanogramComponentType_Panel},
                {PlanogramComponentType.Base, Message.Enum_PlanogramComponentType_Base},
                {PlanogramComponentType.Rod, Message.Enum_PlanogramComponentType_Rod},
                {PlanogramComponentType.ClipStrip, Message.Enum_PlanogramComponentType_ClipStrip},
                {PlanogramComponentType.Pallet, Message.Enum_PlanogramComponentType_Pallet},
                {PlanogramComponentType.SlotWall, Message.Enum_PlanogramComponentType_Slotwall},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramComponentType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramComponentType>()
            {
                {Message.Enum_PlanogramComponentType_Custom.ToLowerInvariant(), PlanogramComponentType.Custom},
                {Message.Enum_PlanogramComponentType_Shelf.ToLowerInvariant(), PlanogramComponentType.Shelf},
                {Message.Enum_PlanogramComponentType_Bar.ToLowerInvariant(), PlanogramComponentType.Bar},
                {Message.Enum_PlanogramComponentType_Peg.ToLowerInvariant(), PlanogramComponentType.Peg},
                {Message.Enum_PlanogramComponentType_Chest.ToLowerInvariant(), PlanogramComponentType.Chest},
                {Message.Enum_PlanogramComponentType_Backboard.ToLowerInvariant(), PlanogramComponentType.Backboard},
                {Message.Enum_PlanogramComponentType_Panel.ToLowerInvariant(), PlanogramComponentType.Panel},
                {Message.Enum_PlanogramComponentType_Base.ToLowerInvariant(), PlanogramComponentType.Base},
                {Message.Enum_PlanogramComponentType_Rod.ToLowerInvariant(), PlanogramComponentType.Rod},
                {Message.Enum_PlanogramComponentType_ClipStrip.ToLowerInvariant(), PlanogramComponentType.ClipStrip},
                {Message.Enum_PlanogramComponentType_Pallet, PlanogramComponentType.Pallet},
                {Message.Enum_PlanogramComponentType_Slotwall, PlanogramComponentType.SlotWall},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramComponentType? PlanogramComponentTypeGetEnum(String friendlyName)
        {
            PlanogramComponentType? returnValue = null;
            PlanogramComponentType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
