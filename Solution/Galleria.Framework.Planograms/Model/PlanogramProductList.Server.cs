﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added batch fetch of custom attributes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramProductList
    {
        #region Constructor
        private PlanogramProductList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all products for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in the order they will be fetched
                    batchContext.Register<PlanogramProductDto, IPlanogramProductDal>((context, dal) => dal.FetchByPlanogramId(criteria.ParentId));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataPlanogramParentType.PlanogramProduct, context.Dtos<PlanogramProductDto>().Select(dto => dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the planogram products
                    foreach (PlanogramProductDto planogramProductDto in batchContext.Dtos<PlanogramProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(planogramProductDto.Id, out customAttributeDataDto);
                        this.Add(PlanogramProduct.Fetch(dalContext, planogramProductDto, customAttributeDataDto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
