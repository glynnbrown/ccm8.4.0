﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva
//      Created.
// V8-27562 : A.Silva
//      Added LoadFrom() method to load data from any instance implementing IPlanogramRenumberingStrategy.
// V8-27624 : A.Silva
//      Amended how priorities are applied, and how direction is determined for positions (on Y and Z axis).
// V8-28078 : A.Probyn
//      Corrected GetComponentCoordinateValueForPriority to compare object ids using the equals method not ==. Parent objects
//      were not correctly being located.
#endregion

#region Version History: CCM801

// V8-28878 : A.Silva
//      Amended name of RestartComponentRenumberingPerBayProperty to make it consistent with the database field name.

#endregion

#region Version History: CCM810
// V8-29818 : L.Ineson
//  Amended defaults.
// V8-30101 : A.Probyn
//  Switched logic in IsDefaultBayComponentRenumberingStrategyForPriority and IsDefaultPositionRenumberingStrategyForPriority for
//  Y axis ordering.
#endregion

#region Version History: CCM820
// V8-30193 : L.Ineson
//  Added IsEnabled property
#endregion

#region Version History: CCM830

// V8-32145 : A.Silva
//  Amended RenumberBays.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     This model represents a renumbering strategy within a planogram.
    ///     This is a child object for <see cref="Planogram" />.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramRenumberingStrategy : ModelObject<PlanogramRenumberingStrategy>,
        IPlanogramRenumberingStrategy
    {
        #region  Fields

        private Int16 startPositionSequenceNumber;

        #endregion 

        #region Constants

        private const PlanogramRenumberingStrategyXRenumberType DefaultXStrategyType =
            PlanogramRenumberingStrategyXRenumberType.LeftToRight;

        private const PlanogramRenumberingStrategyYRenumberType DefaultYStrategyType =
            PlanogramRenumberingStrategyYRenumberType.BottomToTop;

        private const PlanogramRenumberingStrategyZRenumberType DefaultZStrategyType =
            PlanogramRenumberingStrategyZRenumberType.FrontToBack;

        private const Byte FirstRenumberingPriority = 1;
        private const Byte SecondRenumberingPriority = 2;
        private const Byte ThirdRenumberingPriority = 3;

        #endregion

        #region Properties

        #region Parent

        /// <summary>
        ///     Returns a reference to the containing <see cref="Planogram" />.
        /// </summary>
        public new Planogram Parent
        {
            get { return base.Parent as Planogram; }
        }

        #endregion

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name, "Name");

        /// <summary>
        ///     Gets or sets the value for the <see cref="Name" /> property.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region BayComponentXRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentXRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentXRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentXRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentXRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentXRenumberStrategyPriority
        {
            get { return GetProperty(BayComponentXRenumberStrategyPriorityProperty); }
            set { SetProperty(BayComponentXRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentYRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentYRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentYRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentYRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentYRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentYRenumberStrategyPriority
        {
            get { return GetProperty(BayComponentYRenumberStrategyPriorityProperty); }
            set { SetProperty(BayComponentYRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentZRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentZRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentZRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentZRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentZRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentZRenumberStrategyPriority
        {
            get { return GetProperty(BayComponentZRenumberStrategyPriorityProperty); }
            set { SetProperty(BayComponentZRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionXRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionXRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionXRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionXRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionXRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionXRenumberStrategyPriority
        {
            get { return GetProperty(PositionXRenumberStrategyPriorityProperty); }
            set { SetProperty(PositionXRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionYRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionYRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionYRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionYRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionYRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionYRenumberStrategyPriority
        {
            get { return GetProperty(PositionYRenumberStrategyPriorityProperty); }
            set { SetProperty(PositionYRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionZRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionZRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionZRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionZRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionZRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionZRenumberStrategyPriority
        {
            get { return GetProperty(PositionZRenumberStrategyPriorityProperty); }
            set { SetProperty(PositionZRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentXRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentXRenumberStrategy" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramRenumberingStrategyXRenumberType>
            BayComponentXRenumberStrategyProperty
                =
                RegisterModelProperty<PlanogramRenumberingStrategyXRenumberType>(o => o.BayComponentXRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyXRenumberType BayComponentXRenumberStrategy
        {
            get { return GetProperty(BayComponentXRenumberStrategyProperty); }
            set { SetProperty(BayComponentXRenumberStrategyProperty, value); }
        }

        #endregion

        #region BayComponentYRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentYRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyYRenumberType>
            BayComponentYRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyYRenumberType>(o => o.BayComponentYRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyYRenumberType BayComponentYRenumberStrategy
        {
            get { return GetProperty(BayComponentYRenumberStrategyProperty); }
            set { SetProperty(BayComponentYRenumberStrategyProperty, value); }
        }

        #endregion

        #region BayComponentZRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentZRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyZRenumberType>
            BayComponentZRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyZRenumberType>(o => o.BayComponentZRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyZRenumberType BayComponentZRenumberStrategy
        {
            get { return GetProperty(BayComponentZRenumberStrategyProperty); }
            set { SetProperty(BayComponentZRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionXRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionXRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyXRenumberType>
            PositionXRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyXRenumberType>(o => o.PositionXRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyXRenumberType PositionXRenumberStrategy
        {
            get { return GetProperty(PositionXRenumberStrategyProperty); }
            set { SetProperty(PositionXRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionYRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionYRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyYRenumberType>
            PositionYRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyYRenumberType>(o => o.PositionYRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyYRenumberType PositionYRenumberStrategy
        {
            get { return GetProperty(PositionYRenumberStrategyProperty); }
            set { SetProperty(PositionYRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionZRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionZRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyZRenumberType>
            PositionZRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyZRenumberType>(o => o.PositionZRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyZRenumberType PositionZRenumberStrategy
        {
            get { return GetProperty(PositionZRenumberStrategyProperty); }
            set { SetProperty(PositionZRenumberStrategyProperty, value); }
        }

        #endregion

        #region RestartComponentRenumberingPerBay

        /// <summary>
        ///     Metadata for the <see cref="RestartComponentRenumberingPerBay" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> RestartComponentRenumberingPerBayProperty =
            RegisterModelProperty<Boolean>(o => o.RestartComponentRenumberingPerBay);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each bay.
        /// </summary>
        public Boolean RestartComponentRenumberingPerBay
        {
            get { return GetProperty(RestartComponentRenumberingPerBayProperty); }
            set { SetProperty(RestartComponentRenumberingPerBayProperty, value); }
        }

        #endregion

        #region RestartComponentRenumberingPerComponentType

        /// <summary>
        ///     Metadata for the <see cref="RestartComponentRenumberingPerComponentType" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean>
            RestartComponentRenumberingPerComponentTypeProperty =
                RegisterModelProperty<Boolean>(o => o.RestartComponentRenumberingPerComponentType);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each component type.
        /// </summary>
        public Boolean RestartComponentRenumberingPerComponentType
        {
            get { return this.GetProperty(RestartComponentRenumberingPerComponentTypeProperty); }
            set { this.SetProperty(RestartComponentRenumberingPerComponentTypeProperty, value); }
        }

        #endregion

        #region IgnoreNonMerchandisingComponents

        /// <summary>
        ///     Metadata for the <see cref="IgnoreNonMerchandisingComponents" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IgnoreNonMerchandisingComponentsProperty =
            RegisterModelProperty<Boolean>(o => o.IgnoreNonMerchandisingComponents);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering ignores non merchandising ones.
        /// </summary>
        public Boolean IgnoreNonMerchandisingComponents
        {
            get { return GetProperty(IgnoreNonMerchandisingComponentsProperty); }
            set { SetProperty(IgnoreNonMerchandisingComponentsProperty, value); }
        }

        #endregion

        #region RestartPositionRenumberingPerComponent

        /// <summary>
        ///     Metadata for the <see cref="RestartPositionRenumberingPerComponent" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> RestartPositionRenumberingPerComponentProperty =
            RegisterModelProperty<Boolean>(o => o.RestartPositionRenumberingPerComponent);

        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each component.
        /// </summary>
        public Boolean RestartPositionRenumberingPerComponent
        {
            get { return GetProperty(RestartPositionRenumberingPerComponentProperty); }
            set { SetProperty(RestartPositionRenumberingPerComponentProperty, value); }
        }

        #endregion

        #region RestartPositionRenumberingPerBay

        /// <summary>
        ///     Metadata for the <see cref="RestartPositionRenumberingPerBay" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> RestartPositionRenumberingPerBayProperty =
            RegisterModelProperty<Boolean>(o => o.RestartPositionRenumberingPerBay);

        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each Bay.
        /// </summary>
        public Boolean RestartPositionRenumberingPerBay
        {
            get { return this.GetProperty(RestartPositionRenumberingPerBayProperty); }
            set { this.SetProperty(RestartPositionRenumberingPerBayProperty, value); }
        }

        #endregion

        #region UniqueNumberMultiPositionProductsPerComponent

        /// <summary>
        ///     Metadata for the <see cref="UniqueNumberMultiPositionProductsPerComponent" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> UniqueNumberMultiPositionProductsPerComponentProperty =
            RegisterModelProperty<Boolean>(o => o.UniqueNumberMultiPositionProductsPerComponent);

        /// <summary>
        ///     Gets or sets whether different positions for the same product get unique sequence numbers.
        /// </summary>
        public Boolean UniqueNumberMultiPositionProductsPerComponent
        {
            get { return GetProperty(UniqueNumberMultiPositionProductsPerComponentProperty); }
            set { SetProperty(UniqueNumberMultiPositionProductsPerComponentProperty, value); }
        }

        #endregion

        #region ExceptAdjacentPositions

        /// <summary>
        ///     Metadata for the <see cref="ExceptAdjacentPositions" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ExceptAdjacentPositionsProperty =
            RegisterModelProperty<Boolean>(o => o.ExceptAdjacentPositions);

        /// <summary>
        ///     Gets or sets whether different positions for the same product, but adjacent, get the same sequence number when
        ///     unique sequence numbers per position is in place.
        /// </summary>
        public Boolean ExceptAdjacentPositions
        {
            get { return GetProperty(ExceptAdjacentPositionsProperty); }
            set { SetProperty(ExceptAdjacentPositionsProperty, value); }
        }

        #endregion

        #region IsEnabled

        /// <summary>
        ///   IsEnabled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsEnabledProperty =
            RegisterModelProperty<Boolean>(o => o.IsEnabled, Message.PlanogramRenumberingStrategy_IsEnabled);

        /// <summary>
        ///    Gets/Sets whether this renumbering strategy is in use. 
        ///    If false, the user should be able to manually enter sequence numbers.
        /// </summary>
        public Boolean IsEnabled
        {
            get { return GetProperty<Boolean>(IsEnabledProperty); }
            set { SetProperty<Boolean>(IsEnabledProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MinValue<Byte>(BayComponentXRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(BayComponentXRenumberStrategyPriorityProperty, 3));
            BusinessRules.AddRule(new MinValue<Byte>(BayComponentYRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(BayComponentYRenumberStrategyPriorityProperty, 3));
            BusinessRules.AddRule(new MinValue<Byte>(BayComponentZRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(BayComponentZRenumberStrategyPriorityProperty, 3));
            BusinessRules.AddRule(new MinValue<Byte>(PositionXRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(PositionXRenumberStrategyPriorityProperty, 3));
            BusinessRules.AddRule(new MinValue<Byte>(PositionYRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(PositionYRenumberStrategyPriorityProperty, 3));
            BusinessRules.AddRule(new MinValue<Byte>(PositionZRenumberStrategyPriorityProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(PositionZRenumberStrategyPriorityProperty, 3));
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }

        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria

        /// <summary>
        ///     Type to contain the criteria used to fetch an instance of <see cref="PlanogramRenumberingStrategy" />.
        /// </summary>
        [Serializable]
        public sealed class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName

            /// <summary>
            ///     Metadata for the <see cref="DalFactoryName" /> property.
            /// </summary>
            private static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(o => o.DalFactoryName);

            /// <summary>
            ///     Gets or sets the value for the <see cref="DalFactoryName" /> property.
            /// </summary>
            public String DalFactoryName
            {
                get { return ReadProperty(DalFactoryNameProperty); }
            }

            #endregion

            #region PlanogramId

            /// <summary>
            ///     Metadata for the <see cref="PlanogramId" /> property.
            /// </summary>
            private static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(o => o.PlanogramId);

            /// <summary>
            ///     Gets or sets the value for the <see cref="PlanogramId" /> property.
            /// </summary>
            public Object PlanogramId
            {
                get { return ReadProperty(PlanogramIdProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes a new instance of <see cref="FetchByPlanogramIdCriteria" />.
            /// </summary>
            /// <param name="dalFactoryName">Name of the dal factory to use the criteria on.</param>
            /// <param name="planogramId">Unique identifier (either a record Id or a file name) for the planogram.</param>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                LoadProperty(DalFactoryNameProperty, dalFactoryName);
                LoadProperty(PlanogramIdProperty, planogramId);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramRenumberingStrategy" />.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramRenumberingStrategy" /> correctly initialized.</returns>
        public static PlanogramRenumberingStrategy NewPlanogramRenumberingStrategy()
        {
            var item = new PlanogramRenumberingStrategy();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating new instances of <see cref="PlanogramRenumberingStrategy" />.
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(NameProperty, Message.PlanogramRenumberingStrategy_DefaultName);
            this.LoadProperty<Byte>(BayComponentXRenumberStrategyPriorityProperty, 1);
            this.LoadProperty<Byte>(BayComponentYRenumberStrategyPriorityProperty, 2);
            this.LoadProperty<Byte>(BayComponentZRenumberStrategyPriorityProperty, 3);
            this.LoadProperty<Byte>(PositionXRenumberStrategyPriorityProperty, 1);
            this.LoadProperty<Byte>(PositionYRenumberStrategyPriorityProperty, 2);
            this.LoadProperty<Byte>(PositionZRenumberStrategyPriorityProperty, 3);
            this.LoadProperty(BayComponentXRenumberStrategyProperty, DefaultXStrategyType);
            this.LoadProperty(BayComponentYRenumberStrategyProperty, DefaultYStrategyType);
            this.LoadProperty(BayComponentZRenumberStrategyProperty, DefaultZStrategyType);
            this.LoadProperty(PositionXRenumberStrategyProperty, DefaultXStrategyType);
            this.LoadProperty(PositionYRenumberStrategyProperty, DefaultYStrategyType);
            this.LoadProperty(PositionZRenumberStrategyProperty, DefaultZStrategyType);
            this.LoadProperty(RestartComponentRenumberingPerBayProperty, true);
            this.LoadProperty(IgnoreNonMerchandisingComponentsProperty, true);
            this.LoadProperty(RestartPositionRenumberingPerComponentProperty, true);
            this.LoadProperty(UniqueNumberMultiPositionProductsPerComponentProperty, false);
            this.LoadProperty(ExceptAdjacentPositionsProperty, false);
            this.LoadProperty<Boolean>(IsEnabledProperty, true);
            this.LoadProperty(RestartPositionRenumberingPerBayProperty, true);
            this.LoadProperty(RestartComponentRenumberingPerComponentTypeProperty, false);

            MarkAsChild();
            base.Create();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramRenumberingStrategy>(oldId, newId);
        }

        #endregion

        #endregion

        #region Methods

        #region Renumbering

        /// <summary>
        ///     Orders the bays in a planogram.
        /// </summary>
        /// <param name="forced">Whether to force the renumbering even if the renumbering strategy is off.</param>
        /// <returns></returns>
        public void RenumberBays(Boolean forced = false)
        {
            //  Do not execute unless it is a forced renumbering or the strategy is enabled.
            if (!forced && !IsEnabled) return;

            IEnumerable<PlanogramFixtureItem> bays = GetPlanogramBays();

            // Reorder in reverse, so that we get the desired sorting.
            IEnumerable<PlanogramFixtureItem> orderedBays = OrderBaysByPriority(bays);
            Int16 currentSequenceNumber = 1;
            Int16 startComponentSequenceNumber = 1;
            startPositionSequenceNumber = 1;

            if (RestartComponentRenumberingPerComponentType)
            {
                RenumberComponentsPerType(orderedBays);
            }else
            { 
                foreach (PlanogramFixtureItem bay in orderedBays)
                {
                    if (RestartComponentRenumberingPerBay) startComponentSequenceNumber = 1;
                    bay.BaySequenceNumber = currentSequenceNumber++;
                    startComponentSequenceNumber = RenumberComponents(bay, startComponentSequenceNumber);
                }
            }
        }
        /// <summary>
        /// Renumber Components per type
        /// </summary>
        /// <param name="bays"></param>
        public void RenumberComponentsPerType(IEnumerable<PlanogramFixtureItem> bays)
        {

            Int16 CustomComponentSequenceNumber = 1;
            Int16 SelfComponentSequenceNumber = 1;
            Int16 BarComponentSequenceNumber = 1;
            Int16 PegComponentSequenceNumber = 1;
            Int16 ChestComponentSequenceNumber = 1;
            Int16 PanelComponentSequenceNumber = 1;
            Int16 RodComponentSequenceNumber = 1;
            Int16 ClipStripComponentSequenceNumber = 1;
            Int16 PalletComponentSequenceNumber = 1;
            Int16 SlotWallComponentSequenceNumber = 1;

            foreach (var bay in bays)
            {
                var bayComponents = GetBayComponents(bay);
                List<IPlanogramFixtureComponent> components = new List<IPlanogramFixtureComponent>();

                if (IgnoreNonMerchandisingComponents)
                {
                    foreach (IPlanogramFixtureComponent component in bayComponents)
                    {
                        PlanogramComponent planogramComponent = component.GetPlanogramComponent();
                        if (planogramComponent != null)
                        {
                            if (planogramComponent.IsMerchandisable)
                            {
                                components.Add(component);
                            }
                        }
                    }
                }

                var orderedComponents = OrderComponentsByPriority(components, bay, bay.GetPlanogramFixture().Assemblies);
                if (RestartPositionRenumberingPerBay) startPositionSequenceNumber = 1;

                if (RestartComponentRenumberingPerBay)
                {
                    CustomComponentSequenceNumber = 1;
                    SelfComponentSequenceNumber = 1;
                    BarComponentSequenceNumber = 1;
                    PegComponentSequenceNumber = 1;
                    ChestComponentSequenceNumber = 1;
                    PanelComponentSequenceNumber = 1;
                    RodComponentSequenceNumber = 1;
                    ClipStripComponentSequenceNumber = 1;
                    PalletComponentSequenceNumber = 1;
                    SlotWallComponentSequenceNumber = 1;
                }

                foreach (var component in orderedComponents)
                {
                    PlanogramComponent planogramComponent = component.GetPlanogramComponent();

                    switch (planogramComponent.ComponentType)
                    {
                        case PlanogramComponentType.Custom:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = CustomComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Shelf:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = SelfComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Bar:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = BarComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Peg:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = PegComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Chest:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = ChestComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Panel:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = PanelComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Rod:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = RodComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.ClipStrip:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = ClipStripComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.Pallet:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = PalletComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;

                        case PlanogramComponentType.SlotWall:
                            if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                            component.ComponentSequenceNumber = SlotWallComponentSequenceNumber++;
                            startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
                            break;
                    }
                }
            }
        }

        /// <summary>
        ///     Get the current space between the fixtures
        /// </summary>
        /// <returns>Dictionary<PlanogramFixtureItem, float> Dictionary containing the fixture and the additional space between the next fixture</returns>
        public Dictionary<PlanogramFixtureItem, float> GetFixtureSpacing()
        {
            Dictionary<PlanogramFixtureItem, float> fixtureSpacing = new Dictionary<PlanogramFixtureItem, float>();

            IEnumerable<PlanogramFixtureItem> fixtures = GetPlanogramBays();
            List<PlanogramFixtureItem> orderedFixtures = OrderBaysByPriority(fixtures).ToList();

            float fixtureSpace = 0;
            int fixtureNumberNext = 0;            
            int fixtureCount = 0;
            
            fixtureCount = orderedFixtures.Count;

            //Iterate through each fixture.
            for (int fixtureNumber = 0; fixtureNumber < fixtureCount; fixtureNumber++)
            {
                PlanogramFixtureItem fixture = orderedFixtures[fixtureNumber];
                PlanogramFixtureItem fixtureNext = null;
                   
                fixtureNumberNext = fixtureNumber + 1;
                fixtureSpace = 0;

                //Find the next fixture if one exists.
                if (fixtureNumberNext < fixtureCount)
                {                    
                    //Calculate the space for this fixture based upon the next fixture.
                    fixtureNext = orderedFixtures[fixtureNumberNext];
                    fixtureSpace = fixtureNext.X - (fixture.X + fixture.GetPlanogramFixture().Width);

                    //Only include the space if it doesn’t overlap.
                    if (fixtureSpace < 0)
                    {
                        fixtureSpace = 0;
                    }
                }

                //Add the result to the dictionary.
                fixtureSpacing.Add(fixture, fixtureSpace);
            }

            return fixtureSpacing;        
        }

        /// <summary>
        ///     Renumbers the ComponentSequenceNumber of each component according to priority and other rules defined in this instance.
        /// </summary>
        /// <param name="bay"></param>
        /// <param name="currentSequenceNumber"></param>
        private Int16 RenumberComponents(PlanogramFixtureItem bay, Int16 currentSequenceNumber)
        {
            var bayComponents = GetBayComponents(bay);


            List<IPlanogramFixtureComponent> components = new List<IPlanogramFixtureComponent>();

            if (IgnoreNonMerchandisingComponents)
            {
                foreach (IPlanogramFixtureComponent component in bayComponents)
                {
                    PlanogramComponent planogramComponent = component.GetPlanogramComponent();
                    if (planogramComponent != null)
                    {
                        if (planogramComponent.IsMerchandisable)
                        {
                            components.Add(component);
                        }
                    }
                }
            }

            if (!components.Any()) return currentSequenceNumber;

            var orderedComponents = OrderComponentsByPriority(components, bay, bay.GetPlanogramFixture().Assemblies);

            if(RestartPositionRenumberingPerBay) startPositionSequenceNumber = 1;

            foreach (var component in orderedComponents)
            {
                if (RestartPositionRenumberingPerComponent) startPositionSequenceNumber = 1;
                component.ComponentSequenceNumber = currentSequenceNumber++;
                startPositionSequenceNumber = RenumberPositions(component, startPositionSequenceNumber);
            }

            return currentSequenceNumber;
         }

        /// <summary>
        ///     Renumbers the PositionSequenceNumber of each Position according to priority and other rules defined in this instance.
        /// </summary>
        /// <param name="planogramComponent"></param>
        /// <param name="currentSequenceNumber"></param>
        private Int16 RenumberPositions(IPlanogramFixtureComponent planogramComponent, Int16 currentSequenceNumber)
        {
            var positions = GetComponentPositions(planogramComponent);
            var orderedPositions = OrderPositionsByPriority(positions);

            return UniqueNumberMultiPositionProductsPerComponent
                ? (ExceptAdjacentPositions
                    ? RenumberPositionsUniquePositionsExceptAdjacent(orderedPositions, currentSequenceNumber)
                    : RenumberPositionsUniquePositions(orderedPositions, currentSequenceNumber))
                : RenumberPositionsUniqueProducts(orderedPositions, currentSequenceNumber);
        }

        private static Int16 RenumberPositionsUniqueProducts(IEnumerable<PlanogramPosition> orderedPositions, Int16 currentSequenceNumber)
        {
            var sequencedProducts = new Dictionary<Object, Int16>();

            foreach (var position in orderedPositions)
            {
                var productId = position.PlanogramProductId;

                if (!sequencedProducts.ContainsKey(productId))
                {
                    sequencedProducts.Add(productId, currentSequenceNumber++);
                }

                position.PositionSequenceNumber = sequencedProducts[productId];
            }

            return currentSequenceNumber;
        }

        private Int16 RenumberPositionsUniquePositionsExceptAdjacent(IEnumerable<PlanogramPosition> orderedPositions, Int16 currentSequenceNumber)
        {
            PlanogramPosition lastPosition = null;

            // assign a different number per position UNLESS the positions are for adjacent products (same product, and one placed right besides the other).
            foreach (var position in orderedPositions)
            {
                position.PositionSequenceNumber = PositionsAreAdjacent(position, lastPosition) 
                    ? currentSequenceNumber 
                    : currentSequenceNumber++;

                lastPosition = position;
            }

            return currentSequenceNumber;
        }

        private static Int16 RenumberPositionsUniquePositions(IEnumerable<PlanogramPosition> orderedPositions, Int16 currentSequenceNumber)
        {
            // Just assign a different number per position.
            foreach (var position in orderedPositions)
            {
                position.PositionSequenceNumber = currentSequenceNumber++;
            }

            return currentSequenceNumber;
        }

        #endregion

        #region Helper Methods
        /// <summary>
        ///     Gets the bays from a planogram, optionally ordering them.
        /// </summary>
        /// <returns>A list of all the bays in a planogram.</returns>
        private IEnumerable<PlanogramFixtureItem> GetPlanogramBays()
        {
            var fixtureItems = Parent.FixtureItems;
            var fixtures = Parent.Fixtures;
            var items = fixtureItems.Where(item => fixtures.Any(fixture =>
                Equals(fixture.Id, item.PlanogramFixtureId))).ToList();
            return items;
        }

        /// <summary>
        ///     Gets all the components in a bay, whether directly on the fixture or as part of assemblies.
        /// </summary>
        /// <param name="bay"></param>
        /// <returns></returns>
        private static IEnumerable<IPlanogramFixtureComponent> GetBayComponents(PlanogramFixtureItem bay)
        {
            IEnumerable<IPlanogramFixtureComponent> fixtureComponents = bay.GetPlanogramFixture().Components;
            IEnumerable<IPlanogramFixtureComponent> assemblyComponents = bay.GetPlanogramFixture().Assemblies
                .SelectMany(assembly => assembly.GetPlanogramAssembly().Components);
            var components = fixtureComponents.Union(assemblyComponents);
            return components;
        }

        /// <summary>
        ///     Gets all the positions in a component.
        /// </summary>
        /// <param name="planogramComponent"></param>
        /// <returns></returns>
        private static IEnumerable<PlanogramPosition> GetComponentPositions(IPlanogramFixtureComponent planogramComponent)
        {
            var positions = planogramComponent.GetPlanogramSubComponentPlacements()
                .SelectMany(placement => placement.GetPlanogramPositions());
            return positions;
        }

        private IEnumerable<PlanogramFixtureItem> OrderBaysByPriority(IEnumerable<PlanogramFixtureItem> bays)
        {
            var orderedBays = IsDefaultBayComponentRenumberingStrategyForPriority(FirstRenumberingPriority)
                ? bays.OrderBy(item => GetBayCoordinateValueForPriority(FirstRenumberingPriority, item))
                : bays.OrderByDescending(item => GetBayCoordinateValueForPriority(FirstRenumberingPriority, item));
            orderedBays = IsDefaultBayComponentRenumberingStrategyForPriority(SecondRenumberingPriority)
                ? orderedBays.ThenBy(item => GetBayCoordinateValueForPriority(SecondRenumberingPriority, item))
                : orderedBays.ThenByDescending(item => GetBayCoordinateValueForPriority(SecondRenumberingPriority, item));
            orderedBays = IsDefaultBayComponentRenumberingStrategyForPriority(ThirdRenumberingPriority)
                ? orderedBays.ThenBy(item => GetBayCoordinateValueForPriority(ThirdRenumberingPriority, item))
                : orderedBays.ThenByDescending(item => GetBayCoordinateValueForPriority(ThirdRenumberingPriority, item));
            return orderedBays;
        }

        /// <summary>
        ///     Applies the order indicated by the priorities to the collection of components.
        /// </summary>
        /// <param name="components"></param>
        /// <param name="currentFixtureItem"></param>
        /// <param name="fixtureAssemblies"></param>
        /// <returns></returns>
        /// <remarks>This is the nitty-gritty LINQ that does the actual sorting according to priority and type of renumbering.</remarks>
        private IEnumerable<IPlanogramFixtureComponent> OrderComponentsByPriority(IEnumerable<IPlanogramFixtureComponent> components,
            PlanogramFixtureItem currentFixtureItem,
            IEnumerable<PlanogramFixtureAssembly> fixtureAssemblies)
        {
            var orderedComponents = IsDefaultBayComponentRenumberingStrategyForPriority(FirstRenumberingPriority)
                ? components.OrderBy(item => GetComponentCoordinateValueForPriority(FirstRenumberingPriority, item, currentFixtureItem, fixtureAssemblies))
                : components.OrderByDescending(item => GetComponentCoordinateValueForPriority(FirstRenumberingPriority, item, currentFixtureItem, fixtureAssemblies));
            orderedComponents = IsDefaultBayComponentRenumberingStrategyForPriority(SecondRenumberingPriority)
                ? orderedComponents.ThenBy(item => GetComponentCoordinateValueForPriority(SecondRenumberingPriority, item, currentFixtureItem, fixtureAssemblies))
                : orderedComponents.ThenByDescending(item => GetComponentCoordinateValueForPriority(SecondRenumberingPriority, item, currentFixtureItem, fixtureAssemblies));
            orderedComponents = IsDefaultBayComponentRenumberingStrategyForPriority(ThirdRenumberingPriority)
                ? orderedComponents.ThenBy(item => GetComponentCoordinateValueForPriority(ThirdRenumberingPriority, item, currentFixtureItem, fixtureAssemblies))
                : orderedComponents.ThenByDescending(item => GetComponentCoordinateValueForPriority(ThirdRenumberingPriority, item, currentFixtureItem, fixtureAssemblies));
            return orderedComponents;
        }

        /// <summary>
        ///     Applies the order indicated by the priorities to the collection of positions.
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        /// <remarks>This is the nitty-gritty LINQ that does the actual sorting according to priority and type of renumbering.</remarks>
        private IEnumerable<PlanogramPosition> OrderPositionsByPriority(IEnumerable<PlanogramPosition> positions)
        {
            var orderedPositions = IsDefaultPositionRenumberingStrategyForPriority(FirstRenumberingPriority)
                ? positions.OrderBy(item => GetPositionCoordinateValueForPriority(FirstRenumberingPriority, item))
                : positions.OrderByDescending(item => GetPositionCoordinateValueForPriority(FirstRenumberingPriority, item));
            orderedPositions = IsDefaultPositionRenumberingStrategyForPriority(SecondRenumberingPriority)
                ? orderedPositions.ThenBy(item => GetPositionCoordinateValueForPriority(SecondRenumberingPriority, item))
                : orderedPositions.ThenByDescending(item => GetPositionCoordinateValueForPriority(SecondRenumberingPriority, item));
            orderedPositions = IsDefaultPositionRenumberingStrategyForPriority(ThirdRenumberingPriority)
                ? orderedPositions.ThenBy(item => GetPositionCoordinateValueForPriority(ThirdRenumberingPriority, item))
                : orderedPositions.ThenByDescending(item => GetPositionCoordinateValueForPriority(ThirdRenumberingPriority, item));
            return orderedPositions;
        }

        /// <summary>
        ///     Inidicates whether the given <paramref name="priority" /> is set to its default value or not.
        /// </summary>
        /// <param name="priority">The value of the priority.</param>
        /// <returns>
        ///     <c>True</c> if the renumbering strategy for the given <paramref name="priority" /> is the default value or
        ///     <c>false</c> otherwise.
        /// </returns>
        /// <remarks>Y and Z renumbering strategies grow in a different direction than the default, so we take the opposite value to what is selected.
        /// The ordering method does not know when it is processing X, Y or Z, so this will make sure that they are ordered in the correct way for what the USER has chosen.</remarks>
        private Boolean IsDefaultBayComponentRenumberingStrategyForPriority(Byte priority)
        {
            return BayComponentXRenumberStrategyPriority == priority
                ? BayComponentXRenumberStrategy == DefaultXStrategyType
                : BayComponentYRenumberStrategyPriority == priority
                    ? BayComponentYRenumberStrategy == DefaultYStrategyType
                    : BayComponentZRenumberStrategy != DefaultZStrategyType;
        }

        /// <summary>
        ///     Inidicates whether the given <paramref name="priority" /> is set to its default value or not.
        /// </summary>
        /// <param name="priority">The value of the priority.</param>
        /// <returns>
        ///     <c>True</c> if the renumbering strategy for the given <paramref name="priority" /> is the default value or
        ///     <c>false</c> otherwise.
        /// </returns>
        /// <remarks>Y renumbering strategy grows in a different direction than the default, so we take the opposite value to what is selected.
        /// The ordering method does not know when it is processing X, Y or Z, so this will make sure that they are ordered in the correct way for what the USER has chosen.</remarks>
        private Boolean IsDefaultPositionRenumberingStrategyForPriority(Byte priority)
        {
            return PositionXRenumberStrategyPriority == priority
                ? PositionXRenumberStrategy == DefaultXStrategyType
                : PositionYRenumberStrategyPriority == priority
                    ? PositionYRenumberStrategy == DefaultYStrategyType
                    : PositionZRenumberStrategy != DefaultZStrategyType;
        }

        /// <summary>
        ///     Gets the value for the property indicated by the <paramref name="priority"/>.
        /// </summary>
        /// <param name="priority">Indicates the priority for which to find the property.</param>
        /// <param name="item">The instance of <see cref="PlanogramFixtureItem"/> that contains the values.</param>
        /// <returns>A <see cref="Single"/> value from the property indicated by the priority.</returns>
        private Single GetBayCoordinateValueForPriority(Byte priority, PlanogramFixtureItem item)
        {
            return BayComponentXRenumberStrategyPriority == priority
                ? item.GetPlanogramRelativeBoundingBox(item.GetPlanogramFixture()).X
                : BayComponentYRenumberStrategyPriority == priority
                    ? item.GetPlanogramRelativeBoundingBox(item.GetPlanogramFixture()).Y
                    : item.GetPlanogramRelativeBoundingBox(item.GetPlanogramFixture()).Z;
        }

        /// <summary>
        ///     Gets the value for the property indicated by the <paramref name="priority"/>.
        /// </summary>
        /// <param name="priority">Indicates the priority for which to find the property.</param>
        /// <param name="component">The instance of <see cref="PlanogramFixtureComponent"/> OR <see cref="PlanogramAssemblyComponent"/> that contains the values.</param>
        /// <param name="fixtureItem">The <see cref="PlanogramFixtureItem"/> that contains the component.</param>
        /// <param name="fixtureAssemblies">Optionally, for <see cref="PlanogramAssemblyComponent"/> items, the <see cref="PlanogramFixtureAssembly"/> that contains the component.</param>
        /// <returns>A <see cref="Single"/> value from the property indicated by the priority.</returns>
        private Single GetComponentCoordinateValueForPriority(Byte priority, IPlanogramFixtureComponent component,
            PlanogramFixtureItem fixtureItem, IEnumerable<PlanogramFixtureAssembly> fixtureAssemblies = null)
        {
            PointValue? coordinates = null;


            var fixtureComponent = component as PlanogramFixtureComponent;
            if (fixtureComponent != null)
            {
                coordinates = fixtureComponent.GetPlanogramRelativeCoordinates(fixtureItem);
            }

            var assemblyComponent = component as PlanogramAssemblyComponent;
            if (assemblyComponent != null)
            {
                if (fixtureAssemblies == null)
                {
                    throw new InvalidOperationException("To obtain values from a Planogram Assembly Component, the Planogram Fixture Assembly it belongs to needs to be provided.");
                }

                coordinates = assemblyComponent.GetPlanogramRelativeCoordinates(fixtureItem,
                    fixtureAssemblies.FirstOrDefault(
                        fixtureAssembly => fixtureAssembly.PlanogramAssemblyId.Equals(assemblyComponent.Parent.Id)));
            }

            if (coordinates == null)
            {
                throw new InvalidOperationException(String.Format("The component type was not recognized. ({0})", component.GetType()));
            }

            var value = ((PointValue)coordinates);
            return BayComponentXRenumberStrategyPriority == priority
                ? value.X
                : BayComponentYRenumberStrategyPriority == priority
                    ? value.Y
                    : value.Z;
        }

        /// <summary>
        ///     Gets the value for the property indicated by the <paramref name="priority"/>.
        /// </summary>
        /// <param name="priority">Indicates the priority for which to find the property.</param>
        /// <param name="position">The instance of <see cref="PlanogramPosition"/> that contains the values.</param>
        /// <returns>A <see cref="Single"/> value from the property indicated by the priority.</returns>
        private float GetPositionCoordinateValueForPriority(Byte priority, PlanogramPosition position)
        {
            var value = position.GetPlanogramRelativeCoordinates(position.GetPlanogramSubComponentPlacement());
            return PositionXRenumberStrategyPriority == priority
                ? value.X
                : PositionYRenumberStrategyPriority == priority
                    ? value.Y
                    : value.Z;
        }

        /// <summary>
        ///     Determines whether two positions are adjacent or not.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lastPosition"></param>
        /// <returns></returns>
        /// <remarks>To determine if two positions are adjacent we need to look at one of the dimensions (X, Y or Z). The other two dimensions need to be equal in Sequence, and the dimension we are focusing on needs to be apart no more than 1.
        /// For example, a position (1,1,1) and a position (1,2,1) would be adjacent in the Y dimension.. but not at all in the X or Z dimensions.
        /// The dimension that we will use to determine adjancency will be the renumbering dimension with the highest priority.</remarks>
        private Boolean PositionsAreAdjacent(PlanogramPosition position, PlanogramPosition lastPosition)
        {
            if (lastPosition == null) return false;
            if (position.PlanogramProductId != lastPosition.PlanogramProductId) return false;
            if (!Equals(position.PlanogramAssemblyComponentId, lastPosition.PlanogramAssemblyComponentId)) return false;
            if (!Equals(position.PlanogramFixtureComponentId, lastPosition.PlanogramFixtureComponentId)) return false;
            if (Math.Abs(position.SequenceX - lastPosition.SequenceX) != (PositionXRenumberStrategyPriority == FirstRenumberingPriority ? 1 : 0)) return false;
            if (Math.Abs(position.SequenceY - lastPosition.SequenceY) != (PositionYRenumberStrategyPriority == FirstRenumberingPriority ? 1 : 0)) return false;
            if (Math.Abs(position.SequenceZ - lastPosition.SequenceZ) != (PositionZRenumberStrategyPriority == FirstRenumberingPriority ? 1 : 0)) return false;

            return true;
        }

        #endregion

        /// <summary>
        ///     Loads the data contained in the given <paramref name="source"/> into this instance.
        /// </summary>
        /// <param name="source">Instance implementing <see cref="IPlanogramRenumberingStrategy"/> that will provide the values to be loaded into this instance.</param>
        public void LoadFrom(IPlanogramRenumberingStrategy source)
        {
            LoadProperty<Boolean>(IsEnabledProperty, true);
            LoadProperty(NameProperty, source.Name);
            LoadProperty(BayComponentXRenumberStrategyPriorityProperty, source.BayComponentXRenumberStrategyPriority);
            LoadProperty(BayComponentYRenumberStrategyPriorityProperty, source.BayComponentYRenumberStrategyPriority);
            LoadProperty(BayComponentZRenumberStrategyPriorityProperty, source.BayComponentZRenumberStrategyPriority);
            LoadProperty(PositionXRenumberStrategyPriorityProperty, source.PositionXRenumberStrategyPriority);
            LoadProperty(PositionYRenumberStrategyPriorityProperty, source.PositionYRenumberStrategyPriority);
            LoadProperty(PositionZRenumberStrategyPriorityProperty, source.PositionZRenumberStrategyPriority);
            LoadProperty(BayComponentXRenumberStrategyProperty, source.BayComponentXRenumberStrategy);
            LoadProperty(BayComponentYRenumberStrategyProperty, source.BayComponentYRenumberStrategy);
            LoadProperty(BayComponentZRenumberStrategyProperty, source.BayComponentZRenumberStrategy);
            LoadProperty(PositionXRenumberStrategyProperty, source.PositionXRenumberStrategy);
            LoadProperty(PositionYRenumberStrategyProperty, source.PositionYRenumberStrategy);
            LoadProperty(PositionZRenumberStrategyProperty, source.PositionZRenumberStrategy);
            LoadProperty(RestartComponentRenumberingPerBayProperty, source.RestartComponentRenumberingPerBay);
            LoadProperty(IgnoreNonMerchandisingComponentsProperty, source.IgnoreNonMerchandisingComponents);
            LoadProperty(RestartPositionRenumberingPerComponentProperty, source.RestartPositionRenumberingPerComponent);
            LoadProperty(UniqueNumberMultiPositionProductsPerComponentProperty, source.UniqueNumberMultiPositionProductsPerComponent);
            LoadProperty(ExceptAdjacentPositionsProperty, source.ExceptAdjacentPositions);
            LoadProperty(RestartPositionRenumberingPerBayProperty, source.RestartPositionRenumberingPerBay);
            LoadProperty(RestartComponentRenumberingPerComponentTypeProperty, source.RestartComponentRenumberingPerComponentType);
        }

        #endregion
    }
}