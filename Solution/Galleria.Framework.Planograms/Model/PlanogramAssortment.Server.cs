﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortment
    {
        #region Constructor
        private PlanogramAssortment() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortment Fetch(IDalContext dalContext, PlanogramAssortmentDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortment>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramAssortmentDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramAssortmentDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when performing a fetch
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPlanogramAssortmentDal dal = dalContext.GetDal<IPlanogramAssortmentDal>())
                    {
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    this.Create();
                }
            }
            this.MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentDto>(
            (dc) =>
            {
                PlanogramAssortmentDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortment>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramAssortmentDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
