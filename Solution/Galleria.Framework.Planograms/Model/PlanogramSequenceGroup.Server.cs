﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Server side implementation of <see cref="PlanogramSequenceGroup"/>.
    /// </summary>
    public sealed partial class PlanogramSequenceGroup
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to obtain new instances of <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        private PlanogramSequenceGroup() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Invokes the data portal to retrieve the <see cref="PlanogramSequenceGroup"/> item corresponding to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal static PlanogramSequenceGroup Fetch(IDalContext dalContext, PlanogramSequenceGroupDto dto)
        {
            return DataPortal.FetchChild<PlanogramSequenceGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads this instance properties from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramSequenceGroupDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Creates a data transfer object for this instance.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private PlanogramSequenceGroupDto GetDataTransferObject(PlanogramSequence parent)
        {
            return new PlanogramSequenceGroupDto
            {
                Id = this.ReadProperty(IdProperty),
                PlanogramSequenceId = parent.Id,
                Name = this.ReadProperty(NameProperty),
                Colour = this.ReadProperty(ColourProperty),
                ExtendedData = this.ReadProperty(ExtendedDataProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked via reflection by CSLA to fetch the child item.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void Child_Fetch(IDalContext dalContext, PlanogramSequenceGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Invoked via reflection by CSLA to insert the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramSequence parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramSequenceGroupDto>(
                dc =>
                {
                    PlanogramSequenceGroupDto dto = this.GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    this.LoadProperty<Object>(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramSequenceGroup>(oldId, dto.Id);
                });
            this.FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Invoked via reflection by CSLA to update the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Update(BatchSaveContext batchContext, PlanogramSequence parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update(dc => this.GetDataTransferObject(parent));
            }
            this.FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Invoked via reflection by CSLA to delete the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramSequence parent)
        {
            batchContext.Delete(this.GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}