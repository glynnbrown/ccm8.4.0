﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31862 : A.Silva
//  Amended NewPlanogramComparisonItem to accept an initial status.
// V8-31863 : A.Silva
//  Amended NewPlanogramComparisonItem so that it can be used with different planogram item types.

#endregion

#endregion

using System;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Represents the result for an item comparisons agains the same item in the target planogram.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonItem : ModelObject<PlanogramComparisonItem>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        public new PlanogramComparisonResult Parent
        {
            get { return ((PlanogramComparisonItemList)base.Parent).Parent; }
        }

        #endregion

        #region ItemType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="ItemType"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramItemType> ItemTypeProperty =
            RegisterModelProperty<PlanogramItemType>(c => c.ItemType);

        /// <summary>
        ///     Get the type of the item compared in this instance.
        /// </summary>
        public PlanogramItemType ItemType
        {
            get { return GetProperty(ItemTypeProperty); }
            set { SetProperty(ItemTypeProperty, value); }
        }

        #endregion

        #region ItemId

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="ItemId"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ItemIdProperty =
            RegisterModelProperty<String>(c => c.ItemId);

        /// <summary>
        ///     Get ID for the item compared in this instance.
        /// </summary>
        public String ItemId
        {
            get { return GetProperty(ItemIdProperty); }
            set { SetProperty(ItemIdProperty, value); }
        }

        #endregion

        #region Status

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="Status"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComparisonItemStatusType> StatusProperty =
            RegisterModelProperty<PlanogramComparisonItemStatusType>(c => c.Status);

        /// <summary>
        ///     Get the type of the item compared in this instance.
        /// </summary>
        public PlanogramComparisonItemStatusType Status
        {
            get { return GetProperty(StatusProperty); }
            set { SetProperty(StatusProperty, value); }
        }

        #endregion

        #region FieldValues

        public static readonly ModelPropertyInfo<PlanogramComparisonFieldValueList> FieldValuesProperty =
            RegisterModelProperty<PlanogramComparisonFieldValueList>(c => c.FieldValues, RelationshipTypes.LazyLoad);

        public PlanogramComparisonFieldValueList FieldValues
        {
            get { return GetPropertyLazy(FieldValuesProperty, new PlanogramComparisonFieldValueList.FetchByParentIdCriteria(DalFactoryName, Id), true); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(ItemTypeProperty));
            BusinessRules.AddRule(new Required(ItemIdProperty));
            BusinessRules.AddRule(new MaxLength(ItemIdProperty, 50));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        public static PlanogramComparisonItem NewPlanogramComparisonItem()
        {
            var item = new PlanogramComparisonItem();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonResult"/> derived from the given <paramref name="planItemType"/>.
        /// </summary>
        /// <param name="comparisonKey"></param>
        /// <param name="planItemType"></param>
        /// <param name="status"></param>
        public static PlanogramComparisonItem NewPlanogramComparisonItem(Object comparisonKey, PlanogramItemType planItemType, PlanogramComparisonItemStatusType status)
        {
            var item = new PlanogramComparisonItem();
            item.Create(comparisonKey, planItemType, status);
            return item;
        }

        #endregion

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(FieldValuesProperty, PlanogramComparisonFieldValueList.NewPlanogramComparisonFieldValueList());
            LoadProperty(StatusProperty, PlanogramComparisonItemStatusType.Unchanged);
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Initialize all default property values for this instance using the given <paramref name="comparisonKey"/>.
        /// </summary>
        private void Create(Object comparisonKey, PlanogramItemType planItemType, PlanogramComparisonItemStatusType status)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ItemTypeProperty, planItemType);
            LoadProperty(ItemIdProperty, comparisonKey);
            LoadProperty(StatusProperty, status);
            LoadProperty(FieldValuesProperty, PlanogramComparisonFieldValueList.NewPlanogramComparisonFieldValueList());
            MarkAsChild();
            base.Create();
        }

        #endregion
    }
}