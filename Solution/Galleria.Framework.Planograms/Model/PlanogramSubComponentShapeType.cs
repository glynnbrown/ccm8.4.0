﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Sub Component Shape Type Enum
    /// </summary>
    public enum PlanogramSubComponentShapeType
    {
        Box = 0
    }

    /// <summary>
    /// PlanogramSubComponentShapeType Helper Class
    /// </summary>
    public static class PlanogramSubComponentShapeTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentShapeType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentShapeType, String>()
            {
                {PlanogramSubComponentShapeType.Box, Message.Enum_PlanogramSubComponentShapeType_Box},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentShapeType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentShapeType, String>()
            {
                {PlanogramSubComponentShapeType.Box, Message.Enum_PlanogramSubComponentShapeType_Box},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentShapeType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentShapeType>()
            {
                {Message.Enum_PlanogramSubComponentShapeType_Box.ToLowerInvariant(), PlanogramSubComponentShapeType.Box},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentShapeType? PlanogramSubComponentShapeTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentShapeType? returnValue = null;
            PlanogramSubComponentShapeType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}