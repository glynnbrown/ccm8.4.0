﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26815 : A.Silva ~ Added constuctor from source.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Model representing a list of PlanogramValidationTemplateGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationTemplateGroupList : ModelList<PlanogramValidationTemplateGroupList, PlanogramValidationTemplateGroup>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramValidationTemplate Parent
        {
            get { return (PlanogramValidationTemplate)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroupList NewPlanogramValidationTemplateGroupList()
        {
            var item = new PlanogramValidationTemplateGroupList();
            item.Create();
            return item;
        }

        public static PlanogramValidationTemplateGroupList NewPlanogramValidationTemplateGroupList(IValidationTemplate source)
        {
            var item = new PlanogramValidationTemplateGroupList();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        private void Create(IValidationTemplate source)
        {
            AddRange(source.Groups.Select(PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup));
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new group with the given name
        /// </summary>
        public PlanogramValidationTemplateGroup Add(String groupName)
        {
            PlanogramValidationTemplateGroup newGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup(groupName);
            this.Add(newGroup);
            return newGroup;
        }

        #endregion
    }
}
