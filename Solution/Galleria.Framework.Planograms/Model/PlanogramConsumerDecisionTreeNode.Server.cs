﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong
//	Created
#endregion

#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: (CCM 8.2.0)
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
// V8-30956 : D.Pleasance
//  Fixed issue where ids are not resolved at the right time
//  due to the way the BatchSaveContext works
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Csla.Core;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeNode
    {
        #region Constructors
        private PlanogramConsumerDecisionTreeNode() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static PlanogramConsumerDecisionTreeNode FetchPlanogramConsumerDecisionTreeNode(
            IDalContext dalContext, PlanogramConsumerDecisionTreeNodeDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTreeNode>(dalContext, dto, masterNodeDtoList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private PlanogramConsumerDecisionTreeNodeDto GetDataTransferObject(PlanogramConsumerDecisionTreeNode parent)
        {
            return new PlanogramConsumerDecisionTreeNodeDto
            {
                Id = this.ReadProperty<Object>(IdProperty),
                Name = this.ReadProperty<String>(NameProperty),
                PlanogramConsumerDecisionTreeLevelId = this.ReadProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty),
                ParentNodeId = (parent != null) ? parent.Id : null,
                PlanogramConsumerDecisionTreeId = this.ParentPlanogramConsumerDecisionTree.Id,
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                MetaCountOfProducts = this.ReadProperty<Int32?>(MetaCountOfProductsProperty),
                MetaCountOfProductRecommendedInAssortment = this.ReadProperty<Int32?>(MetaCountOfProductRecommendedInAssortmentProperty),
                MetaCountOfProductPlacedOnPlanogram = this.ReadProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty),
                MetaLinearSpaceAllocatedToProductsOnPlanogram = this.ReadProperty<Single?>(MetaLinearSpaceAllocatedToProductsOnPlanogramProperty),
                MetaAreaSpaceAllocatedToProductsOnPlanogram = this.ReadProperty<Single?>(MetaAreaSpaceAllocatedToProductsOnPlanogramProperty),
                MetaVolumetricSpaceAllocatedToProductsOnPlanogram = this.ReadProperty<Single?>(MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty),

                P1 = this.ReadProperty<Single?>(MetaP1Property),
                P2 = this.ReadProperty<Single?>(MetaP2Property),
                P3 = this.ReadProperty<Single?>(MetaP3Property),
                P4 = this.ReadProperty<Single?>(MetaP4Property),
                P5 = this.ReadProperty<Single?>(MetaP5Property),
                P6 = this.ReadProperty<Single?>(MetaP6Property),
                P7 = this.ReadProperty<Single?>(MetaP7Property),
                P8 = this.ReadProperty<Single?>(MetaP8Property),
                P9 = this.ReadProperty<Single?>(MetaP9Property),
                P10 = this.ReadProperty<Single?>(MetaP10Property),
                P11 = this.ReadProperty<Single?>(MetaP11Property),
                P12 = this.ReadProperty<Single?>(MetaP12Property),
                P13 = this.ReadProperty<Single?>(MetaP13Property),
                P14 = this.ReadProperty<Single?>(MetaP14Property),
                P15 = this.ReadProperty<Single?>(MetaP15Property),
                P16 = this.ReadProperty<Single?>(MetaP16Property),
                P17 = this.ReadProperty<Single?>(MetaP17Property),
                P18 = this.ReadProperty<Single?>(MetaP18Property),
                P19 = this.ReadProperty<Single?>(MetaP19Property),
                P20 = this.ReadProperty<Single?>(MetaP20Property),
                MetaP1Percentage = this.ReadProperty<Single?>(MetaP1PercentageProperty),
                MetaP2Percentage = this.ReadProperty<Single?>(MetaP2PercentageProperty),
                MetaP3Percentage = this.ReadProperty<Single?>(MetaP3PercentageProperty),
                MetaP4Percentage = this.ReadProperty<Single?>(MetaP4PercentageProperty),
                MetaP5Percentage = this.ReadProperty<Single?>(MetaP5PercentageProperty),
                MetaP6Percentage = this.ReadProperty<Single?>(MetaP6PercentageProperty),
                MetaP7Percentage = this.ReadProperty<Single?>(MetaP7PercentageProperty),
                MetaP8Percentage = this.ReadProperty<Single?>(MetaP8PercentageProperty),
                MetaP9Percentage = this.ReadProperty<Single?>(MetaP9PercentageProperty),
                MetaP10Percentage = this.ReadProperty<Single?>(MetaP10PercentageProperty),
                MetaP11Percentage = this.ReadProperty<Single?>(MetaP11PercentageProperty),
                MetaP12Percentage = this.ReadProperty<Single?>(MetaP12PercentageProperty),
                MetaP13Percentage = this.ReadProperty<Single?>(MetaP13PercentageProperty),
                MetaP14Percentage = this.ReadProperty<Single?>(MetaP14PercentageProperty),
                MetaP15Percentage = this.ReadProperty<Single?>(MetaP15PercentageProperty),
                MetaP16Percentage = this.ReadProperty<Single?>(MetaP16PercentageProperty),
                MetaP17Percentage = this.ReadProperty<Single?>(MetaP17PercentageProperty),
                MetaP18Percentage = this.ReadProperty<Single?>(MetaP18PercentageProperty),
                MetaP19Percentage = this.ReadProperty<Single?>(MetaP19PercentageProperty),
                MetaP20Percentage = this.ReadProperty<Single?>(MetaP20PercentageProperty),
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(
            IDalContext dalContext, PlanogramConsumerDecisionTreeNodeDto dto,
            IEnumerable<PlanogramConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty, dto.PlanogramConsumerDecisionTreeLevelId);
            this.LoadProperty<Object>(ParentNodeIdProperty, dto.ParentNodeId);

            this.LoadProperty<PlanogramConsumerDecisionTreeNodeList>(ChildListProperty,
                PlanogramConsumerDecisionTreeNodeList.FetchListByParentNodeId(dalContext, dto.Id, masterNodeDtoList));

            this.LoadProperty<PlanogramConsumerDecisionTreeNodeProductList>(ProductsProperty,
                PlanogramConsumerDecisionTreeNodeProductList.FetchListByPlanogramConsumerDecisionTreeNodeId(dalContext, dto.Id));

            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Int32?>(MetaCountOfProductsProperty, dto.MetaCountOfProducts);
            this.LoadProperty<Int32?>(MetaCountOfProductRecommendedInAssortmentProperty, dto.MetaCountOfProductRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty, dto.MetaCountOfProductPlacedOnPlanogram);
            this.LoadProperty<Single?>(MetaLinearSpaceAllocatedToProductsOnPlanogramProperty, dto.MetaLinearSpaceAllocatedToProductsOnPlanogram);
            this.LoadProperty<Single?>(MetaAreaSpaceAllocatedToProductsOnPlanogramProperty, dto.MetaAreaSpaceAllocatedToProductsOnPlanogram);
            this.LoadProperty<Single?>(MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty, dto.MetaVolumetricSpaceAllocatedToProductsOnPlanogram);

            this.LoadProperty<Single?>(MetaP1Property, dto.P1);
            this.LoadProperty<Single?>(MetaP2Property, dto.P2);
            this.LoadProperty<Single?>(MetaP3Property, dto.P3);
            this.LoadProperty<Single?>(MetaP4Property, dto.P4);
            this.LoadProperty<Single?>(MetaP5Property, dto.P5);
            this.LoadProperty<Single?>(MetaP6Property, dto.P6);
            this.LoadProperty<Single?>(MetaP7Property, dto.P7);
            this.LoadProperty<Single?>(MetaP8Property, dto.P8);
            this.LoadProperty<Single?>(MetaP9Property, dto.P9);
            this.LoadProperty<Single?>(MetaP10Property, dto.P10);
            this.LoadProperty<Single?>(MetaP11Property, dto.P11);
            this.LoadProperty<Single?>(MetaP12Property, dto.P12);
            this.LoadProperty<Single?>(MetaP13Property, dto.P13);
            this.LoadProperty<Single?>(MetaP14Property, dto.P14);
            this.LoadProperty<Single?>(MetaP15Property, dto.P15);
            this.LoadProperty<Single?>(MetaP16Property, dto.P16);
            this.LoadProperty<Single?>(MetaP17Property, dto.P17);
            this.LoadProperty<Single?>(MetaP18Property, dto.P18);
            this.LoadProperty<Single?>(MetaP19Property, dto.P19);
            this.LoadProperty<Single?>(MetaP20Property, dto.P20);

            this.LoadProperty<Single?>(MetaP1PercentageProperty,  dto.MetaP1Percentage);
            this.LoadProperty<Single?>(MetaP2PercentageProperty,  dto.MetaP2Percentage);
            this.LoadProperty<Single?>(MetaP3PercentageProperty,  dto.MetaP3Percentage);
            this.LoadProperty<Single?>(MetaP4PercentageProperty,  dto.MetaP4Percentage);
            this.LoadProperty<Single?>(MetaP5PercentageProperty,  dto.MetaP5Percentage);
            this.LoadProperty<Single?>(MetaP6PercentageProperty,  dto.MetaP6Percentage);
            this.LoadProperty<Single?>(MetaP7PercentageProperty,  dto.MetaP7Percentage);
            this.LoadProperty<Single?>(MetaP8PercentageProperty,  dto.MetaP8Percentage);
            this.LoadProperty<Single?>(MetaP9PercentageProperty,  dto.MetaP9Percentage);
            this.LoadProperty<Single?>(MetaP10PercentageProperty, dto.MetaP10Percentage);
            this.LoadProperty<Single?>(MetaP11PercentageProperty, dto.MetaP11Percentage);
            this.LoadProperty<Single?>(MetaP12PercentageProperty, dto.MetaP12Percentage);
            this.LoadProperty<Single?>(MetaP13PercentageProperty, dto.MetaP13Percentage);
            this.LoadProperty<Single?>(MetaP14PercentageProperty, dto.MetaP14Percentage);
            this.LoadProperty<Single?>(MetaP15PercentageProperty, dto.MetaP15Percentage);
            this.LoadProperty<Single?>(MetaP16PercentageProperty, dto.MetaP16Percentage);
            this.LoadProperty<Single?>(MetaP17PercentageProperty, dto.MetaP17Percentage);
            this.LoadProperty<Single?>(MetaP18PercentageProperty, dto.MetaP18Percentage);
            this.LoadProperty<Single?>(MetaP19PercentageProperty, dto.MetaP19Percentage);
            this.LoadProperty<Single?>(MetaP20PercentageProperty, dto.MetaP20Percentage);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeNodeDto dto, IEnumerable<PlanogramConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            LoadDataTransferObject(dalContext, dto, masterNodeDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeNodeDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramConsumerDecisionTreeNodeDto dto = GetDataTransferObject(null);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTreeNode>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeNodeDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramConsumerDecisionTreeNodeDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTreeNode>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeNodeDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(null);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeNodeDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramConsumerDecisionTree parent)
        {
            // delete the node
            batchContext.Delete<PlanogramConsumerDecisionTreeNodeDto>(this.GetDataTransferObject(null));

            //mark any child levels of this as deleted
            foreach (PlanogramConsumerDecisionTreeNode child in this.ChildList)
            {
                //forcibly mark the child as deleted
                ((Csla.Core.IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(batchContext, this);
        }

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            // delete the node
            batchContext.Delete<PlanogramConsumerDecisionTreeNodeDto>(this.GetDataTransferObject(parent));

            //mark any child nodes of this as deleted
            foreach (PlanogramConsumerDecisionTreeNode child in this.ChildList)
            {
                //forcibly mark the child as deleted
                ((Csla.Core.IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #endregion
    }
}
