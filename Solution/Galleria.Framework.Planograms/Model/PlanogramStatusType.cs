﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created but liable to significant change
// V8-30132 : L.Luong
//  Added Unknown type
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum denoting the possible planogram statuses
    /// </summary>
    public enum PlanogramStatusType
    {
        None = 0,
        WorkInProgress = 1,
        Approved = 2,
        Published = 3,
        Archived = 4,
        Unknown = 5
    }

    /// <summary>
    /// PlanogramStatusType Helper Class
    /// </summary>
    public static class PlanogramStatusTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramStatusType, String> FriendlyNames =
            new Dictionary<PlanogramStatusType, String>()
            {
                {PlanogramStatusType.Unknown, String.Empty},
                {PlanogramStatusType.None, Message.Enum_PlanogramStatusType_None},
                {PlanogramStatusType.WorkInProgress, Message.Enum_PlanogramStatusType_WorkInProgress},
                {PlanogramStatusType.Approved, Message.Enum_PlanogramStatusType_Approved},
                {PlanogramStatusType.Published, Message.Enum_PlanogramStatusType_Published},
                {PlanogramStatusType.Archived, Message.Enum_PlanogramStatusType_Archived},
            };
    }
}
