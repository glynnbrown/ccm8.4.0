﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27589 : A.Silva
//      Created.
// V8-27742 : A.Silva
//      Added extra validation warning types.
// V8-27836 : A.Silva
//      Added ProductDoesNotAchieveMinDos, ProductDoesNotAchieveMinCases, ProductDoesNotAchieveMinDeliveries, ProductDoesNotAchieveMinShelfLife
// V8-27924 : A.Silva
//      Added PositionExceedsMaxTopCap. Renamed PositionExceedsMaxCap to PositionExceedsMaxRightCap.
// V8-28121 : A.Silva
//      Localized texts.

#endregion

#region Version History: (CCM v8.2.0)
// V8-30705 : A.Probyn
//      Added new assortment rule warning types
#endregion

#region Version History: (CCM v8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-32088 : M.Pettit
//  Added FriendlyShortNames dictionary
// V8-32396 : A.Probyn
//  Added ProductBreaksDelistFamilyRule
// V8-32521  : J.Pickup
//  Added ComponentIsOutsideOfFixtureArea
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Values that represent different types of validation warnings in a planogram.
    /// </summary>
    public enum PlanogramValidationWarningType
    {
        NotSet,
        ComponentHasCollisions,
        ComponentSlopeWithNoRiser,
        ComponentIsOverfilled,
        ComponentIsOutsideOfFixtureArea,
        PositionHasCollisions,
        PositionIsOutsideMerchandisingSpace,
        PositionExceedsMaxStack,
        PositionExceedsMaxRightCap,
        PositionExceedsMaxTopCap,
        PositionDoesNotAchieveMinDeep,
        PositionExceedsMaxDeep,
        PositionCannotBreakTrayUp,
        PositionCannotBreakTrayDown,
        PositionCannotBreakTrayTop,
        PositionCannotBreakTrayBack,
        PositionHasInvalidMerchStyleSettings,
        PositionIsHangingTray,
        PositionIsHangingWithoutPeg,
        PegIsOverfilled,
        ProductHasDuplicateGtin,
        ProductDoesNotAchieveMinDos,
        ProductDoesNotAchieveMinCases,
        ProductDoesNotAchieveMinDeliveries,
        ProductDoesNotAchieveMinShelfLife,
        ProductBreaksLocalLineRule,
        ProductBreaksDistributionRule,
        ProductBreaksProductRule,
        ProductBreaksFamilyRule,
        ProductBreaksInheritanceRule,
        ProductBreaksCoreRule,
        ProductBreaksDelistProductRule,
        ProductBreaksForceProductRuleBroken,
        ProductBreaksPreserveProductRuleBroken,
        ProductBreaksMinimumHurdleProductRule,
        ProductBreaksMaximumProductFamilyRule,
        ProductBreaksMinimumProductFamilyRule,
        ProductBreaksDependencyFamilyRule,
        ProductBreaksLocationProductIllegalRule,
        ProductBreaksLocationProductLegalRule,
        ProductBreaksDelistFamilyRule,
        PositionPegHolesAreInvalidRule,
        ProductAttributeDoesNotMatchMasterProductAttribute
    }

    /// <summary>
    ///     Helper class for <see cref="PlanogramValidationWarningType" />.
    /// </summary>
    public static class PlanogramValidationWarningTypeHelper
    {
        /// <summary>
        ///     Holds a dictionary mapping each <see cref="PlanogramValidationWarningType" /> value to a friendly name to present
        ///     to the user.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationWarningType, String> FriendlyNames = new Dictionary
            <PlanogramValidationWarningType, String>
        {
            {PlanogramValidationWarningType.NotSet, Message.Enum_PlanogramValidationWarningType_NotSet},
            {PlanogramValidationWarningType.ComponentHasCollisions, Message.Enum_PlanogramValidationWarningType_ComponentHasCollisions},
            {PlanogramValidationWarningType.ComponentSlopeWithNoRiser, Message.Enum_PlanogramValidationWarningType_ComponentSlopeWithNoRiser},
            {PlanogramValidationWarningType.ComponentIsOverfilled, Message.Enum_PlanogramValidationWarningType_ComponentIsOverfilled},
            {PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea, Message.Enum_PlanogramValidationWarningType_ComponentIsOutsideOfFixtureArea},
            {PlanogramValidationWarningType.PositionHasCollisions, Message.Enum_PlanogramValidationWarningType_PositionHasCollisions},
            {PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace, Message.Enum_PlanogramValidationWarningType_PositionIsOutsideMerchandisingSpace},
            {PlanogramValidationWarningType.PositionExceedsMaxStack, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxStack},
            {PlanogramValidationWarningType.PositionExceedsMaxRightCap, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxRightCap},
            {PlanogramValidationWarningType.PositionExceedsMaxTopCap, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxTopCap},
            {PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep, Message.Enum_PlanogramValidationWarningType_PositionDoesNotAchieveMinDeep},
            {PlanogramValidationWarningType.PositionExceedsMaxDeep, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxDeep},
            {PlanogramValidationWarningType.PositionCannotBreakTrayUp, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayUp},
            {PlanogramValidationWarningType.PositionCannotBreakTrayDown, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayDown},
            {PlanogramValidationWarningType.PositionCannotBreakTrayTop, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayTop},
            {PlanogramValidationWarningType.PositionCannotBreakTrayBack, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayBack},
            {PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings, Message.Enum_PlanogramValidationWarningType_PositionHasInvalidMerchStyleSettings},
            {PlanogramValidationWarningType.PositionIsHangingTray, Message.Enum_PlanogramValidationWarningType_PositionIsHangingTray},
            {PlanogramValidationWarningType.PositionIsHangingWithoutPeg, Message.Enum_PlanogramValidationWarningType_PositionIsHangingWithoutPeg},
            {PlanogramValidationWarningType.PegIsOverfilled, Message.Enum_PlanogramValidationWarningType_PegIsOverfilled},
            {PlanogramValidationWarningType.ProductHasDuplicateGtin, Message.Enum_PlanogramValidationWarningType_ProductHasDuplicateGtin},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinDos, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinDos},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinCases, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinCases},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinDeliveries},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinShelfLife},
            {PlanogramValidationWarningType.ProductBreaksLocalLineRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocalLineRule},
            {PlanogramValidationWarningType.ProductBreaksDistributionRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDistributionRule},
            {PlanogramValidationWarningType.ProductBreaksProductRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksProductRule},
            {PlanogramValidationWarningType.ProductBreaksFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksFamilyRule},
            {PlanogramValidationWarningType.ProductBreaksInheritanceRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksInheritanceRule},
            {PlanogramValidationWarningType.ProductBreaksCoreRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksCoreRule},
            {PlanogramValidationWarningType.ProductBreaksDelistProductRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDelistProductRule},
            {PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken, Message.Enum_PlanogramValidationWarningType_ProductBreaksForceProductRuleBroken},
            {PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken, Message.Enum_PlanogramValidationWarningType_ProductBreaksPreserveProductRuleBroken},
            {PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMinimumHurdleProductRule},
            {PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMaximumProductFamilyRule},
            {PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMinimumProductFamilyRule},
            {PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDependencyFamilyRule},
            {PlanogramValidationWarningType.ProductBreaksDelistFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDelistFamilyRule},
            {PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocationProductIllegalRule},
            {PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocationProductLegalRule},
            {PlanogramValidationWarningType.PositionPegHolesAreInvalidRule, Message.Enum_PlanogramValidationWarningType_PositionPegHolesAreInvalidRule},
            {PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute, Message.Enum_PlanogramValidationWarningType_ProductAttributeDoesNotMatchMasterProductAttribute},
        };

        /// <summary>
        ///     Holds a dictionary mapping each <see cref="PlanogramValidationWarningType" /> value to a friendly name to present
        ///     to the user.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationWarningType, String> FriendlyShortNames = new Dictionary
            <PlanogramValidationWarningType, String>
        {
            {PlanogramValidationWarningType.NotSet, Message.Enum_PlanogramValidationWarningType_NotSet},
            {PlanogramValidationWarningType.ComponentHasCollisions, Message.Enum_PlanogramValidationWarningType_ComponentHasCollisionsShortDescription},
            {PlanogramValidationWarningType.ComponentSlopeWithNoRiser, Message.Enum_PlanogramValidationWarningType_ComponentSlopeWithNoRiserShortDescription},
            {PlanogramValidationWarningType.ComponentIsOverfilled, Message.Enum_PlanogramValidationWarningType_ComponentIsOverfilledShortDescription},
            {PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea, Message.Enum_PlanogramValidationWarningType_ComponentIsOutsideOfFixtureAreaShortDescription},
            {PlanogramValidationWarningType.PositionHasCollisions, Message.Enum_PlanogramValidationWarningType_PositionHasCollisionsShortDescription},
            {PlanogramValidationWarningType.PositionIsOutsideMerchandisingSpace, Message.Enum_PlanogramValidationWarningType_PositionIsOutsideMerchandisingSpaceShortDescription},
            {PlanogramValidationWarningType.PositionExceedsMaxStack, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxStackShortDescription},
            {PlanogramValidationWarningType.PositionExceedsMaxRightCap, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxRightCapShortDescription},
            {PlanogramValidationWarningType.PositionExceedsMaxTopCap, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxTopCapShortDescription},
            {PlanogramValidationWarningType.PositionDoesNotAchieveMinDeep, Message.Enum_PlanogramValidationWarningType_PositionDoesNotAchieveMinDeepShortDescription},
            {PlanogramValidationWarningType.PositionExceedsMaxDeep, Message.Enum_PlanogramValidationWarningType_PositionExceedsMaxDeepShortDescription},
            {PlanogramValidationWarningType.PositionCannotBreakTrayUp, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayUpShortDescription},
            {PlanogramValidationWarningType.PositionCannotBreakTrayDown, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayDownShortDescription},
            {PlanogramValidationWarningType.PositionCannotBreakTrayTop, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayTopShortDescription},
            {PlanogramValidationWarningType.PositionCannotBreakTrayBack, Message.Enum_PlanogramValidationWarningType_PositionCannotBreakTrayBackShortDescription},
            {PlanogramValidationWarningType.PositionHasInvalidMerchStyleSettings, Message.Enum_PlanogramValidationWarningType_PositionHasInvalidMerchStyleSettingsShortDescription},
            {PlanogramValidationWarningType.PositionIsHangingTray, Message.Enum_PlanogramValidationWarningType_PositionIsHangingTrayShortDescription},
            {PlanogramValidationWarningType.PositionIsHangingWithoutPeg, Message.Enum_PlanogramValidationWarningType_PositionIsHangingWithoutPegShortDescription},
            {PlanogramValidationWarningType.PegIsOverfilled, Message.Enum_PlanogramValidationWarningType_PegIsOverfilledShortDescription},
            {PlanogramValidationWarningType.ProductHasDuplicateGtin, Message.Enum_PlanogramValidationWarningType_ProductHasDuplicateGtinShortDescription},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinDos, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinDosShortDescription},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinCases, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinCasesShortDescription},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinDeliveriesShortDescription},
            {PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife, Message.Enum_PlanogramValidationWarningType_ProductDoesNotAchieveMinShelfLifeShortDescription},
            {PlanogramValidationWarningType.ProductBreaksLocalLineRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocalLineRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksDistributionRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDistributionRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksProductRule, String.Empty}, //Will be removed?
            {PlanogramValidationWarningType.ProductBreaksFamilyRule, String.Empty}, //Will be removed?
            {PlanogramValidationWarningType.ProductBreaksInheritanceRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksInheritanceRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksCoreRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksCoreRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksDelistProductRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDelistProductRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken, Message.Enum_PlanogramValidationWarningType_ProductBreaksForceProductRuleBrokenShortDescription},
            {PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken, Message.Enum_PlanogramValidationWarningType_ProductBreaksPreserveProductRuleBrokenShortDescription},
            {PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMinimumHurdleProductRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMaximumProductFamilyRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksMinimumProductFamilyRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDependencyFamilyRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksDelistFamilyRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksDelistFamilyRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocationProductIllegalRuleShortDescription},
            {PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule, Message.Enum_PlanogramValidationWarningType_ProductBreaksLocationProductLegalRuleShortDescription},
            {PlanogramValidationWarningType.PositionPegHolesAreInvalidRule, Message.Enum_PlanogramValidationWarningType_PositionPegHolesAreInvalidRuleShortDescription},
            {PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute, Message.Enum_PlanogramValidationWarningType_ProductAttributeDoesNotMatchMasterProductAttribute},
        };
    }
}