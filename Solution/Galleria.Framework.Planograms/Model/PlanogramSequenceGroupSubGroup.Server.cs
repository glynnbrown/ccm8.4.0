﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Csla;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Represents a grouping of products within a <see cref="PlanogramSequenceGroup"/>, that must be placed together on the
    /// same component.
    /// </summary>
    public partial class PlanogramSequenceGroupSubGroup
    {
        #region Constructor
        /// <summary>
        /// Private constructor to force the use of factory methods.
        /// </summary>
        private PlanogramSequenceGroupSubGroup() { } 
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Invokes the data portal to retrieve the <see cref="PlanogramSequenceGroupProduct"/> item corresponding to the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal static PlanogramSequenceGroupSubGroup Fetch(IDalContext dalContext, PlanogramSequenceGroupSubGroupDto dto)
        {
            return DataPortal.FetchChild<PlanogramSequenceGroupSubGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads this instance properties from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramSequenceGroupSubGroupDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(AlignmentProperty, dto.Alignment);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Creates a data transfer object for this instance.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private PlanogramSequenceGroupSubGroupDto GetDataTransferObject(PlanogramSequenceGroup parent)
        {
            return new PlanogramSequenceGroupSubGroupDto
            {
                Id = ReadProperty(IdProperty),
                Name = ReadProperty(NameProperty),
                Alignment = (Byte)ReadProperty<PlanogramSequenceGroupSubGroupAlignmentType>(AlignmentProperty),
                PlanogramSequenceGroupId = parent.Id,
                ExtendedData = ReadProperty(ExtendedDataProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked via reflection by CSLA to fetch the child item.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void Child_Fetch(IDalContext dalContext, PlanogramSequenceGroupSubGroupDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Invoked via reflection by CSLA to insert the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramSequenceGroupSubGroupDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramSequenceGroupSubGroup>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Invoked via reflection by CSLA to update the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Update(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            if (IsSelfDirty)
            {
                batchContext.Update(dc => GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Invoked via reflection by CSLA to delete the child item.
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramSequenceGroup parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}
