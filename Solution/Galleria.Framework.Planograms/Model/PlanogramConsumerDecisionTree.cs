﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27132 : A.Kuszyk
//  Implemented common interface. Added ReplaceConsumerDecisionTree() method.
// V8-27496 : L.Luong
//  Added a ClearAll method
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Header for the planogram consumer decision tree structure
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTree : ModelObject<PlanogramConsumerDecisionTree>, IPlanogramConsumerDecisionTree
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Root Node
        public static readonly ModelPropertyInfo<PlanogramConsumerDecisionTreeNode> RootNodeProperty =
            RegisterModelProperty<PlanogramConsumerDecisionTreeNode>(c => c.RootNode);
        /// <summary>
        /// The root node of the structure
        /// </summary>
        public PlanogramConsumerDecisionTreeNode RootNode
        {
            get { return GetProperty<PlanogramConsumerDecisionTreeNode>(RootNodeProperty); }
        }

        #endregion

        #region Root Level

        public static readonly ModelPropertyInfo<PlanogramConsumerDecisionTreeLevel> RootLevelProperty =
            RegisterModelProperty<PlanogramConsumerDecisionTreeLevel>(c => c.RootLevel);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public PlanogramConsumerDecisionTreeLevel RootLevel
        {
            get { return GetProperty<PlanogramConsumerDecisionTreeLevel>(RootLevelProperty); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramConsumerDecisionTree NewPlanogramConsumerDecisionTree()
        {
            var item = new PlanogramConsumerDecisionTree();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.PlanogramConsumerDecisionTree_Name_Default);
    
            //add in a new root level
            this.LoadProperty<PlanogramConsumerDecisionTreeLevel>(RootLevelProperty, PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel());
            this.RootLevel.Name = Message.PlanogramConsumerDecisionTreeLevel_RootLevel_DefaultName;

            //add in a new root node
            this.LoadProperty<PlanogramConsumerDecisionTreeNode>(RootNodeProperty, PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(this.RootLevel.Id));
            this.RootNode.Name = Message.PlanogramConsumerDecisionTreeNode_RootNode_DefaultName;
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Replaces the contents of this CDT with that provided.
        /// </summary>
        /// <param name="replacementCdt">The CDT to replace the existing one with.</param>
        /// <exception cref="ArgumentNullException">Thrown if replacementCdt is null.</exception>
        public void ReplaceConsumerDecisionTree(IPlanogramConsumerDecisionTree replacementCdt)
        {
            if (replacementCdt == null) throw new ArgumentNullException("replacementCdt cannot be null");

            this.Name = replacementCdt.Name;
            RootLevel.ReplaceConsumerDecisionTreeLevel(replacementCdt.RootLevel);
            RootNode.ReplaceConsumerDecisionTreeNode(replacementCdt.RootNode);
        }

        /// <summary>
        /// clears the cdt
        /// </summary>
        public void ClearAll()
        {
            if (this.RootLevel.ChildLevel != null)
            {
                this.RemoveLevel(this.RootLevel.ChildLevel, true);
            }
            this.RootNode.Products.Clear();

            this.RootNode.Name = Message.PlanogramConsumerDecisionTreeNode_RootNode_DefaultName;
            this.RootLevel.Name = Message.PlanogramConsumerDecisionTreeLevel_RootLevel_DefaultName;

            Name = Message.PlanogramConsumerDecisionTree_Name_Default;
        }

       
        #endregion

        #region Overrides

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramConsumerDecisionTree>(oldId, newId);
        }

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId factory method
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(c => c.PlanogramId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object PlanogramId
            {
                get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(PlanogramIdProperty, planogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Helpers


        /// <summary>
        /// Returns a list of levels in the structure
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<PlanogramConsumerDecisionTreeLevel> FetchAllLevels()
        {
            List<PlanogramConsumerDecisionTreeLevel> returnList = new List<PlanogramConsumerDecisionTreeLevel>();

            //move down through the levels adding each one
            PlanogramConsumerDecisionTreeLevel currentLevel = this.RootLevel;
            while (currentLevel != null)
            {
                returnList.Add(currentLevel);
                currentLevel = currentLevel.ChildLevel;
            }

            return returnList.AsReadOnly();
        }

        /// <summary>
        /// Returns a list of all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<PlanogramConsumerDecisionTreeNode> FetchAllNodes()
        {
            return this.RootNode.FetchAllChildNodes().ToList().AsReadOnly();
        }

        /// <summary>
        /// Inserts a new child under the parent and moves its children down
        /// </summary>
        /// <param name="parentLevel"></param>
        /// <returns>the new child</returns>
        public PlanogramConsumerDecisionTreeLevel InsertNewChildLevel(PlanogramConsumerDecisionTreeLevel parentLevel)
        {
            //create the new child
            PlanogramConsumerDecisionTreeLevel newChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();

            //[SA-15072] give it a unqiue name
            if (parentLevel.ParentPlanogramConsumerDecisionTree != null)
            {
                List<PlanogramConsumerDecisionTreeLevel> existingLevels = parentLevel.ParentPlanogramConsumerDecisionTree.FetchAllLevels().ToList();
                IEnumerable<String> takenNames = existingLevels.Select(l => l.Name);

                Int32 newChildLevelNo = existingLevels.IndexOf(parentLevel) + 2;
                String levelName = String.Empty;
                do
                {
                    /*newChildLevel.Name*/
                    levelName = String.Format(Message.PlanogramCdtMaintenance_NewLevel, newChildLevelNo);
                    newChildLevelNo++;
                }
                while (takenNames.Contains(levelName));

                newChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel(levelName);
            }

            //get the parents current child
            PlanogramConsumerDecisionTreeLevel currentParentChild = parentLevel.ChildLevel;

            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                newChildLevel.ChildLevel = currentParentChild;

                //remove from its old parent
                parentLevel.RemoveLevel(parentLevel.ChildLevel);

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = newChildLevel;

                //need to insert an extra unit underneath all units on the parent level
                Object parentLevelId = parentLevel.Id;
                IEnumerable<PlanogramConsumerDecisionTreeNode> parentLevelUnits =
                    this.FetchAllNodes().Where(u => u.PlanogramConsumerDecisionTreeLevelId.Equals(parentLevelId));

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (PlanogramConsumerDecisionTreeNode unit in parentLevelUnits)
                {
                    //get a list of child units belonging to the parent
                    List<PlanogramConsumerDecisionTreeNode> parentChildUnits = unit.ChildList.ToList();

                    if (parentChildUnits.Count > 0)
                    {
                        //create a new child unit on the new level
                        PlanogramConsumerDecisionTreeNode newChildUnit = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(newChildLevel.Id);
                        newChildUnit.Name = String.Format(Message.PlanogramConsumerDecisionTreeNode_Name_DefaultValue, newChildLevel.Name, addedGroupNumber);

                        //add the children to this new unit
                        newChildUnit.ChildList.AddList(parentChildUnits);

                        unit.ChildList.RaiseListChangedEvents = false;

                        //clear the parent child collection and add the new child unit
                        unit.ChildList.Clear();
                        unit.ChildList.Add(newChildUnit);

                        unit.ChildList.RaiseListChangedEvents = true;
                        unit.ChildList.Reset();
                    }

                    addedGroupNumber++;
                }
            }
            else
            {
                parentLevel.ChildLevel = newChildLevel;
            }

            return newChildLevel;
        }

        /// <summary>
        /// Removes the level from the structure and all its children
        /// </summary>
        /// <param name="levelToRemove"></param>
        public void RemoveLevel(PlanogramConsumerDecisionTreeLevel levelToRemove, Boolean moveProductToParent)
        {
            //need to find the bottom level
            PlanogramConsumerDecisionTreeLevel lowestLevel = levelToRemove;
            if (lowestLevel != null && !lowestLevel.IsRoot)
            {
                while (lowestLevel.ChildLevel != null)
                {
                    lowestLevel = lowestLevel.ChildLevel;
                }

                //go through each parent of the lowest level
                while (lowestLevel != levelToRemove.ParentLevel)
                {
                    //cycle through the parent units deleting all direct child units
                    //as they will be on the removed level
                    Object parentLevelRef = lowestLevel.ParentLevel.Id;
                    IEnumerable<PlanogramConsumerDecisionTreeNode> parentLevelUnits =
                        this.FetchAllNodes().Where(u => u.PlanogramConsumerDecisionTreeLevelId.Equals(parentLevelRef));

                    foreach (PlanogramConsumerDecisionTreeNode parentUnit in parentLevelUnits)
                    {
                        parentUnit.ChildList.RaiseListChangedEvents = false;

                        List<PlanogramConsumerDecisionTreeNode> childUnitsToRemove = parentUnit.ChildList.ToList();
                        foreach (PlanogramConsumerDecisionTreeNode childToRemove in childUnitsToRemove)
                        {
                            if (moveProductToParent)
                            {
                                //move any prodcuts from the actual unit being removed to the parent
                                List<PlanogramConsumerDecisionTreeNodeProduct> childAssignedProducts = childToRemove.Products.ToList();
                                parentUnit.Products.AddList(childAssignedProducts);
                                childToRemove.Products.RemoveList(childAssignedProducts);
                            }
                            //remove the child from its parent
                            parentUnit.ChildList.Remove(childToRemove);
                        }
                        parentUnit.ChildList.RaiseListChangedEvents = true;
                        parentUnit.ChildList.Reset();
                    }
                    if (lowestLevel.ChildLevel != null)
                    {
                        lowestLevel.RemoveLevel(lowestLevel.ChildLevel); 
                    }
                    lowestLevel = lowestLevel.ParentLevel;
                }

                //set the actual level to null
                levelToRemove.ParentLevel.RemoveLevel(levelToRemove);
                //levelToRemove.ParentLevel.ChildLevel = null;
            }
        }

        /// <summary>
        /// Returns the parent level of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public PlanogramConsumerDecisionTreeLevel GetLinkedLevel(PlanogramConsumerDecisionTreeNode node)
        {
            return this.FetchAllLevels().FirstOrDefault(u => u.Id.Equals(node.PlanogramConsumerDecisionTreeLevelId));
        }

        ///// <summary>
        ///// Returns the number of levels (of nodes) in this structure.
        ///// </summary>
        //public Int32 GetLevelCount()
        //{
        //    return RootNode.GetLevelCount();
        //}

        /// <summary>
        /// Returns a list of all product Gtins assigned in the cdt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Object> GetAssignedPlanogramProducts()
        {
            return
                this.RootNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts().Select(p => p.PlanogramProductId)
                .ToList();
        }

        /// <summary>
        /// Adds the provided tree's node under this tree's root
        /// </summary>
        /// <param name="tree"></param>
        public void MergeTree(PlanogramConsumerDecisionTree tree)
        {
            //Get/create the level to add the tree to
            PlanogramConsumerDecisionTreeLevel cdtMergeRootLevel = this.RootLevel.ChildLevel;
            if (cdtMergeRootLevel == null)
            {
                cdtMergeRootLevel = this.InsertNewChildLevel(this.RootLevel);
                cdtMergeRootLevel.Name = Message.PlanogramCdtMaintenance_CDTMergeLevel;
            }

            //Create the levels
            Dictionary<Int32, Int32> levelMatchedIds = cdtMergeRootLevel.MergeFrom(tree.RootLevel);

            //Create the nodes
            this.RootNode.MergeFrom(tree.RootNode);

            //Resolve the ids
            PlanogramConsumerDecisionTreeNode cdtMergeRootNode = this.RootNode.ChildList.LastOrDefault();
            if (cdtMergeRootNode != null)
            {
                List<PlanogramConsumerDecisionTreeNode> nodesToResolve = cdtMergeRootNode.FetchAllChildNodes().ToList();
                foreach (PlanogramConsumerDecisionTreeNode node in nodesToResolve)
                {
                    Int32 levelId = 0;
                    levelMatchedIds.TryGetValue((Int32)node.PlanogramConsumerDecisionTreeLevelId, out levelId);

                    if (levelId != 0)
                    {
                        node.PlanogramConsumerDecisionTreeLevelId = levelId;
                    }
                }
            }
        }

        #endregion

        #region IPlanogramConsumerDecisionTree members

        IPlanogramConsumerDecisionTreeLevel IPlanogramConsumerDecisionTree.RootLevel
        {
            get { return RootLevel as IPlanogramConsumerDecisionTreeLevel; }
        }

        IPlanogramConsumerDecisionTreeNode IPlanogramConsumerDecisionTree.RootNode
        {
            get { return RootNode as IPlanogramConsumerDecisionTreeNode; }
        }

        #endregion
    }
}
