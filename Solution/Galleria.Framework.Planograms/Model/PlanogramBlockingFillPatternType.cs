﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion


using Galleria.Framework.Planograms.Resources.Language;
using System;
using System.Collections.Generic;
namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramBlockingFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }


    public static class PlanogramBlockingFillPatternTypeHelper
    {
        public static readonly Dictionary<PlanogramBlockingFillPatternType, String> FriendlyNames =
           new Dictionary<PlanogramBlockingFillPatternType, String>()
            {
                {PlanogramBlockingFillPatternType.Solid, Message.Enum_PlanogramProductFillPatternType_Solid},
                {PlanogramBlockingFillPatternType.Border, Message.Enum_PlanogramProductFillPatternType_Border},
                {PlanogramBlockingFillPatternType.DiagonalUp, Message.Enum_PlanogramProductFillPatternType_DiagonalUp},
                {PlanogramBlockingFillPatternType.DiagonalDown, Message.Enum_PlanogramProductFillPatternType_DiagonalDown},
                {PlanogramBlockingFillPatternType.Crosshatch, Message.Enum_PlanogramProductFillPatternType_Crosshatch},
                {PlanogramBlockingFillPatternType.Horizontal, Message.Enum_PlanogramProductFillPatternType_Horizontal},
                {PlanogramBlockingFillPatternType.Vertical, Message.Enum_PlanogramProductFillPatternType_Vertical},
                {PlanogramBlockingFillPatternType.Dotted, Message.Enum_PlanogramProductFillPatternType_Dotted}
            };
    }
}
