﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26491 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added helper class.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductRuleType
    {
        None,
        Force,
        Preserve,
        MinimumHurdle,
        Core
    }

    public static class PlanogramAssortmentProductRuleTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductRuleType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentProductRuleType, String>()
            {
                {PlanogramAssortmentProductRuleType.None, Message.Enum_Generic_None},
                {PlanogramAssortmentProductRuleType.Force, Message.Enum_PlanogramAssortmentProductRuleType_Force},
                {PlanogramAssortmentProductRuleType.Preserve, Message.Enum_PlanogramAssortmentProductRuleType_Preserve},
                {PlanogramAssortmentProductRuleType.MinimumHurdle, Message.Enum_PlanogramAssortmentProductRuleType_MinimumHurdle},
                {PlanogramAssortmentProductRuleType.Core, Message.Enum_PlanogramAssortmentProductRuleType_Core}
            };
    }
}
