﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : M.Pettit
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum representing the different thypes of user that may apply
    /// a lock for a Planogram
    /// </summary>
    public enum PackageLockType : byte
    {
        Unknown = 0,
        User = 1,
        System = 2
    }

    /// <summary>
    /// Planogram Lock Type Helper Class.
    /// </summary>
    public static class PackageLockTypeHelper
    {
        /// <summary>
        /// Holds the dictionary mapping <see cref="PlanogramLockType" /> type values to friendly names.
        /// </summary>
        public static readonly Dictionary<PackageLockType, String> FriendlyNames = new Dictionary
            <PackageLockType, String>
        {
            {PackageLockType.User, Message.Enum_PackageLockType_User},
            {PackageLockType.System, Message.Enum_PackageLockType_System}
        };
    }
}

