﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-27510 : A.Kuszyk
//  Amended Remove() to check additional dividers for overlap before removing them.
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
#endregion

#region Version History: CCM802

// V8-29000 : A.Silva
//      Added call to PlanogramBlocking.OnBlockingChangeCompleted after a divider Add or Remove operation completes succesfully.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of BlockingDividers contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramBlockingDividerList : ModelList<PlanogramBlockingDividerList, PlanogramBlockingDivider>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get { return (PlanogramBlocking)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingDividerList NewPlanogramBlockingDividerList()
        {
            PlanogramBlockingDividerList item = new PlanogramBlockingDividerList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingDividerList NewPlanogramBlockingDividerList(IEnumerable<IPlanogramBlockingDivider> sourceList,IResolveContext context)
        {
            PlanogramBlockingDividerList item = new PlanogramBlockingDividerList();
            item.Create(sourceList, context);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IEnumerable<IPlanogramBlockingDivider> sourceList, IResolveContext context)
        {
            foreach (IPlanogramBlockingDivider div in sourceList)
            {
                this.Add(PlanogramBlockingDivider.NewPlanogramBlockingDivider(div, context));
            }

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        protected override PlanogramBlockingDivider AddNewCore()
        {
            PlanogramBlockingDivider divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider();
            this.Add(divider);
            return divider;
        }

        /// <summary>
        /// Adds a new divider to this list at the given position ensuring that groups and
        /// locations are created.
        /// </summary>
        /// <param name="x">The x position percent value between 0 and 1</param>
        /// <param name="y">The yposition percent value between 0 and 1</param>
        /// <param name="type">The type of divider to add</param>
        /// <returns></returns>
        public PlanogramBlockingDivider Add(Single x, Single y, PlanogramBlockingDividerType type)
        {
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null) return AddNew();   

            //create and add the new divider
            Single dividerX, dividerY, dividerLength;
            Byte dividerLevel;
            BlockingHelper.GetDividerValues(parentBlocking, x, y, type, out dividerX, out dividerY, out dividerLength, out dividerLevel);

            PlanogramBlockingDivider newDivider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(dividerLevel, type, dividerX, dividerY, dividerLength);
            this.Add(newDivider);

            if (parentBlocking.Locations.Count > 0)
            {
                //determine which locations the new divider intersects
                foreach (PlanogramBlockingLocation loc in parentBlocking.Locations.ToList())
                {
                    if (BlockingHelper.IntersectsWith(newDivider, loc))
                    {
                        //Add a new group
                        PlanogramBlockingGroup newGroup = parentBlocking.Groups.AddNew();
                        PlanogramBlockingLocation newLoc = parentBlocking.Locations.Add(newGroup);

                        //update the adjacent dividers of the locations
                        if (type == PlanogramBlockingDividerType.Vertical)
                        {
                            Object oldRightId = loc.PlanogramBlockingDividerRightId;
                            loc.PlanogramBlockingDividerRightId = newDivider.Id;

                            newLoc.PlanogramBlockingDividerLeftId = newDivider.Id;
                            newLoc.PlanogramBlockingDividerRightId = oldRightId;
                            newLoc.PlanogramBlockingDividerTopId = loc.PlanogramBlockingDividerTopId;
                            newLoc.PlanogramBlockingDividerBottomId = loc.PlanogramBlockingDividerBottomId;
                        }
                        else
                        {
                            Object oldTopId = loc.PlanogramBlockingDividerTopId;
                            loc.PlanogramBlockingDividerTopId = newDivider.Id;

                            newLoc.PlanogramBlockingDividerTopId = oldTopId;
                            newLoc.PlanogramBlockingDividerBottomId = newDivider.Id;
                            newLoc.PlanogramBlockingDividerLeftId = loc.PlanogramBlockingDividerLeftId;
                            newLoc.PlanogramBlockingDividerRightId = loc.PlanogramBlockingDividerRightId;
                        }
                    }
                }
            }
            else
            {
                //add our first 2 groups
                PlanogramBlockingGroup group1 = parentBlocking.Groups.AddNew();
                PlanogramBlockingLocation loc1 = parentBlocking.Locations.Add(group1);

                PlanogramBlockingGroup group2 = parentBlocking.Groups.AddNew();
                PlanogramBlockingLocation loc2 = parentBlocking.Locations.Add(group2);

                if (type == PlanogramBlockingDividerType.Vertical)
                {
                    loc1.PlanogramBlockingDividerRightId = newDivider.Id;
                    loc2.PlanogramBlockingDividerLeftId = newDivider.Id;
                }
                else
                {
                    loc1.PlanogramBlockingDividerTopId = newDivider.Id;
                    loc2.PlanogramBlockingDividerBottomId = newDivider.Id;
                }
            }

            parentBlocking.OnBlockingChangeCompleted();

            return newDivider;
        }

        /// <summary>
        /// Removes the given divider from the list
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public new Boolean Remove(PlanogramBlockingDivider divider)
        {
            if (divider == null) return false;
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null) return base.Remove(divider);
            if (!this.Contains(divider)) return false;

            PlanogramBlockingDivider boundingDividerBelow;
            PlanogramBlockingDivider boundingDividerAbove;
            GetBoundingDividers(divider, parentBlocking, out boundingDividerBelow, out boundingDividerAbove);
            IEnumerable<PlanogramBlockingDivider> dividersToRemove = parentBlocking.Dividers
                .Where(d => ShouldBeRemoved(d, boundingDividerBelow, boundingDividerAbove, divider)).ToList();
            RemoveRedundantLocations(divider, parentBlocking, dividersToRemove);
            RemoveDividersFromBase(dividersToRemove);
            TidyUpBlockingGroups(parentBlocking);
            UpdateDividerLengths(parentBlocking);
            parentBlocking.OnBlockingChangeCompleted();

            return true;
        }

        private bool ShouldBeRemoved(
            PlanogramBlockingDivider divider, 
            PlanogramBlockingDivider boundingDividerBelow, 
            PlanogramBlockingDivider boundingDividerAbove,
            PlanogramBlockingDivider dividerToRemove)
        {
            if (divider == dividerToRemove) return true;
            if (divider == boundingDividerBelow || divider == boundingDividerAbove) return false;
            Byte boundingDividerBelowLevel = boundingDividerBelow == null ? (Byte)0 : boundingDividerBelow.Level;
            Byte boundingDividerAboveLevel = boundingDividerAbove == null ? (Byte)0 : boundingDividerAbove.Level;
            Single boundingDividerBelowNormalCoordinate = boundingDividerBelow == null ? 0f : boundingDividerBelow.GetNormalCoordinate();
            Single boundingDividerAboveNormalCoordinate = boundingDividerAbove == null ? 1f : boundingDividerAbove.GetNormalCoordinate();

            Boolean directionsAreEqual = divider.Type == dividerToRemove.Type;
            Boolean dividerLevelIsGreaterThanBounds = divider.Level > Math.Max(boundingDividerBelowLevel, boundingDividerAboveLevel);
            Boolean dividerOverlapsWithBoundBelow = DividersOverlapInDirection(divider, boundingDividerBelow);
            Boolean dividerOverlapsWithBoundAbove = DividersOverlapInDirection(divider, boundingDividerAbove);
            Boolean dividerOverlapsWithDividerToRemove = DividersOverlapInDirection(divider, dividerToRemove);
            Boolean dividerIsAboveBoundBelow =
                directionsAreEqual ?
                divider.GetNormalCoordinate().GreaterOrEqualThan(boundingDividerBelowNormalCoordinate) :
                divider.GetDirectionCoordinate().GreaterOrEqualThan(boundingDividerBelowNormalCoordinate);
            Boolean dividerIsBelowBoundAbove = 
                directionsAreEqual ?
                divider.GetNormalCoordinate().LessOrEqualThan(boundingDividerAboveNormalCoordinate) :
                (divider.GetDirectionCoordinate() + divider.Length).LessOrEqualThan(boundingDividerAboveNormalCoordinate);

            return
                dividerLevelIsGreaterThanBounds &&
                dividerOverlapsWithBoundBelow &&
                dividerOverlapsWithBoundAbove &&
                dividerIsAboveBoundBelow &&
                dividerIsBelowBoundAbove &&
                dividerOverlapsWithDividerToRemove;
        }

        private void GetBoundingDividers(
            PlanogramBlockingDivider divider, 
            PlanogramBlocking parentBlocking, 
            out PlanogramBlockingDivider boundingDividerBelow, 
            out PlanogramBlockingDivider boundingDividerAbove)
        {
            boundingDividerAbove = parentBlocking.Dividers
                .Where(d =>
                    d.Type == divider.Type && // Dividers have to be of the same orientation as the divider we're removing.
                    d.Level <= divider.Level && // They need to be the same level or a level above.
                    DividersOverlapInDirection(d, divider) && // Dividers must overlap.
                    d.GetNormalCoordinate().GreaterThan(divider.GetNormalCoordinate())) // Dividers must be above.
                .OrderBy(d => d.GetNormalCoordinate())
                .FirstOrDefault();

            boundingDividerBelow = parentBlocking.Dividers
                .Where(d =>
                    d.Type == divider.Type && // Dividers have to be of the same orientation as the divider we're removing.
                    d.Level <= divider.Level && // They need to be the same level or a level above.
                    DividersOverlapInDirection(d, divider) && // Dividers must overlap.
                    d.GetNormalCoordinate().LessThan(divider.GetNormalCoordinate())) // Dividers must be above.
                .OrderByDescending(d => d.GetNormalCoordinate())
                .FirstOrDefault();
        }

        private Boolean DividersOverlapInDirection(PlanogramBlockingDivider divider1, PlanogramBlockingDivider divider2)
        {
            // If the divider is null, representing the boundary of the blocking space, then we definitly overlap.
            if (divider1 == null || divider2 == null) return true;

            // If the dividers are of a different type, then we need to check if they intersect.
            if (divider1.Type != divider2.Type)
            {
                return
                    divider1.GetNormalCoordinate().GreaterThan(divider2.GetDirectionCoordinate()) &&
                    divider1.GetNormalCoordinate().LessThan(divider2.GetDirectionCoordinate() + divider2.Length);
            }
            else
            {
                // If the dividers are of the same type, check to see if the dividers overlap in their direction.
                return
                    Math.Max(divider1.GetDirectionCoordinate(), divider2.GetDirectionCoordinate())
                    .LessThan(
                    Math.Min(divider1.GetDirectionCoordinate() + divider1.Length, divider2.GetDirectionCoordinate() + divider2.Length));
            }
        }

        private static void RemoveRedundantLocations(
            PlanogramBlockingDivider divider, 
            PlanogramBlocking parentBlocking, 
            IEnumerable<PlanogramBlockingDivider> dividersToRemove)
        {
            // Determine which locations will be affected by the removal of the dividers to be removed.
            IEnumerable<PlanogramBlockingLocation> locationsAffectedByDividerRemoval = parentBlocking.Locations
                .Where(l => dividersToRemove.Any(d => l.IsAssociated(d)))
                .ToList();
            if (!locationsAffectedByDividerRemoval.Any()) return;

            // Decide which of these locations to keep.
            PlanogramBlockingLocation survivingLocation = locationsAffectedByDividerRemoval
                .OrderByDescending(l => l.SpacePercentage)
                .First();
            
            // The locations to remove are all those affected, except the one we're keeping.
            IEnumerable<PlanogramBlockingLocation> locationsToRemove = locationsAffectedByDividerRemoval
                .Where(l => l != survivingLocation)
                .ToList();
            
            // Remove the locations from the blocking.
            parentBlocking.Locations.RemoveList(locationsToRemove);
            
            // Expand the surviving location to occupy the same dividers as all the locations that were removed.
            survivingLocation.ExpandToFill(locationsAffectedByDividerRemoval);
        }

        private void RemoveDividersFromBase(IEnumerable<PlanogramBlockingDivider> dividersToRemove)
        {
            //remove the dividers
            foreach (var div in dividersToRemove)
            {
                base.Remove(div);
            }
        }

        private static void TidyUpBlockingGroups(PlanogramBlocking parentBlocking)
        {
            //cycle through tidying up block groups
            foreach (PlanogramBlockingGroup group in parentBlocking.Groups.ToList())
            {
                if (group.GetBlockingLocations().Count == 0)
                {
                    parentBlocking.Groups.Remove(group);
                }
            }
        }

        private static void UpdateDividerLengths(PlanogramBlocking parentBlocking)
        {
            //update the lengths of all remaining dividers
            foreach (PlanogramBlockingDivider div in parentBlocking.Dividers)
            {
                div.UpdateLength();
            }
        }

        #endregion
    }
}