﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentLocationBuddy
    {
        #region Constructor
        private PlanogramAssortmentLocationBuddy() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortmentLocationBuddy Fetch(IDalContext dalContext, PlanogramAssortmentLocationBuddyDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortmentLocationBuddy>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentLocationBuddyDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            LoadProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty, (PlanogramAssortmentLocationBuddyTreatmentType)dto.TreatmentType);
            LoadProperty<String>(S1LocationCodeProperty, dto.S1LocationCode);
            LoadProperty<Single?>(S1PercentageProperty, dto.S1Percentage);
            LoadProperty<String>(S2LocationCodeProperty, dto.S2LocationCode);
            LoadProperty<Single?>(S2PercentageProperty, dto.S2Percentage);
            LoadProperty<String>(S3LocationCodeProperty, dto.S3LocationCode);
            LoadProperty<Single?>(S3PercentageProperty, dto.S3Percentage);
            LoadProperty<String>(S4LocationCodeProperty, dto.S4LocationCode);
            LoadProperty<Single?>(S4PercentageProperty, dto.S4Percentage);
            LoadProperty<String>(S5LocationCodeProperty, dto.S5LocationCode);
            LoadProperty<Single?>(S5PercentageProperty, dto.S5Percentage);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private PlanogramAssortmentLocationBuddyDto GetDataTransferObject(PlanogramAssortment parent)
        {
            return new PlanogramAssortmentLocationBuddyDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramAssortmentId = parent.Id,
                TreatmentType = (Byte)ReadProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty),
                LocationCode = ReadProperty<String>(LocationCodeProperty),
                S1LocationCode = ReadProperty<String>(S1LocationCodeProperty),
                S1Percentage = ReadProperty<Single?>(S1PercentageProperty),
                S2LocationCode = ReadProperty<String>(S2LocationCodeProperty),
                S2Percentage = ReadProperty<Single?>(S2PercentageProperty),
                S3LocationCode = ReadProperty<String>(S3LocationCodeProperty),
                S3Percentage = ReadProperty<Single?>(S3PercentageProperty),
                S4LocationCode = ReadProperty<String>(S4LocationCodeProperty),
                S4Percentage = ReadProperty<Single?>(S4PercentageProperty),
                S5LocationCode = ReadProperty<String>(S5LocationCodeProperty),
                S5Percentage = ReadProperty<Single?>(S5PercentageProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentLocationBuddyDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentLocationBuddyDto>(
            (dc) =>
            {
                PlanogramAssortmentLocationBuddyDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortmentLocationBuddy>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentLocationBuddyDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            batchContext.Delete<PlanogramAssortmentLocationBuddyDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
