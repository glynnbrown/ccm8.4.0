﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31945 : A.Silva
//  Added Display Property.

#endregion

#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Represents a specific field that is to be used when comparing planograms.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonField : ModelObject<PlanogramComparisonField>, IPlanogramComparisonSettingsField
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparison" />.
        /// </summary>
        public new PlanogramComparison Parent { get { return ((PlanogramComparisonFieldList) base.Parent).Parent; } }

        #endregion

        #region ItemType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="ItemType" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramItemType> ItemTypeProperty =
            RegisterModelProperty<PlanogramItemType>(c => c.ItemType);

        /// <summary>
        ///     The <see cref="PlanogramItemType"/> that this instance's field is.
        /// </summary>
        public PlanogramItemType ItemType { get { return GetProperty(ItemTypeProperty); } set { SetProperty(ItemTypeProperty, value); } }

        #endregion

        #region FieldPlaceholder

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldPlaceholderProperty = RegisterModelProperty<String>(c => c.FieldPlaceholder);

        /// <summary>
        ///     The internal field place holder used to evaluate the value in a given plan item.
        /// </summary>
        public String FieldPlaceholder { get { return GetProperty(FieldPlaceholderProperty); } set { SetProperty(FieldPlaceholderProperty, value); } }

        #endregion

        #region DisplayName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayNameProperty = RegisterModelProperty<String>(c => c.DisplayName);

        /// <summary>
        ///     The display name presented to the user on the UI.
        /// </summary>
        public String DisplayName { get { return GetProperty(DisplayNameProperty); } set { SetProperty(DisplayNameProperty, value); } }

        #endregion

        #region Number

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberProperty = RegisterModelProperty<Int16>(c => c.Number);

        /// <summary>
        ///     The order of this field in the collection of fields (to order columns in a datagrid and such).
        /// </summary>
        public Int16 Number { get { return GetProperty(NumberProperty); } set { SetProperty(NumberProperty, value); } }

        #endregion

        #region Display

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DisplayProperty = RegisterModelProperty<Boolean>(c => c.Display);

        /// <summary>
        ///     Whether this field should be displayed on the UI, when such thing is optional.
        /// </summary>
        public Boolean Display { get { return GetProperty(DisplayProperty); } set { SetProperty(DisplayProperty, value); } }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(FieldPlaceholderProperty));
            BusinessRules.AddRule(new Required(ItemTypeProperty));
            BusinessRules.AddRule(new MaxLength(FieldPlaceholderProperty, 1000));
            BusinessRules.AddRule(new MaxLength(DisplayNameProperty, 100));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonField" />.
        /// </summary>
        public static PlanogramComparisonField NewPlanogramComparisonField()
        {
            var item = new PlanogramComparisonField();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonField" /> derived from the given <paramref name="info" /> and
        ///     targeting the <paramref name="itemType" />.
        /// </summary>
        /// <param name="itemType"></param>
        /// <param name="info">The instance of <see cref="ObjectFieldInfo" /> from which to derive the new instance.</param>
        /// <param name="display">Whether the comparison field should be displayed on the grid by default or not.</param>
        public static PlanogramComparisonField NewPlanogramComparisonField(PlanogramItemType itemType, ObjectFieldInfo info, Boolean display)
        {
            var item = new PlanogramComparisonField();
            item.Create(itemType, info, display);
            return item;
        }

        public static PlanogramComparisonField NewPlanogramComparisonField(PlanogramItemType itemType, IPlanogramComparisonSettingsField field)
        {
            var item = new PlanogramComparisonField();
            item.Create(itemType, field);
            return item;
        }

        public static PlanogramComparisonField NewPlanogramComparisonField(IPlanogramComparisonSettingsField field)
        {
            var item = new PlanogramComparisonField();
            item.Create(field);
            return item;
        }

        #endregion

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Initialize all default property values for this instance using the given <paramref name="info" />.
        /// </summary>
        /// <param name="itemType"></param>
        /// <param name="info">The instance of <see cref="ObjectFieldInfo" /> from which to derive the new instance.</param>
        /// <param name="display"></param>
        private void Create(PlanogramItemType itemType, ObjectFieldInfo info, Boolean display)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ItemTypeProperty, itemType);
            LoadProperty(FieldPlaceholderProperty, info.FieldPlaceholder);
            LoadProperty(DisplayNameProperty, info.FieldFriendlyName);
            LoadProperty(DisplayProperty, display);
            MarkAsChild();
            base.Create();
        }

        private void Create(PlanogramItemType itemType, IPlanogramComparisonSettingsField field)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ItemTypeProperty, itemType);
            LoadProperty(FieldPlaceholderProperty, field.FieldPlaceholder);
            LoadProperty(DisplayNameProperty, field.DisplayName);
            LoadProperty(NumberProperty, field.Number);
            LoadProperty(DisplayProperty, field.Display);
            MarkAsChild();
            base.Create();
        }

        private void Create(IPlanogramComparisonSettingsField field)
        {
            Create(field.ItemType, field);
        }

        #endregion
    }
}