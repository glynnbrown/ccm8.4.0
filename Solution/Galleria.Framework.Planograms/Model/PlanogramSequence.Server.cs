﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Server side implementation for <see cref="PlanogramSequence" />.
    /// </summary>
    public sealed partial class PlanogramSequence
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to obtain new instances of <see cref="PlanogramSequence"/>.
        /// </summary>
        private PlanogramSequence() {}

        #endregion

        #region Factory Methods

        internal static PlanogramSequence Fetch(IDalContext dalContext, PlanogramSequenceDto dto)
        {
            return DataPortal.FetchChild<PlanogramSequence>(dalContext, dto);
        }

        public static PlanogramSequence Fetch(IDalContext dalContext, Object planogramId)
        {
            return DataPortal.FetchChild<PlanogramSequence>(new FetchByPlanogramIdCriteria(null, planogramId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a <see cref="PlanogramSequenceDto"/> from this <see cref="PlanogramSequence"/>.
        /// </summary>
        /// <param name="parent">The parent <see cref="Planogram"/> of this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramSequenceDto"/>.</returns>
        private PlanogramSequenceDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramSequenceDto()
            {
                Id = ReadProperty(IdProperty),
                PlanogramId = parent.Id,
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        /// <summary>
        ///     Loads into this instance the values from the <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramSequenceDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            try
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<IPlanogramSequenceDal>())
                    {
                        LoadDataTransferObject(dalContext, dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
            }
            catch (DtoDoesNotExistException)
            {
                Create();
            }
            MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramSequenceDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert(dc =>
            {
                PlanogramSequenceDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                LoadProperty(IdProperty, dto.Id);
                dc.RegisterId<PlanogramSequence>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }
        #endregion


        #endregion
    }
}