﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Component Merchandise Type Enum
    /// </summary>
    public enum PlanogramComponentMerchandiseType
    {
        Fresh = 0,
        Frozen = 1,
        Ambient = 2,
        HealthAndBeauty = 3,
        Electricals = 4,
        Clothing = 5,
        Display = 6
    }

    /// <summary>
    /// PlanogramComponentMerchandiseType Helper Class
    /// </summary>
    public static class PlanogramComponentMerchandiseTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramComponentMerchandiseType, String> FriendlyNames =
            new Dictionary<PlanogramComponentMerchandiseType, String>()
            {
                {PlanogramComponentMerchandiseType.Fresh, Message.Enum_PlanogramComponentMerchandiseType_Fresh},
                {PlanogramComponentMerchandiseType.Frozen, Message.Enum_PlanogramComponentMerchandiseType_Frozen},
                {PlanogramComponentMerchandiseType.Ambient, Message.Enum_PlanogramComponentMerchandiseType_Ambient},
                {PlanogramComponentMerchandiseType.HealthAndBeauty, Message.Enum_PlanogramComponentMerchandiseType_HealthAndBeauty},
                {PlanogramComponentMerchandiseType.Electricals, Message.Enum_PlanogramComponentMerchandiseType_Electricals},
                {PlanogramComponentMerchandiseType.Clothing, Message.Enum_PlanogramComponentMerchandiseType_Clothing},
                {PlanogramComponentMerchandiseType.Display, Message.Enum_PlanogramComponentMerchandiseType_Display},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramComponentMerchandiseType, String> FriendlyDescriptions =
            new Dictionary<PlanogramComponentMerchandiseType, String>()
            {
                {PlanogramComponentMerchandiseType.Fresh, Message.Enum_PlanogramComponentMerchandiseType_Fresh},
                {PlanogramComponentMerchandiseType.Frozen, Message.Enum_PlanogramComponentMerchandiseType_Frozen},
                {PlanogramComponentMerchandiseType.Ambient, Message.Enum_PlanogramComponentMerchandiseType_Ambient},
                {PlanogramComponentMerchandiseType.HealthAndBeauty, Message.Enum_PlanogramComponentMerchandiseType_HealthAndBeauty},
                {PlanogramComponentMerchandiseType.Electricals, Message.Enum_PlanogramComponentMerchandiseType_Electricals},
                {PlanogramComponentMerchandiseType.Clothing, Message.Enum_PlanogramComponentMerchandiseType_Clothing},
                {PlanogramComponentMerchandiseType.Display, Message.Enum_PlanogramComponentMerchandiseType_Display},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramComponentMerchandiseType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramComponentMerchandiseType>()
            {
                {Message.Enum_PlanogramComponentMerchandiseType_Fresh.ToLowerInvariant(), PlanogramComponentMerchandiseType.Fresh},
                {Message.Enum_PlanogramComponentMerchandiseType_Frozen.ToLowerInvariant(), PlanogramComponentMerchandiseType.Frozen},
                {Message.Enum_PlanogramComponentMerchandiseType_Ambient.ToLowerInvariant(), PlanogramComponentMerchandiseType.Ambient},
                {Message.Enum_PlanogramComponentMerchandiseType_HealthAndBeauty.ToLowerInvariant(), PlanogramComponentMerchandiseType.HealthAndBeauty},
                {Message.Enum_PlanogramComponentMerchandiseType_Electricals.ToLowerInvariant(), PlanogramComponentMerchandiseType.Electricals},
                {Message.Enum_PlanogramComponentMerchandiseType_Clothing.ToLowerInvariant(), PlanogramComponentMerchandiseType.Clothing},
                {Message.Enum_PlanogramComponentMerchandiseType_Display.ToLowerInvariant(), PlanogramComponentMerchandiseType.Display},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramComponentMerchandiseType? PlanogramComponentMerchandiseTypeGetEnum(String friendlyName)
        {
            PlanogramComponentMerchandiseType? returnValue = null;
            PlanogramComponentMerchandiseType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}