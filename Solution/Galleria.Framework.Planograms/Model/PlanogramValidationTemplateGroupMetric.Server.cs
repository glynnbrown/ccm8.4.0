﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva ~ Added result Property.
// V8-26812 : A.Silva ~ Added ValidationType Property.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Server side implementation.
    /// </summary>
    public partial class PlanogramValidationTemplateGroupMetric
    {
        #region Constructor

        /// <summary>
        ///     Private constructor, use factory methods to create new instances of the <see cref="PlanogramValidationTemplateGroupMetric"/> class.
        /// </summary>
        private PlanogramValidationTemplateGroupMetric()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an item from a dto
        /// </summary>
        internal static PlanogramValidationTemplateGroupMetric Fetch(IDalContext dalContext,
            PlanogramValidationTemplateGroupMetricDto dto)
        {
            return DataPortal.FetchChild<PlanogramValidationTemplateGroupMetric>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        private PlanogramValidationTemplateGroupMetricDto GetDataTransferObject(PlanogramValidationTemplateGroup parent)
        {
            return new PlanogramValidationTemplateGroupMetricDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramValidationTemplateGroupId = parent.Id,
                Field = ReadProperty(FieldProperty),
                Threshold1 = ReadProperty(Threshold1Property),
                Threshold2 = ReadProperty(Threshold2Property),
                Score1 = ReadProperty(Score1Property),
                Score2 = ReadProperty(Score2Property),
                Score3 = ReadProperty(Score3Property),
                AggregationType = (Byte) ReadProperty(AggregationTypeProperty),
                ResultType = (Byte) ReadProperty(ResultTypeProperty),
                ValidationType = (Byte) ReadProperty(ValidationTypeProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty),
                Criteria = ReadProperty(CriteriaProperty)
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationTemplateGroupMetricDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(FieldProperty, dto.Field);
            LoadProperty(Threshold1Property, dto.Threshold1);
            LoadProperty(Threshold2Property, dto.Threshold2);
            LoadProperty(Score1Property, dto.Score1);
            LoadProperty(Score2Property, dto.Score2);
            LoadProperty(Score3Property, dto.Score3);
            LoadProperty(AggregationTypeProperty, (PlanogramValidationAggregationType)dto.AggregationType);
            LoadProperty(ResultTypeProperty, (PlanogramValidationAggregationType)dto.ResultType);
            LoadProperty(ValidationTypeProperty, (PlanogramValidationType) dto.ValidationType);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
            LoadProperty(CriteriaProperty, dto.Criteria);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramValidationTemplateGroupMetricDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramValidationTemplateGroup parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramValidationTemplateGroupMetricDto>(
            (dc) =>
            {
                PlanogramValidationTemplateGroupMetricDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty(IdProperty, dto.Id);
                dc.RegisterId<PlanogramValidationTemplateGroupMetric>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramValidationTemplateGroup parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramValidationTemplateGroupMetricDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramValidationTemplateGroup parent)
        {
            batchContext.Delete<PlanogramValidationTemplateGroupMetricDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}