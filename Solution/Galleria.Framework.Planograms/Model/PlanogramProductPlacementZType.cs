﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27154 : L.Luong
//  Created
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available Product Placement Z types
    /// </summary>
    public enum PlanogramProductPlacementZType
    {
        Manual = 0,
        FillDeep = 1
    }

    public static class PlanogramProductPlacementZTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductPlacementZType, String> FriendlyNames =
            new Dictionary<PlanogramProductPlacementZType, String>()
            {
                {PlanogramProductPlacementZType.Manual, Message.Enum_PlanogramProductPlacementZType_Manual},
                {PlanogramProductPlacementZType.FillDeep, Message.Enum_PlanogramProductPlacementZType_FillDeep},
            };
    }
}
