﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of Components contained within an assembly
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAssemblyComponentList : ModelList<PlanogramAssemblyComponentList, PlanogramAssemblyComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssembly Parent
        {
            get { return (PlanogramAssembly)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        public static PlanogramAssemblyComponentList NewPlanogramAssemblyComponentList()
        {
            PlanogramAssemblyComponentList item = new PlanogramAssemblyComponentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void  Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Creates and adds a new component to this list
        /// </summary>
        public PlanogramAssemblyComponent Add(PlanogramComponentType componentType,
            Single width, Single height, Single depth, Int32 fillColour = -16777216)
        {
            PlanogramAssembly assembly = this.Parent;
            if (assembly == null) return null;
            Planogram planogram = assembly.Parent;
            if (planogram == null) return null;

            PlanogramComponent component = planogram.Components.Add(componentType, width, height, depth, fillColour);
            PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(component);
            this.Add(assemblyComponent);
            return assemblyComponent;
        }

        /// <summary>
        /// Creates an adds a new component to this list
        /// </summary>
        public PlanogramAssemblyComponent Add(PlanogramComponentType componentType, IPlanogramSettings settings)
        {
            PlanogramAssembly assembly = this.Parent;
            if (assembly == null) return null;
            Planogram planogram = assembly.Parent;
            if (planogram == null) return null;

            PlanogramComponent component = planogram.Components.Add(componentType, settings);
            PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(component);
            this.Add(assemblyComponent);
            return assemblyComponent;
        }

        #endregion

        #endregion
    }
}
