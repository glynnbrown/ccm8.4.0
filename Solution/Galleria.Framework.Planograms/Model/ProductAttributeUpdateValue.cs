#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32359 : A.Silva
//  Created.
// V8-32808 : A.Silva
//  Added GetUnderlyingTypeCode so that nullable types are correctly identified by their underlying type.

#endregion

#endregion
using System;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Parameter class to help in Product Attribute updates.
    /// </summary>
    public class ProductAttributeUpdateValue
    {
        private Boolean? _isCustomAttribute;
        private String _basePropertyName;

        #region Properties

        /// <summary>
        ///     The friendly name of the property to be updated.
        /// </summary>
        public String Header { get; set; }

        /// <summary>
        ///     The name of the property to be updated.
        /// </summary>
        /// 
        public String PropertyName { get; private set; }

        /// <summary>
        ///     The value to update the propety to.
        /// </summary>
        public Object NewValue { get; set; }

        public Type PropertyType { get; private set; }

        public ModelPropertyDisplayType PropertyDisplayType { get; private set; }

        public Boolean IsCustomAttribute
        {
            get { return (Boolean) (_isCustomAttribute ?? (_isCustomAttribute = PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))); }
        }

        public String BasePropertyName
        {
            get {
                return _basePropertyName ??
                       (_basePropertyName = PropertyName.Replace(String.Format("{0}.", PlanogramProduct.CustomAttributesProperty.Name), String.Empty));
            }
        }

        #endregion

        #region Private Constructor

        private ProductAttributeUpdateValue() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Initialize a new instance of <see cref="ProductAttributeUpdateValue"/>.
        /// </summary>
        /// <param name="info">The data to create the new instance from.</param>
        /// <returns></returns>
        public static ProductAttributeUpdateValue CreateNewUpdateValue(ObjectFieldInfo info)
        {
            return new ProductAttributeUpdateValue
                   {
                       Header = info.FieldFriendlyName,
                       PropertyName = info.PropertyName,
                       PropertyDisplayType = info.PropertyDisplayType,
                       PropertyType = info.PropertyType,
                       NewValue = GetDefaultValue(info)
                   };
        }

        public static ProductAttributeUpdateValue CreateNewUpdateValue(String propertyName, Object newValue)
        {
            return new ProductAttributeUpdateValue
            {
                Header = propertyName,
                PropertyName = propertyName,
                PropertyDisplayType = ModelPropertyDisplayType.None,
                PropertyType = typeof(Object),
                NewValue = newValue
            };
        }

        #endregion
        
        /// <summary>
        ///     Gets the underlying type code for the value.
        /// </summary>
        /// <returns></returns>
        /// <remarks>Nullable types are reported as the underlying types, and not as Object.</remarks>
        public TypeCode GetUnderlyingTypeCode()
        {
            Type type = PropertyType;
            if (type.IsGenericType &&
                type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);
            TypeCode typeCode = Type.GetTypeCode(type);
            return typeCode;
        }

        #region Static Helper Methods

        private static Object GetDefaultValue(ObjectFieldInfo info)
        {
            if (!info.PropertyType.IsValueType ||
                Nullable.GetUnderlyingType(info.PropertyType) != null)
            {
                return null;
            }

            switch (Type.GetTypeCode(info.PropertyType))
            {
                case TypeCode.Boolean:
                    return false;
                case TypeCode.Char:
                    return Char.MinValue;
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return !info.PropertyType.IsEnum ? Convert.ChangeType(0, info.PropertyType) : Activator.CreateInstance(info.PropertyType);
                case TypeCode.DateTime:
                    return DateTime.UtcNow;
                case TypeCode.String:
                    return String.Empty;
            }

            return null;
        }

        #endregion
    }
}