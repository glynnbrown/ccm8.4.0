﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-27126 : L.Ineson
//  Added UpdateSizeFromComponents
// V8-27058 : A.Probyn
//  Added CalculateMetaData
//  Added ClearMetaData
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
#endregion
#region Version History: CCM803
// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
#endregion
#region Version History: CCM810
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramAssembly object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAssembly : ModelObject<PlanogramAssembly>
    {
        #region Constructors
        static PlanogramAssembly()
        {
            FriendlyName = Message.PlanogramAssembly_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramAssemblyList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PlanogramAssembly_Name);
        /// <summary>
        /// The assembly name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.PlanogramAssembly_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The assembly height
        /// </summary>
        public Single Height
        {
            get { return this.GetProperty<Single>(HeightProperty); }
            set { this.SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramAssembly_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The assembly width
        /// </summary>
        public Single Width
        {
            get { return this.GetProperty<Single>(WidthProperty); }
            set { this.SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramAssembly_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The assembly depth
        /// </summary>
        public Single Depth
        {
            get { return this.GetProperty<Single>(DepthProperty); }
            set { this.SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssemblyComponentList> ComponentsProperty =
            RegisterModelProperty<PlanogramAssemblyComponentList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child assembly components list
        /// </summary>
        public PlanogramAssemblyComponentList Components
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssemblyComponentList>(
                    ComponentsProperty,
                    new PlanogramAssemblyComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child assembly components list
        /// </summary>
        public PlanogramAssemblyComponentList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssemblyComponentList>(
                    ComponentsProperty,
                    new PlanogramAssemblyComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region NF - TODO - The following properties need review

        #region Number Of Components
        /// <summary>
        /// NumberOfComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfComponentsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfComponents);
        /// <summary>
        /// The PlanogramAssembly NumberOfComponents
        /// </summary>
        public Int16 NumberOfComponents
        {
            get { return this.GetProperty<Int16>(NumberOfComponentsProperty); }
            private set { this.SetProperty<Int16>(NumberOfComponentsProperty, value); }
        }
        #endregion

        #region TotalComponentCost
        /// <summary>
        /// TotalComponentCost property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TotalComponentCostProperty =
            RegisterModelProperty<Single>(c => c.TotalComponentCost, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// The PlanogramAssembly TotalComponentCost
        /// </summary>
        public Single TotalComponentCost
        {
            get { return this.GetProperty<Single>(TotalComponentCostProperty); }
            set { this.SetProperty<Single>(TotalComponentCostProperty, value); }
        }
        #endregion

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new Required(HeightProperty));
            BusinessRules.AddRule(new Required(WidthProperty));
            BusinessRules.AddRule(new Required(DepthProperty));
            BusinessRules.AddRule(new Required(NumberOfComponentsProperty));
            BusinessRules.AddRule(new Required(TotalComponentCostProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssembly NewPlanogramAssembly()
        {
            PlanogramAssembly item = new PlanogramAssembly();
            item.Create(Message.PlanogramAssembly_Name_Default);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssembly NewPlanogramAssembly(String assemblyName)
        {
            PlanogramAssembly item = new PlanogramAssembly();
            item.Create(assemblyName);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected void Create(String assemblyName)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, assemblyName);
            this.LoadProperty<PlanogramAssemblyComponentList>(ComponentsProperty, PlanogramAssemblyComponentList.NewPlanogramAssemblyComponentList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramAssembly>(oldId, newId);
        }

        /// <summary>
        /// Returns all sub component placements
        /// associated with this assembly
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.Assembly))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        /// <summary>
        /// Updates the Height, Width and Depth values of this assembly
        /// based on its child components.
        /// </summary>
        public void UpdateSizeFromComponents()
        {
            //only update the value if it has child components.
            if (this.Components.Count == 0) return;

            RectValue bounds = new RectValue();

            //union the bounds of the child subcomponents.
            foreach (PlanogramAssemblyComponent ac in this.Components)
            {
                PlanogramComponent component = ac.GetPlanogramComponent();
                if (component == null) continue;

                RectValue childBounds = new RectValue(0, 0, 0,
                    component.Width, component.Height, component.Depth);

                MatrixValue childTransform =
                    MatrixValue.CreateTransformationMatrix(
                    ac.X, ac.Y, ac.Z, ac.Angle, ac.Slope, ac.Roll);

                childBounds = childTransform.TransformBounds(childBounds);

                bounds = bounds.Union(childBounds);
            }

            if (this.Width != bounds.Width) this.Width = bounds.Width;
            if (this.Height != bounds.Height) this.Height = bounds.Height;
            if (this.Depth != bounds.Depth) this.Depth = bounds.Depth;

        }

        /// <summary>
        /// Enumerates through all fixture assemblies associated with this assembly
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramFixtureAssembly> EnumeratePlanogramFixtureAssemblies()
        {
            Planogram parentPlan = this.Parent;
            if (parentPlan == null) yield break;

            foreach (PlanogramFixture fixture in parentPlan.Fixtures)
            {
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies)
                {
                    if (Object.Equals(this.Id, fixtureAssembly.PlanogramAssemblyId))
                    {
                        yield return fixtureAssembly;
                    }
                }
            }
        }


        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(PlanogramAssembly);
            String typeFriendly = PlanogramAssembly.FriendlyName;

            String detailsGroup = Message.PlanogramAssembly_PropertyGroup_Details;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NumberOfComponentsProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);
        }

        /// <summary>
        /// Returns the value of the given field for this .
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            return field.GetValue(this);
        }

        #region Metadata

        /// <summary>
        /// Override of base calcualte metadata to make
        /// sure items are loaded.
        /// </summary>
        protected override void CalculateMetadata()
        {
            base.CalculateMetadata(
                new Object[]
                {
                    this.Components
                });
        }

        /// <summary>
        /// Override of the clear metadata
        /// </summary>
        protected override void ClearMetadata()
        {
            base.ClearMetadata(
                new Object[]
                {
                    this.Components
                });
        }

        #endregion

        #endregion
    }
}