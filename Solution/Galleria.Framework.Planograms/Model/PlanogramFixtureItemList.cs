﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-26986 : A.Kuszyk
//  Added Get Blocking methods.
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
//  Removed TotalFixtureWidth property.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{        
    /// <summary>
    /// A list of fixture items contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramFixtureItemList : ModelList<PlanogramFixtureItemList, PlanogramFixtureItem>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return base.Parent as Planogram; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureItemList NewPlanogramFixtureItemList()
        {
            PlanogramFixtureItemList item = new PlanogramFixtureItemList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add()
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add());
            this.Add(item);
            return item;

            //// get the next fixture number
            //Int32 fixtureNo = this.Count + 1;

            //// TODO - Assign appropriate defaults
            //// add a fixture
            //PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture(String.Format(Message.PlanogramFixture_Name_New, fixtureNo));
            //fixture.Height = 191;
            //fixture.Width = 120;
            //fixture.Depth = 100;
            //this.Parent.Fixtures.Add(fixture);

            //// add a fixture item
            //PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            //fixtureItem.PlanogramFixtureId = fixture.Id;
            //fixtureItem.BaySequenceNumber = (Int16)fixtureNo;
            //this.Add(fixtureItem);

            //// return the fixture item
            //return fixtureItem;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramFixtureItem Add(PlanogramFixture fixture, Int16 baySequenceNumber)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                fixture,
                baySequenceNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(String name, Int16 baySequenceNumber)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(name),
                baySequenceNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(Int16 baySequenceNumber, IPlanogramSettings settings)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(settings),
                baySequenceNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(String name, Int16 baySequenceNumber, IPlanogramSettings settings)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(name, settings),
                baySequenceNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramFixtureItem Add(PlanogramFixture fixture)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                fixture,
                (Int16)(this.Count + 1));
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(String name)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(name),
                (Int16)(this.Count + 1));
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(IPlanogramSettings settings)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(settings),
                (Int16)(this.Count + 1));
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramFixtureItem Add(String name, IPlanogramSettings settings)
        {
            PlanogramFixtureItem item = PlanogramFixtureItem.NewPlanogramFixtureItem(
                this.Parent.Fixtures.Add(name, settings),
                (Int16)(this.Count));
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Gets a list of values that each represent the boundaries of the fixture items in this list.
        /// Each value is the percentage of the total width occupied by the boundary. This list does not
        /// contain the outer boundaries (i.e. 0 and 1).
        /// </summary>
        /// <remarks>
        /// e.g. Three equally spaced fixtures would be represented by the return value as 0.33,0.66,1.00.
        /// </remarks>
        public IEnumerable<Single> GetRelativeFixtureBoundaries()
        {
            if (Parent == null) throw new InvalidOperationException("Parent cannot be null");

            var relativeBoundaries = new List<Single>();
            Single totalFixtureHeight;
            Single totalFixtureWidth;
            Parent.GetBlockingAreaSize(out totalFixtureHeight, out totalFixtureWidth);
            Single cumulativeFixtureWidth = 0;
            foreach (var fixtureItem in this.OrderBy(f=>f.BaySequenceNumber))
            {
                // Increase the cumulative width by the items actual width.
                cumulativeFixtureWidth += fixtureItem.GetPlanogramFixture().Width;
                // Add the percentage position of the left boundary.
                var newBoundary = cumulativeFixtureWidth / totalFixtureWidth;
                if (Math.Round(newBoundary,3) == Math.Round(1.000f)) break;
                relativeBoundaries.Add(newBoundary);
            }
            return relativeBoundaries.Distinct();
        }

        #endregion
    }
}