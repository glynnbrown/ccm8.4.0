﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// CCM-28622 : D.Pleasance
//  Created
#endregion
#region Version History: CCM810
// V8-30215 : M.Brumby
//  Added AggregationType
#endregion
#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A simple model used to represent a mapping of metrics within a planogram.
    /// </summary>
    [Serializable]
    public sealed class PlanogramMetricMapping : ModelReadOnlyObject<PlanogramMetricMapping>
    {
        #region Properties
        
        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricDirectionType> DirectionProperty =
            RegisterModelProperty<MetricDirectionType>(c => c.Direction);
        /// <summary>
        /// Direction
        /// </summary>
        public MetricDirectionType Direction
        {
            get { return GetProperty<MetricDirectionType>(DirectionProperty); }
        }
        #endregion

        #region SpecialType
        /// <summary>
        /// SpecialType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricSpecialType> SpecialTypeProperty =
            RegisterModelProperty<MetricSpecialType>(c => c.SpecialType);
        /// <summary>
        /// SpecialType
        /// </summary>
        public MetricSpecialType SpecialType
        {
            get { return GetProperty<MetricSpecialType>(SpecialTypeProperty); }
        }
        #endregion

        #region MetricType
        /// <summary>
        /// MetricType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricType> MetricTypeProperty =
            RegisterModelProperty<MetricType>(c => c.MetricType);
        /// <summary>
        /// MetricType
        /// </summary>
        public MetricType MetricType
        {
            get { return GetProperty<MetricType>(MetricTypeProperty); }
        }
        #endregion

        #region MetricId
        /// <summary>
        /// MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MetricIdProperty =
            RegisterModelProperty<Byte>(c => c.MetricId);
        /// <summary>
        /// MetricId
        /// </summary>
        public Byte MetricId
        {
            get { return GetProperty<Byte>(MetricIdProperty); }
        }
        #endregion

        #region Source
        /// <summary>
        /// Source property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceProperty =
            RegisterModelProperty<String>(c => c.Source);
        /// <summary>
        /// Gets or sets the source field
        /// </summary>
        public String Source
        {
            get { return this.GetProperty<String>(SourceProperty); }
        }
        #endregion

        #region Aggregation Type

        /// <summary>
        /// Aggregation Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<AggregationType> AggregationTypeProperty =
            RegisterModelProperty<AggregationType>(c => c.AggregationType);
        /// <summary>
        /// Aggregation Type
        /// </summary>
        public AggregationType AggregationType
        {
            get { return GetProperty<AggregationType>(AggregationTypeProperty); }
        }

        #endregion
        #endregion

        #region Constructor
        private PlanogramMetricMapping() { } //force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramMetricMapping NewPlanogramMetricMapping(String name, String description, MetricDirectionType direction, MetricSpecialType specialType,
                                                                        MetricType metricType, AggregationType aggregationType, Byte metricId, String source)
        {
            PlanogramMetricMapping item = new PlanogramMetricMapping();
            item.Create(name, description, direction, specialType, metricType, aggregationType, metricId, source);
            return item;
        }
        #endregion

        #region Data Access

        #region Data Transfer object
        public PlanogramMetricMappingDto GetDataTransferObject()
        {
            return new PlanogramMetricMappingDto()
            {
                Name = this.ReadProperty<String>(NameProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                Direction = (Byte)this.ReadProperty<MetricDirectionType>(DirectionProperty),
                SpecialType = (Byte)this.ReadProperty<MetricSpecialType>(SpecialTypeProperty),
                MetricType = (Byte)this.ReadProperty<MetricType>(MetricTypeProperty),
                MetricId = this.ReadProperty<Byte>(MetricIdProperty),
                Source = ReadProperty<String>(SourceProperty),
                AggregationType = (Byte)ReadProperty<AggregationType>(AggregationTypeProperty),
            };
        }
        #endregion

        #region Create
        /// <summary>
        /// Called when creating a new item.
        /// </summary>
        private void Create(String name, String description, MetricDirectionType direction, MetricSpecialType specialType, MetricType metricType, AggregationType aggregationType, Byte metricId, String source)
        {
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<String>(DescriptionProperty, description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, specialType);
            this.LoadProperty<MetricType>(MetricTypeProperty, metricType);
            this.LoadProperty<Byte>(MetricIdProperty, metricId);
            this.LoadProperty<String>(SourceProperty, source);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, aggregationType);
        }
        #endregion

        #endregion
    }
}