﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created
// V8-29000 : A.Silva
//      Added NewPlanogramSequenceGroup to create an instance from an IPlanogramBlockingGroup.
// V8-29072 : A.Kuszyk
//  Added additional constructor that takes a PlanogramSequenceGroup as a source.
// V8-28999 : A.Silva
//      Amended performance of SequenceProducts and added EnumerateSnakeSequence to get the snake sequence for products in the sequence group.
// V8-29105 : A.Silva
//      Added GetPreviousSequenceProduct to return the previous product in the sequence for a given GTIN.
// V8-29232 : A.Silva
//      Amended SequenceProducts to not store the SequenceNumber in the PositionSequenceNumber field of the PlanogramPosition.
// V8-29015 : A.Silva
//      Refactored GetAdjacentPositionBySequence to be an instance method of PlanogramSequenceGroup and made GetPreviousSequenceProduct and GetNextSequenceProduct internal.
// V8-29198 : A.Silva
//      Refactored EnumerateSubComponentsInSequence to be an instance method of PlanogramBlockingGroup.
//      Refactored GetBlockingGroup to require the actual Blocking instead of figuring it out itself.
// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.

#endregion

#region Version History: CCM 810

// V8-30112 : A.Silva
//  Amended EnumerateSequenceSnake to ensure correct snaking between components on the same group tier.

#endregion

#region Version History : CCM820
// V8-30795 : A.Kuszyk
//  Exposed EnumerateSequenceSnake and made generic for use with different types of position.
// V8-31284 : D.Pleasance
//  Added GetAdjacentPositionsBySequence
#endregion

#region Version History: CCM830
// V8-32089 : N.Haywood
//  Added setting product for product display row
// V8-32504 : A.Kuszyk
//  Added sub groups.
// V8-32661 : A.Kuszyk
//  Added child changed event handler to take care of unique sub-group names and empty sub-groups.
// V8-32830 : A.Kuszyk
//  Added equality comparer to ensure sequencing coordinates are only compared to 1dp.
// V8-32882 : A.Kuszyk
//  Made GetAdjacentPositionsBySequence aware of sequence sub-groups.
// V8-32884 : A.Kuszyk
//  Added business rule for Name length, in line with database schema.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using System.Collections.Specialized;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     A sequence group color coded.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSequenceGroup : ModelObject<PlanogramSequenceGroup>
    {
        #region Parent

        /// <summary>
        ///     Gets the parent <see cref="PlanogramSequence" /> for this instance.
        /// </summary>
        public new PlanogramSequence Parent
        {
            get 
            {
                if (base.Parent == null) return null;
                return ((PlanogramSequenceGroupList) base.Parent).Parent; 
            }
        }

        #endregion

        #region Nested Types

        /// <summary>
        /// An equality comparer for use when grouping sequencable items that compares singles to 2dp.
        /// </summary>
        public class SequencableItemCoordinateComparer : IEqualityComparer<Single>
        {
            /// <summary>
            /// The level of precision to use when comparing Singles in this case.
            /// </summary>
            private const Int32 _precision = 1;

            public bool Equals(Single x, Single y)
            {
                return x.EqualTo(y, _precision);
            }

            public int GetHashCode(Single obj)
            {
                // Always return the same value in order to force the use of the Equals method.
                return 1;
            }
        }

        #endregion

        #region Properties

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<string> NameProperty =
            RegisterModelProperty<string>(o => o.Name);

        /// <summary>
        ///     Gets or sets the name of this instance.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Colour

        /// <summary>
        ///     Metadata for the <see cref="Colour" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<int> ColourProperty =
            RegisterModelProperty<int>(o => o.Colour);

        /// <summary>
        ///     Gets or sets colour associated with this instance.
        /// </summary>
        public Int32 Colour
        {
            get { return GetProperty(ColourProperty); }
            set { SetProperty(ColourProperty, value); }
        }

        #endregion

        #region Products

        /// <summary>
        ///     Metadata for the <see cref="Products" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramSequenceGroupProductList> ProductsProperty =
            RegisterModelProperty<PlanogramSequenceGroupProductList>(o => o.Products, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     Gets the <see cref="PlanogramSequenceGroupProduct"/> collection in this instance.
        /// </summary>
        public PlanogramSequenceGroupProductList Products
        {
            get
            {
                return GetPropertyLazy(ProductsProperty,
                    new ModelList<PlanogramSequenceGroupProductList, PlanogramSequenceGroupProduct>.FetchByParentIdCriteria(DalFactoryName, Id), true);
            }
        }

        /// <summary>
        ///     Gets asynchronously the <see cref="PlanogramSequenceGroupProduct"/> collection in this instance.
        /// </summary>
        public PlanogramSequenceGroupProductList ProductsAsync
        {
            get
            {
                return GetPropertyLazy(ProductsProperty,
                    new ModelList<PlanogramSequenceGroupProductList, PlanogramSequenceGroupProduct>.FetchByParentIdCriteria(DalFactoryName, Id), false);
            }
        }

        #endregion

        #region SubGroups

        /// <summary>
        ///     Metadata for the <see cref="SubGroups" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramSequenceGroupSubGroupList> SubGroupsProperty =
            RegisterModelProperty<PlanogramSequenceGroupSubGroupList>(o => o.SubGroups, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     Gets the <see cref="PlanogramSequenceGroupSubGroup"/> collection in this instance.
        /// </summary>
        public PlanogramSequenceGroupSubGroupList SubGroups
        {
            get
            {
                return GetPropertyLazy(SubGroupsProperty,
                    new ModelList<PlanogramSequenceGroupSubGroupList, PlanogramSequenceGroupSubGroup>.FetchByParentIdCriteria(DalFactoryName, Id), true);
            }
        }

        /// <summary>
        ///     Gets asynchronously the <see cref="PlanogramSequenceGroupSubGroup"/> collection in this instance.
        /// </summary>
        public PlanogramSequenceGroupSubGroupList SubGroupsAsync
        {
            get
            {
                return GetPropertyLazy(SubGroupsProperty,
                    new ModelList<PlanogramSequenceGroupSubGroupList, PlanogramSequenceGroupSubGroup>.FetchByParentIdCriteria(DalFactoryName, Id), false);
            }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Csla.Rules.CommonRules.MaxLength(NameProperty, 50));
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Define the authorization rules for <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //TODO Implement AddObjectAuthorizationRules for PlanogramSequenceGroup.
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        public static PlanogramSequenceGroup NewPlanogramSequenceGroup()
        {
            var item = new PlanogramSequenceGroup();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroup"/> using the<paramref name="source"/> to initialize it.
        /// </summary>
        /// <param name="source">The instance of <see cref="IPlanogramBlockingGroup"/> used to initialize the new one.</param>
        public static PlanogramSequenceGroup NewPlanogramSequenceGroup(IPlanogramBlockingGroup source)
        {
            var item = new PlanogramSequenceGroup();
            item.Create(source);
            return item;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroup"/> using the<paramref name="source"/> to initialize it.
        /// </summary>
        public static PlanogramSequenceGroup NewPlanogramSequenceGroup(PlanogramSequenceGroup source)
        {
            var item = new PlanogramSequenceGroup();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Invoked when creating a new instance of <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ProductsProperty, PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList());
            LoadProperty(SubGroupsProperty, PlanogramSequenceGroupSubGroupList.NewPlanogramSequenceGroupSubGroupList());
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Invoked when creating a new instance of <see cref="PlanogramSequenceGroup"/> from a <paramref name="source"/>.
        /// </summary>
        private void Create(IPlanogramBlockingGroup source)
        {
            LoadProperty(NameProperty, source.Name);
            LoadProperty(ColourProperty, source.Colour);
            Create();
        }

        private void Create(PlanogramSequenceGroup source)
        {
            LoadProperty(NameProperty, source.Name);
            LoadProperty(ColourProperty, source.Colour);
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ProductsProperty, PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList());

            Dictionary<Object, Object> newSubGroupIdsByOldId = new Dictionary<object, object>();
            foreach (PlanogramSequenceGroupSubGroup subGroup in source.SubGroups)
            {
                PlanogramSequenceGroupSubGroup newSubGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(subGroup);
                this.SubGroups.Add(newSubGroup);
                newSubGroupIdsByOldId.Add(subGroup.Id, newSubGroup.Id);
            }

            foreach (PlanogramSequenceGroupProduct sourceProduct in source.Products)
            {
                PlanogramSequenceGroupProduct newProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                newProduct.Gtin = sourceProduct.Gtin;
                newProduct.SequenceNumber = sourceProduct.SequenceNumber;
                newProduct.Product = sourceProduct.Product;
                if (sourceProduct.PlanogramSequenceGroupSubGroupId != null)
                {
                    Object subGroupId;
                    if (newSubGroupIdsByOldId.TryGetValue(sourceProduct.PlanogramSequenceGroupSubGroupId, out subGroupId))
                    {
                        newProduct.PlanogramSequenceGroupSubGroupId = subGroupId;
                    }
                }
                Products.Add(newProduct);
            }
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramSequenceGroup>(oldId, newId);
        }

        /// <summary>
        /// Returns true if this <see cref="PlanogramSequenceGroup"/>'s <see cref="Colour"/> is represented
        /// in the given <paramref name="blocking"/>.
        /// </summary>
        /// <param name="blocking"></param>
        /// <returns></returns>
        public Boolean IsRepresentedIn(PlanogramBlocking blocking)
        {
            return blocking.Groups.Any(g => g.Colour.Equals(this.Colour));
        }

        /// <summary>
        ///     Apply the corresponding <see cref="PlanogramBlockingGroup"/> sequencing preferences to the products in this instance.
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="merchandisingGroups"></param>
        public void SequenceProducts(
            PlanogramBlockingGroup blockingGroup, 
            PlanogramMerchandisingGroupList merchandisingGroups = null,
            IEnumerable<PlanogramPositionPlacement> positionPlacements = null)
        {
            if (blockingGroup == null) return; // No matching blocking group was found.
            if (positionPlacements == null && merchandisingGroups == null) return;

            ICollection<string> sequencedGtins = Products.Select(o => o.Gtin).ToList();
            IEnumerable<PlanogramPositionPlacement> placementsMatchingGtins =
                merchandisingGroups != null ?
                merchandisingGroups.SelectMany(
                    merchandisingGroup => EnumeratePositionPlacementsMatchingGtins(merchandisingGroup, sequencedGtins))
                    .ToList() :
                positionPlacements;

            var nextSequenceNumber = 0;
            foreach (PlanogramSequenceGroupProduct currentProduct in
                EnumerateSequenceSnake<PlanogramPositionPlacement>(blockingGroup, placementsMatchingGtins)
                    .Select(placement => Products.FirstOrDefault(product => product.Gtin == placement.Product.Gtin))
                    .Where(currentProduct => currentProduct != null && currentProduct.SequenceNumber == 0))
            {
                currentProduct.SequenceNumber = ++nextSequenceNumber;
            }
        }

        /// <summary>
        ///     Whether the <paramref name="blockingGroup"/>'s colour matches this one or not.
        /// </summary>
        /// <param name="blockingGroup">The <see cref="PlanogramBlockingGroup"/> to compare the colour with.</param>
        /// <returns>True if the colours match, false otherwise.</returns>
        internal Boolean IsColourMatch(PlanogramBlockingGroup blockingGroup)
        {
            if (blockingGroup == null) return false;
            return blockingGroup.Colour == Colour;
        }

        /// <summary>
        ///     Gets the <see cref="PlanogramBlockingGroup"/> that corresponds to this instance in the Planogram.
        /// </summary>
        /// <param name="blocking"></param>
        /// <returns></returns>
        /// <remarks>A null reference will be returned if there is no Final Blocking or no mathing group color.</remarks>
        public PlanogramBlockingGroup GetBlockingGroup(PlanogramBlocking blocking)
        {
            return blocking == null 
                ? null 
                : blocking.Groups.FirstOrDefault(g => g.Colour == Colour);
        }
        
        /// <summary>
        ///     Gets a list of <see cref="PlanogramPositionPlacement"/> that exists in the collection of <paramref name="placementsByGtin"/>.
        /// </summary>
        /// <param name="gtin">GTIN to find the adjacent positions for.</param>
        /// <param name="placementsByGtin">The list of existing placements by GTIN that are valid adjacent positions.</param>
        /// <param name="searchBackwards">Whether to search forward or backward in the sequence.</param>
        /// <returns>A list of <see cref="PlanogramPositionPlacement"/> that is next/previous (within sub-groups if they exist) or null if none was found.</returns>
        public List<PlanogramPositionPlacement> GetAdjacentPositionsBySequence(String gtin, Dictionary<String, List<PlanogramPositionPlacement>> placementsByGtin, Boolean searchBackwards = false)
        {
            //  Transverse the SequenceGroupProducts to locate the previous GTIN having a placement.
            while (true)
            {
                PlanogramSequenceGroupProduct sequenceProduct = searchBackwards
                    ? GetPreviousSequenceProduct(gtin)
                    : GetNextSequenceProduct(gtin);

                if (sequenceProduct == null)
                {
                    gtin = null;
                    break;
                }
                gtin = sequenceProduct.Gtin;
                if (String.IsNullOrEmpty(gtin) || placementsByGtin.ContainsKey(gtin)) break;
            }

            //  Find and return the list of possible positions in sequence.
            List<PlanogramPositionPlacement> placements = gtin == null ||
                                                      !placementsByGtin.TryGetValue(gtin, out placements)
                                            ? new List<PlanogramPositionPlacement>()
                                            : placements;
            return placements;
        }


        /// <summary>
        ///     Get the <see cref="PlanogramSequenceGroupProduct"/> that comes 
        ///     immediately before the specified <paramref name="gtin"/>.
        /// </summary>
        /// <param name="gtin">The GTIN of the product that the previous one should be retrieved for.</param>
        /// <returns>
        /// The <see cref="PlanogramSequenceGroupProduct"/> instance in the sequence that 
        /// is immediately previous to the one with the given <paramref name="gtin"/>, 
        /// or null if the <paramref name="gtin"/> was not found 
        /// or if there is no previous product.</returns>
        internal PlanogramSequenceGroupProduct GetPreviousSequenceProduct(String gtin)
        {
            if (Products == null || !Products.Any()) return null;

            PlanogramSequenceGroupProduct sequenceProduct = Products.FirstOrDefault(p => p.Gtin.Equals(gtin));
            if (sequenceProduct == null) return null;
            
            return GetPotentialAdjacentProducts(sequenceProduct)
                .OrderByDescending(p => p.SequenceNumber).SkipWhile(p => p.Gtin != gtin).Skip(1).FirstOrDefault();

        }

        /// <summary>
        ///     Get the <see cref="PlanogramSequenceGroupProduct"/> that comes 
        ///     immediately after the specified <paramref name="gtin"/>.
        /// </summary>
        /// <param name="gtin">The GTIN of the product that the next one should be retrieved for.</param>
        /// <returns>
        /// The <see cref="PlanogramSequenceGroupProduct"/> instance in the sequence that 
        /// is immediately following to the one with the given <paramref name="gtin"/>, 
        /// or null if the <paramref name="gtin"/> was not found 
        /// or if there is no next product.</returns>
        internal PlanogramSequenceGroupProduct GetNextSequenceProduct(String gtin)
        {
            if (Products == null || !Products.Any()) return null;

            PlanogramSequenceGroupProduct sequenceProduct = Products.FirstOrDefault(p => p.Gtin.Equals(gtin));
            if (sequenceProduct == null) return null;

            return GetPotentialAdjacentProducts(sequenceProduct)
                .OrderBy(p => p.SequenceNumber).SkipWhile(p => p.Gtin != gtin).Skip(1).FirstOrDefault();
        }

        /// <summary>
        /// Returns products that could potentially be adjacents for positioning purposes. If the
        /// given product belongs to a sub-group, only these products will be returned.
        /// </summary>
        /// <param name="sequenceProduct"></param>
        /// <returns></returns>
        private IEnumerable<PlanogramSequenceGroupProduct> GetPotentialAdjacentProducts(PlanogramSequenceGroupProduct sequenceProduct)
        {
            IEnumerable<PlanogramSequenceGroupProduct> potentialProducts;
            if (sequenceProduct.PlanogramSequenceGroupSubGroupId == null)
            {
                potentialProducts = Products;
            }
            else
            {
                potentialProducts = sequenceProduct.GetSubGroup().EnumerateProducts();
            }
            return potentialProducts;
        }

        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);

            if (e.ChildObject.GetType().Equals(typeof(PlanogramSequenceGroupProduct)))
            {
                if (e.PropertyChangedArgs.PropertyName.Equals(PlanogramSequenceGroupProduct.PlanogramSequenceGroupSubGroupIdProperty.Name))
                {
                    // Ensure that any empty sub groups are cleaned up.
                    IEnumerable<PlanogramSequenceGroupSubGroup> subGroupsToRemove =
                        SubGroups.Where(g => !g.EnumerateProducts().Any()).ToList();
                    SubGroups.RemoveList(subGroupsToRemove);
                }
            }
            else if (e.ChildObject.GetType().Equals(typeof(PlanogramSequenceGroupSubGroup)))
            {
                if (e.PropertyChangedArgs.PropertyName.Equals(PlanogramSequenceGroupSubGroup.NameProperty.Name))
                {
                    // Ensure that the sub group names remain unique.
                    EnsureSubGroupNamesAreUnique(e.ChildObject);
                }
            }
            else if (e.ChildObject is PlanogramSequenceGroupSubGroupList && e.CollectionChangedArgs != null)
            {
                if (e.CollectionChangedArgs.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (Object newItem in e.CollectionChangedArgs.NewItems)
                    {
                        // Ensure that the sub group names remain unique.
                        EnsureSubGroupNamesAreUnique(newItem);
                    }
                }
                else if(e.CollectionChangedArgs.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach(Object subGroupObject in e.CollectionChangedArgs.OldItems)
                    {
                        PlanogramSequenceGroupSubGroup subGroup = subGroupObject as PlanogramSequenceGroupSubGroup;
                        if (subGroup == null) continue;
                        foreach(PlanogramSequenceGroupProduct sequenceProduct in subGroup.EnumerateProducts())
                        {
                            sequenceProduct.PlanogramSequenceGroupSubGroupId = null;
                        }
                    }
                }
            }
        }

        private void EnsureSubGroupNamesAreUnique(Object changedObject)
        {
            PlanogramSequenceGroupSubGroup subGroup = changedObject as PlanogramSequenceGroupSubGroup;
            if (subGroup == null) return;

            // If this sub group doesn't have a name yet, we need to assign it one.
            if (String.IsNullOrWhiteSpace(subGroup.Name))
            {
                subGroup.Name = String.Format("Sub-Group {0}", SubGroups.Count);
            }

            // Now check for uniqueness
            if (SubGroups.Select(g => g.Name).Distinct().Count() < SubGroups.Count)
            {
                // If this name change is the culprit
                if (SubGroups.Where(g => g.Name.Equals(subGroup.Name)).Count() > 1)
                {
                    Int32 countDuplication = 0;
                    String newName = CheckSubGroupNameDuplication(subGroup.Name.Trim(), countDuplication);
                    subGroup.Name = newName;
                }
            }
        }

        private String CheckSubGroupNameDuplication(String groupName, Int32 count)
        {
            String name = String.Format("{0} ({1})", groupName, count);
            if (SubGroups.Any(p => p.Name.ToLowerInvariant().Trim() == name.ToLowerInvariant().Trim()))
            {
                count++;
                return CheckSubGroupNameDuplication(groupName, count);
            }
            else
            {
                return name;
            }
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Enumerates the <see cref="PlanogramPositionPlacement"/> from the <paramref name="merchandisingGroup"/> that match one of the <paramref name="sequencedGtins"/>.
        /// </summary>
        /// <param name="merchandisingGroup">The <see cref="PlanogramMerchandisingGroup"/> to match placements from.</param>
        /// <param name="sequencedGtins">The collection of GTINs that are to be matched.</param>
        /// <returns>An enumeration of placements in the merchandising group that match the gtins provided.</returns>
        private static IEnumerable<PlanogramPositionPlacement> EnumeratePositionPlacementsMatchingGtins(
            PlanogramMerchandisingGroup merchandisingGroup,
            ICollection<string> sequencedGtins)
        {
            return
                merchandisingGroup.PositionPlacements.Where(
                    positionPlacement => sequencedGtins.Contains(positionPlacement.Product.Gtin));
        }

        /// <summary>
        /// Enumerates <paramref name="positions"/> of type <paramref name="T"/> in the sequence directions in <paramref name="blockingGroup"/>.
        /// </summary>
        /// <typeparam name="T">The type of positions to enumerate, implementing <see cref="IPlanogramSubComponentSequencedItem"/>.</typeparam>
        /// <param name="blockingGroup">The <see cref="IPlanogramBlockingGroup"/> containing the sequence directions to snake in.</param>
        /// <param name="positions">The positions to enumerate in sequence.</param>
        /// <returns></returns>
        public static IEnumerable<T> EnumerateSequenceSnake<T>(
            IPlanogramBlockingGroup blockingGroup, 
            IEnumerable<IPlanogramSubComponentSequencedItem> positions)
            where T : IPlanogramSubComponentSequencedItem
        {
            Dictionary<IPlanogramSubComponentSequencedItem, PointValue> coordinatesByPlacement =
                positions.ToDictionary(p => p, p => p.GetParentSubComponentPlacement().GetPlanogramRelativeCoordinates());

            Boolean isTertiaryGroupReversed = !blockingGroup.BlockPlacementTertiaryType.IsPositive();
            Boolean isSecondaryGroupReversed = !blockingGroup.BlockPlacementSecondaryType.IsPositive();
            Boolean isPrimaryGroupReversed = !blockingGroup.BlockPlacementPrimaryType.IsPositive();
            Boolean isTertiaryLayerReversed = !blockingGroup.BlockPlacementTertiaryType.IsPositive();
            Boolean isSecondaryLayerReversed = !blockingGroup.BlockPlacementSecondaryType.IsPositive();
            Boolean isPrimaryLayerReversed = !blockingGroup.BlockPlacementPrimaryType.IsPositive();

            foreach (var tertiaryGroup in OrderAndGroupComponentsBy(blockingGroup.BlockPlacementTertiaryType, coordinatesByPlacement.ToList(), isTertiaryGroupReversed))
            {
                foreach (var secondaryGroup in OrderAndGroupComponentsBy(blockingGroup.BlockPlacementSecondaryType, tertiaryGroup, isSecondaryGroupReversed))
                {
                    foreach (var primaryGroup in OrderAndGroupComponentsBy(blockingGroup.BlockPlacementPrimaryType, secondaryGroup, isPrimaryGroupReversed))
                    {
                        foreach (var tertiaryLayer in OrderAndGroupPositionsBy(blockingGroup.BlockPlacementTertiaryType, primaryGroup, isTertiaryLayerReversed))
                        {
                            foreach (var secondaryLayer in OrderAndGroupPositionsBy(blockingGroup.BlockPlacementSecondaryType, tertiaryLayer, isSecondaryLayerReversed))
                            {
                                foreach (T placement in OrderAndGroupPositionsBy(blockingGroup.BlockPlacementPrimaryType, secondaryLayer, isPrimaryLayerReversed).SelectMany(primaryLayer => primaryLayer.Select(kvp => kvp.Key)))
                                {
                                    yield return placement;
                                }
                                isPrimaryLayerReversed = !isPrimaryLayerReversed;
                            }
                            isSecondaryLayerReversed = !isSecondaryLayerReversed;
                        }
                        //  Primary direction in each component should be that of the primary group.
                        isPrimaryLayerReversed = isPrimaryGroupReversed;
                    }
                    isPrimaryGroupReversed = !isPrimaryGroupReversed;
                    //  Primary direction in each component should be that of the primary group.
                    isPrimaryLayerReversed = isPrimaryGroupReversed;
                    //  Secondary group change in component, revert the last change in secondary direction.
                    isSecondaryLayerReversed = isSecondaryGroupReversed;
                }
                isSecondaryGroupReversed = !isSecondaryGroupReversed;
                //  Secondary direction in each secondary group should be that of the secondary group.
                isSecondaryLayerReversed = isSecondaryGroupReversed;
            }
        }

        private static IEnumerable<IGrouping<Single, KeyValuePair<IPlanogramSubComponentSequencedItem, PointValue>>> OrderAndGroupComponentsBy(
            PlanogramBlockingGroupPlacementType placementType, 
            IEnumerable<KeyValuePair<IPlanogramSubComponentSequencedItem, PointValue>> placementsByCoordinate, 
            Boolean isReversed)
        {
            var ordered = isReversed ? placementsByCoordinate.OrderByDescending(kvp => placementType.GetIndex(kvp.Value))
                    : placementsByCoordinate.OrderBy(kvp => placementType.GetIndex(kvp.Value));
            return ordered.GroupBy(kvp => placementType.GetIndex(kvp.Value), new PlanogramSequenceGroup.SequencableItemCoordinateComparer());
        }

        private static IEnumerable<IGrouping<Single, KeyValuePair<IPlanogramSubComponentSequencedItem, PointValue>>> OrderAndGroupPositionsBy(
            PlanogramBlockingGroupPlacementType placementType,
            IEnumerable<KeyValuePair<IPlanogramSubComponentSequencedItem, PointValue>> placementsByCoordinate, 
            Boolean isReversed)
        {
            var ordered = isReversed ? placementsByCoordinate.OrderByDescending(kvp => placementType.GetIndex(kvp.Key))
                    : placementsByCoordinate.OrderBy(kvp => placementType.GetIndex(kvp.Key));
            return ordered.GroupBy(kvp => placementType.GetIndex(kvp.Key), new PlanogramSequenceGroup.SequencableItemCoordinateComparer());
        }

        #endregion
    }
}