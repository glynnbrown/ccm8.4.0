﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31967 : A.Silva
//  Refactored default comparison fields to be stored in a field.
//  Added OfType method to filter the comparison fields by plan item type.
// V8-31945 : A.Silva
//  Added support for IPlanogramComparisonField.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Collection of <see cref="PlanogramComparisonField" /> instances belonging to the parent
    ///     <see cref="PlanogramComparison" />.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonFieldList : ModelList<PlanogramComparisonFieldList, PlanogramComparisonField>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparison" />.
        /// </summary>
        public new PlanogramComparison Parent { get { return (PlanogramComparison) base.Parent; } }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparisonFieldList" />.
        /// </summary>
        public static PlanogramComparisonFieldList NewPlanogramComparisonFieldList()
        {
            var item = new PlanogramComparisonFieldList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="sourceItems">The collection of <see cref="PlanogramComparisonField"/> to add.</param>
        public void AddRange(IEnumerable<IPlanogramComparisonSettingsField> sourceItems)
        {
            base.AddRange(sourceItems.Select(PlanogramComparisonField.NewPlanogramComparisonField));
        }

        public void AddRange(PlanogramItemType itemType, IEnumerable<IPlanogramComparisonSettingsField> sourceItems)
        {
            base.AddRange(sourceItems.Select(field => PlanogramComparisonField.NewPlanogramComparisonField(itemType, field)));
        }

        /// <summary>
        ///     Add a collection of <see cref="PlanogramComparisonField" /> derived from an enumeration of
        ///     <see cref="ObjectFieldInfo" />.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="range">The enumeration of <see cref="ObjectFieldInfo" /> from which to derive the new items.</param>
        public void AddRange(PlanogramItemType itemType, Dictionary<ObjectFieldInfo, Boolean> range)
        {
            base.AddRange(range.Select(info => PlanogramComparisonField.NewPlanogramComparisonField(itemType, info.Key, info.Value)));
        }

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonField" /> derived from a <see cref="ObjectFieldInfo" />.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this item should be targeting.</param>
        /// <param name="item">The instance of <see cref="ObjectFieldInfo" /> from which to derive the new item.</param>
        /// <param name="display"></param>
        public PlanogramComparisonField Add(PlanogramItemType itemType, ObjectFieldInfo item, Boolean display)
        {
            PlanogramComparisonField newItem = PlanogramComparisonField.NewPlanogramComparisonField(itemType, item, display);
            Add(newItem);
            return newItem;
        }

        /// <summary>
        ///     Enumerate all fields that are of the given <paramref name="planItemType" />.
        /// </summary>
        /// <param name="planItemType">Type of planogram item that the comparison field needs to be of.</param>
        /// <returns>An enumeration of all contained fields that are of the given <paramref name="planItemType" />.</returns>
        public IEnumerable<PlanogramComparisonField> OfItemType(PlanogramItemType planItemType)
        {
            return this.Where(field => field.ItemType == planItemType);
        }

        #endregion
    }
}