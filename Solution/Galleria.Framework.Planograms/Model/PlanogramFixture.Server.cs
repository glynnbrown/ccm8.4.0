﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-27058 : A.Probyn
//  Added new meta data properties
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramFixture
    {
        #region Constructor
        private PlanogramFixture() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramFixture Fetch(IDalContext dalContext, PlanogramFixtureDto dto)
        {
            return DataPortal.FetchChild<PlanogramFixture>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramFixtureDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty, dto.MetaNumberOfMerchandisedSubComponents);
            this.LoadProperty<Single?>(MetaTotalFixtureCostProperty, dto.MetaTotalFixtureCost);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramFixtureDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramFixtureDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                MetaNumberOfMerchandisedSubComponents = this.ReadProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty),
                MetaTotalFixtureCost = this.ReadProperty<Single?>(MetaTotalFixtureCostProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramFixtureDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramFixtureDto>(
            (dc) =>
            {
                PlanogramFixtureDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return this.GetDataTransferObject(parent);
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramFixture>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramFixtureDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramFixtureDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}