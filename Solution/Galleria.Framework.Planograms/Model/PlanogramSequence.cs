﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802
// V8-28996 : A.Silva
//	Created
// V8-29000 : A.Silva
//  Added ApplyFromBlocking method to recalculate the products belonging to each PlanogramSequenceBlockingGroup.
// V8-28997 : A.Silva
//  Refactored ApplyFromBlocking.
// V8-29072 : A.Kuszyk
//  Added Load From method.
// V8-28999 : A.Silva
//  Amended ApplyFromBlocking so that sequence groups calculate product sequences after having the products added.
// V8-29105 : A.Silva
//  Added GetSequenceGroup to find the containing sequence group for a given GTIN.
// V8-29232 : A.Silva
//  Amended ApplyFromBlocking so that the Products are updated with the new sequence number.
#endregion
#region Version History: CCM803
// V8-29402 : N.Foster
//  Added call to update product blocking colours when a seqeunce is loaded
#endregion
#region Version History: CCM820
// V8-30983 : D.Pleasance
//  Amended ApplyFromBlocking, so that overhanging positions that sit on a merchandising group 
//  within the blocking area bounds are included within the product sequence.
// V8-31165 : D.Pleasance
//  Added ApplyFromMerchandisingGroups.
// V8-31172 : A.Kuszyk
//  Re-factored ApplyFromBlocking to sequence multi-sited products.
// V8-31176 : D.Pleasance
//  Added GetSequenceGroups
#endregion 
#region Version History: CCM830
// V8-32089 : N.Haywood
//  Added setting product for product display row
// V8-32504 : A.Kuszyk
//  Added state tracking for sub groups.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Header for the planogram product sequence data.
    /// </summary>
    [Serializable, DefaultNewMethod("NewPlanogramSequence")]
    public sealed partial class PlanogramSequence : ModelObject<PlanogramSequence>
    {
        #region Parent

        /// <summary>
        ///     Gets the parent <see cref="Planogram" /> for this instance.
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram) base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramSequence" />.
        /// </summary>
        /// <returns>New instance with all the correct initialization already done.</returns>
        public static PlanogramSequence NewPlanogramSequence()
        {
            var item = new PlanogramSequence();
            item.Create();
            return item;
        }

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this <see cref="PlanogramSequence" />.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region DataAccess

        /// <summary>
        ///     Initializes a this instance during creation.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(GroupsProperty,
                PlanogramSequenceGroupList.NewPlanogramSequenceGroupList());
            MarkAsChild();
            base.Create();
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Define the authorization rules for <see cref="PlanogramSequence" />.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //TODO Implement AddObjectAuthorizationRules for PlanogramSequence.
        }

        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria

        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Constructor

            /// <summary>
            ///     Instantiates a new instance of <see cref="FetchByPlanogramIdCriteria" />.
            /// </summary>
            /// <param name="dalFactoryName">Name of the DAL factory to use.</param>
            /// <param name="planogramId">Unique identifier for the planogram used to match results.</param>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                LoadProperty(DalFactoryNameProperty, dalFactoryName);
                LoadProperty(PlanogramIdProperty, planogramId);
            }

            #endregion

            #region Properties

            #region DalFactoryName

            /// <summary>
            ///     Metadata for the <see cref="DalFactoryName" /> property.
            /// </summary>
            private static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(o => o.DalFactoryName);

            /// <summary>
            ///     Gets the value for the DAL factory name criteria.
            /// </summary>
            public String DalFactoryName
            {
                get { return ReadProperty(DalFactoryNameProperty); }
            }

            #endregion

            #region PlanogramId

            /// <summary>
            ///     Metadata for the <see cref="PlanogramId" /> property.
            /// </summary>
            private static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(o => o.PlanogramId);

            /// <summary>
            ///     Gets the value for the Planogram Id criteria.
            /// </summary>
            public Object PlanogramId
            {
                get { return ReadProperty(PlanogramIdProperty); }
            }

            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Nested Types
        /// <summary>
        /// A simple class to track the state of <see cref="PlanogramSequenceGroupSubGroup"/>s.
        /// </summary>
        private sealed class SubGroupState
        {
            /// <summary>
            /// The name of the sub group.
            /// </summary>
            public String Name { get; private set; }
            
            /// <summary>
            /// The alignment of the sub group.
            /// </summary>
            public PlanogramSequenceGroupSubGroupAlignmentType Alignment { get; private set; }
            
            /// <summary>
            /// The gtins of the <see cref="PlanogramSequenceGroupProduct"/>s that should be linked to this sub group.
            /// </summary>
            public IEnumerable<String> Gtins { get; private set; }
            
            /// <summary>
            /// The colour of the parent group.
            /// </summary>
            public Int32 GroupColour { get; private set; }
            
            /// <summary>
            /// The name of the parent group.
            /// </summary>
            public String GroupName { get; private set; }

            public SubGroupState(PlanogramSequenceGroupSubGroup subGroup)
            {
                Name = subGroup.Name;
                Alignment = subGroup.Alignment;
                GroupColour = subGroup.Parent.Colour;
                GroupName = subGroup.Parent.Name;
                Gtins = subGroup.EnumerateProducts().Select(p => p.Gtin);
            }
        }
        #endregion

        #region Properties

        #region Groups

        /// <summary>
        ///     Metadata for the <see cref="Groups" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramSequenceGroupList> GroupsProperty =
            RegisterModelProperty<PlanogramSequenceGroupList>(o => o.Groups, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     Gets the collection of <see cref="PlanogramSequenceGroup" /> that are part of this instance.
        /// </summary>
        public PlanogramSequenceGroupList Groups
        {
            get
            {
                return GetPropertyLazy(GroupsProperty,
                    new ModelList<PlanogramSequenceGroupList, PlanogramSequenceGroup>.FetchByParentIdCriteria(
                        DalFactoryName, Id), true);
            }
        }

        /// <summary>
        ///     Gets asynchronously the collection of <see cref="PlanogramSequenceGroup" /> that are part of this instance.
        /// </summary>
        public PlanogramSequenceGroupList GroupsAsync
        {
            get
            {
                return GetPropertyLazy(GroupsProperty,
                    new ModelList<PlanogramSequenceGroupList, PlanogramSequenceGroup>.FetchByParentIdCriteria(
                        DalFactoryName, Id), true);
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Apply the most recent blocking in the containing planogram to get the product sequences.
        /// </summary>
        /// <returns>The products that were not fully sequenced due to multi-sitings.</returns>
        public IEnumerable<PlanogramProduct> ApplyFromBlocking(PlanogramBlocking blocking)
        {
            using (new AlgorithmPerformanceMetric("PlanogramSequence", "ApplyFromBlocking"))
            {
                List<PlanogramProduct> multiSitedProducts = new List<PlanogramProduct>();
                if (blocking == null) return multiSitedProducts; // No blocking to apply product sequencing from.

                //  Get the Blocking Area Size to adjust the coordinate percentage values by.
                Single topDownOffset;
                Single blockingHeight;
                Single blockingWidth;
                Parent.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out topDownOffset);

                // Arrange the locations into a lookup keyed by group.
                ILookup<PlanogramBlockingGroup, PlanogramBlockingLocation> locationsByGroup =
                    blocking.Locations.ToLookup(l => l.GetPlanogramBlockingGroup());

                // Get the design view relative spaces of the locations and arrange them in a dictionary.
                // This allows us to later compare the design view position of the PositionPlacements
                // to the location spaces. The Z axis is irrelevant here, so we just use zeros.
                Dictionary<PlanogramBlockingLocation, RectValue> locationSpacesByLocation =
                    blocking.GetLocationSpacesByLocation(blockingWidth, blockingHeight);

                // Use the blocking groups to create the corresponding sequence groups.
                Groups.Clear();
                Groups.AddRange(blocking.Groups.Select(PlanogramSequenceGroup.NewPlanogramSequenceGroup));

                using (PlanogramMerchandisingGroupList merchandisingGroups = blocking.Parent.GetMerchandisingGroups())
                {
                    Dictionary<PlanogramPositionPlacement, PlanogramSequenceGroupProduct> sequenceProductsByPosition =
                        new Dictionary<PlanogramPositionPlacement, PlanogramSequenceGroupProduct>();
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroups.SelectMany(m => m.PositionPlacements))
                    {
                        PlanogramBlockingGroup blockingGroup = GetMaxIntersectingBlockingGroupForPosition(
                            positionPlacement,
                            locationsByGroup,
                            locationSpacesByLocation,
                            blockingWidth,
                            blockingHeight,
                            topDownOffset);
                        if (blockingGroup == null) continue;

                        PlanogramSequenceGroup sequenceGroup = Groups.FirstOrDefault(o => o.IsColourMatch(blockingGroup));
                        if (sequenceGroup == null) continue;
                        
                        // Ensure no duplicate products are added to the group.
                        PlanogramSequenceGroupProduct sequenceProduct = sequenceGroup.Products
                            .FirstOrDefault(p => p.Gtin.Equals(positionPlacement.Product.Gtin));
                        if (sequenceProduct == null)
                        {
                            sequenceProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(positionPlacement.Product);
                            sequenceProduct.Product = positionPlacement.Product;
                            sequenceGroup.Products.Add(sequenceProduct); 
                        }
                        else
                        {
                            if(!multiSitedProducts.Contains(positionPlacement.Product)) multiSitedProducts.Add(positionPlacement.Product);
                        }
                        sequenceProductsByPosition.Add(positionPlacement, sequenceProduct);
                    }

                    SequenceGroups(merchandisingGroups, sequenceProductsByPosition, blocking);

                    merchandisingGroups.ApplyEdit();
                }

                return multiSitedProducts;
            }
        }

        /// <summary>
        ///     Creates sequence groups from merchandising groups. Groups are created from the merchandising groups sub component placements, 
        ///     the first color is taken as the key and list of sub components as the value (which ultimately hold the sequence products)
        /// </summary>
        /// <param name="merchGroupList"></param>
        public void ApplyFromMerchandisingGroups(PlanogramMerchandisingGroupList merchGroupList)
        {
            // Store the sub group information before clearing the groups, so that we can try to re-build
            // the sub groups after we have re-generated the groups.
            IEnumerable<SubGroupState> subGroupStates = GetSubGroupStates();
            Groups.Clear();

            // Create colour lookups, (combined shelves are considered to be the same sequence group and colour is taken from the first sub component)
            ILookup<Int32, PlanogramMerchandisingGroup> merchGroupsByColour = merchGroupList
                .Where(m => m.SubComponentPlacements.Any())
                .ToLookup(m => m.SubComponentPlacements.First().SubComponent.FillColourFront, m => m);

            // Create distinct color groups and assign products from sub component placements
            Dictionary<PlanogramPositionPlacement, PlanogramSequenceGroupProduct> sequenceProductsByPosition =
                new Dictionary<PlanogramPositionPlacement, PlanogramSequenceGroupProduct>();
            List<PlanogramSequenceGroup> sequenceGroups = new List<PlanogramSequenceGroup>();
            foreach (IGrouping<Int32, PlanogramMerchandisingGroup> merchGroupsForColour in merchGroupsByColour)
            {
                if (!merchGroupsForColour.Any() || !merchGroupsForColour.First().SubComponentPlacements.Any()) continue;

                // get \ create the sequence group
                PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = merchGroupsForColour.Key;
                sequenceGroup.Name = merchGroupsForColour.First().SubComponentPlacements.First().Component.Name;

                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroupsForColour)
                {
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        PlanogramSequenceGroupProduct sequenceProduct = sequenceGroup.Products
                            .FirstOrDefault(p => p.Gtin.Equals(positionPlacement.Product.Gtin));

                        if (sequenceProduct == null)
                        {
                            sequenceProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(positionPlacement.Product);
                            sequenceGroup.Products.Add(sequenceProduct);
                        }

                        sequenceProductsByPosition.Add(positionPlacement, sequenceProduct);
                    }
                }

                sequenceGroups.Add(sequenceGroup);
            }
            Groups.AddRange(sequenceGroups);

            SequenceGroups(merchGroupList, sequenceProductsByPosition);

            // Re-apply the sequence group sub groups.
            RestoreSubGroupStates(subGroupStates);

            merchGroupList.ApplyEdit();
        }

        /// <summary>
        /// Gets the state of the current <see cref="SubGroups"/>.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SubGroupState> GetSubGroupStates()
        {
            IEnumerable<SubGroupState> subGroupStates = Groups.SelectMany(g => g.SubGroups).Select(sg => new SubGroupState(sg)).ToList();
            return subGroupStates;
        }

        /// <summary>
        /// Restores sub groups from the given <paramref name="subGroupStates"/>.
        /// </summary>
        /// <param name="subGroupStates"></param>
        private void RestoreSubGroupStates(IEnumerable<SubGroupState> subGroupStates)
        {
            foreach (PlanogramSequenceGroup group in Groups)
            {
                foreach(SubGroupState subGroupState in 
                    subGroupStates.Where(s => s.GroupColour.Equals(group.Colour) || s.GroupName.Equals(group.Name)))
                {
                    IEnumerable<PlanogramSequenceGroupProduct> products = group.Products
                        .Where(p => subGroupState.Gtins.Contains(p.Gtin));
                    PlanogramSequenceGroupSubGroup subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(products);
                    subGroup.Name = subGroupState.Name;
                    subGroup.Alignment = subGroupState.Alignment;
                    group.SubGroups.Add(subGroup);
                }
            }
        }

        /// <summary>
        /// Sequences the products in <see cref="Groups"/> using the given <paramref name="blocking"/> or 
        /// a default blocking group if null.
        /// </summary>
        /// <param name="merchandisingGroups"></param>
        /// <param name="blocking">Optionally the blocking to use when finding a matching blocking group.</param>
        private void SequenceGroups(
            PlanogramMerchandisingGroupList merchandisingGroups, 
            Dictionary<PlanogramPositionPlacement,PlanogramSequenceGroupProduct> sequenceProductsByPosition, 
            PlanogramBlocking blocking = null)
        {
            PlanogramBlockingGroup blockingGroup = null;
            if (blocking == null) blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();

            // Build a sequence group/position lookup out of the dictionary we were given.
            ILookup<PlanogramSequenceGroup, PlanogramPositionPlacement> positionPlacementsBySequenceGroup =
                sequenceProductsByPosition.ToLookup(kvp => kvp.Value.Parent, kvp => kvp.Key);

            foreach (PlanogramSequenceGroup sequenceGroup in Groups)
            {
                if(blocking != null) blockingGroup = blocking.Groups.FirstOrDefault(sequenceGroup.IsColourMatch);
                if (blockingGroup == null) continue;
                if (!positionPlacementsBySequenceGroup.Contains(sequenceGroup)) continue;
                sequenceGroup.SequenceProducts(blockingGroup, positionPlacements:positionPlacementsBySequenceGroup[sequenceGroup]);
            }

            // Now, ensure that all the positions have the correct sequence number and colour values
            foreach (KeyValuePair<PlanogramPositionPlacement, PlanogramSequenceGroupProduct> kvp in sequenceProductsByPosition)
            {
                kvp.Key.Position.SequenceNumber = kvp.Value.SequenceNumber;
                kvp.Key.Position.SequenceColour = kvp.Value.Parent.Colour;
            }

        }

        /// <summary>
        /// Gets the <see cref="PlanogramBlockingGroup"/> that intersects the most on the given <paramref name="axis"/> type.
        /// </summary>
        /// <param name="positionSpace">The design view space of the position to find a blocking group for.</param>
        /// <param name="locationSpacesByLocation">A dictionary of design view relative spaces for each blocking location.</param>
        /// <param name="matchingLocations">The locations to check for intersections with.</param>
        /// <param name="axis"></param>
        /// <returns></returns>
        private static PlanogramBlockingGroup GetIntersectingBlockingGroupForAxis(
            RectValue positionSpace, 
            IEnumerable<PlanogramBlockingLocation> matchingLocations, 
            Dictionary<PlanogramBlockingLocation, RectValue> locationSpacesByLocation, 
            AxisType axis)
        {
            if (!matchingLocations.Any()) return null;

            // locations that intersect the positions
            IEnumerable<PlanogramBlockingLocation> locationsThatIntersect = locationSpacesByLocation
                .Where(l => matchingLocations.Contains(l.Key))
                .Where(l =>
                    Math.Max(l.Value.GetCoordinate(axis), positionSpace.GetCoordinate(axis))
                    .LessThan(
                    Math.Min(l.Value.GetCoordinate(axis) + l.Value.GetSize(axis), positionSpace.GetCoordinate(axis) + positionSpace.GetSize(axis))))
                .Select(kvp => kvp.Key);

            // the location that intersects the position the most.
            var location = locationsThatIntersect
                .OrderByDescending(l => 
                    {
                        RectValue lSpace = locationSpacesByLocation[l];
                        return
                            Math.Min(
                                lSpace.GetCoordinate(axis) + lSpace.GetSize(axis), 
                                positionSpace.GetCoordinate(axis) + positionSpace.GetSize(axis)) 
                            -
                            Math.Max(
                                lSpace.GetCoordinate(axis), 
                                positionSpace.GetCoordinate(axis));
                    })
                .FirstOrDefault();

            if (location == null) return null;
            return location.GetPlanogramBlockingGroup();
        }

        /// <summary>
        /// Clears the current Sequence and loads new data from the given source.
        /// </summary>
        /// <param name="sourceSequence">The sequence to use as a source for new data.</param>
        public void LoadFrom(PlanogramSequence sourceSequence, Boolean updateSequenceData = true)
        {
            this.Groups.Clear();
            foreach (PlanogramSequenceGroup sourceGroup in sourceSequence.Groups)
            {
                this.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(sourceGroup));
            }
            if (updateSequenceData) Parent.UpdatePositionSequenceData();
        }

        /// <summary>
        ///     Gets the <see cref="PlanogramSequenceGroup"/> that contains a product with the required <paramref name="productGtin"/>.
        /// </summary>
        /// <param name="productGtin">The GTIN of the product to return the parent <see cref="PlanogramSequenceGroup"/> of.</param>
        /// <returns>A <see cref="PlanogramSequenceGroup"/> or null if the <paramref name="productGtin"/> was not found in any.</returns>
        public PlanogramSequenceGroup GetSequenceGroup(String productGtin)
        {
            return Groups.FirstOrDefault(g => g.Products.Any(p => p.Gtin == productGtin));
        }

        /// <summary>
        ///     Gets all <see cref="PlanogramSequenceGroup"/> that contain the required <paramref name="productGtin"/>.
        /// </summary>
        /// <param name="productGtin">The GTIN of the product to lookup</param>
        /// <returns>A list of <see cref="PlanogramSequenceGroup"/> or null if the <paramref name="productGtin"/> was not found in any.</returns>
        public IEnumerable<PlanogramSequenceGroup> GetSequenceGroups(String productGtin)
        {
            return Groups.Where(g => g.Products.Any(p => p.Gtin == productGtin));
        }

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramSequence>(oldId, newId);
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Gets the <see cref="PlanogramBlockingGroup"/> that intersects the most with the <paramref name="positionPlacement"/>.
        /// </summary>
        /// <param name="positionPlacement">The position to find intersecting blocking groups for.</param>
        /// <param name="locationsByGroup">A lookup of <see cref="PlanogramBlockingLocation"/>s keyed by their <see cref="PlanogramBlockingGroup"/>.</param>
        /// <param name="locationSpacesByLocation">A dictionary of design view relative spaces, keyed by their <see cref="PlanogramBlockingLocation"/>.</param>
        /// <param name="blocking">The <see cref="PlanogramBlocking"/> to search for intersecting groups in.</param>
        /// <returns>The <see cref="PlanogramBlockingGroup"/> that intersects the most with the product placements.</returns>
        public PlanogramBlockingGroup GetMaxIntersectingBlockingGroupForPosition(
            PlanogramPositionPlacement positionPlacement,
            ILookup<PlanogramBlockingGroup, PlanogramBlockingLocation> locationsByGroup,
            Dictionary<PlanogramBlockingLocation, RectValue> locationSpacesByLocation,
            Single blockingWidth,
            Single blockingHeight,
            Single topDownOffset)
        {
            // Get the position placement's space in the design view reference frame. This puts the space
            // in the space frame as the locationSpacesByLocation.
            RectValue positionSpace = new DesignViewPosition(positionPlacement, topDownOffset).ToRectValue2D();

            // Try to find the group that the position intersects with the most.
            PlanogramBlockingGroup blockingGroup = locationsByGroup
                .Select(locations => new // Put the blocking group and its total intersection with the position in a temp object.
                {
                    BlockingGroup = locations.Key,
                    TotalArea = locations.Sum(l => 
                        {
                            RectValue locationSpace;
                            if(!locationSpacesByLocation.TryGetValue(l, out locationSpace)) return 0f;
                            return GetIntersectionArea(locationSpace, positionSpace);
                        })
                })
                .Where(o => o.TotalArea > 0) // Filter out blocks with no intersection.
                .OrderByDescending(o => o.TotalArea) // Order by greatest intersection first.
                .Select(o => o.BlockingGroup)
                .FirstOrDefault();
            if (blockingGroup != null) return blockingGroup;

            // If blockingGroup was null, we couldn't find an intersection. However, there might
            // still be an appropriate blocking group, if the merchandising group the position is on
            // does intersect with a blocking group (although it might not be the same one).
            if (!IsMerchGroupInBlockingArea(positionPlacement.MerchandisingGroup.DesignViewPosition, blockingWidth, blockingHeight)) 
            {
                return null;
            }

            // If the merchandising group of the position does intersect with a blocking group, it must be in the blocking area
            // somewhere. The position might not be (in fact, it doesn't otherwise we would have picked it up to start off with)
            // so we need to work out which blocking group is the most appropriate based on the whereabouts of the position
            // outside of the blocking area.
            if (positionSpace.X > blockingWidth) // product is to the right
            {
                if (positionSpace.Y > blockingHeight) // Above
                {
                    PlanogramBlockingLocation topRightLocation = locationSpacesByLocation.Keys
                        .FirstOrDefault(p => p.PlanogramBlockingDividerRightId == null && p.PlanogramBlockingDividerTopId == null);
                    if (topRightLocation == null) return null;
                    return topRightLocation.GetPlanogramBlockingGroup();
                }
                else if (positionSpace.Y < 0) // Below
                {
                    PlanogramBlockingLocation belowRightLocation = locationSpacesByLocation.Keys
                        .FirstOrDefault(p => p.PlanogramBlockingDividerRightId == null && p.PlanogramBlockingDividerBottomId == null);
                    if (belowRightLocation == null) return null;
                    return belowRightLocation.GetPlanogramBlockingGroup();
                }
                else
                {
                    return GetIntersectingBlockingGroupForAxis(
                        positionSpace, 
                        locationSpacesByLocation.Keys.Where(p => p.PlanogramBlockingDividerRightId == null).ToList(),
                        locationSpacesByLocation, 
                        AxisType.Y);
                }
            }
            else if (positionSpace.X < 0) // product is to the left
            {
                if (positionSpace.Y > (blockingHeight)) // Above
                {
                    PlanogramBlockingLocation topLeftLocation = locationSpacesByLocation.Keys
                        .FirstOrDefault(p => p.PlanogramBlockingDividerLeftId == null && p.PlanogramBlockingDividerTopId == null);
                    if (topLeftLocation == null) return null;
                    return topLeftLocation.GetPlanogramBlockingGroup();
                }
                else if (positionSpace.Y < 0) // Below
                {
                    PlanogramBlockingLocation belowLeftLocation = locationSpacesByLocation.Keys
                        .FirstOrDefault(p => p.PlanogramBlockingDividerLeftId == null && p.PlanogramBlockingDividerBottomId == null);
                    if (belowLeftLocation == null) return null;
                    return belowLeftLocation.GetPlanogramBlockingGroup();
                }
                else
                {
                    return GetIntersectingBlockingGroupForAxis(
                        positionSpace, 
                        locationSpacesByLocation.Keys.Where(p => p.PlanogramBlockingDividerLeftId == null).ToList(),
                        locationSpacesByLocation, 
                        AxisType.Y);
                }
            }
            else if (positionSpace.Y > blockingHeight) // Above
            {
                return GetIntersectingBlockingGroupForAxis(
                    positionSpace, 
                    locationSpacesByLocation.Keys.Where(p => p.PlanogramBlockingDividerTopId == null).ToList(), 
                    locationSpacesByLocation, 
                    AxisType.X);
            }
            else if (positionSpace.Y < 0) // Below
            {
                return GetIntersectingBlockingGroupForAxis(
                    positionSpace, 
                    locationSpacesByLocation.Keys.Where(p => p.PlanogramBlockingDividerBottomId == null).ToList(),
                    locationSpacesByLocation, 
                    AxisType.X);
            }

            // If we get here, we haven't been able to find a blocking group.
            return null;
        }

        /// <summary>
        /// Indiciates if the <paramref name="merchGroupDvp"/> is within the blocking area described 
        /// by <paramref name="blockingWidth"/> and <paramref name="blockingHeight"/>.
        /// </summary>
        /// <param name="merchGroupDvp">The <see cref="DesignViewPosition"/> of the <see cref="PlanogramMerchandisingGroup"/> to check.</param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <returns></returns>
        private static Boolean IsMerchGroupInBlockingArea(DesignViewPosition merchGroupDvp, Single blockingWidth, Single blockingHeight)
        {
            return
                Math.Max(merchGroupDvp.BoundX, 0).LessThan(Math.Min(merchGroupDvp.BoundX + merchGroupDvp.BoundWidth, blockingWidth)) &&
                Math.Max(merchGroupDvp.BoundY, 0).LessThan(Math.Min(merchGroupDvp.BoundY + merchGroupDvp.BoundHeight, blockingHeight));
        }

        /// <summary>
        ///     Get the total area determined by the intersection of both <paramref name="rectOne"/> and <paramref name="rectTwo"/>.
        /// </summary>
        /// <param name="rectOne"></param>
        /// <param name="rectTwo"></param>
        /// <returns></returns>
        private static Single GetIntersectionArea(RectValue rectOne, RectValue rectTwo)
        {
            RectValue? rectValue = rectOne.Intersect(rectTwo);
            return rectValue.HasValue ? rectValue.Value.Width * rectValue.Value.Height : 0;
        }

        #endregion

        private List<PlanogramSequenceProductLink> _productLinks = new List<PlanogramSequenceProductLink>();
        public List<PlanogramSequenceProductLink> ProductLinks
        {
            get { return _productLinks; }
        }
    }

    [Serializable]
    public class PlanogramSequenceProductLink
    {
        private PlanogramSequenceGroupProduct _anchorProduct;
        private PlanogramSequenceGroupProduct _targetProduct;

        public PlanogramSequence Parent { get; set; }
        public Object AnchorSequenceGroupProductId { get; set; }
        public Object TargetSequenceGroupProductId { get; set; }
        public PlanogramSequenceProductLinkDirectionType Direction { get; set; }
        public Int32 Priority { get; set; }

        private PlanogramSequenceProductLink()
        {

        }

        public static PlanogramSequenceProductLink NewPlanogramSequenceProductLink(
            PlanogramSequenceGroupProduct anchor, 
            PlanogramSequenceGroupProduct target, 
            PlanogramSequenceProductLinkDirectionType direction,
            Int32 priority,
            PlanogramSequence parent)
        {
            return new PlanogramSequenceProductLink()
            {
                AnchorSequenceGroupProductId = anchor.Id,
                TargetSequenceGroupProductId = target.Id,
                Direction = direction,
                Priority = priority,
                Parent = parent,
                _anchorProduct = anchor,
                _targetProduct = target,
            };
        }

        public PlanogramSequenceGroupProduct GetAnchorProduct()
        {
            return _anchorProduct;
            //return Parent.Groups.SelectMany(g => g.Products).FirstOrDefault(p => p.Id.Equals(AnchorSequenceGroupProductId));
        }

        public PlanogramSequenceGroupProduct GetTargetProduct()
        {
            return _targetProduct;
            //return Parent.Groups.SelectMany(g => g.Products).FirstOrDefault(p => p.Id.Equals(TargetSequenceGroupProductId));
        }
    }

    public enum PlanogramSequenceProductLinkDirectionType
    {
        Wide = 0,
        High = 1,
        Deep = 2,
    }

    public static class PlanogramSequenceProductLinkDirectionTypeHelper
    {
        public static AxisType ToAxisType(this PlanogramSequenceProductLinkDirectionType direction)
        {
            switch(direction)
            {
                case PlanogramSequenceProductLinkDirectionType.Wide:
                    return AxisType.X;
                case PlanogramSequenceProductLinkDirectionType.High:
                    return AxisType.Y;
                case PlanogramSequenceProductLinkDirectionType.Deep:
                    return AxisType.Z;
                default:
                    return AxisType.X;
            }
        }
    }
}