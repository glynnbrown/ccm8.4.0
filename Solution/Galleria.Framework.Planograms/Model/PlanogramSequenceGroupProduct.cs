﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created
// V8-29000 : A.Silva
//      Added NewPlanogramSequenceGroupProduct to create an instance from an IPlanogramProductInfo.

#endregion

#region Version History: CCM830
// V8-32089 : N.Haywood
//  Added setting product for product display row
// V8-32504 : A.Kuszyk
//  Added sub group id property.
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     A sequence group product in sequence order.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSequenceGroupProduct : ModelObject<PlanogramSequenceGroupProduct>
    {
        #region Parent

        /// <summary>
        ///     Gets the parent <see cref="PlanogramSequenceGroup" /> for this instance.
        /// </summary>
        public new PlanogramSequenceGroup Parent
        {
            get { return ((PlanogramSequenceGroupProductList)base.Parent).Parent; }
        }

        #endregion

        #region Properties

        #region Gtin

        /// <summary>
        ///     Metadata for the <see cref="Gtin" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(o => o.Gtin);

        /// <summary>
        ///     Gets or sets the GTIN of this instance.
        /// </summary>
        public String Gtin
        {
            get { return GetProperty(GtinProperty); }
            set { SetProperty(GtinProperty, value); }
        }

        #endregion

        #region Sequence Number

        /// <summary>
        ///     Metadata for the <see cref="SequenceNumber" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SequenceNumberProperty =
            RegisterModelProperty<Int32>(o => o.SequenceNumber);

        /// <summary>
        ///     Gets or sets sequence number associated with this instance.
        /// </summary>
        public Int32 SequenceNumber
        {
            get { return GetProperty(SequenceNumberProperty); }
            set { SetProperty(SequenceNumberProperty, value); }
        }

        #endregion

        #region PlanogramSequenceGroupSubGroupId

        /// <summary>
        ///     Metadata for the <see cref="PlanogramSequenceGroupSubGroupId" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramSequenceGroupSubGroupIdProperty =
            RegisterModelProperty<Object>(o => o.PlanogramSequenceGroupSubGroupId);

        /// <summary>
        ///     Gets or sets the Id of the <see cref="PlanogramSequenceGroupSubGroup"/> that this product belongs to (if any).
        /// </summary>
        public Object PlanogramSequenceGroupSubGroupId
        {
            get { return GetProperty(PlanogramSequenceGroupSubGroupIdProperty); }
            set { SetProperty(PlanogramSequenceGroupSubGroupIdProperty, value); }
        }

        #endregion

        #region Product

        /// <summary>
        ///     Gets or sets product
        /// </summary> 

        private PlanogramProduct _product;
        public PlanogramProduct Product
        {
            get { return _product; }
            set {
                if (value != null)
                {
                    _product = value.Copy();
                }
            }
        }


        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this <see cref="PlanogramSequenceGroupProduct"/>.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Define the authorization rules for <see cref="PlanogramSequenceGroupProduct"/>.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //TODO Implement AddObjectAuthorizationRules for PlanogramSequenceGroupProduct.
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroupProduct"/>.
        /// </summary>
        public static PlanogramSequenceGroupProduct NewPlanogramSequenceGroupProduct()
        {
            var item = new PlanogramSequenceGroupProduct();
            item.Create();
            return item;
        }

        public static PlanogramSequenceGroupProduct NewPlanogramSequenceGroupProduct(PlanogramProduct source)
        {
            var item = new PlanogramSequenceGroupProduct();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Invoked when creating a new instance of <see cref="PlanogramSequenceGroupProduct"/>.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }

        private void Create(IPlanogramProductInfo source)
        {
            LoadProperty(GtinProperty, source.Gtin);
            LoadProperty(SequenceNumberProperty, 0);
            Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Gets the <see cref="PlanogramSequenceGroupSubGroup"/> that this product belongs to (if any).
        /// </summary>
        /// <returns></returns>
        public PlanogramSequenceGroupSubGroup GetSubGroup()
        {
            if(PlanogramSequenceGroupSubGroupId == null) return null;
            return Parent.SubGroups.FindById(PlanogramSequenceGroupSubGroupId);
        }

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramSequenceGroupProduct>(oldId, newId);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // Front
            Object subGroupId = context.ResolveId<PlanogramSequenceGroupSubGroup>(this.ReadProperty<Object>(PlanogramSequenceGroupSubGroupIdProperty));
            if (subGroupId != null) this.LoadProperty<Object>(PlanogramSequenceGroupSubGroupIdProperty, subGroupId);
        }

        #endregion
    }
}
