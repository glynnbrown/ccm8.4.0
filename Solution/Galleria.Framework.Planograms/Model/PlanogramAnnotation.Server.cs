﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAnnotation
    {
        #region Constructor
        private PlanogramAnnotation() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAnnotation Fetch(IDalContext dalContext, PlanogramAnnotationDto dto)
        {
            return DataPortal.FetchChild<PlanogramAnnotation>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAnnotationDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, dto.PlanogramFixtureItemId);
            this.LoadProperty<Object>(PlanogramFixtureAssemblyIdProperty, dto.PlanogramFixtureAssemblyId);
            this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, dto.PlanogramFixtureComponentId);
            this.LoadProperty<Object>(PlanogramAssemblyComponentIdProperty, dto.PlanogramAssemblyComponentId);
            this.LoadProperty<Object>(PlanogramSubComponentIdProperty, dto.PlanogramSubComponentId);
            this.LoadProperty<Object>(PlanogramPositionIdProperty, dto.PlanogramPositionId);
            this.LoadProperty<PlanogramAnnotationType>(AnnotationTypeProperty, (PlanogramAnnotationType)dto.AnnotationType);
            this.LoadProperty<String>(TextProperty, dto.Text);
            this.LoadProperty<Single?>(XProperty, dto.X);
            this.LoadProperty<Single?>(YProperty, dto.Y);
            this.LoadProperty<Single?>(ZProperty, dto.Z);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int32>(BorderColourProperty, dto.BorderColour);
            this.LoadProperty<Single>(BorderThicknessProperty, dto.BorderThickness);
            this.LoadProperty<Int32>(BackgroundColourProperty, dto.BackgroundColour);
            this.LoadProperty<Int32>(FontColourProperty, dto.FontColour);
            this.LoadProperty<Single>(FontSizeProperty, dto.FontSize);
            this.LoadProperty<String>(FontNameProperty, dto.FontName);
            this.LoadProperty<Boolean>(CanReduceFontToFitProperty, dto.CanReduceFontToFit);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramAnnotationDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramAnnotationDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                PlanogramFixtureItemId = this.ReadProperty<Object>(PlanogramFixtureItemIdProperty),
                PlanogramFixtureAssemblyId = this.ReadProperty<Object>(PlanogramFixtureAssemblyIdProperty),
                PlanogramFixtureComponentId = this.ReadProperty<Object>(PlanogramFixtureComponentIdProperty),
                PlanogramAssemblyComponentId = this.ReadProperty<Object>(PlanogramAssemblyComponentIdProperty),
                PlanogramSubComponentId = this.ReadProperty<Object>(PlanogramSubComponentIdProperty),
                PlanogramPositionId = this.ReadProperty<Object>(PlanogramPositionIdProperty),
                AnnotationType = (Byte)this.ReadProperty<PlanogramAnnotationType>(AnnotationTypeProperty),
                Text = this.ReadProperty<String>(TextProperty),
                X = this.ReadProperty<Single?>(XProperty),
                Y = this.ReadProperty<Single?>(YProperty),
                Z = this.ReadProperty<Single?>(ZProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                BorderColour=this.ReadProperty<Int32>(BorderColourProperty),
                BorderThickness=this.ReadProperty<Single>(BorderThicknessProperty),
                BackgroundColour=this.ReadProperty<Int32>(BackgroundColourProperty),
                FontColour=this.ReadProperty<Int32>(FontColourProperty),
                FontSize=this.ReadProperty<Single>(FontSizeProperty),
                FontName=this.ReadProperty<String>(FontNameProperty),
                CanReduceFontToFit=this.ReadProperty<Boolean>(CanReduceFontToFitProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }



        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAnnotationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAnnotationDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramAnnotationDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAnnotation>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAnnotationDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this object
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramAnnotationDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}