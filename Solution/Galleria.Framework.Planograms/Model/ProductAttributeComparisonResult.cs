﻿using System;
using Csla;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable, DefaultNewMethod("NewProductAttributeComparisonResult", "PlanogramId")]
    public partial class ProductAttributeComparisonResult : ModelObject<ProductAttributeComparisonResult>
    {

        #region Parent

        /// <summary>
        ///     The parent instance to this <see cref="ProductAttributeComparisonResult"/>.
        /// </summary>
        /// <remarks>If there is no parent at the time, null is returned. </remarks>
        public new Planogram Parent => ((ProductAttributeComparisonResultList) base.Parent)?.Parent;

        #endregion

        #region ComparedProductAttribute

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="EntityComparisonAttribute" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ComparedProductAttributeProperty =
            RegisterModelProperty<String>(c => c.ComparedProductAttribute);

        /// <summary>
        ///     Get the Id for this instance in the data base.
        /// </summary>
        public String ComparedProductAttribute
        {
            get { return GetProperty<String>(ComparedProductAttributeProperty); }
            set
            {
                SetProperty<String>(ComparedProductAttributeProperty, value);
            }
        }


        /// <summary>
        ///      <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="ProductGtin" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
            RegisterModelProperty<String>(c => c.ProductGtin);

        /// <summary>
        ///     Get the GTIN of the associated product
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }

        /// <summary>
        ///      <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="MasterDataValue" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> MasterDataValueProperty =
            RegisterModelProperty<String>(c => c.MasterDataValue);

        /// <summary>
        ///     Get the value from the MasterData Product Attribute
        /// </summary>
        public String MasterDataValue
        {
            get { return GetProperty<String>(MasterDataValueProperty); }
            set { SetProperty<String>(MasterDataValueProperty, value); }
        }

        /// <summary>
        ///      <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="ProductValue" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductValueProperty =
            RegisterModelProperty<String>(c => c.ProductValue);

        /// <summary>
        ///     Get the value from the Product Attribute
        /// </summary>
        public String ProductValue
        {
            get { return GetProperty<String>(ProductValueProperty); }
            set { SetProperty<String>(ProductValueProperty, value); }
        }

        #endregion
        
        public static ProductAttributeComparisonResult NewEntityComparisonAttributeResult(String productGTIN, String compareAttribute, String masterDataValue, String productValue)
        {
            var item = new ProductAttributeComparisonResult();
            
            item.Create(productGTIN, compareAttribute, masterDataValue, productValue);
            return item;
        }

        /// <summary>
        ///     Create a new instance of <see cref="EntityComparisonAttribute" />.
        /// </summary>
        public static ProductAttributeComparisonResult NewEntityComparisonAttributeResult()
        {
            var item = new ProductAttributeComparisonResult();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }

        private void Create(String productGtin, String compareAttribute, String masterDataValue, String productValue)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ComparedProductAttributeProperty, compareAttribute);
            LoadProperty(ProductGtinProperty, productGtin);
            LoadProperty(MasterDataValueProperty, masterDataValue);
            LoadProperty(ProductValueProperty, productValue);
            MarkAsChild();
            base.Create();
        }

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            //BusinessRules.AddRule(new Required(PlanogramProperty));
            //BusinessRules.AddRule(new MaxLength(MasterDataValueProperty, 250));
            //BusinessRules.AddRule(new MaxLength(ProductValueProperty, 250));
        }

        #endregion

        #region Authorization Rules

        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        #endregion

        public override Boolean Equals(Object obj)
        {
            return Equals(obj as ProductAttributeComparisonResult);
        }


        private Boolean Equals(ProductAttributeComparisonResult other)
        {
            if (other == null) return false;

            if (Parent == null || other.Parent == null) return false;

            Boolean result = Parent == null && other.Parent == null || Parent.Id == other.Parent.Id;
            return result && String.Equals(ProductGtin, other.ProductGtin) && String.Equals(MasterDataValue, other.MasterDataValue) && String.Equals(ProductValue, other.ProductValue);
        }

        public override Int32 GetHashCode()
        {
            unchecked
            {
                Int32 hashCode = (Parent != null ? Parent.Id.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ ComparedProductAttribute.GetHashCode();
                hashCode = (hashCode * 397) ^ (MasterDataValue != null ? MasterDataValue.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ProductValue != null ? ProductValue.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override String ToString()
        {
            var parentId = Parent != null ? Parent.Id : "????";

            return $"Planogram: {parentId}\tGtin: {ProductGtin}\tProperty:{ComparedProductAttribute}\tMaster Data Value:{MasterDataValue}\tProduct Value:{ProductValue}";
        }

        #region FetchByPlanogramIdCriteria

        /// <summary>
        ///     Criteria for the FetchByPlanogramId factory method.
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> definition for the <see cref="DalFactoryName" /> property.
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty = RegisterProperty<String>(c => c.DalFactoryName);

            /// <summary>
            ///     Get the dal factory name.
            /// </summary>
            public String DalFactoryName { get { return ReadProperty(DalFactoryNameProperty); } }

            #endregion

            #region PlanogramId

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> definition for the <see cref="PlanogramId" /> property.
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty = RegisterProperty<Object>(c => c.PlanogramId);

            /// <summary>
            ///     Get the ID corresponding to the parent Planogram.
            /// </summary>
            public Object PlanogramId { get { return ReadProperty(PlanogramIdProperty); } }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initialize a new instance of <see cref="FetchByPlanogramIdCriteria" />.
            /// </summary>
            /// <param name="dalFactoryName">Name of the dal factory in use.</param>
            /// <param name="planogramId">ID corresponding to the parent Planogram.</param>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                LoadProperty(DalFactoryNameProperty, dalFactoryName);
                LoadProperty(PlanogramIdProperty, planogramId);
            }

            #endregion
        }

        #endregion

       
    }
}
