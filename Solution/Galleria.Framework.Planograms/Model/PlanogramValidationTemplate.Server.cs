﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//  Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramValidationTemplate
    {
        #region Constructor
        private PlanogramValidationTemplate() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramValidationTemplate Fetch(IDalContext dalContext, PlanogramValidationTemplateDto dto)
        {
            return DataPortal.FetchChild<PlanogramValidationTemplate>(dalContext, dto);
        }

        public static PlanogramValidationTemplate Fetch(IDalContext dalContext, Object planogramId)
        {
            return DataPortal.FetchChild<PlanogramValidationTemplate>(new FetchByPlanogramIdCriteria(null, planogramId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationTemplateDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramValidationTemplateDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramValidationTemplateDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when performing a fetch
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (var dal = dalContext.GetDal<IPlanogramValidationTemplateDal>())
                    {
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    this.Create();
                }
            }
            this.MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramValidationTemplateDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramValidationTemplateDto>(
            (dc) =>
            {
                PlanogramValidationTemplateDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramValidationTemplate>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramValidationTemplateDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramValidationTemplateDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
