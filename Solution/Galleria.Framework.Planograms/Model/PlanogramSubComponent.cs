﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25304 : L.Ineson
//  Changed rod to be created as merch style hang from bottom
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-27477 : L.Ineson
//  Added combine helpers
// V8-27595 : L.Ineson
//  Amended CanCombine to return false for shelves with a manual x strategy.
// V8-27929 : L.ineson
//  All merchandisable types are now combinable. Added check to ensure types match.
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
// V8-27442 : L.Ineson
//  Added notch helper methods.
// V8-28547 : A.Kuszyk
//  Changed Facing Axis of HangFromBottom subcomponents to Z.
#endregion
#region Version History: (CCM 8.01)
// V8-27433 : L.Ineson
//  Amended GetPegholes so that it no longer draws holes that go outside the sub boundaries.
#endregion
#region Version History: (CCM 802)
// V8-26090 : M.Pettit
//  Added GetMaximumNotchNumberForFixture and amended GetNearestNotchNumber to round to nearest number, 
//      ensuring it is not at a Y-position greater than the height of the fixture
// V8-29023 : M.Pettit
//  Added Merchandisable Depth property
// V8-29207 : D.Pleasance
//  Amended GetNearestNotchNumber so that notch numbers of 0 or less are valid.
// V8-29203 : L.Ineson
//  Added IsProductSqueezeAllowed
#endregion
#region Version History: (CCM 803)
// V8-29557 : L.Ineson
// Amended default strategies so that none are manual.
// V8-29594 : L.Ineson
//  Commented out transparency properties from EnumerateDisplayableFieldInfos for now.
#endregion
#region Version History: CCM810
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29984 : A.Kuszyk
//  Changed default Z merch strategy of Bars to Back.
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
// V8-31055 : L.Ineson
//  Marked colour types
#endregion
#region Version History: (CCM 8.3)
// V8-31816 : L.Ineson
//  Fixed precision issue when comparing height of shelves to be combined.
// V8-32796 : L.Ineson
// Swapped out GetNearestPeghole for a version that doesnt need to cycle through all possible holes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using System.Globalization;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramSubComponent object
    /// (Child of  PlanogramComponent)
    /// 
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSubComponent : ModelObject<PlanogramSubComponent>
    {
        #region Static Constructor
        /// <summary>
        /// Static constructor for this type
        /// </summary>
        static PlanogramSubComponent()
        {
            FriendlyName = Message.PlanogramSubComponent_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference the parent component
        /// </summary>
        public new PlanogramComponent Parent
        {
            get
            {
                PlanogramSubComponentList parentList = base.Parent as PlanogramSubComponentList;
                if (parentList != null)
                {
                    return parentList.Parent as PlanogramComponent;
                }
                return null;
            }
        }
        #endregion

        #region Properties

        #region Mesh3DId
        public static readonly ModelPropertyInfo<Object> Mesh3DIdProperty =
            RegisterModelProperty<Object>(c => c.Mesh3DId);
        /// <summary>
        /// The PlanogramSubComponent Mesh3DId
        /// </summary>
        public Object Mesh3DId
        {
            get { return GetProperty<Object>(Mesh3DIdProperty); }
            set { SetProperty<Object>(Mesh3DIdProperty, value); }
        }
        #endregion

        #region ImageIdFront
        public static readonly ModelPropertyInfo<Object> ImageIdFrontProperty =
            RegisterModelProperty<Object>(c => c.ImageIdFront);
        public Object ImageIdFront
        {
            get { return GetProperty<Object>(ImageIdFrontProperty); }
            set { SetProperty<Object>(ImageIdFrontProperty, value); }
        }
        #endregion

        #region ImageIdBack
        public static readonly ModelPropertyInfo<Object> ImageIdBackProperty =
            RegisterModelProperty<Object>(c => c.ImageIdBack);
        public Object ImageIdBack
        {
            get { return GetProperty<Object>(ImageIdBackProperty); }
            set { SetProperty<Object>(ImageIdBackProperty, value); }
        }
        #endregion

        #region ImageIdTop
        public static readonly ModelPropertyInfo<Object> ImageIdTopProperty =
            RegisterModelProperty<Object>(c => c.ImageIdTop);
        public Object ImageIdTop
        {
            get { return GetProperty<Object>(ImageIdTopProperty); }
            set { SetProperty<Object>(ImageIdTopProperty, value); }
        }
        #endregion

        #region ImageIdBottom
        public static readonly ModelPropertyInfo<Object> ImageIdBottomProperty =
            RegisterModelProperty<Object>(c => c.ImageIdBottom);
        public Object ImageIdBottom
        {
            get { return GetProperty<Object>(ImageIdBottomProperty); }
            set { SetProperty<Object>(ImageIdBottomProperty, value); }
        }
        #endregion

        #region ImageIdLeft
        public static readonly ModelPropertyInfo<Object> ImageIdLeftProperty =
            RegisterModelProperty<Object>(c => c.ImageIdLeft);
        public Object ImageIdLeft
        {
            get { return GetProperty<Object>(ImageIdLeftProperty); }
            set { SetProperty<Object>(ImageIdLeftProperty, value); }
        }
        #endregion

        #region ImageIdRight
        public static readonly ModelPropertyInfo<Object> ImageIdRightProperty =
            RegisterModelProperty<Object>(c => c.ImageIdRight);
        public Object ImageIdRight
        {
            get { return GetProperty<Object>(ImageIdRightProperty); }
            set { SetProperty<Object>(ImageIdRightProperty, value); }
        }
        #endregion

        #region Name
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PlanogramSubComponent_Name);
        /// <summary>
        /// The PlanogramSubComponent Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.PlanogramSubComponent_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent Height
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramSubComponent_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent Width
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramSubComponent_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent Depth
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region X
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.PlanogramSubComponent_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent X
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.PlanogramSubComponent_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent Y
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.PlanogramSubComponent_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent Z
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, Message.PlanogramSubComponent_Slope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramSubComponent Slope
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle, Message.PlanogramSubComponent_Angle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramSubComponent Angle
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, Message.PlanogramSubComponent_Roll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramSubComponent Roll
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region ShapeType
        public static readonly ModelPropertyInfo<PlanogramSubComponentShapeType> ShapeTypeProperty =
            RegisterModelProperty<PlanogramSubComponentShapeType>(c => c.ShapeType, Message.PlanogramSubComponent_ShapeType);
        /// <summary>
        /// The PlanogramSubComponent ShapeType
        /// </summary>
        public PlanogramSubComponentShapeType ShapeType
        {
            get { return GetProperty<PlanogramSubComponentShapeType>(ShapeTypeProperty); }
            set { SetProperty<PlanogramSubComponentShapeType>(ShapeTypeProperty, value); }
        }
        #endregion

        #region MerchandisableHeight
        public static readonly ModelPropertyInfo<Single> MerchandisableHeightProperty =
            RegisterModelProperty<Single>(c => c.MerchandisableHeight, Message.PlanogramSubComponent_MerchandisableHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchandisableHeight
        /// </summary>
        public Single MerchandisableHeight
        {
            get { return GetProperty<Single>(MerchandisableHeightProperty); }
            set { SetProperty<Single>(MerchandisableHeightProperty, value); }
        }
        #endregion

        #region MerchandisableDepth
        public static readonly ModelPropertyInfo<Single> MerchandisableDepthProperty =
            RegisterModelProperty<Single>(c => c.MerchandisableDepth, Message.PlanogramSubComponent_MerchandisableDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchandisableDepth
        /// </summary>
        public Single MerchandisableDepth
        {
            get { return GetProperty<Single>(MerchandisableDepthProperty); }
            set { SetProperty<Single>(MerchandisableDepthProperty, value); }
        }
        #endregion

        #region IsVisible
        public static readonly ModelPropertyInfo<Boolean> IsVisibleProperty =
            RegisterModelProperty<Boolean>(c => c.IsVisible, Message.PlanogramSubComponent_IsVisible);
        /// <summary>
        /// The PlanogramSubComponent IsVisible
        /// </summary>
        public Boolean IsVisible
        {
            get { return GetProperty<Boolean>(IsVisibleProperty); }
            set { SetProperty<Boolean>(IsVisibleProperty, value); }
        }
        #endregion

        #region HasCollisionDetection
        public static readonly ModelPropertyInfo<Boolean> HasCollisionDetectionProperty =
            RegisterModelProperty<Boolean>(c => c.HasCollisionDetection, Message.PlanogramSubComponent_HasCollisionDetection);
        /// <summary>
        /// The PlanogramSubComponent HasCollisionDetection
        /// </summary>
        public Boolean HasCollisionDetection
        {
            get { return GetProperty<Boolean>(HasCollisionDetectionProperty); }
            set { SetProperty<Boolean>(HasCollisionDetectionProperty, value); }
        }
        #endregion

        #region FillPatternTypeFront
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeFrontProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeFront, Message.PlanogramSubComponent_FillPatternTypeFront);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeFront
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeFront
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeFrontProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeFrontProperty, value); }
        }
        #endregion

        #region FillPatternTypeBack
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeBackProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeBack, Message.PlanogramSubComponent_FillPatternTypeBack);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeBack
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeBack
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBackProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBackProperty, value); }
        }
        #endregion

        #region FillPatternTypeTop
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeTopProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeTop, Message.PlanogramSubComponent_FillPatternTypeTop);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeTop
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeTop
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeTopProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeTopProperty, value); }
        }
        #endregion

        #region FillPatternTypeBottom
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeBottomProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeBottom, Message.PlanogramSubComponent_FillPatternTypeBottom);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeBottom
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeBottom
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBottomProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBottomProperty, value); }
        }
        #endregion

        #region FillPatternTypeLeft
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeLeftProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeLeft, Message.PlanogramSubComponent_FillPatternTypeLeft);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeLeft
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeLeft
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeLeftProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeLeftProperty, value); }
        }
        #endregion

        #region FillPatternTypeRight
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> FillPatternTypeRightProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.FillPatternTypeRight, Message.PlanogramSubComponent_FillPatternTypeRight);
        /// <summary>
        /// The PlanogramSubComponent FillPatternTypeRight
        /// </summary>
        public PlanogramSubComponentFillPatternType FillPatternTypeRight
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeRightProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeRightProperty, value); }
        }
        #endregion

        #region FillColourFront
        public static readonly ModelPropertyInfo<Int32> FillColourFrontProperty =
            RegisterModelProperty<Int32>(c => c.FillColourFront, Message.PlanogramSubComponent_FillColourFront,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourFront
        /// </summary>
        public Int32 FillColourFront
        {
            get { return GetProperty<Int32>(FillColourFrontProperty); }
            set { SetProperty<Int32>(FillColourFrontProperty, value); }
        }
        #endregion

        #region FillColourBack
        public static readonly ModelPropertyInfo<Int32> FillColourBackProperty =
            RegisterModelProperty<Int32>(c => c.FillColourBack, Message.PlanogramSubComponent_FillColourBack,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourBack
        /// </summary>
        public Int32 FillColourBack
        {
            get { return GetProperty<Int32>(FillColourBackProperty); }
            set { SetProperty<Int32>(FillColourBackProperty, value); }
        }
        #endregion

        #region FillColourTop
        public static readonly ModelPropertyInfo<Int32> FillColourTopProperty =
            RegisterModelProperty<Int32>(c => c.FillColourTop, Message.PlanogramSubComponent_FillColourTop,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourTop
        /// </summary>
        public Int32 FillColourTop
        {
            get { return GetProperty<Int32>(FillColourTopProperty); }
            set { SetProperty<Int32>(FillColourTopProperty, value); }
        }
        #endregion

        #region FillColourBottom
        public static readonly ModelPropertyInfo<Int32> FillColourBottomProperty =
            RegisterModelProperty<Int32>(c => c.FillColourBottom, Message.PlanogramSubComponent_FillColourBottom,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourBottom
        /// </summary>
        public Int32 FillColourBottom
        {
            get { return GetProperty<Int32>(FillColourBottomProperty); }
            set { SetProperty<Int32>(FillColourBottomProperty, value); }
        }
        #endregion

        #region FillColourLeft
        public static readonly ModelPropertyInfo<Int32> FillColourLeftProperty =
            RegisterModelProperty<Int32>(c => c.FillColourLeft, Message.PlanogramSubComponent_FillColourLeft,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourLeft
        /// </summary>
        public Int32 FillColourLeft
        {
            get { return GetProperty<Int32>(FillColourLeftProperty); }
            set { SetProperty<Int32>(FillColourLeftProperty, value); }
        }
        #endregion

        #region FillColourRight
        public static readonly ModelPropertyInfo<Int32> FillColourRightProperty =
            RegisterModelProperty<Int32>(c => c.FillColourRight, Message.PlanogramSubComponent_FillColourRight,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent FillColourRight
        /// </summary>
        public Int32 FillColourRight
        {
            get { return GetProperty<Int32>(FillColourRightProperty); }
            set { SetProperty<Int32>(FillColourRightProperty, value); }
        }
        #endregion

        #region LineColour
        public static readonly ModelPropertyInfo<Int32> LineColourProperty =
            RegisterModelProperty<Int32>(c => c.LineColour, Message.PlanogramSubComponent_LineColour,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The PlanogramSubComponent LineColour
        /// </summary>
        public Int32 LineColour
        {
            get { return GetProperty<Int32>(LineColourProperty); }
            set { SetProperty<Int32>(LineColourProperty, value); }
        }
        #endregion

        #region TransparencyPercentFront
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentFrontProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentFront, Message.PlanogramSubComponent_TransparencyPercentFront);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentFront
        /// </summary>
        public Int32 TransparencyPercentFront
        {
            get { return GetProperty<Int32>(TransparencyPercentFrontProperty); }
            set { SetProperty<Int32>(TransparencyPercentFrontProperty, value); }
        }
        #endregion

        #region TransparencyPercentBack
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentBackProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentBack, Message.PlanogramSubComponent_TransparencyPercentBack);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentBack
        /// </summary>
        public Int32 TransparencyPercentBack
        {
            get { return GetProperty<Int32>(TransparencyPercentBackProperty); }
            set { SetProperty<Int32>(TransparencyPercentBackProperty, value); }
        }
        #endregion

        #region TransparencyPercentTop
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentTopProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentTop, Message.PlanogramSubComponent_TransparencyPercentTop);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentTop
        /// </summary>
        public Int32 TransparencyPercentTop
        {
            get { return GetProperty<Int32>(TransparencyPercentTopProperty); }
            set { SetProperty<Int32>(TransparencyPercentTopProperty, value); }
        }
        #endregion

        #region TransparencyPercentBottom
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentBottomProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentBottom, Message.PlanogramSubComponent_TransparencyPercentBottom);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentBottom
        /// </summary>
        public Int32 TransparencyPercentBottom
        {
            get { return GetProperty<Int32>(TransparencyPercentBottomProperty); }
            set { SetProperty<Int32>(TransparencyPercentBottomProperty, value); }
        }
        #endregion

        #region TransparencyPercentLeft
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentLeftProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentLeft, Message.PlanogramSubComponent_TransparencyPercentLeft);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentLeft
        /// </summary>
        public Int32 TransparencyPercentLeft
        {
            get { return GetProperty<Int32>(TransparencyPercentLeftProperty); }
            set { SetProperty<Int32>(TransparencyPercentLeftProperty, value); }
        }
        #endregion

        #region TransparencyPercentRight
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentRightProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentRight, Message.PlanogramSubComponent_TransparencyPercentRight);
        /// <summary>
        /// The PlanogramSubComponent TransparencyPercentRight
        /// </summary>
        public Int32 TransparencyPercentRight
        {
            get { return GetProperty<Int32>(TransparencyPercentRightProperty); }
            set { SetProperty<Int32>(TransparencyPercentRightProperty, value); }
        }
        #endregion

        #region FaceThicknessFront
        public static readonly ModelPropertyInfo<Single> FaceThicknessFrontProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessFront, Message.PlanogramSubComponent_FaceThicknessFront, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessFront
        /// </summary>
        public Single FaceThicknessFront
        {
            get { return GetProperty<Single>(FaceThicknessFrontProperty); }
            set { SetProperty<Single>(FaceThicknessFrontProperty, value); }
        }
        #endregion

        #region FaceThicknessBack
        public static readonly ModelPropertyInfo<Single> FaceThicknessBackProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessBack, Message.PlanogramSubComponent_FaceThicknessBack, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessBack
        /// </summary>
        public Single FaceThicknessBack
        {
            get { return GetProperty<Single>(FaceThicknessBackProperty); }
            set { SetProperty<Single>(FaceThicknessBackProperty, value); }
        }
        #endregion

        #region FaceThicknessTop
        public static readonly ModelPropertyInfo<Single> FaceThicknessTopProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessTop, Message.PlanogramSubComponent_FaceThicknessTop, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessTop
        /// </summary>
        public Single FaceThicknessTop
        {
            get { return GetProperty<Single>(FaceThicknessTopProperty); }
            set { SetProperty<Single>(FaceThicknessTopProperty, value); }
        }
        #endregion

        #region FaceThicknessBottom
        public static readonly ModelPropertyInfo<Single> FaceThicknessBottomProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessBottom, Message.PlanogramSubComponent_FaceThicknessBottom, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessBottom
        /// </summary>
        public Single FaceThicknessBottom
        {
            get { return GetProperty<Single>(FaceThicknessBottomProperty); }
            set { SetProperty<Single>(FaceThicknessBottomProperty, value); }
        }
        #endregion

        #region FaceThicknessLeft
        public static readonly ModelPropertyInfo<Single> FaceThicknessLeftProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessLeft, Message.PlanogramSubComponent_FaceThicknessLeft, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessLeft
        /// </summary>
        public Single FaceThicknessLeft
        {
            get { return GetProperty<Single>(FaceThicknessLeftProperty); }
            set { SetProperty<Single>(FaceThicknessLeftProperty, value); }
        }
        #endregion

        #region FaceThicknessRight
        public static readonly ModelPropertyInfo<Single> FaceThicknessRightProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessRight, Message.PlanogramSubComponent_FaceThicknessRight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent FaceThicknessRight
        /// </summary>
        public Single FaceThicknessRight
        {
            get { return GetProperty<Single>(FaceThicknessRightProperty); }
            set { SetProperty<Single>(FaceThicknessRightProperty, value); }
        }
        #endregion

        #region RiserHeight
        public static readonly ModelPropertyInfo<Single> RiserHeightProperty =
            RegisterModelProperty<Single>(c => c.RiserHeight, Message.PlanogramSubComponent_RiserHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent RiserHeight
        /// </summary>
        public Single RiserHeight
        {
            get { return GetProperty<Single>(RiserHeightProperty); }
            set { SetProperty<Single>(RiserHeightProperty, value); }
        }
        #endregion

        #region RiserThickness
        public static readonly ModelPropertyInfo<Single> RiserThicknessProperty =
            RegisterModelProperty<Single>(c => c.RiserThickness, Message.PlanogramSubComponent_RiserThickness, ModelPropertyDisplayType.Length);
        public Single RiserThickness
        {
            get { return GetProperty<Single>(RiserThicknessProperty); }
            set { SetProperty<Single>(RiserThicknessProperty, value); }
        }
        #endregion

        #region RiserColour
        public static readonly ModelPropertyInfo<Int32> RiserColourProperty =
            RegisterModelProperty<Int32>(c => c.RiserColour, Message.PlanogramSubComponent_RiserColour,
            ModelPropertyDisplayType.Colour);
        public Int32 RiserColour
        {
            get { return GetProperty<Int32>(RiserColourProperty); }
            set { SetProperty<Int32>(RiserColourProperty, value); }
        }
        #endregion

        #region RiserTransparencyPercent
        public static readonly ModelPropertyInfo<Int32> RiserTransparencyPercentProperty =
            RegisterModelProperty<Int32>(c => c.RiserTransparencyPercent, Message.PlanogramSubComponent_RiserTransparencyPercent);
        public Int32 RiserTransparencyPercent
        {
            get { return GetProperty<Int32>(RiserTransparencyPercentProperty); }
            set { SetProperty<Int32>(RiserTransparencyPercentProperty, value); }
        }
        #endregion

        #region IsRiserPlacedOnFront
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnFrontProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnFront, Message.PlanogramSubComponent_IsRiserPlacedOnFront);
        /// <summary>
        /// Whether the not is placed on the front of the subcomponent.
        /// </summary>
        public Boolean IsRiserPlacedOnFront
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnFrontProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnFrontProperty, value); }
        }
        #endregion

        #region IsRiserPlacedOnBack
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnBackProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnBack, Message.PlanogramSubComponent_IsRiserPlacedOnBack);
        /// <summary>
        /// Whether the not is placed on the back of the subcomponent.
        /// </summary>
        public Boolean IsRiserPlacedOnBack
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnBackProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnBackProperty, value); }
        }
        #endregion

        #region IsRiserPlacedOnLeft
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnLeft, Message.PlanogramSubComponent_IsRiserPlacedOnLeft);
        /// <summary>
        /// Whether the not is placed on the left of the subcomponent.
        /// </summary>
        public Boolean IsRiserPlacedOnLeft
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnLeftProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnLeftProperty, value); }
        }
        #endregion

        #region IsRiserPlacedOnRight
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnRightProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnRight, Message.PlanogramSubComponent_IsRiserPlacedOnRight);
        /// <summary>
        /// Whether the not is placed on the right of the subcomponent.
        /// </summary>
        public Boolean IsRiserPlacedOnRight
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnRightProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnRightProperty, value); }
        }
        #endregion

        #region RiserFillPatternType
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> RiserFillPatternTypeProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.RiserFillPatternType, Message.PlanogramSubComponent_RiserFillPatternType);
        /// <summary>
        /// The PlanogramSubComponent RiserFillPatternType
        /// </summary>
        public PlanogramSubComponentFillPatternType RiserFillPatternType
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(RiserFillPatternTypeProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(RiserFillPatternTypeProperty, value); }
        }
        #endregion

        #region NotchStartX
        public static readonly ModelPropertyInfo<Single> NotchStartXProperty =
            RegisterModelProperty<Single>(c => c.NotchStartX, Message.PlanogramSubComponent_NotchStartX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchStartX
        /// </summary>
        public Single NotchStartX
        {
            get { return GetProperty<Single>(NotchStartXProperty); }
            set { SetProperty<Single>(NotchStartXProperty, value); }
        }
        #endregion

        #region NotchStartY
        public static readonly ModelPropertyInfo<Single> NotchStartYProperty =
            RegisterModelProperty<Single>(c => c.NotchStartY, Message.PlanogramSubComponent_NotchStartY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchStartY
        /// </summary>
        public Single NotchStartY
        {
            get { return GetProperty<Single>(NotchStartYProperty); }
            set { SetProperty<Single>(NotchStartYProperty, value); }
        }
        #endregion

        #region NotchSpacingX
        public static readonly ModelPropertyInfo<Single> NotchSpacingXProperty =
            RegisterModelProperty<Single>(c => c.NotchSpacingX, Message.PlanogramSubComponent_NotchSpacingX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchSpacingX
        /// </summary>
        public Single NotchSpacingX
        {
            get { return GetProperty<Single>(NotchSpacingXProperty); }
            set { SetProperty<Single>(NotchSpacingXProperty, value); }
        }
        #endregion

        #region NotchSpacingY
        public static readonly ModelPropertyInfo<Single> NotchSpacingYProperty =
           RegisterModelProperty<Single>(c => c.NotchSpacingY, Message.PlanogramSubComponent_NotchSpacingY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchSpacingY
        /// </summary>
        public Single NotchSpacingY
        {
            get { return GetProperty<Single>(NotchSpacingYProperty); }
            set { SetProperty<Single>(NotchSpacingYProperty, value); }
        }
        #endregion

        #region NotchHeight
        public static readonly ModelPropertyInfo<Single> NotchHeightProperty =
            RegisterModelProperty<Single>(c => c.NotchHeight, Message.PlanogramSubComponent_NotchHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchHeight
        /// </summary>
        public Single NotchHeight
        {
            get { return GetProperty<Single>(NotchHeightProperty); }
            set { SetProperty<Single>(NotchHeightProperty, value); }
        }
        #endregion

        #region NotchWidth
        public static readonly ModelPropertyInfo<Single> NotchWidthProperty =
           RegisterModelProperty<Single>(c => c.NotchWidth, Message.PlanogramSubComponent_NotchWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent NotchWidth
        /// </summary>
        public Single NotchWidth
        {
            get { return GetProperty<Single>(NotchWidthProperty); }
            set { SetProperty<Single>(NotchWidthProperty, value); }
        }
        #endregion

        #region IsNotchPlacedOnFront
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnFrontProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnFront, Message.PlanogramSubComponent_IsNotchPlacedOnFront);
        /// <summary>
        /// Whether the not is placed on the front of the subcomponent.
        /// </summary>
        public Boolean IsNotchPlacedOnFront
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnFrontProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnFrontProperty, value); }
        }
        #endregion

        #region IsNotchPlacedOnBack
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnBackProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnBack, Message.PlanogramSubComponent_IsNotchPlacedOnBack);
        /// <summary>
        /// Whether the not is placed on the back of the subcomponent.
        /// </summary>
        public Boolean IsNotchPlacedOnBack
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnBackProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnBackProperty, value); }
        }
        #endregion

        #region IsNotchPlacedOnLeft
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnLeft, Message.PlanogramSubComponent_IsNotchPlacedOnLeft);
        /// <summary>
        /// Whether the not is placed on the left of the subcomponent.
        /// </summary>
        public Boolean IsNotchPlacedOnLeft
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnLeftProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnLeftProperty, value); }
        }
        #endregion

        #region IsNotchPlacedOnRight
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnRightProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnRight, Message.PlanogramSubComponent_IsNotchPlacedOnRight);
        /// <summary>
        /// Whether the not is placed on the right of the subcomponent.
        /// </summary>
        public Boolean IsNotchPlacedOnRight
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnRightProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnRightProperty, value); }
        }
        #endregion

        #region NotchStyleType
        public static readonly ModelPropertyInfo<PlanogramSubComponentNotchStyleType> NotchStyleTypeProperty =
            RegisterModelProperty<PlanogramSubComponentNotchStyleType>(c => c.NotchStyleType, Message.PlanogramSubComponent_NotchStyleType);
        /// <summary>
        /// The PlanogramSubComponent NotchStyleType
        /// </summary>
        public PlanogramSubComponentNotchStyleType NotchStyleType
        {
            get { return GetProperty<PlanogramSubComponentNotchStyleType>(NotchStyleTypeProperty); }
            set { SetProperty<PlanogramSubComponentNotchStyleType>(NotchStyleTypeProperty, value); }
        }
        #endregion

        #region DividerObstructionHeight
        public static readonly ModelPropertyInfo<Single> DividerObstructionHeightProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionHeight, Message.PlanogramSubComponent_DividerObstructionHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionHeight
        /// </summary>
        public Single DividerObstructionHeight
        {
            get { return GetProperty<Single>(DividerObstructionHeightProperty); }
            set { SetProperty<Single>(DividerObstructionHeightProperty, value); }
        }
        #endregion

        #region DividerObstructionWidth
        public static readonly ModelPropertyInfo<Single> DividerObstructionWidthProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionWidth, Message.PlanogramSubComponent_DividerObstructionWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionWidth
        /// </summary>
        public Single DividerObstructionWidth
        {
            get { return GetProperty<Single>(DividerObstructionWidthProperty); }
            set { SetProperty<Single>(DividerObstructionWidthProperty, value); }
        }
        #endregion

        #region DividerObstructionDepth
        public static readonly ModelPropertyInfo<Single> DividerObstructionDepthProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionDepth, Message.PlanogramSubComponent_DividerObstructionDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionDepth
        /// </summary>
        public Single DividerObstructionDepth
        {
            get { return GetProperty<Single>(DividerObstructionDepthProperty); }
            set { SetProperty<Single>(DividerObstructionDepthProperty, value); }
        }
        #endregion

        #region DividerObstructionStartX
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartXProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartX, Message.PlanogramSubComponent_DividerObstructionStartX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionStartX
        /// </summary>
        public Single DividerObstructionStartX
        {
            get { return GetProperty<Single>(DividerObstructionStartXProperty); }
            set { SetProperty<Single>(DividerObstructionStartXProperty, value); }
        }
        #endregion

        #region DividerObstructionSpacingX
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingXProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingX, Message.PlanogramSubComponent_DividerObstructionSpacingX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionSpacingX
        /// </summary>
        public Single DividerObstructionSpacingX
        {
            get { return GetProperty<Single>(DividerObstructionSpacingXProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingXProperty, value); }
        }
        #endregion

        #region DividerObstructionStartY
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartYProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartY, Message.PlanogramSubComponent_DividerObstructionStartY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionStartY
        /// </summary>
        public Single DividerObstructionStartY
        {
            get { return GetProperty<Single>(DividerObstructionStartYProperty); }
            set { SetProperty<Single>(DividerObstructionStartYProperty, value); }
        }
        #endregion

        #region DividerObstructionSpacingY
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingYProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingY, Message.PlanogramSubComponent_DividerObstructionSpacingY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionSpacingY
        /// </summary>
        public Single DividerObstructionSpacingY
        {
            get { return GetProperty<Single>(DividerObstructionSpacingYProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingYProperty, value); }
        }
        #endregion

        #region DividerObstructionStartZ
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartZProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartZ, Message.PlanogramSubComponent_DividerObstructionStartZ, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionStartZ
        /// </summary>
        public Single DividerObstructionStartZ
        {
            get { return GetProperty<Single>(DividerObstructionStartZProperty); }
            set { SetProperty<Single>(DividerObstructionStartZProperty, value); }
        }
        #endregion

        #region DividerObstructionSpacingZ
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingZProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingZ, Message.PlanogramSubComponent_DividerObstructionSpacingZ, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent DividerObstructionSpacingZ
        /// </summary>
        public Single DividerObstructionSpacingZ
        {
            get { return GetProperty<Single>(DividerObstructionSpacingZProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingZProperty, value); }
        }
        #endregion

        #region IsDividerObstructionAtStart
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionAtStartProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionAtStart, Message.PlanogramSubComponent_IsDividerObstructionAtStart);
        /// <summary>
        /// Indicates if a divider will be placed before the first position
        /// </summary>
        public Boolean IsDividerObstructionAtStart
        {
            get { return GetProperty<Boolean>(IsDividerObstructionAtStartProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionAtStartProperty, value); }
        }
        #endregion

        #region IsDividerObstructionAtEnd
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionAtEndProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionAtEnd, Message.PlanogramSubComponent_IsDividerObstructionAtEnd);
        /// <summary>
        /// Indicates if a divider will be placed after the last position
        /// </summary>
        public Boolean IsDividerObstructionAtEnd
        {
            get { return GetProperty<Boolean>(IsDividerObstructionAtEndProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionAtEndProperty, value); }
        }
        #endregion

        #region IsDividerObstructionByFacing
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionByFacingProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionByFacing, Message.PlanogramSubComponent_AreDividersPlacedBetweenFacings);
        /// <summary>
        /// Indicates if the divider start and spacing fields will be ignored, instead a divider will be placed between each facing
        /// </summary>
        public Boolean IsDividerObstructionByFacing
        {
            get { return GetProperty<Boolean>(IsDividerObstructionByFacingProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionByFacingProperty, value); }
        }
        #endregion

        #region DividerObstructionFillColour
        public static readonly ModelPropertyInfo<Int32> DividerObstructionFillColourProperty =
            RegisterModelProperty<Int32>(c => c.DividerObstructionFillColour, Message.PlanogramSubComponent_DividerObstructionFillColour, 
                ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The fill colour that should be used to render dividers.
        /// </summary>
        public Int32 DividerObstructionFillColour
        {
            get { return GetProperty<Int32>(DividerObstructionFillColourProperty); }
            set { SetProperty<Int32>(DividerObstructionFillColourProperty, value); }
        }
        #endregion

        #region DividerObstructionFillPattern
        public static readonly ModelPropertyInfo<PlanogramSubComponentFillPatternType> DividerObstructionFillPatternProperty =
            RegisterModelProperty<PlanogramSubComponentFillPatternType>(c => c.DividerObstructionFillPattern, Message.PlanogramSubComponent_DividerObstructionFillPattern);
        /// <summary>
        /// The fill pattern that should be used to render dividers.
        /// </summary>
        public PlanogramSubComponentFillPatternType DividerObstructionFillPattern
        {
            get { return GetProperty<PlanogramSubComponentFillPatternType>(DividerObstructionFillPatternProperty); }
            set { SetProperty<PlanogramSubComponentFillPatternType>(DividerObstructionFillPatternProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1StartX
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1StartXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1StartX, Message.PlanogramSubComponent_MerchConstraintRow1StartX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow1StartX
        /// </summary>
        public Single MerchConstraintRow1StartX
        {
            get { return GetProperty<Single>(MerchConstraintRow1StartXProperty); }
            set { SetProperty<Single>(MerchConstraintRow1StartXProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1SpacingX
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1SpacingXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1SpacingX, Message.PlanogramSubComponent_MerchConstraintRow1SpacingX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow1SpacingX
        /// </summary>
        public Single MerchConstraintRow1SpacingX
        {
            get { return GetProperty<Single>(MerchConstraintRow1SpacingXProperty); }
            set { SetProperty<Single>(MerchConstraintRow1SpacingXProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1StartY
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1StartYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1StartY, Message.PlanogramSubComponent_MerchConstraintRow1StartY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow1StartY
        /// </summary>
        public Single MerchConstraintRow1StartY
        {
            get { return GetProperty<Single>(MerchConstraintRow1StartYProperty); }
            set { SetProperty<Single>(MerchConstraintRow1StartYProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1SpacingY
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1SpacingYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1SpacingY, Message.PlanogramSubComponent_MerchConstraintRow1SpacingY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow1SpacingY
        /// </summary>
        public Single MerchConstraintRow1SpacingY
        {
            get { return GetProperty<Single>(MerchConstraintRow1SpacingYProperty); }
            set { SetProperty<Single>(MerchConstraintRow1SpacingYProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1Height
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1HeightProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1Height, Message.PlanogramSubComponent_MerchConstraintRow1Height, ModelPropertyDisplayType.Length);
        public Single MerchConstraintRow1Height
        {
            get { return GetProperty<Single>(MerchConstraintRow1HeightProperty); }
            set { SetProperty<Single>(MerchConstraintRow1HeightProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1Width
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1WidthProperty =
           RegisterModelProperty<Single>(c => c.MerchConstraintRow1Width, Message.PlanogramSubComponent_MerchConstraintRow1Width, ModelPropertyDisplayType.Length);
        public Single MerchConstraintRow1Width
        {
            get { return GetProperty<Single>(MerchConstraintRow1WidthProperty); }
            set { SetProperty<Single>(MerchConstraintRow1WidthProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2StartX
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2StartXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2StartX, Message.PlanogramSubComponent_MerchConstraintRow2StartX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow2StartX
        /// </summary>
        public Single MerchConstraintRow2StartX
        {
            get { return GetProperty<Single>(MerchConstraintRow2StartXProperty); }
            set { SetProperty<Single>(MerchConstraintRow2StartXProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2SpacingX
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2SpacingXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2SpacingX, Message.PlanogramSubComponent_MerchConstraintRow2SpacingX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow2SpacingX
        /// </summary>
        public Single MerchConstraintRow2SpacingX
        {
            get { return GetProperty<Single>(MerchConstraintRow2SpacingXProperty); }
            set { SetProperty<Single>(MerchConstraintRow2SpacingXProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2StartY
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2StartYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2StartY, Message.PlanogramSubComponent_MerchConstraintRow2StartY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow2StartY
        /// </summary>
        public Single MerchConstraintRow2StartY
        {
            get { return GetProperty<Single>(MerchConstraintRow2StartYProperty); }
            set { SetProperty<Single>(MerchConstraintRow2StartYProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2SpacingY
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2SpacingYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2SpacingY, Message.PlanogramSubComponent_MerchConstraintRow2SpacingY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramSubComponent MerchConstraintRow2SpacingY
        /// </summary>
        public Single MerchConstraintRow2SpacingY
        {
            get { return GetProperty<Single>(MerchConstraintRow2SpacingYProperty); }
            set { SetProperty<Single>(MerchConstraintRow2SpacingYProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2Height
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2HeightProperty =
           RegisterModelProperty<Single>(c => c.MerchConstraintRow2Height, Message.PlanogramSubComponent_MerchConstraintRow2Height, ModelPropertyDisplayType.Length);
        public Single MerchConstraintRow2Height
        {
            get { return GetProperty<Single>(MerchConstraintRow2HeightProperty); }
            set { SetProperty<Single>(MerchConstraintRow2HeightProperty, value); }
        }
        #endregion

        #region MerchConstraintRow2Width
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2WidthProperty =
           RegisterModelProperty<Single>(c => c.MerchConstraintRow2Width, Message.PlanogramSubComponent_MerchConstraintRow2Width, ModelPropertyDisplayType.Length);
        public Single MerchConstraintRow2Width
        {
            get { return GetProperty<Single>(MerchConstraintRow2WidthProperty); }
            set { SetProperty<Single>(MerchConstraintRow2WidthProperty, value); }
        }
        #endregion

        #region LineThickness
        public static readonly ModelPropertyInfo<Single> LineThicknessProperty =
            RegisterModelProperty<Single>(c => c.LineThickness, Message.PlanogramSubComponent_LineThickness);
        /// <summary>
        /// Gets/Sets the thickness of the subcomponent lines.
        /// </summary>
        public Single LineThickness
        {
            get { return GetProperty<Single>(LineThicknessProperty); }
            set { SetProperty<Single>(LineThicknessProperty, value); }
        }
        #endregion

        #region MerchandisingType
        public static readonly ModelPropertyInfo<PlanogramSubComponentMerchandisingType> MerchandisingTypeProperty =
            RegisterModelProperty<PlanogramSubComponentMerchandisingType>(c => c.MerchandisingType, Message.PlanogramSubComponent_MerchandisingType);
        /// <summary>
        /// Gets/Sets how this subcomponent should be merchandised.
        /// </summary>
        public PlanogramSubComponentMerchandisingType MerchandisingType
        {
            get { return GetProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty); }
            set { SetProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, value); }
        }
        #endregion

        #region CombineType
        public static readonly ModelPropertyInfo<PlanogramSubComponentCombineType> CombineTypeProperty =
            RegisterModelProperty<PlanogramSubComponentCombineType>(c => c.CombineType, Message.PlanogramSubComponent_CombineType);
        /// <summary>
        /// specifies whether a shelf subcomponent can be combined with another.
        /// </summary>
        public PlanogramSubComponentCombineType CombineType
        {
            get { return GetProperty<PlanogramSubComponentCombineType>(CombineTypeProperty); }
            set { SetProperty<PlanogramSubComponentCombineType>(CombineTypeProperty, value); }
        }
        #endregion

        #region IsProductOverlapAllowed
        public static readonly ModelPropertyInfo<Boolean> IsProductOverlapAllowedProperty =
            RegisterModelProperty<Boolean>(c => c.IsProductOverlapAllowed, Message.PlanogramSubComponent_IsProductOverlapAllowed);
        /// <summary>
        /// The PlanogramSubComponent IsProductOverlapAllowed
        /// </summary>
        public Boolean IsProductOverlapAllowed
        {
            get { return GetProperty<Boolean>(IsProductOverlapAllowedProperty); }
            set { SetProperty<Boolean>(IsProductOverlapAllowedProperty, value); }
        }
        #endregion

        #region IsProductSqueezeAllowed
        /// <summary>
        /// IsProductSqueezeAllowed property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsProductSqueezeAllowedProperty =
            RegisterModelProperty<Boolean>(c => c.IsProductSqueezeAllowed, Message.PlanogramSubComponent_IsProductSqueezeAllowed);
        /// <summary>
        /// Gets/Sets whether products on this subcomponent can be squeezed.
        /// </summary>
        public Boolean IsProductSqueezeAllowed
        {
            get { return GetProperty<Boolean>(IsProductSqueezeAllowedProperty); }
            set { SetProperty<Boolean>(IsProductSqueezeAllowedProperty, value); }
        }
        #endregion

        #region MerchandisingStrategyX
        public static readonly ModelPropertyInfo<PlanogramSubComponentXMerchStrategyType> MerchandisingStrategyXProperty =
            RegisterModelProperty<PlanogramSubComponentXMerchStrategyType>(c => c.MerchandisingStrategyX, Message.PlanogramSubComponent_MerchandisingStrategyX);
        /// <summary>
        /// Determines how positions should be placed along the width of the subcomponent.
        /// </summary>
        public PlanogramSubComponentXMerchStrategyType MerchandisingStrategyX
        {
            get { return GetProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty); }
            set { SetProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, value); }
        }
        #endregion

        #region MerchandisingStrategyY
        public static readonly ModelPropertyInfo<PlanogramSubComponentYMerchStrategyType> MerchandisingStrategyYProperty =
            RegisterModelProperty<PlanogramSubComponentYMerchStrategyType>(c => c.MerchandisingStrategyY, Message.PlanogramSubComponent_MerchandisingStrategyY);
        /// <summary>
        /// Determines how positions should be placed along the height of the subcomponent
        /// </summary>
        public PlanogramSubComponentYMerchStrategyType MerchandisingStrategyY
        {
            get { return GetProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty); }
            set { SetProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, value); }
        }
        #endregion

        #region MerchandisingStrategyZ
        public static readonly ModelPropertyInfo<PlanogramSubComponentZMerchStrategyType> MerchandisingStrategyZProperty =
            RegisterModelProperty<PlanogramSubComponentZMerchStrategyType>(c => c.MerchandisingStrategyZ, Message.PlanogramSubComponent_MerchandisingStrategyZ);
        /// <summary>
        /// Determines how positions should be placed along the depth of the subcomponent
        /// </summary>
        public PlanogramSubComponentZMerchStrategyType MerchandisingStrategyZ
        {
            get { return GetProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty); }
            set { SetProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, value); }
        }
        #endregion

        #region LeftOverhang
        public static readonly ModelPropertyInfo<Single> LeftOverhangProperty =
            RegisterModelProperty<Single>(c => c.LeftOverhang, Message.PlanogramSubComponent_LeftOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the left side of the subcomponent.
        /// </summary>
        public Single LeftOverhang
        {
            get { return GetProperty<Single>(LeftOverhangProperty); }
            set { SetProperty<Single>(LeftOverhangProperty, value); }
        }
        #endregion

        #region RightOverhang
        public static readonly ModelPropertyInfo<Single> RightOverhangProperty =
            RegisterModelProperty<Single>(c => c.RightOverhang, Message.PlanogramSubComponent_RightOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the right side of the subcomponent
        /// </summary>
        public Single RightOverhang
        {
            get { return GetProperty<Single>(RightOverhangProperty); }
            set { SetProperty<Single>(RightOverhangProperty, value); }
        }
        #endregion

        #region FrontOverhang
        public static readonly ModelPropertyInfo<Single> FrontOverhangProperty =
            RegisterModelProperty<Single>(c => c.FrontOverhang, Message.PlanogramSubComponent_FrontOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the front of the subcomponent
        /// </summary>
        public Single FrontOverhang
        {
            get { return GetProperty<Single>(FrontOverhangProperty); }
            set { SetProperty<Single>(FrontOverhangProperty, value); }
        }
        #endregion

        #region BackOverhang
        public static readonly ModelPropertyInfo<Single> BackOverhangProperty =
            RegisterModelProperty<Single>(c => c.BackOverhang, Message.PlanogramSubComponent_BackOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the back of the subcomponent
        /// </summary>
        public Single BackOverhang
        {
            get { return GetProperty<Single>(BackOverhangProperty); }
            set { SetProperty<Single>(BackOverhangProperty, value); }
        }
        #endregion

        #region TopOverhang
        public static readonly ModelPropertyInfo<Single> TopOverhangProperty =
            RegisterModelProperty<Single>(c => c.TopOverhang, Message.PlanogramSubComponent_TopOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the top of the subcomponent
        /// </summary>
        public Single TopOverhang
        {
            get { return GetProperty<Single>(TopOverhangProperty); }
            set { SetProperty<Single>(TopOverhangProperty, value); }
        }
        #endregion

        #region BottomOverhang
        public static readonly ModelPropertyInfo<Single> BottomOverhangProperty =
            RegisterModelProperty<Single>(c => c.BottomOverhang, Message.PlanogramSubComponent_BottomOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The amount by which positions can overhang the bottom of the subcomponent
        /// </summary>
        public Single BottomOverhang
        {
            get { return GetProperty<Single>(BottomOverhangProperty); }
            set { SetProperty<Single>(BottomOverhangProperty, value); }
        }
        #endregion

        #endregion

        #region Authorisation Rules

        private static void AddObjectAuthorizationRules()
        {
        }

        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

            ////size values must not be below 0:
            //BusinessRules.AddRule(new MinValue<Single>(HeightProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(WidthProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(DepthProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(MerchandisableHeightProperty, 0));

            ////face thickness values must not be below 0:
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessFrontProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessBackProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessTopProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessBottomProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessLeftProperty, 0));
            //BusinessRules.AddRule(new MinValue<Single>(FaceThicknessRightProperty, 0));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponent NewPlanogramSubComponent()
        {
            PlanogramSubComponent item = new PlanogramSubComponent();
            item.Create(Message.PlanogramSubComponent_Name_Default,
                PlanogramComponentType.Shelf, 0, 0, 0, 0);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSubComponent NewPlanogramSubComponent(String subComponentName, PlanogramComponentType componentType,
            Single width, Single height, Single depth, Int32 fillColour)
        {
            PlanogramSubComponent item = new PlanogramSubComponent();
            item.Create(subComponentName, componentType, width, height, depth, fillColour);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String subComponentName, PlanogramComponentType componentType, Single width, Single height, Single depth, Int32 fillColour)
        {
            // set basic properties
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, subComponentName);
            this.LoadProperty<Single>(WidthProperty, width);
            this.LoadProperty<Single>(HeightProperty, height);
            this.LoadProperty<Single>(DepthProperty, depth);

            // set fill colour properties
            this.LoadProperty<Int32>(FillColourFrontProperty, fillColour);
            this.LoadProperty<Int32>(FillColourBackProperty, fillColour);
            this.LoadProperty<Int32>(FillColourTopProperty, fillColour);
            this.LoadProperty<Int32>(FillColourBottomProperty, fillColour);
            this.LoadProperty<Int32>(FillColourLeftProperty, fillColour);
            this.LoadProperty<Int32>(FillColourRightProperty, fillColour);
            this.LoadProperty<Int32>(DividerObstructionFillColourProperty, -65536);

            // set notch properties
            this.LoadProperty<Single>(NotchHeightProperty, 1);
            this.LoadProperty<Single>(NotchWidthProperty, 2);
            this.LoadProperty<PlanogramSubComponentNotchStyleType>(NotchStyleTypeProperty, PlanogramSubComponentNotchStyleType.SingleRectangle);
            this.LoadProperty<Single>(NotchSpacingXProperty, 2);
            this.LoadProperty<Single>(NotchSpacingYProperty, 4);
            this.LoadProperty<Single>(NotchStartXProperty, 2);
            this.LoadProperty<Single>(NotchStartYProperty, 20);

            //merchandising properties
            this.LoadProperty<Single>(MerchandisableDepthProperty, 0);

            // set line properties
            this.LoadProperty<Int32>(LineColourProperty, -16777216);
            this.LoadProperty<Single>(LineThicknessProperty, 0.5F);

            // set component type properties
            this.LoadProperty<Boolean>(IsProductOverlapAllowedProperty, false);
            this.LoadProperty<Boolean>(IsProductSqueezeAllowedProperty, true);
            switch (componentType)
            {
                case PlanogramComponentType.Custom:
                    throw new NotSupportedException();

                case PlanogramComponentType.Backboard:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.None);
                    this.LoadProperty<Single>(LineThicknessProperty, 0.5F);
                    this.LoadProperty<Int32>(LineColourProperty, -16777216); // black
                    break;

                case PlanogramComponentType.Base:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.None);
                    this.LoadProperty<Single>(LineThicknessProperty, 0.5F);
                    this.LoadProperty<Int32>(LineColourProperty, -16777216); // black
                    break;

                case PlanogramComponentType.Shelf:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Stack);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.Bottom);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Front);
                    this.LoadProperty<Single>(RiserThicknessProperty, 1F);
                    this.LoadProperty<Single>(RiserHeightProperty, height);
                    this.LoadProperty<Int32>(RiserColourProperty, -1);
                    break;

                case PlanogramComponentType.Chest:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Stack);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.Bottom);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.FrontStacked);
                    this.LoadProperty<Single>(FaceThicknessFrontProperty, 4F);
                    this.LoadProperty<Single>(FaceThicknessBackProperty, 4F);
                    this.LoadProperty<Single>(FaceThicknessTopProperty, 0F);
                    this.LoadProperty<Single>(FaceThicknessBottomProperty, 4F);
                    this.LoadProperty<Single>(FaceThicknessLeftProperty, 4F);
                    this.LoadProperty<Single>(FaceThicknessRightProperty, 4F);
                    break;

                case PlanogramComponentType.Bar:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Hang);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.Top);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Back);
                    break;

                case PlanogramComponentType.Peg:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Hang);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.TopStacked);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Back);
                    this.LoadProperty<Single>(MerchConstraintRow1StartXProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1StartYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1HeightProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow2HeightProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow1WidthProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow2WidthProperty, 1F);
                    break;

                case PlanogramComponentType.Rod:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.HangFromBottom);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.Even);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.Top);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.FrontStacked);
                    break;

                case PlanogramComponentType.ClipStrip:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Hang);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.Even);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.TopStacked);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Back);
                    this.LoadProperty<Single>(MerchConstraintRow1StartXProperty, 2F);
                    this.LoadProperty<Single>(MerchConstraintRow1StartYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, 2F);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1HeightProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow2HeightProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow1WidthProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow2WidthProperty, 1F);
                    break;

                case PlanogramComponentType.Pallet:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Stack);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.Bottom);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Front);
                    break;

                case PlanogramComponentType.Panel:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.None);
                    break;
                case PlanogramComponentType.SlotWall:
                    this.LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, PlanogramSubComponentMerchandisingType.Hang);
                    this.LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, PlanogramSubComponentXMerchStrategyType.LeftStacked);
                    this.LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, PlanogramSubComponentYMerchStrategyType.TopStacked);
                    this.LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, PlanogramSubComponentZMerchStrategyType.Back);
                    this.LoadProperty<Single>(MerchConstraintRow1StartXProperty, 0);
                    this.LoadProperty<Single>(MerchConstraintRow1StartYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, 0);
                    this.LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, 4F);
                    this.LoadProperty<Single>(MerchConstraintRow1HeightProperty, 1F);
                    this.LoadProperty<Single>(MerchConstraintRow1WidthProperty, 1F);
                    break;
                default:
                    throw new NotImplementedException();
            }

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramSubComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ImageIdFront
            Object imageIdFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdFrontProperty));
            if (imageIdFront != null) this.LoadProperty<Object>(ImageIdFrontProperty, imageIdFront);

            // ImageIdBack
            Object imageIdBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdBackProperty));
            if (imageIdBack != null) this.LoadProperty<Object>(ImageIdBackProperty, imageIdBack);

            // ImageIdTop
            Object imageIdTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdTopProperty));
            if (imageIdTop != null) this.LoadProperty<Object>(ImageIdTopProperty, imageIdTop);

            // ImageIdBottom
            Object imageIdBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdBottomProperty));
            if (imageIdBottom != null) this.LoadProperty<Object>(ImageIdBottomProperty, imageIdBottom);

            // ImageIdLeft
            Object imageIdLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdLeftProperty));
            if (imageIdLeft != null) this.LoadProperty<Object>(ImageIdLeftProperty, imageIdLeft);

            // ImageIdRight
            Object imageIdRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(ImageIdRightProperty));
            if (imageIdRight != null) this.LoadProperty<Object>(ImageIdRightProperty, imageIdRight);
        }
        #endregion

        #region Planogram Stuff

        /// <summary>
        /// Returns the default sub component
        /// placement for this sub component
        /// </summary>
        public PlanogramSubComponentPlacement GetPlanogramSubComponentPlacement()
        {
            return PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(this);
        }

        /// <summary>
        /// Returns all sub component placements
        /// associated with this sub component
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            PlanogramComponent component = this.Parent;
            if (component == null) yield break;
            Planogram planogram = component.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.SubComponent))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        /// <summary>
        /// Indicates of a position of the specified product
        /// type can be added to this sub component
        /// </summary>
        public Boolean CanAddPosition(PlanogramProduct product)
        {
            return product.CanAddPosition(this);
        }

        /// <summary>
        /// Returns true if this subcomponent has pegholes.
        /// </summary>
        /// <returns></returns>
        public Boolean HasPegHoles()
        {
            Boolean hasRow1 = EnumeratePegholeYCoords(1).Any() && EnumeratePegholeXCoords(1).Any();
            //only need to check row 1 as if it doesnt exist row 2, wont be considered either.
            return hasRow1;
        }

        /// <summary>
        /// Returns the center points for all peg holes
        /// to be drawn for the sub component
        /// Use this with caution as it can return tens of thousands of holes.
        /// </summary>
        public PlanogramPegHoles GetPegHoles()
        {
            List<PointValue> row1 = new List<PointValue>();
            List<PointValue> row2 = new List<PointValue>();

            Boolean hasRow1 = (this.MerchConstraintRow1SpacingY > 0 && this.MerchConstraintRow1Height > 0 && this.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (this.MerchConstraintRow2SpacingY > 0 && this.MerchConstraintRow2Height > 0 && this.MerchConstraintRow2Width > 0);

            //correct the start values.
            Single row1StartX = this.MerchConstraintRow1StartX;
            Single row1StartY = this.MerchConstraintRow1StartY;
            if (hasRow1)
            {
                if ((row1StartX - (this.MerchConstraintRow1Width / 2.0)) < 0)
                {
                    row1StartX = this.MerchConstraintRow1StartX + this.MerchConstraintRow1SpacingX;
                }
                if ((this.Height - row1StartY + (this.MerchConstraintRow1Height / 2.0)) > this.Height)
                {
                    row1StartY = row1StartY + this.MerchConstraintRow1SpacingY;
                }
            }

            Single row2StartX = this.MerchConstraintRow2StartX;
            Single row2StartY = this.MerchConstraintRow2StartY;
            if (hasRow2)
            {
                if ((row2StartX - (this.MerchConstraintRow2Width / 2.0)) < 0)
                {
                    row2StartX = this.MerchConstraintRow2StartX + this.MerchConstraintRow2SpacingX;
                }
                if ((this.Height - row2StartY + (this.MerchConstraintRow2Height / 2.0)) > this.Height)
                {
                    row2StartY = row2StartY + this.MerchConstraintRow2SpacingY;
                }
            }


            if (hasRow1)
            {
                Single minX = 0;
                Single maxX = this.Width;
                Single minY = 0;
                Single maxY = this.Height;

                for (Int32 i = 0; i < 2; i++)
                {
                    if ((i == 0 && hasRow1) || (i == 1 && hasRow2))
                    {
                        List<PointValue> points;
                        Single startX;
                        Single startY;
                        Single spacingX;
                        Single spacingY;
                        Single holeWidth;
                        Single holeHeight;
                        Boolean isSlots;
                        if (i == 0)
                        {
                            points = row1;
                            startX = row1StartX;
                            startY = row1StartY;
                            spacingX = this.MerchConstraintRow1SpacingX;
                            spacingY = this.MerchConstraintRow1SpacingY;
                            holeWidth = this.MerchConstraintRow1Width;
                            holeHeight = this.MerchConstraintRow1Height;
                        }
                        else
                        {
                            points = row2;
                            startX = row2StartX;
                            startY = row2StartY;
                            spacingX = this.MerchConstraintRow2SpacingX;
                            spacingY = this.MerchConstraintRow2SpacingY;
                            holeWidth = this.MerchConstraintRow2Width;
                            holeHeight = this.MerchConstraintRow2Height;
                        }

                        isSlots = spacingX == 0;

                        //draw down.
                        Single curY = maxY - startY;
                        while ((curY - (holeHeight / 2.0)) > minY)
                        {
                            Single curX = minX + startX;
                            if (!isSlots)
                            {
                                while ((curX + (holeWidth / 2.0)) < maxX)
                                {
                                    points.Add(new PointValue(curX, curY, this.Depth));
                                    curX += spacingX;
                                }
                            }
                            else
                            {
                                points.Add(new PointValue(curX + ((this.Width - startX) / 2.0f), curY, this.Depth));
                            }

                            curY -= spacingY;
                        }

                    }

                }
            }

            return new PlanogramPegHoles(row1, row2, this);
        }


        ///// <summary>
        ///// Returns the coordinates of the nearest peghole to the given x y values
        ///// </summary>
        //public PointValue? GetNearestPeghole(Single x, Single y, PlanogramPegHoles pegholes)
        //{
        //    //return null if this subcomponent has no pegholes.
        //    if (!pegholes.All.Any()) return null;

        //    PointValue p1 = new PointValue(x, y, this.Depth);
        //    PointValue? p2 = null;

        //    Single curDist = Single.PositiveInfinity;

        //    foreach (PointValue p in pegholes.All)
        //    {
        //        Single distanceTo = (p - p1).Length;
        //        if (distanceTo < curDist)
        //        {
        //            p2 = p;
        //            curDist = distanceTo;
        //        }
        //    }

        //    return p2;
        //}

        /// <summary>
        /// Returns the coordinates of the nearest peghole to the given x y values
        /// </summary>
        public PointValue? GetNearestPeghole(Single x, Single y)
        {
            PointValue? row1peg = GetNearestPeghole(x, y, 1);
            if (!row1peg.HasValue) return null;

            PointValue? row2peg = GetNearestPeghole(x, y, 2);

            //no row2 peg so return row 1.
            if (!row2peg.HasValue) return row1peg;
            

            //we have row 2 so compare
            PointValue p = new PointValue(x, y, this.Depth);
            if ((p - row2peg.Value).Length < (p - row1peg.Value).Length)
            {
                return row2peg;
            }
            else
            {
                return row1peg;
            }
        }

        /// <summary>
        /// Returns the coordinates of the nearest peghole to the given x y values
        /// on the given row.
        /// </summary>
        private PointValue? GetNearestPeghole(Single x, Single y, Int32 rowTypeNum)
        {
            Single spacingY = (rowTypeNum == 1) ? this.MerchConstraintRow1SpacingY : this.MerchConstraintRow2SpacingY;
            Single spacingX = (rowTypeNum == 1) ? this.MerchConstraintRow1SpacingX : this.MerchConstraintRow2SpacingX;

            Single? rowY = null;
            foreach (Single pegY in EnumeratePegholeYCoords(rowTypeNum))
            {
                if ((rowY == null && MathHelper.GreaterThan(y, pegY))
                    || MathHelper.EqualTo(y, pegY))
                {
                    rowY = pegY;
                    break;
                }

                rowY = pegY;

                if (MathHelper.GreaterThan(y, rowY.Value)
                    && MathHelper.LessThan(y - rowY.Value, (spacingY / 2F)))
                    break;

                if (MathHelper.GreaterThan(rowY.Value, y)
                    && MathHelper.LessOrEqualThan(rowY.Value - y, (spacingY / 2F)))
                    break;
            }
            if (!rowY.HasValue) return null;


            //now find the nearest X
            Single? rowX = null;
            foreach (Single pegX in EnumeratePegholeXCoords(rowTypeNum))
            {
                if ((rowX == null && MathHelper.LessThan(x, pegX))
                    || MathHelper.EqualTo(pegX, x))
                {
                    rowX = pegX;
                    break;
                }

                rowX = pegX;

                if (MathHelper.GreaterThan(rowX.Value, x)
                    && MathHelper.LessThan(rowX.Value - x, (spacingX / 2F)))
                    break;

                if (MathHelper.GreaterThan(x, rowX.Value)
                    && MathHelper.LessOrEqualThan(x - rowX.Value, (spacingX / 2F)))
                    break;
            }
            if (!rowX.HasValue) return null;


            return new PointValue(rowX.Value, rowY.Value, this.Depth);
        }

        /// <summary>
        /// Enumerates through peghole y coords on both row types for this subcomponent.
        /// </summary>
        public IEnumerable<Single> EnumeratePegholeYCoords()
        {
            foreach (Single y in EnumeratePegholeYCoords(1))
                yield return y;

            foreach (Single y in EnumeratePegholeYCoords(2))
                yield return y;
        }

        /// <summary>
        /// Enumerates through peghole y coords on the given row type.
        /// </summary>
        public IEnumerable<Single> EnumeratePegholeYCoords(Int32 rowTypeNum)
        {
            Boolean hasRow1 = (this.MerchConstraintRow1SpacingY > 0 && this.MerchConstraintRow1Height > 0 && this.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (this.MerchConstraintRow2SpacingY > 0 && this.MerchConstraintRow2Height > 0 && this.MerchConstraintRow2Width > 0);

            Single startY, spacingY, holeHeight;
            if (rowTypeNum == 1)
            {
                if (!hasRow1) yield break;

                startY = this.MerchConstraintRow1StartY;
                spacingY = this.MerchConstraintRow1SpacingY;
                holeHeight = this.MerchConstraintRow1Height;
            }
            else if (rowTypeNum == 2)
            {
                if (!hasRow1 || !hasRow2) yield break;

                startY = this.MerchConstraintRow2StartY;
                spacingY = this.MerchConstraintRow2SpacingY;
                holeHeight = this.MerchConstraintRow2Height;
            }
            else yield break;

            //correct start
            if ((this.Height - startY + (holeHeight / 2.0)) > this.Height)
            {
                startY = startY + spacingY;
            }

            Single minY = 0;
            Single maxY = this.Height;

            //draw down.
            Single curY = maxY - startY;
            while ((curY - (holeHeight / 2.0)) > minY)
            {
                yield return curY;
                curY -= spacingY;
            }

        }

        /// <summary>
        /// Enumerates through peghole x coords on both row types for this subcomponent.
        /// </summary>
        public IEnumerable<Single> EnumeratePegholeXCoords()
        {
            foreach (Single x in EnumeratePegholeXCoords(1))
                yield return x;

            foreach (Single x in EnumeratePegholeXCoords(2))
                yield return x;
        }

        /// <summary>
        /// Enumerates through peghole x coords on the given row type.
        /// </summary>
        public IEnumerable<Single> EnumeratePegholeXCoords(Int32 rowTypeNum)
        {
            Boolean hasRow1 = (this.MerchConstraintRow1SpacingY > 0 && this.MerchConstraintRow1Height > 0 && this.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (this.MerchConstraintRow2SpacingY > 0 && this.MerchConstraintRow2Height > 0 && this.MerchConstraintRow2Width > 0);

            Single startX, spacingX, holeWidth;
            if (rowTypeNum == 1)
            {
                if (!hasRow1) yield break;

                startX = this.MerchConstraintRow1StartX;
                spacingX = this.MerchConstraintRow1SpacingX;
                holeWidth = this.MerchConstraintRow1Width;
            }
            else if (rowTypeNum == 2)
            {
                if (!hasRow1 || !hasRow2) yield break;

                startX = this.MerchConstraintRow2StartX;
                spacingX = this.MerchConstraintRow2SpacingX;
                holeWidth = this.MerchConstraintRow2Width;
            }
            else yield break;

            Single minX = 0;
            Single maxX = this.Width;
            Boolean isSlots = spacingX == 0;

            Single curX = minX + startX;
            if (!isSlots)
            {
                while ((curX + (holeWidth / 2.0)) < maxX)
                {
                    yield return curX;
                    curX += spacingX;
                }
            }
            else
            {
                yield return curX + ((this.Width - startX) / 2.0f);
            }
        }


        /// <summary>
        /// Returns true if this subcomponent has notches.
        /// </summary>
        public Boolean HasNotches()
        {
            //Must have a notch flag set.
            if (!(this.IsNotchPlacedOnFront || this.IsNotchPlacedOnBack
                || this.IsNotchPlacedOnLeft || this.IsNotchPlacedOnRight))
                return false;

            //Must have notch dimensions set.
            if (this.NotchWidth == 0 || this.NotchHeight == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Returns the notch number for the given relative y
        /// </summary>
        /// <param name="relativeY"></param>
        /// <returns></returns>
        public Int32? GetNearestNotchNumber(Double relativeY)
        {
            Int32? notch = null;

            if (HasNotches())
            {
                Double notch0 = this.NotchStartY - this.NotchSpacingY;
                notch = (Int32)Math.Round(((relativeY - notch0) / this.NotchSpacingY), 0);

                //ensure top notch is not above top of fixture
                notch = Math.Min((Int32)GetMaximumNotchNumberForFixture(), (Int32)notch);
            }

            return notch;
        }

        /// <summary>
        /// Returns the largest valid notch number for a fixture height. 
        /// Assumes top of top notch is 0.01 below top of fixture (may need amending)
        /// </summary>
        /// <returns>The Top Notch Number</returns>
        public Int32? GetMaximumNotchNumberForFixture()
        {
            Int32? val = null;

            if (HasNotches())
            {
                Single notch0 = this.NotchStartY - this.NotchSpacingY;

                val = (Int32)Math.Floor(((this.Height - 0.01) - notch0) / this.NotchSpacingY);
            }

            return val;
        }

        /// <summary>
        /// Returns the realative y position of the given notch number
        /// </summary>
        /// <param name="notchNumber"></param>
        /// <returns></returns>
        public Single? GetNotchLocalYPosition(Int32 notchNumber)
        {
            Single? val = null;

            if (HasNotches())
            {
                Single notch0 = this.NotchStartY - this.NotchSpacingY;

                val = notch0 + (notchNumber * this.NotchSpacingY);
            }

            return val;
        }

        /// <summary>
        /// Returns true if we need to consider
        /// the face thickness values for this subcomponent.
        /// </summary>
        /// <returns></returns>
        public Boolean HasFaceThickness()
        {
            if (FaceThicknessFront == 0
                && FaceThicknessBack == 0
                && FaceThicknessTop == 0
                && FaceThicknessBottom == 0
                && FaceThicknessLeft == 0
                && FaceThicknessRight == 0)
            {
                //all values are 0
                return false;
            }
            else if (FaceThicknessFront > 0
                && FaceThicknessBack > 0
                && FaceThicknessTop > 0
                && FaceThicknessBottom > 0
                && FaceThicknessLeft > 0
                && FaceThicknessRight > 0)
            {
                //all values are over 0
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns true if this subcomponant can be combined with others.
        /// </summary>
        public Boolean IsCombinable()
        {
            if (this.CombineType == PlanogramSubComponentCombineType.None) return false;
            if (this.MerchandisingType == PlanogramSubComponentMerchandisingType.None) return false;
            if (this.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Manual) return false;
            if (this.HasFaceThickness()) return false;

            return true;
        }

        /// <summary>
        /// Returns true if the subcomponents may be combined
        /// Note - this does not check that they are adjacent, only that properties are suitable.
        /// </summary>
        internal Boolean CanCombineWith(PlanogramSubComponent sub2)
        {
            // must not be the same sub.
            if (this == sub2) { return false; }

            //must both be combinable
            if (!this.IsCombinable()) return false;
            if (!sub2.IsCombinable()) return false;

            //must have the same merch type
            if (this.MerchandisingType != sub2.MerchandisingType) return false;

            //must have the same height
            if (!this.Height.EqualTo(sub2.Height)) { return false; }

            //must have the same depth
            if (!this.Depth.EqualTo(sub2.Depth)) { return false; }

            //NB -Shelves may be combined even if their merch strategies are different.
            // The first shelf in the row then determines the strategies to use.

            return true;
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(PlanogramSubComponent);
            String typeFriendly = PlanogramSubComponent.FriendlyName;

            String detailsGroup = Message.PlanogramSubComponent_PropertyGroup_Details;
            String merchGroup = Message.PlanogramSubComponent_PropertyGroup_Merchandising;
            String dividerGroup = Message.PlanogramSubComponent_PropertyGroup_Dividers;
            String pegGroup = Message.PlanogramSubComponent_PropertyGroup_Pegboard;
            String riserGroup = Message.PlanogramSubComponent_PropertyGroup_Riser;
            String notchGroup = Message.PlanogramSubComponent_PropertyGroup_Notches;
            String stylingGroup = Message.PlanogramSubComponent_PropertyGroup_Styling;

            //General
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AngleProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SlopeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RollProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessBackProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessBottomProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessFrontProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessLeftProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessRightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessTopProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ShapeTypeProperty, detailsGroup);

            //Merchandising
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FrontOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BackOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LeftOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RightOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TopOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BottomOverhangProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CombineTypeProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisableHeightProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisableDepthProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStrategyXProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStrategyYProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStrategyZProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingTypeProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsProductOverlapAllowedProperty, merchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsProductSqueezeAllowedProperty, merchGroup);

            //Divider
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionDepthProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionHeightProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionSpacingXProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionSpacingYProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionSpacingZProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionStartXProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionStartYProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionStartZProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionWidthProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsDividerObstructionAtEndProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsDividerObstructionAtStartProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsDividerObstructionByFacingProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionFillColourProperty, dividerGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DividerObstructionFillPatternProperty, dividerGroup);

            //Peg
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1HeightProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1SpacingXProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1SpacingYProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1StartXProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1StartYProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow1WidthProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2HeightProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2SpacingXProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2SpacingYProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2StartXProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2StartYProperty, pegGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchConstraintRow2WidthProperty, pegGroup);


            //Riser
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRiserPlacedOnBackProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRiserPlacedOnFrontProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRiserPlacedOnLeftProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRiserPlacedOnRightProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RiserColourProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RiserHeightProperty, riserGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RiserThicknessProperty, riserGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RiserTransparencyPercentProperty, riserGroup);//[V8-29594] Not currently in use:

            //Notches
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsNotchPlacedOnBackProperty, notchGroup); //not in use
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsNotchPlacedOnFrontProperty, notchGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsNotchPlacedOnLeftProperty, notchGroup); //not in use
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsNotchPlacedOnRightProperty, notchGroup); //not in use
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchHeightProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchSpacingXProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchSpacingYProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchStartXProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchStartYProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchStyleTypeProperty, notchGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchWidthProperty, notchGroup);

            //Styling
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourBackProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourBottomProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourFrontProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourLeftProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourRightProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourTopProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeBackProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeBottomProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeFrontProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeLeftProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeRightProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeTopProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LineColourProperty, stylingGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LineThicknessProperty, stylingGroup);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentBackProperty, stylingGroup);//[V8-29594] Not currently in use:
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentBottomProperty, stylingGroup);//[V8-29594] Not currently in use:
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentFrontProperty, stylingGroup);//[V8-29594] Not currently in use:
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentLeftProperty, stylingGroup);//[V8-29594] Not currently in use:
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentRightProperty, stylingGroup);//[V8-29594] Not currently in use:
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TransparencyPercentTopProperty, stylingGroup);//[V8-29594] Not currently in use:

        }

        /// <summary>
        /// Returns the value of the given field for this position.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            return field.GetValue(this);
        }

        /// <summary>
        /// Enumerates through all associated image ids.
        /// </summary>
        public IEnumerable<Object> EnumerateImageIds()
        {
            if (ImageIdFront != null) yield return ImageIdFront;
            if (ImageIdLeft != null) yield return ImageIdLeft;
            if (ImageIdTop != null) yield return ImageIdTop;
            if (ImageIdBack != null) yield return ImageIdBack;
            if (ImageIdRight != null) yield return ImageIdRight;
            if (ImageIdBottom != null) yield return ImageIdBottom;
        }

        /// <summary>
        /// Clears all images references.
        /// </summary>
        public void ClearImages()
        {
            this.ImageIdFront = null;
            this.ImageIdLeft = null;
            this.ImageIdTop = null;
            this.ImageIdBack = null;
            this.ImageIdRight = null;
            this.ImageIdBottom = null;
        }

        /// <summary>
        /// Returns a WidthHeightDepthValue representing the un-rotated size of this subcomponent.
        /// </summary>
        /// <returns></returns>
        public WidthHeightDepthValue GetWidthHeightDepth()
        {
            return new WidthHeightDepthValue(Width, Height, Depth);
        }

        #endregion

        #region Position Options
        /// <summary>
        /// Returns the default axis indicating
        /// the direction that a position should
        /// increase or decrease its facings
        /// </summary>
        public AxisType GetFacingAxis()
        {
            switch (this.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return AxisType.Z;
                case PlanogramSubComponentMerchandisingType.Stack:
                    if (this.FaceThicknessFront == 0)
                    {
                        return AxisType.X;
                    }
                    else
                    {
                        return AxisType.Z;
                    }
                default:
                    return AxisType.X;
            }
        }
        #endregion

        #endregion
    }
}