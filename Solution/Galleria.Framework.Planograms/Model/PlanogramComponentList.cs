﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-27496 : L.Luong
//  Added ClearImages method
#endregion
#region Version History: CCM830
// CCM-32563 : L.Ineson
//  Added RemoveComponentAndDependants
// V8-32795 : L.Bailey
//  Dragging bays between plans mixes up products
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of components contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramComponentList : ModelList<PlanogramComponentList, PlanogramComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramComponentList NewPlanogramComponentList()
        {
            PlanogramComponentList item = new PlanogramComponentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new component to this list
        /// </summary>
        public PlanogramComponent Add(PlanogramComponentType componentType,
            Single width, Single height, Single depth, Int32 fillColour = -16777216)
        {
            // get the parent planogram
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            
            // get the new component number
            Int32 componentNo = planogram.Components.Where(c => c.ComponentType == componentType).Count() + 1;

            // create the component
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent(
                String.Format("{0} {1}", PlanogramComponentTypeHelper.FriendlyNames[componentType], componentNo),
                componentType,
                width,
                height,
                depth,
                fillColour);

            // add the component to this list
            this.Add(component);

            // return the component
            return component;
        }

        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramComponent Add(PlanogramComponentType componentType, IPlanogramSettings settings)
        {
            // get the parent planogram
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            
            // get the new component number
            Int32 componentNo = planogram.Components.Where(c => c.ComponentType == componentType).Count() + 1;

            // create the component
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent(
                String.Format("{0} {1}", PlanogramComponentTypeHelper.FriendlyNames[componentType], componentNo),
                componentType,
                settings);

            // add the component to this list
            this.Add(component);

            // return the component
            return component;
        }

        public void ClearImages()
        {
            foreach (PlanogramComponent component in this) 
            {
                foreach (PlanogramSubComponent subComponent in component.SubComponents) 
                {
                    subComponent.ImageIdBack = null;
                    subComponent.ImageIdBottom = null;
                    subComponent.ImageIdFront = null;
                    subComponent.ImageIdLeft = null;
                    subComponent.ImageIdRight = null;
                    subComponent.ImageIdTop = null;
                }
                component.ImageIdBack = null;
                component.ImageIdBottom = null;
                component.ImageIdFront = null;
                component.ImageIdLeft = null;
                component.ImageIdRight = null;
                component.ImageIdTop = null;
            }
        }

        /// <summary>
        /// Removes the given component and all of its dependants from 
        /// the parent planogram.
        /// </summary>
        /// <param name="component">The component to remove</param>
        public void RemoveComponentAndDependants(PlanogramComponent component)
        {
            //if we have no parent then just return.
            if (this.Parent == null)
            {
                Remove(component);
                return;
            }

           
            //remove all fixture components referencing this.
            List<Object> removedFixtureComponentIds = new List<Object>();
            foreach (PlanogramFixture fixture in this.Parent.Fixtures)
            {
                foreach (PlanogramFixtureComponent fc in fixture.Components.ToArray())
                {
                    if (Object.Equals(fc.PlanogramComponentId, component.Id))
                    {
                        removedFixtureComponentIds.Add(fc.Id);
                        fixture.Components.Remove(fc);
                    }
                }
            }

            //remove all assembly components referencing this.
            List<Object> removedAssemblyComponentIds = new List<Object>();
            foreach (PlanogramAssembly assembly in this.Parent.Assemblies)
            {
                foreach (PlanogramAssemblyComponent ac in assembly.Components.ToArray())
                {
                    if (Object.Equals(ac.PlanogramComponentId, component.Id))
                    {
                        removedAssemblyComponentIds.Add(ac.Id);
                        assembly.Components.Remove(ac);
                    }
                }
            }

            List<Object> subComponentIds = component.SubComponents.Select(s => s.Id).ToList();

            List<PlanogramPosition> positions =
                this.Parent.Positions.Where(p =>
                    (p.PlanogramAssemblyComponentId != null && removedAssemblyComponentIds.Contains(p.PlanogramAssemblyComponentId))
                   || (p.PlanogramFixtureComponentId != null && removedFixtureComponentIds.Contains(p.PlanogramFixtureComponentId))
                   || (p.PlanogramSubComponentId != null && subComponentIds.Contains(p.PlanogramSubComponentId)))
                   .ToList();


            List<PlanogramAnnotation> anotations =
                this.Parent.Annotations.Where(a => 
                    (a.PlanogramAssemblyComponentId != null && removedAssemblyComponentIds.Contains(a.PlanogramAssemblyComponentId))
                   || (a.PlanogramFixtureComponentId != null && removedFixtureComponentIds.Contains(a.PlanogramFixtureComponentId))
                   || (a.PlanogramSubComponentId != null && subComponentIds.Contains(a.PlanogramSubComponentId))
                   || (a.PlanogramPositionId != null && positions.Select(p=> p.Id).Contains(a.PlanogramPositionId)))
                   .ToList();


            //remove all positions
            foreach (var pos in positions)
            {
                this.Parent.Positions.Remove(pos);
            }

            //remove all linked annotations
            this.Parent.Annotations.RemoveList(anotations);

            //finally remove this component.
            Remove(component);
        }

        #endregion

    }
}
