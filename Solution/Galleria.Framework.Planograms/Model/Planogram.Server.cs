﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Extended for MetaData properties
// V8-26709 : L.Ineson
//  Removed old unit of measure property.
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Adde UserId (owner) property
// V8-27783 : M.Brumby
//	Added Planogram.UniqueContentReference
// V8-28147 : D.Pleasance
//  Amended to ensure UserName set upon loading of object

#endregion
#region Version History: CCM801

// V8-28722 : N.Foster
//  Added support for batch dal operations

#endregion
#region Version History: CCM802

// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName Properties
// V8-28996 : A.Silva
//      Added BatchContext Registration for PlanogramSequence, PlanogramSequenceGroup and PlanogramSequenceGroupProduct.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single

#endregion
#region Version History: CCM803

// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore

#endregion
#region Version History: CCM810

// V8-29844 : L.Ineson
//  Added more metadata
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents

#endregion
#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.
// V8-30504 : D.Pleasance
//  Amended GetDataTransferObject() to ensure UserName is applied.

#endregion
#region Version History : CCM820
// V8-30728 : A.Kuszyk
//  Added HighlightSequenceStrategy property.
// V8-30705 : A.Probyn
//  Added new assortment rules related meta data properties
// V8-30956 : J.Pikup
//  Insert and Update batch context registrations set to pass in true (individual insert) for CDT Level. 
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion
#region Version History : CCM830
// V8-31550 : A.Probyn
//  Registered new ProductBuddies & LocationBuddies dtos in batch context
// V8-31551 : A.Probyn
//  Registered new InventoryRules dtos in batch context
// V8-32504 : A.Kuszyk
//  Added PlanogramSequenceGroupSubGroup to batch save context.
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public sealed partial class Planogram
    {
        #region Constructors
        private Planogram() { } //force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a planogram from a dto
        /// </summary>
        internal static Planogram Fetch(IDalContext dalContext, PlanogramDto dto)
        {
            return DataPortal.FetchChild<Planogram>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);

            //Should only be an issue in automated NUnit tests otherwise UniqueContentReference should 
            //at least have an empty Guid converted to string
            if(dto.UniqueContentReference != null) this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.Parse(dto.UniqueContentReference));

            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<PlanogramStatusType>(StatusProperty, (PlanogramStatusType) dto.Status);
            this.LoadProperty<String>(UserNameProperty, String.IsNullOrEmpty(dto.UserName) ? ApplicationContext.User.Identity.Name : dto.UserName);
            this.LoadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty, (PlanogramLengthUnitOfMeasureType)dto.LengthUnitsOfMeasure);
            this.LoadProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty, (PlanogramAreaUnitOfMeasureType)dto.AreaUnitsOfMeasure);
            this.LoadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty, (PlanogramVolumeUnitOfMeasureType)dto.VolumeUnitsOfMeasure);
            this.LoadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty, (PlanogramWeightUnitOfMeasureType)dto.WeightUnitsOfMeasure);
            this.LoadProperty<Byte>(AngleUnitsOfMeasureProperty, dto.AngleUnitsOfMeasure);
            this.LoadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty, (PlanogramCurrencyUnitOfMeasureType)dto.CurrencyUnitsOfMeasure);
            this.LoadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, (PlanogramProductPlacementXType)dto.ProductPlacementX);
            this.LoadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, (PlanogramProductPlacementYType)dto.ProductPlacementY);
            this.LoadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, (PlanogramProductPlacementZType)dto.ProductPlacementZ);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<String>(CategoryCodeProperty, dto.CategoryCode);
            this.LoadProperty<String>(CategoryNameProperty, dto.CategoryName);
            this.LoadProperty<String>(SourcePathProperty, dto.SourcePath);
            this.LoadProperty<Int32?>(MetaBayCountProperty, dto.MetaBayCount);
            this.LoadProperty<Int32?>(MetaUniqueProductCountProperty, dto.MetaUniqueProductCount);
            this.LoadProperty<Int32?>(MetaComponentCountProperty, dto.MetaComponentCount);
            this.LoadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, dto.MetaTotalMerchandisableLinearSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, dto.MetaTotalMerchandisableAreaSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, dto.MetaTotalMerchandisableVolumetricSpace);
            this.LoadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, dto.MetaTotalLinearWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, dto.MetaTotalAreaWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, dto.MetaTotalVolumetricWhiteSpace);
            this.LoadProperty<Int32?>(MetaProductsPlacedProperty, dto.MetaProductsPlaced);
            this.LoadProperty<Int32?>(MetaProductsUnplacedProperty, dto.MetaProductsUnplaced);
            this.LoadProperty<Int32?>(MetaNewProductsProperty, dto.MetaNewProducts);
            this.LoadProperty<Int32?>(MetaChangesFromPreviousCountProperty, dto.MetaChangesFromPreviousCount);
            this.LoadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, dto.MetaChangeFromPreviousStarRating);
            this.LoadProperty<Int32?>(MetaBlocksDroppedProperty, dto.MetaBlocksDropped);
            this.LoadProperty<Int32?>(MetaNotAchievedInventoryProperty, dto.MetaNotAchievedInventory);
            this.LoadProperty<Int32?>(MetaTotalFacingsProperty, dto.MetaTotalFacings);
            this.LoadProperty<Single?>(MetaAverageFacingsProperty, dto.MetaAverageFacings);
            this.LoadProperty<Int32?>(MetaTotalUnitsProperty, dto.MetaTotalUnits);
            this.LoadProperty<Single?>(MetaAverageUnitsProperty, dto.MetaAverageUnits);
            this.LoadProperty<Single?>(MetaMinDosProperty, dto.MetaMinDos);
            this.LoadProperty<Single?>(MetaMaxDosProperty, dto.MetaMaxDos);
            this.LoadProperty<Single?>(MetaAverageDosProperty, dto.MetaAverageDos);
            this.LoadProperty<Single?>(MetaMinCasesProperty, dto.MetaMinCases);
            this.LoadProperty<Single?>(MetaAverageCasesProperty, dto.MetaAverageCases);
            this.LoadProperty<Single?>(MetaMaxCasesProperty, dto.MetaMaxCases);
            this.LoadProperty<Int32?>(MetaTotalComponentCollisionsProperty, dto.MetaTotalComponentCollisions);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty, dto.MetaTotalComponentsOverMerchandisedDepth);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty, dto.MetaTotalComponentsOverMerchandisedHeight);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty, dto.MetaTotalComponentsOverMerchandisedWidth);
            this.LoadProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty, dto.MetaHasComponentsOutsideOfFixtureArea);
            this.LoadProperty<Int32?>(MetaTotalPositionCollisionsProperty, dto.MetaTotalPositionCollisions);
            this.LoadProperty<Int16?>(MetaTotalFrontFacingsProperty, dto.MetaTotalFrontFacings);
            this.LoadProperty<Single?>(MetaAverageFrontFacingsProperty, dto.MetaAverageFrontFacings);
            this.LoadProperty<String>(LocationNameProperty, dto.LocationName);
            this.LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            this.LoadProperty<String>(ClusterSchemeNameProperty, dto.ClusterSchemeName);
            this.LoadProperty<String>(ClusterNameProperty, dto.ClusterName);
            this.LoadProperty<Int32?>(MetaNoOfErrorsProperty, dto.MetaNoOfErrors);
            this.LoadProperty<Int32?>(MetaNoOfWarningsProperty, dto.MetaNoOfWarnings);
            this.LoadProperty<Int32?>(MetaNoOfInformationEventsProperty, dto.MetaNoOfInformationEvents);
            this.LoadProperty<Int32?>(MetaNoOfDebugEventsProperty, dto.MetaNoOfDebugEvents);
            this.LoadProperty<Int32?>(MetaTotalErrorScoreProperty, dto.MetaTotalErrorScore);
            this.LoadProperty<Byte?>(MetaHighestErrorScoreProperty, dto.MetaHighestErrorScore);
            this.LoadProperty<DateTime?>(DateWipProperty, dto.DateWip);
            this.LoadProperty<DateTime?>(DateApprovedProperty, dto.DateApproved);
            this.LoadProperty<DateTime?>(DateArchivedProperty, dto.DateArchived);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty, dto.MetaCountOfProductNotAchievedCases);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty, dto.MetaCountOfProductNotAchievedDOS);
            this.LoadProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty, dto.MetaCountOfProductOverShelfLifePercent);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty, dto.MetaCountOfProductNotAchievedDeliveries);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty, dto.MetaPercentOfProductNotAchievedCases);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty, dto.MetaPercentOfProductNotAchievedDOS);
            this.LoadProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty, dto.MetaPercentOfProductOverShelfLifePercent);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty, dto.MetaPercentOfProductNotAchievedDeliveries);
            this.LoadProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty, dto.MetaCountOfPositionsOutsideOfBlockSpace);
            this.LoadProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty, dto.MetaPercentOfPositionsOutsideOfBlockSpace);
            this.LoadProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty, dto.MetaPercentOfPlacedProductsRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty, dto.MetaCountOfPlacedProductsRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty, dto.MetaCountOfPlacedProductsNotRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty, dto.MetaCountOfRecommendedProductsInAssortment);
            this.LoadProperty<PlanogramType>(PlanogramTypeProperty, (PlanogramType) dto.PlanogramType);
            this.LoadProperty<String>(HighlightSequenceStrategyProperty, dto.HighlightSequenceStrategy);
            this.LoadProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty, dto.MetaNumberOfAssortmentRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty, dto.MetaNumberOfProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty, dto.MetaNumberOfFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty, dto.MetaNumberOfInheritanceRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty, dto.MetaNumberOfLocalProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty, dto.MetaNumberOfDistributionRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty, dto.MetaNumberOfCoreRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty, dto.MetaPercentageOfAssortmentRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty, dto.MetaPercentageOfProductRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty, dto.MetaPercentageOfFamilyRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty, dto.MetaPercentageOfInheritanceRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty, dto.MetaPercentageOfLocalProductRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty, dto.MetaPercentageOfDistributionRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty, dto.MetaPercentageOfCoreRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty, dto.MetaNumberOfDelistProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty, dto.MetaNumberOfForceProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty, dto.MetaNumberOfPreserveProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty, dto.MetaNumberOfMinimumHurdleProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty, dto.MetaNumberOfMaximumProductFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty, dto.MetaNumberOfMinimumProductFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty, dto.MetaNumberOfDependencyFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty, dto.MetaNumberOfDelistFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaCountOfProductsBuddiedProperty, dto.MetaCountOfProductsBuddied);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramDto GetDataTransferObject(PackageDto parent)
        {
            return new PlanogramDto
            {
                Id = this.ReadProperty<Object>(IdProperty),
                UniqueContentReference = this.ReadProperty<Guid>(UniqueContentReferenceProperty).ToString(),
                PackageId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                UserName = String.IsNullOrEmpty(this.ReadProperty<String>(UserNameProperty)) ? ApplicationContext.User.Identity.Name : this.ReadProperty<String>(UserNameProperty),
                LengthUnitsOfMeasure = (Byte)this.ReadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty),
                AreaUnitsOfMeasure = (Byte)this.ReadProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty),
                VolumeUnitsOfMeasure = (Byte)this.ReadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty),
                WeightUnitsOfMeasure = (Byte)this.ReadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty),
                AngleUnitsOfMeasure = this.ReadProperty<Byte>(AngleUnitsOfMeasureProperty),
                CurrencyUnitsOfMeasure = (Byte)this.ReadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty),
                ProductPlacementX = (Byte)this.ReadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty),
                ProductPlacementY = (Byte)this.ReadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty),
                ProductPlacementZ = (Byte)this.ReadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                CategoryCode = this.ReadProperty<String>(CategoryCodeProperty),
                CategoryName = this.ReadProperty<String>(CategoryNameProperty),
                SourcePath = this.ReadProperty<String>(SourcePathProperty),
                MetaBayCount = this.ReadProperty<Int32?>(MetaBayCountProperty),
                MetaUniqueProductCount = this.ReadProperty<Int32?>(MetaUniqueProductCountProperty),
                MetaComponentCount = this.ReadProperty<Int32?>(MetaComponentCountProperty),
                MetaTotalMerchandisableLinearSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty),
                MetaTotalMerchandisableAreaSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty),
                MetaTotalMerchandisableVolumetricSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty),
                MetaTotalLinearWhiteSpace = this.ReadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty),
                MetaTotalAreaWhiteSpace = this.ReadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty),
                MetaTotalVolumetricWhiteSpace = this.ReadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty),
                MetaProductsPlaced = this.ReadProperty<Int32?>(MetaProductsPlacedProperty),
                MetaProductsUnplaced = this.ReadProperty<Int32?>(MetaProductsUnplacedProperty),
                MetaNewProducts = this.ReadProperty<Int32?>(MetaNewProductsProperty),
                MetaChangesFromPreviousCount = this.ReadProperty<Int32?>(MetaChangesFromPreviousCountProperty),
                MetaChangeFromPreviousStarRating = this.ReadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty),
                MetaBlocksDropped = this.ReadProperty<Int32?>(MetaBlocksDroppedProperty),
                MetaNotAchievedInventory = this.ReadProperty<Int32?>(MetaNotAchievedInventoryProperty),
                MetaTotalFacings = this.ReadProperty<Int32?>(MetaTotalFacingsProperty),
                MetaAverageFacings = this.ReadProperty<Single?>(MetaAverageFacingsProperty),
                MetaTotalUnits = this.ReadProperty<Int32?>(MetaTotalUnitsProperty),
                MetaAverageUnits = this.ReadProperty<Single?>(MetaAverageUnitsProperty),
                MetaMinDos = this.ReadProperty<Single?>(MetaMinDosProperty),
                MetaMaxDos = this.ReadProperty<Single?>(MetaMaxDosProperty),
                MetaAverageDos = this.ReadProperty<Single?>(MetaAverageDosProperty),
                MetaMinCases = this.ReadProperty<Single?>(MetaMinCasesProperty),
                MetaAverageCases = this.ReadProperty<Single?>(MetaAverageCasesProperty),
                MetaMaxCases = this.ReadProperty<Single?>(MetaMaxCasesProperty),
                MetaTotalComponentCollisions = this.ReadProperty<Int32?>(MetaTotalComponentCollisionsProperty),
                MetaTotalComponentsOverMerchandisedDepth = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty),
                MetaTotalComponentsOverMerchandisedHeight = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty),
                MetaTotalComponentsOverMerchandisedWidth = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty),
                MetaHasComponentsOutsideOfFixtureArea = this.ReadProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty),
                MetaTotalPositionCollisions = this.ReadProperty<Int32?>(MetaTotalPositionCollisionsProperty),
                MetaTotalFrontFacings = this.ReadProperty<Int16?>(MetaTotalFrontFacingsProperty),
                MetaAverageFrontFacings = this.ReadProperty<Single?>(MetaAverageFrontFacingsProperty),
                LocationCode = this.ReadProperty<String>(LocationCodeProperty),
                LocationName = this.ReadProperty<String>(LocationNameProperty),
                ClusterSchemeName = this.ReadProperty<String>(ClusterSchemeNameProperty),
                ClusterName = this.ReadProperty<String>(ClusterNameProperty),
                MetaNoOfErrors = this.ReadProperty<Int32?>(MetaNoOfErrorsProperty),
                MetaNoOfWarnings = this.ReadProperty<Int32?>(MetaNoOfWarningsProperty),
                MetaNoOfInformationEvents = this.ReadProperty<Int32?>(MetaNoOfInformationEventsProperty),
                MetaNoOfDebugEvents = this.ReadProperty<Int32?>(MetaNoOfDebugEventsProperty),
                MetaTotalErrorScore = this.ReadProperty<Int32?>(MetaTotalErrorScoreProperty),
                MetaHighestErrorScore = this.ReadProperty<Byte?>(MetaHighestErrorScoreProperty),
                Status = (Byte) this.ReadProperty<PlanogramStatusType>(StatusProperty),
                DateWip = this.ReadProperty<DateTime?>(DateWipProperty),
                DateApproved = this.ReadProperty<DateTime?>(DateApprovedProperty),
                DateArchived = this.ReadProperty<DateTime?>(DateArchivedProperty),
                MetaCountOfProductNotAchievedCases = this.ReadProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty),
                MetaCountOfProductNotAchievedDOS = this.ReadProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty),
                MetaCountOfProductOverShelfLifePercent = this.ReadProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty),
                MetaCountOfProductNotAchievedDeliveries = this.ReadProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty),
                MetaPercentOfProductNotAchievedCases = this.ReadProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty),
                MetaPercentOfProductNotAchievedDOS = this.ReadProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty),
                MetaPercentOfProductOverShelfLifePercent = this.ReadProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty),
                MetaPercentOfProductNotAchievedDeliveries = this.ReadProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty),
                MetaCountOfPositionsOutsideOfBlockSpace = this.ReadProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty),
                MetaPercentOfPositionsOutsideOfBlockSpace = this.ReadProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty),
                MetaPercentOfPlacedProductsRecommendedInAssortment = this.ReadProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty),
                MetaCountOfPlacedProductsRecommendedInAssortment = this.ReadProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty),
                MetaCountOfPlacedProductsNotRecommendedInAssortment = this.ReadProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty),
                MetaCountOfRecommendedProductsInAssortment = this.ReadProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty),
                PlanogramType = (Byte) this.ReadProperty<PlanogramType>(PlanogramTypeProperty),
                HighlightSequenceStrategy = this.ReadProperty<String>(HighlightSequenceStrategyProperty),
                MetaNumberOfAssortmentRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty),
                MetaNumberOfProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty),
                MetaNumberOfFamilyRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty),
                MetaNumberOfInheritanceRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty),
                MetaNumberOfLocalProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty),
                MetaNumberOfDistributionRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty),
                MetaNumberOfCoreRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty),
                MetaPercentageOfAssortmentRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty),
                MetaPercentageOfProductRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty),
                MetaPercentageOfFamilyRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty),
                MetaPercentageOfInheritanceRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty),
                MetaPercentageOfLocalProductRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty),
                MetaPercentageOfDistributionRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty),
                MetaPercentageOfCoreRulesBroken = this.ReadProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty),
                MetaNumberOfDelistProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty),
                MetaNumberOfForceProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty),
                MetaNumberOfPreserveProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty),
                MetaNumberOfMinimumHurdleProductRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty),
                MetaNumberOfMaximumProductFamilyRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty),
                MetaNumberOfMinimumProductFamilyRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty),
                MetaNumberOfDependencyFamilyRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty),
                MetaNumberOfDelistFamilyRulesBroken = this.ReadProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty),
                MetaCountOfProductsBuddied = this.ReadProperty<Int16?>(MetaCountOfProductsBuddiedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(IDalContext dalContext, PackageDto parent)
        {
            using (BatchSaveContext batchContext = new BatchSaveContext(dalContext))
            {
                // register batch handlers in the order they will be committed
                batchContext.Register<PlanogramImageDto, IPlanogramImageDal>();
                batchContext.Register<PlanogramComponentDto, IPlanogramComponentDal>();
                batchContext.Register<PlanogramAssemblyDto, IPlanogramAssemblyDal>();
                batchContext.Register<PlanogramAssemblyComponentDto, IPlanogramAssemblyComponentDal>();
                batchContext.Register<PlanogramFixtureDto, IPlanogramFixtureDal>();
                batchContext.Register<PlanogramFixtureItemDto, IPlanogramFixtureItemDal>();
                batchContext.Register<PlanogramFixtureAssemblyDto, IPlanogramFixtureAssemblyDal>();
                batchContext.Register<PlanogramFixtureComponentDto, IPlanogramFixtureComponentDal>();
                batchContext.Register<PlanogramSubComponentDto, IPlanogramSubComponentDal>();
                batchContext.Register<PlanogramProductDto, IPlanogramProductDal>();
                batchContext.Register<PlanogramPositionDto, IPlanogramPositionDal>();
                batchContext.Register<PlanogramAnnotationDto, IPlanogramAnnotationDal>();
                batchContext.Register<PlanogramAssortmentDto, IPlanogramAssortmentDal>();
                batchContext.Register<PlanogramAssortmentLocalProductDto, IPlanogramAssortmentLocalProductDal>();
                batchContext.Register<PlanogramAssortmentProductDto, IPlanogramAssortmentProductDal>();
                batchContext.Register<PlanogramAssortmentRegionDto, IPlanogramAssortmentRegionDal>();
                batchContext.Register<PlanogramAssortmentRegionLocationDto, IPlanogramAssortmentRegionLocationDal>();
                batchContext.Register<PlanogramAssortmentRegionProductDto, IPlanogramAssortmentRegionProductDal>();
                batchContext.Register<PlanogramAssortmentInventoryRuleDto, IPlanogramAssortmentInventoryRuleDal>();
                batchContext.Register<PlanogramAssortmentLocationBuddyDto, IPlanogramAssortmentLocationBuddyDal>();
                batchContext.Register<PlanogramAssortmentProductBuddyDto, IPlanogramAssortmentProductBuddyDal>();
                batchContext.Register<PlanogramBlockingDto, IPlanogramBlockingDal>();
                batchContext.Register<PlanogramBlockingGroupDto, IPlanogramBlockingGroupDal>();
                batchContext.Register<PlanogramBlockingDividerDto, IPlanogramBlockingDividerDal>();
                batchContext.Register<PlanogramBlockingLocationDto, IPlanogramBlockingLocationDal>();
                batchContext.Register<PlanogramConsumerDecisionTreeDto, IPlanogramConsumerDecisionTreeDal>();
                batchContext.Register<PlanogramConsumerDecisionTreeLevelDto, IPlanogramConsumerDecisionTreeLevelDal>(true);
                batchContext.Register<PlanogramConsumerDecisionTreeNodeDto, IPlanogramConsumerDecisionTreeNodeDal>(true);
                batchContext.Register<PlanogramConsumerDecisionTreeNodeProductDto, IPlanogramConsumerDecisionTreeNodeProductDal>();
                batchContext.Register<PlanogramEventLogDto, IPlanogramEventLogDal>();
                batchContext.Register<PlanogramInventoryDto, IPlanogramInventoryDal>();
                batchContext.Register<PlanogramMetadataImageDto, IPlanogramMetadataImageDal>();
                batchContext.Register<PlanogramPerformanceDto, IPlanogramPerformanceDal>();
                batchContext.Register<PlanogramPerformanceDataDto, IPlanogramPerformanceDataDal>();
                batchContext.Register<PlanogramPerformanceMetricDto, IPlanogramPerformanceMetricDal>();
                batchContext.Register<PlanogramRenumberingStrategyDto, IPlanogramRenumberingStrategyDal>();
                batchContext.Register<PlanogramValidationTemplateDto, IPlanogramValidationTemplateDal>();
                batchContext.Register<PlanogramValidationTemplateGroupDto, IPlanogramValidationTemplateGroupDal>();
                batchContext.Register<PlanogramValidationTemplateGroupMetricDto, IPlanogramValidationTemplateGroupMetricDal>();
                batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>();
                batchContext.Register<PlanogramSequenceDto, IPlanogramSequenceDal>();
                batchContext.Register<PlanogramSequenceGroupDto, IPlanogramSequenceGroupDal>();
                batchContext.Register<PlanogramSequenceGroupSubGroupDto, IPlanogramSequenceGroupSubGroupDal>();
                batchContext.Register<PlanogramSequenceGroupProductDto, IPlanogramSequenceGroupProductDal>();
                batchContext.Register<PlanogramComparisonDto, IPlanogramComparisonDal>();
                batchContext.Register<PlanogramComparisonFieldDto, IPlanogramComparisonFieldDal>();
                batchContext.Register<PlanogramComparisonResultDto, IPlanogramComparisonResultDal>();
                batchContext.Register<PlanogramComparisonItemDto, IPlanogramComparisonItemDal>();
                batchContext.Register<PlanogramComparisonFieldValueDto, IPlanogramComparisonFieldValueDal>();
                batchContext.Register<ProductAttributeComparisonResultDto, IProductAttributeComparisonResultDal>();

                // insert the planogram
                PlanogramDto dto = this.GetDataTransferObject(parent);
                Object oldId = dto.Id;
                using (IPlanogramDal dal = dalContext.GetDal<IPlanogramDal>())
                {
                    dal.Insert(parent, dto);
                }
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dalContext.RegisterId<Planogram>(oldId, dto.Id);

                // update the children
                FieldManager.UpdateChildren(batchContext, this);

                // commit the batch
                batchContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(IDalContext dalContext, PackageDto parent)
        {
            using (BatchSaveContext batchContext = new BatchSaveContext(dalContext))
            {
                // register batch handlers in the order they will be committed
                batchContext.Register<PlanogramImageDto, IPlanogramImageDal>();
                batchContext.Register<PlanogramComponentDto, IPlanogramComponentDal>();
                batchContext.Register<PlanogramAssemblyDto, IPlanogramAssemblyDal>();
                batchContext.Register<PlanogramAssemblyComponentDto, IPlanogramAssemblyComponentDal>();
                batchContext.Register<PlanogramFixtureDto, IPlanogramFixtureDal>();
                batchContext.Register<PlanogramFixtureItemDto, IPlanogramFixtureItemDal>();
                batchContext.Register<PlanogramFixtureAssemblyDto, IPlanogramFixtureAssemblyDal>();
                batchContext.Register<PlanogramFixtureComponentDto, IPlanogramFixtureComponentDal>();
                batchContext.Register<PlanogramSubComponentDto, IPlanogramSubComponentDal>();
                batchContext.Register<PlanogramProductDto, IPlanogramProductDal>();
                batchContext.Register<PlanogramPositionDto, IPlanogramPositionDal>();
                batchContext.Register<PlanogramAnnotationDto, IPlanogramAnnotationDal>();
                batchContext.Register<PlanogramAssortmentDto, IPlanogramAssortmentDal>();
                batchContext.Register<PlanogramAssortmentLocalProductDto, IPlanogramAssortmentLocalProductDal>();
                batchContext.Register<PlanogramAssortmentProductDto, IPlanogramAssortmentProductDal>();
                batchContext.Register<PlanogramAssortmentRegionDto, IPlanogramAssortmentRegionDal>();
                batchContext.Register<PlanogramAssortmentRegionLocationDto, IPlanogramAssortmentRegionLocationDal>();
                batchContext.Register<PlanogramAssortmentRegionProductDto, IPlanogramAssortmentRegionProductDal>();
                batchContext.Register<PlanogramAssortmentInventoryRuleDto, IPlanogramAssortmentInventoryRuleDal>();
                batchContext.Register<PlanogramAssortmentLocationBuddyDto, IPlanogramAssortmentLocationBuddyDal>();
                batchContext.Register<PlanogramAssortmentProductBuddyDto, IPlanogramAssortmentProductBuddyDal>();
                batchContext.Register<PlanogramBlockingDto, IPlanogramBlockingDal>();
                batchContext.Register<PlanogramBlockingGroupDto, IPlanogramBlockingGroupDal>();
                batchContext.Register<PlanogramBlockingDividerDto, IPlanogramBlockingDividerDal>();
                batchContext.Register<PlanogramBlockingLocationDto, IPlanogramBlockingLocationDal>();
                batchContext.Register<PlanogramConsumerDecisionTreeDto, IPlanogramConsumerDecisionTreeDal>();
                batchContext.Register<PlanogramConsumerDecisionTreeLevelDto, IPlanogramConsumerDecisionTreeLevelDal>(true);
                batchContext.Register<PlanogramConsumerDecisionTreeNodeDto, IPlanogramConsumerDecisionTreeNodeDal>(true);
                batchContext.Register<PlanogramConsumerDecisionTreeNodeProductDto, IPlanogramConsumerDecisionTreeNodeProductDal>();
                batchContext.Register<PlanogramEventLogDto, IPlanogramEventLogDal>();
                batchContext.Register<PlanogramInventoryDto, IPlanogramInventoryDal>();
                batchContext.Register<PlanogramMetadataImageDto, IPlanogramMetadataImageDal>();
                batchContext.Register<PlanogramPerformanceDto, IPlanogramPerformanceDal>();
                batchContext.Register<PlanogramPerformanceDataDto, IPlanogramPerformanceDataDal>();
                batchContext.Register<PlanogramPerformanceMetricDto, IPlanogramPerformanceMetricDal>();
                batchContext.Register<PlanogramRenumberingStrategyDto, IPlanogramRenumberingStrategyDal>();
                batchContext.Register<PlanogramValidationTemplateDto, IPlanogramValidationTemplateDal>();
                batchContext.Register<PlanogramValidationTemplateGroupDto, IPlanogramValidationTemplateGroupDal>();
                batchContext.Register<PlanogramValidationTemplateGroupMetricDto, IPlanogramValidationTemplateGroupMetricDal>();
                batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>();
                batchContext.Register<PlanogramSequenceDto, IPlanogramSequenceDal>();
                batchContext.Register<PlanogramSequenceGroupDto, IPlanogramSequenceGroupDal>();
                batchContext.Register<PlanogramSequenceGroupSubGroupDto, IPlanogramSequenceGroupSubGroupDal>();
                batchContext.Register<PlanogramSequenceGroupProductDto, IPlanogramSequenceGroupProductDal>();
                batchContext.Register<PlanogramComparisonDto, IPlanogramComparisonDal>();
                batchContext.Register<PlanogramComparisonFieldDto, IPlanogramComparisonFieldDal>();
                batchContext.Register<PlanogramComparisonResultDto, IPlanogramComparisonResultDal>();
                batchContext.Register<PlanogramComparisonItemDto, IPlanogramComparisonItemDal>();
                batchContext.Register<PlanogramComparisonFieldValueDto, IPlanogramComparisonFieldValueDal>();
                batchContext.Register<ProductAttributeComparisonResultDto, IProductAttributeComparisonResultDal>();

                // update the planogram
                if (this.IsSelfDirty)
                {
                    using (IPlanogramDal dal = dalContext.GetDal<IPlanogramDal>())
                    {
                        PlanogramDto dto = this.GetDataTransferObject(parent);
                        dal.Update(parent, dto);
                        dalContext.RegisterId<Planogram>(dto.Id);
                    }
                }

                // update the children
                FieldManager.UpdateChildren(batchContext, this);

                // commit the batch
                batchContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PackageDto parent)
        {
            using (IPlanogramDal dal = dalContext.GetDal<IPlanogramDal>())
            {
                dal.DeleteById(this.ReadProperty<Object>(IdProperty));
            }
        }
        #endregion

        #endregion
    }
}