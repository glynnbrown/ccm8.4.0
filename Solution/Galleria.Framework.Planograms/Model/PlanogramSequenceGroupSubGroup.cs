﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Interfaces;
using Csla.Rules.CommonRules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Represents a grouping of products within a <see cref="PlanogramSequenceGroup"/>, that must be placed together on the
    /// same component.
    /// </summary>
    [Serializable]
    public partial class PlanogramSequenceGroupSubGroup : ModelObject<PlanogramSequenceGroupSubGroup>
    {
        #region Parent

        /// <summary>
        ///     Gets the parent <see cref="PlanogramSequence" /> for this instance.
        /// </summary>
        public new PlanogramSequenceGroup Parent
        {
            get
            {
                if (base.Parent == null) return null;
                return ((PlanogramSequenceGroupSubGroupList)base.Parent).Parent;
            }
        }

        #endregion

        #region Properties 

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///     Gets or sets the Name of this instance.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Alignment

        /// <summary>
        ///     Metadata for the <see cref="Alignment" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramSequenceGroupSubGroupAlignmentType> AlignmentProperty =
            RegisterModelProperty<PlanogramSequenceGroupSubGroupAlignmentType>(o => o.Alignment);

        /// <summary>
        ///     Gets or sets the Alignment of this instance.
        /// </summary>
        public PlanogramSequenceGroupSubGroupAlignmentType Alignment
        {
            get { return GetProperty(AlignmentProperty); }
            set { SetProperty(AlignmentProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
        }

        #endregion

        #region Factory Methods
        
        /// <summary>
        /// Creates a new instance from the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PlanogramSequenceGroupSubGroup NewPlanogramSequenceGroupSubGroup(PlanogramSequenceGroupSubGroup source)
        {
            var newSubGroup = new PlanogramSequenceGroupSubGroup();
            newSubGroup.Create(source);
            return newSubGroup;
        }

        /// <summary>
        /// Creates a new instance with default values.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static PlanogramSequenceGroupSubGroup NewPlanogramSequenceGroupSubGroup()
        {
            var newSubGroup = new PlanogramSequenceGroupSubGroup();
            newSubGroup.Create();
            return newSubGroup;
        }

        /// <summary>
        /// Creates a new instance for the given <paramref name="sequenceProduct"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PlanogramSequenceGroupSubGroup NewPlanogramSequenceGroupSubGroup(
            IEnumerable<PlanogramSequenceGroupProduct> sequenceProducts)
        {
            var newSubGroup = new PlanogramSequenceGroupSubGroup();
            newSubGroup.Create(sequenceProducts);
            return newSubGroup;
        }
        #endregion

        #region Data Access
        protected override void Create()
        {
            LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }

        private void Create(PlanogramSequenceGroupSubGroup source)
        {
            this.Create();
            LoadProperty(NameProperty, source.Name);
            LoadProperty(AlignmentProperty, source.Alignment);
        }

        private void Create(IEnumerable<PlanogramSequenceGroupProduct> sequenceProducts)
        {
            this.Create();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in sequenceProducts)
            {
                sequenceProduct.PlanogramSequenceGroupSubGroupId = this.Id;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Enumerates the <see cref="PlanogramSequencegGroupProduct"/>s that are in this sub-group.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramSequenceGroupProduct> EnumerateProducts()
        {
            return Parent.Products.Where(p => Object.Equals(p.PlanogramSequenceGroupSubGroupId, Id));
        }
        #endregion
    }
}
