﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     A list of <see cref="PlanogramSequenceGroup"/> contained withing a planogram Sequence.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSequenceGroupList : ModelList<PlanogramSequenceGroupList, PlanogramSequenceGroup>
    {
        #region Parent

        /// <summary>
        ///     Gets the <see cref="PlanogramSequence"/> that contains this instance.
        /// </summary>
        public new PlanogramSequence Parent
        {
            get { return (PlanogramSequence) base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramSequenceGroupList"/>.
        /// </summary>
        /// <returns></returns>
        public static PlanogramSequenceGroupList NewPlanogramSequenceGroupList()
        {
            var item = new PlanogramSequenceGroupList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Initializes the instance when it is being created.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion
    }
}