﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM820
// V8-30773 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Indicates the value type of a Planogram Metric
    /// </summary>
    public enum PlanogramInventoryMetricType
    {
        RegularSalesUnits,
        PromotionalSalesUnits,
    }

    /// <summary>
    /// PlanogramInventoryMetricType Helper Class
    /// </summary>
    public static class PlanogramInventoryMetricTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramInventoryMetricType, String> FriendlyNames =
            new Dictionary<PlanogramInventoryMetricType, String>()
            {
                {PlanogramInventoryMetricType.RegularSalesUnits, Message.Enum_PlanogramInventoryMetricType_RegularSalesUnits},
                {PlanogramInventoryMetricType.PromotionalSalesUnits, Message.Enum_PlanogramInventoryMetricType_PromotionalSalesUnits},
            };
    }
}
