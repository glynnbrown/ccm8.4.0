﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27759 : A.Kuszyk
//  Added additional options.
// V8-24779 : D.Pleasance
//  Added Import
#endregion
#region Version History: (CCM v8.3.0)
// V8-31546 : M.Pettit
//  Added Export
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Indicates the value type of a PlanogramEventLog Event Type
    /// </summary>
    public enum PlanogramEventLogEventType
    {
        General = 0,
        Addition = 1,
        Modification = 2,
        Removal = 3,
        Import = 4,
        Export = 5
    }

    /// <summary>
    /// PlanogramEventLogEventTypeHelper Helper Class
    /// </summary>
    public static class PlanogramEventLogEventTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public readonly static Dictionary<PlanogramEventLogEventType, String> FriendlyNames = new Dictionary<PlanogramEventLogEventType, String>
        {
            {PlanogramEventLogEventType.General, Message.Enum_PlanogramEventLogEventType_General},
            {PlanogramEventLogEventType.Addition, Message.Enum_PlanogramEventLogEventType_Addition},
            {PlanogramEventLogEventType.Modification, Message.Enum_PlanogramEventLogEventType_Modification},
            {PlanogramEventLogEventType.Removal, Message.Enum_PlanogramEventLogEventType_Removal},
            {PlanogramEventLogEventType.Import, Message.Enum_PlanogramEventLogEventType_Import},
            {PlanogramEventLogEventType.Export, Message.Enum_PlanogramEventLogEventType_Export},
        };

        public readonly static Dictionary<PlanogramEventLogEventType, String> FriendlyPastTenseVerbs = 
            new Dictionary<PlanogramEventLogEventType, String>
            {
                {PlanogramEventLogEventType.General, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_General},
                {PlanogramEventLogEventType.Addition, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_Added},
                {PlanogramEventLogEventType.Modification, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_Modified},
                {PlanogramEventLogEventType.Removal, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_Removed},
                {PlanogramEventLogEventType.Import, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_Import},
                {PlanogramEventLogEventType.Export, Message.Enum_PlanogramEventLogEventType_FriendlyPastTenseVerbs_Export},
            };
    }
}
