﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.
// V8-26782 : A.Silva ~ Corrected an issue with new groups not having a valid name when just added.
// V8-26815 : A.Silva ~ Added LoadFrom methog.
// V8-26817 : A.Silva ~ Modified the implementation of IValidationTemplate.LoadFrom, to make it implicit.
// V8-26952 : A.Kuszyk ~ Ensured lazy load properties have correct RelationshipType.
// V8-27237 : A.Silva   ~ Extended for ClearValidationData.
// V8-27496 : L.Luong
//  Added a ClearAll method
#endregion
#region Version History: CCM820
// V8-31483 : N.Foster
//  Improved performance of validation data calculation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramValidationTemplate model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationTemplate : ModelObject<PlanogramValidationTemplate>, IValidationTemplate
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        ///     Gets or sets the name used for this instance.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Groups
        /// <summary>
        /// Products property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateGroupList> GroupsProperty =
            RegisterModelProperty<PlanogramValidationTemplateGroupList>(c => c.Groups, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the planogram validation group list
        /// </summary>
        public PlanogramValidationTemplateGroupList Groups
        {
            get
            {
                return this.GetPropertyLazy<PlanogramValidationTemplateGroupList>(
                    GroupsProperty,
                    new PlanogramValidationTemplateGroupList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Returns the planogram validation group list
        /// </summary>
        public PlanogramValidationTemplateGroupList GroupsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramValidationTemplateGroupList>(
                    GroupsProperty,
                    new PlanogramValidationTemplateGroupList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId factory method
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(c => c.PlanogramId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object PlanogramId
            {
                get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(PlanogramIdProperty, planogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplate NewPlanogramValidationTemplate()
        {
            var item = new PlanogramValidationTemplate();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.PlanogramValidationTemplate_Name_Default);
            this.LoadProperty<PlanogramValidationTemplateGroupList>(GroupsProperty, PlanogramValidationTemplateGroupList.NewPlanogramValidationTemplateGroupList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramValidationTemplate>(oldId, newId);
        }

        /// <summary>
        /// Clears the Validation Template
        /// </summary>
        public void ClearAll()
        {
            foreach (PlanogramValidationTemplateGroup Group in Groups) 
            {
                Group.Metrics.Clear();
            }
            Groups.Clear();

            Name = Message.PlanogramValidationTemplate_Name_Default;
        }

        #region Validation data
        /// <summary>
        /// Calculates validation data for this instance
        /// </summary>
        protected override void CalculateValidationData()
        {
            ((IModelList)this.Groups).CalculateValidationData();
        }

        /// <summary>
        /// Clears metadata for this instance
        /// </summary>
        protected override void ClearValidationData()
        {
            ((IModelList)this.Groups).ClearValidationData();
        }

        #endregion

        #endregion

        #region IValidationTemplate Members

        IEnumerable<IValidationTemplateGroup> IValidationTemplate.Groups
        {
            get { return Groups.Select(@group => @group as IValidationTemplateGroup); }
        }

        IValidationTemplateGroup IValidationTemplate.AddNewGroup(String name)
        {
            return this.Groups.Add(name);
        }

        void IValidationTemplate.RemoveGroup(IValidationTemplateGroup templateGroup)
        {
            var removedItem = templateGroup as PlanogramValidationTemplateGroup;
            if (removedItem == null || !this.Groups.Contains(removedItem)) return;

            this.Groups.Remove(removedItem);
        }

        /// <summary>
        ///     Loads the data contained in the given <paramref name="template"/> into this instance.
        /// </summary>
        /// <param name="template">Instance implementing <see cref="IValidationTemplate"/> that will provide the values to be loaded into this instance.</param>
        public void LoadFrom(IValidationTemplate template)
        {
            Groups.Clear();
            Name = template.Name;
            foreach (var group in template.Groups)
            {
                Groups.Add(PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup(group));
            }
        }

        #endregion
    }
}
