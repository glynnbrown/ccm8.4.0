﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.
// V8-26815 : A.Silva ~ Added constructor from source.
// V8-26954 : A.Silva ~ Addded ResultTypeProperty.
// V8-26812 : A.Silva ~ Addded ValidationType property.
// V8-27062 : A.Silva ~ Added implementation for Validation Calculations.
// V8-27237 : A.Silva   ~ Extended for ClearValidationData.
// V8-27447 : L.Ineson
//  Master field list now comes from PlanogramHelper method.
// V8-27479 : A.Silva
//      Amended to ignore PlanogramComponents with IsCarPark == true.
#endregion
#region Version History: CCM810
//  V8-29899 : M.Brumby
//      Handle nullable booleans
// V8-30017 : L.Ineson
//      Where the metric property is a percentage type, user entered thresholds are now adjusted.
// V8-30116 : N.Haywood
//      Made assortment specific meta data be ignored where assortment content is not assigned to the planogram or has no products (default plans have empty assortments)
#endregion
#region Version History: CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark flag
// V8-31483 : N.Foster
//  Improved performance of validation data calculation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     PlanogramValidationTemplateGroupMetric Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationTemplateGroupMetric :
        ModelObject<PlanogramValidationTemplateGroupMetric>, IValidationTemplateGroupMetric
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Returns a reference to the parent object
        /// </summary>
        public new PlanogramValidationTemplateGroup Parent
        {
            get { return ((PlanogramValidationTemplateGroupMetricList)base.Parent).Parent; }
        }

        #endregion

        #region Field

        /// <summary>
        ///     Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<string> FieldProperty =
            RegisterModelProperty<string>(c => c.Field);

        /// <summary>
        ///     Field
        /// </summary>
        public String Field
        {
            get { return GetProperty(FieldProperty); }
            set { SetProperty(FieldProperty, value); }
        }

        #endregion

        #region Threshold1

        /// <summary>
        ///     Threshold1 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<float> Threshold1Property =
            RegisterModelProperty<float>(c => c.Threshold1);

        /// <summary>
        ///     Threshold1
        /// </summary>
        public Single Threshold1
        {
            get { return GetProperty(Threshold1Property); }
            set { SetProperty(Threshold1Property, value); }
        }

        #endregion

        #region Threshold2

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<float> Threshold2Property =
            RegisterModelProperty<float>(c => c.Threshold2);

        /// <summary>
        ///     Threshold2
        /// </summary>
        public Single Threshold2
        {
            get { return GetProperty(Threshold2Property); }
            set { SetProperty(Threshold2Property, value); }
        }

        #endregion

        #region Score1

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<float> Score1Property =
            RegisterModelProperty<float>(c => c.Score1);

        /// <summary>
        ///     Threshold2
        /// </summary>
        public Single Score1
        {
            get { return GetProperty(Score1Property); }
            set { SetProperty(Score1Property, value); }
        }

        #endregion

        #region Score2

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<float> Score2Property =
            RegisterModelProperty<float>(c => c.Score2);

        /// <summary>
        ///     Threshold2
        /// </summary>
        public Single Score2
        {
            get { return GetProperty(Score2Property); }
            set { SetProperty(Score2Property, value); }
        }

        #endregion

        #region Score3

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<float> Score3Property =
            RegisterModelProperty<float>(c => c.Score3);

        /// <summary>
        ///     Threshold2
        /// </summary>
        public Single Score3
        {
            get { return GetProperty(Score3Property); }
            set { SetProperty(Score3Property, value); }
        }

        #endregion

        #region AggregationType

        /// <summary>
        ///     AggregationType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationAggregationType> AggregationTypeProperty =
            RegisterModelProperty<PlanogramValidationAggregationType>(c => c.AggregationType);

        /// <summary>
        ///     Gets/Sets the AggregationType value
        /// </summary>
        public PlanogramValidationAggregationType AggregationType
        {
            get { return GetProperty(AggregationTypeProperty); }
            set { SetProperty(AggregationTypeProperty, value); }
        }

        #endregion

        #region ResultType

        /// <summary>
        ///     Metadata for the <see cref="ResultType" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateResultType> ResultTypeProperty =
            RegisterModelProperty<PlanogramValidationTemplateResultType>(o => o.ResultType);

        /// <summary>
        ///     Gets or sets the value for the <see cref="ResultType" /> property.
        /// </summary>
        public PlanogramValidationTemplateResultType ResultType
        {
            get { return GetProperty(ResultTypeProperty); }
            set { SetProperty(ResultTypeProperty, value); }
        }

        #endregion

        #region ValidationType

        /// <summary>
        ///     Metadata for the <see cref="ValidationType" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationType> ValidationTypeProperty =
            RegisterModelProperty<PlanogramValidationType>(o => o.ValidationType);

        /// <summary>
        ///     Gets or sets the value for the <see cref="ValidationType" /> property.
        /// </summary>
        public PlanogramValidationType ValidationType
        {
            get { return GetProperty(ValidationTypeProperty); }
            set { SetProperty(ValidationTypeProperty, value); }
        }

        #endregion

        #region Criteria

        /// <summary>
        ///     Criteria property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> CriteriaProperty =
            RegisterModelProperty<String>(o => o.Criteria);

        /// <summary>
        ///     Gets or sets the value for the <see cref="Criteria" /> property.
        /// </summary>
        public String Criteria
        {
            get { return GetProperty(CriteriaProperty); }
            set { SetProperty(CriteriaProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(FieldProperty));
            BusinessRules.AddRule(new MaxLength(FieldProperty, 255));
            BusinessRules.AddRule(new PlanogramHelper.ValidationTypeConstraint(ValidationTypeProperty,
                Threshold1Property, Threshold2Property));
            BusinessRules.AddRule(new Dependency(Threshold1Property, ValidationTypeProperty));
            BusinessRules.AddRule(new Dependency(Threshold2Property, ValidationTypeProperty));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroupMetric NewPlanogramValidationTemplateGroupMetric()
        {
            var item = new PlanogramValidationTemplateGroupMetric();
            item.Create();
            return item;
        }

        public static PlanogramValidationTemplateGroupMetric NewPlanogramValidationTemplateGroupMetric(
            IValidationTemplateGroupMetric source)
        {
            var item = new PlanogramValidationTemplateGroupMetric();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(Score1Property, 0F);
            LoadProperty(Score2Property, 5F);
            LoadProperty(Score3Property, 20F);
            MarkAsChild();
            base.Create();
        }

        private void Create(IValidationTemplateGroupMetric source)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(FieldProperty, source.Field);
            LoadProperty(AggregationTypeProperty, source.AggregationType);
            LoadProperty(Threshold1Property, source.Threshold1);
            LoadProperty(Threshold2Property, source.Threshold2);
            LoadProperty(Score1Property, source.Score1);
            LoadProperty(Score2Property, source.Score2);
            LoadProperty(Score3Property, source.Score3);
            LoadProperty(ValidationTypeProperty, source.ValidationType);
            LoadProperty(CriteriaProperty, source.Criteria);
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0} {1} {2} {3}",
                PlanogramValidationAggregationTypeHelper.FriendlyNames[AggregationType],
                Field,
                PlanogramValidationTypeHelper.FriendlyGreenResultCaptions[ValidationType],
                Threshold1);
        }

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramValidationTemplateGroupMetric>(oldId, newId);
        }

        #region Validation Data
        /// <summary>
        /// Calculates validation data for this instance
        /// </summary>
        protected override void CalculateValidationData()
        {
            PlanogramValidationTemplateResultType validationResult = PlanogramValidationTemplateResultType.Unknown;
            var masterFieldDictionary = PlanogramFieldHelper.GetPlanogramObjectFieldInfos(); 

            ObjectFieldInfo field = ObjectFieldInfo.ExtractFieldsFromText(Field,  masterFieldDictionary.SelectMany(d=> d.Value)).FirstOrDefault();
            if (field != null)
            {
                Object metricValue = GetValue(field);
                if (field.PropertyName == Planogram.MetaCountOfPlacedProductsRecommendedInAssortmentProperty.Name ||
                    field.PropertyName == Planogram.MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty.Name ||
                    field.PropertyName == Planogram.MetaCountOfRecommendedProductsInAssortmentProperty.Name ||
                    field.PropertyName == Planogram.MetaPercentOfPlacedProductsRecommendedInAssortmentProperty.Name ||
                    field.PropertyName == Planogram.MetaNotAchievedInventoryProperty.Name ||
                    field.PropertyName == Planogram.MetaPercentOfProductNotAchievedInventoryProperty.Name)
                {
                    var planogram = Parent.Parent.Parent;
                    if (planogram != null)
                    {
                        if (planogram.Assortment == null || planogram.Assortment.Products.Count == 0)
                        {
                            validationResult = PlanogramValidationTemplateResultType.Green;
                        }
                    }
                }
                else if (metricValue != null)
                {
                    if (field.PropertyType == typeof(String))
                    {
                        List<Object> stringValues = new List<Object>();
                        stringValues = metricValue as List<Object>;

                        validationResult = ValidationType.GetResultTypeString(stringValues, Criteria , Threshold1);
                    }
                    else if (field.PropertyType == typeof(Boolean) || field.PropertyType == typeof(Boolean?))
                    {
                        List<Object> booleanValues = metricValue as List<Object>;
                        booleanValues = metricValue as List<Object>;

                        validationResult = ValidationType.GetResultTypeBoolean(booleanValues, Threshold1);
                    }
                    else if (field.PropertyDisplayType == ModelPropertyDisplayType.Percentage)
                    {
                        //[V8-30017] If the property is a percentage we should divide thresholds by 100.
                        validationResult = this.ValidationType.GetResultType(
                            Convert.ToSingle(metricValue), 
                            this.Threshold1/100F, 
                            this.Threshold2/100F);
                    }
                    else
                    {
                        validationResult = this.ValidationType.GetResultType(Convert.ToSingle(metricValue), this.Threshold1, this.Threshold2);
                    }
                }
            }

            this.ResultType = validationResult;
        }

        private Object GetValue(ObjectFieldInfo objectFieldInfo)
        {
            var planogram = Parent.Parent.Parent;
            var ownerType = objectFieldInfo.OwnerType;

            IList<Framework.Model.IModelObject> collection;
            if (ownerType == typeof(Planogram))
            {
                return objectFieldInfo.GetValue(planogram);
            }

            if (ownerType == typeof(PlanogramProduct))
            {
                //Assortment products are special so we have to do this sort of stupid thing
                //everywhere, otherwise to do it right we would have to re-write large sections
                //of code.
                if (objectFieldInfo.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
                {
                    collection = planogram.Assortment?.Products?.Cast<Framework.Model.IModelObject>().ToList();

                    if (objectFieldInfo.IsNumeric)
                    {
                        var aggregate = collection.Select(o => Convert.ToSingle(WpfHelper.GetItemPropertyValue(o, objectFieldInfo.PropertyName.Split('.')[1])))
                        .Aggregate((f, f1) => AggregationType.CalculateAggregation(f, f1));
                        if (AggregationType == PlanogramValidationAggregationType.Avg) aggregate /= collection.Count;
                        return aggregate;
                    }
                    else
                    {
                        return collection.Select(o => WpfHelper.GetItemPropertyValue(o, objectFieldInfo.PropertyName.Split('.')[1])).ToList();
                    }
                }
                else
                {
                    collection = planogram.Products.Cast<Framework.Model.IModelObject>().ToList();
                }
            }
            else if (ownerType == typeof(PlanogramPerformanceData))
            {
                collection = planogram.Products.Select(p => p.GetPlanogramPerformanceData()).Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramPosition))
            {
                collection = planogram.Positions.Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramSubComponent))
            {
                collection = planogram.Components.SelectMany(c => c.SubComponents).Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramComponent))
            {
                collection = planogram.Components.Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramFixtureComponent))
            {
                collection = planogram.Fixtures.SelectMany(f => f.Components).Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramFixture))
            {
                collection = planogram.Fixtures.Cast<Framework.Model.IModelObject>().ToList();
            }
            else if (ownerType == typeof(PlanogramFixtureItem))
            {
                collection = planogram.FixtureItems.Cast<Framework.Model.IModelObject>().ToList();
            }
            else
            {
                return null;
            }

            if (collection.Count == 0) return 0F;

            if (objectFieldInfo.IsNumeric)
            {
                var aggregate = collection.Select(o => Convert.ToSingle(objectFieldInfo.GetValue(o)))
                .Aggregate((f, f1) => AggregationType.CalculateAggregation(f, f1));
                if (AggregationType == PlanogramValidationAggregationType.Avg) aggregate /= collection.Count;
                return aggregate;
            }
            else
            {
                return collection.Select(o => objectFieldInfo.GetValue(o)).ToList();
            }
        }

        /// <summary>
        /// Clears the validation data for this instance
        /// </summary>
        protected override void ClearValidationData()
        {
            this.ResultType = PlanogramValidationTemplateResultType.Unknown;
        }

        #endregion

        #endregion

        #region IValidationTemplateGroupMetric Members

        IValidationTemplateGroup IValidationTemplateGroupMetric.Parent
        {
            get { return Parent; }
        }

        #endregion
    }
}