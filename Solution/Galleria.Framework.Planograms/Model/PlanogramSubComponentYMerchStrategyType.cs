﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-24974 : L.Hodson
//      Added BottomStacked and TopStacked
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramSubComponentYMerchStrategyType
    {
        Manual = 0,         // Do not align the child planogram products
        Bottom = 1,         // The bottom facing of all positions are aligned along the bottom of the merchandisable area. All positions are forced to a SequenceY value of 1
        BottomStacked = 2,  // The position groups are  aligned along the bottom of the merchandisable area. Positions are allowed to stack on top of each other and so SequenceY values above 1 are allowed.
        Top = 3,            // The top facing of all positions are aligned along the top of the merchandisable area. All positions are forced to a SequenceY value of 1
        TopStacked = 4,     // The position groups are aligned along the top of the merchandisable area. Positions are allowed to stack underneath each other and so SequenceY values above 1 are allowed.
        Even = 5            // ?
    }

    /// <summary>
    /// PlanogramSubComponentYMerchStrategyType Helper Class
    /// </summary>
    public static class PlanogramSubComponentYMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentYMerchStrategyType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentYMerchStrategyType, String>()
            {
                {PlanogramSubComponentYMerchStrategyType.Manual, Message.Enum_PlanogramSubComponentYMerchStrategyType_Manual},
                {PlanogramSubComponentYMerchStrategyType.Bottom, Message.Enum_PlanogramSubComponentYMerchStrategyType_Bottom},
                {PlanogramSubComponentYMerchStrategyType.BottomStacked, Message.Enum_PlanogramSubComponentYMerchStrategyType_BottomStacked},
                {PlanogramSubComponentYMerchStrategyType.Top, Message.Enum_PlanogramSubComponentYMerchStrategyType_Top},
                {PlanogramSubComponentYMerchStrategyType.TopStacked, Message.Enum_PlanogramSubComponentYMerchStrategyType_TopStacked},
                {PlanogramSubComponentYMerchStrategyType.Even, Message.Enum_PlanogramSubComponentYMerchStrategyType_Even},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentYMerchStrategyType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentYMerchStrategyType>()
            {
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_Manual.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.Manual},
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_Bottom.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.Bottom},
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_BottomStacked.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.BottomStacked},
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_Top.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.Top},
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_TopStacked.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.TopStacked},
                {Message.Enum_PlanogramSubComponentYMerchStrategyType_Even.ToLowerInvariant(), PlanogramSubComponentYMerchStrategyType.Even},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentYMerchStrategyType? PlanogramSubComponentYMerchStrategyTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentYMerchStrategyType? returnValue = null;
            PlanogramSubComponentYMerchStrategyType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
