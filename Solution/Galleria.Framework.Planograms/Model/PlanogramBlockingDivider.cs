﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-26986 : A.Kuszyk
//  Added Get Blocking methods.
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
// V8-27403 : A.Kuszyk
//  Ensured car park shelves won't be picked up as snappable.
// V8-27402 : A.Kuszyk
//  Added peg holes to snappable component boundaries.
// V8-27467 : A.Kuszyk
//  Adjusted the way snapping boundaries are selected.
// V8-27502 : A.Kuszyk
//  Added top down offset to planogram relative component Y positions.
// V8-27510 : A.Kuszyk
//  Changed references to positions to use DesignViewHelper.
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
// V8-27546 : A.Kuszyk
//  Amended GetOverlap to use BlockingHelper.
// V8-27439 : A.Kuszyk
//  Added GetCeilingDividerId method.
// V8-27585 : A.Kuszyk
//  Fixed IsOverDesignViewPosition method not using relative positions.
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
#endregion

#region Version History: CCM802
// V8-29000: A.Silva
//      Added call to Parent.OnBlockingChangeCompleted so that changes to the divider are notified when complete.
#endregion

#region Version History : CCM820
// V8-30686 : A.Kuszyk
//  Updated GetParallelParentDivider to check for overlaps.
// V8-31393 : L.Ineson
//  Amended how notifications are passed around
#endregion

#region Version History : CCM830
// V8-31680 : A.Kuszyk
//  Corrected an issue in GetParallelParentDivider relating to the selection of parent dividers.
// V8-32018 : A.Kuszyk
//  Added SetState method for undo actions in snapping calculations.
// V8-32539 : A.Kuszyk
//  Added Get coordinate methods.
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
// CCM-13855 : A.Kuszyk
//  Made X, Y and Length properties publicly settable so that they can be un-done from a UI.
#endregion
#endregion

using System;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Helpers;
using System.Diagnostics;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a Planogram Blocking Divider object
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Type} - X:{X}, Y:{Y}, Length:{Length}")]
    public sealed partial class PlanogramBlockingDivider : ModelObject<PlanogramBlockingDivider>, IPlanogramBlockingDivider
    {
        #region Constants
        public const Byte RootLevelNumber = 1;
        
        /// <summary>
        /// The threshold above which to consider the overlap between a divider and a fixture indicates
        /// that the divider is over the fixture.
        /// </summary>
        private const Single _fixtureSpanThreshold = 0f;
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get 
            {
                PlanogramBlockingDividerList parentList = base.Parent as PlanogramBlockingDividerList;
                if (parentList == null) return null;
                return parentList.Parent; 
            }
        }
        #endregion

        #region Properties

        #region Type
        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingDividerType> TypeProperty =
            RegisterModelProperty<PlanogramBlockingDividerType>(c => c.Type);
        /// <summary>
        /// The type of divider this is
        /// </summary>
        public PlanogramBlockingDividerType Type
        {
            get { return GetProperty<PlanogramBlockingDividerType>(TypeProperty); }
            set { SetProperty<PlanogramBlockingDividerType>(TypeProperty, value); }
        }
        #endregion

        #region Level
        /// <summary>
        /// Level property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> LevelProperty =
            RegisterModelProperty<Byte>(c => c.Level);
        /// <summary>
        /// The level of this divider
        /// </summary>
        public Byte Level
        {
            get { return GetProperty<Byte>(LevelProperty); }
            set { SetProperty<Byte>(LevelProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);
        /// <summary>
        /// The x placement of this divider
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set 
            {
                Single currentValue = this.ReadProperty<Single>(XProperty);
                if (value != currentValue)
                {
                    SetProperty<Single>(XProperty, value);
                    NotifyDividerMoved();
                }
            }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);
        /// <summary>
        /// The y placement of this divider
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set 
            {
                Single currentValue = this.ReadProperty<Single>(YProperty);
                if (value != currentValue)
                {
                    SetProperty<Single>(YProperty, value);
                    NotifyDividerMoved();
                }
            }
        }
        #endregion

        #region Length
        /// <summary>
        /// Length property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LengthProperty =
            RegisterModelProperty<Single>(c => c.Length);
        /// <summary>
        /// The length of this divider
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single Length
        {
            get { return GetProperty<Single>(LengthProperty); }
            set { SetProperty<Single>(LengthProperty, value); }
        }
        #endregion

        #region IsSnapped
        /// <summary>
        /// IsSnapped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsSnappedProperty =
            RegisterModelProperty<Boolean>(c => c.IsSnapped);
        /// <summary>
        /// Get/Sets whether this divider should snap to uprights
        /// </summary>
        public Boolean IsSnapped
        {
            get { return GetProperty<Boolean>(IsSnappedProperty); }
            set { SetProperty<Boolean>(IsSnappedProperty, value); }
        }
        #endregion

        #region IsLimited

        /// <summary>
        /// IsLimited property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLimitedProperty =
            RegisterModelProperty<Boolean>(c => c.IsLimited);

        /// <summary>
        /// Indicates whether or not the divider's position is limited when block
        /// space is re-calculated using performance. If <b>true</b>, the <see cref="LimitedPercentage"/> 
        /// threshold is used
        /// 
        /// </summary>
        public Boolean IsLimited
        {
            get { return GetProperty<Boolean>(IsLimitedProperty); }
            set { SetProperty<Boolean>(IsLimitedProperty, value); }
        }
        
        #endregion

        #region LimitedPercentage

        /// <summary>
        /// LimitedPercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LimitedPercentageProperty =
            RegisterModelProperty<Single>(c => c.LimitedPercentage);

        /// <summary>
        /// Gets/Sets the LimitedPercentage value
        /// </summary>
        public Single LimitedPercentage
        {
            get { return GetProperty<Single>(LimitedPercentageProperty); }
            set { SetProperty<Single>(LimitedPercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Byte>(LevelProperty, 0));
            BusinessRules.AddRule(new MinValue<Single>(XProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(XProperty, 1));
            BusinessRules.AddRule(new MinValue<Single>(YProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(YProperty, 1));
            BusinessRules.AddRule(new MinValue<Single>(LengthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(LengthProperty, 1));
            BusinessRules.AddRule(new MaxPercentageValue<Single>(LimitedPercentageProperty, 1));
            BusinessRules.AddRule(new MinPercentageValue<Single>(LimitedPercentageProperty, 0f));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingDivider NewPlanogramBlockingDivider()
        {
            PlanogramBlockingDivider item = new PlanogramBlockingDivider();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingDivider NewPlanogramBlockingDivider(IPlanogramBlockingDivider source, IResolveContext context)
        {
            PlanogramBlockingDivider item = new PlanogramBlockingDivider();
            item.Create(source, context);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramBlockingDivider NewPlanogramBlockingDivider(
            Byte levelNo, PlanogramBlockingDividerType type, Single x, Single y, Single length)
        {
            PlanogramBlockingDivider item = new PlanogramBlockingDivider();
            item.Create(levelNo, type, x, y, length);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramBlockingDivider source, IResolveContext context)
        {
            var newId = IdentityHelper.GetNextInt32();
            context.RegisterId<PlanogramBlockingDivider>(source.Id, newId);

            this.LoadProperty<Object>(IdProperty, newId);
            this.LoadProperty<PlanogramBlockingDividerType>(TypeProperty, source.Type);
            this.LoadProperty<Single>(XProperty, source.X);
            this.LoadProperty<Single>(YProperty, source.Y);
            this.LoadProperty<Single>(LengthProperty, source.Length);
            this.LoadProperty<Boolean>(IsSnappedProperty, source.IsSnapped);
            this.LoadProperty<Byte>(LevelProperty, source.Level);
            this.LoadProperty<Boolean>(IsLimitedProperty, source.IsLimited);
            this.LoadProperty<Single>(LimitedPercentageProperty, source.LimitedPercentage);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Byte levelNo, PlanogramBlockingDividerType type, Single x, Single y, Single length)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(LevelProperty, levelNo);
            this.LoadProperty<PlanogramBlockingDividerType>(TypeProperty, type);
            this.LoadProperty<Single>(XProperty, x);
            this.LoadProperty<Single>(YProperty, y);
            this.LoadProperty<Single>(LengthProperty, length);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Gets the coordinate of this divider in the same direction as its <see cref="Type"/>. E.g. for
        /// a horizontal divider, this method returns the <see cref="X"/> coordinate.
        /// </summary>
        /// <returns></returns>
        public Single GetDirectionCoordinate()
        {
            return Type == PlanogramBlockingDividerType.Horizontal ? X : Y;
        }

        /// <summary>
        /// Gets the coordinate of this divider in the direction normal to its <see cref="Type"/>. E.g. for
        /// a horizontal divider, this method returns the <see cref="Y"/> coordinate.
        /// </summary>
        /// <returns></returns>
        public Single GetNormalCoordinate()
        {
            return Type == PlanogramBlockingDividerType.Horizontal ? Y : X;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramBlockingDivider>(oldId, newId);
        }

        /// <summary>
        /// Moves this divider by the given delta in the correct direction according to its
        /// type. Also ensures that other dividers in the blocking are correctly updated.
        /// </summary>
        /// <param name="delta"></param>
        public void MoveTo(Single newPosition)
        {
            if (this.Type == PlanogramBlockingDividerType.Vertical)
            {
                MoveVerticalDividerTo(this, newPosition);
            }
            else
            {
                MoveHorizontalDividerTo(this, newPosition);
            }
        }

        /// <summary>
        /// Moves this divider by the given delta.
        /// </summary>
        /// <param name="delta"></param>
        public void MoveBy(Single delta)
        {
            if (this.Parent == null) return;

            Single newPosition;
            if (this.Type == PlanogramBlockingDividerType.Horizontal)
            {
                newPosition = this.Y + delta;
            }
            else
            {
                newPosition = this.X + delta;
            }

            MoveTo(newPosition);
        }

        private static void MoveVerticalDividerTo(PlanogramBlockingDivider divider, Single newDivX)
        {
            Debug.Assert(divider.Type == PlanogramBlockingDividerType.Vertical);

            Single oldX = divider.X;
            if (newDivX == oldX) return;

            //get a list of adjacent dividers
            List<PlanogramBlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

            //get the min and max based on dividers either side
            PlanogramBlockingDivider minParallel, maxParallel;
            divider.GetParallelDividers(out minParallel, out maxParallel);

            Single minX = Convert.ToSingle(Math.Round((minParallel != null) ? minParallel.X + 0.01F : 0.01F, 5));
            Single maxX = Convert.ToSingle(Math.Round((maxParallel != null) ? maxParallel.X - 0.01F : 0.99F, 5));


            //Clamp the delta and apply
            newDivX = Math.Max(minX, newDivX);
            newDivX = Math.Min(maxX, newDivX);
            divider.X = newDivX;

            //update all affected dividers
            foreach (PlanogramBlockingDivider div in adjacentDividers)
            {
                if (div.X == oldX)
                {
                    div.X = divider.X;
                }
                div.UpdateLength();
            }

            if (divider.Parent != null) divider.Parent.OnBlockingChangeCompleted();
        }

        private static void MoveHorizontalDividerTo(PlanogramBlockingDivider divider, Single newDivY)
        {
            Debug.Assert(divider.Type == PlanogramBlockingDividerType.Horizontal);

            Single oldY = divider.Y;
            if (newDivY == oldY) return;

            //get a list of adjacent dividers
            List<PlanogramBlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

            //get the min and max based on dividers either side
            PlanogramBlockingDivider minParallel, maxParallel;
            divider.GetParallelDividers(out minParallel, out maxParallel);

            Single minY = Convert.ToSingle(Math.Round((minParallel != null) ? minParallel.Y + 0.01F : 0.01F, 5));
            Single maxY = Convert.ToSingle(Math.Round((maxParallel != null) ? maxParallel.Y - 0.01F : 0.99F, 5));


            //Clamp the delta and apply
            newDivY = Math.Max(minY, newDivY);
            newDivY = Math.Min(maxY, newDivY);
            divider.Y = newDivY;

            //update all affected dividers
            foreach (PlanogramBlockingDivider div in adjacentDividers)
            {
                if (div.Y == oldY)
                {
                    div.Y = divider.Y;
                }
                div.UpdateLength();
            }

            if (divider.Parent != null) divider.Parent.OnBlockingChangeCompleted();
        }

        /// <summary>
        /// Updates the length value of this divider
        /// </summary>
        internal void UpdateLength()
        {
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null)
            {
                this.Length = 1;
                return;
            }
            else
            {
                Single max = 0;
                Boolean crossFound = false;
                if (this.Type == PlanogramBlockingDividerType.Vertical)
                {
                    foreach (PlanogramBlockingLocation l in parentBlocking.Locations)
                    {
                        if (Object.Equals(l.PlanogramBlockingDividerLeftId, this.Id))
                        {
                            max = Math.Max(max, l.Y + l.Height);
                            crossFound = true;
                        }
                    }

                    this.Length = Convert.ToSingle(Math.Round((crossFound) ? max - this.Y : 1, 5));
                }
                else
                {
                    foreach (PlanogramBlockingLocation l in parentBlocking.Locations)
                    {
                        if (Object.Equals(l.PlanogramBlockingDividerBottomId, this.Id))
                        {
                            max = Math.Max(max, l.X + l.Width);
                            crossFound = true;
                        }
                    }

                    this.Length = Convert.ToSingle(Math.Round((crossFound) ? max - this.X : 1, 5));
                }

            }
        }
        
        /// <summary>
        /// Sets the state of the <see cref="X"/>, <see cref="Y"/> and <see cref="Length"/> properties, by-passing
        /// all the associated calculations that normally takes place.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="length"></param>
        internal void SetState(Single x, Single y, Single length)
        {
            this.X = x;
            this.Y = y;
            this.Length = length;
        }

        /// <summary>
        /// Returns all dividers which are adjacent to this one.
        /// </summary>
        public List<PlanogramBlockingDivider> GetAdjacentDividers()
        {
            return BlockingHelper.GetAdjacentDividers(this, this.Parent);
        }

        /// <summary>
        /// Returns an the nearest parallel dividers before and after this one
        /// </summary>
        /// <returns></returns>
        public void GetParallelDividers(out PlanogramBlockingDivider nearestMin, out PlanogramBlockingDivider nearestMax)
        {
            BlockingHelper.GetParallelDividers(this, this.Parent, out nearestMin, out nearestMax);
        }

        /// <summary>
        /// Gets a collection of locations with which this divider is associated.
        /// </summary>
        public IEnumerable<PlanogramBlockingLocation> GetLocations()
        {
            if (Parent == null) throw new InvalidOperationException("Parent cannot be null");
            return Parent.Locations.Where(l => l.IsAssociated(this));
        }

        #region GetBlocking Methods

        /// <summary>
        /// Gets the FixtureItems that are spanned by this divider by more than the Fixture Span Threshold.
        /// </summary>
        /// <returns>The FixtureItems that this divider is over.</returns>
        public IEnumerable<PlanogramFixtureItem> GetParentFixtureItems()
        {
            if (Parent == null || Parent.Parent == null) throw new InvalidOperationException("Parent cannot be null");
            return Parent.Parent.FixtureItems.Where(f => GetAmountOfFixtureSpanned(f) > _fixtureSpanThreshold);
        }

        /// <summary>
        /// Determines if this divider is positioned over the co-ordinates given.
        /// </summary>
        /// <param name="designViewPosition">The position to evaluate.</param>
        /// <param name="planogramHeight">The design view planogram height, against which the given position will be normalised.</param>
        /// <param name="planogramWidth">The design view planogram width, against which the given position will be normalised.</param>
        /// <returns>True if this divider is situated over the given position.</returns>
        public Boolean IsOverDesignViewPosition(DesignViewPosition designViewPosition,Single planogramWidth, Single planogramHeight)
        {
            if (planogramWidth == 0 || planogramHeight == 0) return false;

            Single dvpRelativeX = designViewPosition.BoundX / planogramWidth;
            Single dvpRelativeWidth = designViewPosition.BoundWidth / planogramWidth;
            Single dvpRelativeY = designViewPosition.BoundY / planogramHeight;
            Single dvpRelativeHeight = designViewPosition.BoundHeight / planogramHeight;

            if(Type==PlanogramBlockingDividerType.Horizontal)
            {
                return
                    BlockingHelper.GetOverlap(X, Length, dvpRelativeX, dvpRelativeWidth).GreaterThan(0) &&
                    Y.GreaterOrEqualThan(dvpRelativeY) &&
                    Y.LessOrEqualThan(dvpRelativeY + dvpRelativeHeight);
            }
            else
            {
                return
                    BlockingHelper.GetOverlap(Y, Length, dvpRelativeY, dvpRelativeHeight).GreaterThan(0) &&
                    X.GreaterOrEqualThan(dvpRelativeX) &&
                    X.LessOrEqualThan(dvpRelativeX + dvpRelativeWidth);
            }
        }

        /// <summary>
        /// Returns the percentage of the given Fixture that is spanned by this divider.
        /// </summary>
        /// <param name="fixture">The Fixture against which to determine an overlap.</param>
        /// <returns>A percentage where 1 represents complete overlap and 0 represents none.</returns>
        internal Single GetAmountOfFixtureSpanned(PlanogramFixtureItem fixtureItem)
        {
            // If divider is vertical, just determine if its X position is within the fixture's bounds.
            if (Type == PlanogramBlockingDividerType.Vertical)
            {
                var fixtureX = fixtureItem.PercentageX;
                if (X >= fixtureX && X <= (fixtureX + fixtureItem.PercentageWidth))
                {
                    return 1f;
                }
                return 0f;
            }

            // Calculate the percentage of the divider that overlaps the fixture's width.
            return BlockingHelper.GetOverlap(fixtureItem.PercentageX, fixtureItem.PercentageWidth, X, Length);
        }

        /// <summary>
        /// Returns the percentage by which the given divider overlaps with this divider, 
        /// in the dimension of the divider's orientation.
        /// </summary>
        /// <param name="divider">The divider against which the overlap is to be evaluated.</param>
        /// <returns>A value between 0 and 1 representing the amount that this divider overlaps with that given.</returns>
        public Single GetOverlap(PlanogramBlockingDivider divider)
        {
            // A null divider represents an edge, so just return 1.
            if (divider == null) return 1f;
            return BlockingHelper.GetOverlap(this, divider);
        }

        /// <summary>
        /// Gets the largest distance to a neighbouring divider of the same level with the same starting position.
        /// </summary>
        /// <returns>The largest absolute distance.</returns>
        public Single GetMaxDistanceToNeighbour()
        {
            return GetDistanceToNeighbour(true);
        }

        /// <summary>
        /// Gets the smallest distance to a neighbouring divider of the same level with the same starting position.
        /// </summary>
        /// <returns>The smallest absolute distance.</returns>
        public Single GetMinDistanceToNeighbour()
        {
            return GetDistanceToNeighbour(false);
        }

        /// <summary>
        /// Gets the distance to the nearest or furthest neighbour.
        /// </summary>
        /// <param name="maximum">True for maximum, false for minimum.</param>
        /// <returns>Distance to neighbour.</returns>
        private Single GetDistanceToNeighbour(Boolean maximum)
        {
            Single positiveDistance;
            Single negativeDistance;

            if (Type == PlanogramBlockingDividerType.Horizontal)
            {
                positiveDistance = GetTopNeighbourY() - Y;
                negativeDistance = Y - GetBottomNeighbourY();
            }
            else
            {
                positiveDistance = GetRightNeighbourX() - X;
                negativeDistance = X-GetLeftNeighbourX();
            }

            if (maximum)
            {
                return positiveDistance > negativeDistance ? positiveDistance : negativeDistance;
            }
            return positiveDistance > negativeDistance ? negativeDistance : positiveDistance;
        }

        /// <summary>
        /// Gets the Y position of the divider immediately above this one (or 1 if there is no 
        /// such divider).
        /// </summary>
        public Single GetTopNeighbourY()
        {
            if (Type == PlanogramBlockingDividerType.Vertical)
                throw new InvalidOperationException("Top neighbour cannot be determined on vertical dividers");
            
            PlanogramBlockingDivider min, max;
            BlockingHelper.GetParallelDividers(this, Parent, out min, out max);

            if (min == null && max == null) return 1f;
            if (min == null)
            {
                return max.Y > Y ? max.Y : 1f;
            }
            else if (max == null)
            {
                return min.Y > Y ? min.Y : 1f;
            }
            else
            {
                return max.Y > Y ? max.Y : min.Y;
            }
        }

        /// <summary>
        /// Gets the Y position of the divider immediately below this one (or 0 if there is
        /// no such divider.
        /// </summary>
        public Single GetBottomNeighbourY()
        {
            if (Type == PlanogramBlockingDividerType.Vertical)
                throw new InvalidOperationException("Bottom neighbour cannot be determined on vertical dividers");

            PlanogramBlockingDivider min, max;
            BlockingHelper.GetParallelDividers(this, Parent, out min, out max);

            if (min == null && max == null) return 0f;
            if (min == null)
            {
                return max.Y < Y ? max.Y : 0f;
            }
            else if (max == null)
            {
                return min.Y < Y ? min.Y : 0f;
            }
            else
            {
                return max.Y < Y ? max.Y : min.Y;
            }
        }

        /// <summary>
        /// Gets the X position of the divider immediately to the right of this one (or 1 if there is
        /// no such divider.
        /// </summary>
        public Single GetRightNeighbourX()
        {
            if (Type == PlanogramBlockingDividerType.Horizontal)
                throw new InvalidOperationException("Right neighbour cannot be determined on horizontal dividers");

            PlanogramBlockingDivider min, max;
            BlockingHelper.GetParallelDividers(this, Parent, out min, out max);

            if (min == null && max == null) return 1f;
            if (min == null)
            {
                return max.X > X ? max.X : 1f;
            }
            else if (max == null)
            {
                return min.X > X ? min.X : 1f;
            }
            else
            {
                return max.X > X ? max.X : min.X;
            }
        }

        /// <summary>
        /// Gets the X position of the divider immediately to the left of this one (or 0 if there is
        /// no such divider.
        /// </summary>
        public Single GetLeftNeighbourX()
        {
            if (Type == PlanogramBlockingDividerType.Horizontal)
                throw new InvalidOperationException("Left neighbour cannot be determined on horizontal dividers");

            PlanogramBlockingDivider min, max;
            BlockingHelper.GetParallelDividers(this, Parent, out min, out max);

            if (min == null && max == null) return 0f;
            if (min == null)
            {
                return max.X < X ? max.X : 0f;
            }
            else if (max == null)
            {
                return min.X < X ? min.X : 0f;
            }
            else
            {
                return max.X < X ? max.X : min.X;
            }
        }

        /// <summary>
        /// Gets the parent divider that is parallel to this one, either above (right) or below (left).
        /// </summary>
        /// <param name="getTop">True will get the parent above or to the right.</param>
        /// <returns>The parent divider.</returns>
        /// <exception cref="InvalidOperationException">Thrown if Parent is null.</exception>
        public PlanogramBlockingDivider GetParallelParentDivider(Boolean getTop)
        {
            if (Parent == null) throw new InvalidOperationException("Parent cannot be null");
            // Root level and level below both border the edge of the blocking space.
            if (Level <= RootLevelNumber + 1) return null;

            Boolean isHorizontal = this.Type == PlanogramBlockingDividerType.Horizontal;

            List<PlanogramBlockingDivider> possibleCeilingDividers = new List<PlanogramBlockingDivider>();
            foreach (PlanogramBlockingDivider divider in Parent.Dividers)
            {
                if (divider.Type != this.Type) continue; // We're only after parallel parents.
                if (divider.Level >= this.Level) continue; // We're only interested in dividers with a lower level number than this one (potential parents).
                Boolean isAboveOrBelow =
                    getTop ?
                    (isHorizontal ? divider.Y : divider.X) > (isHorizontal ? this.Y : this.X) : // Above or right
                    (isHorizontal ? divider.Y : divider.X) < (isHorizontal ? this.Y : this.X); // Below or left
                if (!isAboveOrBelow) continue;
                Boolean isOverlapping =
                    isHorizontal ?
                    Math.Max(divider.X, this.X).LessThan(Math.Min(divider.X + divider.Length, this.X + this.Length)) :
                    Math.Max(divider.Y, this.Y).LessThan(Math.Min(divider.Y + divider.Length, this.Y + this.Length));
                if (!isOverlapping) continue;
                possibleCeilingDividers.Add(divider);
            }

            if (!possibleCeilingDividers.Any()) return null;

            // Return the closest divider.
            var closestPos = 
                getTop ?
                possibleCeilingDividers.Min(d => isHorizontal ? d.Y : d.X) :
                possibleCeilingDividers.Max(d => isHorizontal ? d.Y : d.X);
            return possibleCeilingDividers.First(d => isHorizontal ? d.Y == closestPos : d.X == closestPos);
        }


        #endregion

        /// <summary>
        /// Notifies object graph siblings that this divider has moved.
        /// </summary>
        private void NotifyDividerMoved()
        {
            if (this.Parent == null) return;

            PlanogramBlocking parentBlocking = this.Parent;
            foreach (PlanogramBlockingLocation location in parentBlocking.Locations)
            {
                location.NotifyDividerMoved(this);
            }
        }

        #endregion

    }
}
