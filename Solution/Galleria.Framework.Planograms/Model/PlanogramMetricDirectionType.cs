﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26271 : A.Kuszyk
////  Created.
//// V8-26338 : A.Kuszyk
////  Added Helper class with friendly names.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.Planograms.Resources.Language;

//namespace Galleria.Framework.Planograms.Model
//{
//    /// <summary>
//    /// Indicates the optimization direction of a Planogram Metric.
//    /// </summary>
//    public enum MetricDirectionType
//    {
//        MaximumIncrease,
//        MaximumDecrease
//    }

//    /// <summary>
//    /// PlanogramMetricDirectionType Helper Class
//    /// </summary>
//    public static class PlanogramMetricDirectionTypeHelper
//    {
//        /// <summary>
//        /// Dictionary with enum value as key and friendly name as value.
//        /// </summary>
//        public static readonly Dictionary<MetricDirectionType, String> FriendlyNames =
//            new Dictionary<MetricDirectionType, String>()
//            {
//                {MetricDirectionType.MaximumDecrease, Message.Enum_PlanogramMetricDirectionType_MaximumDecrease},
//                {MetricDirectionType.MaximumIncrease, Message.Enum_PlanogramMetricDirectionType_MaximumIncrease}
//            };
//    }
//}
