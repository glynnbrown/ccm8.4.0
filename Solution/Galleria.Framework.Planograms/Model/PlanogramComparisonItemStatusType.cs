#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31862 : A.Silva
//  Created
// V8-32107 : A.Silva
//  Amended PlanogramComparisonItemStatusType members and added FriendlyDescriptions to the Enum Helper.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     The different values for the Comparison Status that a <c>planogram item</c> may have in a compared planogram.
    /// </summary>
    public enum PlanogramComparisonItemStatusType
    {
        /// <summary>
        ///     The Planogram Item has the same values as the Master Planogram.
        /// </summary>
        Unchanged = 0,
        /// <summary>
        ///     The Planogram Item is not found in the Master Planogram.
        /// </summary>
        New = 1,
        /// <summary>
        ///     The Planogram Item was not found in the Compared Planogram, but exists in the Master Planogram.
        /// </summary>
        NotFound = 2,
        /// <summary>
        ///     The Planogram Item has different values from the Master Planogram.
        /// </summary>
        Changed = 3,
    }

    /// <summary>
    ///     PlanogramComparisonItemStatusType Helper Class
    /// </summary>
    public static class PlanogramComparisonItemStatusTypeHelper
    {
        /// <summary>
        ///     Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramComparisonItemStatusType, String> FriendlyNames =
            new Dictionary<PlanogramComparisonItemStatusType, String>
            {
                {PlanogramComparisonItemStatusType.Unchanged, Message.Enum_PlanogramComparisonItemStatusType_Unchanged},
                {PlanogramComparisonItemStatusType.New, Message.Enum_PlanogramComparisonItemStatusType_New},
                {PlanogramComparisonItemStatusType.NotFound, Message.Enum_PlanogramComparisonItemStatusType_NotFound},
                {PlanogramComparisonItemStatusType.Changed, Message.Enum_PlanogramComparisonItemStatusType_Changed},
            };

        /// <summary>
        ///     Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramItemComparisonStatusType, String> FriendlyDescriptions =
            new Dictionary<PlanogramItemComparisonStatusType, String>
            {
                {PlanogramItemComparisonStatusType.Unchanged, Message.Enum_PlanogramComparisonItemStatusType_Unchanged_Description},
                {PlanogramItemComparisonStatusType.New, Message.Enum_PlanogramComparisonItemStatusType_New_Description},
                {PlanogramItemComparisonStatusType.NotFound, Message.Enum_PlanogramComparisonItemStatusType_NotFound_Description},
                {PlanogramItemComparisonStatusType.Changed, Message.Enum_PlanogramComparisonItemStatusType_Changed_Description},
            };
    }
}