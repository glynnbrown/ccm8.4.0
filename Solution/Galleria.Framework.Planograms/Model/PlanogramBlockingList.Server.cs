﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingList
    {
        #region Constructor
        private PlanogramBlockingList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all annotations for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            if (criteria.ParentId != null && !Object.Equals(0, criteria.ParentId))
            {
                this.RaiseListChangedEvents = false;
                IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramBlockingDal dal = dalContext.GetDal<IPlanogramBlockingDal>())
                    {
                        IEnumerable<PlanogramBlockingDto> dtoList = dal.FetchByPlanogramId(criteria.ParentId);
                        foreach (PlanogramBlockingDto dto in dtoList)
                        {
                            this.Add(PlanogramBlocking.Fetch(dalContext, dto));
                        }
                    }
                }
                this.RaiseListChangedEvents = true;
            }
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
