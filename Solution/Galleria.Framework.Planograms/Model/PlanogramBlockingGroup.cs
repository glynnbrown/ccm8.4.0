﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-27392 : L.Ineson
//  CanCompromiseSequenceProperty now defaults to false
// V8-27439 : A.Kuszyk
//  Added Blocking re-calculation methods.
// V8-27476 : L.Luong
//      Added BlockPlacementX and BlockPlacementY 
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
#endregion

#region Version History : CCM 802
// V8-28989 : A.Kuszyk
//  Added TotalSpacePercentage to Create method.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28995 : A.Kuszyk
//  Removed BlockPlacementTypeX and BlockPlacementTypeY and replaced with BlockPlacementPrimaryType, 
//  BlockPlacementSecondaryType and BlockPlacementTertiaryType.
// V8-29015 : A.Silva
//      Refactored GetPlacementType to be an instance method of PlanogramBlockingGroup.
// V8-29198 : A.Silva
//      Refactored EnumerateSubComponentsInSequence to be an instance method of PlanogramBlockingGroup.
#endregion

#region Version History : CCM 803
// V8-29512 : A.Kuszyk
//	Added check for invalid values in new from source constructor.
// V8-29137 : A.Kuszyk
//  Updated get intersecting merch groups method to use DesignViewPosition.OverlapsWith().
#endregion

#region Version History : CCM810
// V8-29889 : A.Kuszyk
//  Updated EnumerateSubComponentsInSequence to only pass reverse booleans if the method has reversed direction directly.
#endregion

#region Version History : CCM 820
// V8-30713 : A.Kuszyk
//  Protected against a multi-thread related InvalidOperationException in GetBlockingLocations.
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
// V8-31004 : D.Pleasance
//	Added GetPlacementType overload for AxisType param, also added EnumerateSubComponentPositionsInSequence to enumerate positions in sequence on a given component.
// V8-31162 : A.Kuszyk
//  Added business rule for sequence group mapping.
// V8-30737 : M.Shelley
//  Added Meta performance fields P1-P20 and MetaP1Percentage-MetaP20Percentage
// V8-31393 : L.Ineson
//  Amended how notifications are passed around
// V8-31482 : M.Shelley
//  Fixed incorrect Original, Performance and Final percent allocated values
// V8-31495 : D.Pleasance
//  Renamed GetPlanogramProducts() to GetRangedPlanogramProducts(). As this now also filters by products being ranged.
#endregion

#region Version History : CCM830
// V8-31804 : A.Kuszyk
//  Added GetIntersectingLocationsByMerchGroup.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32539 : A.Kuszyk
//  Implemented IPlanogramSequencableItem and made EnumerateSubComponentPositionsInSequence and EnumerateSequencableItemsInSequence
//  generic.
// V8-32830 : A.Kuszyk
//  Added equality comparer to ensure sequencing coordinates are only compared to 1dp.
// V8-32870 : A.Kuszyk
//  Amended GetLocationsAndGroups to create a tuple for every blocking location.
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
// CCM-18440 : A.Kuszyk
//  Re-factored location/merchandising group helper methods.
// CCM-18321 : L.Bailey
//  Multi-select Blocks & Dividers and apply certain settings to all selected
// CCM-18674 : M.Pettit
//  Added BlockSpaceLimitedPercentage helper property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Enums;
using Csla.Rules;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a Planogram Blocking Group object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramBlockingGroup : ModelObject<PlanogramBlockingGroup>, IPlanogramBlockingGroup, IPlanogramSequencableItem
    {
        #region Fields

        //Remember whether the user changed the limited space % or the block space %. 
        //We need to know this when updating multiple items at once
        private Boolean _blockPercentageIsChanged = false;

        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get
            {
                if (base.Parent == null) return null;
                return ((PlanogramBlockingGroupList)base.Parent).Parent;
            }
        }
        #endregion

        #region Properties

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<string> NameProperty =
            RegisterModelProperty<string>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Colour

        /// <summary>
        /// Colour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<int> ColourProperty =
            RegisterModelProperty<int>(c => c.Colour);

        /// <summary>
        /// Gets/Sets the Colour value
        /// </summary>
        public Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
            set { SetProperty<Int32>(ColourProperty, value); }
        }

        #endregion

        #region FillPatternType

        /// <summary>
        /// FillPatternType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingFillPatternType> FillPatternTypeProperty =
            RegisterModelProperty<PlanogramBlockingFillPatternType>(c => c.FillPatternType);

        /// <summary>
        /// Gets/Sets the FillPatternType value
        /// </summary>
        public PlanogramBlockingFillPatternType FillPatternType
        {
            get { return GetProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty); }
            set { SetProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, value); }
        }

        #endregion

        #region CanCompromiseSequence

        /// <summary>
        /// CanCompromiseSequence property definition
        /// </summary>
        public static readonly ModelPropertyInfo<bool> CanCompromiseSequenceProperty =
            RegisterModelProperty<bool>(c => c.CanCompromiseSequence);

        /// <summary>
        /// Gets/Sets the CanCompromiseSequence value
        /// </summary>
        public Boolean CanCompromiseSequence
        {
            get { return GetProperty<Boolean>(CanCompromiseSequenceProperty); }
            set { SetProperty<Boolean>(CanCompromiseSequenceProperty, value); }
        }

        #endregion

        #region IsRestrictedByComponentType

        /// <summary>
        /// IsRestrictedByComponentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<bool> IsRestrictedByComponentTypeProperty =
            RegisterModelProperty<bool>(c => c.IsRestrictedByComponentType);

        /// <summary>
        /// Gets/Sets the IsRestrictedByComponentType value
        /// </summary>
        public Boolean IsRestrictedByComponentType
        {
            get { return GetProperty<Boolean>(IsRestrictedByComponentTypeProperty); }
            set { SetProperty<Boolean>(IsRestrictedByComponentTypeProperty, value); }
        }

        #endregion

        #region CanOptimisePercentage
        public Single? CanOptimisePercentage { get; set; }
        #endregion

        #region CanMerge

        /// <summary>
        /// CanMerge property definition
        /// </summary>
        public static readonly ModelPropertyInfo<bool> CanMergeProperty =
            RegisterModelProperty<bool>(c => c.CanMerge);

        /// <summary>
        /// Gets/Sets the CanMerge value
        /// </summary>
        public Boolean CanMerge
        {
            get { return GetProperty<Boolean>(CanMergeProperty); }
            set { SetProperty<Boolean>(CanMergeProperty, value); }
        }

        #endregion

        #region TotalSpacePercentage

        /// <summary>
        /// TotalSpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> TotalSpacePercentageProperty =
            RegisterModelProperty<float>(c => c.TotalSpacePercentage);

        /// <summary>
        /// Returns the total percentage of space allocated to this group.
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single TotalSpacePercentage
        {
            get { return GetProperty<Single>(TotalSpacePercentageProperty); }
            private set { SetProperty<Single?>(TotalSpacePercentageProperty, value); }
        }

        /// <summary>
        /// Updates the value of the total space percentage property.
        /// </summary>
        /// <returns></returns>
        private Single CalculateTotalSpacePercentage()
        {
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null) return 0;

            return parentBlocking.Locations
                .Where(l => Equals(l.PlanogramBlockingGroupId, this.Id))
                .Sum(l => l.SpacePercentage);
        }

        #endregion

        #region IsLimited

        /// <summary>
        /// IsLimited property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLimitedProperty =
            RegisterModelProperty<Boolean>(c => c.IsLimited);

        /// <summary>
        /// Gets/Sets the IsLimited value
        /// </summary>
        public Boolean IsLimited
        {
            get { return GetProperty<Boolean>(IsLimitedProperty); }
            set { SetProperty<Boolean>(IsLimitedProperty, value); }
        }

        #endregion

        #region LimitedPercentage

        /// <summary>
        /// LimitedPercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LimitedPercentageProperty =
            RegisterModelProperty<Single>(c => c.LimitedPercentage);

        /// <summary>
        /// Gets/Sets the LimitedPercentage value - this is the prcentage change allowed from the specified
        /// planogram percentage during optimisation
        /// </summary>
        public Single LimitedPercentage
        {
            get
            {
                return GetProperty<Single>(LimitedPercentageProperty);
            }
            set
            {
                SetProperty<Single>(LimitedPercentageProperty, value);
                //ensure the block space percent is also set here
                SetProperty<Single>(BlockSpaceLimitedPercentageProperty, value / GetProperty<Single>(TotalSpacePercentageProperty));
            }
        }

        #endregion

        #region BlockSpaceLimitedPercentage

        /// <summary>
        /// BlockSpaceLimitedPercentage property definition - not saved down, but used as a conversion method when setting LimitedSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BlockSpaceLimitedPercentageProperty =
            RegisterModelProperty<Single>(c => c.BlockSpaceLimitedPercentage);

        /// <summary>
        /// Gets/Sets the LimitedPercentage value - this is the prcentage change allowed from the specified
        /// planogram percentage during optimisation
        /// This value is not saved but converted to PlanogramlimitedPercentage, which is saved
        /// </summary>
        public Single BlockSpaceLimitedPercentage
        {
            get
            {
                return
                    GetProperty<Single>(TotalSpacePercentageProperty).EqualTo(0) ?
                    0f :
                    GetProperty<Single>(LimitedPercentageProperty) / GetProperty<Single>(TotalSpacePercentageProperty);
            }
            set
            {

                _blockPercentageIsChanged = true;
                //set the limited percentage to the calculated value - this is the value stored for the model
                SetProperty<Single>(LimitedPercentageProperty, (value * GetProperty<Single>(TotalSpacePercentageProperty)));
                //set the BlockSpaceLimitedPercentage value. We never read from it -it is only set as a workaround to force the property change to fire correctly
                SetProperty<Single>(BlockSpaceLimitedPercentageProperty, value);
                _blockPercentageIsChanged = false;
            }
        }

        #endregion

        #region BlockPlacementPrimaryType

        /// <summary>
        /// BlockPlacementPrimaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementPrimaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementPrimaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementPrimaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementPrimaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty, value); }
        }

        #endregion

        #region BlockPlacementSecondaryType

        /// <summary>
        /// BlockPlacementSecondaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementSecondaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementSecondaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementSecondaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementSecondaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty, value); }
        }

        #endregion

        #region BlockPlacementTertiaryType

        /// <summary>
        /// BlockPlacementTertiaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementTertiaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementTertiaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementTertiaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementTertiaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty, value); }
        }

        #endregion

        #region MetaProperties

        #region MetaCountOfProducts property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProducts);
        /// <summary>
        /// Number of products in associated sequence group.
        /// </summary>
        public Int32? MetaCountOfProducts
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductsProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductsProperty, value); }
        }

        #endregion

        #region MetaCountOfProductRecommendedOnPlanogram

        /// <summary>
        /// MetaCountOfProductRecommendedOnPlanogram property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductRecommendedOnPlanogramProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductRecommendedOnPlanogram);

        /// <summary>
        /// Gets/Sets the MetaCountOfProductRecommendedOnPlanogram value
        /// </summary>
        public Int32? MetaCountOfProductRecommendedOnPlanogram
        {
            get { return GetProperty<Int32?>(MetaCountOfProductRecommendedOnPlanogramProperty); }
            set { SetProperty<Int32?>(MetaCountOfProductRecommendedOnPlanogramProperty, value); }
        }

        #endregion

        #region MetaCountOfProductPlacedOnPlanogram

        /// <summary>
        /// MetaCountOfProductPlacedOnPlanogram property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductPlacedOnPlanogramProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductPlacedOnPlanogram);

        /// <summary>
        /// Gets/Sets the MetaCountOfProductPlacedOnPlanogram value
        /// </summary>
        public Int32? MetaCountOfProductPlacedOnPlanogram
        {
            get { return GetProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty); }
            set { SetProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty, value); }
        }

        #endregion

        #region MetaOriginalPercentAllocated
        /// <summary>
        /// MetaOriginalPercentAllocated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaOriginalPercentAllocatedProperty =
            RegisterModelProperty<Single?>(c => c.MetaOriginalPercentAllocated);

        /// <summary>
        /// Gets/Sets the MetaOriginalPercentAllocated value
        /// </summary>
        public Single? MetaOriginalPercentAllocated
        {
            get { return GetProperty<Single?>(MetaOriginalPercentAllocatedProperty); }
            set { SetProperty<Single?>(MetaOriginalPercentAllocatedProperty, value); }
        }

        #endregion

        #region MetaPerformancePercentAllocated
        /// <summary>
        /// MetaPerformancePercentAllocated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPerformancePercentAllocatedProperty =
            RegisterModelProperty<Single?>(c => c.MetaPerformancePercentAllocated);

        /// <summary>
        /// Gets/Sets the MetaPerformancePercentAllocated value
        /// </summary>
        public Single? MetaPerformancePercentAllocated
        {
            get { return GetProperty<Single?>(MetaPerformancePercentAllocatedProperty); }
            set { SetProperty<Single?>(MetaPerformancePercentAllocatedProperty, value); }
        }

        #endregion

        #region MetaFinalPercentAllocated
        /// <summary>
        /// MetaFinalPercentAllocated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaFinalPercentAllocatedProperty =
            RegisterModelProperty<Single?>(c => c.MetaFinalPercentAllocated);

        /// <summary>
        /// Gets/Sets the MetaFinalPercentAllocated value
        /// </summary>
        public Single? MetaFinalPercentAllocated
        {
            get { return GetProperty<Single?>(MetaFinalPercentAllocatedProperty); }
            set { SetProperty<Single?>(MetaFinalPercentAllocatedProperty, value); }
        }

        #endregion

        #region MetaLinearProductPlacementPercent
        /// <summary>
        /// MetaLinearProductPlacementPercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaLinearProductPlacementPercentProperty =
            RegisterModelProperty<Single?>(c => c.MetaLinearProductPlacementPercent);

        /// <summary>
        /// Gets/Sets the MetaLinearProductPlacementPercent value
        /// </summary>
        public Single? MetaLinearProductPlacementPercent
        {
            get { return GetProperty<Single?>(MetaLinearProductPlacementPercentProperty); }
            set { SetProperty<Single?>(MetaLinearProductPlacementPercentProperty, value); }
        }

        #endregion

        #region MetaAreaProductPlacementPercent
        /// <summary>
        /// MetaAreaProductPlacementPercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAreaProductPlacementPercentProperty =
            RegisterModelProperty<Single?>(c => c.MetaAreaProductPlacementPercent);

        /// <summary>
        /// Gets/Sets the MetaAreaProductPlacementPercent value
        /// </summary>
        public Single? MetaAreaProductPlacementPercent
        {
            get { return GetProperty<Single?>(MetaAreaProductPlacementPercentProperty); }
            set { SetProperty<Single?>(MetaAreaProductPlacementPercentProperty, value); }
        }

        #endregion

        #region MetaVolumetricProductPlacementPercent
        /// <summary>
        /// MetaVolumetricProductPlacementPercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaVolumetricProductPlacementPercentProperty =
            RegisterModelProperty<Single?>(c => c.MetaVolumetricProductPlacementPercent);

        /// <summary>
        /// Gets/Sets the MetaVolumeticProductPlacementPercent value
        /// </summary>
        public Single? MetaVolumetricProductPlacementPercent
        {
            get { return GetProperty<Single?>(MetaVolumetricProductPlacementPercentProperty); }
            set { SetProperty<Single?>(MetaVolumetricProductPlacementPercentProperty, value); }
        }

        #endregion

        #region MetaP1 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP1Property =
            RegisterModelProperty<Single?>(c => c.MetaP1);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP1
        {
            get { return this.GetProperty<Single?>(MetaP1Property); }
            set { this.SetProperty<Single?>(MetaP1Property, value); }
        }

        #endregion

        #region MetaP2 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP2Property =
            RegisterModelProperty<Single?>(c => c.MetaP2);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP2
        {
            get { return this.GetProperty<Single?>(MetaP2Property); }
            set { this.SetProperty<Single?>(MetaP2Property, value); }
        }

        #endregion

        #region MetaP3 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP3Property =
            RegisterModelProperty<Single?>(c => c.MetaP3);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP3
        {
            get { return this.GetProperty<Single?>(MetaP3Property); }
            set { this.SetProperty<Single?>(MetaP3Property, value); }
        }

        #endregion

        #region MetaP4 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP4Property =
            RegisterModelProperty<Single?>(c => c.MetaP4);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP4
        {
            get { return this.GetProperty<Single?>(MetaP4Property); }
            set { this.SetProperty<Single?>(MetaP4Property, value); }
        }

        #endregion

        #region MetaP5 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP5Property =
            RegisterModelProperty<Single?>(c => c.MetaP5);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP5
        {
            get { return this.GetProperty<Single?>(MetaP5Property); }
            set { this.SetProperty<Single?>(MetaP5Property, value); }
        }

        #endregion

        #region MetaP6 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP6Property =
            RegisterModelProperty<Single?>(c => c.MetaP6);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP6
        {
            get { return this.GetProperty<Single?>(MetaP6Property); }
            set { this.SetProperty<Single?>(MetaP6Property, value); }
        }

        #endregion

        #region MetaP7 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP7Property =
            RegisterModelProperty<Single?>(c => c.MetaP7);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP7
        {
            get { return this.GetProperty<Single?>(MetaP7Property); }
            set { this.SetProperty<Single?>(MetaP7Property, value); }
        }

        #endregion

        #region MetaP8 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP8Property =
            RegisterModelProperty<Single?>(c => c.MetaP8);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP8
        {
            get { return this.GetProperty<Single?>(MetaP8Property); }
            set { this.SetProperty<Single?>(MetaP8Property, value); }
        }

        #endregion

        #region MetaP9 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP9Property =
            RegisterModelProperty<Single?>(c => c.MetaP9);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP9
        {
            get { return this.GetProperty<Single?>(MetaP9Property); }
            set { this.SetProperty<Single?>(MetaP9Property, value); }
        }

        #endregion

        #region MetaP10 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP10Property =
            RegisterModelProperty<Single?>(c => c.MetaP10);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP10
        {
            get { return this.GetProperty<Single?>(MetaP10Property); }
            set { this.SetProperty<Single?>(MetaP10Property, value); }
        }

        #endregion

        #region MetaP11 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP11Property =
            RegisterModelProperty<Single?>(c => c.MetaP11);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP11
        {
            get { return this.GetProperty<Single?>(MetaP11Property); }
            set { this.SetProperty<Single?>(MetaP11Property, value); }
        }

        #endregion

        #region MetaP12 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP12Property =
            RegisterModelProperty<Single?>(c => c.MetaP12);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP12
        {
            get { return this.GetProperty<Single?>(MetaP12Property); }
            set { this.SetProperty<Single?>(MetaP12Property, value); }
        }

        #endregion

        #region MetaP13 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP13Property =
            RegisterModelProperty<Single?>(c => c.MetaP13);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP13
        {
            get { return this.GetProperty<Single?>(MetaP13Property); }
            set { this.SetProperty<Single?>(MetaP13Property, value); }
        }

        #endregion

        #region MetaP14 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP14Property =
            RegisterModelProperty<Single?>(c => c.MetaP14);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP14
        {
            get { return this.GetProperty<Single?>(MetaP14Property); }
            set { this.SetProperty<Single?>(MetaP14Property, value); }
        }

        #endregion

        #region MetaP15 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP15Property =
            RegisterModelProperty<Single?>(c => c.MetaP15);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP15
        {
            get { return this.GetProperty<Single?>(MetaP15Property); }
            set { this.SetProperty<Single?>(MetaP15Property, value); }
        }

        #endregion

        #region MetaP16 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP16Property =
            RegisterModelProperty<Single?>(c => c.MetaP16);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP16
        {
            get { return this.GetProperty<Single?>(MetaP16Property); }
            set { this.SetProperty<Single?>(MetaP16Property, value); }
        }

        #endregion

        #region MetaP17 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP17Property =
            RegisterModelProperty<Single?>(c => c.MetaP17);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP17
        {
            get { return this.GetProperty<Single?>(MetaP17Property); }
            set { this.SetProperty<Single?>(MetaP17Property, value); }
        }

        #endregion

        #region MetaP18 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP18Property =
            RegisterModelProperty<Single?>(c => c.MetaP18);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP18
        {
            get { return this.GetProperty<Single?>(MetaP18Property); }
            set { this.SetProperty<Single?>(MetaP18Property, value); }
        }

        #endregion

        #region MetaP19 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP19Property =
            RegisterModelProperty<Single?>(c => c.MetaP19);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP19
        {
            get { return this.GetProperty<Single?>(MetaP19Property); }
            set { this.SetProperty<Single?>(MetaP19Property, value); }
        }

        #endregion

        #region MetaP20 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP20Property =
            RegisterModelProperty<Single?>(c => c.MetaP20);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP20
        {
            get { return this.GetProperty<Single?>(MetaP20Property); }
            set { this.SetProperty<Single?>(MetaP20Property, value); }
        }

        #endregion

        #region MetaP1Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP1PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP1Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP1Percentage
        {
            get { return this.GetProperty<Single?>(MetaP1PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP1PercentageProperty, value); }
        }

        #endregion

        #region MetaP2Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP2PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP2Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP2Percentage
        {
            get { return this.GetProperty<Single?>(MetaP2PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP2PercentageProperty, value); }
        }

        #endregion

        #region MetaP3Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP3PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP3Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP3Percentage
        {
            get { return this.GetProperty<Single?>(MetaP3PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP3PercentageProperty, value); }
        }

        #endregion

        #region MetaP4Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP4PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP4Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP4Percentage
        {
            get { return this.GetProperty<Single?>(MetaP4PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP4PercentageProperty, value); }
        }

        #endregion

        #region MetaP5Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP5PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP5Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP5Percentage
        {
            get { return this.GetProperty<Single?>(MetaP5PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP5PercentageProperty, value); }
        }

        #endregion

        #region MetaP6Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP6PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP6Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP6Percentage
        {
            get { return this.GetProperty<Single?>(MetaP6PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP6PercentageProperty, value); }
        }

        #endregion

        #region MetaP7Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP7PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP7Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP7Percentage
        {
            get { return this.GetProperty<Single?>(MetaP7PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP7PercentageProperty, value); }
        }

        #endregion

        #region MetaP8Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP8PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP8Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP8Percentage
        {
            get { return this.GetProperty<Single?>(MetaP8PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP8PercentageProperty, value); }
        }

        #endregion

        #region MetaP9Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP9PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP9Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP9Percentage
        {
            get { return this.GetProperty<Single?>(MetaP9PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP9PercentageProperty, value); }
        }

        #endregion

        #region MetaP10Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP10PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP10Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP10Percentage
        {
            get { return this.GetProperty<Single?>(MetaP10PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP10PercentageProperty, value); }
        }

        #endregion

        #region MetaP11Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP11PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP11Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP11Percentage
        {
            get { return this.GetProperty<Single?>(MetaP11PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP11PercentageProperty, value); }
        }

        #endregion

        #region MetaP12Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP12PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP12Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP12Percentage
        {
            get { return this.GetProperty<Single?>(MetaP12PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP12PercentageProperty, value); }
        }

        #endregion

        #region MetaP13Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP13PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP13Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP13Percentage
        {
            get { return this.GetProperty<Single?>(MetaP13PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP13PercentageProperty, value); }
        }

        #endregion

        #region MetaP14Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP14PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP14Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP14Percentage
        {
            get { return this.GetProperty<Single?>(MetaP14PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP14PercentageProperty, value); }
        }

        #endregion

        #region MetaP15Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP15PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP15Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP15Percentage
        {
            get { return this.GetProperty<Single?>(MetaP15PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP15PercentageProperty, value); }
        }

        #endregion

        #region MetaP16Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP16PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP16Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP16Percentage
        {
            get { return this.GetProperty<Single?>(MetaP16PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP16PercentageProperty, value); }
        }

        #endregion

        #region MetaP17Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP17PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP17Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP17Percentage
        {
            get { return this.GetProperty<Single?>(MetaP17PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP17PercentageProperty, value); }
        }

        #endregion

        #region MetaP18Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP18PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP18Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP18Percentage
        {
            get { return this.GetProperty<Single?>(MetaP18PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP18PercentageProperty, value); }
        }

        #endregion

        #region MetaP19Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP19PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP19Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP19Percentage
        {
            get { return this.GetProperty<Single?>(MetaP19PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP19PercentageProperty, value); }
        }

        #endregion

        #region MetaP20Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP20PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP20Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP20Percentage
        {
            get { return this.GetProperty<Single?>(MetaP20PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP20PercentageProperty, value); }
        }

        #endregion


        #endregion

        #region Helper Properties

        public Boolean BlockPercentageIsChanged
        {
            get { return _blockPercentageIsChanged; }
        }
        #endregion

        #endregion

        #region Nested Types

        /// <summary>
        /// A simple class that represents the overlap between a <see cref="PlanogramMerchandisingGroup"/> and 
        /// a <see cref="PlanogramBlockingLocation"/>.
        /// </summary>
        private sealed class LocationMerchandisingGroupOverlap
        {
            #region Properties
            /// <summary>
            /// The <see cref="PlanogramBlockingLocation"/> that the <see cref="MerchandisingGroup"/> overlaps
            /// with.
            /// </summary>
            public PlanogramBlockingLocation Location { get; }

            /// <summary>
            /// The <see cref="PlanogramMerchandisingGroup"/> that <see cref="Location"/> overlaps with.
            /// </summary>
            public PlanogramMerchandisingGroup MerchandisingGroup { get; }

            /// <summary>
            /// The percentage amount of <see cref="MerchandisingGroup"/> that overlaps with <see cref="Location"/>.
            /// </summary>
            public Double Overlap { get; }
            #endregion

            #region Constructors
            /// <summary>
            /// Instantiates a new instance for the given location, merchandising group and overlap.
            /// </summary>
            /// <param name="location"></param>
            /// <param name="merchandisingGroup"></param>
            /// <param name="overlap"></param>
            private LocationMerchandisingGroupOverlap(
                PlanogramBlockingLocation location,
                PlanogramMerchandisingGroup merchandisingGroup,
                Double overlap)
            {
                Location = location;
                MerchandisingGroup = merchandisingGroup;
                Overlap = overlap;
            }
            #endregion

            #region Methods
            /// <summary>
            /// Gets the <see cref="PlanogramMerchandisingGroup"/>s from <paramref name="merchandisingGroupsAndDvps"/>
            /// that intersect with this Group's <see cref="PlanogramBlockingLocation"/>s, arranging them in a simple 
            /// list of <see cref="Tuple"/>s containing the Location, Merchandising Group and the amount that the 
            /// Merchandising Group overlaps with the Location.
            /// </summary>
            /// <param name="merchandisingGroupsAndDvps"></param>
            /// <param name="overlapThreshold"></param>
            /// <param name="planogramBlockingWidth"></param>
            /// <param name="planogramBlockingHeight"></param>
            /// <returns></returns>
            public static IEnumerable<LocationMerchandisingGroupOverlap> GetLocationMerchandisingGroupOverlaps(
                Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
                IEnumerable<PlanogramBlockingLocation> blockingLocations,
                Single overlapThreshold,
                Single planogramBlockingWidth,
                Single planogramBlockingHeight)
            {
                List<LocationMerchandisingGroupOverlap> locationsAndGroups = new List<LocationMerchandisingGroupOverlap>();

                foreach (PlanogramBlockingLocation blockingLocation in blockingLocations)
                {
                    foreach (KeyValuePair<PlanogramMerchandisingGroup, DesignViewPosition> kvp in merchandisingGroupsAndDvps)
                    {
                        DesignViewPosition merchGroupDvp = kvp.Value;
                        PlanogramMerchandisingGroup merchandisingGroup = kvp.Key;

                        // Compare the merch group design view position to the blocking location for an intersection.
                        Double overlap;

                        // If is a self and the MerchandisingType is Stack then check a FullOverlapsBy 
                        if (merchandisingGroup.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack && !merchandisingGroup.SubComponentPlacements.Any(c => c.SubComponent.HasFaceThickness()))
                        {
                            overlap = merchGroupDvp
                                .FullOverlapsBy(blockingLocation, planogramBlockingWidth, planogramBlockingHeight);
                        }
                        // otherwise do it as before and check for PartialOverlapsBy 
                        else
                        {
                            overlap = merchGroupDvp
                                .PartialOverlapsBy(blockingLocation, planogramBlockingWidth, planogramBlockingHeight);
                        }

                        if (overlap.GreaterOrEqualThan(overlapThreshold))
                        {
                            locationsAndGroups.Add(new LocationMerchandisingGroupOverlap(
                                blockingLocation, merchandisingGroup, overlap));
                        }
                    }
                }
                return locationsAndGroups;
            }
            #endregion
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));

            // Mark the block placement types as inter-dependant.
            BusinessRules.AddRule(new Dependency(
                BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new Dependency(
                BlockPlacementSecondaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new Dependency(
                BlockPlacementTertiaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty));

            // Ensure there are no duplicates for each dimension.
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementSecondaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementTertiaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty));

            BusinessRules.AddRule(new MaxPercentageValue<Single>(LimitedPercentageProperty, 1));
            BusinessRules.AddRule(new MinPercentageValue<Single>(LimitedPercentageProperty, 0f));

            BusinessRule blockSpaceRule = new BlockSpacePercentageLimitRule(BlockSpaceLimitedPercentageProperty, Message.BlockSpacePercentageLimitRule_Message);
            BusinessRules.AddRule(blockSpaceRule);

            // Ensure that this group is mapped to a sequence group, if there are sequence 
            // groups in the plan.
            this.BusinessRules.AddRule(new Lambda(
                ColourProperty,
                (RuleContext ruleContext) =>
                {
                    PlanogramBlockingGroup group = ruleContext.Target as PlanogramBlockingGroup;
                    if (group == null) return;
                    if (group.Parent == null ||
                        group.Parent.Parent == null ||
                        group.Parent.Parent.Sequence == null ||
                        !group.Parent.Parent.Sequence.Groups.Any())
                    {
                        return;
                    }
                    if (group.Parent.Parent.Sequence.Groups.Select(g => g.Colour).Contains(group.Colour)) return;
                    ruleContext.AddErrorResult(Message.PlanogramBlockingGroup_MustBeMappedToSequence);
                }));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingGroup NewPlanogramBlockingGroup()
        {
            PlanogramBlockingGroup item = new PlanogramBlockingGroup();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramBlockingGroup NewPlanogramBlockingGroup(String name, Int32 colour)
        {
            PlanogramBlockingGroup item = new PlanogramBlockingGroup();
            item.Create(name, colour);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingGroup NewPlanogramBlockingGroup(IPlanogramBlockingGroup source, IResolveContext context)
        {
            PlanogramBlockingGroup item = new PlanogramBlockingGroup();
            item.Create(source, context);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, PlanogramBlockingFillPatternType.Solid);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, false);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, true);
            this.LoadProperty<Boolean>(CanMergeProperty, true);
            this.LoadProperty<Boolean>(IsLimitedProperty, false);
            this.LoadProperty<Single>(LimitedPercentageProperty, 0);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty, PlanogramBlockingGroupPlacementType.LeftToRight);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty, PlanogramBlockingGroupPlacementType.BottomToTop);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty, PlanogramBlockingGroupPlacementType.FrontToBack);

            SetMetaPerformance();

            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(String name, Int32 colour)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, PlanogramBlockingFillPatternType.Solid);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, true);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, true);
            this.LoadProperty<Boolean>(CanMergeProperty, true);
            this.LoadProperty<Boolean>(IsLimitedProperty, false);
            this.LoadProperty<Single>(LimitedPercentageProperty, 0);
            SetDefaultBlockPlacementPropertyValues();
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<Int32>(ColourProperty, colour);

            SetMetaPerformance();

            this.MarkAsChild();
            base.Create();
        }

        private void SetDefaultBlockPlacementPropertyValues()
        {
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty, PlanogramBlockingGroupPlacementType.LeftToRight);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty, PlanogramBlockingGroupPlacementType.BottomToTop);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty, PlanogramBlockingGroupPlacementType.FrontToBack);
        }

        private void SetMetaPerformance()
        {
            this.LoadProperty<Single?>(MetaP1Property, 3141.59F);
            this.LoadProperty<Single?>(MetaP2Property, 2718.82F);
            this.LoadProperty<Single?>(MetaP3Property, 1414.21F);
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramBlockingGroup source, IResolveContext context)
        {
            var newId = IdentityHelper.GetNextInt32();
            context.RegisterId<PlanogramBlockingGroup>(source.Id, newId);

            this.LoadProperty<Object>(IdProperty, newId);
            this.LoadProperty<String>(NameProperty, source.Name);
            this.LoadProperty<Int32>(ColourProperty, source.Colour);
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, source.FillPatternType);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, source.CanCompromiseSequence);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, source.IsRestrictedByComponentType);
            this.LoadProperty<Boolean>(CanMergeProperty, source.CanMerge);
            this.LoadProperty<Boolean>(IsLimitedProperty, source.IsLimited);
            this.LoadProperty<Single>(LimitedPercentageProperty, source.LimitedPercentage);
            this.LoadProperty<Single>(TotalSpacePercentageProperty, source.TotalSpacePercentage);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty, source.BlockPlacementPrimaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty, source.BlockPlacementSecondaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty, source.BlockPlacementTertiaryType);
            BusinessRules.CheckRules();
            if (!IsValid)
            {
                SetDefaultBlockPlacementPropertyValues();
            }

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the <see cref="PlanogramSequenceGroup"/> that matches this group's <see cref="Colour"/>, or
        /// null if none can be found.
        /// </summary>
        /// <returns></returns>
        public PlanogramSequenceGroup GetPlanogramSequenceGroup()
        {
            if (Parent == null || Parent.Parent == null || Parent.Parent.Sequence == null) return null;
            return Parent.Parent.Sequence.Groups.FirstOrDefault(g => g.Colour.Equals(Colour));
        }

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramBlockingGroup>(oldId, newId);
        }

        /// <summary>
        /// Notifies this group that it should recalculate its TotalSpacePercentage value.
        /// </summary>
        public void NotifyTotalSpacePercentageChanged()
        {
            this.TotalSpacePercentage = CalculateTotalSpacePercentage();
        }


        /// <summary>
        /// Returns a list of blocking locations for this group.
        /// </summary>
        /// <returns></returns>
        public List<PlanogramBlockingLocation> GetBlockingLocations()
        {
            List<PlanogramBlockingLocation> returnList = new List<PlanogramBlockingLocation>();
            if (this.Parent != null)
            {
                try
                {
                    foreach (PlanogramBlockingLocation loc in this.Parent.Locations)
                    {
                        if (Equals(loc.PlanogramBlockingGroupId, this.Id))
                        {
                            returnList.Add(loc);
                        }
                    }
                }
                catch (InvalidOperationException)
                {
                    // An invalid operation exception will be thrown from the enumeration of Parent.Locations
                    // if this collection has been modified by another thread. Because this method is called from
                    // background threads, the risk of Parent.Locations being modified elsewhere is a real one.
                    // If the invalid operation exception is thrown because of this, we just return an empty collection
                    // of locations so that the calling methods don't have any data to work with.
                    return new List<PlanogramBlockingLocation>();
                }
            }
            return returnList;
        }

        /// <summary>
        /// Ensures that the given locations and only the given locations
        /// are associated with this group.
        /// </summary>
        public void UpdateAssociatedLocations(IEnumerable<PlanogramBlockingLocation> locations)
        {
            PlanogramBlocking parentBlocking = this.Parent;
            if (parentBlocking == null) return;

            //get the list of locations currently associated with this group.
            List<PlanogramBlockingLocation> currentAssociated = GetBlockingLocations();

            //first unassociate any locations that are no longer in the group
            foreach (PlanogramBlockingLocation current in currentAssociated.ToList())
            {
                if (!locations.Contains(current))
                {
                    PlanogramBlockingGroup newGroup = parentBlocking.Groups.AddNew();
                    current.PlanogramBlockingGroupId = newGroup.Id;

                    currentAssociated.Remove(current);
                }
            }

            //next, add in any locations that are missing
            //List<BlockingLocation> remainingLocs = locations.Except(currentAssociated).ToList();

            Queue<PlanogramBlockingLocation> remainingLocs = new Queue<PlanogramBlockingLocation>(locations.Except(currentAssociated));
            while (remainingLocs.Count > 0
                && remainingLocs.Any(l => currentAssociated.Any(c => c.IsAdjacent(l))))
            {
                PlanogramBlockingLocation nextLoc = remainingLocs.Dequeue();
                if (currentAssociated.Any(c => c.IsAdjacent(nextLoc)))
                {
                    //associate the location
                    nextLoc.PlanogramBlockingGroupId = this.Id;
                }
                else
                {
                    //add back in the queue
                    remainingLocs.Enqueue(nextLoc);
                }

            }


            //clean up any unused groups
            foreach (PlanogramBlockingGroup group in parentBlocking.Groups.ToList())
            {
                if (group.GetBlockingLocations().Count == 0)
                {
                    parentBlocking.Groups.Remove(group);
                }
            }

        }

        /// <summary>
        /// Gets the Ranged Planogram Products that are associated with this Group in the order they appear in the sequence.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown if Parent or Parent.Parent is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the parent Planogram contains no Sequence.</exception>
        /// <returns>
        /// The Planogram Products associated with this Group from the Planogram Sequence, in the order they appear
        /// in the sequence. If no sequence group has the same colour as this group, no products are returned.
        /// </returns>
        public IEnumerable<PlanogramProduct> EnumerateRangedPlanogramProducts()
        {
            // Check that we have a parent and that parent contains a sequence.
            if (Parent == null || Parent.Parent == null) throw new InvalidOperationException("Parent cannot be null");
            PlanogramSequence sequence = Parent.Parent.Sequence;
            if (sequence == null) throw new InvalidOperationException("Planogram Sequence cannot be null");
            PlanogramAssortment assortment = Parent.Parent.Assortment;
            if (assortment == null) yield break;

            // Find the matching sequence group. If none exists, return an empty collection.
            PlanogramSequenceGroup sequenceGroup = sequence.Groups.FirstOrDefault(g => g.Colour == this.Colour);
            if (sequenceGroup == null) yield break;

            // Get the group's product gtins in sequence order.
            IEnumerable<String> groupProductGtins = sequenceGroup.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin);

            // Build a dictionary of the plan's products so that we can return them in sequence order.
            Dictionary<String, PlanogramProduct> planogramProductsByGtin = Parent.Parent.Products.ToDictionary(p => p.Gtin, p => p);

            // get the planograms ranged products
            IEnumerable<String> rangedAssortmentGtins = assortment.Products.Where(p => p.IsRanged).Select(p => p.Gtin);

            // Return the plan products in sequence order.
            foreach (String gtin in groupProductGtins)
            {
                PlanogramProduct planogramProduct;
                if (planogramProductsByGtin.TryGetValue(gtin, out planogramProduct) && rangedAssortmentGtins.Contains(gtin))
                {
                    yield return planogramProduct;
                }
            }
            yield break;
        }

        /// <summary>
        ///     Find the <see cref="PlanogramBlockingGroupPlacementType"/> that can be applied for the <paramref name="merchandisingGroup"/> using this instance settings.
        /// </summary>
        /// <param name="merchandisingGroup">The <see cref="PlanogramMerchandisingGroup"/> to apply this instance's settings on to get the appropriate <see cref="PlanogramBlockingGroupPlacementType"/>.</param>
        public PlanogramBlockingGroupPlacementType GetPlacementType(PlanogramMerchandisingGroup merchandisingGroup)
        {
            //  Determine where to insert the new position.
            //  Can we apply the block placement types?
            Boolean canInsertX = (merchandisingGroup.StrategyX == PlanogramSubComponentXMerchStrategyType.Even ||
                                  merchandisingGroup.StrategyX == PlanogramSubComponentXMerchStrategyType.LeftStacked ||
                                  merchandisingGroup.StrategyX == PlanogramSubComponentXMerchStrategyType.RightStacked);
            Boolean canInsertY = (merchandisingGroup.StrategyY == PlanogramSubComponentYMerchStrategyType.Even ||
                                  merchandisingGroup.StrategyY == PlanogramSubComponentYMerchStrategyType.BottomStacked ||
                                  merchandisingGroup.StrategyY == PlanogramSubComponentYMerchStrategyType.TopStacked);
            Boolean canInsertZ = (merchandisingGroup.StrategyZ == PlanogramSubComponentZMerchStrategyType.Even ||
                                  merchandisingGroup.StrategyZ == PlanogramSubComponentZMerchStrategyType.BackStacked ||
                                  merchandisingGroup.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked);
            Boolean canApplyPrimaryBlockPlacementType = (BlockPlacementPrimaryType.IsX() && canInsertX) ||
                                                        (BlockPlacementPrimaryType.IsY() && canInsertY) ||
                                                        (BlockPlacementPrimaryType.IsZ() && canInsertZ);
            Boolean canApplySecondaryBlockPlacementType = (BlockPlacementSecondaryType.IsX() && canInsertX) ||
                                                          (BlockPlacementSecondaryType.IsY() && canInsertY) ||
                                                          (BlockPlacementSecondaryType.IsZ() && canInsertZ);
            Boolean canApplyTertiaryBlockPlacementType = (BlockPlacementTertiaryType.IsX() && canInsertX) ||
                                                         (BlockPlacementTertiaryType.IsY() && canInsertY) ||
                                                         (BlockPlacementTertiaryType.IsZ() && canInsertZ);

            PlanogramBlockingGroupPlacementType placementType = canApplyPrimaryBlockPlacementType
                ? BlockPlacementPrimaryType
                : canApplySecondaryBlockPlacementType
                    ? BlockPlacementSecondaryType
                    : canApplyTertiaryBlockPlacementType
                        ? BlockPlacementTertiaryType
                        : PlanogramBlockingGroupPlacementType.LeftToRight;
            return placementType;
        }

        /// <summary>
        /// Returns the <see cref="PlanogramBlockingGroupPlacementType"/> based upon the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="axis">The <see cref="AxisType"/> given.</param>
        /// <returns></returns>
        public PlanogramBlockingGroupPlacementType GetPlacementType(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    if (BlockPlacementPrimaryType.IsX()) return BlockPlacementPrimaryType;
                    if (BlockPlacementSecondaryType.IsX()) return BlockPlacementSecondaryType;
                    if (BlockPlacementTertiaryType.IsX()) return BlockPlacementTertiaryType;
                    break;
                case AxisType.Y:
                    if (BlockPlacementPrimaryType.IsY()) return BlockPlacementPrimaryType;
                    if (BlockPlacementSecondaryType.IsY()) return BlockPlacementSecondaryType;
                    if (BlockPlacementTertiaryType.IsY()) return BlockPlacementTertiaryType;
                    break;
                case AxisType.Z:
                    if (BlockPlacementPrimaryType.IsZ()) return BlockPlacementPrimaryType;
                    if (BlockPlacementSecondaryType.IsZ()) return BlockPlacementSecondaryType;
                    if (BlockPlacementTertiaryType.IsZ()) return BlockPlacementTertiaryType;
                    break;
            }

            return BlockPlacementPrimaryType;
        }

        /// <summary>
        ///     Enumerates the <see cref="PlanogramSequencedItem"/> values corresponding to the given <paramref name="items"/>.
        /// </summary>
        /// <param name="items">Enumeration of <see cref="PlanogramSubComponentPlacement"/> instances to convert to <see cref="PlanogramSequencedItem"/>.</param>
        public IEnumerable<PlanogramSequencedItem<T>> EnumerateSequencableItemsInSequence<T>(IEnumerable<T> items)
            where T : IPlanogramSequencableItem
        {
            Dictionary<T, PointValue> coordinatesByItem = items.ToDictionary(p => p, p => p.GetSequencableCoords());

            Boolean reversePrimary = false;
            Boolean reverseSecondary = false;
            Boolean reverseTertiary = false;

            Int32 sequence = 1;
            var tertiaryGroups = OrderAndGroupItemsBy(
                BlockPlacementTertiaryType,
                coordinatesByItem.ToList(),
                !BlockPlacementTertiaryType.Invert(reverseTertiary).IsPositive());
            foreach (var tertiaryGroup in tertiaryGroups)
            {
                var secondaryGroups = OrderAndGroupItemsBy(
                    BlockPlacementSecondaryType,
                    tertiaryGroup,
                    !BlockPlacementSecondaryType.Invert(reverseSecondary).IsPositive());
                foreach (var secondaryGroup in secondaryGroups)
                {
                    var primaryGroup = OrderAndGroupItemsBy(
                        BlockPlacementPrimaryType,
                        secondaryGroup,
                        !BlockPlacementPrimaryType.Invert(reversePrimary).IsPositive())
                        .SelectMany(group => group.Select(g => g.Key));
                    foreach (T item in primaryGroup)
                    {
                        yield return new PlanogramSequencedItem<T>(
                            item,
                            reversePrimary,
                            reverseSecondary,
                            reverseTertiary,
                            sequence++);
                    }
                    reversePrimary = !reversePrimary;
                }
                reverseSecondary = !reverseSecondary;
            }
        }

        /// <summary>
        ///     Enumerates the <see cref="PlanogramSequencedPositionPlacement"/> values corresponding to the given <paramref name="positions"/>.
        /// </summary>
        /// <param name="planogramSequencedSubComponent">The <see cref="PlanogramSequencedPositionPlacement"/> that contains the positions</param>
        /// <param name="positions">Enumeration of <see cref="IPlanogramSubComponentSequencedItem"/> instances to convert to <see cref="PlanogramSequencedPositionPlacement"/>.</param>
        public IEnumerable<PlanogramSequencedPositionPlacement> EnumerateSubComponentPositionsInSequence<T>(
            PlanogramSequencedItem<T> planogramSequencedSubComponent,
            IEnumerable<IPlanogramSubComponentSequencedItem> positions) where T : IPlanogramSequencableItem
        {
            Boolean isBlockTertiaryPlacementTypeReversed = planogramSequencedSubComponent.IsTertiaryReversed;
            Boolean isBlockSecondaryPlacementTypeReversed = planogramSequencedSubComponent.IsSecondaryReversed;
            Boolean isBlockPrimaryPlacementTypeReversed = planogramSequencedSubComponent.IsPrimaryReversed;

            foreach (var tertiaryGrouping in OrderAndGroupPositionsBy(
                BlockPlacementTertiaryType,
                positions,
                !BlockPlacementTertiaryType.Invert(isBlockTertiaryPlacementTypeReversed).IsPositive()))
            {
                foreach (var secondaryGrouping in OrderAndGroupPositionsBy(
                    BlockPlacementSecondaryType,
                    tertiaryGrouping,
                    !BlockPlacementSecondaryType.Invert(isBlockSecondaryPlacementTypeReversed).IsPositive()))
                {
                    foreach (IPlanogramSubComponentSequencedItem position in OrderAndGroupPositionsBy(
                        BlockPlacementPrimaryType,
                        secondaryGrouping,
                        !BlockPlacementPrimaryType.Invert(isBlockPrimaryPlacementTypeReversed).IsPositive())
                        .SelectMany(primaryLayer => primaryLayer.Select(p => p)))
                    {
                        yield return new PlanogramSequencedPositionPlacement(
                            position,
                            isBlockPrimaryPlacementTypeReversed,
                            isBlockSecondaryPlacementTypeReversed,
                            isBlockTertiaryPlacementTypeReversed);
                    }
                    isBlockPrimaryPlacementTypeReversed = !isBlockPrimaryPlacementTypeReversed;
                }
                isBlockSecondaryPlacementTypeReversed = !isBlockSecondaryPlacementTypeReversed;
            }
        }

        private static IEnumerable<IGrouping<Single, IPlanogramSubComponentSequencedItem>> OrderAndGroupPositionsBy(
            PlanogramBlockingGroupPlacementType placementType,
            IEnumerable<IPlanogramSubComponentSequencedItem> positions,
            Boolean isReversed)
        {
            var ordered =
                isReversed ?
                positions.OrderByDescending(p => placementType.GetIndex(p)) :
                positions.OrderBy(p => placementType.GetIndex(p));
            return ordered.GroupBy(p => placementType.GetIndex(p), new PlanogramSequenceGroup.SequencableItemCoordinateComparer());
        }

        #region IPlanogramBlockingGroup Members
        void IPlanogramBlockingGroup.UpdateAssociatedLocations(IEnumerable<IPlanogramBlockingLocation> locations)
        {
            UpdateAssociatedLocations(locations.Cast<PlanogramBlockingLocation>());
        }

        IEnumerable<IPlanogramBlockingLocation> IPlanogramBlockingGroup.GetBlockingLocations()
        {
            return GetBlockingLocations();
        }

        #endregion

        /// <summary>
        /// Gets a lookup of merchandising groups keyed by the blocking location that intersect with this blocking group.
        /// Each blocking location key has a list of intersecting merchandising groups attached to it, so merchandising groups
        /// may appear in more that one list.
        /// </summary>
        /// <param name="merchandisingGroupsAndDvps">A dictionary of merchandising groups and the Design View Positions to check for intersections.</param>
        /// <param name="overlapThreshold">The percentage overlap above which merchandising groups should be considered to intersect with a location.</param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <returns></returns>
        public ILookup<PlanogramBlockingLocation, PlanogramMerchandisingGroup> GetIntersectingMerchGroupsByLocation(
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
            Single overlapThreshold,
            Single planogramBlockingWidth,
            Single planogramBlockingHeight)
        {
            IEnumerable<LocationMerchandisingGroupOverlap> locationGroupOverlaps =
                LocationMerchandisingGroupOverlap.GetLocationMerchandisingGroupOverlaps(
                    merchandisingGroupsAndDvps,
                    GetBlockingLocations(),
                    overlapThreshold,
                    planogramBlockingWidth,
                    planogramBlockingHeight);

            return locationGroupOverlaps.ToLookup(i => i.Location, i => i.MerchandisingGroup);
        }


        /// <summary>
        /// Gets a dictionary of the <see cref="PlanogramMerchandisingGroup"/>s that this group intersects with and
        /// the <see cref="PlanogramBlockingLocation"/>s that each group intersects with the most.
        /// </summary>
        /// <param name="merchandisingGroupsAndDvps"></param>
        /// <param name="overlapThreshold"></param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <returns></returns>
        public Dictionary<PlanogramMerchandisingGroup, PlanogramBlockingLocation> GetMaxIntersectingLocationByMerchGroup(
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
            Single overlapThreshold,
            Single planogramBlockingWidth,
            Single planogramBlockingHeight)
        {
            IEnumerable<LocationMerchandisingGroupOverlap> locationGroupOverlaps =
                LocationMerchandisingGroupOverlap.GetLocationMerchandisingGroupOverlaps(
                    merchandisingGroupsAndDvps,
                    GetBlockingLocations(),
                    overlapThreshold,
                    planogramBlockingWidth,
                    planogramBlockingHeight);

            Dictionary<PlanogramMerchandisingGroup, PlanogramBlockingLocation> maxIntersectingLocationByMerchGroup =
                new Dictionary<PlanogramMerchandisingGroup, PlanogramBlockingLocation>();

            foreach (IGrouping<PlanogramMerchandisingGroup, LocationMerchandisingGroupOverlap> locationOverlapGrouping
                in locationGroupOverlaps.ToLookup(l => l.MerchandisingGroup))
            {
                Double maxOverlap = locationOverlapGrouping.Max(l => l.Overlap);
                LocationMerchandisingGroupOverlap maxOverlappingGrouping = locationOverlapGrouping
                    .FirstOrDefault(l => l.Overlap.EqualTo(maxOverlap));
                if (maxOverlappingGrouping == null) continue;
                maxIntersectingLocationByMerchGroup.Add(
                    maxOverlappingGrouping.MerchandisingGroup, maxOverlappingGrouping.Location);
            }

            return maxIntersectingLocationByMerchGroup;
        }

        /// <summary>
        /// Gets a dictionary of planogram blocking locations keyed by the merchandising groups they intersect with, similar to 
        /// <see cref="GetIntersectingMerchGroupsByLocation"/>.
        /// </summary>
        /// <param name="merchandisingGroupsAndDvps"></param>
        /// <param name="overlapThreshold"></param>
        /// <param name="planogramBlockingWidth"></param>
        /// <param name="planogramBlockingHeight"></param>
        /// <returns></returns>
        public Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> GetIntersectingLocationsByMerchGroup(
            Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> merchandisingGroupsAndDvps,
            Single overlapThreshold,
            Single planogramBlockingWidth,
            Single planogramBlockingHeight)
        {
            IEnumerable<LocationMerchandisingGroupOverlap> locationGroupOverlaps =
                LocationMerchandisingGroupOverlap.GetLocationMerchandisingGroupOverlaps(
                    merchandisingGroupsAndDvps,
                    GetBlockingLocations(),
                    overlapThreshold,
                    planogramBlockingWidth,
                    planogramBlockingHeight);

            Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>> intersectingLocationsByMerchGroup =
                new Dictionary<PlanogramMerchandisingGroup, List<PlanogramBlockingLocation>>();

            foreach (LocationMerchandisingGroupOverlap locationGroupOverlap in locationGroupOverlaps)
            {
                List<PlanogramBlockingLocation> locations;
                if (!intersectingLocationsByMerchGroup.TryGetValue(locationGroupOverlap.MerchandisingGroup, out locations))
                {
                    locations = new List<PlanogramBlockingLocation>();
                    intersectingLocationsByMerchGroup.Add(locationGroupOverlap.MerchandisingGroup, locations);
                }
                locations.Add(locationGroupOverlap.Location);
            }

            return intersectingLocationsByMerchGroup;
        }

        /// <summary>
        /// Returns the coordinates by which this block should be sequenced.
        /// </summary>
        /// <returns></returns>
        public PointValue GetSequencableCoords()
        {
            IEnumerable<PlanogramBlockingLocation> locations = GetBlockingLocations();
            return new PointValue(
                locations.Min(l => l.X),
                locations.Min(l => l.Y),
                0);
        }

        #endregion

        #region Metadata

        protected override void OnCalculateMetadata()
        {
            if (this.Parent == null)
            {
                return;
            }

            var blockProductCache = new List<PlanogramProduct>();
            Planogram parentPlan = this.Parent.Parent;
            PlanogramMetadataDetails planMetaDataDetails = parentPlan.GetPlanogramMetadataDetails();

            //Single blocking = planMetaDataDetails.GetBlockingGroupSpacePercentage(this); 
            //List<PlanogramProduct> products = planMetaDataDetails.GetPlanogramProductsByBlockingGroup(this);

            // Calculate the number of placed products
            // First fetch the appropriate sequence that matches this block on the "Colour" property
            var matchSequenceItem = parentPlan.Sequence.Groups.Where(x => x.Colour.Equals(this.Colour)).FirstOrDefault();
            if (matchSequenceItem == null)
            {
                // There was no matching sequence so reset the product count metadata for this block
                OnClearMetadata();

                return;
            }

            // Calculate the number of products that are recommended in the assortment 
            var products = matchSequenceItem.Products;
            Int32? productAssortmentCount = null;
            Int32? productPlacedCount = null;
            foreach (PlanogramSequenceGroupProduct sequenceProduct in products)
            {
                foreach (PlanogramAssortmentProduct assortmentProd in parentPlan.Assortment.Products)
                {
                    if (!assortmentProd.IsRanged) continue;

                    if (assortmentProd.Gtin == sequenceProduct.Gtin)
                    {
                        productAssortmentCount = (productAssortmentCount == null ? 1 : productAssortmentCount + 1);
                    }
                }

                // Calculate the number of products that are placed i.e. have MetaPositionCount > 0 for the PlanogramProduct record
                // Get the appropriate PlanogramProduct record
                var planogramProductMatch = parentPlan.Products.Where(x => x.Gtin == sequenceProduct.Gtin).FirstOrDefault();
                if (planogramProductMatch != null && planogramProductMatch != null && planogramProductMatch.MetaPositionCount > 0)
                {
                    productPlacedCount = (productPlacedCount == null ? 1 : productPlacedCount + 1);
                    blockProductCache.Add(planogramProductMatch);
                }
            }

            this.MetaCountOfProducts = products.Count;
            this.MetaCountOfProductRecommendedOnPlanogram = productAssortmentCount;
            this.MetaCountOfProductPlacedOnPlanogram = productPlacedCount;

            // Calculate the various percentage values
            this.MetaOriginalPercentAllocated = planMetaDataDetails.GetBlockingGroupSpacePercentage(this, PlanogramBlockingType.Initial);
            this.MetaFinalPercentAllocated = planMetaDataDetails.GetBlockingGroupSpacePercentage(this, PlanogramBlockingType.Final);
            this.MetaPerformancePercentAllocated = planMetaDataDetails.GetBlockingGroupSpacePercentage(this, PlanogramBlockingType.PerformanceApplied);

            this.MetaLinearProductPlacementPercent = blockProductCache.Sum(x => x.MetaPlanogramLinearSpacePercentage);
            this.MetaAreaProductPlacementPercent = blockProductCache.Sum(x => x.MetaPlanogramAreaSpacePercentage);
            this.MetaVolumetricProductPlacementPercent = blockProductCache.Sum(x => x.MetaPlanogramVolumetricSpacePercentage);

            // Get the plan metadata object
            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();

            // Calculate the Meta performance values
            metadataDetails.CalculateBlockingGroupPerformance(this, blockProductCache);
        }

        protected override void OnClearMetadata()
        {
            this.MetaOriginalPercentAllocated = null;
            this.MetaFinalPercentAllocated = null;
            this.MetaPerformancePercentAllocated = null;
            this.MetaLinearProductPlacementPercent = null;
            this.MetaAreaProductPlacementPercent = null;
            this.MetaVolumetricProductPlacementPercent = null;

            this.MetaCountOfProducts = null;
            this.MetaCountOfProductPlacedOnPlanogram = null;
            this.MetaCountOfProductRecommendedOnPlanogram = null;
        }

        #endregion

        #region Static Helper Methods

        private static IEnumerable<IGrouping<Single, KeyValuePair<T, PointValue>>> OrderAndGroupItemsBy<T>(
            PlanogramBlockingGroupPlacementType placementType,
            IEnumerable<KeyValuePair<T, PointValue>> placementsByCoordinate,
            Boolean isReversed) where T : IPlanogramSequencableItem
        {
            IOrderedEnumerable<KeyValuePair<T, PointValue>> ordered = isReversed
                ? placementsByCoordinate.OrderByDescending(kvp => placementType.GetIndex(kvp.Value))
                : placementsByCoordinate.OrderBy(kvp => placementType.GetIndex(kvp.Value));
            return ordered.GroupBy(kvp => placementType.GetIndex(kvp.Value), new PlanogramSequenceGroup.SequencableItemCoordinateComparer());
        }

        #endregion

    }

    public class BlockSpacePercentageLimitRule : Csla.Rules.BusinessRule
    {
        #region Fields
        private String _message;
        #endregion

        #region Constructor
        public BlockSpacePercentageLimitRule(Csla.Core.IPropertyInfo primaryProperty, String message) :
            base(primaryProperty)
        {
            _message = message;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the check to ensure task name is unique
        /// </summary>
        protected override void Execute(RuleContext context)
        {
            var task = (PlanogramBlockingGroup)context.Target;

            if (task.IsLimited)
            {
                if (task.LimitedPercentage > 1F)
                {
                    context.AddErrorResult(_message);
                    return;
                }

                context.AddSuccessResult(false);
            }
            context.AddSuccessResult(false);
        }
        #endregion
    }
}
