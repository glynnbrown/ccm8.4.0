﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27474 : A.Silva
//      Added ComponentSequenceNumber.
// V8-27570 : A.Silva
//      Moved IPlanogramComponent out to its own file.
#endregion

#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: CCM802
// V8-28811 : L.Luong
//  Added MetaPercentageLinearSpaceFilled
#endregion

#region Version History: CCM803
// V8-29044 : M.Shelley
//  Added NotchNumber property to allow saving to the database
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion

#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
#endregion

#region Version History: (CCM 8.3)
// V8-32521 : J.Pickup
//  Added MetaIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramFixtureComponent: IPlanogramFixtureComponent
    {
        #region Constructor
        private PlanogramFixtureComponent() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramFixtureComponent Fetch(IDalContext dalContext, PlanogramFixtureComponentDto dto)
        {
            return DataPortal.FetchChild<PlanogramFixtureComponent>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramFixtureComponentDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<Object>(PlanogramComponentIdProperty, dto.PlanogramComponentId);
            LoadProperty<Int16?>(ComponentSequenceNumberProperty, dto.ComponentSequenceNumber);
            LoadProperty<Single>(XProperty, dto.X);
            LoadProperty<Single>(YProperty, dto.Y);
            LoadProperty<Single>(ZProperty, dto.Z);
            LoadProperty<Single>(SlopeProperty, dto.Slope);
            LoadProperty<Single>(AngleProperty, dto.Angle);
            LoadProperty<Single>(RollProperty, dto.Roll);
            LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, dto.MetaTotalMerchandisableLinearSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, dto.MetaTotalMerchandisableAreaSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, dto.MetaTotalMerchandisableVolumetricSpace);
            this.LoadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, dto.MetaTotalLinearWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, dto.MetaTotalAreaWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, dto.MetaTotalVolumetricWhiteSpace);
            this.LoadProperty<Int32?>(MetaProductsPlacedProperty, dto.MetaProductsPlaced);
            this.LoadProperty<Int32?>(MetaNewProductsProperty, dto.MetaNewProducts);
            this.LoadProperty<Int32?>(MetaChangesFromPreviousCountProperty, dto.MetaChangesFromPreviousCount);
            this.LoadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, dto.MetaChangeFromPreviousStarRating);
            this.LoadProperty<Int32?>(MetaBlocksDroppedProperty, dto.MetaBlocksDropped);
            this.LoadProperty<Int32?>(MetaNotAchievedInventoryProperty, dto.MetaNotAchievedInventory);
            this.LoadProperty<Int32?>(MetaTotalFacingsProperty, dto.MetaTotalFacings);
            this.LoadProperty<Int16?>(MetaAverageFacingsProperty, dto.MetaAverageFacings);
            this.LoadProperty<Int32?>(MetaTotalUnitsProperty, dto.MetaTotalUnits);
            this.LoadProperty<Int32?>(MetaAverageUnitsProperty, dto.MetaAverageUnits);
            this.LoadProperty<Single?>(MetaMinDosProperty, dto.MetaMinDos);
            this.LoadProperty<Single?>(MetaMaxDosProperty, dto.MetaMaxDos);
            this.LoadProperty<Single?>(MetaAverageDosProperty, dto.MetaAverageDos);
            this.LoadProperty<Single?>(MetaMinCasesProperty, dto.MetaMinCases);
            this.LoadProperty<Single?>(MetaAverageCasesProperty, dto.MetaAverageCases);
            this.LoadProperty<Single?>(MetaMaxCasesProperty, dto.MetaMaxCases);
            this.LoadProperty<Int16?>(MetaSpaceToUnitsIndexProperty, dto.MetaSpaceToUnitsIndex);
            this.LoadProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty, dto.MetaIsComponentSlopeWithNoRiser);
            this.LoadProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty, dto.MetaIsOverMerchandisedDepth);
            this.LoadProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty, dto.MetaIsOverMerchandisedHeight);
            this.LoadProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty, dto.MetaIsOverMerchandisedWidth);
            this.LoadProperty<Int32?>(MetaTotalComponentCollisionsProperty, dto.MetaTotalComponentCollisions);
            this.LoadProperty<Int32?>(MetaTotalPositionCollisionsProperty, dto.MetaTotalPositionCollisions);
            this.LoadProperty<Int16?>(MetaTotalFrontFacingsProperty, dto.MetaTotalFrontFacings);
            this.LoadProperty<Single?>(MetaAverageFrontFacingsProperty, dto.MetaAverageFrontFacings);
            this.LoadProperty<Single?>(MetaPercentageLinearSpaceFilledProperty, dto.MetaPercentageLinearSpaceFilled);
            this.LoadProperty<Int32?>(NotchNumberProperty, dto.NotchNumber);
            this.LoadProperty<Single?>(MetaWorldXProperty, dto.MetaWorldX);
            this.LoadProperty<Single?>(MetaWorldYProperty, dto.MetaWorldY);
            this.LoadProperty<Single?>(MetaWorldZProperty, dto.MetaWorldZ);
            this.LoadProperty<Single?>(MetaWorldAngleProperty, dto.MetaWorldAngle);
            this.LoadProperty<Single?>(MetaWorldSlopeProperty, dto.MetaWorldSlope);
            this.LoadProperty<Single?>(MetaWorldRollProperty, dto.MetaWorldRoll);
            this.LoadProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty, dto.MetaIsOutsideOfFixtureArea);
            
        }

        /// <summary>
        /// Creates a data transfer object for this instance
        /// </summary>
        private PlanogramFixtureComponentDto GetDataTransferObject(PlanogramFixture parent)
        {
            return new PlanogramFixtureComponentDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramFixtureId = parent.Id,
                PlanogramComponentId = ReadProperty<Object>(PlanogramComponentIdProperty),
                ComponentSequenceNumber = ReadProperty<Int16?>(ComponentSequenceNumberProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                Slope = ReadProperty<Single>(SlopeProperty),
                Angle = ReadProperty<Single>(AngleProperty),
                Roll = ReadProperty<Single>(RollProperty),
                ExtendedData = ReadProperty<Object>(ExtendedDataProperty),
                MetaTotalMerchandisableLinearSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty),
                MetaTotalMerchandisableAreaSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty),
                MetaTotalMerchandisableVolumetricSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty),
                MetaTotalLinearWhiteSpace = this.ReadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty),
                MetaTotalAreaWhiteSpace = this.ReadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty),
                MetaTotalVolumetricWhiteSpace = this.ReadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty),
                MetaProductsPlaced = this.ReadProperty<Int32?>(MetaProductsPlacedProperty),
                MetaNewProducts = this.ReadProperty<Int32?>(MetaNewProductsProperty),
                MetaChangesFromPreviousCount = this.ReadProperty<Int32?>(MetaChangesFromPreviousCountProperty),
                MetaChangeFromPreviousStarRating = this.ReadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty),
                MetaBlocksDropped = this.ReadProperty<Int32?>(MetaBlocksDroppedProperty),
                MetaNotAchievedInventory = this.ReadProperty<Int32?>(MetaNotAchievedInventoryProperty),
                MetaTotalFacings = this.ReadProperty<Int32?>(MetaTotalFacingsProperty),
                MetaAverageFacings = this.ReadProperty<Int16?>(MetaAverageFacingsProperty),
                MetaTotalUnits = this.ReadProperty<Int32?>(MetaTotalUnitsProperty),
                MetaAverageUnits = this.ReadProperty<Int32?>(MetaAverageUnitsProperty),
                MetaMinDos = this.ReadProperty<Single?>(MetaMinDosProperty),
                MetaMaxDos = this.ReadProperty<Single?>(MetaMaxDosProperty),
                MetaAverageDos = this.ReadProperty<Single?>(MetaAverageDosProperty),
                MetaMinCases = this.ReadProperty<Single?>(MetaMinCasesProperty),
                MetaAverageCases = this.ReadProperty<Single?>(MetaAverageCasesProperty),
                MetaMaxCases = this.ReadProperty<Single?>(MetaMaxCasesProperty),
                MetaSpaceToUnitsIndex = this.ReadProperty<Int16?>(MetaSpaceToUnitsIndexProperty),
                MetaIsComponentSlopeWithNoRiser = this.ReadProperty<Boolean?>(MetaIsComponentSlopeWithNoRiserProperty),
                MetaIsOverMerchandisedDepth = this.ReadProperty<Boolean?>(MetaIsOverMerchandisedDepthProperty),
                MetaIsOverMerchandisedHeight = this.ReadProperty<Boolean?>(MetaIsOverMerchandisedHeightProperty),
                MetaIsOverMerchandisedWidth = this.ReadProperty<Boolean?>(MetaIsOverMerchandisedWidthProperty),
                MetaTotalComponentCollisions = this.ReadProperty<Int32?>(MetaTotalComponentCollisionsProperty),
                MetaTotalPositionCollisions = this.ReadProperty<Int32?>(MetaTotalPositionCollisionsProperty),
                MetaTotalFrontFacings = this.ReadProperty<Int16?>(MetaTotalFrontFacingsProperty),
                MetaAverageFrontFacings = this.ReadProperty<Single?>(MetaAverageFrontFacingsProperty),
                MetaPercentageLinearSpaceFilled = this.ReadProperty<Single?>(MetaPercentageLinearSpaceFilledProperty),
                NotchNumber = this.ReadProperty<Int32?>(NotchNumberProperty),
                MetaWorldX = this.ReadProperty<Single?>(MetaWorldXProperty),
                MetaWorldY = this.ReadProperty<Single?>(MetaWorldYProperty),
                MetaWorldZ = this.ReadProperty<Single?>(MetaWorldZProperty),
                MetaWorldAngle = this.ReadProperty<Single?>(MetaWorldAngleProperty),
                MetaWorldSlope = this.ReadProperty<Single?>(MetaWorldSlopeProperty),
                MetaWorldRoll = this.ReadProperty<Single?>(MetaWorldRollProperty),
                MetaIsOutsideOfFixtureArea = this.ReadProperty<Boolean?>(MetaIsOutsideOfFixtureAreaProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramFixtureComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramFixture parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramFixtureComponentDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramFixtureComponentDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramFixtureComponent>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramFixture parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramFixtureComponentDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramFixture parent)
        {
            batchContext.Delete<PlanogramFixtureComponentDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}