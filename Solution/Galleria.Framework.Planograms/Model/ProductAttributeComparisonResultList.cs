using System;
using System.Collections.Generic;
using System.Linq;
using Csla;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class ProductAttributeComparisonResultList : ModelList<ProductAttributeComparisonResultList, ProductAttributeComparisonResult>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="Planogram"/>.
        /// </summary>
        public new Planogram Parent => (Planogram)base.Parent;

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="EntityComparisonAttributeResultList"/>.
        /// </summary>
        public static ProductAttributeComparisonResultList NewEntityComparisonAttributeResultList()
        {
            var item = new ProductAttributeComparisonResultList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Returns all specified products by their ids
        /// </summary>
        public static ProductAttributeComparisonResultList FetchByPlanogramId(Int32 planogramId)
        {
            return DataPortal.Fetch<ProductAttributeComparisonResultList>(new ProductAttributeComparisonResult.FetchByPlanogramIdCriteria("ProductAttributeComparisonResultDal", planogramId));
        }
        
        #endregion

        #endregion

        #region Methods

        public new void AddRange(IEnumerable<ProductAttributeComparisonResult> sourceItems)
        {
            base.AddRange(sourceItems);
        }

        #endregion

       /// <summary>
       /// Method to store the comparison result into the database.
       /// </summary>
       /// <param name="gtin"></param>
       /// <param name="attributeCompared"></param>
       /// <param name="masterDataValue"></param>
       /// <param name="productValue"></param>
       /// <returns></returns>
        public ProductAttributeComparisonResult StoreResult(String gtin, String attributeCompared,
            String masterDataValue, String productValue)
        {

            ProductAttributeComparisonResult itemToUpdate =
                    base.Items.FirstOrDefault(
                        prod => prod.ProductGtin == gtin && prod.ComparedProductAttribute == attributeCompared);
            if (itemToUpdate != null)
            {
                itemToUpdate.MasterDataValue = masterDataValue;
                itemToUpdate.ProductValue = productValue;

                return itemToUpdate;
            }
         
            ProductAttributeComparisonResult newProductAttributeComparisonResult =
                ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(gtin, attributeCompared,
                    masterDataValue, productValue);

            Add(newProductAttributeComparisonResult);

            return newProductAttributeComparisonResult;
        }
    }
}