﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Collection of <see cref="PlanogramComparisonResult"/> instances belonging to the parent <see cref="PlanogramComparison"/>.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonResultList : ModelList<PlanogramComparisonResultList, PlanogramComparisonResult>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparison"/>.
        /// </summary>
        public new PlanogramComparison Parent { get { return (PlanogramComparison)base.Parent; } }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparisonResultList"/>.
        /// </summary>
        public static PlanogramComparisonResultList NewPlanogramComparisonResultList()
        {
            var item = new PlanogramComparisonResultList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Add a collection of <see cref="PlanogramComparisonResult"/> derived from an enumeration of <see cref="Planogram"/>.
        /// </summary>
        /// <param name="range">The enumeration of <see cref="Planogram"/> from which to derive the new items.</param>
        public void AddRange(IEnumerable<Planogram> range)
        {
            base.AddRange(range.Select(PlanogramComparisonResult.NewPlanogramComparisonResult));
        }

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonResult"/> derived from a <see cref="Planogram"/>.
        /// </summary>
        /// <param name="item">The instance of <see cref="Planogram"/> from which to derive the new item.</param>
        public PlanogramComparisonResult Add(Planogram item)
        {
            PlanogramComparisonResult newItem = PlanogramComparisonResult.NewPlanogramComparisonResult(item);
            Add(newItem);
            return newItem;
        }

        #endregion
    }
}