﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26815 : A.Silva ~ Added constructor from source.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Model representing a list of PlanogramValidationTemplateGroupMetric objects.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationTemplateGroupMetricList : ModelList<PlanogramValidationTemplateGroupMetricList, PlanogramValidationTemplateGroupMetric>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramValidationTemplateGroup Parent
        {
            get { return (PlanogramValidationTemplateGroup)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramValidationTemplateGroupMetricList NewPlanogramValidationTemplateGroupMetricList()
        {
            var item = new PlanogramValidationTemplateGroupMetricList();
            item.Create();
            return item;
        }
 
        public static PlanogramValidationTemplateGroupMetricList NewPlanogramValidationTemplateGroupMetricList(IValidationTemplateGroup source)
        {
            var item = new PlanogramValidationTemplateGroupMetricList();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        private void Create(IValidationTemplateGroup source)
        {
            AddRange(source.Metrics.Select(PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric));
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        new PlanogramValidationTemplateGroupMetric this[int index]
        {
            get { return Count > index ? base[index] : null; }
        }
    }
}
