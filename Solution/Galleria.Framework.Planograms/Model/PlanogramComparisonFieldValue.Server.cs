﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonFieldValue
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonFieldValue() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto"/>.
        /// </summary>
        internal static PlanogramComparisonFieldValue Fetch(IDalContext dalContext, PlanogramComparisonFieldValueDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonFieldValue>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldValueDto"/> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonFieldValueDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(FieldPlaceholderProperty, dto.FieldPlaceholder);
            LoadProperty(ValueProperty, dto.Value);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonFieldValueDto"/> with the data in this instance.</returns>
        private PlanogramComparisonFieldValueDto GetDataTransferObject(PlanogramComparisonItem parent)
        {
            return new PlanogramComparisonFieldValueDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramComparisonItemId = parent.Id,
                FieldPlaceholder = ReadProperty(FieldPlaceholderProperty),
                Value = ReadProperty(ValueProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonFieldValue"/>.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonFieldValueDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparisonFieldValue"/>.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramComparisonItem parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramComparisonFieldValueDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramComparisonFieldValue>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonFieldValue"/>.
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramComparisonItem parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparisonFieldValue"/>.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramComparisonItem parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}