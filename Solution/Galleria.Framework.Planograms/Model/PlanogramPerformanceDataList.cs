﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM810
// V8-29993 : N.Foster
//  Added methods to quickly allow creation of performance data for products
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of Planogram Performance Data, contained within a Planogram Performance item.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPerformanceDataList : ModelList<PlanogramPerformanceDataList,PlanogramPerformanceData>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramPerformance Parent
        {
            get { return (PlanogramPerformance)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceDataList NewPlanogramPerformanceDataList()
        {
            var item = new PlanogramPerformanceDataList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the parent's business rules are checked when the collection is changed.
        /// </summary>
        protected override void OnBulkCollectionChanged(Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (this.RaiseListChangedEvents)
            {
                Parent.CheckBusinessRules(PlanogramPerformance.PerformanceDataProperty);
            }

            base.OnBulkCollectionChanged(e);
        }

        /// <summary>
        /// Creates and adds a range of performance data
        /// for a collection of planogram products
        /// </summary>
        public IEnumerable<PlanogramPerformanceData> AddList(IEnumerable<PlanogramProduct> products)
        {
            // build a dictionary of products indexed by their product Id
            Dictionary<Object, PlanogramProduct> productLookup = new Dictionary<Object, PlanogramProduct>();
            foreach (PlanogramProduct product in products)
            {
                productLookup.Add(product.Id, product);
            }

            // remove those products that already have performance data
            foreach (PlanogramPerformanceData item in this)
            {
                if (productLookup.ContainsKey(item.PlanogramProductId))
                    productLookup.Remove(item.PlanogramProductId);
            }

            // now build a list of performance data to add
            List<PlanogramPerformanceData> itemsToAdd = new List<PlanogramPerformanceData>();
            foreach (PlanogramProduct product in productLookup.Values)
            {
                itemsToAdd.Add(PlanogramPerformanceData.NewPlanogramPerformanceData(product));
            }

            // add the performance data to this collection
            this.AddList(itemsToAdd);

            // return the list of added performance data
            return itemsToAdd;
        }

        #endregion
    }
}
