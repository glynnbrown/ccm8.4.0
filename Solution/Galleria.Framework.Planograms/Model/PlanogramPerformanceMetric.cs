﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
// V8-26805 : A.Kuszyk
//  Added copy values method.
// V8-26673 : N.Foster
//  Added interface for performance metrics
#endregion

#region Version History: CCM803
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion

#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing an item of Planogram Performance Metric.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPerformanceMetric :
        ModelObject<PlanogramPerformanceMetric>,
        IPlanogramPerformanceMetric
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramPerformance Parent
        {
            get { return ((PlanogramPerformanceMetricList)base.Parent).Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PanogramPerformanceMetric_Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description, Message.PanogramPerformanceMetric_Description);
        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricDirectionType> DirectionProperty =
            RegisterModelProperty<MetricDirectionType>(c => c.Direction, Message.PanogramPerformanceMetric_Direction);
        /// <summary>
        /// Direction
        /// </summary>
        public MetricDirectionType Direction
        {
            get { return GetProperty<MetricDirectionType>(DirectionProperty); }
            set { SetProperty<MetricDirectionType>(DirectionProperty, value); }
        }
        #endregion

        #region SpecialType
        /// <summary>
        /// SpecialType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricSpecialType> SpecialTypeProperty =
            RegisterModelProperty<MetricSpecialType>(c => c.SpecialType, Message.PanogramPerformanceMetric_SpecialType);
        /// <summary>
        /// SpecialType
        /// </summary>
        public MetricSpecialType SpecialType
        {
            get { return GetProperty<MetricSpecialType>(SpecialTypeProperty); }
            set
            {
                SetProperty<MetricSpecialType>(SpecialTypeProperty, value);

                //update the planogram performance
                if (this.Parent != null)
                {
                    this.Parent.UpdateAllInventoryValues();
                }
            }
        }
        #endregion

        #region MetricType
        /// <summary>
        /// MetricType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricType> MetricTypeProperty =
            RegisterModelProperty<MetricType>(c => c.Type, Message.PanogramPerformanceMetric_MetricType);
        /// <summary>
        /// MetricType
        /// </summary>
        public MetricType Type
        {
            get { return GetProperty<MetricType>(MetricTypeProperty); }
            set { SetProperty<MetricType>(MetricTypeProperty, value); }
        }
        #endregion

        #region MetricId
        /// <summary>
        /// MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MetricIdProperty =
            RegisterModelProperty<Byte>(c => c.MetricId, Message.PanogramPerformanceMetric_MetricId);
        /// <summary>
        /// MetricId
        /// </summary>
        public Byte MetricId
        {
            get { return GetProperty<Byte>(MetricIdProperty); }
            set { SetProperty<Byte>(MetricIdProperty, value); }
        }
        #endregion

        #region AggregationType
        /// <summary>
        /// AggregationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<AggregationType> AggregationTypeProperty =
            RegisterModelProperty<AggregationType>(c => c.AggregationType, Message.PanogramPerformanceMetric_AggregationType);
        /// <summary>
        /// AggregationType
        /// </summary>
        public AggregationType AggregationType
        {
            get { return GetProperty<AggregationType>(AggregationTypeProperty); }
            set { SetProperty<AggregationType>(AggregationTypeProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new Required(MetricIdProperty));
            BusinessRules.AddRule(new MinValue<Byte>(MetricIdProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(MetricIdProperty, Constants.MaximumMetricsPerPerformanceSource));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceMetric NewPlanogramPerformanceMetric()
        {
            var item = new PlanogramPerformanceMetric();
            item.Create(0);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceMetric NewPlanogramPerformanceMetric(Byte metricId)
        {
            var item = new PlanogramPerformanceMetric();
            item.Create(metricId);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformanceMetric NewPlanogramPerformanceMetric(IPlanogramPerformanceMetric metric)
        {
            var item = new PlanogramPerformanceMetric();
            item.Create(metric);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Byte metricId)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(MetricIdProperty, metricId);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IPlanogramPerformanceMetric metric)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, metric.Name);
            this.LoadProperty<String>(DescriptionProperty, metric.Description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, metric.Direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, metric.SpecialType);
            this.LoadProperty<MetricType>(MetricTypeProperty, metric.Type);
            this.LoadProperty<Byte>(MetricIdProperty, metric.MetricId);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Copies the values from the given metric
        /// </summary>
        public void CopyValues(IPlanogramPerformanceMetric metric)
        {
            this.Name = metric.Name;
            this.Description = metric.Description;
            this.Direction = metric.Direction;
            this.SpecialType = metric.SpecialType;
            this.Type = metric.Type;
            this.MetricId = metric.MetricId;
        }
        #endregion
    }
}
