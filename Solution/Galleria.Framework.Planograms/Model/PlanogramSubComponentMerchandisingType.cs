﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-25304 : L.Ineson
//  Added HangFromBottom for rods and bars where the product hangs from the bottom facing.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the different ways in which a subcomponent can be
    /// merchandised.
    /// </summary>
    public enum PlanogramSubComponentMerchandisingType
    {
        /// <summary>
        /// Indicates that the subcomponent is not merchandisable in any way.
        /// </summary>
        None = 0,

        /// <summary>
        /// The subcomponent is merchandised by placing products onto its
        /// top facing e.g. a shelf.
        /// </summary>
        Stack = 1,

        /// <summary>
        /// The subcomponent is merchandised by hanging products 
        /// from its front facing e.g. a pegboard or hanging bar
        /// </summary>
        Hang = 2,

        /// <summary>
        /// The subcomponent is merchandised by hanging products beneath it 
        /// from its bottom facing e.g. a rod.
        /// </summary>
        HangFromBottom = 3,
    }

    /// <summary>
    /// PlanogramSubComponentMerchandisingType Helper Class
    /// </summary>
    public static class PlanogramSubComponentMerchandisingTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentMerchandisingType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentMerchandisingType, String>()
            {
                {PlanogramSubComponentMerchandisingType.None, Message.Enum_PlanogramSubComponentMerchandisingType_None},
                {PlanogramSubComponentMerchandisingType.Stack, Message.Enum_PlanogramSubComponentMerchandisingType_Stack},
                {PlanogramSubComponentMerchandisingType.Hang, Message.Enum_PlanogramSubComponentMerchandisingType_Hang},
                {PlanogramSubComponentMerchandisingType.HangFromBottom, Message.Enum_PlanogramSubComponentMerchandisingType_HangFromBottom},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentMerchandisingType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentMerchandisingType, String>()
            {
                {PlanogramSubComponentMerchandisingType.None, Message.Enum_PlanogramSubComponentMerchandisingType_None},
                {PlanogramSubComponentMerchandisingType.Stack, Message.Enum_PlanogramSubComponentMerchandisingType_Stack},
                {PlanogramSubComponentMerchandisingType.Hang, Message.Enum_PlanogramSubComponentMerchandisingType_Hang},
                {PlanogramSubComponentMerchandisingType.HangFromBottom, Message.Enum_PlanogramSubComponentMerchandisingType_HangFromBottom},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentMerchandisingType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentMerchandisingType>()
            {
                {Message.Enum_PlanogramSubComponentMerchandisingType_None.ToLowerInvariant(), PlanogramSubComponentMerchandisingType.None},
                {Message.Enum_PlanogramSubComponentMerchandisingType_Stack.ToLowerInvariant(), PlanogramSubComponentMerchandisingType.Stack},
                {Message.Enum_PlanogramSubComponentMerchandisingType_Hang.ToLowerInvariant(), PlanogramSubComponentMerchandisingType.Hang},
                {Message.Enum_PlanogramSubComponentMerchandisingType_HangFromBottom.ToLowerInvariant(), PlanogramSubComponentMerchandisingType.HangFromBottom},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentMerchandisingType? PlanogramSubComponentMerchandisingTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentMerchandisingType? returnValue = null;
            PlanogramSubComponentMerchandisingType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
