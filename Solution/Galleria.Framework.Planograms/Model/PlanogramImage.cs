﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25479 : A.Kuszyk
//      Business rules added.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class PlanogramImage: ModelObject<PlanogramImage>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramImageList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        private static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);
        public String FileName
        {
            get { return GetProperty<String>(FileNameProperty); }
            set { SetProperty<String>(FileNameProperty, value); }
        }

        private static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// A description of the image.
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }


        private static readonly ModelPropertyInfo<Byte[]> ImageDataProperty =
            RegisterModelProperty<Byte[]>(c => c.ImageData);
        /// <summary>
        /// Gets/Sets the actual image data.
        /// </summary>
        public Byte[] ImageData
        {
            get { return GetProperty<Byte[]>(ImageDataProperty); }
            set { SetProperty<Byte[]>(ImageDataProperty, value); }
        }

        #endregion

        #region Authorisation Rules

        private static void AddObjectAuthorizationRules()
        {
        }

        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new MaxLength(FileNameProperty, 255));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type.
        /// </summary>
        /// <returns></returns>
        public static PlanogramImage NewPlanogramImage()
        {
            PlanogramImage item = new PlanogramImage();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramImage>(oldId, newId);
        }
        #endregion
    }
}
