﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Sub Component Notch Style Type Enum
    /// </summary>
    public enum PlanogramSubComponentNotchStyleType
    {
        SingleSquare = 0,
        SingleRectangle = 1,
        SingleCircle = 2,
        DoubleSquare = 3,
        DoubleRectangle = 4,
        DoubleCircle = 5,
        SingleH = 6
    }

    /// <summary>
    /// PlanogramSubComponentNotchStyleType Helper Class
    /// </summary>
    public static class PlanogramSubComponentNotchStyleTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentNotchStyleType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentNotchStyleType, String>()
            {
                {PlanogramSubComponentNotchStyleType.SingleSquare, Message.Enum_PlanogramSubComponentNotchStyleType_SingleSquare},
                {PlanogramSubComponentNotchStyleType.SingleRectangle, Message.Enum_PlanogramSubComponentNotchStyleType_SingleRectangle},
                {PlanogramSubComponentNotchStyleType.SingleCircle, Message.Enum_PlanogramSubComponentNotchStyleType_SingleCircle},
                {PlanogramSubComponentNotchStyleType.DoubleSquare, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleSquare},
                {PlanogramSubComponentNotchStyleType.DoubleRectangle, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleRectangle},
                {PlanogramSubComponentNotchStyleType.DoubleCircle, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleCircle},
                {PlanogramSubComponentNotchStyleType.SingleH, Message.Enum_PlanogramSubComponentNotchStyleType_SingleH},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentNotchStyleType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentNotchStyleType, String>()
            {
                {PlanogramSubComponentNotchStyleType.SingleSquare, Message.Enum_PlanogramSubComponentNotchStyleType_SingleSquare},
                {PlanogramSubComponentNotchStyleType.SingleRectangle, Message.Enum_PlanogramSubComponentNotchStyleType_SingleRectangle},
                {PlanogramSubComponentNotchStyleType.SingleCircle, Message.Enum_PlanogramSubComponentNotchStyleType_SingleCircle},
                {PlanogramSubComponentNotchStyleType.DoubleSquare, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleSquare},
                {PlanogramSubComponentNotchStyleType.DoubleRectangle, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleRectangle},
                {PlanogramSubComponentNotchStyleType.DoubleCircle, Message.Enum_PlanogramSubComponentNotchStyleType_DoubleCircle},
                {PlanogramSubComponentNotchStyleType.SingleH, Message.Enum_PlanogramSubComponentNotchStyleType_SingleH},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentNotchStyleType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentNotchStyleType>()
            {
                {Message.Enum_PlanogramSubComponentNotchStyleType_SingleSquare.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.SingleSquare},
                {Message.Enum_PlanogramSubComponentNotchStyleType_SingleRectangle.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.SingleRectangle},
                {Message.Enum_PlanogramSubComponentNotchStyleType_SingleCircle.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.SingleCircle},
                {Message.Enum_PlanogramSubComponentNotchStyleType_DoubleSquare.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.DoubleSquare},
                {Message.Enum_PlanogramSubComponentNotchStyleType_DoubleRectangle.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.DoubleRectangle},
                {Message.Enum_PlanogramSubComponentNotchStyleType_DoubleCircle.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.DoubleCircle},
                {Message.Enum_PlanogramSubComponentNotchStyleType_SingleH.ToLowerInvariant(), PlanogramSubComponentNotchStyleType.SingleH},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentNotchStyleType? PlanogramSubComponentNotchStyleTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentNotchStyleType? returnValue = null;
            PlanogramSubComponentNotchStyleType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}