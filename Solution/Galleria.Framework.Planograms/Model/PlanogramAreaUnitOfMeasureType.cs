﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24953 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available area unit of measure types
    /// </summary>
    public enum PlanogramAreaUnitOfMeasureType
    {
        Unknown = 0, 
        SquareCentimeters = 1, 
        SquareInches = 2
    }

    /// <summary>
    /// PlanogramAreaUnitOfMeasureType Helper Class
    /// </summary>
    public static class PlanogramAreaUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAreaUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<PlanogramAreaUnitOfMeasureType, String>()
            {
                {PlanogramAreaUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramAreaUnitOfMeasureType.SquareCentimeters, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareCentimeters},
                {PlanogramAreaUnitOfMeasureType.SquareInches, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareInches},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAreaUnitOfMeasureType, String> Abbreviations =
            new Dictionary<PlanogramAreaUnitOfMeasureType, String>()
            {
                {PlanogramAreaUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramAreaUnitOfMeasureType.SquareCentimeters, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareCentimeters_Abbrev},
                {PlanogramAreaUnitOfMeasureType.SquareInches, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareInches_Abbrev},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAreaUnitOfMeasureType, String> AvailableValues =
            new Dictionary<PlanogramAreaUnitOfMeasureType, String>()
            {
                //{PlanogramAreaUnitOfMeasureType.Unknown, String.Empty},//removed so that this isn't selectable in ui.
                {PlanogramAreaUnitOfMeasureType.SquareCentimeters, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareCentimeters},
                {PlanogramAreaUnitOfMeasureType.SquareInches, Message.Enum_PlanogramAreaUnitOfMeasureType_SquareInches},
            };

        public static PlanogramAreaUnitOfMeasureType FromLength(PlanogramLengthUnitOfMeasureType value)
        {
            switch (value)
            {
                case PlanogramLengthUnitOfMeasureType.Centimeters: return PlanogramAreaUnitOfMeasureType.SquareCentimeters;
                case PlanogramLengthUnitOfMeasureType.Inches: return PlanogramAreaUnitOfMeasureType.SquareInches;

                default:
                case PlanogramLengthUnitOfMeasureType.Unknown: return PlanogramAreaUnitOfMeasureType.Unknown;
            }
        }
    }
}
