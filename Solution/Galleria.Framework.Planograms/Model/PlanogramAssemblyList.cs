﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#region Version History: CCM830
// CCM-32563 : L.Ineson
//  Added RemoveAssemblyAndDependants
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of assemblies contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAssemblyList : ModelList<PlanogramAssemblyList, PlanogramAssembly>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssemblyList NewPlanogramAssemblyList()
        {
            PlanogramAssemblyList item = new PlanogramAssemblyList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Removes the given assembly and all of its dependants from 
        /// the parent planogram.
        /// </summary>
        /// <param name="assembly">The assembly to remove</param>
        public void RemoveAssemblyAndDependants(PlanogramAssembly assembly)
        {
            //if we have no parent then just return.
            if (this.Parent == null)
            {
                Remove(assembly);
                return;
            } 

            //first remove all fixture assemblies referencing this assembly
            List<Object> removedFixtureAssemblyIds = new List<Object>();
            foreach (PlanogramFixture fixture in this.Parent.Fixtures)
            {
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies.ToArray())
                {
                    if (Object.Equals(fixtureAssembly.PlanogramAssemblyId, assembly.Id))
                    {
                        removedFixtureAssemblyIds.Add(fixtureAssembly.Id);
                        fixture.Assemblies.Remove(fixtureAssembly);
                    }
                }
            }

            //remove positions
            this.Parent.Positions.RemoveList(
                this.Parent.Positions.Where(p => 
                    p.PlanogramFixtureAssemblyId != null
                    && removedFixtureAssemblyIds.Contains(p.PlanogramFixtureAssemblyId))
                    .ToList());

            //remove annotations.
            this.Parent.Annotations.RemoveList(
                this.Parent.Annotations.Where(a => 
                    a.PlanogramFixtureAssemblyId != null
                    && removedFixtureAssemblyIds.Contains(a.PlanogramFixtureAssemblyId))
                    .ToList());



            //Remove all child components that are only linked to this assembly
            List<Object> referencedComponentIds = 
                this.Parent.Fixtures.SelectMany(f=> f.Components.Select(c=> c.Id))
                .Union(this.Parent.Assemblies.Where(a=> a!= assembly).SelectMany(a=> a.Components.Select(c=> c.Id)))
                .ToList();

            List<PlanogramComponent> planComponent =
                assembly.Components
                .Where(c=> !referencedComponentIds.Contains(c.PlanogramComponentId))
                .Select(c => c.GetPlanogramComponent()).ToList();

            foreach (PlanogramComponent component in planComponent)
            {
                //remove the component and all of its dependants.
                this.Parent.Components.RemoveComponentAndDependants(component);
            }

            //finally remove this assembly.
            Remove(assembly);
        }

        #endregion
    }
}
