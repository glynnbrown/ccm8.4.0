﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of event logs contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramEventLogList : ModelList<PlanogramEventLogList, PlanogramEventLog>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        public static PlanogramEventLogList NewPlanogramEventLogList()
        {
            PlanogramEventLogList item = new PlanogramEventLogList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        public PlanogramEventLog Add()
        {
            PlanogramEventLog item = PlanogramEventLog.NewPlanogramEventLog();
            this.Add(item);
            return item;
        }

        #endregion
    }
}
