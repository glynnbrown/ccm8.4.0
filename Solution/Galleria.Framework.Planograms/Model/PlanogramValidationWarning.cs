﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25976 : L.Ineson
//  Created
#endregion
#region Version History: CCM820
// V8-30705 : A.Probyn
//  Extended to support assortment warnings with no positions.
#endregion

#region Version History: CCM830

// V8-32593 : A.Silva
//  Added SourceAssemblyComponent and amended the factory method to store it. 

#endregion

#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Represents a general validation warning raised against a planogram
    /// </summary>
    [Serializable]
    public sealed class PlanogramValidationWarning : ModelReadOnlyObject<PlanogramValidationWarning>
    {
        #region Properties

        #region SourceFixtureItem

        /// <summary>
        /// SourceFixtureItem property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureItem> SourceFixtureItemProperty =
             RegisterModelProperty<PlanogramFixtureItem>(c => c.SourceFixtureItem);
        /// <summary>
        /// Gets the fixture assembly model object that this warning originated from.
        /// </summary>
        public PlanogramFixtureItem SourceFixtureItem
        {
            get { return this.GetProperty<PlanogramFixtureItem>(SourceFixtureItemProperty); }
        }

        #endregion

        #region SourceFixtureAssembly

        /// <summary>
        /// SourceFixtureAssembly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureAssembly> SourceFixtureAssemblyProperty =
             RegisterModelProperty<PlanogramFixtureAssembly>(c => c.SourceFixtureAssembly);
        /// <summary>
        /// Gets the fixture assembly model object that this warning originated from.
        /// </summary>
        public PlanogramFixtureAssembly SourceFixtureAssembly
        {
            get { return this.GetProperty<PlanogramFixtureAssembly>(SourceFixtureAssemblyProperty); }
        }

        #endregion

        #region SourceFixtureComponent

        /// <summary>
        /// SourceFixtureComponent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureComponent> SourceFixtureComponentProperty =
             RegisterModelProperty<PlanogramFixtureComponent>(c => c.SourceFixtureComponent);
        /// <summary>
        /// Gets the fixture assembly model object that this warning originated from.
        /// </summary>
        public PlanogramFixtureComponent SourceFixtureComponent
        {
            get { return this.GetProperty<PlanogramFixtureComponent>(SourceFixtureComponentProperty); }
        }

        #endregion

        #region SourceAssemblyComponent

        /// <summary>
        /// SourceAssemblyComponent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssemblyComponent> SourceAssemblyComponentProperty =
             RegisterModelProperty<PlanogramAssemblyComponent>(c => c.SourceAssemblyComponent);
        /// <summary>
        /// Gets the assembly component model object that this warning originated from.
        /// </summary>
        public PlanogramAssemblyComponent SourceAssemblyComponent
        {
            get { return this.GetProperty<PlanogramAssemblyComponent>(SourceAssemblyComponentProperty); }
        }

        #endregion

        #region SourcePosition

        /// <summary>
        /// SourcePosition property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPosition> SourcePositionProperty =
            RegisterModelProperty<PlanogramPosition>(c => c.SourcePosition);
        /// <summary>
        /// Gets the position model object that this warning originated from.
        /// </summary>
        public PlanogramPosition SourcePosition
        {
            get { return this.GetProperty<PlanogramPosition>(SourcePositionProperty); }
        }
        #endregion

        #region ProductAttributeComparisonResult

        /// <summary>
        /// SourceProduct property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProductAttributeComparisonResult> ProductAttributeComparisonResultProperty =
             RegisterModelProperty<ProductAttributeComparisonResult>(c => c.ProductAttributeComparisonResult);
        /// <summary>
        /// Gets the fixture assembly model object that this warning originated from.
        /// </summary>
        public ProductAttributeComparisonResult ProductAttributeComparisonResult
        {
            get { return this.GetProperty<ProductAttributeComparisonResult>(ProductAttributeComparisonResultProperty); }
        }

        #endregion

        #region SourceProduct

        /// <summary>
        /// SourceProduct property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProduct> SourceProductProperty =
             RegisterModelProperty<PlanogramProduct>(c => c.SourceProduct);
        /// <summary>
        /// Gets the fixture assembly model object that this warning originated from.
        /// </summary>
        public PlanogramProduct SourceProduct
        {
            get { return this.GetProperty<PlanogramProduct>(SourceProductProperty); }
        }

        #endregion

        #region WarningType

        /// <summary>
        /// WarningType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramValidationWarningType> WarningTypeProperty =
            RegisterModelProperty<PlanogramValidationWarningType>(c => c.WarningType);
        /// <summary>
        /// Gets the type of warning that has been raised
        /// </summary>
        public PlanogramValidationWarningType WarningType
        {
            get { return this.GetProperty<PlanogramValidationWarningType>(WarningTypeProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //none.
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramValidationWarning NewPlanogramValidationWarning(PlanogramValidationWarningType warningType, PlanogramPosition sourcePosition)
        {
            PlanogramValidationWarning item = new PlanogramValidationWarning();
            item.Create(warningType, sourcePosition);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramValidationWarning NewPlanogramValidationWarning(PlanogramValidationWarningType warningType, PlanogramProduct sourceProduct)
        {
            PlanogramValidationWarning item = new PlanogramValidationWarning();
            item.Create(warningType, sourceProduct);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramValidationWarning NewPlanogramValidationWarning(PlanogramValidationWarningType warningType, PlanogramProduct sourceProduct, ProductAttributeComparisonResult result)
        {
            PlanogramValidationWarning item = new PlanogramValidationWarning();
            item.Create(warningType, sourceProduct, result);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramValidationWarning NewPlanogramValidationWarning(PlanogramValidationWarningType warningType, PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureAssembly sourceFixtureAssembly, PlanogramAssemblyComponent sourceAssemblyComponent)
        {
            PlanogramValidationWarning item = new PlanogramValidationWarning();
            item.Create(warningType, sourceFixtureItem, sourceFixtureAssembly, sourceAssemblyComponent);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public static PlanogramValidationWarning NewPlanogramValidationWarning(PlanogramValidationWarningType warningType,
            PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureComponent sourceFixtureComponent)
        {
            PlanogramValidationWarning item = new PlanogramValidationWarning();
            item.Create(warningType, sourceFixtureItem, sourceFixtureComponent);
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private void Create(PlanogramValidationWarningType warningType, PlanogramPosition sourcePosition)
        {
            this.LoadProperty<PlanogramValidationWarningType>(WarningTypeProperty, warningType);
            this.LoadProperty<PlanogramPosition>(SourcePositionProperty, sourcePosition);
        }

        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private void Create(PlanogramValidationWarningType warningType, PlanogramProduct sourceProduct)
        {
            this.LoadProperty<PlanogramValidationWarningType>(WarningTypeProperty, warningType);
            this.LoadProperty<PlanogramProduct>(SourceProductProperty, sourceProduct);
        }

        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private void Create(PlanogramValidationWarningType warningType, PlanogramProduct sourceProduct, ProductAttributeComparisonResult compartisonResult)
        {
            this.LoadProperty<PlanogramValidationWarningType>(WarningTypeProperty, warningType);
            this.LoadProperty<PlanogramProduct>(SourceProductProperty, sourceProduct);
            this.LoadProperty<ProductAttributeComparisonResult>(ProductAttributeComparisonResultProperty, compartisonResult);
        }
        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private void Create(PlanogramValidationWarningType warningType, PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureAssembly sourceFixtureAssembly, PlanogramAssemblyComponent sourceAssemblyComponent)
        {
            this.LoadProperty<PlanogramValidationWarningType>(WarningTypeProperty, warningType);
            this.LoadProperty<PlanogramFixtureItem>(SourceFixtureItemProperty, sourceFixtureItem);
            this.LoadProperty<PlanogramFixtureAssembly>(SourceFixtureAssemblyProperty, sourceFixtureAssembly);
            this.LoadProperty<PlanogramAssemblyComponent>(SourceAssemblyComponentProperty, sourceAssemblyComponent);
        }


        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private void Create(PlanogramValidationWarningType warningType, PlanogramFixtureItem sourceFixtureItem, PlanogramFixtureComponent sourceFixtureComponent)
        {
            this.LoadProperty<PlanogramValidationWarningType>(WarningTypeProperty, warningType);
            this.LoadProperty<PlanogramFixtureItem>(SourceFixtureItemProperty, sourceFixtureItem);
            this.LoadProperty<PlanogramFixtureComponent>(SourceFixtureComponentProperty, sourceFixtureComponent);
        }


        #endregion
    }
}
