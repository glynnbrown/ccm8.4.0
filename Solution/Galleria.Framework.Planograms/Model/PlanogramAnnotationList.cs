﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of Annotations contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramAnnotationList : ModelList<PlanogramAnnotationList, PlanogramAnnotation>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAnnotationList NewPlanogramAnnotationList()
        {
            PlanogramAnnotationList item = new PlanogramAnnotationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates and adds a new item to this collection
        /// </summary>
        public PlanogramAnnotation Add(PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            PlanogramAnnotation annotation = PlanogramAnnotation.NewPlanogramAnnotation(fixtureItem, settings);
            this.Add(annotation);
            return annotation;
        }

        /// <summary>
        /// Creates and adds a new item to this collection
        /// </summary>
        public PlanogramAnnotation Add(PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem fixtureItem, IPlanogramSettings settings)
        {
            PlanogramAnnotation annotation = PlanogramAnnotation.NewPlanogramAnnotation(fixtureComponent, fixtureItem, settings);
            this.Add(annotation);
            return annotation;
        }
        #endregion
    }
}