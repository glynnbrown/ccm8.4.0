﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550: A.Probyn
//	Copied from SA.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductBuddyTreatmentType
    {
        Avg,
        Sum,
        TopPercentAvg,
        BottomPercentAvg
    }

    public static class PlanogramAssortmentProductBuddyTreatmentTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String>()
            {
                {PlanogramAssortmentProductBuddyTreatmentType.Avg, Message.Enum_PlanogramAssortmentProductBuddyTreatmentType_Avg},
                {PlanogramAssortmentProductBuddyTreatmentType.Sum, Message.Enum_PlanogramAssortmentProductBuddyTreatmentType_Sum},
                {PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg, Message.Enum_PlanogramAssortmentProductBuddyTreatmentType_TopPercentAvg},
                {PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg, Message.Enum_PlanogramAssortmentProductBuddyTreatmentType_BottomPercentAvg},
            };

        /// <summary>
        /// Returns the matching enum value from the specified friendlyname
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramAssortmentProductBuddyTreatmentType? ParseFromFriendlyName(String friendlyName, Boolean closestMatch = false)
        {
            String cleanFriendlyName = friendlyName.ToLowerInvariant().Trim();

            foreach (KeyValuePair<PlanogramAssortmentProductBuddyTreatmentType, String> keyPair in PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.ToLowerInvariant().Equals(cleanFriendlyName))
                {
                    return keyPair.Key;
                }
            }

            if (closestMatch)
            {
                foreach (KeyValuePair<PlanogramAssortmentProductBuddyTreatmentType, String> keyPair in PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames)
                {
                    if (keyPair.Value.ToLowerInvariant().Contains(cleanFriendlyName))
                    {
                        return keyPair.Key;
                    }
                }
            }
            return null;
        }

    }
}
