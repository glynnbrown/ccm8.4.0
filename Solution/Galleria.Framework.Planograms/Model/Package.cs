﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25546 : N.Foster
//  Added check for operation in progress before saving
// V8-25653 : L.Ineson
//  Added SaveAs method
// V8-25916 : N.Foster
//  Added RowVersion, DateDeleted, Removed CreatedBy, LastModifiedBy, Description
// V8-25949 : N.Foster
//  Implemented package locking
// V8-25881 : A.Probyn
//  Replaced PlanogramCount with MetaPlanogramCount
// V8-27062 : A.Silva
//  Implemented CalculateValidationData so that it serves as an entry point for the validation inside the Package.
// V8-27058 : A.Probyn 
//  Extended for CalculateMetaData and ClearMetaData
//  Added DateMetadataCalculated
// V8-27229 : A.Silva
//  Modified CalculateValidationData so that it calculates metadata if needed before running the actual validation process.
// V8-27237 : A.Silva
//  Added DateMetadataCalculated.
//  Extended for ClearValidationData. Clearing Metadata clears Validation Data too.
// V8-27490 : N.Foster
//  Refactory fast-lookup code and moved into framework
// V8-27411 : M.Pettit
//  FetchCriteria now has an additional FetchReadOnly property
// V8-27411 : M.Pettit
//  Package locking now requires userId and locktype
// V8-28147 : D.Pleasance
//  Added business rule for UserName
// V8-28332 : A.Kuszyk
//  Added ClearValidationData to Save methods when Metadata is cleared.
// V8-28515 : L.Ineson
//  Commented out username required business rule
// V8-28535 : L.Ineson
//  Added delete command
#endregion
#region Version History: CCM802
// V8-28840 : L.Luong
//  Added EntityId
// V8-29230 : N.Foster
//  Update username when performing a save as
#endregion
#region Version History: CCM803
// V8-29384 : N.Foster
//  When copying a read-only package, ensure the copy is not read-only as well
#endregion
#region Version History: CCM810
// V8-28242 : M.shelley
//  Added a method to allow bulk update of planogram attributes.
//  Currently these are the DateApproved, DateArchived, DateWip and Status values
// V8-29980 : M.Brumby
//  Added an extra CalculateValidationData method to call if we are wanting to update meta data in
//  the same way that CalculateMetadata does. The OnCalculateValidationData nolonger tries to create
//  metadata.
// V8-29590 : L.Ineson
//  Planogram event log metadata now gets calculated before save.
// V8-29533 : D.Pleasance
//  Added method override for TryUnlockPackageById so that FileSystem package types can also be unlocked
// V8-29173 : N.Haywood
//  Moved CalculateEventLogMetadata to outside the clearMetadata in save
#endregion
#region Version History: CCM811
// V8-30561 : N.Foster
//  Added override to allow data last modified to not be updated
#endregion
#region Version History: CCM820
// V8-31483 : N.Foster
//  Improved performance of validation data calculation
#endregion
#region Version History: CCM830
// V8-31547 : M.Pettit
//  Added ExportToExternalFile method
//  Added FieldMappings property
//  Altered SaveAs for external file types (JDA, Apollo etc) to use an Export functional methodology
// CCM-13446 : R.Cooper
//  Added SaveAsCopy method to prevent DateLastModified being set to the current UTC time.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Csla.Core;
using Csla.Rules.CommonRules;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Defines a planogram package.
    /// A package contains a group of planograms and
    /// their associated data. A package could represent
    /// a file or group of planograms within a database.
    /// </summary>
    [Serializable]
    public sealed partial class Package : ModelObject<Package>, IDisposable
    {
        #region Private Fields

        private Boolean _isDisposed;
        private Boolean _processMetadataImages = false;
        private IPlanogramMetadataHelper _metadataHelper = null;

        #endregion

        #region Properties

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override string DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region PackageType
        /// <summary>
        /// PackageType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PackageType> PackageTypeProperty =
            RegisterModelProperty<PackageType>(c => c.PackageType);
        /// <summary>
        /// Returns the package type
        /// </summary>
        public PackageType PackageType
        {
            get { return this.GetProperty<PackageType>(PackageTypeProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the package rowversion
        /// </summary>
        public RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> EntityIdProperty =
            RegisterModelProperty<Object>(c => c.EntityId);
        /// <summary>
        /// Gets or sets the EntityId
        /// </summary>
        public Object EntityId
        {
            get { return this.GetProperty<Object>(EntityIdProperty); }
            set { this.SetProperty<Object>(EntityIdProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the package name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region UserName
        /// <summary>
        /// UserName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        /// <summary>
        /// Gets or sets the package UserName (owner)
        /// </summary>
        public String UserName
        {
            get { return this.GetProperty<String>(UserNameProperty); }
            set { this.SetProperty<String>(UserNameProperty, value); }
        }
        #endregion

        #region DateCreated
        /// <summary>
        /// Date Created property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// The date and time the file was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.GetProperty<DateTime>(DateCreatedProperty); }
        }
        #endregion

        #region DateLastModified
        /// <summary>
        /// Date Last Modified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// The date and time the file was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return this.GetProperty<DateTime>(DateLastModifiedProperty); }
        }
        #endregion

        #region DateDeleted
        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        /// <summary>
        /// Returns the date deleted
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return this.GetProperty<DateTime?>(DateDeletedProperty); }
        }
        #endregion

        #region Planograms
        /// <summary>
        /// Planograms property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramList> PlanogramsProperty =
            RegisterModelProperty<PlanogramList>(c => c.Planograms, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the planograms within this package
        /// </summary>
        public PlanogramList Planograms
        {
            get
            {
                return this.GetPropertyLazy<PlanogramList>(
                    PlanogramsProperty,
                    new PlanogramList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the planograms within this package
        /// </summary>
        public PlanogramList PlanogramsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramList>(
                    PlanogramsProperty,
                    new PlanogramList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region MetaPlanogramCount
        /// <summary>
        /// MetaPlanogramCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaPlanogramCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaPlanogramCount);
        /// <summary>
        /// The meta data representing number of planograms within the file
        /// </summary>
        public Int32? MetaPlanogramCount
        {
            get { return this.GetProperty<Int32?>(MetaPlanogramCountProperty); }
            set { this.SetProperty<Int32?>(MetaPlanogramCountProperty, value); }
        }
        #endregion

        #region DateMetadataCalculated
        /// <summary>
        /// DateMetadataCalculated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateMetadataCalculatedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateMetadataCalculated);
        /// <summary>
        /// The date the meta data was last calculated
        /// </summary>
        public DateTime? DateMetadataCalculated
        {
            get { return this.GetProperty<DateTime?>(DateMetadataCalculatedProperty); }
            set { this.SetProperty<DateTime?>(DateMetadataCalculatedProperty, value); }
        }
        #endregion

        #region DateValidationDataCalculated

        /// <summary>
        ///		Metadata for the <see cref="DateValidationDataCalculated"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime?> DateValidationDataCalculatedProperty =
            RegisterModelProperty<DateTime?>(o => o.DateValidationDataCalculated);

        /// <summary>
        ///		Gets or sets the value for the <see cref="DateValidationDataCalculated"/> property.
        /// </summary>
        public DateTime? DateValidationDataCalculated
        {
            get { return this.GetProperty<DateTime?>(DateValidationDataCalculatedProperty); }
            set { this.SetProperty<DateTime?>(DateValidationDataCalculatedProperty, value); }
        }

        #endregion

        #region IsReadOnly
        /// <summary>
        /// IsReadOnly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsReadOnly);
        /// <summary>
        /// Indicates if the package was opened read-only
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }
        #endregion

        #region IncludeImages
        /// <summary>
        /// Helper property for meta data to include images
        /// </summary>
        internal Boolean ProcessMetadataImages
        {
            get { return _processMetadataImages; }
        }
        #endregion

        #region MetadataHelper

        public IPlanogramMetadataHelper metadataHelper
        {
            get { return _metadataHelper; }
        }

        #endregion

        #region LockType
        /// <summary>
        /// LockType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PackageLockType> LockTypeProperty =
            RegisterModelProperty<PackageLockType>(c => c.LockType);
        /// <summary>
        /// Holds the type of lock that must be applied to the package, depending on whether the package
        /// is opened by a system engine process or a user. (Since the engine can run under the current user
        /// account we cannot just use the user id for this)
        /// </summary>
        public PackageLockType LockType
        {
            get { return this.ReadProperty<PackageLockType>(LockTypeProperty); }
        }
        #endregion

        #region LockUserId
        /// <summary>
        /// LockUserId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> LockUserIdProperty =
            RegisterModelProperty<Object>(c => c.LockUserId);
        /// <summary>
        /// Holds the id of the user that locked the package
        /// </summary>
        /// <remarks>Note that this may not be the same person who created the package</remarks>
        public Object LockUserId
        {
            get { return this.ReadProperty<Object>(LockUserIdProperty); }
        }
        #endregion

        #region FieldMappings
        /// <summary>
        /// FieldMappings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> FieldMappingsProperty =
            RegisterModelProperty<Object>(c => c.FieldMappings);

        /// <summary>
        /// Gets or sets the object field mappings
        /// </summary>
        protected Object FieldMappings
        {
            get { return ReadProperty(FieldMappingsProperty); }
            set { LoadProperty(FieldMappingsProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(IdProperty));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            // V8-28515 - removed so that we can save plans without a repository connection
            //BusinessRules.AddRule(new Required(UserNameProperty));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria
        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchCriteria : CriteriaBase<FetchCriteria>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageType> PackageTypeProperty =
                RegisterProperty<PackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package type
            /// </summary>
            public PackageType PackageType
            {
                get { return this.ReadProperty<PackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region LockUserId
            /// <summary>
            /// LockUserId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> LockUserIdProperty =
                RegisterProperty<Object>(c => c.LockUserId);
            /// <summary>
            /// Holds the id of the user that locked the package
            /// </summary>
            public Object LockUserId
            {
                get { return this.ReadProperty<Object>(LockUserIdProperty); }
            }
            #endregion

            #region LockType
            /// <summary>
            /// LockType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageLockType> LockTypeProperty =
                RegisterProperty<PackageLockType>(c => c.LockType);
            /// <summary>
            /// Returns the lock type
            /// </summary>
            public PackageLockType LockType
            {
                get { return this.ReadProperty<PackageLockType>(LockTypeProperty); }
            }
            #endregion

            #region LockReadOnly
            /// <summary>
            /// FetchReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> LockReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.LockReadOnly);
            /// <summary>
            /// Returns the package type
            /// </summary>
            public Boolean LockReadOnly
            {
                get { return this.ReadProperty<Boolean>(LockReadOnlyProperty); }
            }
            #endregion

            #region FetchArgument
            /// <summary>
            /// FetchArgument property definition
            /// </summary>
            public static readonly PropertyInfo<Object> FetchArgumentProperty =
                RegisterProperty<Object>(c => c.FetchArgument);
            /// <summary>
            /// Returns the optional fetch argument.
            /// </summary>
            public Object FetchArgument
            {
                get { return this.ReadProperty<Object>(FetchArgumentProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria(PackageType sourceType, Object id,
                Object lockUserId, PackageLockType lockType, Boolean lockReadOnly)
                : this(sourceType, id, lockUserId, lockType, lockReadOnly, null)
            { }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria(PackageType sourceType, Object id,
                Object lockUserId, PackageLockType lockType, Boolean lockReadOnly,
                Object fetchArgument)
            {
                this.LoadProperty<PackageType>(PackageTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
                this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
                this.LoadProperty<Boolean>(LockReadOnlyProperty, lockReadOnly);
                this.LoadProperty<Object>(FetchArgumentProperty, fetchArgument);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods

        #region Lock
        /// <summary>
        /// Locks a package so that it cannot be
        /// edited or opened by another process
        /// </summary>
        public static PackageLockResult LockPackageById(Object id, Object lockUserId, PackageLockType lockType)
        {
            return DataPortal.Execute<LockPackageCommand>(new LockPackageCommand(PackageType.Unknown, id, lockUserId, lockType, false)).Result;
        }
        #endregion

        #region Unlock

        /// <summary>
        /// Tries to unlock the package with the given id.
        /// </summary>
        public static Boolean TryUnlockPackageById(Object id, Object lockUserId, PackageLockType lockType)
        {
            Boolean success = false;
            try
            {
                PackageUnlockResult result = UnlockPackageById(id, lockUserId, lockType);
                success = (result == PackageUnlockResult.Success);
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Tries to unlock the package with the given id.
        /// </summary>
        public static Boolean TryUnlockPackageById(PackageType packageType, Object id, Object lockUserId, PackageLockType lockType)
        {
            Boolean success = false;
            try
            {
                PackageUnlockResult result = UnlockPackageById(packageType, id, lockUserId, lockType, false);
                success = (result == PackageUnlockResult.Success);
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Unlocks a package
        /// </summary>
        public static PackageUnlockResult UnlockPackageById(Object id, Object lockUserId, PackageLockType lockType)
        {
            return DataPortal.Execute<UnlockPackageCommand>(new UnlockPackageCommand(PackageType.Unknown, id, lockUserId, lockType, false)).Result;
        }

        /// <summary>
        /// Unlocks a package
        /// </summary>
        private static PackageUnlockResult UnlockPackageById(PackageType packageType, Object id, Object lockUserId, PackageLockType lockType, Boolean lockReadOnly)
        {
            return DataPortal.Execute<UnlockPackageCommand>(new UnlockPackageCommand(packageType, id, lockUserId, lockType, lockReadOnly)).Result;
        }

        #endregion

        #region New
        /// <summary>
        /// Returns a new package
        /// </summary>
        public static Package NewPackage(Object lockUserId, PackageLockType lockType)
        {
            Package item = new Package();
            item.Create(lockUserId, lockType);
            return item;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes the package with the given id.
        /// </summary>
        public static void DeletePackageById(Object id, Object lockUserId, PackageLockType lockType)
        {
            DataPortal.Execute<DeletePackageByIdCommand>(new DeletePackageByIdCommand(PackageType.Unknown, id, lockUserId, lockType));
        }

        /// <summary>
        /// Deletes the package with the given filename
        /// </summary>
        public static void DeletePackageByFilename(String filename, Object lockUserId, PackageLockType lockType)
        {
            DataPortal.Execute<DeletePackageByIdCommand>(new DeletePackageByIdCommand(PackageType.FileSystem, filename, lockUserId, lockType));
        }

        #endregion

        #region Update Attributes

        /// <summary>
        /// Tries to update planogram attributes
        /// </summary>
        public static Boolean TryUpdatePlanogramAttributes(IEnumerable<PlanogramAttributes> planAttributesList)
        {
            Byte returnStatus;

            returnStatus = DataPortal.Execute<UpdatePlanogramAttributesCommand>(new UpdatePlanogramAttributesCommand(planAttributesList)).Result;

            return (returnStatus == 0);
        }

        #endregion

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Object lockUserId, PackageLockType lockType)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramList>(PlanogramsProperty, PlanogramList.NewPlanogramList());
            this.LoadProperty<String>(NameProperty, Message.Package_Name_Default);
            this.LoadProperty<String>(UserNameProperty, ApplicationContext.User.Identity.Name);
            this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
            this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
            base.Create();
        }
        #endregion

        #endregion

        #region Commands

        #region LockPackageCommand
        /// <summary>
        /// Performs the locking of a package
        /// </summary>
        [Serializable]
        private partial class LockPackageCommand : CommandBase<LockPackageCommand>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageType> PackageTypeProperty =
                RegisterProperty<PackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public PackageType PackageType
            {
                get { return this.ReadProperty<PackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region LockUserId
            /// <summary>
            /// LockUserId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> LockUserIdProperty =
                RegisterProperty<Object>(c => c.UserId);
            /// <summary>
            /// Returns the Lock User Id
            /// </summary>
            public Object UserId
            {
                get { return this.ReadProperty<Object>(LockUserIdProperty); }
            }
            #endregion

            #region LockType
            /// <summary>
            /// LockType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageLockType> LockTypeProperty =
                RegisterProperty<PackageLockType>(c => c.LockType);
            /// <summary>
            /// Returns the Lock Type (user or system)
            /// </summary>
            public PackageLockType LockType
            {
                get { return this.ReadProperty<PackageLockType>(LockTypeProperty); }
            }
            #endregion

            #region LockReadOnly
            /// <summary>
            /// ReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> LockReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.LockReadOnly);
            /// <summary>
            /// Indicates if we are attempting to obtain a read only lock
            /// </summary>
            public Boolean LockReadOnly
            {
                get { return this.ReadProperty<Boolean>(LockReadOnlyProperty); }
            }
            #endregion

            #region Result
            /// <summary>
            /// Result property definition
            /// </summary>
            public static readonly PropertyInfo<PackageLockResult> ResultProperty =
                RegisterProperty<PackageLockResult>(c => c.Result);
            /// <summary>
            /// Indicates the outcome of the lock attempt
            /// </summary>
            public PackageLockResult Result
            {
                get { return this.ReadProperty<PackageLockResult>(ResultProperty); }
                private set { this.LoadProperty<PackageLockResult>(ResultProperty, value); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockPackageCommand(PackageType packageType, Object id, Object lockUserId, PackageLockType lockType, Boolean lockReadOnly)
            {
                this.LoadProperty<PackageType>(PackageTypeProperty, packageType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
                this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
                this.LoadProperty<Boolean>(LockReadOnlyProperty, lockReadOnly);
            }
            #endregion
        }
        #endregion

        #region UnlockPackageCommand
        /// <summary>
        /// Performs the unlocking of a package
        /// </summary>
        [Serializable]
        private partial class UnlockPackageCommand : CommandBase<UnlockPackageCommand>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageType> PackageTypeProperty =
                RegisterProperty<PackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public PackageType PackageType
            {
                get { return this.ReadProperty<PackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region LockUserId
            /// <summary>
            /// LockUserId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> LockUserIdProperty =
                RegisterProperty<Object>(c => c.LockUserId);
            /// <summary>
            /// Returns the Lock User Id
            /// </summary>
            public Object LockUserId
            {
                get { return this.ReadProperty<Object>(LockUserIdProperty); }
            }
            #endregion

            #region LockType
            /// <summary>
            /// LockType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageLockType> LockTypeProperty =
                RegisterProperty<PackageLockType>(c => c.LockType);
            /// <summary>
            /// Returns the Lock Type (user or system)
            /// </summary>
            public PackageLockType LockType
            {
                get { return this.ReadProperty<PackageLockType>(LockTypeProperty); }
            }
            #endregion

            #region LockReadOnly
            /// <summary>
            /// LockReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> LockReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.LockReadOnly);
            /// <summary>
            /// Indicates if the package was originally locked read-only
            /// </summary>
            public Boolean LockReadOnly
            {
                get { return this.ReadProperty<Boolean>(LockReadOnlyProperty); }
            }
            #endregion

            #region Result
            /// <summary>
            /// Result property definition
            /// </summary>
            public static readonly PropertyInfo<PackageUnlockResult> ResultProperty =
                RegisterProperty<PackageUnlockResult>(c => c.Result);
            /// <summary>
            /// Indicates the outcome of the lock attempt
            /// </summary>
            public PackageUnlockResult Result
            {
                get { return this.ReadProperty<PackageUnlockResult>(ResultProperty); }
                private set { this.LoadProperty<PackageUnlockResult>(ResultProperty, value); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockPackageCommand(PackageType packageType, Object id, Object lockUserId, PackageLockType lockType, Boolean lockReadOnly)
            {
                this.LoadProperty<PackageType>(PackageTypeProperty, packageType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
                this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
                this.LoadProperty<Boolean>(LockReadOnlyProperty, lockReadOnly);
            }
            #endregion
        }
        #endregion

        #region DeletePackageByIdCommand

        /// <summary>
        /// Deletes the package with the given id.
        /// </summary>
        [Serializable]
        private partial class DeletePackageByIdCommand : CommandBase<DeletePackageByIdCommand>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageType> PackageTypeProperty =
                RegisterProperty<PackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public PackageType PackageType
            {
                get { return this.ReadProperty<PackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region LockUserId
            /// <summary>
            /// LockUserId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> LockUserIdProperty =
                RegisterProperty<Object>(c => c.UserId);
            /// <summary>
            /// Returns the Lock User Id
            /// </summary>
            public Object UserId
            {
                get { return this.ReadProperty<Object>(LockUserIdProperty); }
            }
            #endregion

            #region LockType
            /// <summary>
            /// LockType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageLockType> LockTypeProperty =
                RegisterProperty<PackageLockType>(c => c.LockType);
            /// <summary>
            /// Returns the Lock Type (user or system)
            /// </summary>
            public PackageLockType LockType
            {
                get { return this.ReadProperty<PackageLockType>(LockTypeProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="packageType">the type of package to delete</param>
            /// <param name="id">the id of the package to delete</param>
            /// <param name="lockUserId">the id of the user performing the delete</param>
            /// <param name="lockType">the type of lock to place before deleting.</param>
            public DeletePackageByIdCommand(PackageType packageType, Object id, Object lockUserId, PackageLockType lockType)
            {
                this.LoadProperty<PackageType>(PackageTypeProperty, PackageType.Unknown);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
                this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
            }

            #endregion
        }

        #endregion

        #region UpdatePlanogramAttributesCommand
        
        /// <summary>
        /// Performs the updating of planogram attribute values
        /// </summary>
        [Serializable]
        private sealed partial class UpdatePlanogramAttributesCommand : CommandBase<UpdatePlanogramAttributesCommand>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<PackageType> PackageTypeProperty =
                RegisterProperty<PackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public PackageType PackageType
            {
                get { return this.ReadProperty<PackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AttributesList property

            /// <summary>
            /// AttributesList property definition
            /// </summary>
            public static readonly PropertyInfo<IEnumerable<PlanogramAttributes>> AttributesListProperty =
                RegisterProperty<IEnumerable<PlanogramAttributes>>(c => c.AttributesList);
            /// <summary>
            /// Gets/Sets the list of attibutes to update
            /// </summary>
            public IEnumerable<PlanogramAttributes> AttributesList
            {
                get { return this.ReadProperty<IEnumerable<PlanogramAttributes>>(AttributesListProperty); }
            }

            #region Result property

            /// <summary>
            /// Result property definition
            /// </summary>
            public static readonly PropertyInfo<Byte> ResultProperty =
                RegisterProperty<Byte>(c => c.Result);
            /// <summary>
            /// Gets/Sets the result of the update.
            /// </summary>
            public Byte Result
            {
                get { return this.ReadProperty<Byte>(ResultProperty); }
                private set { this.LoadProperty<Byte>(ResultProperty, value); }
            }

            #endregion

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UpdatePlanogramAttributesCommand(IEnumerable<PlanogramAttributes> attributes)
            {
                this.LoadProperty<PackageType>(PackageTypeProperty, PackageType.Unknown);
                this.LoadProperty(AttributesListProperty, attributes);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Fetch a list of Ids corresponding to ucrs not contained in the given <paramref name="ucrs"/> list.
        /// </summary>
        /// <param name="ucrs">List of ucrs for packages whose ids we do NOT want back.</param>
        /// <returns></returns>
        public static IList<Object> FetchIdsExceptFor(IEnumerable<String> ucrs)
        {
            IDalFactory dalFactory = new Package().GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
            {
                return dal.FetchIdsExceptFor(ucrs);
            }
        }

        /// <summary>
        /// Fetch a list of Ids for plans that have been modified from the given <paramref name="Ucrs"/> list.
        /// </summary>
        /// <param name="ucrs">List of Ids of plans we require.</param>
        /// <returns></returns>
        public static IList<Object> FetchPlansModifiedAfter(IEnumerable<String> Ucrs, DateTime dateLastChanged)
        {
            IDalFactory dalFactory = new Package().GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
            {
                return dal.FetchByUcrsDateLastModified(Ucrs, dateLastChanged);
            }
        }

        /// <summary>
        /// Fetch a list of UCRs that have been deleted from the database that are included in the given UCR list
        /// </summary>
        /// <returns></returns>
        public static IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            IDalFactory dalFactory = new Package().GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
            {
                return dal.FetchDeletedByUcrs(ucrs);
            }
        }

        

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            // update the id
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<Package>(oldId, newId);

            // ensure the planogram is no longer read-only
            this.LoadProperty<Boolean>(IsReadOnlyProperty, false);
        }
        #endregion

        #region Save
        /// <summary>
        /// Called when saving this package
        /// </summary>
        public override Package Save()
        {
            return this.Save(true, true);
        }

        /// <summary>
        /// Called when saving this package
        /// </summary>
        public Package Save(Boolean clearMetadata)
        {
            return this.Save(clearMetadata, true);
        }

        /// <summary>
        /// Called when saving this package
        /// </summary>
        /// <param name="clearMetadata">Indicates if metadata should be cleared</param>
        /// <param name="updateDateLastModified">Indicates if the last modified date should be updated</param>
        public Package Save(Boolean clearMetadata, Boolean updateDateLastModified)
        {
            // if the package is read only, then throw an exception
            if (this.IsReadOnly) throw new PackageReadOnlyException();

            // clear metadata if required
            if (clearMetadata)
            {
                this.ResetMetadata();
                this.ClearValidationData();
            }

            // update the last modified date if reqired
            if (updateDateLastModified)
            {
                this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            }

            //ensure that the metadata fields about the event log are calculated,
            // as we need these to always be populated.
            foreach (Planogram planogram in this.Planograms)
            {
                planogram.CalculateEventLogMetadata();
            }

            // perform the save
            return base.Save();
        }

        /// <summary>
        /// Called when saving this package
        /// </summary>
        public override void BeginSave(Boolean forceUpdate, EventHandler<SavedEventArgs> handler, Object userState)
        {
            this.BeginSave(forceUpdate, handler, userState, true, true);
        }

        /// <summary>
        /// Called when saving this package
        /// </summary>
        public void BeginSave(Boolean forceUpdate, EventHandler<SavedEventArgs> handler, Object userState, Boolean clearMetadata, Boolean updateDateLastModified)
        {
            // if the package is read only, then throw an exception
            if (this.IsReadOnly) throw new PackageReadOnlyException();

            // clear metadata if required
            if (clearMetadata)
            {
                this.ResetMetadata();
                this.ClearValidationData();

                //ensure that the metadata fields about the event log are calculated,
                // as we need these to always be populated.
                foreach (Planogram planogram in this.Planograms)
                {
                    planogram.CalculateEventLogMetadata();
                }
            }

            // update the last modified date if reqired
            if (updateDateLastModified)
            {
                this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            }

            // perform the save
            base.BeginSave(forceUpdate, handler, userState);
        }
        #endregion

        #region SaveAs
        /// <summary>
        /// Saves this Package to a file with a particular file name.
        /// </summary>
        public Package SaveAs(Object userId, String fileName)
        {
            return this.SaveAs(userId, fileName, /*clearMetadata*/true);
        }

        /// <summary>
        /// Saves this Package to a file with a particular file name.
        /// </summary>
        public Package SaveAs(Object userId, String fileName, Boolean clearMetadata)
        {
            if ((!this.IsNew) && (fileName == this.Id.ToString())) return this.Save();
            if (File.Exists(fileName)) File.Delete(fileName);
            return this.SaveAs(PackageType.FileSystem, fileName, userId, fileName, clearMetadata, 0, /*updateLastModified*/true);
        }

        /// <summary>
        /// Saves this package to the default factory type.
        /// </summary>
        public Package SaveAs(Object userId, Object entityId)
        {
            return this.SaveAs(userId, /*clearMetadata*/false, entityId);
        }

        /// <summary>
        /// Saves this package to the default factory type.
        /// </summary>
        public Package SaveAsCopy(Object userId, Object entityId)
        {
            return this.SaveAs(userId, /*clearMetaData*/false, entityId, /*updateLastModified*/false);
        }

        /// <summary>
        /// Saves this package to the default factory type.
        /// </summary>
        public Package SaveAs(Object userId, Boolean clearMetadata, Object entityId, Boolean updateLastModified = true)
        {
            return this.SaveAs(PackageType.Unknown, DalContainer.DalName, userId, this.Id, clearMetadata, entityId, updateLastModified);
        }

        /// <summary>
        /// Saves the package using the specified package type
        /// </summary>
        private Package SaveAs(PackageType newPackageType, String newDalFactoryName, Object userId, Object newId, Boolean clearMetadata, Object entityId, Boolean updateLastModified)
        {
            // record the old package properties
            PackageType oldPackageType = this.PackageType;
            String oldDalFactoryName = this.DalFactoryName;
            Object oldId = this.Id;
            Object oldLockUserId = this.LockUserId;
            PackageLockType oldLockType = this.LockType;
            Boolean oldIsReadOnly = this.IsReadOnly;
            Boolean oldIsNew = this.IsNew;

            // we are performing a save as, therefore
            // we always mark this package as new
            if (!this.IsNew) this.MarkGraphAsNew();

            // set the new package properties
            this.LoadProperty<PackageType>(PackageTypeProperty, newPackageType);
            this.LoadProperty<String>(DalFactoryNameProperty, newDalFactoryName);
            this.LoadProperty<Object>(LockUserIdProperty, userId);
            this.LoadProperty<Object>(IdProperty, newId);
            this.LoadProperty<Boolean>(IsReadOnlyProperty, false);
            this.LoadProperty<Object>(EntityIdProperty, entityId);

            // ensure all planogram are associated with the current user
            foreach (Planogram planogram in this.Planograms)
            {
                planogram.UserName = ApplicationContext.User.Identity.Name;
            }

            // perform the save
            Package returnValue = this.Save(!oldIsNew, updateLastModified);

            // save has been performed successfully,
            // so we can now safely unlock the old pacakge
            if (!oldIsNew)
            {
                UnlockPackageById(oldPackageType, oldId, oldLockUserId, oldLockType, oldIsReadOnly);
            }

            // and return the new package
            return returnValue;
        }

        #endregion

        #region Export To External File Type

        /// <summary>
        /// Exports a plangram to an external file type (Apollo, JDA, Spaceman)
        /// </summary>
        /// <param name="userId">The current user</param>
        /// <param name="planogram">The planogram in the package to export</param>
        /// <param name="fileType">THe type of file to save to</param>
        /// <param name="fileName">The path of the file to create</param>
        public Package ExportToExternalFile(Object userId, PlanogramExportFileType fileType, String fileName, Object context)
        {
            PackageType packageType = PackageType.FileSystem;

            //export the file
            return ExportToExternalFile(packageType, fileName, userId, fileName, context);
        }

        private Package ExportToExternalFile(PackageType newPackageType, String newDalFactoryName, Object userId, Object newId, Object context)
        {
            // we are performing a save as, therefore
            // we always mark copy package as new
            if (!this.IsNew) this.MarkGraphAsNew();

            // set the new package properties
            this.LoadProperty<PackageType>(PackageTypeProperty, newPackageType);
            this.LoadProperty<String>(DalFactoryNameProperty, newDalFactoryName);
            this.LoadProperty<Object>(LockUserIdProperty, userId);
            this.LoadProperty<Object>(IdProperty, newId);
            this.LoadProperty<Boolean>(IsReadOnlyProperty, false);
            this.LoadProperty<Object>(EntityIdProperty, 0);
            this.LoadProperty<Object>(FieldMappingsProperty, context);

            // perform the save
            Package returnValue = this.Save(false);

            //return the source plan
            return this;
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Calculates metadata for this instance
        /// </summary>
        /// <param name="processMetadataImages">if false, meta images will not be created.</param>
        public void CalculateMetadata(Boolean processMetadataImages, IPlanogramMetadataHelper metadataHelper)
        {
            _processMetadataImages = processMetadataImages;
            _metadataHelper = metadataHelper;

            this.CalculateMetadata();

            _metadataHelper = null;
        }

        /// <summary>
        /// Overide of base method called when calculating metadata for
        /// this type.
        /// </summary>
        protected override void CalculateMetadata()
        {
            using (CodePerformanceMetric calcMetric = new CodePerformanceMetric())
            {
                //Clear all existing meta data first
                // but only if we are processing meta data images
                // if we are not then meta data is being calculated on the
                // fly and by not clearing first we improve performance.
                if (_processMetadataImages)
                {
                    this.ClearMetadata();
                }

                //Call the base method but with children specified
                // to force them to load.
                base.CalculateMetadata(
                    new Object[]
                    {
                        this.Planograms
                    });
            }
        }

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            //Set meta data properties
            this.MetaPlanogramCount = this.Planograms.Count;
            this.DateMetadataCalculated = DateTime.UtcNow;
        }

        /// <summary>
        /// Clears metadata for this instance
        /// </summary>
        public new void ClearMetadata()
        {
            //Call the base method but with children specified
            // to force them to load.
            base.ClearMetadata(
                new Object[]
                    {
                        this.Planograms
                    });
        }

        /// <summary>
        /// Clears Images metadata only for save
        /// </summary>
        public void ResetMetadata()
        {
            //Set meta data properties
            this.DateMetadataCalculated = null;

            //clear off any plan thumbnails.
            foreach (Planogram planogram in this.Planograms)
            {
                if (planogram.PlanogramMetadataImages != null)
                {
                    planogram.PlanogramMetadataImages.Clear();
                }
            }

            // Clear all validation data as there is no more metadata.
            ClearValidationData();
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            //Set meta data properties
            this.MetaPlanogramCount = null;
            this.DateMetadataCalculated = null;

            // Clear all validation data as there is no more metadata.
            ClearValidationData();
        }

        #endregion

        #region ValidationData
        /// <summary>
        /// Calculates the validation data for this instance. Metadata will be created
        /// if it has not already been created.
        /// </summary>
        /// <param name="processMetadataImages">if false, meta images will not be created.</param>
        public void CalculateValidationData(Boolean processMetadataImages, IPlanogramMetadataHelper metadataHelper)
        {
            // calculate metadata if we need to.
            if (DateMetadataCalculated == null)
                this.CalculateMetadata(processMetadataImages, metadataHelper);

            // calculate validation data
            this.CalculateValidationData();
        }

        /// <summary>
        /// Calculates the validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Only Call if metadata has been calculated already!
        /// Hide base implementation with a public method so it can be invoked from this type.
        /// </remarks>
        public new void CalculateValidationData()
        {
            // calculate validation data on all planograms
            ((IModelList)this.Planograms).CalculateValidationData();

            // update the date validation calculated property
            this.DateValidationDataCalculated = DateTime.UtcNow;
        }

        /// <summary>
        /// Clears all validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Hide base implementation with a public method so it can be invoked from this type.
        /// </remarks>
        public new void ClearValidationData()
        {
            // update the date validation calculated property
            this.DateValidationDataCalculated = null;

            // clear validation data on all planograms
            ((IModelList)this.Planograms).ClearValidationData();
        }

        #endregion

        #region Unlock
        /// <summary>
        /// Unlocks this package
        /// </summary>
        public void Unlock()
        {
            if (!this.IsNew)
            {
                UnlockPackageById(this.PackageType, this.Id, this.LockUserId, this.LockType, this.IsReadOnly);
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Disposes of this instance
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    this.Unlock();
                }
                _isDisposed = true;
            }
        }
        #endregion

        #endregion
    }
}
