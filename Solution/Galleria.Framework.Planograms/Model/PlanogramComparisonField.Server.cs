﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Added Display field.

#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComparisonField
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonField() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto"/>.
        /// </summary>
        internal static PlanogramComparisonField Fetch(IDalContext dalContext, PlanogramComparisonFieldDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonField>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonFieldDto"/> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonFieldDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ItemTypeProperty, dto.ItemType);
            LoadProperty(FieldPlaceholderProperty, dto.FieldPlaceholder);
            LoadProperty(DisplayNameProperty, dto.DisplayName);
            LoadProperty(NumberProperty, dto.Number);
            LoadProperty(DisplayProperty, dto.Display);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonFieldDto"/> with the data in this instance.</returns>
        private PlanogramComparisonFieldDto GetDataTransferObject(PlanogramComparison parent)
        {
            return new PlanogramComparisonFieldDto
                   {
                       Id = ReadProperty(IdProperty),
                       PlanogramComparisonId = parent.Id,
                       ItemType = (Byte) ReadProperty(ItemTypeProperty),
                       FieldPlaceholder = ReadProperty(FieldPlaceholderProperty),
                       DisplayName = ReadProperty(DisplayNameProperty),
                       Number = ReadProperty(NumberProperty),
                       Display = ReadProperty(DisplayProperty),
                       ExtendedData = ReadProperty(ExtendedDataProperty)
                   };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonField"/>.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonFieldDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparisonField"/>.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            Object oldId = null;
            batchContext.Insert(
                dc =>
                {
                    PlanogramComparisonFieldDto dto = GetDataTransferObject(parent);
                    oldId = dto.Id;
                    return dto;
                },
                (dc, dto) =>
                {
                    LoadProperty(IdProperty, dto.Id);
                    dc.RegisterId<PlanogramComparisonField>(oldId, dto.Id);
                });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            if (IsSelfDirty) batchContext.Update(dc => GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparison"/>.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramComparison parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}