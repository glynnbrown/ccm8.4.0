﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// An enumeration that defines a package type
    /// which indicates the source of a package
    /// </summary>
    public enum PackageType
    {
        Unknown = 0,
        FileSystem = 1,
        // GfsDatabase
        // GfsWebServices
        // CcmDatabase
        // CcmRepository
        // IkbDatabase
    }

    /// <summary>
    /// PackageType Helper Class
    /// </summary>
    public static class PackageTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PackageType, String> FriendlyNames =
            new Dictionary<PackageType, String>()
            {
                {PackageType.Unknown, Message.Enum_PackageType_Unknown},
                {PackageType.FileSystem, Message.Enum_PackageType_FileSystem},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PackageType, String> FriendlyDescriptions =
            new Dictionary<PackageType, String>()
            {
                {PackageType.Unknown, Message.Enum_PackageType_Unknown},
                {PackageType.FileSystem, Message.Enum_PackageType_FileSystem},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PackageType> EnumFromFriendlyName =
            new Dictionary<String, PackageType>()
            {
                {Message.Enum_PackageType_Unknown.ToLowerInvariant(), PackageType.Unknown},
                {Message.Enum_PackageType_FileSystem.ToLowerInvariant(), PackageType.FileSystem},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PackageType? PackageTypeGetEnum(String friendlyName)
        {
            PackageType? returnValue = null;
            PackageType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
