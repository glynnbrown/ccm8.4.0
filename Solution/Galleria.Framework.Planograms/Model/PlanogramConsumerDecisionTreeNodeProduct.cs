﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27132 : A.Kuszyk
//  Implemented common interface.
// V8-27898 : L.Ineson
//  Added notifyobjectgraph call to  PlanogramProductId set
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramConsumerDecisionTreeNodeProduct Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeNodeProduct : ModelObject<PlanogramConsumerDecisionTreeNodeProduct>, IPlanogramConsumerDecisionTreeNodeProduct
    {
        #region Parent

        /// <summary>
        /// Returns the parent node of this product
        /// </summary>
        public PlanogramConsumerDecisionTreeNode ParentPlanogramConsumerDecisionTreeNode
        {
            get
            {
                PlanogramConsumerDecisionTreeNodeProductList parentList = base.Parent as PlanogramConsumerDecisionTreeNodeProductList;
                if (parentList != null)
                {
                    return parentList.Parent as PlanogramConsumerDecisionTreeNode;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Parent Property
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramConsumerDecisionTreeNode Parent
        {
            get { return (PlanogramConsumerDecisionTreeNode)((PlanogramConsumerDecisionTreeNodeProductList)base.Parent).Parent; }
        }
        #endregion

        #region PlanogramProductId Property

        public static readonly ModelPropertyInfo<Object> PlanogramProductIdProperty =
        RegisterModelProperty<Object>(c => c.PlanogramProductId);
        /// <summary>
        /// The id of the level this belongs to
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object PlanogramProductId
        {
            get { return GetProperty<Object>(PlanogramProductIdProperty); }
            private set {SetProperty<Object>(PlanogramProductIdProperty, value);}
        }

        #endregion

        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods

        public static PlanogramConsumerDecisionTreeNodeProduct NewPlanogramConsumerDecisionTreeNodeProduct(Object productId)
        {
            PlanogramConsumerDecisionTreeNodeProduct item = new PlanogramConsumerDecisionTreeNodeProduct();
            item.Create(productId);
            return item;
        }

        /// <summary>
        /// Creates a new Node Product from that given and the parent Planogram.
        /// </summary>
        /// <param name="nodeProduct">The node product to copy.</param>
        /// <param name="planogram">The parent Planogram.</param>
        /// <returns>A new node product.</returns>
        public static PlanogramConsumerDecisionTreeNodeProduct NewPlanogramConsumerDecisionTreeNodeProduct(
            IPlanogramConsumerDecisionTreeNodeProduct nodeProduct, Planogram planogram)
        {
            if (planogram == null) throw new ArgumentNullException("planogram cannot be null");
            if (nodeProduct == null) throw new ArgumentNullException("nodeProduct cannot be null");

            PlanogramConsumerDecisionTreeNodeProduct item = new PlanogramConsumerDecisionTreeNodeProduct();
            item.Create(nodeProduct, planogram);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Object productId)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(PlanogramProductIdProperty, productId);
            this.MarkAsChild();
            base.Create();
        }

        private void Create(IPlanogramConsumerDecisionTreeNodeProduct nodeProduct, Planogram planogram)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            var product = planogram.Products.FirstOrDefault(p => p.Gtin == nodeProduct.ProductGtin);
            if (product != null)
            {
                this.LoadProperty<Object>(PlanogramProductIdProperty, product.Id);
            }
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            var oldId = this.Id;
            var newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramConsumerDecisionTreeNodeProduct>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramProductId
            Object planogramProductId = context.ResolveId<PlanogramProduct>(this.ReadProperty<Object>(PlanogramProductIdProperty));
            if (planogramProductId != null) this.LoadProperty<Object>(PlanogramProductIdProperty, planogramProductId);
        }
        #endregion

        #region IPlanogramConsumerDecisionTreeNodeProduct members
        String IPlanogramConsumerDecisionTreeNodeProduct.ProductGtin
        {
            get
            {
                if (Parent == null || Parent.ParentConsumerDecisionTree == null || Parent.ParentConsumerDecisionTree.Parent == null) return null;
                var product = Parent.ParentConsumerDecisionTree.Parent.Products.FirstOrDefault(p => p.Id == this.PlanogramProductId);
                return product == null ? null : product.Gtin;
            }
        }
        #endregion
    }
}
