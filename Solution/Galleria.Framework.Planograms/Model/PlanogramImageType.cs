﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26041 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramImageType
    {
        None= 0,
        Display = 1,
        Tray = 2,
        PointOfPurchase = 3,
        Alternate = 4,
        Case = 5
    }

    public enum PlanogramImageFacingType
    {
        Front = 0,
        Left = 1,
        Top= 2,
        Back = 3,
        Right = 4,
        Bottom = 5
    }
}
