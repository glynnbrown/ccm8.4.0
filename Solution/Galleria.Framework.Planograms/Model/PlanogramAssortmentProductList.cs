﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-29089 : N.Foster
//  Added ability to add planogram products
#endregion

#region Version History: CCM830
// V8-32132 : A.Silva
//  Added AppendNew method to append a second Planogram Assortment Product List's new items as copies.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentProductList : ModelList<PlanogramAssortmentProductList, PlanogramAssortmentProduct>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentProductList NewPlanogramAssortmentProductList()
        {
            var item = new PlanogramAssortmentProductList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a collection of objects that implement IPlanogramAssortmentProduct.
        /// </summary>
        /// <param name="range">The collection of objects to add.</param>
        public void AddRange(IEnumerable<IPlanogramAssortmentProduct> range)
        {
            base.AddRange(range.Select(o=>PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(o)));
        }

        /// <summary>
        /// Adds a product to this list
        /// </summary>
        public PlanogramAssortmentProduct Add(PlanogramProduct product)
        {
            PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product);
            this.Add(assortmentProduct);
            return assortmentProduct;
        }

        /// <summary>
        ///     Append any new items from the <paramref name="sourceItems"/> collection.
        /// </summary>
        /// <param name="sourceItems">Collection of <see cref="PlanogramProduct"/> items to add missing ones from.</param>
        /// <remarks>Uniqueness is determined by GTIN. Only copies of the source items are added to this instance.</remarks>
        public void AppendNew(PlanogramAssortmentProductList sourceItems)
        {
            //  Look up existing GTINs in this list.
            List<String> existingGtins = Items.Select(item => item.Gtin).ToList();

            //  Find missing GTINs in the source products. These are the new products that need copying into this instance.
            IEnumerable<PlanogramAssortmentProduct> newItems = sourceItems.Where(item => !existingGtins.Contains(item.Gtin));

            //  Add copies of the new products.
            AddRange(newItems.Select(item => item.Copy()));
        }

        #endregion
    }
}