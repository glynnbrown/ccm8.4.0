﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802
// V8-29137 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Enums;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A simple helper class to assist with the switching of PlanogramSubComponent Merchandising Strategy X, Y and Z enums.
    /// All of these enums have similar properties, but they are hard to deal with polymorphically, because they are enums.
    /// This class accepts a value from each in the constructor and provides common properties that identify which option was
    /// selected.
    /// </summary>
    public sealed class PlanogramSubComponentMerchandisingStrategyHelper
    {
        #region Properties
        /// <summary>
        /// E.g. Right, Top, Front.
        /// </summary>
        public Boolean IsMax { get; private set; }

        /// <summary>
        /// E.g. Left, Bottom, Back.
        /// </summary>
        public Boolean IsMin { get; private set; }

        /// <summary>
        /// E.g. Even.
        /// </summary>
        public Boolean IsEven { get; private set; }

        /// <summary>
        /// E.g. Manual
        /// </summary>
        public Boolean IsManual { get; private set; }

        /// <summary>
        /// E.g. Right Stacked, Top Stacked, Front Stacked.
        /// </summary>
        public Boolean IsMaxStacked { get; private set; }

        /// <summary>
        /// E.g. Left Stacked, Bottom Stacked, Back Stacked.
        /// </summary>
        public Boolean IsMinStacked { get; private set; } 
        #endregion

        #region Constructors
        public PlanogramSubComponentMerchandisingStrategyHelper(PlanogramSubComponentXMerchStrategyType strategy)
        {
            Initialise(strategy);
        }

        public PlanogramSubComponentMerchandisingStrategyHelper(PlanogramSubComponentYMerchStrategyType strategy)
        {
            Initialise(strategy);
        }

        public PlanogramSubComponentMerchandisingStrategyHelper(PlanogramSubComponentZMerchStrategyType strategy)
        {
            Initialise(strategy);
        } 

        public PlanogramSubComponentMerchandisingStrategyHelper(PlanogramSubComponent subComponent, AxisType axis)
        {
            switch(axis)
            {
                case AxisType.X:
                    Initialise(subComponent.MerchandisingStrategyX);
                    break;
                case AxisType.Y:
                    Initialise(subComponent.MerchandisingStrategyY);
                    break;
                case AxisType.Z:
                    Initialise(subComponent.MerchandisingStrategyZ);
                    break;
                default:
                    throw new NotSupportedException("Axis was not x, y or z");
            }

        }

        #endregion

        #region Methods

        public void Initialise(PlanogramSubComponentXMerchStrategyType strategy)
        {
            switch (strategy)
            {
                case PlanogramSubComponentXMerchStrategyType.Right:
                    IsMax = true;
                    break;
                case PlanogramSubComponentXMerchStrategyType.Left:
                    IsMin = true;
                    break;
                case PlanogramSubComponentXMerchStrategyType.Even:
                    IsEven = true;
                    break;
                case PlanogramSubComponentXMerchStrategyType.Manual:
                    IsManual = true;
                    break;
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    IsMaxStacked = true;
                    break;
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    IsMinStacked = true;
                    break;
                default:
                    throw new ArgumentException("Unrecognised strategy");
            }
        }

        public void Initialise(PlanogramSubComponentYMerchStrategyType strategy)
        {
            switch (strategy)
            {
                case PlanogramSubComponentYMerchStrategyType.Top:
                    IsMax = true;
                    break;
                case PlanogramSubComponentYMerchStrategyType.Bottom:
                    IsMin = true;
                    break;
                case PlanogramSubComponentYMerchStrategyType.Even:
                    IsEven = true;
                    break;
                case PlanogramSubComponentYMerchStrategyType.Manual:
                    IsManual = true;
                    break;
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                    IsMaxStacked = true;
                    break;
                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                    IsMinStacked = true;
                    break;
                default:
                    throw new ArgumentException("Unrecognised strategy");
            }
        }

        public void Initialise(PlanogramSubComponentZMerchStrategyType strategy)
        {
            switch (strategy)
            {
                case PlanogramSubComponentZMerchStrategyType.Front:
                    IsMax = true;
                    break;
                case PlanogramSubComponentZMerchStrategyType.Back:
                    IsMin = true;
                    break;
                case PlanogramSubComponentZMerchStrategyType.Even:
                    IsEven = true;
                    break;
                case PlanogramSubComponentZMerchStrategyType.Manual:
                    IsManual = true;
                    break;
                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                    IsMaxStacked = true;
                    break;
                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    IsMinStacked = true;
                    break;
                default:
                    throw new ArgumentException("Unrecognised strategy");
            }
        } 

        #endregion
    }
}
