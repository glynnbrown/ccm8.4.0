﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550: A.Probyn
//	Copied from SA.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentLocationBuddyTreatmentType
    {
        Avg,
        Sum
    }

    public static class PlanogramAssortmentLocationBuddyTreatmentTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentLocationBuddyTreatmentType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentLocationBuddyTreatmentType, String>()
            {
                {PlanogramAssortmentLocationBuddyTreatmentType.Avg, Message.Enum_PlanogramAssortmentLocationBuddyTreatmentType_Avg},
                {PlanogramAssortmentLocationBuddyTreatmentType.Sum, Message.Enum_PlanogramAssortmentLocationBuddyTreatmentType_Sum}
            };

        /// <summary>
        /// Returns the matching enum value from the specified friendlyname
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramAssortmentLocationBuddyTreatmentType? ParseFromFriendlyName(String friendlyName, Boolean closestMatch = false)
        {
            String cleanFriendlyName = friendlyName.ToLowerInvariant().Trim();

            foreach (KeyValuePair<PlanogramAssortmentLocationBuddyTreatmentType, String> keyPair in PlanogramAssortmentLocationBuddyTreatmentTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.ToLowerInvariant().Equals(cleanFriendlyName))
                {
                    return keyPair.Key;
                }
            }

            if (closestMatch)
            {
                foreach (KeyValuePair<PlanogramAssortmentLocationBuddyTreatmentType, String> keyPair in PlanogramAssortmentLocationBuddyTreatmentTypeHelper.FriendlyNames)
                {
                    if (keyPair.Value.ToLowerInvariant().Contains(cleanFriendlyName))
                    {
                        return keyPair.Key;
                    }
                }
            }
            return null;
        }

    }
}
