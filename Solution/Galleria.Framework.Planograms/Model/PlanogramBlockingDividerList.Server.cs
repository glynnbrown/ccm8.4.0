﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingDividerList
    {
        #region Constructor
        private PlanogramBlockingDividerList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all annotations for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramBlockingDividerDal dal = dalContext.GetDal<IPlanogramBlockingDividerDal>())
                {
                    IEnumerable<PlanogramBlockingDividerDto> dtoList = dal.FetchByPlanogramBlockingId(criteria.ParentId);
                    foreach (PlanogramBlockingDividerDto dto in dtoList)
                    {
                        this.Add(PlanogramBlockingDivider.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
