﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//  Created
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Added isset properties.
#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAttributes
    {
        #region Constructor
        private PlanogramAttributes() { } //force use of factory methods.
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        internal PlanogramAttributesDto GetDataTransferObject()
        {
            return new PlanogramAttributesDto()
            {
                PlanogramId = this.ReadProperty<Object>(PlanogramIdProperty),
                CategoryCode = this.ReadProperty<String>(CategoryCodeProperty),
                CategoryCodeIsSet = this.ReadProperty<Boolean>(CategoryCodeIsSetProperty),
                CategoryName = this.ReadProperty<String>(CategoryNameProperty),
                CategoryNameIsSet = this.ReadProperty<Boolean>(CategoryNameIsSetProperty),
                LocationCode = this.ReadProperty<String>(LocationCodeProperty),
                LocationCodeIsSet = this.ReadProperty<Boolean>(LocationCodeIsSetProperty),
                LocationName = this.ReadProperty<String>(LocationNameProperty),
                LocationNameIsSet = this.ReadProperty<Boolean>(LocationNameIsSetProperty),
                ClusterSchemeName = this.ReadProperty<String>(ClusterSchemeNameProperty),
                ClusterSchemeNameIsSet = this.ReadProperty<Boolean>(ClusterSchemeNameIsSetProperty),
                ClusterName = this.ReadProperty<String>(ClusterNameProperty),
                ClusterNameIsSet = this.ReadProperty<Boolean>(ClusterNameIsSetProperty),
                DateApproved = this.ReadProperty<DateTime?>(DateApprovedProperty),
                DateApprovedIsSet = this.ReadProperty<Boolean>(DateApprovedIsSetProperty),
                DateArchived = this.ReadProperty<DateTime?>(DateArchivedProperty),
                DateArchivedIsSet = this.ReadProperty<Boolean>(DateArchivedIsSetProperty),
                DateWip = this.ReadProperty<DateTime?>(DateWipProperty),
                DateWipIsSet = this.ReadProperty<Boolean>(DateWipIsSetProperty),
                Status = this.ReadProperty<PlanogramStatusType>(StatusProperty),
                StatusIsSet = this.ReadProperty<Boolean>(StatusIsSetProperty),
                PlanogramType = this.ReadProperty<PlanogramType>(PlanogramTypeProperty),
                PlanogramTypeIsSet = this.ReadProperty<Boolean>(PlanogramTypeIsSetProperty),
            };
        }

        #endregion

        #endregion
    }
}

