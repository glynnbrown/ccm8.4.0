﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-26760 : L.Ineson
//  Populated property display types
// V8-27058 : A.Probyn
//  Added new meta data properties
//  Added ClearMetaData
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
#endregion
#region Version History: CCM803
// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
#endregion
#region Version History: CCM810
//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramFixture object
    [Serializable]
    public sealed partial class PlanogramFixture : ModelObject<PlanogramFixture>
    {
        #region Constructors
        static PlanogramFixture()
        {
            FriendlyName = Message.PlanogramFixture_FriendlyName;
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramFixtureList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the fixture name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.Generic_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the fixture height
        /// </summary>
        public Single Height
        {
            get { return this.GetProperty<Single>(HeightProperty); }
            set { this.SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramFixture_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the fixture width
        /// </summary>
        public Single Width
        {
            get { return this.GetProperty<Single>(WidthProperty); }
            set { this.SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramFixture_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the fixture depth
        /// </summary>
        public Single Depth
        {
            get { return this.GetProperty<Single>(DepthProperty); }
            set { this.SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region Assemblies
        /// <summary>
        /// Assemblies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureAssemblyList> AssembliesProperty =
            RegisterModelProperty<PlanogramFixtureAssemblyList>(c => c.Assemblies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child fixture assemblies
        /// </summary>
        public PlanogramFixtureAssemblyList Assemblies
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureAssemblyList>(
                    AssembliesProperty,
                    new PlanogramFixtureAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child fixture assemblies
        /// </summary>
        public PlanogramFixtureAssemblyList AssembliesAsyc
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureAssemblyList>(
                    AssembliesProperty,
                    new PlanogramFixtureAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureComponentList> ComponentsProperty =
            RegisterModelProperty<PlanogramFixtureComponentList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child fixture components
        /// </summary>
        public PlanogramFixtureComponentList Components
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureComponentList>(
                    ComponentsProperty,
                    new PlanogramFixtureComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child fixture components
        /// </summary>
        public PlanogramFixtureComponentList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureComponentList>(
                    ComponentsProperty,
                    new PlanogramFixtureComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion
        
        #region Meta Data Properties

        #region MetaNumberOfMerchandisedSubComponents
        /// <summary>
        /// MetaNumberOfMerchandisedSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMerchandisedSubComponentsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMerchandisedSubComponents, Message.PlanogramFixture_MetaNumberOfMerchandisedSubComponents);
        /// <summary>
        /// Returns the number of merchandised sub components
        /// </summary>
        public Int16? MetaNumberOfMerchandisedSubComponents
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty); }
            private set { this.SetProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty, value); }
        }
        #endregion
        
        #region MetaTotalFixtureCost
        /// <summary>
        /// MetaTotalFixtureCost property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalFixtureCostProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalFixtureCost, Message.PlanogramFixture_MetaTotalFixtureCost, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Returns the total fixture cost
        /// </summary>
        public Single? MetaTotalFixtureCost
        {
            get { return this.GetProperty<Single?>(MetaTotalFixtureCostProperty); }
            set { this.SetProperty<Single?>(MetaTotalFixtureCostProperty, value); }
        }
        #endregion
        
        #endregion
        
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            base.AddBusinessRules();

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixture NewPlanogramFixture()
        {
            PlanogramFixture item = new PlanogramFixture();
            item.Create(null, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixture NewPlanogramFixture(String name)
        {
            PlanogramFixture item = new PlanogramFixture();
            item.Create(name, null);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixture NewPlanogramFixture(IPlanogramSettings settings)
        {
            PlanogramFixture item = new PlanogramFixture();
            item.Create(null, settings);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixture NewPlanogramFixture(String name, IPlanogramSettings settings)
        {
            PlanogramFixture item = new PlanogramFixture();
            item.Create(name, settings);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String name, IPlanogramSettings settings)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, name == null ? Message.PlanogramFixture_Name_Default : name);
            this.LoadProperty<PlanogramFixtureAssemblyList>(AssembliesProperty, PlanogramFixtureAssemblyList.NewPlanogramFixtureAssemblyList());
            this.LoadProperty<PlanogramFixtureComponentList>(ComponentsProperty, PlanogramFixtureComponentList.NewPlanogramFixtureComponentList());
            if (settings != null)
            {
                this.LoadProperty<Single>(WidthProperty, settings.FixtureWidth);
                this.LoadProperty<Single>(HeightProperty, settings.FixtureHeight);
                this.LoadProperty<Single>(DepthProperty, settings.FixtureDepth);
            }
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramFixture>(oldId, newId);
        }

        /// <summary>
        /// Returns all planogram sub component placements
        /// associated to this fixture
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements())
            {
                if (this.Equals(subComponentPlacement.Fixture))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(PlanogramFixture);
            String typeFriendly = PlanogramFixture.FriendlyName;

            String detailsGroup = Message.PlanogramFixture_PropertyGroup_Details;
            String sizeGroup = Message.PlanogramFixture_PropertyGroup_Size;
            String metaDataGroup = Message.PlanogramFixture_PropertyGroup_Metadata;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);

            //Metadata fields
            if (includeMetadata)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfMerchandisedSubComponentsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFixtureCostProperty, metaDataGroup);
            }
            
        }

        /// <summary>
        /// Returns the value of the given field for this .
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            return field.GetValue(this);
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Override of base calcualte metadata to make
        /// sure items are loaded.
        /// </summary>
        protected override void CalculateMetadata()
        {
            base.CalculateMetadata(
                new Object[]
                {
                    this.Components,
                    this.Assemblies
                });
        }

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            //Set meta data properties based on child meta data

            //Calculate total fixture cost & number of merchandisable sub components
            Single fixtureCost = 0;
            Int16 totalMerchandisableSubComponents = 0;
            foreach (PlanogramFixtureComponent fixComp in this.Components)
            { 
                //Lookup component from cache
                PlanogramComponent component = this.Parent.Components.FindById(fixComp.PlanogramComponentId);
                if (component != null)
                {
                    //Get supplier cost
                    if (component.SupplierCostPrice.HasValue)
                    {
                        //Increment by supplier cost price
                        fixtureCost += component.SupplierCostPrice.Value;
                    }

                    //Enumerate through sub components
                    foreach (PlanogramSubComponent subComponent in component.SubComponents)
                    {
                        //MerchandisingType used to see if merchandisable, increment by 1 if so
                        totalMerchandisableSubComponents += (Int16)((subComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None) ? 1 : 0);
                    }
                }
            }
            this.MetaTotalFixtureCost = (Single)Math.Round(fixtureCost, 2, MidpointRounding.AwayFromZero);
            this.MetaNumberOfMerchandisedSubComponents = totalMerchandisableSubComponents;
        }

        /// <summary>
        /// Override of the clear metadata
        /// </summary>
        protected override void ClearMetadata()
        {
            base.ClearMetadata(
                new Object[]
                {
                    this.Components,
                    this.Assemblies
                });
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            //Set meta data properties
            this.MetaTotalFixtureCost = null;
            this.MetaNumberOfMerchandisedSubComponents = null;
        }

        #endregion


        #endregion
    }
}