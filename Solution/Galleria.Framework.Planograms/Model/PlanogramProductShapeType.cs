﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : A.Kuszyk
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramProductShapeType
    {
        Box = 0,
        Bottle = 1,
        //Jar = 2,
        //Can = 3,
    }

    /// <summary>
    /// PlanogramProductShapeType Helper Class
    /// </summary>
    public static class PlanogramProductShapeTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductShapeType, String> FriendlyNames =
            new Dictionary<PlanogramProductShapeType, String>()
            {
                {PlanogramProductShapeType.Box, Message.Enum_PlanogramProductShapeType_Box},
                {PlanogramProductShapeType.Bottle, Message.Enum_PlanogramProductShapeType_Bottle},
                //{PlanogramProductShapeType.Jar, Message.Enum_PlanogramProductShapeType_Jar},
                //{PlanogramProductShapeType.Can, Message.Enum_PlanogramProductShapeType_Can}
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductShapeType, String> FriendlyDescriptions =
            new Dictionary<PlanogramProductShapeType, String>()
            {
                {PlanogramProductShapeType.Box, Message.Enum_PlanogramProductShapeType_Box},
                {PlanogramProductShapeType.Bottle, Message.Enum_PlanogramProductShapeType_Bottle},
                //{PlanogramProductShapeType.Jar, Message.Enum_PlanogramProductShapeType_Jar},
                //{PlanogramProductShapeType.Can, Message.Enum_PlanogramProductShapeType_Can}
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductShapeType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductShapeType>()
            {
                {Message.Enum_PlanogramProductShapeType_Box.ToLowerInvariant(), PlanogramProductShapeType.Box},
                {Message.Enum_PlanogramProductShapeType_Bottle.ToLowerInvariant(), PlanogramProductShapeType.Bottle},
                //{Message.Enum_PlanogramProductShapeType_Jar.ToLowerInvariant(), PlanogramProductShapeType.Jar},
                //{Message.Enum_PlanogramProductShapeType_Can.ToLowerInvariant(), PlanogramProductShapeType.Can}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        public static PlanogramProductShapeType? PlanogramProductShapeTypeGetEnum(String friendlyName)
        {
            PlanogramProductShapeType? returnValue = null;
            PlanogramProductShapeType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}