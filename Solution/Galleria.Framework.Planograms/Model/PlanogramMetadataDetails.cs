﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27740 : L.Ineson
//  Created
// V8-27742 : A.Silva
//      Added Duplicate Gtin lookup collection.
// V8-27836 : A.Silva
//      Added PerformanceData per product look up dictionary.
#endregion

#region Version History: CCM802
// CCM-28816 : D.Pleasance
//  Amended PositionWorldSpaceBounds so that key is PlanogramPositionPlacementId so that we can obtain references
// CCM-29291 : L.Ineson
//  Added in code to only load space bounds dictionaries when they are requested.
//  Left it loading all in create for now and will finish when we have branched.
#endregion

#region Version History: CCM803
// V8-29291 : L.Ineson
//  Dictionaries now only get created as required.
// V8-28491 : L.Luong
//  Now takes in IPlanogramMetadataHelper
#endregion

#region Version History: CCM810
// V8-29736 : L.Ineson
//  Added helpers for blocking metadata.
// V8-29546 : L.Ineson
//  Planogram performance totals calc now only incudes placed products.
// V8-29192 : L.Ineson
//  Tidied up and merged a couple of dictionaries that contained the same thing.
// V8-30103 : N.Foster
//  Autofill performance enhancements
#endregion

#region Version History: CCM811
// V8-30456 : A.Kuszyk
//  Updated PlanogramMerchandisingGroupInfo constructor to use rotated merchandising space.
// V8-30544 : A.Kuszyk
//  Updated PlanogramSubComponentPlacementInfo constructor to maximise depth for hang merchandising space.
#endregion

#region Version History: CCM820
// V8-30536 : A.Kuszyk
//  Re-factored constructor method to accept merchandising groups as an argument.
// V8-30762 : I.George
// Added MetaData calculation for PlanogramAssortmentProduct
// V8-29998 : I.George
// Added PerformanceRankFieldCalculations
// V8-30705 : A.Probyn
//  Added FindPlanogramProductsByFamilyName and caches to support it.
//  Removed unncessary code from IsProductPlacedOnPlanogram that would have been consuming some memory needlessly.
// V8-30765 : I.George
//  Added method to calculation metaData for BlockingGroup
// V8-30763 : I.George
//  Added method to calculate metaData for CDT
// V8-30737 : M.Shelley
//  Added new method (CalculateCDTPerformance) to calculate metadata fields for selected CDT nodes
//  Added new method (CalculateBlockingGroupPerformance) to calculate metadata fields for a PlanogramBlockingGroup
// V8-31219 : M.Shelley
//  Modified the CalculateProductRankByPerformance method to correctly calculate the rank taking 
//  into account the decimal places.
#endregion

#region Version History: CCM830
// V8-31922 : L.Ineson
//  Stopped erroring when assortment product has null gtin.
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-32264 : A.Probyn
//  Updated PopulateProductRankByPerformance to be able to deal with values above AND below 0. Even if the performance values 
//  are 0 or below, they should be still ranked.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
//V8-32562 : L.Ineson
//  Made sure that Is
// CCM-13761 : L.Ineson
//  Changed bounds dictionaries to use BoundaryInfo.
// CCM-13565 : L.Ineson
//  Removed ComponentWorldBounds - collision checks are now at sub level.
// CCM-13956 : M.Pettit
//  Unwound changes made in V8-32264 to PopulateProductRankByPerformance
#endregion

#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A class used to cache details that will be used by multiple model objects
    /// when calculating metadata.
    /// </summary>
    [Serializable]
    public sealed class PlanogramMetadataDetails
    {
        #region NestedClasses

        /// <summary>
        /// Class containing information about a subcomponent placement
        /// </summary>
        [Serializable]
        public sealed class PlanogramSubComponentPlacementInfo
        {
            #region Fields
            private PlanogramSubComponentPlacementId _placementId;
            private PlanogramSubComponentPlacement _placement;
            private PlanogramMerchandisingSpace _merchandisingSpace;
            private BoundaryInfo _worldBoundary;
            #endregion

            #region Properties

            public PlanogramSubComponentPlacementId PlacementId
            {
                get { return _placementId; }
            }

            public PlanogramSubComponentPlacement Placement
            {
                get { return _placement; }
            }

            public RectValue WorldSpaceBounds
            {
                get { return _worldBoundary.WorldRelativeBounds; }
            }

            public PlanogramMerchandisingSpace MerchandisingSpace
            {
                get { return _merchandisingSpace; }
            }

            public BoundaryInfo WorldBoundary
            {
                get { return _worldBoundary; }
            }

            #endregion

            #region Constructor
            private PlanogramSubComponentPlacementInfo() { } 
            #endregion

            #region Factory Methods

            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            /// <param name="subPlacement"></param>
            /// <param name="allSubPlacements"></param>
            /// <returns></returns>
            public static PlanogramSubComponentPlacementInfo NewPlanogramSubComponentPlacementInfo(
                PlanogramSubComponentPlacement subPlacement,
                IEnumerable<PlanogramSubComponentPlacement> allSubPlacements)
            {
                PlanogramSubComponentPlacementInfo item = new PlanogramSubComponentPlacementInfo();
                item.Create(subPlacement, allSubPlacements);
                return item;
            }


            #endregion

            #region Data Access

            private void Create(
                PlanogramSubComponentPlacement subPlacement,
                IEnumerable<PlanogramSubComponentPlacement> allSubPlacements)
            {
                _placementId = (PlanogramSubComponentPlacementId)subPlacement.Id;
                _placement = subPlacement;

                _worldBoundary = subPlacement.GetPlanogramRelativeBoundaryInfo();

                _merchandisingSpace =
                    (subPlacement.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None) ?
                    new PlanogramMerchandisingSpace(
                        subPlacement,
                        allSubPlacements,
                    // We don't care about overlapping space, we just want the space for this subcomp. 
                        ignoreSpaceOverlaps: true,
                    // We want to maximise depth for hang components, so that we get the space to the 
                    // front of the plan (or the merch depth).
                        maximiseDepthForHang: true)
                    : null;

            }

            #endregion

            #region Methods

            /// <summary>
            /// Returns true if this placement is a child of the assembly component placement
            /// </summary>
            /// <param name="ac"></param>
            /// <param name="fa"></param>
            /// <param name="fi"></param>
            /// <returns></returns>
            public Boolean IsChildOf(PlanogramAssemblyComponent ac, PlanogramFixtureAssembly fa, PlanogramFixtureItem fi)
            {
                if (this.Placement.AssemblyComponent != ac) return false;
                if (this.Placement.FixtureAssembly != fa) return false;
                if (this.Placement.FixtureItem != fi) return false;
                return true;
            }

            /// <summary>
            /// Returns true if this placement is a child of the fixture component placement
            /// </summary>
            /// <param name="fc"></param>
            /// <param name="fi"></param>
            /// <returns></returns>
            public Boolean IsChildOf(PlanogramFixtureComponent fc, PlanogramFixtureItem fi)
            {
                if (this.Placement.FixtureComponent != fc) return false;
                if (this.Placement.FixtureItem != fi) return false;
                return true;
            }

            /// <summary>
            /// Returns the placement id for the component this belongs to.
            /// </summary>
            /// <returns></returns>
            public PlanogramComponentPlacementId GetComponentPlacementId()
            {
                return new PlanogramComponentPlacementId(this.Placement);
            }

            #endregion
        }

        /// <summary>
        /// Class containing information about a merchandising group.
        /// </summary>
        [Serializable]
        public sealed class PlanogramMerchandisingGroupInfo
        {
            #region Fields
            private List<Object> _placementIds;
            private RectValue _planRelativeMerchandisingSpaceBounds;
            private Boolean _isOverfilledX;
            private Boolean _isOverfilledY;
            private Boolean _isOverfilledZ;
            private RectValue _merchandisingSpaceBounds;
            private List<Single> _combinedPlacementOffsets = null;
            #endregion

            #region Properties

            /// <summary>
            /// Returns the subcomponent placement ids
            /// which make up this merch group.
            /// </summary>
            public IEnumerable<Object> SucomponentPlacementIds
            {
                get { return _placementIds; }
            }

            /// <summary>
            /// Returns the bounds of the merchandising area
            /// relative to the entire planogram.
            /// </summary>
            public RectValue PlanRelativeMerchandisingSpaceBounds
            {
                get { return _planRelativeMerchandisingSpaceBounds; }
            }

            /// <summary>
            /// Returns the bounds of the merchandising space
            /// relative to its subcomponents.
            /// </summary>
            public RectValue MerchandisingSpaceBounds
            {
                get{return _merchandisingSpaceBounds;}
            }

            /// <summary>
            /// Returns true if the merch group is overfilled in 
            /// the x axis.
            /// </summary>
            public Boolean IsOverfilledX
            {
                get { return _isOverfilledX; }
            }

            /// <summary>
            /// Returns true if the merch group is overfilled in 
            /// the y axis.
            /// </summary>
            public Boolean IsOverfilledY
            {
                get { return _isOverfilledY; }
            }

            /// <summary>
            /// Returns true if the merch group is overfilled in 
            /// the z axis.
            /// </summary>
            public Boolean IsOverfilledZ
            {
                get { return _isOverfilledZ; }
            }

            #endregion

            #region Constructor
            private PlanogramMerchandisingGroupInfo() { }
            #endregion

            #region Factory Methods

            public static PlanogramMerchandisingGroupInfo NewPlanogramMerchandisingGroupInfo(
                PlanogramMerchandisingGroup merchGroup,
                Dictionary<Object, PlanogramSubComponentPlacementInfo> subPlacementInfos)
            {
                PlanogramMerchandisingGroupInfo item = new PlanogramMerchandisingGroupInfo();
                item.Create(merchGroup, subPlacementInfos);
                return item;
            }

            #endregion

            #region Data Access

            private void Create(PlanogramMerchandisingGroup merchGroup,
                Dictionary<Object, PlanogramSubComponentPlacementInfo> subPlacementInfos)
            {
                _placementIds = merchGroup.SubComponentPlacements.Select(s => s.Id).ToList();
                _combinedPlacementOffsets = new List<Single>(_placementIds.Count);

                //note that this needs to be plan relative but the overfilled check does not.
                Single xOffset = 0;
                RectValue? merchandisingSpaceBounds = null;
                foreach (PlanogramSubComponentPlacement sp in merchGroup.SubComponentPlacements)
                {
                    PlanogramSubComponentPlacementInfo spInfo;
                    if (!subPlacementInfos.TryGetValue(sp.Id, out spInfo)) continue;

                    if (merchandisingSpaceBounds.HasValue)
                    {
                        merchandisingSpaceBounds =
                            merchandisingSpaceBounds.Value.Union(spInfo.MerchandisingSpace.RotatedSpaceReducedByObstructions);
                    }
                    else
                    {
                        merchandisingSpaceBounds = spInfo.MerchandisingSpace.RotatedSpaceReducedByObstructions;
                    }
                    _combinedPlacementOffsets.Add(xOffset);
                    xOffset += sp.SubComponent.Width;
                }
                if (merchandisingSpaceBounds.HasValue)
                {
                    _planRelativeMerchandisingSpaceBounds = merchandisingSpaceBounds.Value;
                }


                PlanogramMerchandisingSpace groupSpace = merchGroup.GetMerchandisingSpace(false, false);
                _isOverfilledX = groupSpace.UnhinderedSpace.Width.GreaterThan(0) && merchGroup.IsOverfilled(AxisType.X, groupSpace, checkBoundaries: true);
                _isOverfilledY = groupSpace.UnhinderedSpace.Height.GreaterThan(0) && merchGroup.IsOverfilled(AxisType.Y, groupSpace, checkBoundaries:true);
                _isOverfilledZ = groupSpace.UnhinderedSpace.Depth.GreaterThan(0) && merchGroup.IsOverfilled(AxisType.Z, groupSpace, checkBoundaries: true);
                _merchandisingSpaceBounds = groupSpace.UnhinderedSpace;

            }

            #endregion

            #region Methods

            /// <summary>
            /// Returns the x offset of the subcomponent placement relative to the merch group.
            /// </summary>
            /// <param name="subPlacementId"></param>
            /// <returns></returns>
            public Single GetSubPlacementXOffset(Object subPlacementId)
            {
                Int32 idx = _placementIds.IndexOf(subPlacementId);
                if (idx < 0) return 0;
                else return _combinedPlacementOffsets[idx];
            }

            #endregion
        }

        #endregion

        #region Fields
        private Planogram _planogram;
        private Dictionary<PlanogramPositionPlacementId, BoundaryInfo> _positionPlacementIdToWorldSpaceBounds;
        private Dictionary<Object, RectValue> _designViewBoundsByPositionId;
        private Dictionary<Object, IEnumerable<PlanogramPosition>> _productIdToPositions;
        private Dictionary<Object, PlanogramPositionDetails> _positionIdToPositionDetails;
        private Dictionary<String, PlanogramAssortmentProduct> _gtinToAssortmentProduct;
        private Dictionary<String, IGrouping<String, PlanogramProduct>> _gtinToPlanogramProducts;
        private IEnumerable<IGrouping<String, PlanogramAssortmentProduct>> _planogramAssortmentProductFamilies;
        private IEnumerable<PlanogramSubComponentPlacement> _subComponentPlacements;
        private List<String> _duplicateProductGtins;
        private Dictionary<Object, PlanogramPerformanceData> _productIdToPerformanceData;
        private Single?[] _performanceTotals;
        private IPlanogramMetadataHelper _metadataHelper;
        private Single _planogramDesignHeight;
        private Single _planogramDesignWidth;
        private Single _planogramDesignOffset;

        private List<PlanogramMerchandisingGroupInfo> _merchGroupInfos = new List<PlanogramMerchandisingGroupInfo>();
        private Dictionary<Object, PlanogramSubComponentPlacementInfo> _infoBySubComponentPlacementId;
        private ILookup<PlanogramComponentPlacementId, PlanogramSubComponentPlacementInfo> _subPlacementInfosByComponent;
        private Dictionary<Object, PlanogramMerchandisingGroupInfo> _merchGroupInfoBySubComponentPlacementId;
        private Dictionary<Object, List<PlanogramProduct>> _productsByBlockingGroupId;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the planogram for which this 
        /// details object has been created.
        /// </summary>
        public Planogram Planogram
        {
            get { return _planogram; }
        }

        /// <summary>
        /// Returns all subcomponent placements in the planogram
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> SubComponentPlacements
        {
            get { return _subComponentPlacements; }
        }

        /// <summary>
        /// Gets the dictionary of PlanogramPositionId to the position world space bounds
        /// </summary>
        public Dictionary<PlanogramPositionPlacementId, BoundaryInfo> PositionPlacementIdToWorldSpaceBounds
        {
            get { return _positionPlacementIdToWorldSpaceBounds; }
        }

        /// <summary>
        /// Gets the lookup of subcompnent placement infos grouped by component placement id.
        /// </summary>
        public ILookup<PlanogramComponentPlacementId, PlanogramSubComponentPlacementInfo> SubPlacementInfosByComponent
        {
            get
            {
                if(_subPlacementInfosByComponent == null)
                {
                    _subPlacementInfosByComponent = _infoBySubComponentPlacementId.Values.ToLookup(g => g.GetComponentPlacementId());
                }
                return _subPlacementInfosByComponent;
            }
        }

        /// <summary>
        ///	Returns the list of duplicated gtins.
        /// </summary>
        public IEnumerable<String> DuplicateProductGtins
        {
            get { return _duplicateProductGtins; }
        }

        /// <summary>
        /// Returns a dictionary of performance number to planogram total value
        /// </summary>
        public Single?[] PlanogramPerformanceTotals
        {
            get { return _performanceTotals; }
        }

        /// <summary>
        /// Returns a dictionary of position id to planogram position detail
        /// </summary>
        public Dictionary<Object, PlanogramPositionDetails> PositionDetails
        {
            get { return _positionIdToPositionDetails; }
        }

        /// <summary>
        /// Returns the meta data helper for data that is external to 
        /// the planogram.
        /// </summary>
        public IPlanogramMetadataHelper MetadataHelper
        {
            get { return _metadataHelper; }
        }

        /// <summary>
        /// Gets the design height of the planogram.
        /// </summary>
        public Single PlanogramDesignHeight
        {
            get { return _planogramDesignHeight; }
        }

        /// <summary>
        /// Gets the design width of the planogram.
        /// </summary>
        public Single PlanogramDesignWidth
        {
            get { return _planogramDesignWidth; }
        }

        #endregion

        #region Constructor
        private PlanogramMetadataDetails() { } //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static PlanogramMetadataDetails NewPlanogramMetadataDetails(
            Planogram plan, IPlanogramMetadataHelper metadataHelper, PlanogramMerchandisingGroupList merchGroupList)
        {
            PlanogramMetadataDetails item = new PlanogramMetadataDetails();
            item.Create(plan, merchGroupList);
            item._metadataHelper = metadataHelper;
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static PlanogramMetadataDetails NewPlanogramMetadataDetails(
            Planogram plan, IPlanogramMetadataHelper metadataHelper)
        {
            PlanogramMetadataDetails item = new PlanogramMetadataDetails();
            item.Create(plan, null);
            item._metadataHelper = metadataHelper;
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="merchGroupList"></param>
        private void Create(Planogram plan, PlanogramMerchandisingGroupList merchGroupList)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                _planogram = plan;

                Single designHeight, designWidth, designTopDownOffset;
                this.Planogram.GetBlockingAreaSize(out designHeight, out designWidth, out designTopDownOffset);

                _planogramDesignHeight = designHeight;
                _planogramDesignWidth = designWidth;
                _planogramDesignOffset = designTopDownOffset;

                //get all subcomponent placements
                List<PlanogramSubComponentPlacement> allSubPlacements = this.Planogram.GetPlanogramSubComponentPlacements().ToList();
                _subComponentPlacements = allSubPlacements;


                //create the sub placement infos
                _infoBySubComponentPlacementId = new Dictionary<Object, PlanogramSubComponentPlacementInfo>(allSubPlacements.Count);
                foreach (PlanogramSubComponentPlacement sp in allSubPlacements)
                {
                    _infoBySubComponentPlacementId[sp.Id] =
                        PlanogramSubComponentPlacementInfo.NewPlanogramSubComponentPlacementInfo(sp, allSubPlacements);
                }

                //create merch group infos
                _merchGroupInfos = new List<PlanogramMerchandisingGroupInfo>();
                _merchGroupInfoBySubComponentPlacementId = new Dictionary<Object, PlanogramMerchandisingGroupInfo>();
                _designViewBoundsByPositionId = new Dictionary<Object, RectValue>();


                Boolean hasBlocking = plan.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) != null;

                Boolean disposeOfMerchandisingGroups = merchGroupList == null;
                if (merchGroupList == null) merchGroupList = plan.GetMerchandisingGroups();
                try
                {
                    foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
                    {
                        PlanogramMerchandisingGroupInfo groupInfo =
                            PlanogramMerchandisingGroupInfo.NewPlanogramMerchandisingGroupInfo(
                            merchGroup, _infoBySubComponentPlacementId);


                        _merchGroupInfos.Add(groupInfo);

                        foreach (Object subId in groupInfo.SucomponentPlacementIds)
                        {
                            _merchGroupInfoBySubComponentPlacementId[subId] = groupInfo;
                        }


                        //if we have blocking the we need to take a dictionary of the design view position placements.
                        if (hasBlocking)
                        {
                            foreach (PlanogramPositionPlacement p in merchGroup.PositionPlacements)
                            {
                                DesignViewPosition designViewPos = new DesignViewPosition(p, _planogramDesignOffset);

                                _designViewBoundsByPositionId[p.Position.Id] =
                                    new RectValue(
                                        designViewPos.BoundX,
                                        designViewPos.BoundY,
                                        0,
                                        designViewPos.BoundWidth,
                                        designViewPos.BoundHeight,
                                        1);
                            }
                        }

                    }

                    //products
                    _duplicateProductGtins = plan.Products.GroupBy(p => p.Gtin).Where(g => g.Count() > 1).Select(g => g.Key).ToList();

                    _productIdToPositions = new Dictionary<Object, IEnumerable<PlanogramPosition>>();
                    foreach (var posGroup in plan.Positions.GroupBy(p => p.PlanogramProductId))
                    {
                        _productIdToPositions[posGroup.Key] = posGroup.ToArray();
                    }

                    _gtinToAssortmentProduct = (plan.Assortment != null) ?
                        plan.Assortment.Products.ToLookupDictionary(p => (p.Gtin != null) ? p.Gtin : String.Empty)
                        : new Dictionary<String, PlanogramAssortmentProduct>();

                    //If assortment exists, setup assortment rule related caches
                    if (plan.Assortment != null && plan.Assortment.Products.Count > 0)
                    {
                        //product families groupings
                        _planogramAssortmentProductFamilies = plan.Assortment.Products.GroupBy(p => p.FamilyRuleName);

                        //gtin to planogram products dictionary
                        _gtinToPlanogramProducts = plan.Products.GroupBy(p => p.Gtin).ToDictionary(p => p.Key);
                    }


                    //positions
                    _positionIdToPositionDetails = new Dictionary<Object, PlanogramPositionDetails>();
                    _positionPlacementIdToWorldSpaceBounds = new Dictionary<PlanogramPositionPlacementId, BoundaryInfo>();
                    foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroupList)
                    {
                        foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                        {
                            PlanogramPositionDetails posDetails = positionPlacement.GetPositionDetails();
                            _positionIdToPositionDetails[positionPlacement.Position.Id] = posDetails;

                            PlanogramPositionPlacementId placementId = new PlanogramPositionPlacementId(positionPlacement.Position);
                            _positionPlacementIdToWorldSpaceBounds[placementId] = positionPlacement.GetPlanogramRelativeBoundaryInfo();
                        }
                    }


                    #region Blocking products

                    //  Get the Blocking Area Size to adjust the coordinate percentage values by.
                    _productsByBlockingGroupId = new Dictionary<Object, List<PlanogramProduct>>();

                    //Single topDownOffset;
                    //Single blockingHeight;
                    //Single blockingWidth;
                    //this.Planogram.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out topDownOffset);

                    ILookup<PlanogramProduct, RectValue> rectValueByPlacement = null;

                    foreach (PlanogramBlocking blocking in this.Planogram.Blocking)
                    {
                        if (!blocking.Groups.Any()) continue;

                        Dictionary<PlanogramBlockingGroup, List<RectValue>> rectValuesByBlockingGroup =
                            LookupRectValuesByBlockingGroup(blocking, designWidth, designHeight);


                        if (rectValueByPlacement == null)
                        {
                            rectValueByPlacement = LookupRectValueByPlacement(merchGroupList, designTopDownOffset);
                        }


                        foreach (IGrouping<PlanogramProduct, RectValue> rectValuesByProduct in rectValueByPlacement)
                        {
                            PlanogramBlockingGroup blockingGroup = MaxIntersectingBlockingGroup(rectValuesByBlockingGroup, rectValuesByProduct);
                            if (blockingGroup == null) continue;

                            if (!_productsByBlockingGroupId.ContainsKey(blockingGroup.Id))
                            {
                                _productsByBlockingGroupId.Add(
                                   blockingGroup.Id, new List<PlanogramProduct>() { rectValuesByProduct.Key });
                            }
                            //else
                            //{
                            //    List<PlanogramProduct> listOfProducts = _productsByBlockingGroupId[blockingGroup.Id];
                            //    if (!listOfProducts.Contains(rectValuesByProduct.Key))
                            //    {
                            //        listOfProducts.Add(rectValuesByProduct.Key);
                            //    }
                            //}

                        }
                    }

                    #endregion

                }
                finally
                {
                    if (disposeOfMerchandisingGroups) merchGroupList.Dispose();
                }

                //performance data
                _productIdToPerformanceData = new Dictionary<Object, PlanogramPerformanceData>();
                foreach (PlanogramPerformanceData dataItem in plan.Performance.PerformanceData)
                    if (!_productIdToPerformanceData.ContainsKey(dataItem.PlanogramProductId))
                        _productIdToPerformanceData.Add(dataItem.PlanogramProductId, dataItem);

                _performanceTotals = CreatePlanogramPerformanceTotals(plan);
                PopulateProductRankByPerformance(plan, _performanceTotals);

            }
        }

        #endregion

        #region Methods

        #region SubComponents

        /// <summary>
        /// Returns an enumerable of all merchandising space on the plan.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramMerchandisingSpace> GetAllMerchandisingSpace()
        {
            return _infoBySubComponentPlacementId.Values
                .Where(s => s.MerchandisingSpace != null)
                .Select(s => s.MerchandisingSpace).ToList();
        }

        public PlanogramMerchandisingSpace FindMerchandisingSpaceBySubComponentPlacementId(Object subComponentPlacementId)
        {
            PlanogramSubComponentPlacementInfo subPlacementInfo;
            if (_infoBySubComponentPlacementId.TryGetValue(subComponentPlacementId, out subPlacementInfo))
            {
                return subPlacementInfo.MerchandisingSpace;
            }
            return null;
        }

        public IEnumerable<PlanogramMerchandisingSpace> GetAssemblyComponentMerchandisingSpace(
            PlanogramAssemblyComponent assemblyComponent, PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramMerchandisingSpace> merchandisingSpace = new List<PlanogramMerchandisingSpace>();
            if (assemblyComponent == null || fixtureAssembly == null || fixtureItem == null) return merchandisingSpace;

            foreach (PlanogramSubComponentPlacement placement in this.SubComponentPlacements)
            {
                if (placement.SubComponent == null || placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;
                if (placement.AssemblyComponent == null) continue;
                if (placement.FixtureAssembly == null) continue;
                if (placement.FixtureItem == null) continue;

                if (!Object.Equals(placement.AssemblyComponent.Id, assemblyComponent.Id)) continue;
                if (!Object.Equals(placement.FixtureAssembly.Id, fixtureAssembly.Id)) continue;
                if (!Object.Equals(placement.FixtureItem.Id, fixtureItem.Id)) continue;

                PlanogramMerchandisingSpace space = FindMerchandisingSpaceBySubComponentPlacementId(placement.Id);
                if (space == null) continue;

                merchandisingSpace.Add(space);
            }

            return merchandisingSpace;
        }

        public IEnumerable<PlanogramMerchandisingSpace> GetFixtureAssemblyMerchandisingSpace(
            PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramMerchandisingSpace> merchandisingSpace = new List<PlanogramMerchandisingSpace>();
            if (fixtureAssembly == null || fixtureItem == null) return merchandisingSpace;

            foreach (PlanogramSubComponentPlacement placement in this.SubComponentPlacements)
            {
                if (placement.SubComponent == null || placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;
                if (placement.FixtureAssembly == null) continue;
                if (placement.FixtureItem == null) continue;

                if (!Object.Equals(placement.FixtureAssembly.Id, fixtureAssembly.Id)) continue;
                if (!Object.Equals(placement.FixtureItem.Id, fixtureItem.Id)) continue;

                PlanogramMerchandisingSpace space = FindMerchandisingSpaceBySubComponentPlacementId(placement.Id);
                if (space == null) continue;

                merchandisingSpace.Add(space);
            }

            return merchandisingSpace;
        }

        /// <summary>
        /// Returns the merchandising spaces for the given fixture component.
        /// </summary>
        /// <param name="fixtureComponent"></param>
        /// <param name="fixtureItem"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramMerchandisingSpace> GetFixtureComponentMerchandisingSpace(
            PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramMerchandisingSpace> merchandisingSpace = new List<PlanogramMerchandisingSpace>();
            if (fixtureComponent == null || fixtureItem == null) return merchandisingSpace;

            foreach (PlanogramSubComponentPlacement placement in this.SubComponentPlacements)
            {
                if (placement.SubComponent == null || placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;
                if (placement.FixtureComponent == null) continue;
                if (placement.FixtureItem == null) continue;

                if (!Object.Equals(placement.FixtureComponent.Id, fixtureComponent.Id)) continue;
                if (!Object.Equals(placement.FixtureItem.Id, fixtureItem.Id)) continue;

                PlanogramMerchandisingSpace space = FindMerchandisingSpaceBySubComponentPlacementId(placement.Id);
                if (space == null) continue;

                merchandisingSpace.Add(space);
            }

            return merchandisingSpace;
        }

        public IEnumerable<PlanogramMerchandisingSpace> GetFixtureItemMerchandisingSpace(PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramMerchandisingSpace> merchandisingSpace = new List<PlanogramMerchandisingSpace>();
            if (fixtureItem == null) return merchandisingSpace;

            foreach (PlanogramSubComponentPlacement placement in this.SubComponentPlacements)
            {
                if (placement.SubComponent == null || placement.SubComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) continue;
                if (placement.FixtureItem == null) continue;

                if (!Object.Equals(placement.FixtureItem.Id, fixtureItem.Id)) continue;

                PlanogramMerchandisingSpace space = FindMerchandisingSpaceBySubComponentPlacementId(placement.Id);
                if (space == null) continue;

                merchandisingSpace.Add(space);
            }

            return merchandisingSpace;
        }

        /// <summary>
        /// Returns the subcomponent placement for the given position.
        /// </summary>
        public PlanogramSubComponentPlacement FindSubComponentPlacementForPosition(PlanogramPosition position)
        {
            foreach (PlanogramSubComponentPlacement placement in this.SubComponentPlacements)
            {
                if (placement.IsPositionAssociated(position)) return placement;
            }
            return null;
        }

        /// <summary>
        /// Returns the info for the merchandising group to which the given subcomponent placement id 
        /// belongs.
        /// </summary>
        /// <param name="subComponentPlacementId">the id of the placement to look for.</param>
        /// <returns>the group info.</returns>
        public PlanogramMerchandisingGroupInfo GetMerchandisingGroupInfo(Object subComponentPlacementId)
        {
            PlanogramMerchandisingGroupInfo groupInfo;
            if (_merchGroupInfoBySubComponentPlacementId.TryGetValue(subComponentPlacementId, out groupInfo))
            {
                return groupInfo;
            }
            return null;
        }

        /// <summary>
        /// Returns true if the merchandising group to which the sub placement id belongs is overfilled.
        /// </summary>
        /// <param name="subComponentPlacementId">the id of the subcomponent placement to find the merch group of.</param>
        /// <param name="axis">Optional axis to check</param>
        /// <returns></returns>
        public Boolean IsMerchandisingGroupOverfilled(Object subComponentPlacementId, AxisType? axis = null)
        {
            PlanogramMerchandisingGroupInfo groupInfo;
            if (_merchGroupInfoBySubComponentPlacementId.TryGetValue(subComponentPlacementId, out groupInfo))
            {
                if (axis.HasValue)
                {
                    switch (axis.Value)
                    {
                        case AxisType.X: return groupInfo.IsOverfilledX;
                        case AxisType.Y: return groupInfo.IsOverfilledY;
                        case AxisType.Z: return groupInfo.IsOverfilledZ;
                    }
                }
                return groupInfo.IsOverfilledX || groupInfo.IsOverfilledY || groupInfo.IsOverfilledZ;
            }
            return false;
        }

        /// <summary>
        /// Enumerates through infos for all subcomponent placements on the plan
        /// </summary>
        /// <returns>IEnumerable of infos</returns>
        public IEnumerable<PlanogramSubComponentPlacementInfo> EnumerateSubComponentPlacementInfos()
        {
            foreach (var entry in _infoBySubComponentPlacementId) yield return entry.Value;
        }

        #endregion

        #region Positions & Products

        /// <summary>
        /// Returns a list of positions for the given PlanogramProductId
        /// </summary>
        public IEnumerable<PlanogramPosition> FindPositionsByPlanogramProductId(Object planogramProductId)
        {
            IEnumerable<PlanogramPosition> positions;
            if (_productIdToPositions.TryGetValue(planogramProductId, out positions))
            {
                return positions;
            }
            else
            {
                return new PlanogramPosition[] { };
            }
        }

        /// <summary>
        /// Returns the position details for the given planogram position id.
        /// </summary>
        public PlanogramPositionDetails FindPositionDetailsByPlanogramPositionId(Object planogramPositionId)
        {
            PlanogramPositionDetails details = null;
            _positionIdToPositionDetails.TryGetValue(planogramPositionId, out details);
            return details;
        }

        /// <summary>
        /// Returns the assortment product with the given gtin.
        /// </summary>
        public PlanogramAssortmentProduct FindPlanogramAssortmentProductByGtin(String productGtin)
        {
            PlanogramAssortmentProduct product = null;
            _gtinToAssortmentProduct.TryGetValue(productGtin, out product);
            return product;
        }

        /// <summary>
        /// Returns true of the given gtin exists in masterdata.
        /// </summary>
        public Boolean IsProductGtinInMasterData(String gtin)
        {
            if (_metadataHelper == null) return false;
            else return _metadataHelper.IsProductGtinInMasterData(gtin);
        }

        /// <summary>
        /// Returns true if the product is illegal for the location/category combination
        /// (is present in masterdata.locationproductillegal)
        /// </summary>
        public Boolean IsProductIllegal(Planogram plan, String gtin)
        {
            if (_metadataHelper == null) return false;
            else return _metadataHelper.IsProductIllegal(plan, gtin);
        }

        /// <summary>
        /// Returns true if the product is not legal for the location/category combination
        /// (is not present in masterdata.locationproductlegal)
        /// </summary>
        public Boolean IsProductNotLegal(Planogram plan, String gtin)
        {
            if (_metadataHelper == null) return false;
            else return _metadataHelper.IsProductNotLegal(plan, gtin);
        }

        /// <summary>
        /// Returns the 2d design view bounds for the position with the given id.
        /// </summary>
        public RectValue GetDesignViewBoundsByPositionId(Object positionId)
        {
            ////create the dictionary if required.
            //if (_designViewBoundsByPositionId == null)
            //{
            //    _designViewBoundsByPositionId = new Dictionary<Object, RectValue>(this.Planogram.Positions.Count);

            //    using (PlanogramMerchandisingGroupList merchGroupList = this.Planogram.GetMerchandisingGroups())
            //    {
            //        foreach (PlanogramMerchandisingGroup merchGroup in merchGroupList)
            //        {
            //            foreach (PlanogramPositionPlacement p in merchGroup.PositionPlacements)
            //            {
            //                DesignViewPosition designViewPos = new DesignViewPosition(p, _planogramDesignOffset);

            //                _designViewBoundsByPositionId[p.Position.Id] =
            //                    new RectValue(
            //                        designViewPos.BoundX,
            //                        designViewPos.BoundY,
            //                        0,
            //                        designViewPos.BoundWidth,
            //                        designViewPos.BoundHeight,
            //                        1);
            //            }
            //        }

            //    }
            //}

            RectValue bounds = new RectValue();
            _designViewBoundsByPositionId.TryGetValue(positionId, out bounds);
            return bounds;
        }

        /// <summary>
        /// Returns the performance data for the given product id.
        /// </summary>
        public PlanogramPerformanceData FindPerformanceDataByPlanogramProductId(Object planogramProductId)
        {
            PlanogramPerformanceData data = null;
            _productIdToPerformanceData.TryGetValue(planogramProductId, out data);
            return data;
        }

        /// <summary>
        /// Returns all of the assortment products for a product family
        /// </summary>
        /// <param name="familyName"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramProduct> FindPlanogramProductsByFamilyName(String familyName)
        {
            //Get products in family
            IGrouping<String, PlanogramAssortmentProduct> familyProducts = _planogramAssortmentProductFamilies.FirstOrDefault(p => p.Key.Equals(familyName));
            if (familyProducts != null)
            {
                //Now lookup the actual plan products
                List<PlanogramProduct> familyPlanogramProducts = new List<PlanogramProduct>();
                foreach (PlanogramAssortmentProduct familyProduct in familyProducts)
                {
                    IGrouping<String, PlanogramProduct> planProducts = null;
                    if (_gtinToPlanogramProducts.TryGetValue(familyProduct.Gtin, out planProducts))
                    {
                        familyPlanogramProducts.AddRange(planProducts.ToArray());
                    }
                }
                return familyPlanogramProducts;
            }
            return null;
        }

        /// <summary>
        /// Returns the planogram product with the given gtin.
        /// </summary>
        public PlanogramProduct FindPlanogramProductByGtin(String gtin)
        {
            if (gtin == null) gtin = String.Empty;

            IGrouping<String, PlanogramProduct> planProducts = null;
            _gtinToPlanogramProducts.TryGetValue(gtin, out planProducts);

            if (planProducts != null) return planProducts.FirstOrDefault();
            return null;
        }

        #endregion

        #region Planogram

        /// <summary>
        /// Returns the settings to use when producing the planogram thumbnail.
        /// </summary>
        public IPlanogramImageSettings GetPlanogramImageSettings(Planogram plan)
        {
            if (_metadataHelper == null) return null;
            else return _metadataHelper.GetPlanogramImageSettings(plan);
        }

        /// <summary>
        /// Creates the array of performance data totals.
        /// </summary>
        private static Single?[] CreatePlanogramPerformanceTotals(Planogram planogram)
        {
            Single?[] totals = new Single?[Constants.MaximumMetricsPerPerformanceSource];

            //return out if we have no performance data.
            if (planogram.Performance == null || planogram.Performance.PerformanceData == null)
            {
                return totals;
            }

            //only include data for placed products.
            List<Object> placedProductIds = planogram.Positions.Select(p => p.PlanogramProductId).Distinct().ToList();

            IEnumerable<PlanogramPerformanceData> data =
                planogram.Performance.PerformanceData.Where(d => placedProductIds.Contains(d.PlanogramProductId)).ToList();

            foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
            {
                totals[(Int32)column - 1] = data.Sum(o => o.GetValue(column));
            }
            
            return totals;
        }

        /// <summary>
        /// Populates the performance data ranks.
        /// </summary>
        private static void PopulateProductRankByPerformance(Planogram planogram, Single?[] totals)
        {
            List<Object> productIds = planogram.Positions.Select(p => p.PlanogramProductId).Distinct().ToList();
            List<PlanogramPerformanceData> planPerformanceData = planogram.Performance.PerformanceData.Where(d => productIds.Contains(d.PlanogramProductId)).ToList();

            foreach (PerformanceSourceMetricColumnNumber column in Enum.GetValues(typeof(PerformanceSourceMetricColumnNumber)))
            {
                SetRank(totals, planPerformanceData, column);
            }
            
        }

        private static void SetRank(float?[] totals, List<PlanogramPerformanceData> planPerformanceData, PerformanceSourceMetricColumnNumber column)
        {
            if (totals[(Int32)column - 1] != null)
            {
                Int32 i = 0;
                foreach (PlanogramPerformanceData data1 in planPerformanceData.OrderByDescending(p => p.GetValue(column)))
                {
                    Single? value = data1.GetValue(column);
                    if (value.HasValue)
                    {
                        if (value.Value > 0)
                        {
                            i++;
                            data1.SetRankValue(i, column);
                        }
                        else
                        {
                            data1.SetRankValue(0, column);
                        }
                    }
                }
            }
        }
        #endregion

        #region PlanogramConsumerDecisionTreeNode

        //public Dictionary<Object, Int32> CalculateCountOfNodeProductOld(Object id)
        //{
        //    Dictionary<Object, Int32> productCount = new Dictionary<Object, Int32>();

        //    PlanogramConsumerDecisionTreeNode node = Planogram.ConsumerDecisionTree.RootNode;
        //    List<PlanogramProduct> PlanogramProducts = new List<PlanogramProduct>();
        //    foreach (PlanogramConsumerDecisionTreeNode cdtNode in node.FetchAllChildNodes())
        //    {

        //        foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProd in cdtNode.Products)
        //        {
        //            PlanogramProducts.Add(this.Planogram.Products.FindById(nodeProd.PlanogramProductId));
        //        }
        //        productCount.Add(cdtNode.Id, cdtNode.Products.Count);
        //    }
        //    return productCount;
        //}

        /// <summary>
        /// Calculate the total number of products associated with a node
        /// </summary>
        /// <param name="parentNode">The specified CDT Node</param>
        /// <returns></returns>
        public Int32 CalculateCountOfNodeProduct(PlanogramConsumerDecisionTreeNode parentNode)
        {
            Int32 nodeProductCount = 0;

            // iterate through all the child nodes
            foreach (PlanogramConsumerDecisionTreeNode subNode in parentNode.FetchAllChildNodes())
            {
                // and get the count of products in the sub node
                var subNodeProducts = subNode.Products;
                var subNodeProductCount = subNode.Products.Count();

                nodeProductCount += subNodeProductCount;
            }

            return nodeProductCount;
        }

        /// <summary>
        /// Calculate the number of placed products that are in the recommended assortment
        /// </summary>
        /// <param name="cdtNode">The specified CDT Node</param>
        /// <returns></returns>
        public Int32 CalculateNodeRecommendedAssortmentProductCount(PlanogramConsumerDecisionTreeNode cdtNode)
        {
            int countMatchingProducts = 0;
            List<PlanogramProduct> PlanogramProducts = new List<PlanogramProduct>();

            var nodeProducts = cdtNode.Products.ToList();

            if (!nodeProducts.Any())
            {
                var childNodeProducts = cdtNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts();

                if (childNodeProducts.Any())
                {
                    nodeProducts = childNodeProducts.ToList();
                }
            }

            foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProd in nodeProducts)
            {
                PlanogramProducts.Add(this.Planogram.Products.FindById(nodeProd.PlanogramProductId));
            }

            List<PlanogramAssortmentProduct> assortmentProduct = Planogram.Assortment.Products.ToList();

            foreach (PlanogramAssortmentProduct assortProduct in assortmentProduct)
            {
                if (!assortProduct.IsRanged) continue;
                foreach (PlanogramProduct planProd in PlanogramProducts)
                {
                    if (assortProduct.Gtin == planProd.Gtin)
                    {
                        countMatchingProducts++;
                    }
                }
            }

            return countMatchingProducts;
        }

        /// <summary>
        /// Calculate the number of placed products in the node
        /// </summary>
        /// <param name="parentNode">The specified CDT Node</param>
        /// <returns></returns>
        public Int32 CalculatePlacedProductCount(PlanogramConsumerDecisionTreeNode parentNode)
        {
            Int32 placedCount = 0;

            // iterate through all the child nodes
            foreach (PlanogramConsumerDecisionTreeNode subNode in parentNode.FetchAllChildNodes())
            {
                // and get the count of products in the sub node
                var subNodeProducts = subNode.Products;
                foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProd in subNode.Products)
                {
                    var planProduct = this.Planogram.Products.FindById(nodeProd.PlanogramProductId);

                    if (planProduct != null && planProduct.MetaPositionCount > 0)
                    {
                        placedCount++;
                    }
                }
            }

            return placedCount;
        }

        /// <summary>
        /// Returns a list of all products in child nodes of the given node, but only if those products 
        /// the planogram products list
        /// </summary>
        /// <param name="parentNode"></param>
        /// <returns></returns>
        public List<PlanogramProduct> FetchChildNodeProducts(PlanogramConsumerDecisionTreeNode parentNode)
        {
            List<PlanogramProduct> planogramProductsList = new List<PlanogramProduct>();

            //build a list fo the child nodes for this node
            var childNodes = parentNode.FetchAllChildNodes().ToList();

            foreach (PlanogramConsumerDecisionTreeNode cdtNode in childNodes)
            {
                foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProd in cdtNode.Products)
                {
                    var planProducts = this.Planogram.Products.FindById(nodeProd.PlanogramProductId);
                    //CCM-19193 - deterime that the product exists on the planogram products list - if not do not add to the list returned.
                    // this prevents errors when calculating the meta data values for the parent node.
                    if (planProducts != null)
                    {
                        planogramProductsList.Add(planProducts);
                    }
                }
            }

            return planogramProductsList;
        }


        /// <summary>
        /// TODO: Move into PlanogramConsumerDecisionTreeNode class
        /// </summary>
        /// <param name="cdtNode"></param>
        public void CalculateCDTPerformance(PlanogramConsumerDecisionTreeNode cdtNode)
        {
            if (this.Planogram.Performance == null || this.Planogram.Performance.PerformanceData == null)
            {
                return;
            }

            // Get the total planogram performance values
            Single?[] performanceTotals = this.PlanogramPerformanceTotals;

            //var planPerfData = this.Planogram.Performance.PerformanceData;
            //var p1Total = planPerfData.Sum(x => x.P1);
            //var p2Total = planPerfData.Sum(x => x.P2);
            //var p3Total = planPerfData.Sum(x => x.P3);
            //var p4Total = planPerfData.Sum(x => x.P4);
            //var p5Total = planPerfData.Sum(x => x.P5);
            //var p6Total = planPerfData.Sum(x => x.P6);
            //var p7Total = planPerfData.Sum(x => x.P7);
            //var p8Total = planPerfData.Sum(x => x.P8);
            //var p9Total = planPerfData.Sum(x => x.P9);
            //var p10Total = planPerfData.Sum(x => x.P10);
            //var p11Total = planPerfData.Sum(x => x.P11);
            //var p12Total = planPerfData.Sum(x => x.P12);
            //var p13Total = planPerfData.Sum(x => x.P13);
            //var p14Total = planPerfData.Sum(x => x.P14);
            //var p15Total = planPerfData.Sum(x => x.P15);
            //var p16Total = planPerfData.Sum(x => x.P16);
            //var p17Total = planPerfData.Sum(x => x.P17);
            //var p18Total = planPerfData.Sum(x => x.P18);
            //var p19Total = planPerfData.Sum(x => x.P19);
            //var p20Total = planPerfData.Sum(x => x.P20);

            // reset the node performance meta values
            cdtNode.MetaP1 = 0.0F;
            cdtNode.MetaP2 = 0.0F;
            cdtNode.MetaP3 = 0.0F;
            cdtNode.MetaP4 = 0.0F;
            cdtNode.MetaP5 = 0.0F;
            cdtNode.MetaP6 = 0.0F;
            cdtNode.MetaP7 = 0.0F;
            cdtNode.MetaP8 = 0.0F;
            cdtNode.MetaP9 = 0.0F;
            cdtNode.MetaP10 = 0.0F;
            cdtNode.MetaP11 = 0.0F;
            cdtNode.MetaP12 = 0.0F;
            cdtNode.MetaP13 = 0.0F;
            cdtNode.MetaP14 = 0.0F;
            cdtNode.MetaP15 = 0.0F;
            cdtNode.MetaP16 = 0.0F;
            cdtNode.MetaP17 = 0.0F;
            cdtNode.MetaP18 = 0.0F;
            cdtNode.MetaP19 = 0.0F;
            cdtNode.MetaP20 = 0.0F;

            IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> nodeProductList;

            // If this is the root node get all the child node products
            if (cdtNode.ParentNode == null)
            {
                nodeProductList = cdtNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts().ToList();
            }
            else
            {
                // otherwise just use the Products list
                nodeProductList = cdtNode.Products.ToList();
            }

            foreach (PlanogramConsumerDecisionTreeNodeProduct cdtProductItem in nodeProductList)
            {
                //PlanogramProduct productItem = this.Planogram.Products.FindById(cdtProductItem.PlanogramProductId);
                //if (productItem == null) continue;

                //PlanogramPerformanceData productItemPerf = productItem.GetPlanogramPerformanceData();
                PlanogramPerformanceData productItemPerf = this.FindPerformanceDataByPlanogramProductId(cdtProductItem.PlanogramProductId);
                if (productItemPerf == null) continue;

                cdtNode.MetaP1 = PerformanceCheckNullUpdate(productItemPerf.P1, cdtNode.MetaP1);
                cdtNode.MetaP2 = PerformanceCheckNullUpdate(productItemPerf.P2, cdtNode.MetaP2);
                cdtNode.MetaP3 = PerformanceCheckNullUpdate(productItemPerf.P3, cdtNode.MetaP3);
                cdtNode.MetaP4 = PerformanceCheckNullUpdate(productItemPerf.P4, cdtNode.MetaP4);
                cdtNode.MetaP5 = PerformanceCheckNullUpdate(productItemPerf.P5, cdtNode.MetaP5);
                cdtNode.MetaP6 = PerformanceCheckNullUpdate(productItemPerf.P6, cdtNode.MetaP6);
                cdtNode.MetaP7 = PerformanceCheckNullUpdate(productItemPerf.P7, cdtNode.MetaP7);
                cdtNode.MetaP8 = PerformanceCheckNullUpdate(productItemPerf.P8, cdtNode.MetaP8);
                cdtNode.MetaP9 = PerformanceCheckNullUpdate(productItemPerf.P9, cdtNode.MetaP9);
                cdtNode.MetaP10 = PerformanceCheckNullUpdate(productItemPerf.P10, cdtNode.MetaP10);
                cdtNode.MetaP11 = PerformanceCheckNullUpdate(productItemPerf.P11, cdtNode.MetaP11);
                cdtNode.MetaP12 = PerformanceCheckNullUpdate(productItemPerf.P12, cdtNode.MetaP12);
                cdtNode.MetaP13 = PerformanceCheckNullUpdate(productItemPerf.P13, cdtNode.MetaP13);
                cdtNode.MetaP14 = PerformanceCheckNullUpdate(productItemPerf.P14, cdtNode.MetaP14);
                cdtNode.MetaP15 = PerformanceCheckNullUpdate(productItemPerf.P15, cdtNode.MetaP15);
                cdtNode.MetaP16 = PerformanceCheckNullUpdate(productItemPerf.P16, cdtNode.MetaP16);
                cdtNode.MetaP17 = PerformanceCheckNullUpdate(productItemPerf.P17, cdtNode.MetaP17);
                cdtNode.MetaP18 = PerformanceCheckNullUpdate(productItemPerf.P18, cdtNode.MetaP18);
                cdtNode.MetaP19 = PerformanceCheckNullUpdate(productItemPerf.P19, cdtNode.MetaP19);
                cdtNode.MetaP20 = PerformanceCheckNullUpdate(productItemPerf.P20, cdtNode.MetaP20);
            }

            cdtNode.MetaP1Percentage = PerformanceCheckNullPercent(cdtNode.MetaP1, performanceTotals[0]);
            cdtNode.MetaP2Percentage = PerformanceCheckNullPercent(cdtNode.MetaP2, performanceTotals[1]);
            cdtNode.MetaP3Percentage = PerformanceCheckNullPercent(cdtNode.MetaP3, performanceTotals[2]);
            cdtNode.MetaP4Percentage = PerformanceCheckNullPercent(cdtNode.MetaP4, performanceTotals[3]);
            cdtNode.MetaP5Percentage = PerformanceCheckNullPercent(cdtNode.MetaP5, performanceTotals[4]);
            cdtNode.MetaP6Percentage = PerformanceCheckNullPercent(cdtNode.MetaP6, performanceTotals[5]);
            cdtNode.MetaP7Percentage = PerformanceCheckNullPercent(cdtNode.MetaP7, performanceTotals[6]);
            cdtNode.MetaP8Percentage = PerformanceCheckNullPercent(cdtNode.MetaP8, performanceTotals[7]);
            cdtNode.MetaP9Percentage = PerformanceCheckNullPercent(cdtNode.MetaP9, performanceTotals[8]);
            cdtNode.MetaP10Percentage = PerformanceCheckNullPercent(cdtNode.MetaP10, performanceTotals[9]);
            cdtNode.MetaP11Percentage = PerformanceCheckNullPercent(cdtNode.MetaP11, performanceTotals[10]);
            cdtNode.MetaP12Percentage = PerformanceCheckNullPercent(cdtNode.MetaP12, performanceTotals[11]);
            cdtNode.MetaP13Percentage = PerformanceCheckNullPercent(cdtNode.MetaP13, performanceTotals[12]);
            cdtNode.MetaP14Percentage = PerformanceCheckNullPercent(cdtNode.MetaP14, performanceTotals[13]);
            cdtNode.MetaP15Percentage = PerformanceCheckNullPercent(cdtNode.MetaP15, performanceTotals[14]);
            cdtNode.MetaP16Percentage = PerformanceCheckNullPercent(cdtNode.MetaP16, performanceTotals[15]);
            cdtNode.MetaP17Percentage = PerformanceCheckNullPercent(cdtNode.MetaP17, performanceTotals[16]);
            cdtNode.MetaP18Percentage = PerformanceCheckNullPercent(cdtNode.MetaP18, performanceTotals[17]);
            cdtNode.MetaP19Percentage = PerformanceCheckNullPercent(cdtNode.MetaP19, performanceTotals[18]);
            cdtNode.MetaP20Percentage = PerformanceCheckNullPercent(cdtNode.MetaP20, performanceTotals[19]);
        }

        /// <summary>
        /// Check for null values in either the source or destination values
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        private Single? PerformanceCheckNullUpdate(Single? source, Single? dest)
        {
            if (source == null)
            {
                return dest;
            }
            else if (dest == null)
            {
                return source;
            }

            return source + dest;
        }

        /// <summary>
        /// Check for a null or 0 total so there are no "divide by zero" exceptions
        /// </summary>
        /// <param name="source"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private Single? PerformanceCheckNullPercent(Single? source, Single? total)
        {
            return (source / (total == null || total == 0.0F ? 1.0F : total));
        }

        #endregion

        #region BlockingGroup

        /// <summary>
        ///     Lookup <see cref="RectValue"/> of placements for each product in the <paramref name="merchandisingGroups"/>.
        /// </summary>
        /// <param name="merchandisingGroups"></param>
        /// <param name="topDownOffset"></param>
        /// <returns></returns>
        private static ILookup<PlanogramProduct, RectValue> LookupRectValueByPlacement(PlanogramMerchandisingGroupList merchandisingGroups, Single topDownOffset)
        {
            return merchandisingGroups.SelectMany(o => o.PositionPlacements)
                .ToLookup(o => o.Product, o => new DesignViewPosition(o, topDownOffset).ToRectValue2D());
        }

        /// <summary>
        ///     Look up the <see cref="RectValue"/> of blocking locations for each blocking group in the <paramref name="blocking"/>.
        /// </summary>
        /// <param name="blocking"></param>
        /// <param name="blockingWidth"></param>
        /// <param name="blockingHeight"></param>
        /// <returns></returns>
        private static Dictionary<PlanogramBlockingGroup, List<RectValue>> LookupRectValuesByBlockingGroup(PlanogramBlocking blocking, Single blockingWidth, Single blockingHeight)
        {
            return blocking.Groups.ToDictionary(o => o,
                o =>
                    o.GetBlockingLocations()
                        .Select(
                            location =>
                                new RectValue(location.X * blockingWidth, location.Y * blockingHeight, 0F,
                                    location.Width * blockingWidth,
                                    location.Height * blockingHeight, 0F))
                        .ToList());
        }

        /// <summary>
        ///     Get the total area determined by the intersection of both <paramref name="rectOne"/> and <paramref name="rectTwo"/>.
        /// </summary>
        /// <param name="rectOne"></param>
        /// <param name="rectTwo"></param>
        /// <returns></returns>
        private static Single GetIntersectionArea(RectValue rectOne, RectValue rectTwo)
        {
            RectValue? rectValue = rectOne.Intersect(rectTwo);
            return rectValue.HasValue ? rectValue.Value.Width * rectValue.Value.Height : 0;
        }

        /// <summary>
        ///     Gets the <see cref="PlanogramBlockingGroup"/> that intersects the most with the <paramref name="rectValuesByProduct"/>.
        /// </summary>
        /// <param name="rectValuesByBlockingGroup">The dictionary of rect values (of blocking locations) by blocking group.</param>
        /// <param name="rectValuesByProduct">The collection of rect values (of placements) by product.</param>
        /// <returns>The <see cref="PlanogramBlockingGroup"/> that interesects the most with the product placements.</returns>
        private static PlanogramBlockingGroup MaxIntersectingBlockingGroup(Dictionary<PlanogramBlockingGroup, List<RectValue>> rectValuesByBlockingGroup, IEnumerable<RectValue> rectValuesByProduct)
        {
            return rectValuesByBlockingGroup
                .Select(o => new
                {
                    blockingGroup = o,
                    totalArea = o.Value
                        .Sum(value =>
                        {
                            return rectValuesByProduct.Sum(r => GetIntersectionArea(value, r));
                        })
                })
                .Where(o => o.totalArea > 0)
                .OrderByDescending(o => o.totalArea)
                .Select(o => o.blockingGroup.Key)
                .FirstOrDefault();
        }


        /// <summary>
        /// Returns the list of products for the given blocking group.
        /// </summary>
        public List<PlanogramProduct> GetPlanogramProductsByBlockingGroup(PlanogramBlockingGroup blocking)
        {
            List<PlanogramProduct> prodList = new List<PlanogramProduct>();
            if (blocking == null) return prodList;

            _productsByBlockingGroupId.TryGetValue(blocking.Id, out prodList);

            if (prodList == null) return new List<PlanogramProduct>();

            return prodList;
        }

        /// <summary>
        /// Calculate the space percentage for each blocking group
        /// </summary>
        /// <param name="blockingGroup"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Single GetBlockingGroupSpacePercentage(PlanogramBlockingGroup blockingGroup, PlanogramBlockingType type)
        {
            Single spacePercentage = 0;

            foreach (PlanogramBlocking blockingPlan in this.Planogram.Blocking)
            {
                foreach (PlanogramBlockingGroup group in blockingPlan.Groups)
                {
                    if (type == blockingPlan.Type && group.Colour == blockingGroup.Colour)
                    {
                        spacePercentage = group.TotalSpacePercentage;
                        return spacePercentage;
                    }
                }
            }

            return spacePercentage;
        }

        public Single? GetLinearProductPlacementinBlockingGroup(PlanogramBlockingGroup blockingGroup)
        {
            List<PlanogramProduct> prodList = new List<PlanogramProduct>();
            Single? productTotalLinearSpace = 0;
            if (blockingGroup == null) return 0;

            if (_productsByBlockingGroupId.TryGetValue(blockingGroup.Id, out prodList))
            {
                productTotalLinearSpace = prodList.Sum(p => p.MetaTotalLinearSpace);
                return productTotalLinearSpace / (blockingGroup.TotalSpacePercentage);
            }

            return 0;
        }

        public Single? GetAreaProductPlacementinBlockingGroup(PlanogramBlockingGroup blockingGroup)
        {
            List<PlanogramProduct> prodList = new List<PlanogramProduct>();
            Single? productsTotalAreaSpace;

            if (blockingGroup == null) return 0;

            if (_productsByBlockingGroupId.TryGetValue(blockingGroup.Id, out prodList))
            {
                productsTotalAreaSpace = prodList.Sum(p => p.MetaTotalAreaSpace);
                return productsTotalAreaSpace / (blockingGroup.TotalSpacePercentage);
            }

            return 0;
        }

        public Single? GetVolumeProductPlacementinBlockingGroup(PlanogramBlockingGroup blockingGroup)
        {
            List<PlanogramProduct> prodList = new List<PlanogramProduct>();
            Single? productsTotalVolumetricSpace;

            if (blockingGroup == null) return 0;

            if (_productsByBlockingGroupId.TryGetValue(blockingGroup.Id, out prodList))
            {
                productsTotalVolumetricSpace = prodList.Sum(p => p.MetaTotalVolumetricSpace);
                return productsTotalVolumetricSpace / (blockingGroup.TotalSpacePercentage * 100);
            }
            return 0;
        }

        public void CalculateBlockingGroupPerformance(PlanogramBlockingGroup blockingGroup, IEnumerable<PlanogramProduct> blockPlanProductCache)
        {
            if (this.Planogram.Performance == null || this.Planogram.Performance.PerformanceData == null)
            {
                return;
            }

            // Get the total planogram performance values
            //var planPerfData = this.Planogram.Performance.PerformanceData;

            //var p1Total = planPerfData.Sum(x => x.P1);
            //var p2Total = planPerfData.Sum(x => x.P2);
            //var p3Total = planPerfData.Sum(x => x.P3);
            //var p4Total = planPerfData.Sum(x => x.P4);
            //var p5Total = planPerfData.Sum(x => x.P5);
            //var p6Total = planPerfData.Sum(x => x.P6);
            //var p7Total = planPerfData.Sum(x => x.P7);
            //var p8Total = planPerfData.Sum(x => x.P8);
            //var p9Total = planPerfData.Sum(x => x.P9);
            //var p10Total = planPerfData.Sum(x => x.P10);
            //var p11Total = planPerfData.Sum(x => x.P11);
            //var p12Total = planPerfData.Sum(x => x.P12);
            //var p13Total = planPerfData.Sum(x => x.P13);
            //var p14Total = planPerfData.Sum(x => x.P14);
            //var p15Total = planPerfData.Sum(x => x.P15);
            //var p16Total = planPerfData.Sum(x => x.P16);
            //var p17Total = planPerfData.Sum(x => x.P17);
            //var p18Total = planPerfData.Sum(x => x.P18);
            //var p19Total = planPerfData.Sum(x => x.P19);
            //var p20Total = planPerfData.Sum(x => x.P20);

            // Get the total planogram performance values
            Single?[] performanceTotals = this.PlanogramPerformanceTotals;

            // reset the node performance meta values
            blockingGroup.MetaP1 = 0.0F;
            blockingGroup.MetaP2 = 0.0F;
            blockingGroup.MetaP3 = 0.0F;
            blockingGroup.MetaP4 = 0.0F;
            blockingGroup.MetaP5 = 0.0F;
            blockingGroup.MetaP6 = 0.0F;
            blockingGroup.MetaP7 = 0.0F;
            blockingGroup.MetaP8 = 0.0F;
            blockingGroup.MetaP9 = 0.0F;
            blockingGroup.MetaP10 = 0.0F;
            blockingGroup.MetaP11 = 0.0F;
            blockingGroup.MetaP12 = 0.0F;
            blockingGroup.MetaP13 = 0.0F;
            blockingGroup.MetaP14 = 0.0F;
            blockingGroup.MetaP15 = 0.0F;
            blockingGroup.MetaP16 = 0.0F;
            blockingGroup.MetaP17 = 0.0F;
            blockingGroup.MetaP18 = 0.0F;
            blockingGroup.MetaP19 = 0.0F;
            blockingGroup.MetaP20 = 0.0F;

            // Get the performance data for the products in the sequence (passed in blockPlanProductCache)
            foreach (var planProductItem in blockPlanProductCache)
            {
                //var productItem = this.Planogram.Products.FindById(planProductItem.Id);
                //if (productItem == null)
                //{
                //    continue;
                //}
                //var productItemPerf = productItem.GetPlanogramPerformanceData();

                PlanogramPerformanceData productItemPerf = this.FindPerformanceDataByPlanogramProductId(planProductItem.Id);

                if (productItemPerf == null)
                {
                    continue;
                }

                blockingGroup.MetaP1 = PerformanceCheckNullUpdate(productItemPerf.P1, blockingGroup.MetaP1);
                blockingGroup.MetaP2 = PerformanceCheckNullUpdate(productItemPerf.P2, blockingGroup.MetaP2);
                blockingGroup.MetaP3 = PerformanceCheckNullUpdate(productItemPerf.P3, blockingGroup.MetaP3);
                blockingGroup.MetaP4 = PerformanceCheckNullUpdate(productItemPerf.P4, blockingGroup.MetaP4);
                blockingGroup.MetaP5 = PerformanceCheckNullUpdate(productItemPerf.P5, blockingGroup.MetaP5);
                blockingGroup.MetaP6 = PerformanceCheckNullUpdate(productItemPerf.P6, blockingGroup.MetaP6);
                blockingGroup.MetaP7 = PerformanceCheckNullUpdate(productItemPerf.P7, blockingGroup.MetaP7);
                blockingGroup.MetaP8 = PerformanceCheckNullUpdate(productItemPerf.P8, blockingGroup.MetaP8);
                blockingGroup.MetaP9 = PerformanceCheckNullUpdate(productItemPerf.P9, blockingGroup.MetaP9);
                blockingGroup.MetaP10 = PerformanceCheckNullUpdate(productItemPerf.P10, blockingGroup.MetaP10);
                blockingGroup.MetaP11 = PerformanceCheckNullUpdate(productItemPerf.P11, blockingGroup.MetaP11);
                blockingGroup.MetaP12 = PerformanceCheckNullUpdate(productItemPerf.P12, blockingGroup.MetaP12);
                blockingGroup.MetaP13 = PerformanceCheckNullUpdate(productItemPerf.P13, blockingGroup.MetaP13);
                blockingGroup.MetaP14 = PerformanceCheckNullUpdate(productItemPerf.P14, blockingGroup.MetaP14);
                blockingGroup.MetaP15 = PerformanceCheckNullUpdate(productItemPerf.P15, blockingGroup.MetaP15);
                blockingGroup.MetaP16 = PerformanceCheckNullUpdate(productItemPerf.P16, blockingGroup.MetaP16);
                blockingGroup.MetaP17 = PerformanceCheckNullUpdate(productItemPerf.P17, blockingGroup.MetaP17);
                blockingGroup.MetaP18 = PerformanceCheckNullUpdate(productItemPerf.P18, blockingGroup.MetaP18);
                blockingGroup.MetaP19 = PerformanceCheckNullUpdate(productItemPerf.P19, blockingGroup.MetaP19);
                blockingGroup.MetaP20 = PerformanceCheckNullUpdate(productItemPerf.P20, blockingGroup.MetaP20);
            }

            blockingGroup.MetaP1Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP1, performanceTotals[0]);
            blockingGroup.MetaP2Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP2, performanceTotals[1]);
            blockingGroup.MetaP3Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP3, performanceTotals[2]);
            blockingGroup.MetaP4Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP4, performanceTotals[3]);
            blockingGroup.MetaP5Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP5, performanceTotals[4]);
            blockingGroup.MetaP6Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP6, performanceTotals[5]);
            blockingGroup.MetaP7Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP7, performanceTotals[6]);
            blockingGroup.MetaP8Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP8, performanceTotals[7]);
            blockingGroup.MetaP9Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP9, performanceTotals[8]);
            blockingGroup.MetaP10Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP10, performanceTotals[9]);
            blockingGroup.MetaP11Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP11, performanceTotals[10]);
            blockingGroup.MetaP12Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP12, performanceTotals[11]);
            blockingGroup.MetaP13Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP13, performanceTotals[12]);
            blockingGroup.MetaP14Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP14, performanceTotals[13]);
            blockingGroup.MetaP15Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP15, performanceTotals[14]);
            blockingGroup.MetaP16Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP16, performanceTotals[15]);
            blockingGroup.MetaP17Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP17, performanceTotals[16]);
            blockingGroup.MetaP18Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP18, performanceTotals[17]);
            blockingGroup.MetaP19Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP19, performanceTotals[18]);
            blockingGroup.MetaP20Percentage = PerformanceCheckNullPercent(blockingGroup.MetaP20, performanceTotals[19]);
        }

        #endregion

        #endregion
    }

    #region PlanogramComponentPlacementId

    /// <summary>
    /// A class used to uniquely identify
    /// a planogram component placement
    /// </summary>
    [Serializable]
    public class PlanogramComponentPlacementId
    {
        #region Properties
        public Object PlanogramId { get; private set; }
        public Object AssemblyComponentId { get; private set; }
        public Object FixtureAssemblyId { get; private set; }
        public Object FixtureComponentId { get; private set; }
        public Object FixtureItemId { get; private set; }

        public Object ComponentId { get; private set; }
        #endregion

        #region Constructors


        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramComponentPlacementId(
            PlanogramAssemblyComponent assemblyComponent,
            PlanogramFixtureAssembly fixtureAssembly,
            PlanogramFixtureItem fixtureItem,
            Planogram planogram)
        {
            this.PlanogramId = (planogram == null) ? null : planogram.Id;
            this.AssemblyComponentId = (assemblyComponent == null) ? null : assemblyComponent.Id;
            this.FixtureAssemblyId = (fixtureAssembly == null) ? null : fixtureAssembly.Id;
            this.FixtureItemId = (fixtureItem == null) ? null : fixtureItem.Id;

            this.ComponentId = (assemblyComponent == null) ? null : assemblyComponent.PlanogramComponentId;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramComponentPlacementId(
            PlanogramFixtureComponent fixtureComponent,
            PlanogramFixtureItem fixtureItem,
            Planogram planogram)
        {
            this.PlanogramId = (planogram == null) ? null : planogram.Id;
            this.FixtureComponentId = (fixtureComponent == null) ? null : fixtureComponent.Id;
            this.FixtureItemId = (fixtureItem == null) ? null : fixtureItem.Id;

            this.ComponentId = (fixtureComponent == null) ? null : fixtureComponent.PlanogramComponentId;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramComponentPlacementId(PlanogramSubComponentPlacement subPlacement)
        {
            this.PlanogramId = (subPlacement.Planogram == null) ? null : subPlacement.Planogram.Id;
            this.AssemblyComponentId = (subPlacement.AssemblyComponent == null) ? null : subPlacement.AssemblyComponent.Id;
            this.FixtureAssemblyId = (subPlacement.FixtureAssembly == null) ? null : subPlacement.FixtureAssembly.Id;
            this.FixtureItemId = (subPlacement.FixtureItem == null) ? null : subPlacement.FixtureItem.Id;
            this.FixtureComponentId = (subPlacement.FixtureComponent == null) ? null : subPlacement.FixtureComponent.Id;
            this.FixtureItemId = (subPlacement.FixtureItem == null) ? null : subPlacement.FixtureItem.Id;
            this.ComponentId = (subPlacement.AssemblyComponent == null) ? null : subPlacement.AssemblyComponent.PlanogramComponentId;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return
                ((PlanogramId == null) ? 0 : PlanogramId.GetHashCode()) ^
                ((AssemblyComponentId == null) ? 0 : AssemblyComponentId.GetHashCode()) ^
                ((FixtureAssemblyId == null) ? 0 : FixtureAssemblyId.GetHashCode()) ^
                ((FixtureComponentId == null) ? 0 : FixtureComponentId.GetHashCode()) ^
                ((FixtureItemId == null) ? 0 : FixtureItemId.GetHashCode());
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramComponentPlacementId other = obj as PlanogramComponentPlacementId;
            if (other != null)
            {
                if (!AreEqual(other.PlanogramId, this.PlanogramId)) return false;
                if (!AreEqual(other.AssemblyComponentId, this.AssemblyComponentId)) return false;
                if (!AreEqual(other.FixtureAssemblyId, this.FixtureAssemblyId)) return false;
                if (!AreEqual(other.FixtureComponentId, this.FixtureComponentId)) return false;
                if (!AreEqual(other.FixtureItemId, this.FixtureItemId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public static Boolean operator ==(PlanogramComponentPlacementId p1, PlanogramComponentPlacementId p2)
        {
            return Object.Equals(p1, p2);
        }

        public static Boolean operator !=(PlanogramComponentPlacementId p1, PlanogramComponentPlacementId p2)
        {
            return !Object.Equals(p1, p2);
        }

        /// <summary>
        /// Asserts if two object references
        /// are equal
        /// </summary>
        private Boolean AreEqual(Object one, Object two)
        {
            if ((one == null) && (two != null)) return false;
            if ((one != null) && (two == null)) return false;
            if ((one == null) && (two == null)) return true;
            return (one.Equals(two));
        }
        #endregion
    }

    #endregion

    #region PlanogramPositionPlacementId

    /// <summary>
    /// A class used to uniquely identify
    /// a planogram position placement
    /// </summary>
    [Serializable]
    public class PlanogramPositionPlacementId
    {
        #region Properties
        public Object PositionId { get; private set; }
        public Object SubComponentId { get; private set; }
        public Object AssemblyComponentId { get; private set; }
        public Object FixtureAssemblyId { get; private set; }
        public Object FixtureItemId { get; private set; }
        public Object FixtureComponentId { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramPositionPlacementId(
            PlanogramPosition position)
        {
            this.PositionId = (position == null) ? null : position.Id;
            this.SubComponentId = (position == null) ? null : position.PlanogramSubComponentId;
            this.AssemblyComponentId = (position == null) ? null : position.PlanogramAssemblyComponentId;
            this.FixtureAssemblyId = (position == null) ? null : position.PlanogramFixtureAssemblyId;
            this.FixtureItemId = (position == null) ? null : position.PlanogramFixtureItemId;
            this.FixtureComponentId = (position == null) ? null : position.PlanogramFixtureComponentId;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return
                ((PositionId == null) ? 0 : PositionId.GetHashCode()) ^
                ((SubComponentId == null) ? 0 : SubComponentId.GetHashCode()) ^
                ((AssemblyComponentId == null) ? 0 : AssemblyComponentId.GetHashCode()) ^
                ((FixtureAssemblyId == null) ? 0 : FixtureAssemblyId.GetHashCode()) ^
                ((FixtureItemId == null) ? 0 : FixtureItemId.GetHashCode()) ^
                ((FixtureComponentId == null) ? 0 : FixtureComponentId.GetHashCode());
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramPositionPlacementId other = obj as PlanogramPositionPlacementId;
            if (other != null)
            {
                if (!AreEqual(other.PositionId, this.PositionId)) return false;
                if (!AreEqual(other.SubComponentId, this.SubComponentId)) return false;
                if (!AreEqual(other.AssemblyComponentId, this.AssemblyComponentId)) return false;
                if (!AreEqual(other.FixtureAssemblyId, this.FixtureAssemblyId)) return false;
                if (!AreEqual(other.FixtureItemId, this.FixtureItemId)) return false;
                if (!AreEqual(other.FixtureComponentId, this.FixtureComponentId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        public static Boolean operator ==(PlanogramPositionPlacementId p1, PlanogramPositionPlacementId p2)
        {
            return Object.Equals(p1, p2);
        }

        public static Boolean operator !=(PlanogramPositionPlacementId p1, PlanogramPositionPlacementId p2)
        {
            return !Object.Equals(p1, p2);
        }

        /// <summary>
        /// Asserts if two object references
        /// are equal
        /// </summary>
        private Boolean AreEqual(Object one, Object two)
        {
            if ((one == null) && (two != null)) return false;
            if ((one != null) && (two == null)) return false;
            if ((one == null) && (two == null)) return true;
            return (one.Equals(two));
        }
        #endregion
    }

    #endregion


}
