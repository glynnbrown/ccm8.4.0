﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//		Created
// V8-27476 : L.Luong
//      Added BlockPlacementX and BlockPlacementY 
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#region Version History : CCM8.2.0
// V8-30765 :I.George
//	Added PlanogramBlockingGroup metaData Properties
// V8-30737 : M.Shelley
//  Added Meta performance fields P1-P20 and MetaP1Percentage-MetaP20Percentage
#endregion
#region Version History : CCM830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingGroupIsLimited \ PlanogramBlockingGroupLimitedPercentage
//  Removed PlanogramBlockingGroupCanOptimise
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingGroup
    {
        #region Constructor
        private PlanogramBlockingGroup() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramBlockingGroup Fetch(IDalContext dalContext, PlanogramBlockingGroupDto dto)
        {
            return DataPortal.FetchChild<PlanogramBlockingGroup>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramBlockingGroupDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, (PlanogramBlockingFillPatternType)dto.FillPatternType);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, dto.CanCompromiseSequence);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, dto.IsRestrictedByComponentType);
            this.LoadProperty<Boolean>(CanMergeProperty, dto.CanMerge);
            this.LoadProperty<Boolean>(IsLimitedProperty, dto.IsLimited);
            this.LoadProperty<Single>(LimitedPercentageProperty, dto.LimitedPercentage);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementPrimaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementPrimaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementSecondaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementSecondaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementTertiaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementTertiaryType);
            this.LoadProperty<Single>(TotalSpacePercentageProperty, dto.TotalSpacePercentage);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Int32?>(MetaCountOfProductsProperty, dto.MetaCountOfProducts);
            this.LoadProperty<Int32?>(MetaCountOfProductRecommendedOnPlanogramProperty, dto.MetaCountOfProductRecommendedOnPlanogram);
            this.LoadProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty, dto.MetaCountOfProductPlacedOnPlanogram);
            this.LoadProperty<Single?>(MetaOriginalPercentAllocatedProperty, dto.MetaOriginalPercentAllocated);
            this.LoadProperty<Single?>(MetaPerformancePercentAllocatedProperty, dto.MetaPerformancePercentAllocated);
            this.LoadProperty<Single?>(MetaFinalPercentAllocatedProperty, dto.MetaFinalPercentAllocated);
            this.LoadProperty<Single?>(MetaLinearProductPlacementPercentProperty, dto.MetaLinearProductPlacementPercent);
            this.LoadProperty<Single?>(MetaAreaProductPlacementPercentProperty, dto.MetaAreaProductPlacementPercent);
            this.LoadProperty<Single?>(MetaVolumetricProductPlacementPercentProperty, dto.MetaVolumetricProductPlacementPercent);

            this.LoadProperty<Single?>(MetaP1Property, dto.P1);
            this.LoadProperty<Single?>(MetaP2Property, dto.P2);
            this.LoadProperty<Single?>(MetaP3Property, dto.P3);
            this.LoadProperty<Single?>(MetaP4Property, dto.P4);
            this.LoadProperty<Single?>(MetaP5Property, dto.P5);
            this.LoadProperty<Single?>(MetaP6Property, dto.P6);
            this.LoadProperty<Single?>(MetaP7Property, dto.P7);
            this.LoadProperty<Single?>(MetaP8Property, dto.P8);
            this.LoadProperty<Single?>(MetaP9Property, dto.P9);
            this.LoadProperty<Single?>(MetaP10Property, dto.P10);
            this.LoadProperty<Single?>(MetaP11Property, dto.P11);
            this.LoadProperty<Single?>(MetaP12Property, dto.P12);
            this.LoadProperty<Single?>(MetaP13Property, dto.P13);
            this.LoadProperty<Single?>(MetaP14Property, dto.P14);
            this.LoadProperty<Single?>(MetaP15Property, dto.P15);
            this.LoadProperty<Single?>(MetaP16Property, dto.P16);
            this.LoadProperty<Single?>(MetaP17Property, dto.P17);
            this.LoadProperty<Single?>(MetaP18Property, dto.P18);
            this.LoadProperty<Single?>(MetaP19Property, dto.P19);
            this.LoadProperty<Single?>(MetaP20Property, dto.P20);

            this.LoadProperty<Single?>(MetaP1PercentageProperty,  dto.MetaP1Percentage);
            this.LoadProperty<Single?>(MetaP2PercentageProperty,  dto.MetaP2Percentage);
            this.LoadProperty<Single?>(MetaP3PercentageProperty,  dto.MetaP3Percentage);
            this.LoadProperty<Single?>(MetaP4PercentageProperty,  dto.MetaP4Percentage);
            this.LoadProperty<Single?>(MetaP5PercentageProperty,  dto.MetaP5Percentage);
            this.LoadProperty<Single?>(MetaP6PercentageProperty,  dto.MetaP6Percentage);
            this.LoadProperty<Single?>(MetaP7PercentageProperty,  dto.MetaP7Percentage);
            this.LoadProperty<Single?>(MetaP8PercentageProperty,  dto.MetaP8Percentage);
            this.LoadProperty<Single?>(MetaP9PercentageProperty,  dto.MetaP9Percentage);
            this.LoadProperty<Single?>(MetaP10PercentageProperty, dto.MetaP10Percentage);
            this.LoadProperty<Single?>(MetaP11PercentageProperty, dto.MetaP11Percentage);
            this.LoadProperty<Single?>(MetaP12PercentageProperty, dto.MetaP12Percentage);
            this.LoadProperty<Single?>(MetaP13PercentageProperty, dto.MetaP13Percentage);
            this.LoadProperty<Single?>(MetaP14PercentageProperty, dto.MetaP14Percentage);
            this.LoadProperty<Single?>(MetaP15PercentageProperty, dto.MetaP15Percentage);
            this.LoadProperty<Single?>(MetaP16PercentageProperty, dto.MetaP16Percentage);
            this.LoadProperty<Single?>(MetaP17PercentageProperty, dto.MetaP17Percentage);
            this.LoadProperty<Single?>(MetaP18PercentageProperty, dto.MetaP18Percentage);
            this.LoadProperty<Single?>(MetaP19PercentageProperty, dto.MetaP19Percentage);
            this.LoadProperty<Single?>(MetaP20PercentageProperty, dto.MetaP20Percentage);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramBlockingGroupDto GetDataTransferObject(PlanogramBlocking parent)
        {
            return new PlanogramBlockingGroupDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramBlockingId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Colour = this.ReadProperty<Int32>(ColourProperty),
                FillPatternType = (Byte)this.ReadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty),
                CanCompromiseSequence = this.ReadProperty<Boolean>(CanCompromiseSequenceProperty),
                IsRestrictedByComponentType = this.ReadProperty<Boolean>(IsRestrictedByComponentTypeProperty),
                CanMerge = this.ReadProperty<Boolean>(CanMergeProperty),
                IsLimited = this.ReadProperty<Boolean>(IsLimitedProperty),
                LimitedPercentage = this.ReadProperty<Single>(LimitedPercentageProperty),
                BlockPlacementPrimaryType = (Byte)this.ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty),
                BlockPlacementSecondaryType = (Byte)this.ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty),
                BlockPlacementTertiaryType = (Byte)this.ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty),
                TotalSpacePercentage = this.ReadProperty<Single>(TotalSpacePercentageProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                MetaCountOfProducts = this.ReadProperty<Int32?>(MetaCountOfProductsProperty),
                MetaCountOfProductRecommendedOnPlanogram = this.ReadProperty<Int32?>(MetaCountOfProductRecommendedOnPlanogramProperty),
                MetaCountOfProductPlacedOnPlanogram = this.ReadProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty),
                MetaOriginalPercentAllocated = this.ReadProperty<Single?>(MetaOriginalPercentAllocatedProperty),
                MetaFinalPercentAllocated = this.ReadProperty<Single?>(MetaFinalPercentAllocatedProperty),
                MetaLinearProductPlacementPercent = this.ReadProperty<Single?>(MetaLinearProductPlacementPercentProperty),
                MetaAreaProductPlacementPercent = this.ReadProperty<Single?>(MetaAreaProductPlacementPercentProperty),
                MetaVolumetricProductPlacementPercent = this.ReadProperty<Single?>(MetaVolumetricProductPlacementPercentProperty),
                MetaPerformancePercentAllocated = this.ReadProperty<Single?>(MetaPerformancePercentAllocatedProperty),
                P1 = this.ReadProperty<Single?>(MetaP1Property),
                P2 = this.ReadProperty<Single?>(MetaP2Property),
                P3 = this.ReadProperty<Single?>(MetaP3Property),
                P4 = this.ReadProperty<Single?>(MetaP4Property),
                P5 = this.ReadProperty<Single?>(MetaP5Property),
                P6 = this.ReadProperty<Single?>(MetaP6Property),
                P7 = this.ReadProperty<Single?>(MetaP7Property),
                P8 = this.ReadProperty<Single?>(MetaP8Property),
                P9 = this.ReadProperty<Single?>(MetaP9Property),
                P10 = this.ReadProperty<Single?>(MetaP10Property),
                P11 = this.ReadProperty<Single?>(MetaP11Property),
                P12 = this.ReadProperty<Single?>(MetaP12Property),
                P13 = this.ReadProperty<Single?>(MetaP13Property),
                P14 = this.ReadProperty<Single?>(MetaP14Property),
                P15 = this.ReadProperty<Single?>(MetaP15Property),
                P16 = this.ReadProperty<Single?>(MetaP16Property),
                P17 = this.ReadProperty<Single?>(MetaP17Property),
                P18 = this.ReadProperty<Single?>(MetaP18Property),
                P19 = this.ReadProperty<Single?>(MetaP19Property),
                P20 = this.ReadProperty<Single?>(MetaP20Property),
                MetaP1Percentage = this.ReadProperty<Single?>(MetaP1PercentageProperty),
                MetaP2Percentage = this.ReadProperty<Single?>(MetaP2PercentageProperty),
                MetaP3Percentage = this.ReadProperty<Single?>(MetaP3PercentageProperty),
                MetaP4Percentage = this.ReadProperty<Single?>(MetaP4PercentageProperty),
                MetaP5Percentage = this.ReadProperty<Single?>(MetaP5PercentageProperty),
                MetaP6Percentage = this.ReadProperty<Single?>(MetaP6PercentageProperty),
                MetaP7Percentage = this.ReadProperty<Single?>(MetaP7PercentageProperty),
                MetaP8Percentage = this.ReadProperty<Single?>(MetaP8PercentageProperty),
                MetaP9Percentage = this.ReadProperty<Single?>(MetaP9PercentageProperty),
                MetaP10Percentage = this.ReadProperty<Single?>(MetaP10PercentageProperty),
                MetaP11Percentage = this.ReadProperty<Single?>(MetaP11PercentageProperty),
                MetaP12Percentage = this.ReadProperty<Single?>(MetaP12PercentageProperty),
                MetaP13Percentage = this.ReadProperty<Single?>(MetaP13PercentageProperty),
                MetaP14Percentage = this.ReadProperty<Single?>(MetaP14PercentageProperty),
                MetaP15Percentage = this.ReadProperty<Single?>(MetaP15PercentageProperty),
                MetaP16Percentage = this.ReadProperty<Single?>(MetaP16PercentageProperty),
                MetaP17Percentage = this.ReadProperty<Single?>(MetaP17PercentageProperty),
                MetaP18Percentage = this.ReadProperty<Single?>(MetaP18PercentageProperty),
                MetaP19Percentage = this.ReadProperty<Single?>(MetaP19PercentageProperty),
                MetaP20Percentage = this.ReadProperty<Single?>(MetaP20PercentageProperty),
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramBlockingGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramBlockingGroupDto>(
            (dc) =>
            {
                PlanogramBlockingGroupDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramBlockingGroup>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramBlockingGroupDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this object
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            batchContext.Delete<PlanogramBlockingGroupDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
