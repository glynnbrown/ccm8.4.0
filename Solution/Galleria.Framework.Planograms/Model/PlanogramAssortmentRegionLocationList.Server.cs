﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentRegionLocationList
    {
        #region Constructor
        private PlanogramAssortmentRegionLocationList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all assemblies for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            var dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramAssortmentRegionLocationDal>())
                {
                    var dtoList = dal.FetchByPlanogramAssortmentRegionId(criteria.ParentId);
                    foreach (var dto in dtoList)
                    {
                        this.Add(PlanogramAssortmentRegionLocation.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
