﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24953 : L.Hodson
//  Created
#endregion

#region Version History: CCM801
// V8-28668 : L.Luong
//  Added HKD.
#endregion

#region Version History: CCM810
//V8-29592 : L.Ineson
//  Added Euro.
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{

    /// <summary>
    /// Denotes the available currency unit of measure types
    /// </summary>
    public enum PlanogramCurrencyUnitOfMeasureType
    {
        Unknown = 0,
        GBP = 1,
        USD = 2,
        HKD = 3,
        EUR = 4
    }

    /// <summary>
    /// PlanogramLengthUnitOfMeasureType Helper Class
    /// </summary>
    public static class PlanogramCurrencyUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramCurrencyUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<PlanogramCurrencyUnitOfMeasureType, String>()
            {
                {PlanogramCurrencyUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramCurrencyUnitOfMeasureType.GBP, Message.Enum_PlanogramCurrencyUnitOfMeasureType_GBP},
                {PlanogramCurrencyUnitOfMeasureType.USD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_USD},
                {PlanogramCurrencyUnitOfMeasureType.HKD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_HKD},
                {PlanogramCurrencyUnitOfMeasureType.EUR, Message.Enum_PlanogramCurrencyUnitOfMeasureType_EUR},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramCurrencyUnitOfMeasureType, String> Abbreviations =
            new Dictionary<PlanogramCurrencyUnitOfMeasureType, String>()
            {
                {PlanogramCurrencyUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramCurrencyUnitOfMeasureType.GBP, Message.Enum_PlanogramCurrencyUnitOfMeasureType_GBP_Abbrev},
                {PlanogramCurrencyUnitOfMeasureType.USD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_USD_Abbrev},
                {PlanogramCurrencyUnitOfMeasureType.HKD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_HKD_Abbrev},
                {PlanogramCurrencyUnitOfMeasureType.EUR, Message.Enum_PlanogramCurrencyUnitOfMeasureType_EUR_Abbrev},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramCurrencyUnitOfMeasureType, String> AvailableValues =
            new Dictionary<PlanogramCurrencyUnitOfMeasureType, String>()
            {
                //{PlanogramCurrencyUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramCurrencyUnitOfMeasureType.GBP, Message.Enum_PlanogramCurrencyUnitOfMeasureType_GBP},
                {PlanogramCurrencyUnitOfMeasureType.USD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_USD},
                {PlanogramCurrencyUnitOfMeasureType.HKD, Message.Enum_PlanogramCurrencyUnitOfMeasureType_HKD},
                {PlanogramCurrencyUnitOfMeasureType.EUR, Message.Enum_PlanogramCurrencyUnitOfMeasureType_EUR},
            };
    }
}
