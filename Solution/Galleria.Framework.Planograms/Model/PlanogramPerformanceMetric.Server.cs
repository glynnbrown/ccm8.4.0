﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Enums;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramPerformanceMetric
    {
        #region Constructor
        private PlanogramPerformanceMetric() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramPerformanceMetric Fetch(IDalContext dalContext, PlanogramPerformanceMetricDto dto)
        {
            return DataPortal.FetchChild<PlanogramPerformanceMetric>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramPerformanceMetricDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty,dto.Name);
            this.LoadProperty<String>(DescriptionProperty,dto.Description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty,(MetricDirectionType)dto.Direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty,(MetricSpecialType)dto.SpecialType);
            this.LoadProperty<MetricType>(MetricTypeProperty,(MetricType)dto.MetricType);
            this.LoadProperty<Byte>(MetricIdProperty,dto.MetricId);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, (AggregationType)dto.AggregationType);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramPerformanceMetricDto GetDataTransferObject(PlanogramPerformance parent)
        {
            return new PlanogramPerformanceMetricDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramPerformanceId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                Direction = (Byte)this.ReadProperty<MetricDirectionType>(DirectionProperty),
                SpecialType = (Byte)this.ReadProperty<MetricSpecialType>(SpecialTypeProperty),
                MetricType = (Byte)this.ReadProperty<MetricType>(MetricTypeProperty),
                MetricId = this.ReadProperty<Byte>(MetricIdProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                AggregationType = (Byte)this.ReadProperty<AggregationType>(AggregationTypeProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramPerformanceMetricDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramPerformanceMetricDto>(
            (dc) =>
            {
                PlanogramPerformanceMetricDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<IPlanogramPerformanceMetricDal>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramPerformanceMetricDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramPerformance parent)
        {
            batchContext.Delete<PlanogramPerformanceMetricDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
