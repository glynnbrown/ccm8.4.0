﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
// V8-27399 : A.Kuszyk
//  Used BlockingHelper to choose the next colour for combined blocks.
// V8-27467 : A.Kuszyk
//  Removed Clean method as it is now obselete.
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
// V8-27741 : A.Kuszyk
//  Added CopyContext to new from source factory method and ensured that new Ids are assigned upon creation.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;
using System.Globalization;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of BlockingGroups contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramBlockingGroupList : ModelList<PlanogramBlockingGroupList, PlanogramBlockingGroup>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramBlocking Parent
        {
            get { return (PlanogramBlocking)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramBlockingGroupList NewPlanogramBlockingGroupList()
        {
            PlanogramBlockingGroupList item = new PlanogramBlockingGroupList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static PlanogramBlockingGroupList NewPlanogramBlockingGroupList(IEnumerable<IPlanogramBlockingGroup> sourceList, IResolveContext context)
        {
            PlanogramBlockingGroupList item = new PlanogramBlockingGroupList();
            item.Create(sourceList, context);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IEnumerable<IPlanogramBlockingGroup> sourceList, IResolveContext context)
        {
            foreach (var item in sourceList)
            {
                this.Add(PlanogramBlockingGroup.NewPlanogramBlockingGroup(item, context));
            }

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        protected override PlanogramBlockingGroup AddNewCore()
        {
            Int32 groupNumber = Items.Count + 1;

            //get the next available group colour
            Int32 colour = (this.Parent != null) ? this.Parent.GetNextBlockingGroupColour() : 0;

            //create the group
            PlanogramBlockingGroup newGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup(
                String.Format(CultureInfo.CurrentCulture, Message.PlanogramBlockingGroup_DefaultName, groupNumber),
                colour);

            //add it.
            this.Add(newGroup);

            return newGroup;
        }

        #endregion
    }
}
