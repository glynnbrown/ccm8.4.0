﻿using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public sealed partial class ProductAttributeComparisonResult
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        public ProductAttributeComparisonResult()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto" />.
        /// </summary>
        internal static ProductAttributeComparisonResult Fetch(IDalContext dalContext, ProductAttributeComparisonResultDto dto)
        {
            return DataPortal.FetchChild<ProductAttributeComparisonResult>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto" /> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="ProductAttributeComparisonResultDto" /> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductAttributeComparisonResultDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ProductGtinProperty, dto.ProductGtin);
            LoadProperty(ComparedProductAttributeProperty, dto.ComparedProductAttribute);
            LoadProperty(MasterDataValueProperty, dto.MasterDataValue);
            LoadProperty(ProductValueProperty, dto.ProductValue);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <returns>A new instance of <see cref="ProductAttributeComparisonResultDto" /> with the data in this instance.</returns>
        private ProductAttributeComparisonResultDto GetDataTransferObject()
        {
            return new ProductAttributeComparisonResultDto
            {
                Id = (Int32)Id,
                PlanogramId = (Int32)Parent.Id,
                ProductGtin = ReadProperty(ProductGtinProperty),
                ComparedProductAttribute = ReadProperty(ComparedProductAttributeProperty),
                MasterDataValue = ReadProperty(MasterDataValueProperty),
                ProductValue = ReadProperty(ProductValueProperty)
            };
        }

        private ProductAttributeComparisonResultDto GetDataTransferObject(Planogram parent)
        {
            return new ProductAttributeComparisonResultDto
            {
                Id = (Int32)Id,
                PlanogramId = (Int32)parent.Id,
                ProductGtin = ReadProperty(ProductGtinProperty),
                ComparedProductAttribute = ReadProperty(ComparedProductAttributeProperty),
                MasterDataValue = ReadProperty(MasterDataValueProperty),
                ProductValue = ReadProperty(ProductValueProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="Planogram" />.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, ProductAttributeComparisonResultDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        ///// <summary>
        /////     Called by reflection when inserting an instance of <see cref="Planogram" />.
        ///// </summary>
        //private void Child_Insert(IDalContext dalContext, Planogram parent)
        //{
        //    ProductAttributeComparisonResultDto dto = GetDataTransferObject();
        //    using (var dal = dalContext.GetDal<IProductAttributeComparisonResultDal>())
        //    {
        //        dal.Insert(dto);
        //    }
        //    FieldManager.UpdateChildren(dalContext, this);
        //}

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="Planogram" />.
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            if (IsSelfDirty)
            {
                batchContext.Insert(dc => GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="Planogram"/>
        /// </summary>
        /// <param name="batchContext"></param>
        /// <param name="parent"></param>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (IsSelfDirty)
            {
                batchContext.Update(dc => GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        #region Update
        /// <summary>
        ///     Called by reflection when updating an instance of<see cref= "Planogram" />.
        /// </ summary >
        private void Child_Update(IDalContext dalContext, Planogram parent)
        {
            if (IsSelfDirty)
            {
                ProductAttributeComparisonResultDto dto = GetDataTransferObject();
                using (var dal = dalContext.GetDal<IProductAttributeComparisonResultDal>())
                {
                    dal.Update(dto);
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete(GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}

