﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26812 : A.Silva
//      Created.
// V8-27456 : A.Silva
//      Added EqualTo value and friendly captions.

#endregion
#region Version History : CCM820
// V8-30844 : A.Kuszyk
//  Added LessThanOrEqualTo and GreaterThanOrEqualTo option.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Specifies the type of validation calculations that will be used by the CCM engine.
    /// </summary>
    public enum PlanogramValidationType
    {
        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>greater than</c> the Threshold1 value.
        /// </summary>
        GreaterThan = 0,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>greater than or equal to</c> the Threshold1 value.
        /// </summary>
        GreaterThanOrEqualTo = 9,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>less than</c> the Threshold1 value.
        /// </summary>
        LessThan = 1,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>less than or equal to</c> the Threshold1 value.
        /// </summary>
        LessThanOrEqualTo = 8,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>equal to</c> the Threshold1 value.
        /// </summary>
        EqualTo = 2,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>contains</c> the Threshold1 value.
        /// </summary>
        Contains = 3,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>does not contain</c> the Threshold1 value.
        /// </summary>
        DoesNotContain = 4,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>true</c> the Threshold1 value.
        /// </summary>
        True = 5,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>false</c> the Threshold1 value.
        /// </summary>
        False = 6,

        /// <summary>
        ///     The validation calculation will succeed if the metric value is <c>not equal to</c> the Threshold1 value.
        /// </summary>
        NotEqualTo = 7,
    }

    /// <summary>
    ///     Provides helper methods to work with the <see cref="PlanogramValidationType" /> enumeration.
    /// </summary>
    public static class PlanogramValidationTypeHelper
    {
        #region FiendlyGreenResultCaptions dictionary

        /// <summary>
        ///     Contains the mapping of type values and friendly green result captions for <see cref="PlanogramValidationType"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationType, String> FriendlyGreenResultCaptions =
            new Dictionary<PlanogramValidationType, String>
            {
                {PlanogramValidationType.GreaterThan, Message.Enum_PlanogramValidationType_GreenGreaterThan},
                {PlanogramValidationType.LessThan, Message.Enum_PlanogramValidationType_GreenLessThan},
                {PlanogramValidationType.EqualTo, Message.Enum_PlanogramValidationType_GreenEqualTo},
                {PlanogramValidationType.LessThanOrEqualTo, Message.Enum_PlanogramValidationType_GreenLessThanOrEqualTo}, 
                {PlanogramValidationType.GreaterThanOrEqualTo, Message.Enum_PlanogramValidationType_GreenGreaterThanOrEqualTo}, 
            };

        #endregion

        #region FriendlyYellowResultCaptions dictionary

        /// <summary>
        ///     Contains the mapping of type values and friendly yellow result captions for <see cref="PlanogramValidationType"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationType, String> FriendlyYellowResultCaptions =
            new Dictionary<PlanogramValidationType, String>
            {
                {PlanogramValidationType.GreaterThan, Message.Enum_PlanogramValidationType_YellowGreaterThan},
                {PlanogramValidationType.LessThan, Message.Enum_PlanogramValidationType_YellowLessThan},
                {PlanogramValidationType.LessThanOrEqualTo, Message.Enum_PlanogramValidationType_YellowLessThanOrEqualTo}, 
                {PlanogramValidationType.GreaterThanOrEqualTo, Message.Enum_PlanogramValidationType_YellowGreaterThanOrEqualTo}, 
                {PlanogramValidationType.EqualTo, Message.Enum_PlanogramValidationType_YellowEqualTo},
            };

        #endregion

        #region FriendlyRedResultCaptions dictionary

        /// <summary>
        ///     Contains the mapping of type values and friendly red result captions for <see cref="PlanogramValidationType"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationType, String> FriendlyRedResultCaptions =
            new Dictionary<PlanogramValidationType, String>
            {
                {PlanogramValidationType.GreaterThan, Message.Enum_PlanogramValidationType_RedGreaterThan},
                {PlanogramValidationType.GreaterThanOrEqualTo, Message.Enum_PlanogramValidationType_RedGreaterThan},
                {PlanogramValidationType.LessThan, Message.Enum_PlanogramValidationType_RedLessThan},
                {PlanogramValidationType.LessThanOrEqualTo, Message.Enum_PlanogramValidationType_RedLessThan},
                {PlanogramValidationType.EqualTo, Message.Enum_PlanogramValidationType_RedEqualTo}
            };

        #endregion

        #region FiendlyGreenResultStringCaptions dictionary

        /// <summary>
        ///     Contains the mapping of type values and friendly green result captions for String <see cref="PlanogramValidationType"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationType, String> FriendlyGreenResultStringCaptions =
            new Dictionary<PlanogramValidationType, String>
            {
                {PlanogramValidationType.Contains, Message.Enum_PlanogramValidationType_GreenContains},
                {PlanogramValidationType.DoesNotContain, Message.Enum_PlanogramValidationType_GreenDoesNotContain},
                {PlanogramValidationType.EqualTo, Message.Enum_PlanogramValidationType_GreenEqualToCriteria},
                {PlanogramValidationType.NotEqualTo, Message.Enum_PlanogramValidationType_GreenDoesNotEqualToCriteria}
            };

        #endregion

        #region FiendlyGreenResultBooleanCaptions dictionary

        /// <summary>
        ///     Contains the mapping of type values and friendly green result captions for Boolean <see cref="PlanogramValidationType"/>.
        /// </summary>
        public static readonly Dictionary<PlanogramValidationType, String> FriendlyGreenResultBooleanCaptions =
            new Dictionary<PlanogramValidationType, String>
            {
                {PlanogramValidationType.True, Message.Enum_PlanogramValidationType_GreenTrue},
                {PlanogramValidationType.False, Message.Enum_PlanogramValidationType_GreenFalse}
            };

        #endregion
    }
}