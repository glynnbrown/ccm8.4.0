﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26704 : A.Kuszyk
//  Added AddRange for IPlanogramAssortmentLocalProduct.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentLocalProductList : ModelList<PlanogramAssortmentLocalProductList, PlanogramAssortmentLocalProduct>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentLocalProductList NewPlanogramAssortmentLocalProductList()
        {
            var item = new PlanogramAssortmentLocalProductList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a range of IPlanogramAssortmentLocalProduct items.
        /// </summary>
        public void AddRange(IEnumerable<IPlanogramAssortmentLocalProduct> range)
        {
            base.AddRange(range.Select(o => PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct(o)));
        }
        #endregion
    }
}
