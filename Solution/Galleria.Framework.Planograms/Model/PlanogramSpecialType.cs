﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramSpecialType
    {
        None,
        RegularSalesUnits,
        ReplenishmentDays
    }
}
