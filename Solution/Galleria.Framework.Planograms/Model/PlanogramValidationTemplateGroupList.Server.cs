﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Linq;


namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramValidationTemplateGroupList
    {
        #region Constructor
        private PlanogramValidationTemplateGroupList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all assemblies for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            var dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramValidationTemplateGroupDal>())
                {
                    var dtoList = dal.FetchByPlanogramValidationTemplateId(criteria.ParentId);
                    foreach (var dto in dtoList.OrderBy(g=> g.Id))
                    {
                        this.Add(PlanogramValidationTemplateGroup.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
