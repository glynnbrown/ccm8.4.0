﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Updated so NumberOfSubComponents, NumberOfMerchandisableSubComponent, NumberOfShelfEdgeLabels are meta data
// V8-27058 : A.Probyn
//  Removed MetaNumberOfShelfEdgeLabels
// V8-27468 : L.Ineson
// Added IsMerchandisedTopDown
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: (CCM 8.2)
// V8-30873 : L.Ineson
//  Removed IsCarPark.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramComponent
    {
        #region Constructor
        private PlanogramComponent() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramComponent Fetch(IDalContext dalContext, PlanogramComponentDto dto)
        {
            return DataPortal.FetchChild<PlanogramComponent>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComponentDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(Mesh3DIdProperty, dto.Mesh3DId);
            this.LoadProperty<Object>(ImageIdFrontProperty, dto.ImageIdFront);
            this.LoadProperty<Object>(ImageIdBackProperty, dto.ImageIdBack);
            this.LoadProperty<Object>(ImageIdTopProperty, dto.ImageIdTop);
            this.LoadProperty<Object>(ImageIdBottomProperty, dto.ImageIdBottom);
            this.LoadProperty<Object>(ImageIdLeftProperty, dto.ImageIdLeft);
            this.LoadProperty<Object>(ImageIdRightProperty, dto.ImageIdRight);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int16?>(MetaNumberOfSubComponentsProperty, dto.MetaNumberOfSubComponents);
            this.LoadProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty, dto.MetaNumberOfMerchandisedSubComponents);
            this.LoadProperty<Boolean>(IsMoveableProperty, dto.IsMoveable);
            this.LoadProperty<Boolean>(IsDisplayOnlyProperty, dto.IsDisplayOnly);
            this.LoadProperty<Boolean>(CanAttachShelfEdgeLabelProperty, dto.CanAttachShelfEdgeLabel);
            this.LoadProperty<String>(RetailerReferenceCodeProperty, dto.RetailerReferenceCode);
            this.LoadProperty<String>(BarCodeProperty, dto.BarCode);
            this.LoadProperty<String>(ManufacturerProperty, dto.Manufacturer);
            this.LoadProperty<String>(ManufacturerPartNameProperty, dto.ManufacturerPartName);
            this.LoadProperty<String>(ManufacturerPartNumberProperty, dto.ManufacturerPartNumber);
            this.LoadProperty<String>(SupplierNameProperty, dto.SupplierName);
            this.LoadProperty<String>(SupplierPartNumberProperty, dto.SupplierPartNumber);
            this.LoadProperty<Single?>(SupplierCostPriceProperty, dto.SupplierCostPrice);
            this.LoadProperty<Single?>(SupplierDiscountProperty, dto.SupplierDiscount);
            this.LoadProperty<Single?>(SupplierLeadTimeProperty, dto.SupplierLeadTime);
            this.LoadProperty<Int32?>(MinPurchaseQtyProperty, dto.MinPurchaseQty);
            this.LoadProperty<Single?>(WeightLimitProperty, dto.WeightLimit);
            this.LoadProperty<Single?>(WeightProperty, dto.Weight);
            this.LoadProperty<Single?>(VolumeProperty, dto.Volume);
            this.LoadProperty<Single?>(DiameterProperty, dto.Diameter);
            this.LoadProperty<Int16?>(CapacityProperty, dto.Capacity);
            this.LoadProperty<PlanogramComponentType>(ComponentTypeProperty, (PlanogramComponentType)dto.ComponentType);
            this.LoadProperty<Boolean>(IsMerchandisedTopDownProperty, dto.IsMerchandisedTopDown);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramComponentDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramComponentDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                Mesh3DId = this.ReadProperty<Object>(Mesh3DIdProperty),
                ImageIdFront = this.ReadProperty<Object>(ImageIdFrontProperty),
                ImageIdBack = this.ReadProperty<Object>(ImageIdBackProperty),
                ImageIdTop = this.ReadProperty<Object>(ImageIdTopProperty),
                ImageIdBottom = this.ReadProperty<Object>(ImageIdBottomProperty),
                ImageIdLeft = this.ReadProperty<Object>(ImageIdLeftProperty),
                ImageIdRight = this.ReadProperty<Object>(ImageIdRightProperty),
                Name = this.ReadProperty<String>(NameProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                MetaNumberOfSubComponents = this.ReadProperty<Int16?>(MetaNumberOfSubComponentsProperty),
                MetaNumberOfMerchandisedSubComponents = this.ReadProperty<Int16?>(MetaNumberOfMerchandisedSubComponentsProperty),
                IsMoveable = this.ReadProperty<Boolean>(IsMoveableProperty),
                IsDisplayOnly = this.ReadProperty<Boolean>(IsDisplayOnlyProperty),
                CanAttachShelfEdgeLabel = this.ReadProperty<Boolean>(CanAttachShelfEdgeLabelProperty),
                RetailerReferenceCode = this.ReadProperty<String>(RetailerReferenceCodeProperty),
                BarCode = this.ReadProperty<String>(BarCodeProperty),
                Manufacturer = this.ReadProperty<String>(ManufacturerProperty),
                ManufacturerPartName = this.ReadProperty<String>(ManufacturerPartNameProperty),
                ManufacturerPartNumber = this.ReadProperty<String>(ManufacturerPartNumberProperty),
                SupplierName = this.ReadProperty<String>(SupplierNameProperty),
                SupplierPartNumber = this.ReadProperty<String>(SupplierPartNumberProperty),
                SupplierCostPrice = this.ReadProperty<Single?>(SupplierCostPriceProperty),
                SupplierDiscount = this.ReadProperty<Single?>(SupplierDiscountProperty),
                SupplierLeadTime = this.ReadProperty<Single?>(SupplierLeadTimeProperty),
                MinPurchaseQty = this.ReadProperty<Int32?>(MinPurchaseQtyProperty),
                WeightLimit = this.ReadProperty<Single?>(WeightLimitProperty),
                Weight = this.ReadProperty<Single?>(WeightProperty),
                Volume = this.ReadProperty<Single?>(VolumeProperty),
                Diameter = this.ReadProperty<Single?>(DiameterProperty),
                Capacity = this.ReadProperty<Int16?>(CapacityProperty),
                ComponentType = (Byte)this.ReadProperty<PlanogramComponentType>(ComponentTypeProperty),
                IsMerchandisedTopDown = this.ReadProperty<Boolean>(IsMerchandisedTopDownProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramComponentDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramComponentDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramComponent>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramComponentDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramComponentDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}