﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;


namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeLevelList
    {
        #region Constructor
        private PlanogramConsumerDecisionTreeLevelList() { } // force use of factory methods
        #endregion
    }
}
