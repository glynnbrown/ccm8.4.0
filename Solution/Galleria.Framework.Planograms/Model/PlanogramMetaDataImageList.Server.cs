﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27058 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramMetadataImageList
    {
        #region Constructor
        private PlanogramMetadataImageList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all annotations for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramMetadataImageDal dal = dalContext.GetDal<IPlanogramMetadataImageDal>())
                {
                    IEnumerable<PlanogramMetadataImageDto> dtoList = dal.FetchByPlanogramId(criteria.ParentId);
                    foreach (PlanogramMetadataImageDto dto in dtoList)
                    {
                        this.Add(PlanogramMetadataImage.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
