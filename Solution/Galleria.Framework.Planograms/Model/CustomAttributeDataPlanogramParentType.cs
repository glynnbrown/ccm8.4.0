﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26147 : L.Ineson
//	Created
// V8-26442 : I.George
// Added Planogram Component
#endregion
#endregion


namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Defines the different CustomAttributeData parent types
    /// for the planogram structure.
    /// </summary>
    public enum CustomAttributeDataPlanogramParentType : byte
    {
        Planogram = 0,
        PlanogramProduct =1,
        PlanogramComponent = 2,
    }
}
