﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentProductBuddyList
    {
        #region Constructor
        private PlanogramAssortmentProductBuddyList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all assemblies for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            var dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramAssortmentProductBuddyDal>())
                {
                    var dtoList = dal.FetchByPlanogramAssortmentId(criteria.ParentId);
                    foreach (var dto in dtoList)
                    {
                        this.Add(PlanogramAssortmentProductBuddy.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
