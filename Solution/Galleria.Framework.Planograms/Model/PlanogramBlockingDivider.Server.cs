﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created 
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-32985 : D.Pleasance
//  Added PlanogramBlockingDividerIsLimited \ PlanogramBlockingDividerLimitedPercentage
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingDivider
    {
        #region Constructor
        private PlanogramBlockingDivider() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramBlockingDivider Fetch(IDalContext dalContext, PlanogramBlockingDividerDto dto)
        {
            return DataPortal.FetchChild<PlanogramBlockingDivider>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramBlockingDividerDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramBlockingDividerType>(TypeProperty, (PlanogramBlockingDividerType)dto.Type);
            this.LoadProperty<Byte>(LevelProperty, dto.Level);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(LengthProperty, dto.Length);
            this.LoadProperty<Boolean>(IsSnappedProperty, dto.IsSnapped);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Boolean>(IsLimitedProperty, dto.IsLimited);
            this.LoadProperty<Single>(LimitedPercentageProperty, dto.LimitedPercentage);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramBlockingDividerDto GetDataTransferObject(PlanogramBlocking parent)
        {
            return new PlanogramBlockingDividerDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramBlockingId = parent.Id,
                Type = (Byte)this.ReadProperty<PlanogramBlockingDividerType>(TypeProperty),
                Level = this.ReadProperty<Byte>(LevelProperty),
                X = this.ReadProperty<Single>(XProperty),
                Y = this.ReadProperty<Single>(YProperty),
                Length = this.ReadProperty<Single>(LengthProperty),
                IsSnapped = this.ReadProperty<Boolean>(IsSnappedProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                IsLimited = this.ReadProperty<Boolean>(IsLimitedProperty),
                LimitedPercentage = this.ReadProperty<Single>(LimitedPercentageProperty)
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramBlockingDividerDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramBlockingDividerDto>(
            (dc) =>
            {
                PlanogramBlockingDividerDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramBlockingDivider>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramBlockingDividerDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this object
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramBlocking parent)
        {
            batchContext.Delete<PlanogramBlockingDividerDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
