﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A simple class used to hold all the
    /// available peg holes contained within
    /// a planogram sub component
    /// </summary>
    public class PlanogramPegHoles
    {
        #region Fields
        private IEnumerable<PointValue> _row1;
        private IEnumerable<PointValue> _row2;
        private PlanogramSubComponent _parent = null;
        private Single _height = 0, _width = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Returns the row 1 peg holes
        /// </summary>
        public IEnumerable<PointValue> Row1
        {
            get { return _row1; }
        }

        /// <summary>
        /// Returns the row 2 peg holes
        /// </summary>
        public IEnumerable<PointValue> Row2
        {
            get { return _row2; }
        }

        /// <summary>
        /// Returns all peg holes
        /// </summary>
        public IEnumerable<PointValue> All
        {
            get { return _row1.Union(_row2); }
        }

        public Boolean PegSlotsRow1
        {
            get
            {
                if (_parent != null)
                {
                    return _parent.MerchConstraintRow1SpacingX == 0;
                }

                return false;
            }
        }
        
        public Single PegWidthRow1
        {
            get
            {
                if (_parent != null)
                {
                    if (PegSlotsRow1)
                    {
                        return _parent.Width - _parent.MerchConstraintRow1StartX;
                    }
                    else
                    {
                        return _parent.MerchConstraintRow1Width;
                    }
                }

                return _width;
            }
        }

        public Single PegHeightRow1
        {
            get
            {
                if (_parent != null)
                {
                    return _parent.MerchConstraintRow1Height;
                }
                return _height;
            }
        }

        public Boolean PegSlotsRow2
        {
            get
            {
                if (_parent != null)
                {
                    return _parent.MerchConstraintRow2SpacingX == 0;
                }
                return false;
            }
        }

        public Single PegWidthRow2
        {
            get
            {
                if (_parent != null)
                {
                    if (PegSlotsRow2)
                    {
                        return _parent.Width - _parent.MerchConstraintRow2StartX;
                    }
                    else
                    {
                        return _parent.MerchConstraintRow2Width;
                    }
                }
                return _width;
            }
        }

        public Single PegHeightRow2
        {
            get
            {
                if (_parent != null)
                {
                    return _parent.MerchConstraintRow2Height;
                }
                return _height;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramPegHoles(IEnumerable<PointValue> row1, IEnumerable<PointValue> row2, PlanogramSubComponent parent)
        {
            _parent = parent;
            _row1 = row1;
            _row2 = row2;
        }

        public PlanogramPegHoles(IEnumerable<PointValue> row1, IEnumerable<PointValue> row2)
        {
            _row1 = row1;
            _row2 = row2;
        }
        #endregion

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramPegHoles;
            if (other == null)
                return false;

            return Equals(other);
        }

        protected bool Equals(PlanogramPegHoles other)
        {
            return Equals(_row1, other._row1) && Equals(_row2, other._row2) && Equals(_parent, other._parent) && _height.Equals(other._height) && _width.Equals(other._width);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (_row1 != null ? _row1.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_row2 != null ? _row2.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (_parent != null ? _parent.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ _height.GetHashCode();
                hashCode = (hashCode*397) ^ _width.GetHashCode();
                return hashCode;
            }
        }
    }
}
