﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26799 : A.Kuszyk
//  Removed LocationCode property.
// V8-27058 : A.Probyn
//  Updated Gtin to be GTIN
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-30705 : A.Probyn
//  Extended to support DelistedDueToAssortmentRule
// V8-30762 : I.George
//  Added MetaData field MetaIsProductPlacedOnPlanogra
#endregion
#region Version History: CCM 830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentProduct
    {
        #region Constructor
        private PlanogramAssortmentProduct() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortmentProduct Fetch(IDalContext dalContext, PlanogramAssortmentProductDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortmentProduct>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentProductDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<String>(GtinProperty, dto.Gtin);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Boolean>(IsRangedProperty, dto.IsRanged);
            this.LoadProperty<Int16>(RankProperty, dto.Rank);
            this.LoadProperty<String>(SegmentationProperty, dto.Segmentation);
            this.LoadProperty<Byte>(FacingsProperty, dto.Facings);
            this.LoadProperty<Int16>(UnitsProperty, dto.Units);
            this.LoadProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty, (PlanogramAssortmentProductTreatmentType)dto.ProductTreatmentType);
            this.LoadProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty, (PlanogramAssortmentProductLocalizationType)dto.ProductLocalizationType);
            this.LoadProperty<String>(CommentsProperty, dto.Comments);
            this.LoadProperty<Byte?>(ExactListFacingsProperty, dto.ExactListFacings);
            this.LoadProperty<Int16?>(ExactListUnitsProperty, dto.ExactListUnits);
            this.LoadProperty<Byte?>(PreserveListFacingsProperty, dto.PreserveListFacings);
            this.LoadProperty<Int16?>(PreserveListUnitsProperty, dto.PreserveListUnits);
            this.LoadProperty<Byte?>(MaxListFacingsProperty, dto.MaxListFacings);
            this.LoadProperty<Int16?>(MaxListUnitsProperty, dto.MaxListUnits);
            this.LoadProperty<Byte?>(MinListFacingsProperty, dto.MinListFacings);
            this.LoadProperty<Int16?>(MinListUnitsProperty, dto.MinListUnits);
            this.LoadProperty<String>(FamilyRuleNameProperty, dto.FamilyRuleName);
            this.LoadProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty, (PlanogramAssortmentProductFamilyRuleType)dto.FamilyRuleType);
            this.LoadProperty<Byte?>(FamilyRuleValueProperty, dto.FamilyRuleValue);
            this.LoadProperty<Boolean>(DelistedDueToAssortmentRuleProperty, dto.DelistedDueToAssortmentRule);
            this.LoadProperty<Boolean>(IsPrimaryRegionalProductProperty, dto.IsPrimaryRegionalProduct);
            this.LoadProperty<Boolean?>(MetaIsProductPlacedOnPlanogramProperty, dto.MetaIsProductPlacedOnPlanogram);
            
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramAssortmentProductDto GetDataTransferObject(PlanogramAssortment parent)
        {
            return new PlanogramAssortmentProductDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramAssortmentId = parent.Id,
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                Gtin = this.ReadProperty<String>(GtinProperty),
                Name = this.ReadProperty<String>(NameProperty),
                IsRanged = this.ReadProperty<Boolean>(IsRangedProperty),
                Rank = this.ReadProperty<Int16>(RankProperty),
                Segmentation = this.ReadProperty<String>(SegmentationProperty),
                Facings = this.ReadProperty<Byte>(FacingsProperty),
                Units = this.ReadProperty<Int16>(UnitsProperty),
                ProductTreatmentType = (Byte)this.ReadProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty),
                ProductLocalizationType = (Byte)this.ReadProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty),
                Comments = this.ReadProperty<String>(CommentsProperty),
                ExactListFacings = this.ReadProperty<Byte?>(ExactListFacingsProperty),
                ExactListUnits = this.ReadProperty<Int16?>(ExactListUnitsProperty),
                PreserveListFacings = this.ReadProperty<Byte?>(PreserveListFacingsProperty),
                PreserveListUnits = this.ReadProperty<Int16?>(PreserveListUnitsProperty),
                MaxListFacings = this.ReadProperty<Byte?>(MaxListFacingsProperty),
                MaxListUnits = this.ReadProperty<Int16?>(MaxListUnitsProperty),
                MinListFacings = this.ReadProperty<Byte?>(MinListFacingsProperty),
                MinListUnits = this.ReadProperty<Int16?>(MinListUnitsProperty),
                FamilyRuleName = this.ReadProperty<String>(FamilyRuleNameProperty),
                FamilyRuleType = (Byte)this.ReadProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty),
                FamilyRuleValue = this.ReadProperty<Byte?>(FamilyRuleValueProperty),
                DelistedDueToAssortmentRule = this.ReadProperty<Boolean>(DelistedDueToAssortmentRuleProperty),
                IsPrimaryRegionalProduct = this.ReadProperty<Boolean>(IsPrimaryRegionalProductProperty),
                MetaIsProductPlacedOnPlanogram = this.ReadProperty<Boolean?>(MetaIsProductPlacedOnPlanogramProperty),
                
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentProductDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentProductDto>(
            (dc) =>
            {
                PlanogramAssortmentProductDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortmentProduct>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentProductDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            batchContext.Delete<PlanogramAssortmentProductDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
