﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26271 : A.Kuszyk
////  Created.
//// V8-26338 : A.Kuszyk
////  Added Helper class with friendly names.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.Planograms.Resources.Language;

//namespace Galleria.Framework.Planograms.Model
//{
//    /// <summary>
//    /// Indicates the value type of a Planogram Metric
//    /// </summary>
//    public enum MetricType
//    {
//        Integer,
//        Currency,
//        Decimal
//    }

//    /// <summary>
//    /// PlanogramMetricType Helper Class
//    /// </summary>
//    public static class PlanogramMetricTypeHelper
//    {
//        /// <summary>
//        /// Dictionary with enum value as key and friendly name as value.
//        /// </summary>
//        public static readonly Dictionary<MetricType, String> FriendlyNames =
//            new Dictionary<MetricType, String>()
//            {
//                {MetricType.Integer, Message.Enum_PlanogramMetricType_Integer},
//                {MetricType.Currency, Message.Enum_PlanogramMetricType_Currency},
//                {MetricType.Decimal, Message.Enum_PlanogramMetricType_Decimal}
//            };
//    }
//}
