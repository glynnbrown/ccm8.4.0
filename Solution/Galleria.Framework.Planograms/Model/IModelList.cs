﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-27062 : A.Silva ~ Added method CalculateValidationData.
// V8-27058 : A.Probyn ~ Added method ClearMetadata.
// V8-27237 : A.Silva   ~ Added method ClearValidationData.

#endregion

#region Version History: CCM810
// V8-30519 : L.Ineson
//  Added FindChildModelById
#endregion

#endregion

using System;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Defines a model list within this assembly
    /// </summary>
    internal interface IModelList
    {
        #region Properties

        /// <summary>
        ///     Returns the dal factory name
        /// </summary>
        String DalFactoryName { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates metadata for this instance
        /// </summary>
        void CalculateMetadata();

        /// <summary>
        /// Clears meta data for this instance.
        /// </summary>
        void ClearMetadata();

        /// <summary>
        ///     Calculates the validation data for this instance.
        /// </summary>
        void CalculateValidationData();

        /// <summary>
        ///     Clears all validation data for this instance.
        /// </summary>
        void ClearValidationData();

        /// <summary>
        /// Recurses through the entire model tree looking for the object
        /// with the given type and id.
        /// </summary>
        Object FindChildModelById(Type childModelType, Object id);

        #endregion
    }
}