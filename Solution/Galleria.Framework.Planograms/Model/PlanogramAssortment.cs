﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortment.
#endregion
#region Version History: CCM820
// V8-30762 : I.George
//  Added Calculate MetaData method
#endregion
#region Version History: CCM830

// V8-32787 : A.Silva
//  Added GetAssortmentRuleType.

#endregion
#region Version History: CCM832

// CCM-18959 : G.Richards
// Assortment products are now excluded if they are regional and the region they belong to are not associated to the planograms location.
// Assortment products are now excluded if they are local products and are not associated to the planograms location.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortment : ModelObject<PlanogramAssortment>, IPlanogramAssortment
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Products
        /// <summary>
        /// Products property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentProductList> ProductsProperty =
            RegisterModelProperty<PlanogramAssortmentProductList>(c => c.Products,RelationshipTypes.LazyLoad);

        /// <summary>
        /// Products list
        /// </summary>
        public PlanogramAssortmentProductList Products
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentProductList>(
                    ProductsProperty,
                    new PlanogramAssortmentProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Products list
        /// </summary>
        public PlanogramAssortmentProductList ProductsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentProductList>(
                    ProductsProperty,
                    new PlanogramAssortmentProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        /// <summary>
        /// IPlanogramAssortment Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentProduct> IPlanogramAssortment.Products
        {
            get { return this.Products.Select(i => (IPlanogramAssortmentProduct)i); }
        }

        #endregion

        #region LocalProducts
        /// <summary>
        /// LocalProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentLocalProductList> LocalProductsProperty =
            RegisterModelProperty<PlanogramAssortmentLocalProductList>(c => c.LocalProducts, RelationshipTypes.LazyLoad);

        /// <summary>
        /// LocalProducts list
        /// </summary>
        public PlanogramAssortmentLocalProductList LocalProducts
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentLocalProductList>(
                    LocalProductsProperty,
                    new PlanogramAssortmentLocalProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// LocalProducts list
        /// </summary>
        public PlanogramAssortmentLocalProductList LocalProductsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentLocalProductList>(
                    LocalProductsProperty,
                    new PlanogramAssortmentLocalProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        /// <summary>
        /// IPlanogramAssortment Local Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentLocalProduct> IPlanogramAssortment.LocalProducts
        {
            get { return LocalProducts.Select(o => (IPlanogramAssortmentLocalProduct)o); }
        }
        #endregion

        #region Regions
        /// <summary>
        /// Regions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentRegionList> RegionsProperty =
            RegisterModelProperty<PlanogramAssortmentRegionList>(c => c.Regions, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Regions list
        /// </summary>
        public PlanogramAssortmentRegionList Regions
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentRegionList>(
                    RegionsProperty,
                    new PlanogramAssortmentRegionList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Regions list
        /// </summary>
        public PlanogramAssortmentRegionList RegionsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentRegionList>(
                    RegionsProperty,
                    new PlanogramAssortmentRegionList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        /// <summary>
        /// IPlanogramAssortment Regions member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegion> IPlanogramAssortment.Regions
        {
            get { return Regions.Select(o => (IPlanogramAssortmentRegion)o); }
        }
        #endregion

        #region InventoryRules
        /// <summary>
        /// InventoryRules property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentInventoryRuleList> InventoryRulesProperty =
            RegisterModelProperty<PlanogramAssortmentInventoryRuleList>(c => c.InventoryRules, RelationshipTypes.LazyLoad);

        /// <summary>
        /// InventoryRules list
        /// </summary>
        public PlanogramAssortmentInventoryRuleList InventoryRules
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentInventoryRuleList>(
                    InventoryRulesProperty,
                    new PlanogramAssortmentInventoryRuleList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// InventoryRules list
        /// </summary>
        public PlanogramAssortmentInventoryRuleList InventoryRulesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentInventoryRuleList>(
                    InventoryRulesProperty,
                    new PlanogramAssortmentInventoryRuleList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        
        /// <summary>
        /// IPlanogramAssortment InventoryRules member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentInventoryRule> IPlanogramAssortment.InventoryRules
        {
            get { return InventoryRules.Select(o => (IPlanogramAssortmentInventoryRule)o); }
        }

        #endregion

        #region ProductBuddies
        /// <summary>
        /// ProductBuddies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentProductBuddyList> ProductBuddiesProperty =
            RegisterModelProperty<PlanogramAssortmentProductBuddyList>(c => c.ProductBuddies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// ProductBuddies list
        /// </summary>
        public PlanogramAssortmentProductBuddyList ProductBuddies
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentProductBuddyList>(
                    ProductBuddiesProperty,
                    new PlanogramAssortmentProductBuddyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// ProductBuddies list
        /// </summary>
        public PlanogramAssortmentProductBuddyList ProductBuddiesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentProductBuddyList>(
                    ProductBuddiesProperty,
                    new PlanogramAssortmentProductBuddyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        /// <summary>
        /// IPlanogramAssortment ProductBuddies member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentProductBuddy> IPlanogramAssortment.ProductBuddies
        {
            get { return ProductBuddies.Select(o => (IPlanogramAssortmentProductBuddy)o); }
        }
        
        #endregion

        #region LocationBuddies
        /// <summary>
        /// LocationBuddies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortmentLocationBuddyList> LocationBuddiesProperty =
            RegisterModelProperty<PlanogramAssortmentLocationBuddyList>(c => c.LocationBuddies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// LocationBuddies list
        /// </summary>
        public PlanogramAssortmentLocationBuddyList LocationBuddies
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentLocationBuddyList>(
                    LocationBuddiesProperty,
                    new PlanogramAssortmentLocationBuddyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// LocationBuddies list
        /// </summary>
        public PlanogramAssortmentLocationBuddyList LocationBuddiesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortmentLocationBuddyList>(
                    LocationBuddiesProperty,
                    new PlanogramAssortmentLocationBuddyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        /// <summary>
        /// IPlanogramAssortment LocationBuddies member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentLocationBuddy> IPlanogramAssortment.LocationBuddies
        {
            get { return LocationBuddies.Select(o => (IPlanogramAssortmentLocationBuddy)o); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 255));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId factory method
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(c => c.PlanogramId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object PlanogramId
            {
                get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(PlanogramIdProperty, planogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortment NewPlanogramAssortment()
        {
            var item = new PlanogramAssortment();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.PlanogramAssortment_Name_Default);
            this.LoadProperty<PlanogramAssortmentProductList>(ProductsProperty, PlanogramAssortmentProductList.NewPlanogramAssortmentProductList());
            this.LoadProperty<PlanogramAssortmentLocalProductList>(LocalProductsProperty, PlanogramAssortmentLocalProductList.NewPlanogramAssortmentLocalProductList());
            this.LoadProperty<PlanogramAssortmentRegionList>(RegionsProperty, PlanogramAssortmentRegionList.NewPlanogramAssortmentRegionList());
            this.LoadProperty<PlanogramAssortmentInventoryRuleList>(InventoryRulesProperty, PlanogramAssortmentInventoryRuleList.NewPlanogramAssortmentInventoryRuleList());
            this.LoadProperty<PlanogramAssortmentProductBuddyList>(ProductBuddiesProperty, PlanogramAssortmentProductBuddyList.NewPlanogramAssortmentProductBuddyList());
            this.LoadProperty<PlanogramAssortmentLocationBuddyList>(LocationBuddiesProperty, PlanogramAssortmentLocationBuddyList.NewPlanogramAssortmentLocationBuddyList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Replaces the existing Assortment's content with the content of the given Assortment.
        /// </summary>
        /// <param name="assortment">The Assortment to replace with.</param>
        public void ReplaceAssortment(IPlanogramAssortment assortment)
        {
            Products.Clear();
            LocalProducts.Clear();
            Regions.Clear();
            InventoryRules.Clear();
            ProductBuddies.Clear();
            LocationBuddies.Clear();

            Name = assortment.Name;
            AddAssortment(assortment);
        }

        /// <summary>
        /// clears the assortment
        /// </summary>
        public void ClearAll()
        {
            Products.Clear();
            LocalProducts.Clear();
            Regions.Clear();
            InventoryRules.Clear();
            ProductBuddies.Clear();
            LocationBuddies.Clear();

            Name = Message.PlanogramAssortment_Name_Default;
        }

        /// <summary>
        /// Adds the given Assortment to the existing Assortment's content.
        /// </summary>
        /// <param name="assortment">The Assortment to add.</param>
        public void AddAssortment(IPlanogramAssortment assortment)
        {
            //Populate assortment data
            Products.AddRange(assortment.Products);
            LocalProducts.AddRange(assortment.LocalProducts);
            Regions.AddRange(assortment.Regions);
            InventoryRules.AddRange(assortment.InventoryRules);
            ProductBuddies.AddRange(assortment.ProductBuddies);
            LocationBuddies.AddRange(assortment.LocationBuddies);

            //If we have a location code perform validation for regional and local products.
            if (!string.IsNullOrEmpty(this.Parent.LocationCode))
            {
                foreach (PlanogramAssortmentProduct product in Products.ToList())
                {
                    bool removeProduct = false;

                    switch (product.ProductLocalizationType)
                    {
                        case PlanogramAssortmentProductLocalizationType.None:
                            break;
                        case PlanogramAssortmentProductLocalizationType.Regional:
                            //Does the product exist for a region that contains this planogram location?
                            PlanogramAssortmentRegion region = Regions.Where(w => (w.Products.Select(e => e.RegionalProductGtin).Contains(product.Gtin) || w.Products.Select(e => e.PrimaryProductGtin).Contains(product.Gtin)) && w.Locations.Select(x => x.LocationCode).Contains(this.Parent.LocationCode)).FirstOrDefault();
                            
                            //If we cannot find a region this product and location it should be removed
                            if (region == null)
                            {
                                removeProduct = Products.Contains(product);
                            }
                            break;
                        case PlanogramAssortmentProductLocalizationType.Local:
                            //Does the product exist as a local product for this planogram location?
                            PlanogramAssortmentLocalProduct localProduct = LocalProducts.Where(w => w.ProductGtin == product.Gtin && w.LocationCode == this.Parent.LocationCode).FirstOrDefault();

                            //If we cannot find a local product and location it should be removed
                            if (localProduct == null)
                            {
                                removeProduct = Products.Contains(product);
                            }
                            break;
                        default:
                            break;
                    }

                    //Remove product from the product list for this planogram
                    if (removeProduct)
                    {
                        Products.Remove(product);
                    }
                }                
            }
        }

        public PlanogramAssortmentRuleType GetAssortmentRuleType(PlanogramAssortmentProduct product, IEnumerable<PlanogramAssortmentRuleType> assortmentRuleTypesPriorityOrder)
        {
            //  Check the rules in priority order so that the highest assortment rule type is returned in cases when the product may belong to more than one.
            foreach (PlanogramAssortmentRuleType type in assortmentRuleTypesPriorityOrder)
            {
                switch (type)
                {
                    case PlanogramAssortmentRuleType.Optional:
                        if (product.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Optional) return PlanogramAssortmentRuleType.Optional;
                        break;
                    case PlanogramAssortmentRuleType.Normal:
                        if (product.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Normal) return PlanogramAssortmentRuleType.Normal;
                        break;
                    case PlanogramAssortmentRuleType.Inheritance:
                        if (product.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Inheritance) return PlanogramAssortmentRuleType.Inheritance;
                        break;
                    case PlanogramAssortmentRuleType.Family:
                        if (product.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None) return PlanogramAssortmentRuleType.Family;
                        break;
                    case PlanogramAssortmentRuleType.Product:
                        if (product.RuleType != PlanogramAssortmentProductRuleType.None) return PlanogramAssortmentRuleType.Product;
                        break;
                    case PlanogramAssortmentRuleType.Distribution:
                        if (product.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Distribution)
                            return PlanogramAssortmentRuleType.Distribution;
                        break;
                    case PlanogramAssortmentRuleType.LocalLine:
                        IEnumerable<String> localProductGtins =
                            LocalProducts.Select(localProduct => localProduct.ProductGtin);
                        if (localProductGtins.Contains(product.Gtin)) return PlanogramAssortmentRuleType.LocalLine;
                        break;
                    default:
                        return PlanogramAssortmentRuleType.Normal;
                }
            }
            return PlanogramAssortmentRuleType.Normal;
        }

        public Int32 GetAssortmentRulePriorityRank(PlanogramAssortmentProduct product, IList<PlanogramAssortmentRuleType> assortmentRuleTypesPriorityOrder)
        {
            return assortmentRuleTypesPriorityOrder.IndexOf(GetAssortmentRuleType(product, assortmentRuleTypesPriorityOrder));
        }

        #region Metadata

        /// <summary>
        /// Override of base calcualte metadata to make
        /// sure items are loaded.
        /// </summary>
        protected override void CalculateMetadata()
        {
            base.CalculateMetadata(
                new Object[]
                {
                    this.Products
                });
        }

        /// <summary>
        /// Override of the clear metadata
        /// </summary>
        protected override void ClearMetadata()
        {
            base.ClearMetadata(
                new Object[]
                {
                    this.Products
                });
        }

        #endregion

        #endregion
    }
}
