﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramProductMerchandisingStyle
    {
        Unit = 0,
        Tray = 1,
        Case = 2,
        Alternate = 3,
        Display = 4,
        PointOfPurchase = 5
    }

    /// <summary>
    /// PlanogramProductMerchandisingStyle Helper Class
    /// </summary>
    public static class PlanogramProductMerchandisingStyleHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductMerchandisingStyle, String> FriendlyNames =
            new Dictionary<PlanogramProductMerchandisingStyle, String>()
            {
                {PlanogramProductMerchandisingStyle.Unit, Message.Enum_PlanogramProductMerchandisingStyle_Unit},
                {PlanogramProductMerchandisingStyle.Tray, Message.Enum_PlanogramProductMerchandisingStyle_Tray},
                {PlanogramProductMerchandisingStyle.Case, Message.Enum_PlanogramProductMerchandisingStyle_Case},
                {PlanogramProductMerchandisingStyle.Alternate, Message.Enum_PlanogramProductMerchandisingStyle_Alternate},
                {PlanogramProductMerchandisingStyle.Display, Message.Enum_PlanogramProductMerchandisingStyle_Display},
                {PlanogramProductMerchandisingStyle.PointOfPurchase, Message.Enum_PlanogramProductMerchandisingStyle_PointOfPurchase},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductMerchandisingStyle, String> FriendlyDescriptions =
            new Dictionary<PlanogramProductMerchandisingStyle, String>()
            {
                {PlanogramProductMerchandisingStyle.Unit, Message.Enum_PlanogramProductMerchandisingStyle_Unit},
                {PlanogramProductMerchandisingStyle.Tray, Message.Enum_PlanogramProductMerchandisingStyle_Tray},
                {PlanogramProductMerchandisingStyle.Case, Message.Enum_PlanogramProductMerchandisingStyle_Case},
                {PlanogramProductMerchandisingStyle.Alternate, Message.Enum_PlanogramProductMerchandisingStyle_Alternate},
                {PlanogramProductMerchandisingStyle.Display, Message.Enum_PlanogramProductMerchandisingStyle_Display},
                {PlanogramProductMerchandisingStyle.PointOfPurchase, Message.Enum_PlanogramProductMerchandisingStyle_PointOfPurchase},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductMerchandisingStyle> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductMerchandisingStyle>()
            {
                {Message.Enum_PlanogramProductMerchandisingStyle_Unit.ToLowerInvariant(), PlanogramProductMerchandisingStyle.Unit},
                {Message.Enum_PlanogramProductMerchandisingStyle_Tray.ToLowerInvariant(), PlanogramProductMerchandisingStyle.Tray},
                {Message.Enum_PlanogramProductMerchandisingStyle_Case.ToLowerInvariant(), PlanogramProductMerchandisingStyle.Case},
                {Message.Enum_PlanogramProductMerchandisingStyle_Alternate.ToLowerInvariant(), PlanogramProductMerchandisingStyle.Alternate},
                {Message.Enum_PlanogramProductMerchandisingStyle_Display.ToLowerInvariant(), PlanogramProductMerchandisingStyle.Display},
                {Message.Enum_PlanogramProductMerchandisingStyle_PointOfPurchase.ToLowerInvariant(), PlanogramProductMerchandisingStyle.PointOfPurchase},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramProductMerchandisingStyle? PlanogramProductMerchandisingStyleGetEnum(String friendlyName)
        {
            PlanogramProductMerchandisingStyle? returnValue = null;
            PlanogramProductMerchandisingStyle enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
