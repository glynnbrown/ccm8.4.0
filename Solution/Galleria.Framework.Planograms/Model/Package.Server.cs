﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25916 : N.Foster
//  Added RowVersion, DateDeleted. Removed Description
// V8-25949 : N.Foster
//  Implemented package locking
// V8-25881 : A.Probyn
//  Replaced PlanogramCount with MetaPlanogramCount
//  Added DateMetadataCalculated
// V8-27237 : A.Silva   ~ Added DateValidationDataCalculated.
// V8-27411 : M.Pettit
//  Packages can now be fetched as readonly or read-write. A lock is only placed if fetched as read-writable object
// V8-27411 : M.Pettit
//  Package locking now requires userId and locktype
// V8-28147 : D.Pleasance
//  Amended to ensure UserName set upon loading of object
// V8-28535 : L.Ineson
//  Added delete command
#endregion
#region Version History: CCM801
// CCM-28622 : D.Pleasance
//  Added metricMappings
// V8-28774 : A.Kuszyk
//  Amended DataPortal_Update to set IsReadOnly to true before throwing an exception if a lock cannot be obtained.
#endregion
#region Version History: CCM802
// V8-28840 : L.Luong
//  Added EntityId
#endregion
#region Version History: CCM811
// V8-30504 : D.Pleasance
//  Amended GetDataTransferObject() to ensure UserName is applied.
// V8-30561 : N.Foster
//  Added override to allow date last modified to not be updated
#endregion
#region Version History: CCM820

// V8-31023 : A.Silva
//  Added an extra optional parameter to FetchByFileName(String, IEnumerable<PlanogramFieldMapping>, IEnumerable<PlanogramMetricMapping>, String) to allow fetching a specific planogram from multi-planogram package files (such as ProSpace).
// V8-31152 : M.Brumby
//  External file import uses a context now
// V8-31175 : A.Silva
//  Removed extra parameter to FetchByFileName.

#endregion
#region version History: CCM830
// V8-31546 : M.Pettit
//  Added FieldMappings property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.External;

namespace Galleria.Framework.Planograms.Model
{
    public sealed partial class Package
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Framework.Planograms.Dal.{0}.dll";
        #endregion

        #region Constructor
        private Package() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified package
        /// </summary>
        public static Package FetchByFileName(String fileName, Object userId, PackageLockType lockType)
        {
            return DataPortal.Fetch<Package>(new FetchCriteria(PackageType.FileSystem, fileName, userId, lockType, false));
        }

        /// <summary>
        /// Returns a read only version of the specified package
        /// </summary>
        public static Package FetchByFileName(String fileName)
        {
            return DataPortal.Fetch<Package>(new FetchCriteria(PackageType.FileSystem, fileName, null, PackageLockType.Unknown, true));
        }

        /// <summary>
        /// Returns the package with the given filename.
        /// </summary>
        /// <param name="fileName">The filename to fetch</param>
        /// <param name="mappings">The mappings to apply when converting an external format</param>
        /// <param name="metricMappings"></param>
        /// <returns></returns>
        public static Package FetchByFileName(String fileName, ImportContext context)
        {
            //List<PlanogramFieldMappingDto> mappingDtos = mappings.Select(m => m.GetDataTransferObject()).ToList();
            //List<PlanogramMetricMappingDto> metricMappingDtos = metricMappings.Select(m => m.GetDataTransferObject()).ToList();

            Object[] packageMappings = new Object[] { context };
            return DataPortal.Fetch<Package>(new FetchCriteria(PackageType.FileSystem, fileName, null, PackageLockType.Unknown, true, packageMappings));
        }

        /// <summary>
        /// Returns the specified package
        /// </summary>
        public static Package FetchById(Object id, Object userId, PackageLockType lockType)
        {
            return DataPortal.Fetch<Package>(new FetchCriteria(PackageType.Unknown, id, userId, lockType, false));
        }

        /// <summary>
        /// Returns a read only version of the specified package
        /// </summary>
        public static Package FetchById(Object id)
        {
            return DataPortal.Fetch<Package>(new FetchCriteria(PackageType.Unknown, id, null, PackageLockType.Unknown, true));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PackageDto dto, PackageType packageType, Object lockUserId, PackageLockType lockType, Boolean isReadOnly)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Object>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(UserNameProperty, (dto.UserName == null || dto.UserName.Length == 0) ? ApplicationContext.User.Identity.Name : dto.UserName);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            this.LoadProperty<Int32?>(MetaPlanogramCountProperty, dto.MetaPlanogramCount);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<DateTime?>(DateMetadataCalculatedProperty, dto.DateMetadataCalculated);
            this.LoadProperty<DateTime?>(DateValidationDataCalculatedProperty, dto.DateValidationDataCalculated);
            this.LoadProperty<Object>(LockUserIdProperty, lockUserId);
            this.LoadProperty<PackageLockType>(LockTypeProperty, lockType);
            this.LoadProperty<Boolean>(IsReadOnlyProperty, isReadOnly);
            this.LoadProperty<Object>(FieldMappingsProperty, dto.FieldMappings);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PackageDto GetDataTransferObject()
        {
            return new PackageDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Object>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                UserName = String.IsNullOrEmpty(this.ReadProperty<String>(UserNameProperty)) ? ApplicationContext.User.Identity.Name : this.ReadProperty<String>(UserNameProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
                MetaPlanogramCount = ReadProperty<Int32?>(MetaPlanogramCountProperty),
                ExtendedData = ReadProperty<Object>(ExtendedDataProperty),
                DateMetadataCalculated = ReadProperty<DateTime?>(DateMetadataCalculatedProperty),
                DateValidationDataCalculated = ReadProperty<DateTime?>(DateValidationDataCalculatedProperty),
                FieldMappings = ReadProperty<Object>(FieldMappingsProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.PackageType, criteria.Id);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        // attempt to lock the package before opening it
                        Boolean lockReadOnly = criteria.LockReadOnly;
                        if (dal.LockById(criteria.Id, criteria.LockUserId, (Byte)criteria.LockType, lockReadOnly) == (Byte)PackageLockResult.Failed)
                        {
                            if (lockReadOnly) throw new PackageLockException();
                            lockReadOnly = true;
                            if (dal.LockById(criteria.Id, criteria.LockUserId, (Byte)criteria.LockType, lockReadOnly) == (Byte)PackageLockResult.Failed)
                                throw new PackageLockException();
                        }

                        // fetch the packagage
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchById(criteria.Id, criteria.FetchArgument),
                            criteria.PackageType,
                            criteria.LockUserId,
                            criteria.LockType,
                            lockReadOnly);
                    }
                }
                catch
                {
                    throw;
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        protected override void DataPortal_Insert()
        {
            if (this.IsReadOnly) throw new PackageReadOnlyException();
            IDalFactory dalFactory = this.GetDalFactory(this.PackageType, this.Id);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        // attempt to lock the package before saving it
                        // file based dals require the lock to be applied
                        // before we take any action as the lock triggers
                        // the opening of the output file
                        // However export file based Dals do not require a lock on the package??
                        if (this.PackageType == PackageType.FileSystem)
                        {
                            if (dal.LockById(this.Id, this.LockUserId, (Byte)this.LockType, this.IsReadOnly) == (Byte)PackageLockResult.Failed)
                                throw new PackageLockException();
                        }

                        // save the package
                        dalContext.Begin();
                        PackageDto dto = this.GetDataTransferObject();
                        Object oldId = dto.Id;
                        dal.Insert(dto);
                        FieldManager.UpdateChildren(dalContext, dto);
                        this.LoadProperty<Object>(IdProperty, dto.Id);
                        this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                        this.LoadProperty<Object>(EntityIdProperty, dto.EntityId);
                        this.LoadProperty<String>(NameProperty, dto.Name);
                        this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                        this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                        dalContext.RegisterId<Package>(oldId, dto.Id);
                        dalContext.Commit();

                        // attempt to lock the package after saving it
                        // database based dals require the planogram to
                        // be inserted first before we can lock it as the
                        // planogram id does not exist in the database
                        // until after the save has occurred
                        if (this.PackageType != PackageType.FileSystem)
                        {
                            if (dal.LockById(this.Id, this.LockUserId, (Byte)this.LockType, this.IsReadOnly) == (Byte)PackageLockResult.Failed)
                                throw new PackageLockException();
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        protected override void DataPortal_Update()
        {
            if (this.IsReadOnly) throw new PackageReadOnlyException();
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        // attempt to lock the package before saving it
                        if (dal.LockById(this.Id, this.LockUserId, (Byte)this.LockType, this.IsReadOnly) == (Byte)PackageLockResult.Failed)
                        {
                            // Set the read-only property to true, because this package is probably locked by another use.
                            // We set the property, in case any other objects are monitoring this one for changes.
                            this.SetProperty<Boolean>(IsReadOnlyProperty, true);
                            throw new PackageLockException();
                        }

                        // save the package
                        dalContext.Begin();
                        PackageDto dto = this.GetDataTransferObject();
                        dal.Update(dto);
                        FieldManager.UpdateChildren(dalContext, dto);
                        this.LoadProperty<Object>(IdProperty, dto.Id);
                        this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                        this.LoadProperty<Object>(EntityIdProperty, dto.EntityId);
                        this.LoadProperty<String>(NameProperty, dto.Name);
                        this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                        this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                        dalContext.RegisterId<Package>(dto.Id);
                        dalContext.Commit();
                    }
                }
                catch
                {
                    throw;
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            if (this.IsReadOnly) throw new PackageReadOnlyException();
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    // attempt to lock the package before saving it
                    if (dal.LockById(this.Id, this.LockUserId, (Byte)this.LockType, this.IsReadOnly) == (Byte)PackageLockResult.Failed)
                        throw new PackageLockException();

                    // delete the package
                    dal.DeleteById(this.Id);
                }
            }
        }
        #endregion

        #endregion

        #region Commands

        #region LockPackageCommand
        /// <summary>
        /// Performs the locking of a package
        /// </summary>
        private partial class LockPackageCommand : CommandBase<LockPackageCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.PackageType, this.Id, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        this.Result = (PackageLockResult)dal.LockById(this.Id, this.UserId, (Byte)this.LockType, this.LockReadOnly);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockPackageCommand
        /// <summary>
        /// Performs the unlocking of a package
        /// </summary>
        private partial class UnlockPackageCommand : CommandBase<UnlockPackageCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.PackageType, this.Id, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        this.Result = (PackageUnlockResult)dal.UnlockById(this.Id, this.LockUserId, (Byte)this.LockType, this.LockReadOnly);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region DeletePackageByIdCommand

        /// <summary>
        /// Deletes the package with the given id.
        /// </summary>
        private partial class DeletePackageByIdCommand : CommandBase<DeletePackageByIdCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.PackageType, this.Id, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        // attempt to lock the package first so that we know that noone is currently editing it and it is safe to
                        // perform the delete.
                        if (dal.LockById(this.Id, this.UserId, (Byte)this.LockType, /*lockReadOnly*/false) == (Byte)PackageLockResult.Failed)
                        {
                            throw new PackageLockException();
                        }

                        //now delete the package
                        dal.DeleteById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }

        #endregion

        #region UpdatePlanogramAttributesCommand
        ///
        /// <summary>
        /// Performs the unlocking of a package
        /// </summary>
        private partial class UpdatePlanogramAttributesCommand : CommandBase<UpdatePlanogramAttributesCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.PackageType, this.Id, out dalFactoryName);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                    {
                        var planAttributesDtoList = new List<PlanogramAttributesDto>();

                        foreach (PlanogramAttributes planAttributesItem in this.AttributesList)
                        {
                            planAttributesDtoList.Add(planAttributesItem.GetDataTransferObject());
                        }

                        this.Result = dal.UpdatePlanogramAttributes(planAttributesDtoList);
                    }
                }
            }

            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns the correct dal factory based on the
        /// package type and id
        /// </summary>
        private IDalFactory GetDalFactory(PackageType packageType, Object id)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(packageType, id, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<PackageType>(PackageTypeProperty, packageType);
            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the
        /// package type and id
        /// </summary>
        private static IDalFactory GetDalFactory(PackageType packageType, Object id, out String dalFactoryName)
        {
            switch (packageType)
            {
                case PackageType.FileSystem:
                    return GetFileSystemDalFactory(id.ToString(), out dalFactoryName);
                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        /// <summary>
        /// Returns a dal factory for a file system object
        /// </summary>
        private static IDalFactory GetFileSystemDalFactory(String fileName, out String dalFactoryName)
        {
            // set the dal factory name
            dalFactoryName = fileName;

            // determine if the dal already exists
            if (DalContainer.Contains(dalFactoryName))
                return DalContainer.GetDalFactory(dalFactoryName);

            Int32 planNameIndex = fileName.IndexOf("::", StringComparison.InvariantCultureIgnoreCase);
            if (planNameIndex != -1)
            {
                //  Split the plan name and the real file path.
                //_planName = _fileName.Substring(planNameIndex + 2, _fileName.Length - planNameIndex - 2);
                fileName = fileName.Substring(0, planNameIndex); 
            }

            // the dal factory does not exist so
            // attempt to register a dal based on
            // the file extension
            String fileExtension = Path.GetExtension(fileName).Trim('.');
            fileExtension =
                fileExtension.Substring(0, 1).ToUpperInvariant() +
                fileExtension.Substring(1).ToLowerInvariant();


            // get the dal assembly name
            String dalAssemblyName = String.Format(
                _dalAssemblyName,
                fileExtension);

            // create the config information for the dal container
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                dalFactoryName,
                dalAssemblyName);

            // register the dal inside the dal container
            DalContainer.RegisterFactory(dalFactoryConfig);

            // and return the dal factory
            return DalContainer.GetDalFactory(dalFactoryName);
        }
        #endregion
    }
}
