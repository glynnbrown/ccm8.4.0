﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeNodeList
    {
        #region Constructor
        private PlanogramConsumerDecisionTreeNodeList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static PlanogramConsumerDecisionTreeNodeList FetchListByParentNodeId(
            IDalContext dalContext, Object parentId, IEnumerable<PlanogramConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTreeNodeList>(dalContext, parentId, masterNodeDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Object parentId, IEnumerable<PlanogramConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            IEnumerable<PlanogramConsumerDecisionTreeNodeDto> childList = masterNodeDtoList.Where(p => Object.Equals(p.ParentNodeId,parentId));
            foreach (PlanogramConsumerDecisionTreeNodeDto childDto in childList)
            {
                this.Add(PlanogramConsumerDecisionTreeNode.FetchPlanogramConsumerDecisionTreeNode(dalContext, childDto, masterNodeDtoList));
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
