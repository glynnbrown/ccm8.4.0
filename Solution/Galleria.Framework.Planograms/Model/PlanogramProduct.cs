﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-25479 : A.Kuszyk
//      Business rules added.
// V8-26041 : A.Kuszyk
//  Additional Product properties added. Added LoadProductImages
// V8-26062 : A.Kuszyk
//  Added friendly names to new properties.
// V8-26147 : L.Ineson
//  Added CustomAttributeData property
// V8-26516 : L.Ineson
//  Corrected use of squeeze height width and depth.
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-26377 : L.Ineson
//  Added GetPerformanceData
// V8-26702 : A.Kuszyk
//  Changed use of AutoMapper to use generic Map method.
// V8-27058 : A.Probyn
//  Added new meta data properties and calculations
//  Added ClearMetadata
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-27836 : A.Silva
//      Added IsDuplicateGtin, IsMinDosAchieved, IsMinCasesAchieved, 
//      IsMinDeliveriesAchieved, IsMinShelfLifeAchieved.
// V8-26777 : L.Ineson
//  Removed CanMerch properties
// V8-27898 : L.Ineson
// Added FieldInfo methods and 
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
// V8-25976 : L.Ineson
//  Added GetBrokenValidationWarnings method.
// V8-27824 : D.Pleasance
//  Amended GetUnorientatedUnitSize to allow product dimensions of less than 1 but still default to 1 if less than equal to 0.
// V8-28015 : L.Ineson
//  Added GetUnorientatedTraySize
// V8-28295 : A.Probyn
//  Updated EnumerateDisplayableFieldInfos to include performance data. This had been commented out
//  and no comment as to why.
// V8-28367 : A.Proboyn
//  Found out why the issue in 28295 had been done. Code has been split into 2 in PlanogramProductViewModelBase so it explicity
//  calls performance data EnumerateDisplayableFieldInfos. I've added this to ProductColumnLayoutFactory where it was missing.
// V8-28461 
//  Moved TrayPackUnitsProperty to SizeGroup in displayable fields
// V8-28348 : A.Kuszyk
//  Protected Squeeze properties from divisions by zero.

#endregion
#region Version History: CCM801

// V8-27494 : A.Kuszyk
//  Added UpdateFrom method.
// V8-28676 : A.Silva
//      Updated GetMerchStyleImageAsync for new signature of GetImageAsync.
//      Overloaded GetMerchStyleImageAsync to accept a callback to be invoked when the images are really loaded if not done so immediatelly.
// V8-27987 : A.Kuszyk
//  Added ShapeType to EnumerateDisplayableFieldInfos.
// V8-28722 : N.Foster
//  Added support for batch dal operations
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
// V8-28825 : A.Kuszyk
//  Correct issue in UpdateFrom with CustomAttributes not being updated correctly.
// V8-28939 : A.Silva
//      Removed old overload for GetMerchStyleImageAsync and modified the existing one so that it substitutes by the Unit Image if there is no Img or Img.ImageData.

#endregion
#region Version History: CCM802

// V8-28455 : A.Kuszyk
//  Added validation rules to ensure that nesting sizes are less than or equal to unit sizes.
// V8-29030 : J.Pickup
//  Enhanced reflection so that it now reflects on product performance data to update those properties. (Required IProductPerformanceData) (UpdateFrom()).
//  Also EnumerateObjectFieldInfos now allows for the returning of the performance data fields (Boolean switch).
// V8-29028 : L.Ineson
//  Squeeze values are now passed into GetUnorientatedUnitSize
// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-28811 : L.Luong
//  Added MetaPlanogramLinearSpacePercentageProperty
//  Added MetaPlanogramAreaSpacePercentageProperty
//  Added MetaPlanogramVolumetricSpacePercentageProperty
// V8-29180 : N.Foster
//  Corrected issue with MetaNotAchieveInventory calculation
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.
// V8-29291 : L.Ineson
//  Removed unnecessary Calculate and Clear meta data overrides.

#endregion
#region Version History: CCM803

// V8-29137 : A.Kuszyk
//  Added GetSize helper method.
// V8-29506 : M.Shelley
//  Add new metadata properties for reporting:
//      MetaPositionCount, MetaTotalFacings, MetaTotalLinearSpace, MetaTotalAreaSpace, MetaTotalVolumetricSpace
// V8-28491 : L.Luong
//  Added MetaIsInMasterData

#endregion
#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29593 : L.Ineson
//Fill colour now defaults to white.
// V8-30216 : A.Kuszyk
//  Made some performance improvements to UpdateFrom to use automapper if passed an IPlanogramProduct as a source.
// V8-30118 : A.Silva
//  Added IsMerchandisingStyleAnyOf to determine if a Product is of any of the merchandising styles passed to the method.

#endregion
#region Version History: CCM811

// V8-29544 : L.Ineson
//  Marked FillColour and BlockingColour properties as colour types.
// V8-30398 : A.Silva
//  Amended CalculateMetaData to correctly calculate Product Facings.
// V8-30577 : A.Probyn
//  Updated GetFieldValue to apply rounding to performance data values

#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//  Added ColourGroupValue property.
// V8-30733 : L.Luong
//  Added UpdateLocationProductAttributes
// V8-30791 : D.Pleasance
//  Amended so that changing the fill color resets the ColourGroupValue
// V8-30763 : I.George
// Removed the calculated CDTNode property and added metaDataCDTNode property
// V8-30705 : A.Probyn
//  Added new assortment rules related meta data properties
//  Updated OnCalculateMetadata & CalculatePostPlanogramMetadata
// V8-30904 : A.Probyn
//  Updated UpdateLocationProductAttributes to take into account 'blank values' not just null. Strings are reference types
//  and so never equal null.
// V8-31164 : D.Pleasance
//  Removed BlockingColour \ SequenceNumber
// V8-31269 : L.Ineson
//  Added method for image synchronous fetch.
// V8-31243 : A.Probyn
//  Added Fix to CalculatePostPlanogramMetadata for dependency family rule meta data calculation.
// V8-31246 : A.Kuszyk
//  Added optional parameter to EnumerateDisplayableFieldInfos to exclude assortment fields.
// V8-31456 : N.Foster
//  Removed lazy loading of custom attributes
// V8-31476 : A.Probyn
//  Added missing business rules for schema NVARCHAR limits
// V8-30968 : A.Kuszyk
//  Commented out rule supression in UpdateFrom to fix property changed notification issue.
#endregion
#region Version History: CCM830
// V8-31612 : A.Kuszyk
//  Performance improvements to UpdateFrom method.
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31866 : D.Pleasance
//  Amended business rules so that Master.Product match Content.PlanogramProduct
//  V8-30697 : L.Ineson
//  Removed Max Nest High and Max Nest Deep from displayable field infos.
// V8-31947 : A.Silva
//  Added Metadata property for Comparison Status.
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-32166 : A.Probyn
//  Added fix to GetFieldValue as Performance Data Rank cannot be cast to Single, it must be converted.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32424 : L.Ineson
//  GetFieldValue now returns percentage display types to 5 dp rather then 2 as this was causing precision loss.
// V8-32505 : A.Kuszyk
//  Added debugger display to be GTIN: Name.
// V8-32396 : A.Probyn
//  Added MetaIsDependencyFamilyRuleBroken
// V8-32579 : M.Brumby
//  Changing Gtin will change Assortment Gtin if it exists
// V8-31626 : A.Heathcote
//  Removed GetPlanogramBlockingGroup() as the method was outdated and was making PlanogramPosition misbehave.
//  The method is no longer needed (Not referenced anywhere anymore).
// V8-32609 : L.Ineson
//  Removed old PegProngOffset field
// V8-32819 : M.Pettit
//  Added MetaIsBuddied
// V8-32953 : L.Ineson
// GetImage methods now accept a userstate parameter and output the actual type of image returned.
// V8-32962 : A.Kuszyk
//  Changed CSLA properties to use ReadProperty instead of GetProperty, as this bypasses
//  property level security (which we don't use) and yields a performance increase.
// CCM-18628 : M.Brumby
//  When creating a new product from an existing one the GTIN will be used as the name if there is no name.
//  This is a quick fix untill we handle business rule errors better.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using AutoMapper;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Enums;
using Galleria.Framework.Logging;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a planogram product
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Gtin}: {Name}")]
    public sealed partial class PlanogramProduct :
        ModelObject<PlanogramProduct>,
        IPlanogramProduct,
        IPlanogramProductInfo,
        ICustomAttributeDataParent
    {
        #region Constants
        public static readonly String AssortmentFieldPrefix = "AssortmentProduct.";
        #endregion

        #region Static Constructor
        /// <summary>
        /// Static constructor for this type
        /// </summary>
        static PlanogramProduct()
        {
            FriendlyName = Message.PlanogramProduct_FriendlyName;
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Fields

        /// <summary>
        /// A dictionary of the properties of <see cref="PlanogramProduct"/> keyed by their name.
        /// </summary>
        private static Dictionary<String, PropertyInfo> _propertyInfosByName = null;

        /// <summary>
        /// A dictionary of <see cref="PropertyInfo"/>s keyed by their type, for all the types that <see cref="PlanogramProduct"/>
        /// has had to update from.
        /// </summary>
        private static Dictionary<Type, List<PropertyInfo>> _sourcePropertiesLookup = new Dictionary<Type, List<PropertyInfo>>();

        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get
            {
                PlanogramProductList parentList = base.Parent as PlanogramProductList;
                if (parentList == null) return null;
                return parentList.Parent;
            }
        }
        #endregion

        #region Properties

        #region Static Properties

        /// <summary>
        /// A dictionary of the properties of <see cref="PlanogramProduct"/> keyed by their name.
        /// </summary>
        private static Dictionary<String, PropertyInfo> PropertyInfosByName
        {
            get
            {
                if (_propertyInfosByName == null)
                {
                    _propertyInfosByName =
                        typeof(PlanogramProduct)
                        .GetProperties()
                        .Where(p => p.CanWrite)
                        .ToDictionary(p => p.Name, p => p);
                }
                return _propertyInfosByName;
            }
        }

        #endregion

        #region GTIN
        /// <summary>
        /// GTIN property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin, Message.PlanogramProduct_Gtin);
        /// <summary>
        /// Gets or sets the products GTIN
        /// </summary>
        public String Gtin
        {
            get { return this.ReadProperty<String>(GtinProperty); }
            set
            {
                //if we have assortment products then we should set the Gtin for them as well
                //so we don't loose them due to the link being via Gtin
                PlanogramAssortmentProduct assortmentProduct = this.GetPlanogramAssortmentProduct();
                if (assortmentProduct != null)
                {
                    assortmentProduct.Gtin = value;
                }
                this.SetProperty<String>(GtinProperty, value);
            }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);
        /// <summary>
        /// Gets or sets the products name
        /// </summary>
        public String Name
        {
            get { return this.ReadProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Brand
        /// <summary>
        /// Brand property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BrandProperty =
            RegisterModelProperty<String>(c => c.Brand, Message.PlanogramProduct_Brand);
        /// <summary>
        /// Gets or sets the product brand
        /// </summary>
        public String Brand
        {
            get { return this.ReadProperty<String>(BrandProperty); }
            set { this.SetProperty<String>(BrandProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.PlanogramProduct_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the product height
        /// </summary>
        public Single Height
        {
            get { return this.ReadProperty<Single>(HeightProperty); }
            set { this.SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramProduct_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the product width
        /// </summary>
        public Single Width
        {
            get { return this.ReadProperty<Single>(WidthProperty); }
            set { this.SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramProduct_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the product depth
        /// </summary>
        public Single Depth
        {
            get { return this.ReadProperty<Single>(DepthProperty); }
            set { this.SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region DisplayHeight
        /// <summary>
        /// DisplayHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DisplayHeightProperty =
            RegisterModelProperty<Single>(c => c.DisplayHeight, Message.PlanogramProduct_DisplayHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the display height
        /// </summary>
        public Single DisplayHeight
        {
            get { return this.ReadProperty<Single>(DisplayHeightProperty); }
            set { this.SetProperty<Single>(DisplayHeightProperty, value); }
        }
        #endregion

        #region DisplayWidth
        /// <summary>
        /// DisplayWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DisplayWidthProperty =
            RegisterModelProperty<Single>(c => c.DisplayWidth, Message.PlanogramProduct_DisplayWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products display width
        /// </summary>
        public Single DisplayWidth
        {
            get { return this.ReadProperty<Single>(DisplayWidthProperty); }
            set { this.SetProperty<Single>(DisplayWidthProperty, value); }
        }
        #endregion

        #region DisplayDepth
        /// <summary>
        /// DisplayDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DisplayDepthProperty =
            RegisterModelProperty<Single>(c => c.DisplayDepth, Message.PlanogramProduct_DisplayDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products display depth
        /// </summary>
        public Single DisplayDepth
        {
            get { return this.ReadProperty<Single>(DisplayDepthProperty); }
            set { this.SetProperty<Single>(DisplayDepthProperty, value); }
        }
        #endregion

        #region AlternateHeight
        /// <summary>
        /// AlternateHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AlternateHeightProperty =
            RegisterModelProperty<Single>(c => c.AlternateHeight, Message.PlanogramProduct_AlternateHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products alternate height
        /// </summary>
        public Single AlternateHeight
        {
            get { return this.ReadProperty<Single>(AlternateHeightProperty); }
            set { this.SetProperty<Single>(AlternateHeightProperty, value); }
        }
        #endregion

        #region AlternateWidth
        /// <summary>
        /// AlternateWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AlternateWidthProperty =
            RegisterModelProperty<Single>(c => c.AlternateWidth, Message.PlanogramProduct_AlternateWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products alternate width
        /// </summary>
        public Single AlternateWidth
        {
            get { return this.ReadProperty<Single>(AlternateWidthProperty); }
            set { this.SetProperty<Single>(AlternateWidthProperty, value); }
        }
        #endregion

        #region AlternateDepth
        /// <summary>
        /// AlternateDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AlternateDepthProperty =
            RegisterModelProperty<Single>(c => c.AlternateDepth, Message.PlanogramProduct_AlternateDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products alternate depth
        /// </summary>
        public Single AlternateDepth
        {
            get { return this.ReadProperty<Single>(AlternateDepthProperty); }
            set { this.SetProperty<Single>(AlternateDepthProperty, value); }
        }
        #endregion

        #region PointOfPurchaseHeight
        /// <summary>
        /// PointOfPurchase property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseHeightProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseHeight, Message.PlanogramProduct_PointOfPurchaseHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products point of purchase height
        /// </summary>
        public Single PointOfPurchaseHeight
        {
            get { return this.ReadProperty<Single>(PointOfPurchaseHeightProperty); }
            set { this.SetProperty<Single>(PointOfPurchaseHeightProperty, value); }
        }
        #endregion

        #region PointOfPurchaseWidth
        /// <summary>
        /// PointOfPurchaseWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseWidthProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseWidth, Message.PlanogramProduct_PointOfPurchaseWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the products point of purchase width
        /// </summary>
        public Single PointOfPurchaseWidth
        {
            get { return this.ReadProperty<Single>(PointOfPurchaseWidthProperty); }
            set { this.SetProperty<Single>(PointOfPurchaseWidthProperty, value); }
        }
        #endregion

        #region PointOfPurchaseDepth
        /// <summary>
        /// PointOfPurchaseDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseDepthProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseDepth, Message.PlanogramProduct_PointOfPurchaseDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct PointOfPurchaseDepth
        /// </summary>
        public Single PointOfPurchaseDepth
        {
            get { return this.ReadProperty<Single>(PointOfPurchaseDepthProperty); }
            set { this.SetProperty<Single>(PointOfPurchaseDepthProperty, value); }
        }
        #endregion

        #region NumberOfPegHoles
        /// <summary>
        /// NumberOfPegHoles property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> NumberOfPegHolesProperty =
            RegisterModelProperty<Byte>(c => c.NumberOfPegHoles, Message.PlanogramProduct_NumberOfPegHoles);
        /// <summary>
        /// Gets or sets the number of peg holes
        /// </summary>
        public Byte NumberOfPegHoles
        {
            get { return this.ReadProperty<Byte>(NumberOfPegHolesProperty); }
            set { this.SetProperty<Byte>(NumberOfPegHolesProperty, value); }
        }
        #endregion

        #region PegX
        /// <summary>
        /// PegX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegXProperty =
            RegisterModelProperty<Single>(c => c.PegX, Message.PlanogramProduct_PegX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct PegX
        /// </summary>
        public Single PegX
        {
            get { return this.ReadProperty<Single>(PegXProperty); }
            set { this.SetProperty<Single>(PegXProperty, value); }
        }
        #endregion

        #region PegX2
        /// <summary>
        /// PegX2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegX2Property =
            RegisterModelProperty<Single>(c => c.PegX2, Message.PlanogramProduct_PegX2, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the pegx2
        /// </summary>
        public Single PegX2
        {
            get { return this.ReadProperty<Single>(PegX2Property); }
            set { this.SetProperty<Single>(PegX2Property, value); }
        }
        #endregion

        #region PegX3
        /// <summary>
        /// PegX3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegX3Property =
            RegisterModelProperty<Single>(c => c.PegX3, Message.PlanogramProduct_PegX3, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the pegx3
        /// </summary>
        public Single PegX3
        {
            get { return this.ReadProperty<Single>(PegX3Property); }
            set { this.SetProperty<Single>(PegX3Property, value); }
        }
        #endregion

        #region PegY
        /// <summary>
        /// PegY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegYProperty =
            RegisterModelProperty<Single>(c => c.PegY, Message.PlanogramProduct_PegY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the peg y
        /// </summary>
        public Single PegY
        {
            get { return this.ReadProperty<Single>(PegYProperty); }
            set { this.SetProperty<Single>(PegYProperty, value); }
        }
        #endregion

        #region PegY2
        /// <summary>
        /// PegY2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegY2Property =
            RegisterModelProperty<Single>(c => c.PegY2, Message.PlanogramProduct_PegY2, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the pegy 2
        /// </summary>
        public Single PegY2
        {
            get { return this.ReadProperty<Single>(PegY2Property); }
            set { this.SetProperty<Single>(PegY2Property, value); }
        }
        #endregion

        #region PegY3
        /// <summary>
        /// PegY3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegY3Property =
            RegisterModelProperty<Single>(c => c.PegY3, Message.PlanogramProduct_PegY3, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the peg y3
        /// </summary>
        public Single PegY3
        {
            get { return this.ReadProperty<Single>(PegY3Property); }
            set { this.SetProperty<Single>(PegY3Property, value); }
        }
        #endregion

        #region PegProngOffsetX
        /// <summary>
        /// PegProngOffsetX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegProngOffsetXProperty =
            RegisterModelProperty<Single>(c => c.PegProngOffsetX, Message.PlanogramProduct_PegProngOffsetX, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the peg prong X offset
        /// </summary>
        public Single PegProngOffsetX
        {
            get { return this.ReadProperty<Single>(PegProngOffsetXProperty); }
            set { this.SetProperty<Single>(PegProngOffsetXProperty, value); }
        }
        #endregion

        #region PegProngOffsetY
        /// <summary>
        /// PegProngOffsetY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegProngOffsetYProperty =
            RegisterModelProperty<Single>(c => c.PegProngOffsetY, Message.PlanogramProduct_PegProngOffsetY, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the peg prong X offset
        /// </summary>
        public Single PegProngOffsetY
        {
            get { return this.ReadProperty<Single>(PegProngOffsetYProperty); }
            set { this.SetProperty<Single>(PegProngOffsetYProperty, value); }
        }
        #endregion

        #region PegDepth
        /// <summary>
        /// PegDepth property
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegDepthProperty =
            RegisterModelProperty<Single>(c => c.PegDepth, Message.PlanogramProduct_PegDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the peg depth
        /// </summary>
        public Single PegDepth
        {
            get { return this.ReadProperty<Single>(PegDepthProperty); }
            set { this.SetProperty<Single>(PegDepthProperty, value); }
        }
        #endregion

        #region SqueezeHeight
        /// <summary>
        /// SqueezeHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeHeightProperty =
            RegisterModelProperty<Single>(c => c.SqueezeHeight, Message.PlanogramProduct_SqueezeHeight, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The percentage by which the height of this product can be squeezed. A value of 0 or 1
        /// represents no squeeze, but a value of 0.5 indicates that this product can be squeezed
        /// to half its actual height.
        /// </summary>
        public Single SqueezeHeight
        {
            get { return this.ReadProperty<Single>(SqueezeHeightProperty); }
            set
            {
                SetProperty<Single>(SqueezeHeightProperty, value);
                SetProperty<Single>(SqueezeHeightActualProperty, this.Height * (value / (Single)1.00));
            }
        }

        public static readonly ModelPropertyInfo<Single> SqueezeHeightActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeHeightActual, Message.PlanogramProduct_SqueezeHeightActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeHeightActual
        {
            get { return this.Height * (this.SqueezeHeight / (Single)1.00); }
            set
            {
                this.SqueezeHeight = (this.Height.EqualTo(0) ? 0F : (value / this.Height)) * (Single)1.00;
                this.SetProperty<Single>(SqueezeHeightActualProperty, value);
            }
        }

        #endregion

        #region SqueezeWidth
        /// <summary>
        /// SqueezeWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeWidthProperty =
            RegisterModelProperty<Single>(c => c.SqueezeWidth, Message.PlanogramProduct_SqueezeWidth, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The percentage by which the width of this product can be squeezed. A value of 0 or 1
        /// represents no squeeze, but a value of 0.5 indicates that this product can be squeezed
        /// to half its actual width.
        /// </summary>
        public Single SqueezeWidth
        {
            get { return this.ReadProperty<Single>(SqueezeWidthProperty); }
            set
            {
                this.SetProperty<Single>(SqueezeWidthProperty, value);
                this.SetProperty<Single>(SqueezeWidthActualProperty, this.Width * (value / (Single)1.00));
            }
        }

        public static readonly ModelPropertyInfo<Single> SqueezeWidthActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeWidthActual, Message.PlanogramProduct_SqueezeWidthActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeWidthActual
        {
            get { return this.Width * (this.SqueezeWidth / (Single)1.00); }
            set
            {
                this.SqueezeWidth = (this.Width.EqualTo(0) ? 0F : (value / this.Width)) * (Single)1.00;
                this.SetProperty<Single>(SqueezeWidthActualProperty, value);
            }
        }

        #endregion

        #region SqueezeDepth
        /// <summary>
        /// SqueezeDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeDepthProperty =
            RegisterModelProperty<Single>(c => c.SqueezeDepth, Message.PlanogramProduct_SqueezeDepth, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The percentage by which the depth of this product can be squeezed. A value of 0 or 1
        /// represents no squeeze, but a value of 0.5 indicates that this product can be squeezed
        /// to half its actual depth.
        /// </summary>
        public Single SqueezeDepth
        {
            get { return this.ReadProperty<Single>(SqueezeDepthProperty); }
            set
            {
                this.SetProperty<Single>(SqueezeDepthProperty, value);
                this.SetProperty<Single>(SqueezeDepthActualProperty, this.Depth * (value / (Single)1.00));
            }
        }



        public static readonly ModelPropertyInfo<Single> SqueezeDepthActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeDepthActual, Message.PlanogramProduct_SqueezeDepthActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeDepthActual
        {
            get { return this.Depth * (this.SqueezeDepth / (Single)1.00); }
            set
            {
                this.SqueezeDepth = (this.Depth.EqualTo(0) ? 0F : (value / this.Depth)) * (Single)1.00;
                this.SetProperty<Single>(SqueezeDepthActualProperty, value);
            }
        }

        #endregion

        #region NestingHeight
        /// <summary>
        /// NestingHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NestingHeightProperty =
            RegisterModelProperty<Single>(c => c.NestingHeight, Message.PlanogramProduct_NestingHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the nesting height
        /// </summary>
        public Single NestingHeight
        {
            get { return this.ReadProperty<Single>(NestingHeightProperty); }
            set { this.SetProperty<Single>(NestingHeightProperty, value); }
        }
        #endregion

        #region NestingWidth
        /// <summary>
        /// NestingWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NestingWidthProperty =
            RegisterModelProperty<Single>(c => c.NestingWidth, Message.PlanogramProduct_NestingWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the nesting width
        /// </summary>
        public Single NestingWidth
        {
            get { return this.ReadProperty<Single>(NestingWidthProperty); }
            set { this.SetProperty<Single>(NestingWidthProperty, value); }
        }
        #endregion

        #region NestingDepth
        /// <summary>
        /// NestingDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NestingDepthProperty =
            RegisterModelProperty<Single>(c => c.NestingDepth, Message.PlanogramProduct_NestingDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the nesting depth
        /// </summary>
        public Single NestingDepth
        {
            get { return this.ReadProperty<Single>(NestingDepthProperty); }
            set { this.SetProperty<Single>(NestingDepthProperty, value); }
        }
        #endregion

        #region CasePackUnits
        /// <summary>
        /// CasePackUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CasePackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.CasePackUnits, Message.PlanogramProduct_CasePackUnits);
        /// <summary>
        /// Gets or sets the case pack units
        /// </summary>
        public Int16 CasePackUnits
        {
            get { return this.ReadProperty<Int16>(CasePackUnitsProperty); }
            set
            {
                this.SetProperty<Int16>(CasePackUnitsProperty, value);

                //tell any linked performance data to update.
                PlanogramPerformanceData perfData = GetPlanogramPerformanceData();
                if (perfData != null) perfData.UpdateInventoryValues();
            }
        }
        #endregion

        #region CaseHigh
        /// <summary>
        /// CaseHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> CaseHighProperty =
            RegisterModelProperty<Byte>(c => c.CaseHigh, Message.PlanogramProduct_CaseHigh);
        /// <summary>
        /// Gets or sets the case high
        /// </summary>
        public Byte CaseHigh
        {
            get { return this.ReadProperty<Byte>(CaseHighProperty); }
            set { this.SetProperty<Byte>(CaseHighProperty, value); }
        }
        #endregion

        #region CaseWide
        /// <summary>
        /// CaseWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> CaseWideProperty =
            RegisterModelProperty<Byte>(c => c.CaseWide, Message.PlanogramProduct_CaseWide);
        /// <summary>
        /// The product CaseWide
        /// </summary>
        public Byte CaseWide
        {
            get { return this.ReadProperty<Byte>(CaseWideProperty); }
            set { this.SetProperty<Byte>(CaseWideProperty, value); }
        }
        #endregion

        #region CaseDeep
        /// <summary>
        /// CaseDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> CaseDeepProperty =
            RegisterModelProperty<Byte>(c => c.CaseDeep, Message.PlanogramProduct_CaseDeep);
        /// <summary>
        /// Gets or sets the case deep
        /// </summary>
        public Byte CaseDeep
        {
            get { return this.ReadProperty<Byte>(CaseDeepProperty); }
            set { this.SetProperty<Byte>(CaseDeepProperty, value); }
        }
        #endregion

        #region CaseHeight
        /// <summary>
        /// CaseHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> CaseHeightProperty =
            RegisterModelProperty<Single>(c => c.CaseHeight, Message.PlanogramProduct_CaseHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the case height
        /// </summary>
        public Single CaseHeight
        {
            get { return this.ReadProperty<Single>(CaseHeightProperty); }
            set { this.SetProperty<Single>(CaseHeightProperty, value); }
        }
        #endregion

        #region CaseWidth
        /// <summary>
        /// CaseWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> CaseWidthProperty =
            RegisterModelProperty<Single>(c => c.CaseWidth, Message.PlanogramProduct_CaseWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the case width
        /// </summary>
        public Single CaseWidth
        {
            get { return this.ReadProperty<Single>(CaseWidthProperty); }
            set { this.SetProperty<Single>(CaseWidthProperty, value); }
        }
        #endregion

        #region CaseDepth
        /// <summary>
        /// CaseDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> CaseDepthProperty =
            RegisterModelProperty<Single>(c => c.CaseDepth, Message.PlanogramProduct_CaseDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the case depth
        /// </summary>
        public Single CaseDepth
        {
            get { return this.ReadProperty<Single>(CaseDepthProperty); }
            set { this.SetProperty<Single>(CaseDepthProperty, value); }
        }
        #endregion

        #region MaxStack
        /// <summary>
        /// MaxStack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MaxStackProperty =
            RegisterModelProperty<Byte>(c => c.MaxStack, Message.PlanogramProduct_MaxStack);
        /// <summary>
        /// Gets or sets the max stack
        /// </summary>
        public Byte MaxStack
        {
            get { return this.ReadProperty<Byte>(MaxStackProperty); }
            set { this.SetProperty<Byte>(MaxStackProperty, value); }
        }
        #endregion

        #region MaxTopCap
        /// <summary>
        /// MaxTopCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MaxTopCapProperty =
            RegisterModelProperty<Byte>(c => c.MaxTopCap, Message.PlanogramProduct_MaxTopCap);
        /// <summary>
        /// Gets or sets the max top cap
        /// </summary>
        public Byte MaxTopCap
        {
            get { return this.ReadProperty<Byte>(MaxTopCapProperty); }
            set { this.SetProperty<Byte>(MaxTopCapProperty, value); }
        }
        #endregion

        #region MaxRightCap
        /// <summary>
        /// MaxRightCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MaxRightCapProperty =
            RegisterModelProperty<Byte>(c => c.MaxRightCap, Message.PlanogramProduct_MaxRightCap);
        /// <summary>
        /// Gets or sets the max right cap
        /// </summary>
        public Byte MaxRightCap
        {
            get { return this.ReadProperty<Byte>(MaxRightCapProperty); }
            set { this.SetProperty<Byte>(MaxRightCapProperty, value); }
        }
        #endregion

        #region MinDeep
        /// <summary>
        /// MinDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MinDeepProperty =
            RegisterModelProperty<Byte>(c => c.MinDeep, Message.PlanogramProduct_MinDeep);
        /// <summary>
        /// Gets or sets the min deep property
        /// </summary>
        public Byte MinDeep
        {
            get { return this.ReadProperty<Byte>(MinDeepProperty); }
            set { this.SetProperty<Byte>(MinDeepProperty, value); }
        }
        #endregion

        #region MaxDeep
        /// <summary>
        /// MaxDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MaxDeepProperty =
            RegisterModelProperty<Byte>(c => c.MaxDeep, Message.PlanogramProduct_MaxDeep);
        /// <summary>
        /// Gets or sets the max deep
        /// </summary>
        public Byte MaxDeep
        {
            get { return this.ReadProperty<Byte>(MaxDeepProperty); }
            set { this.SetProperty<Byte>(MaxDeepProperty, value); }
        }
        #endregion

        #region TrayPackUnits
        /// <summary>
        /// TrayPackUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayPackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.TrayPackUnits, Message.PlanogramProduct_TrayPackUnits);
        /// <summary>
        /// Gets or sets the tray pack units
        /// </summary>
        public Int16 TrayPackUnits
        {
            get { return this.ReadProperty<Int16>(TrayPackUnitsProperty); }
            set { this.SetProperty<Int16>(TrayPackUnitsProperty, value); }
        }
        #endregion

        #region TrayHigh
        /// <summary>
        /// TrayHigh property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> TrayHighProperty =
            RegisterModelProperty<Byte>(c => c.TrayHigh, Message.PlanogramProduct_TrayHigh);
        /// <summary>
        /// The PlanogramProduct TrayHigh
        /// </summary>
        public Byte TrayHigh
        {
            get { return this.ReadProperty<Byte>(TrayHighProperty); }
            set { this.SetProperty<Byte>(TrayHighProperty, value); }
        }
        #endregion

        #region TrayWide
        /// <summary>
        /// TrayWide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> TrayWideProperty =
            RegisterModelProperty<Byte>(c => c.TrayWide, Message.PlanogramProduct_TrayWide);
        /// <summary>
        /// The PlanogramProduct TrayWide
        /// </summary>
        public Byte TrayWide
        {
            get { return this.ReadProperty<Byte>(TrayWideProperty); }
            set { this.SetProperty<Byte>(TrayWideProperty, value); }
        }
        #endregion

        #region TrayDeep
        /// <summary>
        /// TrayDeep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> TrayDeepProperty =
            RegisterModelProperty<Byte>(c => c.TrayDeep, Message.PlanogramProduct_TrayDeep);
        /// <summary>
        /// The PlanogramProduct TrayDeep
        /// </summary>
        public Byte TrayDeep
        {
            get { return this.ReadProperty<Byte>(TrayDeepProperty); }
            set { this.SetProperty<Byte>(TrayDeepProperty, value); }
        }
        #endregion

        #region TrayHeight
        /// <summary>
        /// TrayHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayHeightProperty =
            RegisterModelProperty<Single>(c => c.TrayHeight, Message.PlanogramProduct_TrayHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product TrayHeight
        /// </summary>
        public Single TrayHeight
        {
            get { return this.ReadProperty<Single>(TrayHeightProperty); }
            set { this.SetProperty<Single>(TrayHeightProperty, value); }
        }
        #endregion

        #region TrayWidth
        /// <summary>
        /// TrayWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayWidthProperty =
            RegisterModelProperty<Single>(c => c.TrayWidth, Message.PlanogramProduct_TrayWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product TrayWidth
        /// </summary>
        public Single TrayWidth
        {
            get { return this.ReadProperty<Single>(TrayWidthProperty); }
            set { this.SetProperty<Single>(TrayWidthProperty, value); }
        }
        #endregion

        #region TrayDepth
        /// <summary>
        /// TrayDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayDepthProperty =
            RegisterModelProperty<Single>(c => c.TrayDepth, Message.PlanogramProduct_TrayDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the tray depth
        /// </summary>
        public Single TrayDepth
        {
            get { return this.ReadProperty<Single>(TrayDepthProperty); }
            set { this.SetProperty<Single>(TrayDepthProperty, value); }
        }
        #endregion

        #region TrayThickHeight
        /// <summary>
        /// TrayThickHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickHeightProperty =
            RegisterModelProperty<Single>(c => c.TrayThickHeight, Message.PlanogramProduct_TrayThickHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct TrayThickHeight
        /// </summary>
        public Single TrayThickHeight
        {
            get { return this.ReadProperty<Single>(TrayThickHeightProperty); }
            set { this.SetProperty<Single>(TrayThickHeightProperty, value); }
        }
        #endregion

        #region TrayThickWidth
        /// <summary>
        /// TrayThickWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickWidthProperty =
            RegisterModelProperty<Single>(c => c.TrayThickWidth, Message.PlanogramProduct_TrayThickWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct TrayThickWidth
        /// </summary>
        public Single TrayThickWidth
        {
            get { return this.ReadProperty<Single>(TrayThickWidthProperty); }
            set { this.SetProperty<Single>(TrayThickWidthProperty, value); }
        }
        #endregion

        #region TrayThickDepth
        /// <summary>
        /// TrayThickDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickDepthProperty =
            RegisterModelProperty<Single>(c => c.TrayThickDepth, Message.PlanogramProduct_TrayThickDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct TrayThickDepth
        /// </summary>
        public Single TrayThickDepth
        {
            get { return this.ReadProperty<Single>(TrayThickDepthProperty); }
            set { this.SetProperty<Single>(TrayThickDepthProperty, value); }
        }
        #endregion

        #region FrontOverhang
        /// <summary>
        /// FrontOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FrontOverhangProperty =
            RegisterModelProperty<Single>(c => c.FrontOverhang, Message.PlanogramProduct_FrontOverhang, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct FrontOverhang
        /// </summary>
        public Single FrontOverhang
        {
            get { return this.ReadProperty<Single>(FrontOverhangProperty); }
            set { this.SetProperty<Single>(FrontOverhangProperty, value); }
        }
        #endregion

        #region FingetSpaceAbove
        /// <summary>
        /// FingerSpaceAbove property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FingerSpaceAboveProperty =
            RegisterModelProperty<Single>(c => c.FingerSpaceAbove, Message.PlanogramProduct_FingerSpaceAbove, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct FingerSpaceAbove
        /// </summary>
        public Single FingerSpaceAbove
        {
            get { return this.ReadProperty<Single>(FingerSpaceAboveProperty); }
            set { this.SetProperty<Single>(FingerSpaceAboveProperty, value); }
        }
        #endregion

        #region FingerSpaceToTheSide
        /// <summary>
        /// FingerSpaceToTheSide property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FingerSpaceToTheSideProperty =
            RegisterModelProperty<Single>(c => c.FingerSpaceToTheSide, Message.PlanogramProduct_FingerSpaceToTheSide, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramProduct FingerSpaceToTheSide
        /// </summary>
        public Single FingerSpaceToTheSide
        {
            get { return this.ReadProperty<Single>(FingerSpaceToTheSideProperty); }
            set { this.SetProperty<Single>(FingerSpaceToTheSideProperty, value); }
        }
        #endregion

        #region StatusType
        /// <summary>
        /// StatusType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductStatusType> StatusTypeProperty =
            RegisterModelProperty<PlanogramProductStatusType>(c => c.StatusType, Message.PlanogramProduct_StatusType);
        /// <summary>
        /// The PlanogramProduct StatusType
        /// </summary>
        public PlanogramProductStatusType StatusType
        {
            get { return this.ReadProperty<PlanogramProductStatusType>(StatusTypeProperty); }
            set { this.SetProperty<PlanogramProductStatusType>(StatusTypeProperty, value); }
        }
        #endregion

        #region OrientationType
        /// <summary>
        /// OrientationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductOrientationType> OrientationTypeProperty =
            RegisterModelProperty<PlanogramProductOrientationType>(c => c.OrientationType, Message.PlanogramProduct_OrientationType);
        /// <summary>
        /// The PlanogramProduct OrientationType
        /// </summary>
        public PlanogramProductOrientationType OrientationType
        {
            get { return this.ReadProperty<PlanogramProductOrientationType>(OrientationTypeProperty); }
            set { this.SetProperty<PlanogramProductOrientationType>(OrientationTypeProperty, value); }
        }
        #endregion

        #region MerchandisingStyle
        /// <summary>
        /// MerchandisingStyle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductMerchandisingStyle> MerchandisingStyleProperty =
            RegisterModelProperty<PlanogramProductMerchandisingStyle>(c => c.MerchandisingStyle, Message.PlanogramProduct_MerchandisingStyle);
        /// <summary>
        /// The PlanogramProduct MerchandisingStyle
        /// </summary>
        public PlanogramProductMerchandisingStyle MerchandisingStyle
        {
            get { return this.ReadProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty); }
            set { this.SetProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty, value); }
        }
        #endregion

        #region IsFrontOnly
        /// <summary>
        /// IsFrontOnly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsFrontOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsFrontOnly, Message.PlanogramProduct_IsFrontOnly);
        /// <summary>
        /// The PlanogramProduct IsFrontOnly
        /// </summary>
        public Boolean IsFrontOnly
        {
            get { return this.ReadProperty<Boolean>(IsFrontOnlyProperty); }
            set { this.SetProperty<Boolean>(IsFrontOnlyProperty, value); }
        }
        #endregion

        

        #region IsPlaceHolderProduct
        /// <summary>
        /// IsPlaceHolderProduct property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPlaceHolderProductProperty =
            RegisterModelProperty<Boolean>(c => c.IsPlaceHolderProduct, Message.PlanogramProduct_IsPlaceHolderProduct);
        /// <summary>
        /// The PlanogramProduct IsPlaceHolderProduct
        /// </summary>
        public Boolean IsPlaceHolderProduct
        {
            get { return this.ReadProperty<Boolean>(IsPlaceHolderProductProperty); }
            set { this.SetProperty<Boolean>(IsPlaceHolderProductProperty, value); }
        }
        #endregion

        #region IsActive
        /// <summary>
        /// IsActive property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsActiveProperty =
            RegisterModelProperty<Boolean>(c => c.IsActive, Message.PlanogramProduct_IsActive);
        /// <summary>
        /// The PlanogramProduct IsActive
        /// </summary>
        public Boolean IsActive
        {
            get { return this.ReadProperty<Boolean>(IsActiveProperty); }
            set { this.SetProperty<Boolean>(IsActiveProperty, value); }
        }
        #endregion

        #region CanBreakTrayUp
        /// <summary>
        /// CanBreakTrayUp property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayUpProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayUp, Message.PlanogramProduct_CanBreakTrayUp);
        /// <summary>
        ///     Whether this instance, when placed on a tray, can hace side unit caps added to it,
        /// </summary>
        public Boolean CanBreakTrayUp
        {
            get { return this.ReadProperty<Boolean>(CanBreakTrayUpProperty); }
            set { this.SetProperty<Boolean>(CanBreakTrayUpProperty, value); }
        }
        #endregion

        #region CanBreakTrayDown
        /// <summary>
        /// CanBreakTrayDown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayDownProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayDown, Message.PlanogramProduct_CanBreakTrayDown);
        /// <summary>
        ///     Whether this instance, when placed on a tray, can have less units than a tray's worth placed down as units.
        /// </summary>
        public Boolean CanBreakTrayDown
        {
            get { return this.ReadProperty<Boolean>(CanBreakTrayDownProperty); }
            set { this.SetProperty<Boolean>(CanBreakTrayDownProperty, value); }
        }
        #endregion

        #region CanBreakTrayBack
        /// <summary>
        /// CanBreakTrayBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayBackProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayBack, Message.PlanogramProduct_CanBreakTrayBack);
        /// <summary>
        ///     Whether this instance, when placed on a tray, can have unit caps placed behind it.
        /// </summary>
        public Boolean CanBreakTrayBack
        {
            get { return this.ReadProperty<Boolean>(CanBreakTrayBackProperty); }
            set { this.SetProperty<Boolean>(CanBreakTrayBackProperty, value); }
        }
        #endregion

        #region CanBreakTrayTop
        /// <summary>
        /// CanBreakTrayTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayTopProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayTop, Message.PlanogramProduct_CanBreakTrayTop);
        /// <summary>
        ///     Whether this instance, when placed on a tray, can have unit caps placed on top.
        /// </summary>
        public Boolean CanBreakTrayTop
        {
            get { return this.ReadProperty<Boolean>(CanBreakTrayTopProperty); }
            set { this.SetProperty<Boolean>(CanBreakTrayTopProperty, value); }
        }
        #endregion

        #region ForceMiddleCap
        /// <summary>
        /// ForceMiddleCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ForceMiddleCapProperty =
            RegisterModelProperty<Boolean>(c => c.ForceMiddleCap, Message.PlanogramProduct_ForceMiddleCap);
        /// <summary>
        /// The PlanogramProduct ForceMiddleCap
        /// </summary>
        public Boolean ForceMiddleCap
        {
            get { return this.ReadProperty<Boolean>(ForceMiddleCapProperty); }
            set { this.SetProperty<Boolean>(ForceMiddleCapProperty, value); }
        }
        #endregion

        #region ForceBottomCap
        /// <summary>
        /// ForceBottomCap property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ForceBottomCapProperty =
            RegisterModelProperty<Boolean>(c => c.ForceBottomCap, Message.PlanogramProduct_ForceBottomCap);
        /// <summary>
        /// The PlanogramProduct ForceBottomCap
        /// </summary>
        public Boolean ForceBottomCap
        {
            get { return this.ReadProperty<Boolean>(ForceBottomCapProperty); }
            set { this.SetProperty<Boolean>(ForceBottomCapProperty, value); }
        }
        #endregion

        #region PlanogramImageIdFront
        /// <summary>
        /// PlanogramImageIdFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdFront);
        /// <summary>
        /// The id of the front product image.
        /// </summary>
        public Object PlanogramImageIdFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdBack
        /// <summary>
        /// PlanogramImageIdBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdBack);
        /// <summary>
        /// The id of the back product image.
        /// </summary>
        public Object PlanogramImageIdBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTop
        /// <summary>
        /// PlanogramImageIdTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTop);
        /// <summary>
        /// The id of the top product image.
        /// </summary>
        public Object PlanogramImageIdTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdBottom
        /// <summary>
        /// PlanogramIdBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdBottom);
        /// <summary>
        /// The id of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdLeft
        /// <summary>
        /// PlanogramImageIdLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdLeft);
        /// <summary>
        /// The id of the left product image.
        /// </summary>
        public Object PlanogramImageIdLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdRight
        /// <summary>
        /// PlanogramImageIdRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdRight);
        /// <summary>
        /// The id of the right product image.
        /// </summary>
        public Object PlanogramImageIdRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdRightProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayFront
        /// <summary>
        /// PlanogramImageIdDisplayFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayFront);
        /// <summary>
        /// The IdDisplay of the front product image.
        /// </summary>
        public Object PlanogramImageIdDisplayFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayBack
        /// <summary>
        /// PlanogramImageIdDisplayBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayBack);
        /// <summary>
        /// The IdDisplay of the back product image.
        /// </summary>
        public Object PlanogramImageIdDisplayBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayTop
        /// <summary>
        /// PlanogramImageIdDisplayTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayTop);
        /// <summary>
        /// The IdDisplay of the top product image.
        /// </summary>
        public Object PlanogramImageIdDisplayTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayBottom
        /// <summary>
        /// PlanogramIdDisplayBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayBottom);
        /// <summary>
        /// The IdDisplay of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdDisplayBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayLeft
        /// <summary>
        /// PlanogramImageIdDisplayLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayLeft);
        /// <summary>
        /// The IdDisplay of the left product image.
        /// </summary>
        public Object PlanogramImageIdDisplayLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdDisplayRight
        /// <summary>
        /// PlanogramImageIdDisplayRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdDisplayRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdDisplayRight);
        /// <summary>
        /// The IdDisplay of the right product image.
        /// </summary>
        public Object PlanogramImageIdDisplayRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdDisplayRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdDisplayRightProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayFront
        /// <summary>
        /// PlanogramImageIdTrayFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayFront);
        /// <summary>
        /// The IdTray of the front product image.
        /// </summary>
        public Object PlanogramImageIdTrayFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayBack
        /// <summary>
        /// PlanogramImageIdTrayBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayBack);
        /// <summary>
        /// The IdTray of the back product image.
        /// </summary>
        public Object PlanogramImageIdTrayBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayTop
        /// <summary>
        /// PlanogramImageIdTrayTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayTop);
        /// <summary>
        /// The IdTray of the top product image.
        /// </summary>
        public Object PlanogramImageIdTrayTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayBottom
        /// <summary>
        /// PlanogramIdTrayBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayBottom);
        /// <summary>
        /// The IdTray of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdTrayBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayLeft
        /// <summary>
        /// PlanogramImageIdTrayLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayLeft);
        /// <summary>
        /// The IdTray of the left product image.
        /// </summary>
        public Object PlanogramImageIdTrayLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdTrayRight
        /// <summary>
        /// PlanogramImageIdTrayRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdTrayRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdTrayRight);
        /// <summary>
        /// The IdTray of the right product image.
        /// </summary>
        public Object PlanogramImageIdTrayRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdTrayRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdTrayRightProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseFront
        /// <summary>
        /// PlanogramImageIdPointOfPurchaseFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseFront);
        /// <summary>
        /// The IdPointOfPurchase of the front product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseBack
        /// <summary>
        /// PlanogramImageIdPointOfPurchaseBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseBack);
        /// <summary>
        /// The IdPointOfPurchase of the back product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseTop
        /// <summary>
        /// PlanogramImageIdPointOfPurchaseTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseTop);
        /// <summary>
        /// The IdPointOfPurchase of the top product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseBottom
        /// <summary>
        /// PlanogramIdPointOfPurchaseBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseBottom);
        /// <summary>
        /// The IdPointOfPurchase of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseLeft
        /// <summary>
        /// PlanogramImageIdPointOfPurchaseLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseLeft);
        /// <summary>
        /// The IdPointOfPurchase of the left product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdPointOfPurchaseRight
        /// <summary>
        /// PlanogramImageIdPointOfPurchaseRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdPointOfPurchaseRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdPointOfPurchaseRight);
        /// <summary>
        /// The IdPointOfPurchase of the right product image.
        /// </summary>
        public Object PlanogramImageIdPointOfPurchaseRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateFront
        /// <summary>
        /// PlanogramImageIdAlternateFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateFront);
        /// <summary>
        /// The IdAlternate of the front product image.
        /// </summary>
        public Object PlanogramImageIdAlternateFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateBack
        /// <summary>
        /// PlanogramImageIdAlternateBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateBack);
        /// <summary>
        /// The IdAlternate of the back product image.
        /// </summary>
        public Object PlanogramImageIdAlternateBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateTop
        /// <summary>
        /// PlanogramImageIdAlternateTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateTop);
        /// <summary>
        /// The IdAlternate of the top product image.
        /// </summary>
        public Object PlanogramImageIdAlternateTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateBottom
        /// <summary>
        /// PlanogramIdAlternateBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateBottom);
        /// <summary>
        /// The IdAlternate of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdAlternateBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateLeft
        /// <summary>
        /// PlanogramImageIdAlternateLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateLeft);
        /// <summary>
        /// The IdAlternate of the left product image.
        /// </summary>
        public Object PlanogramImageIdAlternateLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdAlternateRight
        /// <summary>
        /// PlanogramImageIdAlternateRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdAlternateRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdAlternateRight);
        /// <summary>
        /// The IdAlternate of the right product image.
        /// </summary>
        public Object PlanogramImageIdAlternateRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdAlternateRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdAlternateRightProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseFront
        /// <summary>
        /// PlanogramImageIdCaseFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseFrontProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseFront);
        /// <summary>
        /// The IdCase of the front product image.
        /// </summary>
        public Object PlanogramImageIdCaseFront
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseFrontProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseFrontProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseBack
        /// <summary>
        /// PlanogramImageIdCaseBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseBackProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseBack);
        /// <summary>
        /// The IdCase of the back product image.
        /// </summary>
        public Object PlanogramImageIdCaseBack
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseBackProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseBackProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseTop
        /// <summary>
        /// PlanogramImageIdCaseTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseTopProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseTop);
        /// <summary>
        /// The IdCase of the top product image.
        /// </summary>
        public Object PlanogramImageIdCaseTop
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseTopProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseTopProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseBottom
        /// <summary>
        /// PlanogramIdCaseBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseBottomProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseBottom);
        /// <summary>
        /// The IdCase of the bottom product image.
        /// </summary>
        public Object PlanogramImageIdCaseBottom
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseBottomProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseBottomProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseLeft
        /// <summary>
        /// PlanogramImageIdCaseLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseLeftProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseLeft);
        /// <summary>
        /// The IdCase of the left product image.
        /// </summary>
        public Object PlanogramImageIdCaseLeft
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseLeftProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseLeftProperty, value); }
        }
        #endregion

        #region PlanogramImageIdCaseRight
        /// <summary>
        /// PlanogramImageIdCaseRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramImageIdCaseRightProperty =
            RegisterModelProperty<Object>(c => c.PlanogramImageIdCaseRight);
        /// <summary>
        /// The IdCase of the right product image.
        /// </summary>
        public Object PlanogramImageIdCaseRight
        {
            get { return this.ReadProperty<Object>(PlanogramImageIdCaseRightProperty); }
            set { this.SetProperty<Object>(PlanogramImageIdCaseRightProperty, value); }
        }
        #endregion

        #region ShapeType
        /// <summary>
        /// ShapeType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductShapeType> ShapeTypeProperty =
            RegisterModelProperty<PlanogramProductShapeType>(c => c.ShapeType, Message.PlanogramProduct_ShapeType);
        /// <summary>
        /// Specifies the shape of the product, default is box.
        /// </summary>
        /// <remarks>
        /// Not to be confused with the Shape property, which is simply a string description
        /// of the product's shape.
        /// </remarks>
        public PlanogramProductShapeType ShapeType
        {
            get { return ReadProperty<PlanogramProductShapeType>(ShapeTypeProperty); }
            set { SetProperty<PlanogramProductShapeType>(ShapeTypeProperty, value); }
        }
        #endregion

        #region FillPatternType
        /// <summary>
        /// FillPatternType property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductFillPatternType> FillPatternTypeProperty =
            RegisterModelProperty<PlanogramProductFillPatternType>(c => c.FillPatternType, Message.PlanogramProduct_FillPatternType);
        /// <summary>
        /// The fill pattern to be used when colouring the product. Solid is the default.
        /// </summary>
        public PlanogramProductFillPatternType FillPatternType
        {
            get { return ReadProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty); }
            set { SetProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty, value); }
        }
        #endregion

        #region FillColour
        /// <summary>
        /// FillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourProperty =
            RegisterModelProperty<Int32>(c => c.FillColour, Message.PlanogramProduct_FillColour,
            ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The display colour of the product. Note that this is completely separate to the position blocking colour.
        /// </summary>
        public Int32 FillColour
        {
            get { return ReadProperty<Int32>(FillColourProperty); }
            set
            {
                SetProperty<Int32>(FillColourProperty, value);
                SetProperty<String>(ColourGroupValueProperty, null);
            }
        }
        #endregion

        #region PointOfPurchaseDescription
        /// <summary>
        /// PointOfPurchaseDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PointOfPurchaseDescriptionProperty =
            RegisterModelProperty<String>(c => c.PointOfPurchaseDescription, Message.PlanogramProduct_PointOfPurchaseDescription);
        /// <summary>
        /// Point Of Purchase Description
        ///</summary>
        public String PointOfPurchaseDescription
        {
            get { return ReadProperty<String>(PointOfPurchaseDescriptionProperty); }
            set { SetProperty<String>(PointOfPurchaseDescriptionProperty, value); }
        }
        #endregion

        #region ShortDescription
        /// <summary>
        /// ShortDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ShortDescriptionProperty =
            RegisterModelProperty<String>(c => c.ShortDescription, Message.PlanogramProduct_ShortDescription);
        /// <summary>
        /// Short Description
        ///</summary>
        public String ShortDescription
        {
            get { return ReadProperty<String>(ShortDescriptionProperty); }
            set { SetProperty<String>(ShortDescriptionProperty, value); }
        }
        #endregion

        #region Subcategory
        /// <summary>
        /// Subcategory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SubcategoryProperty =
            RegisterModelProperty<String>(c => c.Subcategory, Message.PlanogramProduct_Subcategory);
        /// <summary>
        /// Subcategory
        ///</summary>
        public String Subcategory
        {
            get { return ReadProperty<String>(SubcategoryProperty); }
            set { SetProperty<String>(SubcategoryProperty, value); }
        }
        #endregion

        #region CustomerStatus
        /// <summary>
        /// CustomerStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomerStatusProperty =
            RegisterModelProperty<String>(c => c.CustomerStatus, Message.PlanogramProduct_CustomerStatus);
        /// <summary>
        /// Customer Status
        ///</summary>
        public String CustomerStatus
        {
            get { return ReadProperty<String>(CustomerStatusProperty); }
            set { SetProperty<String>(CustomerStatusProperty, value); }
        }
        #endregion

        #region Colour
        /// <summary>
        /// Colour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ColourProperty =
            RegisterModelProperty<String>(c => c.Colour, Message.PlanogramProduct_Colour);
        /// <summary>
        /// Colour
        ///</summary>
        public String Colour
        {
            get { return ReadProperty<String>(ColourProperty); }
            set { SetProperty<String>(ColourProperty, value); }
        }
        #endregion

        #region Flavour
        /// <summary>
        /// Flavour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FlavourProperty =
            RegisterModelProperty<String>(c => c.Flavour, Message.PlanogramProduct_Flavour);
        /// <summary>
        /// Flavour
        ///</summary>
        public String Flavour
        {
            get { return ReadProperty<String>(FlavourProperty); }
            set { SetProperty<String>(FlavourProperty, value); }
        }
        #endregion

        #region PackagingShape
        /// <summary>
        /// PackagingShape property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PackagingShapeProperty =
            RegisterModelProperty<String>(c => c.PackagingShape, Message.PlanogramProduct_PackagingShape);
        /// <summary>
        /// Packaging Shape
        ///</summary>
        public String PackagingShape
        {
            get { return ReadProperty<String>(PackagingShapeProperty); }
            set { SetProperty<String>(PackagingShapeProperty, value); }
        }
        #endregion

        #region PackagingType
        /// <summary>
        /// PackagingType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PackagingTypeProperty =
            RegisterModelProperty<String>(c => c.PackagingType, Message.PlanogramProduct_PackagingType);
        /// <summary>
        /// Packaging Type
        ///</summary>
        public String PackagingType
        {
            get { return ReadProperty<String>(PackagingTypeProperty); }
            set { SetProperty<String>(PackagingTypeProperty, value); }
        }
        #endregion

        #region CountryOfOrigin
        /// <summary>
        /// CountryOfOrigin property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountryOfOriginProperty =
            RegisterModelProperty<String>(c => c.CountryOfOrigin, Message.PlanogramProduct_CountryOfOrigin);
        /// <summary>
        /// Country Of Origin
        ///</summary>
        public String CountryOfOrigin
        {
            get { return ReadProperty<String>(CountryOfOriginProperty); }
            set { SetProperty<String>(CountryOfOriginProperty, value); }
        }
        #endregion

        #region CountryOfProcessing
        /// <summary>
        /// CountryOfProcessing property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountryOfProcessingProperty =
            RegisterModelProperty<String>(c => c.CountryOfProcessing, Message.PlanogramProduct_CountryOfProcessing);
        /// <summary>
        /// Country Of Processing
        ///</summary>
        public String CountryOfProcessing
        {
            get { return ReadProperty<String>(CountryOfProcessingProperty); }
            set { SetProperty<String>(CountryOfProcessingProperty, value); }
        }
        #endregion

        #region ShelfLife
        /// <summary>
        /// ShelfLife property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ShelfLifeProperty =
            RegisterModelProperty<Int16>(c => c.ShelfLife, Message.PlanogramProduct_ShelfLife);
        /// <summary>
        /// Shelf Life
        ///</summary>
        public Int16 ShelfLife
        {
            get { return ReadProperty<Int16>(ShelfLifeProperty); }
            set
            {
                SetProperty<Int16>(ShelfLifeProperty, value);

                //tell any linked performance data to update.
                PlanogramPerformanceData perfData = GetPlanogramPerformanceData();
                if (perfData != null) perfData.UpdateInventoryValues();
            }
        }
        #endregion

        #region DeliveryFrequencyDays
        /// <summary>
        /// DeliveryFrequencyDays property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> DeliveryFrequencyDaysProperty =
            RegisterModelProperty<Single?>(c => c.DeliveryFrequencyDays, Message.PlanogramProduct_DeliveryFrequencyDays);
        /// <summary>
        /// Delivery Frequency Days
        ///</summary>
        public Single? DeliveryFrequencyDays
        {
            get { return ReadProperty<Single?>(DeliveryFrequencyDaysProperty); }
            set
            {
                SetProperty<Single?>(DeliveryFrequencyDaysProperty, value);

                //tell any linked performance data to update.
                PlanogramPerformanceData perfData = GetPlanogramPerformanceData();
                if (perfData != null) perfData.UpdateInventoryValues();
            }
        }
        #endregion

        #region DeliveryMethod
        /// <summary>
        /// DeliveryMethod property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DeliveryMethodProperty =
            RegisterModelProperty<String>(c => c.DeliveryMethod, Message.PlanogramProduct_DeliveryMethod);
        /// <summary>
        /// Delivery Method
        ///</summary>
        public String DeliveryMethod
        {
            get { return ReadProperty<String>(DeliveryMethodProperty); }
            set { SetProperty<String>(DeliveryMethodProperty, value); }
        }
        #endregion

        #region VendorCode
        /// <summary>
        /// VendorCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> VendorCodeProperty =
            RegisterModelProperty<String>(c => c.VendorCode, Message.PlanogramProduct_VendorCode);
        /// <summary>
        /// Vendor Code
        ///</summary>
        public String VendorCode
        {
            get { return ReadProperty<String>(VendorCodeProperty); }
            set { SetProperty<String>(VendorCodeProperty, value); }
        }
        #endregion

        #region Vendor
        /// <summary>
        /// Vendor property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> VendorProperty =
            RegisterModelProperty<String>(c => c.Vendor, Message.PlanogramProduct_Vendor);
        /// <summary>
        /// Vendor
        ///</summary>
        public String Vendor
        {
            get { return ReadProperty<String>(VendorProperty); }
            set { SetProperty<String>(VendorProperty, value); }
        }
        #endregion

        #region ManufacturerCode
        /// <summary>
        /// ManufacturerCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerCodeProperty =
            RegisterModelProperty<String>(c => c.ManufacturerCode, Message.PlanogramProduct_ManufacturerCode);
        /// <summary>
        /// Manufacturer Code
        ///</summary>
        public String ManufacturerCode
        {
            get { return ReadProperty<String>(ManufacturerCodeProperty); }
            set { SetProperty<String>(ManufacturerCodeProperty, value); }
        }
        #endregion

        #region Manufacturer
        /// <summary>
        /// Manufacturer property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
            RegisterModelProperty<String>(c => c.Manufacturer, Message.PlanogramProduct_Manufacturer);
        /// <summary>
        /// Manufacturer
        ///</summary>
        public String Manufacturer
        {
            get { return ReadProperty<String>(ManufacturerProperty); }
            set { SetProperty<String>(ManufacturerProperty, value); }
        }
        #endregion

        #region Size
        /// <summary>
        /// Size property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SizeProperty =
            RegisterModelProperty<String>(c => c.Size, Message.PlanogramProduct_Size);
        /// <summary>
        /// Size
        ///</summary>
        public String Size
        {
            get { return ReadProperty<String>(SizeProperty); }
            set { SetProperty<String>(SizeProperty, value); }
        }
        #endregion

        #region UnitOfMeasure
        /// <summary>
        /// UnitOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UnitOfMeasureProperty =
            RegisterModelProperty<String>(c => c.UnitOfMeasure, Message.PlanogramProduct_UnitOfMeasure);
        /// <summary>
        /// Unit Of Measure
        ///</summary>
        public String UnitOfMeasure
        {
            get { return ReadProperty<String>(UnitOfMeasureProperty); }
            set { SetProperty<String>(UnitOfMeasureProperty, value); }
        }
        #endregion

        #region DateIntroduced
        /// <summary>
        /// DateIntroduced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateIntroducedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateIntroduced, Message.PlanogramProduct_DateIntroduced);
        /// <summary>
        /// Date Introduced
        ///</summary>
        public DateTime? DateIntroduced
        {
            get { return ReadProperty<DateTime?>(DateIntroducedProperty); }
            set { SetProperty<DateTime?>(DateIntroducedProperty, value); }
        }
        #endregion

        #region DateDiscontinued
        /// <summary>
        /// DateDiscontinued property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDiscontinuedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDiscontinued, Message.PlanogramProduct_DateDiscontinued);
        /// <summary>
        /// Date Discontinued
        ///</summary>
        public DateTime? DateDiscontinued
        {
            get { return ReadProperty<DateTime?>(DateDiscontinuedProperty); }
            set { SetProperty<DateTime?>(DateDiscontinuedProperty, value); }
        }
        #endregion

        #region DateEffective
        /// <summary>
        /// DateEffective property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateEffectiveProperty =
            RegisterModelProperty<DateTime?>(c => c.DateEffective, Message.PlanogramProduct_DateEffective);
        /// <summary>
        /// Date Effective
        ///</summary>
        public DateTime? DateEffective
        {
            get { return ReadProperty<DateTime?>(DateEffectiveProperty); }
            set { SetProperty<DateTime?>(DateEffectiveProperty, value); }
        }
        #endregion

        #region Health
        /// <summary>
        /// Health property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HealthProperty =
            RegisterModelProperty<String>(c => c.Health, Message.PlanogramProduct_Health);
        /// <summary>
        /// Health
        ///</summary>
        public String Health
        {
            get { return ReadProperty<String>(HealthProperty); }
            set { SetProperty<String>(HealthProperty, value); }
        }
        #endregion

        #region CorporateCode
        /// <summary>
        /// CorporateCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CorporateCodeProperty =
            RegisterModelProperty<String>(c => c.CorporateCode, Message.PlanogramProduct_CorporateCode);
        /// <summary>
        /// Corporate Code
        ///</summary>
        public String CorporateCode
        {
            get { return ReadProperty<String>(CorporateCodeProperty); }
            set { SetProperty<String>(CorporateCodeProperty, value); }
        }
        #endregion

        #region Barcode
        /// <summary>
        /// Barcode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BarcodeProperty =
            RegisterModelProperty<String>(c => c.Barcode, Message.PlanogramProduct_Barcode);
        /// <summary>
        /// Barcode
        ///</summary>
        public String Barcode
        {
            get { return ReadProperty<String>(BarcodeProperty); }
            set { SetProperty<String>(BarcodeProperty, value); }
        }
        #endregion

        #region SellPrice
        /// <summary>
        /// SellPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SellPriceProperty =
            RegisterModelProperty<Single?>(c => c.SellPrice, Message.PlanogramProduct_SellPrice, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Sell Price
        ///</summary>
        public Single? SellPrice
        {
            get { return ReadProperty<Single?>(SellPriceProperty); }
            set { SetProperty<Single?>(SellPriceProperty, value); }
        }
        #endregion

        #region SellPackCount
        /// <summary>
        /// SellPackCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> SellPackCountProperty =
            RegisterModelProperty<Int16?>(c => c.SellPackCount, Message.PlanogramProduct_SellPackCount);
        /// <summary>
        /// Sell Pack Count
        ///</summary>
        public Int16? SellPackCount
        {
            get { return ReadProperty<Int16?>(SellPackCountProperty); }
            set { SetProperty<Int16?>(SellPackCountProperty, value); }
        }
        #endregion

        #region SellPackDescription
        /// <summary>
        /// SellPackDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SellPackDescriptionProperty =
            RegisterModelProperty<String>(c => c.SellPackDescription, Message.PlanogramProduct_SellPackDescription);
        /// <summary>
        /// Sell Pack Description
        ///</summary>
        public String SellPackDescription
        {
            get { return ReadProperty<String>(SellPackDescriptionProperty); }
            set { SetProperty<String>(SellPackDescriptionProperty, value); }
        }
        #endregion

        #region RecommendedRetailPrice
        /// <summary>
        /// RecommendedRetailPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> RecommendedRetailPriceProperty =
            RegisterModelProperty<Single?>(c => c.RecommendedRetailPrice, Message.PlanogramProduct_RecommendedRetailPrice, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Recommended Retail Price
        ///</summary>
        public Single? RecommendedRetailPrice
        {
            get { return ReadProperty<Single?>(RecommendedRetailPriceProperty); }
            set { SetProperty<Single?>(RecommendedRetailPriceProperty, value); }
        }
        #endregion

        #region ManufacturerRecommendedRetailPrice
        /// <summary>
        /// ManufacturerRecommendedRetailPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> ManufacturerRecommendedRetailPriceProperty =
            RegisterModelProperty<Single?>(c => c.ManufacturerRecommendedRetailPrice, Message.PlanogramProduct_ManufacturerRecommendedRetailPrice, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Manufacturer Recommended Retail Price
        ///</summary>
        public Single? ManufacturerRecommendedRetailPrice
        {
            get { return ReadProperty<Single?>(ManufacturerRecommendedRetailPriceProperty); }
            set { SetProperty<Single?>(ManufacturerRecommendedRetailPriceProperty, value); }
        }
        #endregion

        #region CostPrice
        /// <summary>
        /// CostPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CostPriceProperty =
            RegisterModelProperty<Single?>(c => c.CostPrice, Message.PlanogramProduct_CostPrice, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Cost Price
        ///</summary>
        public Single? CostPrice
        {
            get { return ReadProperty<Single?>(CostPriceProperty); }
            set { SetProperty<Single?>(CostPriceProperty, value); }
        }
        #endregion

        #region CaseCost
        /// <summary>
        /// CaseCost property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> CaseCostProperty =
            RegisterModelProperty<Single?>(c => c.CaseCost, Message.PlanogramProduct_CaseCost, ModelPropertyDisplayType.Currency);
        /// <summary>
        /// Case Cost
        ///</summary>
        public Single? CaseCost
        {
            get { return ReadProperty<Single?>(CaseCostProperty); }
            set { SetProperty<Single?>(CaseCostProperty, value); }
        }
        #endregion

        #region TaxRate
        /// <summary>
        /// TaxRate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> TaxRateProperty =
            RegisterModelProperty<Single?>(c => c.TaxRate, Message.PlanogramProduct_TaxRate, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Tax Rate
        ///</summary>
        public Single? TaxRate
        {
            get { return ReadProperty<Single?>(TaxRateProperty); }
            set { SetProperty<Single?>(TaxRateProperty, value); }
        }
        #endregion

        #region ConsumerInformation
        /// <summary>
        /// ConsumerInformation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ConsumerInformationProperty =
            RegisterModelProperty<String>(c => c.ConsumerInformation, Message.PlanogramProduct_ConsumerInformation);
        /// <summary>
        /// Consumer Information
        ///</summary>
        public String ConsumerInformation
        {
            get { return ReadProperty<String>(ConsumerInformationProperty); }
            set { SetProperty<String>(ConsumerInformationProperty, value); }
        }
        #endregion

        #region Texture
        /// <summary>
        /// Texture property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextureProperty =
            RegisterModelProperty<String>(c => c.Texture, Message.PlanogramProduct_Texture);
        /// <summary>
        /// Texture
        ///</summary>
        public String Texture
        {
            get { return ReadProperty<String>(TextureProperty); }
            set { SetProperty<String>(TextureProperty, value); }
        }
        #endregion

        #region StyleNumber
        /// <summary>
        /// StyleNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> StyleNumberProperty =
            RegisterModelProperty<Int16?>(c => c.StyleNumber, Message.PlanogramProduct_StyleNumber);
        /// <summary>
        /// Style Number
        ///</summary>
        public Int16? StyleNumber
        {
            get { return ReadProperty<Int16?>(StyleNumberProperty); }
            set { SetProperty<Int16?>(StyleNumberProperty, value); }
        }
        #endregion

        #region Pattern
        /// <summary>
        /// Pattern property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PatternProperty =
            RegisterModelProperty<String>(c => c.Pattern, Message.PlanogramProduct_Pattern);
        /// <summary>
        /// Pattern
        ///</summary>
        public String Pattern
        {
            get { return ReadProperty<String>(PatternProperty); }
            set { SetProperty<String>(PatternProperty, value); }
        }
        #endregion

        #region Model
        /// <summary>
        /// Model property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ModelProperty =
            RegisterModelProperty<String>(c => c.Model, Message.PlanogramProduct_Model);
        /// <summary>
        /// Model
        ///</summary>
        public String Model
        {
            get { return ReadProperty<String>(ModelProperty); }
            set { SetProperty<String>(ModelProperty, value); }
        }
        #endregion

        #region GarmentType
        /// <summary>
        /// GarmentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> GarmentTypeProperty =
            RegisterModelProperty<String>(c => c.GarmentType, Message.PlanogramProduct_GarmentType);
        /// <summary>
        /// Garment Type
        ///</summary>
        public String GarmentType
        {
            get { return ReadProperty<String>(GarmentTypeProperty); }
            set { SetProperty<String>(GarmentTypeProperty, value); }
        }
        #endregion

        #region IsPrivateLabel
        /// <summary>
        /// IsPrivateLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPrivateLabelProperty =
            RegisterModelProperty<Boolean>(c => c.IsPrivateLabel, Message.PlanogramProduct_IsPrivateLabel);
        /// <summary>
        /// Is Private Label
        ///</summary>
        public Boolean IsPrivateLabel
        {
            get { return ReadProperty<Boolean>(IsPrivateLabelProperty); }
            set { SetProperty<Boolean>(IsPrivateLabelProperty, value); }
        }
        #endregion

        #region IsNewProduct
        /// <summary>
        /// IsNewProduct property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsNewProductProperty =
            RegisterModelProperty<Boolean>(c => c.IsNewProduct, Message.PlanogramProduct_IsNewProduct);
        /// <summary>
        /// Is New Product
        ///</summary>
        public Boolean IsNewProduct
        {
            get { return ReadProperty<Boolean>(IsNewProductProperty); }
            set { SetProperty<Boolean>(IsNewProductProperty, value); }
        }
        #endregion

        #region Shape
        /// <summary>
        /// Shape property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ShapeProperty =
            RegisterModelProperty<String>(c => c.Shape, Message.PlanogramProduct_Shape);
        /// <summary>
        /// String description of the Product's shape.
        /// </summary>
        /// <remarks>
        /// Not to be confused with the ShapeType property, which is an enum that describes
        /// how the product should be rendered in 3D.
        /// </remarks>
        public String Shape
        {
            get { return ReadProperty<String>(ShapeProperty); }
            set { SetProperty<String>(ShapeProperty, value); }
        }
        #endregion

        #region FinancialGroupCode

        /// <summary>
        ///FinancialGroupCode property description
        /// </summary>
        public static readonly ModelPropertyInfo<String> FinancialGroupCodeProperty =
            RegisterModelProperty<String>(c => c.FinancialGroupCode, Message.PlanogramProduct_FinancialGroupCode);

        /// <summary>
        /// Gets/Sets the code of the financial group that this product belongs to.
        /// </summary>
        public String FinancialGroupCode
        {
            get { return ReadProperty<String>(FinancialGroupCodeProperty); }
            set { SetProperty<String>(FinancialGroupCodeProperty, value); }
        }

        #endregion

        #region FinancialGroupName

        /// <summary>
        /// FinancialGroupName property description
        /// </summary>
        public static readonly ModelPropertyInfo<String> FinancialGroupNameProperty =
            RegisterModelProperty<String>(c => c.FinancialGroupName, Message.PlanogramProduct_FinancialGroupName);

        /// <summary>
        /// Gets/Sets the name of the financial group that this product belongs to.
        /// </summary>
        public String FinancialGroupName
        {
            get { return ReadProperty<String>(FinancialGroupNameProperty); }
            set { SetProperty<String>(FinancialGroupNameProperty, value); }
        }

        #endregion

        #region CustomAttributes

        /// <summary>
        /// CustomAttributes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomAttributeData> CustomAttributesProperty =
            RegisterModelProperty<CustomAttributeData>(c => c.CustomAttributes, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child custom attributes object for this plan.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get { return ReadProperty<CustomAttributeData>(CustomAttributesProperty); }
        }

        /// <summary>
        /// Gets the child custom attributes object for this plan.
        /// Explicit implementation of IPlanogramProduct member
        /// </summary>
        ICustomAttributeData IPlanogramProduct.CustomAttributes
        {
            get { return CustomAttributes; }
        }

        #endregion

        #region Meta Data Properties

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaNotAchievedInventory, Message.PlanogramProduct_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data achieved cases
        /// </summary>
        public Boolean? MetaNotAchievedInventory
        {
            get { return this.ReadProperty<Boolean?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Boolean?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalUnits

        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, Message.PlanogramProduct_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta field for the total
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.ReadProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }

        #endregion

        #region MetaPlanogramLinearSpacePercentage

        public static readonly ModelPropertyInfo<Single?> MetaPlanogramLinearSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramLinearSpacePercentage, Message.PlanogramProduct_MetaPlanogramLinearSpacePercentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the MetaPlanogramLinearSpacePercentage
        /// </summary>
        public Single? MetaPlanogramLinearSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty, value); }
        }

        #endregion

        #region MetaPlanogramAreaSpacePercentage

        public static readonly ModelPropertyInfo<Single?> MetaPlanogramAreaSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramAreaSpacePercentage, Message.PlanogramProduct_MetaPlanogramAreaSpacePercentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the MetaPlanogramAreaSpacePercentage
        /// </summary>
        public Single? MetaPlanogramAreaSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty, value); }
        }

        #endregion

        #region MetaPlanogramVolumetricSpacePercentage

        public static readonly ModelPropertyInfo<Single?> MetaPlanogramVolumetricSpacePercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaPlanogramVolumetricSpacePercentage, Message.PlanogramProduct_MetaPlanogramVolumetricSpacePercentage, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the MetaPlanogramVolumetricSpacePercentage
        /// </summary>
        public Single? MetaPlanogramVolumetricSpacePercentage
        {
            get { return this.ReadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty); }
            set { this.SetProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty, value); }
        }

        #endregion

        #region MetaIsInMasterData

        public static readonly ModelPropertyInfo<Boolean?> MetaIsInMasterDataProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsInMasterData, Message.PlanogramProduct_MetaIsInMasterData);
        /// <summary>
        /// Gets/Sets the MetaIsInMasterData
        /// </summary>
        public Boolean? MetaIsInMasterData
        {
            get { return this.ReadProperty<Boolean?>(MetaIsInMasterDataProperty); }
            set { this.SetProperty<Boolean?>(MetaIsInMasterDataProperty, value); }
        }

        #endregion

        #region MetaPositionCount property

        public static readonly ModelPropertyInfo<Int32?> MetaPositionCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaPositionCount, Message.PlanogramProduct_MetaPositionCount);
        /// <summary>
        /// Number of positions a product has on a planogram
        /// </summary>
        public Int32? MetaPositionCount
        {
            get { return this.ReadProperty<Int32?>(MetaPositionCountProperty); }
            set { this.SetProperty<Int32?>(MetaPositionCountProperty, value); }
        }

        #endregion

        #region MetaTotalFacings property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, Message.PlanogramProduct_MetaTotalFacings);
        /// <summary>
        /// 
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.ReadProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }

        #endregion

        #region MetaTotalLinearSpace property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearSpace, Message.PlanogramPosition_MetaTotalLinearSpace, ModelPropertyDisplayType.Length);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaTotalLinearSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearSpaceProperty, value); }
        }

        #endregion

        #region MetaTotalAreaSpace property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaSpace, Message.PlanogramPosition_MetaTotalAreaSpace, ModelPropertyDisplayType.Area);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaTotalAreaSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaSpaceProperty, value); }
        }

        #endregion

        #region MetaTotalVolumetricSpace property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricSpace, Message.PlanogramPosition_MetaTotalVolumetricSpace, ModelPropertyDisplayType.Volume);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaTotalVolumetricSpace
        {
            get { return this.ReadProperty<Single?>(MetaTotalVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricSpaceProperty, value); }
        }

        #endregion

        #region MetaNotAchievedCases
        public static readonly ModelPropertyInfo<Boolean?> MetaNotAchievedCasesProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaNotAchievedCases, Message.PlanogramProduct_MetaNotAchievedCases);
        /// <summary>
        /// Gets/Sets the MetaNotAchievedCases
        /// </summary>
        public Boolean? MetaNotAchievedCases
        {
            get { return this.ReadProperty<Boolean?>(MetaNotAchievedCasesProperty); }
            set { this.SetProperty<Boolean?>(MetaNotAchievedCasesProperty, value); }
        }
        #endregion

        #region MetaNotAchievedDOS
        public static readonly ModelPropertyInfo<Boolean?> MetaNotAchievedDOSProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaNotAchievedDOS, Message.PlanogramProduct_MetaNotAchievedDOS);
        /// <summary>
        /// Gets/Sets the MetaNotAchievedDOS
        /// </summary>
        public Boolean? MetaNotAchievedDOS
        {
            get { return this.ReadProperty<Boolean?>(MetaNotAchievedDOSProperty); }
            set { this.SetProperty<Boolean?>(MetaNotAchievedDOSProperty, value); }
        }
        #endregion

        #region MetaIsOverShelfLifePercent
        public static readonly ModelPropertyInfo<Boolean?> MetaIsOverShelfLifePercentProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsOverShelfLifePercent, Message.PlanogramProduct_MetaIsOverShelfLifePercent);
        /// <summary>
        /// Gets/Sets the MetaIsOverShelfLifePercent
        /// </summary>
        public Boolean? MetaIsOverShelfLifePercent
        {
            get { return this.ReadProperty<Boolean?>(MetaIsOverShelfLifePercentProperty); }
            set { this.SetProperty<Boolean?>(MetaIsOverShelfLifePercentProperty, value); }
        }
        #endregion

        #region MetaNotAchievedDeliveries
        public static readonly ModelPropertyInfo<Boolean?> MetaNotAchievedDeliveriesProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaNotAchievedDeliveries, Message.PlanogramProduct_MetaNotAchievedDeliveries);
        /// <summary>
        /// Gets/Sets the MetaNotAchievedDeliveries
        /// </summary>
        public Boolean? MetaNotAchievedDeliveries
        {
            get { return this.ReadProperty<Boolean?>(MetaNotAchievedDeliveriesProperty); }
            set { this.SetProperty<Boolean?>(MetaNotAchievedDeliveriesProperty, value); }
        }
        #endregion

        #region MetaTotalMainFacings
        public static readonly ModelPropertyInfo<Int32?> MetaTotalMainFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalMainFacings, Message.PlanogramProduct_MetaTotalMainFacings);
        /// <summary>
        /// Gets/Sets the MetaTotalMainFacings
        /// </summary>
        public Int32? MetaTotalMainFacings
        {
            get { return this.ReadProperty<Int32?>(MetaTotalMainFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalMainFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalXFacings
        public static readonly ModelPropertyInfo<Int32?> MetaTotalXFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalXFacings, Message.PlanogramProduct_MetaTotalXFacings);
        /// <summary>
        /// Gets/Sets the MetaTotalXFacings
        /// </summary>
        public Int32? MetaTotalXFacings
        {
            get { return this.ReadProperty<Int32?>(MetaTotalXFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalXFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalYFacings
        public static readonly ModelPropertyInfo<Int32?> MetaTotalYFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalYFacings, Message.PlanogramProduct_MetaTotalYFacings);
        /// <summary>
        /// Gets/Sets the MetaTotalYFacings
        /// </summary>
        public Int32? MetaTotalYFacings
        {
            get { return this.ReadProperty<Int32?>(MetaTotalYFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalYFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalZFacings
        public static readonly ModelPropertyInfo<Int32?> MetaTotalZFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalZFacings, Message.PlanogramProduct_MetaTotalZFacings);
        /// <summary>
        /// Gets/Sets the MetaTotalZFacings
        /// </summary>
        public Int32? MetaTotalZFacings
        {
            get { return this.ReadProperty<Int32?>(MetaTotalZFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalZFacingsProperty, value); }
        }
        #endregion

        #region MetaIsRangedInAssortment
        public static readonly ModelPropertyInfo<Boolean?> MetaIsRangedInAssortmentProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsRangedInAssortment, Message.PlanogramProduct_MetaIsRangedInAssortment);
        /// <summary>
        /// Gets/Sets the MetaIsRangedInAssortment
        /// </summary>
        public Boolean? MetaIsRangedInAssortment
        {
            get { return this.ReadProperty<Boolean?>(MetaIsRangedInAssortmentProperty); }
            set { this.SetProperty<Boolean?>(MetaIsRangedInAssortmentProperty, value); }
        }
        #endregion

        #region MetaCDTNode
        public static readonly ModelPropertyInfo<String> MetaCDTNodeProperty =
            RegisterModelProperty<String>(c => c.MetaCDTNode, Message.PlanogramProduct_MetaCDTNode);
        /// <summary>
        /// Gets/Sets the MetaCDTNode
        /// </summary>
        public String MetaCDTNode
        {
            get { return this.ReadProperty<String>(MetaCDTNodeProperty); }
            set { this.SetProperty<String>(MetaCDTNodeProperty, value); }
        }
        #endregion

        #region MetaIsProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsProductRuleBroken, Message.PlanogramProduct_MetaIsProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsProductRuleBroken
        /// </summary>
        public Boolean? MetaIsProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsFamilyRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsFamilyRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsFamilyRuleBroken, Message.PlanogramProduct_MetaIsFamilyRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsFamilyRuleBroken
        /// </summary>
        public Boolean? MetaIsFamilyRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsFamilyRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsFamilyRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsInheritanceRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsInheritanceRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsInheritanceRuleBroken, Message.PlanogramProduct_MetaIsInheritanceRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsInheritanceRuleBroken
        /// </summary>
        public Boolean? MetaIsInheritanceRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsInheritanceRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsInheritanceRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsLocalProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsLocalProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsLocalProductRuleBroken, Message.PlanogramProduct_MetaIsLocalProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsLocalProductRuleBroken
        /// </summary>
        public Boolean? MetaIsLocalProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsLocalProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsLocalProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsDistributionRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsDistributionRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsDistributionRuleBroken, Message.PlanogramProduct_MetaIsDistributionRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsDistributionRuleBroken
        /// </summary>
        public Boolean? MetaIsDistributionRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsDistributionRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsDistributionRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsCoreRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsCoreRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsCoreRuleBroken, Message.PlanogramProduct_MetaIsCoreRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsCoreRuleBroken
        /// </summary>
        public Boolean? MetaIsCoreRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsCoreRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsCoreRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsDelistProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsDelistProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsDelistProductRuleBroken, Message.PlanogramProduct_MetaIsDelistProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsDelistProductRuleBroken
        /// </summary>
        public Boolean? MetaIsDelistProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsDelistProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsDelistProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsForceProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsForceProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsForceProductRuleBroken, Message.PlanogramProduct_MetaIsForceProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsForceProductRuleBroken
        /// </summary>
        public Boolean? MetaIsForceProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsForceProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsForceProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsPreserveProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsPreserveProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsPreserveProductRuleBroken, Message.PlanogramProduct_MetaIsPreserveProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsPreserveProductRuleBroken
        /// </summary>
        public Boolean? MetaIsPreserveProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsPreserveProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsPreserveProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsMinimumHurdleProductRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsMinimumHurdleProductRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsMinimumHurdleProductRuleBroken, Message.PlanogramProduct_MetaIsMinimumHurdleProductRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsMinimumHurdleProductRuleBroken
        /// </summary>
        public Boolean? MetaIsMinimumHurdleProductRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsMinimumHurdleProductRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsMinimumHurdleProductRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsMaximumProductFamilyRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsMaximumProductFamilyRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsMaximumProductFamilyRuleBroken, Message.PlanogramProduct_MetaIsMaximumProductFamilyRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsMaximumProductFamilyRuleBroken
        /// </summary>
        public Boolean? MetaIsMaximumProductFamilyRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsMaximumProductFamilyRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsMaximumProductFamilyRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsMinimumProductFamilyRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsMinimumProductFamilyRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsMinimumProductFamilyRuleBroken, Message.PlanogramProduct_MetaIsMinimumProductFamilyRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsMinimumProductFamilyRuleBroken
        /// </summary>
        public Boolean? MetaIsMinimumProductFamilyRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsMinimumProductFamilyRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsMinimumProductFamilyRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsDependencyFamilyRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsDependencyFamilyRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsDependencyFamilyRuleBroken, Message.PlanogramProduct_MetaIsDependencyFamilyRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsDependencyFamilyRuleBroken
        /// </summary>
        public Boolean? MetaIsDependencyFamilyRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsDependencyFamilyRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsDependencyFamilyRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaIsDelistFamilyRuleBroken
        public static readonly ModelPropertyInfo<Boolean?> MetaIsDelistFamilyRuleBrokenProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsDelistFamilyRuleBroken, Message.PlanogramProduct_MetaIsDelistFamilyRuleBroken);
        /// <summary>
        /// Gets/Sets the MetaIsDelistFamilyRuleBroken
        /// </summary>
        public Boolean? MetaIsDelistFamilyRuleBroken
        {
            get { return this.ReadProperty<Boolean?>(MetaIsDelistFamilyRuleBrokenProperty); }
            set { this.SetProperty<Boolean?>(MetaIsDelistFamilyRuleBrokenProperty, value); }
        }
        #endregion

        #region MetaComparisonStatus

        private static readonly ModelPropertyInfo<PlanogramItemComparisonStatusType> MetaComparisonStatusProperty =
            RegisterModelProperty<PlanogramItemComparisonStatusType>(c => c.MetaComparisonStatus, Message.PlanogramProduct_MetaComparisonStatus);

        public PlanogramItemComparisonStatusType MetaComparisonStatus
        {
            get { return ReadProperty(MetaComparisonStatusProperty); }
            private set { SetProperty(MetaComparisonStatusProperty, value); }
        }

        #region MetaIsIllegalProduct

        public static readonly ModelPropertyInfo<Boolean?> MetaIsIllegalProductProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsIllegalProduct, Message.PlanogramProduct_MetaIsIllegalProduct);
        /// <summary>
        /// Gets/Sets the MetaIsIllegalProduct
        /// </summary>
        public Boolean? MetaIsIllegalProduct
        {
            get { return this.ReadProperty<Boolean?>(MetaIsIllegalProductProperty); }
            set { this.SetProperty<Boolean?>(MetaIsIllegalProductProperty, value); }
        }

        #endregion

        #region MetaIsNotLegalProduct

        public static readonly ModelPropertyInfo<Boolean?> MetaIsNotLegalProductProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsNotLegalProduct, Message.PlanogramProduct_MetaIsNotLegalProduct);
        /// <summary>
        /// Gets/Sets the MetaIsNotLegalProduct
        /// </summary>
        public Boolean? MetaIsNotLegalProduct
        {
            get { return this.ReadProperty<Boolean?>(MetaIsNotLegalProductProperty); }
            set { this.SetProperty<Boolean?>(MetaIsNotLegalProductProperty, value); }
        }

        #endregion

        #endregion

        #region MetaIsBuddied
        public static readonly ModelPropertyInfo<Boolean?> MetaIsBuddiedProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaIsBuddied, Message.PlanogramProduct_MetaIsBuddied);
        /// <summary>
        /// Gets/Sets the MetaIsBuddied value. If true, the planogram product is buddied to another product
        /// and will obtain its performance from its buddy
        /// </summary>
        public Boolean? MetaIsBuddied
        {
            get { return this.ReadProperty<Boolean?>(MetaIsBuddiedProperty); }
            set { this.SetProperty<Boolean?>(MetaIsBuddiedProperty, value); }
        }
        #endregion

        #endregion

        #region ParentType
        /// <summary>
        /// Returns the custom attribute data parent type
        /// </summary>
        Byte ICustomAttributeDataParent.ParentType
        {
            get { return (Byte)CustomAttributeDataPlanogramParentType.PlanogramProduct; }
        }
        #endregion

        #region ColourGroupValue

        /// <summary>
        /// The colour group this Product is a member of in the highlight that has been applied to the plan.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ColourGroupValueProperty =
            RegisterModelProperty<String>(c => c.ColourGroupValue, Message.PlanogramProduct_ColorGroupValue);

        /// <summary>
        /// The colour group this Product is a member of in the highlight that has been applied to the plan.
        ///</summary>
        public String ColourGroupValue
        {
            get { return ReadProperty<String>(ColourGroupValueProperty); }
            set { SetProperty<String>(ColourGroupValueProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(GtinProperty));
            BusinessRules.AddRule(new MaxLength(GtinProperty, 14));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));            
            BusinessRules.AddRule(new MaxLength(ColourGroupValueProperty, 255));

            //V8 - 31476 New business rules based on DB schema
            BusinessRules.AddRule(new MaxLength(ShapeProperty, 20));
            BusinessRules.AddRule(new MaxLength(FillColourProperty, 20));
            BusinessRules.AddRule(new MaxLength(FillPatternTypeProperty, 20));
            BusinessRules.AddRule(new MaxLength(PointOfPurchaseDescriptionProperty, 50));
            BusinessRules.AddRule(new MaxLength(ShortDescriptionProperty, 50));
            BusinessRules.AddRule(new MaxLength(SubcategoryProperty, 50));
            BusinessRules.AddRule(new MaxLength(CustomerStatusProperty, 50));
            BusinessRules.AddRule(new MaxLength(ColourProperty, 50));
            BusinessRules.AddRule(new MaxLength(FlavourProperty, 100));
            BusinessRules.AddRule(new MaxLength(PackagingShapeProperty, 50));
            BusinessRules.AddRule(new MaxLength(PackagingTypeProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountryOfOriginProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountryOfProcessingProperty, 50));
            BusinessRules.AddRule(new MaxLength(DeliveryMethodProperty, 20));
            BusinessRules.AddRule(new MaxLength(BrandProperty, 100));
            BusinessRules.AddRule(new MaxLength(VendorCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(VendorProperty, 100));
            BusinessRules.AddRule(new MaxLength(ManufacturerCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerProperty, 100));
            BusinessRules.AddRule(new MaxLength(SizeProperty, 50));
            BusinessRules.AddRule(new MaxLength(UnitOfMeasureProperty, 50));
            BusinessRules.AddRule(new MaxLength(HealthProperty, 50));
            BusinessRules.AddRule(new MaxLength(CorporateCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(BarcodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(SellPackDescriptionProperty, 100));
            BusinessRules.AddRule(new MaxLength(ConsumerInformationProperty, 50));
            BusinessRules.AddRule(new MaxLength(TextureProperty, 100));
            BusinessRules.AddRule(new MaxLength(PatternProperty, 100));
            BusinessRules.AddRule(new MaxLength(ModelProperty, 100));
            BusinessRules.AddRule(new MaxLength(GarmentTypeProperty, 100));
            BusinessRules.AddRule(new MaxLength(FinancialGroupCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(FinancialGroupNameProperty, 100));

            BusinessRules.AddRule(new NullableMinValue<Single>(SellPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(SellPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(CostPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(CostPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(RecommendedRetailPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(RecommendedRetailPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(CaseCostProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(CaseCostProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(TaxRateProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(TaxRateProperty, 999.99f));
            BusinessRules.AddRule(new NullableMinValue<Int16>(ShelfLifeProperty, 0));

            BusinessRules.AddRule(new NullableMaxValue<Int16>(SellPackCountProperty, 32767));
            BusinessRules.AddRule(new NullableMaxValue<Int16>(StyleNumberProperty, 32767));

            // V8-28455
            // Ensure that the Nesting size does not exceed the product size.
            BusinessRules.AddRule(new Dependency(HeightProperty, NestingHeightProperty));
            BusinessRules.AddRule(new Dependency(WidthProperty, NestingWidthProperty));
            BusinessRules.AddRule(new Dependency(DepthProperty, NestingDepthProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingHeightProperty, HeightProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingWidthProperty, WidthProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingDepthProperty, DepthProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramProduct NewPlanogramProduct()
        {
            PlanogramProduct item = new PlanogramProduct();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramProduct NewPlanogramProduct(IPlanogramProduct product)
        {
            PlanogramProduct item = new PlanogramProduct();
            item.Create(product);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating an instance of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.Generic_New);

            this.LoadProperty<Int32>(FillColourProperty, /*white*/-1);

            this.LoadProperty<Single>(SqueezeHeightProperty, 1);
            this.LoadProperty<Single>(SqueezeWidthProperty, 1);
            this.LoadProperty<Single>(SqueezeDepthProperty, 1);

            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating an instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(IPlanogramProduct product)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());

            this.LoadProperty<Int32>(FillColourProperty, /*white*/-1);

            this.LoadProperty<Single>(SqueezeHeightProperty, 1);
            this.LoadProperty<Single>(SqueezeWidthProperty, 1);
            this.LoadProperty<Single>(SqueezeDepthProperty, 1);

            Mapper.Map<IPlanogramProduct, PlanogramProduct>(product, this);

            //CCM-18628 - Temporary change to ensure name is valid when copying products.
            if (String.IsNullOrWhiteSpace(this.Name))
            {
                this.LoadProperty<String>(NameProperty, this.Gtin);
            }

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramProduct>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            #region Standard Images

            // Front
            Object planogramImageIdFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdFrontProperty));
            if (planogramImageIdFront != null) this.LoadProperty<Object>(PlanogramImageIdFrontProperty, planogramImageIdFront);

            // Back
            Object planogramImageIdBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdBackProperty));
            if (planogramImageIdBack != null) this.LoadProperty<Object>(PlanogramImageIdBackProperty, planogramImageIdBack);

            // Top
            Object planogramImageIdTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTopProperty));
            if (planogramImageIdTop != null) this.LoadProperty<Object>(PlanogramImageIdTopProperty, planogramImageIdTop);

            // Bottom
            Object planogramImageIdBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdBottomProperty));
            if (planogramImageIdBottom != null) this.LoadProperty<Object>(PlanogramImageIdBottomProperty, planogramImageIdBottom);

            // Left
            Object planogramImageIdLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdLeftProperty));
            if (planogramImageIdLeft != null) this.LoadProperty<Object>(PlanogramImageIdLeftProperty, planogramImageIdLeft);

            // Right
            Object planogramImageIdRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdRightProperty));
            if (planogramImageIdRight != null) this.LoadProperty<Object>(PlanogramImageIdRightProperty, planogramImageIdRight);

            #endregion

            #region Display Images

            // Front
            Object planogramImageIdDisplayFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayFrontProperty));
            if (planogramImageIdDisplayFront != null) this.LoadProperty<Object>(PlanogramImageIdDisplayFrontProperty, planogramImageIdDisplayFront);

            // Back
            Object planogramImageIdDisplayBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayBackProperty));
            if (planogramImageIdDisplayBack != null) this.LoadProperty<Object>(PlanogramImageIdDisplayBackProperty, planogramImageIdDisplayBack);

            // Top
            Object planogramImageIdDisplayTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayTopProperty));
            if (planogramImageIdDisplayTop != null) this.LoadProperty<Object>(PlanogramImageIdDisplayTopProperty, planogramImageIdDisplayTop);

            // Bottom
            Object planogramImageIdDisplayBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayBottomProperty));
            if (planogramImageIdDisplayBottom != null) this.LoadProperty<Object>(PlanogramImageIdDisplayBottomProperty, planogramImageIdDisplayBottom);

            // Left
            Object planogramImageIdDisplayLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayLeftProperty));
            if (planogramImageIdDisplayLeft != null) this.LoadProperty<Object>(PlanogramImageIdDisplayLeftProperty, planogramImageIdDisplayLeft);

            // Right
            Object planogramImageIdDisplayRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdDisplayRightProperty));
            if (planogramImageIdDisplayRight != null) this.LoadProperty<Object>(PlanogramImageIdDisplayRightProperty, planogramImageIdDisplayRight);

            #endregion

            #region Tray Images

            // Front
            Object planogramImageIdTrayFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayFrontProperty));
            if (planogramImageIdTrayFront != null) this.LoadProperty<Object>(PlanogramImageIdTrayFrontProperty, planogramImageIdTrayFront);

            // Back
            Object planogramImageIdTrayBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayBackProperty));
            if (planogramImageIdTrayBack != null) this.LoadProperty<Object>(PlanogramImageIdTrayBackProperty, planogramImageIdTrayBack);

            // Top
            Object planogramImageIdTrayTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayTopProperty));
            if (planogramImageIdTrayTop != null) this.LoadProperty<Object>(PlanogramImageIdTrayTopProperty, planogramImageIdTrayTop);

            // Bottom
            Object planogramImageIdTrayBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayBottomProperty));
            if (planogramImageIdTrayBottom != null) this.LoadProperty<Object>(PlanogramImageIdTrayBottomProperty, planogramImageIdTrayBottom);

            // Left
            Object planogramImageIdTrayLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayLeftProperty));
            if (planogramImageIdTrayLeft != null) this.LoadProperty<Object>(PlanogramImageIdTrayLeftProperty, planogramImageIdTrayLeft);

            // Right
            Object planogramImageIdTrayRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdTrayRightProperty));
            if (planogramImageIdTrayRight != null) this.LoadProperty<Object>(PlanogramImageIdTrayRightProperty, planogramImageIdTrayRight);

            #endregion

            #region Point Of Purchase Images

            // Front
            Object planogramImageIdPointOfPurchaseFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty));
            if (planogramImageIdPointOfPurchaseFront != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty, planogramImageIdPointOfPurchaseFront);

            // Back
            Object planogramImageIdPointOfPurchaseBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty));
            if (planogramImageIdPointOfPurchaseBack != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty, planogramImageIdPointOfPurchaseBack);

            // Top
            Object planogramImageIdPointOfPurchaseTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty));
            if (planogramImageIdPointOfPurchaseTop != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty, planogramImageIdPointOfPurchaseTop);

            // Bottom
            Object planogramImageIdPointOfPurchaseBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty));
            if (planogramImageIdPointOfPurchaseBottom != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty, planogramImageIdPointOfPurchaseBottom);

            // Left
            Object planogramImageIdPointOfPurchaseLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty));
            if (planogramImageIdPointOfPurchaseLeft != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty, planogramImageIdPointOfPurchaseLeft);

            // Right
            Object planogramImageIdPointOfPurchaseRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty));
            if (planogramImageIdPointOfPurchaseRight != null) this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty, planogramImageIdPointOfPurchaseRight);

            #endregion

            #region Alternate Images

            // Front
            Object planogramImageIdAlternateFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateFrontProperty));
            if (planogramImageIdAlternateFront != null) this.LoadProperty<Object>(PlanogramImageIdAlternateFrontProperty, planogramImageIdAlternateFront);

            // Back
            Object planogramImageIdAlternateBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateBackProperty));
            if (planogramImageIdAlternateBack != null) this.LoadProperty<Object>(PlanogramImageIdAlternateBackProperty, planogramImageIdAlternateBack);

            // Top
            Object planogramImageIdAlternateTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateTopProperty));
            if (planogramImageIdAlternateTop != null) this.LoadProperty<Object>(PlanogramImageIdAlternateTopProperty, planogramImageIdAlternateTop);

            // Bottom
            Object planogramImageIdAlternateBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateBottomProperty));
            if (planogramImageIdAlternateBottom != null) this.LoadProperty<Object>(PlanogramImageIdAlternateBottomProperty, planogramImageIdAlternateBottom);

            // Left
            Object planogramImageIdAlternateLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateLeftProperty));
            if (planogramImageIdAlternateLeft != null) this.LoadProperty<Object>(PlanogramImageIdAlternateLeftProperty, planogramImageIdAlternateLeft);

            // Right
            Object planogramImageIdAlternateRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdAlternateRightProperty));
            if (planogramImageIdAlternateRight != null) this.LoadProperty<Object>(PlanogramImageIdAlternateRightProperty, planogramImageIdAlternateRight);

            #endregion

            #region Case Images

            // Front
            Object planogramImageIdCaseFront = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseFrontProperty));
            if (planogramImageIdCaseFront != null) this.LoadProperty<Object>(PlanogramImageIdCaseFrontProperty, planogramImageIdCaseFront);

            // Back
            Object planogramImageIdCaseBack = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseBackProperty));
            if (planogramImageIdCaseBack != null) this.LoadProperty<Object>(PlanogramImageIdCaseBackProperty, planogramImageIdCaseBack);

            // Top
            Object planogramImageIdCaseTop = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseTopProperty));
            if (planogramImageIdCaseTop != null) this.LoadProperty<Object>(PlanogramImageIdCaseTopProperty, planogramImageIdCaseTop);

            // Bottom
            Object planogramImageIdCaseBottom = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseBottomProperty));
            if (planogramImageIdCaseBottom != null) this.LoadProperty<Object>(PlanogramImageIdCaseBottomProperty, planogramImageIdCaseBottom);

            // Left
            Object planogramImageIdCaseLeft = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseLeftProperty));
            if (planogramImageIdCaseLeft != null) this.LoadProperty<Object>(PlanogramImageIdCaseLeftProperty, planogramImageIdCaseLeft);

            // Right
            Object planogramImageIdCaseRight = context.ResolveId<PlanogramImage>(this.ReadProperty<Object>(PlanogramImageIdCaseRightProperty));
            if (planogramImageIdCaseRight != null) this.LoadProperty<Object>(PlanogramImageIdCaseRightProperty, planogramImageIdCaseRight);

            #endregion
        }

        #endregion

        /// <summary>
        /// Updates this object with values from the given source. If no properties to update
        /// are provided, all properties that match between this and the source object will
        /// be updated.
        /// </summary>
        /// <param name="source">
        /// The object whose values should be copied. This might be an IPlanogramProduct, 
        /// or a LocationProductAttributes, or something else with matching property names.
        /// </param>
        /// <param name="propertiesToUpdate">Any properties that should be included in the update.</param>
        public void UpdateFrom(Object source, params String[] propertiesToUpdate)
        {
            using (new CodePerformanceMetric())
            {
                if (source == null) return;

                // First, we update the attributes on this model using values from the source.
                UpdateProductAttributes(source, propertiesToUpdate);

                // Next, we may need to update individual properties in this model's custom attribute data,
                // so we try to do so using the custom attributes in the source.
                UpdateCustomAttributes(source, propertiesToUpdate);

                // Finally, there might be performance data in the source, so we try to update the associated
                // performance data in the planogram to match.
                UpdatePerformanceData(source, propertiesToUpdate);
            }
        }

        private void UpdatePerformanceData(Object source, String[] propertiesToUpdate)
        {
            Regex regexPerformanceData = new Regex(String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name));
            IEnumerable<String> performanceDataAttributePropertyNames = propertiesToUpdate.
                Where(p => regexPerformanceData.IsMatch(p)).
                Select(p => regexPerformanceData.Match(p).Groups[1].ToString());
            if (performanceDataAttributePropertyNames.Any())
            {
                // Now, try to get performance data property and the object it is on using reflection.
                PropertyInfo sourcePerformanceDataProperty = source.GetType().GetProperty(PlanogramPerformance.PerformanceDataProperty.Name);

                if (sourcePerformanceDataProperty == null) return;
                IPlanogramPerformanceData sourcePerformanceDataObject = sourcePerformanceDataProperty.GetValue(source, null) as IPlanogramPerformanceData;

                if (sourcePerformanceDataObject == null) return;

                // Finally, iterate over the performance Data property names and try to get and set values against our this/source
                // performance data properties.
                Type iPlanogramPerformanceDataType = typeof(IPlanogramPerformanceData);
                PlanogramPerformanceData thisPerformanceData = this.GetPlanogramPerformanceData();
                if (thisPerformanceData != null)
                {
                    foreach (String performanceDataPropertyName in performanceDataAttributePropertyNames)
                    {
                        PropertyInfo performanceDataProperty = iPlanogramPerformanceDataType.GetProperty(performanceDataPropertyName);
                        if (performanceDataProperty == null) continue;
                        Object sourceValue = performanceDataProperty.GetValue(sourcePerformanceDataObject, null);
                        performanceDataProperty.SetValue(thisPerformanceData, sourceValue, null);
                    }
                }
            }
        }

        /// <summary>
        /// Updates this object's custom attributes using the custom attributes on the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="propertiesToUpdate"></param>
        private void UpdateCustomAttributes(Object source, String[] propertiesToUpdate)
        {
            // Get the property names of the custom attribute specific properties (they come in the format CustomAttributes.PropertyName).
            Regex regexCustomAttribute = new Regex(String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name));
            IEnumerable<String> customAttributePropertyNames = propertiesToUpdate.
                Where(p => regexCustomAttribute.IsMatch(p)).
                Select(p => regexCustomAttribute.Match(p).Groups[1].ToString());
            if (!customAttributePropertyNames.Any()) return;

            // Now, try to get custom attribute properties from our source.
            PropertyInfo sourceCustomProperty = 
                GetSourceProperties(source, new[] { PlanogramProduct.CustomAttributesProperty.Name }).FirstOrDefault();
            if (sourceCustomProperty == null) return;
            ICustomAttributeData sourceCustomObject = sourceCustomProperty.GetValue(source, null) as ICustomAttributeData;
            if (sourceCustomObject == null) return;

            // Finally, ask this object's CustomAttributes to update using the custom attributes source.
            this.CustomAttributes.UpdateFrom(sourceCustomObject, customAttributePropertyNames.ToArray());
        }

        /// <summary>
        /// Updates product attributes on this product using the given <paramref name="source"/> and <paramref name="propertiesToUpdate"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="propertiesToUpdate"></param>
        private void UpdateProductAttributes(Object source, String[] propertiesToUpdate)
        {
            // Get the property infos for the source object (these are cached, so we try to get them from a dictionary).
            foreach (PropertyInfo sourceProperty in GetSourceProperties(source, propertiesToUpdate))
            {
                // We need to map CustomAttributeData slightly differently, so we make a
                // call to UpdateFrom here which will use AutoMapper to set the new values.
                if (sourceProperty.Name.Equals(PlanogramProduct.CustomAttributesProperty.Name))
                {
                    CustomAttributeData value = sourceProperty.GetValue(source, null) as CustomAttributeData;
                    if (value == null) continue;
                    this.CustomAttributes.UpdateFrom(value);
                }
                else // Anything other than CustomAttributes can be mapped by simply setting the value.
                {
                    // Try to get the matching property path for this property by looking for a path 
                    // with the same name and Property on the end.
                    PropertyInfo thisProperty;
                    if (!PropertyInfosByName.TryGetValue(sourceProperty.Name, out thisProperty)) continue;

                    // If we found a property path, use it to load in a new value. 
                    // Note that LoadProperty is used so that no events are fired and no rules are checked.
                    thisProperty.SetValue(this, sourceProperty.GetValue(source, null), null);
                }
            }
        }

        /// <summary>
        /// Tries to get the source properties for the given <paramref name="source"/> type that match
        /// the <paramref name="propertiesToUpdate"/>. If no properties have been found yet for this type
        /// they are captured and cached in case they are required later.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="propertiesToUpdate"></param>
        /// <returns></returns>
        private IEnumerable<PropertyInfo> GetSourceProperties(Object source, String[] propertiesToUpdate)
        {
            List<PropertyInfo> sourceProperties;
            Type sourceType = source.GetType();
            if (!_sourcePropertiesLookup.TryGetValue(sourceType, out sourceProperties))
            {
                sourceProperties = sourceType.GetProperties().Where(p => !p.Name.Equals("Id")).ToList();
                _sourcePropertiesLookup.Add(sourceType, sourceProperties);
            }

            if (propertiesToUpdate.Any())
            {
                return sourceProperties.Where(p => propertiesToUpdate.Contains(p.Name));
            }
            else
            {
                return sourceProperties;
            }
        }

        public void UpdateLocationProductAttributes(Object source, Boolean updateNull, params String[] propertiesToUpdate)
        {
            IEnumerable<PropertyInfo> sourceProperties;

            sourceProperties = source.GetType().GetProperties().Where(p => propertiesToUpdate.Contains(p.Name));

            // Iterate over the properties that we have identified and try to update this
            // if they exist here as well.
            Dictionary<String, PropertyInfo> thisPropertiesByName = this
                .GetType()
                .GetProperties()
                .Where(p => p.CanWrite)
                .ToDictionary(p => p.Name, p => p);
            foreach (PropertyInfo sourceProperty in sourceProperties)
            {
                PropertyInfo thisProperty;
                if (sourceProperty.Name.Equals("Description"))
                {
                    if (!thisPropertiesByName.TryGetValue("ShortDescription", out thisProperty)) continue;
                }
                else
                {
                    if (!thisPropertiesByName.TryGetValue(sourceProperty.Name, out thisProperty)) continue;
                }

                if (!updateNull)
                {
                    if (sourceProperty.GetValue(source, null) == null) continue;
                    //V8-30904 - cater for empty string values on string object types. These won't be null.
                    if (String.IsNullOrEmpty(sourceProperty.GetValue(source, null).ToString())) continue;
                }

                thisProperty.SetValue(this, sourceProperty.GetValue(source, null), null);
            }
        }

        /// <summary>
        /// Gets the width, height or depth of the product, based on the given axis type.
        /// </summary>
        /// <param name="axis">E.g. provide X to return Width.</param>
        /// <returns></returns>
        public Single GetSize(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return Width;
                case AxisType.Y:
                    return Height;
                case AxisType.Z:
                    return Depth;
                default:
                    throw new NotSupportedException("axis");
            }
        }

        /// <summary>
        /// Returns all positions associated to this product
        /// </summary>
        public IEnumerable<PlanogramPosition> GetPlanogramPositions()
        {
            List<PlanogramPosition> positions = new List<PlanogramPosition>();
            foreach (PlanogramPosition position in this.Parent.Positions)
            {
                if (this.Id.Equals(position.PlanogramProductId))
                    positions.Add(position);
            }
            return positions;
        }

        /// <summary>
        /// Returns the planogram performance data object for this product.
        /// </summary>
        /// <returns></returns>
        public PlanogramPerformanceData GetPlanogramPerformanceData()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) { return null; }
            if (planogram.Performance == null) return null;
            if (planogram.Performance.PerformanceData == null) return null;

            return planogram.Performance.PerformanceData
                .FirstOrDefault(p => Object.Equals(p.PlanogramProductId, this.Id));
        }

        /// <summary>
        /// Returns the cdt node to which this product belongs.
        /// </summary>
        /// <returns></returns>
        public PlanogramConsumerDecisionTreeNode GetPlanogramConsumerDecisionTreeNode()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) { return null; }
            if (planogram.ConsumerDecisionTree == null) return null;

            Object id = this.Id;
            foreach (PlanogramConsumerDecisionTreeNode node in planogram.ConsumerDecisionTree.FetchAllNodes())
            {
                foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProduct in node.Products)
                {
                    if (Object.Equals(nodeProduct.PlanogramProductId, id)) return node;
                }
            }
            return null;
        }
       
        /// <summary>
        /// Returns the assortment product related to this
        /// </summary>
        public PlanogramAssortmentProduct GetPlanogramAssortmentProduct()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            if (planogram.Assortment == null) return null;

            return planogram.Assortment.Products.FirstOrDefault(p => p.Gtin == this.Gtin);
        }

        /// <summary>
        /// Indicates if a position of this product
        /// can be added to the specified sub component
        /// </summary>
        public Boolean CanAddPosition(PlanogramSubComponent subComponent)
        {
            // sub component must be merchandisable
            if (subComponent.MerchandisingType == PlanogramSubComponentMerchandisingType.None) return false;

            // must be from the same planogram
            if (subComponent.Parent.Parent != this.Parent) return false;

            // indicate success
            return true;
        }

        /// <summary>
        /// Adds a position of this product to the planogram
        /// </summary>
        public PlanogramPosition AddPosition()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.Positions.Add(this);
        }

        /// <summary>
        /// Adds a position of this product to the planogram
        /// </summary>
        public PlanogramPosition AddPosition(PlanogramSubComponentPlacement subComponentPlacement)
        {
            Planogram planogram = this.Parent;
            if (planogram == null) return null;
            return planogram.Positions.Add(this, subComponentPlacement);
        }

        /// <summary>
        /// Returns the unorientated unit size of the product
        /// </summary>
        public WidthHeightDepthValue GetUnorientatedUnitSize(PlanogramProductMerchandisingStyle merchStyle,
            Single squeezeHeight, Single squeezeWidth, Single squeezeDepth)
        {
            WidthHeightDepthValue returnValue;
            switch (merchStyle)
            {
                case PlanogramProductMerchandisingStyle.Alternate:
                    returnValue = new WidthHeightDepthValue(this.AlternateWidth, this.AlternateHeight, this.AlternateDepth);
                    break;

                case PlanogramProductMerchandisingStyle.Case:
                    returnValue = new WidthHeightDepthValue(this.CaseWidth, this.CaseHeight, this.CaseDepth);
                    break;

                case PlanogramProductMerchandisingStyle.Display:
                    returnValue = new WidthHeightDepthValue(this.DisplayWidth, this.DisplayHeight, this.DisplayDepth);
                    break;

                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    returnValue = new WidthHeightDepthValue(this.PointOfPurchaseWidth, this.PointOfPurchaseHeight, this.PointOfPurchaseDepth);
                    break;

                case PlanogramProductMerchandisingStyle.Tray:
                    {
                        Single trayThickWidth = (this.TrayThickWidth > 0) ? this.TrayThickWidth : 0.2F;
                        Int32 unitsInTrayWide = Math.Max((Int32)this.TrayWide, 1);
                        Single innerTrayWidth = this.TrayWidth - trayThickWidth;
                        Single width = innerTrayWidth / unitsInTrayWide;

                        Single trayThickHeight = (this.TrayThickHeight > 0) ? this.TrayThickHeight : 0.2F;
                        Int32 unitsInTrayHigh = Math.Max((Int32)this.TrayHigh, 1);
                        Single innerTrayHeight = this.TrayHeight - trayThickHeight;
                        Single height = innerTrayHeight / unitsInTrayHigh;

                        Single trayThickDepth = (this.TrayThickDepth > 0) ? this.TrayThickDepth : 0.2F;
                        Int32 unitsInTrayDeep = Math.Max((Int32)this.TrayDeep, 1);
                        Single innerTrayDepth = this.TrayDepth - trayThickDepth;
                        Single depth = innerTrayDepth / unitsInTrayDeep;

                        returnValue = new WidthHeightDepthValue(width, height, depth);
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Unit:
                    {
                        Single height = this.Height;
                        Single width = this.Width;
                        Single depth = this.Depth;

                        //apply squeeze
                        // Squeeze values are percentages. 1 = no squeeze.
                        // nb - 0 is not allowed either as it would make the product disappear.
                        if (squeezeHeight != 1 && squeezeHeight > 0)
                        {
                            height = height * squeezeHeight;
                        }

                        if (squeezeWidth != 1 && squeezeWidth > 0)
                        {
                            width = width * squeezeWidth;
                        }

                        if (squeezeDepth != 1 && squeezeWidth > 0)
                        {
                            depth = depth * squeezeDepth;
                        }

                        returnValue = new WidthHeightDepthValue(width, height, depth);
                    }
                    break;

                default:
                    Debug.Fail("TODO");
                    returnValue = new WidthHeightDepthValue();
                    break;
            }



            //default to the base product size if any values are 0.
            if (returnValue.Height <= 0 || returnValue.Width <= 0 || returnValue.Depth <= 0)
            {
                Single width = this.Width > 0 ? this.Width : 1F;
                Single height = this.Height > 0 ? this.Height : 1F;
                Single depth = this.Depth > 0 ? this.Depth : 1F;
                returnValue = new WidthHeightDepthValue(width, height, depth);
            }

            // return the size
            return returnValue;
        }

        /// <summary>
        /// Returns the unorientated tray size for this product.
        /// </summary>
        /// <returns></returns>
        public WidthHeightDepthValue GetUnorientatedTraySize()
        {
            Single trayHeight = this.TrayHeight;
            Single trayWidth = this.TrayWidth;
            Single trayDepth = this.TrayDepth;

            if (trayHeight <= 0 || trayWidth <= 0 || trayDepth <= 0)
            {
                //default to the unit dimensions.
                Int16 trayHigh = this.TrayHigh;
                Int16 trayWide = this.TrayWide;
                Int16 trayDeep = this.TrayDeep;

                //ensure at least one unit is placed.
                if (trayHigh <= 0) trayHigh = 1;
                if (trayWide <= 0) trayWide = 1;
                if (trayDeep <= 0) trayDeep = 1;


                //[V8-28015] if any of the tray dimensions are 0 then 
                // default back to the unit size.
                if (trayHeight <= 0) trayHeight = ((this.Height > 0) ? this.Height : 1) * trayHigh;
                if (trayWidth <= 0) trayWidth = ((this.Width > 0) ? this.Width : 1) * trayWide;
                if (trayDepth <= 0) trayDepth = ((this.Depth > 0) ? this.Depth : 1) * trayDeep;
            }
            return new WidthHeightDepthValue(trayWidth, trayHeight, trayDepth);
        }

        /// <summary>
        ///     Check whether the <c>Product</c> has any of the given <paramref name="merchandisingStyles"/>.
        /// </summary>
        /// <param name="merchandisingStyles">Parameter list of <see cref="PlanogramPositionMerchandisingStyle"/> to check this <c>Product</c> against.</param>
        /// <returns><c>True</c> if the <c>Product</c>'s MerchandisingStyle is <b>any</b> of the ones provided.</returns>
        public Boolean IsMerchandisingStyleAnyOf(params PlanogramProductMerchandisingStyle[] merchandisingStyles)
        {
            return merchandisingStyles.Any(o => o.Equals(MerchandisingStyle));
        }

        /// <summary>
        ///     Get the max cap units for the given <paramref name="axis"/>.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="axis">The axis to get the max cap value for.</param>
        /// <returns>The maximum number of units that can be placed as cap units on the given <paramref name="axis"/>.</returns>
        /// <remarks>The Z axis has no max cap units value currently and will return 0.</remarks>
        public Int32 GetMaxCapUnits(AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    return MaxRightCap;
                case AxisType.Y:
                    return MaxTopCap;
                case AxisType.Z:
                    Debug.Fail("There is no max cap units value for axis Z.");
                    break;
                default:
                    Debug.Fail("Unknown axis when calling GetMaxCapUnits(axis, product).");
                    break;
            }
            return 0;
        }

        /// <summary>
        ///     Calculate the rotated tray size for this instance and <paramref name="product"/>.
        /// </summary>
        /// <param name="product">The <see cref="PlanogramProduct"/> to use for this instance when calculating the rotated tray size.</param>
        /// <param name="orientation"></param>
        /// <returns>A <see cref="WidthHeightDepthValue"/> with the calculated size for this instance's <see cref="PlanogramPositionOrientationType"/>.</returns>
        public WideHighDeepValue GetRotatedTraySize(PlanogramPositionOrientationType orientation)
        {
            const Byte minSize = (Byte)1;

            //  Use the product's orientation type if the position's is set to default.
            PlanogramPositionOrientationType orientationType = orientation == PlanogramPositionOrientationType.Default
                                                                   ? OrientationType.ToPositionOrientationType()
                                                                   : orientation;

            //  Start with the unrotated values.
            Int16 wide = TrayWide;
            Int16 high = TrayHigh;
            Int16 deep = TrayDeep;

            switch (orientationType)
            {
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Back180:
                    //  Nothing really changes.
                    break;
                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Back270:
                    //  Rotation ended up exchanging width for height.
                    wide = high;
                    high = TrayWide;
                    break;
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Bottom180:
                    //  Rotation exchanged height for depth.
                    high = deep;
                    deep = TrayHigh;
                    break;
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Bottom270:
                    // Rotation exchanged all dimensions.
                    wide = deep;
                    high = TrayWide;
                    deep = TrayHigh;
                    break;
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Left180:
                    //  Rotation exchanged width for depth.
                    wide = deep;
                    deep = TrayWide;
                    break;
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Left270:
                    //  Rotation exchanged all dimensions.
                    wide = high;
                    high = TrayDeep;
                    deep = TrayWide;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return new WideHighDeepValue(Math.Max(wide, minSize), Math.Max(high, minSize), Math.Max(deep, minSize));
        }

        #region Images

        /// <summary>
        /// Loads the relevant images from the given Product into PlanogramImages and adds references
        /// to the Ids of these PlanogramImages to the appropriate properties of this PlanogramProduct.
        /// </summary>
        /// <param name="product">The Product whose images should be loaded.</param>
        /// <exception cref="InvalidOperationException">
        /// Thrown when the Parent property is null.
        /// </exception>
        /// <remarks>This only seems to be used for testing.. probably should be removed.</remarks>
        internal void LoadProductImages(IPlanogramProductImageHelper product)
        {
            // Ensure that the Parent property is accessible.
            if (Parent == null) throw new InvalidOperationException("LoadProductImages cannot be called before Parent is set.");

            foreach (var imageTypeName in Enum.GetNames(typeof(PlanogramImageType)))
            {
                foreach (var facingTypeName in Enum.GetNames(typeof(PlanogramImageFacingType)))
                {
                    // Find matching property for image type.
                    var propertyInfo = typeof(PlanogramProduct).GetProperty(
                        String.Format(
                            "PlanogramImageId{0}{1}",
                            imageTypeName == "None" ? String.Empty : imageTypeName,
                            facingTypeName));
                    if (propertyInfo == null) continue;

                    // Get corresponding image data from image helper.
                    var imageType = (PlanogramImageType)Enum.Parse(typeof(PlanogramImageType), imageTypeName);
                    var facingType = (PlanogramImageFacingType)Enum.Parse(typeof(PlanogramImageFacingType), facingTypeName);
                    Byte[] imageData;
                    try
                    {
                        imageData = product.GetImageData(imageType, facingType);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        continue;
                    }
                    if (imageData == null) continue;

                    // Create PlanogramImage from image data.
                    var planogramImage = PlanogramImage.NewPlanogramImage();
                    planogramImage.ImageData = new Byte[imageData.Length];
                    imageData.CopyTo(planogramImage.ImageData, 0);
                    Parent.Images.Add(planogramImage);

                    // Set property with the Id of the new Planogram Image.
                    propertyInfo.SetValue(this, planogramImage.Id, null);
                }
            }
        }

        /// <summary>
        ///     Gets the planogram image corresponding to the given parameters.
        /// </summary>
        /// <param name="merchStyle">The merchandising style of the desired image.</param>
        /// <param name="faceType">The facing type of the desired image.</param>
        /// <param name="imageUpdateCallBack">A callback to invoke when the image is finally retrieved IF not retrieved immediately.</param>
        /// <param name="imageType">The type of image returned as this may be different to the one requested</param>
        /// <returns></returns>
        public PlanogramImage GetMerchStyleImageAsync(PlanogramProductMerchandisingStyle merchStyle, PlanogramProductFaceType faceType, 
            Action<Object> imageUpdateCallBack, Object userState, out PlanogramImageType imageType)
        {
            PlanogramImage img = null;
            imageType = PlanogramImageType.None;

            Planogram plan = this.Parent;
            if (plan == null) return null;

            switch (merchStyle)
            {
                case PlanogramProductMerchandisingStyle.Unit:
                    imageType = PlanogramImageType.None;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Display:
                    imageType = PlanogramImageType.Display;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdDisplayRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Tray:
                    imageType = PlanogramImageType.Tray;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdTrayRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    imageType = PlanogramImageType.PointOfPurchase;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdPointOfPurchaseRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Alternate:
                    imageType = PlanogramImageType.Alternate;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdAlternateRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Case:
                    imageType = PlanogramImageType.Case;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseFront, PlanogramImageFacingType.Front, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseBack, PlanogramImageFacingType.Back, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseTop, PlanogramImageFacingType.Top, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseBottom, PlanogramImageFacingType.Bottom, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseLeft, PlanogramImageFacingType.Left, imageType, imageUpdateCallBack, userState); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImageLazy(this, this.PlanogramImageIdCaseRight, PlanogramImageFacingType.Right, imageType, imageUpdateCallBack, userState); break;
                    }
                    break;
            }

            //if the image was null try to return the normal image.
            if ((img == null || img.ImageData == null) && merchStyle != PlanogramProductMerchandisingStyle.Unit)
            {
                img = this.GetMerchStyleImageAsync(PlanogramProductMerchandisingStyle.Unit, faceType, imageUpdateCallBack,userState, out imageType);
            }

            // return the image
            return img;
        }

        /// <summary>
        ///     Gets the planogram image corresponding to the given parameters.
        /// </summary>
        /// <param name="merchStyle">The merchandising style of the desired image.</param>
        /// <param name="faceType">The facing type of the desired image.</param>
        /// <param name="imageType">The type of image returned as this may be different to the one requested</param>
        /// <returns></returns>
        public PlanogramImage GetMerchStyleImage(PlanogramProductMerchandisingStyle merchStyle, PlanogramProductFaceType faceType, 
            out PlanogramImageType imageType)
        {
            PlanogramImage img = null;
            imageType = PlanogramImageType.None;

            Planogram plan = this.Parent;
            if (plan == null) return null;

            switch (merchStyle)
            {
                case PlanogramProductMerchandisingStyle.Unit:
                    imageType = PlanogramImageType.None;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Display:
                    imageType = PlanogramImageType.Display;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdDisplayFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdDisplayBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdDisplayTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdDisplayBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdDisplayLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdDisplayRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Tray:
                    imageType = PlanogramImageType.Tray;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdTrayFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdTrayBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdTrayTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdTrayBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdTrayLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdTrayRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    imageType = PlanogramImageType.PointOfPurchase;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdPointOfPurchaseRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Alternate:
                    imageType = PlanogramImageType.Alternate;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdAlternateFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdAlternateBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdAlternateTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdAlternateBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdAlternateLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdAlternateRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Case:
                    imageType = PlanogramImageType.Case;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = plan.GetImage(this, this.PlanogramImageIdCaseFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = plan.GetImage(this, this.PlanogramImageIdCaseBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = plan.GetImage(this, this.PlanogramImageIdCaseTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = plan.GetImage(this, this.PlanogramImageIdCaseBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = plan.GetImage(this, this.PlanogramImageIdCaseLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = plan.GetImage(this, this.PlanogramImageIdCaseRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;
            }

            //if the image was null try to return the normal image.
            if ((img == null || img.ImageData == null) && merchStyle != PlanogramProductMerchandisingStyle.Unit)
            {
                img = this.GetMerchStyleImage(PlanogramProductMerchandisingStyle.Unit, faceType, out imageType);
            }

            // return the image
            return img;
        }

        /// <summary>
        /// Enumerates through all linked image ids.
        /// </summary>
        public IEnumerable<Object> EnumerateImageIds()
        {
            if (this.PlanogramImageIdFront != null) yield return this.PlanogramImageIdFront;
            if (this.PlanogramImageIdLeft != null) yield return this.PlanogramImageIdLeft;
            if (this.PlanogramImageIdTop != null) yield return this.PlanogramImageIdTop;
            if (this.PlanogramImageIdBack != null) yield return this.PlanogramImageIdBack;
            if (this.PlanogramImageIdRight != null) yield return this.PlanogramImageIdRight;
            if (this.PlanogramImageIdBottom != null) yield return this.PlanogramImageIdBottom;

            //display
            if (this.PlanogramImageIdDisplayFront != null) yield return this.PlanogramImageIdDisplayFront;
            if (this.PlanogramImageIdDisplayLeft != null) yield return this.PlanogramImageIdDisplayLeft;
            if (this.PlanogramImageIdDisplayTop != null) yield return this.PlanogramImageIdDisplayTop;
            if (this.PlanogramImageIdDisplayBack != null) yield return this.PlanogramImageIdDisplayBack;
            if (this.PlanogramImageIdDisplayRight != null) yield return this.PlanogramImageIdDisplayRight;
            if (this.PlanogramImageIdDisplayBottom != null) yield return this.PlanogramImageIdDisplayBottom;

            //case
            if (this.PlanogramImageIdCaseFront != null) yield return this.PlanogramImageIdCaseFront;
            if (this.PlanogramImageIdCaseLeft != null) yield return this.PlanogramImageIdCaseLeft;
            if (this.PlanogramImageIdCaseTop != null) yield return this.PlanogramImageIdCaseTop;
            if (this.PlanogramImageIdCaseBack != null) yield return this.PlanogramImageIdCaseBack;
            if (this.PlanogramImageIdCaseRight != null) yield return this.PlanogramImageIdCaseRight;
            if (this.PlanogramImageIdCaseBottom != null) yield return this.PlanogramImageIdCaseBottom;

            //alternate
            if (this.PlanogramImageIdAlternateFront != null) yield return this.PlanogramImageIdAlternateFront;
            if (this.PlanogramImageIdAlternateLeft != null) yield return this.PlanogramImageIdAlternateLeft;
            if (this.PlanogramImageIdAlternateTop != null) yield return this.PlanogramImageIdAlternateTop;
            if (this.PlanogramImageIdAlternateBack != null) yield return this.PlanogramImageIdAlternateBack;
            if (this.PlanogramImageIdAlternateRight != null) yield return this.PlanogramImageIdAlternateRight;
            if (this.PlanogramImageIdAlternateBottom != null) yield return this.PlanogramImageIdAlternateBottom;

            //point of purchase
            if (this.PlanogramImageIdPointOfPurchaseFront != null) yield return this.PlanogramImageIdPointOfPurchaseFront;
            if (this.PlanogramImageIdPointOfPurchaseLeft != null) yield return this.PlanogramImageIdPointOfPurchaseLeft;
            if (this.PlanogramImageIdPointOfPurchaseTop != null) yield return this.PlanogramImageIdPointOfPurchaseTop;
            if (this.PlanogramImageIdPointOfPurchaseBack != null) yield return this.PlanogramImageIdPointOfPurchaseBack;
            if (this.PlanogramImageIdPointOfPurchaseRight != null) yield return this.PlanogramImageIdPointOfPurchaseRight;
            if (this.PlanogramImageIdPointOfPurchaseBottom != null) yield return this.PlanogramImageIdPointOfPurchaseBottom;

            //tray
            if (this.PlanogramImageIdTrayFront != null) yield return this.PlanogramImageIdTrayFront;
            if (this.PlanogramImageIdTrayLeft != null) yield return this.PlanogramImageIdTrayLeft;
            if (this.PlanogramImageIdTrayTop != null) yield return this.PlanogramImageIdTrayTop;
            if (this.PlanogramImageIdTrayBack != null) yield return this.PlanogramImageIdTrayBack;
            if (this.PlanogramImageIdTrayRight != null) yield return this.PlanogramImageIdTrayRight;
            if (this.PlanogramImageIdTrayBottom != null) yield return this.PlanogramImageIdTrayBottom;
        }

        /// <summary>
        ///     Clears ALL used images from the planogram product.
        /// </summary>
        public void ClearImages()
        {
            this.PlanogramImageIdFront = null;
            this.PlanogramImageIdLeft = null;
            this.PlanogramImageIdTop = null;
            this.PlanogramImageIdBack = null;
            this.PlanogramImageIdRight = null;
            this.PlanogramImageIdBottom = null;
            this.PlanogramImageIdDisplayFront = null;
            this.PlanogramImageIdDisplayLeft = null;
            this.PlanogramImageIdDisplayTop = null;
            this.PlanogramImageIdDisplayBack = null;
            this.PlanogramImageIdDisplayRight = null;
            this.PlanogramImageIdDisplayBottom = null;
            this.PlanogramImageIdCaseFront = null;
            this.PlanogramImageIdCaseLeft = null;
            this.PlanogramImageIdCaseTop = null;
            this.PlanogramImageIdCaseBack = null;
            this.PlanogramImageIdCaseRight = null;
            this.PlanogramImageIdCaseBottom = null;
            this.PlanogramImageIdAlternateFront = null;
            this.PlanogramImageIdAlternateLeft = null;
            this.PlanogramImageIdAlternateTop = null;
            this.PlanogramImageIdAlternateBack = null;
            this.PlanogramImageIdAlternateRight = null;
            this.PlanogramImageIdAlternateBottom = null;
            this.PlanogramImageIdPointOfPurchaseFront = null;
            this.PlanogramImageIdPointOfPurchaseLeft = null;
            this.PlanogramImageIdPointOfPurchaseTop = null;
            this.PlanogramImageIdPointOfPurchaseBack = null;
            this.PlanogramImageIdPointOfPurchaseRight = null;
            this.PlanogramImageIdPointOfPurchaseBottom = null;
            this.PlanogramImageIdTrayFront = null;
            this.PlanogramImageIdTrayLeft = null;
            this.PlanogramImageIdTrayTop = null;
            this.PlanogramImageIdTrayBack = null;
            this.PlanogramImageIdTrayRight = null;
            this.PlanogramImageIdTrayBottom = null;
        }

        #endregion

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(
            Boolean includeMetadata,
            Boolean includeCustom,
            Boolean includePerformanceData,
            PlanogramPerformance performanceContext = null,
            Boolean includeAssortmentData = true)
        {
            Type type = typeof(PlanogramProduct);
            String typeFriendly = PlanogramProduct.FriendlyName;

            String detailsGroup = Message.PlanogramProduct_PropertyGroup_Details;
            String attributesGroup = Message.PlanogramProduct_PropertyGroup_Attributes;
            String sizeGroup = Message.PlanogramProduct_PropertyGroup_Size;
            String placementGroup = Message.PlanogramProduct_PropertyGroup_Placement;
            String customGroup = Message.PlanogramProduct_PropertyGroup_Custom;
            String metaGroup = Message.PlanogramProduct_PropertyGroup_Metadata;
            String specialGroup = Message.PlanogramProduct_PropertyGroup_Special;
            String performanceDataGroup = Message.PlanogramProduct_PropertyGroup_Performance;
            String assortmentProductGroup = Message.PlanogramProduct_PropertyGroup_AssortmentProduct;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, GtinProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, StatusTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SellPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RecommendedRetailPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerRecommendedRetailPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CostPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseCostProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TaxRateProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsPlaceHolderProductProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsActiveProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ForceMiddleCapProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ForceBottomCapProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ShapeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ShapeTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillColourProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FillPatternTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SizeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DateIntroducedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DateDiscontinuedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DateEffectiveProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CorporateCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsPrivateLabelProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsNewProductProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ShortDescriptionProperty, detailsGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrientationTypeProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsFrontOnlyProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NumberOfPegHolesProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegXProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegX2Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegX3Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegYProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegY2Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegY3Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegProngOffsetXProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegProngOffsetYProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PegDepthProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseHighProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseWideProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxStackProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxTopCapProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxRightCapProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FrontOverhangProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FingerSpaceAboveProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FingerSpaceToTheSideProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisingStyleProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanBreakTrayUpProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanBreakTrayDownProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanBreakTrayBackProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CanBreakTrayTopProperty, placementGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CasePackUnitsProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayPackUnitsProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DisplayHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DisplayWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DisplayDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AlternateHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AlternateWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AlternateDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PointOfPurchaseHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PointOfPurchaseWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PointOfPurchaseDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SqueezeHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SqueezeWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SqueezeDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NestingHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NestingWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NestingDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CaseDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayThickHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayThickWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayThickDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayHighProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayWideProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TrayDeepProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PointOfPurchaseDescriptionProperty, sizeGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SubcategoryProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomerStatusProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FlavourProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PackagingShapeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PackagingTypeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CountryOfOriginProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CountryOfProcessingProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ShelfLifeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DeliveryFrequencyDaysProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DeliveryMethodProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BrandProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VendorCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VendorProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ManufacturerProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ColourProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitOfMeasureProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HealthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BarcodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SellPackCountProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SellPackDescriptionProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ConsumerInformationProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, TextureProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, StyleNumberProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PatternProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ModelProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, GarmentTypeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FinancialGroupCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FinancialGroupNameProperty, attributesGroup);

            //return properties from custom attribute data.
            if (includeCustom)
            {
                foreach (IModelPropertyInfo property in CustomAttributeData.EnumerateDisplayablePropertyInfos())
                {
                    var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, property, customGroup);
                    fieldInfo.PropertyName = CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
                    yield return fieldInfo;
                }
            }

            //return assortment product properties
            if (includeAssortmentData)
            {
                foreach (IModelPropertyInfo propertyInfo in PlanogramAssortmentProduct.EnumerateDisplayablePropertyInfos())
                {
                    yield return ObjectFieldInfo.NewObjectFieldInfo(
                          type, typeFriendly,
                          AssortmentFieldPrefix + propertyInfo.Name,
                           Message.PlanogramProduct_AssortmentProductNamePrefix + " " + propertyInfo.FriendlyName,
                           propertyInfo.Type,
                           propertyInfo.DisplayType,
                        /*isSpecial*/true,
                           assortmentProductGroup);
                }
            }

            //Metadata fields
            if (includeMetadata)
            {
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CdtNodeNameProperty, detailsGroup);
                Boolean isReadonly = true;

                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedInventoryProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalUnitsProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramLinearSpacePercentageProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramAreaSpacePercentageProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPlanogramVolumetricSpacePercentageProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsInMasterDataProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsRangedInAssortmentProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMainFacingsProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalXFacingsProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalYFacingsProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalZFacingsProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedCasesProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedDOSProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsOverShelfLifePercentProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedDeliveriesProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPositionCountProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCDTNodeProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsFamilyRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsInheritanceRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsLocalProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsDistributionRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsCoreRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsDelistProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsForceProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsPreserveProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsMinimumHurdleProductRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsMaximumProductFamilyRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsMinimumProductFamilyRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsDependencyFamilyRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsDelistFamilyRuleBrokenProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsIllegalProductProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsNotLegalProductProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaComparisonStatusProperty, metaGroup, isReadonly);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaIsBuddiedProperty, metaGroup, isReadonly);
            }

            //Performance data fields
            if (includePerformanceData)
            {
                foreach (ObjectFieldInfo fieldInfo in PlanogramPerformanceData.EnumeratePlanogramDisplayableFieldInfos(performanceContext, includeMetadata))
                {
                    yield return fieldInfo;
                }
            }
        }

        /// <summary>
        /// Returns the value of the given field for this product.
        /// </summary>
        internal Object GetFieldValue(ObjectFieldInfo field)
        {
            return GetFieldValue(field, null);
        }

        /// <summary>
        /// Returns the value of the given field for this product.
        /// </summary>
        internal Object GetFieldValue(ObjectFieldInfo field, PlanogramPerformanceData performanceData)
        {
            Debug.Assert(field.OwnerType == typeof(PlanogramProduct), "Field is not for this object type");

            Object value = null;
            try
            {

                if (field.PropertyName.StartsWith(PlanogramProduct.CustomAttributesProperty.Name + "."))
                {
                    //Custom attribute data field.
                    value = WpfHelper.GetItemPropertyValue(this.CustomAttributes, field.PropertyName.Split('.')[1]);
                }
                else if (field.PropertyName.StartsWith(PlanogramPerformance.PerformanceDataProperty.Name + "."))
                {
                    //Performance data field
                    if (performanceData == null)
                    {
                        if (this.Parent == null) return null;
                        if (this.Parent.Performance == null) return null;

                        performanceData = this.Parent.Performance.PerformanceData.FirstOrDefault(p => Object.Equals(p.PlanogramProductId, this.Id));
                        if (performanceData == null) return null;
                    }

                    //30577 : Ensure rounded if not null
                    //32424: using 5 dp as default is causing precision loss for percentage types
                    Object performanceValue = WpfHelper.GetItemPropertyValue(performanceData, field.PropertyName.Split('.')[1]);
                    if (performanceValue != null && field.PropertyDisplayType != ModelPropertyDisplayType.Percentage)
                    {
                        performanceValue = (Single)(Math.Round(Convert.ToSingle(performanceValue), 5));
                    }
                    value = performanceValue;
                }
                else if (field.PropertyName.StartsWith(AssortmentFieldPrefix))
                {
                    //Assortment Product field
                    PlanogramAssortmentProduct assortmentProduct = GetPlanogramAssortmentProduct();
                    if (assortmentProduct == null) return null;
                    value = WpfHelper.GetItemPropertyValue(assortmentProduct, field.PropertyName.Split('.')[1]);
                }
                else
                {
                    value = field.GetValue(this);
                }
            }
            catch { }

            return value;
        }

        /// <summary>
        ///     Get the available <see cref="PlanogramProduct"/> attributes.
        /// </summary>
        /// <returns>An enumeration of <see cref="ProductAttributeUpdateValue"/> items derived from the <c>Product Attributes</c>.</returns>
        public static IEnumerable<ProductAttributeUpdateValue> GetAvailableProductAttributeUpdateValues()
        {
            return EnumerateDisplayableFieldInfos(false, true, false, null, false).
                Select(ProductAttributeUpdateValue.CreateNewUpdateValue);
        }

        #endregion

        #region Validation

        /// <summary>
        /// Returns a list of validation warning types that are broken for this product.
        /// </summary>
        /// <param name="metaDetails"></param>
        /// <returns></returns>
        public List<PlanogramValidationWarning> GetBrokenValidationWarnings(PlanogramMetadataDetails metaDetails)
        {
            List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();
            List<PlanogramValidationWarningType> brokenRules = new List<PlanogramValidationWarningType>();
            List<PlanogramValidationWarningType> brokenAssortmentRules = new List<PlanogramValidationWarningType>();

            PlanogramInventory inventory = metaDetails.Planogram.Inventory;
            PlanogramPerformanceData performanceData = metaDetails.FindPerformanceDataByPlanogramProductId(this.Id);

            // Product Gtin is duplicated.
            if (this.IsDuplicateGtin(metaDetails))
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductHasDuplicateGtin);
            }

            // Check DOS performance if set to do so.
            if (inventory.IsDosValidated && !this.IsMinDosAchieved(performanceData, inventory))
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductDoesNotAchieveMinDos);
            }

            // Check Case Pack performance if set to do so.
            if (inventory.IsCasePacksValidated && !this.IsMinCasesAchieved(performanceData, inventory))
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductDoesNotAchieveMinCases);
            }

            // Check Deliveries performance if set to do so.
            if (inventory.IsDeliveriesValidated && !this.IsMinDeliveriesAchieved(performanceData, inventory))
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductDoesNotAchieveMinDeliveries);
            }

            // Check Shelf Life performance if set to do so.
            if (inventory.IsShelfLifeValidated && !this.IsMinShelfLifeAchieved(performanceData, inventory))
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductDoesNotAchieveMinShelfLife);
            }

            if (this.MetaIsIllegalProduct.HasValue && this.MetaIsIllegalProduct.Value)
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductBreaksLocationProductIllegalRule);
            }

            if (this.MetaIsNotLegalProduct.HasValue && this.MetaIsNotLegalProduct.Value)
            {
                brokenRules.Add(PlanogramValidationWarningType.ProductBreaksLocationProductLegalRule);
            }

            #region Check assortment rules
            //local line products
            if (this.MetaIsLocalProductRuleBroken.HasValue && this.MetaIsLocalProductRuleBroken.Value)
            {
                brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksLocalLineRule);
            }
            //distribution products
            if (this.MetaIsDistributionRuleBroken.HasValue && this.MetaIsDistributionRuleBroken.Value)
            {
                brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksDistributionRule);
            }
            //product rules
            if (this.MetaIsProductRuleBroken.HasValue && this.MetaIsProductRuleBroken.Value)
            {
                if (this.MetaIsDelistProductRuleBroken.HasValue && this.MetaIsDelistProductRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksDelistProductRule);
                }
                else if (this.MetaIsForceProductRuleBroken.HasValue && this.MetaIsForceProductRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksForceProductRuleBroken);
                }
                else if (this.MetaIsMinimumHurdleProductRuleBroken.HasValue && this.MetaIsMinimumHurdleProductRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksMinimumHurdleProductRule);
                }
                else if (this.MetaIsPreserveProductRuleBroken.HasValue && this.MetaIsPreserveProductRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksPreserveProductRuleBroken);
                }
                //Nb - core is covered by MetaIsCoreRuleBroken further down.
            }
            //family rules
            if (this.MetaIsFamilyRuleBroken.HasValue && this.MetaIsFamilyRuleBroken.Value)
            {
                if (this.MetaIsDependencyFamilyRuleBroken.HasValue && this.MetaIsDependencyFamilyRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksDependencyFamilyRule);
                }
                else if (this.MetaIsDelistFamilyRuleBroken.HasValue && this.MetaIsDelistFamilyRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksDelistFamilyRule);
                }
                else if (this.MetaIsMaximumProductFamilyRuleBroken.HasValue && this.MetaIsMaximumProductFamilyRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksMaximumProductFamilyRule);
                }
                else if (this.MetaIsMinimumProductFamilyRuleBroken.HasValue && this.MetaIsMinimumProductFamilyRuleBroken.Value)
                {
                    brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksMinimumProductFamilyRule);
                }
            }
            //inheritance products
            if (this.MetaIsInheritanceRuleBroken.HasValue && this.MetaIsInheritanceRuleBroken.Value)
            {
                brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksInheritanceRule);
            }
            //core products
            if (this.MetaIsCoreRuleBroken.HasValue && this.MetaIsCoreRuleBroken.Value)
            {
                brokenAssortmentRules.Add(PlanogramValidationWarningType.ProductBreaksCoreRule);
            }
            #endregion

            //add all assortment rule warnings regardless of whether they are placed or not
            if (brokenAssortmentRules.Count > 0)
            {
                //Get all positions (if any exist)
                IEnumerable<PlanogramPosition> positions = this.GetPlanogramPositions();
                foreach (PlanogramValidationWarningType rule in brokenAssortmentRules)
                {
                    //If product is placed, specific position in warning
                    if (positions.Any())
                    {
                        foreach (PlanogramPosition position in positions)
                        {
                            warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(rule, position));
                        }
                    }
                    else
                    {
                        //Other use the product in the warning
                        warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(rule, this));
                    }
                }
            }

            //create the warnings on a per position basis.
            if (brokenRules.Count > 0)
            {
                foreach (PlanogramPosition position in this.GetPlanogramPositions())
                {
                    foreach (PlanogramValidationWarningType rule in brokenRules)
                    {
                        warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(rule, position));
                    }
                }
            }

            return warnings;
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            Planogram parentPlan = this.Parent;
            if (parentPlan == null) return;

            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();

            // get position units
            ICollection<PlanogramPosition> productPositions = metadataDetails.FindPositionsByPlanogramProductId(this.Id).ToList();
            Int32 productTotalUnits = productPositions.Sum(p => p.TotalUnits);

            // product not achieved inventory
            PlanogramAssortmentProduct assortmentProduct = metadataDetails.FindPlanogramAssortmentProductByGtin(this.Gtin);
            Boolean isInAssortment = assortmentProduct != null;
            Int32 assortmentUnits = isInAssortment ? assortmentProduct.Units : 0;
            this.MetaNotAchievedInventory = (productTotalUnits < assortmentUnits);
            this.MetaIsRangedInAssortment = isInAssortment && assortmentProduct.IsRanged;



            this.MetaTotalUnits = productTotalUnits;
            this.MetaPositionCount = productPositions.Count();
            this.MetaTotalMainFacings = CalculateMetaTotalFacings(productPositions, PlanogramPositionBlockType.Main);
            this.MetaTotalXFacings = CalculateMetaTotalFacings(productPositions, PlanogramPositionBlockType.X);
            this.MetaTotalYFacings = CalculateMetaTotalFacings(productPositions, PlanogramPositionBlockType.Y);
            this.MetaTotalZFacings = CalculateMetaTotalFacings(productPositions, PlanogramPositionBlockType.Z);
            this.MetaTotalFacings = this.MetaTotalMainFacings + this.MetaTotalXFacings;

            this.MetaIsInMasterData = metadataDetails.IsProductGtinInMasterData(this.Gtin);

            this.MetaIsIllegalProduct = metadataDetails.IsProductIllegal(this.Parent, this.Gtin);
            this.MetaIsNotLegalProduct = metadataDetails.IsProductNotLegal(this.Parent, this.Gtin);

            PlanogramPerformanceData performanceData = metadataDetails.FindPerformanceDataByPlanogramProductId(this.Id);
            PlanogramInventory inventory = parentPlan.Inventory;
            this.MetaNotAchievedCases = !IsMinCasesAchieved(performanceData, inventory);
            this.MetaNotAchievedDOS = !IsMinDosAchieved(performanceData, inventory);
            this.MetaIsOverShelfLifePercent = !IsMinShelfLifeAchieved(performanceData, inventory);
            this.MetaNotAchievedDeliveries = !IsMinDeliveriesAchieved(performanceData, inventory);

            this.MetaCDTNode = GetCdtNodeName();

            //Calculate assortment rules related meta data 
            //Default all to false as no assortment rule is guranteed to be setup
            this.MetaIsProductRuleBroken = false;
            this.MetaIsFamilyRuleBroken = false;
            this.MetaIsInheritanceRuleBroken = false;
            this.MetaIsLocalProductRuleBroken = false;
            this.MetaIsDistributionRuleBroken = false;
            this.MetaIsCoreRuleBroken = false;
            this.MetaIsDelistProductRuleBroken = false;
            this.MetaIsForceProductRuleBroken = false;
            this.MetaIsPreserveProductRuleBroken = false;
            this.MetaIsMinimumHurdleProductRuleBroken = false;
            this.MetaIsMaximumProductFamilyRuleBroken = false;
            this.MetaIsMinimumProductFamilyRuleBroken = false;
            this.MetaIsDependencyFamilyRuleBroken = false;
            this.MetaIsDelistFamilyRuleBroken = false;
            this.MetaIsBuddied = false;

            //if assortment product exists
            if (assortmentProduct != null)
            {
                //Check product rules
                if (assortmentProduct.RuleType != PlanogramAssortmentProductRuleType.None)
                {
                    //Force delist
                    if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Force
                        && assortmentProduct.RuleValue == 0
                        && this.MetaTotalUnits > 0)
                    {
                        this.MetaIsProductRuleBroken = true;
                        this.MetaIsDelistProductRuleBroken = true;
                    }
                    else if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Force
                        && assortmentProduct.RuleValue > 0)
                    {
                        //Get products value facings/units
                        Int32? productValue = (assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Facings) ? this.MetaTotalFacings : this.MetaTotalUnits;
                        //If the value doesn't match that set in the force rule
                        if (productValue.Value != assortmentProduct.RuleValue.Value)
                        {
                            this.MetaIsProductRuleBroken = true;
                            this.MetaIsForceProductRuleBroken = true;
                        }
                    }
                    else if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.MinimumHurdle
                        || assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Preserve)
                    {
                        //Get products value facings/units
                        Int32? productValue = (assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Facings) ? this.MetaTotalFacings : this.MetaTotalUnits;

                        //If the value has met the minimum
                        if (productValue.Value < assortmentProduct.RuleValue)
                        {
                            this.MetaIsProductRuleBroken = true;
                            if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.MinimumHurdle)
                            {
                                this.MetaIsMinimumHurdleProductRuleBroken = true;
                            }
                            else
                            {
                                this.MetaIsPreserveProductRuleBroken = true;
                            }
                        }

                        //If maximum is set, and ranged value exceeds max
                        if (assortmentProduct.HasMaximum &&
                            (productValue.Value > assortmentProduct.RuleMaximumValue))
                        {
                            this.MetaIsProductRuleBroken = true;
                            if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.MinimumHurdle)
                            {
                                this.MetaIsMinimumHurdleProductRuleBroken = true;
                            }
                            else
                            {
                                this.MetaIsPreserveProductRuleBroken = true;
                            }
                        }
                    }
                    else if (assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Core &&
                        this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0)
                    {
                        //If core and not ranged
                        this.MetaIsProductRuleBroken = true;
                        this.MetaIsCoreRuleBroken = true;
                    }
                }

                if (assortmentProduct.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None)
                {
                    //Performed in CalculatePostPlanogramMetadata so we have meta data from all of
                    //the products that may be in the family too.
                }

                if (assortmentProduct.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Core
                    && this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0)
                {
                    //If core and not ranged - failed
                    this.MetaIsCoreRuleBroken = true;
                }

                if (assortmentProduct.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Distribution
                    && this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0)
                {
                    //If a distribution product and not ranged - failed
                    this.MetaIsDistributionRuleBroken = true;
                }

                if (assortmentProduct.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Inheritance
                    && this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0)
                {
                    //If an inheritance product and not ranged - failed
                    this.MetaIsInheritanceRuleBroken = true;
                }

                //Check local line products and plan has to be location specific
                if (!String.IsNullOrEmpty(this.Parent.LocationCode)
                   && assortmentProduct.Parent.LocalProducts.FirstOrDefault(p => p.ProductGtin.Equals(this.Gtin)) != null
                   && !assortmentProduct.Parent.LocalProducts.Where(p => p.ProductGtin.Equals(this.Gtin)).Any(p => p.LocationCode.Equals(this.Parent.LocationCode))
                   && this.MetaPositionCount.HasValue && this.MetaPositionCount.Value > 0)
                {
                    //Ff a local product for this location and not ranged
                    this.MetaIsLocalProductRuleBroken = true;
                }

                //Check if it has a buddy
                PlanogramAssortmentProductBuddy buddy = assortmentProduct.Parent.ProductBuddies.FirstOrDefault(p => p.ProductGtin == this.Gtin);
                if (assortmentProduct.Parent.ProductBuddies.FirstOrDefault(p => p.ProductGtin == this.Gtin && p.GetNumberOfSourceProducts() > 0) != null)
                {
                    this.MetaIsBuddied = true;
                }

            }

            //  Calculate Comparison related data if there is any.
            if (Parent.Comparison.Results.Any())
            {
                MetaComparisonStatus = Parent.Comparison.GetComparisonStatus(this);
            }

            //NB: Some of the meta data properties require planogram values to be calculated first.
            // See CalculatePostPlanogramMetadata method for these.

        }

        /// <summary>
        /// Calculates product meta data that relies on planogram values
        /// having being calculated first.
        /// </summary>
        internal void CalculatePostPlanogramMetadata()
        {
            Planogram parentPlan = this.Parent;
            if (parentPlan == null) return;
            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();

            IEnumerable<PlanogramPosition> productPositions = metadataDetails.FindPositionsByPlanogramProductId(this.Id);

            this.MetaTotalLinearSpace = productPositions.Sum(p => p.MetaTotalLinearSpace);
            this.MetaTotalAreaSpace = productPositions.Sum(p => p.MetaTotalAreaSpace);
            this.MetaTotalVolumetricSpace = productPositions.Sum(p => p.MetaTotalVolumetricSpace);


            // product planogram linear space percentage
            if (parentPlan.MetaTotalMerchandisableLinearSpace.HasValue)
            {
                Single percentage = parentPlan.MetaTotalMerchandisableLinearSpace > 0 ? (this.MetaTotalLinearSpace.Value / parentPlan.MetaTotalMerchandisableLinearSpace.Value) : 0;
                this.MetaPlanogramLinearSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramLinearSpacePercentage = null;
            }

            // product planogram area space percentage
            if (parentPlan.MetaTotalMerchandisableAreaSpace.HasValue)
            {
                Single percentage = parentPlan.MetaTotalMerchandisableAreaSpace > 0 ? (this.MetaTotalAreaSpace.Value / parentPlan.MetaTotalMerchandisableAreaSpace.Value) : 0;
                this.MetaPlanogramAreaSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramAreaSpacePercentage = null;
            }

            // product planogram volumetric space percentage
            if (parentPlan.MetaTotalMerchandisableVolumetricSpace.HasValue)
            {
                Single percentage =
                    parentPlan.MetaTotalMerchandisableVolumetricSpace > 0 ?
                    (this.MetaTotalVolumetricSpace.Value / parentPlan.MetaTotalMerchandisableVolumetricSpace.Value) : 0;

                this.MetaPlanogramVolumetricSpacePercentage = (Single)Math.Round(percentage, 4, MidpointRounding.AwayFromZero);
            }
            else
            {
                this.MetaPlanogramVolumetricSpacePercentage = null;
            }

            //Calculate any family rule related meta data
            PlanogramAssortmentProduct assortmentProduct = metadataDetails.FindPlanogramAssortmentProductByGtin(this.Gtin);
            if (assortmentProduct != null)
            {
                if (assortmentProduct.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None)
                {
                    //Find all planogram products in this family using caches in meta data details
                    IEnumerable<PlanogramProduct> familyPlanogramProducts = metadataDetails.FindPlanogramProductsByFamilyName(assortmentProduct.FamilyRuleName);

                    if (familyPlanogramProducts != null)
                    {
                        //If the product is in a dependency list family rule
                        if (assortmentProduct.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.DependencyList &&
                            familyPlanogramProducts.Any(p => this.MetaPositionCount.HasValue && p.MetaPositionCount > 0) &&
                            (this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0))
                        {
                            ////If the product is in a dependency list family rule, and some of the family are ranged but this isn't - Broken
                            this.MetaIsFamilyRuleBroken = true;
                            this.MetaIsDependencyFamilyRuleBroken = true;
                        }
                        else if (assortmentProduct.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.Delist &&
                            (this.MetaPositionCount.HasValue && this.MetaPositionCount.Value > 0))
                        {
                            ////If the product is in a delist list family rule, and this product is ranged - Broken
                            this.MetaIsFamilyRuleBroken = true;
                            this.MetaIsDelistFamilyRuleBroken = true;
                        }
                        else if (assortmentProduct.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MinimumProductCount
                            && familyPlanogramProducts.Count(p => (p.MetaPositionCount.HasValue && p.MetaPositionCount.Value > 0)) < assortmentProduct.FamilyRuleValue.Value
                            && (this.MetaPositionCount.HasValue && this.MetaPositionCount.Value <= 0))
                        {
                            //If the product is in a minimum product family rule, and not enough are ranged and this isn't - Broken
                            this.MetaIsFamilyRuleBroken = true;
                            this.MetaIsMinimumProductFamilyRuleBroken = true;
                        }
                        else if (assortmentProduct.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MaximumProductCount
                            && familyPlanogramProducts.Count(p => (p.MetaPositionCount.HasValue && p.MetaPositionCount.Value > 0)) > assortmentProduct.FamilyRuleValue.Value
                            && (this.MetaPositionCount.HasValue && this.MetaPositionCount.Value > 0))
                        {
                            //If the product is in a maximum product family rule, and too many are ranged and this is ranged too - Broken
                            this.MetaIsFamilyRuleBroken = true;
                            this.MetaIsMaximumProductFamilyRuleBroken = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            this.MetaNotAchievedInventory = null;
            this.MetaTotalUnits = null;
            this.MetaPlanogramLinearSpacePercentage = null;
            this.MetaPlanogramAreaSpacePercentage = null;
            this.MetaPlanogramVolumetricSpacePercentage = null;
            this.MetaPositionCount = null;
            this.MetaTotalFacings = null;
            this.MetaTotalLinearSpace = null;
            this.MetaTotalAreaSpace = null;
            this.MetaTotalVolumetricSpace = null;
            this.MetaIsInMasterData = null;
            this.MetaNotAchievedCases = null;
            this.MetaNotAchievedDOS = null;
            this.MetaIsOverShelfLifePercent = null;
            this.MetaNotAchievedDeliveries = null;
            this.MetaTotalMainFacings = null;
            this.MetaTotalXFacings = null;
            this.MetaTotalYFacings = null;
            this.MetaTotalZFacings = null;
            this.MetaIsRangedInAssortment = null;
            this.MetaCDTNode = null;
            this.MetaIsProductRuleBroken = null;
            this.MetaIsFamilyRuleBroken = null;
            this.MetaIsInheritanceRuleBroken = null;
            this.MetaIsLocalProductRuleBroken = null;
            this.MetaIsDistributionRuleBroken = null;
            this.MetaIsCoreRuleBroken = null;
            this.MetaIsDelistProductRuleBroken = null;
            this.MetaIsForceProductRuleBroken = null;
            this.MetaIsPreserveProductRuleBroken = null;
            this.MetaIsMinimumHurdleProductRuleBroken = null;
            this.MetaIsMaximumProductFamilyRuleBroken = null;
            this.MetaIsMinimumProductFamilyRuleBroken = null;
            this.MetaIsDependencyFamilyRuleBroken = null;
            this.MetaIsDelistFamilyRuleBroken = null;
            this.MetaIsIllegalProduct = null;
            this.MetaIsNotLegalProduct = null;
            this.MetaIsBuddied = null;
        }

        /// <summary>
        ///     Returns whether the product has a duplicate Gtin 
        /// in another product elsewhere in the planogram.
        /// </summary>
        private Boolean IsDuplicateGtin(PlanogramMetadataDetails context)
        {
            return context.DuplicateProductGtins.Any(s => s == this.Gtin);
        }

        /// <summary>
        ///     Returns whether the product achieves min dos in the planogram or not.
        /// </summary>
        private Boolean IsMinDosAchieved(PlanogramPerformanceData performanceData, PlanogramInventory inventory)
        {
            if (performanceData == null) return true;

            Single? achievedDos = performanceData.AchievedDos;
            if (!achievedDos.HasValue) return true;

            return achievedDos >= inventory.MinDos;
        }

        /// <summary>
        ///     Returns whether the product achieves min cases in the planogram or not.
        /// </summary>
        private Boolean IsMinCasesAchieved(PlanogramPerformanceData performanceData, PlanogramInventory inventory)
        {
            if (performanceData == null) return true;

            Single? achievedCasePacks = performanceData.AchievedCasePacks;
            if (!achievedCasePacks.HasValue) return true;

            return achievedCasePacks >= inventory.MinCasePacks;
        }

        /// <summary>
        ///     Returns whether the product achieves min delivery frequency days in the planogram or not.
        /// </summary>
        private Boolean IsMinDeliveriesAchieved(PlanogramPerformanceData performanceData, PlanogramInventory inventory)
        {
            if (performanceData == null) return true;

            Single? achievedDeliveries = performanceData.AchievedDeliveries;
            if (!achievedDeliveries.HasValue) return true;

            return achievedDeliveries >= inventory.MinDeliveries;
        }

        /// <summary>
        ///     Returns whether the product achieves min shelf life percent in the planogram or not.
        /// </summary>
        private Boolean IsMinShelfLifeAchieved(PlanogramPerformanceData performanceData, PlanogramInventory inventory)
        {
            if (performanceData == null) return true;

            Single? achievedShelfLife = performanceData.AchievedShelfLife;
            if (!achievedShelfLife.HasValue) return true;

            return achievedShelfLife <= inventory.MinShelfLife;
        }

        /// <summary>
        ///     Calculate the metadata value for <c>Total Facings</c> for the given <paramref name="positions"/> and <paramref name="blockType"/>.
        /// </summary>
        /// <param name="positions">Enumeration of <see cref="PlanogramPosition"/> items for this instance to aggregate facings for.</param>
        /// <param name="blockType">The <see cref="PlanogramPositionBlockType"/> to aggregate facings for.</param>
        /// <returns>The <c>Sum</c> of all facings wide for the given <paramref name="positions"/> and <paramref name="blockType"/>.</returns>
        private static Int32 CalculateMetaTotalFacings(IEnumerable<PlanogramPosition> positions, PlanogramPositionBlockType blockType)
        {
            switch (blockType)
            {
                case PlanogramPositionBlockType.Main:
                    return positions.Sum(position => position.FacingsWide);
                case PlanogramPositionBlockType.X:
                    return positions.Sum(position => position.FacingsXWide);
                case PlanogramPositionBlockType.Y:
                    return positions.Sum(position => position.FacingsYWide);
                case PlanogramPositionBlockType.Z:
                    return positions.Sum(position => position.FacingsZWide);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Returns the full name of the cdt node to which this product belongs.
        /// </summary>
        /// <returns></returns>
        private String GetCdtNodeName()
        {
            PlanogramConsumerDecisionTreeNode node = GetPlanogramConsumerDecisionTreeNode();
            if (node == null) return null;

            return node.Name;

            //TODO: Increase the size of db field and return this instead:
            //return node.FetchFullPathString();
        }

        #endregion

        #endregion

        #region Mapping
        /// <summary>
        /// Creates all mappings for this type
        /// </summary>        
        private static void CreateMappings()
        {
            // interface to model
            Mapper.CreateMap<IPlanogramProduct, PlanogramProduct>()
                .ForMember(dest => dest.Id, map => map.Ignore())
                .ForMember(dest => dest.CustomAttributes, map => map.MapFrom(s => s.CustomAttributes))
                .ForMember(dest => dest.CustomAttributes, map => map.UseDestinationValue());
        }
        #endregion
    }
}