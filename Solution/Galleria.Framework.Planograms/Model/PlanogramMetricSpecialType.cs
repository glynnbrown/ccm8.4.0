﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26271 : A.Kuszyk
////  Created.
//// V8-26338 : A.Kuszyk
////  Added Helper class with friendly names.
//#endregion
//#region Version History: CCM820
//// V8-30772 : J.Pickup
////  Added Promotional Sales Units
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.Planograms.Resources.Language;

//namespace Galleria.Framework.Planograms.Model
//{
//    /// <summary>
//    /// Indicates the Special Type of a Planogram Metric.
//    /// </summary>
//    public enum MetricSpecialType
//    {
//        None,
//        RegularSalesUnits,
//        ReplenishmentDays,
//        PromotionalSalesUnits
//    }

//    /// <summary>
//    /// PlanogramMetricSpecialType Helper Class
//    /// </summary>
//    public static class PlanogramMetricSpecialTypeHelper
//    {
//        /// <summary>
//        /// Dictionary with enum value as key and friendly name as value.
//        /// </summary>
//        public static readonly Dictionary<MetricSpecialType, String> FriendlyNames =
//            new Dictionary<MetricSpecialType, String>()
//            {
//                {MetricSpecialType.None, Message.Enum_PlanogramMetricSpecialType_None},
//                {MetricSpecialType.RegularSalesUnits, Message.Enum_PlanogramMetricSpecialType_RegularSalesUnits},
//                {MetricSpecialType.ReplenishmentDays, Message.Enum_PlanogramMetricSpecialType_ReplenishmentDays},
//                {MetricSpecialType.PromotionalSalesUnits, Message.Enum_PlanogramMetricSpecialType_PromotionalSalesUnits},
//            };
//    }
//}
