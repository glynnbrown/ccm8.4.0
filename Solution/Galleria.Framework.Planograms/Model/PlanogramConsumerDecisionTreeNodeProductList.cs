﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27898 : L.Ineson
//  Added notifyobjectgraph call to  OnCollectionChanged
#endregion
#region Version History: (CCM 8.20)
// V8-30926 : J.Pickup
//  Removed notifyobjectgraph call to OnCollectionChanged() because of performance and also productCDT node is being moved to metadata calcs.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Represents a list containing PlanogramConsumerDecisionTreeNodeProduct objects
    /// (Child of PlanogramConsumerDecisionTreeNode)
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeNodeProductList : ModelList<PlanogramConsumerDecisionTreeNodeProductList, PlanogramConsumerDecisionTreeNodeProduct>
    {
        #region Parent

        public PlanogramConsumerDecisionTreeNode ParentPlanogramConsumerDecisionTreeNode
        {
            get { return base.Parent as PlanogramConsumerDecisionTreeNode; }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramConsumerDecisionTreeNodeProductList NewPlanogramConsumerDecisionTreeNodeProductList()
        {
            PlanogramConsumerDecisionTreeNodeProductList item = new PlanogramConsumerDecisionTreeNodeProductList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Called when creating a new list from a list of product Gtins
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramConsumerDecisionTreeNodeProductList NewPlanogramConsumerDecisionTreeNodeProductList(IEnumerable<String> productGtinList)
        {
            PlanogramConsumerDecisionTreeNodeProductList item = new PlanogramConsumerDecisionTreeNodeProductList();
            item.Create(productGtinList);
            return item;
        }

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramConsumerDecisionTreeNodeProductList NewPlanogramConsumerDecisionTreeNodeProductList(
            PlanogramConsumerDecisionTreeNodeProductList products, PlanogramConsumerDecisionTreeNode rootNode)
        {
            PlanogramConsumerDecisionTreeNodeProductList item = new PlanogramConsumerDecisionTreeNodeProductList();
            item.Create(products, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IEnumerable<Object> planogramProductIdList)
        {
            foreach (Int32 planogramProductId in planogramProductIdList)
            {
                this.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProductId));
            }

            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramConsumerDecisionTreeNodeProductList products, PlanogramConsumerDecisionTreeNode rootNode)
        {
            //get the products assigned to the root node as this is where the child products were moved
            IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> rootProducts = rootNode.Products.ToList();

            foreach (PlanogramConsumerDecisionTreeNodeProduct product in products)
            {
                PlanogramConsumerDecisionTreeNodeProduct rootProductFound = rootProducts.FirstOrDefault(p => p.PlanogramProductId == product.PlanogramProductId);

                if (rootProductFound != null)
                {
                    this.Add(rootProductFound);
                    rootNode.Products.Remove(rootProductFound);
                }
                else
                {
                    this.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product.PlanogramProductId));
                }
            }
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            base.OnBulkCollectionChanged(e);

            if (this.ParentPlanogramConsumerDecisionTreeNode != null)
            {
                this.ParentPlanogramConsumerDecisionTreeNode.RaiseTotalProductCountChanged();
            }
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new ConsumerDecisionTreeNodeProduct to this collection for the given productId
        /// </summary>
        /// <param name="productId">The Product Id for the new item</param>
        public PlanogramConsumerDecisionTreeNodeProduct Add(PlanogramProduct product)
        {
            PlanogramConsumerDecisionTreeNodeProduct addedItem = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product);
            this.Add(addedItem);
            return addedItem;
        }

        /// <summary>
        /// Adds a collection of products to this collection
        /// </summary>
        /// <param name="productIds">The Product Ids for the new items</param>
        public void AddRange(IEnumerable<PlanogramProduct> products)
        {
            this.RaiseListChangedEvents = false;

            //cycle through adding all the items
            List<PlanogramConsumerDecisionTreeNodeProduct> addedItems = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            foreach (PlanogramProduct product in products)
            {
                addedItems.Add(this.Add(product));
            }

            this.RaiseListChangedEvents = true;

            //raise out a bulk change notification
            NotifyBulkChange(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        ///// <summary>
        ///// Removes the node product linked to the given product
        ///// </summary>
        ///// <param name="scenLocation"></param>
        //public void Remove(Product product)
        //{
        //    PlanogramConsumerDecisionTreeNodeProduct foundProd =
        //        this.Items.FirstOrDefault(l => product.Id.Equals(l.ProductId));

        //    if (foundProd != null)
        //    {
        //        this.Items.Remove(foundProd);
        //    }
        //}

        #endregion
    }
}
