﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26706 : L.Luong
//		Created
// V8-26959 : L.Luong
//      Removed methods that was using ModelPropertyDescription and INode
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
// V8-27132 : A.Kuszyk
//  Implemented common interface.
#endregion

#region Version History: (CCM 8.20)
// V8-30926 : J.Pickup
//  Performance Enhancement. 
// V8-30763 : I.George
//  Added method to calculation metaData for CDT
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
// V8-31423 : M.Shelley
//  Modified the product count metadata calculations to add the total 
//  node product count and the node placed product count
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using System.Diagnostics;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// PlanogramConsumerDecisionTreeNode Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramConsumerDecisionTreeNode : ModelObject<PlanogramConsumerDecisionTreeNode>, IPlanogramConsumerDecisionTreeNode
    {
        #region Parent

        /// <summary>
        /// Returns the parent node of this
        /// </summary>
        public PlanogramConsumerDecisionTreeNode ParentNode
        {
            get
            {
                PlanogramConsumerDecisionTreeNodeList list = base.Parent as PlanogramConsumerDecisionTreeNodeList;
                if (list != null)
                {
                    return list.ParentNode;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Returns the parent model of this
        /// </summary>
        public PlanogramConsumerDecisionTree ParentPlanogramConsumerDecisionTree
        {
            get
            {
                PlanogramConsumerDecisionTree parentConsumerDecisionTree = base.Parent as PlanogramConsumerDecisionTree;
                if (parentConsumerDecisionTree == null)
                {
                    PlanogramConsumerDecisionTreeNode currentParent = this.ParentNode;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentNode != null)
                        {
                            currentParent = currentParent.ParentNode;
                        }
                        else
                        {
                            return currentParent.ParentConsumerDecisionTree as PlanogramConsumerDecisionTree;
                        }
                    }
                }
                else
                {
                    return parentConsumerDecisionTree;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        #region Parent Property

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public PlanogramConsumerDecisionTree ParentConsumerDecisionTree
        {
            get 
            {
                PlanogramConsumerDecisionTreeNode parentNode = this.ParentPlanogramConsumerDecisionTreeNode;
                if(parentNode == null) return base.Parent as PlanogramConsumerDecisionTree;

                while(parentNode != null)
                {
                    if(parentNode.Parent is PlanogramConsumerDecisionTree)
                    {
                        return parentNode.Parent as PlanogramConsumerDecisionTree;
                    }
                    else
                    {
                        parentNode = parentNode.ParentNode;
                    }
                }
                return null; 
            }
        }

        public PlanogramConsumerDecisionTreeNode ParentPlanogramConsumerDecisionTreeNode
        {
            get 
            {
                PlanogramConsumerDecisionTreeNodeList parentList = base.Parent as PlanogramConsumerDecisionTreeNodeList;
                if(parentList != null) return parentList.Parent as PlanogramConsumerDecisionTreeNode;
                return null;
            }
        }

        #endregion

        #region Name

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The node name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Level Id

        public static readonly ModelPropertyInfo<Object> PlanogramConsumerDecisionTreeLevelIdProperty =
        RegisterModelProperty<Object>(c => c.PlanogramConsumerDecisionTreeLevelId);
        /// <summary>
        /// The id of the level this belongs to
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object PlanogramConsumerDecisionTreeLevelId
        {
            get { return GetProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty); }
            set { SetProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty, value); }
        }

        #endregion

        #region Parent Node

        public static readonly ModelPropertyInfo<Object> ParentNodeIdProperty =
        RegisterModelProperty<Object>(c => c.ParentNodeId);
        /// <summary>
        /// The ParentNodeId of the level this belongs to
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Object ParentNodeId
        {
            get { return GetProperty<Object>(ParentNodeIdProperty); }
            set { SetProperty<Object>(ParentNodeIdProperty, value); }
        }

        #endregion

        #region Child List

        public static readonly ModelPropertyInfo<PlanogramConsumerDecisionTreeNodeList> ChildListProperty =
        RegisterModelProperty<PlanogramConsumerDecisionTreeNodeList>(c => c.ChildList, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The list of child nodes
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeList ChildList
        {
            get
            {
                return GetPropertyLazy<PlanogramConsumerDecisionTreeNodeList>(
                    ChildListProperty,
                    new PlanogramConsumerDecisionTreeNodeList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of child nodes
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeList ChildListAsync
        {
            get
            {
                return GetPropertyLazy<PlanogramConsumerDecisionTreeNodeList>(
                    ChildListProperty,
                    new PlanogramConsumerDecisionTreeNodeList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region Products

        public static readonly ModelPropertyInfo<PlanogramConsumerDecisionTreeNodeProductList> ProductsProperty =
            RegisterModelProperty<PlanogramConsumerDecisionTreeNodeProductList>(c => c.Products, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The list of products assigned to this node
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeProductList Products
        {
            get
            {
                return GetPropertyLazy<PlanogramConsumerDecisionTreeNodeProductList>(
                    ProductsProperty,
                    new PlanogramConsumerDecisionTreeNodeProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The list of products assigned to this node
        /// </summary>
        public PlanogramConsumerDecisionTreeNodeProductList ProductsAsync
        {
            get
            {
                return GetPropertyLazy<PlanogramConsumerDecisionTreeNodeProductList>(
                    ProductsProperty,
                    new PlanogramConsumerDecisionTreeNodeProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region MetaDataProperties

        #region MetaCountOfProducts property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProducts);
        /// <summary>
        /// 
        /// </summary>
        public Int32? MetaCountOfProducts
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductsProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductsProperty, value); }
        }

        #endregion

        #region MetaCountOfProductRecommendedInAssortment
				
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductRecommendedInAssortmentProperty =
            RegisterModelProperty<Int32?>(p => p.MetaCountOfProductRecommendedInAssortment, Message.PlanogramConsumerDecisionTreeNode_MetaCountOfProductRecommendedInAssortment);

        public Int32? MetaCountOfProductRecommendedInAssortment
        {
            get { return GetProperty<Int32?>(MetaCountOfProductRecommendedInAssortmentProperty); }
            set { SetProperty<Int32?>(MetaCountOfProductRecommendedInAssortmentProperty, value); }
        }

        #endregion

        #region MetaCountOfProductPlacedOnPlanogram

        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductPlacedOnPlanogramProperty =
            RegisterModelProperty<Int32?>(p => p.MetaCountOfProductPlacedOnPlanogram);

        public Int32? MetaCountOfProductPlacedOnPlanogram
        {
            get { return GetProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty); }
            set { SetProperty<Int32?>(MetaCountOfProductPlacedOnPlanogramProperty, value); }
        }

        #endregion

        #region MetaLinearSpaceAllocatedToProductsOnPlanogram

        public static readonly ModelPropertyInfo<Single?> MetaLinearSpaceAllocatedToProductsOnPlanogramProperty =
            RegisterModelProperty<Single?>(p => p.MetaLinearSpaceAllocatedToProductsOnPlanogram);

        public Single? MetaLinearSpaceAllocatedToProductsOnPlanogram
        {
            get { return GetProperty<Single?>(MetaLinearSpaceAllocatedToProductsOnPlanogramProperty); }
            set { SetProperty<Single?>(MetaLinearSpaceAllocatedToProductsOnPlanogramProperty, value); }
        }

        #endregion

        #region MetaAreaSpaceAllocatedToProductsOnPlanogram

        public static readonly ModelPropertyInfo<Single?> MetaAreaSpaceAllocatedToProductsOnPlanogramProperty =
            RegisterModelProperty<Single?>(p => p.MetaAreaSpaceAllocatedToProductsOnPlanogram);

        public Single? MetaAreaSpaceAllocatedToProductsOnPlanogram
        {
            get { return GetProperty<Single?>(MetaAreaSpaceAllocatedToProductsOnPlanogramProperty); }
            set { SetProperty<Single?>(MetaAreaSpaceAllocatedToProductsOnPlanogramProperty, value); }
        }

        #endregion

        #region MetaVolumetricSpaceAllocatedToProductsOnPlanogram

        public static readonly ModelPropertyInfo<Single?> MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty =
            RegisterModelProperty<Single?>(p => p.MetaVolumetricSpaceAllocatedToProductsOnPlanogram);

        public Single? MetaVolumetricSpaceAllocatedToProductsOnPlanogram
        {
            get { return GetProperty<Single?>(MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty); }
            set { SetProperty<Single?>(MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty, value); }
        }

        #endregion

        #region Performance Metadata properties

        #region MetaP1 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP1Property =
            RegisterModelProperty<Single?>(c => c.MetaP1);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP1
        {
            get { return this.GetProperty<Single?>(MetaP1Property); }
            set { this.SetProperty<Single?>(MetaP1Property, value); }
        }

        #endregion

        #region MetaP2 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP2Property =
            RegisterModelProperty<Single?>(c => c.MetaP2);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP2
        {
            get { return this.GetProperty<Single?>(MetaP2Property); }
            set { this.SetProperty<Single?>(MetaP2Property, value); }
        }

        #endregion

        #region MetaP3 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP3Property =
            RegisterModelProperty<Single?>(c => c.MetaP3);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP3
        {
            get { return this.GetProperty<Single?>(MetaP3Property); }
            set { this.SetProperty<Single?>(MetaP3Property, value); }
        }

        #endregion

        #region MetaP4 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP4Property =
            RegisterModelProperty<Single?>(c => c.MetaP4);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP4
        {
            get { return this.GetProperty<Single?>(MetaP4Property); }
            set { this.SetProperty<Single?>(MetaP4Property, value); }
        }

        #endregion

        #region MetaP5 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP5Property =
            RegisterModelProperty<Single?>(c => c.MetaP5);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP5
        {
            get { return this.GetProperty<Single?>(MetaP5Property); }
            set { this.SetProperty<Single?>(MetaP5Property, value); }
        }

        #endregion

        #region MetaP6 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP6Property =
            RegisterModelProperty<Single?>(c => c.MetaP6);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP6
        {
            get { return this.GetProperty<Single?>(MetaP6Property); }
            set { this.SetProperty<Single?>(MetaP6Property, value); }
        }

        #endregion

        #region MetaP7 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP7Property =
            RegisterModelProperty<Single?>(c => c.MetaP7);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP7
        {
            get { return this.GetProperty<Single?>(MetaP7Property); }
            set { this.SetProperty<Single?>(MetaP7Property, value); }
        }

        #endregion

        #region MetaP8 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP8Property =
            RegisterModelProperty<Single?>(c => c.MetaP8);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP8
        {
            get { return this.GetProperty<Single?>(MetaP8Property); }
            set { this.SetProperty<Single?>(MetaP8Property, value); }
        }

        #endregion

        #region MetaP9 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP9Property =
            RegisterModelProperty<Single?>(c => c.MetaP9);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP9
        {
            get { return this.GetProperty<Single?>(MetaP9Property); }
            set { this.SetProperty<Single?>(MetaP9Property, value); }
        }

        #endregion

        #region MetaP10 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP10Property =
            RegisterModelProperty<Single?>(c => c.MetaP10);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP10
        {
            get { return this.GetProperty<Single?>(MetaP10Property); }
            set { this.SetProperty<Single?>(MetaP10Property, value); }
        }

        #endregion

        #region MetaP11 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP11Property =
            RegisterModelProperty<Single?>(c => c.MetaP11);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP11
        {
            get { return this.GetProperty<Single?>(MetaP11Property); }
            set { this.SetProperty<Single?>(MetaP11Property, value); }
        }

        #endregion

        #region MetaP12 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP12Property =
            RegisterModelProperty<Single?>(c => c.MetaP12);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP12
        {
            get { return this.GetProperty<Single?>(MetaP12Property); }
            set { this.SetProperty<Single?>(MetaP12Property, value); }
        }

        #endregion

        #region MetaP13 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP13Property =
            RegisterModelProperty<Single?>(c => c.MetaP13);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP13
        {
            get { return this.GetProperty<Single?>(MetaP13Property); }
            set { this.SetProperty<Single?>(MetaP13Property, value); }
        }

        #endregion

        #region MetaP14 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP14Property =
            RegisterModelProperty<Single?>(c => c.MetaP14);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP14
        {
            get { return this.GetProperty<Single?>(MetaP14Property); }
            set { this.SetProperty<Single?>(MetaP14Property, value); }
        }

        #endregion

        #region MetaP15 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP15Property =
            RegisterModelProperty<Single?>(c => c.MetaP15);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP15
        {
            get { return this.GetProperty<Single?>(MetaP15Property); }
            set { this.SetProperty<Single?>(MetaP15Property, value); }
        }

        #endregion

        #region MetaP16 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP16Property =
            RegisterModelProperty<Single?>(c => c.MetaP16);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP16
        {
            get { return this.GetProperty<Single?>(MetaP16Property); }
            set { this.SetProperty<Single?>(MetaP16Property, value); }
        }

        #endregion

        #region MetaP17 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP17Property =
            RegisterModelProperty<Single?>(c => c.MetaP17);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP17
        {
            get { return this.GetProperty<Single?>(MetaP17Property); }
            set { this.SetProperty<Single?>(MetaP17Property, value); }
        }

        #endregion

        #region MetaP18 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP18Property =
            RegisterModelProperty<Single?>(c => c.MetaP18);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP18
        {
            get { return this.GetProperty<Single?>(MetaP18Property); }
            set { this.SetProperty<Single?>(MetaP18Property, value); }
        }

        #endregion

        #region MetaP19 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP19Property =
            RegisterModelProperty<Single?>(c => c.MetaP19);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP19
        {
            get { return this.GetProperty<Single?>(MetaP19Property); }
            set { this.SetProperty<Single?>(MetaP19Property, value); }
        }

        #endregion

        #region MetaP20 property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP20Property =
            RegisterModelProperty<Single?>(c => c.MetaP20);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP20
        {
            get { return this.GetProperty<Single?>(MetaP20Property); }
            set { this.SetProperty<Single?>(MetaP20Property, value); }
        }

        #endregion

        #region MetaP1Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP1PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP1Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP1Percentage
        {
            get { return this.GetProperty<Single?>(MetaP1PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP1PercentageProperty, value); }
        }

        #endregion

        #region MetaP2Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP2PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP2Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP2Percentage
        {
            get { return this.GetProperty<Single?>(MetaP2PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP2PercentageProperty, value); }
        }

        #endregion

        #region MetaP3Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP3PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP3Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP3Percentage
        {
            get { return this.GetProperty<Single?>(MetaP3PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP3PercentageProperty, value); }
        }

        #endregion

        #region MetaP4Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP4PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP4Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP4Percentage
        {
            get { return this.GetProperty<Single?>(MetaP4PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP4PercentageProperty, value); }
        }

        #endregion

        #region MetaP5Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP5PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP5Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP5Percentage
        {
            get { return this.GetProperty<Single?>(MetaP5PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP5PercentageProperty, value); }
        }

        #endregion

        #region MetaP6Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP6PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP6Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP6Percentage
        {
            get { return this.GetProperty<Single?>(MetaP6PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP6PercentageProperty, value); }
        }

        #endregion

        #region MetaP7Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP7PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP7Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP7Percentage
        {
            get { return this.GetProperty<Single?>(MetaP7PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP7PercentageProperty, value); }
        }

        #endregion

        #region MetaP8Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP8PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP8Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP8Percentage
        {
            get { return this.GetProperty<Single?>(MetaP8PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP8PercentageProperty, value); }
        }

        #endregion

        #region MetaP9Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP9PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP9Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP9Percentage
        {
            get { return this.GetProperty<Single?>(MetaP9PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP9PercentageProperty, value); }
        }

        #endregion

        #region MetaP10Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP10PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP10Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP10Percentage
        {
            get { return this.GetProperty<Single?>(MetaP10PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP10PercentageProperty, value); }
        }

        #endregion

        #region MetaP11Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP11PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP11Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP11Percentage
        {
            get { return this.GetProperty<Single?>(MetaP11PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP11PercentageProperty, value); }
        }

        #endregion

        #region MetaP12Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP12PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP12Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP12Percentage
        {
            get { return this.GetProperty<Single?>(MetaP12PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP12PercentageProperty, value); }
        }

        #endregion

        #region MetaP13Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP13PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP13Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP13Percentage
        {
            get { return this.GetProperty<Single?>(MetaP13PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP13PercentageProperty, value); }
        }

        #endregion

        #region MetaP14Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP14PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP14Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP14Percentage
        {
            get { return this.GetProperty<Single?>(MetaP14PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP14PercentageProperty, value); }
        }

        #endregion

        #region MetaP15Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP15PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP15Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP15Percentage
        {
            get { return this.GetProperty<Single?>(MetaP15PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP15PercentageProperty, value); }
        }

        #endregion

        #region MetaP16Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP16PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP16Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP16Percentage
        {
            get { return this.GetProperty<Single?>(MetaP16PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP16PercentageProperty, value); }
        }

        #endregion

        #region MetaP17Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP17PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP17Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP17Percentage
        {
            get { return this.GetProperty<Single?>(MetaP17PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP17PercentageProperty, value); }
        }

        #endregion

        #region MetaP18Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP18PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP18Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP18Percentage
        {
            get { return this.GetProperty<Single?>(MetaP18PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP18PercentageProperty, value); }
        }

        #endregion

        #region MetaP19Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP19PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP19Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP19Percentage
        {
            get { return this.GetProperty<Single?>(MetaP19PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP19PercentageProperty, value); }
        }

        #endregion

        #region MetaP20Percentage property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaP20PercentageProperty =
            RegisterModelProperty<Single?>(c => c.MetaP20Percentage);
        /// <summary>
        /// 
        /// </summary>
        public Single? MetaP20Percentage
        {
            get { return this.GetProperty<Single?>(MetaP20PercentageProperty); }
            set { this.SetProperty<Single?>(MetaP20PercentageProperty, value); }
        }

        #endregion

        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns true if this is the root unit
        /// </summary>
        public Boolean IsRoot
        {
            get { return this.ParentNode == null; }
        }

        //#region Total Product Count

        /// <summary>
        /// Returns the total number of products
        /// below this node in the mode
        /// </summary>
        public Int32 TotalProductCount
        {
            get
            {
                return this.ChildList.TotalProductCount + this.Products.Count;
            }
        }

        /// <summary>
        /// Notifies that the TotalProductCount property has changed
        /// and also raise the notification upwards
        /// </summary>
        public void RaiseTotalProductCountChanged()
        {
            OnPropertyChanged("TotalProductCount");

            if (this.ParentNode != null)
            {
                this.ParentNode.RaiseTotalProductCountChanged();
            }
        }

        /// <summary>
        /// Returns the full name of the node.
        /// This is a concatonation of the n
        /// </summary>
        /// 

        public String FullName
        {
            get
            {
                String value = this.Name;

                PlanogramConsumerDecisionTreeNode node = this;
                while (node.ParentNode != null && !node.ParentNode.IsRoot)
                {
                    String parentName = node.ParentNode.Name;

                    //if ((value.Length + parentName.Length + /*spacing*/3) <= 300)
                    //{
                    value = String.Format(CultureInfo.CurrentCulture, "{0} - {1}", parentName, value);

                    node = node.ParentNode;
                    //}
                    //else { break; }
                }

                return value;
            }
        }

        #endregion

        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Node from the node given, preparing it for the level provided.
        /// </summary>
        /// <param name="node">The node to copy.</param>
        /// <param name="level">The level of the new node.</param>
        /// <param name="planogram">The parent planogram of the CDT.</param>
        /// <returns>A new node.</returns>
        /// <exception cref="ArgumentNullException">Thrown if the given node is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if the given level is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if the given planogram is null.</exception>
        public static PlanogramConsumerDecisionTreeNode NewPlanogramConsumerDecisionTreeNode(
            IPlanogramConsumerDecisionTreeNode node, PlanogramConsumerDecisionTreeLevel level, Planogram planogram)
        {
            if (node == null) throw new ArgumentNullException("node cannot be null");
            if (level == null) throw new ArgumentNullException("level cannot be null");
            if (planogram == null) throw new ArgumentNullException("planogram cannot be null");

            PlanogramConsumerDecisionTreeNode item = new PlanogramConsumerDecisionTreeNode();
            item.Create(node, level, planogram);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static PlanogramConsumerDecisionTreeNode NewPlanogramConsumerDecisionTreeNode(Object levelId)
        {
            PlanogramConsumerDecisionTreeNode item = new PlanogramConsumerDecisionTreeNode();
            item.Create(levelId);
            return item;
        }

        public static PlanogramConsumerDecisionTreeNode NewPlanogramConsumerDecisionTreeNode
            (PlanogramConsumerDecisionTreeNode node, PlanogramConsumerDecisionTreeNode rootNode)
        {
            PlanogramConsumerDecisionTreeNode item = new PlanogramConsumerDecisionTreeNode();
            item.Create(node, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Object levelId)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());

            LoadProperty<PlanogramConsumerDecisionTreeNodeList>(ChildListProperty, PlanogramConsumerDecisionTreeNodeList.NewPlanogramConsumerDecisionTreeNodeList());
            LoadProperty<PlanogramConsumerDecisionTreeNodeProductList>(ProductsProperty, PlanogramConsumerDecisionTreeNodeProductList.NewPlanogramConsumerDecisionTreeNodeProductList());

            try
            {
                this.PlanogramConsumerDecisionTreeLevelId = levelId;
            }
            catch (InvalidCastException e)
            {
                throw new ArgumentOutOfRangeException("levelId", e, "The given levelId was not of type Int32.");
            }

            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Creates a Node by copying the node given for the level given.
        /// </summary>
        /// <param name="node">The node to copy.</param>
        /// <param name="level">The level of the new node.</param>
        private void Create(IPlanogramConsumerDecisionTreeNode node, PlanogramConsumerDecisionTreeLevel level, Planogram planogram)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(NameProperty, node.Name);
            this.PlanogramConsumerDecisionTreeLevelId = level.Id;

            LoadProperty<PlanogramConsumerDecisionTreeNodeList>(ChildListProperty, PlanogramConsumerDecisionTreeNodeList.NewPlanogramConsumerDecisionTreeNodeList());
            ChildList.AddRange(node.ChildList.Select(n => PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(n, level.ChildLevel, planogram)));

            LoadProperty<PlanogramConsumerDecisionTreeNodeProductList>(ProductsProperty, PlanogramConsumerDecisionTreeNodeProductList.NewPlanogramConsumerDecisionTreeNodeProductList());
            Products.AddRange(node.Products.
                Where(np => planogram.Products.Any(p => p.Gtin == np.ProductGtin)).
                Select(np => PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(np, planogram)));

            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramConsumerDecisionTreeNode node, PlanogramConsumerDecisionTreeNode rootNode)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, node.Name);

            //Set the level id to the node's for now
            this.PlanogramConsumerDecisionTreeLevelId = node.PlanogramConsumerDecisionTreeLevelId;

            LoadProperty<PlanogramConsumerDecisionTreeNodeList>(ChildListProperty,
                PlanogramConsumerDecisionTreeNodeList.NewPlanogramConsumerDecisionTreeNodeList(node.ChildList, rootNode));
            LoadProperty<PlanogramConsumerDecisionTreeNodeProductList>(ProductsProperty,
                PlanogramConsumerDecisionTreeNodeProductList.NewPlanogramConsumerDecisionTreeNodeProductList(node.Products, rootNode));

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            var oldId = this.Id;
            var newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramConsumerDecisionTreeNode>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramConsumerDecisionTreeLevelId
            Object planogramConsumerDecisionTreeLevelId = context.ResolveId<PlanogramConsumerDecisionTreeLevel>(this.ReadProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty));
            if (planogramConsumerDecisionTreeLevelId != null) this.LoadProperty<Object>(PlanogramConsumerDecisionTreeLevelIdProperty, planogramConsumerDecisionTreeLevelId);
        }
        #endregion

        #region Helper Methods

        public void ReplaceConsumerDecisionTreeNode(IPlanogramConsumerDecisionTreeNode replacementNode)
        {
            if (ParentConsumerDecisionTree == null || ParentConsumerDecisionTree.Parent == null) throw new InvalidOperationException("Parent cannot be null");
            var planogram = ParentConsumerDecisionTree.Parent;
            var level = ParentConsumerDecisionTree.FetchAllLevels().First(l => l.Id.Equals(PlanogramConsumerDecisionTreeLevelId));
            if (level == null) throw new InvalidOperationException("this node must have a corresponding level");

            Name = replacementNode.Name;

            // Clear and re-add Child Nodes.
            ChildList.Clear();
            ChildList.AddRange(replacementNode.ChildList.
                Select(replacementChild => NewPlanogramConsumerDecisionTreeNode(replacementChild, level.ChildLevel, planogram)));

            // Clear and re-add Products.
            Products.Clear();
            Products.AddRange(replacementNode.Products.
                Where(np => planogram.Products.Any(p => p.Gtin == np.ProductGtin)).
                Select(np => PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(np, planogram)));
        }

        /// <summary>
        /// Returns a list  of this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramConsumerDecisionTreeNode> FetchAllChildNodes()
        {
            IEnumerable<PlanogramConsumerDecisionTreeNode> returnList =
                new List<PlanogramConsumerDecisionTreeNode>() { this };

            foreach (PlanogramConsumerDecisionTreeNode child in this.ChildList)
            {
                returnList = returnList.Union(child.FetchAllChildNodes());
            }

            return returnList;
        }

        /// <summary>
        /// Returns a list of all products which sit under this node
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> FetchAllChildPlanogramConsumerDecisionTreeNodeProducts()
        {
            IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> returnList = this.Products;

            foreach (PlanogramConsumerDecisionTreeNode child in this.ChildList)
            {
                returnList = returnList.Union(child.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts());
            }

            return returnList;
        }

        /// <summary>
        /// Creates a list of nodes representing a path down to the requested child
        /// </summary>
        /// <returns></returns>
        public List<PlanogramConsumerDecisionTreeNode> FetchParentPath()
        {
            List<PlanogramConsumerDecisionTreeNode> inversePathList = new List<PlanogramConsumerDecisionTreeNode>();
            PlanogramConsumerDecisionTreeNode currentParent = this.ParentNode;

            while (currentParent != null)
            {
                inversePathList.Add(currentParent);
                currentParent = currentParent.ParentNode;
            }

            //invert the list so the top parent is the first item
            List<PlanogramConsumerDecisionTreeNode> pathList = new List<PlanogramConsumerDecisionTreeNode>();
            int currentIndex = inversePathList.Count - 1;
            while (currentIndex >= 0)
            {
                pathList.Add(inversePathList[currentIndex]);
                currentIndex--;
            }

            return pathList;
        }

        /// <summary>
        /// Retruns a string showing the full name of this node 
        /// including parent nodes.
        /// </summary>
        /// <returns></returns>
        public String FetchFullPathString()
        {
            StringBuilder sb = new StringBuilder();

            foreach(var parent in FetchParentPath())
            {
                if (parent.ParentNode == null) continue;

                sb.Append(parent.Name);
                sb.Append('/');
            }

            sb.Append(this.Name);

            return sb.ToString();
        }


        /// <summary>
        /// Returns the percentage of products held by this node in relation to its parent
        /// </summary>
        /// <returns></returns>
        public Double GetPercentOfParent()
        {
            if (this.ParentNode != null)
            {
                if (this.ParentNode.TotalProductCount == 0)
                {
                    //divide to parent size by the number of children as they all have 0 products
                    return 100 / (Double)this.ParentNode.ChildList.Count;
                }
                else if (this.TotalProductCount == 0)
                {
                    return 0;
                }
                else
                {
                    return ((Double)this.TotalProductCount /
                    (Double)this.ParentNode.TotalProductCount) * 100;
                }
            }
            else
            {
                return 100;
            }
        }

        /// <summary>
        /// Splits the node based on the given product related properties
        /// </summary>
        /// <param name="propertyDescriptionList"></param>
        /// <param name="masterProductsList"></param>
        public PlanogramConsumerDecisionTreeNode SplitByProductProperties(
            IEnumerable<String> propertyDescriptionList,
            IEnumerable<PlanogramProduct> masterProductsList)
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            PlanogramConsumerDecisionTreeNode returnNullValueNode = null;

            //firstly remove all child nodes as we are going to re-split
            if (this.ChildList.Count > 0)
            {
                this.ChildList.Clear();
            }

            if (propertyDescriptionList.Any())
            {
                String splitDescriptionString = String.Empty;
                foreach (String desc in propertyDescriptionList)
                {
                    if (!String.IsNullOrEmpty(splitDescriptionString))
                    {
                        splitDescriptionString = String.Format("{0} {1}", splitDescriptionString, desc);
                    }
                    else
                    {
                        splitDescriptionString = desc;
                    }
                }

                //create the property value groupings
                IEnumerable<Object> prodIds = this.Products.Select(l => l.PlanogramProductId);
                IEnumerable<PlanogramProduct> referencedProds = masterProductsList.Where(l => prodIds.Contains(l.Id));

                IEnumerable<IGrouping<Object, PlanogramProduct>> groupings =
                    referencedProds.GroupBy(p => CreateCombinedPropertiesKey(p, propertyDescriptionList));

                //Check for a child level
                PlanogramConsumerDecisionTreeLevel childLevel;
                PlanogramConsumerDecisionTreeLevel parentLevel = this.ParentPlanogramConsumerDecisionTree.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.PlanogramConsumerDecisionTreeLevelId));
                if (parentLevel.ChildLevel == null)
                {
                    childLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
                    parentLevel.ChildLevel = childLevel;
                }
                else
                {
                    childLevel = parentLevel.ChildLevel;
                }

                //V8:30926 JP:Create a buffer to hold the items to add - Very important for performance do not remove.
                List<PlanogramConsumerDecisionTreeNode> cdtNodeBuffer = new List<PlanogramConsumerDecisionTreeNode>();

                //cycle through creating the group nodes
                foreach (IGrouping<Object, PlanogramProduct> grp in groupings.OrderBy(g => g.Key))
                {
                    Boolean isNullValueNode = false;

                    Object value = grp.Key;
                    if (value == null || value.ToString() == String.Empty)
                    {
                        isNullValueNode = true;
                        value = Message.PlanogramConsumerDecisionTreeNode_NullNodeName;
                    }

                    //create a node
                    PlanogramConsumerDecisionTreeNode childNode = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(childLevel.Id);
                    childNode.Name = value.ToString();
                    cdtNodeBuffer.Add(childNode);

                    List<PlanogramConsumerDecisionTreeNodeProduct> cdtChildNodeProductBuffer = new List<PlanogramConsumerDecisionTreeNodeProduct>();
                    
                    //add in all the products
                    foreach (PlanogramProduct p in grp)
                    {
                        PlanogramConsumerDecisionTreeNodeProduct cdtProduct = this.Products.FirstOrDefault(q => q.PlanogramProductId == p.Id);
                        if (cdtProduct != null)
                        {
                            cdtChildNodeProductBuffer.Add(cdtProduct);
                        }
                        else
                        {
                            cdtChildNodeProductBuffer.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(p.Id));
                        }
                    }

                    //Add range all at once. 
                    childNode.Products.AddRange(cdtChildNodeProductBuffer);

                    //If this is a null value node
                    if (isNullValueNode)
                    {
                        //Set the return value to be this node
                        returnNullValueNode = childNode;
                    }
                }

                //Load in the buffered data.
                this.ChildList.AddRange(cdtNodeBuffer);

                //Update level name
                UpdateLevelName(childLevel, splitDescriptionString);
            }

            //remove all products from this node as we only need store them at the lowest level
            this.Products.Clear();

            this.ChildList.RaiseListChangedEvents = true;
            this.ChildList.Reset();
            this.Products.RaiseListChangedEvents = true;
            this.Products.Reset();

            //Return the null value node
            return returnNullValueNode;
        }

        /// <summary>
        /// Creates the value key for a product based on the given property descriptions
        /// </summary>
        /// <param name="prod"></param>
        /// <param name="propertyDescriptions"></param>
        /// <returns></returns>
        private Object CreateCombinedPropertiesKey(PlanogramProduct prod, IEnumerable<String> propertyDescriptions)
        {
            Object propertyKey = null;

            foreach (String p in propertyDescriptions)
            {
                if (p != null)
                {
                    Object value = null;
                    Object foundProperty = null;

                    foundProperty =
                        Galleria.Framework.Helpers.WpfHelper.GetItemPropertyValue(prod, p);
                    if (foundProperty != null)
                    {
                        value = foundProperty.ToString();
                    }

                    if (value != null)
                    {
                        if (propertyKey != null)
                        {
                            propertyKey = String.Format("{0}, {1}", propertyKey, value);
                        }
                        else
                        {
                            //[20165]Added code so if data is empty string, flags up as null
                            if (value.ToString() == String.Empty)
                            {
                                value = null;
                            }

                            propertyKey = value;
                        }
                    }

                }
            }

            return propertyKey;
        }

        /// <summary>
        /// Creates a new child node using this node's products
        /// </summary>
        public PlanogramConsumerDecisionTreeNode AddNewChildNode()
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            PlanogramConsumerDecisionTreeNode childNode = null;

            //Get the parent level
            PlanogramConsumerDecisionTreeLevel parentLevel = this.ParentPlanogramConsumerDecisionTree.GetLinkedLevel(this);

            if (parentLevel != null)
            {
                PlanogramConsumerDecisionTreeLevel newLevel;

                //A new level is required for the new node
                if (parentLevel.ChildLevel == null)
                {
                    newLevel = this.ParentPlanogramConsumerDecisionTree.InsertNewChildLevel(parentLevel);
                }
                else
                {
                    //use the current child level
                    newLevel = parentLevel.ChildLevel;
                }

                //Create the child node
                childNode = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(newLevel.Id);
                //set name
                //find out the other level names
                List<String> siblingNames = new List<String>();
                String name = Message.PlanogramCdtMaintenance_NewNode;
                Int32 nameNumber = 1;


                foreach (PlanogramConsumerDecisionTreeNode node in ChildList)
                {
                    siblingNames.Add(node.Name.ToUpperInvariant());
                }
                if (siblingNames.Contains(name.ToUpperInvariant()))
                {
                    while (siblingNames.Contains(name.ToUpperInvariant() + " " + nameNumber))
                    {
                        nameNumber++;
                    }
                    childNode.Name = name + " " + nameNumber;
                }
                else
                {
                    childNode.Name = name;
                }

                childNode.Products.AddList(this.Products);
                this.ChildList.Add(childNode);

                //remove all products from this node as we only need store them at the lowest level
                this.Products.Clear();

                this.ChildList.RaiseListChangedEvents = true;
                this.ChildList.Reset();
                this.Products.RaiseListChangedEvents = true;
                this.Products.Reset();
            }
            return childNode;
        }

        /// <summary>
        /// Updates the details of this node from the given decision tree node.
        /// </summary>
        public void MergeFrom(PlanogramConsumerDecisionTreeNode node)
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            PlanogramConsumerDecisionTreeNode mergeRootNode = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(node, this);
            mergeRootNode.Name = node.ParentPlanogramConsumerDecisionTree.Name;

            this.ChildList.Add(mergeRootNode);

            //get a list of allocated products that are assigned to a child node
            IEnumerable<Object> allocatedPlanogramProductIds = new List<Object>();
            foreach (PlanogramConsumerDecisionTreeNode child in this.ChildList)
            {
                allocatedPlanogramProductIds = allocatedPlanogramProductIds.Union(child.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts().Select(s => s.PlanogramProductId));
            }

            //remove all these from the root so we are just left with the root products and any that are unallocated.
            this.Products.RemoveList(
                this.Products.Where(p => allocatedPlanogramProductIds.Contains(p.PlanogramProductId)).ToList());

            this.ChildList.RaiseListChangedEvents = true;
            this.ChildList.Reset();
            this.Products.RaiseListChangedEvents = true;
            this.Products.Reset();
        }

        /// <summary>
        /// Method to update the level name upon splitting a node
        /// </summary>
        /// <param name="level"></param>
        /// <param name="splitDescriptionString"></param>
        /// <remarks>This should only be used when splitting as there could be multiple split 
        /// by properties in a single level, requiring the custom name to be set.</remarks>
        public void UpdateLevelName(PlanogramConsumerDecisionTreeLevel level, String splitDescriptionString)
        {
            //Update level name if the split description is different
            if (level.Name != splitDescriptionString)
            {
                //If split description is valid, and string is not empty
                if (splitDescriptionString != "")
                {
                    //If name is empty - set to split description string
                    if (String.IsNullOrWhiteSpace(level.Name))
                    {
                        //Set it to split description
                        level.Name = splitDescriptionString;
                    }
                    else
                    {
                        //If the level name doesn't contain the split description
                        if (!level.Name.ToUpperInvariant().Contains(splitDescriptionString.ToUpperInvariant()))
                        {
                            //Append it to the end
                            level.Name = String.Format("{0} - {1}", level.Name, splitDescriptionString);
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to handle removing the node
        /// </summary>
        public void RemoveNode()
        {
            //Fetch levels
            IEnumerable<PlanogramConsumerDecisionTreeLevel> levels = this.ParentPlanogramConsumerDecisionTree.FetchAllLevels();

            //Find node level
            PlanogramConsumerDecisionTreeLevel level = levels.FirstOrDefault(p => p.Id.Equals(this.Id));

            //Fetch nodes at this level
            IEnumerable<PlanogramConsumerDecisionTreeNode> levelNodes = this.ParentPlanogramConsumerDecisionTree.FetchAllNodes().Where(p => p.Id.Equals(this.PlanogramConsumerDecisionTreeLevelId) && p != this);

            //Check if this is the last node at level, if so, remove level
            if (!levelNodes.Any())
            {
                if (level != null)
                {
                    //Remove level
                    this.ParentPlanogramConsumerDecisionTree.RemoveLevel(level, true);
                }
            }
            else
            {
                //Otherwise remove node normally
                this.ParentNode.ChildList.Remove(this);

                //Update level name
                UpdateLevelName(level, "");
            }

            if (level != null)
            {
                //Get any child levels
                IEnumerable<PlanogramConsumerDecisionTreeLevel> childLevels = this.ParentPlanogramConsumerDecisionTree.FetchAllLevels().Where(p => p.LevelNumber > level.LevelNumber);

                //If lower child levels exist, clear the ones up that dont have any nodes assigned
                if (childLevels.Any())
                {
                    //Get all nodes
                    IEnumerable<PlanogramConsumerDecisionTreeNode> currentNodes = this.ParentPlanogramConsumerDecisionTree.FetchAllNodes();

                    //Enumerate through levels, starting with lowest
                    foreach (PlanogramConsumerDecisionTreeLevel childLevel in childLevels.OrderByDescending(p => p.LevelNumber))
                    {
                        //If there are no nodes at this level
                        if (!currentNodes.Any(p => p.Id.Equals(childLevel.Id)))
                        {
                            //Remove child level
                            this.ParentPlanogramConsumerDecisionTree.RemoveLevel(childLevel, true);
                        }
                    }
                }
            }
        }

        #region Metadata Calculation

        protected override void OnCalculateMetadata()
        {
            PlanogramMetadataDetails metadataDetails = this.ParentConsumerDecisionTree.Parent.GetPlanogramMetadataDetails();

            this.MetaCountOfProducts = metadataDetails.CalculateCountOfNodeProduct(this);
            this.MetaCountOfProductRecommendedInAssortment = metadataDetails.CalculateNodeRecommendedAssortmentProductCount(this);
            this.MetaCountOfProductPlacedOnPlanogram = metadataDetails.CalculatePlacedProductCount(this);
        }

        internal void CalculatePostMetaData()
        {
            PlanogramMetadataDetails metadataDetails = this.ParentConsumerDecisionTree.Parent.GetPlanogramMetadataDetails();

            var childNodes = this.ChildList;
            foreach (var subNode in childNodes)
            {
                if (subNode.Equals(this)) continue;

                subNode.CalculatePostMetaData();
            }

            // Add actual space of the products in the current node
            var nodeProductsList = metadataDetails.FetchChildNodeProducts(this);

            this.MetaLinearSpaceAllocatedToProductsOnPlanogram = nodeProductsList.Sum(x => x.MetaTotalLinearSpace);
            this.MetaAreaSpaceAllocatedToProductsOnPlanogram = nodeProductsList.Sum(x => x.MetaTotalAreaSpace);
            this.MetaVolumetricSpaceAllocatedToProductsOnPlanogram = nodeProductsList.Sum(x => x.MetaTotalVolumetricSpace);

            // Work out CDT node performance
            metadataDetails.CalculateCDTPerformance(this);
        }

        protected override void ClearMetadata()
        {
            this.MetaCountOfProductPlacedOnPlanogram = null;
            this.MetaCountOfProductRecommendedInAssortment = null;
            this.MetaAreaSpaceAllocatedToProductsOnPlanogram = null;
            this.MetaLinearSpaceAllocatedToProductsOnPlanogram = null;
            this.MetaVolumetricSpaceAllocatedToProductsOnPlanogram = null;

            // Clear the node performance meta values
            this.MetaP1 = null;
            this.MetaP2 = null;
            this.MetaP3 = null;
            this.MetaP4 = null;
            this.MetaP5 = null;
            this.MetaP6 = null;
            this.MetaP7 = null;
            this.MetaP8 = null;
            this.MetaP9 = null;
            this.MetaP10 = null;
            this.MetaP11 = null;
            this.MetaP12 = null;
            this.MetaP13 = null;
            this.MetaP14 = null;
            this.MetaP15 = null;
            this.MetaP16 = null;
            this.MetaP17 = null;
            this.MetaP18 = null;
            this.MetaP19 = null;
            this.MetaP20 = null;

            // Clear the node performance meta percentage values
            this.MetaP1Percentage = null;
            this.MetaP2Percentage = null;
            this.MetaP3Percentage = null;
            this.MetaP4Percentage = null;
            this.MetaP5Percentage = null;
            this.MetaP6Percentage = null;
            this.MetaP7Percentage = null;
            this.MetaP8Percentage = null;
            this.MetaP9Percentage = null;
            this.MetaP10Percentage = null;
            this.MetaP11Percentage = null;
            this.MetaP12Percentage = null;
            this.MetaP13Percentage = null;
            this.MetaP14Percentage = null;
            this.MetaP15Percentage = null;
            this.MetaP16Percentage = null;
            this.MetaP17Percentage = null;
            this.MetaP18Percentage = null;
            this.MetaP19Percentage = null;
            this.MetaP20Percentage = null;
        }

        #endregion

        #endregion

        #region IPlanogramConsumerDecisionTreeNode members

        IEnumerable<IPlanogramConsumerDecisionTreeNode> IPlanogramConsumerDecisionTreeNode.ChildList
        {
            get { return ChildList.Cast<IPlanogramConsumerDecisionTreeNode>(); }
        }

        IEnumerable<IPlanogramConsumerDecisionTreeNodeProduct> IPlanogramConsumerDecisionTreeNode.Products
        {
            get { return Products.Cast<IPlanogramConsumerDecisionTreeNodeProduct>(); }
        }

        #endregion

    }
}
