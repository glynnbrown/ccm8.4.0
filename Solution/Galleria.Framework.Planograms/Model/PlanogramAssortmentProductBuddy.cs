﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A class representing an Assortment Location Buddy within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    public partial class PlanogramAssortmentProductBuddy : ModelObject<PlanogramAssortmentProductBuddy>, IPlanogramAssortmentProductBuddy
    {
        #region Static Constructor
        static PlanogramAssortmentProductBuddy()
        {
        }
        #endregion

        #region Properties

        #region Parent

        /// <summary>
        /// Returns the parent assortment
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get
            {
                PlanogramAssortmentProductBuddyList listParent = base.Parent as PlanogramAssortmentProductBuddyList;
                if (listParent != null)
                {
                    return listParent.Parent;
                }
                return null;
            }
        }

        #endregion
        
        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
            RegisterModelProperty<String>(c => c.ProductGtin);
        /// <summary>
        /// The Location Id
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddySourceType> SourceTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddySourceType>(c => c.SourceType);
        public PlanogramAssortmentProductBuddySourceType SourceType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddyTreatmentType> TreatmentTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddyTreatmentType>(c => c.TreatmentType);
        public PlanogramAssortmentProductBuddyTreatmentType TreatmentType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty, value); }
        }

        public static ModelPropertyInfo<Single> TreatmentTypePercentageProperty =
        RegisterModelProperty<Single>(c => c.TreatmentTypePercentage);
        public Single TreatmentTypePercentage
        {
            get { return GetProperty<Single>(TreatmentTypePercentageProperty); }
            set { SetProperty<Single>(TreatmentTypePercentageProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddyProductAttributeType> ProductAttributeTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddyProductAttributeType>(c => c.ProductAttributeType);
        public PlanogramAssortmentProductBuddyProductAttributeType ProductAttributeType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty, value); }
        }

        #region S1

        #region S1ProductGtin

        private static readonly ModelPropertyInfo<String> S1ProductGtinProperty =
            RegisterModelProperty<String>(c => c.S1ProductGtin);
        /// <summary>
        /// The ProductGtin of the source Location 1
        /// </summary>
        public String S1ProductGtin
        {
            get { return GetProperty<String>(S1ProductGtinProperty); }
            set { SetProperty<String>(S1ProductGtinProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S1PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S1Percentage);
        public Single? S1Percentage
        {
            get { return GetProperty<Single?>(S1PercentageProperty); }
            set { SetProperty<Single?>(S1PercentageProperty, value); }
        }

        #endregion

        #region S2

        #region S2ProductGtin

        private static readonly ModelPropertyInfo<String> S2ProductGtinProperty =
            RegisterModelProperty<String>(c => c.S2ProductGtin);
        /// <summary>
        /// The ProductGtin of the source Location 2
        /// </summary>
        public String S2ProductGtin
        {
            get { return GetProperty<String>(S2ProductGtinProperty); }
            set { SetProperty<String>(S2ProductGtinProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S2PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S2Percentage);
        public Single? S2Percentage
        {
            get { return GetProperty<Single?>(S2PercentageProperty); }
            set { SetProperty<Single?>(S2PercentageProperty, value); }
        }

        #endregion

        #region S3

        #region S3ProductGtin

        private static readonly ModelPropertyInfo<String> S3ProductGtinProperty =
            RegisterModelProperty<String>(c => c.S3ProductGtin);
        /// <summary>
        /// The ProductGtin of the source Location 3
        /// </summary>
        public String S3ProductGtin
        {
            get { return GetProperty<String>(S3ProductGtinProperty); }
            set { SetProperty<String>(S3ProductGtinProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S3PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S3Percentage);
        public Single? S3Percentage
        {
            get { return GetProperty<Single?>(S3PercentageProperty); }
            set { SetProperty<Single?>(S3PercentageProperty, value); }
        }

        #endregion

        #region S4

        #region S4ProductGtin

        private static readonly ModelPropertyInfo<String> S4ProductGtinProperty =
            RegisterModelProperty<String>(c => c.S4ProductGtin);
        /// <summary>
        /// The ProductGtin of the source Location 4
        /// </summary>
        public String S4ProductGtin
        {
            get { return GetProperty<String>(S4ProductGtinProperty); }
            set { SetProperty<String>(S4ProductGtinProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S4PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S4Percentage);
        public Single? S4Percentage
        {
            get { return GetProperty<Single?>(S4PercentageProperty); }
            set { SetProperty<Single?>(S4PercentageProperty, value); }
        }

        #endregion

        #region S5

        #region S5ProductGtin

        private static readonly ModelPropertyInfo<String> S5ProductGtinProperty =
            RegisterModelProperty<String>(c => c.S5ProductGtin);
        /// <summary>
        /// The ProductGtin of the source Location 5
        /// </summary>
        public String S5ProductGtin
        {
            get { return GetProperty<String>(S5ProductGtinProperty); }
            set { SetProperty<String>(S5ProductGtinProperty, value); }
        }

        #endregion

        public static readonly ModelPropertyInfo<Single?> S5PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S5Percentage);
        public Single? S5Percentage
        {
            get { return GetProperty<Single?>(S5PercentageProperty); }
            set { SetProperty<Single?>(S5PercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {

            base.AddBusinessRules();

            BusinessRules.AddRule(new NullableMaxValue<Single>(S1PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S2PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S3PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S4PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S5PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMinValue<Single>(S1PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S2PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S3PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S4PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S5PercentageProperty, 0.01f));
            BusinessRules.AddRule(new MaxLength(S1PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S2PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S3PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S4PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S5PercentageProperty, 4));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramAssortmentProductBuddy NewPlanogramAssortmentProductBuddy()
        {
            PlanogramAssortmentProductBuddy item = new PlanogramAssortmentProductBuddy();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramAssortmentProductBuddy NewPlanogramAssortmentProductBuddy(String productGtin)
        {
            PlanogramAssortmentProductBuddy item = new PlanogramAssortmentProductBuddy();
            item.Create(productGtin);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramAssortmentProductBuddy NewPlanogramAssortmentProductBuddy(IPlanogramAssortmentProductBuddy buddy)
        {
            PlanogramAssortmentProductBuddy item = new PlanogramAssortmentProductBuddy();
            item.Create(buddy);
            return item;
        }

        

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="Location"></param>
        private void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="Location"></param>
        private void Create(String productGtin)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(ProductGtinProperty, productGtin);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="Location"></param>
        private void Create(IPlanogramAssortmentProductBuddy buddy)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(ProductGtinProperty, buddy.ProductGtin);
            this.LoadProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty, buddy.SourceType);
            this.LoadProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty, buddy.TreatmentType);
            this.LoadProperty<Single>(TreatmentTypePercentageProperty, buddy.TreatmentTypePercentage);
            this.LoadProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty, buddy.ProductAttributeType);
            this.LoadProperty<String>(S1ProductGtinProperty, buddy.S1ProductGtin);
            this.LoadProperty<Single?>(S1PercentageProperty, buddy.S1Percentage);
            this.LoadProperty<String>(S2ProductGtinProperty, buddy.S2ProductGtin);
            this.LoadProperty<Single?>(S2PercentageProperty, buddy.S2Percentage);
            this.LoadProperty<String>(S3ProductGtinProperty, buddy.S3ProductGtin);
            this.LoadProperty<Single?>(S3PercentageProperty, buddy.S3Percentage);
            this.LoadProperty<String>(S4ProductGtinProperty, buddy.S4ProductGtin);
            this.LoadProperty<Single?>(S4PercentageProperty, buddy.S4Percentage);
            this.LoadProperty<String>(S5ProductGtinProperty, buddy.S5ProductGtin);
            this.LoadProperty<Single?>(S5PercentageProperty, buddy.S5Percentage);
            this.MarkAsChild();
            base.Create();
        }

        

        #endregion

        #endregion


        #region Methods

        public Int32 GetNumberOfSourceProducts()
        {
            Int32 numOfSourceProducts = 0;

            if (String.IsNullOrEmpty(this.S1ProductGtin))
            {
                return numOfSourceProducts;
            }
            numOfSourceProducts++;

            if (String.IsNullOrEmpty(this.S2ProductGtin))
            {
                return numOfSourceProducts;
            }
            numOfSourceProducts++;

            if (String.IsNullOrEmpty(this.S3ProductGtin))
            {
                return numOfSourceProducts;
            }
            numOfSourceProducts++;

            if (String.IsNullOrEmpty(this.S4ProductGtin))
            {
                return numOfSourceProducts;
            }
            numOfSourceProducts++;

            if (String.IsNullOrEmpty(this.S5ProductGtin))
            {
                return numOfSourceProducts;
            }
            numOfSourceProducts++;

            return numOfSourceProducts;
        }

        #endregion
    }
}
