﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentLocalProduct.
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentLocalProduct : ModelObject<PlanogramAssortmentLocalProduct>,IPlanogramAssortmentLocalProduct
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)((PlanogramAssortmentLocalProductList)base.Parent).Parent; }
        }
        #endregion

        #region LocationCode
        /// <summary>
        /// LocationCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// LocationCode
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
            set { SetProperty<String>(LocationCodeProperty, value); }
        }
        #endregion

        #region ProductGtin
        /// <summary>
        /// ProductGtin property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
            RegisterModelProperty<String>(c => c.ProductGtin);
        /// <summary>
        /// ProductGtin
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(ProductGtinProperty));
            BusinessRules.AddRule(new MaxLength(ProductGtinProperty, 14));
            BusinessRules.AddRule(new Required(LocationCodeProperty));
            BusinessRules.AddRule(new MaxLength(LocationCodeProperty, 50));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentLocalProduct NewPlanogramAssortmentLocalProduct()
        {
            var item = new PlanogramAssortmentLocalProduct();
            item.Create();
            return item;
        }

        public static PlanogramAssortmentLocalProduct NewPlanogramAssortmentLocalProduct(IPlanogramAssortmentLocalProduct localProduct)
        {
            var item = new PlanogramAssortmentLocalProduct();
            item.Create(localProduct);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IPlanogramAssortmentLocalProduct localProduct)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(LocationCodeProperty, localProduct.LocationCode);
            this.LoadProperty<String>(ProductGtinProperty, localProduct.ProductGtin);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
