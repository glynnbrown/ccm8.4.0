﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26954 : A.Silva - Added ResultType Property.
// V8-26812 : A.Silva ~ Added ValidationType Property.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Server side implementation.
    /// </summary>
    public partial class PlanogramValidationTemplateGroup
    {
        #region Constructor

        /// <summary>
        ///     Private constructor, use factory methods to create new instances of the <see cref="PlanogramValidationTemplateGroup"/> class.
        /// </summary>
        private PlanogramValidationTemplateGroup()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an item from a dto
        /// </summary>
        internal static PlanogramValidationTemplateGroup Fetch(IDalContext dalContext,
            PlanogramValidationTemplateGroupDto dto)
        {
            return DataPortal.FetchChild<PlanogramValidationTemplateGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        private PlanogramValidationTemplateGroupDto GetDataTransferObject(PlanogramValidationTemplate parent)
        {
            return new PlanogramValidationTemplateGroupDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramValidationTemplateId = parent.Id,
                Name = ReadProperty(NameProperty),
                Threshold1 = ReadProperty(Threshold1Property),
                Threshold2 = ReadProperty(Threshold2Property),
                ResultType = (Byte) ReadProperty(ResultTypeProperty),
                ValidationType = (Byte) ReadProperty(ValidationTypeProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty)
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationTemplateGroupDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(Threshold1Property, dto.Threshold1);
            LoadProperty(Threshold2Property, dto.Threshold2);
            LoadProperty(ResultTypeProperty, (PlanogramValidationTemplateResultType)dto.ResultType);
            LoadProperty(ValidationTypeProperty, (PlanogramValidationType) dto.ValidationType);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramValidationTemplateGroupDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///  Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramValidationTemplate parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramValidationTemplateGroupDto>(
            (dc) =>
            {
                PlanogramValidationTemplateGroupDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty(IdProperty, dto.Id);
                dc.RegisterId<PlanogramValidationTemplateGroup>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        ///     Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramValidationTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramValidationTemplateGroupDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramValidationTemplate parent)
        {
            batchContext.Delete<PlanogramValidationTemplateGroupDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}