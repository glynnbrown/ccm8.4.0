﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-27062 : A.Silva ~ Added base implementation of virtual methods CalculateValidationData and OnCalculateValidationData.
// V8-27058 : A.Probyn 
//  Extended for ClearMetaData
// V8-27237 : A.Silva
//  Added base implementation of virtual methods ClearValidationData and OnClearValidationData.
// V8-27490 : N.Foster
//  Refactory fast-lookup code and moved into framework
#endregion
#region Version History: CCM803
// V8-29291 : L.Ineson
//  Added additional metadata meta and metrics
// V8-29291 : L.Ineson
// Added CopyMetadataState as a temporary way of speeding things up.
#endregion
#region Version History: CCM810
// V8-30016 : A.Kuszyk
//  Added GetAllBrokenRules method.
// V8-30519 : L.Ineson
//  Added Child changing - its here temporarily so that it only affects the planogram model 
//  but later on it can be moved to the framework for all model objects.
#endregion
#region Version History: CCM820
// V8-31483 : N.Foster
//  Improved performance of calculate validation data
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Csla.Core;
using Csla.Rules;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Defines a base model class from
    /// which the other model objects in
    /// this assembly will inherit
    /// </summary>
    [Serializable]
    public abstract partial class ModelObject<T> : Framework.Model.ModelObject<T>, IModelObject, INotifyChildChanging
        where T : ModelObject<T>
    {
        #region Properties

        #region DalFactoryName
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected virtual String DalFactoryName
        {
            get
            {
                // attempt to get the name from a parent model object
                var modelObject = Parent as IModelObject;
                if (modelObject != null) return modelObject.DalFactoryName;

                // attempt to get the name from a parent model list
                var modelList = Parent as IModelList;
                if (modelList != null) return modelList.DalFactoryName;

                return null;
            }
        }

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String IModelObject.DalFactoryName
        {
            get { return DalFactoryName; }
        }
        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);

        /// <summary>
        /// Returns the unique package id
        /// </summary>
        public Object Id
        {
            get { return GetProperty(IdProperty); }
            set { SetProperty(IdProperty, value); }
        }
        #endregion

        #region ExtendedData
        /// <summary>
        /// ExtendedData property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> ExtendedDataProperty =
            RegisterModelProperty<Object>(c => c.ExtendedData);

        /// <summary>
        /// Gets or sets the object extended data
        /// </summary>
        protected Object ExtendedData
        {
            get { return ReadProperty(ExtendedDataProperty); }
            set { LoadProperty(ExtendedDataProperty, value); }
        }
        #endregion

        #endregion

        #region Methods

        #region Overrides

        /// <summary>
        /// Hook into child object events.
        /// </summary>
        protected override void OnAddEventHooks(IBusinessObject child)
        {
            base.OnAddEventHooks(child);

            
            INotifyPropertyChanging pc = child as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging += Child_PropertyChanging;
            }

            INotifyChildChanging cc = child as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging += Child_Changing;
            }
        }


        /// <summary>
        /// Unhook child object events.
        /// </summary>
        protected override void OnRemoveEventHooks(IBusinessObject child)
        {
            base.OnRemoveEventHooks(child);

            INotifyPropertyChanging pc = child as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyChildChanging cc = child as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging -= Child_Changing;
            }
        }

        #endregion

        #region GetDalFactory
        /// <summary>
        /// Returns the correct dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(DalFactoryName);
        }

        /// <summary>
        /// Returns the specified dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory(String dalName)
        {
            return DalContainer.GetDalFactory(dalName);
        }
        #endregion

        #region Metadata
        /// <summary>
        /// Calculates metadata for this instance
        /// </summary>
        void IModelObject.CalculateMetadata()
        {
            CalculateMetadata();
        }

        /// <summary>
        /// Calculates metadata for this instance
        /// and all of its loaded children.
        /// </summary>
        protected virtual void CalculateMetadata()
        {
            CalculateMetadata(FieldManager.GetChildren());
        }

        /// <summary>
        /// Calculates the meta data for the given children and this instance
        /// </summary>
        protected void CalculateMetadata(IEnumerable<Object> childrenToUpdate)
        {
            // update child meta data
            foreach (var child in childrenToUpdate)
            {
                if (child == null) continue;

                var modelList = child as IModelList;
                if (modelList != null)
                {
                    modelList.CalculateMetadata();
                }
                else
                {
                    var modelObject = child as IModelObject;
                    if (modelObject != null)
                    {
                        modelObject.CalculateMetadata();
                    }
                }
            }

            // calculate metadata for this instance
            //using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                OnCalculateMetadata();
            }
        }

        /// <summary>
        ///     Called when metadata is required to
        ///     be calculated for this instance
        /// </summary>
        protected virtual void OnCalculateMetadata()
        {
        }

        /// <summary>
        /// Clears metadata for this instance
        /// </summary>
        void IModelObject.ClearMetadata()
        {
            ClearMetadata();
        }

        /// <summary>
        /// Clears metadata for this instance
        /// </summary>
        protected virtual void ClearMetadata()
        {
            ClearMetadata(FieldManager.GetChildren());
        }

        /// <summary>
        /// Clears the meta data for the given children and this instance
        /// </summary>
        protected void ClearMetadata(IEnumerable<Object> childrenToUpdate)
        {
            // update child meta data
            foreach (var child in childrenToUpdate)
            {
                if (child == null) continue;

                var modelList = child as IModelList;
                if (modelList != null)
                {
                    modelList.ClearMetadata();
                }
                else
                {
                    var modelObject = child as IModelObject;
                    if (modelObject != null)
                    {
                        modelObject.ClearMetadata();
                    }
                }
            }

            // clear metadata for this instance
            OnClearMetadata();
        }

        /// <summary>
        ///     Called when metadata is required to
        ///     be cleared for this instance
        /// </summary>
        protected virtual void OnClearMetadata()
        {
        }

        /// <summary>
        /// Copies the state of one model object
        /// into this model object
        /// </summary>
        /// <param name="other">The object to copy from</param>
        /// <remarks>
        /// At present this peforms a shallow copy
        /// so no children are modified as part of this copy
        /// </remarks>
        public void CopyMetadataState(T other)
        {
            ((IModelReadOnlyObject)this).ClearCache();
            Type modelType = typeof(T);
            foreach (IPropertyInfo propertyInfo in FieldManager.GetRegisteredProperties())
            {
                if (!propertyInfo.Name.StartsWith("Meta")) continue;

                if (/*(!propertyInfo.Equals(IsDeleteSupressedProperty)) &&*/
                    (!propertyInfo.Equals(IsInitializedProperty)) &&
                    (!typeof(IModelReadOnlyObject).IsAssignableFrom(propertyInfo.Type)))
                {
                    // attempt to get the property
                    PropertyInfo property = modelType.GetProperty(propertyInfo.Name);

                    // attempt to set the property using the setter
                    // note that we need to do this in case the property
                    // has a backing field rather than using field manager
                    if ((property != null) && (property.CanWrite))
                    {
                        property.SetValue(this, other.ReadProperty(propertyInfo), null);
                    }
                    else
                    {
                        // fall back on setting the property directly
                        this.SetProperty(propertyInfo, other.ReadProperty(propertyInfo));
                    }
                }
            }
        }

        /// <summary>
        /// Clones this object
        /// </summary>
        /// <param name="loadChildren">
        /// if true, all child objects will be loaded prior to cloning.</param>
        /// <returns></returns>
        public T Clone(Boolean loadChildren)
        {
            if (loadChildren)
            {
                //force the load of all child objects
                // so that they get included in the copy.
                LoadChildren(true);
            }

            return (T)GetClone();
        }

        #endregion

        #region Validation Data
        /// <summary>
        /// Calculates the validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Implemented explicitly to hide this method unless accessed through the interface.
        /// Types inheriting from <see cref="ModelObject{T}" /> must implement their own
        /// <see cref="CalculateValidationData()" /> method for it to do anything.
        /// </remarks>
        void IModelObject.CalculateValidationData()
        {
            CalculateValidationData();
        }

        /// <summary>
        /// Calculates the validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Types inheriting from <see cref="ModelObject{T}" /> can hide this method to provide a especific mechanism
        /// to control the calculation of validation data.
        /// Leave as it is if the base implementation is enough.
        /// </remarks>
        protected virtual void CalculateValidationData()
        {
            // do nothing here
        }

        /// <summary>
        /// Clears all validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Implemented explicitly to hide this method unless accessed through the interface.
        /// Types inheriting from <see cref="ModelObject{T}" /> must implement their own
        /// <see cref="ClearValidationData()" /> method for it to do anything.
        /// </remarks>
        void IModelObject.ClearValidationData()
        {
            ClearValidationData();
        }

        /// <summary>
        /// Clears all validation data for this instance.
        /// </summary>
        /// <remarks>
        /// Types inheriting from <see cref="ModelObject{T}" /> can hide this method to provide a especific mechanism
        /// to control the clearing of validation data.
        /// Leave as it is if the base implementation is enough.
        /// </remarks>
        protected virtual void ClearValidationData()
        {
            // do nothing here

        }

        #endregion

        #region GetIdValue
        /// <summary>
        /// Called when returning a unique id for this value
        /// </summary>
        protected override Object GetIdValue()
        {
            return this.ReadProperty<Object>(IdProperty);
        }
        #endregion

        /// <summary>
        /// Returns true if the property data has been loaded.
        /// This gives a way to check safely without actually forcing the data to be fetched.
        /// </summary>
        public Boolean IsPropertyLoaded(IModelPropertyInfo property)
        {
            return FieldManager.FieldExists(property);
        }

        /// <summary>
        /// Helper method to return all broken rules in the 
        /// object graph.
        /// </summary>
        /// <returns></returns>
        public BrokenRulesCollection GetAllBrokenRules()
        {
            BrokenRulesCollection list = new BrokenRulesCollection();

            if (FieldManager.HasFields)
            {
                // add rules for this.
                list.AddRange(this.BrokenRulesCollection);

                //check child objects
                foreach (object child in this.FieldManager.GetChildren())
                {
                    if ((child is IModelObject) && (child as IEditableBusinessObject != null) && (((IEditableBusinessObject)child).IsValid == false))
                    {
                        //add child object rules and recurse
                        list.AddRange(((IModelObject)child).GetAllBrokenRules());
                    }
                    else
                    {
                        //cycle through child list objects
                        if (child is IEditableCollection)
                        {
                            foreach (Object childObject in (IEnumerable)child)
                            {
                                if (childObject is IModelObject)
                                {
                                    list.AddRange((childObject as IModelObject).GetAllBrokenRules());
                                }
                            }
                        }
                    }
                }

            }

            return list;
        }

        /// <summary>
        /// Recurses through the all children looking for the object
        /// with the given type and id.
        /// </summary>
        public Object FindChildModelById(Type childModelType, Object id)
        {
            //check this against the criteria
            if (this.GetType() == childModelType
               && Object.Equals(this.GetIdValue(), id))
                return this;
      
            //This is not the model object we are looking for.. 
            // so cycle through its children.
            foreach (var child in FieldManager.GetChildren())
            {
                Object foundChild = null;
                
                var childList = child as IModelList;
                if (childList != null)
                {
                   foundChild = childList.FindChildModelById(childModelType, id);
                }
                else
                {
                    var childObject = child as IModelObject;
                    if (childObject == null) continue;
                    foundChild = childObject.FindChildModelById(childModelType, id);
                }


                if (foundChild != null) return foundChild;
            }
            return null;
        }

        #endregion

        #region Child Changing Notification

        //TODO: This needs moving into the framework model object.

        [NonSerialized]
        [Csla.NotUndoable]
        private EventHandler<ChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Event raised when a child object is about to change.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public event EventHandler<ChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        /// <summary>
        /// Raises the child changing event to indicate that a child object is about to change.
        /// </summary>
        /// <param name="e">ChildChangingEventArgs object</param>
        protected virtual void OnChildChanging(ChildChangingEventArgs e)
        {
            if (_childChangingHandlers != null)
                _childChangingHandlers.Invoke(this, e);
        }

        /// <summary>
        /// Creates a ChildChangingEventArgs and raises the event. 
        /// </summary>
        private void RaiseChildChanging(object childObject, PropertyChangingEventArgs propertyArgs)
        {
            ChildChangingEventArgs args = new ChildChangingEventArgs(childObject, propertyArgs);
            OnChildChanging(args);
        }
        
        /// <summary>
        /// Raises the event
        /// </summary>
        private void RaiseChildChanging(ChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Handles any PropertyChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            RaiseChildChanging(sender, e);
        }

        /// <summary>
        /// Handles any ChildChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_Changing(object sender, ChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        #endregion
    }

}