﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26569 : L.Ineson
//  Added on methods to get world rotation and position
// V8-26760 : L.Ineson
//  Populated property display types
// V8-27058 : A.Probyn
//  Added new meta data properties calculations
//  Added ClearMetadata
// V8-27477 : L.Ineson
//  Added more world space helper methods
// V8-25976 : L.Ineson
//  Added GetBrokenValidationWarnings method.
// V8-28078 : A.Probyn
//      Added defensive code to GetPlanogramRelativeCoordinates

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29369 : L.Luong
//  Modified Meta data calculation so they are correct
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single

#endregion

#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-30210 : M.Brumby
//  Case pack metadata was being converted to Int16 erroneously

#endregion

#region Version History: CCM811
// V8-30352 : A.Probyn
//  ~Added missing display property types to white space meta data properties.
//  ~Updated OnCalculateMetadata so that the white space is calculated at a cumulative merchandising space level, 
//  not as a total for the planogram. Components which are overfaced were masking white space on other shelves.
// V8-30398 : A.Silva
//  Amended MetaTotalFacings to use PlanogramHelper.CalculateMetaTotalFacings (that accounts for tray real facings).
#endregion

#region Version History: CCM820
// V8-31222 : L.Ineson
//  Removed unpopulated metadata properties from displayable field infos.
#endregion

#region Version History: CCM830
// V8-32521  : J.Pickup
//  Added ComponentOutsideOfFixtureArea Validation. 
// V8-32593 : A.Silva
//  Amended GetBrokenValidationWarnings to register the warning per assembly component and not multiple times per assembly.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using System.Diagnostics;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramFixtureAssembly object
    [Serializable]
    public sealed partial class PlanogramFixtureAssembly : ModelObject<PlanogramFixtureAssembly>
    {
        #region Constants
        const String WorldXFieldName = "WorldX";
        const String WorldYFieldName = "WorldY";
        const String WorldZFieldName = "WorldZ";
        const String WorldSlopeFieldName = "WorldSlope";
        const String WorldAngleFieldName = "WorldAngle";
        const String WorldRollFieldName = "WorldRoll";
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramFixture Parent
        {
            get { return ((PlanogramFixtureAssemblyList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region PlanogramAssemblyId
        /// <summary>
        /// PlanogramAssemblyId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramAssemblyIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramAssemblyId);
        /// <summary>
        /// The PlanogramFixtureAssembly PlanogramAssemblyId
        /// </summary>
        public Object PlanogramAssemblyId
        {
            get { return GetProperty<Object>(PlanogramAssemblyIdProperty); }
            set { SetProperty<Object>(PlanogramAssemblyIdProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.PlanogramFixtureAssembly_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramFixtureAssembly X
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.PlanogramFixtureAssembly_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramFixtureAssembly Y
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.PlanogramFixtureAssembly_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The PlanogramFixtureAssembly Z
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramFixtureAssembly Slope
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramFixtureAssembly Angle
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The PlanogramFixtureAssembly Roll
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaComponentCount
        /// <summary>
        /// MetaComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaComponentCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaComponentCount, Message.PlanogramFixtureAssembly_MetaComponentCount);
        /// <summary>
        /// Gets/Sets the meta data component count;
        /// </summary>
        public Int32? MetaComponentCount
        {
            get { return this.GetProperty<Int32?>(MetaComponentCountProperty); }
            set { this.SetProperty<Int32?>(MetaComponentCountProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace, Message.PlanogramFixtureAssembly_MetaTotalMerchandisableLinearSpace, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace, Message.PlanogramFixtureAssembly_MetaTotalMerchandisableAreaSpace, ModelPropertyDisplayType.Area);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace, Message.PlanogramFixtureAssembly_MetaTotalMerchandisableVolumetricSpace, ModelPropertyDisplayType.Volume);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace, Message.PlanogramFixtureAssembly_MetaTotalLinearWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace, Message.PlanogramFixtureAssembly_MetaTotalAreaWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace, Message.PlanogramFixtureAssembly_MetaTotalVolumetricWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced, Message.PlanogramFixtureAssembly_MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsPlacedProperty, value); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts, Message.PlanogramFixtureAssembly_MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
            set { this.SetProperty<Int32?>(MetaNewProductsProperty, value); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount, Message.PlanogramFixtureAssembly_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
            set { this.SetProperty<Int32?>(MetaChangesFromPreviousCountProperty, value); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating, Message.PlanogramFixtureAssembly_MetaChangeFromPreviousStarRating);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
            set { this.SetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, value); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped, Message.PlanogramFixtureAssembly_MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
            set { this.SetProperty<Int32?>(MetaBlocksDroppedProperty, value); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory, Message.PlanogramFixtureAssembly_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Int32?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, Message.PlanogramFixtureAssembly_MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFacings
        /// <summary>
        /// MetaAverageFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaAverageFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaAverageFacings, Message.PlanogramFixtureAssembly_MetaAverageFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFacings
        /// </summary>
        public Int16? MetaAverageFacings
        {
            get { return this.GetProperty<Int16?>(MetaAverageFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaAverageFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, Message.PlanogramFixtureAssembly_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }
        #endregion

        #region MetaAverageUnits
        /// <summary>
        /// MetaAverageUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaAverageUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaAverageUnits, Message.PlanogramFixtureAssembly_MetaAverageUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageUnits
        /// </summary>
        public Int32? MetaAverageUnits
        {
            get { return this.GetProperty<Int32?>(MetaAverageUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaAverageUnitsProperty, value); }
        }
        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos, Message.PlanogramFixtureAssembly_MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
            set { this.SetProperty<Single?>(MetaMinDosProperty, value); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos, Message.PlanogramFixtureAssembly_MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
            set { this.SetProperty<Single?>(MetaMaxDosProperty, value); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos, Message.PlanogramFixtureAssembly_MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
            set { this.SetProperty<Single?>(MetaAverageDosProperty, value); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases, Message.PlanogramFixtureAssembly_MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
            set { this.SetProperty<Single?>(MetaMinCasesProperty, value); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases, Message.PlanogramFixtureAssembly_MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
            set { this.SetProperty<Single?>(MetaAverageCasesProperty, value); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases, Message.PlanogramFixtureAssembly_MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
            set { this.SetProperty<Single?>(MetaMaxCasesProperty, value); }
        }
        #endregion

        #region MetaSpaceToUnitsIndex
        /// <summary>
        /// MetaSpaceToUnitsIndex property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaSpaceToUnitsIndexProperty =
            RegisterModelProperty<Int16?>(c => c.MetaSpaceToUnitsIndex, Message.PlanogramFixtureAssembly_MetaSpaceToUnitsIndex);
        /// <summary>
        /// Gets/Sets the meta data MetaSpaceToUnitsIndex
        /// </summary>
        public Int16? MetaSpaceToUnitsIndex
        {
            get { return this.GetProperty<Int16?>(MetaSpaceToUnitsIndexProperty); }
            set { this.SetProperty<Int16?>(MetaSpaceToUnitsIndexProperty, value); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, Message.PlanogramFixtureAssembly_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaTotalFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, Message.PlanogramFixtureAssembly_MetaAverageFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
            set { this.SetProperty<Single?>(MetaAverageFrontFacingsProperty, value); }
        }
        #endregion

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureAssembly NewPlanogramFixtureAssembly()
        {
            PlanogramFixtureAssembly item = new PlanogramFixtureAssembly();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramFixtureAssembly>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
            base.OnCopyComplete(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramAssemblyId
            Object planogramAssemblyId = context.ResolveId<PlanogramAssembly>(this.ReadProperty<Object>(PlanogramAssemblyIdProperty));
            if (planogramAssemblyId != null) this.LoadProperty<Object>(PlanogramAssemblyIdProperty, planogramAssemblyId);
        }

        /// <summary>
        /// Returns the referenced Planogram Assembly.
        /// </summary>
        /// <returns></returns>
        public PlanogramAssembly GetPlanogramAssembly()
        {
            PlanogramAssembly assembly = null;

            if (this.PlanogramAssemblyId != null)
            {
                PlanogramFixture parentFixture = this.Parent;
                if (parentFixture != null)
                {
                    Planogram parentPlanogram = parentFixture.Parent;
                    if (parentPlanogram != null)
                    {
                        assembly = parentPlanogram.Assemblies.FindById(this.PlanogramAssemblyId);
                    }
                }
            }

            return assembly;
        }

        #region Placement Related

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private MatrixValue GetPlanogramRelativeTransform(PlanogramFixtureItem fixtureItem, Boolean includeLocal)
        {
            //return the full planogram relative tranform for this item.
            // nb - order is important here, must build upwards.
            MatrixValue relativeTransform = MatrixValue.Identity;

            //assembly
            if (includeLocal)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        this.X, this.Y, this.Z,
                        this.Angle, this.Slope, this.Roll));
            }

            //Fixture
            if (fixtureItem != null)
            {
                relativeTransform.Append(
                    MatrixValue.CreateTransformationMatrix(
                        fixtureItem.X, fixtureItem.Y, fixtureItem.Z,
                        fixtureItem.Angle, fixtureItem.Slope, fixtureItem.Roll));
            }


            return relativeTransform;
        }

        /// <summary>
        /// Returns the rotation for this component relative to the planogram
        /// </summary>
        /// <param name="fixtureItem">the parent fixture item.</param>
        /// <param name="includeLocal">if false, parent transform will be returned</param>
        private RotationValue GetPlanogramRelativeRotation(PlanogramFixtureItem fixtureItem, Boolean includeLocal)
        {
            RotationValue rotation = new RotationValue();

            //Assembly
            if (includeLocal)
            {
                rotation.Angle += this.Angle;
                rotation.Slope += this.Slope;
                rotation.Roll += this.Roll;
            }

            //Fixture
            if (fixtureItem != null)
            {
                rotation.Angle += fixtureItem.Angle;
                rotation.Slope += fixtureItem.Slope;
                rotation.Roll += fixtureItem.Roll;
            }

            return rotation;
        }

        /// <summary>
        /// Returns the position of this component relative to the planogram
        /// as a whole.
        /// </summary>
        public PointValue GetPlanogramRelativeCoordinates(PlanogramFixtureItem fixtureItem)
        {
            Debug.Assert(fixtureItem != null, "FixtureItem should never be null");

            PointValue localCoordinates = new PointValue(this.X, this.Y, this.Z);
            if (fixtureItem == null) return localCoordinates;

            //get the relative transform
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/false);

            //trasnform and return.
            PointValue worldCoordinates = localCoordinates.Transform(relativeTransform);
            return worldCoordinates;
        }

        /// <summary>
        /// Sets the x,y,z  values of this item according to the given
        /// planogram relative coordinates
        /// </summary>
        public void SetCoordinatesFromPlanogramRelative(PointValue worldCoordinate, PlanogramFixtureItem fixtureItem)
        {
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/false);
            relativeTransform.Invert();

            PointValue newLocalCoordinate = worldCoordinate.Transform(relativeTransform);

            this.X = newLocalCoordinate.X;
            this.Y = newLocalCoordinate.Y;
            this.Z = newLocalCoordinate.Z;
        }

        /// <summary>
        /// Returns the rotation of this component relative to the planogram as a whole.
        /// </summary>
        public RotationValue GetPlanogramRelativeRotation(PlanogramFixtureItem fixtureItem)
        {
            return GetPlanogramRelativeRotation(fixtureItem, /*incLocal*/true);
        }

        /// <summary>
        /// Sets the local rotation values based on the given world rotation.
        /// </summary>
        /// <param name="worldRotation"></param>
        public void SetRotationFromPlanogramRelative(RotationValue worldRotation, PlanogramFixtureItem fixtureItem)
        {
            //get the parent relative rotation
            RotationValue parentRotation = GetPlanogramRelativeRotation(fixtureItem, /*incLocal*/false);

            //strip away the parent rotation to get the new local values.
            this.Angle = worldRotation.Angle - parentRotation.Angle;
            this.Slope = worldRotation.Slope - parentRotation.Slope;
            this.Roll = worldRotation.Roll - parentRotation.Roll;
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetPlanogramRelativeBoundingBox(PlanogramAssembly assembly, PlanogramFixtureItem fixtureItem)
        {
            RectValue bounds = new RectValue();
            bounds.Width = assembly.Width;
            bounds.Height = assembly.Height;
            bounds.Depth = assembly.Depth;

            //create the relative transform.
            MatrixValue relativeTransform = GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            return relativeTransform.TransformBounds(bounds);
        }

        /// <summary>
        /// Converts the given local point to a planogram relative one.
        /// </summary>
        public static PointValue ToWorld(PointValue localPoint, PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureAssembly.GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            //trasnform and return.
            PointValue worldPoint = localPoint.Transform(relativeTransform);
            return worldPoint;
        }

        /// <summary>
        /// Converts the given world point to one which is local
        /// to this subcomponent placement.
        /// </summary>
        public static PointValue ToLocal(PointValue worldPoint, PlanogramFixtureItem fixtureItem, PlanogramFixtureAssembly fixtureAssembly)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureAssembly.GetPlanogramRelativeTransform(fixtureItem, /*incLocal*/true);

            //invert the transform.
            relativeTransform.Invert();

            return worldPoint.Transform(relativeTransform);
        }

        #endregion

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(PlanogramAssembly);
            String typeFriendly = PlanogramAssembly.FriendlyName;
            String detailsGroup = Message.PlanogramComponent_PropertyGroup_Details;

            //WorldX
            ObjectFieldInfo worldX = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            worldX.PropertyName = WorldXFieldName;
            yield return worldX;

            //WorldY
            ObjectFieldInfo worldY = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            worldY.PropertyName = WorldYFieldName;
            yield return worldY;

            //WorldZ
            ObjectFieldInfo worldZ = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);
            worldZ.PropertyName = WorldZFieldName;
            yield return worldZ;

            //WorldSlope
            ObjectFieldInfo worldSlope = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SlopeProperty, detailsGroup);
            worldSlope.PropertyName = WorldSlopeFieldName;
            yield return worldSlope;

            //WorldAngle
            ObjectFieldInfo worldAngle = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AngleProperty, detailsGroup);
            worldAngle.PropertyName = WorldAngleFieldName;
            yield return worldAngle;

            //WorldRoll
            ObjectFieldInfo worldRoll = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RollProperty, detailsGroup);
            worldRoll.PropertyName = WorldRollFieldName;
            yield return worldRoll;

            //Metadata fields
            if (includeMetadata)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaComponentCountProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableLinearSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableAreaSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableVolumetricSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalLinearWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalAreaWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalVolumetricWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaProductsPlacedProperty, detailsGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNewProductsProperty, detailsGroup); //not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangesFromPreviousCountProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangeFromPreviousStarRatingProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaBlocksDroppedProperty, detailsGroup);//not in use
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedInventoryProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalUnitsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageUnitsProperty, detailsGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinDosProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxDosProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageDosProperty, detailsGroup);//not in use
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxCasesProperty, detailsGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaSpaceToUnitsIndexProperty, detailsGroup);//not in use
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFrontFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFrontFacingsProperty, detailsGroup);
            }


        }


        /// <summary>
        /// Returns the value of the given field for this position.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field, PlanogramFixtureItem fixtureItem)
        {
            switch (field.PropertyName)
            {
                case WorldXFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeCoordinates(fixtureItem).X;

                case WorldYFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeCoordinates(fixtureItem).Y;

                case WorldZFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeCoordinates(fixtureItem).Z;

                case WorldSlopeFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeRotation(fixtureItem).Slope;

                case WorldAngleFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeRotation(fixtureItem).Angle;

                case WorldRollFieldName:
                    if (fixtureItem == null) return null;
                    return GetPlanogramRelativeRotation(fixtureItem).Roll;


                default:
                    return field.GetValue(this);
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Returns a list of validation warnings currently broken by this assembly.
        /// </summary>
        public List<PlanogramValidationWarning> GetBrokenValidationWarnings(PlanogramMetadataDetails metaDetails, PlanogramFixtureItem fixtureItem)
        {
            List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();

            Boolean hasComponentCollision = false;
            Boolean hasSlopeWithNoRiser = false;
            Boolean hasComponentIsOverfilled = false;

            foreach (PlanogramAssemblyComponent assemblyComponent in this.GetPlanogramAssembly().Components)
            {
                PlanogramComponent component = assemblyComponent.GetPlanogramComponent();

                // Collision validation. At least one of the component has at least one collision.
                if (!hasComponentCollision)
                {
                    if ((assemblyComponent.MetaTotalComponentCollisions.HasValue) ? assemblyComponent.MetaTotalComponentCollisions > 0
                        : assemblyComponent.EnumerateComponentCollisions(metaDetails, fixtureItem, this).Any())
                    {
                        hasComponentCollision = true;
                        warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentHasCollisions, fixtureItem, this, assemblyComponent));
                    }
                }

                // Slope with no riser validation. At least one of the components fails the check.
                if (!hasSlopeWithNoRiser)
                {
                    if ((assemblyComponent.MetaIsComponentSlopeWithNoRiser.HasValue) ? assemblyComponent.MetaIsComponentSlopeWithNoRiser.Value :
                        assemblyComponent.IsSlopedWithNoRiser(component))
                    {
                        hasSlopeWithNoRiser = true;
                        warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentSlopeWithNoRiser, fixtureItem, this, assemblyComponent));
                    }
                }

                // Component is overfilled. At least one of the components fails the check.
                if (!hasComponentIsOverfilled)
                {
                    if ((assemblyComponent.MetaPercentageLinearSpaceFilled.HasValue) ? assemblyComponent.MetaPercentageLinearSpaceFilled > 1 :
                        assemblyComponent.IsOverfilled(metaDetails))
                    {
                        hasComponentIsOverfilled = true;
                        warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentIsOverfilled, fixtureItem, this, assemblyComponent));
                    }
                }

                if (hasComponentCollision && hasSlopeWithNoRiser) break;


                if (assemblyComponent.MetaIsOutsideOfFixtureArea.HasValue && assemblyComponent.MetaIsOutsideOfFixtureArea.Value)
                {
                    warnings.Add(PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ComponentIsOutsideOfFixtureArea, fixtureItem, this, assemblyComponent));
                }
            }

            return warnings;
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            if (this.Parent == null) return;
            Planogram parentPlan = this.Parent.Parent;
            if (parentPlan == null) return;
            PlanogramPositionList positionList = parentPlan.Positions;
            if (positionList == null) return;
            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();

            //Get linked objects
            PlanogramAssembly assembly = this.Parent.Parent.Assemblies.FindById(this.PlanogramAssemblyId);
            PlanogramFixtureItem fixtureItem = parentPlan.FixtureItems.FirstOrDefault(i => Object.Equals(i.PlanogramFixtureId, this.Parent.Id));
            if (assembly == null || fixtureItem == null)
            {
                //clear out and return if none found.
                OnClearMetadata();
                return;
            }


            //Get all positions assigned to this fixture assembly
            List<PlanogramPosition> positions = positionList.Where(p => this.Id.Equals(p.PlanogramFixtureAssemblyId)).ToList();
            
            //get the position details and products for each position.
            List<PlanogramPositionDetails> fixtureAssemblyPositionDetails = new List<PlanogramPositionDetails>(positions.Count);
            List<PlanogramProduct> products = new List<PlanogramProduct>(positions.Count);
            
            foreach (PlanogramPosition position in positions)
            {
                PlanogramPositionDetails positionDetails;
                if (metadataDetails.PositionDetails.TryGetValue(position.Id, out positionDetails))
                {
                    fixtureAssemblyPositionDetails.Add(positionDetails);
                }

                PlanogramProduct product = parentPlan.Products.FindById(position.PlanogramProductId);
                if (product != null)
                {
                    products.Add(product);
                }
            }

            //Get sub assembly components
            List<PlanogramAssemblyComponent> assemblyComponents = assembly.Components.ToList();
           
            
            this.MetaAverageCases = positions.Any() ? positions.Average(p => p.MetaAchievedCases) : 0;
            this.MetaAverageDos = 0; //TODO
            this.MetaAverageUnits = positions.Any() ? Convert.ToInt32(positions.Average(p => p.TotalUnits)) : (Int32)0;
            this.MetaBlocksDropped = 0; //TODO
            this.MetaChangeFromPreviousStarRating = 0; //TODO
            this.MetaChangesFromPreviousCount = 0; //TODO
            this.MetaComponentCount = assemblyComponents.Count;
            this.MetaMaxCases = positions.Any() ? positions.Max(p => p.MetaAchievedCases) : 0;
            this.MetaMaxDos = 0; //TODO
            this.MetaMinCases = positions.Any() ? positions.Min(p => p.MetaAchievedCases) : 0;
            this.MetaMinDos = 0; //TODO
            this.MetaNewProducts = 0; //TODO
            this.MetaNotAchievedInventory = products.Any() ? products.Count(p => p.MetaNotAchievedInventory == true) : (Int16)0;
            this.MetaProductsPlaced = products.Count;
            this.MetaSpaceToUnitsIndex = 0; //TODO
            this.MetaTotalFacings = PlanogramHelper.CalculateMetaTotalFacings(positions, products);
            this.MetaAverageFacings = Convert.ToInt16(positions.Count > 0 ? MetaTotalFacings / positions.Count : 0);
            this.MetaTotalUnits = positions.Sum(p => p.TotalUnits);
            this.MetaTotalFrontFacings = Convert.ToInt16(positions.Count > 0 ? positions.Sum(p => p.MetaFrontFacingsWide) : 0);
            this.MetaAverageFrontFacings = Convert.ToSingle(positions.Count > 0 ? Math.Round((double)(this.MetaTotalFrontFacings / positions.Count), 2, MidpointRounding.AwayFromZero) : 0);


            //Get the merch space of all of the subcomponents in this assembly.
            IEnumerable<PlanogramMerchandisingSpace> merchandisingSpaces = metadataDetails.GetFixtureAssemblyMerchandisingSpace(this, fixtureItem);

            this.MetaTotalMerchandisableLinearSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableLinear()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableAreaSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableArea()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableVolumetricSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableVolume()), 2, MidpointRounding.AwayFromZero);

            //V830352 - Calculate the white space at a cumulative merchandising space level, not as a total for the planogram.
            // Components which are overfaced mask white space on other shelves.
            Single totalPlanLinearWhiteSpace = 0, totalPlanAreaWhiteSpace = 0, totalPlanVolumetricWhiteSpace = 0;
            Int32 numberOfMerchSpaces = merchandisingSpaces.Count();
            foreach (PlanogramMerchandisingSpace merchSpace in merchandisingSpaces)
            {
                if (merchSpace.SubComponentPlacement != null)
                {
                    //Get all positions in this merch space
                    IEnumerable<PlanogramPosition> merchSpacePositions = merchSpace.SubComponentPlacement.GetPlanogramPositions();

                    //Get position details
                    List<PlanogramPositionDetails> merchSpacePositionDetails = new List<PlanogramPositionDetails>();
                    foreach (PlanogramPosition pos in merchSpacePositions)
                    {
                        PlanogramPositionDetails posDetails = null;
                        if (metadataDetails.PositionDetails.TryGetValue(pos.Id, out posDetails))
                        {
                            merchSpacePositionDetails.Add(posDetails);
                        }
                    }

                    // The total width of white space. That is, the difference between the sum of the widths
                    // of all the positions at the front of the plan and the total width of merchandising space of
                    // all of the subcomponents.
                    totalPlanLinearWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableLinear(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width)).Value / numberOfMerchSpaces);

                    // The total area of white space. That is, the difference between the sum of all the positions
                    // that are at the front of the plan and the sum of the merchandisable areas of all the subcomponents.
                    totalPlanAreaWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableArea(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height)).Value / numberOfMerchSpaces);

                    // The total volume of white space. That is, the difference between the sum of the volumes 
                    // of all the positions and the sum of the merchandisable volumes of all the subcomponents.
                    totalPlanVolumetricWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableVolume(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height * p.TotalSize.Depth)).Value / numberOfMerchSpaces);
                }
            }

            //Set cumulative values to be totals
            this.MetaTotalLinearWhiteSpace = (Single?)Math.Round(totalPlanLinearWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalAreaWhiteSpace = (Single?)Math.Round(totalPlanAreaWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalVolumetricWhiteSpace = (Single?)Math.Round(totalPlanVolumetricWhiteSpace, 2, MidpointRounding.AwayFromZero);
            
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            this.MetaAverageCases = null;
            this.MetaAverageDos = null;
            this.MetaAverageFacings = null;
            this.MetaAverageUnits = null;
            this.MetaBlocksDropped = null;
            this.MetaChangeFromPreviousStarRating = null;
            this.MetaChangesFromPreviousCount = null;
            this.MetaComponentCount = null;
            this.MetaMaxCases = null;
            this.MetaMaxDos = null;
            this.MetaMinCases = null;
            this.MetaMinDos = null;
            this.MetaNewProducts = null;
            this.MetaNotAchievedInventory = null;
            this.MetaProductsPlaced = null;
            this.MetaSpaceToUnitsIndex = null;
            this.MetaTotalMerchandisableLinearSpace = null;
            this.MetaTotalMerchandisableAreaSpace = null;
            this.MetaTotalMerchandisableVolumetricSpace = null;
            this.MetaTotalFacings = null;
            this.MetaTotalUnits = null;
            this.MetaTotalLinearWhiteSpace = null;
            this.MetaTotalAreaWhiteSpace = null;
            this.MetaTotalVolumetricWhiteSpace = null;
            this.MetaTotalFrontFacings = null;
            this.MetaAverageFrontFacings = null;
        }

        #endregion

        #endregion
    }
}