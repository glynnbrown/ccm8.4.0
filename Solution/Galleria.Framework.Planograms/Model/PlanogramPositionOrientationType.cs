﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25543 : L.Ineson
//  Created

#endregion

#region Version History: (CCM 810)

// V8-28766 : J.Pickup
//  Introduced ToPositionOrientationType();
// V8-30146 : A.Silva
//  Refactored ToPositionOrientationType() to be an extension method.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Enum for the Product Orientation type property
    /// </summary>
    /// <remarks>
    ///     To get to the position, from Front0, rotate to bring the given face upfront, then rotate while keeping that
    ///     face upfront.
    /// </remarks>
    public enum PlanogramPositionOrientationType
    {
        /// <summary>
        ///     Default Orientation to the placed Product Orientation Type.
        /// </summary>
        Default = 0,
        /// <summary>
        ///     Front face upfront, upright.
        /// </summary>
        Front0 = 1,
        /// <summary>
        ///     Front face upfront, rotated 90 right.
        /// </summary>
        Front90 = 2,
        /// <summary>
        ///     Top face upfront, upright.
        /// </summary>
        Top0 = 3,
        /// <summary>
        ///     Top face upfront, rotated 90 right.
        /// </summary>
        Top90 = 4,
        /// <summary>
        ///     Right face upfront, upright.
        /// </summary>
        Right0 = 5,
        /// <summary>
        ///     Right face upfront, rotated 90 right.
        /// </summary>
        Right90 = 6,
        /// <summary>
        ///     Left face upfront, upright.
        /// </summary>
        Left0 = 7,
        /// <summary>
        ///     Left face upfront, rotated 90 right.
        /// </summary>
        Left90 = 8,
        /// <summary>
        ///     Back face upfront, upright.
        /// </summary>
        Back0 = 9,
        /// <summary>
        ///     Back face upfront, rotated 90 right.
        /// </summary>
        Back90 = 10,
        /// <summary>
        ///     Bottom face upfront, upright.
        /// </summary>
        Bottom0 = 11,
        /// <summary>
        ///     Bottom face upfront, rotated 90 right.
        /// </summary>
        Bottom90 = 12,
        /// <summary>
        ///     Front face upfront, upsidedown.
        /// </summary>
        Front180 = 13,
        /// <summary>
        ///     Front face upfront, rotated 90 left.
        /// </summary>
        Front270 = 14,
        /// <summary>
        ///     Top face upfront, upsidedown.
        /// </summary>
        Top180 = 15,
        /// <summary>
        ///     Top face upfront, rotated 90 left.
        /// </summary>
        Top270 = 16,
        /// <summary>
        ///     Right face upfront, upsidedown.
        /// </summary>
        Right180 = 17,
        /// <summary>
        ///     Right face upfront, rotated 90 left.
        /// </summary>
        Right270 = 18,
        /// <summary>
        ///     Left face upfront, upsidedown.
        /// </summary>
        Left180 = 19,
        /// <summary>
        ///     Left face upfront, rotated 90 left.
        /// </summary>
        Left270 = 20,
        /// <summary>
        ///     Back face upfront, upsidedown.
        /// </summary>
        Back180 = 21,
        /// <summary>
        ///     Back face upfront, rotated 90 left.
        /// </summary>
        Back270 = 22,
        /// <summary>
        ///     Bottom face upfront, upsidedown.
        /// </summary>
        Bottom180 = 23,
        /// <summary>
        ///     Bottom face upfront, rotated 90 left.
        /// </summary>
        Bottom270 = 24
    }

    /// <summary>
    /// PlanogramPositionOrientationType Helper Class
    /// </summary>
    public static class PlanogramPositionOrientationTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramPositionOrientationType, String> FriendlyNames =
            new Dictionary<PlanogramPositionOrientationType, String>()
            {
                {PlanogramPositionOrientationType.Default, Message.Enum_PlanogramPositionOrientationType_Default},
                {PlanogramPositionOrientationType.Front0, Message.Enum_PlanogramProductOrientationType_Front0},
                {PlanogramPositionOrientationType.Front90, Message.Enum_PlanogramProductOrientationType_Front90},
                {PlanogramPositionOrientationType.Front180, Message.Enum_PlanogramProductOrientationType_Front180},
                {PlanogramPositionOrientationType.Front270, Message.Enum_PlanogramProductOrientationType_Front270},
                {PlanogramPositionOrientationType.Top0, Message.Enum_PlanogramProductOrientationType_Top0},
                {PlanogramPositionOrientationType.Top90, Message.Enum_PlanogramProductOrientationType_Top90},
                 {PlanogramPositionOrientationType.Top180, Message.Enum_PlanogramProductOrientationType_Top180},
                {PlanogramPositionOrientationType.Top270, Message.Enum_PlanogramProductOrientationType_Top270},
                {PlanogramPositionOrientationType.Right0, Message.Enum_PlanogramProductOrientationType_Right0},
                {PlanogramPositionOrientationType.Right90, Message.Enum_PlanogramProductOrientationType_Right90},
                {PlanogramPositionOrientationType.Right180, Message.Enum_PlanogramProductOrientationType_Right180},
                {PlanogramPositionOrientationType.Right270, Message.Enum_PlanogramProductOrientationType_Right270},
                {PlanogramPositionOrientationType.Left0, Message.Enum_PlanogramProductOrientationType_Left0},
                {PlanogramPositionOrientationType.Left90, Message.Enum_PlanogramProductOrientationType_Left90},
                {PlanogramPositionOrientationType.Left180, Message.Enum_PlanogramProductOrientationType_Left180},
                {PlanogramPositionOrientationType.Left270, Message.Enum_PlanogramProductOrientationType_Left270},
                {PlanogramPositionOrientationType.Back0, Message.Enum_PlanogramProductOrientationType_Back0},
                {PlanogramPositionOrientationType.Back90, Message.Enum_PlanogramProductOrientationType_Back90},
                {PlanogramPositionOrientationType.Back180, Message.Enum_PlanogramProductOrientationType_Back180},
                {PlanogramPositionOrientationType.Back270, Message.Enum_PlanogramProductOrientationType_Back270},
                {PlanogramPositionOrientationType.Bottom0, Message.Enum_PlanogramProductOrientationType_Bottom0},
                {PlanogramPositionOrientationType.Bottom90, Message.Enum_PlanogramProductOrientationType_Bottom90},
                {PlanogramPositionOrientationType.Bottom180, Message.Enum_PlanogramProductOrientationType_Bottom180},
                {PlanogramPositionOrientationType.Bottom270, Message.Enum_PlanogramProductOrientationType_Bottom270},
            };

       

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramProductOrientationType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramProductOrientationType>()
            {
                {Message.Enum_PlanogramProductOrientationType_Front0.ToLowerInvariant(), PlanogramProductOrientationType.Front0},
                {Message.Enum_PlanogramProductOrientationType_Front90.ToLowerInvariant(), PlanogramProductOrientationType.Front90},
                {Message.Enum_PlanogramProductOrientationType_Top0.ToLowerInvariant(), PlanogramProductOrientationType.Top0},
                {Message.Enum_PlanogramProductOrientationType_Top90.ToLowerInvariant(), PlanogramProductOrientationType.Top90},
                {Message.Enum_PlanogramProductOrientationType_Right0.ToLowerInvariant(), PlanogramProductOrientationType.Right0},
                {Message.Enum_PlanogramProductOrientationType_Right90.ToLowerInvariant(), PlanogramProductOrientationType.Right90},
                {Message.Enum_PlanogramProductOrientationType_Left0.ToLowerInvariant(), PlanogramProductOrientationType.Left0},
                {Message.Enum_PlanogramProductOrientationType_Left90.ToLowerInvariant(), PlanogramProductOrientationType.Left90},
                {Message.Enum_PlanogramProductOrientationType_Back0.ToLowerInvariant(), PlanogramProductOrientationType.Back0},
                {Message.Enum_PlanogramProductOrientationType_Back90.ToLowerInvariant(), PlanogramProductOrientationType.Back90},
                {Message.Enum_PlanogramProductOrientationType_Bottom0.ToLowerInvariant(), PlanogramProductOrientationType.Bottom0},
                {Message.Enum_PlanogramProductOrientationType_Bottom90.ToLowerInvariant(), PlanogramProductOrientationType.Bottom90},
                {Message.Enum_PlanogramProductOrientationType_Front180.ToLowerInvariant(), PlanogramProductOrientationType.Front180},
                {Message.Enum_PlanogramProductOrientationType_Front270.ToLowerInvariant(), PlanogramProductOrientationType.Front270},
                {Message.Enum_PlanogramProductOrientationType_Top180.ToLowerInvariant(), PlanogramProductOrientationType.Top180},
                {Message.Enum_PlanogramProductOrientationType_Top270.ToLowerInvariant(), PlanogramProductOrientationType.Top270},
                {Message.Enum_PlanogramProductOrientationType_Right180.ToLowerInvariant(), PlanogramProductOrientationType.Right180},
                {Message.Enum_PlanogramProductOrientationType_Right270.ToLowerInvariant(), PlanogramProductOrientationType.Right270},
                {Message.Enum_PlanogramProductOrientationType_Left180.ToLowerInvariant(), PlanogramProductOrientationType.Left180},
                {Message.Enum_PlanogramProductOrientationType_Left270.ToLowerInvariant(), PlanogramProductOrientationType.Left270},
                {Message.Enum_PlanogramProductOrientationType_Back180.ToLowerInvariant(), PlanogramProductOrientationType.Back180},
                {Message.Enum_PlanogramProductOrientationType_Back270.ToLowerInvariant(), PlanogramProductOrientationType.Back270},
                {Message.Enum_PlanogramProductOrientationType_Bottom180.ToLowerInvariant(), PlanogramProductOrientationType.Bottom180},
                {Message.Enum_PlanogramProductOrientationType_Bottom270.ToLowerInvariant(), PlanogramProductOrientationType.Bottom270},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="friendlyName">Friendly name to return the <see cref="PlanogramPositionOrientationType"/> for.</param>
        /// <returns>The value asociated to the <paramref name="friendlyName"/> or <c>null</c> if no match was found.</returns>
        public static PlanogramProductOrientationType? PlanogramProductOrientationTypeGetEnum(String friendlyName)
        {
            PlanogramProductOrientationType? returnValue = null;
            PlanogramProductOrientationType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }

        /// <summary>
        ///     Extension method to obtain the <see cref="PlanogramProductOrientationType" /> equivalent of the given
        ///     <paramref name="productOrientationType" />.
        /// </summary>
        /// <param name="productOrientationType">
        ///     The <see cref="PlanogramProductOrientationType" /> to obtain the <see cref="PlanogramPositionOrientationType" />
        ///     equivalent of.
        /// </param>
        /// <returns>Equivalent value in the <see cref="PlanogramPositionOrientationType" /> enumeration.</returns>
        public static PlanogramPositionOrientationType ToPositionOrientationType(this PlanogramProductOrientationType productOrientationType)
        {
            switch (productOrientationType)
            {
                case PlanogramProductOrientationType.Front0:
                    return PlanogramPositionOrientationType.Front0;
                case PlanogramProductOrientationType.Front180:
                    return PlanogramPositionOrientationType.Front180;
                case PlanogramProductOrientationType.Front270:
                    return PlanogramPositionOrientationType.Front270;
                case PlanogramProductOrientationType.Front90:
                    return PlanogramPositionOrientationType.Front90;
                case PlanogramProductOrientationType.Top0:
                    return PlanogramPositionOrientationType.Top0;
                case PlanogramProductOrientationType.Top180:
                    return PlanogramPositionOrientationType.Top180;
                case PlanogramProductOrientationType.Top270:
                    return PlanogramPositionOrientationType.Top270;
                case PlanogramProductOrientationType.Top90:
                    return PlanogramPositionOrientationType.Top90;
                case PlanogramProductOrientationType.Right0:
                    return PlanogramPositionOrientationType.Right0;
                case PlanogramProductOrientationType.Right180:
                    return PlanogramPositionOrientationType.Right180;
                case PlanogramProductOrientationType.Right270:
                    return PlanogramPositionOrientationType.Right270;
                case PlanogramProductOrientationType.Right90:
                    return PlanogramPositionOrientationType.Right90;
                case PlanogramProductOrientationType.Left0:
                    return PlanogramPositionOrientationType.Left0;
                case PlanogramProductOrientationType.Left180:
                    return PlanogramPositionOrientationType.Left180;
                case PlanogramProductOrientationType.Left270:
                    return PlanogramPositionOrientationType.Left270;
                case PlanogramProductOrientationType.Left90:
                    return PlanogramPositionOrientationType.Left90;
                case PlanogramProductOrientationType.Back0:
                    return PlanogramPositionOrientationType.Back0;
                case PlanogramProductOrientationType.Back180:
                    return PlanogramPositionOrientationType.Back180;
                case PlanogramProductOrientationType.Back270:
                    return PlanogramPositionOrientationType.Back270;
                case PlanogramProductOrientationType.Back90:
                    return PlanogramPositionOrientationType.Back90;
                case PlanogramProductOrientationType.Bottom0:
                    return PlanogramPositionOrientationType.Bottom0;
                case PlanogramProductOrientationType.Bottom180:
                    return PlanogramPositionOrientationType.Bottom180;
                case PlanogramProductOrientationType.Bottom270:
                    return PlanogramPositionOrientationType.Bottom270;
                case PlanogramProductOrientationType.Bottom90:
                    return PlanogramPositionOrientationType.Bottom90;
                default:
                    throw new ArgumentOutOfRangeException("productOrientationType", productOrientationType, null);
            }
        }
    }
}
