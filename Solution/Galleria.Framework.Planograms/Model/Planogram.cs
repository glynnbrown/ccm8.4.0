﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A. Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25546 : N.Foster
//  Added operation in progress property
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties and removed old obsolete properties
// V8-26147 : L.Ineson
//  Added CustomAttributeData property
// V8-26271 : A.Kuszyk
//  Added Performance property.
// V8-26337 : A.Kuszyk
//  Amended OnChildChanged, such that a new PlanogramPerformanceData is added when a Product is added.
// V8-26426 : A.Kuszyk
//  Added Assortment property.
// V8-26477 : L.Ineson
//  Added friendly names to meta properties
// V8-26573 : L.Luong
//  Added Validation property
// V8-26709 : L.Ineson
//  Removed old unit of measure property.
// V8-26760 : L.Ineson
//  Populated model property display types.
// V8-26706 : L.Luong
//  Added ConsumerDecisionTree property
// V8-26836 : L.Luong
//  Added EventLog property
// V8-26891 : L.Ineson
//  Added Blocking property
// V8-27126 : L.Ineson
//  Added UpdateSizeFromFixtures method
// V8-27062 : A.Silva ~ Implemented OnValidationCalculation() so that ValidationTemplate gets validated.
//  (it won't do by default as it is Lazy loaded)
// V8-27058 : A.Probyn 
//  Extended for CalculateMetaData and ClearMetaData
//  Replaced MetaThumbnailId with PlanogramMetaDataImages
// V8-27237 : A.Silva
//  Extended for ClearValidationData.
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27153 : A.Silva
//  Added RenumberingStrategy property.
// V8-27490 : N.Foster
//  Refactory fast-lookup code and moved into framework
// V8-27468 : L.Ineson
//  Updated GetBlockingAreaSize to consider IsMerchandisedTopDown flag.
// V8-27502 : A.Kuszyk
//  Overloaded GetBlockingSize to allow top down offset value to be returned.
// V8-27411 : M.Pettit
//  RenderPlanogramImage.Process fetch now requires userId and locktype
// V8-27458 : M.Shelley
//  Added a null check for the PlanogramMetadataImages property in the OnClearMetadata method as
//  this was throwing a Null Ref Exception, preventing saving a Planogram to a file.
// V8-27496 : L.Luong
//  Added ClearImages method
// V8-27745 : A.Kuszyk
//  Added GetMerchandisingGroups method.
// V8-27647 : L.Luong
//  Added Inventory Property
// V8-27740 : L.Ineson
// Added GetPlanogramMetadataDetails
// V8-27411 : M.Pettit
//  Adde UserName (owner) property
// V8-27938 : N.Haywood
//  Added EnumerateDisplayableFieldInfos
// V8-27783 : M.Brumby
//	Added Planogram.UniqueContentReference
// V8-25976 : L.Ineson
//  Added GetAllBrokenValidationWarnings method.
// V8-28147 : D.Pleasance
//  Added business rule for UserName
// V8-27726 : L.ineson
//  Added mark new overide and moved new ucr set to there so that
// it gets called on package save as.
// V8-28228 : A.Kuszyk
//  Amended the metadata calculations for available merchandisable space and white space.
// V8-28466 : A.Silva
//  Amended GetCarParkShelf() so that the new car park shelf gets marked as such.
// V8-28475 : N.Haywood
//  Changed planogram inventory object field infos to display planogram inventory instead of planogram
// V8-28515 : L.Ineson
//  Commented out username required business rule

#endregion
#region Version History: CCM801

// V8-28722 : N.Foster
//  Added support for batch dal operations
// V8-28676 : A.Silva
//      Amended GetImageAsync so that it will try to get the image from other sources if there is no image stored in the planogram.
// V8-28759 : J.Pickup
//      MetaMinDos, MetaMaxDos, MetaAverageDos now get calculated.
// V8-28862 : A.Kuszyk
//  Corrected issue with CalculateMerchandisableSpace returning negative results in some circumstances.

#endregion
#region Version History: CCM802

// V8-28987 : I.George
// Added LocationCode, LocationName, ClusterSchemeName and ClusterName Properties
// V8-28996 : A.Silva
//      Added Sequence Property.
// V8-29028 : L.Ineson
//  GetMerchandisingGroups no longer returns groups for subs that have a merch type of none.

#endregion
#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-29643 : D.Pleasance
//  Added EnumerateAttributeFieldInfos. For holding details of properties that can be updated through update planogram attribute task.
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 

#endregion
#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29844 : L.Ineson
//  Added more metadata
// V8-29836 : L.Ineson
//  Made sure that cluster scheme and cluster name are in field infos.
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-29192 : L.Ineson
//  Added method to process metadata and get broken warnings in one hit
//  to speed up space planning.
// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents
// V8-30043 : N.Haywood
//  Fixed bad assertion in GetFieldValue

#endregion
#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>. 
// V8-30352 : A.Probyn
//  Updated OnCalculateMetadata so that the white space is calculated at a cumulative merchandising space level, 
//  not as a total for the planogram. Components which are overfaced were masking white space on other shelves.
// V8-30449 : A.Probyn
//  Corrected MetaNotAchievedInventory meta data calculation.
// V8-30398 : A.Silva
//  Amended MetaTotalFacings to use PlanogramHelper.CalculateMetaTotalFacings (that accounts for tray real facings).
// V8-30574 : M.Brumby
//  White space calculations now correctly divide by the number of merch spaces instead of only when the merch space is null.
#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//  Added HighlightSequenceStrategy property.
// V8-30794 : A.Kuszyk
//  Added UpdateProductColoursFromHighlight method.
// V8-30738 : L.Ineson
//  Updated field info group names.
// V8-30536 : A.Kuszyk
//  Re-factored CalculateMetadataAndGetValidationWarnings to accept a list of merchandising groups.
// V8-30762 : I.George
//  Added Assortment to Calculate MetaData method
// V8-30705 : A.Probyn
//  Added new assortment rules related meta data properties
//  Updated OnCalculateMetadata & CalculatePostPlanogramMetadata
// V8-31128 : D.Pleasance
//  Added Info settings to EnumerateAttributeFieldInfos
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
// V8-31164 : D.Pleasance
//  Amended UpdateProductBlockingColours \ UpdateProductSequenceNumbers to update Sequence Colour \ Sequence Number on the position
// V8-31172 : A.Kuszyk
//  Refactoed UpdateProductBlockingColours and UpdateProductSequenceNumbers into UpdatePositionSequenceData.
// V8-31222 : L.Ineson
//  Removed yield of unpopulated metadata properties from EnumerateDisplayableFieldInfos
// V8-31269 : L.Ineson
//  Added method for image synchronous fetch.
// V8-31483 : N.Foster
//  Improved performance of validation data calculation
#endregion
#region Version History: CCM830
// V8-31294 : D.Pleasance
//  Amended Name 255 chars.
// V8-31819 : A.Silva
//  Added Comparison property to reference the planogram's Comparison object.
// V8-32143 : A.Silva
//  Added normalizing method for bays, so that their X coordinates can be set to positive values.
// V8-31804 : D.Pleasance
//  Added GetMerchandisingGroupsExcludingCarpark().
// V8-32396 : A.Probyn
//  Added MetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32633 : N.Haywood
//  Moved CleanUpOrphanedParts to the planogram model
// V8-32359 : A.Silva
//  Added UpdateProductAttributes to batch update the products in the instance.
// V8-32617 : D.Pleasance
//  Amended MetaMinDos, MetaMaxDos, MetaAverageDos - calculated based on products that are placed rather than all planogram products.
// V8-32782 : L.Ineson
//  Updated CleanUpOrphanedParts to include fixturecomponents etc that link to an item which does not exist.
// V8-32817 : A.Silva
//  Amended CreateUpdateProduct to ensure the correct type is used when updating the attribute via reflection.
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
// V8-32886 : A.Silva
//  Amended CreateUpdateProduct to deal with nullable types.
// V8-32953 : L.Ineson
// Added userstate parameter to GetImageLazy.
// V8-32967 : A.Silva
//  Amended CreateUpdateProduct to correctly convert Enum values from Int32.
// CCM-18563 : A.Silva
//  Added GetMerchandisingGroupsInBlockingArea to get only merch groups within, or partially within, the blocking area.
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Csla;
using Csla.Core;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{ 
    /// <summary>
    /// Model representing a Planogram object
    /// </summary>
    [Serializable]
    public sealed partial class Planogram : ModelObject<Planogram>, ICustomAttributeDataParent, IProductAttributeValidationWarning
    {
        #region Fields

        private Object _subComponentCacheLock = new Object(); // object used for locking
        [NonSerialized]
        private Dictionary<Object, PlanogramSubComponent> _subComponentCache; // sub component cache object

        private Object _fixtureComponentCacheLock = new Object(); // object used for locking
        [NonSerialized]
        private Dictionary<Object, PlanogramFixtureComponent> _fixtureComponentCache; // fixture component cache object

        private Object _fixtureAssemblyCacheLock = new Object(); // object used for locking
        [NonSerialized]
        private Dictionary<Object, PlanogramFixtureAssembly> _fixtureAssemblyCache; // fixture assembly cache object

        private Object _assemblyComponentCacheLock = new Object(); // object used for locking
        [NonSerialized]
        private Dictionary<Object, PlanogramAssemblyComponent> _assemblyComponentCache; // assembly component cache object

        private Object _subComponentPlacementCacheLock = new Object(); // object used for locking
        [NonSerialized]
        private List<PlanogramSubComponentPlacement> _subComponentPlacementCache; // sub component placement cache
        
        [NonSerialized]
        private PlanogramMetadataDetails _metadataDetails; 

        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Package Parent
        {
            get 
            {   
                PlanogramList parentList = base.Parent as PlanogramList;
                if (parentList == null) return null;
                return parentList.Parent; 
            }
        }
        #endregion

        #region Properties

        #region UniqueContentReference
        /// <summary>
        /// UniqueContentReference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference, Message.Planogram_UniqueContentReference);
        /// <summary>
        /// Gets or sets the planogram UniqueContentReference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return this.GetProperty<Guid>(UniqueContentReferenceProperty); }
            set { this.SetProperty<Guid>(UniqueContentReferenceProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);
        /// <summary>
        /// Gets or sets the planogram name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.Generic_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the planogram height
        /// </summary>
        public Single Height
        {
            get { return this.GetProperty<Single>(HeightProperty); }
            set { this.SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.Generic_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the planogram width
        /// </summary>
        public Single Width
        {
            get { return this.GetProperty<Single>(WidthProperty); }
            set { this.SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.Generic_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets or sets the planogram depth
        /// </summary>
        public Single Depth
        {
            get { return this.GetProperty<Single>(DepthProperty); }
            set { this.SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region UserName
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        /// <summary>
        /// Gets or sets the owner of the planogram
        /// </summary>
        public String UserName
        {
            get { return this.GetProperty<String>(UserNameProperty); }
            set { this.SetProperty<String>(UserNameProperty, value); }
        }
        #endregion

        #region LengthUnitsOfMeasure
        /// <summary>
        /// LengthUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramLengthUnitOfMeasureType> LengthUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramLengthUnitOfMeasureType>(c => c.LengthUnitsOfMeasure, Message.Planogram_LengthUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for length values
        /// 0 = Unknown, 1 = Centimeters, 2 = Inches
        /// </summary>
        public PlanogramLengthUnitOfMeasureType LengthUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty); }
            set { this.SetProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region AreaUnitsOfMeasure
        /// <summary>
        /// AreaUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAreaUnitOfMeasureType> AreaUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramAreaUnitOfMeasureType>(c => c.AreaUnitsOfMeasure, Message.Planogram_AreaUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for area values
        /// 0 = Unknown, 1 = Square Centimeters, 2 = Square Inches
        /// </summary>
        public PlanogramAreaUnitOfMeasureType AreaUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty); }
            set { this.SetProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region VolumeUnitsOfMeasure
        /// <summary>
        /// VolumeUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramVolumeUnitOfMeasureType> VolumeUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramVolumeUnitOfMeasureType>(c => c.VolumeUnitsOfMeasure, Message.Planogram_VolumeUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for volume values
        /// 0 = Unknown, 1 = Litres, 2 = Cubic Feet
        /// </summary>
        public PlanogramVolumeUnitOfMeasureType VolumeUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty); }
            set { this.SetProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region WeightUnitsOfMeasure
        /// <summary>
        /// WeightUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramWeightUnitOfMeasureType> WeightUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramWeightUnitOfMeasureType>(c => c.WeightUnitsOfMeasure, Message.Planogram_WeightUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for weight values
        /// 0 = Unknown, 1 = Kilograms, 2 = Pounds
        /// </summary>
        public PlanogramWeightUnitOfMeasureType WeightUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty); }
            set { this.SetProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region AngleUnitsOfMeasure
        /// <summary>
        /// AngleUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> AngleUnitsOfMeasureProperty =
            RegisterModelProperty<Byte>(c => c.AngleUnitsOfMeasure, Message.Planogram_AngleUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for angle values
        /// 0 = Unknown, 1 = Radians, 2 = Degrees
        /// </summary>
        public Byte AngleUnitsOfMeasure
        {
            get { return this.GetProperty<Byte>(AngleUnitsOfMeasureProperty); }
            set { this.SetProperty<Byte>(AngleUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region CurrencyUnitsOfMeasure
        /// <summary>
        /// CurrencyUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramCurrencyUnitOfMeasureType> CurrencyUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramCurrencyUnitOfMeasureType>(c => c.CurrencyUnitsOfMeasure, Message.Planogram_CurrencyUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Unknown, 1 = GBP, 2 = USD
        /// </summary>
        public PlanogramCurrencyUnitOfMeasureType CurrencyUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty); }
            set { this.SetProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region ProductPlacementX
        /// <summary>
        /// ProductPlacementX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementXType> ProductPlacementXProperty =
            RegisterModelProperty<PlanogramProductPlacementXType>(c => c.ProductPlacementX, Message.Planogram_ProductPlacementX);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillWide
        /// </summary>
        public PlanogramProductPlacementXType ProductPlacementX
        {
            get { return this.GetProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty); }
            set { this.SetProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, value); }
        }
        #endregion

        #region ProductPlacementY
        /// <summary>
        /// ProductPlacementY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementYType> ProductPlacementYProperty =
            RegisterModelProperty<PlanogramProductPlacementYType>(c => c.ProductPlacementY, Message.Planogram_ProductPlacementY);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillHigh
        /// </summary>
        public PlanogramProductPlacementYType ProductPlacementY
        {
            get { return this.GetProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty); }
            set { this.SetProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, value); }
        }
        #endregion

        #region ProductPlacementZ
        /// <summary>
        /// ProductPlacementZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementZType> ProductPlacementZProperty =
            RegisterModelProperty<PlanogramProductPlacementZType>(c => c.ProductPlacementZ, Message.Planogram_ProductPlacementZ);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillDeep
        /// </summary>
        public PlanogramProductPlacementZType ProductPlacementZ
        {
            get { return this.GetProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty); }
            set { this.SetProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, value); }
        }
        #endregion

        #region FixtureItems
        /// <summary>
        /// FixtureItems property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureItemList> FixtureItemsProperty =
            RegisterModelProperty<PlanogramFixtureItemList>(c => c.FixtureItems, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The fixture structure for this planogram
        /// </summary>
        public PlanogramFixtureItemList FixtureItems
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureItemList>(
                    FixtureItemsProperty,
                    new PlanogramFixtureItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The fixture structure for this planogram
        /// </summary>
        public PlanogramFixtureItemList FixtureItemsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureItemList>(
                    FixtureItemsProperty,
                    new PlanogramFixtureItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Fixtures
        /// <summary>
        /// Fixtures property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFixtureList> FixturesProperty =
            RegisterModelProperty<PlanogramFixtureList>(c => c.Fixtures, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The fixtures used by this planogram
        /// </summary>
        public PlanogramFixtureList Fixtures
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureList>(
                    FixturesProperty,
                    new PlanogramFixtureList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The fixtures used by this planogram
        /// </summary>
        public PlanogramFixtureList FixturesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramFixtureList>(
                    FixturesProperty,
                    new PlanogramFixtureList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Assemblies
        /// <summary>
        /// Assemblies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssemblyList> AssembliesProperty =
            RegisterModelProperty<PlanogramAssemblyList>(c => c.Assemblies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The assemblies used by this planogram
        /// </summary>
        public PlanogramAssemblyList Assemblies
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssemblyList>(
                    AssembliesProperty,
                    new PlanogramAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The assemblies used by this planogram
        /// </summary>
        public PlanogramAssemblyList AssembliesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssemblyList>(
                    AssembliesProperty,
                    new PlanogramAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComponentList> ComponentsProperty =
            RegisterModelProperty<PlanogramComponentList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The components used by this planogram
        /// </summary>
        public PlanogramComponentList Components
        {
            get
            {
                return this.GetPropertyLazy<PlanogramComponentList>(
                    ComponentsProperty,
                    new PlanogramComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The components used by this planogram
        /// </summary>
        public PlanogramComponentList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramComponentList>(
                    ComponentsProperty,
                    new PlanogramComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Products
        /// <summary>
        /// Products property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductList> ProductsProperty =
            RegisterModelProperty<PlanogramProductList>(c => c.Products, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The products used by this planogram
        /// </summary>
        public PlanogramProductList Products
        {
            get
            {
                return this.GetPropertyLazy<PlanogramProductList>(
                    ProductsProperty,
                    new PlanogramProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The products used by this planogram
        /// </summary>
        public PlanogramProductList ProductsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramProductList>(
                    ProductsProperty,
                    new PlanogramProductList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Positions
        /// <summary>
        /// Positions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPositionList> PositionsProperty =
            RegisterModelProperty<PlanogramPositionList>(c => c.Positions, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child posiitions of this planogram
        /// </summary>
        public PlanogramPositionList Positions
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPositionList>(
                    PositionsProperty,
                    new PlanogramPositionList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child positions of this planogram
        /// </summary>
        public PlanogramPositionList PositionsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPositionList>(
                    PositionsProperty,
                    new PlanogramPositionList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Annotations
        /// <summary>
        /// Annotations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAnnotationList> AnnotationsProperty =
            RegisterModelProperty<PlanogramAnnotationList>(c => c.Annotations, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child Annotations of this planogram
        /// </summary>
        public PlanogramAnnotationList Annotations
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAnnotationList>(
                    AnnotationsProperty,
                    new PlanogramAnnotationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child Annotations of this planogram
        /// </summary>
        public PlanogramAnnotationList AnnotationsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAnnotationList>(
                    AnnotationsProperty,
                    new PlanogramAnnotationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Images
        /// <summary>
        /// Images property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramImageList> ImagesProperty =
            RegisterModelProperty<PlanogramImageList>(c => c.Images, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child Images of this planogram
        /// </summary>
        public PlanogramImageList Images
        {
            get
            {
                return this.GetPropertyLazy<PlanogramImageList>(
                    ImagesProperty,
                    new PlanogramImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child Images of this planogram
        /// </summary>
        public PlanogramImageList ImagesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramImageList>(
                    ImagesProperty,
                    new PlanogramImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region CategoryName
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryNameProperty =
            RegisterModelProperty<String>(c => c.CategoryName, Message.Planogram_CategoryName);
        /// <summary>
        /// Gets or sets the planogram Category name
        /// </summary>
        public String CategoryName
        {
            get { return this.GetProperty<String>(CategoryNameProperty); }
            set { this.SetProperty<String>(CategoryNameProperty, value); }
        }
        #endregion

        #region CategoryCode
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryCodeProperty =
            RegisterModelProperty<String>(c => c.CategoryCode, Message.Planogram_CategoryCode);
        /// <summary>
        /// Gets or sets the planogram Category Code
        /// </summary>
        public String CategoryCode
        {
            get { return this.GetProperty<String>(CategoryCodeProperty); }
            set { this.SetProperty<String>(CategoryCodeProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaBayCount
        /// <summary>
        /// MetaBayCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBayCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBayCount, Message.Planogram_MetaBayCount);
        /// <summary>
        /// Gets/Sets the meta data bay count;
        /// </summary>
        public Int32? MetaBayCount
        {
            get { return this.GetProperty<Int32?>(MetaBayCountProperty); }
            set { this.SetProperty<Int32?>(MetaBayCountProperty, value); }
        }
        #endregion

        #region MetaUniqueProductCount
        /// <summary>
        /// MetaUniqueProductCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaUniqueProductCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaUniqueProductCount, Message.Planogram_MetaUniqueProductCount);
        /// <summary>
        /// Gets/Sets the meta data unique product count;
        /// </summary>
        public Int32? MetaUniqueProductCount
        {
            get { return this.GetProperty<Int32?>(MetaUniqueProductCountProperty); }
            set { this.SetProperty<Int32?>(MetaUniqueProductCountProperty, value); }
        }
        #endregion

        #region MetaComponentCount
        /// <summary>
        /// MetaComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaComponentCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaComponentCount, Message.Planogram_MetaComponentCount);
        /// <summary>
        /// Gets/Sets the meta data component count;
        /// </summary>
        public Int32? MetaComponentCount
        {
            get { return this.GetProperty<Int32?>(MetaComponentCountProperty); }
            set { this.SetProperty<Int32?>(MetaComponentCountProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace, Message.Planogram_MetaTotalMerchandisableLinearSpace, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace, Message.Planogram_MetaTotalMerchandisableAreaSpace, ModelPropertyDisplayType.Area);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace,
            Message.Planogram_MetaTotalMerchandisableVolumetricSpace, ModelPropertyDisplayType.LengthCubed);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace, Message.Planogram_MetaTotalLinearWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace, Message.Planogram_MetaTotalAreaWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace, Message.Planogram_MetaTotalVolumetricWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced, Message.Planogram_MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsPlacedProperty, value); }
        }
        #endregion

        #region MetaProductsUnplaced
        /// <summary>
        /// MetaProductsUnplaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsUnplacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsUnplaced, Message.Planogram_MetaProductsUnplaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsUnplaced
        /// </summary>
        public Int32? MetaProductsUnplaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsUnplacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsUnplacedProperty, value); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts, Message.Planogram_MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
            set { this.SetProperty<Int32?>(MetaNewProductsProperty, value); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount, Message.Planogram_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
            set { this.SetProperty<Int32?>(MetaChangesFromPreviousCountProperty, value); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating, Message.Planogram_MetaChangeFromPreviousStarRating);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
            set { this.SetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, value); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped, Message.Planogram_MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
            set { this.SetProperty<Int32?>(MetaBlocksDroppedProperty, value); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory, Message.Planogram_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Int32?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, Message.Planogram_MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFacings

        /// <summary>
        ///     Property definition for <see cref="MetaAverageFacings"/>.
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFacings, Message.Planogram_MetaAverageFacings);

        /// <summary>
        ///     Gets or sets the average number of <c>Facings</c> per <c>Product</c> on the <c>Planogram</c>.
        /// </summary>
        /// <remarks>
        ///     The value is calculated as:
        ///     <para></para>
        ///     Average of <c>PlanogramPosition_FacingsWide</c> for all the records linked to the <c>Planogram</c> in the
        ///     <c>planogram position</c> table.
        /// </remarks>
        public Single? MetaAverageFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFacingsProperty); } 
            set { this.SetProperty<Single?>(MetaAverageFacingsProperty, value); }
        }
        
        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, Message.Planogram_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }
        #endregion

        #region MetaAverageUnits

        /// <summary>
        ///     Property definition for <see cref="MetaAverageUnits"/>.
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageUnitsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageUnits, Message.Planogram_MetaAverageUnits);

        /// <summary>
        ///     Gets or sets the average number of <c>Units</c> per <c>Product</c> on the <c>Planogram</c>.
        /// </summary>
        /// <remarks>
        ///     The value is calculated as:
        ///     <para></para>
        ///     Average of <c>PlanogramPosition_TotalUnits</c> for all the records linked to the <c>Planogram</c> in the
        ///     <c>planogram position</c> table.
        /// </remarks>
        public Single? MetaAverageUnits
        {
            get { return this.GetProperty<Single?>(MetaAverageUnitsProperty); }
            set { this.SetProperty<Single?>(MetaAverageUnitsProperty, value); }
        }
        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos, Message.Planogram_MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
            set { this.SetProperty<Single?>(MetaMinDosProperty, value); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos, Message.Planogram_MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
            set { this.SetProperty<Single?>(MetaMaxDosProperty, value); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos, Message.Planogram_MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
            set { this.SetProperty<Single?>(MetaAverageDosProperty, value); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases, Message.Planogram_MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
            set { this.SetProperty<Single?>(MetaMinCasesProperty, value); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases, Message.Planogram_MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
            set { this.SetProperty<Single?>(MetaAverageCasesProperty, value); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases, Message.Planogram_MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
            set { this.SetProperty<Single?>(MetaMaxCasesProperty, value); }
        }
        #endregion
        
        #region PlanogramMetadataImages
        /// <summary>
        /// PlanogramMetaDataImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramMetadataImageList> PlanogramMetadataImagesProperty =
            RegisterModelProperty<PlanogramMetadataImageList>(c => c.PlanogramMetadataImages, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The child meta data images of this planogram
        /// </summary>
        public PlanogramMetadataImageList PlanogramMetadataImages
        {
            get
            {
                return this.GetPropertyLazy<PlanogramMetadataImageList>(
                    PlanogramMetadataImagesProperty,
                    new PlanogramMetadataImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child meta data images of this planogram
        /// </summary>
        public PlanogramMetadataImageList PlanogramMetaDataImagesAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramMetadataImageList>(
                    PlanogramMetadataImagesProperty,
                    new PlanogramMetadataImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region MetaTotalComponentCollisions
        /// <summary>
        /// MetaTotalComponentCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentCollisions, Message.Planogram_MetaTotalComponentCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentCollisions
        /// </summary>
        public Int32? MetaTotalComponentCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedDepth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedDepthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedDepth, Message.Planogram_MetaTotalComponentsOverMerchandisedDepth);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedDepth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedHeight
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedHeightProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedHeight, Message.Planogram_MetaTotalComponentsOverMerchandisedHeight);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedHeight
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedWidth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedWidthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedWidth, Message.Planogram_MetaTotalComponentsOverMerchandisedWidth);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedWidth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty, value); }
        }
        #endregion

        #region MetaHasComponentsOutsideOfFixtureArea
        /// <summary>
        /// HasComponentsOutsideOfFixtureArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaHasComponentsOutsideOfFixtureAreaProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaHasComponentsOutsideOfFixtureArea, Message.Planogram_MetaHasComponentsOutsideOfFixtureArea);
        /// <summary>
        /// Gets/Sets the meta data HasComponentsOutsideOfFixtureArea
        /// </summary>
        public Boolean? MetaHasComponentsOutsideOfFixtureArea
        {
            get { return this.GetProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty); }
            set { this.SetProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty, value); }
        }
        #endregion

        #region MetaTotalPositionCollisions
        /// <summary>
        /// MetaTotalPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalPositionCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalPositionCollisions, Message.Planogram_MetaTotalPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalPositionCollisions
        /// </summary>
        public Int32? MetaTotalPositionCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalPositionCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalPositionCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, Message.Planogram_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaTotalFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, Message.Planogram_MetaAverageFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
            set { this.SetProperty<Single?>(MetaAverageFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaNoOfErrors
        /// <summary>
        /// MetaNoOfErrors property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfErrorsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfErrors, Message.Planogram_MetaNoOfErrors);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfErrors
        /// </summary>
        public Int32? MetaNoOfErrors
        {
            get { return this.GetProperty<Int32?>(MetaNoOfErrorsProperty); }
            set { this.SetProperty<Int32?>(MetaNoOfErrorsProperty, value); }
        }
        #endregion

        #region MetaNoOfWarnings
        /// <summary>
        /// MetaNoOfWarnings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfWarningsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfWarnings, Message.Planogram_MetaNoOfWarnings);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfWarnings
        /// </summary>
        public Int32? MetaNoOfWarnings
        {
            get { return this.GetProperty<Int32?>(MetaNoOfWarningsProperty); }
            set { this.SetProperty<Int32?>(MetaNoOfWarningsProperty, value); }
        }
        #endregion

        #region MetaNoOfInformationEvents
        /// <summary>
        /// MetaNoOfInformationEvents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfInformationEventsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfInformationEvents, Message.Planogram_MetaNoOfInformationEvents);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfInformationEvents
        /// </summary>
        public Int32? MetaNoOfInformationEvents
        {
            get { return this.GetProperty<Int32?>(MetaNoOfInformationEventsProperty); }
            set { this.SetProperty<Int32?>(MetaNoOfInformationEventsProperty, value); }
        }
        #endregion

        #region MetaNoOfDebugEvents
        /// <summary>
        /// MetaNoOfDebugEvents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfDebugEventsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfDebugEvents, Message.Planogram_MetaNoOfDebugEvents);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfDebugEvents
        /// </summary>
        public Int32? MetaNoOfDebugEvents
        {
            get { return this.GetProperty<Int32?>(MetaNoOfDebugEventsProperty); }
            set { this.SetProperty<Int32?>(MetaNoOfDebugEventsProperty, value); }
        }
        #endregion

        #region MetaTotalErrorScore
        /// <summary>
        /// MetaTotalErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalErrorScoreProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalErrorScore, Message.Planogram_MetaTotalErrorScore);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalErrorScore
        /// </summary>
        public Int32? MetaTotalErrorScore
        {
            get { return this.GetProperty<Int32?>(MetaTotalErrorScoreProperty); }
            set { this.SetProperty<Int32?>(MetaTotalErrorScoreProperty, value); }
        }
        #endregion

        #region MetaHighestErrorScore
        /// <summary>
        /// MetaHighestErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> MetaHighestErrorScoreProperty =
            RegisterModelProperty<Byte?>(c => c.MetaHighestErrorScore, Message.Planogram_MetaHighestErrorScore);
        /// <summary>
        /// Gets/Sets the meta data MetaHighestErrorScore
        /// </summary>
        public Byte? MetaHighestErrorScore
        {
            get { return this.GetProperty<Byte?>(MetaHighestErrorScoreProperty); }
            set { this.SetProperty<Byte?>(MetaHighestErrorScoreProperty, value); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedCases
        /// <summary>
        /// MetaCountOfProductNotAchievedCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedCasesProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedCases, Message.Planogram_MetaCountOfProductNotAchievedCases);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedCases value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedCases
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty, value); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedDOS
        /// <summary>
        /// MetaCountOfProductNotAchievedDOS property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedDOSProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedDOS, Message.Planogram_MetaCountOfProductNotAchievedDOS);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedDOS value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedDOS
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty, value); }
        }
        #endregion

        #region MetaCountOfProductOverShelfLifePercent
        /// <summary>
        /// MetaCountOfProductOverShelfLifePercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductOverShelfLifePercentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductOverShelfLifePercent, Message.Planogram_MetaCountOfProductOverShelfLifePercent);

        /// <summary>
        /// Gets/Sets MetaCountOfProductOverShelfLifePercent value
        /// </summary>
        public Int32? MetaCountOfProductOverShelfLifePercent
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty, value); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedDeliveries
        /// <summary>
        /// MetaCountOfProductNotAchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedDeliveriesProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedDeliveries, Message.Planogram_MetaCountOfProductNotAchievedDeliveries);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedDeliveries value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedDeliveries
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty, value); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedCases
        /// <summary>
        /// MetaPercentOfProductNotAchievedCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedCases, Message.Planogram_MetaPercentOfProductNotAchievedCases,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedCases value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedCases
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty, value); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedDOS
        /// <summary>
        /// MetaPercentOfProductNotAchievedDOS property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedDOSProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedDOS, Message.Planogram_MetaPercentOfProductNotAchievedDOS,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedDOS value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedDOS
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty, value); }
        }
        #endregion

        #region MetaPercentOfProductOverShelfLifePercent
        /// <summary>
        /// MetaPercentOfProductOverShelfLifePercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductOverShelfLifePercentProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductOverShelfLifePercent, Message.Planogram_MetaPercentOfProductOverShelfLifePercent,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductOverShelfLifePercent value
        /// </summary>
        public Single? MetaPercentOfProductOverShelfLifePercent
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty, value); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedDeliveries
        /// <summary>
        /// MetaPercentOfProductNotAchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedDeliveriesProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedDeliveries, Message.Planogram_MetaPercentOfProductNotAchievedDeliveries,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedDeliveries value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedDeliveries
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty, value); }
        }
        #endregion

        #region MetaCountOfPositionsOutsideOfBlockSpace
        /// <summary>
        /// MetaCountOfPositionsOutsideOfBlockSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPositionsOutsideOfBlockSpaceProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPositionsOutsideOfBlockSpace, Message.Planogram_MetaCountOfPositionsOutsideOfBlockSpace);

        /// <summary>
        /// Gets/Sets MetaCountOfPositionsOutsideOfBlockSpace value
        /// </summary>
        public Int32? MetaCountOfPositionsOutsideOfBlockSpace
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty, value); }
        }
        #endregion

        #region MetaPercentOfPositionsOutsideOfBlockSpace
        /// <summary>
        /// MetaPercentOfPositionsOutsideOfBlockSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfPositionsOutsideOfBlockSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfPositionsOutsideOfBlockSpace,
            Message.Planogram_MetaPercentOfPositionsOutsideOfBlockSpace, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfPositionsOutsideOfBlockSpace value
        /// </summary>
        public Single? MetaPercentOfPositionsOutsideOfBlockSpace
        {
            get { return this.GetProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty, value); }
        }
        #endregion

        #region MetaPercentOfPlacedProductsRecommendedInAssortment
        /// <summary>
        /// MetaPercentOfPlacedProductsRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfPlacedProductsRecommendedInAssortmentProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfPlacedProductsRecommendedInAssortment, 
            Message.Planogram_MetaPercentOfPlacedProductsRecommendedInAssortment, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfPlacedProductsRecommendedInAssortment value
        /// </summary>
        public Single? MetaPercentOfPlacedProductsRecommendedInAssortment
        {
            get { return this.GetProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty, value); }
        }
        #endregion

        #region MetaCountOfPlacedProductsRecommendedInAssortment
        /// <summary>
        /// MetaCountOfPlacedProductsRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPlacedProductsRecommendedInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPlacedProductsRecommendedInAssortment, Message.Planogram_MetaCountOfPlacedProductsRecommendedInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfPlacedProductsRecommendedInAssortment value
        /// </summary>
        public Int32? MetaCountOfPlacedProductsRecommendedInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty, value); }
        }
        #endregion

        #region MetaCountOfPlacedProductsNotRecommendedInAssortment
        /// <summary>
        /// MetaCountOfPlacedProductsNotRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPlacedProductsNotRecommendedInAssortment, Message.Planogram_MetaCountOfPlacedProductsNotRecommendedInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfPlacedProductsNotRecommendedInAssortment value
        /// </summary>
        public Int32? MetaCountOfPlacedProductsNotRecommendedInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty, value); }
        }
        #endregion

        #region MetaCountOfRecommendedProductsInAssortment
        /// <summary>
        /// MetaCountOfRecommendedProductsInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfRecommendedProductsInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfRecommendedProductsInAssortment, Message.Planogram_MetaCountOfRecommendedProductsInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfRecommendedProductsInAssortment value
        /// </summary>
        public Int32? MetaCountOfRecommendedProductsInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty); }
            set { this.SetProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty, value); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedInventory
        /// <summary>
        /// MetaPercentOfProductNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedInventoryProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedInventory, Message.Planogram_MetaPercentOfProductNotAchievedInventory,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedInventory value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedInventory
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedInventoryProperty); }
            set { this.SetProperty<Single?>(MetaPercentOfProductNotAchievedInventoryProperty, value); }
        }
        #endregion
	
        #region MetaNumberOfAssortmentRulesBroken
        /// <summary>
        /// MetaNumberOfAssortmentRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfAssortmentRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfAssortmentRulesBroken, Message.Planogram_MetaNumberOfAssortmentRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfAssortmentRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfAssortmentRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfProductRulesBroken
        /// <summary>
        /// MetaNumberOfProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfProductRulesBroken, Message.Planogram_MetaNumberOfProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty, value); }
        }
        #endregion

        #region MetaNumberOfFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfFamilyRulesBroken, Message.Planogram_MetaNumberOfFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty, value); }
        }
        #endregion
                
        #region MetaNumberOfInheritanceRulesBroken
        /// <summary>
        /// MetaNumberOfInheritanceRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfInheritanceRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfInheritanceRulesBroken, Message.Planogram_MetaNumberOfInheritanceRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfInheritanceRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfInheritanceRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfLocalProductRulesBroken
        /// <summary>
        /// MetaNumberOfLocalProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfLocalProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfLocalProductRulesBroken, Message.Planogram_MetaNumberOfLocalProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfLocalProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfLocalProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfDistributionRulesBroken
        /// <summary>
        /// MetaNumberOfDistributionRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDistributionRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDistributionRulesBroken, Message.Planogram_MetaNumberOfDistributionRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDistributionRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDistributionRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfCoreRulesBroken
        /// <summary>
        /// MetaNumberOfCoreRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfCoreRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfCoreRulesBroken, Message.Planogram_MetaNumberOfCoreRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfCoreRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfCoreRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfAssortmentRulesBroken
        /// <summary>
        /// MetaPercentageOfAssortmentRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfAssortmentRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfAssortmentRulesBroken, Message.Planogram_MetaPercentageOfAssortmentRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfAssortmentRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfAssortmentRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfProductRulesBroken
        /// <summary>
        /// MetaPercentageOfProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfProductRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfProductRulesBroken, Message.Planogram_MetaPercentageOfProductRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfProductRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfProductRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfFamilyRulesBroken
        /// <summary>
        /// MetaPercentageOfFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfFamilyRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfFamilyRulesBroken, Message.Planogram_MetaPercentageOfFamilyRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfFamilyRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfFamilyRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfInheritanceRulesBroken
        /// <summary>
        /// MetaPercentageOfInheritanceRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfInheritanceRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfInheritanceRulesBroken, Message.Planogram_MetaPercentageOfInheritanceRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfInheritanceRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfInheritanceRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfLocalProductRulesBroken
        /// <summary>
        /// MetaPercentageOfLocalProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfLocalProductRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfLocalProductRulesBroken, Message.Planogram_MetaPercentageOfLocalProductRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfLocalProductRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfLocalProductRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfDistributionRulesBroken
        /// <summary>
        /// MetaPercentageOfDistributionRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfDistributionRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfDistributionRulesBroken, Message.Planogram_MetaPercentageOfDistributionRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfDistributionRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfDistributionRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaPercentageOfCoreRulesBroken
        /// <summary>
        /// MetaPercentageOfCoreRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfCoreRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfCoreRulesBroken, Message.Planogram_MetaPercentageOfCoreRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfCoreRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfCoreRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty); }
            set { this.SetProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty, value); }
        }
        #endregion

        #region MetaNumberOfDelistProductRulesBroken
        /// <summary>
        /// MetaNumberOfDelistProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDelistProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDelistProductRulesBroken, Message.Planogram_MetaNumberOfDelistProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDelistProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDelistProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfForceProductRulesBroken
        /// <summary>
        /// MetaNumberOfForceProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfForceProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfForceProductRulesBroken, Message.Planogram_MetaNumberOfForceProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfForceProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfForceProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfPreserveProductRulesBroken
        /// <summary>
        /// MetaNumberOfPreserveProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfPreserveProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfPreserveProductRulesBroken, Message.Planogram_MetaNumberOfPreserveProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfPreserveProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfPreserveProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfMinimumHurdleProductRulesBroken
        /// <summary>
        /// MetaNumberOfMinimumHurdleProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMinimumHurdleProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMinimumHurdleProductRulesBroken, Message.Planogram_MetaNumberOfMinimumHurdleProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMinimumHurdleProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMinimumHurdleProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfMaximumProductFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfMaximumProductFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMaximumProductFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMaximumProductFamilyRulesBroken, Message.Planogram_MetaNumberOfMaximumProductFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMaximumProductFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMaximumProductFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfMinimumProductFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfMinimumProductFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMinimumProductFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMinimumProductFamilyRulesBroken, Message.Planogram_MetaNumberOfMinimumProductFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMinimumProductFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMinimumProductFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty, value); }
        }
        #endregion
        
        #region MetaNumberOfDependencyFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfDependencyFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDependencyFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDependencyFamilyRulesBroken, Message.Planogram_MetaNumberOfDependencyFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDependencyFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDependencyFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty, value); }
        }
        #endregion

        #region MetaNumberOfDelistFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfDelistFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDelistFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDelistFamilyRulesBroken, Message.Planogram_MetaNumberOfDelistFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDelistFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDelistFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty); }
            set { this.SetProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty, value); }
        }
        #endregion

        #region MetaCountOfProductsBuddied
        /// <summary>
        /// MetaCountOfProductsBuddied property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaCountOfProductsBuddiedProperty =
            RegisterModelProperty<Int16?>(c => c.MetaCountOfProductsBuddied, Message.Planogram_MetaCountOfProductsBuddied,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaCountOfProductsBuddied value
        /// </summary>
        public Int16? MetaCountOfProductsBuddied
        {
            get { return this.GetProperty<Int16?>(MetaCountOfProductsBuddiedProperty); }
            set { this.SetProperty<Int16?>(MetaCountOfProductsBuddiedProperty, value); }
        }
        #endregion

        #endregion

        #region CustomAttributes

        /// <summary>
        /// CustomAttributes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomAttributeData> CustomAttributesProperty =
            RegisterModelProperty<CustomAttributeData>(c => c.CustomAttributes, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child custom attributes object for this plan.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get
            {
                return this.GetPropertyLazy<CustomAttributeData>(
                    CustomAttributesProperty,
                    new CustomAttributeData.FetchCriteria(this.DalFactoryName,
                        (Byte)CustomAttributeDataPlanogramParentType.Planogram, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child CustomAttributeData of this planogram
        /// </summary>
        public CustomAttributeData CustomAttributesAsync
        {
            get
            {
                return this.GetPropertyLazy<CustomAttributeData>(
                    CustomAttributesProperty,
                    new CustomAttributeData.FetchCriteria(this.DalFactoryName,
                        (Byte)CustomAttributeDataPlanogramParentType.Planogram, this.Id),
                    false);
            }
        }

        #endregion

        #region Performance

        /// <summary>
        /// Performance property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPerformance> PerformanceProperty =
            RegisterModelProperty<PlanogramPerformance>(c => c.Performance, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child Performance object for this plan.
        /// </summary>
        public PlanogramPerformance Performance
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPerformance>(
                    PerformanceProperty,
                    new PlanogramPerformance.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child PlanogramPerformance of this planogram
        /// </summary>
        public PlanogramPerformance PerformanceAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPerformance>(
                    PerformanceProperty,
                    new PlanogramPerformance.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region Assortment

        /// <summary>
        /// Assortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAssortment> AssortmentProperty =
            RegisterModelProperty<PlanogramAssortment>(c => c.Assortment, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child Assortment object for this plan.
        /// </summary>
        public PlanogramAssortment Assortment
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortment>(
                    AssortmentProperty,
                    new PlanogramAssortment.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child PlanogramAssortment of this planogram
        /// </summary>
        public PlanogramAssortment AssortmentAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramAssortment>(
                    AssortmentProperty,
                    new PlanogramAssortment.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region ValidationTemplate

        /// <summary>
        /// ValidationTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramValidationTemplate> ValidationTemplateProperty =
            RegisterModelProperty<PlanogramValidationTemplate>(c => c.ValidationTemplate, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child ValidationTemplate object for this plan.
        /// </summary>
        public PlanogramValidationTemplate ValidationTemplate
        {
            get
            {
                return this.GetPropertyLazy<PlanogramValidationTemplate>(
                    ValidationTemplateProperty,
                    new PlanogramValidationTemplate.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child PlanogramValidationTemplate of this planogram
        /// </summary>
        public PlanogramValidationTemplate ValidationTemplateAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramValidationTemplate>(
                    ValidationTemplateProperty,
                    new PlanogramValidationTemplate.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region ConsumerDecisionTree

        /// <summary>
        /// ConsumerDecisionTree property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramConsumerDecisionTree> ConsumerDecisionTreeProperty =
            RegisterModelProperty<PlanogramConsumerDecisionTree>(c => c.ConsumerDecisionTree, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child ConsumerDecisionTree object for this plan.
        /// </summary>
        public PlanogramConsumerDecisionTree ConsumerDecisionTree
        {
            get
            {
                return this.GetPropertyLazy<PlanogramConsumerDecisionTree>(
                    ConsumerDecisionTreeProperty,
                    new PlanogramConsumerDecisionTree.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child PlanogramConsumerDecisionTree of this planogram
        /// </summary>
        public PlanogramConsumerDecisionTree ConsumerDecisionTreeAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramConsumerDecisionTree>(
                    ConsumerDecisionTreeProperty,
                    new PlanogramConsumerDecisionTree.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region EventLogs
        /// <summary>
        /// Event Logs property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogList> EventLogsProperty =
            RegisterModelProperty<PlanogramEventLogList>(c => c.EventLogs, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The Event Logs used by this planogram
        /// </summary>
        public PlanogramEventLogList EventLogs
        {
            get
            {
                return this.GetPropertyLazy<PlanogramEventLogList>(
                    EventLogsProperty,
                    new PlanogramEventLogList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The Event Logs used by this planogram
        /// </summary>
        public PlanogramEventLogList EventLogsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramEventLogList>(
                    EventLogsProperty,
                    new PlanogramEventLogList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Blocking
        /// <summary>
        /// Blocking property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingList> BlockingProperty =
            RegisterModelProperty<PlanogramBlockingList>(c => c.Blocking, RelationshipTypes.LazyLoad);

        /// <summary>
        /// The Blocking styles used by this planogram
        /// </summary>
        public PlanogramBlockingList Blocking
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingList>(
                    BlockingProperty,
                    new PlanogramBlockingList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The Blocking styles used by this planogram
        /// </summary>
        public PlanogramBlockingList BlockingAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramBlockingList>(
                    BlockingProperty,
                    new PlanogramBlockingList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region RenumberingStrategy

        /// <summary>
        ///		Metadata for the <see cref="RenumberingStrategy"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramRenumberingStrategy> RenumberingStrategyProperty =
            RegisterModelProperty<PlanogramRenumberingStrategy>(o => o.RenumberingStrategy, RelationshipTypes.LazyLoad);

        /// <summary>
        ///		Gets or sets the value for the <see cref="RenumberingStrategy"/> property.
        /// </summary>
        public PlanogramRenumberingStrategy RenumberingStrategy
        {
            get
            {
                return this.GetPropertyLazy<PlanogramRenumberingStrategy>(
                    RenumberingStrategyProperty,
                    new PlanogramRenumberingStrategy.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        ///		Gets or sets the value for the <see cref="RenumberingStrategy"/> property asynchronously.
        /// </summary>
        public PlanogramRenumberingStrategy RenumberingStrategyAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramRenumberingStrategy>
                    (RenumberingStrategyProperty,
                    new PlanogramRenumberingStrategy.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region Inventory

        /// <summary>
        /// Inventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramInventory> InventoryProperty =
            RegisterModelProperty<PlanogramInventory>(c => c.Inventory, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child Inventory object for this plan.
        /// </summary>
        public PlanogramInventory Inventory
        {
            get
            {
                return this.GetPropertyLazy<PlanogramInventory>(
                    InventoryProperty,
                    new PlanogramInventory.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// The child PlanogramInventory of this planogram
        /// </summary>
        public PlanogramInventory InventoryAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramInventory>(
                    InventoryProperty,
                    new PlanogramInventory.FetchByPlanogramIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #region SourcePath
        /// <summary>
        /// Source path property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourcePathProperty =
            RegisterModelProperty<String>(c => c.SourcePath);
        /// <summary>
        /// Gets or sets the SourcePath of the planogram
        /// </summary>
        public String SourcePath
        {
            get { return this.GetProperty<String>(SourcePathProperty); }
            set { this.SetProperty<String>(SourcePathProperty, value); }
        }
        #endregion

        #region Parent Type
        /// <summary>
        /// Returns the custom attribute data parent type
        /// </summary>
        Byte ICustomAttributeDataParent.ParentType
        {
            get { return (Byte)CustomAttributeDataPlanogramParentType.Planogram; }
        }
        #endregion

        #region LocationCode
        /// <summary>
        /// LocationCode Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode, Message.Planogram_LocationCode);

        /// <summary>
        /// Gets/Sets Planogram LocationCode
        /// </summary>
        public String LocationCode
        {
            get { return this.GetProperty<String>(LocationCodeProperty); }
            set { this.SetProperty<String>(LocationCodeProperty, value); }
        }

        #endregion

        #region LocationName

        /// <summary>
        /// LocationName Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationNameProperty =
            RegisterModelProperty<String>(c => c.LocationName, Message.Planogram_LocationName);

        /// <summary>
        /// Gets/sets Planogram LocationName
        /// </summary>
        public String LocationName
        {
            get { return this.GetProperty<String>(LocationNameProperty); }
            set { this.SetProperty<String>(LocationNameProperty, value); }
        }

        #endregion

        #region ClusterSchemeName
        /// <summary>
        /// ClusterSchemeName Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterSchemeNameProperty =
            RegisterModelProperty<String>(c => c.ClusterSchemeName, Message.Planogram_ClusterSchemeName);

        /// <summary>
        /// Gets/Sets Planogram ClusterScheme 
        /// </summary>
        public String ClusterSchemeName
        {
            get { return this.GetProperty<String>(ClusterSchemeNameProperty); }
            set { this.SetProperty<String>(ClusterSchemeNameProperty, value); }
        }
        #endregion

        #region ClusterName
        /// <summary>
        /// ClusterName Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterNameProperty =
            RegisterModelProperty<String>(c => c.ClusterName, Message.Planogram_ClusterName);

        /// <summary>
        /// Gets/Sets Planogram Cluster Name
        /// </summary>
        public String ClusterName
        {
            get { return this.GetProperty<String>(ClusterNameProperty); }
            set { this.SetProperty<String>(ClusterNameProperty, value); }
        }
        #endregion

        #region Sequence

        /// <summary>
        ///		Metadata for the <see cref="Sequence"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramSequence> SequenceProperty =
            RegisterModelProperty<PlanogramSequence>(o => o.Sequence, RelationshipTypes.LazyLoad);

        /// <summary>
        ///		Gets the sequence for this planogram.
        /// </summary>
        public PlanogramSequence Sequence
        {
            get
            {
                return GetPropertyLazy(SequenceProperty,
                    new PlanogramSequence.FetchByPlanogramIdCriteria(DalFactoryName, Id), true);
            }
        }

        /// <summary>
        ///		Gets asynchronously the sequence for this planogram.
        /// </summary>
        public PlanogramSequence SequenceAsync
        {
            get
            {
                return GetPropertyLazy(SequenceProperty,
                    new PlanogramSequence.FetchByPlanogramIdCriteria(DalFactoryName, Id), false);
            }
        }

        #endregion

        #region Status

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramStatusType> StatusProperty =
            RegisterModelProperty<PlanogramStatusType>(c => c.Status, Message.Planogram_Status);
        /// <summary>
        /// 
        /// </summary>
        public PlanogramStatusType Status
        {
            get { return this.GetProperty<PlanogramStatusType>(StatusProperty); }
            set { this.SetProperty<PlanogramStatusType>(StatusProperty, value); }
        }

        #endregion

        #region DateWIP

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateWipProperty =
            RegisterModelProperty<DateTime?>(c => c.DateWip, Message.Planogram_DateWip);
        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateWip
        {
            get { return this.GetProperty<DateTime?>(DateWipProperty); }
            set { this.SetProperty<DateTime?>(DateWipProperty, value); }
        }

        #endregion

        #region DateApproved

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateApprovedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateApproved, Message.Planogram_DateApproved);
        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateApproved
        {
            get { return this.GetProperty<DateTime?>(DateApprovedProperty); }
            set { this.SetProperty<DateTime?>(DateApprovedProperty, value); }
        }

        #endregion

        #region DateArchived

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateArchivedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateArchived, Message.Planogram_DateArchived);
        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateArchived
        {
            get { return this.GetProperty<DateTime?>(DateArchivedProperty); }
            set { this.SetProperty<DateTime?>(DateArchivedProperty, value); }
        }

        #endregion

        #region PlanogramType property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramType> PlanogramTypeProperty =
            RegisterModelProperty<PlanogramType>(c => c.PlanogramType, Message.Planogram_PlanogramType);
        /// <summary>
        /// 
        /// </summary>
        public PlanogramType PlanogramType
        {
            get { return this.GetProperty<PlanogramType>(PlanogramTypeProperty); }
            set { this.SetProperty<PlanogramType>(PlanogramTypeProperty, value); }
        }

        #endregion

        #region Highlight Sequence Strategy
        /// <summary>
        /// The name of the highlight that has most recently been applied to the plan.
        /// </summary>
        public static readonly ModelPropertyInfo<String> HighlightSequenceStrategyProperty =
            RegisterModelProperty<String>(c => c.HighlightSequenceStrategy, Message.Planogram_HighlightSequenceStrategy); 
        /// <summary>
        /// The name of the highlight that has most recently been applied to the plan.
        /// </summary>
        public String HighlightSequenceStrategy
        {
            get { return this.GetProperty<String>(HighlightSequenceStrategyProperty); }
            set { this.SetProperty<String>(HighlightSequenceStrategyProperty, value); }
        }
        #endregion

        #region Comparison

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="Comparison"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComparison> ComparisonProperty =
            RegisterModelProperty<PlanogramComparison>(c => c.Comparison, RelationshipTypes.LazyLoad);

        /// <summary>
        ///     Get the <see cref="PlanogramComparison"/> contained in this instance.
        /// </summary>
        public PlanogramComparison Comparison
        {
           
            get { return GetPropertyLazy(ComparisonProperty, new PlanogramComparison.FetchByPlanogramIdCriteria(DalFactoryName, Id), true); }
        }

        /// <summary>
        ///     [<c>Async</c>] Get the <see cref="PlanogramComparison"/> contained in this instance.
        /// </summary>
        public PlanogramComparison ComparisonAsync
        {
            get { return GetPropertyLazy(ComparisonProperty, new PlanogramComparison.FetchByPlanogramIdCriteria(DalFactoryName, Id), false); }
        }

        #endregion

        #region ProductAttributeComparisonResults
        public static readonly ModelPropertyInfo<ProductAttributeComparisonResultList> ProductAttributeComparisonResultsProperty =
          RegisterModelProperty<ProductAttributeComparisonResultList>(c => c.ProductAttributeComparisonResult, RelationshipTypes.LazyLoad);

        public ProductAttributeComparisonResultList ProductAttributeComparisonResult
        {
            get
            {
                return GetPropertyLazy(ProductAttributeComparisonResultsProperty,
                                       new ModelList<ProductAttributeComparisonResultList, ProductAttributeComparisonResult>.FetchByParentIdCriteria(
                                           DalFactoryName,
                                           Id),
                                       true);
            }
        }

        public ProductAttributeComparisonResultList ProductAttributeComparisonResultAsync
        {
            get
            {
                return GetPropertyLazy(ProductAttributeComparisonResultsProperty,
                    ProductAttributeComparisonResultList.FetchByPlanogramId((Int32) Id), true);
            }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 255));
            BusinessRules.AddRule(new MaxLength(LocationCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(LocationNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(ClusterSchemeNameProperty, 255));
            BusinessRules.AddRule(new MaxLength(ClusterNameProperty, 255));

            // V8-28515 - removed so that we can save plans without a repository connection
            //BusinessRules.AddRule(new Required(UserNameProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Planogram NewPlanogram()
        {
            Planogram item = new Planogram();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            this.LoadProperty<String>(NameProperty, Message.Planogram_Name_Default);
            this.LoadProperty<String>(UserNameProperty, ApplicationContext.User.Identity.Name);
            this.LoadProperty<PlanogramFixtureItemList>(FixtureItemsProperty, PlanogramFixtureItemList.NewPlanogramFixtureItemList());
            this.LoadProperty<PlanogramFixtureList>(FixturesProperty, PlanogramFixtureList.NewPlanogramFixtureList());
            this.LoadProperty<PlanogramAssemblyList>(AssembliesProperty, PlanogramAssemblyList.NewPlanogramAssemblyList());
            this.LoadProperty<PlanogramComponentList>(ComponentsProperty, PlanogramComponentList.NewPlanogramComponentList());
            this.LoadProperty<PlanogramProductList>(ProductsProperty, PlanogramProductList.NewPlanogramProductList());
            this.LoadProperty<PlanogramPositionList>(PositionsProperty, PlanogramPositionList.NewPlanogramPositionList());
            this.LoadProperty<PlanogramAnnotationList>(AnnotationsProperty, PlanogramAnnotationList.NewPlanogramAnnotationList());
            this.LoadProperty<PlanogramImageList>(ImagesProperty, PlanogramImageList.NewPlanogramImageList());
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.LoadProperty<PlanogramPerformance>(PerformanceProperty, PlanogramPerformance.NewPlanogramPerformance());
            this.LoadProperty<PlanogramAssortment>(AssortmentProperty, PlanogramAssortment.NewPlanogramAssortment());
            this.LoadProperty<PlanogramValidationTemplate>(ValidationTemplateProperty, PlanogramValidationTemplate.NewPlanogramValidationTemplate());
            this.LoadProperty<PlanogramConsumerDecisionTree>(ConsumerDecisionTreeProperty, PlanogramConsumerDecisionTree.NewPlanogramConsumerDecisionTree());
            this.LoadProperty<PlanogramEventLogList>(EventLogsProperty, PlanogramEventLogList.NewPlanogramEventLogList());
            this.LoadProperty<PlanogramBlockingList>(BlockingProperty, PlanogramBlockingList.NewPlanogramBlockingList());
            this.LoadProperty<PlanogramRenumberingStrategy>(RenumberingStrategyProperty, PlanogramRenumberingStrategy.NewPlanogramRenumberingStrategy());
            this.LoadProperty<PlanogramInventory>(InventoryProperty, PlanogramInventory.NewPlanogramInventory());
            this.LoadProperty<PlanogramSequence>(SequenceProperty, PlanogramSequence.NewPlanogramSequence());
            this.LoadProperty<PlanogramStatusType>(StatusProperty, PlanogramStatusType.WorkInProgress);
            this.LoadProperty<PlanogramType>(PlanogramTypeProperty, PlanogramType.Planogram);
            this.LoadProperty<PlanogramComparison>(ComparisonProperty, PlanogramComparison.NewPlanogramComparison());
            this.LoadProperty<PlanogramMetadataImageList>(PlanogramMetadataImagesProperty, PlanogramMetadataImageList.NewPlanogramMetaDataImageList());
            this.LoadProperty<ProductAttributeComparisonResultList>(ProductAttributeComparisonResultsProperty, ProductAttributeComparisonResultList.NewEntityComparisonAttributeResultList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Event Handlers
        /// <summary>
        /// Responds to child object changes
        /// Enusures that when an item is removed so to are linked objects
        /// </summary>
        protected override void OnChildChanged(ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);
            if (e.CollectionChangedArgs != null)
            {
                #region PlanogramProduct
                if (e.ChildObject is PlanogramProductList)
                {
                    this.MetaUniqueProductCount = Convert.ToInt16(this.Products.Count);

                    if (e.CollectionChangedArgs.OldItems != null &&
                        e.CollectionChangedArgs.Action == NotifyCollectionChangedAction.Remove)
                    {
                        foreach (PlanogramProduct p in e.CollectionChangedArgs.OldItems)
                        {
                            OnChildPlanogramProductRemoved(p);
                        }
                    }
                    if (e.CollectionChangedArgs.NewItems != null &&
                        e.CollectionChangedArgs.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (PlanogramProduct p in e.CollectionChangedArgs.NewItems)
                        {
                            OnChildPlanogramProductAdded(p);
                        }
                    }
                }
                #endregion

                #region PlanogramFixtureItem
                else if (e.ChildObject is PlanogramFixtureItemList)
                {
                    if (RenumberingStrategy.IsEnabled) RenumberingStrategy.RenumberBays();
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramFixture
                else if (e.ChildObject is PlanogramFixtureList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramFixtureAssembly
                else if (e.ChildObject is PlanogramFixtureAssemblyList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramAssembly
                else if (e.ChildObject is PlanogramAssemblyList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramAssemblyComponent
                else if (e.ChildObject is PlanogramAssemblyComponentList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramComponent
                else if (e.ChildObject is PlanogramComponentList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion

                #region PlanogramSubComponent
                else if (e.ChildObject is PlanogramSubComponentList)
                {
                    this.ClearSubComponentPlacementCache();
                }
                #endregion
            }
            else if (e.PropertyChangedArgs != null)
            {
                #region PlanogramFixtureItem
                if (e.ChildObject is PlanogramFixtureItemList)
                {
                    if (e.PropertyChangedArgs.PropertyName == PlanogramFixtureItem.XProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramFixtureItem.YProperty.Name
                        || e.PropertyChangedArgs.PropertyName == PlanogramFixtureItem.ZProperty.Name)
                    {
                        if (RenumberingStrategy.IsEnabled) RenumberingStrategy.RenumberBays();
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// Called when a planogram product is removed
        /// </summary>
        private void OnChildPlanogramProductRemoved(PlanogramProduct item)
        {
            //// remove all related positions
            //this.Positions.RemoveList(
            //    this.Positions.Where(
            //        p => Object.Equals(p.LinkedPlanogramProductId, item.Id)));                    
            var dataItem = Performance.PerformanceData
                .Where(data => data.PlanogramProductId.Equals(item.Id))
                .FirstOrDefault();
            if (dataItem != null)
            {
                Performance.PerformanceData.Remove(dataItem);
            }
        }

        /// <summary>
        /// Called when a planogram product is added
        /// </summary>
        private void OnChildPlanogramProductAdded(PlanogramProduct item)
        {
            if (!Performance.PerformanceData.Any(data => data.PlanogramProductId.Equals(item.Id)))
            {
                Performance.PerformanceData.Add(
                    PlanogramPerformanceData.NewPlanogramPerformanceData(item));
            }
        }

        //private void OnChildPlanogramSubComponentRemoved(PlanogramSubComponent item)
        //{
        //    //// remove all related positions
        //    //this.Positions.RemoveList(
        //    //    this.Positions.Where(
        //    //        p => Object.Equals(p.LinkedPlanogramSubComponentId, item.PlanogramSubComponentId)));
        //}

        //private void OnChildPlanogramComponentRemoved(PlanogramComponent item)
        //{
        //    //Object itemId = item.PlanogramComponentId;

        //    ////process the remove of child subcomponents
        //    //foreach (PlanogramSubComponent childSubComponent in item.SubComponents)
        //    //{
        //    //    OnChildPlanogramSubComponentRemoved(childSubComponent);
        //    //}

        //    ////remove related PlanogramAssemblyComponents
        //    //List<PlanogramAssemblyComponent> assemblyComponentsToRemove =
        //    //    this.Assemblies.SelectMany(a =>
        //    //        a.Components.Where(c =>
        //    //            Object.Equals(c.LinkedPlanogramComponentId, itemId))).ToList();

        //    //assemblyComponentsToRemove.ForEach
        //    //    (c => c.ParentPlanogramAssembly.Components.Remove(c));
        //}

        //private void OnChildPlanogramAssemblyComponentRemoved(PlanogramAssemblyComponent item)
        //{
        //    //Object itemId = item.PlanogramAssemblyComponentId;

        //    ////remove related positions
        //    //List<PlanogramPosition> positionsToRemove =
        //    //    this.Positions.Where(
        //    //    p => Object.Equals(p.LinkedPlanogramAssemblyComponentId, itemId))
        //    //    .ToList();

        //    //this.Positions.RemoveList(positionsToRemove);

        //}

        //private void OnChildPlanogramAssemblyRemoved(PlanogramAssembly item)
        //{
        //    //Object itemId = item.PlanogramAssemblyId;

        //    ////process the remove of child assembly components
        //    //foreach (PlanogramAssemblyComponent childAssemblyComponent in item.Components)
        //    //{
        //    //    OnChildPlanogramAssemblyComponentRemoved(childAssemblyComponent);
        //    //}

        //    ////remove related PlanogramFixtureAssemblies
        //    //List<PlanogramFixtureAssembly> fixtureAssembliesToRemove =
        //    //    this.Fixtures.SelectMany(a =>
        //    //        a.Assemblies.Where(c =>
        //    //            Object.Equals(c.LinkedPlanogramAssemblyId, itemId))).ToList();

        //    //fixtureAssembliesToRemove.ForEach
        //    //    (c => c.ParentPlanogramFixture.Assemblies.Remove(c));

        //}

        //private void OnChildPlanogramFixtureAssemblyRemoved(PlanogramFixtureAssembly item)
        //{
        //    //Object itemId = item.PlanogramFixtureAssemblyId;

        //    ////remove related positions
        //    //List<PlanogramPosition> positionsToRemove =
        //    //    this.Positions.Where(
        //    //    p => Object.Equals(p.LinkedPlanogramFixtureAssemblyId, itemId))
        //    //    .ToList();

        //    //this.Positions.RemoveList(positionsToRemove);
        //}

        //private void OnChildPlanogramFixtureRemoved(PlanogramFixture item)
        //{
        //    //Object itemId = item.PlanogramFixtureId;

        //    ////process the remove of child assembly components
        //    //foreach (PlanogramFixtureAssembly childFixtureAssembly in item.Assemblies)
        //    //{
        //    //    OnChildPlanogramFixtureAssemblyRemoved(childFixtureAssembly);
        //    //}

        //    ////remove related PlanogramFixtureItems
        //    //List<PlanogramFixtureItem> fixtureItemsToRemove =
        //    //    this.FixtureItems.Where(c =>
        //    //            Object.Equals(c.LinkedPlanogramFixtureId, itemId)).ToList();

        //    //fixtureItemsToRemove.ForEach(c => this.FixtureItems.Remove(c));
        //}

        //private void OnChildPlanogramFixtureItemRemoved(PlanogramFixtureItem item)
        //{
        //    //Object itemId = item.PlanogramFixtureItemId;

        //    ////remove related positions
        //    //List<PlanogramPosition> positionsToRemove =
        //    //    this.Positions.Where(
        //    //    p => Object.Equals(p.LinkedPlanogramFixtureAssemblyId, itemId))
        //    //    .ToList();

        //    //this.Positions.RemoveList(positionsToRemove);
        //}
        #endregion

        #region Methods

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<Planogram>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called whenever the planogram model is marked 
        /// as a new item.
        /// </summary>
        protected override void MarkNew()
        {
            //set a new ucr - done here to make sure that it gets called for package save as.
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            base.MarkNew();
        }

        #endregion

        #region Caching
        /// <summary>
        /// Clear the planogram cache
        /// </summary>
        protected override void OnClearCache()
        {
            this.ClearSubComponentCache();
            this.ClearFixtureComponentCache();
            this.ClearFixtureAssemblyCache();
            this.ClearAssemblyComponentCache();
            this.ClearSubComponentPlacementCache();
        }

        /// <summary>
        /// Clears the sub component cache
        /// </summary>
        private void ClearSubComponentCache()
        {
            if (_subComponentPlacementCache != null)
            {
                lock (_subComponentPlacementCacheLock)
                {
                    if (_subComponentCache != null)
                    {
                        _subComponentCache.Clear();
                        _subComponentCache = null;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the fixture component cache
        /// </summary>
        private void ClearFixtureComponentCache()
        {
            if (_fixtureComponentCache != null)
            {
                lock (_fixtureComponentCacheLock)
                {
                    if (_fixtureComponentCache != null)
                    {
                        _fixtureComponentCache.Clear();
                        _fixtureComponentCache = null;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the fixture assembly cache
        /// </summary>
        private void ClearFixtureAssemblyCache()
        {
            if (_fixtureAssemblyCache != null)
            {
                lock (_fixtureAssemblyCacheLock)
                {
                    if (_fixtureAssemblyCache != null)
                    {
                        _fixtureAssemblyCache.Clear();
                        _fixtureAssemblyCache = null;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the assembly component cache
        /// </summary>
        private void ClearAssemblyComponentCache()
        {
            if (_assemblyComponentCache != null)
            {
                lock (_assemblyComponentCacheLock)
                {
                    if (_assemblyComponentCache != null)
                    {
                        _assemblyComponentCache.Clear();
                        _assemblyComponentCache = null;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the sub component placement cache
        /// </summary>
        private void ClearSubComponentPlacementCache()
        {
            if (_subComponentPlacementCache != null)
            {
                lock (_subComponentPlacementCacheLock)
                {
                    if (_subComponentPlacementCache != null)
                    {
                        _subComponentPlacementCache.Clear();
                        _subComponentPlacementCache = null;
                    }
                }
            }
        }

        #endregion

        #region Planogram Stuff
        /// <summary>
        /// Updates the FillColour and ColourGroupValue properties of the products in this plan using the given <paramref name="highlight"/>.
        /// </summary>
        /// <param name="highlight">The highlight to apply to the plan.</param>
        /// <param name="products">The products to update in the plan. If null, all products will be updated.</param>
        /// <returns>The products that were updated.</returns>
        public IEnumerable<PlanogramProduct> UpdateProductColoursFromHighlight(
            IPlanogramHighlight highlight, IEnumerable<PlanogramProduct> products = null)
        {
            // Check that we have a highlight.
            List<PlanogramProduct> productsAffected = new List<PlanogramProduct>();
            if (highlight == null) return productsAffected;

            // If no products were supplied, use the products list of the plan.
            if (products == null) products = this.Products;

            // Get the result of applying the highlight to the plan.
            PlanogramHighlightResult highlightResult = PlanogramHighlightHelper.ProcessPositionHighlight(this, highlight);

            foreach (PlanogramHighlightResultGroup highlightResultGroup in highlightResult.Groups)
            {
                foreach (Object item in highlightResultGroup.Items)
                {
                    // The items come out weakly typed as objects. To use them, we assume they are positions.
                    PlanogramPosition position = item as PlanogramPosition;
                    if (position == null) continue;

                    // Check if the product has been filtered for processing.
                    PlanogramProduct product = position.GetPlanogramProduct();
                    if (!products.Contains(product)) continue;

                    product.FillColour = highlightResultGroup.Colour;
                    product.ColourGroupValue = highlightResultGroup.Label;
                    productsAffected.Add(product);
                }
            }

            // Save the name of the highlight applied against the HighlightSequenceStrategy property.
            this.HighlightSequenceStrategy = highlight.Name;

            return productsAffected;
        }

        /// <summary>
        ///     Normalize fixture coordinates so they are all positive.
        /// </summary>
        public void NormalizeBays()
        {
            //  Check whether there are negative X coordinates.
            Single minX = FixtureItems.Min(item => item.X);
            if (!minX.EqualTo(0))
            {
                //  Offset all X coordinates by the minX to normalize them.
                foreach (PlanogramFixtureItem item in FixtureItems)
                {
                    item.X -= minX;
                }
            }

            //  Check whether there are negative X coordinates.
            Single minY = FixtureItems.Min(item => item.Y);
            if (!minY.EqualTo(0))
            {
                //  Offset all X coordinates by the minX to normalize them.
                foreach (PlanogramFixtureItem item in FixtureItems)
                {
                    item.Y -= minY;
                }
            }

            //  Check whether there are negative X coordinates.
            Single minZ = FixtureItems.Min(item => item.Z);
            if (!minZ.EqualTo(0))
            {
                //  Offset all X coordinates by the minX to normalize them.
                foreach (PlanogramFixtureItem item in FixtureItems)
                {
                    item.Z -= minZ;
                }
            }
        }

        /// <summary>
        ///     Recalculate Fixture X Positions including any fixture spacing if set
        /// </summary>
        public void RecalculateFixtureXPositions(Dictionary<PlanogramFixtureItem, float> fixtureSpacing, bool includeFixtureSpacing)
        {
            float xPosition = 0;
            List<PlanogramFixtureItem> orderedFixtureItems = FixtureItems.OrderBy(o => o.X).ToList();

            for (int fixture = 0; fixture < orderedFixtureItems.Count; fixture++)
            {
                orderedFixtureItems[fixture].X = xPosition;
                float baySpace = 0;

                if (includeFixtureSpacing)
                {
                    baySpace = fixtureSpacing.Values.ElementAt(fixture);
                }

                xPosition = xPosition + orderedFixtureItems[fixture].GetPlanogramFixture().Width + baySpace;
            }            
        }

        /// <summary>
        ///     Can the Fixture X Positions Be Recalculated
        ///     Fixtures can only be recalculated if the Y, Z and Angle values are the same.
        ///     If they are the same that means the fixture run in order on the axis.
        /// </summary>
        public bool CanReCalculateFixtureXPositions()
        {
            int fixtureCount = 0;
            int fixtureValidCount = 0;
            bool fixtureCalculatePositions = false;
            PlanogramFixtureItem fixture = null;

            if (FixtureItems != null && FixtureItems.Count > 0)
            {
                fixtureCount = FixtureItems.Count();

                fixture = FixtureItems.FirstOrDefault();

                if (fixture != null)
                {
                    fixtureValidCount = FixtureItems.Where(w => w.Y == fixture.Y && w.Z == fixture.Z && w.Angle == fixture.Angle).Count();

                    if (fixtureCount == fixtureValidCount)
                    {
                        fixtureCalculatePositions = true;
                    }
                }
            }

            return fixtureCalculatePositions;            
        }

        /// <summary>
        /// Returns the specified sub component based on its id
        /// </summary>
        public PlanogramSubComponent GetPlanogramSubComponent(Object subComponentId)
        {
            PlanogramSubComponent subComponent;
            lock (_subComponentPlacementCacheLock)
            {
                // determine if the item already exists within the cache
                if ((_subComponentCache != null) &&
                    (_subComponentCache.TryGetValue(subComponentId, out subComponent)))
                {
                    return subComponent;
                }

                // item does not exist within the cache
                // so rebuild the cache from the model
                if (_subComponentCache == null) _subComponentCache = new Dictionary<Object, PlanogramSubComponent>();
                _subComponentCache.Clear();
                foreach (PlanogramComponent c in this.Components)
                {
                    foreach (PlanogramSubComponent sc in c.SubComponents)
                    {
                        _subComponentCache.Add(sc.Id, sc);
                    }
                }

                // determine if the item now exists within the cache
                if (_subComponentCache.TryGetValue(subComponentId, out subComponent))
                {
                    return subComponent;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a fixture item based on its id
        /// </summary>
        public PlanogramFixtureItem GetPlanogramFixtureItem(Object fixtureItemId)
        {
            return this.FixtureItems.FindById(fixtureItemId);
        }

        /// <summary>
        /// Returns a fixture based on its id
        /// </summary>
        public PlanogramFixture GetPlanogramFixture(Object fixtureId)
        {
            return this.Fixtures.FindById(fixtureId);
        }

        /// <summary>
        /// Returns a fixture component based on its id
        /// </summary>
        public PlanogramFixtureComponent GetPlanogramFixtureComponent(Object fixtureComponentId)
        {
            PlanogramFixtureComponent fixtureComponent;
            lock (_fixtureComponentCacheLock)
            {
                // determine if the item already exists within the cache
                if ((_fixtureComponentCache != null) &&
                    (_fixtureComponentCache.TryGetValue(fixtureComponentId, out fixtureComponent)))
                {
                    return fixtureComponent;
                }

                // item does not exist within the cache
                // so rebuild the cache from the model
                if (_fixtureComponentCache == null)
                    _fixtureComponentCache = new Dictionary<Object, PlanogramFixtureComponent>();
                _fixtureComponentCache.Clear();
                foreach (PlanogramFixture f in this.Fixtures)
                {
                    foreach (PlanogramFixtureComponent fc in f.Components)
                    {
                        _fixtureComponentCache[fc.Id] = fc;
                    }
                }

                // determine if the item now exists within the cache
                if (_fixtureComponentCache.TryGetValue(fixtureComponentId, out fixtureComponent))
                {
                    return fixtureComponent;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a fixture assembly based on its id
        /// </summary>
        public PlanogramFixtureAssembly GetPlanogramFixtureAssembly(Object fixtureAssemblyId)
        {
            PlanogramFixtureAssembly fixtureAssembly;
            lock (_fixtureAssemblyCacheLock)
            {
                // determine if the item already exists within the cache
                if ((_fixtureAssemblyCache != null) &&
                    (_fixtureAssemblyCache.TryGetValue(fixtureAssemblyId, out fixtureAssembly)))
                {
                    return fixtureAssembly;
                }

                // item does not exist within the cache
                // so rebuild the cache from the model
                if (_fixtureAssemblyCache == null)
                    _fixtureAssemblyCache = new Dictionary<Object, PlanogramFixtureAssembly>();
                _fixtureAssemblyCache.Clear();
                foreach (PlanogramFixture f in this.Fixtures)
                {
                    foreach (PlanogramFixtureAssembly fa in f.Assemblies)
                    {
                        _fixtureAssemblyCache[fa.Id] = fa;
                    }
                }

                // determine if the item now exists within the cache
                if (_fixtureAssemblyCache.TryGetValue(fixtureAssemblyId, out fixtureAssembly))
                {
                    return fixtureAssembly;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns an assembly based on its id
        /// </summary>
        public PlanogramAssembly GetPlanogramAssembly(Object assemblyId)
        {
            return this.Assemblies.FindById(assemblyId);
        }

        /// <summary>
        /// Returns an assembly component based on its id
        /// </summary>
        public PlanogramAssemblyComponent GetPlanogramAssemblyComponent(Object assemblyComponentId)
        {
            PlanogramAssemblyComponent assemblyComponent;
            lock (_assemblyComponentCacheLock)
            {
                // determine if the item already exists within the cache
                if ((_assemblyComponentCache != null) &&
                    (_assemblyComponentCache.TryGetValue(assemblyComponentId, out assemblyComponent)))
                {
                    return assemblyComponent;
                }

                // item does not exist within the cache
                // so rebuild the cache from the model
                if (_assemblyComponentCache == null)
                    _assemblyComponentCache = new Dictionary<Object, PlanogramAssemblyComponent>();
                _assemblyComponentCache.Clear();
                foreach (PlanogramAssembly a in this.Assemblies)
                {
                    foreach (PlanogramAssemblyComponent ac in a.Components)
                    {
                        _assemblyComponentCache[ac.Id] = ac;
                    }
                }

                // determine if the item now exists within the cache
                if (_assemblyComponentCache.TryGetValue(assemblyComponentId, out assemblyComponent))
                {
                    return assemblyComponent;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns an enumerable list of all
        /// planogram sub component placements
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            if (_subComponentPlacementCache != null) return _subComponentPlacementCache.ToList();

            lock (_subComponentPlacementCacheLock)
            {
                if (_subComponentPlacementCache != null) return _subComponentPlacementCache.ToList();

                // create the sub component placement cache
                _subComponentPlacementCache = new List<PlanogramSubComponentPlacement>();

                // enumerate through all fixture items
                foreach (PlanogramFixtureItem fixtureItem in this.FixtureItems)
                {
                    // get the fixture details
                    PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
                    if (fixture == null) continue;

                    // enumerate all fixture components
                    foreach (PlanogramFixtureComponent fixtureComponent in fixture.Components)
                    {
                        PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
                        if (component == null) continue;
                                
                        foreach (PlanogramSubComponent subComponent in component.SubComponents)
                        {
                            _subComponentPlacementCache.Add(PlanogramSubComponentPlacement
                                .NewPlanogramSubComponentPlacement(
                                    subComponent, this, fixtureItem, fixture, fixtureComponent, component));
                        }
                    }

                    // enumerate assembly components
                    foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies)
                    {
                        PlanogramAssembly assembly = fixtureAssembly.GetPlanogramAssembly();
                        if (assembly == null) continue;

                        foreach (PlanogramAssemblyComponent assemblyComponent in assembly.Components)
                        {
                            PlanogramComponent component = assemblyComponent.GetPlanogramComponent();
                            if (component == null) continue;

                            foreach (PlanogramSubComponent subComponent in component.SubComponents)
                            {
                                _subComponentPlacementCache.Add(PlanogramSubComponentPlacement
                                    .NewPlanogramSubComponentPlacement(
                                        subComponent, this, fixtureItem, fixture, fixtureAssembly, assembly,
                                        assemblyComponent, component));
                            }
                        }
                    }
                }
            }

            return _subComponentPlacementCache.ToList();
        }

        /// <summary>
        /// Updates the Height, Width and Depth values of this planogram
        /// based on its child fixture items.
        /// </summary>
        public void UpdateSizeFromFixtures()
        {
            //only update the value if it has child fixtures.
            if (this.FixtureItems.Count == 0) return;

            RectValue bounds = new RectValue();

            //union the bounds of the child subcomponents.
            foreach (PlanogramFixtureItem item in this.FixtureItems)
            {
                PlanogramFixture fixture = item.GetPlanogramFixture();
                if (fixture == null) continue;

                RectValue childBounds = new RectValue(0, 0, 0,
                    fixture.Width, fixture.Height, fixture.Depth);

                MatrixValue childTransform =
                    MatrixValue.CreateTransformationMatrix(
                        item.X, item.Y, item.Z, item.Angle, item.Slope, item.Roll);

                childBounds = childTransform.TransformBounds(childBounds);

                bounds = bounds.Union(childBounds);
            }

            if (this.Width != bounds.Width) this.Width = bounds.Width;
            if (this.Height != bounds.Height) this.Height = bounds.Height;
            if (this.Depth != bounds.Depth) this.Depth = bounds.Depth;
        }

        /// <summary>
        /// Returns the height and width to use when blocking this plan
        /// </summary>
        /// <param name="blockingHeight">the height of the blocking area</param>
        /// <param name="blockingWidth">the width of the blocking area.</param>
        public void GetBlockingAreaSize(out Single blockingHeight, out Single blockingWidth)
        {
            Single topDownHeightOffset;
            GetBlockingAreaSize(out blockingHeight, out blockingWidth, out topDownHeightOffset);
        }

        /// <summary>
        /// Returns the height and width to use when blocking this plan
        /// </summary>
        /// <param name="blockingHeight">the height of the blocking area</param>
        /// <param name="blockingWidth">the width of the blocking area.</param>
        /// <param name="topDownHeightOffset">
        /// The amount by which the height of the plan is offset due to any top-down components appearing below the bottom of the fixtures.
        /// </param>
        public void GetBlockingAreaSize(out Single blockingHeight, out Single blockingWidth,
            out Single topDownHeightOffset)
        {
            topDownHeightOffset = 0;
            Single height = 0;
            Single width = 0;

            // When blocking, the planogram will be viewed in design mode -
            // all bays will be viewed front on and next to each other.

            //Only the size of fixtures will be considered for this.
            // There may be cases where positions overhang the fixture but this will be ignored.

            List<PlanogramFixture> fixtures = this.FixtureItems.Select(f => f.GetPlanogramFixture()).ToList();

            width = fixtures.Sum(f => f.Width);
            height = (fixtures.Count != 0) ? fixtures.Max(f => f.Height) : 0;

            //now check if we have any components that will be merchandised top down.
            // If so they will be rotated when blocking so we need to consider if they drop
            // below the floor.
            if (this.Components.Any(c => c.IsMerchandisedTopDown))
            {
                Single minY = 0;
                foreach (PlanogramFixtureItem fixtureItem in this.FixtureItems)
                {
                    PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
                    foreach (PlanogramFixtureComponent fc in fixture.Components)
                    {
                        PlanogramComponent component = fc.GetPlanogramComponent();
                        if (component.IsMerchandisedTopDown)
                        {
                            RectValue compBounds = new RectValue(0, 0, 0, component.Width, component.Height,
                                component.Depth);
                            MatrixValue relativeTransform = MatrixValue.Identity;
                            relativeTransform.Append(MatrixValue.CreateTransformationMatrix(0, fixtureItem.Y, 0, 0, 0, 0));
                            relativeTransform.Append(MatrixValue.CreateTransformationMatrix(
                                fc.X, fc.Y + component.Height, fc.Z, fc.Angle, Convert.ToSingle(fc.Slope - (Math.PI/2)),
                                fc.Roll));
                            compBounds = relativeTransform.TransformBounds(compBounds);

                            minY = Math.Min(compBounds.Y, minY);
                        }
                    }
                    foreach (PlanogramFixtureAssembly fa in fixture.Assemblies)
                    {
                        PlanogramAssembly assembly = fa.GetPlanogramAssembly();
                        foreach (PlanogramAssemblyComponent ac in assembly.Components)
                        {
                            PlanogramComponent component = ac.GetPlanogramComponent();
                            if (component.IsMerchandisedTopDown)
                            {
                                RectValue compBounds = new RectValue(0, 0, 0, component.Width, component.Height,
                                    component.Depth);
                                MatrixValue relativeTransform = MatrixValue.Identity;
                                relativeTransform.Append(MatrixValue.CreateTransformationMatrix(0, fixtureItem.Y, 0, 0,
                                    0, 0));
                                relativeTransform.Append(MatrixValue.CreateTransformationMatrix(fa.X, fa.Y, fa.Z,
                                    fa.Angle, fa.Slope, fa.Roll));
                                relativeTransform.Append(MatrixValue.CreateTransformationMatrix(
                                    ac.X, ac.Y + component.Height, ac.Z, ac.Angle,
                                    Convert.ToSingle(ac.Slope - (Math.PI/2)), ac.Roll));
                                compBounds = relativeTransform.TransformBounds(compBounds);

                                minY = Math.Min(compBounds.Y, minY);
                            }
                        }
                    }
                }

                if (minY < 0)
                {
                    height -= minY;
                    topDownHeightOffset = Math.Abs(minY);
                }
            }


            //set the output values.
            blockingHeight = height;
            blockingWidth = width;
        }

        /// <summary>
        /// Gets all the Merchandising Groups for the Sub Components in this Planogram. Note: this method
        /// returns a disposable list object which should be disposed of after use.
        /// </summary>
        /// <returns>A disposable PlanogramMerchandisingGroupList.</returns>
        public PlanogramMerchandisingGroupList GetMerchandisingGroups()
        {
            var availableSubComponentPlacements = GetPlanogramSubComponentPlacements()
                .Where(s => s.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None).ToList();

            return GetMerchandisingGroups(availableSubComponentPlacements);
        }

        /// <summary>
        /// Recursively gets Merchandising Groups from the available placements given.
        /// </summary>
        /// <param name="availablePlacements">The available SubComponentPlacements that can be grouped.</param>
        /// <returns></returns>
        public PlanogramMerchandisingGroupList GetMerchandisingGroups(List<PlanogramSubComponentPlacement> availablePlacements)
        {
            var returnList = new PlanogramMerchandisingGroupList();

            // Break if there are no available groups left.
            Int32 availableGroupsCount = availablePlacements.Count();
            if (availableGroupsCount == 0) return returnList;

            // Take and remove the first item from the list.
            var firstPlacement = availablePlacements[0];
            availablePlacements.Remove(firstPlacement);

            // Get the placements that can combine with the first placement and remove these
            // from the available list as well.
            var combinedPlacements = firstPlacement.GetCombinedWithList(availablePlacements).ToList();
            foreach (var placementInGroup in combinedPlacements)
            {
                availablePlacements.Remove(placementInGroup);
            }

            // Create a new merchandising group from the combined placements.
            returnList.Add(PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(combinedPlacements));

            // Recursively add the remaining available placements.
            returnList.AddRange(GetMerchandisingGroups(availablePlacements));

            return returnList;
        }

        /// <summary>
        ///     Gets the <see cref="PlanogramMerchandisingGroup"/> instances that are withing the active <see cref="Blocking"/> space.
        /// </summary>
        /// <returns>An dispoable list of <see cref="PlanogramMerchandisingGroup"/> instances that are either fully or partially in the area covered by this instances active <see cref="Blocking"/> space.</returns>
        /// <remarks>The <see cref="PlanogramMerchandisingGroupList"/> returned by this method is disposable and should be disposed of after use.</remarks>
        public PlanogramMerchandisingGroupList GetMerchandisingGroupsInBlockingArea()
        {
            Single blockingWidth;
            Single blockingHeight;
            Single topDownHeighOffset;
            GetBlockingAreaSize(out blockingHeight, out blockingWidth, out topDownHeighOffset);
            var blockingAreaRectValue = new RectValue(0, 0, 0, blockingWidth, blockingHeight, 0);

            //  Filter all placements in the blocking space and their combined components (in case they are outside the blocking space).
            List<PlanogramSubComponentPlacement> placementsWithingBlockingSpace =
                GetPlanogramSubComponentPlacements()
                    .Where(scp => scp.SubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None && OverlapInAxes(new DesignViewPosition(scp.FixtureComponent,scp.FixtureItem,topDownHeighOffset).ToRectValue2D(), blockingAreaRectValue, AxisType.Z.GetNormals()))
                    .ToList();
            List<PlanogramSubComponentPlacement> placementsWithinBlockingSpaceAndCombined =
                placementsWithingBlockingSpace
                    .Union(placementsWithingBlockingSpace.SelectMany(placement => placement.GetCombinedWithList()))
                    .ToList();

            return GetMerchandisingGroups(placementsWithinBlockingSpaceAndCombined);
        }

        /// <summary>
        /// Updates the SequenceNumber and SequenceColour properties against the plan's Positions
        /// such that they match the values they should have in the plan's Sequence.
        /// </summary>
        public void UpdatePositionSequenceData(PlanogramMerchandisingGroupList merchandisingGroups = null)
        {
            if (Sequence == null || Products == null || Products.Count == 0 || Positions == null || Positions.Count == 0) return;

            ILookup<String, PlanogramSequenceGroup> sequenceGroupsByProduct = Sequence.Groups
                .SelectMany(g => g.Products)
                .ToLookup(p => p.Gtin, p => p.Parent);

            // Get the best blocking - final if its there, if not performance or initial.
            PlanogramBlocking blocking =
                Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ??
                Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial);

            // Define a variable to hold a dictionary of blocking location spaces. We'll only
            // populate this if we need to.
            Dictionary<PlanogramBlockingLocation, RectValue> locationSpacesByLocation = null;
            Single blockingWidth = 0f;
            Single blockingHeight = 0f;
            Single blockingOffset = 0f;

            // Define a variable to hold a lookup of blocking locations by group. Again, we'll only
            // populate this if we need to.
            ILookup<PlanogramBlockingGroup, PlanogramBlockingLocation> locationsByGroup = null;

            Boolean disposeOfMerchandisingGroups = merchandisingGroups == null;
            try
            {
                if (merchandisingGroups == null) merchandisingGroups = GetMerchandisingGroups();

                ILookup<String, PlanogramPositionPlacement> positionPlacementsByGtin = merchandisingGroups
                    .SelectMany(m => m.PositionPlacements)
                    .ToLookup(p => p.Product.Gtin, p => p);

                foreach (PlanogramProduct product in Products)
                {
                    // Get sequence group for product, if its sequenced at all.
                    if (!sequenceGroupsByProduct.Contains(product.Gtin)) continue;
                    IEnumerable<PlanogramSequenceGroup> sequenceGroups = sequenceGroupsByProduct[product.Gtin].ToList();
                    if (!sequenceGroups.Any()) continue;

                    // Get position placements for product, if its sequenced at all.
                    if (!positionPlacementsByGtin.Contains(product.Gtin)) continue;
                    IEnumerable<PlanogramPositionPlacement> positionPlacements = positionPlacementsByGtin[product.Gtin].ToList();
                    if (!positionPlacements.Any()) continue;

                    foreach (PlanogramPositionPlacement positionPlacement in positionPlacements)
                    {
                        // If there is only one sequence group with this product in, then we know
                        // that the position (all of them) have the sequence number and colour from
                        // this sequence group.
                        if (sequenceGroups.Count() == 1)
                        {
                            PlanogramSequenceGroupProduct sequenceProduct =
                                sequenceGroups.First().Products.FirstOrDefault(p => p.Gtin.Equals(product.Gtin));
                            if (sequenceProduct != null) positionPlacement.Position.SequenceNumber = sequenceProduct.SequenceNumber;
                            positionPlacement.Position.SequenceColour = sequenceGroups.First().Colour;
                        }
                        else
                        {
                            // If there are more than one sequence group, then this product is multi-sited
                            // in the sequence. If we've got blocking, we can try to pin down the right
                            // sequence group for this position. If not, then we won't be able to identify
                            // which sequence group the position belongs to.
                            if (blocking != null)
                            {
                                if (locationSpacesByLocation == null)
                                {
                                    GetBlockingAreaSize(out blockingHeight, out blockingWidth, out blockingOffset);
                                    locationSpacesByLocation = blocking.GetLocationSpacesByLocation(blockingWidth, blockingHeight);
                                }

                                if (locationsByGroup == null)
                                {
                                    locationsByGroup = blocking.Locations.ToLookup(l => l.GetPlanogramBlockingGroup());
                                }

                                // Try to get a blocking group that intersects with the position.
                                PlanogramBlockingGroup intersectingGroup = Sequence.GetMaxIntersectingBlockingGroupForPosition(
                                    positionPlacement,
                                    locationsByGroup,
                                    locationSpacesByLocation,
                                    blockingWidth,
                                    blockingHeight,
                                    blockingOffset);
                                if (intersectingGroup == null) continue;

                                // Try to get the sequence group that matches the blocking group.
                                PlanogramSequenceGroup sequenceGroup = sequenceGroups.FirstOrDefault(g => g.IsColourMatch(intersectingGroup));
                                if (sequenceGroup == null) continue;

                                // If we managed to get a group, try setting the sequence number and colour properties.
                                PlanogramSequenceGroupProduct sequenceProduct = sequenceGroup.Products
                                    .FirstOrDefault(p => p.Gtin.Equals(product.Gtin));
                                if (sequenceProduct != null) positionPlacement.Position.SequenceNumber = sequenceProduct.SequenceNumber;
                                positionPlacement.Position.SequenceColour = sequenceGroup.Colour;
                            }
                            else
                            {
                                positionPlacement.Position.SequenceNumber = null;
                                positionPlacement.Position.SequenceColour = null;
                            }
                        }
                    }
                }
            }
            finally
            {
                // Ensure any changes we've made to the positions are commited and dispose of the
                // merch groups if we instantiated them.
                merchandisingGroups.ApplyEdit();
                if (disposeOfMerchandisingGroups) merchandisingGroups.Dispose();
            }
        }

        /// <summary>
        ///     Update all the products in this Planogram with the data contained in <paramref name="updateValues"/>.
        /// </summary>
        /// <param name="updateValues">The list of new attribute values to be used when updating products.</param>
        /// <param name="gtins">What Gtins to update. If null or empty all will be updated.</param>
        public Int32 UpdateProductAttributes(List<ProductAttributeUpdateValue> updateValues, ICollection<String> gtins = null)
        {
            var updateCount = 0;
            IPlanogramProduct updateProduct = CreateUpdateProduct(updateValues);
            String[] propertiesToUpdate = updateValues.Select(data => data.PropertyName).ToArray();
            Boolean allGtins = gtins == null || gtins.Count == 0;
            IEnumerable<PlanogramProduct> planogramProducts = allGtins
                                                                  ? Products
                                                                  : Products.Where(product => gtins.Contains(product.Gtin));
            foreach (PlanogramProduct product in planogramProducts)
            {
                product.UpdateFrom(updateProduct, propertiesToUpdate);
                updateCount++;
            }

            return updateCount;
        }

        /// <summary>
        ///     Create a new instance implementing <see cref="IPlanogramProduct"/> containing the values that will be used to update other products.
        /// </summary>
        /// <param name="updateData">The enumeration of <see cref="ProductAttributeUpdateValue"/> used to populate the new instance.</param>
        /// <returns></returns>
        private static PlanogramProduct CreateUpdateProduct(IEnumerable<ProductAttributeUpdateValue> updateData)
        {
            PlanogramProduct updateProduct = PlanogramProduct.NewPlanogramProduct();
            Type productType = updateProduct.GetType();
            PropertyInfo[] productPropertyInfos = productType.GetProperties();
            Type customAttributesType = updateProduct.CustomAttributes.GetType();
            PropertyInfo[] customAttributesPropertyInfos = customAttributesType.GetProperties();
            foreach (ProductAttributeUpdateValue data in updateData)
            {
                String propertyName = data.BasePropertyName;
                PropertyInfo[] infos = data.IsCustomAttribute
                                           ? customAttributesPropertyInfos
                                           : productPropertyInfos;
                PropertyInfo propertyInfo = infos.FirstOrDefault(info => info.Name == propertyName);
                if (propertyInfo == null) continue;
                Object targetObject = data.IsCustomAttribute ? (Object)updateProduct.CustomAttributes : updateProduct;
                // V8-32817 : Make sure that the new value is of the appropriate type, as sometimes the UI might return strings for numbers.
                // V8-32967 : Make sure enum types are converted on their own as they cannot be converted straight away from Int32.
                // V8-32886 : Make sure nullable types and null values are managed correctly.
                Object safeValue = data.NewValue == null
                                       ? null
                                       : data.PropertyType.IsEnum
                                       ? Enum.Parse(data.PropertyType,data.NewValue.ToString())
                                       : Convert.ChangeType(data.NewValue, Nullable.GetUnderlyingType(data.PropertyType) ?? data.PropertyType);
                propertyInfo.SetValue(targetObject, safeValue, null);
            }
            return updateProduct;
        }

        /// <summary>
        /// Cleans up any orphaned parts from the planogram.
        /// </summary>
        public void CleanUpOrphanedParts()
        {
            //CleanUp Fixtures
            List<Object> referencedIds =
                this.FixtureItems.Select(f => f.PlanogramFixtureId).ToList();

            foreach (PlanogramFixture fixture in this.Fixtures.ToList())
            {
                //if the fixture has no references, remove it.
                if (!referencedIds.Contains(fixture.Id))
                {
                    this.Fixtures.Remove(fixture);
                    break;
                }

                //otherwise clean up fixture components and assemblies which link to
                // items that do not exist.
                fixture.Components.RemoveList(
                    fixture.Components.Where(fc => !this.Components.Select(c => c.Id).Contains(fc.PlanogramComponentId)).ToArray());

                fixture.Assemblies.RemoveList(
                    fixture.Assemblies.Where(fa => !this.Assemblies.Select(a => a.Id).Contains(fa.PlanogramAssemblyId)).ToArray());
            }

            //CleanUp Assemblies
            referencedIds =
                this.Fixtures.SelectMany(f => f.Assemblies.Select(c => c.PlanogramAssemblyId))
                .Distinct().ToList();

            foreach (PlanogramAssembly assembly in this.Assemblies.ToList())
            {
                //if the assembly has no references, remove it.
                if (!referencedIds.Contains(assembly.Id))
                {
                    this.Assemblies.Remove(assembly);
                    break;
                }


                //otherwise clean up components which link to items that no longer exist.
                assembly.Components.RemoveList(
                    assembly.Components.Where(ac => !this.Components.Select(c => c.Id).Contains(ac.PlanogramComponentId)).ToArray());

            }


            //CleanUp Components
            referencedIds =
                this.Fixtures.SelectMany(f => f.Components.Select(c => c.PlanogramComponentId))
                .Union(this.Assemblies.SelectMany(f => f.Components.Select(c => c.PlanogramComponentId)))
                .Distinct().ToList();

            List<Object> subComponentIds = new List<Object>();
            foreach (PlanogramComponent component in this.Components.ToList())
            {
                if (!referencedIds.Contains(component.Id))
                {
                    this.Components.Remove(component);
                }
                else
                {
                    subComponentIds.AddRange(component.SubComponents.Select(s => s.Id));
                }
            }

            //CleanUp Positions
            foreach (PlanogramPosition pos in this.Positions.ToList())
            {
                if (!subComponentIds.Contains(pos.PlanogramSubComponentId))
                {
                    this.Positions.Remove(pos);
                }
            }

            //CleanUp Annotations
            List<Object> fixtureComponentIds = this.Fixtures.SelectMany(f => f.Components.Select(c => c.Id)).ToList();
            List<Object> assemblyComponentIds = this.Assemblies.SelectMany(f => f.Components.Select(c => c.Id)).ToList();
            foreach (PlanogramAnnotation anno in this.Annotations.ToList())
            {
                if ((anno.PlanogramFixtureItemId != null && !this.FixtureItems.Any(f => Object.Equals(f.Id, anno.PlanogramFixtureItemId)))
                    || (anno.PlanogramFixtureComponentId != null && !fixtureComponentIds.Contains(anno.PlanogramFixtureComponentId))
                    || (anno.PlanogramAssemblyComponentId != null && !assemblyComponentIds.Contains(anno.PlanogramAssemblyComponentId))
                    || (anno.PlanogramSubComponentId != null && !subComponentIds.Contains(anno.PlanogramSubComponentId)))
                {
                    this.Annotations.Remove(anno);
                }
            }

            //CleanUp Images
            if (this.IsPropertyLoaded(Planogram.ImagesProperty))
            {
                RemoveOrphanImages();
            }
        }

        #region Images

        /// <summary>
        /// Tries to get the image with the given id. If not yet loaded
        /// this will return null and trigger an async fetch.
        /// </summary>
        public PlanogramImage GetImageLazy(Object imageTarget, Object imageId, PlanogramImageFacingType facingType, PlanogramImageType imageType,
            Action<Object> imageUpdateCallBack = null, Object userState = null)
        {
            Boolean placeHolder;
            return GetImageLazy(out placeHolder, imageTarget, imageId, facingType, imageType, imageUpdateCallBack, userState);
        }

        /// <summary>
        ///     Tries to get the image with the given id, or from the real image provider if the id is null.
        ///     If not yet loaded it will return null, trigger an async fetch and report as loading.
        /// </summary>
        /// <param name="isLoadingImages">OUT parameter containing whether the planogram images are loading images or done doing so.</param>
        /// <param name="imageTarget"></param>
        /// <param name="imageId">The Id of the image that should be fetched.</param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <param name="imageUpdateCallBack">the callback to invoke when the async fetch is complete</param>
        /// <param name="userState">the userstate to pass to the callback as a parameter.</param>
        /// <returns>The <see cref="PlanogramImage"/> that matches the <paramref name="imageId"/> in the planogram's image list.</returns>
        public PlanogramImage GetImageLazy(out Boolean isLoadingImages, Object imageTarget, Object imageId, PlanogramImageFacingType facingType, 
            PlanogramImageType imageType, Action<Object> imageUpdateCallBack = null, Object userState = null)
        {
            PlanogramImage img = null;
            if (imageId != null)
            {
                PlanogramImageList imageList = this.ImagesAsync;
                isLoadingImages = imageList == null;
                if (!isLoadingImages)
                {
                    img = imageList.FindById(imageId);
                }
            }
            else
            {
                isLoadingImages = false;
                img = PlanogramImagesHelper.GetRealImageAsync(imageTarget, facingType, imageType, imageUpdateCallBack, userState);
            }

            return img;
        }

        /// <summary>
        /// Tries to get the image with the given id, or from the real image provider if the id is null.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="imageId">The Id of the image that should be fetched.</param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <returns>The <see cref="PlanogramImage"/> that matches the <paramref name="imageId"/> in the planogram's image list.</returns>
        public PlanogramImage GetImage(Object imageTarget, Object imageId, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            if (imageId != null)
            {
                return this.Images.FindById(imageId);
            }
            else
            {
                return PlanogramImagesHelper.GetRealImage(imageTarget, facingType, imageType);
            }
        }

        /// <summary>
        /// Clears all images from the planogram.
        /// </summary>
        public void ClearImages()
        {
            Products.ClearImages();
            Components.ClearImages();
            Images.Clear();
        }
        
        /// <summary>
        ///     Removes any orphaned images (no longer referenced by this Planogram's Products or SubComponents) from the Image list.
        /// </summary>
        public void RemoveOrphanImages()
        {
            if (this == null || this.Images == null) return;

            // Get the referenced Ids 
            // from products and subcomponents.
            List<Object> referencedImageIds =
                this.Products.SelectMany(p => p.EnumerateImageIds())
                .Union(this.Components.SelectMany(c => c.SubComponents.SelectMany(s => s.EnumerateImageIds())))
                .Distinct()
                .ToList();

            // Remove each non referenced Id.
            foreach (PlanogramImage thisImage in this.Images.Where(o => referencedImageIds.All(id => !id.Equals(o.Id))).ToList())
            {
                //  NB remove the ImageData content to avoid unnecesary use of memory, 
                //  even if we remove it from the Images collection CSLA will keep a copy 
                //  in the DeletedList until the graph is saved, which could be a while.
                thisImage.ImageData = null;
                this.Images.Remove(thisImage);
            }
        }

        #endregion

        #endregion

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(Planogram);
            String typeFriendly = Planogram.FriendlyName;

            String detailsGroup = Message.Planogram_PropertyGroup_Details;
            String customGroup = Message.PlanogramComponent_PropertyGroup_Custom;
            String inventoryGroup = Message.Planogram_PropertyGroup_Inventory;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);


            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CategoryCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CategoryNameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationNameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ClusterSchemeNameProperty, detailsGroup)
                ;
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ClusterNameProperty, detailsGroup);


            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LengthUnitsOfMeasureProperty, detailsGroup); //dont display
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CurrencyUnitsOfMeasureProperty, detailsGroup);//dont display
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AreaUnitsOfMeasureProperty, detailsGroup);//dont display
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, VolumeUnitsOfMeasureProperty, detailsGroup);//dont display
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WeightUnitsOfMeasureProperty, detailsGroup);//dont display

            //inventory properties
            foreach (var property in PlanogramInventory.EnumerateDisplayablePropertyInfos())
            {
                var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, PlanogramInventory.FriendlyName,
                    InventoryProperty.Name + "." + property.Name,
                    property.FriendlyName,
                    property.Type, property.DisplayType, false, inventoryGroup);
                yield return fieldInfo;
            }

            //return properties from custom attribute data.
            foreach (var property in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, property, customGroup);
                fieldInfo.PropertyName = CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            //Metadata fields
            if (includeMetadata)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageDosProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFrontFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageUnitsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaBayCountProperty, detailsGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaBlocksDroppedProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangeFromPreviousStarRatingProperty, detailsGroup);//not in use
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangesFromPreviousCountProperty, detailsGroup);//not in use
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaComponentCountProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxDosProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinDosProperty, detailsGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNewProductsProperty, detailsGroup);//not in use
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedInventoryProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaProductsPlacedProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaProductsUnplacedProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalAreaWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentCollisionsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedDepthProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedHeightProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedWidthProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaHasComponentsOutsideOfFixtureAreaProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFrontFacingsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalLinearWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableAreaSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableLinearSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableVolumetricSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalPositionCollisionsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalUnitsProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalVolumetricWhiteSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaUniqueProductCountProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfProductNotAchievedCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfProductNotAchievedDOSProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfProductOverShelfLifePercentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfProductNotAchievedDeliveriesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfProductNotAchievedCasesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfProductNotAchievedDOSProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfProductOverShelfLifePercentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfProductNotAchievedDeliveriesProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfPositionsOutsideOfBlockSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfPositionsOutsideOfBlockSpaceProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfPlacedProductsRecommendedInAssortmentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfPlacedProductsRecommendedInAssortmentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfRecommendedProductsInAssortmentProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentOfProductNotAchievedInventoryProperty, detailsGroup);

                //Assortment rules related
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfAssortmentRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfInheritanceRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfLocalProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfDistributionRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfCoreRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfAssortmentRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfInheritanceRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfLocalProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfDistributionRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaPercentageOfCoreRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfDelistProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfForceProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfPreserveProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfMinimumHurdleProductRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfMaximumProductFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfMinimumProductFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfDependencyFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNumberOfDelistFamilyRulesBrokenProperty, detailsGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaCountOfProductsBuddiedProperty, detailsGroup);
            }
        }

        /// <summary>
        /// Returns the value of the given field for this product.
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            Debug.Assert(field.OwnerType == typeof(Planogram), "Field is not for this object type");

            Object value = null;

            try
            {
                if (field.PropertyName.StartsWith(Planogram.CustomAttributesProperty.Name + "."))
                {
                    //Custom attribute data field.
                    value = WpfHelper.GetItemPropertyValue(this.CustomAttributes, field.PropertyName.Split('.')[1]);
                }
                else if (field.PropertyName.StartsWith(Planogram.InventoryProperty.Name + "."))
                {
                    //Inventory property
                    value = WpfHelper.GetItemPropertyValue(this.Inventory, field.PropertyName.Split('.')[1]);
                }
                else if (field.PropertyName.StartsWith(Planogram.RenumberingStrategyProperty.Name + "."))
                {
                    //Renumbering Strategy property
                    value = WpfHelper.GetItemPropertyValue(this.RenumberingStrategy, field.PropertyName.Split('.')[1]);
                }
                else
                {
                    value = field.GetValue(this);
                }
            }
            catch
            {
            }

            return value;
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be updated via update planogram attribute task.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateAttributeFieldInfos()
        {
            Type type = typeof(Planogram);
            String typeFriendly = Planogram.FriendlyName;

            String group = Message.Planogram_PropertyGroup_General;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.CategoryCodeProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.CategoryNameProperty, group);

            #region Custom Data Text

            group = Message.Planogram_PropertyGroup_CustomDataText;

            var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text1Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text2Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text3Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text4Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text5Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text6Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text7Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text8Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text9Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text10Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text11Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text12Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text13Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text14Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text15Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text16Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text17Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text18Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text19Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text20Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text21Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text22Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text23Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text24Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text25Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text26Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text27Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text28Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text29Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text30Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text31Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text32Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text33Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text34Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text35Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text36Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text37Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text38Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text39Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text40Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text41Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text42Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text43Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text44Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text45Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text46Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text47Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text48Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text49Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Text50Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;

            #endregion

            #region Custom Data Value

            group = Message.Planogram_PropertyGroup_CustomDataValue;

            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value1Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value2Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value3Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value4Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value5Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value6Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value7Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value8Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value9Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value10Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value11Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value12Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value13Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value14Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value15Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value16Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value17Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value18Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value19Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value20Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value21Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value22Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value23Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value24Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value25Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value26Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value27Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value28Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value29Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value30Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value31Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value32Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value33Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value34Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value35Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value36Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value37Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value38Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value39Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value40Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value41Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value42Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value43Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value44Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value45Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value46Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value47Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value48Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value49Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Value50Property,
                group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;

            #endregion

            #region Custom Data Flag

            group = Message.Planogram_PropertyGroup_CustomDataFlag;

            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag1Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag2Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag3Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag4Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag5Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag6Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag7Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag8Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag9Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Flag10Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;

            #endregion

            #region Custom Data Date

            group = Message.Planogram_PropertyGroup_CustomDataDate;

            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date1Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date2Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date3Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date4Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date5Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date6Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date7Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date8Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date9Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Date10Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;

            #endregion

            #region Custom Data Note

            group = Message.Planogram_PropertyGroup_CustomDataNote;

            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Note1Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Note2Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Note3Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Note4Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;
            fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, CustomAttributeData.Note5Property, group);
            fieldInfo.PropertyName = Planogram.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
            yield return fieldInfo;


            #endregion

            group = Message.Planogram_PropertyGroup_Settings;

            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.LengthUnitsOfMeasureProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.AreaUnitsOfMeasureProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.VolumeUnitsOfMeasureProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.WeightUnitsOfMeasureProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.CurrencyUnitsOfMeasureProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.ProductPlacementXProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.ProductPlacementYProperty, group);
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.ProductPlacementZProperty, group);

            //renumbering strategy is enabled:
            yield return
                ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,
                Planogram.RenumberingStrategyProperty.Name + "." + PlanogramRenumberingStrategy.IsEnabledProperty.Name,
                PlanogramRenumberingStrategy.IsEnabledProperty.FriendlyName,
                PlanogramRenumberingStrategy.IsEnabledProperty.Type,
                PlanogramRenumberingStrategy.IsEnabledProperty.DisplayType,
                false,
                group);

            group = Message.Planogram_PropertyGroup_Info;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.PlanogramTypeProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.StatusProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.DateWipProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.DateApprovedProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Planogram.DateArchivedProperty, group);
        }

        #endregion

        #region Metadata

        private Boolean _createMetadataImage;

        /// <summary>
        /// Calculates metadata for this planogram only
        /// and returns a list of broken validation warnings.
        /// </summary>
        /// <remarks>This method mainly gets used by space planning as it
        /// calculates metadata on the fly.</remarks>
        public List<PlanogramValidationWarning> CalculateMetadataAndGetValidationWarnings(
            Boolean processMetadataImages, IPlanogramMetadataHelper metadataHelper, PlanogramMerchandisingGroupList merchGroupList = null)
        {
            Boolean disposeOfMerchGroups = merchGroupList == null;
            if (merchGroupList == null) merchGroupList = GetMerchandisingGroups();

            try
            {
                List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();

                //create a new metadetails object to use and keep a reference so we can just
                //reuse it for the validation warnings for speed.
                PlanogramMetadataDetails metaDetails =
                    PlanogramMetadataDetails.NewPlanogramMetadataDetails(this, metadataHelper, merchGroupList);

                _createMetadataImage = processMetadataImages;
                _metadataDetails = metaDetails;

                //call all calculate metadata methods
                CalculateTreeMetadata();

                //get the validation warnings.
                warnings = GetAllBrokenValidationWarnings(metaDetails);

                metaDetails = null;

                return warnings;
            }
            finally
            {
                if (disposeOfMerchGroups) merchGroupList.Dispose();
            }
        }

        /// <summary>
        /// Overide of base method called when calculating metadata for
        /// this type.
        /// </summary>
        /// <remarks>Called when calculating metadata for the package.</remarks>
        protected override void CalculateMetadata()
        {
            //ensure that the metadata details start null
            _metadataDetails = null;

            Package parentPackage = this.Parent;
            _createMetadataImage = (parentPackage != null && parentPackage.ProcessMetadataImages);

            CalculateTreeMetadata();
        }

        /// <summary>
        /// Calls calculate metadata for all required child objects
        /// and then OnCalculateMetadata for this planogram.
        /// </summary>
        private void CalculateTreeMetadata()
        {
            //NB Enumerate through children in set order due to dependencies as the csla 
            //field manager does not see lazy loaded children properties when looking for children.
            base.CalculateMetadata(
                new Object[]
                {
                    //1. Positions
                    this.Positions,

                    //2. Products (rely on position data)
                    this.Products,

                    //3. Fixtures (rely on position & product meta data)
                    this.Fixtures,

                    // 4. (no dependencies)
                    this.Assemblies,

                    // 5. Fixture Items (rely on fixture meta data)
                    this.FixtureItems,

                    //6. Components (no dependencies)
                    this.Components,

                     //7. Annotations (no dependencies)
                     this.Annotations,

                     //8. Performance
                     this.Performance,

                     //9.Assortment
                     this.Assortment,

                     //10.Blocking
                     this.Blocking,
                      //11.ConsumerDecisionTree
                     this.ConsumerDecisionTree,
                });
        }

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            Boolean createMetadataImage = _createMetadataImage;

            PlanogramMetadataDetails metadataDetails = GetPlanogramMetadataDetails();
            PlanogramPositionList positions = this.Positions;
            Int32 positionCount = positions.Count;
            Boolean hasAnyPositions = positionCount > 0;
            PlanogramProductList products = this.Products;
            PlanogramFixtureList fixtures = this.Fixtures;
            PlanogramPerformanceDataList performanceData = this.Performance.PerformanceData;

            List<Object> placedProductIds = positions.Select(p => p.PlanogramProductId).Distinct().ToList();
            List<PlanogramProduct> placedProducts = products.Where(p => placedProductIds.Contains(p.Id)).ToList();
            
            //get performance data for products that are placed on the plan
            IEnumerable<PlanogramPerformanceData> positionPerformanceData = performanceData.Where(p => placedProductIds.Contains(p.PlanogramProductId));

            //Calculate all positions details
            Dictionary<Object, PlanogramPositionDetails> positionDetailsByPosition = metadataDetails.PositionDetails;

            // Get an enumeration of all the positions that lie at the front of the plan.
            //IEnumerable<Object> positionsAtFrontOfPlan = GetPositionsAtFrontOfPlan(); // not in use?

            //Set meta data properties based on child meta data
            this.MetaBayCount = this.FixtureItems.Count;
            this.MetaUniqueProductCount = products.Count;
            this.MetaComponentCount = this.Components.Count;

            var merchSpaces = (ICollection<PlanogramMerchandisingSpace>)metadataDetails.GetAllMerchandisingSpace();


            // Total merchandisable linear space.
            // This is the sum of all the widths of the components in the plan, which gives us the 
            // amount of space we can merchandise in the X direction. This is right for shelves,
            // pegboards and chests, but may not be suitable for rods or clipstrips.
            this.MetaTotalMerchandisableLinearSpace = (Single)Math.Round(merchSpaces.Sum(s => s.GetMerchandisableLinear()), 2, MidpointRounding.AwayFromZero);

            // Total merchandisable area space.
            // This is, the sum of all the merchandisable areas of the sub components.
            this.MetaTotalMerchandisableAreaSpace = Convert.ToSingle(Math.Round(merchSpaces.Sum(s => s.GetMerchandisableArea()), 2, MidpointRounding.AwayFromZero));


            // Total merchandisable area volume.
            // This is, the sum of all the merchandisable areas of the sub components, multiplied by their depths.
            this.MetaTotalMerchandisableVolumetricSpace = Convert.ToSingle(Math.Round(merchSpaces.Sum(s => s.GetMerchandisableVolume()), 2, MidpointRounding.AwayFromZero));

            //V830352 - Calculate the white space at a cumulative merchandising space level, not as a total for the planogram.
            // Components which are overfaced mask white space on other shelves.
            Single totalPlanLinearWhiteSpace = 0, totalPlanAreaWhiteSpace = 0, totalPlanVolumetricWhiteSpace = 0;
            Int32 numberOfMerchSpaces = merchSpaces.Count();
            foreach (PlanogramMerchandisingSpace merchSpace in merchSpaces)
            {
                if (merchSpace.SubComponentPlacement == null) continue;

                //Get all positions in this merch space
                IEnumerable<PlanogramPosition> merchSpacePositions = merchSpace.SubComponentPlacement.GetPlanogramPositions();

                //Get position details
                var merchSpacePositionDetails = new List<PlanogramPositionDetails>();
                foreach (PlanogramPosition pos in merchSpacePositions)
                { 
                    PlanogramPositionDetails posDetails = null;
                    if(positionDetailsByPosition.TryGetValue(pos.Id, out posDetails))
                    {
                        merchSpacePositionDetails.Add(posDetails);
                    }
                }

                // The total width of white space. That is, the difference between the sum of the widths
                // of all the positions at the front of the plan and the total width of merchandising space of
                // all of the subcomponents.
                totalPlanLinearWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                    merchSpace.GetMerchandisableLinear(),
                    merchSpacePositionDetails.Sum(p => p.TotalSize.Width)) ?? 0.0F) / numberOfMerchSpaces;

                // The total area of white space. That is, the difference between the sum of all the positions
                // that are at the front of the plan and the sum of the merchandisable areas of all the subcomponents.
                totalPlanAreaWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                    merchSpace.GetMerchandisableArea(),
                    merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height)) ?? 0.0F) / numberOfMerchSpaces;

                // The total volume of white space. That is, the difference between the sum of the volumes 
                // of all the positions and the sum of the merchandisable volumes of all the subcomponents.
                totalPlanVolumetricWhiteSpace += ((PlanogramHelper.CalculateMetaTotalWhiteSpace(
                    merchSpace.GetMerchandisableVolume(),
                    merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height * p.TotalSize.Depth)) ?? 0.0F) / numberOfMerchSpaces);
            }

            //Set cumulative values to be totals
            this.MetaTotalLinearWhiteSpace = (Single?)Math.Round(totalPlanLinearWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalAreaWhiteSpace = (Single?)Math.Round(totalPlanAreaWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalVolumetricWhiteSpace = (Single?)Math.Round(totalPlanVolumetricWhiteSpace, 2, MidpointRounding.AwayFromZero);

            this.MetaProductsPlaced = positions.Select(p => p.PlanogramProductId).Distinct().Count();
            this.MetaProductsUnplaced = this.MetaUniqueProductCount - this.MetaProductsPlaced;
            this.MetaNewProducts = 0; //TODO
            this.MetaChangesFromPreviousCount = 0; //TODO
            this.MetaChangeFromPreviousStarRating = 0; //TODO
            this.MetaBlocksDropped = 0; //TODO
            this.MetaTotalFacings = PlanogramHelper.CalculateMetaTotalFacings(positions, products);
            this.MetaAverageFacings = Convert.ToSingle(hasAnyPositions ? MetaTotalFacings / positionCount : 0);
            this.MetaTotalUnits = positions.Sum(p => p.TotalUnits);
            this.MetaAverageUnits = Convert.ToSingle(hasAnyPositions ? positions.Average(p => p.TotalUnits) : 0);
            this.MetaMinDos = positionPerformanceData.Min(p => p.AchievedDos) ?? 0;
            this.MetaMaxDos = positionPerformanceData.Max(p => p.AchievedDos) ?? 0;
            this.MetaAverageDos = positionPerformanceData.Average(p => p.AchievedDos) ?? 0;
            this.MetaMinCases = hasAnyPositions ? positions.Min(p => p.MetaAchievedCases) : 0;
            this.MetaAverageCases = hasAnyPositions ? positions.Average(p => p.MetaAchievedCases) : 0;
            this.MetaMaxCases = hasAnyPositions ? positions.Max(p => p.MetaAchievedCases) : 0;
            this.MetaTotalComponentsOverMerchandisedWidth = fixtures.Sum(p => p.Components.Count(r => r.MetaIsOverMerchandisedWidth == true));
            this.MetaTotalComponentsOverMerchandisedHeight = fixtures.Sum(p => p.Components.Count(r => r.MetaIsOverMerchandisedHeight == true));
            this.MetaTotalComponentsOverMerchandisedDepth = fixtures.Sum(p => p.Components.Count(r => r.MetaIsOverMerchandisedDepth == true));
            this.MetaTotalFrontFacings = Convert.ToInt16(hasAnyPositions ? positions.Sum(p => p.MetaFrontFacingsWide) : 0);
            this.MetaAverageFrontFacings = Convert.ToSingle(hasAnyPositions ? MetaTotalFrontFacings / positionCount : 0);

            
            
            //Count all of the positions relating to this planogram that have a collision
            this.MetaTotalPositionCollisions = positions.Count(p => p.MetaIsPositionCollisions == true);

            //Sum all of the fixture items component collisions
            this.MetaTotalComponentCollisions = this.FixtureItems.Sum(p => p.MetaTotalComponentCollisions);


            //Assortment related:
            this.MetaCountOfPlacedProductsRecommendedInAssortment = placedProducts.Count(p => p.MetaIsRangedInAssortment == true);
            this.MetaCountOfPlacedProductsNotRecommendedInAssortment = placedProducts.Count(p => p.MetaIsRangedInAssortment == false);
            this.MetaCountOfRecommendedProductsInAssortment = this.Assortment.Products.Count(p => p.IsRanged);

            this.MetaPercentOfPlacedProductsRecommendedInAssortment =
                (this.MetaCountOfRecommendedProductsInAssortment.Value > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaCountOfPlacedProductsRecommendedInAssortment.Value / (Double)this.MetaCountOfRecommendedProductsInAssortment.Value),
                4, MidpointRounding.AwayFromZero)) : 0;

            this.MetaNotAchievedInventory = placedProducts.Count(p => p.MetaNotAchievedInventory == true);

            this.MetaPercentOfProductNotAchievedInventory =
                (this.MetaProductsPlaced.Value > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaNotAchievedInventory.Value / (Double)this.MetaProductsPlaced.Value),
                4, MidpointRounding.AwayFromZero)) : 0;

            //Inventory related:
            this.MetaCountOfProductNotAchievedCases = placedProducts.Count(p => p.MetaNotAchievedCases == true);
            this.MetaCountOfProductNotAchievedDOS = placedProducts.Count(p => p.MetaNotAchievedDOS == true);
            this.MetaCountOfProductOverShelfLifePercent = placedProducts.Count(p => p.MetaIsOverShelfLifePercent == true);
            this.MetaCountOfProductNotAchievedDeliveries = placedProducts.Count(p => p.MetaNotAchievedDeliveries == true);

            this.MetaPercentOfProductNotAchievedCases =
                (this.MetaProductsPlaced.Value > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaCountOfProductNotAchievedCases.Value / (Double)this.MetaProductsPlaced.Value),
                4, MidpointRounding.AwayFromZero)) : 0;

            this.MetaPercentOfProductNotAchievedDOS =
                (this.MetaProductsPlaced.Value > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaCountOfProductNotAchievedDOS.Value / (Double)this.MetaProductsPlaced.Value),
                4, MidpointRounding.AwayFromZero)) : 0;

            this.MetaPercentOfProductOverShelfLifePercent =
                (this.MetaProductsPlaced.Value > 0) ?
                 Convert.ToSingle(Math.Round(((Double)this.MetaCountOfProductOverShelfLifePercent.Value / (Double)this.MetaProductsPlaced.Value),
                 4, MidpointRounding.AwayFromZero)) : 0;

            this.MetaPercentOfProductNotAchievedDeliveries =
                (this.MetaProductsPlaced.Value > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaCountOfProductNotAchievedDeliveries.Value / (Double)this.MetaProductsPlaced.Value),
                4, MidpointRounding.AwayFromZero)) : 0;


            //Blocking related:
            this.MetaCountOfPositionsOutsideOfBlockSpace = positions.Count(p => p.MetaIsOutsideOfBlockSpace == true);
            this.MetaPercentOfPositionsOutsideOfBlockSpace =
                (positionCount > 0) ?
                Convert.ToSingle(Math.Round(((Double)this.MetaCountOfPositionsOutsideOfBlockSpace.Value / (Double)positionCount),
                4, MidpointRounding.AwayFromZero)) : 0;


            // recalculate products as some meta data requires planogram meta data to be calculate first
            foreach (PlanogramPosition position in positions) position.CalculatePostPlanogramMetadata();
            foreach (PlanogramProduct product in products) product.CalculatePostPlanogramMetadata();

            // calculate CDT meta space values as product data may have changed
            this.ConsumerDecisionTree.RootNode.CalculatePostMetaData();

            //Assortment rules related meta data
            if (this.Assortment != null && this.Assortment.Products.Count > 0)
            {
                //work out broken rules at most granular level so we can reuse calculations            
                this.MetaNumberOfDelistProductRulesBroken = (Int16)products.Count(p => p.MetaIsDelistProductRuleBroken.HasValue && p.MetaIsDelistProductRuleBroken.Value);
                this.MetaNumberOfForceProductRulesBroken = (Int16)products.Count(p => p.MetaIsForceProductRuleBroken.HasValue && p.MetaIsForceProductRuleBroken.Value);
                this.MetaNumberOfPreserveProductRulesBroken = (Int16)products.Count(p => p.MetaIsPreserveProductRuleBroken.HasValue && p.MetaIsPreserveProductRuleBroken.Value);
                this.MetaNumberOfMinimumHurdleProductRulesBroken = (Int16)products.Count(p => p.MetaIsMinimumHurdleProductRuleBroken.HasValue && p.MetaIsMinimumHurdleProductRuleBroken.Value);
                this.MetaNumberOfMaximumProductFamilyRulesBroken = (Int16)products.Count(p => p.MetaIsMaximumProductFamilyRuleBroken.HasValue && p.MetaIsMaximumProductFamilyRuleBroken.Value);
                this.MetaNumberOfMinimumProductFamilyRulesBroken = (Int16)products.Count(p => p.MetaIsMinimumProductFamilyRuleBroken.HasValue && p.MetaIsMinimumProductFamilyRuleBroken.Value);
                this.MetaNumberOfDependencyFamilyRulesBroken = (Int16)products.Count(p => p.MetaIsDependencyFamilyRuleBroken.HasValue && p.MetaIsDependencyFamilyRuleBroken.Value);
                this.MetaNumberOfDelistFamilyRulesBroken = (Int16)products.Count(p => p.MetaIsDelistFamilyRuleBroken.HasValue && p.MetaIsDelistFamilyRuleBroken.Value);

                //work out higher level broken rule counts
                this.MetaNumberOfInheritanceRulesBroken = (Int16)products.Count(p => p.MetaIsInheritanceRuleBroken.HasValue && p.MetaIsInheritanceRuleBroken.Value);
                this.MetaNumberOfLocalProductRulesBroken = (Int16)products.Count(p => p.MetaIsLocalProductRuleBroken.HasValue && p.MetaIsLocalProductRuleBroken.Value);
                this.MetaNumberOfDistributionRulesBroken = (Int16)products.Count(p => p.MetaIsDistributionRuleBroken.HasValue && p.MetaIsDistributionRuleBroken.Value);
                this.MetaNumberOfCoreRulesBroken = (Int16)products.Count(p => p.MetaIsCoreRuleBroken.HasValue && p.MetaIsCoreRuleBroken.Value);
                this.MetaNumberOfProductRulesBroken = (Int16)(MetaNumberOfDelistProductRulesBroken.Value + MetaNumberOfForceProductRulesBroken.Value
                    + MetaNumberOfPreserveProductRulesBroken.Value + MetaNumberOfMinimumHurdleProductRulesBroken.Value + MetaNumberOfCoreRulesBroken.Value);
                this.MetaNumberOfFamilyRulesBroken = (Int16)(MetaNumberOfMaximumProductFamilyRulesBroken.Value
                    + MetaNumberOfMinimumProductFamilyRulesBroken.Value + MetaNumberOfDependencyFamilyRulesBroken.Value + MetaNumberOfDelistFamilyRulesBroken.Value);

                //sum all assortment rules broken together
                this.MetaNumberOfAssortmentRulesBroken = (Int16)(MetaNumberOfProductRulesBroken.Value + MetaNumberOfFamilyRulesBroken.Value + MetaNumberOfInheritanceRulesBroken.Value
                    + MetaNumberOfLocalProductRulesBroken.Value + MetaNumberOfLocalProductRulesBroken.Value + MetaNumberOfCoreRulesBroken.Value);

                //calculate percentages
                Double numberOfProductRules = (Double)this.Assortment.Products.Count(p => p.RuleType != PlanogramAssortmentProductRuleType.None);
                this.MetaPercentageOfProductRulesBroken =
                    (numberOfProductRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfProductRulesBroken.Value / numberOfProductRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double numberOfFamilyRules = (Double)this.Assortment.Products.Count(p => p.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None);
                this.MetaPercentageOfFamilyRulesBroken =
                    (numberOfFamilyRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfFamilyRulesBroken.Value / numberOfFamilyRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double numberOfInheritanceRules = (Double)this.Assortment.Products.Count(p => p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Inheritance);
                this.MetaPercentageOfInheritanceRulesBroken =
                    (numberOfInheritanceRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfInheritanceRulesBroken.Value / numberOfInheritanceRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double numberLocalProductRules = (Double)this.Assortment.LocalProducts.Count();
                this.MetaPercentageOfLocalProductRulesBroken =
                    (numberLocalProductRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfLocalProductRulesBroken.Value / numberLocalProductRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double numberOfDistributionRules = (Double)this.Assortment.Products.Count(p => p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Distribution);
                this.MetaPercentageOfDistributionRulesBroken =
                    (numberOfDistributionRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfDistributionRulesBroken.Value / numberOfDistributionRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double numberOfCoreRules = (Double)this.Assortment.Products.Count(p => p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Core);
                this.MetaPercentageOfCoreRulesBroken =
                    (numberOfCoreRules > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfCoreRulesBroken.Value / numberOfCoreRules),
                    4, MidpointRounding.AwayFromZero)) : 0;

                Double totalNumberOfRulesSetup = (Double)(numberOfProductRules + numberOfFamilyRules + numberOfInheritanceRules
                    + numberOfDistributionRules + numberOfCoreRules + numberLocalProductRules);
                this.MetaPercentageOfAssortmentRulesBroken =
                    (totalNumberOfRulesSetup > 0) ?
                    Convert.ToSingle(Math.Round(((Double)this.MetaNumberOfAssortmentRulesBroken.Value / totalNumberOfRulesSetup),
                    4, MidpointRounding.AwayFromZero)) : 0;

                this.MetaCountOfProductsBuddied = (Int16)products.Count(p => p.MetaIsBuddied.HasValue && p.MetaIsBuddied.Value);
                
            }
            else
            { 
                //Default all values to 0
                this.MetaNumberOfAssortmentRulesBroken = 0;
                this.MetaNumberOfProductRulesBroken = 0;
                this.MetaNumberOfFamilyRulesBroken = 0;
                this.MetaNumberOfInheritanceRulesBroken = 0;
                this.MetaNumberOfLocalProductRulesBroken = 0;
                this.MetaNumberOfDistributionRulesBroken = 0;
                this.MetaNumberOfCoreRulesBroken = 0;
                this.MetaPercentageOfAssortmentRulesBroken = 0;
                this.MetaPercentageOfProductRulesBroken = 0;
                this.MetaPercentageOfFamilyRulesBroken = 0;
                this.MetaPercentageOfInheritanceRulesBroken = 0;
                this.MetaPercentageOfLocalProductRulesBroken = 0;
                this.MetaPercentageOfDistributionRulesBroken = 0;
                this.MetaPercentageOfCoreRulesBroken = 0;
                this.MetaNumberOfDelistProductRulesBroken = 0;
                this.MetaNumberOfForceProductRulesBroken = 0;
                this.MetaNumberOfPreserveProductRulesBroken = 0;
                this.MetaNumberOfMinimumHurdleProductRulesBroken = 0;
                this.MetaNumberOfMaximumProductFamilyRulesBroken = 0;
                this.MetaNumberOfMinimumProductFamilyRulesBroken = 0;
                this.MetaNumberOfDependencyFamilyRulesBroken = 0;
                this.MetaNumberOfDelistFamilyRulesBroken = 0;
                this.MetaCountOfProductsBuddied = 0;
            }

            //Calcualte is outside of fixture area. 
            this.MetaHasComponentsOutsideOfFixtureArea = this.CheckComponentsOutsideOfFixtureAreaExists();

            //calculate metadata about the event log.
            CalculateEventLogMetadata();


            //Nb Planogram render does not work on threaded process in UT.
            if (createMetadataImage && PlanogramImagesHelper.IsPlanogramImageRendererRegistered())
            {
                //Create thumbnail using process synchronously
                Framework.Planograms.Processes.RenderPlanogramImage.Process planogramRenderProcess =
                    new Framework.Planograms.Processes.RenderPlanogramImage.Process(
                        this, true, Convert.ToInt32(this.Parent.LockUserId),
                        metadataDetails.GetPlanogramImageSettings(this));

                planogramRenderProcess.Execute();

                //Create thumbnail and add to image
                PlanogramMetadataImage metadataImage = PlanogramMetadataImage.NewPlanogramMetadataImage();
                if (planogramRenderProcess.ImageData != null)
                {
                    metadataImage.ImageData = planogramRenderProcess.ImageData;
                    metadataImage.FileName = String.Empty; //Required?
                    metadataImage.Description = String.Empty; //Required?
                    this.PlanogramMetadataImages.Add(metadataImage);
                }
            }

            //clear off the cached metadata details as we are done with them
            _metadataDetails = null;
        }

        /// <summary>
        /// Checks whether the plan contains any fixture components or assembly conponents that are outside of the fiture area. Uses other metadata from components.
        /// </summary>
        /// <returns>true when contains a component is outside fixture space.</returns>
        private Boolean? CheckComponentsOutsideOfFixtureAreaExists()
        {
            Boolean fixtureComponent = this.FixtureItems.Any(f => f.GetPlanogramFixture().Components.Any(c => c.MetaIsOutsideOfFixtureArea == true));
            if (fixtureComponent) return true;

            Boolean AssemblyComponent = this.Assemblies.Any(a => a.Components.Any(c => c.MetaIsOutsideOfFixtureArea == true));
            if (AssemblyComponent) return true;
            
            return false;
        }

        /// <summary>
        /// Gets all the positions that are at the front of the plan. I.e. those positions that have the highest
        /// z sequence out of all the positions in their merchandising group.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Object> GetPositionsAtFrontOfPlan()
        {
            using (PlanogramMerchandisingGroupList merchGroups = GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroups)
                {
                    if (merchandisingGroup.PositionPlacements.Count == 0) continue;
                    Int16 maxSequenceZ = merchandisingGroup.PositionPlacements.Max(p => p.Position.SequenceZ);
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        if (positionPlacement.Position.SequenceZ == maxSequenceZ)
                        {
                            yield return positionPlacement.Position.Id;
                        }
                    }
                }
            }
            yield break;
        }

        /// <summary>
        /// Override of the call to clear the metadata
        /// of this instance and all of its child objects.
        /// </summary>
        protected override void ClearMetadata()
        {
            base.ClearMetadata(
                new Object[]
                {
                    this.Positions,
                    this.Products,
                    this.Fixtures,
                    this.Assemblies,
                    this.FixtureItems,
                    this.Components,
                    this.Annotations,
                    this.Performance,
                    this.Assortment,
                    this.Blocking,
                    this.ConsumerDecisionTree
                });
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            //Set meta data properties based on child meta data
            this.MetaBayCount = null;
            this.MetaUniqueProductCount = null;
            this.MetaComponentCount = null;
            this.MetaTotalMerchandisableLinearSpace = null;
            this.MetaTotalMerchandisableAreaSpace = null;
            this.MetaTotalMerchandisableVolumetricSpace = null;
            this.MetaTotalLinearWhiteSpace = null;
            this.MetaTotalAreaWhiteSpace = null;
            this.MetaTotalVolumetricWhiteSpace = null;
            this.MetaProductsPlaced = null;
            this.MetaProductsUnplaced = null;
            this.MetaNewProducts = null;
            this.MetaChangesFromPreviousCount = null;
            this.MetaChangeFromPreviousStarRating = null;
            this.MetaBlocksDropped = null;
            this.MetaNotAchievedInventory = null;
            this.MetaTotalFacings = null;
            this.MetaAverageFacings = null;
            this.MetaTotalUnits = null;
            this.MetaAverageUnits = null;
            this.MetaMinDos = null;
            this.MetaMaxDos = null;
            this.MetaAverageDos = null;
            this.MetaMinCases = null;
            this.MetaAverageCases = null;
            this.MetaMaxCases = null;
            this.MetaHasComponentsOutsideOfFixtureArea = null;
            this.MetaTotalComponentsOverMerchandisedWidth = null;
            this.MetaTotalComponentsOverMerchandisedHeight = null;
            this.MetaTotalComponentsOverMerchandisedDepth = null;
            this.MetaTotalComponentCollisions = null;
            this.MetaTotalPositionCollisions = null;
            this.MetaTotalFrontFacings = null;
            this.MetaAverageFrontFacings = null;
            this.MetaNoOfErrors = null;
            this.MetaNoOfWarnings = null;
            this.MetaNoOfInformationEvents = null;
            this.MetaNoOfDebugEvents = null;
            this.MetaTotalErrorScore = null;
            this.MetaHighestErrorScore = null;
            this.MetaCountOfProductNotAchievedCases = null;
            this.MetaCountOfProductNotAchievedDOS = null;
            this.MetaCountOfProductOverShelfLifePercent = null;
            this.MetaCountOfProductNotAchievedDeliveries = null;
            this.MetaPercentOfProductNotAchievedCases = null;
            this.MetaPercentOfProductNotAchievedDOS = null;
            this.MetaPercentOfProductOverShelfLifePercent = null;
            this.MetaPercentOfProductNotAchievedDeliveries = null;
            this.MetaCountOfPositionsOutsideOfBlockSpace = null;
            this.MetaPercentOfPositionsOutsideOfBlockSpace = null;
            this.MetaPercentOfPlacedProductsRecommendedInAssortment = null;
            this.MetaCountOfPlacedProductsRecommendedInAssortment = null;
            this.MetaCountOfPlacedProductsNotRecommendedInAssortment = null;
            this.MetaCountOfRecommendedProductsInAssortment = null;
            this.MetaPercentOfProductNotAchievedInventory = null;
            this.MetaNumberOfAssortmentRulesBroken = null;
            this.MetaNumberOfProductRulesBroken = null;
            this.MetaNumberOfFamilyRulesBroken = null;
            this.MetaNumberOfInheritanceRulesBroken = null;
            this.MetaNumberOfLocalProductRulesBroken = null;
            this.MetaNumberOfDistributionRulesBroken = null;
            this.MetaNumberOfCoreRulesBroken = null;
            this.MetaPercentageOfAssortmentRulesBroken = null;
            this.MetaPercentageOfProductRulesBroken = null;
            this.MetaPercentageOfFamilyRulesBroken = null;
            this.MetaPercentageOfInheritanceRulesBroken = null;
            this.MetaPercentageOfLocalProductRulesBroken = null;
            this.MetaPercentageOfDistributionRulesBroken = null;
            this.MetaPercentageOfCoreRulesBroken = null;
            this.MetaNumberOfDelistProductRulesBroken = null;
            this.MetaNumberOfForceProductRulesBroken = null;
            this.MetaNumberOfPreserveProductRulesBroken = null;
            this.MetaNumberOfMinimumHurdleProductRulesBroken = null;
            this.MetaNumberOfMaximumProductFamilyRulesBroken = null;
            this.MetaNumberOfMinimumProductFamilyRulesBroken = null;
            this.MetaNumberOfDependencyFamilyRulesBroken = null;
            this.MetaNumberOfDelistFamilyRulesBroken = null;
            this.MetaCountOfProductsBuddied = null;

            //clear the meta data images.
            if (this.Parent.ProcessMetadataImages)
            {
                if (this.PlanogramMetadataImages != null
                    && this.PlanogramMetadataImages.Count > 0)
                {
                    this.PlanogramMetadataImages.Clear();
                }
            }

            _metadataDetails = null;
        }
        
        /// <summary>
        /// Returns the metadata details object for this plan.
        /// This contains cached results that may be used by multiple model objects
        /// while meta data is being calculated.
        /// </summary>
        /// <returns></returns>
        internal PlanogramMetadataDetails GetPlanogramMetadataDetails()
        {
            if (_metadataDetails == null)
            {
                _metadataDetails = PlanogramMetadataDetails.NewPlanogramMetadataDetails(this, this.Parent.metadataHelper);
            }
            return _metadataDetails;
        }

        internal void CalculateEventLogMetadata()
        {
            //Event log related:
            this.MetaNoOfErrors = this.EventLogs.Count(p => p.EntryType == PlanogramEventLogEntryType.Error);
            this.MetaNoOfWarnings = this.EventLogs.Count(p => p.EntryType == PlanogramEventLogEntryType.Warning);
            this.MetaNoOfInformationEvents = this.EventLogs.Count(p => p.EntryType == PlanogramEventLogEntryType.Information);
            this.MetaNoOfDebugEvents = this.EventLogs.Count(p => p.EntryType == PlanogramEventLogEntryType.Debug);
            this.MetaTotalErrorScore = this.MetaNoOfErrors > 0 ? this.EventLogs.Where(p => p.EntryType == PlanogramEventLogEntryType.Error).Sum(p => p.Score) : 0;
            this.MetaHighestErrorScore = this.MetaNoOfErrors > 0 ? this.EventLogs.Where(p => p.EntryType == PlanogramEventLogEntryType.Error).Max(p => p.Score) : (Byte)0;

        }

        #endregion

        #region Validation Data
        /// <summary>
        /// Calculates validation data for this instance
        /// </summary>
        protected override void CalculateValidationData()
        {
            ((IModelObject)this.ValidationTemplate).CalculateValidationData();
        }

        /// <summary>
        /// Clears validation data for this instance
        /// </summary>
        protected override void ClearValidationData()
        {
            ((IModelObject)this.ValidationTemplate).ClearValidationData();
        }

        /// <summary>
        /// Returns a list of all validation warnings which have been broken.
        /// </summary>
        public List<PlanogramValidationWarning> GetAllBrokenValidationWarnings()
        {
            //ensure metadata details are clear.
            _metadataDetails = null;

            List<PlanogramValidationWarning> warnings =
                GetAllBrokenValidationWarnings(GetPlanogramMetadataDetails());

            //clear off details again.
            _metadataDetails = null;

            return warnings;
        }

        /// <summary>
        /// Returns a list of all validation warnings which have been broken.
        /// </summary>
        private List<PlanogramValidationWarning> GetAllBrokenValidationWarnings(PlanogramMetadataDetails metaDetails)
        {
            List<PlanogramValidationWarning> warnings = new List<PlanogramValidationWarning>();

            // Component warnings (both Fixture and Assembly).
            foreach (PlanogramFixtureItem fixtureItem in this.FixtureItems)
            {
                PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
                if (fixture == null) continue;

                foreach (PlanogramFixtureComponent fixtureComponent in fixture.Components)
                {
                    warnings.AddRange(fixtureComponent.GetBrokenValidationWarnings(metaDetails, fixtureItem));
                }

                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies)
                {
                    warnings.AddRange(fixtureAssembly.GetBrokenValidationWarnings(metaDetails, fixtureItem));
                }
            }


            // Position warnings.
            foreach (PlanogramPosition position in this.Positions)
            {
                warnings.AddRange(position.GetBrokenValidationWarnings(metaDetails));
            }

            // Product related warnings (shown for the positions holding them).
            foreach (PlanogramProduct product in this.Products)
            {
                warnings.AddRange(product.GetBrokenValidationWarnings(metaDetails));

                // Attribute Validation to go here.
                warnings.AddRange(ProductAttributeComparisonResult.Where(r => r.ProductGtin == product.Gtin).Select(r => PlanogramValidationWarning.NewPlanogramValidationWarning(PlanogramValidationWarningType.ProductAttributeDoesNotMatchMasterProductAttribute, product, r)));
            }


            return warnings;
        }

        /// <summary>
        /// Method to clear all the product attribute validation warnings
        /// </summary>
        public void ClearProductAttributeValidationWarnings()
        {
            ProductAttributeComparisonResult.Clear();
        }

        /// <summary>
        /// Method to add Product Attribute Validation Warning
        /// </summary>
        /// <param name="gtin">Product GTIN</param>
        /// <param name="attributeCompared">Attribute compared</param>
        /// <param name="expectedMasterDataValue">Value in the master data</param>
        /// <param name="actualProductValue">Value from the product attribute</param>
        public void AddProductAttributeValidationWarning(String gtin, String attributeCompared, String expectedMasterDataValue,
            String actualProductValue)
        {
            ProductAttributeComparisonResult.StoreResult(gtin, attributeCompared, expectedMasterDataValue, actualProductValue);
        }

        /// <summary>
        /// Product that need to be validated
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramProduct> GetProductsToValidateAttributesFor()
        {
            return Products;
        }
        
        #endregion

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Compares two <see cref="RectValue"/> instances and determines whether they overlap in all the given <paramref name="axes"/>.
        /// </summary>
        /// <param name="rectA"></param>
        /// <param name="rectB"></param>
        /// <param name="axes"></param>
        /// <returns></returns>
        private static Boolean OverlapInAxes(RectValue rectA, RectValue rectB, IEnumerable<AxisType> axes)
        {
            //  Two RectValues overlap in an axis when...
            foreach (AxisType axisType in axes)
            {
                //  They both have the same origin in that axis...
                Single rectAMinCoord = rectA.GetCoordinate(axisType);
                Single rectBMinCoord = rectB.GetCoordinate(axisType);
                Boolean originIsShared = rectAMinCoord.EqualTo(rectBMinCoord);
                if (originIsShared) continue;

                //  One of them starts within the other...
                Single rectAMaxCoord = rectAMinCoord + rectA.GetSize(axisType);
                Boolean rectAStartsWithinRectB = (rectAMaxCoord > rectBMinCoord) && (rectBMinCoord > rectAMinCoord);
                if (rectAStartsWithinRectB) continue;

                //  The other starts within the one...
                Single rectBMaxCoord = rectBMinCoord + rectB.GetSize(axisType);
                Boolean rectBStartsWithinRectA = (rectBMaxCoord > rectAMinCoord) && (rectAMinCoord > rectBMinCoord);
                if (rectBStartsWithinRectA) continue;

                //  Otherwise, they do not overlap in the axis.
                return false;
            }

            //  If all required axes overlap, the rect values overlap in them.
            return true;
        }

        



        #endregion
    }
}