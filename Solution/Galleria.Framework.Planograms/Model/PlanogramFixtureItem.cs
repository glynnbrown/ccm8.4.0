﻿#region Header Information

// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26760 : L.Ineson
//  Populated property display types
// V8-26986 : A.Kuszyk
//  Added Get Blocking methods.
// V8-27058 : A.Probyn
//  Added new meta data properties & calculations
//  Added ClearMetadata
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
// V8-27510 : A.Kuszyk
//  Changed references to positions to use DesignViewHelper.
// V8-27477 : L.Ineson
//  Added more world space helper methods
// V8-27558 : L.Ineson
//  Removed IsBay

#endregion

#region Version History: CCM803

// V8-29291 : L.Ineson
//  Amended metadata methods so that items dont get calculated twice.
// V8-29369 : L.Luong
//  Modified Meta data calculation so they are correct
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single

#endregion

#region Version History: CCM810

//V8-28662 : L.Ineson
//  Updated EnumerateDisplayableFieldInfos
// V8-29777 : L.Ineson
//  Corrected white space properties display types
// V8-28382 : L.Ineson
// Updated planogram relative transform methods
// V8-30210 : M.Brumby
//  Case pack metadata was being converted to Int16 erroneously

#endregion

#region Version History: CCM811

// V8-30352 : A.Probyn
//  ~Updated OnCalculateMetadata so that the white space is calculated at a cumulative merchandising space level, 
//  not as a total for the planogram. Components which are overfaced were masking white space on other shelves.
// V8-30398 : A.Silva
//  Amended MetaTotalFacings to use PlanogramHelper.CalculateMetaTotalFacings (that accounts for tray real facings).

#endregion

#region Version History: CCM820
// V8-31222 : L.Ineson
//  Removed yield of unpopulated metadata properties from EnumerateDisplayableFieldInfos
//  DOS values now get correctly populated.
#endregion

#region Version History: CCM830
//V8-32563 : L.Ineson
//  Added move component and move assembly helper methods.
//V8-32563 : M.Brumby
//  Moving linked annotations now checks the fixture component Id so unlinked annotations from the bay don't move as well.
//V8-32895 : M.Brumby
//  Moving assemblies no longer brings annotations along for the ride.
//V8-14035 : J.Pickup
//  MoveComponent() execution order has been changed as effects other logic adverseley.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a planogram fixture item
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramFixtureItem : ModelObject<PlanogramFixtureItem>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get
            {
                if (base.Parent == null) return null;
                return ((PlanogramFixtureItemList)base.Parent).Parent;
            }
        }
        #endregion

        #region Properties

        #region PlanogramFixtureId
        /// <summary>
        /// PlanogramFixtureId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureIdProperty =
            RegisterModelProperty<Object>(c => c.PlanogramFixtureId);
        /// <summary>
        /// The linked planogram fixture id
        /// </summary>
        public Object PlanogramFixtureId
        {
            get { return GetProperty<Object>(PlanogramFixtureIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureIdProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.PlanogramFixtureItem_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The x position
        /// </summary>
        public Single X
        {
            get { return this.GetProperty<Single>(XProperty); }
            set { this.SetProperty<Single>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.PlanogramFixtureItem_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The x position
        /// </summary>
        public Single Y
        {
            get { return this.GetProperty<Single>(YProperty); }
            set { this.SetProperty<Single>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.PlanogramFixtureItem_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The z position
        /// </summary>
        public Single Z
        {
            get { return this.GetProperty<Single>(ZProperty); }
            set { this.SetProperty<Single>(ZProperty, value); }
        }
        #endregion

        #region Slope
        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, Message.PlanogramFixtureItem_Slope, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The slope
        /// </summary>
        public Single Slope
        {
            get { return this.GetProperty<Single>(SlopeProperty); }
            set { this.SetProperty<Single>(SlopeProperty, value); }
        }
        #endregion

        #region Angle
        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle, Message.PlanogramFixtureItem_Angle, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The fixture item angle
        /// </summary>
        public Single Angle
        {
            get { return this.GetProperty<Single>(AngleProperty); }
            set { this.SetProperty<Single>(AngleProperty, value); }
        }
        #endregion

        #region Roll
        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, Message.PlanogramFixtureItem_Roll, ModelPropertyDisplayType.Angle);
        /// <summary>
        /// The fixture item roll
        /// </summary>
        public Single Roll
        {
            get { return this.GetProperty<Single>(RollProperty); }
            set { this.SetProperty<Single>(RollProperty, value); }
        }
        #endregion

        #region BaySequenceNumber
        /// <summary>
        /// Indicates the bay sequence number
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> BaySequenceNumberProperty =
            RegisterModelProperty<Int16>(c => c.BaySequenceNumber, Message.PlanogramFixtureItem_BaySequenceNumber);
        /// <summary>
        /// The order number for the bay
        /// </summary>
        public Int16 BaySequenceNumber
        {
            get { return this.GetProperty<Int16>(BaySequenceNumberProperty); }
            set { this.SetProperty<Int16>(BaySequenceNumberProperty, value); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaComponentCount
        /// <summary>
        /// MetaComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaComponentCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaComponentCount, Message.PlanogramFixtureItem_MetaComponentCount);
        /// <summary>
        /// Gets/Sets the meta data component count;
        /// </summary>
        public Int32? MetaComponentCount
        {
            get { return this.GetProperty<Int32?>(MetaComponentCountProperty); }
            set { this.SetProperty<Int32?>(MetaComponentCountProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace, Message.PlanogramFixtureItem_MetaTotalMerchandisableLinearSpace);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace, Message.PlanogramFixtureItem_MetaTotalMerchandisableAreaSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace, Message.PlanogramFixtureItem_MetaTotalMerchandisableVolumetricSpace);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace, Message.PlanogramFixtureItem_MetaTotalLinearWhiteSpace,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace, Message.PlanogramFixtureItem_MetaTotalAreaWhiteSpace,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace, Message.PlanogramFixtureItem_MetaTotalVolumetricWhiteSpace,
            ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
            set { this.SetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, value); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced, Message.PlanogramFixtureItem_MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
            set { this.SetProperty<Int32?>(MetaProductsPlacedProperty, value); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts, Message.PlanogramFixtureItem_MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
            set { this.SetProperty<Int32?>(MetaNewProductsProperty, value); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount, Message.PlanogramFixtureItem_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
            set { this.SetProperty<Int32?>(MetaChangesFromPreviousCountProperty, value); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating, Message.PlanogramFixtureItem_MetaChangeFromPreviousStarRating);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
            set { this.SetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, value); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped, Message.PlanogramFixtureItem_MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
            set { this.SetProperty<Int32?>(MetaBlocksDroppedProperty, value); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory, Message.PlanogramFixtureItem_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
            set { this.SetProperty<Int32?>(MetaNotAchievedInventoryProperty, value); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, Message.PlanogramFixtureItem_MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFacings
        /// <summary>
        /// MetaAverageFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaAverageFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaAverageFacings, Message.PlanogramFixtureItem_MetaAverageFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFacings
        /// </summary>
        public Int16? MetaAverageFacings
        {
            get { return this.GetProperty<Int16?>(MetaAverageFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaAverageFacingsProperty, value); }
        }
        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, Message.PlanogramFixtureItem_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalUnitsProperty, value); }
        }
        #endregion

        #region MetaAverageUnits
        /// <summary>
        /// MetaAverageUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaAverageUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaAverageUnits, Message.PlanogramFixtureItem_MetaAverageUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageUnits
        /// </summary>
        public Int32? MetaAverageUnits
        {
            get { return this.GetProperty<Int32?>(MetaAverageUnitsProperty); }
            set { this.SetProperty<Int32?>(MetaAverageUnitsProperty, value); }
        }
        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos, Message.PlanogramFixtureItem_MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
            set { this.SetProperty<Single?>(MetaMinDosProperty, value); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos, Message.PlanogramFixtureItem_MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
            set { this.SetProperty<Single?>(MetaMaxDosProperty, value); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos, Message.PlanogramFixtureItem_MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
            set { this.SetProperty<Single?>(MetaAverageDosProperty, value); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases, Message.PlanogramFixtureItem_MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
            set { this.SetProperty<Single?>(MetaMinCasesProperty, value); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases, Message.PlanogramFixtureItem_MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
            set { this.SetProperty<Single?>(MetaAverageCasesProperty, value); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases, Message.PlanogramFixtureItem_MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
            set { this.SetProperty<Single?>(MetaMaxCasesProperty, value); }
        }
        #endregion

        #region MetaSpaceToUnitsIndex
        /// <summary>
        /// MetaSpaceToUnitsIndex property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaSpaceToUnitsIndexProperty =
            RegisterModelProperty<Int16?>(c => c.MetaSpaceToUnitsIndex, Message.PlanogramFixtureItem_MetaSpaceToUnitsIndex);
        /// <summary>
        /// Gets/Sets the meta data MetaSpaceToUnitsIndex
        /// </summary>
        public Int16? MetaSpaceToUnitsIndex
        {
            get { return this.GetProperty<Int16?>(MetaSpaceToUnitsIndexProperty); }
            set { this.SetProperty<Int16?>(MetaSpaceToUnitsIndexProperty, value); }
        }
        #endregion

        #region MetaTotalComponentCollisions
        /// <summary>
        /// MetaTotalComponentCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentCollisions, Message.PlanogramFixtureItem_MetaTotalComponentCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentCollisions
        /// </summary>
        public Int32? MetaTotalComponentCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedDepth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedDepthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedDepth, Message.PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedDepth);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedDepth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedHeight
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedHeightProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedHeight, Message.PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedHeight);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedHeight
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty, value); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedWidth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedWidthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedWidth, Message.PlanogramFixtureItem_MetaTotalComponentsOverMerchandisedWidth);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedWidth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty); }
            set { this.SetProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty, value); }
        }
        #endregion

        #region MetaTotalPositionCollisions
        /// <summary>
        /// MetaTotalPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalPositionCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalPositionCollisions, Message.PlanogramFixtureItem_MetaTotalPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalPositionCollisions
        /// </summary>
        public Int32? MetaTotalPositionCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalPositionCollisionsProperty); }
            set { this.SetProperty<Int32?>(MetaTotalPositionCollisionsProperty, value); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, Message.PlanogramFixtureItem_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
            set { this.SetProperty<Int16?>(MetaTotalFrontFacingsProperty, value); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, Message.PlanogramFixtureItem_MetaAverageFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
            set { this.SetProperty<Single?>(MetaAverageFrontFacingsProperty, value); }
        }
        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Gets the X position of this item expressed as a percentage of the total width of
        /// all the fixture items in this Planogram.
        /// </summary>
        public Single PercentageX
        {
            get
            {
                if (Parent == null) throw new InvalidOperationException("Parent cannot be null");
                Single planogramWidth, planogramHeight;
                Parent.GetBlockingAreaSize(out planogramHeight, out planogramWidth);
                return new DesignViewPosition(this).BoundX / planogramWidth;
            }
        }

        /// <summary>
        /// Gets the width of this item expressed as a percentage of the total width of
        /// all the fixture items in this Planogram.
        /// </summary>
        public Single PercentageWidth
        {
            get
            {
                if (Parent == null) throw new InvalidOperationException("Parent cannot be null");
                Single planogramWidth, planogramHeight;
                Parent.GetBlockingAreaSize(out planogramHeight, out planogramWidth);
                return new DesignViewPosition(this).BoundWidth / planogramWidth;
            }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(PlanogramFixtureIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureItem NewPlanogramFixtureItem(PlanogramFixture fixture, Int16 baySequenceNumber)
        {
            PlanogramFixtureItem item = new PlanogramFixtureItem();
            item.Create(fixture, baySequenceNumber);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureItem NewPlanogramFixtureItem()
        {
            PlanogramFixtureItem item = new PlanogramFixtureItem();
            item.Create(null, 0);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureItem NewPlanogramFixtureItem(PlanogramFixture fixture)
        {
            PlanogramFixtureItem item = new PlanogramFixtureItem();
            item.Create(fixture, 0);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureItem NewPlanogramFixtureItem(Int16 baySequenceNumber)
        {
            PlanogramFixtureItem item = new PlanogramFixtureItem();
            item.Create(null, 0);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramFixture fixture, Int16 baySequenceNumber)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(PlanogramFixtureIdProperty, fixture == null ? null : fixture.Id);
            this.LoadProperty<Int16>(BaySequenceNumberProperty, baySequenceNumber);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramFixtureItem>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes for this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // PlanogramFixtureId
            Object planogramFixtureId = context.ResolveId<PlanogramFixture>(this.ReadProperty<Object>(PlanogramFixtureIdProperty));
            if (planogramFixtureId != null) this.LoadProperty<Object>(PlanogramFixtureIdProperty, planogramFixtureId);
        }

        /// <summary>
        /// Returns the linked PlanogramFixture model
        /// </summary>
        /// <returns></returns>
        public PlanogramFixture GetPlanogramFixture()
        {
            PlanogramFixture fixture = null;
            Object planogramFixtureId = this.PlanogramFixtureId;
            if (planogramFixtureId != null)
            {
                Planogram planogram = this.Parent;
                if (planogram != null)
                {
                    fixture = planogram.Fixtures.FindById(planogramFixtureId);
                }
            }
            return fixture;
        }

        /// <summary>
        /// Adds an annotation to this fixture item
        /// </summary>
        public PlanogramAnnotation AddAnnotation(IPlanogramSettings settings)
        {
            return this.Parent.Annotations.Add(this, settings);
        }

        /// <summary>
        /// Enumerates through all annotations which are linked directly to this fixture.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramAnnotation> EnumerateLinkedFixtureAnnotations()
        {
            Planogram plan = this.Parent;
            if (plan == null) yield break;

            foreach(var anno in plan.Annotations)
            {
                if (anno.IsAssociated(this))
                    yield return anno;
            }
        }

        #region Placement Related

        /// <summary>
        /// Returns the matrix tranform for this component relative to the planogram.
        /// This will always include local.
        /// </summary>
        private MatrixValue GetPlanogramRelativeTransform()
        {
            MatrixValue relativeTransform = MatrixValue.Identity;

            relativeTransform.Append(
                MatrixValue.CreateTransformationMatrix(
                    this.X, this.Y, this.Z,
                    this.Angle, this.Slope, this.Roll));

            return relativeTransform;
        }

        /// <summary>
        /// Converts the given local point to a planogram relative one.
        /// </summary>
        public static PointValue ToWorld(PointValue localPoint, PlanogramFixtureItem fixtureItem)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureItem.GetPlanogramRelativeTransform();

            //trasnform and return.
            PointValue worldPoint = localPoint.Transform(relativeTransform);
            return worldPoint;
        }

        /// <summary>
        /// Converts the given world point to one which is local
        /// to this subcomponent placement.
        /// </summary>
        public static PointValue ToLocal(PointValue worldPoint, PlanogramFixtureItem fixtureItem)
        {
            //get the relative transform
            MatrixValue relativeTransform = fixtureItem.GetPlanogramRelativeTransform();

            //invert the transform.
            relativeTransform.Invert();

            return worldPoint.Transform(relativeTransform);
        }

        /// <summary>
        /// Returns the bounding box for this component
        /// relative to the planogram.
        /// </summary>
        /// <returns></returns>
        public RectValue GetPlanogramRelativeBoundingBox(PlanogramFixture fixture)
        {
            RectValue bounds = new RectValue();
            bounds.Width = fixture.Width;
            bounds.Height = fixture.Height;
            bounds.Depth = fixture.Depth;

            //create the relative transform.
            MatrixValue relativeTransform = GetPlanogramRelativeTransform();

            return relativeTransform.TransformBounds(bounds);
        }

        #endregion

        /// <summary>
        /// Returns all sub component placements associated
        /// to this placement fixture item
        /// </summary>
        public IEnumerable<PlanogramSubComponentPlacement> GetPlanogramSubComponentPlacements()
        {
            Planogram planogram = this.Parent;
            if (planogram == null) yield break;
            foreach (PlanogramSubComponentPlacement subComponentPlacement in planogram.GetPlanogramSubComponentPlacements().ToArray())
            {
                if (this.Equals(subComponentPlacement.FixtureItem))
                {
                    yield return subComponentPlacement;
                }
            }
        }

        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean includeMetadata)
        {
            Type type = typeof(PlanogramFixture);
            String typeFriendly = PlanogramFixture.FriendlyName;

            String detailsGroup = Message.PlanogramFixture_DetailsFieldGroup;
            String positionGroup = Message.PlanogramFixture_PositionFieldGroup;
            String metaDataGroup = Message.PlanogramFixture_MetaDataFieldGroup;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, AngleProperty, positionGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, BaySequenceNumberProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, positionGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, positionGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, positionGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SlopeProperty, positionGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RollProperty, positionGroup);


            //Metadata fields
            if (includeMetadata)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageCasesProperty, metaDataGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageDosProperty, metaDataGroup);//not populated.
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFacingsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageFrontFacingsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaAverageUnitsProperty, metaDataGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaBlocksDroppedProperty, metaDataGroup);//not populated.
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangeFromPreviousStarRatingProperty, metaDataGroup);//not populated.
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaChangesFromPreviousCountProperty, metaDataGroup);//not populated.
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaComponentCountProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxCasesProperty, metaDataGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMaxDosProperty, metaDataGroup);//not populated.
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinCasesProperty, metaDataGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaMinDosProperty, metaDataGroup);//not populated.
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNewProductsProperty, metaDataGroup); //not populated.
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaNotAchievedInventoryProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaProductsPlacedProperty, metaDataGroup);
                //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaSpaceToUnitsIndexProperty, metaDataGroup);//not populated.
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalAreaWhiteSpaceProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentCollisionsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedDepthProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedHeightProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalComponentsOverMerchandisedWidthProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFacingsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalFrontFacingsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalLinearWhiteSpaceProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableAreaSpaceProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableLinearSpaceProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalMerchandisableVolumetricSpaceProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalPositionCollisionsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalUnitsProperty, metaDataGroup);
                yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MetaTotalVolumetricWhiteSpaceProperty, metaDataGroup);
            }
        }

        /// <summary>
        /// Returns the value of the given field for this .
        /// </summary>
        public Object GetFieldValue(ObjectFieldInfo field)
        {
            return field.GetValue(this);
        }

        #endregion

        #region Metadata

        /// <summary>
        /// Called when calculating metadata for this instance
        /// </summary>
        protected override void OnCalculateMetadata()
        {
            Planogram parentPlan = this.Parent;
            if (parentPlan == null) return;

            PlanogramPositionList positionList = parentPlan.Positions;
            if (positionList == null) return;

            PlanogramMetadataDetails metadataDetails = parentPlan.GetPlanogramMetadataDetails();

            //Get fixture
            PlanogramFixture fixture = this.Parent.Fixtures.FindById(this.PlanogramFixtureId);
            if (fixture == null)
            {
                OnClearMetadata();
                return;
            }


            //Get all positions assigned to this fixture item
            List<PlanogramPosition> positions = positionList.Where(p => p.PlanogramFixtureItemId.Equals(this.Id)).ToList();

            //Get all products assigned to the positions
            List<PlanogramProduct> products = new List<PlanogramProduct>();
            List<PlanogramPositionDetails> fixtureItemPositionDetails = new List<PlanogramPositionDetails>();
            List<PlanogramPerformanceData> performanceData =new List<PlanogramPerformanceData>();
            foreach (PlanogramPosition position in positions)
            {
                PlanogramPositionDetails positionDetails;
                if (metadataDetails.PositionDetails.TryGetValue(position.Id, out positionDetails))
                {
                    fixtureItemPositionDetails.Add(positionDetails);
                }

                PlanogramProduct product = this.Parent.Products.FindById(position.PlanogramProductId);
                if (product != null && !products.Contains(product))
                {
                    products.Add(product);

                    PlanogramPerformanceData performanceDataItem = metadataDetails.FindPerformanceDataByPlanogramProductId(product.Id);
                    if(performanceDataItem != null) performanceData.Add(performanceDataItem);
                }
            }


            //Get fixture components
            PlanogramFixtureComponentList fixtureComponents = fixture.Components;

            //Get all merchandising space for this fixture.
            IEnumerable<PlanogramMerchandisingSpace> merchandisingSpaces = metadataDetails.GetFixtureItemMerchandisingSpace(this);
            this.MetaTotalMerchandisableLinearSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableLinear()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableAreaSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableArea()), 2, MidpointRounding.AwayFromZero);
            this.MetaTotalMerchandisableVolumetricSpace = (Single)Math.Round(merchandisingSpaces.Sum(s => s.GetMerchandisableVolume()), 2, MidpointRounding.AwayFromZero);

            //V830352 - Calculate the white space at a cumulative merchandising space level, not as a total for the planogram.
            // Components which are overfaced mask white space on other shelves.
            Single totalPlanLinearWhiteSpace = 0, totalPlanAreaWhiteSpace = 0, totalPlanVolumetricWhiteSpace = 0;
            Int32 numberOfMerchSpaces = merchandisingSpaces.Count();
            foreach (PlanogramMerchandisingSpace merchSpace in merchandisingSpaces)
            {
                if (merchSpace.SubComponentPlacement != null)
                {
                    //Get all positions in this merch space
                    IEnumerable<PlanogramPosition> merchSpacePositions = merchSpace.SubComponentPlacement.GetPlanogramPositions();

                    //Get position details
                    List<PlanogramPositionDetails> merchSpacePositionDetails = new List<PlanogramPositionDetails>();
                    foreach (PlanogramPosition pos in merchSpacePositions)
                    {
                        PlanogramPositionDetails posDetails = null;
                        if (metadataDetails.PositionDetails.TryGetValue(pos.Id, out posDetails))
                        {
                            merchSpacePositionDetails.Add(posDetails);
                        }
                    }

                    // The total width of white space. That is, the difference between the sum of the widths
                    // of all the positions at the front of the plan and the total width of merchandising space of
                    // all of the subcomponents.
                    totalPlanLinearWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableLinear(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width)).Value / numberOfMerchSpaces);

                    // The total area of white space. That is, the difference between the sum of all the positions
                    // that are at the front of the plan and the sum of the merchandisable areas of all the subcomponents.
                    totalPlanAreaWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableArea(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height)).Value / numberOfMerchSpaces);

                    // The total volume of white space. That is, the difference between the sum of the volumes 
                    // of all the positions and the sum of the merchandisable volumes of all the subcomponents.
                    totalPlanVolumetricWhiteSpace += (PlanogramHelper.CalculateMetaTotalWhiteSpace(
                        (Single?)merchSpace.GetMerchandisableVolume(),
                        merchSpacePositionDetails.Sum(p => p.TotalSize.Width * p.TotalSize.Height * p.TotalSize.Depth)).Value / numberOfMerchSpaces);
                }
            }

            //Set cumulative values to be totals
            this.MetaTotalLinearWhiteSpace = (Single?)Math.Round(totalPlanLinearWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalAreaWhiteSpace = (Single?)Math.Round(totalPlanAreaWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaTotalVolumetricWhiteSpace = (Single?)Math.Round(totalPlanVolumetricWhiteSpace, 2, MidpointRounding.AwayFromZero);
            this.MetaComponentCount = fixtureComponents.Count;
            this.MetaProductsPlaced = products.Count;
            this.MetaNotAchievedInventory = products.Any() ? products.Count(p => p.MetaNotAchievedInventory == true) : (Int16)0;
            this.MetaTotalFacings = PlanogramHelper.CalculateMetaTotalFacings(positions, products);
            this.MetaAverageFacings = Convert.ToInt16(positions.Count > 0 ? MetaTotalFacings / positions.Count : 0);
            this.MetaTotalUnits = positions.Sum(p => p.TotalUnits);
            this.MetaAverageUnits = positions.Any() ? Convert.ToInt32(positions.Average(p => p.TotalUnits)) : (Int32)0;
            this.MetaMinCases = positions.Any() ? positions.Min(p => p.MetaAchievedCases) : 0;
            this.MetaAverageCases = positions.Any() ? positions.Average(p => p.MetaAchievedCases) : 0;
            this.MetaMaxCases = positions.Any() ? positions.Max(p => p.MetaAchievedCases) : 0;
            this.MetaTotalComponentsOverMerchandisedWidth = fixtureComponents.Count(p => p.MetaIsOverMerchandisedWidth == true);
            this.MetaTotalComponentsOverMerchandisedHeight = fixtureComponents.Count(p => p.MetaIsOverMerchandisedHeight == true);
            this.MetaTotalComponentsOverMerchandisedDepth = fixtureComponents.Count(p => p.MetaIsOverMerchandisedDepth == true);
            this.MetaTotalFrontFacings = Convert.ToInt16(positions.Any() ? positions.Sum(p => p.MetaFrontFacingsWide) : 0);
            this.MetaAverageFrontFacings = Convert.ToSingle(positions.Any() ? Math.Round(((double)this.MetaTotalFrontFacings / positions.Count), 2, MidpointRounding.AwayFromZero) : 0);
            this.MetaMinDos = performanceData.Min(p => p.AchievedDos) ?? 0;
            this.MetaMaxDos = performanceData.Max(p => p.AchievedDos) ?? 0;
            this.MetaAverageDos = performanceData.Average(p => p.AchievedDos) ?? 0;

            //Count all of the positions relating to this planogram fixture item that have a collision
            this.MetaTotalPositionCollisions = parentPlan.Positions.Count(p => Object.Equals(p.PlanogramFixtureItemId, this.Id) && p.MetaIsPositionCollisions == true);

            //Count all components in this fixtures collisions
            this.MetaTotalComponentCollisions = fixtureComponents.Sum(p => p.MetaTotalComponentCollisions);

            //TODO:
            this.MetaNewProducts = 0; //TODO
            this.MetaChangesFromPreviousCount = 0; //TODO
            this.MetaChangeFromPreviousStarRating = 0; //TODO
            this.MetaBlocksDropped = 0; //TODO
            this.MetaSpaceToUnitsIndex = 0; //TODO
        }

        /// <summary>
        /// Called when clearing metadata for this instance
        /// </summary>
        protected override void OnClearMetadata()
        {
            this.MetaComponentCount = null;
            this.MetaTotalMerchandisableLinearSpace = null;
            this.MetaTotalMerchandisableAreaSpace = null;
            this.MetaTotalMerchandisableVolumetricSpace = null;
            this.MetaTotalLinearWhiteSpace = null;
            this.MetaTotalAreaWhiteSpace = null;
            this.MetaTotalVolumetricWhiteSpace = null;
            this.MetaProductsPlaced = null;
            this.MetaNewProducts = null;
            this.MetaChangesFromPreviousCount = null;
            this.MetaChangeFromPreviousStarRating = null;
            this.MetaBlocksDropped = null;
            this.MetaNotAchievedInventory = null;
            this.MetaTotalFacings = null;
            this.MetaAverageFacings = null;
            this.MetaTotalUnits = null;
            this.MetaAverageUnits = null;
            this.MetaMinDos = null;
            this.MetaMaxDos = null;
            this.MetaAverageDos = null;
            this.MetaMinCases = null;
            this.MetaAverageCases = null;
            this.MetaMaxCases = null;
            this.MetaSpaceToUnitsIndex = null;
            this.MetaTotalComponentsOverMerchandisedWidth = null;
            this.MetaTotalComponentsOverMerchandisedHeight = null;
            this.MetaTotalComponentsOverMerchandisedDepth = null;
            this.MetaTotalComponentCollisions = null;
            this.MetaTotalPositionCollisions = null;
            this.MetaTotalFrontFacings = null;
            this.MetaAverageFrontFacings = null;
        }
        #endregion


        /// <summary>
        /// Moves the given fixture component to this fixture item, relinking all of its associated child positions and annotations.
        /// </summary>
        public PlanogramFixtureComponent MoveComponent(PlanogramFixtureComponent fixtureComponent, PlanogramFixtureItem oldFixtureItem)
        {
            if (oldFixtureItem == this) return fixtureComponent;
            if (oldFixtureItem.Parent != this.Parent) throw new ArgumentException("Should be from the same plan");


            Planogram parentPlan = this.Parent;
            PlanogramFixture oldFixture = oldFixtureItem.GetPlanogramFixture();
            PlanogramFixture newFixture = this.GetPlanogramFixture();
            PlanogramComponent planogramComponent = fixtureComponent.GetPlanogramComponent();

            //check the move arguments are valid.
            if (oldFixture == null || newFixture == null || planogramComponent == null
                || !oldFixture.Components.Contains(fixtureComponent)
                || newFixture.Components.Contains(fixtureComponent))
                return fixtureComponent;


            List<PlanogramAnnotation> anotationsToMove =
                parentPlan.Annotations.Where(a => 
                    Object.Equals(a.PlanogramFixtureComponentId, fixtureComponent.Id) &&
                    Object.Equals(a.PlanogramFixtureItemId, oldFixtureItem.Id)).ToList();

            List<PlanogramPosition> positionsToMove = fixtureComponent.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions()).ToList();

            PointValue oldWorldCoordinates = fixtureComponent.GetPlanogramRelativeCoordinates(oldFixtureItem);

            //move all annotations
            foreach (PlanogramAnnotation annotation in anotationsToMove)
            {
                annotation.PlanogramFixtureItemId = this.Id;
            }

            //move all positions
            foreach (PlanogramPosition position in positionsToMove)
            {
                position.PlanogramFixtureItemId = this.Id;
            }

            //CCM-14035: Move all the items before triggering the coord move as otherwise results in empty merch groups when processed in particular order.
            //move the fixture component & correct its coordinates
            PlanogramFixtureComponent newFixtureComponent = fixtureComponent.Move<PlanogramFixtureComponentList>(oldFixture.Components, newFixture.Components);
            newFixtureComponent.SetCoordinatesFromPlanogramRelative(oldWorldCoordinates, this);


            return newFixtureComponent;

        }

        /// <summary>
        /// Moves the given assembly component to this fixture item, removing it from its original assembly 
        /// and relinking all of its associated child positions and annotations.
        /// </summary>
        public PlanogramFixtureComponent MoveComponent(PlanogramAssemblyComponent assemblyComponent,  
            PlanogramFixtureAssembly oldFixtureAssembly, PlanogramFixtureItem oldFixtureItem)
        {
            Planogram parentPlan = this.Parent;
            PlanogramAssembly oldAssembly = assemblyComponent.Parent;
            PlanogramComponent planogramComponent = assemblyComponent.GetPlanogramComponent();
            PlanogramFixture fixture = this.GetPlanogramFixture();

            List<PlanogramAnnotation> anotationsToMove =
                parentPlan.Annotations.Where(a => Object.Equals(a.PlanogramAssemblyComponentId, assemblyComponent.Id)).ToList();

            List<PlanogramPosition> positionsToMove = assemblyComponent.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions()).ToList();

            PointValue oldWorldCoordinates = assemblyComponent.GetPlanogramRelativeCoordinates(oldFixtureItem, oldFixtureAssembly);

            //create the new fixture component & correct its coordinates
            PlanogramFixtureComponent newFixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogramComponent);
            fixture.Components.Add(newFixtureComponent);
            newFixtureComponent.SetCoordinatesFromPlanogramRelative(oldWorldCoordinates, this);
  

            //move all annotations
            foreach (PlanogramAnnotation annotation in anotationsToMove)
            {
                annotation.PlanogramFixtureItemId = this.Id;
                annotation.PlanogramFixtureAssemblyId = null;
                annotation.PlanogramAssemblyComponentId = null;
            }

            //move all positions
            foreach (PlanogramPosition position in positionsToMove)
            {
                position.PlanogramFixtureItemId = this.Id;
                position.PlanogramFixtureAssemblyId = null;
                position.PlanogramAssemblyComponentId = null;
            }

            //remove the original assembly component 
            oldAssembly.Components.Remove(assemblyComponent);

            //remove the assembly completely if this was the last component.
            if (!oldAssembly.Components.Any())
            {
                parentPlan.Assemblies.RemoveAssemblyAndDependants(oldAssembly);
            }
            

            return newFixtureComponent;

        }

        /// <summary>
        /// Moves the given fixture assembly to this fixture item, relinking all of its associated child positions and annotations.
        /// </summary>
        public PlanogramFixtureAssembly MoveAssembly(PlanogramFixtureAssembly fixtureAssembly, PlanogramFixtureItem oldFixtureItem)
        {
            if (oldFixtureItem == this) return fixtureAssembly;
            if (oldFixtureItem.Parent != this.Parent) throw new ArgumentException("Should be from the same plan");


            Planogram parentPlan = this.Parent;
            PlanogramFixture oldFixture = oldFixtureItem.GetPlanogramFixture();
            PlanogramFixture newFixture = this.GetPlanogramFixture();
            PlanogramAssembly planogramAssembly = fixtureAssembly.GetPlanogramAssembly();

            //check the move arguments are valid.
            if (oldFixture == null || newFixture == null || planogramAssembly == null
                || !oldFixture.Assemblies.Contains(fixtureAssembly)
                || newFixture.Assemblies.Contains(fixtureAssembly))
                return fixtureAssembly;


            List<PlanogramAnnotation> anotationsToMove =
                parentPlan.Annotations.Where(a =>
                    Object.Equals(a.PlanogramFixtureAssemblyId, fixtureAssembly.Id) &&
                    Object.Equals(a.PlanogramFixtureItemId, oldFixtureItem.Id)).ToList();

            List<PlanogramPosition> positionsToMove = planogramAssembly.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions()).ToList();

            PointValue oldWorldCoordinates = fixtureAssembly.GetPlanogramRelativeCoordinates(oldFixtureItem);

            //move the fixture component & correct its coordinates
            PlanogramFixtureAssembly newFixtureAssembly = fixtureAssembly.Move<PlanogramFixtureAssemblyList>(oldFixture.Assemblies, newFixture.Assemblies);
            newFixtureAssembly.SetCoordinatesFromPlanogramRelative(oldWorldCoordinates, this);

            //move all annotations
            foreach (PlanogramAnnotation annotation in anotationsToMove)
            {
                annotation.PlanogramFixtureItemId = this.Id;
            }

            //move all positions
            foreach (PlanogramPosition position in positionsToMove)
            {
                position.PlanogramFixtureItemId = this.Id;
            }

            return newFixtureAssembly;
        }

        /// <summary>
        /// Moves the annotation to this fixture item
        /// </summary>
        /// <param name="annotation"></param>
        /// <param name="oldFixtureItem"></param>
        /// <returns></returns>
        public void MoveAnnotation(PlanogramAnnotation annotation, PlanogramFixtureItem oldFixtureItem)
        {
            //do some quick checks to make sure that the move is valid.
            if (oldFixtureItem == this) return;
            if (oldFixtureItem == null || !annotation.IsAssociated(oldFixtureItem))
            {
                Debug.Fail("Incorrect info.");
                return;
            }

            //reassociate the annotation to this fixture
            annotation.Associate(this);
        }

        #endregion
    }
}