﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM810
// V8-29237 : A.Probyn
//  Extended for new Inheritance and distribution enum types
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductTreatmentType
    {
        Normal = 0,
        Core = 1,
        Optional = 2,
        Distribution = 3,
        Inheritance = 4
    }

    public static class PlanogramAssortmentProductTreatmentTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductTreatmentType, string> FriendlyNames =
            new Dictionary<PlanogramAssortmentProductTreatmentType, string>()
            {
                {PlanogramAssortmentProductTreatmentType.Normal, Message.Enum_PlanogramAssortmentProductTreatementType_Normal},
                {PlanogramAssortmentProductTreatmentType.Core, Message.Enum_PlanogramAssortmentProductTreatmentType_Core},
                {PlanogramAssortmentProductTreatmentType.Optional, Message.Enum_PlanogramAssortmentProductTreatmentType_Optional },
                {PlanogramAssortmentProductTreatmentType.Distribution, Message.Enum_PlanogramAssortmentProductTreatmentType_Distribution },
                {PlanogramAssortmentProductTreatmentType.Inheritance, Message.Enum_PlanogramAssortmentProductTreatmentType_Inheritance }
            };


        /// <summary>
        /// Dictionary with Friendly Name as key and ProductTreatmentType as value
        /// </summary>
        public static readonly Dictionary<String, PlanogramAssortmentProductTreatmentType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramAssortmentProductTreatmentType>()
                {
                    {Message.Enum_PlanogramAssortmentProductTreatementType_Normal.ToLowerInvariant(), PlanogramAssortmentProductTreatmentType.Normal},
                    {Message.Enum_PlanogramAssortmentProductTreatmentType_Core.ToLowerInvariant(), PlanogramAssortmentProductTreatmentType.Core},
                    {Message.Enum_PlanogramAssortmentProductTreatmentType_Optional.ToLowerInvariant(), PlanogramAssortmentProductTreatmentType.Optional},
                    {Message.Enum_PlanogramAssortmentProductTreatmentType_Distribution.ToLowerInvariant(), PlanogramAssortmentProductTreatmentType.Distribution},
                    {Message.Enum_PlanogramAssortmentProductTreatmentType_Inheritance.ToLowerInvariant(), PlanogramAssortmentProductTreatmentType.Inheritance}
                };
    }
}
