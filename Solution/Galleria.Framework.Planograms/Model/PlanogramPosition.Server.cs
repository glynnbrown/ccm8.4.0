﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24971 : L.Hodson
//  Changes to structure.
// V8-25543 : L.Ineson
//  Changed all orientation type fields to use PlanogramPositionOrientationType
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27474 : A.Silva
//  Added PositionSequenceNumber.
// V8-27823 : L.Ineson
//  Facings properties are now Int16
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-29028 : L.Ineson
//  Added position squeeze properties
// V8-29054 : M.Pettit
//  Added Meta validation properties
#endregion
#region Version History: CCM810
// V8-29844 : L.Ineson
//  Added more metadata
// V8-30011 : L.Ineson
//  Added addition linear, area and volumetric metadata.
#endregion
#region Version History : CCM 820
// V8-31164 : D.Pleasance
//  Added SequenceColour \ SequenceNumber
#endregion
#region Version History : CCM830
// V8-32884 : A.Kuszyk
//  Added MetaSequenceSubGroupName.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramPosition
    {
        #region Constructor
        private PlanogramPosition() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramPosition Fetch(IDalContext dalContext, PlanogramPositionDto dto)
        {
            return DataPortal.FetchChild<PlanogramPosition>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramPositionDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(PlanogramFixtureItemIdProperty, dto.PlanogramFixtureItemId);
            this.LoadProperty<Object>(PlanogramFixtureAssemblyIdProperty, dto.PlanogramFixtureAssemblyId);
            this.LoadProperty<Object>(PlanogramAssemblyComponentIdProperty, dto.PlanogramAssemblyComponentId);
            this.LoadProperty<Object>(PlanogramFixtureComponentIdProperty, dto.PlanogramFixtureComponentId);
            this.LoadProperty<Object>(PlanogramSubComponentIdProperty, dto.PlanogramSubComponentId);
            this.LoadProperty<Object>(PlanogramProductIdProperty, dto.PlanogramProductId);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(ZProperty, dto.Z);
            this.LoadProperty<Single>(SlopeProperty, dto.Slope);
            this.LoadProperty<Single>(AngleProperty, dto.Angle);
            this.LoadProperty<Single>(RollProperty, dto.Roll);
            this.LoadProperty<Int16>(FacingsHighProperty, dto.FacingsHigh);
            this.LoadProperty<Int16>(FacingsWideProperty, dto.FacingsWide);
            this.LoadProperty<Int16>(FacingsDeepProperty, dto.FacingsDeep);
            this.LoadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleProperty, (PlanogramPositionMerchandisingStyle)dto.MerchandisingStyle);
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeProperty, (PlanogramPositionOrientationType)dto.OrientationType);
            this.LoadProperty<Int16>(FacingsXHighProperty, dto.FacingsXHigh);
            this.LoadProperty<Int16>(FacingsXWideProperty, dto.FacingsXWide);
            this.LoadProperty<Int16>(FacingsXDeepProperty, dto.FacingsXDeep);
            this.LoadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleXProperty, (PlanogramPositionMerchandisingStyle)dto.MerchandisingStyleX);
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeXProperty, (PlanogramPositionOrientationType)dto.OrientationTypeX);
            this.LoadProperty<Boolean>(IsXPlacedLeftProperty, dto.IsXPlacedLeft);
            this.LoadProperty<Int16>(FacingsYHighProperty, dto.FacingsYHigh);
            this.LoadProperty<Int16>(FacingsYWideProperty, dto.FacingsYWide);
            this.LoadProperty<Int16>(FacingsYDeepProperty, dto.FacingsYDeep);
            this.LoadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleYProperty, (PlanogramPositionMerchandisingStyle)dto.MerchandisingStyleY);
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeYProperty, (PlanogramPositionOrientationType)dto.OrientationTypeY);
            this.LoadProperty<Boolean>(IsYPlacedBottomProperty, dto.IsYPlacedBottom);
            this.LoadProperty<Int16>(FacingsZHighProperty, dto.FacingsZHigh);
            this.LoadProperty<Int16>(FacingsZWideProperty, dto.FacingsZWide);
            this.LoadProperty<Int16>(FacingsZDeepProperty, dto.FacingsZDeep);
            this.LoadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleZProperty, (PlanogramPositionMerchandisingStyle)dto.MerchandisingStyleZ);
            this.LoadProperty<PlanogramPositionOrientationType>(OrientationTypeZProperty, (PlanogramPositionOrientationType)dto.OrientationTypeZ);
            this.LoadProperty<Boolean>(IsZPlacedFrontProperty, dto.IsZPlacedFront);
            this.LoadProperty<Int16>(SequenceProperty, dto.Sequence);
            this.LoadProperty<Int16>(SequenceXProperty, dto.SequenceX);
            this.LoadProperty<Int16>(SequenceYProperty, dto.SequenceY);
            this.LoadProperty<Int16>(SequenceZProperty, dto.SequenceZ);
            this.LoadProperty<Int16>(UnitsHighProperty, dto.UnitsHigh);
            this.LoadProperty<Int16>(UnitsWideProperty, dto.UnitsWide);
            this.LoadProperty<Int16>(UnitsDeepProperty, dto.UnitsDeep);
            this.LoadProperty<Int32>(TotalUnitsProperty, dto.TotalUnits);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Single?>(MetaAchievedCasesProperty, dto.MetaAchievedCases);
            this.LoadProperty<Boolean?>(MetaIsPositionCollisionsProperty, dto.MetaIsPositionCollisions);
            this.LoadProperty<Int16?>(MetaFrontFacingsWideProperty, dto.MetaFrontFacingsWide);
            this.LoadProperty<Int16?>(PositionSequenceNumberProperty, dto.PositionSequenceNumber);
            this.LoadProperty<Single>(HorizontalSqueezeProperty, dto.HorizontalSqueeze);
            this.LoadProperty<Single>(VerticalSqueezeProperty, dto.VerticalSqueeze);
            this.LoadProperty<Single>(DepthSqueezeProperty, dto.DepthSqueeze);
            this.LoadProperty<Single>(HorizontalSqueezeXProperty, dto.HorizontalSqueezeX);
            this.LoadProperty<Single>(VerticalSqueezeXProperty, dto.VerticalSqueezeX);
            this.LoadProperty<Single>(DepthSqueezeXProperty, dto.DepthSqueezeX);
            this.LoadProperty<Single>(HorizontalSqueezeYProperty, dto.HorizontalSqueezeY);
            this.LoadProperty<Single>(VerticalSqueezeYProperty, dto.VerticalSqueezeY);
            this.LoadProperty<Single>(DepthSqueezeYProperty, dto.DepthSqueezeY);
            this.LoadProperty<Single>(HorizontalSqueezeZProperty, dto.HorizontalSqueezeZ);
            this.LoadProperty<Single>(VerticalSqueezeZProperty, dto.VerticalSqueezeZ);
            this.LoadProperty<Single>(DepthSqueezeZProperty, dto.DepthSqueezeZ);
            this.LoadProperty<Boolean?>(MetaIsUnderMinDeepProperty, dto.MetaIsUnderMinDeep);
            this.LoadProperty<Boolean?>(MetaIsOverMaxDeepProperty, dto.MetaIsOverMaxDeep);
            this.LoadProperty<Boolean?>(MetaIsOverMaxRightCapProperty, dto.MetaIsOverMaxRightCap);
            this.LoadProperty<Boolean?>(MetaIsOverMaxStackProperty, dto.MetaIsOverMaxStack);
            this.LoadProperty<Boolean?>(MetaIsOverMaxTopCapProperty, dto.MetaIsOverMaxTopCap);
            this.LoadProperty<Boolean?>(MetaIsInvalidMerchandisingStyleProperty, dto.MetaIsInvalidMerchandisingStyle);
            this.LoadProperty<Boolean?>(MetaIsHangingTrayProperty, dto.MetaIsHangingTray);
            this.LoadProperty<Boolean?>(MetaIsPegOverfilledProperty, dto.MetaIsPegOverfilled);
            this.LoadProperty<Boolean?>(MetaIsOutsideMerchandisingSpaceProperty, dto.MetaIsOutsideMerchandisingSpace);
            this.LoadProperty<Boolean>(IsManuallyPlacedProperty, dto.IsManuallyPlaced);
            this.LoadProperty<Boolean?>(MetaIsOutsideOfBlockSpaceProperty, dto.MetaIsOutsideOfBlockSpace);
            this.LoadProperty<Single?>(MetaHeightProperty, dto.MetaHeight);
            this.LoadProperty<Single?>(MetaWidthProperty, dto.MetaWidth);
            this.LoadProperty<Single?>(MetaDepthProperty, dto.MetaDepth);
            this.LoadProperty<Single?>(MetaWorldXProperty, dto.MetaWorldX);
            this.LoadProperty<Single?>(MetaWorldYProperty, dto.MetaWorldY);
            this.LoadProperty<Single?>(MetaWorldZProperty, dto.MetaWorldZ);
            this.LoadProperty<Single?>(MetaTotalLinearSpaceProperty, dto.MetaTotalLinearSpace);
            this.LoadProperty<Single?>(MetaTotalAreaSpaceProperty, dto.MetaTotalAreaSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricSpaceProperty, dto.MetaTotalVolumetricSpace);
            this.LoadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty, dto.MetaPlanogramLinearSpacePercentage);
            this.LoadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty, dto.MetaPlanogramAreaSpacePercentage);
            this.LoadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty, dto.MetaPlanogramVolumetricSpacePercentage);
            this.LoadProperty<Int32?>(SequenceNumberProperty, dto.SequenceNumber);
            this.LoadProperty<Int32?>(SequenceColourProperty, dto.SequenceColour);
            this.LoadProperty<PlanogramItemComparisonStatusType>(MetaComparisonStatusProperty, (PlanogramItemComparisonStatusType) dto.MetaComparisonStatus);
            this.LoadProperty<String>(MetaSequenceSubGroupNameProperty, dto.MetaSequenceSubGroupName);
            this.LoadProperty<String>(MetaSequenceGroupNameProperty, dto.MetaSequenceGroupName);
            this.LoadProperty<Int32?>(MetaPegRowNumberProperty, dto.MetaPegRowNumber);
            this.LoadProperty<Int32?>(MetaPegColumnNumberProperty, dto.MetaPegColumnNumber);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramPositionDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramPositionDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent == null ? null : parent.Id,
                PlanogramFixtureItemId = this.ReadProperty<Object>(PlanogramFixtureItemIdProperty),
                PlanogramFixtureAssemblyId = this.ReadProperty<Object>(PlanogramFixtureAssemblyIdProperty),
                PlanogramAssemblyComponentId = this.ReadProperty<Object>(PlanogramAssemblyComponentIdProperty),
                PlanogramFixtureComponentId = this.ReadProperty<Object>(PlanogramFixtureComponentIdProperty),
                PlanogramSubComponentId = this.ReadProperty<Object>(PlanogramSubComponentIdProperty),
                PlanogramProductId = this.ReadProperty<Object>(PlanogramProductIdProperty), 
                X = this.ReadProperty<Single>(XProperty),
                Y = this.ReadProperty<Single>(YProperty),
                Z = this.ReadProperty<Single>(ZProperty),
                Slope = this.ReadProperty<Single>(SlopeProperty),
                Angle = this.ReadProperty<Single>(AngleProperty),
                Roll = this.ReadProperty<Single>(RollProperty),
                FacingsHigh = this.ReadProperty<Int16>(FacingsHighProperty),
                FacingsWide = this.ReadProperty<Int16>(FacingsWideProperty),
                FacingsDeep = this.ReadProperty<Int16>(FacingsDeepProperty),
                OrientationType = (Byte)this.ReadProperty<PlanogramPositionOrientationType>(OrientationTypeProperty),
                MerchandisingStyle = (Byte)this.ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleProperty),
                FacingsXHigh = this.ReadProperty<Int16>(FacingsXHighProperty),
                FacingsXWide = this.ReadProperty<Int16>(FacingsXWideProperty),
                FacingsXDeep = this.ReadProperty<Int16>(FacingsXDeepProperty),
                MerchandisingStyleX = (Byte)this.ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleXProperty),
                OrientationTypeX = (Byte)this.ReadProperty<PlanogramPositionOrientationType>(OrientationTypeXProperty),
                IsXPlacedLeft = this.ReadProperty<Boolean>(IsXPlacedLeftProperty),
                FacingsYHigh = this.ReadProperty<Int16>(FacingsYHighProperty),
                FacingsYWide = this.ReadProperty<Int16>(FacingsYWideProperty),
                FacingsYDeep = this.ReadProperty<Int16>(FacingsYDeepProperty),
                MerchandisingStyleY = (Byte)this.ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleYProperty),
                OrientationTypeY = (Byte)this.ReadProperty<PlanogramPositionOrientationType>(OrientationTypeYProperty),
                IsYPlacedBottom = this.ReadProperty<Boolean>(IsYPlacedBottomProperty),
                FacingsZHigh = this.ReadProperty<Int16>(FacingsZHighProperty),
                FacingsZWide = this.ReadProperty<Int16>(FacingsZWideProperty),
                FacingsZDeep = this.ReadProperty<Int16>(FacingsZDeepProperty),
                MerchandisingStyleZ = (Byte)this.ReadProperty<PlanogramPositionMerchandisingStyle>(MerchandisingStyleZProperty),
                OrientationTypeZ = (Byte)this.ReadProperty<PlanogramPositionOrientationType>(OrientationTypeZProperty),
                IsZPlacedFront = this.ReadProperty<Boolean>(IsZPlacedFrontProperty),
                Sequence = this.ReadProperty<Int16>(SequenceProperty),
                SequenceX = this.ReadProperty<Int16>(SequenceXProperty),
                SequenceY = this.ReadProperty<Int16>(SequenceYProperty),
                SequenceZ = this.ReadProperty<Int16>(SequenceZProperty),
                UnitsHigh = this.ReadProperty<Int16>(UnitsHighProperty),
                UnitsWide = this.ReadProperty<Int16>(UnitsWideProperty),
                UnitsDeep = this.ReadProperty<Int16>(UnitsDeepProperty),
                TotalUnits = this.ReadProperty<Int32>(TotalUnitsProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                MetaAchievedCases = this.ReadProperty<Single?>(MetaAchievedCasesProperty),
                MetaIsPositionCollisions = this.ReadProperty<Boolean?>(MetaIsPositionCollisionsProperty),
                MetaFrontFacingsWide = this.ReadProperty<Int16?>(MetaFrontFacingsWideProperty),
                PositionSequenceNumber = this.ReadProperty<Int16?>(PositionSequenceNumberProperty),
                HorizontalSqueeze = this.ReadProperty<Single>(HorizontalSqueezeProperty),
                VerticalSqueeze = this.ReadProperty<Single>(VerticalSqueezeProperty),
                DepthSqueeze = this.ReadProperty<Single>(DepthSqueezeProperty),
                HorizontalSqueezeX = this.ReadProperty<Single>(HorizontalSqueezeXProperty),
                VerticalSqueezeX = this.ReadProperty<Single>(VerticalSqueezeXProperty),
                DepthSqueezeX = this.ReadProperty<Single>(DepthSqueezeXProperty),
                HorizontalSqueezeY = this.ReadProperty<Single>(HorizontalSqueezeYProperty),
                VerticalSqueezeY = this.ReadProperty<Single>(VerticalSqueezeYProperty),
                DepthSqueezeY = this.ReadProperty<Single>(DepthSqueezeYProperty),
                HorizontalSqueezeZ = this.ReadProperty<Single>(HorizontalSqueezeZProperty),
                VerticalSqueezeZ = this.ReadProperty<Single>(VerticalSqueezeZProperty),
                DepthSqueezeZ = this.ReadProperty<Single>(DepthSqueezeZProperty),
                MetaIsUnderMinDeep = this.ReadProperty<Boolean?>(MetaIsUnderMinDeepProperty),
                MetaIsOverMaxDeep = this.ReadProperty<Boolean?>(MetaIsOverMaxDeepProperty),
                MetaIsOverMaxRightCap = this.ReadProperty<Boolean?>(MetaIsOverMaxRightCapProperty),
                MetaIsOverMaxStack = this.ReadProperty<Boolean?>(MetaIsOverMaxStackProperty),
                MetaIsOverMaxTopCap = this.ReadProperty<Boolean?>(MetaIsOverMaxTopCapProperty),
                MetaIsInvalidMerchandisingStyle = this.ReadProperty<Boolean?>(MetaIsInvalidMerchandisingStyleProperty),
                MetaIsHangingTray = this.ReadProperty<Boolean?>(MetaIsHangingTrayProperty),
                MetaIsPegOverfilled = this.ReadProperty<Boolean?>(MetaIsPegOverfilledProperty),
                MetaIsOutsideMerchandisingSpace = this.ReadProperty<Boolean?>(MetaIsOutsideMerchandisingSpaceProperty),
                IsManuallyPlaced = this.ReadProperty<Boolean>(IsManuallyPlacedProperty),
                MetaIsOutsideOfBlockSpace = this.ReadProperty<Boolean?>(MetaIsOutsideOfBlockSpaceProperty),
                MetaHeight = this.ReadProperty<Single?>(MetaHeightProperty),
                MetaWidth = this.ReadProperty<Single?>(MetaWidthProperty),
                MetaDepth = this.ReadProperty<Single?>(MetaDepthProperty),
                MetaWorldX = this.ReadProperty<Single?>(MetaWorldXProperty),
                MetaWorldY = this.ReadProperty<Single?>(MetaWorldYProperty),
                MetaWorldZ = this.ReadProperty<Single?>(MetaWorldZProperty),
                MetaTotalLinearSpace = this.ReadProperty<Single?>(MetaTotalLinearSpaceProperty),
                MetaTotalAreaSpace = this.ReadProperty<Single?>(MetaTotalAreaSpaceProperty),
                MetaTotalVolumetricSpace = this.ReadProperty<Single?>(MetaTotalVolumetricSpaceProperty),
                MetaPlanogramLinearSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty),
                MetaPlanogramAreaSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty),
                MetaPlanogramVolumetricSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty),
                SequenceNumber = this.ReadProperty<Int32?>(SequenceNumberProperty),
                SequenceColour = this.ReadProperty<Int32?>(SequenceColourProperty),
                MetaComparisonStatus = (Byte)this.ReadProperty<PlanogramItemComparisonStatusType>(MetaComparisonStatusProperty),
                MetaSequenceSubGroupName = this.ReadProperty<String>(MetaSequenceSubGroupNameProperty),
                MetaSequenceGroupName = this.ReadProperty<String>(MetaSequenceGroupNameProperty),
                MetaPegColumnNumber = this.ReadProperty<Int32?>(MetaPegColumnNumberProperty),
                MetaPegRowNumber = this.ReadProperty<Int32?>(MetaPegRowNumberProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramPositionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramPositionDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramPositionDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramPosition>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramPositionDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramPositionDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}