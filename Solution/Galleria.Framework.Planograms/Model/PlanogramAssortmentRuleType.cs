#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32787 : A.Silva
//  Created.

#endregion

#endregion

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Enum of the assortment rule for use in common methods.
    /// </summary>
    public enum PlanogramAssortmentRuleType
    {
        Optional,
        Normal,
        Inheritance,
        Family,
        Product,
        Distribution,
        LocalLine,
        //Extend for new rules as/when
    }
}