﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//  Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramSubComponentCombineType
    {
        None = 0,
        Both = 1,
        LeftOnly = 2,
        RightOnly = 3
    }

    /// <summary>
    /// PlanogramSubComponentCombineType Helper Class
    /// </summary>
    public static class PlanogramSubComponentCombineTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentCombineType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentCombineType, String>()
            {
                {PlanogramSubComponentCombineType.None, Message.Enum_PlanogramSubComponentCombineType_None},
                {PlanogramSubComponentCombineType.Both, Message.Enum_PlanogramSubComponentCombineType_Both},
                {PlanogramSubComponentCombineType.LeftOnly, Message.Enum_PlanogramSubComponentCombineType_LeftOnly},
                {PlanogramSubComponentCombineType.RightOnly, Message.Enum_PlanogramSubComponentCombineType_RightOnly},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentCombineType, String> FriendlyDescriptions =
            new Dictionary<PlanogramSubComponentCombineType, String>()
            {
                {PlanogramSubComponentCombineType.None, Message.Enum_PlanogramSubComponentCombineType_None},
                {PlanogramSubComponentCombineType.Both, Message.Enum_PlanogramSubComponentCombineType_Both},
                {PlanogramSubComponentCombineType.LeftOnly, Message.Enum_PlanogramSubComponentCombineType_LeftOnly},
                {PlanogramSubComponentCombineType.RightOnly, Message.Enum_PlanogramSubComponentCombineType_RightOnly},
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentCombineType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentCombineType>()
            {
                {Message.Enum_PlanogramSubComponentCombineType_None.ToLowerInvariant(), PlanogramSubComponentCombineType.None},
                {Message.Enum_PlanogramSubComponentCombineType_Both.ToLowerInvariant(), PlanogramSubComponentCombineType.Both},
                {Message.Enum_PlanogramSubComponentCombineType_LeftOnly.ToLowerInvariant(), PlanogramSubComponentCombineType.LeftOnly},
                {Message.Enum_PlanogramSubComponentCombineType_RightOnly.ToLowerInvariant(), PlanogramSubComponentCombineType.RightOnly},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentCombineType? PlanogramSubComponentCombineTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentCombineType? returnValue = null;
            PlanogramSubComponentCombineType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
