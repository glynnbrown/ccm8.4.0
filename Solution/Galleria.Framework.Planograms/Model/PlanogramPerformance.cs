﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
// V8-26338 : A.Kuszyk
//  Added business rules for unique and max length lists.
// V8-26773 : N.Foster
//  Created interface for performance
// V8-26825 : L.Ineson
//  Marked Metrics property as lazy load.
// V8-26825 : A.Kuszyk
//  Amended CheckBusinessRules to load property before checking rules.
// V8-26952 : A.Kuszyk
//  Added RelationshipType to PerformanceData property.
// V8-27496 : L.Luong
//  Added ClearAll method
// V8-27888 : L.Luong
//  Added DefaultMetrics to Performance
#endregion

#region Version History : CCM 811
// V8-28563 : A.Kuszyk
//  Moved UniqueList and MaxListLength business rules into the framework.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Core;
using Csla.Rules.CommonRules;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public sealed partial class PlanogramPerformance :
        ModelObject<PlanogramPerformance>,
        IPlanogramPerformance
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c=>c.Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c=>c.Description);
        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Metrics
        /// <summary>
        /// Metrics property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPerformanceMetricList> MetricsProperty =
            RegisterModelProperty<PlanogramPerformanceMetricList>(c => c.Metrics, RelationshipTypes.LazyLoad);
        /// <summary>
        /// Synchronously returns a list of metrics for this performance source
        /// </summary>
        public PlanogramPerformanceMetricList Metrics
        {
            get 
            { 
                return this.GetPropertyLazy<PlanogramPerformanceMetricList>(
                    MetricsProperty,
                    new PlanogramPerformanceMetricList.FetchByParentIdCriteria(this.DalFactoryName,this.Id),
                    true); 
            }
        }

        /// <summary>
        /// Asynchronously returns a list of metrics for this performance source
        /// </summary>
        public PlanogramPerformanceMetricList MetricsAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPerformanceMetricList>(
                    MetricsProperty,
                    new PlanogramPerformanceMetricList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false); 
            }
        }

        /// <summary>
        /// Synchronously returns a list of metrics for this performance source
        /// </summary>
        IEnumerable<IPlanogramPerformanceMetric> IPlanogramPerformance.Metrics
        {
            get { return this.Metrics.Select(metric => (IPlanogramPerformanceMetric)metric); }
        }

        #endregion

        #region PerformanceData
        /// <summary>
        /// PerformanceData property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramPerformanceDataList> PerformanceDataProperty =
            RegisterModelProperty<PlanogramPerformanceDataList>(c => c.PerformanceData,RelationshipTypes.LazyLoad);

        /// <summary>
        /// A list of the Performance Data that relate to this Planogram
        /// </summary>
        public PlanogramPerformanceDataList PerformanceData
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPerformanceDataList>(
                    PerformanceDataProperty,
                    new PlanogramPerformanceDataList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// A list of the Performance Data that relate to this Planogram
        /// </summary>
        public PlanogramPerformanceDataList PerformanceDataAsync
        {
            get
            {
                return this.GetPropertyLazy<PlanogramPerformanceDataList>(
                    PerformanceDataProperty,
                    new PlanogramPerformanceDataList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new MaxListLength<PlanogramPerformanceMetric>(MetricsProperty, Constants.MaximumMetricsPerPerformanceSource));
            BusinessRules.AddRule(new UniqueList<PlanogramPerformanceMetric>(MetricsProperty, PlanogramPerformanceMetric.MetricIdProperty));
            BusinessRules.AddRule(new UniqueList<PlanogramPerformanceMetric>(MetricsProperty, PlanogramPerformanceMetric.SpecialTypeProperty, new Object[] { MetricSpecialType.None }));
            BusinessRules.AddRule(new UniqueList<PlanogramPerformanceData>(PerformanceDataProperty, PlanogramPerformanceData.PlanogramProductIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId factory method
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(c => c.PlanogramId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object PlanogramId
            {
                get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(PlanogramIdProperty, planogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPerformance NewPlanogramPerformance()
        {
            var item = new PlanogramPerformance();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.PlanogramPerformance_Name_Default);
            this.LoadProperty<PlanogramPerformanceMetricList>(MetricsProperty, PlanogramPerformanceMetricList.NewPlanogramPerformanceMetricList());
            this.LoadProperty<PlanogramPerformanceDataList>(PerformanceDataProperty, PlanogramPerformanceDataList.NewPlanogramPerformanceDataList());

            this.DefaultMetrics();
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Checks the business rules for the given property.
        /// </summary>
        /// <param name="property">The property whose rules should be checked.</param>
        /// <returns>The IsValid state of the model.</returns>
        public Boolean CheckBusinessRules(Csla.Core.IPropertyInfo property)
        {
            var propertyInfo = this.GetType().GetProperty(property.Name);
            if (propertyInfo == null) throw new ArgumentException("property did not match a property on this type.");
            propertyInfo.GetValue(this, null); // Load property into memory
            BusinessRules.CheckRules(property);
            return IsValid;
        }

        /// <summary>
        /// Clears all Performance data
        /// </summary>
        public void ClearAll()
        {
            this.Metrics.Clear();
            foreach (var data in this.PerformanceData) data.ClearAllValues();

            this.Name = Message.PlanogramPerformance_Name_Default;
            this.Description = "";
        }

        /// <summary>
        /// Adds the default metrics
        /// </summary>
        private void DefaultMetrics()
        {
            this.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1));
            this.Metrics[0].Name = Message.PlanogramPerformance_DefaultMetric1_SalesValue;
            this.Metrics[0].Description = Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue;
            this.Metrics[0].Direction = MetricDirectionType.MaximiseIncrease;
            this.Metrics[0].Type = MetricType.Decimal;
            this.Metrics[0].SpecialType = MetricSpecialType.None;

            this.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2));
            this.Metrics[1].Name = Message.PlanogramPerformance_DefaultMetric2_SalesVolume;
            this.Metrics[1].Description = Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume;
            this.Metrics[1].Direction = MetricDirectionType.MaximiseIncrease;
            this.Metrics[1].Type = MetricType.Integer;
            this.Metrics[1].SpecialType = MetricSpecialType.RegularSalesUnits;

            this.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(3));
            this.Metrics[2].Name = Message.PlanogramPerformance_DefaultMetric3_SalesMargin;
            this.Metrics[2].Description = Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin;
            this.Metrics[2].Direction = MetricDirectionType.MaximiseIncrease;
            this.Metrics[2].Type = MetricType.Decimal;
            this.Metrics[2].SpecialType = MetricSpecialType.None;
        }

        /// <summary>
        /// Cycles through all data updating inventory values.
        /// </summary>
        public void UpdateAllInventoryValues()
        {
            if (this.Parent == null) return;

            foreach (PlanogramPerformanceData data in this.PerformanceData)
            {
                data.UpdateInventoryValues();
            }
        }

        #endregion
    }
}
