﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-24974 : L.Hodson
//      Added LeftStacked and RightStacked
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramSubComponentXMerchStrategyType
    {
        Manual = 0,
        Left = 1,
        LeftStacked = 2,
        Right = 3,
        RightStacked = 4,
        Even = 5,
    }

    /// <summary>
    /// PlanogramSubComponentXMerchStrategyType Helper Class
    /// </summary>
    public static class PlanogramSubComponentXMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramSubComponentXMerchStrategyType, String> FriendlyNames =
            new Dictionary<PlanogramSubComponentXMerchStrategyType, String>()
            {
                {PlanogramSubComponentXMerchStrategyType.Manual, Message.Enum_PlanogramSubComponentXMerchStrategyType_Manual},
                {PlanogramSubComponentXMerchStrategyType.Left, Message.Enum_PlanogramSubComponentXMerchStrategyType_Left},
                {PlanogramSubComponentXMerchStrategyType.LeftStacked, Message.Enum_PlanogramSubComponentXMerchStrategyType_LeftStacked},
                {PlanogramSubComponentXMerchStrategyType.Right, Message.Enum_PlanogramSubComponentXMerchStrategyType_Right},
                {PlanogramSubComponentXMerchStrategyType.RightStacked, Message.Enum_PlanogramSubComponentXMerchStrategyType_RightStacked},
                {PlanogramSubComponentXMerchStrategyType.Even, Message.Enum_PlanogramSubComponentXMerchStrategyType_Even},
            };

        
        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, PlanogramSubComponentXMerchStrategyType> EnumFromFriendlyName =
            new Dictionary<String, PlanogramSubComponentXMerchStrategyType>()
            {
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_Manual.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.Manual},
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_Left.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.Left},
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_LeftStacked.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.LeftStacked},
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_Right.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.Right},
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_RightStacked.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.RightStacked},
                {Message.Enum_PlanogramSubComponentXMerchStrategyType_Even.ToLowerInvariant(), PlanogramSubComponentXMerchStrategyType.Even},
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramSubComponentXMerchStrategyType? PlanogramSubComponentXMerchStrategyTypeGetEnum(String friendlyName)
        {
            PlanogramSubComponentXMerchStrategyType? returnValue = null;
            PlanogramSubComponentXMerchStrategyType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}
