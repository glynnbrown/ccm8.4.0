﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramSequenceGroupSubGroupList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to obtain new instances of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        private PlanogramSequenceGroupSubGroupList() {}

        #endregion

        /// <summary>
        ///     Invoked by CSLA via reflection when returning all the <see cref="PlanogramSequenceGroupProduct"/> items in the <see cref="PlanogramSequenceGroup"/>.
        /// </summary>
        /// <param name="criteria"><see cref="ModelList{T,T2}.FetchByParentIdCriteria"/> containing the parent Id of the items to be fetched.</param>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramSequenceGroupSubGroupDal>())
            {
                IEnumerable<PlanogramSequenceGroupSubGroupDto> dtos = dal.FetchByPlanogramSequenceGroupId(criteria.ParentId);
                foreach (PlanogramSequenceGroupSubGroupDto dto in dtos)
                {
                    Add(PlanogramSequenceGroupSubGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }
    }
}
