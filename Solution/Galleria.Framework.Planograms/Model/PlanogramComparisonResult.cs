﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Rules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Represents the final result for comparisons agains one target planogram. Contains information to run the comparison again.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonResult : ModelObject<PlanogramComparisonResult>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparison"/>.
        /// </summary>
        public new PlanogramComparison Parent
        {
            get { return ((PlanogramComparisonResultList)base.Parent).Parent; }
        }

        #endregion

        #region Planogram Name

        public static readonly ModelPropertyInfo<String> PlanogramNameProperty =
            RegisterModelProperty<String>(c => c.PlanogramName);

        public String PlanogramName { get { return GetProperty(PlanogramNameProperty); } set { SetProperty(PlanogramNameProperty, value); } }

        #endregion

        #region Status

        public static readonly ModelPropertyInfo<PlanogramComparisonResultStatusType> StatusProperty =
            RegisterModelProperty<PlanogramComparisonResultStatusType>(c => c.Status);

        public PlanogramComparisonResultStatusType Status { get { return GetProperty(StatusProperty); } set { SetProperty(StatusProperty, value); } }

        #endregion

        #region ComparedItems

        public static readonly ModelPropertyInfo<PlanogramComparisonItemList> ComparedItemsProperty =
            RegisterModelProperty<PlanogramComparisonItemList>(c => c.ComparedItems, RelationshipTypes.LazyLoad);

        public PlanogramComparisonItemList ComparedItems
        {
            get { return GetPropertyLazy(ComparedItemsProperty, new PlanogramComparisonItemList.FetchByParentIdCriteria(DalFactoryName, Id), true); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(PlanogramNameProperty));
            BusinessRules.AddRule(new MaxLength(PlanogramNameProperty, 100));
            BusinessRules.AddRule(new UniqueList<PlanogramComparisonItem>(ComparedItemsProperty, PlanogramComparisonItem.ItemIdProperty));
        }

        #endregion

        #region Authorization Rules

        private static void AddObjectAuthorizationRules() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonResult"/>.
        /// </summary>
        public static PlanogramComparisonResult NewPlanogramComparisonResult()
        {
            var item = new PlanogramComparisonResult();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Create a new insance of <see cref="PlanogramComparisonResult"/> derived from the given <paramref name="planogram"/>.
        /// </summary>
        /// <param name="planogram">The instance of <see cref="Planogram"/> from which to derive the new instance.</param>
        public static PlanogramComparisonResult NewPlanogramComparisonResult(Planogram planogram)
        {
            var item = new PlanogramComparisonResult();
            item.Create(planogram.Name);
            return item;
        }

        #endregion

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            SetDefaultValues();
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Initialize all default property values for this instance using the given <paramref name="planogramName"/>.
        /// </summary>
        private void Create(String planogramName)
        {
            SetDefaultValues();
            LoadProperty(PlanogramNameProperty, planogramName);
            MarkAsChild();
            base.Create();
        }

        private void SetDefaultValues()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(ComparedItemsProperty, PlanogramComparisonItemList.NewPlanogramComparisonItemList());
        }

        #endregion
    }

    public enum PlanogramComparisonResultStatusType
    {
        Unchanged = 0,
        Changed = 1,
        AddedItems = 2,
        RemovedItems = 3
    }

    public static class PlanogramComparisonResultStatusTypeHelper
    {
        public static readonly Dictionary<PlanogramComparisonResultStatusType, String> FriendlyNames =
            new Dictionary<PlanogramComparisonResultStatusType, String>
            {
                {PlanogramComparisonResultStatusType.Unchanged, Message.PlanogramComparisonResultStatusType_Unchanged},
                {PlanogramComparisonResultStatusType.Changed, Message.PlanogramComparisonResultStatusType_Changed},
                {PlanogramComparisonResultStatusType.AddedItems, Message.PlanogramComparisonResultStatusType_AddedItems},
                {PlanogramComparisonResultStatusType.RemovedItems, Message.PlanogramComparisonResultStatusType_RemovedItems},
            };
    }
}