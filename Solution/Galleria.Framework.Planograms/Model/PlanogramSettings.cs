﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26421 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29592 : L.Ineson
//  Added imperial region defaults.
#endregion
#region Version History: CCM820
// V8-30936 : M.Brumby
//  Slotwall defaults
#endregion
#region Version History: CCM830
// V8-32591 :L.Ineson
//  Added textbox defaults.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using System.Globalization;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Simple object holding default planogam settings
    /// </summary>
    public partial class PlanogramSettings : ModelReadOnlyObject<PlanogramSettings>, IPlanogramSettings
    {
        #region Constructor
        private PlanogramSettings() { } // force use of factory methods
        #endregion

        #region Properties

        #region FixtureHeight
        /// <summary>
        /// FixtureHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureHeightProperty =
            RegisterModelProperty<Single>(c => c.FixtureHeight);
        /// <summary>
        /// Gets or sets the default fixture height
        /// </summary>
        public Single FixtureHeight
        {
            get { return this.ReadProperty<Single>(FixtureHeightProperty); }
            set { this.LoadProperty<Single>(FixtureHeightProperty, value); }
        }
        #endregion

        #region FixtureWidth
        /// <summary>
        /// FixtureWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureWidthProperty =
            RegisterModelProperty<Single>(c => c.FixtureWidth);
        /// <summary>
        /// Gets or sets the default fixture width
        /// </summary>
        public Single FixtureWidth
        {
            get { return this.ReadProperty<Single>(FixtureWidthProperty); }
            set { this.LoadProperty<Single>(FixtureWidthProperty, value); }
        }
        #endregion

        #region FixtureDepth
        /// <summary>
        /// FixtureDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureDepthProperty =
            RegisterModelProperty<Single>(c => c.FixtureDepth);
        /// <summary>
        /// Gets or sets the default fixture depth
        /// </summary>
        public Single FixtureDepth
        {
            get { return this.ReadProperty<Single>(FixtureDepthProperty); }
            set { this.LoadProperty<Single>(FixtureDepthProperty, value); }
        }
        #endregion

        #region BaseHeight
        /// <summary>
        /// BaseHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseHeightProperty =
            RegisterModelProperty<Single>(c => c.BaseHeight);
        /// <summary>
        /// Gets or set the default base height
        /// </summary>
        public Single BaseHeight
        {
            get { return this.ReadProperty<Single>(BaseHeightProperty); }
            set { this.LoadProperty<Single>(BaseHeightProperty, value); }
        }
        #endregion

        #region BackboardDepth
        /// <summary>
        /// BackboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BackboardDepthProperty =
            RegisterModelProperty<Single>(c => c.BackboardDepth);
        /// <summary>
        /// Gets or sets the default backboard depth
        /// </summary>
        public Single BackboardDepth
        {
            get { return this.ReadProperty<Single>(BackboardDepthProperty); }
            set { this.LoadProperty<Single>(BackboardDepthProperty, value); }
        }
        #endregion

        #region BarHeight
        /// <summary>
        /// BarHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarHeightProperty =
            RegisterModelProperty<Single>(c => c.BarHeight);
        /// <summary>
        /// Gets or sets the default bar height
        /// </summary>
        public Single BarHeight
        {
            get { return this.ReadProperty<Single>(BarHeightProperty); }
            set { this.LoadProperty<Single>(BarHeightProperty, value); }
        }
        #endregion

        #region BarWidth
        /// <summary>
        /// BarWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarWidthProperty =
            RegisterModelProperty<Single>(c => c.BarWidth);
        /// <summary>
        /// Gets or sets the bar width
        /// </summary>
        public Single BarWidth
        {
            get { return this.ReadProperty<Single>(BarWidthProperty); }
            set { this.LoadProperty<Single>(BarWidthProperty, value); }
        }
        #endregion

        #region BarDepth
        /// <summary>
        /// BarDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarDepthProperty =
            RegisterModelProperty<Single>(c => c.BarDepth);
        /// <summary>
        /// Gets or sets the default bar depth
        /// </summary>
        public Single BarDepth
        {
            get { return this.ReadProperty<Single>(BarDepthProperty); }
            set { this.LoadProperty<Single>(BarDepthProperty, value); }
        }
        #endregion

        #region BarFillColour
        /// <summary>
        /// BarFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BarFillColourProperty =
            RegisterModelProperty<Int32>(c => c.BarFillColour);
        /// <summary>
        /// Gets or sets the default bar fill colour
        /// </summary>
        public Int32 BarFillColour
        {
            get { return this.ReadProperty<Int32>(BarFillColourProperty); }
            set { this.LoadProperty<Int32>(BarFillColourProperty, value); }
        }
        #endregion

        #region ChestHeight
        /// <summary>
        /// ChestHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestHeightProperty =
            RegisterModelProperty<Single>(c => c.ChestHeight);
        /// <summary>
        /// Gets or sets the default chest height
        /// </summary>
        public Single ChestHeight
        {
            get { return this.ReadProperty<Single>(ChestHeightProperty); }
            set { this.LoadProperty<Single>(ChestHeightProperty, value); }
        }
        #endregion

        #region ChestWidth
        /// <summary>
        /// ChestWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestWidthProperty =
            RegisterModelProperty<Single>(c => c.ChestWidth);
        /// <summary>
        /// Gets or sets the default chest width
        /// </summary>
        public Single ChestWidth
        {
            get { return this.ReadProperty<Single>(ChestWidthProperty); }
            set { this.LoadProperty<Single>(ChestWidthProperty, value); }
        }
        #endregion

        #region ChestDepth
        /// <summary>
        /// ChestDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestDepthProperty =
            RegisterModelProperty<Single>(c => c.ChestDepth);
        /// <summary>
        /// Gets or sets the default chest depth
        /// </summary>
        public float ChestDepth
        {
            get { return this.ReadProperty<Single>(ChestDepthProperty); }
            set { this.LoadProperty<Single>(ChestDepthProperty, value); }
        }
        #endregion

        #region ChestFillColour
        /// <summary>
        /// ChestFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ChestFillColourProperty =
            RegisterModelProperty<Int32>(c => c.ChestFillColour);
        /// <summary>
        /// Gets or sets the chest fill colour
        /// </summary>
        public Int32 ChestFillColour
        {
            get { return this.ReadProperty<Int32>(ChestFillColourProperty); }
            set { this.LoadProperty<Int32>(ChestFillColourProperty, value); }
        }
        #endregion

        #region ClipStripHeight
        /// <summary>
        /// ClipStripHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripHeightProperty =
            RegisterModelProperty<Single>(c => c.ClipStripHeight);
        /// <summary>
        /// Gets or sets the default clip strip height
        /// </summary>
        public Single ClipStripHeight
        {
            get { return this.ReadProperty<Single>(ClipStripHeightProperty); }
            set { this.LoadProperty<Single>(ClipStripHeightProperty, value); }
        }
        #endregion

        #region ClipStripWidth
        /// <summary>
        /// ClipStripWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripWidthProperty =
            RegisterModelProperty<Single>(c => c.ClipStripWidth);
        /// <summary>
        /// Gets or sets the default clip strip width
        /// </summary>
        public Single ClipStripWidth
        {
            get { return this.ReadProperty<Single>(ClipStripWidthProperty); }
            set { this.LoadProperty<Single>(ClipStripWidthProperty, value); }
        }
        #endregion

        #region ClipStripDepth
        /// <summary>
        /// ClipStripDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripDepthProperty =
            RegisterModelProperty<Single>(c => c.ClipStripDepth);
        /// <summary>
        /// Gets or sets the default clip strip depth
        /// </summary>
        public Single ClipStripDepth
        {
            get { return this.ReadProperty<Single>(ClipStripDepthProperty); }
            set { this.LoadProperty<Single>(ClipStripDepthProperty, value); }
        }
        #endregion

        #region ClipStripFillColour
        /// <summary>
        /// ClipStripFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClipStripFillColourProperty =
            RegisterModelProperty<Int32>(c => c.ClipStripFillColour);
        /// <summary>
        /// Gets or sets the default clip strip fill colour
        /// </summary>
        public Int32 ClipStripFillColour
        {
            get { return this.ReadProperty<Int32>(ClipStripFillColourProperty); }
            set { this.LoadProperty<Int32>(ClipStripFillColourProperty, value); }
        }
        #endregion

        #region ShelfHeight
        /// <summary>
        /// ShelfHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfHeightProperty =
            RegisterModelProperty<Single>(c => c.ShelfHeight);
        /// <summary>
        /// Gets or sets the default shelf height
        /// </summary>
        public Single ShelfHeight
        {
            get { return this.ReadProperty<Single>(ShelfHeightProperty); }
            set { this.LoadProperty<Single>(ShelfHeightProperty, value); }
        }
        #endregion

        #region ShelfWidth
        /// <summary>
        /// ShelfWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfWidthProperty =
            RegisterModelProperty<Single>(c => c.ShelfWidth);
        /// <summary>
        /// Gets or sets the default shelf width
        /// </summary>
        public Single ShelfWidth
        {
            get { return this.ReadProperty<Single>(ShelfWidthProperty); }
            set { this.LoadProperty<Single>(ShelfWidthProperty, value); }
        }
        #endregion

        #region ShelfDepth
        /// <summary>
        /// ShelfDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfDepthProperty =
            RegisterModelProperty<Single>(c => c.ShelfDepth);
        /// <summary>
        /// Gets or sets the default shelf depth
        /// </summary>
        public Single ShelfDepth
        {
            get { return this.ReadProperty<Single>(ShelfDepthProperty); }
            set { this.LoadProperty<Single>(ShelfDepthProperty, value); }
        }
        #endregion

        #region ShelfFillColour
        /// <summary>
        /// ShelfFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ShelfFillColourProperty =
            RegisterModelProperty<Int32>(c => c.ShelfFillColour);
        /// <summary>
        /// Gets or sets the default shelf fill colour
        /// </summary>
        public Int32 ShelfFillColour
        {
            get { return this.ReadProperty<Int32>(ShelfFillColourProperty); }
            set { this.LoadProperty<Int32>(ShelfFillColourProperty, value); }
        }
        #endregion

        #region PegboardHeight
        /// <summary>
        /// PegboardHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardHeightProperty =
            RegisterModelProperty<Single>(c => c.PegboardHeight);
        /// <summary>
        /// Gets or sets the default pegboard height
        /// </summary>
        public Single PegboardHeight
        {
            get { return this.ReadProperty<Single>(PegboardHeightProperty); }
            set { this.LoadProperty<Single>(PegboardHeightProperty, value); }
        }
        #endregion

        #region PegboardWidth
        /// <summary>
        /// PegboardWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardWidthProperty =
            RegisterModelProperty<Single>(c => c.PegboardWidth);
        /// <summary>
        /// Gets or sets the default pegboard width
        /// </summary>
        public Single PegboardWidth
        {
            get { return this.ReadProperty<Single>(PegboardWidthProperty); }
            set { this.LoadProperty<Single>(PegboardWidthProperty, value); }
        }
        #endregion

        #region PegboardDepth
        /// <summary>
        /// PegboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardDepthProperty =
            RegisterModelProperty<Single>(c => c.PegboardDepth);
        /// <summary>
        /// Gets or sets the default pegboard depth
        /// </summary>
        public Single PegboardDepth
        {
            get { return this.ReadProperty<Single>(PegboardDepthProperty); }
            set { this.LoadProperty<Single>(PegboardDepthProperty, value); }
        }
        #endregion

        #region PegboardFillColour
        /// <summary>
        /// PegboardFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PegboardFillColourProperty =
            RegisterModelProperty<Int32>(c => c.PegboardFillColour);
        /// <summary>
        /// Gets or sets the default pegboard fill colour
        /// </summary>
        public Int32 PegboardFillColour
        {
            get { return this.ReadProperty<Int32>(PegboardFillColourProperty); }
            set { this.LoadProperty<Int32>(PegboardFillColourProperty, value); }
        }
        #endregion

        #region RodHeight
        /// <summary>
        /// RodHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodHeightProperty =
            RegisterModelProperty<Single>(c => c.RodHeight);
        /// <summary>
        /// Gets or sets the default rod height
        /// </summary>
        public Single RodHeight
        {
            get { return this.ReadProperty<Single>(RodHeightProperty); }
            set { this.LoadProperty<Single>(RodHeightProperty, value); }
        }
        #endregion

        #region RodWidth
        /// <summary>
        /// RowWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodWidthProperty =
            RegisterModelProperty<Single>(c => c.RodWidth);
        /// <summary>
        /// Gets or sets the default rod width
        /// </summary>
        public Single RodWidth
        {
            get { return this.ReadProperty<Single>(RodWidthProperty); }
            set { this.LoadProperty<Single>(RodWidthProperty, value); }
        }
        #endregion

        #region RodDepth
        /// <summary>
        /// RodDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodDepthProperty =
            RegisterModelProperty<Single>(c => c.RodDepth);
        /// <summary>
        /// Gets or sets the default rod depth
        /// </summary>
        public Single RodDepth
        {
            get { return this.ReadProperty<Single>(RodDepthProperty); }
            set { this.LoadProperty<Single>(RodDepthProperty, value); }
        }
        #endregion

        #region RodFillColour
        /// <summary>
        /// RodFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RodFillColourProperty =
            RegisterModelProperty<Int32>(c => c.RodFillColour);
        /// <summary>
        /// Gets or sets the default rod fill colour
        /// </summary>
        public Int32 RodFillColour
        {
            get { return this.ReadProperty<Int32>(RodFillColourProperty); }
            set { this.LoadProperty<Int32>(RodFillColourProperty, value); }
        }
        #endregion

        #region PalletHeight
        /// <summary>
        /// PalletHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletHeightProperty =
            RegisterModelProperty<Single>(c => c.PalletHeight);
        /// <summary>
        /// Gets or sets the default pallet height
        /// </summary>
        public Single PalletHeight
        {
            get { return this.ReadProperty<Single>(PalletHeightProperty); }
            set { this.LoadProperty<Single>(PalletHeightProperty, value); }
        }
        #endregion

        #region PalletWidth
        /// <summary>
        /// PalletWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletWidthProperty =
            RegisterModelProperty<Single>(c => c.PalletWidth);
        /// <summary>
        /// Gets or sets the defaul width
        /// </summary>
        public Single PalletWidth
        {
            get { return this.ReadProperty<Single>(PalletWidthProperty); }
            set { this.LoadProperty<Single>(PalletWidthProperty, value); }
        }
        #endregion

        #region PalletDepth
        /// <summary>
        /// PalletDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletDepthProperty =
            RegisterModelProperty<Single>(c => c.PalletDepth);
        /// <summary>
        /// Gets or sets the default pallet depth
        /// </summary>
        public Single PalletDepth
        {
            get { return this.ReadProperty<Single>(PalletDepthProperty); }
            set { this.LoadProperty<Single>(PalletDepthProperty, value); }
        }
        #endregion

        #region PalletFillColour
        /// <summary>
        /// PalletFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PalletFillColourProperty =
            RegisterModelProperty<Int32>(c => c.PalletFillColour);
        /// <summary>
        /// Gets or sets the default pallet fill colour
        /// </summary>
        public Int32 PalletFillColour
        {
            get { return this.ReadProperty<Int32>(PalletFillColourProperty); }
            set { this.LoadProperty<Int32>(PalletFillColourProperty, value); }
        }
        #endregion

        #region SlotwallHeight
        /// <summary>
        /// SlotwallHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallHeightProperty =
            RegisterModelProperty<Single>(c => c.SlotwallHeight);
        /// <summary>
        /// Gets or sets the default Slotwall height
        /// </summary>
        public Single SlotwallHeight
        {
            get { return this.ReadProperty<Single>(SlotwallHeightProperty); }
            set { this.LoadProperty<Single>(SlotwallHeightProperty, value); }
        }
        #endregion

        #region SlotwallWidth
        /// <summary>
        /// SlotwallWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallWidthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallWidth);
        /// <summary>
        /// Gets or sets the default Slotwall width
        /// </summary>
        public Single SlotwallWidth
        {
            get { return this.ReadProperty<Single>(SlotwallWidthProperty); }
            set { this.LoadProperty<Single>(SlotwallWidthProperty, value); }
        }
        #endregion

        #region SlotwallDepth
        /// <summary>
        /// SlotwallDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallDepthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallDepth);
        /// <summary>
        /// Gets or sets the default Slotwall depth
        /// </summary>
        public Single SlotwallDepth
        {
            get { return this.ReadProperty<Single>(SlotwallDepthProperty); }
            set { this.LoadProperty<Single>(SlotwallDepthProperty, value); }
        }
        #endregion

        #region SlotwallFillColour
        /// <summary>
        /// SlotwallFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SlotwallFillColourProperty =
            RegisterModelProperty<Int32>(c => c.SlotwallFillColour);
        /// <summary>
        /// Gets or sets the default Slotwall fill colour
        /// </summary>
        public Int32 SlotwallFillColour
        {
            get { return this.ReadProperty<Int32>(SlotwallFillColourProperty); }
            set { this.LoadProperty<Int32>(SlotwallFillColourProperty, value); }
        }
        #endregion

        #region TextBoxFont

        /// <summary>
        /// TextBoxFont property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> TextBoxFontProperty =
            RegisterModelProperty<String>(o => o.TextBoxFont);

        /// <summary>
        /// Gets/Sets the default textbox font
        /// </summary>
        public String TextBoxFont
        {
            get { return this.GetProperty<String>(TextBoxFontProperty); }
            set { this.LoadProperty<String>(TextBoxFontProperty, value); }
        }

        #endregion

        #region TextBoxFontSize

        /// <summary>
        /// TextBoxFontSize property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxFontSizeProperty =
            RegisterModelProperty<Single>(o => o.TextBoxFontSize);

        /// <summary>
        /// Returns the default textbox font size
        /// </summary>
        public Single TextBoxFontSize
        {
            get { return this.GetProperty<Single>(TextBoxFontSizeProperty); }
            set { this.LoadProperty<Single>(TextBoxFontSizeProperty, value); }
        }

        #endregion

        #region TextBoxCanReduceToFit

        /// <summary>
        /// TextBoxCanReduceToFit property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> TextBoxCanReduceToFitProperty =
            RegisterModelProperty<Boolean>(o => o.TextBoxCanReduceToFit);


        /// <summary>
        /// Returns true of the textbox setting for reduce to fit is on.
        /// </summary>
        public Boolean TextBoxCanReduceToFit
        {
            get { return this.GetProperty<Boolean>(TextBoxCanReduceToFitProperty); }
            set { this.LoadProperty<Boolean>(TextBoxCanReduceToFitProperty, value); }
        }

        #endregion

        #region TextBoxFontColour

        /// <summary>
        /// TextBoxFontColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxFontColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxFontColour);

        /// <summary>
        /// The default textbox font colour.
        /// </summary>
        public Int32 TextBoxFontColour
        {
            get { return this.GetProperty<Int32>(TextBoxFontColourProperty); }
            set { this.LoadProperty<Int32>(TextBoxFontColourProperty, value); }
        }

        #endregion

        #region TextBoxBackgroundColour

        /// <summary>
        /// TextBoxBackgroundColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBackgroundColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBackgroundColour);

        /// <summary>
        /// The default textbox background colour
        /// </summary>
        public Int32 TextBoxBackgroundColour
        {
            get { return this.GetProperty<Int32>(TextBoxBackgroundColourProperty); }
            set { this.LoadProperty<Int32>(TextBoxBackgroundColourProperty, value); }
        }


        #endregion

        #region TextBoxBorderColour

        /// <summary>
        /// TextBoxBorderColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBorderColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBorderColour);


        /// <summary>
        /// The default textbox border colour
        /// </summary>
        public Int32 TextBoxBorderColour
        {
            get { return this.GetProperty<Int32>(TextBoxBorderColourProperty); }
            set { this.LoadProperty<Int32>(TextBoxBorderColourProperty, value); }
        }

        #endregion

        #region TextBoxBorderThickness

        /// <summary>
        /// TextBoxBorderThickness property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxBorderThicknessProperty =
            RegisterModelProperty<Single>(o => o.TextBoxBorderThickness);

        /// <summary>
        /// The default textbox border thickness
        /// </summary>
        public Single TextBoxBorderThickness
        {
            get { return this.GetProperty<Single>(TextBoxBorderThicknessProperty); }
            set { this.LoadProperty<Single>(TextBoxBorderThicknessProperty, value); }
        }

        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramSettings NewPlanogramSettings()
        {
            PlanogramSettings item = new PlanogramSettings();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        public void Create()
        {
            if (RegionInfo.CurrentRegion.IsMetric)
            {
                // fixture settings
                this.FixtureWidth = 120F;
                this.FixtureHeight = 200F;
                this.FixtureDepth = 76F;
                this.BackboardDepth = 1F;
                this.BaseHeight = 10F;

                // shelf settings
                this.ShelfWidth = this.FixtureWidth;
                this.ShelfHeight = 4F;
                this.ShelfDepth = 75F;

                // pegboard settings
                this.PegboardWidth = this.FixtureWidth;
                this.PegboardHeight = 100F;
                this.PegboardDepth = 1F;

                // chest settings
                this.ChestWidth = this.FixtureWidth;
                this.ChestHeight = 50F;
                this.ChestDepth = 75F;

                // bar settings
                this.BarWidth = this.FixtureWidth;
                this.BarHeight = 2F;
                this.BarDepth = 2F;

                // rod settings
                this.RodWidth = 4F;
                this.RodHeight = 4F;
                this.RodDepth = 60F;

                // clip strip settings
                this.ClipStripWidth = 4F;
                this.ClipStripHeight = 100F;
                this.ClipStripDepth = 1F;

                // pallet settings
                this.PalletWidth = this.FixtureWidth;
                this.PalletHeight = 10F;
                this.PalletDepth = 50F;

                // Slotwall settings
                this.SlotwallWidth = this.FixtureWidth;
                this.SlotwallHeight = 100F;
                this.SlotwallDepth = 1F;
            }
            else
            {
                // fixture settings
                this.FixtureHeight = 78F;
                this.FixtureWidth = 48F;
                this.FixtureDepth = 24F;
                this.BackboardDepth = 1F;
                this.BaseHeight = 6F;

                // shelf settings
                this.ShelfWidth = this.FixtureWidth;
                this.ShelfHeight = 1F;
                this.ShelfDepth = 24F;

                // pegboard settings
                this.PegboardWidth = this.FixtureWidth;
                this.PegboardHeight = 48F;
                this.PegboardDepth = 1F;

                // chest settings
                this.ChestWidth = this.FixtureWidth;
                this.ChestHeight = 36F;
                this.ChestDepth = 24F;

                // rod settings
                this.RodWidth = 1F;
                this.RodHeight = 1F;
                this.RodDepth = 24F;

                // bar settings
                this.BarWidth = this.FixtureWidth;
                this.BarHeight = 1F;
                this.BarDepth = 1F;

                // clip strip settings
                this.ClipStripWidth = 1F;
                this.ClipStripHeight = 30F;
                this.ClipStripDepth = 1F;

                // pallet settings
                this.PalletWidth = 40F;
                this.PalletHeight = 4F;
                this.PalletDepth = 40F;

                // Slotwall settings
                this.SlotwallWidth = this.FixtureWidth;
                this.SlotwallHeight = 148F;
                this.SlotwallDepth = 1F;
            }

            //colours
            this.ShelfFillColour = -16777216;
            this.PegboardFillColour = -3308225;
            this.ChestFillColour = -10185235;
            this.BarFillColour = -11179217;
            this.RodFillColour = -5171;
            this.ClipStripFillColour = -5103070;
            this.PalletFillColour = -7650029;
            this.SlotwallFillColour = -7177553;

            //Textbox
            this.TextBoxFont = "Arial";
            this.TextBoxFontSize = 20;
            this.TextBoxCanReduceToFit = true;
            this.TextBoxFontColour = -16777216;//black
            this.TextBoxBorderColour = -16777216;//black
            this.TextBoxBackgroundColour = -1;//white
            this.TextBoxBorderThickness = (RegionInfo.CurrentRegion.IsMetric) ? 1 : 0.2F;

        }
        #endregion

        #endregion
    }
}
