﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of assembly items contained within a fixture
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramFixtureComponentList : ModelList<PlanogramFixtureComponentList, PlanogramFixtureComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramFixture Parent
        {
            get { return (PlanogramFixture)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramFixtureComponentList NewPlanogramFixtureComponentList()
        {
            PlanogramFixtureComponentList item = new PlanogramFixtureComponentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates and adds a new component to this list
        /// </summary>
        public PlanogramFixtureComponent Add(PlanogramComponentType componentType,
            Single width, Single height, Single depth, Int32 fillColour = -16777216)
        {
            PlanogramFixture fixture = this.Parent;
            if (fixture == null) return null;
            Planogram planogram = fixture.Parent;
            if (planogram == null) return null;

            PlanogramComponent component = planogram.Components.Add(componentType, width, height, depth, fillColour);
            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            this.Add(fixtureComponent);
            return fixtureComponent;
        }

        /// <summary>
        /// Creates and adds a new component to this list
        /// </summary>
        public PlanogramFixtureComponent Add(PlanogramComponentType componentType, IPlanogramSettings settings)
        {
            PlanogramFixture fixture = this.Parent;
            if (fixture == null) return null;
            Planogram planogram = fixture.Parent;
            if (planogram == null) return null;

            PlanogramComponent component = planogram.Components.Add(componentType, settings);
            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            this.Add(fixtureComponent);
            return fixtureComponent;
        }

        #endregion
    }
}