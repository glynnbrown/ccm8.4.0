﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created 
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramBlockingLocationList
    {
        #region Constructor
        private PlanogramBlockingLocationList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all annotations for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramBlockingLocationDal dal = dalContext.GetDal<IPlanogramBlockingLocationDal>())
                {
                    IEnumerable<PlanogramBlockingLocationDto> dtoList = dal.FetchByPlanogramBlockingId(criteria.ParentId);
                    foreach (PlanogramBlockingLocationDto dto in dtoList)
                    {
                        this.Add(PlanogramBlockingLocation.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
