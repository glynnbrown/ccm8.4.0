﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramAssortmentLocalProduct
    {
        #region Constructor
        private PlanogramAssortmentLocalProduct() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramAssortmentLocalProduct Fetch(IDalContext dalContext, PlanogramAssortmentLocalProductDto dto)
        {
            return DataPortal.FetchChild<PlanogramAssortmentLocalProduct>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramAssortmentLocalProductDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            this.LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramAssortmentLocalProductDto GetDataTransferObject(PlanogramAssortment parent)
        {
            return new PlanogramAssortmentLocalProductDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramAssortmentId = parent.Id,
                ProductGtin = this.ReadProperty<String>(ProductGtinProperty),
                LocationCode = this.ReadProperty<String>(LocationCodeProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramAssortmentLocalProductDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramAssortmentLocalProductDto>(
            (dc) =>
            {
                PlanogramAssortmentLocalProductDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramAssortmentLocalProduct>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramAssortmentLocalProductDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramAssortment parent)
        {
            batchContext.Delete<PlanogramAssortmentLocalProductDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
