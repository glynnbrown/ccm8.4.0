﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Server side implementation for <see cref="PlanogramSequenceGroupList"/>.
    /// </summary>
    public sealed partial class PlanogramSequenceGroupList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to obtain new instances of <see cref="PlanogramSequenceGroupList"/>.
        /// </summary>
        private PlanogramSequenceGroupList() {}

        #endregion

        /// <summary>
        ///     Invoked by CSLA via reflection when returning all the <see cref="PlanogramSequenceGroup"/> items in the <see cref="PlanogramSequence"/>.
        /// </summary>
        /// <param name="criteria"><see cref="ModelList{T,T2}.FetchByParentIdCriteria"/> containing the parent Id of the items to be fetched.</param>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramSequenceGroupDal>())
            {
                IEnumerable<PlanogramSequenceGroupDto> dtos = dal.FetchByPlanogramSequenceId(criteria.ParentId);
                foreach (PlanogramSequenceGroupDto dto in dtos)
                {
                    Add(PlanogramSequenceGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }
    }
}