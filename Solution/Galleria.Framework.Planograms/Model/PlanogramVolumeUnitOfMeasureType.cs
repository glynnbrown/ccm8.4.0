﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24953 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available length unit of measure types
    /// </summary>
    public enum PlanogramVolumeUnitOfMeasureType
    {
        Unknown = 0, 
        Litres = 1, 
        CubicFeet = 2
    }

    /// <summary>
    /// PlanogramVolumnUnitOfMeasureType Helper Class
    /// </summary>
    public static class PlanogramVolumeUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramVolumeUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<PlanogramVolumeUnitOfMeasureType, String>()
            {
                {PlanogramVolumeUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramVolumeUnitOfMeasureType.Litres, Message.Enum_PlanogramVolumnUnitOfMeasureType_Litres},
                {PlanogramVolumeUnitOfMeasureType.CubicFeet, Message.Enum_PlanogramVolumnUnitOfMeasureType_CubicFeet},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramVolumeUnitOfMeasureType, String> Abbreviations =
            new Dictionary<PlanogramVolumeUnitOfMeasureType, String>()
            {
                {PlanogramVolumeUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramVolumeUnitOfMeasureType.Litres, Message.Enum_PlanogramVolumnUnitOfMeasureType_Litres_Abbrev},
                {PlanogramVolumeUnitOfMeasureType.CubicFeet, Message.Enum_PlanogramVolumnUnitOfMeasureType_CubicFeet_Abbrev},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramVolumeUnitOfMeasureType, String> AvailableValues =
            new Dictionary<PlanogramVolumeUnitOfMeasureType, String>()
            {
                //{PlanogramVolumeUnitOfMeasureType.Unknown, String.Empty},//removed so that this isn't selectable in ui.
                {PlanogramVolumeUnitOfMeasureType.Litres, Message.Enum_PlanogramVolumnUnitOfMeasureType_Litres},
                {PlanogramVolumeUnitOfMeasureType.CubicFeet, Message.Enum_PlanogramVolumnUnitOfMeasureType_CubicFeet},
            };

    }
}
