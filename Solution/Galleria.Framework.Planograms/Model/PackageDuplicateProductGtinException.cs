﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-29533 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public class PackageDuplicateProductGtinException : Exception
    {
        public PackageDuplicateProductGtinException(string message) 
            : base(String.Format(Galleria.Framework.Planograms.Resources.Language.Message.PackageDuplicateProductGtinException, message)) { }        
    }
}