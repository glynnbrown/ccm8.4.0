﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of positions contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramPositionList : ModelList<PlanogramPositionList, PlanogramPosition>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramPositionList NewPlanogramPositionList()
        {
            PlanogramPositionList item = new PlanogramPositionList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Creates and adds a new position to this planogram
        /// </summary>
        public PlanogramPosition Add()
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(this.GetNextSequence());
            this.Add(position);
            return position;
        }

        /// <summary>
        /// Creates and adds a new position to this planogram
        /// </summary>
        public PlanogramPosition Add(PlanogramProduct product)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(this.GetNextSequence(), product);
            this.Add(position);
            return position;
        }

        /// <summary>
        /// Creates and adds a new position to this planogram
        /// </summary>
        public PlanogramPosition Add(PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(this.GetNextSequence(), product, subComponentPlacement);
            this.Add(position);
            return position;
        }

        #endregion

        #region Add Range
        /// <summary>
        /// Creates an adds a series of position to this planogram
        /// </summary>
        public IEnumerable<PlanogramPosition> AddRange(IEnumerable<PlanogramProduct> products,
            PlanogramSubComponentPlacement subComponentPlacement)
        {
            List<PlanogramPosition> positions = new List<PlanogramPosition>();
            Int16 sequence = this.GetNextSequence();
            foreach (PlanogramProduct product in products)
            {
                positions.Add(PlanogramPosition.NewPlanogramPosition(sequence, product, subComponentPlacement));
                sequence++;
            }
            this.AddRange(positions);
            return positions;
        }

        #endregion

        #region Sequencing
        /// <summary>
        /// Returns the next sequence value
        /// based on the positions within this list
        /// </summary>
        public Int16 GetNextSequence()
        {
            return (Int16)(this.Count == 0 ? 1 : this.Max(p => p.Sequence) + 1);
        }

        #endregion

        #endregion
    }
}
