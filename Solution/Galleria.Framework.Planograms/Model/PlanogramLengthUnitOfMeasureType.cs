﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24953 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Resources.Language;


namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available length unit of measure types
    /// </summary>
    public enum PlanogramLengthUnitOfMeasureType
    {
        Unknown = 0, 
        Centimeters = 1, 
        Inches = 2
    }

    /// <summary>
    /// PlanogramLengthUnitOfMeasureType Helper Class
    /// </summary>
    public static class PlanogramLengthUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramLengthUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<PlanogramLengthUnitOfMeasureType, String>()
            {
                {PlanogramLengthUnitOfMeasureType.Unknown, String.Empty}, 
                {PlanogramLengthUnitOfMeasureType.Centimeters, Message.Enum_PlanogramLengthUnitOfMeasureType_Centimeters},
                {PlanogramLengthUnitOfMeasureType.Inches, Message.Enum_PlanogramAnnotationType_Inches},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramLengthUnitOfMeasureType, String> Abbreviations =
            new Dictionary<PlanogramLengthUnitOfMeasureType, String>()
            {
                {PlanogramLengthUnitOfMeasureType.Unknown, String.Empty},
                {PlanogramLengthUnitOfMeasureType.Centimeters, Message.Enum_PlanogramLengthUnitOfMeasureType_Centimeters_Abbrev},
                {PlanogramLengthUnitOfMeasureType.Inches, Message.Enum_PlanogramAnnotationType_Inches_Abbrev},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramLengthUnitOfMeasureType, String> AvailableValues =
            new Dictionary<PlanogramLengthUnitOfMeasureType, String>()
            {
                //{PlanogramLengthUnitOfMeasureType.Unknown, String.Empty}, //removed so that this isn't selectable in ui.
                {PlanogramLengthUnitOfMeasureType.Centimeters, Message.Enum_PlanogramLengthUnitOfMeasureType_Centimeters},
                {PlanogramLengthUnitOfMeasureType.Inches, Message.Enum_PlanogramAnnotationType_Inches},
            };

        public static String GetLengthCubedFriendlyName(PlanogramLengthUnitOfMeasureType lengthType)
        {
            switch (lengthType)
            {
                default:
                case PlanogramLengthUnitOfMeasureType.Unknown: return null;

                case PlanogramLengthUnitOfMeasureType.Centimeters: return Message.Enum_PlanogramLengthUnitOfMeasureType_CentimetersCubed;
                case PlanogramLengthUnitOfMeasureType.Inches: return Message.Enum_PlanogramLengthUnitOfMeasureType_InchesCubed;
            }
        }

        public static String GetLengthCubedAbbreviation(PlanogramLengthUnitOfMeasureType lengthType)
        {
            switch (lengthType)
            {
                default:
                case PlanogramLengthUnitOfMeasureType.Unknown: return null;

                case PlanogramLengthUnitOfMeasureType.Centimeters: return Message.Enum_PlanogramLengthUnitOfMeasureType_CentimetersCubed_Abbrev;
                case PlanogramLengthUnitOfMeasureType.Inches: return Message.Enum_PlanogramLengthUnitOfMeasureType_InchesCubed_Abbrev;
            }
        }
    }
}
