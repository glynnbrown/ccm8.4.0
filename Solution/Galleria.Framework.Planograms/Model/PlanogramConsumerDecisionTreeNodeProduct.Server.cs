﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26706 : L.Luong
//	Created
// V8-26956 : L.Luong
//  Modified PlanogramConsumerDecisionTreeNodeProduct ProductGtin to PlanogramProductId
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramConsumerDecisionTreeNodeProduct
    {
        #region Constructors
        private PlanogramConsumerDecisionTreeNodeProduct() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static PlanogramConsumerDecisionTreeNodeProduct FetchPlanogramConsumerDecisionTreeNodeProduct
            (IDalContext dalContext, PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            return DataPortal.FetchChild<PlanogramConsumerDecisionTreeNodeProduct>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private PlanogramConsumerDecisionTreeNodeProductDto GetDataTransferObject(PlanogramConsumerDecisionTreeNode parent)
        {
            return new PlanogramConsumerDecisionTreeNodeProductDto
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramProductId = ReadProperty<Object>(PlanogramProductIdProperty),
                PlanogramConsumerDecisionTreeNodeId = parent.Id
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(PlanogramProductIdProperty, dto.PlanogramProductId);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);

        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramConsumerDecisionTreeNodeProductDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramConsumerDecisionTreeNodeProductDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramConsumerDecisionTreeNodeProductDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramConsumerDecisionTreeNodeProduct>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramConsumerDecisionTreeNodeProductDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramConsumerDecisionTreeNode parent)
        {
            batchContext.Delete<PlanogramConsumerDecisionTreeNodeProductDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
