﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27647 : L.Luong 
//   Created 
// V8-28008 : L.Ineson
//  Added EnumerateDisplayablePropertyInfos
#endregion
#region Version History: (CCM v810)
// V8-30063 : D.Pleasance
//  Added EnumerateAttributeFieldInfos to provide attributes that can be updated through update planogram task.
// V8-30023 : D.Pleasance
//  Added missing property friendly names
#endregion
#region Version History: (CCM v820)
// V8-30773 : J.Pickup
//  Added InventoryMetricType.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramInventory object
    /// </summary>
    [Serializable]
    public partial class PlanogramInventory : ModelObject<PlanogramInventory>
    {
        #region Properties
        
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return (Planogram)base.Parent; }
        }
        #endregion

        #region DaysOfPerformance
        /// <summary>
        /// DaysOfPerformance property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DaysOfPerformanceProperty =
            RegisterModelProperty<Single>(c => c.DaysOfPerformance, Message.PlanogramInventory_DaysOfPerformance);
        /// <summary>
        /// DaysOfPerformance
        /// </summary>
        public Single DaysOfPerformance
        {
            get { return GetProperty<Single>(DaysOfPerformanceProperty); }
            set 
            {
                SetProperty<Single>(DaysOfPerformanceProperty, value);

                //update the planogram performance
                Planogram planogram = this.Parent;
                if (planogram != null && planogram.Performance != null)
                {
                    planogram.Performance.UpdateAllInventoryValues();
                }
            }
        }
        #endregion

        #region MinCasePacks
        /// <summary>
        /// MinCasePacks property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinCasePacksProperty =
            RegisterModelProperty<Single>(c => c.MinCasePacks, Message.PlanogramInventory_MinCasePacks);
        /// <summary>
        /// MinCasePacks
        /// </summary>
        public Single MinCasePacks
        {
            get { return GetProperty<Single>(MinCasePacksProperty); }
            set { SetProperty<Single>(MinCasePacksProperty, value); }
        }
        #endregion

        #region MinDos
        /// <summary>
        /// MinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinDosProperty =
            RegisterModelProperty<Single>(c => c.MinDos, Message.PlanogramInventory_MinDos);
        /// <summary>
        /// MinDos
        /// </summary>
        public Single MinDos
        {
            get { return GetProperty<Single>(MinDosProperty); }
            set { SetProperty<Single>(MinDosProperty, value); }
        }
        #endregion

        #region MinShelfLife
        /// <summary>
        /// MinShelfLife property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinShelfLifeProperty =
            RegisterModelProperty<Single>(c => c.MinShelfLife, Message.PlanogramInventory_MinShelfLife);
        /// <summary>
        /// MinShelfLife
        /// </summary>
        public Single MinShelfLife
        {
            get { return GetProperty<Single>(MinShelfLifeProperty); }
            set { SetProperty<Single>(MinShelfLifeProperty, value); }
        }
        #endregion

        #region MinDeliveries
        /// <summary>
        /// MinDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinDeliveriesProperty =
            RegisterModelProperty<Single>(c => c.MinDeliveries, Message.PlanogramInventory_MinDeliveries);
        /// <summary>
        /// MinDeliveries
        /// </summary>
        public Single MinDeliveries
        {
            get { return GetProperty<Single>(MinDeliveriesProperty); }
            set { SetProperty<Single>(MinDeliveriesProperty, value); }
        }
        #endregion

        #region IsCasePacksValidated
        /// <summary>
        /// IsCasePacksValidated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsCasePacksValidatedProperty =
            RegisterModelProperty<Boolean>(c => c.IsCasePacksValidated, Message.PlanogramInventory_IsCasePacksValidated);
        /// <summary>
        /// IsCasePacksValidated
        /// </summary>
        public Boolean IsCasePacksValidated
        {
            get { return GetProperty<Boolean>(IsCasePacksValidatedProperty); }
            set { SetProperty<Boolean>(IsCasePacksValidatedProperty, value); }
        }
        #endregion

        #region IsDosValidated
        /// <summary>
        /// IsDosValidated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDosValidatedProperty =
            RegisterModelProperty<Boolean>(c => c.IsDosValidated, Message.PlanogramInventory_IsDosValidated);
        /// <summary>
        /// IsDosValidated
        /// </summary>
        public Boolean IsDosValidated
        {
            get { return GetProperty<Boolean>(IsDosValidatedProperty); }
            set { SetProperty<Boolean>(IsDosValidatedProperty, value); }
        }
        #endregion

        #region IsShelfLifeValidated
        /// <summary>
        /// IsShelfLifeValidated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsShelfLifeValidatedProperty =
            RegisterModelProperty<Boolean>(c => c.IsShelfLifeValidated, Message.PlanogramInventory_IsShelfLifeValidated);
        /// <summary>
        /// IsShelfLifeValidated
        /// </summary>
        public Boolean IsShelfLifeValidated
        {
            get { return GetProperty<Boolean>(IsShelfLifeValidatedProperty); }
            set { SetProperty<Boolean>(IsShelfLifeValidatedProperty, value); }
        }
        #endregion

        #region IsDeliveriesValidated
        /// <summary>
        /// IsDeliveriesValidated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDeliveriesValidatedProperty =
            RegisterModelProperty<Boolean>(c => c.IsDeliveriesValidated, Message.PlanogramInventory_IsDeliveriesValidated);
        /// <summary>
        /// IsDeliveriesValidated
        /// </summary>
        public Boolean IsDeliveriesValidated
        {
            get { return GetProperty<Boolean>(IsDeliveriesValidatedProperty); }
            set { SetProperty<Boolean>(IsDeliveriesValidatedProperty, value); }
        }
        #endregion

        #region InventoryMetricType
        /// <summary>
        /// InventoryMetricType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramInventoryMetricType> InventoryMetricTypeProperty =
            RegisterModelProperty<PlanogramInventoryMetricType>(c => c.InventoryMetricType, Message.PlanogramInventory_InventoryMetricTypeProperty);
        /// <summary>
        /// InventoryMetricType
        /// </summary>
        public PlanogramInventoryMetricType InventoryMetricType
        {
            get { return GetProperty<PlanogramInventoryMetricType>(InventoryMetricTypeProperty); }
            set { SetProperty<PlanogramInventoryMetricType>(InventoryMetricTypeProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramInventory NewPlanogramInventory()
        {
            var item = new PlanogramInventory();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Single>(DaysOfPerformanceProperty, 7F);
            this.LoadProperty<Single>(MinCasePacksProperty, 1.5F);
            this.LoadProperty<Single>(MinDosProperty, 3F);
            this.LoadProperty<Single>(MinShelfLifeProperty, 0.8F);
            this.LoadProperty<Single>(MinDeliveriesProperty, 1.25F);

            this.LoadProperty<Boolean>(IsCasePacksValidatedProperty, true);
            this.LoadProperty<Boolean>(IsDosValidatedProperty, true);
            this.LoadProperty<Boolean>(IsShelfLifeValidatedProperty, false);
            this.LoadProperty<Boolean>(IsDeliveriesValidatedProperty, false);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<PlanogramInventory>(oldId, newId);
        }

        #endregion

        #region Criteria

        #region FetchByPlanogramIdCriteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId factory method
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> PlanogramIdProperty =
                RegisterProperty<Object>(c => c.PlanogramId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object PlanogramId
            {
                get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdCriteria(String dalFactoryName, Object planogramId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(PlanogramIdProperty, planogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePropertyInfos()
        {
            yield return DaysOfPerformanceProperty;
            yield return MinCasePacksProperty;
            yield return MinDosProperty;
            yield return MinShelfLifeProperty;
            yield return MinDeliveriesProperty;
            //yield return IsCasePacksValidatedProperty;
            //yield return IsDosValidatedProperty;
            //yield return IsShelfLifeValidatedProperty;
            //yield return IsDeliveriesValidatedProperty;
            yield return InventoryMetricTypeProperty;
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be updated via update planogram attribute task.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateAttributeFieldInfos()
        {
            Type type = typeof(PlanogramInventory);
            String typeFriendly = PlanogramInventory.FriendlyName;
            String group = Message.PlanogramInventory_PropertyGroup;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.MinCasePacksProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.MinDosProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.MinShelfLifeProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.MinDeliveriesProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.IsCasePacksValidatedProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.IsDosValidatedProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.IsShelfLifeValidatedProperty, group);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanogramInventory.IsDeliveriesValidatedProperty, group);
        }

        #endregion
    }
}
