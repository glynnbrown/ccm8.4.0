﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Types of X (horizontal) renumber strategies.
    /// </summary>
    [Serializable]
    public enum PlanogramRenumberingStrategyXRenumberType
    {
        LeftToRight,
        RightToLeft
    }

    public static class PlanogramRenumberingStrategyXRenumberTypeHelper
    {
        /// <summary>
        ///     Maps each value to its friendly name.
        /// </summary>
        public static readonly Dictionary<PlanogramRenumberingStrategyXRenumberType, String>  FriendlyNames =
            new Dictionary<PlanogramRenumberingStrategyXRenumberType, String>
            {
                {PlanogramRenumberingStrategyXRenumberType.LeftToRight, "Left to Right"},
                {PlanogramRenumberingStrategyXRenumberType.RightToLeft, "Right to Left"}
            };
    }
}
