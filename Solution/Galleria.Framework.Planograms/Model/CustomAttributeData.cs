﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26147 : L.Ineson
//	Created (Auto-generated)
#endregion

#region Version History: CCM 801
// V8-27494 : A.Kuszyk
//  Added UpdateFrom method.
#endregion

#region Version History : CCM820
// V8-30968 : A.Kuszyk
//  Commented out rule supression in UpdateFrom to fix property changed notification issue.
#endregion

#region Version History : CCM830
// V8-31612 : A.Kuszyk
//  Performance improvements to UpdateFrom method.
#endregion

#region Version History : CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Globalization;
using AutoMapper;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;
using System.Reflection;
using Csla.Core;
using System.Linq;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// CustomAttributeData Model object
    /// </summary>
    [Serializable]
    public sealed partial class CustomAttributeData : ModelObject<CustomAttributeData>, ICustomAttributeData
    {
        #region Static Constructor
        static CustomAttributeData()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Fields

        /// <summary>
        /// A dictionary of the properties of <see cref="CustomAttributeData"/> keyed by their name.
        /// </summary>
        private static Dictionary<String, PropertyInfo> _propertyInfosByName = null;

        /// <summary>
        /// A dictionary of <see cref="PropertyInfo"/>s keyed by their type, for all the types that <see cref="CustomAttributeData"/>
        /// has had to update from.
        /// </summary>
        private static Dictionary<Type, List<PropertyInfo>> _sourcePropertiesLookup = new Dictionary<Type, List<PropertyInfo>>();

        #endregion

        #region Properties

        #region Static Properties

        /// <summary>
        /// A dictionary of the properties of <see cref="CustomAttributeData"/> keyed by their name.
        /// </summary>
        private static Dictionary<String, PropertyInfo> PropertyInfosByName
        {
            get
            {
                if (_propertyInfosByName == null)
                {
                    _propertyInfosByName =
                        typeof(CustomAttributeData)
                        .GetProperties()
                        .Where(p => p.CanWrite)
                        .ToDictionary(p => p.Name, p => p);
                }
                return _propertyInfosByName;
            }
        }

        #endregion

        #region Text Properties

        #region Text1

        /// <summary>
        /// Text1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
            RegisterModelProperty<String>(c => c.Text1,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "01"));

        /// <summary>
        /// Gets/Sets the Text1 value
        /// </summary>
        public String Text1
        {
            get { return GetProperty<String>(Text1Property); }
            set { SetProperty<String>(Text1Property, value); }
        }

        #endregion

        #region Text2

        /// <summary>
        /// Text2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
            RegisterModelProperty<String>(c => c.Text2,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "02"));

        /// <summary>
        /// Gets/Sets the Text2 value
        /// </summary>
        public String Text2
        {
            get { return GetProperty<String>(Text2Property); }
            set { SetProperty<String>(Text2Property, value); }
        }

        #endregion

        #region Text3

        /// <summary>
        /// Text3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
            RegisterModelProperty<String>(c => c.Text3,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "03"));

        /// <summary>
        /// Gets/Sets the Text3 value
        /// </summary>
        public String Text3
        {
            get { return GetProperty<String>(Text3Property); }
            set { SetProperty<String>(Text3Property, value); }
        }

        #endregion

        #region Text4

        /// <summary>
        /// Text4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
            RegisterModelProperty<String>(c => c.Text4,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "04"));

        /// <summary>
        /// Gets/Sets the Text4 value
        /// </summary>
        public String Text4
        {
            get { return GetProperty<String>(Text4Property); }
            set { SetProperty<String>(Text4Property, value); }
        }

        #endregion

        #region Text5

        /// <summary>
        /// Text5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
            RegisterModelProperty<String>(c => c.Text5,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "05"));

        /// <summary>
        /// Gets/Sets the Text5 value
        /// </summary>
        public String Text5
        {
            get { return GetProperty<String>(Text5Property); }
            set { SetProperty<String>(Text5Property, value); }
        }

        #endregion

        #region Text6

        /// <summary>
        /// Text6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text6Property =
            RegisterModelProperty<String>(c => c.Text6,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "06"));

        /// <summary>
        /// Gets/Sets the Text6 value
        /// </summary>
        public String Text6
        {
            get { return GetProperty<String>(Text6Property); }
            set { SetProperty<String>(Text6Property, value); }
        }

        #endregion

        #region Text7

        /// <summary>
        /// Text7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text7Property =
            RegisterModelProperty<String>(c => c.Text7,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "07"));

        /// <summary>
        /// Gets/Sets the Text7 value
        /// </summary>
        public String Text7
        {
            get { return GetProperty<String>(Text7Property); }
            set { SetProperty<String>(Text7Property, value); }
        }

        #endregion

        #region Text8

        /// <summary>
        /// Text8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text8Property =
            RegisterModelProperty<String>(c => c.Text8,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "08"));

        /// <summary>
        /// Gets/Sets the Text8 value
        /// </summary>
        public String Text8
        {
            get { return GetProperty<String>(Text8Property); }
            set { SetProperty<String>(Text8Property, value); }
        }

        #endregion

        #region Text9

        /// <summary>
        /// Text9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text9Property =
            RegisterModelProperty<String>(c => c.Text9,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, "09"));

        /// <summary>
        /// Gets/Sets the Text9 value
        /// </summary>
        public String Text9
        {
            get { return GetProperty<String>(Text9Property); }
            set { SetProperty<String>(Text9Property, value); }
        }

        #endregion

        #region Text10

        /// <summary>
        /// Text10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text10Property =
            RegisterModelProperty<String>(c => c.Text10,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 10));

        /// <summary>
        /// Gets/Sets the Text10 value
        /// </summary>
        public String Text10
        {
            get { return GetProperty<String>(Text10Property); }
            set { SetProperty<String>(Text10Property, value); }
        }

        #endregion

        #region Text11

        /// <summary>
        /// Text11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text11Property =
            RegisterModelProperty<String>(c => c.Text11,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 11));

        /// <summary>
        /// Gets/Sets the Text11 value
        /// </summary>
        public String Text11
        {
            get { return GetProperty<String>(Text11Property); }
            set { SetProperty<String>(Text11Property, value); }
        }

        #endregion

        #region Text12

        /// <summary>
        /// Text12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text12Property =
            RegisterModelProperty<String>(c => c.Text12,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 12));

        /// <summary>
        /// Gets/Sets the Text12 value
        /// </summary>
        public String Text12
        {
            get { return GetProperty<String>(Text12Property); }
            set { SetProperty<String>(Text12Property, value); }
        }

        #endregion

        #region Text13

        /// <summary>
        /// Text13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text13Property =
            RegisterModelProperty<String>(c => c.Text13,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 13));

        /// <summary>
        /// Gets/Sets the Text13 value
        /// </summary>
        public String Text13
        {
            get { return GetProperty<String>(Text13Property); }
            set { SetProperty<String>(Text13Property, value); }
        }

        #endregion

        #region Text14

        /// <summary>
        /// Text14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text14Property =
            RegisterModelProperty<String>(c => c.Text14,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 14));

        /// <summary>
        /// Gets/Sets the Text14 value
        /// </summary>
        public String Text14
        {
            get { return GetProperty<String>(Text14Property); }
            set { SetProperty<String>(Text14Property, value); }
        }

        #endregion

        #region Text15

        /// <summary>
        /// Text15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text15Property =
            RegisterModelProperty<String>(c => c.Text15,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 15));

        /// <summary>
        /// Gets/Sets the Text15 value
        /// </summary>
        public String Text15
        {
            get { return GetProperty<String>(Text15Property); }
            set { SetProperty<String>(Text15Property, value); }
        }

        #endregion

        #region Text16

        /// <summary>
        /// Text16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text16Property =
            RegisterModelProperty<String>(c => c.Text16,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 16));

        /// <summary>
        /// Gets/Sets the Text16 value
        /// </summary>
        public String Text16
        {
            get { return GetProperty<String>(Text16Property); }
            set { SetProperty<String>(Text16Property, value); }
        }

        #endregion

        #region Text17

        /// <summary>
        /// Text17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text17Property =
            RegisterModelProperty<String>(c => c.Text17,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 17));

        /// <summary>
        /// Gets/Sets the Text17 value
        /// </summary>
        public String Text17
        {
            get { return GetProperty<String>(Text17Property); }
            set { SetProperty<String>(Text17Property, value); }
        }

        #endregion

        #region Text18

        /// <summary>
        /// Text18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text18Property =
            RegisterModelProperty<String>(c => c.Text18,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 18));

        /// <summary>
        /// Gets/Sets the Text18 value
        /// </summary>
        public String Text18
        {
            get { return GetProperty<String>(Text18Property); }
            set { SetProperty<String>(Text18Property, value); }
        }

        #endregion

        #region Text19

        /// <summary>
        /// Text19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text19Property =
            RegisterModelProperty<String>(c => c.Text19,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 19));

        /// <summary>
        /// Gets/Sets the Text19 value
        /// </summary>
        public String Text19
        {
            get { return GetProperty<String>(Text19Property); }
            set { SetProperty<String>(Text19Property, value); }
        }

        #endregion

        #region Text20

        /// <summary>
        /// Text20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text20Property =
            RegisterModelProperty<String>(c => c.Text20,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 20));

        /// <summary>
        /// Gets/Sets the Text20 value
        /// </summary>
        public String Text20
        {
            get { return GetProperty<String>(Text20Property); }
            set { SetProperty<String>(Text20Property, value); }
        }

        #endregion

        #region Text21

        /// <summary>
        /// Text21 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text21Property =
            RegisterModelProperty<String>(c => c.Text21,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 21));

        /// <summary>
        /// Gets/Sets the Text21 value
        /// </summary>
        public String Text21
        {
            get { return GetProperty<String>(Text21Property); }
            set { SetProperty<String>(Text21Property, value); }
        }

        #endregion

        #region Text22

        /// <summary>
        /// Text22 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text22Property =
            RegisterModelProperty<String>(c => c.Text22,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 22));

        /// <summary>
        /// Gets/Sets the Text22 value
        /// </summary>
        public String Text22
        {
            get { return GetProperty<String>(Text22Property); }
            set { SetProperty<String>(Text22Property, value); }
        }

        #endregion

        #region Text23

        /// <summary>
        /// Text23 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text23Property =
            RegisterModelProperty<String>(c => c.Text23,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 23));

        /// <summary>
        /// Gets/Sets the Text23 value
        /// </summary>
        public String Text23
        {
            get { return GetProperty<String>(Text23Property); }
            set { SetProperty<String>(Text23Property, value); }
        }

        #endregion

        #region Text24

        /// <summary>
        /// Text24 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text24Property =
            RegisterModelProperty<String>(c => c.Text24,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 24));

        /// <summary>
        /// Gets/Sets the Text24 value
        /// </summary>
        public String Text24
        {
            get { return GetProperty<String>(Text24Property); }
            set { SetProperty<String>(Text24Property, value); }
        }

        #endregion

        #region Text25

        /// <summary>
        /// Text25 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text25Property =
            RegisterModelProperty<String>(c => c.Text25,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 25));

        /// <summary>
        /// Gets/Sets the Text25 value
        /// </summary>
        public String Text25
        {
            get { return GetProperty<String>(Text25Property); }
            set { SetProperty<String>(Text25Property, value); }
        }

        #endregion

        #region Text26

        /// <summary>
        /// Text26 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text26Property =
            RegisterModelProperty<String>(c => c.Text26,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 26));

        /// <summary>
        /// Gets/Sets the Text26 value
        /// </summary>
        public String Text26
        {
            get { return GetProperty<String>(Text26Property); }
            set { SetProperty<String>(Text26Property, value); }
        }

        #endregion

        #region Text27

        /// <summary>
        /// Text27 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text27Property =
            RegisterModelProperty<String>(c => c.Text27,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 27));

        /// <summary>
        /// Gets/Sets the Text27 value
        /// </summary>
        public String Text27
        {
            get { return GetProperty<String>(Text27Property); }
            set { SetProperty<String>(Text27Property, value); }
        }

        #endregion

        #region Text28

        /// <summary>
        /// Text28 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text28Property =
            RegisterModelProperty<String>(c => c.Text28,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 28));

        /// <summary>
        /// Gets/Sets the Text28 value
        /// </summary>
        public String Text28
        {
            get { return GetProperty<String>(Text28Property); }
            set { SetProperty<String>(Text28Property, value); }
        }

        #endregion

        #region Text29

        /// <summary>
        /// Text29 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text29Property =
            RegisterModelProperty<String>(c => c.Text29,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 29));

        /// <summary>
        /// Gets/Sets the Text29 value
        /// </summary>
        public String Text29
        {
            get { return GetProperty<String>(Text29Property); }
            set { SetProperty<String>(Text29Property, value); }
        }

        #endregion

        #region Text30

        /// <summary>
        /// Text30 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text30Property =
            RegisterModelProperty<String>(c => c.Text30,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 30));

        /// <summary>
        /// Gets/Sets the Text30 value
        /// </summary>
        public String Text30
        {
            get { return GetProperty<String>(Text30Property); }
            set { SetProperty<String>(Text30Property, value); }
        }

        #endregion

        #region Text31

        /// <summary>
        /// Text31 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text31Property =
            RegisterModelProperty<String>(c => c.Text31,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 31));

        /// <summary>
        /// Gets/Sets the Text31 value
        /// </summary>
        public String Text31
        {
            get { return GetProperty<String>(Text31Property); }
            set { SetProperty<String>(Text31Property, value); }
        }

        #endregion

        #region Text32

        /// <summary>
        /// Text32 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text32Property =
            RegisterModelProperty<String>(c => c.Text32,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 32));

        /// <summary>
        /// Gets/Sets the Text32 value
        /// </summary>
        public String Text32
        {
            get { return GetProperty<String>(Text32Property); }
            set { SetProperty<String>(Text32Property, value); }
        }

        #endregion

        #region Text33

        /// <summary>
        /// Text33 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text33Property =
            RegisterModelProperty<String>(c => c.Text33,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 33));

        /// <summary>
        /// Gets/Sets the Text33 value
        /// </summary>
        public String Text33
        {
            get { return GetProperty<String>(Text33Property); }
            set { SetProperty<String>(Text33Property, value); }
        }

        #endregion

        #region Text34

        /// <summary>
        /// Text34 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text34Property =
            RegisterModelProperty<String>(c => c.Text34,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 34));

        /// <summary>
        /// Gets/Sets the Text34 value
        /// </summary>
        public String Text34
        {
            get { return GetProperty<String>(Text34Property); }
            set { SetProperty<String>(Text34Property, value); }
        }

        #endregion

        #region Text35

        /// <summary>
        /// Text35 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text35Property =
            RegisterModelProperty<String>(c => c.Text35,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 35));

        /// <summary>
        /// Gets/Sets the Text35 value
        /// </summary>
        public String Text35
        {
            get { return GetProperty<String>(Text35Property); }
            set { SetProperty<String>(Text35Property, value); }
        }

        #endregion

        #region Text36

        /// <summary>
        /// Text36 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text36Property =
            RegisterModelProperty<String>(c => c.Text36,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 36));

        /// <summary>
        /// Gets/Sets the Text36 value
        /// </summary>
        public String Text36
        {
            get { return GetProperty<String>(Text36Property); }
            set { SetProperty<String>(Text36Property, value); }
        }

        #endregion

        #region Text37

        /// <summary>
        /// Text37 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text37Property =
            RegisterModelProperty<String>(c => c.Text37,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 37));

        /// <summary>
        /// Gets/Sets the Text37 value
        /// </summary>
        public String Text37
        {
            get { return GetProperty<String>(Text37Property); }
            set { SetProperty<String>(Text37Property, value); }
        }

        #endregion

        #region Text38

        /// <summary>
        /// Text38 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text38Property =
            RegisterModelProperty<String>(c => c.Text38,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 38));

        /// <summary>
        /// Gets/Sets the Text38 value
        /// </summary>
        public String Text38
        {
            get { return GetProperty<String>(Text38Property); }
            set { SetProperty<String>(Text38Property, value); }
        }

        #endregion

        #region Text39

        /// <summary>
        /// Text39 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text39Property =
            RegisterModelProperty<String>(c => c.Text39,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 39));

        /// <summary>
        /// Gets/Sets the Text39 value
        /// </summary>
        public String Text39
        {
            get { return GetProperty<String>(Text39Property); }
            set { SetProperty<String>(Text39Property, value); }
        }

        #endregion

        #region Text40

        /// <summary>
        /// Text40 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text40Property =
            RegisterModelProperty<String>(c => c.Text40,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 40));

        /// <summary>
        /// Gets/Sets the Text40 value
        /// </summary>
        public String Text40
        {
            get { return GetProperty<String>(Text40Property); }
            set { SetProperty<String>(Text40Property, value); }
        }

        #endregion

        #region Text41

        /// <summary>
        /// Text41 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text41Property =
            RegisterModelProperty<String>(c => c.Text41,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 41));

        /// <summary>
        /// Gets/Sets the Text41 value
        /// </summary>
        public String Text41
        {
            get { return GetProperty<String>(Text41Property); }
            set { SetProperty<String>(Text41Property, value); }
        }

        #endregion

        #region Text42

        /// <summary>
        /// Text42 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text42Property =
            RegisterModelProperty<String>(c => c.Text42,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 42));

        /// <summary>
        /// Gets/Sets the Text42 value
        /// </summary>
        public String Text42
        {
            get { return GetProperty<String>(Text42Property); }
            set { SetProperty<String>(Text42Property, value); }
        }

        #endregion

        #region Text43

        /// <summary>
        /// Text43 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text43Property =
            RegisterModelProperty<String>(c => c.Text43,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 43));

        /// <summary>
        /// Gets/Sets the Text43 value
        /// </summary>
        public String Text43
        {
            get { return GetProperty<String>(Text43Property); }
            set { SetProperty<String>(Text43Property, value); }
        }

        #endregion

        #region Text44

        /// <summary>
        /// Text44 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text44Property =
            RegisterModelProperty<String>(c => c.Text44,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 44));

        /// <summary>
        /// Gets/Sets the Text44 value
        /// </summary>
        public String Text44
        {
            get { return GetProperty<String>(Text44Property); }
            set { SetProperty<String>(Text44Property, value); }
        }

        #endregion

        #region Text45

        /// <summary>
        /// Text45 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text45Property =
            RegisterModelProperty<String>(c => c.Text45,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 45));

        /// <summary>
        /// Gets/Sets the Text45 value
        /// </summary>
        public String Text45
        {
            get { return GetProperty<String>(Text45Property); }
            set { SetProperty<String>(Text45Property, value); }
        }

        #endregion

        #region Text46

        /// <summary>
        /// Text46 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text46Property =
            RegisterModelProperty<String>(c => c.Text46,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 46));

        /// <summary>
        /// Gets/Sets the Text46 value
        /// </summary>
        public String Text46
        {
            get { return GetProperty<String>(Text46Property); }
            set { SetProperty<String>(Text46Property, value); }
        }

        #endregion

        #region Text47

        /// <summary>
        /// Text47 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text47Property =
            RegisterModelProperty<String>(c => c.Text47,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 47));

        /// <summary>
        /// Gets/Sets the Text47 value
        /// </summary>
        public String Text47
        {
            get { return GetProperty<String>(Text47Property); }
            set { SetProperty<String>(Text47Property, value); }
        }

        #endregion

        #region Text48

        /// <summary>
        /// Text48 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text48Property =
            RegisterModelProperty<String>(c => c.Text48,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 48));

        /// <summary>
        /// Gets/Sets the Text48 value
        /// </summary>
        public String Text48
        {
            get { return GetProperty<String>(Text48Property); }
            set { SetProperty<String>(Text48Property, value); }
        }

        #endregion

        #region Text49

        /// <summary>
        /// Text49 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text49Property =
            RegisterModelProperty<String>(c => c.Text49,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 49));

        /// <summary>
        /// Gets/Sets the Text49 value
        /// </summary>
        public String Text49
        {
            get { return GetProperty<String>(Text49Property); }
            set { SetProperty<String>(Text49Property, value); }
        }

        #endregion

        #region Text50

        /// <summary>
        /// Text50 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text50Property =
            RegisterModelProperty<String>(c => c.Text50,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Text, 50));

        /// <summary>
        /// Gets/Sets the Text50 value
        /// </summary>
        public String Text50
        {
            get { return GetProperty<String>(Text50Property); }
            set { SetProperty<String>(Text50Property, value); }
        }

        #endregion

        #endregion

        #region Value Properties

        #region Value1

        /// <summary>
        /// Value1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value1Property =
            RegisterModelProperty<Single>(c => c.Value1,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "01"));

        /// <summary>
        /// Gets/Sets the Value1 value
        /// </summary>
        public Single Value1
        {
            get { return GetProperty<Single>(Value1Property); }
            set { SetProperty<Single>(Value1Property, value); }
        }

        #endregion

        #region Value2

        /// <summary>
        /// Value2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value2Property =
            RegisterModelProperty<Single>(c => c.Value2,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "02"));

        /// <summary>
        /// Gets/Sets the Value2 value
        /// </summary>
        public Single Value2
        {
            get { return GetProperty<Single>(Value2Property); }
            set { SetProperty<Single>(Value2Property, value); }
        }

        #endregion

        #region Value3

        /// <summary>
        /// Value3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value3Property =
            RegisterModelProperty<Single>(c => c.Value3,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "03"));

        /// <summary>
        /// Gets/Sets the Value3 value
        /// </summary>
        public Single Value3
        {
            get { return GetProperty<Single>(Value3Property); }
            set { SetProperty<Single>(Value3Property, value); }
        }

        #endregion

        #region Value4

        /// <summary>
        /// Value4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value4Property =
            RegisterModelProperty<Single>(c => c.Value4,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "04"));

        /// <summary>
        /// Gets/Sets the Value4 value
        /// </summary>
        public Single Value4
        {
            get { return GetProperty<Single>(Value4Property); }
            set { SetProperty<Single>(Value4Property, value); }
        }

        #endregion

        #region Value5

        /// <summary>
        /// Value5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value5Property =
            RegisterModelProperty<Single>(c => c.Value5,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "05"));

        /// <summary>
        /// Gets/Sets the Value5 value
        /// </summary>
        public Single Value5
        {
            get { return GetProperty<Single>(Value5Property); }
            set { SetProperty<Single>(Value5Property, value); }
        }

        #endregion

        #region Value6

        /// <summary>
        /// Value6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value6Property =
            RegisterModelProperty<Single>(c => c.Value6,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "06"));

        /// <summary>
        /// Gets/Sets the Value6 value
        /// </summary>
        public Single Value6
        {
            get { return GetProperty<Single>(Value6Property); }
            set { SetProperty<Single>(Value6Property, value); }
        }

        #endregion

        #region Value7

        /// <summary>
        /// Value7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value7Property =
            RegisterModelProperty<Single>(c => c.Value7,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "07"));

        /// <summary>
        /// Gets/Sets the Value7 value
        /// </summary>
        public Single Value7
        {
            get { return GetProperty<Single>(Value7Property); }
            set { SetProperty<Single>(Value7Property, value); }
        }

        #endregion

        #region Value8

        /// <summary>
        /// Value8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value8Property =
            RegisterModelProperty<Single>(c => c.Value8,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "08"));

        /// <summary>
        /// Gets/Sets the Value8 value
        /// </summary>
        public Single Value8
        {
            get { return GetProperty<Single>(Value8Property); }
            set { SetProperty<Single>(Value8Property, value); }
        }

        #endregion

        #region Value9

        /// <summary>
        /// Value9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value9Property =
            RegisterModelProperty<Single>(c => c.Value9,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, "09"));

        /// <summary>
        /// Gets/Sets the Value9 value
        /// </summary>
        public Single Value9
        {
            get { return GetProperty<Single>(Value9Property); }
            set { SetProperty<Single>(Value9Property, value); }
        }

        #endregion

        #region Value10

        /// <summary>
        /// Value10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value10Property =
            RegisterModelProperty<Single>(c => c.Value10,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 10));

        /// <summary>
        /// Gets/Sets the Value10 value
        /// </summary>
        public Single Value10
        {
            get { return GetProperty<Single>(Value10Property); }
            set { SetProperty<Single>(Value10Property, value); }
        }

        #endregion

        #region Value11

        /// <summary>
        /// Value11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value11Property =
            RegisterModelProperty<Single>(c => c.Value11,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 11));

        /// <summary>
        /// Gets/Sets the Value11 value
        /// </summary>
        public Single Value11
        {
            get { return GetProperty<Single>(Value11Property); }
            set { SetProperty<Single>(Value11Property, value); }
        }

        #endregion

        #region Value12

        /// <summary>
        /// Value12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value12Property =
            RegisterModelProperty<Single>(c => c.Value12,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 12));

        /// <summary>
        /// Gets/Sets the Value12 value
        /// </summary>
        public Single Value12
        {
            get { return GetProperty<Single>(Value12Property); }
            set { SetProperty<Single>(Value12Property, value); }
        }

        #endregion

        #region Value13

        /// <summary>
        /// Value13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value13Property =
            RegisterModelProperty<Single>(c => c.Value13,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 13));

        /// <summary>
        /// Gets/Sets the Value13 value
        /// </summary>
        public Single Value13
        {
            get { return GetProperty<Single>(Value13Property); }
            set { SetProperty<Single>(Value13Property, value); }
        }

        #endregion

        #region Value14

        /// <summary>
        /// Value14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value14Property =
            RegisterModelProperty<Single>(c => c.Value14,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 14));

        /// <summary>
        /// Gets/Sets the Value14 value
        /// </summary>
        public Single Value14
        {
            get { return GetProperty<Single>(Value14Property); }
            set { SetProperty<Single>(Value14Property, value); }
        }

        #endregion

        #region Value15

        /// <summary>
        /// Value15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value15Property =
            RegisterModelProperty<Single>(c => c.Value15,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 15));

        /// <summary>
        /// Gets/Sets the Value15 value
        /// </summary>
        public Single Value15
        {
            get { return GetProperty<Single>(Value15Property); }
            set { SetProperty<Single>(Value15Property, value); }
        }

        #endregion

        #region Value16

        /// <summary>
        /// Value16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value16Property =
            RegisterModelProperty<Single>(c => c.Value16,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 16));

        /// <summary>
        /// Gets/Sets the Value16 value
        /// </summary>
        public Single Value16
        {
            get { return GetProperty<Single>(Value16Property); }
            set { SetProperty<Single>(Value16Property, value); }
        }

        #endregion

        #region Value17

        /// <summary>
        /// Value17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value17Property =
            RegisterModelProperty<Single>(c => c.Value17,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 17));

        /// <summary>
        /// Gets/Sets the Value17 value
        /// </summary>
        public Single Value17
        {
            get { return GetProperty<Single>(Value17Property); }
            set { SetProperty<Single>(Value17Property, value); }
        }

        #endregion

        #region Value18

        /// <summary>
        /// Value18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value18Property =
            RegisterModelProperty<Single>(c => c.Value18,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 18));

        /// <summary>
        /// Gets/Sets the Value18 value
        /// </summary>
        public Single Value18
        {
            get { return GetProperty<Single>(Value18Property); }
            set { SetProperty<Single>(Value18Property, value); }
        }

        #endregion

        #region Value19

        /// <summary>
        /// Value19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value19Property =
            RegisterModelProperty<Single>(c => c.Value19,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 19));

        /// <summary>
        /// Gets/Sets the Value19 value
        /// </summary>
        public Single Value19
        {
            get { return GetProperty<Single>(Value19Property); }
            set { SetProperty<Single>(Value19Property, value); }
        }

        #endregion

        #region Value20

        /// <summary>
        /// Value20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value20Property =
            RegisterModelProperty<Single>(c => c.Value20,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 20));

        /// <summary>
        /// Gets/Sets the Value20 value
        /// </summary>
        public Single Value20
        {
            get { return GetProperty<Single>(Value20Property); }
            set { SetProperty<Single>(Value20Property, value); }
        }

        #endregion

        #region Value21

        /// <summary>
        /// Value21 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value21Property =
            RegisterModelProperty<Single>(c => c.Value21,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 21));

        /// <summary>
        /// Gets/Sets the Value21 value
        /// </summary>
        public Single Value21
        {
            get { return GetProperty<Single>(Value21Property); }
            set { SetProperty<Single>(Value21Property, value); }
        }

        #endregion

        #region Value22

        /// <summary>
        /// Value22 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value22Property =
            RegisterModelProperty<Single>(c => c.Value22,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 22));

        /// <summary>
        /// Gets/Sets the Value22 value
        /// </summary>
        public Single Value22
        {
            get { return GetProperty<Single>(Value22Property); }
            set { SetProperty<Single>(Value22Property, value); }
        }

        #endregion

        #region Value23

        /// <summary>
        /// Value23 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value23Property =
            RegisterModelProperty<Single>(c => c.Value23,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 23));

        /// <summary>
        /// Gets/Sets the Value23 value
        /// </summary>
        public Single Value23
        {
            get { return GetProperty<Single>(Value23Property); }
            set { SetProperty<Single>(Value23Property, value); }
        }

        #endregion

        #region Value24

        /// <summary>
        /// Value24 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value24Property =
            RegisterModelProperty<Single>(c => c.Value24,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 24));

        /// <summary>
        /// Gets/Sets the Value24 value
        /// </summary>
        public Single Value24
        {
            get { return GetProperty<Single>(Value24Property); }
            set { SetProperty<Single>(Value24Property, value); }
        }

        #endregion

        #region Value25

        /// <summary>
        /// Value25 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value25Property =
            RegisterModelProperty<Single>(c => c.Value25,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 25));

        /// <summary>
        /// Gets/Sets the Value25 value
        /// </summary>
        public Single Value25
        {
            get { return GetProperty<Single>(Value25Property); }
            set { SetProperty<Single>(Value25Property, value); }
        }

        #endregion

        #region Value26

        /// <summary>
        /// Value26 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value26Property =
            RegisterModelProperty<Single>(c => c.Value26,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 26));

        /// <summary>
        /// Gets/Sets the Value26 value
        /// </summary>
        public Single Value26
        {
            get { return GetProperty<Single>(Value26Property); }
            set { SetProperty<Single>(Value26Property, value); }
        }

        #endregion

        #region Value27

        /// <summary>
        /// Value27 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value27Property =
            RegisterModelProperty<Single>(c => c.Value27,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 27));

        /// <summary>
        /// Gets/Sets the Value27 value
        /// </summary>
        public Single Value27
        {
            get { return GetProperty<Single>(Value27Property); }
            set { SetProperty<Single>(Value27Property, value); }
        }

        #endregion

        #region Value28

        /// <summary>
        /// Value28 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value28Property =
            RegisterModelProperty<Single>(c => c.Value28,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 28));

        /// <summary>
        /// Gets/Sets the Value28 value
        /// </summary>
        public Single Value28
        {
            get { return GetProperty<Single>(Value28Property); }
            set { SetProperty<Single>(Value28Property, value); }
        }

        #endregion

        #region Value29

        /// <summary>
        /// Value29 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value29Property =
            RegisterModelProperty<Single>(c => c.Value29,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 29));

        /// <summary>
        /// Gets/Sets the Value29 value
        /// </summary>
        public Single Value29
        {
            get { return GetProperty<Single>(Value29Property); }
            set { SetProperty<Single>(Value29Property, value); }
        }

        #endregion

        #region Value30

        /// <summary>
        /// Value30 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value30Property =
            RegisterModelProperty<Single>(c => c.Value30,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 30));

        /// <summary>
        /// Gets/Sets the Value30 value
        /// </summary>
        public Single Value30
        {
            get { return GetProperty<Single>(Value30Property); }
            set { SetProperty<Single>(Value30Property, value); }
        }

        #endregion

        #region Value31

        /// <summary>
        /// Value31 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value31Property =
            RegisterModelProperty<Single>(c => c.Value31,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 31));

        /// <summary>
        /// Gets/Sets the Value31 value
        /// </summary>
        public Single Value31
        {
            get { return GetProperty<Single>(Value31Property); }
            set { SetProperty<Single>(Value31Property, value); }
        }

        #endregion

        #region Value32

        /// <summary>
        /// Value32 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value32Property =
            RegisterModelProperty<Single>(c => c.Value32,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 32));

        /// <summary>
        /// Gets/Sets the Value32 value
        /// </summary>
        public Single Value32
        {
            get { return GetProperty<Single>(Value32Property); }
            set { SetProperty<Single>(Value32Property, value); }
        }

        #endregion

        #region Value33

        /// <summary>
        /// Value33 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value33Property =
            RegisterModelProperty<Single>(c => c.Value33,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 33));

        /// <summary>
        /// Gets/Sets the Value33 value
        /// </summary>
        public Single Value33
        {
            get { return GetProperty<Single>(Value33Property); }
            set { SetProperty<Single>(Value33Property, value); }
        }

        #endregion

        #region Value34

        /// <summary>
        /// Value34 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value34Property =
            RegisterModelProperty<Single>(c => c.Value34,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 34));

        /// <summary>
        /// Gets/Sets the Value34 value
        /// </summary>
        public Single Value34
        {
            get { return GetProperty<Single>(Value34Property); }
            set { SetProperty<Single>(Value34Property, value); }
        }

        #endregion

        #region Value35

        /// <summary>
        /// Value35 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value35Property =
            RegisterModelProperty<Single>(c => c.Value35,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 35));

        /// <summary>
        /// Gets/Sets the Value35 value
        /// </summary>
        public Single Value35
        {
            get { return GetProperty<Single>(Value35Property); }
            set { SetProperty<Single>(Value35Property, value); }
        }

        #endregion

        #region Value36

        /// <summary>
        /// Value36 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value36Property =
            RegisterModelProperty<Single>(c => c.Value36,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 36));

        /// <summary>
        /// Gets/Sets the Value36 value
        /// </summary>
        public Single Value36
        {
            get { return GetProperty<Single>(Value36Property); }
            set { SetProperty<Single>(Value36Property, value); }
        }

        #endregion

        #region Value37

        /// <summary>
        /// Value37 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value37Property =
            RegisterModelProperty<Single>(c => c.Value37,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 37));

        /// <summary>
        /// Gets/Sets the Value37 value
        /// </summary>
        public Single Value37
        {
            get { return GetProperty<Single>(Value37Property); }
            set { SetProperty<Single>(Value37Property, value); }
        }

        #endregion

        #region Value38

        /// <summary>
        /// Value38 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value38Property =
            RegisterModelProperty<Single>(c => c.Value38,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 38));

        /// <summary>
        /// Gets/Sets the Value38 value
        /// </summary>
        public Single Value38
        {
            get { return GetProperty<Single>(Value38Property); }
            set { SetProperty<Single>(Value38Property, value); }
        }

        #endregion

        #region Value39

        /// <summary>
        /// Value39 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value39Property =
            RegisterModelProperty<Single>(c => c.Value39,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 39));

        /// <summary>
        /// Gets/Sets the Value39 value
        /// </summary>
        public Single Value39
        {
            get { return GetProperty<Single>(Value39Property); }
            set { SetProperty<Single>(Value39Property, value); }
        }

        #endregion

        #region Value40

        /// <summary>
        /// Value40 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value40Property =
            RegisterModelProperty<Single>(c => c.Value40,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 40));

        /// <summary>
        /// Gets/Sets the Value40 value
        /// </summary>
        public Single Value40
        {
            get { return GetProperty<Single>(Value40Property); }
            set { SetProperty<Single>(Value40Property, value); }
        }

        #endregion

        #region Value41

        /// <summary>
        /// Value41 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value41Property =
            RegisterModelProperty<Single>(c => c.Value41,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 41));

        /// <summary>
        /// Gets/Sets the Value41 value
        /// </summary>
        public Single Value41
        {
            get { return GetProperty<Single>(Value41Property); }
            set { SetProperty<Single>(Value41Property, value); }
        }

        #endregion

        #region Value42

        /// <summary>
        /// Value42 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value42Property =
            RegisterModelProperty<Single>(c => c.Value42,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 42));

        /// <summary>
        /// Gets/Sets the Value42 value
        /// </summary>
        public Single Value42
        {
            get { return GetProperty<Single>(Value42Property); }
            set { SetProperty<Single>(Value42Property, value); }
        }

        #endregion

        #region Value43

        /// <summary>
        /// Value43 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value43Property =
            RegisterModelProperty<Single>(c => c.Value43,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 43));

        /// <summary>
        /// Gets/Sets the Value43 value
        /// </summary>
        public Single Value43
        {
            get { return GetProperty<Single>(Value43Property); }
            set { SetProperty<Single>(Value43Property, value); }
        }

        #endregion

        #region Value44

        /// <summary>
        /// Value44 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value44Property =
            RegisterModelProperty<Single>(c => c.Value44,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 44));

        /// <summary>
        /// Gets/Sets the Value44 value
        /// </summary>
        public Single Value44
        {
            get { return GetProperty<Single>(Value44Property); }
            set { SetProperty<Single>(Value44Property, value); }
        }

        #endregion

        #region Value45

        /// <summary>
        /// Value45 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value45Property =
            RegisterModelProperty<Single>(c => c.Value45,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 45));

        /// <summary>
        /// Gets/Sets the Value45 value
        /// </summary>
        public Single Value45
        {
            get { return GetProperty<Single>(Value45Property); }
            set { SetProperty<Single>(Value45Property, value); }
        }

        #endregion

        #region Value46

        /// <summary>
        /// Value46 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value46Property =
            RegisterModelProperty<Single>(c => c.Value46,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 46));

        /// <summary>
        /// Gets/Sets the Value46 value
        /// </summary>
        public Single Value46
        {
            get { return GetProperty<Single>(Value46Property); }
            set { SetProperty<Single>(Value46Property, value); }
        }

        #endregion

        #region Value47

        /// <summary>
        /// Value47 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value47Property =
            RegisterModelProperty<Single>(c => c.Value47,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 47));

        /// <summary>
        /// Gets/Sets the Value47 value
        /// </summary>
        public Single Value47
        {
            get { return GetProperty<Single>(Value47Property); }
            set { SetProperty<Single>(Value47Property, value); }
        }

        #endregion

        #region Value48

        /// <summary>
        /// Value48 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value48Property =
            RegisterModelProperty<Single>(c => c.Value48,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 48));

        /// <summary>
        /// Gets/Sets the Value48 value
        /// </summary>
        public Single Value48
        {
            get { return GetProperty<Single>(Value48Property); }
            set { SetProperty<Single>(Value48Property, value); }
        }

        #endregion

        #region Value49

        /// <summary>
        /// Value49 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value49Property =
            RegisterModelProperty<Single>(c => c.Value49,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 49));

        /// <summary>
        /// Gets/Sets the Value49 value
        /// </summary>
        public Single Value49
        {
            get { return GetProperty<Single>(Value49Property); }
            set { SetProperty<Single>(Value49Property, value); }
        }

        #endregion

        #region Value50

        /// <summary>
        /// Value50 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Value50Property =
            RegisterModelProperty<Single>(c => c.Value50,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Value, 50));

        /// <summary>
        /// Gets/Sets the Value50 value
        /// </summary>
        public Single Value50
        {
            get { return GetProperty<Single>(Value50Property); }
            set { SetProperty<Single>(Value50Property, value); }
        }

        #endregion

        #endregion

        #region Flag Properties

        #region Flag1

        /// <summary>
        /// Flag1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag1Property =
            RegisterModelProperty<Boolean>(c => c.Flag1,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "01"));

        /// <summary>
        /// Gets/Sets the Flag1 value
        /// </summary>
        public Boolean Flag1
        {
            get { return GetProperty<Boolean>(Flag1Property); }
            set { SetProperty<Boolean>(Flag1Property, value); }
        }

        #endregion

        #region Flag2

        /// <summary>
        /// Flag2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag2Property =
            RegisterModelProperty<Boolean>(c => c.Flag2,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "02"));

        /// <summary>
        /// Gets/Sets the Flag2 value
        /// </summary>
        public Boolean Flag2
        {
            get { return GetProperty<Boolean>(Flag2Property); }
            set { SetProperty<Boolean>(Flag2Property, value); }
        }

        #endregion

        #region Flag3

        /// <summary>
        /// Flag3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag3Property =
            RegisterModelProperty<Boolean>(c => c.Flag3,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "03"));

        /// <summary>
        /// Gets/Sets the Flag3 value
        /// </summary>
        public Boolean Flag3
        {
            get { return GetProperty<Boolean>(Flag3Property); }
            set { SetProperty<Boolean>(Flag3Property, value); }
        }

        #endregion

        #region Flag4

        /// <summary>
        /// Flag4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag4Property =
            RegisterModelProperty<Boolean>(c => c.Flag4,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "04"));

        /// <summary>
        /// Gets/Sets the Flag4 value
        /// </summary>
        public Boolean Flag4
        {
            get { return GetProperty<Boolean>(Flag4Property); }
            set { SetProperty<Boolean>(Flag4Property, value); }
        }

        #endregion

        #region Flag5

        /// <summary>
        /// Flag5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag5Property =
            RegisterModelProperty<Boolean>(c => c.Flag5,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "05"));

        /// <summary>
        /// Gets/Sets the Flag5 value
        /// </summary>
        public Boolean Flag5
        {
            get { return GetProperty<Boolean>(Flag5Property); }
            set { SetProperty<Boolean>(Flag5Property, value); }
        }

        #endregion

        #region Flag6

        /// <summary>
        /// Flag6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag6Property =
            RegisterModelProperty<Boolean>(c => c.Flag6,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "06"));

        /// <summary>
        /// Gets/Sets the Flag6 value
        /// </summary>
        public Boolean Flag6
        {
            get { return GetProperty<Boolean>(Flag6Property); }
            set { SetProperty<Boolean>(Flag6Property, value); }
        }

        #endregion

        #region Flag7

        /// <summary>
        /// Flag7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag7Property =
            RegisterModelProperty<Boolean>(c => c.Flag7,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "07"));

        /// <summary>
        /// Gets/Sets the Flag7 value
        /// </summary>
        public Boolean Flag7
        {
            get { return GetProperty<Boolean>(Flag7Property); }
            set { SetProperty<Boolean>(Flag7Property, value); }
        }

        #endregion

        #region Flag8

        /// <summary>
        /// Flag8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag8Property =
            RegisterModelProperty<Boolean>(c => c.Flag8,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "08"));

        /// <summary>
        /// Gets/Sets the Flag8 value
        /// </summary>
        public Boolean Flag8
        {
            get { return GetProperty<Boolean>(Flag8Property); }
            set { SetProperty<Boolean>(Flag8Property, value); }
        }

        #endregion

        #region Flag9

        /// <summary>
        /// Flag9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag9Property =
            RegisterModelProperty<Boolean>(c => c.Flag9,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, "09"));

        /// <summary>
        /// Gets/Sets the Flag9 value
        /// </summary>
        public Boolean Flag9
        {
            get { return GetProperty<Boolean>(Flag9Property); }
            set { SetProperty<Boolean>(Flag9Property, value); }
        }

        #endregion

        #region Flag10

        /// <summary>
        /// Flag10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Flag10Property =
            RegisterModelProperty<Boolean>(c => c.Flag10,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Flag, 10));

        /// <summary>
        /// Gets/Sets the Flag10 value
        /// </summary>
        public Boolean Flag10
        {
            get { return GetProperty<Boolean>(Flag10Property); }
            set { SetProperty<Boolean>(Flag10Property, value); }
        }

        #endregion

        #endregion

        #region Date Properties

        #region Date1

        /// <summary>
        /// Date1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date1Property =
            RegisterModelProperty<DateTime?>(c => c.Date1,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "01"));

        /// <summary>
        /// Gets/Sets the Date1 value
        /// </summary>
        public DateTime? Date1
        {
            get { return GetProperty<DateTime?>(Date1Property); }
            set { SetProperty<DateTime?>(Date1Property, value); }
        }

        #endregion

        #region Date2

        /// <summary>
        /// Date2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date2Property =
            RegisterModelProperty<DateTime?>(c => c.Date2,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "02"));

        /// <summary>
        /// Gets/Sets the Date2 value
        /// </summary>
        public DateTime? Date2
        {
            get { return GetProperty<DateTime?>(Date2Property); }
            set { SetProperty<DateTime?>(Date2Property, value); }
        }

        #endregion

        #region Date3

        /// <summary>
        /// Date3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date3Property =
            RegisterModelProperty<DateTime?>(c => c.Date3,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "03"));

        /// <summary>
        /// Gets/Sets the Date3 value
        /// </summary>
        public DateTime? Date3
        {
            get { return GetProperty<DateTime?>(Date3Property); }
            set { SetProperty<DateTime?>(Date3Property, value); }
        }

        #endregion

        #region Date4

        /// <summary>
        /// Date4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date4Property =
            RegisterModelProperty<DateTime?>(c => c.Date4,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "04"));

        /// <summary>
        /// Gets/Sets the Date4 value
        /// </summary>
        public DateTime? Date4
        {
            get { return GetProperty<DateTime?>(Date4Property); }
            set { SetProperty<DateTime?>(Date4Property, value); }
        }

        #endregion

        #region Date5

        /// <summary>
        /// Date5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date5Property =
            RegisterModelProperty<DateTime?>(c => c.Date5,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "05"));

        /// <summary>
        /// Gets/Sets the Date5 value
        /// </summary>
        public DateTime? Date5
        {
            get { return GetProperty<DateTime?>(Date5Property); }
            set { SetProperty<DateTime?>(Date5Property, value); }
        }

        #endregion

        #region Date6

        /// <summary>
        /// Date6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date6Property =
            RegisterModelProperty<DateTime?>(c => c.Date6,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "06"));

        /// <summary>
        /// Gets/Sets the Date6 value
        /// </summary>
        public DateTime? Date6
        {
            get { return GetProperty<DateTime?>(Date6Property); }
            set { SetProperty<DateTime?>(Date6Property, value); }
        }

        #endregion

        #region Date7

        /// <summary>
        /// Date7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date7Property =
            RegisterModelProperty<DateTime?>(c => c.Date7,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "07"));

        /// <summary>
        /// Gets/Sets the Date7 value
        /// </summary>
        public DateTime? Date7
        {
            get { return GetProperty<DateTime?>(Date7Property); }
            set { SetProperty<DateTime?>(Date7Property, value); }
        }

        #endregion

        #region Date8

        /// <summary>
        /// Date8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date8Property =
            RegisterModelProperty<DateTime?>(c => c.Date8,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "08"));

        /// <summary>
        /// Gets/Sets the Date8 value
        /// </summary>
        public DateTime? Date8
        {
            get { return GetProperty<DateTime?>(Date8Property); }
            set { SetProperty<DateTime?>(Date8Property, value); }
        }

        #endregion

        #region Date9

        /// <summary>
        /// Date9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date9Property =
            RegisterModelProperty<DateTime?>(c => c.Date9,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, "09"));

        /// <summary>
        /// Gets/Sets the Date9 value
        /// </summary>
        public DateTime? Date9
        {
            get { return GetProperty<DateTime?>(Date9Property); }
            set { SetProperty<DateTime?>(Date9Property, value); }
        }

        #endregion

        #region Date10

        /// <summary>
        /// Date10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date10Property =
            RegisterModelProperty<DateTime?>(c => c.Date10,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Date, 10));

        /// <summary>
        /// Gets/Sets the Date10 value
        /// </summary>
        public DateTime? Date10
        {
            get { return GetProperty<DateTime?>(Date10Property); }
            set { SetProperty<DateTime?>(Date10Property, value); }
        }

        #endregion

        #endregion

        #region Note Properties

        #region Note1

        /// <summary>
        /// Note1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note1Property =
            RegisterModelProperty<String>(c => c.Note1,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Note, "01"));

        /// <summary>
        /// Gets/Sets the Note1 value
        /// </summary>
        public String Note1
        {
            get { return GetProperty<String>(Note1Property); }
            set { SetProperty<String>(Note1Property, value); }
        }

        #endregion

        #region Note2

        /// <summary>
        /// Note2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note2Property =
            RegisterModelProperty<String>(c => c.Note2,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Note, "02"));

        /// <summary>
        /// Gets/Sets the Note2 value
        /// </summary>
        public String Note2
        {
            get { return GetProperty<String>(Note2Property); }
            set { SetProperty<String>(Note2Property, value); }
        }

        #endregion

        #region Note3

        /// <summary>
        /// Note3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note3Property =
            RegisterModelProperty<String>(c => c.Note3,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Note, "03"));

        /// <summary>
        /// Gets/Sets the Note3 value
        /// </summary>
        public String Note3
        {
            get { return GetProperty<String>(Note3Property); }
            set { SetProperty<String>(Note3Property, value); }
        }

        #endregion

        #region Note4

        /// <summary>
        /// Note4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note4Property =
            RegisterModelProperty<String>(c => c.Note4,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Note, "04"));

        /// <summary>
        /// Gets/Sets the Note4 value
        /// </summary>
        public String Note4
        {
            get { return GetProperty<String>(Note4Property); }
            set { SetProperty<String>(Note4Property, value); }
        }

        #endregion

        #region Note5

        /// <summary>
        /// Note5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note5Property =
            RegisterModelProperty<String>(c => c.Note5,
            String.Format(CultureInfo.CurrentCulture, Message.CustomAttributeData_Note, "05"));

        /// <summary>
        /// Gets/Sets the Note5 value
        /// </summary>
        public String Note5
        {
            get { return GetProperty<String>(Note5Property); }
            set { SetProperty<String>(Note5Property, value); }
        }

        #endregion

        #endregion


        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MaxLength(Text1Property, 255));
            BusinessRules.AddRule(new MaxLength(Text2Property, 255));
            BusinessRules.AddRule(new MaxLength(Text3Property, 255));
            BusinessRules.AddRule(new MaxLength(Text4Property, 255));
            BusinessRules.AddRule(new MaxLength(Text5Property, 255));
            BusinessRules.AddRule(new MaxLength(Text6Property, 255));
            BusinessRules.AddRule(new MaxLength(Text7Property, 255));
            BusinessRules.AddRule(new MaxLength(Text8Property, 255));
            BusinessRules.AddRule(new MaxLength(Text9Property, 255));
            BusinessRules.AddRule(new MaxLength(Text10Property, 255));
            BusinessRules.AddRule(new MaxLength(Text11Property, 255));
            BusinessRules.AddRule(new MaxLength(Text12Property, 255));
            BusinessRules.AddRule(new MaxLength(Text13Property, 255));
            BusinessRules.AddRule(new MaxLength(Text14Property, 255));
            BusinessRules.AddRule(new MaxLength(Text15Property, 255));
            BusinessRules.AddRule(new MaxLength(Text16Property, 255));
            BusinessRules.AddRule(new MaxLength(Text17Property, 255));
            BusinessRules.AddRule(new MaxLength(Text18Property, 255));
            BusinessRules.AddRule(new MaxLength(Text19Property, 255));
            BusinessRules.AddRule(new MaxLength(Text20Property, 255));
            BusinessRules.AddRule(new MaxLength(Text21Property, 255));
            BusinessRules.AddRule(new MaxLength(Text22Property, 255));
            BusinessRules.AddRule(new MaxLength(Text23Property, 255));
            BusinessRules.AddRule(new MaxLength(Text24Property, 255));
            BusinessRules.AddRule(new MaxLength(Text25Property, 255));
            BusinessRules.AddRule(new MaxLength(Text26Property, 255));
            BusinessRules.AddRule(new MaxLength(Text27Property, 255));
            BusinessRules.AddRule(new MaxLength(Text28Property, 255));
            BusinessRules.AddRule(new MaxLength(Text29Property, 255));
            BusinessRules.AddRule(new MaxLength(Text30Property, 255));
            BusinessRules.AddRule(new MaxLength(Text31Property, 255));
            BusinessRules.AddRule(new MaxLength(Text32Property, 255));
            BusinessRules.AddRule(new MaxLength(Text33Property, 255));
            BusinessRules.AddRule(new MaxLength(Text34Property, 255));
            BusinessRules.AddRule(new MaxLength(Text35Property, 255));
            BusinessRules.AddRule(new MaxLength(Text36Property, 255));
            BusinessRules.AddRule(new MaxLength(Text37Property, 255));
            BusinessRules.AddRule(new MaxLength(Text38Property, 255));
            BusinessRules.AddRule(new MaxLength(Text39Property, 255));
            BusinessRules.AddRule(new MaxLength(Text40Property, 255));
            BusinessRules.AddRule(new MaxLength(Text41Property, 255));
            BusinessRules.AddRule(new MaxLength(Text42Property, 255));
            BusinessRules.AddRule(new MaxLength(Text43Property, 255));
            BusinessRules.AddRule(new MaxLength(Text44Property, 255));
            BusinessRules.AddRule(new MaxLength(Text45Property, 255));
            BusinessRules.AddRule(new MaxLength(Text46Property, 255));
            BusinessRules.AddRule(new MaxLength(Text47Property, 255));
            BusinessRules.AddRule(new MaxLength(Text48Property, 255));
            BusinessRules.AddRule(new MaxLength(Text49Property, 255));
            BusinessRules.AddRule(new MaxLength(Text50Property, 255));

            BusinessRules.AddRule(new MaxLength(Note1Property, 1000));
            BusinessRules.AddRule(new MaxLength(Note2Property, 1000));
            BusinessRules.AddRule(new MaxLength(Note3Property, 1000));
            BusinessRules.AddRule(new MaxLength(Note4Property, 1000));
            BusinessRules.AddRule(new MaxLength(Note5Property, 1000));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Criteria

        #region FetchCriteria
        /// <summary>
        /// Criteria for the FetchByParentTypeParentId factory method
        /// </summary>
        [Serializable]
        public class FetchCriteria : CriteriaBase<FetchCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region ParentType
            /// <summary>
            /// ParentType property definition
            /// </summary>
            public static readonly PropertyInfo<Byte> ParentTypeProperty =
                RegisterProperty<Byte>(c => c.ParentType);
            /// <summary>
            /// Returns the parent type
            /// </summary>
            public Byte ParentType
            {
                get { return this.ReadProperty<Byte>(ParentTypeProperty); }
            }
            #endregion

            #region ParentId
            /// <summary>
            /// ParentId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> ParentIdProperty =
                RegisterProperty<Object>(c => c.ParentId);
            /// <summary>
            /// Returns the parent id
            /// </summary>
            public Object ParentId
            {
                get { return this.ReadProperty<Object>(ParentIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria(String dalFactoryName, Byte parentType, Object parentId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Byte>(ParentTypeProperty, parentType);
                this.LoadProperty<Object>(ParentIdProperty, parentId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static CustomAttributeData NewCustomAttributeData()
        {
            CustomAttributeData item = new CustomAttributeData();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Tries to get the source properties for the given <paramref name="source"/> type that match
        /// the <paramref name="propertiesToUpdate"/>. If no properties have been found yet for this type
        /// they are captured and cached in case they are required later.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="propertiesToUpdate"></param>
        /// <returns></returns>
        private IEnumerable<PropertyInfo> GetSourceProperties(Object source, String[] propertiesToUpdate)
        {
            List<PropertyInfo> sourceProperties;
            Type sourceType = source.GetType();
            if (!_sourcePropertiesLookup.TryGetValue(sourceType, out sourceProperties))
            {
                sourceProperties = sourceType.GetProperties().Where(p => !p.Name.Equals("Id")).ToList();
                _sourcePropertiesLookup.Add(sourceType, sourceProperties);
            }

            if (propertiesToUpdate.Any())
            {
                return sourceProperties.Where(p => propertiesToUpdate.Contains(p.Name));
            }
            else
            {
                return sourceProperties;
            }
        }

        /// <summary>
        /// Updates this object use the values in the given source.
        /// </summary>
        /// <param name="source">The source ICustomAttributeData to copy values from.</param>
        /// <remarks>Uses AutoMapper to copy the values from source to this.</remarks>
        public void UpdateFrom(ICustomAttributeData source, params String[] propertiesToUpdate)
        {
            // Otherwise, we iterate through the properties updating this from the source.
            foreach (PropertyInfo sourcePropertyInfo in GetSourceProperties(source, propertiesToUpdate))
            {
                // Try to get the matching property path for this property by looking for a path 
                // with the same name and Property on the end.
                PropertyInfo thisPropertyInfo;
                if (!PropertyInfosByName.TryGetValue(sourcePropertyInfo.Name, out thisPropertyInfo)) continue;

                thisPropertyInfo.SetValue(this,thisPropertyInfo.GetValue(source, null), null);
            }
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<CustomAttributeData>(oldId, newId);
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePropertyInfos()
        {

            yield return Text1Property;
            yield return Text2Property;
            yield return Text3Property;
            yield return Text4Property;
            yield return Text5Property;
            yield return Text6Property;
            yield return Text7Property;
            yield return Text8Property;
            yield return Text9Property;
            yield return Text10Property;
            yield return Text11Property;
            yield return Text12Property;
            yield return Text13Property;
            yield return Text14Property;
            yield return Text15Property;
            yield return Text16Property;
            yield return Text17Property;
            yield return Text18Property;
            yield return Text19Property;
            yield return Text20Property;
            yield return Text21Property;
            yield return Text22Property;
            yield return Text23Property;
            yield return Text24Property;
            yield return Text25Property;
            yield return Text26Property;
            yield return Text27Property;
            yield return Text28Property;
            yield return Text29Property;
            yield return Text30Property;
            yield return Text31Property;
            yield return Text32Property;
            yield return Text33Property;
            yield return Text34Property;
            yield return Text35Property;
            yield return Text36Property;
            yield return Text37Property;
            yield return Text38Property;
            yield return Text39Property;
            yield return Text40Property;
            yield return Text41Property;
            yield return Text42Property;
            yield return Text43Property;
            yield return Text44Property;
            yield return Text45Property;
            yield return Text46Property;
            yield return Text47Property;
            yield return Text48Property;
            yield return Text49Property;
            yield return Text50Property;
            yield return Value1Property;
            yield return Value2Property;
            yield return Value3Property;
            yield return Value4Property;
            yield return Value5Property;
            yield return Value6Property;
            yield return Value7Property;
            yield return Value8Property;
            yield return Value9Property;
            yield return Value10Property;
            yield return Value11Property;
            yield return Value12Property;
            yield return Value13Property;
            yield return Value14Property;
            yield return Value15Property;
            yield return Value16Property;
            yield return Value17Property;
            yield return Value18Property;
            yield return Value19Property;
            yield return Value20Property;
            yield return Value21Property;
            yield return Value22Property;
            yield return Value23Property;
            yield return Value24Property;
            yield return Value25Property;
            yield return Value26Property;
            yield return Value27Property;
            yield return Value28Property;
            yield return Value29Property;
            yield return Value30Property;
            yield return Value31Property;
            yield return Value32Property;
            yield return Value33Property;
            yield return Value34Property;
            yield return Value35Property;
            yield return Value36Property;
            yield return Value37Property;
            yield return Value38Property;
            yield return Value39Property;
            yield return Value40Property;
            yield return Value41Property;
            yield return Value42Property;
            yield return Value43Property;
            yield return Value44Property;
            yield return Value45Property;
            yield return Value46Property;
            yield return Value47Property;
            yield return Value48Property;
            yield return Value49Property;
            yield return Value50Property;
            yield return Flag1Property;
            yield return Flag2Property;
            yield return Flag3Property;
            yield return Flag4Property;
            yield return Flag5Property;
            yield return Flag6Property;
            yield return Flag7Property;
            yield return Flag8Property;
            yield return Flag9Property;
            yield return Flag10Property;
            yield return Date1Property;
            yield return Date2Property;
            yield return Date3Property;
            yield return Date4Property;
            yield return Date5Property;
            yield return Date6Property;
            yield return Date7Property;
            yield return Date8Property;
            yield return Date9Property;
            yield return Date10Property;
            yield return Note1Property;
            yield return Note2Property;
            yield return Note3Property;
            yield return Note4Property;
            yield return Note5Property;
        }

        #endregion

        #region Mapping
        /// <summary>
        /// Creates all mappings for this type
        /// </summary>        
        private static void CreateMappings()
        {
            // interface to model
            Mapper.CreateMap<ICustomAttributeData, CustomAttributeData>()
                .ForMember(dest => dest.Id, map => map.Ignore());
        }
        #endregion

        public T GetAttributeValueByName<T>(String attributeName)
        {
            PropertyInfo thisPropertyInfo;
            if (PropertyInfosByName.TryGetValue(attributeName, out thisPropertyInfo))
                return (T) Convert.ChangeType(thisPropertyInfo.GetValue(this, null), typeof(T));

            return default(T);
        }
    }
}
