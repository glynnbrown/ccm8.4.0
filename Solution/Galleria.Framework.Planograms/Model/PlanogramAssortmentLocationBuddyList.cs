﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentLocationBuddyList : ModelList<PlanogramAssortmentLocationBuddyList, PlanogramAssortmentLocationBuddy>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent objecta
        /// </summary>
        public new PlanogramAssortment Parent
        {
            get { return (PlanogramAssortment)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentLocationBuddyList NewPlanogramAssortmentLocationBuddyList()
        {
            var item = new PlanogramAssortmentLocationBuddyList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        public void AddRange(IEnumerable<IPlanogramAssortmentLocationBuddy> buddies)
        {
            base.AddRange(buddies.Select(b => PlanogramAssortmentLocationBuddy.NewPlanogramAssortmentLocationBuddy(b)));
        }

        #endregion
    }
}
