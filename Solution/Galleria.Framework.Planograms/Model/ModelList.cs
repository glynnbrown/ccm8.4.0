﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-27062 : A.Silva ~ Added base implementation of CalculateValidationData.
// V8-27058 : A.Probyn 
//  Extended for ClearMetaData
// V8-27237 : A.Silva
//  Added base implementation of ClearValidationData.
// V8-27490 : N.Foster
//  Refactory fast-lookup code and moved into framework
#endregion
#region Version History : CCM810
// V8-30519 : L.Ineson
//  Added Child changing - its here temporarily so that it only affects the planogram model 
//  but later on it can be moved to the framework for all model objects.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using System.ComponentModel;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Defines a base model list class from
    /// which the other model lists in
    /// this assembly will inherit
    /// </summary>
    [Serializable]
    public abstract partial class ModelList<TList, TItem> : Framework.Model.ModelList<TList, TItem>, IModelList, INotifyChildChanging
        where TList : ModelList<TList, TItem>
        where TItem : ModelObject<TItem>
    {
        #region Properties

        #region DalFactoryName
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected virtual String DalFactoryName
        {
            get
            {
                // attempt to get the name from a parent model object
                var modelObject = Parent as IModelObject;
                if (modelObject != null) return modelObject.DalFactoryName;

                // attempt to get the name from a parent model list
                var modelList = Parent as IModelList;
                if (modelList != null) return modelList.DalFactoryName;

                // throw an exception
                // NF - TODO
                throw new InvalidOperationException("The dal factory name could not be determined");
            }
        }

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String IModelList.DalFactoryName
        {
            get { return DalFactoryName; }
        }
        #endregion

        #endregion

        #region Criteria
        /// <summary>
        /// Generic criteria used when fetching a list
        /// by its parent id
        /// </summary>
        [Serializable]
        public class FetchByParentIdCriteria : CriteriaBase<FetchByParentIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return ReadProperty(DalFactoryNameProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// ParentId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> ParentIdProperty =
                RegisterProperty<Object>(c => c.ParentId);
            /// <summary>
            /// Retusn the parent id property
            /// </summary>
            public Object ParentId
            {
                get { return ReadProperty(ParentIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByParentIdCriteria(String dalFactoryName, Object parentId)
            {
                LoadProperty(DalFactoryNameProperty, dalFactoryName);
                LoadProperty(ParentIdProperty, parentId);
            }
            #endregion
        }
        #endregion

        #region Methods

        #region Overrides

        /// <summary>
        /// Hook into child object events.
        /// </summary>
        protected override void OnAddEventHooks(TItem item)
        {
            base.OnAddEventHooks(item);

            INotifyPropertyChanging pc = item as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging += Child_PropertyChanging;
            }

            INotifyChildChanging cc = item as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging += Child_Changing;
            }
        }

        /// <summary>
        /// Unhook child object events.
        /// </summary>
        protected override void OnRemoveEventHooks(TItem item)
        {
            base.OnRemoveEventHooks(item);

            INotifyPropertyChanging pc = item as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyChildChanging cc = item as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging -= Child_Changing;
            }
        }

        #endregion

        #region GetDalFactory
        /// <summary>
        /// Returns the correct dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(DalFactoryName);
        }

        /// <summary>
        /// Returns the specified dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory(String dalName)
        {
            return DalContainer.GetDalFactory(dalName);
        }
        #endregion

        #region Metadata
        /// <summary>
        /// Calculates metadata for this instance
        /// </summary>
        void IModelList.CalculateMetadata()
        {
            foreach (var item in this)
            {
                IModelObject modelObject = item;
                modelObject.CalculateMetadata();
            }
        }

        /// <summary>
        /// Clears metadata for this instance
        /// </summary>
        void IModelList.ClearMetadata()
        {
            foreach (var item in this)
            {
                IModelObject modelObject = item;
                modelObject.ClearMetadata();
            }
        }
        #endregion

        #region ValidationData
        /// <summary>
        /// Calculates the validation data for this instance's items, if they implement it.
        /// </summary>
        /// <remarks>
        /// Implemented explicitly to hide this method unless accessed through the interface.
        /// </remarks>
        void IModelList.CalculateValidationData()
        {
            var modelObjects = this.Select(item => item as IModelObject);
            foreach (var modelObject in modelObjects)
            {
                modelObject.CalculateValidationData();
            }
        }

        /// <summary>
        /// Clears all validation data for this instance.
        /// </summary>
        void IModelList.ClearValidationData()
        {
            var modelObjects = this.Select(item => item as IModelObject);
            foreach (var modelObject in modelObjects)
            {
                modelObject.ClearValidationData();
            }
        }
        #endregion

        /// <summary>
        /// Recurses through the entire model tree looking for the object
        /// with the given type and id.
        /// </summary>
        public object FindChildModelById(Type childModelType, object id)
        {
            var modelObjects = this.Select(item => item as IModelObject);
            foreach (var modelObject in modelObjects)
            {
               var foundChild = modelObject.FindChildModelById(childModelType, id);
               if (foundChild != null) return foundChild;
            }
            return null;
        }

        #region Child Changing Notification

        //TODO: This needs moving into the framework model object.

        [NonSerialized]
        [Csla.NotUndoable]
        private EventHandler<ChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Event raised when a child object is about to change.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public event EventHandler<ChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        /// <summary>
        /// Raises the child changing event to indicate that a child object is about to change.
        /// </summary>
        /// <param name="e">ChildChangingEventArgs object</param>
        protected virtual void OnChildChanging(ChildChangingEventArgs e)
        {
            if (_childChangingHandlers != null)
                _childChangingHandlers.Invoke(this, e);
        }

        /// <summary>
        /// Creates a ChildChangingEventArgs and raises the event. 
        /// </summary>
        private void RaiseChildChanging(object childObject, PropertyChangingEventArgs propertyArgs)
        {
            ChildChangingEventArgs args = new ChildChangingEventArgs(childObject, propertyArgs);
            OnChildChanging(args);
        }

        /// <summary>
        /// Raises the event
        /// </summary>
        private void RaiseChildChanging(ChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Handles any PropertyChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            RaiseChildChanging(sender, e);
        }

        /// <summary>
        /// Handles any ChildChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_Changing(object sender, ChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        #endregion

        #endregion

    }
}