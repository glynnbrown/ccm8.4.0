﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550: A.Probyn
//	Copied from SA.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using System.Diagnostics;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductBuddyProductAttributeType
    {
        CasePackUnits = 0,
        SubCategory = 1,
        Colour = 2,
        Flavour = 3,
        PackagingShape = 4,
        PackagingType = 5,
        CountryOfOrigin = 6,
        CountryOfProcessing = 7,
        ShelfLife = 8,
        DeliveryMethod = 9,
        Brand = 10,
        Vendor = 11,
        Manufacturer = 12,
        Size = 13,
        Health = 14,
        SellPrice = 15,
        CostPrice = 16,
        RecommendedRetailPrice = 17,
        CaseCost = 18,
        Texture = 19,
        StyleNumber = 20,
        Pattern = 21,
        Model = 22,
        GarmentType = 23,
        IsPrivateLabel = 24
    }
    
    public static class PlanogramAssortmentProductBuddyProductAttributeTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductBuddyProductAttributeType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentProductBuddyProductAttributeType, String>()
            {
                    {PlanogramAssortmentProductBuddyProductAttributeType.CasePackUnits, PlanogramProduct.CasePackUnitsProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.SubCategory, PlanogramProduct.SubcategoryProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Colour, PlanogramProduct.ColourProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Flavour, PlanogramProduct.FlavourProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.PackagingShape,PlanogramProduct.PackagingShapeProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.PackagingType, PlanogramProduct.PackagingTypeProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.CountryOfOrigin, PlanogramProduct.CountryOfOriginProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.CountryOfProcessing, PlanogramProduct.CountryOfProcessingProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.ShelfLife, PlanogramProduct.ShelfLifeProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.DeliveryMethod, PlanogramProduct.DeliveryMethodProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Brand, PlanogramProduct.BrandProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Vendor, PlanogramProduct.VendorProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Manufacturer, PlanogramProduct.ManufacturerProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Size, PlanogramProduct.SizeProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Health, PlanogramProduct.HealthProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.SellPrice, PlanogramProduct.SellPriceProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.CostPrice, PlanogramProduct.CostPriceProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.RecommendedRetailPrice, PlanogramProduct.RecommendedRetailPriceProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.CaseCost, PlanogramProduct.CaseCostProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Texture, PlanogramProduct.TextureProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.StyleNumber, PlanogramProduct.StyleNumberProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Pattern, PlanogramProduct.PatternProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.Model, PlanogramProduct.ModelProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.GarmentType, PlanogramProduct.GarmentTypeProperty.FriendlyName},
                    {PlanogramAssortmentProductBuddyProductAttributeType.IsPrivateLabel, PlanogramProduct.IsPrivateLabelProperty.FriendlyName}
            };

        public static Object GetValue(IPlanogramProduct product, PlanogramAssortmentProductBuddyProductAttributeType attributeType)
        {
            switch (attributeType)
            {
                case PlanogramAssortmentProductBuddyProductAttributeType.Brand:
                    return product.Brand;
                case PlanogramAssortmentProductBuddyProductAttributeType.CaseCost:
                    return product.CaseCost;
                case PlanogramAssortmentProductBuddyProductAttributeType.CasePackUnits:
                    return product.CasePackUnits;
                case PlanogramAssortmentProductBuddyProductAttributeType.Colour:
                    return product.Colour;
                case PlanogramAssortmentProductBuddyProductAttributeType.CostPrice:
                    return product.CostPrice;
                case PlanogramAssortmentProductBuddyProductAttributeType.CountryOfOrigin:
                    return product.CountryOfOrigin;
                case PlanogramAssortmentProductBuddyProductAttributeType.CountryOfProcessing:
                    return product.CountryOfProcessing;
                case PlanogramAssortmentProductBuddyProductAttributeType.DeliveryMethod:
                    return product.DeliveryMethod;
                case PlanogramAssortmentProductBuddyProductAttributeType.Flavour:
                    return product.Flavour;
                case PlanogramAssortmentProductBuddyProductAttributeType.GarmentType:
                    return product.GarmentType;
                case PlanogramAssortmentProductBuddyProductAttributeType.Health:
                    return product.Health;
                case PlanogramAssortmentProductBuddyProductAttributeType.IsPrivateLabel:
                    return product.IsPrivateLabel;
                case PlanogramAssortmentProductBuddyProductAttributeType.Manufacturer:
                    return product.Manufacturer;
                case PlanogramAssortmentProductBuddyProductAttributeType.Model:
                    return product.Model;
                case PlanogramAssortmentProductBuddyProductAttributeType.PackagingShape:
                    return product.PackagingShape;
                case PlanogramAssortmentProductBuddyProductAttributeType.PackagingType:
                    return product.PackagingType;
                case PlanogramAssortmentProductBuddyProductAttributeType.Pattern:
                    return product.Pattern;
                case PlanogramAssortmentProductBuddyProductAttributeType.RecommendedRetailPrice:
                    return product.RecommendedRetailPrice;
                case PlanogramAssortmentProductBuddyProductAttributeType.SellPrice:
                    return product.SellPrice;
                case PlanogramAssortmentProductBuddyProductAttributeType.ShelfLife:
                    return product.ShelfLife;
                case PlanogramAssortmentProductBuddyProductAttributeType.Size:
                    return product.Size;
                case PlanogramAssortmentProductBuddyProductAttributeType.StyleNumber:
                    return product.StyleNumber;
                case PlanogramAssortmentProductBuddyProductAttributeType.SubCategory:
                    return product.Subcategory;
                case PlanogramAssortmentProductBuddyProductAttributeType.Texture:
                    return product.Texture;
                case PlanogramAssortmentProductBuddyProductAttributeType.Vendor:
                    return product.Vendor;
                default:
                    Debug.Fail("PlanogramAssortmentProductBuddyProductAttributeType value not handled");
                    return null;
            }
        }
        
        /// <summary>
        /// Returns the matching enum value from the specified friendlyname
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanogramAssortmentProductBuddyProductAttributeType? ParseFromFriendlyName(String friendlyName, Boolean closestMatch = false)
        {
            String cleanFriendlyName = friendlyName.ToLowerInvariant().Trim();

            foreach (KeyValuePair<PlanogramAssortmentProductBuddyProductAttributeType, String> keyPair in PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.ToLowerInvariant().Equals(cleanFriendlyName))
                {
                    return keyPair.Key;
                }
            }

            if (closestMatch)
            {
                foreach (KeyValuePair<PlanogramAssortmentProductBuddyProductAttributeType, String> keyPair in PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames)
                {
                    if (keyPair.Value.ToLowerInvariant().Contains(cleanFriendlyName))
                    {
                        return keyPair.Key;
                    }
                }
            }
            return null;
        }

    }
}
