﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Changed LevelType to EntryType and added Content
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId properties. 
// V8-27765 : A.Kuszyk
//  Added ResolveId method.
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId properties.
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
// V8-30079 : L.Ineson
//  Added score parameter to new factory method.
#endregion

#region Version History: (CCM 8.3.0)
// CCM-31546 : M.Brumby
//  Added new Factory for use when displaying export logs (Apollo/JDA/Spaceman) where the
//  logs are not stored in a planogram but as a dto list.
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Model representing a PlanogramEventLog object
    [Serializable]
    public sealed partial class PlanogramEventLog : ModelObject<PlanogramEventLog>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Planogram Parent
        {
            get { return ((PlanogramEventLogList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region EventType
        /// <summary>
        /// EventType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogEventType> EventTypeProperty =
            RegisterModelProperty<PlanogramEventLogEventType>(c => c.EventType);
        /// <summary>
        /// Returns the Event type
        /// </summary>
        public PlanogramEventLogEventType EventType
        {
            get { return this.GetProperty<PlanogramEventLogEventType>(EventTypeProperty); }
            set { this.SetProperty<PlanogramEventLogEventType>(EventTypeProperty, value); }
        }
        #endregion

        #region EntryType
        /// <summary>
        /// EntryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogEntryType> EntryTypeProperty =
            RegisterModelProperty<PlanogramEventLogEntryType>(c => c.EntryType);
        /// <summary>
        /// Returns the Entry type
        /// </summary>
        public PlanogramEventLogEntryType EntryType
        {
            get { return this.GetProperty<PlanogramEventLogEntryType>(EntryTypeProperty); }
            set { this.SetProperty<PlanogramEventLogEntryType>(EntryTypeProperty, value); }
        }
        #endregion

        #region AffectedType
        /// <summary>
        /// AffectedType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogAffectedType?> AffectedTypeProperty =
            RegisterModelProperty<PlanogramEventLogAffectedType?>(c => c.AffectedType);
        /// <summary>
        /// Returns the Affected type
        /// </summary>
        public PlanogramEventLogAffectedType? AffectedType
        {
            get { return this.GetProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty); }
            set { this.SetProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty, value); }
        }
        #endregion

        #region AffectedId
        /// <summary>
        /// AffectedId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> AffectedIdProperty =
            RegisterModelProperty<Object>(c => c.AffectedId);
        /// <summary>
        /// Returns the Affected Id
        /// </summary>
        public Object AffectedId
        {
            get { return this.GetProperty<Object>(AffectedIdProperty); }
            set { this.SetProperty<Object>(AffectedIdProperty, value); }
        }
        #endregion

        #region WorkpackageSource
        /// <summary>
        /// WorkpackageSource property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> WorkpackageSourceProperty =
            RegisterModelProperty<String>(c => c.WorkpackageSource);
        /// <summary>
        /// Returns the name of the Workpackage that generated this Event Log entry.
        /// </summary>
        public String WorkpackageSource
        {
            get { return this.GetProperty<String>(WorkpackageSourceProperty); }
            set { this.SetProperty<String>(WorkpackageSourceProperty, value); }
        }
        #endregion

        #region TaskSource
        /// <summary>
        /// TaskSource property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TaskSourceProperty =
            RegisterModelProperty<String>(c => c.TaskSource);
        /// <summary>
        /// Returns the name of the Engine task that generated this Event Log entry.
        /// </summary>
        public String TaskSource
        {
            get { return this.GetProperty<String>(TaskSourceProperty); }
            set { this.SetProperty<String>(TaskSourceProperty, value); }
        }
        #endregion

        #region EventId
        /// <summary>
        /// EventId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EventIdProperty =
            RegisterModelProperty<Int32>(c => c.EventId);
        /// <summary>
        /// Returns the Id of the event that this event log entry relates to. 
        /// </summary>
        /// <remarks>
        /// Event Ids can be found in the Galleria.Ccm.Logging.EventLogEvent enum.
        /// </remarks>
        public Int32 EventId
        {
            get { return this.GetProperty<Int32>(EventIdProperty); }
            set { this.SetProperty<Int32>(EventIdProperty, value); }
        }
        #endregion

        #region DateTime
        /// <summary>
        /// DateTime property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateTimeProperty =
            RegisterModelProperty<DateTime>(c => c.DateTime);
        /// <summary>
        /// Returns the DateTime
        /// </summary>
        public DateTime DateTime
        {
            get { return this.GetProperty<DateTime>(DateTimeProperty); }
            set { this.SetProperty<DateTime>(DateTimeProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Returns the EventLog Description
        /// </summary>
        public String Description
        {
            get { return this.GetProperty<String>(DescriptionProperty); }
            set { this.SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Content
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ContentProperty =
            RegisterModelProperty<String>(c => c.Content);
        /// <summary>
        /// Returns the EventLog Description
        /// </summary>
        public String Content
        {
            get { return this.GetProperty<String>(ContentProperty); }
            set { this.SetProperty<String>(ContentProperty, value); }
        }
        #endregion

        #region Score
        /// <summary>
        /// Score property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> ScoreProperty =
            RegisterModelProperty<Byte>(c => c.Score);
        /// <summary>
        /// Returns the EventLog Score
        /// </summary>
        public Byte Score
        {
            get { return this.GetProperty<Byte>(ScoreProperty); }
            set { this.SetProperty<Byte>(ScoreProperty, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 1000));
            BusinessRules.AddRule(new MaxLength(WorkpackageSourceProperty, 100));
            BusinessRules.AddRule(new MaxLength(TaskSourceProperty, 100));

            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramEventLog NewPlanogramEventLog()
        {
            PlanogramEventLog item = new PlanogramEventLog();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates an initialized instance of this type.
        /// </summary>
        /// <returns></returns>
        public static PlanogramEventLog NewPlanogramEventLog(
            PlanogramEventLogEntryType entryType, 
            PlanogramEventLogEventType eventType, 
            PlanogramEventLogAffectedType? affectedType, 
            Object affectedId, 
            String description, 
            String content,
            String workpackageSource,
            String taskSource,
            Int32 eventId,
            Byte score)
        {
            PlanogramEventLog item = new PlanogramEventLog();
            item.Create(entryType,eventType,affectedType,affectedId,description,content, workpackageSource, taskSource, eventId, score);
            return item;
        }

        /// <summary>
        /// Creates an initialized instance of this type.
        /// </summary>
        /// <returns></returns>
        /// <remarks>Created for showing export to Apollo/JDA/Spaceman logs which are
        /// only temporarily stored as a list of dtos.</remarks>
        public static PlanogramEventLog NewPlanogramEventLog(PlanogramEventLogDto dto)
        {
            PlanogramEventLog item = new PlanogramEventLog();
            item.Create((PlanogramEventLogEntryType)dto.EntryType, 
                (PlanogramEventLogEventType)dto.EventType, 
                (PlanogramEventLogAffectedType?)dto.AffectedType, 
                dto.AffectedId, dto.Description, dto.Content, 
                dto.WorkpackageSource, dto.TaskSource, dto.EventId, dto.Score);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        
        /// <summary>
        /// Called when creating an initialized instance of this type.
        /// </summary>
        private void Create(
            PlanogramEventLogEntryType entryType, 
            PlanogramEventLogEventType eventType, 
            PlanogramEventLogAffectedType? affectedType, 
            Object affectedId, 
            String description, 
            String content,
            String workpackageSource,
            String taskSource,
            Int32 eventId,
            Byte score)
        {
            LoadProperty<PlanogramEventLogEntryType>(EntryTypeProperty, entryType);
            LoadProperty<PlanogramEventLogEventType>(EventTypeProperty, eventType);
            if(affectedType!=null) LoadProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty, affectedType);
            LoadProperty<Object>(AffectedIdProperty, affectedId);
            LoadProperty<String>(DescriptionProperty, description);
            LoadProperty<String>(ContentProperty, content);
            LoadProperty<String>(WorkpackageSourceProperty, workpackageSource);
            LoadProperty<String>(TaskSourceProperty, taskSource);
            LoadProperty<Int32>(EventIdProperty, eventId);
            LoadProperty<Byte>(ScoreProperty, score);
            this.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateTimeProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramEventLog>(oldId, newId);
        }

        /// <summary>
        /// Resolves the Id stored in the AffectedId property based in the option in AffectedType.
        /// </summary>
        /// <param name="dalContext"></param>
        private void ResolveIds(IDalContext dalContext)
        {
            if (!AffectedType.HasValue) return;
            Object newId = null;
            Type affectedTypeType = null;
            if (PlanogramEventLogAffectedTypeHelper.TypesByOption.TryGetValue(AffectedType.Value, out affectedTypeType))
            {
                newId = dalContext.ResolveId(affectedTypeType, AffectedId);
            }
            if(newId!=null) LoadProperty<Object>(AffectedIdProperty, newId);
        }

        #endregion
    }
}
