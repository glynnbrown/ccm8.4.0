﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created  - refactored from CCM.Model.PlanogramFileType
// V8-32826 : M.Pettit
//  Added ExternalTypeFriendlyNames for Combo box selectors
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramExportFileType
    {
        POG,
        JDA,
        Apollo,
        Spaceman
    }
    
    public static class PlanogramExportFileTypeHelper
    {
        //List of external type sonly (w/o POG)
        public static readonly Dictionary<PlanogramExportFileType, String> ExternalTypeFriendlyNames =
            new Dictionary<PlanogramExportFileType, String>()
            {
                {PlanogramExportFileType.Spaceman, Message.Enum_PlanogramExportFileType_Spaceman_Name },
                {PlanogramExportFileType.JDA, Message.Enum_PlanogramExportFileType_JDA_Name },
                {PlanogramExportFileType.Apollo, Message.Enum_PlanogramExportFileType_Apollo_Name}
            };
        
        public static readonly Dictionary<PlanogramExportFileType, String> FriendlyNames =
            new Dictionary<PlanogramExportFileType, String>()
            {
                {PlanogramExportFileType.POG, Message.Enum_PlanogramExportFileType_POG_Name },
                {PlanogramExportFileType.JDA, Message.Enum_PlanogramExportFileType_JDA_Name },
                {PlanogramExportFileType.Apollo, Message.Enum_PlanogramExportFileType_Apollo_Name},
                {PlanogramExportFileType.Spaceman, Message.Enum_PlanogramExportFileType_Spaceman_Name },
            };

        public static readonly Dictionary<PlanogramExportFileType, String> FriendlyDescriptions =
            new Dictionary<PlanogramExportFileType, String>()
            {
                {PlanogramExportFileType.POG, Message.Enum_PlanogramExportFileType_POG_Name },
                {PlanogramExportFileType.JDA, Message.Enum_PlanogramExportFileType_JDA_Name },
                {PlanogramExportFileType.Apollo, Message.Enum_PlanogramExportFileType_Apollo_Name},
                {PlanogramExportFileType.Spaceman, Message.Enum_PlanogramExportFileType_Spaceman_Name },
            };

        public static readonly Dictionary<PlanogramExportFileType, String> Extensions =
            new Dictionary<PlanogramExportFileType, String>
            {
                {PlanogramExportFileType.POG, ".pog" },
                {PlanogramExportFileType.JDA, ".psa" },
                {PlanogramExportFileType.Apollo, ".xmz"},
                {PlanogramExportFileType.Spaceman, ".pln" },
            };

        public static readonly Dictionary<PlanogramExportFileType, String> Filters =
            new Dictionary<PlanogramExportFileType, String>
            {
                {PlanogramExportFileType.POG, "Galleria Planogram (.pog)|*.pog" },
                {PlanogramExportFileType.JDA, "JDA Space Planning File (.psa)|*.psa" },
                {PlanogramExportFileType.Apollo, "Apollo File (.xmz)|*.xmz"},
                {PlanogramExportFileType.Spaceman, "Spaceman File (.pln)|*.pln" },
            };

        /// <summary>
        ///     Lookup of max supported versions per planogram file type.
        /// </summary>
        public static readonly Dictionary<PlanogramExportFileType, String> MaxSupportedVersion =
            new Dictionary<PlanogramExportFileType, String>
            {
                {PlanogramExportFileType.POG, "9999" },
                {PlanogramExportFileType.JDA, "8" },
                {PlanogramExportFileType.Apollo, "11"},
                {PlanogramExportFileType.Spaceman, "9" },
            };

        /// <summary>
        /// summary, returns a diction of available names based on what we are told we are allowed.
        /// </summary>
        /// <param name="includeSpaceman"></param>
        /// <param name="includeJDA"></param>
        /// <param name="IncludeApollo"></param>
        /// <returns></returns>
        public static Dictionary<PlanogramExportFileType, String> GetAvailableExternalTypeFriendlyNames(Boolean includeSpaceman, Boolean includeJDA, Boolean IncludeApollo)
        {
                Dictionary<PlanogramExportFileType, String> availableValues = new Dictionary<PlanogramExportFileType, String>();

                if (includeSpaceman) availableValues.Add(PlanogramExportFileType.Spaceman, Message.Enum_PlanogramExportFileType_Spaceman_Name);
                if (includeJDA) availableValues.Add(PlanogramExportFileType.JDA, Message.Enum_PlanogramExportFileType_JDA_Name);
                if (IncludeApollo) availableValues.Add(PlanogramExportFileType.Apollo, Message.Enum_PlanogramExportFileType_Apollo_Name);

                return availableValues;
        }
    }
}
