﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-27062 : A.Silva ~ Added CalculateAggregation internal extension.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramValidationAggregationType
    {
        Avg,
        Count,
        Sum,
        Min,
        Max
    }

    public static class PlanogramValidationAggregationTypeHelper
    {
        public static readonly Dictionary<PlanogramValidationAggregationType, String> FriendlyNames = new Dictionary
            <PlanogramValidationAggregationType, String>
        {
            {PlanogramValidationAggregationType.Avg, Message.Enum_PlanogramValidationAggregationType_Avg},
            {PlanogramValidationAggregationType.Count, Message.Enum_PlanogramValidationAggregationType_Count},
            {PlanogramValidationAggregationType.Max, Message.Enum_PlanogramValidationAggregationType_Max},
            {PlanogramValidationAggregationType.Min, Message.Enum_PlanogramValidationAggregationType_Min},
            {PlanogramValidationAggregationType.Sum, Message.Enum_PlanogramValidationAggregationType_Sum}
        };

        /// <summary>
        ///     Calculates the aggregation result for two values.
        /// </summary>
        /// <param name="aggregationType">The <see cref="PlanogramValidationAggregationType" />/> to use.</param>
        /// <param name="arg1">The first value.</param>
        /// <param name="arg2">The second value</param>
        /// <returns>A <see cref="Single" /> result dependant on the <see cref="PlanogramValidationAggregationType" />.</returns>
        /// <remarks>
        ///     For the <see cref="PlanogramValidationAggregationType.Avg" /> value, it is treated as
        ///     <see cref="PlanogramValidationAggregationType.Sum" />, which is the default behavior. It is still necessary to
        ///     divide by the total number of elements in the source collection.
        /// </remarks>
        internal static float CalculateAggregation(this PlanogramValidationAggregationType aggregationType, float arg1,
            float arg2)
        {
            switch (aggregationType)
            {
                case PlanogramValidationAggregationType.Count:
                    return arg1 + 1; // If counting, just add one to the first value (tally count value).
                case PlanogramValidationAggregationType.Max:
                    return Math.Max(arg1, arg2);
                case PlanogramValidationAggregationType.Min:
                    return Math.Min(arg1, arg2);
                default:
                    return arg1 + arg2; // Sum is default. Avg uses Sum too, just divide by the total number of items summed up.
            }
        }
    }
}