﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramSequenceGroupSubGroupList : ModelList<PlanogramSequenceGroupSubGroupList, PlanogramSequenceGroupSubGroup>
    {
        #region Parent

        /// <summary>
        ///     Gets the <see cref="PlanogramSequence"/> that contains this instance.
        /// </summary>
        public new PlanogramSequenceGroup Parent
        {
            get { return (PlanogramSequenceGroup)base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramSequenceGroupProductList"/>.</returns>
        public static PlanogramSequenceGroupSubGroupList NewPlanogramSequenceGroupSubGroupList()
        {
            var item = new PlanogramSequenceGroupSubGroupList();
            item.Create();
            return item;
        }

        #endregion

        #region Create

        /// <summary>
        ///     Invoked when creating a new instance of <see cref="PlanogramSequenceGroupProductList"/>.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion
    }
}
