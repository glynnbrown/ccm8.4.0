﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26491 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added helper class.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    public enum PlanogramAssortmentProductRuleValueType
    {
        Units,
        Facings
    }

    public static class PlanogramAssortmentProductRuleValueTypeHelper
    {
        public static readonly Dictionary<PlanogramAssortmentProductRuleValueType, String> FriendlyNames =
           new Dictionary<PlanogramAssortmentProductRuleValueType, String>()
            {
                {PlanogramAssortmentProductRuleValueType.Units, Message.Enum_PlanogramAssortmentProductRuleValueType_Units},
                {PlanogramAssortmentProductRuleValueType.Facings, Message.Enum_PlanogramAssortmentProductRuleValueType_Facings},
            };
    }
}
