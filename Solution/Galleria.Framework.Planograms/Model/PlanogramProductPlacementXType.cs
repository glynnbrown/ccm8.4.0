﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27154 : L.Luong
//  Created
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available Product Placement X types
    /// </summary>
    public enum PlanogramProductPlacementXType
    {
        Manual = 0,
        FillWide = 1
    }

    public static class PlanogramProductPlacementXTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductPlacementXType, String> FriendlyNames =
            new Dictionary<PlanogramProductPlacementXType, String>()
            {
                {PlanogramProductPlacementXType.Manual, Message.Enum_PlanogramProductPlacementXType_Manual},
                {PlanogramProductPlacementXType.FillWide, Message.Enum_PlanogramProductPlacementXType_FillWide},
            };
    }
}
