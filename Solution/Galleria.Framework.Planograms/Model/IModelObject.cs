﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800

// CCM-24290 : N.Foster
//  Created
// V8-27062 : A.Silva ~ Added method CalculateValidationData.
// V8-27058 : A.Probyn ~ Added method ClearMetadata.
// V8-27237 : A.Silva   ~ Added method ClearValidationData.

#endregion
#region Version History: CCM810
// V8-30016 : A.Kuszyk
//  Added GetAllBrokenRules method.
// V8-30519 : L.Ineson
//  Added FindChildModelById
#endregion
#region Version History: CCM820
// V8-31483 : N.Foster
//  Improved performance of calculate validation data
#endregion
#endregion

using System;
using Csla.Rules;
using Galleria.Framework.Model;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    ///     Defines a model object within this assembly
    /// </summary>
    internal interface IModelObject : IModelReadOnlyObject
    {
        #region Properties
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String DalFactoryName { get; }

        #endregion

        #region Methods
        /// <summary>
        /// Calculates meta data for this instance
        /// </summary>
        void CalculateMetadata();

        /// <summary>
        /// Clears meta data for this instance.
        /// </summary>
        void ClearMetadata();

        /// <summary>
        /// Calculates the validation data for this instance.
        /// </summary>
        void CalculateValidationData();

        /// <summary>
        /// Clears all validation data for this instance.
        /// </summary>
        void ClearValidationData();

        /// <summary>
        /// Helper method to return all broken rules in the 
        /// object graph.
        /// </summary>
        BrokenRulesCollection GetAllBrokenRules();

        /// <summary>
        /// Recurses through the entire model tree looking for the object
        /// with the given type and id.
        /// </summary>
        Object FindChildModelById(Type childModelType, Object id);

        #endregion
    }
}