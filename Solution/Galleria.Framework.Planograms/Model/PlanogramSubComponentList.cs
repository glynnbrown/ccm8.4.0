﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// A list of subcomponents contained within a Component
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramSubComponentList : ModelList<PlanogramSubComponentList, PlanogramSubComponent>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Parent

        public PlanogramComponent ParentPlanogramComponent
        {
            get { return base.Parent as PlanogramComponent; }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static PlanogramSubComponentList NewPlanogramSubComponentList()
        {
            PlanogramSubComponentList item = new PlanogramSubComponentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void  Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
