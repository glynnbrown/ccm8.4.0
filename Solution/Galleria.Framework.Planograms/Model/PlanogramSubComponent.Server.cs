﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-0 : L.Hodson
//	Created (Auto-generated)
// V8-24290 : K.Pickup/A.Kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-26091 : L.Ineson
//  Added additional FaceThickness properties
//  Removed FaceTopOffset and CanMultiMerchX,Y,Z
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-29203 : L.Ineson
//  Added IsProductSqueezeAllowed
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramSubComponent
    {
        #region Constructor
        private PlanogramSubComponent() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramSubComponent Fetch(IDalContext dalContext, PlanogramSubComponentDto dto)
        {
            return DataPortal.FetchChild<PlanogramSubComponent>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramSubComponentDto GetDataTransferObject(PlanogramComponent parent)
        {
            return new PlanogramSubComponentDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                PlanogramComponentId = parent.Id,
                Mesh3DId = ReadProperty<Object>(Mesh3DIdProperty),
                ImageIdFront = ReadProperty<Object>(ImageIdFrontProperty),
                ImageIdBack = ReadProperty<Object>(ImageIdBackProperty),
                ImageIdTop = ReadProperty<Object>(ImageIdTopProperty),
                ImageIdBottom = ReadProperty<Object>(ImageIdBottomProperty),
                ImageIdLeft = ReadProperty<Object>(ImageIdLeftProperty),
                ImageIdRight = ReadProperty<Object>(ImageIdRightProperty),
                Name = ReadProperty<String>(NameProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                Slope = ReadProperty<Single>(SlopeProperty),
                Angle = ReadProperty<Single>(AngleProperty),
                Roll = ReadProperty<Single>(RollProperty),
                ShapeType = (Byte)ReadProperty<PlanogramSubComponentShapeType>(ShapeTypeProperty),
                MerchandisableHeight = ReadProperty<Single>(MerchandisableHeightProperty),
                MerchandisableDepth = ReadProperty<Single>(MerchandisableDepthProperty),
                IsVisible = ReadProperty<Boolean>(IsVisibleProperty),
                HasCollisionDetection = ReadProperty<Boolean>(HasCollisionDetectionProperty),
                FillPatternTypeFront = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeFrontProperty),
                FillPatternTypeBack = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBackProperty),
                FillPatternTypeTop = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeTopProperty),
                FillPatternTypeBottom = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBottomProperty),
                FillPatternTypeLeft = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeLeftProperty),
                FillPatternTypeRight = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeRightProperty),
                FillColourFront = ReadProperty<Int32>(FillColourFrontProperty),
                FillColourBack = ReadProperty<Int32>(FillColourBackProperty),
                FillColourTop = ReadProperty<Int32>(FillColourTopProperty),
                FillColourBottom = ReadProperty<Int32>(FillColourBottomProperty),
                FillColourLeft = ReadProperty<Int32>(FillColourLeftProperty),
                FillColourRight = ReadProperty<Int32>(FillColourRightProperty),
                LineColour = ReadProperty<Int32>(LineColourProperty),
                TransparencyPercentFront = ReadProperty<Int32>(TransparencyPercentFrontProperty),
                TransparencyPercentBack = ReadProperty<Int32>(TransparencyPercentBackProperty),
                TransparencyPercentTop = ReadProperty<Int32>(TransparencyPercentTopProperty),
                TransparencyPercentBottom = ReadProperty<Int32>(TransparencyPercentBottomProperty),
                TransparencyPercentLeft = ReadProperty<Int32>(TransparencyPercentLeftProperty),
                TransparencyPercentRight = ReadProperty<Int32>(TransparencyPercentRightProperty),
                FaceThicknessFront = ReadProperty<Single>(FaceThicknessFrontProperty),
                FaceThicknessBack = ReadProperty<Single>(FaceThicknessBackProperty),
                FaceThicknessTop = ReadProperty<Single>(FaceThicknessTopProperty),
                FaceThicknessBottom = ReadProperty<Single>(FaceThicknessBottomProperty),
                FaceThicknessLeft = ReadProperty<Single>(FaceThicknessLeftProperty),
                FaceThicknessRight = ReadProperty<Single>(FaceThicknessRightProperty),
                RiserHeight = ReadProperty<Single>(RiserHeightProperty),
                RiserThickness = ReadProperty<Single>(RiserThicknessProperty),
                IsRiserPlacedOnFront = ReadProperty<Boolean>(IsRiserPlacedOnFrontProperty),
                IsRiserPlacedOnBack = ReadProperty<Boolean>(IsRiserPlacedOnBackProperty),
                IsRiserPlacedOnLeft = ReadProperty<Boolean>(IsRiserPlacedOnLeftProperty),
                IsRiserPlacedOnRight = ReadProperty<Boolean>(IsRiserPlacedOnRightProperty),
                RiserFillPatternType = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(RiserFillPatternTypeProperty),
                RiserColour = ReadProperty<Int32>(RiserColourProperty),
                RiserTransparencyPercent = ReadProperty<Int32>(RiserTransparencyPercentProperty),
                NotchStartX = ReadProperty<Single>(NotchStartXProperty),
                NotchSpacingX = ReadProperty<Single>(NotchSpacingXProperty),
                NotchStartY = ReadProperty<Single>(NotchStartYProperty),
                NotchSpacingY = ReadProperty<Single>(NotchSpacingYProperty),
                NotchHeight = ReadProperty<Single>(NotchHeightProperty),
                NotchWidth = ReadProperty<Single>(NotchWidthProperty),
                IsNotchPlacedOnFront = ReadProperty<Boolean>(IsNotchPlacedOnFrontProperty),
                IsNotchPlacedOnBack = ReadProperty<Boolean>(IsNotchPlacedOnBackProperty),
                IsNotchPlacedOnLeft = ReadProperty<Boolean>(IsNotchPlacedOnLeftProperty),
                IsNotchPlacedOnRight = ReadProperty<Boolean>(IsNotchPlacedOnRightProperty),
                NotchStyleType = (Byte)ReadProperty<PlanogramSubComponentNotchStyleType>(NotchStyleTypeProperty),
                DividerObstructionHeight = ReadProperty<Single>(DividerObstructionHeightProperty),
                DividerObstructionWidth = ReadProperty<Single>(DividerObstructionWidthProperty),
                DividerObstructionDepth = ReadProperty<Single>(DividerObstructionDepthProperty),
                DividerObstructionStartX = ReadProperty<Single>(DividerObstructionStartXProperty),
                DividerObstructionSpacingX = ReadProperty<Single>(DividerObstructionSpacingXProperty),
                DividerObstructionStartY = ReadProperty<Single>(DividerObstructionStartYProperty),
                DividerObstructionSpacingY = ReadProperty<Single>(DividerObstructionSpacingYProperty),
                DividerObstructionStartZ = ReadProperty<Single>(DividerObstructionStartZProperty),
                DividerObstructionSpacingZ = ReadProperty<Single>(DividerObstructionSpacingZProperty),
                IsDividerObstructionAtStart=ReadProperty<Boolean>(IsDividerObstructionAtStartProperty),
                IsDividerObstructionAtEnd = ReadProperty<Boolean>(IsDividerObstructionAtEndProperty),
                IsDividerObstructionByFacing = ReadProperty<Boolean>(IsDividerObstructionByFacingProperty),
                DividerObstructionFillColour = ReadProperty<Int32>(DividerObstructionFillColourProperty),
                DividerObstructionFillPattern = (Byte)ReadProperty<PlanogramSubComponentFillPatternType>(DividerObstructionFillPatternProperty),
                MerchConstraintRow1StartX = ReadProperty<Single>(MerchConstraintRow1StartXProperty),
                MerchConstraintRow1SpacingX = ReadProperty<Single>(MerchConstraintRow1SpacingXProperty),
                MerchConstraintRow1StartY = ReadProperty<Single>(MerchConstraintRow1StartYProperty),
                MerchConstraintRow1SpacingY = ReadProperty<Single>(MerchConstraintRow1SpacingYProperty),
                MerchConstraintRow1Height = ReadProperty<Single>(MerchConstraintRow1HeightProperty),
                MerchConstraintRow1Width = ReadProperty<Single>(MerchConstraintRow1WidthProperty),
                MerchConstraintRow2StartX = ReadProperty<Single>(MerchConstraintRow2StartXProperty),
                MerchConstraintRow2SpacingX = ReadProperty<Single>(MerchConstraintRow2SpacingXProperty),
                MerchConstraintRow2StartY = ReadProperty<Single>(MerchConstraintRow2StartYProperty),
                MerchConstraintRow2SpacingY = ReadProperty<Single>(MerchConstraintRow2SpacingYProperty),
                MerchConstraintRow2Height = ReadProperty<Single>(MerchConstraintRow2HeightProperty),
                MerchConstraintRow2Width = ReadProperty<Single>(MerchConstraintRow2WidthProperty),
                LineThickness = ReadProperty<Single>(LineThicknessProperty),
                MerchandisingType = (Byte)ReadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty),
                CombineType = (Byte)ReadProperty<PlanogramSubComponentCombineType>(CombineTypeProperty),
                IsProductOverlapAllowed = (Boolean)ReadProperty<Boolean>(IsProductOverlapAllowedProperty),
                IsProductSqueezeAllowed = (Boolean)ReadProperty<Boolean>(IsProductSqueezeAllowedProperty),
                MerchandisingStrategyX = (Byte)ReadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty),
                MerchandisingStrategyY = (Byte)ReadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty),
                MerchandisingStrategyZ = (Byte)ReadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty),
                LeftOverhang = ReadProperty<Single>(LeftOverhangProperty),
                RightOverhang = ReadProperty<Single>(RightOverhangProperty),
                FrontOverhang = ReadProperty<Single>(FrontOverhangProperty),
                BackOverhang = ReadProperty<Single>(BackOverhangProperty),
                TopOverhang = ReadProperty<Single>(TopOverhangProperty),
                BottomOverhang = ReadProperty<Single>(BottomOverhangProperty),
                ExtendedData = ReadProperty<Object>(ExtendedDataProperty),
               
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramSubComponentDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<Object>(Mesh3DIdProperty, dto.Mesh3DId);
            LoadProperty<Object>(ImageIdFrontProperty, dto.ImageIdFront);
            LoadProperty<Object>(ImageIdBackProperty, dto.ImageIdBack);
            LoadProperty<Object>(ImageIdTopProperty, dto.ImageIdTop);
            LoadProperty<Object>(ImageIdBottomProperty, dto.ImageIdBottom);
            LoadProperty<Object>(ImageIdLeftProperty, dto.ImageIdLeft);
            LoadProperty<Object>(ImageIdRightProperty, dto.ImageIdRight);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Single>(HeightProperty, dto.Height);
            LoadProperty<Single>(WidthProperty, dto.Width);
            LoadProperty<Single>(DepthProperty, dto.Depth);
            LoadProperty<Single>(XProperty, dto.X);
            LoadProperty<Single>(YProperty, dto.Y);
            LoadProperty<Single>(ZProperty, dto.Z);
            LoadProperty<Single>(SlopeProperty, dto.Slope);
            LoadProperty<Single>(AngleProperty, dto.Angle);
            LoadProperty<Single>(RollProperty, dto.Roll);
            LoadProperty<PlanogramSubComponentShapeType>(ShapeTypeProperty, (PlanogramSubComponentShapeType)dto.ShapeType);
            LoadProperty<Single>(MerchandisableHeightProperty, dto.MerchandisableHeight);
            LoadProperty<Single>(MerchandisableDepthProperty, dto.MerchandisableDepth);
            LoadProperty<Boolean>(IsVisibleProperty, dto.IsVisible);
            LoadProperty<Boolean>(HasCollisionDetectionProperty, dto.HasCollisionDetection);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeFrontProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeFront);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBackProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeBack);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeTopProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeTop);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeBottomProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeBottom);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeLeftProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeLeft);
            LoadProperty<PlanogramSubComponentFillPatternType>(FillPatternTypeRightProperty, (PlanogramSubComponentFillPatternType)dto.FillPatternTypeRight);
            LoadProperty<Int32>(FillColourFrontProperty, dto.FillColourFront);
            LoadProperty<Int32>(FillColourBackProperty, dto.FillColourBack);
            LoadProperty<Int32>(FillColourTopProperty, dto.FillColourTop);
            LoadProperty<Int32>(FillColourBottomProperty, dto.FillColourBottom);
            LoadProperty<Int32>(FillColourLeftProperty, dto.FillColourLeft);
            LoadProperty<Int32>(FillColourRightProperty, dto.FillColourRight);
            LoadProperty<Int32>(LineColourProperty, dto.LineColour);
            LoadProperty<Int32>(TransparencyPercentFrontProperty, dto.TransparencyPercentFront);
            LoadProperty<Int32>(TransparencyPercentBackProperty, dto.TransparencyPercentBack);
            LoadProperty<Int32>(TransparencyPercentTopProperty, dto.TransparencyPercentTop);
            LoadProperty<Int32>(TransparencyPercentBottomProperty, dto.TransparencyPercentBottom);
            LoadProperty<Int32>(TransparencyPercentLeftProperty, dto.TransparencyPercentLeft);
            LoadProperty<Int32>(TransparencyPercentRightProperty, dto.TransparencyPercentRight);
            LoadProperty<Single>(FaceThicknessFrontProperty, dto.FaceThicknessFront);
            LoadProperty<Single>(FaceThicknessBackProperty, dto.FaceThicknessBack);
            LoadProperty<Single>(FaceThicknessTopProperty, dto.FaceThicknessTop);
            LoadProperty<Single>(FaceThicknessBottomProperty, dto.FaceThicknessBottom);
            LoadProperty<Single>(FaceThicknessLeftProperty, dto.FaceThicknessLeft);
            LoadProperty<Single>(FaceThicknessRightProperty, dto.FaceThicknessRight);
            LoadProperty<Single>(RiserHeightProperty, dto.RiserHeight);
            LoadProperty<Single>(RiserThicknessProperty, dto.RiserThickness);
            LoadProperty<Boolean>(IsRiserPlacedOnFrontProperty, dto.IsRiserPlacedOnFront);
            LoadProperty<Boolean>(IsRiserPlacedOnBackProperty, dto.IsRiserPlacedOnBack);
            LoadProperty<Boolean>(IsRiserPlacedOnLeftProperty, dto.IsRiserPlacedOnLeft);
            LoadProperty<Boolean>(IsRiserPlacedOnRightProperty, dto.IsRiserPlacedOnRight);
            LoadProperty<PlanogramSubComponentFillPatternType>(RiserFillPatternTypeProperty, (PlanogramSubComponentFillPatternType)dto.RiserFillPatternType);
            LoadProperty<Int32>(RiserColourProperty, dto.RiserColour);
            LoadProperty<Int32>(RiserTransparencyPercentProperty, dto.RiserTransparencyPercent);
            LoadProperty<Single>(NotchStartXProperty, dto.NotchStartX);
            LoadProperty<Single>(NotchSpacingXProperty, dto.NotchSpacingX);
            LoadProperty<Single>(NotchStartYProperty, dto.NotchStartY);
            LoadProperty<Single>(NotchSpacingYProperty, dto.NotchSpacingY);
            LoadProperty<Single>(NotchHeightProperty, dto.NotchHeight);
            LoadProperty<Single>(NotchWidthProperty, dto.NotchWidth);
            LoadProperty<Boolean>(IsNotchPlacedOnFrontProperty, dto.IsNotchPlacedOnFront);
            LoadProperty<Boolean>(IsNotchPlacedOnBackProperty, dto.IsNotchPlacedOnBack);
            LoadProperty<Boolean>(IsNotchPlacedOnLeftProperty, dto.IsNotchPlacedOnLeft);
            LoadProperty<Boolean>(IsNotchPlacedOnRightProperty, dto.IsNotchPlacedOnRight);
            LoadProperty<PlanogramSubComponentNotchStyleType>(NotchStyleTypeProperty, (PlanogramSubComponentNotchStyleType)dto.NotchStyleType);
            LoadProperty<Single>(DividerObstructionHeightProperty, dto.DividerObstructionHeight);
            LoadProperty<Single>(DividerObstructionWidthProperty, dto.DividerObstructionWidth);
            LoadProperty<Single>(DividerObstructionDepthProperty, dto.DividerObstructionDepth);
            LoadProperty<Single>(DividerObstructionStartXProperty, dto.DividerObstructionStartX);
            LoadProperty<Single>(DividerObstructionSpacingXProperty, dto.DividerObstructionSpacingX);
            LoadProperty<Single>(DividerObstructionStartYProperty, dto.DividerObstructionStartY);
            LoadProperty<Single>(DividerObstructionSpacingYProperty, dto.DividerObstructionSpacingY);
            LoadProperty<Single>(DividerObstructionStartZProperty, dto.DividerObstructionStartZ);
            LoadProperty<Single>(DividerObstructionSpacingZProperty, dto.DividerObstructionSpacingZ);
            LoadProperty<Boolean>(IsDividerObstructionAtStartProperty, dto.IsDividerObstructionAtStart);
            LoadProperty<Boolean>(IsDividerObstructionAtEndProperty, dto.IsDividerObstructionAtEnd);
            LoadProperty<Boolean>(IsDividerObstructionByFacingProperty, dto.IsDividerObstructionByFacing);
            LoadProperty<Int32>(DividerObstructionFillColourProperty, dto.DividerObstructionFillColour);
            LoadProperty<PlanogramSubComponentFillPatternType>(DividerObstructionFillPatternProperty, (PlanogramSubComponentFillPatternType)dto.DividerObstructionFillPattern);
            LoadProperty<Single>(MerchConstraintRow1StartXProperty, dto.MerchConstraintRow1StartX);
            LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, dto.MerchConstraintRow1SpacingX);
            LoadProperty<Single>(MerchConstraintRow1StartYProperty, dto.MerchConstraintRow1StartY);
            LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, dto.MerchConstraintRow1SpacingY);
            LoadProperty<Single>(MerchConstraintRow1HeightProperty, dto.MerchConstraintRow1Height);
            LoadProperty<Single>(MerchConstraintRow1WidthProperty, dto.MerchConstraintRow1Width);
            LoadProperty<Single>(MerchConstraintRow2StartXProperty, dto.MerchConstraintRow2StartX);
            LoadProperty<Single>(MerchConstraintRow2SpacingXProperty, dto.MerchConstraintRow2SpacingX);
            LoadProperty<Single>(MerchConstraintRow2StartYProperty, dto.MerchConstraintRow2StartY);
            LoadProperty<Single>(MerchConstraintRow2SpacingYProperty, dto.MerchConstraintRow2SpacingY);
            LoadProperty<Single>(MerchConstraintRow2HeightProperty, dto.MerchConstraintRow2Height);
            LoadProperty<Single>(MerchConstraintRow2WidthProperty, dto.MerchConstraintRow2Width);
            LoadProperty<Single>(LineThicknessProperty, dto.LineThickness);
            LoadProperty<PlanogramSubComponentMerchandisingType>(MerchandisingTypeProperty, (PlanogramSubComponentMerchandisingType)dto.MerchandisingType);
            LoadProperty<PlanogramSubComponentCombineType>(CombineTypeProperty, (PlanogramSubComponentCombineType)dto.CombineType);
            LoadProperty<Boolean>(IsProductOverlapAllowedProperty, dto.IsProductOverlapAllowed);
            LoadProperty<Boolean>(IsProductSqueezeAllowedProperty, dto.IsProductSqueezeAllowed);
            LoadProperty<PlanogramSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, (PlanogramSubComponentXMerchStrategyType)dto.MerchandisingStrategyX);
            LoadProperty<PlanogramSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, (PlanogramSubComponentYMerchStrategyType)dto.MerchandisingStrategyY);
            LoadProperty<PlanogramSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, (PlanogramSubComponentZMerchStrategyType)dto.MerchandisingStrategyZ);
            LoadProperty<Single>(LeftOverhangProperty, dto.LeftOverhang);
            LoadProperty<Single>(RightOverhangProperty, dto.RightOverhang);
            LoadProperty<Single>(FrontOverhangProperty, dto.FrontOverhang);
            LoadProperty<Single>(BackOverhangProperty, dto.BackOverhang);
            LoadProperty<Single>(TopOverhangProperty, dto.TopOverhang);
            LoadProperty<Single>(BottomOverhangProperty, dto.BottomOverhang);
            LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramSubComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(BatchSaveContext batchContext, PlanogramComponent parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramSubComponentDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramSubComponentDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramSubComponent>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, PlanogramComponent parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramSubComponentDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, PlanogramComponent parent)
        {
            batchContext.Delete<PlanogramSubComponentDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}