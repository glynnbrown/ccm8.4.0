﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26147 : L.Ineson
//	Created (Auto-generated)
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31647 : D.Pleasance
//  Amended LoadDataTransferObject() to ensure dto id has a value
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.Model
{
    public partial class CustomAttributeData
    {
        #region Constructor
        private CustomAttributeData() { } //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item for the given parent type and id
        /// </summary>
        internal static CustomAttributeData Fetch(IDalContext dalContext, Byte parentType, Object parentId)
        {
            return DataPortal.FetchChild<CustomAttributeData>(dalContext, new FetchCriteria(null, parentType, parentId));
        }

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        public static CustomAttributeData Fetch(IDalContext dalContext, CustomAttributeDataDto dto)
        {
            return DataPortal.FetchChild<CustomAttributeData>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, CustomAttributeDataDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id ?? IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(Text1Property, dto.Text1);
            this.LoadProperty<String>(Text2Property, dto.Text2);
            this.LoadProperty<String>(Text3Property, dto.Text3);
            this.LoadProperty<String>(Text4Property, dto.Text4);
            this.LoadProperty<String>(Text5Property, dto.Text5);
            this.LoadProperty<String>(Text6Property, dto.Text6);
            this.LoadProperty<String>(Text7Property, dto.Text7);
            this.LoadProperty<String>(Text8Property, dto.Text8);
            this.LoadProperty<String>(Text9Property, dto.Text9);
            this.LoadProperty<String>(Text10Property, dto.Text10);
            this.LoadProperty<String>(Text11Property, dto.Text11);
            this.LoadProperty<String>(Text12Property, dto.Text12);
            this.LoadProperty<String>(Text13Property, dto.Text13);
            this.LoadProperty<String>(Text14Property, dto.Text14);
            this.LoadProperty<String>(Text15Property, dto.Text15);
            this.LoadProperty<String>(Text16Property, dto.Text16);
            this.LoadProperty<String>(Text17Property, dto.Text17);
            this.LoadProperty<String>(Text18Property, dto.Text18);
            this.LoadProperty<String>(Text19Property, dto.Text19);
            this.LoadProperty<String>(Text20Property, dto.Text20);
            this.LoadProperty<String>(Text21Property, dto.Text21);
            this.LoadProperty<String>(Text22Property, dto.Text22);
            this.LoadProperty<String>(Text23Property, dto.Text23);
            this.LoadProperty<String>(Text24Property, dto.Text24);
            this.LoadProperty<String>(Text25Property, dto.Text25);
            this.LoadProperty<String>(Text26Property, dto.Text26);
            this.LoadProperty<String>(Text27Property, dto.Text27);
            this.LoadProperty<String>(Text28Property, dto.Text28);
            this.LoadProperty<String>(Text29Property, dto.Text29);
            this.LoadProperty<String>(Text30Property, dto.Text30);
            this.LoadProperty<String>(Text31Property, dto.Text31);
            this.LoadProperty<String>(Text32Property, dto.Text32);
            this.LoadProperty<String>(Text33Property, dto.Text33);
            this.LoadProperty<String>(Text34Property, dto.Text34);
            this.LoadProperty<String>(Text35Property, dto.Text35);
            this.LoadProperty<String>(Text36Property, dto.Text36);
            this.LoadProperty<String>(Text37Property, dto.Text37);
            this.LoadProperty<String>(Text38Property, dto.Text38);
            this.LoadProperty<String>(Text39Property, dto.Text39);
            this.LoadProperty<String>(Text40Property, dto.Text40);
            this.LoadProperty<String>(Text41Property, dto.Text41);
            this.LoadProperty<String>(Text42Property, dto.Text42);
            this.LoadProperty<String>(Text43Property, dto.Text43);
            this.LoadProperty<String>(Text44Property, dto.Text44);
            this.LoadProperty<String>(Text45Property, dto.Text45);
            this.LoadProperty<String>(Text46Property, dto.Text46);
            this.LoadProperty<String>(Text47Property, dto.Text47);
            this.LoadProperty<String>(Text48Property, dto.Text48);
            this.LoadProperty<String>(Text49Property, dto.Text49);
            this.LoadProperty<String>(Text50Property, dto.Text50);
            this.LoadProperty<Single>(Value1Property, dto.Value1);
            this.LoadProperty<Single>(Value2Property, dto.Value2);
            this.LoadProperty<Single>(Value3Property, dto.Value3);
            this.LoadProperty<Single>(Value4Property, dto.Value4);
            this.LoadProperty<Single>(Value5Property, dto.Value5);
            this.LoadProperty<Single>(Value6Property, dto.Value6);
            this.LoadProperty<Single>(Value7Property, dto.Value7);
            this.LoadProperty<Single>(Value8Property, dto.Value8);
            this.LoadProperty<Single>(Value9Property, dto.Value9);
            this.LoadProperty<Single>(Value10Property, dto.Value10);
            this.LoadProperty<Single>(Value11Property, dto.Value11);
            this.LoadProperty<Single>(Value12Property, dto.Value12);
            this.LoadProperty<Single>(Value13Property, dto.Value13);
            this.LoadProperty<Single>(Value14Property, dto.Value14);
            this.LoadProperty<Single>(Value15Property, dto.Value15);
            this.LoadProperty<Single>(Value16Property, dto.Value16);
            this.LoadProperty<Single>(Value17Property, dto.Value17);
            this.LoadProperty<Single>(Value18Property, dto.Value18);
            this.LoadProperty<Single>(Value19Property, dto.Value19);
            this.LoadProperty<Single>(Value20Property, dto.Value20);
            this.LoadProperty<Single>(Value21Property, dto.Value21);
            this.LoadProperty<Single>(Value22Property, dto.Value22);
            this.LoadProperty<Single>(Value23Property, dto.Value23);
            this.LoadProperty<Single>(Value24Property, dto.Value24);
            this.LoadProperty<Single>(Value25Property, dto.Value25);
            this.LoadProperty<Single>(Value26Property, dto.Value26);
            this.LoadProperty<Single>(Value27Property, dto.Value27);
            this.LoadProperty<Single>(Value28Property, dto.Value28);
            this.LoadProperty<Single>(Value29Property, dto.Value29);
            this.LoadProperty<Single>(Value30Property, dto.Value30);
            this.LoadProperty<Single>(Value31Property, dto.Value31);
            this.LoadProperty<Single>(Value32Property, dto.Value32);
            this.LoadProperty<Single>(Value33Property, dto.Value33);
            this.LoadProperty<Single>(Value34Property, dto.Value34);
            this.LoadProperty<Single>(Value35Property, dto.Value35);
            this.LoadProperty<Single>(Value36Property, dto.Value36);
            this.LoadProperty<Single>(Value37Property, dto.Value37);
            this.LoadProperty<Single>(Value38Property, dto.Value38);
            this.LoadProperty<Single>(Value39Property, dto.Value39);
            this.LoadProperty<Single>(Value40Property, dto.Value40);
            this.LoadProperty<Single>(Value41Property, dto.Value41);
            this.LoadProperty<Single>(Value42Property, dto.Value42);
            this.LoadProperty<Single>(Value43Property, dto.Value43);
            this.LoadProperty<Single>(Value44Property, dto.Value44);
            this.LoadProperty<Single>(Value45Property, dto.Value45);
            this.LoadProperty<Single>(Value46Property, dto.Value46);
            this.LoadProperty<Single>(Value47Property, dto.Value47);
            this.LoadProperty<Single>(Value48Property, dto.Value48);
            this.LoadProperty<Single>(Value49Property, dto.Value49);
            this.LoadProperty<Single>(Value50Property, dto.Value50);
            this.LoadProperty<Boolean>(Flag1Property, dto.Flag1);
            this.LoadProperty<Boolean>(Flag2Property, dto.Flag2);
            this.LoadProperty<Boolean>(Flag3Property, dto.Flag3);
            this.LoadProperty<Boolean>(Flag4Property, dto.Flag4);
            this.LoadProperty<Boolean>(Flag5Property, dto.Flag5);
            this.LoadProperty<Boolean>(Flag6Property, dto.Flag6);
            this.LoadProperty<Boolean>(Flag7Property, dto.Flag7);
            this.LoadProperty<Boolean>(Flag8Property, dto.Flag8);
            this.LoadProperty<Boolean>(Flag9Property, dto.Flag9);
            this.LoadProperty<Boolean>(Flag10Property, dto.Flag10);
            this.LoadProperty<DateTime?>(Date1Property, dto.Date1);
            this.LoadProperty<DateTime?>(Date2Property, dto.Date2);
            this.LoadProperty<DateTime?>(Date3Property, dto.Date3);
            this.LoadProperty<DateTime?>(Date4Property, dto.Date4);
            this.LoadProperty<DateTime?>(Date5Property, dto.Date5);
            this.LoadProperty<DateTime?>(Date6Property, dto.Date6);
            this.LoadProperty<DateTime?>(Date7Property, dto.Date7);
            this.LoadProperty<DateTime?>(Date8Property, dto.Date8);
            this.LoadProperty<DateTime?>(Date9Property, dto.Date9);
            this.LoadProperty<DateTime?>(Date10Property, dto.Date10);
            this.LoadProperty<String>(Note1Property, dto.Note1);
            this.LoadProperty<String>(Note2Property, dto.Note2);
            this.LoadProperty<String>(Note3Property, dto.Note3);
            this.LoadProperty<String>(Note4Property, dto.Note4);
            this.LoadProperty<String>(Note5Property, dto.Note5);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private CustomAttributeDataDto GetDataTransferObject(ICustomAttributeDataParent parent)
        {
            return new CustomAttributeDataDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                ParentId = parent.Id,
                ParentType = parent.ParentType,
                Text1 = ReadProperty<String>(Text1Property),
                Text2 = ReadProperty<String>(Text2Property),
                Text3 = ReadProperty<String>(Text3Property),
                Text4 = ReadProperty<String>(Text4Property),
                Text5 = ReadProperty<String>(Text5Property),
                Text6 = ReadProperty<String>(Text6Property),
                Text7 = ReadProperty<String>(Text7Property),
                Text8 = ReadProperty<String>(Text8Property),
                Text9 = ReadProperty<String>(Text9Property),
                Text10 = ReadProperty<String>(Text10Property),
                Text11 = ReadProperty<String>(Text11Property),
                Text12 = ReadProperty<String>(Text12Property),
                Text13 = ReadProperty<String>(Text13Property),
                Text14 = ReadProperty<String>(Text14Property),
                Text15 = ReadProperty<String>(Text15Property),
                Text16 = ReadProperty<String>(Text16Property),
                Text17 = ReadProperty<String>(Text17Property),
                Text18 = ReadProperty<String>(Text18Property),
                Text19 = ReadProperty<String>(Text19Property),
                Text20 = ReadProperty<String>(Text20Property),
                Text21 = ReadProperty<String>(Text21Property),
                Text22 = ReadProperty<String>(Text22Property),
                Text23 = ReadProperty<String>(Text23Property),
                Text24 = ReadProperty<String>(Text24Property),
                Text25 = ReadProperty<String>(Text25Property),
                Text26 = ReadProperty<String>(Text26Property),
                Text27 = ReadProperty<String>(Text27Property),
                Text28 = ReadProperty<String>(Text28Property),
                Text29 = ReadProperty<String>(Text29Property),
                Text30 = ReadProperty<String>(Text30Property),
                Text31 = ReadProperty<String>(Text31Property),
                Text32 = ReadProperty<String>(Text32Property),
                Text33 = ReadProperty<String>(Text33Property),
                Text34 = ReadProperty<String>(Text34Property),
                Text35 = ReadProperty<String>(Text35Property),
                Text36 = ReadProperty<String>(Text36Property),
                Text37 = ReadProperty<String>(Text37Property),
                Text38 = ReadProperty<String>(Text38Property),
                Text39 = ReadProperty<String>(Text39Property),
                Text40 = ReadProperty<String>(Text40Property),
                Text41 = ReadProperty<String>(Text41Property),
                Text42 = ReadProperty<String>(Text42Property),
                Text43 = ReadProperty<String>(Text43Property),
                Text44 = ReadProperty<String>(Text44Property),
                Text45 = ReadProperty<String>(Text45Property),
                Text46 = ReadProperty<String>(Text46Property),
                Text47 = ReadProperty<String>(Text47Property),
                Text48 = ReadProperty<String>(Text48Property),
                Text49 = ReadProperty<String>(Text49Property),
                Text50 = ReadProperty<String>(Text50Property),
                Value1 = ReadProperty<Single>(Value1Property),
                Value2 = ReadProperty<Single>(Value2Property),
                Value3 = ReadProperty<Single>(Value3Property),
                Value4 = ReadProperty<Single>(Value4Property),
                Value5 = ReadProperty<Single>(Value5Property),
                Value6 = ReadProperty<Single>(Value6Property),
                Value7 = ReadProperty<Single>(Value7Property),
                Value8 = ReadProperty<Single>(Value8Property),
                Value9 = ReadProperty<Single>(Value9Property),
                Value10 = ReadProperty<Single>(Value10Property),
                Value11 = ReadProperty<Single>(Value11Property),
                Value12 = ReadProperty<Single>(Value12Property),
                Value13 = ReadProperty<Single>(Value13Property),
                Value14 = ReadProperty<Single>(Value14Property),
                Value15 = ReadProperty<Single>(Value15Property),
                Value16 = ReadProperty<Single>(Value16Property),
                Value17 = ReadProperty<Single>(Value17Property),
                Value18 = ReadProperty<Single>(Value18Property),
                Value19 = ReadProperty<Single>(Value19Property),
                Value20 = ReadProperty<Single>(Value20Property),
                Value21 = ReadProperty<Single>(Value21Property),
                Value22 = ReadProperty<Single>(Value22Property),
                Value23 = ReadProperty<Single>(Value23Property),
                Value24 = ReadProperty<Single>(Value24Property),
                Value25 = ReadProperty<Single>(Value25Property),
                Value26 = ReadProperty<Single>(Value26Property),
                Value27 = ReadProperty<Single>(Value27Property),
                Value28 = ReadProperty<Single>(Value28Property),
                Value29 = ReadProperty<Single>(Value29Property),
                Value30 = ReadProperty<Single>(Value30Property),
                Value31 = ReadProperty<Single>(Value31Property),
                Value32 = ReadProperty<Single>(Value32Property),
                Value33 = ReadProperty<Single>(Value33Property),
                Value34 = ReadProperty<Single>(Value34Property),
                Value35 = ReadProperty<Single>(Value35Property),
                Value36 = ReadProperty<Single>(Value36Property),
                Value37 = ReadProperty<Single>(Value37Property),
                Value38 = ReadProperty<Single>(Value38Property),
                Value39 = ReadProperty<Single>(Value39Property),
                Value40 = ReadProperty<Single>(Value40Property),
                Value41 = ReadProperty<Single>(Value41Property),
                Value42 = ReadProperty<Single>(Value42Property),
                Value43 = ReadProperty<Single>(Value43Property),
                Value44 = ReadProperty<Single>(Value44Property),
                Value45 = ReadProperty<Single>(Value45Property),
                Value46 = ReadProperty<Single>(Value46Property),
                Value47 = ReadProperty<Single>(Value47Property),
                Value48 = ReadProperty<Single>(Value48Property),
                Value49 = ReadProperty<Single>(Value49Property),
                Value50 = ReadProperty<Single>(Value50Property),
                Flag1 = ReadProperty<Boolean>(Flag1Property),
                Flag2 = ReadProperty<Boolean>(Flag2Property),
                Flag3 = ReadProperty<Boolean>(Flag3Property),
                Flag4 = ReadProperty<Boolean>(Flag4Property),
                Flag5 = ReadProperty<Boolean>(Flag5Property),
                Flag6 = ReadProperty<Boolean>(Flag6Property),
                Flag7 = ReadProperty<Boolean>(Flag7Property),
                Flag8 = ReadProperty<Boolean>(Flag8Property),
                Flag9 = ReadProperty<Boolean>(Flag9Property),
                Flag10 = ReadProperty<Boolean>(Flag10Property),
                Date1 = ReadProperty<DateTime?>(Date1Property),
                Date2 = ReadProperty<DateTime?>(Date2Property),
                Date3 = ReadProperty<DateTime?>(Date3Property),
                Date4 = ReadProperty<DateTime?>(Date4Property),
                Date5 = ReadProperty<DateTime?>(Date5Property),
                Date6 = ReadProperty<DateTime?>(Date6Property),
                Date7 = ReadProperty<DateTime?>(Date7Property),
                Date8 = ReadProperty<DateTime?>(Date8Property),
                Date9 = ReadProperty<DateTime?>(Date9Property),
                Date10 = ReadProperty<DateTime?>(Date10Property),
                Note1 = ReadProperty<String>(Note1Property),
                Note2 = ReadProperty<String>(Note2Property),
                Note3 = ReadProperty<String>(Note3Property),
                Note4 = ReadProperty<String>(Note4Property),
                Note5 = ReadProperty<String>(Note5Property)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when performing a fetch
        /// </summary>
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
                    {
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchByParentTypeParentId(criteria.ParentType, criteria.ParentId));
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    this.Create();
                }
            }
            this.MarkAsChild();
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchCriteria criteria)
        {
            CustomAttributeDataDto dto;
            using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
            {
                dto = dal.FetchByParentTypeParentId(criteria.ParentType, criteria.ParentId);
            }
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, CustomAttributeDataDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ICustomAttributeDataParent parent)
        {
            CustomAttributeDataDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Object>(IdProperty, dto.Id);
            dalContext.RegisterId<CustomAttributeData>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(BatchSaveContext batchContext, ICustomAttributeDataParent parent)
        {
            Object oldId = null;
            batchContext.Insert<CustomAttributeDataDto>(
            (dc) =>
            {
                CustomAttributeDataDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<CustomAttributeData>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ICustomAttributeDataParent parent)
        {
            if (this.IsSelfDirty)
            {
                using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, ICustomAttributeDataParent parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<CustomAttributeDataDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, ICustomAttributeDataParent parent)
        {
            using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
            {
                dal.DeleteById(this.GetDataTransferObject(parent));
            }
        }

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, ICustomAttributeDataParent parent)
        {
            batchContext.Delete<CustomAttributeDataDto>(this.GetDataTransferObject(parent));
        }

        #endregion

        #endregion
    }
}
