﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26836 : L.Luong 
//  Created (Auto-generated)
// V8-27404 : L.Luong
//  Renamed LevelType to EntryType and added Content
// V8-27759 : A.Kuszyk
//  Added AffectedType and AffectedId properties.
// V8-28059 : A.Kuszyk
//  Added WorkpackageSource, TaskSource and EventId properties.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new Score property
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramEventLog
    {
        #region Constructor
        private PlanogramEventLog() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramEventLog Fetch(IDalContext dalContext, PlanogramEventLogDto dto)
        {
            return DataPortal.FetchChild<PlanogramEventLog>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramEventLogDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramEventLogEventType>(EventTypeProperty, (PlanogramEventLogEventType)dto.EventType);
            this.LoadProperty<PlanogramEventLogEntryType>(EntryTypeProperty, (PlanogramEventLogEntryType)dto.EntryType);
            this.LoadProperty<Object>(AffectedIdProperty, dto.AffectedId);
            this.LoadProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty, (PlanogramEventLogAffectedType?)dto.AffectedType);
            this.LoadProperty<String>(WorkpackageSourceProperty, dto.WorkpackageSource);
            this.LoadProperty<String>(TaskSourceProperty, dto.TaskSource);
            this.LoadProperty<Int32>(EventIdProperty, dto.EventId);
            this.LoadProperty<DateTime>(DateTimeProperty, dto.DateTime);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<String>(ContentProperty, dto.Content);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Byte>(ScoreProperty, dto.Score);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramEventLogDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramEventLogDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                EventType = (Byte)this.ReadProperty<PlanogramEventLogEventType>(EventTypeProperty),
                EntryType = (Byte)this.ReadProperty<PlanogramEventLogEntryType>(EntryTypeProperty),
                AffectedId = this.ReadProperty<Object>(AffectedIdProperty),
                AffectedType = (Byte?)this.ReadProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty),
                WorkpackageSource = this.ReadProperty<String>(WorkpackageSourceProperty),
                TaskSource = this.ReadProperty<String>(TaskSourceProperty),
                EventId = this.ReadProperty<Int32>(EventIdProperty),
                DateTime = this.ReadProperty<DateTime>(DateTimeProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                Content = this.ReadProperty<String>(ContentProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                Score = this.ReadProperty<Byte>(ScoreProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramEventLogDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramEventLogDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramEventLogDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramEventLog>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramEventLogDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramEventLogDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
