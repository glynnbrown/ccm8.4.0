﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27153 : A.Silva
//  Created.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-30193 : L.Ineson
//  Added IsEnabled property
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramRenumberingStrategy
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to obtain new instances of <see cref="PlanogramRenumberingStrategy"/>.
        /// </summary>
        private PlanogramRenumberingStrategy()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Gets a new instance of <see cref="PlanogramRenumberingStrategy"/> from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns>A new instance initialized from the given <paramref name="dto"/>.</returns>
        public static PlanogramRenumberingStrategy Fetch(IDalContext dalContext, PlanogramRenumberingStrategyDto dto)
        {
            return DataPortal.FetchChild<PlanogramRenumberingStrategy>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object


        /// <summary>
        ///     Creates a <see cref="PlanogramRenumberingStrategyDto"/> with the values in this instance.
        /// </summary>
        /// <returns>New instance of <see cref="PlanogramRenumberingStrategyDto"/> with the values in this instance.</returns>
        private PlanogramRenumberingStrategyDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramRenumberingStrategyDto
            {
                Id = ReadProperty(IdProperty),
                PlanogramId = parent.Id,
                Name = ReadProperty(NameProperty),
                BayComponentXRenumberStrategyPriority = ReadProperty(BayComponentXRenumberStrategyPriorityProperty),
                BayComponentYRenumberStrategyPriority = ReadProperty(BayComponentYRenumberStrategyPriorityProperty),
                BayComponentZRenumberStrategyPriority = ReadProperty(BayComponentZRenumberStrategyPriorityProperty),
                PositionXRenumberStrategyPriority = ReadProperty(PositionXRenumberStrategyPriorityProperty),
                PositionYRenumberStrategyPriority = ReadProperty(PositionYRenumberStrategyPriorityProperty),
                PositionZRenumberStrategyPriority = ReadProperty(PositionZRenumberStrategyPriorityProperty),
                BayComponentXRenumberStrategy = (Byte)ReadProperty(BayComponentXRenumberStrategyProperty),
                BayComponentYRenumberStrategy = (Byte)ReadProperty(BayComponentYRenumberStrategyProperty),
                BayComponentZRenumberStrategy = (Byte)ReadProperty(BayComponentZRenumberStrategyProperty),
                PositionXRenumberStrategy = (Byte)ReadProperty(PositionXRenumberStrategyProperty),
                PositionYRenumberStrategy = (Byte)ReadProperty(PositionYRenumberStrategyProperty),
                PositionZRenumberStrategy = (Byte)ReadProperty(PositionZRenumberStrategyProperty),
                RestartComponentRenumberingPerBay = ReadProperty(RestartComponentRenumberingPerBayProperty),
                IgnoreNonMerchandisingComponents = ReadProperty(IgnoreNonMerchandisingComponentsProperty),
                RestartPositionRenumberingPerComponent = ReadProperty(RestartPositionRenumberingPerComponentProperty),
                UniqueNumberMultiPositionProductsPerComponent = ReadProperty(UniqueNumberMultiPositionProductsPerComponentProperty),
                ExceptAdjacentPositions = ReadProperty(ExceptAdjacentPositionsProperty),
                ExtendedData = ReadProperty(ExtendedDataProperty),
                IsEnabled = ReadProperty<Boolean>(IsEnabledProperty),
                RestartPositionRenumberingPerBay = ReadProperty(RestartPositionRenumberingPerBayProperty),
                RestartComponentRenumberingPerComponentType = ReadProperty(RestartComponentRenumberingPerComponentTypeProperty)
            };
        }

        /// <summary>
        ///     Loads the values from the provided <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="PlanogramRenumberingStrategyDto"/> containing the values to be loaded.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramRenumberingStrategyDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(BayComponentXRenumberStrategyPriorityProperty, dto.BayComponentXRenumberStrategyPriority);
            LoadProperty(BayComponentYRenumberStrategyPriorityProperty, dto.BayComponentYRenumberStrategyPriority);
            LoadProperty(BayComponentZRenumberStrategyPriorityProperty, dto.BayComponentZRenumberStrategyPriority);
            LoadProperty(PositionXRenumberStrategyPriorityProperty, dto.PositionXRenumberStrategyPriority);
            LoadProperty(PositionYRenumberStrategyPriorityProperty, dto.PositionYRenumberStrategyPriority);
            LoadProperty(PositionZRenumberStrategyPriorityProperty, dto.PositionZRenumberStrategyPriority);
            LoadProperty(BayComponentXRenumberStrategyProperty, dto.BayComponentXRenumberStrategy);
            LoadProperty(BayComponentYRenumberStrategyProperty, dto.BayComponentYRenumberStrategy);
            LoadProperty(BayComponentZRenumberStrategyProperty, dto.BayComponentZRenumberStrategy);
            LoadProperty(PositionXRenumberStrategyProperty, dto.PositionXRenumberStrategy);
            LoadProperty(PositionYRenumberStrategyProperty, dto.PositionYRenumberStrategy);
            LoadProperty(PositionZRenumberStrategyProperty, dto.PositionZRenumberStrategy);
            LoadProperty(RestartComponentRenumberingPerBayProperty, dto.RestartComponentRenumberingPerBay);
            LoadProperty(IgnoreNonMerchandisingComponentsProperty, dto.IgnoreNonMerchandisingComponents);
            LoadProperty(RestartPositionRenumberingPerComponentProperty, dto.RestartPositionRenumberingPerComponent);
            LoadProperty(UniqueNumberMultiPositionProductsPerComponentProperty, dto.UniqueNumberMultiPositionProductsPerComponent);
            LoadProperty(ExceptAdjacentPositionsProperty, dto.ExceptAdjacentPositions);
            LoadProperty(ExtendedDataProperty, dto.ExtendedData);
            LoadProperty<Boolean>(IsEnabledProperty, dto.IsEnabled);
            LoadProperty(RestartPositionRenumberingPerBayProperty, dto.RestartPositionRenumberingPerBay);
            LoadProperty(RestartComponentRenumberingPerComponentTypeProperty, dto.RestartComponentRenumberingPerComponentType);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling DataPortal.FetchChild on this instance.
        /// </summary>
        /// <param name="criteria">The criteria object to perform the fetch.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            var dalFactory = GetDalFactory(criteria.DalFactoryName);
            using (var dalContext = dalFactory.CreateContext())
            {
                try
                {
                    using (IPlanogramRenumberingStrategyDal dal = dalContext.GetDal<IPlanogramRenumberingStrategyDal>())
                    {
                        this.LoadDataTransferObject(
                            dalContext,
                            dal.FetchByPlanogramId(criteria.PlanogramId));
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    this.Create();
                }
            }
            MarkAsChild();
        }

        /// <summary>
        ///     Invoked by CSLA via reflection when calling DataPortal.FetchChild in this instance.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="PlanogramRenumberingStrategyDto"/> containing the values to be loaded.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramRenumberingStrategyDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Invoked by CSLA via reflection when calling Save on this instance, and it is new.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramRenumberingStrategyDto>(
            (dc) =>
            {
                PlanogramRenumberingStrategyDto dto = GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty(IdProperty, dto.Id);
                dc.RegisterId<PlanogramRenumberingStrategy>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        ///     Invoked by CSLA via reflection when calling Save on this instance, and it is not new.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramRenumberingStrategyDto>(
                (dc) =>
                {
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        ///     Invoked by CSLA via reflection when calling Delete on this instance.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramRenumberingStrategyDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
