﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-24290 : K.Pickup/A.kuszyk
//  Ongoing work toward an initial planogram structure that can be saved.
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-26041 : A.Kuszyk
//  Added additional Product properties.
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27606 : L.Ineson
//  Added FinancialGroupCode and FinancialGroupName
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
// V8-27636 : L.Ineson
//  Added MetaTotalUnits
#endregion
#region Version History: CCM802

// V8-28998 : A.Kuszyk
//  Added PlanogramProductBlockingColour.
// V8-28811 : L.Luong
//  Added MetaPlanogramLinearSpacePercentageProperty
//  Added MetaPlanogramAreaSpacePercentageProperty
//  Added MetaPlanogramVolumetricSpacePercentageProperty
// V8-29232 : A.Silva
//      Added PlanogramProductSequenceNumber.

#endregion
#region Version History: CCM803
// V8-28491 : L.Luong
//  Added MetaIsInMasterData
// V8-29844 : L.Ineson
//  Added more metadata
#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//  Added ColourGroupValue property.
// V8-30763 : I.George
//  Added metaDataCDTNode property
// V8-31164 : D.Pleasance
//  Removed BlockingColour \ SequenceNumber
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attribute data
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31947 : A.Silva
//  Added MetaComparisonStatus field.
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32609 : L.Ineson
//  Removed old PegProngOffset field
// V8-32819 : M.Pettit
//  Added PlanogramProduct_MetaIsBuddied
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramProduct
    {      
        #region Constructor
        private PlanogramProduct() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramProduct Fetch(IDalContext dalContext, PlanogramProductDto planogramProductDto, CustomAttributeDataDto customAttributeDataDto)
        {
            return DataPortal.FetchChild<PlanogramProduct>(dalContext, planogramProductDto, customAttributeDataDto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramProductDto planogramProductDto, CustomAttributeDataDto customAttributeDataDto = null)
        {
            this.LoadProperty<Object>(IdProperty, planogramProductDto.Id);
            this.LoadProperty<String>(GtinProperty, planogramProductDto.Gtin);
            this.LoadProperty<String>(NameProperty, planogramProductDto.Name);
            this.LoadProperty<String>(BrandProperty, planogramProductDto.Brand);
            this.LoadProperty<Single>(HeightProperty, planogramProductDto.Height);
            this.LoadProperty<Single>(WidthProperty, planogramProductDto.Width);
            this.LoadProperty<Single>(DepthProperty, planogramProductDto.Depth);
            this.LoadProperty<Single>(DisplayHeightProperty, planogramProductDto.DisplayHeight);
            this.LoadProperty<Single>(DisplayWidthProperty, planogramProductDto.DisplayWidth);
            this.LoadProperty<Single>(DisplayDepthProperty, planogramProductDto.DisplayDepth);
            this.LoadProperty<Single>(AlternateHeightProperty, planogramProductDto.AlternateHeight);
            this.LoadProperty<Single>(AlternateWidthProperty, planogramProductDto.AlternateWidth);
            this.LoadProperty<Single>(AlternateDepthProperty, planogramProductDto.AlternateDepth);
            this.LoadProperty<Single>(PointOfPurchaseHeightProperty, planogramProductDto.PointOfPurchaseHeight);
            this.LoadProperty<Single>(PointOfPurchaseWidthProperty, planogramProductDto.PointOfPurchaseWidth);
            this.LoadProperty<Single>(PointOfPurchaseDepthProperty, planogramProductDto.PointOfPurchaseDepth);
            this.LoadProperty<Byte>(NumberOfPegHolesProperty, planogramProductDto.NumberOfPegHoles);
            this.LoadProperty<Single>(PegXProperty, planogramProductDto.PegX);
            this.LoadProperty<Single>(PegX2Property, planogramProductDto.PegX2);
            this.LoadProperty<Single>(PegX3Property, planogramProductDto.PegX3);
            this.LoadProperty<Single>(PegYProperty, planogramProductDto.PegY);
            this.LoadProperty<Single>(PegY2Property, planogramProductDto.PegY2);
            this.LoadProperty<Single>(PegY3Property, planogramProductDto.PegY3);
            this.LoadProperty<Single>(PegProngOffsetXProperty, planogramProductDto.PegProngOffsetX);
            this.LoadProperty<Single>(PegProngOffsetYProperty, planogramProductDto.PegProngOffsetY);
            this.LoadProperty<Single>(PegDepthProperty, planogramProductDto.PegDepth);
            this.LoadProperty<Single>(SqueezeHeightProperty, planogramProductDto.SqueezeHeight);
            this.LoadProperty<Single>(SqueezeWidthProperty, planogramProductDto.SqueezeWidth);
            this.LoadProperty<Single>(SqueezeDepthProperty, planogramProductDto.SqueezeDepth);
            this.LoadProperty<Single>(NestingHeightProperty, planogramProductDto.NestingHeight);
            this.LoadProperty<Single>(NestingWidthProperty, planogramProductDto.NestingWidth);
            this.LoadProperty<Single>(NestingDepthProperty, planogramProductDto.NestingDepth);
            this.LoadProperty<Int16>(CasePackUnitsProperty, planogramProductDto.CasePackUnits);
            this.LoadProperty<Byte>(CaseHighProperty, planogramProductDto.CaseHigh);
            this.LoadProperty<Byte>(CaseWideProperty, planogramProductDto.CaseWide);
            this.LoadProperty<Byte>(CaseDeepProperty, planogramProductDto.CaseDeep);
            this.LoadProperty<Single>(CaseHeightProperty, planogramProductDto.CaseHeight);
            this.LoadProperty<Single>(CaseWidthProperty, planogramProductDto.CaseWidth);
            this.LoadProperty<Single>(CaseDepthProperty, planogramProductDto.CaseDepth);
            this.LoadProperty<Byte>(MaxStackProperty, planogramProductDto.MaxStack);
            this.LoadProperty<Byte>(MaxTopCapProperty, planogramProductDto.MaxTopCap);
            this.LoadProperty<Byte>(MaxRightCapProperty, planogramProductDto.MaxRightCap);
            this.LoadProperty<Byte>(MinDeepProperty, planogramProductDto.MinDeep);
            this.LoadProperty<Byte>(MaxDeepProperty, planogramProductDto.MaxDeep);
            this.LoadProperty<Int16>(TrayPackUnitsProperty, planogramProductDto.TrayPackUnits);
            this.LoadProperty<Byte>(TrayHighProperty, planogramProductDto.TrayHigh);
            this.LoadProperty<Byte>(TrayWideProperty, planogramProductDto.TrayWide);
            this.LoadProperty<Byte>(TrayDeepProperty, planogramProductDto.TrayDeep);
            this.LoadProperty<Single>(TrayHeightProperty, planogramProductDto.TrayHeight);
            this.LoadProperty<Single>(TrayWidthProperty, planogramProductDto.TrayWidth);
            this.LoadProperty<Single>(TrayDepthProperty, planogramProductDto.TrayDepth);
            this.LoadProperty<Single>(TrayThickHeightProperty, planogramProductDto.TrayThickHeight);
            this.LoadProperty<Single>(TrayThickWidthProperty, planogramProductDto.TrayThickWidth);
            this.LoadProperty<Single>(TrayThickDepthProperty, planogramProductDto.TrayThickDepth);
            this.LoadProperty<Single>(FrontOverhangProperty, planogramProductDto.FrontOverhang);
            this.LoadProperty<Single>(FingerSpaceAboveProperty, planogramProductDto.FingerSpaceAbove);
            this.LoadProperty<Single>(FingerSpaceToTheSideProperty, planogramProductDto.FingerSpaceToTheSide);
            this.LoadProperty<PlanogramProductStatusType>(StatusTypeProperty, (PlanogramProductStatusType)planogramProductDto.StatusType);
            this.LoadProperty<PlanogramProductOrientationType>(OrientationTypeProperty, (PlanogramProductOrientationType)planogramProductDto.OrientationType);
            this.LoadProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty, (PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle);
            this.LoadProperty<Boolean>(IsFrontOnlyProperty, planogramProductDto.IsFrontOnly);
            this.LoadProperty<Boolean>(IsPlaceHolderProductProperty, planogramProductDto.IsPlaceHolderProduct);
            this.LoadProperty<Boolean>(IsActiveProperty, planogramProductDto.IsActive);
            this.LoadProperty<Boolean>(CanBreakTrayUpProperty, planogramProductDto.CanBreakTrayUp);
            this.LoadProperty<Boolean>(CanBreakTrayDownProperty, planogramProductDto.CanBreakTrayDown);
            this.LoadProperty<Boolean>(CanBreakTrayBackProperty, planogramProductDto.CanBreakTrayBack);
            this.LoadProperty<Boolean>(CanBreakTrayTopProperty, planogramProductDto.CanBreakTrayTop);
            this.LoadProperty<Boolean>(ForceMiddleCapProperty, planogramProductDto.ForceMiddleCap);
            this.LoadProperty<Boolean>(ForceBottomCapProperty, planogramProductDto.ForceBottomCap);
            this.LoadProperty<Object>(PlanogramImageIdFrontProperty, planogramProductDto.PlanogramImageIdFront);
            this.LoadProperty<Object>(PlanogramImageIdBackProperty, planogramProductDto.PlanogramImageIdBack);
            this.LoadProperty<Object>(PlanogramImageIdTopProperty, planogramProductDto.PlanogramImageIdTop);
            this.LoadProperty<Object>(PlanogramImageIdBottomProperty, planogramProductDto.PlanogramImageIdBottom);
            this.LoadProperty<Object>(PlanogramImageIdLeftProperty, planogramProductDto.PlanogramImageIdLeft);
            this.LoadProperty<Object>(PlanogramImageIdRightProperty, planogramProductDto.PlanogramImageIdRight);
            this.LoadProperty<Object>(PlanogramImageIdDisplayFrontProperty, planogramProductDto.PlanogramImageIdDisplayFront);
            this.LoadProperty<Object>(PlanogramImageIdDisplayBackProperty, planogramProductDto.PlanogramImageIdDisplayBack);
            this.LoadProperty<Object>(PlanogramImageIdDisplayTopProperty, planogramProductDto.PlanogramImageIdDisplayTop);
            this.LoadProperty<Object>(PlanogramImageIdDisplayBottomProperty, planogramProductDto.PlanogramImageIdDisplayBottom);
            this.LoadProperty<Object>(PlanogramImageIdDisplayLeftProperty, planogramProductDto.PlanogramImageIdDisplayLeft);
            this.LoadProperty<Object>(PlanogramImageIdDisplayRightProperty, planogramProductDto.PlanogramImageIdDisplayRight);
            this.LoadProperty<Object>(PlanogramImageIdTrayFrontProperty, planogramProductDto.PlanogramImageIdTrayFront);
            this.LoadProperty<Object>(PlanogramImageIdTrayBackProperty, planogramProductDto.PlanogramImageIdTrayBack);
            this.LoadProperty<Object>(PlanogramImageIdTrayTopProperty, planogramProductDto.PlanogramImageIdTrayTop);
            this.LoadProperty<Object>(PlanogramImageIdTrayBottomProperty, planogramProductDto.PlanogramImageIdTrayBottom);
            this.LoadProperty<Object>(PlanogramImageIdTrayLeftProperty, planogramProductDto.PlanogramImageIdTrayLeft);
            this.LoadProperty<Object>(PlanogramImageIdTrayRightProperty, planogramProductDto.PlanogramImageIdTrayRight);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseFront);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseBack);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseTop);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseBottom);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseLeft);
            this.LoadProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty, planogramProductDto.PlanogramImageIdPointOfPurchaseRight);
            this.LoadProperty<Object>(PlanogramImageIdAlternateFrontProperty, planogramProductDto.PlanogramImageIdAlternateFront);
            this.LoadProperty<Object>(PlanogramImageIdAlternateBackProperty, planogramProductDto.PlanogramImageIdAlternateBack);
            this.LoadProperty<Object>(PlanogramImageIdAlternateTopProperty, planogramProductDto.PlanogramImageIdAlternateTop);
            this.LoadProperty<Object>(PlanogramImageIdAlternateBottomProperty, planogramProductDto.PlanogramImageIdAlternateBottom);
            this.LoadProperty<Object>(PlanogramImageIdAlternateLeftProperty, planogramProductDto.PlanogramImageIdAlternateLeft);
            this.LoadProperty<Object>(PlanogramImageIdAlternateRightProperty, planogramProductDto.PlanogramImageIdAlternateRight);
            this.LoadProperty<Object>(PlanogramImageIdCaseFrontProperty, planogramProductDto.PlanogramImageIdCaseFront);
            this.LoadProperty<Object>(PlanogramImageIdCaseBackProperty, planogramProductDto.PlanogramImageIdCaseBack);
            this.LoadProperty<Object>(PlanogramImageIdCaseTopProperty, planogramProductDto.PlanogramImageIdCaseTop);
            this.LoadProperty<Object>(PlanogramImageIdCaseBottomProperty, planogramProductDto.PlanogramImageIdCaseBottom);
            this.LoadProperty<Object>(PlanogramImageIdCaseLeftProperty, planogramProductDto.PlanogramImageIdCaseLeft);
            this.LoadProperty<Object>(PlanogramImageIdCaseRightProperty, planogramProductDto.PlanogramImageIdCaseRight);
            this.LoadProperty<Object>(ExtendedDataProperty, planogramProductDto.ExtendedData);
            this.LoadProperty<PlanogramProductShapeType>(ShapeTypeProperty, (PlanogramProductShapeType)planogramProductDto.ShapeType);
            this.LoadProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty, (PlanogramProductFillPatternType)planogramProductDto.FillPatternType);
            this.LoadProperty<Int32>(FillColourProperty, planogramProductDto.FillColour);
            this.LoadProperty<String>(ShapeProperty, planogramProductDto.Shape); 
            this.LoadProperty<String>(PointOfPurchaseDescriptionProperty, planogramProductDto.PointOfPurchaseDescription); 
            this.LoadProperty<String>(ShortDescriptionProperty, planogramProductDto.ShortDescription); 
            this.LoadProperty<String>(SubcategoryProperty, planogramProductDto.Subcategory); 
            this.LoadProperty<String>(CustomerStatusProperty, planogramProductDto.CustomerStatus); 
            this.LoadProperty<String>(ColourProperty, planogramProductDto.Colour); 
            this.LoadProperty<String>(FlavourProperty, planogramProductDto.Flavour); 
            this.LoadProperty<String>(PackagingShapeProperty, planogramProductDto.PackagingShape); 
            this.LoadProperty<String>(PackagingTypeProperty, planogramProductDto.PackagingType); 
            this.LoadProperty<String>(CountryOfOriginProperty, planogramProductDto.CountryOfOrigin); 
            this.LoadProperty<String>(CountryOfProcessingProperty, planogramProductDto.CountryOfProcessing); 
            this.LoadProperty<Int16>(ShelfLifeProperty, planogramProductDto.ShelfLife); 
            this.LoadProperty<Single?>(DeliveryFrequencyDaysProperty, planogramProductDto.DeliveryFrequencyDays); 
            this.LoadProperty<String>(DeliveryMethodProperty, planogramProductDto.DeliveryMethod); 
            this.LoadProperty<String>(VendorCodeProperty, planogramProductDto.VendorCode); 
            this.LoadProperty<String>(VendorProperty, planogramProductDto.Vendor); 
            this.LoadProperty<String>(ManufacturerCodeProperty, planogramProductDto.ManufacturerCode); 
            this.LoadProperty<String>(ManufacturerProperty, planogramProductDto.Manufacturer); 
            this.LoadProperty<String>(SizeProperty, planogramProductDto.Size); 
            this.LoadProperty<String>(UnitOfMeasureProperty, planogramProductDto.UnitOfMeasure); 
            this.LoadProperty<DateTime?>(DateIntroducedProperty, planogramProductDto.DateIntroduced); 
            this.LoadProperty<DateTime?>(DateDiscontinuedProperty, planogramProductDto.DateDiscontinued); 
            this.LoadProperty<DateTime?>(DateEffectiveProperty, planogramProductDto.DateEffective); 
            this.LoadProperty<String>(HealthProperty, planogramProductDto.Health); 
            this.LoadProperty<String>(CorporateCodeProperty, planogramProductDto.CorporateCode); 
            this.LoadProperty<String>(BarcodeProperty, planogramProductDto.Barcode); 
            this.LoadProperty<Single?>(SellPriceProperty, planogramProductDto.SellPrice); 
            this.LoadProperty<Int16?>(SellPackCountProperty, planogramProductDto.SellPackCount); 
            this.LoadProperty<String>(SellPackDescriptionProperty, planogramProductDto.SellPackDescription); 
            this.LoadProperty<Single?>(RecommendedRetailPriceProperty, planogramProductDto.RecommendedRetailPrice); 
            this.LoadProperty<Single?>(ManufacturerRecommendedRetailPriceProperty, planogramProductDto.ManufacturerRecommendedRetailPrice); 
            this.LoadProperty<Single?>(CostPriceProperty, planogramProductDto.CostPrice); 
            this.LoadProperty<Single?>(CaseCostProperty, planogramProductDto.CaseCost); 
            this.LoadProperty<Single?>(TaxRateProperty, planogramProductDto.TaxRate); 
            this.LoadProperty<String>(ConsumerInformationProperty, planogramProductDto.ConsumerInformation); 
            this.LoadProperty<String>(TextureProperty, planogramProductDto.Texture); 
            this.LoadProperty<Int16?>(StyleNumberProperty, planogramProductDto.StyleNumber); 
            this.LoadProperty<String>(PatternProperty, planogramProductDto.Pattern); 
            this.LoadProperty<String>(ModelProperty, planogramProductDto.Model); 
            this.LoadProperty<String>(GarmentTypeProperty, planogramProductDto.GarmentType); 
            this.LoadProperty<Boolean>(IsPrivateLabelProperty, planogramProductDto.IsPrivateLabel);
            this.LoadProperty<Boolean>(IsNewProductProperty, planogramProductDto.IsNewProduct);
            this.LoadProperty<String>(FinancialGroupCodeProperty, planogramProductDto.FinancialGroupCode);
            this.LoadProperty<String>(FinancialGroupNameProperty, planogramProductDto.FinancialGroupName);
            this.LoadProperty<Boolean?>(MetaNotAchievedInventoryProperty, planogramProductDto.MetaNotAchievedInventory);
            this.LoadProperty<Int32?>(MetaTotalUnitsProperty, planogramProductDto.MetaTotalUnits);
            this.LoadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty, planogramProductDto.MetaPlanogramLinearSpacePercentage);
            this.LoadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty, planogramProductDto.MetaPlanogramAreaSpacePercentage);
            this.LoadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty, planogramProductDto.MetaPlanogramVolumetricSpacePercentage);
            this.LoadProperty<Int32?>(MetaPositionCountProperty, planogramProductDto.MetaPositionCount);
            this.LoadProperty<Int32?>(MetaTotalFacingsProperty, planogramProductDto.MetaTotalFacings);
            this.LoadProperty<Single?>(MetaTotalLinearSpaceProperty, planogramProductDto.MetaTotalLinearSpace);
            this.LoadProperty<Single?>(MetaTotalAreaSpaceProperty, planogramProductDto.MetaTotalAreaSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricSpaceProperty, planogramProductDto.MetaTotalVolumetricSpace);
            this.LoadProperty<Boolean?>(MetaIsInMasterDataProperty, planogramProductDto.MetaIsInMasterData);
            this.LoadProperty<Boolean?>(MetaNotAchievedCasesProperty, planogramProductDto.MetaNotAchievedCases);
            this.LoadProperty<Boolean?>(MetaNotAchievedDOSProperty, planogramProductDto.MetaNotAchievedDOS);
            this.LoadProperty<Boolean?>(MetaIsOverShelfLifePercentProperty, planogramProductDto.MetaIsOverShelfLifePercent);
            this.LoadProperty<Boolean?>(MetaNotAchievedDeliveriesProperty, planogramProductDto.MetaNotAchievedDeliveries);
            this.LoadProperty<Int32?>(MetaTotalMainFacingsProperty, planogramProductDto.MetaTotalMainFacings);
            this.LoadProperty<Int32?>(MetaTotalXFacingsProperty, planogramProductDto.MetaTotalXFacings);
            this.LoadProperty<Int32?>(MetaTotalYFacingsProperty, planogramProductDto.MetaTotalYFacings);
            this.LoadProperty<Int32?>(MetaTotalZFacingsProperty, planogramProductDto.MetaTotalZFacings);
            this.LoadProperty<Boolean?>(MetaIsRangedInAssortmentProperty, planogramProductDto.MetaIsRangedInAssortment);
            this.LoadProperty<String>(ColourGroupValueProperty, planogramProductDto.ColourGroupValue);
            this.LoadProperty<String>(MetaCDTNodeProperty, planogramProductDto.MetaCDTNode);
            this.LoadProperty<Boolean?>(MetaIsProductRuleBrokenProperty, planogramProductDto.MetaIsProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsFamilyRuleBrokenProperty, planogramProductDto.MetaIsFamilyRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsInheritanceRuleBrokenProperty, planogramProductDto.MetaIsInheritanceRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsLocalProductRuleBrokenProperty, planogramProductDto.MetaIsLocalProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsDistributionRuleBrokenProperty, planogramProductDto.MetaIsDistributionRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsCoreRuleBrokenProperty, planogramProductDto.MetaIsCoreRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsDelistProductRuleBrokenProperty, planogramProductDto.MetaIsDelistProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsForceProductRuleBrokenProperty, planogramProductDto.MetaIsForceProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsPreserveProductRuleBrokenProperty, planogramProductDto.MetaIsPreserveProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsMinimumHurdleProductRuleBrokenProperty, planogramProductDto.MetaIsMinimumHurdleProductRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsMaximumProductFamilyRuleBrokenProperty, planogramProductDto.MetaIsMaximumProductFamilyRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsMinimumProductFamilyRuleBrokenProperty, planogramProductDto.MetaIsMinimumProductFamilyRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsDependencyFamilyRuleBrokenProperty, planogramProductDto.MetaIsDependencyFamilyRuleBroken);
            this.LoadProperty<Boolean?>(MetaIsDelistFamilyRuleBrokenProperty, planogramProductDto.MetaIsDelistFamilyRuleBroken);
            this.LoadProperty<PlanogramItemComparisonStatusType>(MetaComparisonStatusProperty, (PlanogramItemComparisonStatusType)planogramProductDto.MetaComparisonStatus);
            this.LoadProperty<Boolean?>(MetaIsBuddiedProperty, planogramProductDto.MetaIsBuddied);
       
            if (customAttributeDataDto == null) customAttributeDataDto = new CustomAttributeDataDto();
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.Fetch(dalContext, customAttributeDataDto));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramProductDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramProductDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                Gtin = this.ReadProperty<String>(GtinProperty),
                Name = this.ReadProperty<String>(NameProperty),
                Brand = this.ReadProperty<String>(BrandProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                DisplayHeight = this.ReadProperty<Single>(DisplayHeightProperty),
                DisplayWidth = this.ReadProperty<Single>(DisplayWidthProperty),
                DisplayDepth = this.ReadProperty<Single>(DisplayDepthProperty),
                AlternateHeight = this.ReadProperty<Single>(AlternateHeightProperty),
                AlternateWidth = this.ReadProperty<Single>(AlternateWidthProperty),
                AlternateDepth = this.ReadProperty<Single>(AlternateDepthProperty),
                PointOfPurchaseHeight = this.ReadProperty<Single>(PointOfPurchaseHeightProperty),
                PointOfPurchaseWidth = this.ReadProperty<Single>(PointOfPurchaseWidthProperty),
                PointOfPurchaseDepth = this.ReadProperty<Single>(PointOfPurchaseDepthProperty),
                NumberOfPegHoles = this.ReadProperty<Byte>(NumberOfPegHolesProperty),
                PegX = this.ReadProperty<Single>(PegXProperty),
                PegX2 = this.ReadProperty<Single>(PegX2Property),
                PegX3 = this.ReadProperty<Single>(PegX3Property),
                PegY = this.ReadProperty<Single>(PegYProperty),
                PegY2 = this.ReadProperty<Single>(PegY2Property),
                PegY3 = this.ReadProperty<Single>(PegY3Property),
                PegProngOffsetX = this.ReadProperty<Single>(PegProngOffsetXProperty),
                PegProngOffsetY = this.ReadProperty<Single>(PegProngOffsetYProperty),
                PegDepth = this.ReadProperty<Single>(PegDepthProperty),
                SqueezeHeight = this.ReadProperty<Single>(SqueezeHeightProperty),
                SqueezeWidth = this.ReadProperty<Single>(SqueezeWidthProperty),
                SqueezeDepth = this.ReadProperty<Single>(SqueezeDepthProperty),
                NestingHeight = this.ReadProperty<Single>(NestingHeightProperty),
                NestingWidth = this.ReadProperty<Single>(NestingWidthProperty),
                NestingDepth = this.ReadProperty<Single>(NestingDepthProperty),
                CasePackUnits = this.ReadProperty<Int16>(CasePackUnitsProperty),
                CaseHigh = this.ReadProperty<Byte>(CaseHighProperty),
                CaseWide = this.ReadProperty<Byte>(CaseWideProperty),
                CaseDeep = this.ReadProperty<Byte>(CaseDeepProperty),
                CaseHeight = this.ReadProperty<Single>(CaseHeightProperty),
                CaseWidth = this.ReadProperty<Single>(CaseWidthProperty),
                CaseDepth = this.ReadProperty<Single>(CaseDepthProperty),
                MaxStack = this.ReadProperty<Byte>(MaxStackProperty),
                MaxTopCap = this.ReadProperty<Byte>(MaxTopCapProperty),
                MaxRightCap = this.ReadProperty<Byte>(MaxRightCapProperty),
                MinDeep = this.ReadProperty<Byte>(MinDeepProperty),
                MaxDeep = this.ReadProperty<Byte>(MaxDeepProperty),
                TrayPackUnits = this.ReadProperty<Int16>(TrayPackUnitsProperty),
                TrayHigh = this.ReadProperty<Byte>(TrayHighProperty),
                TrayWide = this.ReadProperty<Byte>(TrayWideProperty),
                TrayDeep = this.ReadProperty<Byte>(TrayDeepProperty),
                TrayHeight = this.ReadProperty<Single>(TrayHeightProperty),
                TrayWidth = this.ReadProperty<Single>(TrayWidthProperty),
                TrayDepth = this.ReadProperty<Single>(TrayDepthProperty),
                TrayThickHeight = this.ReadProperty<Single>(TrayThickHeightProperty),
                TrayThickWidth = this.ReadProperty<Single>(TrayThickWidthProperty),
                TrayThickDepth = this.ReadProperty<Single>(TrayThickDepthProperty),
                FrontOverhang = this.ReadProperty<Single>(FrontOverhangProperty),
                FingerSpaceAbove = this.ReadProperty<Single>(FingerSpaceAboveProperty),
                FingerSpaceToTheSide = this.ReadProperty<Single>(FingerSpaceToTheSideProperty),
                StatusType = (Byte)ReadProperty<PlanogramProductStatusType>(StatusTypeProperty),
                OrientationType = (Byte)ReadProperty<PlanogramProductOrientationType>(OrientationTypeProperty),
                MerchandisingStyle = (Byte)ReadProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty),
                IsFrontOnly = this.ReadProperty<Boolean>(IsFrontOnlyProperty),
                IsPlaceHolderProduct = this.ReadProperty<Boolean>(IsPlaceHolderProductProperty),
                IsActive = this.ReadProperty<Boolean>(IsActiveProperty),
                CanBreakTrayUp = this.ReadProperty<Boolean>(CanBreakTrayUpProperty),
                CanBreakTrayDown = this.ReadProperty<Boolean>(CanBreakTrayDownProperty),
                CanBreakTrayBack = this.ReadProperty<Boolean>(CanBreakTrayBackProperty),
                CanBreakTrayTop = this.ReadProperty<Boolean>(CanBreakTrayTopProperty),
                ForceMiddleCap = this.ReadProperty<Boolean>(ForceMiddleCapProperty),
                ForceBottomCap = this.ReadProperty<Boolean>(ForceBottomCapProperty),
                PlanogramImageIdFront = this.ReadProperty<Object>(PlanogramImageIdFrontProperty),
                PlanogramImageIdBack = this.ReadProperty<Object>(PlanogramImageIdBackProperty),
                PlanogramImageIdTop = this.ReadProperty<Object>(PlanogramImageIdTopProperty),
                PlanogramImageIdBottom = this.ReadProperty<Object>(PlanogramImageIdBottomProperty),
                PlanogramImageIdLeft = this.ReadProperty<Object>(PlanogramImageIdLeftProperty),
                PlanogramImageIdRight = this.ReadProperty<Object>(PlanogramImageIdRightProperty),
                PlanogramImageIdDisplayFront = this.ReadProperty<Object>(PlanogramImageIdDisplayFrontProperty),
                PlanogramImageIdDisplayBack = this.ReadProperty<Object>(PlanogramImageIdDisplayBackProperty),
                PlanogramImageIdDisplayTop = this.ReadProperty<Object>(PlanogramImageIdDisplayTopProperty),
                PlanogramImageIdDisplayBottom = this.ReadProperty<Object>(PlanogramImageIdDisplayBottomProperty),
                PlanogramImageIdDisplayLeft = this.ReadProperty<Object>(PlanogramImageIdDisplayLeftProperty),
                PlanogramImageIdDisplayRight = this.ReadProperty<Object>(PlanogramImageIdDisplayRightProperty),
                PlanogramImageIdTrayFront = this.ReadProperty<Object>(PlanogramImageIdTrayFrontProperty),
                PlanogramImageIdTrayBack = this.ReadProperty<Object>(PlanogramImageIdTrayBackProperty),
                PlanogramImageIdTrayTop = this.ReadProperty<Object>(PlanogramImageIdTrayTopProperty),
                PlanogramImageIdTrayBottom = this.ReadProperty<Object>(PlanogramImageIdTrayBottomProperty),
                PlanogramImageIdTrayLeft = this.ReadProperty<Object>(PlanogramImageIdTrayLeftProperty),
                PlanogramImageIdTrayRight = this.ReadProperty<Object>(PlanogramImageIdTrayRightProperty),
                PlanogramImageIdPointOfPurchaseFront = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseFrontProperty),
                PlanogramImageIdPointOfPurchaseBack = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBackProperty),
                PlanogramImageIdPointOfPurchaseTop = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseTopProperty),
                PlanogramImageIdPointOfPurchaseBottom = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseBottomProperty),
                PlanogramImageIdPointOfPurchaseLeft = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseLeftProperty),
                PlanogramImageIdPointOfPurchaseRight = this.ReadProperty<Object>(PlanogramImageIdPointOfPurchaseRightProperty),
                PlanogramImageIdAlternateFront = this.ReadProperty<Object>(PlanogramImageIdAlternateFrontProperty),
                PlanogramImageIdAlternateBack = this.ReadProperty<Object>(PlanogramImageIdAlternateBackProperty),
                PlanogramImageIdAlternateTop = this.ReadProperty<Object>(PlanogramImageIdAlternateTopProperty),
                PlanogramImageIdAlternateBottom = this.ReadProperty<Object>(PlanogramImageIdAlternateBottomProperty),
                PlanogramImageIdAlternateLeft = this.ReadProperty<Object>(PlanogramImageIdAlternateLeftProperty),
                PlanogramImageIdAlternateRight = this.ReadProperty<Object>(PlanogramImageIdAlternateRightProperty),
                PlanogramImageIdCaseFront = this.ReadProperty<Object>(PlanogramImageIdCaseFrontProperty),
                PlanogramImageIdCaseBack = this.ReadProperty<Object>(PlanogramImageIdCaseBackProperty),
                PlanogramImageIdCaseTop = this.ReadProperty<Object>(PlanogramImageIdCaseTopProperty),
                PlanogramImageIdCaseBottom = this.ReadProperty<Object>(PlanogramImageIdCaseBottomProperty),
                PlanogramImageIdCaseLeft = this.ReadProperty<Object>(PlanogramImageIdCaseLeftProperty),
                PlanogramImageIdCaseRight = this.ReadProperty<Object>(PlanogramImageIdCaseRightProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                ShapeType=(Byte)this.ReadProperty<PlanogramProductShapeType>(ShapeTypeProperty),
                FillPatternType=(Byte)this.ReadProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty),
                FillColour=this.ReadProperty<Int32>(FillColourProperty),
                Shape = this.ReadProperty<String>(ShapeProperty),
                PointOfPurchaseDescription = this.ReadProperty<String>(PointOfPurchaseDescriptionProperty), 
                ShortDescription = this.ReadProperty<String>(ShortDescriptionProperty), 
                Subcategory = this.ReadProperty<String>(SubcategoryProperty), 
                CustomerStatus = this.ReadProperty<String>(CustomerStatusProperty), 
                Colour = this.ReadProperty<String>(ColourProperty), 
                Flavour = this.ReadProperty<String>(FlavourProperty), 
                PackagingShape = this.ReadProperty<String>(PackagingShapeProperty), 
                PackagingType = this.ReadProperty<String>(PackagingTypeProperty), 
                CountryOfOrigin = this.ReadProperty<String>(CountryOfOriginProperty), 
                CountryOfProcessing = this.ReadProperty<String>(CountryOfProcessingProperty), 
                ShelfLife = this.ReadProperty<Int16>(ShelfLifeProperty), 
                DeliveryFrequencyDays = this.ReadProperty<Single?>(DeliveryFrequencyDaysProperty), 
                DeliveryMethod = this.ReadProperty<String>(DeliveryMethodProperty), 
                VendorCode = this.ReadProperty<String>(VendorCodeProperty), 
                Vendor = this.ReadProperty<String>(VendorProperty), 
                ManufacturerCode = this.ReadProperty<String>(ManufacturerCodeProperty), 
                Manufacturer = this.ReadProperty<String>(ManufacturerProperty), 
                Size = this.ReadProperty<String>(SizeProperty), 
                UnitOfMeasure = this.ReadProperty<String>(UnitOfMeasureProperty), 
                DateIntroduced = this.ReadProperty<DateTime?>(DateIntroducedProperty), 
                DateDiscontinued = this.ReadProperty<DateTime?>(DateDiscontinuedProperty), 
                DateEffective = this.ReadProperty<DateTime?>(DateEffectiveProperty), 
                Health = this.ReadProperty<String>(HealthProperty), 
                CorporateCode = this.ReadProperty<String>(CorporateCodeProperty), 
                Barcode = this.ReadProperty<String>(BarcodeProperty), 
                SellPrice = this.ReadProperty<Single?>(SellPriceProperty), 
                SellPackCount = this.ReadProperty<Int16?>(SellPackCountProperty), 
                SellPackDescription = this.ReadProperty<String>(SellPackDescriptionProperty), 
                RecommendedRetailPrice = this.ReadProperty<Single?>(RecommendedRetailPriceProperty), 
                ManufacturerRecommendedRetailPrice = this.ReadProperty<Single?>(ManufacturerRecommendedRetailPriceProperty), 
                CostPrice = this.ReadProperty<Single?>(CostPriceProperty), 
                CaseCost = this.ReadProperty<Single?>(CaseCostProperty), 
                TaxRate = this.ReadProperty<Single?>(TaxRateProperty), 
                ConsumerInformation = this.ReadProperty<String>(ConsumerInformationProperty), 
                Texture = this.ReadProperty<String>(TextureProperty), 
                StyleNumber = this.ReadProperty<Int16?>(StyleNumberProperty), 
                Pattern = this.ReadProperty<String>(PatternProperty), 
                Model = this.ReadProperty<String>(ModelProperty), 
                GarmentType = this.ReadProperty<String>(GarmentTypeProperty), 
                IsPrivateLabel = this.ReadProperty<Boolean>(IsPrivateLabelProperty), 
                IsNewProduct = this.ReadProperty<Boolean>(IsNewProductProperty),
                FinancialGroupCode = this.ReadProperty<String>(FinancialGroupCodeProperty),
                FinancialGroupName = this.ReadProperty<String>(FinancialGroupNameProperty),
                MetaNotAchievedInventory = this.ReadProperty<Boolean?>(MetaNotAchievedInventoryProperty),
                MetaTotalUnits = this.ReadProperty<Int32?>(MetaTotalUnitsProperty),
                MetaPlanogramLinearSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramLinearSpacePercentageProperty),
                MetaPlanogramAreaSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramAreaSpacePercentageProperty),
                MetaPlanogramVolumetricSpacePercentage = this.ReadProperty<Single?>(MetaPlanogramVolumetricSpacePercentageProperty),
                MetaPositionCount = this.ReadProperty<Int32?>(MetaPositionCountProperty),
                MetaTotalFacings = this.ReadProperty<Int32?>(MetaTotalFacingsProperty),
                MetaTotalLinearSpace = this.ReadProperty<Single?>(MetaTotalLinearSpaceProperty),
                MetaTotalAreaSpace = this.ReadProperty<Single?>(MetaTotalAreaSpaceProperty),
                MetaTotalVolumetricSpace = this.ReadProperty<Single?>(MetaTotalVolumetricSpaceProperty),
                MetaIsInMasterData = this.ReadProperty<Boolean?>(MetaIsInMasterDataProperty),
                MetaNotAchievedCases = this.ReadProperty<Boolean?>(MetaNotAchievedCasesProperty),
                MetaNotAchievedDOS = this.ReadProperty<Boolean?>(MetaNotAchievedDOSProperty),
                MetaIsOverShelfLifePercent = this.ReadProperty<Boolean?>(MetaIsOverShelfLifePercentProperty),
                MetaNotAchievedDeliveries = this.ReadProperty<Boolean?>(MetaNotAchievedDeliveriesProperty),
                MetaTotalMainFacings = this.ReadProperty<Int32?>(MetaTotalMainFacingsProperty),
                MetaTotalXFacings = this.ReadProperty<Int32?>(MetaTotalXFacingsProperty),
                MetaTotalYFacings = this.ReadProperty<Int32?>(MetaTotalYFacingsProperty),
                MetaTotalZFacings = this.ReadProperty<Int32?>(MetaTotalZFacingsProperty),
                MetaIsRangedInAssortment = this.ReadProperty<Boolean?>(MetaIsRangedInAssortmentProperty),
                ColourGroupValue = this.ReadProperty<String>(ColourGroupValueProperty),
                MetaCDTNode = this.ReadProperty<String>(MetaCDTNodeProperty),
                MetaIsProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsProductRuleBrokenProperty),
                MetaIsFamilyRuleBroken = this.ReadProperty<Boolean?>(MetaIsFamilyRuleBrokenProperty),
                MetaIsInheritanceRuleBroken = this.ReadProperty<Boolean?>(MetaIsInheritanceRuleBrokenProperty),
                MetaIsLocalProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsLocalProductRuleBrokenProperty),
                MetaIsDistributionRuleBroken = this.ReadProperty<Boolean?>(MetaIsDistributionRuleBrokenProperty),
                MetaIsCoreRuleBroken = this.ReadProperty<Boolean?>(MetaIsCoreRuleBrokenProperty),
                MetaIsDelistProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsDelistProductRuleBrokenProperty),
                MetaIsForceProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsForceProductRuleBrokenProperty),
                MetaIsPreserveProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsPreserveProductRuleBrokenProperty),
                MetaIsMinimumHurdleProductRuleBroken = this.ReadProperty<Boolean?>(MetaIsMinimumHurdleProductRuleBrokenProperty),
                MetaIsMaximumProductFamilyRuleBroken = this.ReadProperty<Boolean?>(MetaIsMaximumProductFamilyRuleBrokenProperty),
                MetaIsMinimumProductFamilyRuleBroken = this.ReadProperty<Boolean?>(MetaIsMinimumProductFamilyRuleBrokenProperty),
                MetaIsDependencyFamilyRuleBroken = this.ReadProperty<Boolean?>(MetaIsDependencyFamilyRuleBrokenProperty),
                MetaIsDelistFamilyRuleBroken = this.ReadProperty<Boolean?>(MetaIsDelistFamilyRuleBrokenProperty),
                MetaComparisonStatus = (Byte)this.ReadProperty<PlanogramItemComparisonStatusType>(MetaComparisonStatusProperty),
                MetaIsBuddied = this.ReadProperty<Boolean?>(MetaIsBuddiedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramProductDto planogramProductDto, CustomAttributeDataDto customAttributeDataDto)
        {
            this.LoadDataTransferObject(dalContext, planogramProductDto, customAttributeDataDto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramProductDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramProductDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramProduct>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramProductDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramProductDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}
