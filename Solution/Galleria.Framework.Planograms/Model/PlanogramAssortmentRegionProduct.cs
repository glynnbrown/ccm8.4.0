﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
// V8-26491 : A.Kuszyk
//  Added null reference check to Parent property getter.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegionProduct.
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    [Serializable]
    public partial class PlanogramAssortmentRegionProduct : ModelObject<PlanogramAssortmentRegionProduct>, IPlanogramAssortmentRegionProduct
    {
        #region Properties

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramAssortmentRegion Parent
        {
            get 
            {
                if (base.Parent == null) return null;
                return (PlanogramAssortmentRegion)((PlanogramAssortmentRegionProductList)base.Parent).Parent; 
            }
        }
        #endregion

        #region PrimaryProductGtin
        /// <summary>
        /// PrimaryProductGtin property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PrimaryProductGtinProperty =
            RegisterModelProperty<String>(c => c.PrimaryProductGtin);
        /// <summary>
        /// PrimaryProductGtin
        /// </summary>
        public String PrimaryProductGtin
        {
            get { return GetProperty<String>(PrimaryProductGtinProperty); }
            set { SetProperty<String>(PrimaryProductGtinProperty, value); }
        }
        #endregion

        #region RegionalProductGtin
        /// <summary>
        /// RegionalProductGtin property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionalProductGtinProperty =
            RegisterModelProperty<String>(c => c.RegionalProductGtin);
        /// <summary>
        /// RegionalProductGtin
        /// </summary>
        public String RegionalProductGtin
        {
            get { return GetProperty<String>(RegionalProductGtinProperty); }
            set { SetProperty<String>(RegionalProductGtinProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(PrimaryProductGtinProperty));
            BusinessRules.AddRule(new MaxLength(PrimaryProductGtinProperty, 14));
            BusinessRules.AddRule(new MaxLength(RegionalProductGtinProperty, 14));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramAssortmentRegionProduct NewPlanogramAssortmentRegionProduct()
        {
            var item = new PlanogramAssortmentRegionProduct();
            item.Create();
            return item;
        }

        public static PlanogramAssortmentRegionProduct NewPlanogramAssortmentRegionProduct(IPlanogramAssortmentRegionProduct regionProduct)
        {
            var item = new PlanogramAssortmentRegionProduct();
            item.Create(regionProduct);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        private void Create(IPlanogramAssortmentRegionProduct regionProduct)
        {
            LoadProperty<String>(PrimaryProductGtinProperty, regionProduct.PrimaryProductGtin);
            LoadProperty<String>(RegionalProductGtinProperty, regionProduct.RegionalProductGtin);
            this.Create();
        }
        #endregion

        #endregion
    }
}
