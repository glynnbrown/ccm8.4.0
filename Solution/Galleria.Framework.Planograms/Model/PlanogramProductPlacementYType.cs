﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27154 : L.Luong
//  Created

#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Model
{
    /// <summary>
    /// Denotes the available Product Placement Y types
    /// </summary>
    public enum PlanogramProductPlacementYType
    {
        Manual = 0,
        FillHigh = 1
    }

    public static class PlanogramProductPlacementYTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramProductPlacementYType, String> FriendlyNames =
            new Dictionary<PlanogramProductPlacementYType, String>()
            {
                {PlanogramProductPlacementYType.Manual, Message.Enum_PlanogramProductPlacementYType_Manual},
                {PlanogramProductPlacementYType.FillHigh, Message.Enum_PlanogramProductPlacementYType_FillHigh},
            };
    }
}
