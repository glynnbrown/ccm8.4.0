﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// CCM-24290 : N.Foster
//  Created
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-27058 : A.Probyn
//  Added new meta data properties
// V8-27547 : L.Ineson
//  Added missing Child_DeleteSelf
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29431 : L.Luong
//  Changed meta Dos and Cases to single
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Model
{
    public partial class PlanogramFixtureItem
    {
        #region Constructor
        private PlanogramFixtureItem() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramFixtureItem Fetch(IDalContext dalContext, PlanogramFixtureItemDto dto)
        {
            return DataPortal.FetchChild<PlanogramFixtureItem>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramFixtureItemDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(PlanogramFixtureIdProperty, dto.PlanogramFixtureId);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(ZProperty, dto.Z);
            this.LoadProperty<Single>(SlopeProperty, dto.Slope);
            this.LoadProperty<Single>(AngleProperty, dto.Angle);
            this.LoadProperty<Single>(RollProperty, dto.Roll);
            this.LoadProperty<Int16>(BaySequenceNumberProperty, dto.BaySequenceNumber);
            this.LoadProperty<Object>(ExtendedDataProperty, dto.ExtendedData);
            this.LoadProperty<Int32?>(MetaComponentCountProperty, dto.MetaComponentCount);
            this.LoadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, dto.MetaTotalMerchandisableLinearSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, dto.MetaTotalMerchandisableAreaSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, dto.MetaTotalMerchandisableVolumetricSpace);
            this.LoadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, dto.MetaTotalLinearWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, dto.MetaTotalAreaWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, dto.MetaTotalVolumetricWhiteSpace);
            this.LoadProperty<Int32?>(MetaProductsPlacedProperty, dto.MetaProductsPlaced);
            this.LoadProperty<Int32?>(MetaNewProductsProperty, dto.MetaNewProducts);
            this.LoadProperty<Int32?>(MetaChangesFromPreviousCountProperty, dto.MetaChangesFromPreviousCount);
            this.LoadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, dto.MetaChangeFromPreviousStarRating);
            this.LoadProperty<Int32?>(MetaBlocksDroppedProperty, dto.MetaBlocksDropped);
            this.LoadProperty<Int32?>(MetaNotAchievedInventoryProperty, dto.MetaNotAchievedInventory);
            this.LoadProperty<Int32?>(MetaTotalFacingsProperty, dto.MetaTotalFacings);
            this.LoadProperty<Int16?>(MetaAverageFacingsProperty, dto.MetaAverageFacings);
            this.LoadProperty<Int32?>(MetaTotalUnitsProperty, dto.MetaTotalUnits);
            this.LoadProperty<Int32?>(MetaAverageUnitsProperty, dto.MetaAverageUnits);
            this.LoadProperty<Single?>(MetaMinDosProperty, dto.MetaMinDos);
            this.LoadProperty<Single?>(MetaMaxDosProperty, dto.MetaMaxDos);
            this.LoadProperty<Single?>(MetaAverageDosProperty, dto.MetaAverageDos);
            this.LoadProperty<Single?>(MetaMinCasesProperty, dto.MetaMinCases);
            this.LoadProperty<Single?>(MetaAverageCasesProperty, dto.MetaAverageCases);
            this.LoadProperty<Single?>(MetaMaxCasesProperty, dto.MetaMaxCases);
            this.LoadProperty<Int16?>(MetaSpaceToUnitsIndexProperty, dto.MetaSpaceToUnitsIndex);
            this.LoadProperty<Int32?>(MetaTotalComponentCollisionsProperty, dto.MetaTotalComponentCollisions);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty, dto.MetaTotalComponentsOverMerchandisedDepth);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty, dto.MetaTotalComponentsOverMerchandisedHeight);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty, dto.MetaTotalComponentsOverMerchandisedWidth);
            this.LoadProperty<Int32?>(MetaTotalPositionCollisionsProperty, dto.MetaTotalPositionCollisions);
            this.LoadProperty<Int16?>(MetaTotalFrontFacingsProperty, dto.MetaTotalFrontFacings);
            this.LoadProperty<Single?>(MetaAverageFrontFacingsProperty, dto.MetaAverageFrontFacings);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramFixtureItemDto GetDataTransferObject(Planogram parent)
        {
            return new PlanogramFixtureItemDto()
            {
                Id = this.ReadProperty<Object>(IdProperty),
                PlanogramId = parent.Id,
                PlanogramFixtureId = this.ReadProperty<Object>(PlanogramFixtureIdProperty),
                X = this.ReadProperty<Single>(XProperty),
                Y = this.ReadProperty<Single>(YProperty),
                Z = this.ReadProperty<Single>(ZProperty),
                Slope = this.ReadProperty<Single>(SlopeProperty),
                Angle = this.ReadProperty<Single>(AngleProperty),
                Roll = this.ReadProperty<Single>(RollProperty),
                BaySequenceNumber = this.ReadProperty<Int16>(BaySequenceNumberProperty),
                ExtendedData = this.ReadProperty<Object>(ExtendedDataProperty),
                MetaComponentCount = this.ReadProperty<Int32?>(MetaComponentCountProperty),
                MetaTotalMerchandisableLinearSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty),
                MetaTotalMerchandisableAreaSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty),
                MetaTotalMerchandisableVolumetricSpace = this.ReadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty),
                MetaTotalLinearWhiteSpace = this.ReadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty),
                MetaTotalAreaWhiteSpace = this.ReadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty),
                MetaTotalVolumetricWhiteSpace = this.ReadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty),
                MetaProductsPlaced = this.ReadProperty<Int32?>(MetaProductsPlacedProperty),
                MetaNewProducts = this.ReadProperty<Int32?>(MetaNewProductsProperty),
                MetaChangesFromPreviousCount = this.ReadProperty<Int32?>(MetaChangesFromPreviousCountProperty),
                MetaChangeFromPreviousStarRating = this.ReadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty),
                MetaBlocksDropped = this.ReadProperty<Int32?>(MetaBlocksDroppedProperty),
                MetaNotAchievedInventory = this.ReadProperty<Int32?>(MetaNotAchievedInventoryProperty),
                MetaTotalFacings = this.ReadProperty<Int32?>(MetaTotalFacingsProperty),
                MetaAverageFacings = this.ReadProperty<Int16?>(MetaAverageFacingsProperty),
                MetaTotalUnits = this.ReadProperty<Int32?>(MetaTotalUnitsProperty),
                MetaAverageUnits = this.ReadProperty<Int32?>(MetaAverageUnitsProperty),
                MetaMinDos = this.ReadProperty<Single?>(MetaMinDosProperty),
                MetaMaxDos = this.ReadProperty<Single?>(MetaMaxDosProperty),
                MetaAverageDos = this.ReadProperty<Single?>(MetaAverageDosProperty),
                MetaMinCases = this.ReadProperty<Single?>(MetaMinCasesProperty),
                MetaAverageCases = this.ReadProperty<Single?>(MetaAverageCasesProperty),
                MetaMaxCases = this.ReadProperty<Single?>(MetaMaxCasesProperty),
                MetaSpaceToUnitsIndex = this.ReadProperty<Int16?>(MetaSpaceToUnitsIndexProperty),
                MetaTotalComponentCollisions = this.ReadProperty<Int32?>(MetaTotalComponentCollisionsProperty),
                MetaTotalComponentsOverMerchandisedDepth = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty),
                MetaTotalComponentsOverMerchandisedHeight = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty),
                MetaTotalComponentsOverMerchandisedWidth = this.ReadProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty),
                MetaTotalPositionCollisions = this.ReadProperty<Int32?>(MetaTotalPositionCollisionsProperty),
                MetaTotalFrontFacings = this.ReadProperty<Int16?>(MetaTotalFrontFacingsProperty),
                MetaAverageFrontFacings = this.ReadProperty<Single?>(MetaAverageFrontFacingsProperty)  
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramFixtureItemDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Planogram parent)
        {
            Object oldId = null;
            batchContext.Insert<PlanogramFixtureItemDto>(
            (dc) =>
            {
                this.ResolveIds(dc);
                PlanogramFixtureItemDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Object>(IdProperty, dto.Id);
                dc.RegisterId<PlanogramFixtureItem>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Planogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<PlanogramFixtureItemDto>(
                (dc) =>
                {
                    this.ResolveIds(dc);
                    return this.GetDataTransferObject(parent);
                });
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Planogram parent)
        {
            batchContext.Delete<PlanogramFixtureItemDto>(this.GetDataTransferObject(parent));
        }
        #endregion

        #endregion
    }
}