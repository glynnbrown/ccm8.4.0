﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rendering;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines the expected memebers of a class containing 
    /// planogram render settings.
    /// </summary>
    public interface IPlanRenderSettings : INotifyPropertyChanged
    {
        /// <summary>
        /// If true, labels and other performance 
        /// intensive operations will be rendered asyncronously.
        /// </summary>
        Boolean CanRenderAsync { get; }

        /// <summary>
        /// Returns true if mesh normals should be generated.
        /// </summary>
        Boolean GenerateNormals { get; }

        /// <summary>
        /// Returns true if mesh texture coordinates should be generated.
        /// </summary>
        Boolean GenerateTextureCoordinates { get; }

        /// <summary>
        /// Gets/Sets whether performance intensive updates should be paused.
        /// </summary>
        Boolean SuppressPerformanceIntensiveUpdates { get; set; }

        /// <summary>
        /// Gets/Sets the thickness of the selection lines
        /// </summary>
        Double SelectionLineThickness { get; }

        /// <summary>
        /// Gets/Sets the thickness of the selection colour.
        /// </summary>
        ModelColour SelectionColour { get; }

        /// <summary>
        /// Gets/Sets the fill pattern type on the selection border lines.
        /// </summary>
        ModelMaterial3D.FillPatternType SelectionFillPattern { get; }

        /// <summary>
        /// Toggles the visibility of shelf risers
        /// </summary>
        Boolean ShowShelfRisers { get; set; }

        /// <summary>
        /// Toggles the visibility of chest walls
        /// </summary>
        Boolean ShowChestWalls { get; set; }

        /// <summary>
        /// Toggles the rendering of fixture images
        /// </summary>
        Boolean ShowFixtureImages { get; set; }

        /// <summary>
        /// Toggles the rendering of all fixtures and components.
        /// </summary>
        Boolean ShowFixtures { get; set; }

        /// <summary>
        /// Toggles the rendering of fixture fill patterns
        /// </summary>
        Boolean ShowFixtureFillPatterns { get; set; }

        /// <summary>
        /// If true only a wireframe will be drawn.
        /// Nb - this does not improve performance.
        /// </summary>
        Boolean ShowWireframeOnly { get; set; }

        /// <summary>
        /// Toggles the visibility of notches
        /// </summary>
        Boolean ShowNotches { get; set; }

        /// <summary>
        /// Toggles the visibility of pegholes
        /// </summary>
        Boolean ShowPegHoles { get; set; }

        /// <summary>
        /// Toggles the visibility of divider lines
        /// </summary>
        Boolean ShowDividerLines { get; set; }

        /// <summary>
        /// Toggles the visibility of positions
        /// </summary>
        Boolean ShowPositions { get; set; }

        /// <summary>
        /// Toggles the rendering of position units.
        /// If off, solid blocks will be drawn.
        /// </summary>
        Boolean ShowPositionUnits { get; set; }

        /// <summary>
        /// Toggles the rotation of top down components
        /// </summary>
        Boolean RotateTopDownComponents { get; set; }

        /// <summary>
        /// Toggles the render of product images.
        /// </summary>
        Boolean ShowProductImages { get; set; }

        /// <summary>
        /// Toggles the render of product shapes
        /// </summary>
        Boolean ShowProductShapes { get; set; }

        /// <summary>
        /// Toggles the render of product fill colours
        /// </summary>
        Boolean ShowProductFillColours { get; set; }

        /// <summary>
        /// Toggles the render of product fill patterns
        /// </summary>
        Boolean ShowProductFillPatterns { get; set; }

        /// <summary>
        /// Toggles the visibility of pegs
        /// </summary>
        Boolean ShowPegs { get; set; }

        /// <summary>
        /// Toggles the visibility of dividers
        /// </summary>
        Boolean ShowDividers { get; set; }

        /// <summary>
        /// Get/Sets the product label to be drawn
        /// </summary>
        IPlanogramLabel ProductLabel { get; set; }

        /// <summary>
        /// Get/Sets the fixture label to be drawn
        /// </summary>
        IPlanogramLabel FixtureLabel { get; set; }

        /// <summary>
        /// Gets/Sets a flag which indicates whether product labels 
        /// on top down components should be drawn to the front when the component
        /// is not rotated.
        /// </summary>
        Boolean ShowTopDownProductLabelsAsFront { get; }

        /// <summary>
        /// Gets the position highlights to be applied.
        /// </summary>
        PlanogramPositionHighlightColours PositionHighlights { get; }

        /// <summary>
        /// Gets the text of labels to be applied against positions.
        /// </summary>
        PlanogramPositionLabelTexts PositionLabelText { get; }

        /// <summary>
        /// Gets the text of labels to be applied against components.
        /// </summary>
        PlanogramFixtureLabelTexts FixtureLabelText { get; }

        /// <summary>
        /// Toggles the visibility of annotations
        /// </summary>
        Boolean ShowAnnotations { get; set; }

        /// <summary>
        /// Returns the type of camera used to display this model.
        /// Used for performance optimizations.
        /// </summary>
        CameraViewType CameraType { get; }
    }
}
