﻿#region header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM 8.3.2)
// CCM-18938 : M.Pettit
//  Added PegX2,PegY2, PegX3, PegY3 properties
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members required to render a plan product.
    /// </summary>
    /// <remarks>Consider merging with position as we don't actually render a product itself.</remarks>
    public interface IPlanProductRenderable : INotifyPropertyChanged
    {
        Boolean IsUpdating { get; }

        /// <summary>
        /// The total height of a single tray
        /// </summary>
        Single TrayHeight { get; }
        /// <summary>
        /// The total width of a single tray
        /// </summary>
        Single TrayWidth { get; }
        /// <summary>
        /// The total depth of a single tray
        /// </summary>
        Single TrayDepth { get; }

        /// <summary>
        /// The tray thick height value
        /// </summary>
        Single TrayThickHeight { get; }
        /// <summary>
        /// The tray thick width value
        /// </summary>
        Single TrayThickWidth { get; }
        /// <summary>
        /// The tray thick depth value
        /// </summary>
        Single TrayThickDepth { get; }

        /// <summary>
        /// The number of products high per tray
        /// </summary>
        Byte TrayHigh { get; }
        /// <summary>
        /// The number of products wide per tray
        /// </summary>
        Byte TrayWide { get; }
        /// <summary>
        /// The number of products deep per tray.
        /// </summary>
        Byte TrayDeep { get; }

        /// <summary>
        /// The number of peg holes on a single product
        /// </summary>
        Byte NumberOfPegHoles { get; }
        /// <summary>
        /// The first peg hole x position
        /// </summary>
        Single PegX { get; }
        /// <summary>
        /// The first peg hole y position.
        /// </summary>
        Single PegY { get; }
        /// <summary>
        /// The first peg hole x position
        /// </summary>
        Single PegX2 { get; }
        /// <summary>
        /// The first peg hole y position.
        /// </summary>
        Single PegY2 { get; }
        /// <summary>
        /// The first peg hole x position
        /// </summary>
        Single PegX3 { get; }
        /// <summary>
        /// The first peg hole y position.
        /// </summary>
        Single PegY3 { get; }
        /// <summary>
        /// The depth of the peg.
        /// </summary>
        Single PegDepth { get; }

        /// <summary>
        /// The nesting height of the product
        /// </summary>
        Single NestingHeight { get; }
        /// <summary>
        /// The nesting width of the product
        /// </summary>
        Single NestingWidth { get; }
        /// <summary>
        /// The nesting depth of the product
        /// </summary>
        Single NestingDepth { get; }

        /// <summary>
        /// The shape type of the product
        /// </summary>
        PlanogramProductShapeType ShapeType { get; }

        /// <summary>
        /// The fill pattern type of the product
        /// </summary>
        PlanogramProductFillPatternType FillPatternType { get; }

        /// <summary>
        /// The fill colour of the product
        /// </summary>
        Int32 FillColour { get; }

        /// <summary>
        /// The default height to use when drawing the product peg
        /// </summary>
        Single PegDefaultHeight { get; }
        /// <summary>
        /// The default width to use when drawing the product peg
        /// </summary>
        Single PegDefaultWidth { get; }
    }
}
