﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Represents ModelConstruct3DData used to create a PlanogramFixture.
    /// </summary>
    public sealed class PlanFixture3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        private readonly Dictionary<IPlanAssemblyRenderable, PlanAssembly3DData> _assemblyModelsDict = new Dictionary<IPlanAssemblyRenderable, PlanAssembly3DData>();
        private readonly Dictionary<IPlanComponentRenderable, PlanComponent3DData> _componentModelsDict = new Dictionary<IPlanComponentRenderable, PlanComponent3DData>();
        private readonly Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData> _annoModelsDict = new Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData>();

        private IPlanRenderSettings _settingsController;
        private IPlanFixtureRenderable _fixture;

        private Boolean _isPlacementChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source fixture
        /// </summary>
        public IPlanFixtureRenderable Fixture
        {
            get { return _fixture; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fixtureView">the source fixture</param>
        /// <param name="settingsController">the plan render settings to use</param>
        public PlanFixture3DData(IPlanFixtureRenderable fixtureView, IPlanRenderSettings settingsController)
        {
            _fixture = fixtureView;
            _settingsController = settingsController;
            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fixtureView">the source fixture</param>
        /// <param name="settingsController">the plan render settings to use</param>
        internal PlanFixture3DData(ModelConstruct3DData parentData, IPlanFixtureRenderable fixtureView, IPlanRenderSettings settingsController)
        {
            _fixture = fixtureView;
            _settingsController = settingsController;
            this.ParentModelData = parentData;

            Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to property changes on the main component view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fixture_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    {
                        OnPlacementChanged();
                    }
                    break;

                case "Height":
                case "Width":
                case "Depth":
                    {
                        this.Size = new WidthHeightDepthValue
                        {
                            Width = this.Fixture.Width,
                            Height = this.Fixture.Height,
                            Depth = this.Fixture.Depth
                        };
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the assemblies collection changes on the fixture data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fixture_AssembliesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanAssemblyRenderable assembly in e.NewItems)
                        {
                            AddAssemblyModel(assembly);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanAssemblyRenderable assembly in e.OldItems)
                        {
                            RemoveAssemblyModel(assembly);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanAssemblyRenderable> assemblyViews = this.Fixture.Assemblies.ToList();

                        //remove as required
                        foreach (IPlanAssemblyRenderable modelledAssembly in _assemblyModelsDict.Keys.ToList())
                        {
                            if (!assemblyViews.Contains(modelledAssembly))
                            {
                                RemoveAssemblyModel(modelledAssembly);
                            }
                            else
                            {
                                assemblyViews.Remove(modelledAssembly);
                            }
                        }

                        //add remaining
                        foreach (IPlanAssemblyRenderable assemblyView in assemblyViews)
                        {
                            AddAssemblyModel(assemblyView);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the components collection changes on the fixture data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fixture_ComponentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanComponentRenderable component in e.NewItems)
                        {
                            AddComponentModel(component);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanComponentRenderable component in e.OldItems)
                        {
                            RemoveComponentModel(component);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        // create the views order by x so that rendering looks neater
                        List<IPlanComponentRenderable> componentViews =
                            this.Fixture.Components.OrderBy(c => c.X).ToList();

                        //remove as required
                        foreach (IPlanComponentRenderable modelledComponent in _componentModelsDict.Keys.ToList())
                        {
                            if (!componentViews.Contains(modelledComponent))
                            {
                                RemoveComponentModel(modelledComponent);
                            }
                            else
                            {
                                componentViews.Remove(modelledComponent);
                            }
                        }

                        //add remaining
                        foreach (IPlanComponentRenderable componentView in componentViews)
                        {
                            AddComponentModel(componentView);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the annotations collection changes on the source IPlanogramFixture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fixture_AnnotationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanAnnotationRenderable view in e.NewItems)
                        {
                            AddAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanAnnotationRenderable view in e.OldItems)
                        {
                            RemoveAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanAnnotationRenderable> views = this.Fixture.Annotations.ToList();

                        //remove as required
                        foreach (IPlanAnnotationRenderable modelledView in _annoModelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveAnnotationModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanAnnotationRenderable v in views)
                        {
                            AddAnnotationModel(v);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initialization actions.
        /// </summary>
        private void Initialize()
        {
            _fixture.PropertyChanged += Fixture_PropertyChanged;
            _fixture.AssembliesCollectionChanged += Fixture_AssembliesCollectionChanged;
            _fixture.ComponentsCollectionChanged += Fixture_ComponentsCollectionChanged;
            _fixture.AnnotationsCollectionChanged += Fixture_AnnotationsCollectionChanged;


            this.Size = new WidthHeightDepthValue
            {
                Width = _fixture.Width,
                Height = _fixture.Height,
                Depth = _fixture.Depth
            };

            OnPlacementChanged();

            //add child assemblies
            foreach (var assembly in _fixture.Assemblies)
            {
                AddAssemblyModel(assembly);
            }

            //add child components
            foreach (var component in _fixture.Components)
            {
                AddComponentModel(component);
            }

            //Add child annotations
            foreach (var anno in _fixture.Annotations)
            {
                AddAnnotationModel(anno);
            }
        }

        /// <summary>
        /// Updates the placement of this model.
        /// </summary>
        private void UpdateModelPlacement()
        {
            IPlanFixtureRenderable fixtureView = this.Fixture;

            this.Position = new PointValue
            {
                X = fixtureView.X,
                Y = fixtureView.Y,
                Z = fixtureView.Z
            };

            this.Rotation = new RotationValue
            {
                Angle = fixtureView.Angle,
                Slope = fixtureView.Slope,
                Roll = fixtureView.Roll
            };

        }

        /// <summary>
        /// Adds a new child for the given assembly view
        /// </summary>
        /// <param name="assemblyView"></param>
        private void AddAssemblyModel(IPlanAssemblyRenderable assemblyView)
        {
            PlanAssembly3DData model = new PlanAssembly3DData(this, assemblyView, _settingsController);
            AddChild(model);
            _assemblyModelsDict.Add(assemblyView, model);
        }

        /// <summary>
        /// Removes the child model for the given assembly view.
        /// </summary>
        /// <param name="assemblyView"></param>
        private void RemoveAssemblyModel(IPlanAssemblyRenderable assemblyView)
        {
            PlanAssembly3DData model;

            if (_assemblyModelsDict.TryGetValue(assemblyView, out model))
            {
                RemoveChild(model);
                _assemblyModelsDict.Remove(assemblyView);
            }
        }

        /// <summary>
        /// Adds a new child for the given component view.
        /// </summary>
        /// <param name="componentView"></param>
        private void AddComponentModel(IPlanComponentRenderable componentView)
        {
            PlanComponent3DData model = new PlanComponent3DData(this, componentView, _settingsController);
            AddChild(model);
            _componentModelsDict.Add(componentView, model);
        }

        /// <summary>
        /// Removes the model for the given component.
        /// </summary>
        /// <param name="componentView"></param>
        private void RemoveComponentModel(IPlanComponentRenderable componentView)
        {
            PlanComponent3DData model;

            if (_componentModelsDict.TryGetValue(componentView, out model))
            {
                RemoveChild(model);
                _componentModelsDict.Remove(componentView);
            }
        }

        /// <summary>
        /// Adds a new child for the given annotation view.
        /// </summary>
        /// <param name="view"></param>
        private void AddAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model = new PlanAnnotation3DData(view, _settingsController);
            AddChild(model);
            _annoModelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the model for the given view.
        /// </summary>
        /// <param name="view"></param>
        private void RemoveAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model;

            if (_annoModelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _annoModelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Enumerates through all child assembly data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanAssembly3DData> EnumerateAssemblyModels()
        {
            foreach (var entry in _assemblyModelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Enumerates through child annotation data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanAnnotation3DData> EnumerateAnnotationModels()
        {
            foreach (var entry in _annoModelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Enumerates through child component data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanComponent3DData> EnumerateComponentModels()
        {
            foreach (var entry in _componentModelsDict)
            {
                yield return entry.Value;
            }
        }

        #endregion

        #region IPlanPart3DData Members

        Object IPlanPart3DData.PlanPart
        {
            get { return _fixture; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;

            //if we arent flagged as updating then its ok
            // to trigger a refresh.
            if (!_isUpdating) EndUpdate();


            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.UnpauseUpdates();
            }
        }

        #endregion

        #region IUpdateable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }
        }

        private void OnPlacementChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void OnFreeze()
        {
            base.OnFreeze();

            _fixture.PropertyChanged -= Fixture_PropertyChanged;
            _fixture.AssembliesCollectionChanged -= Fixture_AssembliesCollectionChanged;
            _fixture.AnnotationsCollectionChanged -= Fixture_AnnotationsCollectionChanged;
            _fixture.ComponentsCollectionChanged -= Fixture_ComponentsCollectionChanged;
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                _fixture.PropertyChanged -= Fixture_PropertyChanged;
                _fixture.AssembliesCollectionChanged -= Fixture_AssembliesCollectionChanged;
                _fixture.AnnotationsCollectionChanged -= Fixture_AnnotationsCollectionChanged;
                _fixture.ComponentsCollectionChanged -= Fixture_ComponentsCollectionChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
