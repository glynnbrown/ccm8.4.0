﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members required to render a planogram subcomponent.
    /// </summary>
    public interface IPlanSubComponentRenderable : INotifyPropertyChanged
    {
        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }


        Boolean IsUpdating { get; }

        /// <summary>
        /// Returns true if the subcomponent is merchandisable
        /// </summary>
        Boolean IsMerchandisable { get; }

        /// <summary>
        /// Gets the shape that this subcomponent should be rendered as.
        /// </summary>
        PlanogramSubComponentShapeType ShapeType { get; }

        /// <summary>
        /// Flag denoting it this is part of a chest (helper)
        /// </summary>
        Boolean IsChest { get; }

        //******************************
        // Face Fill Fields
        //*******************************

        /// <summary>
        /// Fill colour for the front facing
        /// </summary>
        Int32 FillColourFront { get; }

        /// <summary>
        /// Fill colour for the back facing
        /// </summary>
        Int32 FillColourBack { get; }

        /// <summary>
        /// Fill colour for the top facing
        /// </summary>
        Int32 FillColourTop { get; }

        /// <summary>
        /// Fill colour for the bottom facing
        /// </summary>
        Int32 FillColourBottom { get; }

        /// <summary>
        /// Fill colour for the left facing
        /// </summary>
        Int32 FillColourLeft { get; }

        /// <summary>
        /// Fill colour for the right facing
        /// </summary>
        Int32 FillColourRight { get; }

        /// <summary>
        /// The fill pattern type to use on the front facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeFront { get; }

        /// <summary>
        /// The fill pattern type to use on the back facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeBack { get; }

        /// <summary>
        /// The fill pattern type to use on the top facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeTop { get; }

        /// <summary>
        /// The fill pattern type to use on the bottom facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeBottom { get; }

        /// <summary>
        /// The fill pattern type to use on the letf facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeLeft { get; }

        /// <summary>
        /// The fill pattern type to use on the right facing
        /// </summary>
        PlanogramSubComponentFillPatternType FillPatternTypeRight { get; }

        /// <summary>
        /// The colour of lines on the subcomponent
        /// </summary>
        Int32 LineColour { get; }

        /// <summary>
        /// The thickness of lines on the subcomponent.
        /// </summary>
        Single LineThickness { get; }

        /// <summary>
        /// The transparency percentage of the front facing
        /// </summary>
        Int32 TransparencyPercentFront { get; }

        /// <summary>
        /// The transparency percentage of the back facing
        /// </summary>
        Int32 TransparencyPercentBack { get; }

        /// <summary>
        /// The transparency percentage of the top facing
        /// </summary>
        Int32 TransparencyPercentTop { get; }

        /// <summary>
        /// The transparency percentage of the bottom facing
        /// </summary>
        Int32 TransparencyPercentBottom { get; }

        /// <summary>
        /// The transparency percentage of the left facing
        /// </summary>
        Int32 TransparencyPercentLeft { get; }

        /// <summary>
        /// The transparency percentage of the right facing
        /// </summary>
        Int32 TransparencyPercentRight { get; }

        Byte[] FrontImageData { get; }
        Byte[] BackImageData { get; }
        Byte[] TopImageData { get; }
        Byte[] BottomImageData { get; }
        Byte[] LeftImageData { get; }
        Byte[] RightImageData { get; }

        //******************************
        // Face Thickness Fields
        //*******************************

        /// <summary>
        /// The thickness of the front face
        /// </summary>
        Single FaceThicknessFront { get; }

        /// <summary>
        /// The thickness of the back face
        /// </summary>
        Single FaceThicknessBack { get; }

        /// <summary>
        /// The thickness of the top face
        /// </summary>
        Single FaceThicknessTop { get; }

        /// <summary>
        /// The thickness of the bottom face
        /// </summary>
        Single FaceThicknessBottom { get; }

        /// <summary>
        /// The thickness of the left face
        /// </summary>
        Single FaceThicknessLeft { get; }

        /// <summary>
        /// The thickness of the right face.
        /// </summary>
        Single FaceThicknessRight { get; }

        //******************************
        // Riser Fields
        //*******************************

        /// <summary>
        /// The height of risers
        /// </summary>
        Single RiserHeight { get; }

        /// <summary>
        /// The thickness of risers.
        /// </summary>
        Single RiserThickness { get; }

        /// <summary>
        /// The colour of the riser
        /// </summary>
        Int32 RiserColour { get; }

        /// <summary>
        /// The transparency percentage of the riser colour.
        /// </summary>
        Int32 RiserTransparencyPercent { get; }

        /// <summary>
        /// Returns true if the front riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnFront { get; }

        /// <summary>
        /// Returns true if the back riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnBack { get; }

        /// <summary>
        /// Returns true if the left riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnLeft { get; }

        /// <summary>
        /// Returns true if the right riser should be drawn
        /// </summary>
        Boolean IsRiserPlacedOnRight { get; }

        ///// <summary>
        ///// The fill pattern to be used on the riser.  - not in use
        ///// </summary>
        //SubComponentFillPatternType RiserFillPatternType { get; }


        //******************************
        // Notch Fields
        //*******************************

        /// <summary>
        /// The x start position of backboard notches
        /// </summary>
        Single NotchStartX { get; }

        /// <summary>
        /// The x spacing of backboard notches
        /// </summary>
        Single NotchSpacingX { get; }

        /// <summary>
        /// The y start position of backboard notches
        /// </summary>
        Single NotchStartY { get; }

        /// <summary>
        /// The y spacing of backboard notches
        /// </summary>
        Single NotchSpacingY { get; }

        /// <summary>
        /// The height of the backboard notches
        /// </summary>
        Single NotchHeight { get; }

        /// <summary>
        /// The width of backboard notches
        /// </summary>
        Single NotchWidth { get; }

        /// <summary>
        /// Returns true if notches should be placed on the front facing
        /// </summary>
        Boolean IsNotchPlacedOnFront { get; }

        /// <summary>
        /// The style type of the notches
        /// </summary>
        PlanogramSubComponentNotchStyleType NotchStyleType { get; }

        ///// <summary>
        ///// Returns true if notches should be placed on the back facing - not in use
        ///// </summary>
        //Boolean IsNotchPlacedOnBack { get; }

        ///// <summary>
        ///// Return true if notches should be placed on the left facing. - not in use
        ///// </summary>
        //Boolean IsNotchPlacedOnLeft { get; }

        ///// <summary>
        ///// Returns true if notches should be placed on the right facing. - not in use
        ///// </summary>
        //Boolean IsNotchPlacedOnRight { get; }


        //******************************
        // Divider lines
        //*******************************

        /// <summary>
        /// The fill colour to use when rendering dividers
        /// </summary>
        Int32 DividerObstructionFillColour { get; }

        /// <summary>
        /// The fill pattern to use when rendering dividers
        /// </summary>
        PlanogramSubComponentFillPatternType DividerObstructionFillPattern { get; }


        /// <summary>
        /// Returns the the divider line rects for this subcomponent.
        /// </summary>
        List<RectValue> GetDividerLines();


        //******************************
        // Peg Hole Fields
        //*******************************
        /// <summary>
        /// The height of holes on row 1
        /// </summary>
        Single MerchConstraintRow1Height { get; }

        /// <summary>
        /// The width of holes on row 1
        /// </summary>
        Single MerchConstraintRow1Width { get; }

        /// <summary>
        /// The height of holes on row 2
        /// </summary>
        Single MerchConstraintRow2Height { get; }

        /// <summary>
        /// The width of holes on row 2
        /// </summary>
        Single MerchConstraintRow2Width { get; }


        /// <summary>
        /// Returns the pegholes for this subcomponent
        /// </summary>
        PlanogramPegHoles GetPegHoles();


        //******************************
        // Collection Changed events
        //*******************************

        event NotifyCollectionChangedEventHandler PositionsCollectionChanged;
        IEnumerable<IPlanPositionRenderable> Positions { get; }

        event NotifyCollectionChangedEventHandler AnnotationsCollectionChanged;
        IEnumerable<IPlanAnnotationRenderable> Annotations { get; }

        event NotifyCollectionChangedEventHandler DividersCollectionChanged;
        IEnumerable<ISubComponentDividerRenderable> Dividers { get; }

    }
}
