﻿// Copyright © Galleria RTS Ltd 2016

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines memebers required to render a planogram component.
    /// </summary>
    public interface IPlanComponentRenderable : INotifyPropertyChanged
    {
        /// <summary>
        /// The unique id for the component placement.
        /// </summary>
        Object Id { get; }

        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }

        /// <summary>
        /// Returns true if this component should be 
        /// flipped if the view setting RotateTopDownComponents is true.
        /// </summary>
        Boolean IsMerchandisedTopDown { get; }

        event NotifyCollectionChangedEventHandler SubComponentsCollectionChanged;
        IEnumerable<IPlanSubComponentRenderable> SubComponents { get; }

        event NotifyCollectionChangedEventHandler AnnotationsCollectionChanged;
        IEnumerable<IPlanAnnotationRenderable> Annotations { get; }

        Boolean IsUpdating { get; }
    }
}
