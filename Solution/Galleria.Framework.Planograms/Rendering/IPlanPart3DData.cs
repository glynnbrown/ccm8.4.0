﻿// Copyright © Galleria RTS Ltd 2016
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    public interface IPlanPart3DData
    {
        Object PlanPart { get; }
        void PauseUpdates();
        void UnpauseUpdates();
    }
}
