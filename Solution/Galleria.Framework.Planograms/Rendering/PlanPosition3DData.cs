﻿#region header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.2)
// CCM-18938 : M.Pettit
//  Added PegX2,PegY2, PegX3, PegY3 properties
#endregion
#endregion

using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Model data implementation for a PlanPosition.
    /// </summary>
    public sealed class PlanPosition3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Nested Classes

        /// <summary>
        /// Denotes the available 3d part types.
        /// </summary>
        public enum PlanPosition3DPartType
        {
            MainBlock,
            XBlock,
            YBlock,
            ZBlock,
            Tray
        }

        #endregion

        #region Fields

        const Int32 _hollowStart = 1;

        private IPlanRenderSettings _settingsController;

        private IPlanPositionRenderable _planPosition = null;
        private IPlanProductRenderable _planProduct = null;
        private static readonly ModelColour _trayColour = new ModelColour() { Alpha = 255, Red = 100, Blue = 100, Green = 100 };
        private Boolean _isSelected;

        private ModelLabel3DData _label;

        private List<ModelConstructPart3DData> _trayData = new List<ModelConstructPart3DData>();
        private Boolean _isVisibleOverride = true;

        private List<ModelConstructPart3DData> _pegParts = new List<ModelConstructPart3DData>();

        private Boolean _isPlacementChanged; //flag to indicate that the placement needs updating.
        private Boolean _isVisibilityChanged; //flag to indicate that the visibility needs updating.
        private Boolean _isPartChanged; //flag to indicate that the parts need updating
        private Boolean _isMaterialChanged; // flag to indicate the material needs updating.
        private Boolean _isPegsChanged; //flag to indicate that the pegs need updating.
        private Boolean _isLabelChanged; //flag to indicate that the label need updating.

        private ModelColour? _lastHighlightColour;

        private BoxModelPart3DData _selectionOutlinePart;

        private Boolean _isDrawingUnitlessTrays;
        private Boolean _isDrawingUnitlessTraysX;
        private Boolean _isDrawingUnitlessTraysY;
        private Boolean _isDrawingUnitlessTraysZ;

        private Dictionary<PlanogramPositionBlockType, BoxFaceType> _visibleFaces;
        #endregion

        #region Properties

        /// <summary>
        /// The source position record
        /// </summary>
        public IPlanPositionRenderable PlanPosition
        {
            get { return _planPosition; }
        }

        /// <summary>
        /// Planogram Product Record
        /// </summary>
        public IPlanProductRenderable PlanProduct
        {
            get { return _planProduct; }
        }

        /// <summary>
        /// Gets/Sets the selection state of this model.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");

                    UpdateSelectionOutline();
                    //UpdateMaterial();
                }
            }
        }

        /// <summary>
        /// Returns the setting to use for product labels.
        /// If null, no label will be created.
        /// </summary>
        public IPlanogramLabel ProductLabel
        {
            get { return _settingsController.ProductLabel; }
        }

        /// <summary>
        /// Returns true if units should be displayed.
        /// </summary>
        public Boolean ShowUnits
        {
            get { return _settingsController.ShowPositionUnits; }
        }

        /// <summary>
        /// Returns true if product images should be drawn.
        /// </summary>
        public Boolean ShowProductImages
        {
            get
            {
                //must also have show units on.
                return this.ShowUnits
                    && _settingsController.ShowProductImages
                    && !(this.ShowProductShapes && this.PlanProduct.ShapeType != PlanogramProductShapeType.Box)
                    && !this.SupressPerformanceIntensiveUpdates
                    && /*No highlights applied*/_settingsController.PositionHighlights == null;
            }
        }

        /// <summary>
        /// Returns true if product shapes should be drawn.
        /// </summary>
        public Boolean ShowProductShapes
        {
            get { return _settingsController.ShowProductShapes; }
        }

        /// <summary>
        /// Returns true if fill colours should be drawn.
        /// Only used when highlights are not applied.
        /// </summary>
        public Boolean ShowProductFillColours
        {
            get { return _settingsController.ShowProductFillColours; }
        }

        /// <summary>
        /// Returns true if fillpatterns should be drawn.
        /// </summary>
        public Boolean ShowProductFillPatterns
        {
            get
            {
                return _settingsController.ShowProductFillPatterns
                    && !this.SupressPerformanceIntensiveUpdates;
            }
        }

        /// <summary>
        /// Returns true if pegs are to be drawn.
        /// </summary>
        public Boolean ShowPegs
        {
            get
            {
                if (_settingsController.ShowPegs)
                {
                    if (this.PlanPosition.IsOnPegs)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public Boolean IsVisibleOverride
        {
            get { return _isVisibleOverride; }
            set
            {
                _isVisibleOverride = value;
                OnVisibilityChanged();
            }
        }

        /// <summary>
        /// Returns true if performance intensive updates should be supressed.
        /// </summary>
        private Boolean SupressPerformanceIntensiveUpdates
        {
            get { return _settingsController.SuppressPerformanceIntensiveUpdates; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="position">the source position</param>
        /// <param name="settingsController">the settings controller to use</param>
        public PlanPosition3DData(IPlanPositionRenderable position, IPlanRenderSettings settingsController)
        {
            _planPosition = position;
            _planProduct = position.Product;
            _settingsController = settingsController;

            _visibleFaces = new Dictionary<PlanogramPositionBlockType, BoxFaceType>(4)
            {
                {PlanogramPositionBlockType.Main,BoxFaceType.All },
                {PlanogramPositionBlockType.X,BoxFaceType.All },
                {PlanogramPositionBlockType.Y,BoxFaceType.All },
                {PlanogramPositionBlockType.Z,BoxFaceType.All },
            };

            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="position">the source position</param>
        /// <param name="settingsController">the settings controller to use</param>
        /// <param name="parentData">the parent model data that we are attaching to.</param>
        internal PlanPosition3DData(ModelConstruct3DData parentData, IPlanPositionRenderable position, IPlanRenderSettings settingsController)
        {
            _planPosition = position;
            _planProduct = position.Product;
            _settingsController = settingsController;

            _visibleFaces = new Dictionary<PlanogramPositionBlockType, BoxFaceType>(4)
            {
                {PlanogramPositionBlockType.Main,BoxFaceType.All },
                {PlanogramPositionBlockType.X,BoxFaceType.All },
                {PlanogramPositionBlockType.Y,BoxFaceType.All },
                {PlanogramPositionBlockType.Z,BoxFaceType.All },
            };

            this.ParentModelData = parentData;

            Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a property value on the plan position changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanPosition_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsUpdating":
                    OnIsUpdatingChanged(this.PlanPosition.IsUpdating);
                    break;

                case "X":
                case "Y":
                case "Slope":
                case "Angle":
                case "Roll":
                    OnPlacementChanged();
                    break;

                case "PegX":
                case "PegY":
                case "PegX2":
                case "PegY2":
                case "PegX3":
                case "PegY3":
                    OnPegsChanged();
                    break;

                case "Z":
                    OnPlacementChanged();
                    OnPegsChanged();
                    break;

                case "FacingsHigh":
                case "FacingsWide":
                case "FacingsDeep":
                case "UnitOrientationType":
                case "IsMerchandisedAsTrays":
                case "UnitHeight":
                case "UnitWidth":
                case "UnitDepth":
                case "BlockMainStartX":
                case "BlockMainStartY":
                case "BlockMainStartZ":
                case "FacingsXHigh":
                case "FacingsXWide":
                case "FacingsXDeep":
                case "UnitXOrientationType":
                case "IsXMerchandisedAsTrays":
                case "UnitXHeight":
                case "UnitXWidth":
                case "UnitXDepth":
                case "IsXPlacedLeft":
                case "BlockXStartX":
                case "BlockXStartY":
                case "BlockXStartZ":
                case "FacingsYHigh":
                case "FacingsYWide":
                case "FacingsYDeep":
                case "UnitYOrientationType":
                case "IsYMerchandisedAsTrays":
                case "UnitYHeight":
                case "UnitYWidth":
                case "UnitYDepth":
                case "IsYPlacedBottom":
                case "BlockYStartX":
                case "BlockYStartY":
                case "BlockYStartZ":
                case "FacingsZHigh":
                case "FacingsZWide":
                case "FacingsZDeep":
                case "UnitZOrientationType":
                case "IsZMerchandisedAsTrays":
                case "UnitZHeight":
                case "UnitZWidth":
                case "UnitZDepth":
                case "IsZPlacedFront":
                case "BlockZStartX":
                case "BlockZStartY":
                case "BlockZStartZ":
                case "IsOnPegs":
                case "IsOnTopDownComponent":
                case "FacingSpaceX":
                case "FacingSpaceY":
                case "FacingSpaceZ":
                case "Height":
                case "Width":
                case "Depth":
                    OnPartChanged();
                    break;

                case "FrontImageData":
                case "BackImageData":
                case "TopImageData":
                case "BottomImageData":
                case "LeftImageData":
                case "RightImageData":
                    if (this.ShowProductImages)
                    {
                        OnImagesChanged();
                    }
                    break;

                case "FrontXImageData":
                case "BackXImageData":
                case "TopXImageData":
                case "BottomXImageData":
                case "LeftXImageData":
                case "RightXImageData":
                    if (this.ShowProductImages
                         && this.PlanPosition.FacingsXHigh > 0
                         && this.PlanPosition.FacingsXWide > 0
                         && this.PlanPosition.FacingsXDeep > 0)
                    {
                        OnImagesChanged();
                    }
                    break;


                case "FrontYImageData":
                case "BackYImageData":
                case "TopYImageData":
                case "BottomYImageData":
                case "LeftYImageData":
                case "RightYImageData":
                    if (this.ShowProductImages
                        && this.PlanPosition.FacingsYHigh > 0
                        && this.PlanPosition.FacingsYWide > 0
                        && this.PlanPosition.FacingsYDeep > 0)
                    {
                        OnImagesChanged();
                    }
                    break;

                case "FrontZImageData":
                case "BackZImageData":
                case "TopZImageData":
                case "BottomZImageData":
                case "LeftZImageData":
                case "RightZImageData":
                    if (this.ShowProductImages
                        && this.PlanPosition.FacingsZHigh > 0
                        && this.PlanPosition.FacingsZWide > 0
                        && this.PlanPosition.FacingsZDeep > 0)
                    {
                        OnImagesChanged();
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when a property value on the product changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanProduct_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsUpdating":
                    OnIsUpdatingChanged(this.PlanProduct.IsUpdating);
                    break;

                case "NestingDepth":
                case "NestingHeight":
                case "NestingWidth":
                    OnPartChanged();
                    break;

                case "ShapeType":
                    if (this.ShowProductShapes) OnPartChanged();
                    break;

                case "TrayHeight":
                case "TrayWidth":
                case "TrayDepth":
                case "TrayThickHeight":
                case "TrayThickWidth":
                case "TrayThickDepth":
                case "TrayHigh":
                case "TrayWide":
                    if (this.PlanPosition.IsMerchandisedAsTrays
                        || this.PlanPosition.IsXMerchandisedAsTrays
                        || this.PlanPosition.IsYMerchandisedAsTrays
                        || this.PlanPosition.IsZMerchandisedAsTrays)
                    {
                        OnPartChanged();
                    }
                    break;

                case "FillPatternType":
                    if (this.ShowProductFillPatterns) OnMaterialChanged();
                    break;

                case "FillColour":
                    if (!GetHighlightColour().HasValue) OnMaterialChanged();
                    break;

                case "NumberOfPegHoles":
                    OnPegsChanged();
                    break;

                case "PegX":
                case "PegY":
                case "PegDepth":
                    if (this.PlanProduct.NumberOfPegHoles > 0) OnPegsChanged();
                    break;
            }
        }

        /// <summary>
        /// Called when a property changes on the parent plangram 3d data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case PlanRenderSettings.ShowPositionsPropertyName:
                    UpdateVisibility();
                    break;

                case PlanRenderSettings.ShowPositionUnitsPropertyName:
                    OnPartChanged();
                    break;

                case PlanRenderSettings.ShowProductImagesPropertyName:
                    if (this.ShowUnits)
                    {
                        OnImagesChanged(onlyWhenShowing: false);
                    }
                    break;

                case PlanRenderSettings.ShowProductFillPatternsPropertyName:
                    OnMaterialChanged();
                    break;

                case PlanRenderSettings.PositionHighlightsPropertyName:
                    //only update if the colour has actually changed.
                    if (_lastHighlightColour != GetHighlightColour())
                    {
                        OnMaterialChanged();

                        //[V8-32598] If the label is show over images then reload that too
                        // as it could be affected
                        if (this.ProductLabel != null && !this.ProductLabel.ShowOverImages)
                        {
                            OnLabelChanged();
                        }
                    }
                    break;

                case PlanRenderSettings.ShowProductShapesPropertyName:
                    if (this.ShowUnits) OnPartChanged();
                    break;

                case PlanRenderSettings.PositionLabelTextPropertyName:
                case PlanRenderSettings.ProductLabelPropertyName:
                    OnLabelChanged();
                    break;


                case PlanRenderSettings.RotateTopDownComponentsPropertyName:
                    if (this.PlanPosition.IsOnTopDownComponent) OnLabelChanged();
                    break;

                case PlanRenderSettings.ShowPegsPropertyName:
                    if (this.PlanPosition.IsOnPegs)
                    {
                        //fire off on visibility changed to deal with showin hiding pegs.
                        OnVisibilityChanged();
                    }
                    break;

                case PlanRenderSettings.SelectionColourPropertyName:
                    if (this.IsSelected && !this.SupressPerformanceIntensiveUpdates) UpdateSelectionOutline();
                    break;

                case PlanRenderSettings.SuppressPerformanceIntensiveUpdatesPropertyName:
                    OnSupressPerformanceIntensiveUpdatesChanged();
                    break;
            }
        }

        /// <summary>
        /// Called whenever the settings controller property value changes
        /// </summary>
        private void OnSupressPerformanceIntensiveUpdatesChanged()
        {
            //update the material if this is a selected item
            // so that images can be hidden while performing the update.
            Boolean isSelected = this.IsSelected;
            if (!IsSelected)
            {
                PlanSubComponent3DData parentSubData = this.ParentModelData as PlanSubComponent3DData;
                if (parentSubData != null) isSelected = parentSubData.IsSelected;
            }

            if (isSelected)
            {
                OnMaterialChanged();
            }
        }

        /// <summary>
        /// Called when the world transform changes.
        /// </summary>
        protected override void OnUpdateWorldTransform()
        {
            base.OnUpdateWorldTransform();

            if (UpdateVisibleFaces()) OnPartChanged();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initialization of this model.
        /// </summary>
        private void Initialize()
        {
            _settingsController.PropertyChanged += SettingsController_PropertyChanged;

            //load models
            LoadPositionModels();

            //hook into property changed
            _planPosition.PropertyChanged += PlanPosition_PropertyChanged;
            _planProduct.PropertyChanged += PlanProduct_PropertyChanged;
        }

        /// <summary>
        /// Updates the model size and position
        /// </summary>
        private void UpdateModelPlacement()
        {
            IPlanPositionRenderable pos = _planPosition;

            this.Size = new WidthHeightDepthValue(pos.Width, pos.Height, pos.Depth);

            this.Position = new PointValue(pos.X, pos.Y, pos.Z);

            //nb -Rotation is processed for individual units.

            //label size will need updating.
            if (!_isLabelChanged && _label != null)
            {
                _label.MaxSize = this.Size;
            }
        }

        /// <summary>
        /// Updates the material used by the parts of this model.
        /// </summary>
        private void UpdateMaterial()
        {
            IPlanPositionRenderable pos = this.PlanPosition;
            IPlanProductRenderable planProd = this.PlanProduct;
            IPlanRenderSettings settings = _settingsController;

            ModelColour? highlightColour = GetHighlightColour();
            _lastHighlightColour = highlightColour;

            //Fill Colour:
            ModelColour fillColour;
            if (highlightColour.HasValue) fillColour = highlightColour.Value;
            else if (this.ShowProductFillColours) fillColour = ModelColour.IntToColor(this.PlanProduct.FillColour);
            else fillColour = ModelColour.White;
            if (fillColour.Alpha == 0) fillColour.Alpha = 255;

            //Fill Pattern:
            ModelMaterial3D.FillPatternType fillPattern =
                (this.ShowProductFillPatterns) ? ToFillPatternType(this.PlanProduct.FillPatternType) : ModelMaterial3D.FillPatternType.Solid;

            //Line Colour:
            ModelColour unitLineColour = ModelColour.Black;
            if (this.ShowProductShapes && this.PlanProduct.ShapeType != PlanogramProductShapeType.Box)
            {
                //if this is an non-box shaped mesh the hide the lines by setting them
                // to match the fill colour. Cannot set thick to 0 otherwise this shows funny.
                unitLineColour = fillColour;
            }

            //Line Thickness:
            Double lineThickness = (settings.SelectionLineThickness > 0) ? settings.SelectionLineThickness : 1;


            foreach (IModelConstructPart3DData partData in GetAllParts())
            {
                PlanPosition3DPartType? partType = partData.Tag as PlanPosition3DPartType?;
                if (partType.HasValue)
                {
                    partData.BeginEdit();

                    //Tray
                    if (partType.Value == PlanPosition3DPartType.Tray)
                    {
                        partData.Material = ModelMaterial3D.CreateMaterial(_trayColour);
                        partData.LineThickness = lineThickness;
                        partData.LineMaterial = ModelMaterial3D.CreateMaterial(unitLineColour);
                    }

                    //Unit block
                    else
                    {
                        Byte[] frontImageData = null, backImageData = null, topImageData = null,
                            bottomImageData = null, leftImageData = null, rightImageData = null;

                        //get images
                        Boolean hasImages = false;
                        if (this.ShowProductImages)
                        {
                            GetVisibleImageData(pos, ToBlockType(partType.Value),
                                out frontImageData, out backImageData,
                                out topImageData, out bottomImageData,
                                out leftImageData, out rightImageData);

                            hasImages =
                                (frontImageData != null) || (backImageData != null) || topImageData != null
                                || bottomImageData != null || leftImageData != null || rightImageData != null;
                        }

                        //set the facing materials
                        Dictionary<BoxFaceType, ModelMaterial3D> productMaterials = new Dictionary<BoxFaceType, ModelMaterial3D>()
                                {
                                    {BoxFaceType.Front, new ModelMaterial3D(fillColour, fillPattern, frontImageData)},
                                    {BoxFaceType.Back, new ModelMaterial3D(fillColour, fillPattern, backImageData)},
                                    {BoxFaceType.Top, new ModelMaterial3D(fillColour, fillPattern, topImageData)},
                                    {BoxFaceType.Bottom, new ModelMaterial3D(fillColour, fillPattern, bottomImageData)},
                                    {BoxFaceType.Left, new ModelMaterial3D(fillColour, fillPattern, leftImageData)},
                                    {BoxFaceType.Right, new ModelMaterial3D(fillColour, fillPattern, rightImageData)}
                                };
                        if (partData is TiledBoxModelPart3DData) ((TiledBoxModelPart3DData)partData).SetMaterials(productMaterials);
                        else if (partData is BoxMeshPart3DData) ((BoxMeshPart3DData)partData).SetMaterials(productMaterials);
                        else if (partData is BoxModelPart3DData) ((BoxModelPart3DData)partData).SetMaterials(productMaterials);
                        else partData.Material = productMaterials[BoxFaceType.Front];

                        //set the line materials
                        partData.LineMaterial = ModelMaterial3D.CreateMaterial(unitLineColour);
                        partData.LineThickness = lineThickness;//(hasImages /*&& !isSelected*/) ? 0 : lineThickness;
                    }


                    partData.EndEdit();
                }
            }
        }

        /// <summary>
        /// Updates the product label for this position.
        /// </summary>
        private void UpdateProductLabel()
        {
            IPlanogramLabel labelSetting = this.ProductLabel;

            String text = null;
            if (labelSetting != null && this.IsVisible)
            {
                //turn off the label if we are showing any images
                // and the label setting is to not show it over the top.
                if (!labelSetting.ShowOverImages
                    && this.ShowProductImages
                    && HasAvailableImages())
                {
                    text = null;
                }
                else
                {
                    text = GetLabelText();
                }
            }


            if (!String.IsNullOrEmpty(text))
            {
                //add the label if required.
                if (_label == null)
                {
                    _label = new ModelLabel3DData();
                    AddLabel(_label);
                }

                ModelLabel3DData labelModel = _label;
                labelModel.BeginEdit();
                {
                    labelModel.IsAsync = _settingsController.CanRenderAsync;

                    WidthHeightDepthValue maxSize = this.Size;
                    if (this.PlanPosition.IsOnPegs)
                    {
                        //[CCM-18775] reduce the max area available to the label so that it does not overlap the peg.
                        IPlanSubComponentRenderable sub = this.PlanPosition.ParentSubComponent;
                        Single barHeight = (sub.MerchConstraintRow1Height > 0) ? sub.MerchConstraintRow1Height : this.PlanProduct.PegDefaultHeight;

                        maxSize = new WidthHeightDepthValue(
                            this.Size.Width,
                            this.Size.Height - this.PlanProduct.PegY - barHeight,
                            this.Size.Depth);

                    }
                    labelModel.MaxSize = maxSize;

                    labelModel.TextColour = ModelColour.IntToColor(labelSetting.TextColour);
                    labelModel.BackgroundColour = ModelColour.IntToColor(labelSetting.BackgroundColour);
                    labelModel.BorderColour = ModelColour.IntToColor(labelSetting.BorderColour);
                    labelModel.BorderThickness = labelSetting.BorderThickness;
                    labelModel.FontSize = labelSetting.FontSize;
                    labelModel.RotateFontToFit = labelSetting.IsRotateToFitOn;
                    labelModel.ShrinkFontToFit = labelSetting.IsShrinkToFitOn;
                    labelModel.FontName = labelSetting.Font;
                    labelModel.Text = text;
                    labelModel.TextBoxFontBold = labelSetting.TextBoxFontBold;
                    labelModel.TextBoxFontItalic = labelSetting.TextBoxFontItalic;
                    labelModel.TextBoxFontUnderlined = labelSetting.TextBoxFontUnderlined;
                    labelModel.LabelHorizontalPlacement = labelSetting.LabelHorizontalPlacement.ToModelHorizontalAlignment();
                    labelModel.LabelVerticalPlacement = labelSetting.LabelVerticalPlacement.ToModelVerticalAlignment();
                    labelModel.Alignment = labelSetting.TextBoxTextAlignment.ToModelTextAlignment();
                    labelModel.TextDirection = labelSetting.TextDirection.ToModelTextDirection();
                    labelModel.TextWrapping = labelSetting.TextWrapping.ToModelTextWrapping();
                    labelModel.BackgroundTransparency = labelSetting.BackgroundTransparency;
                    labelModel.MinFontSize = 0.1;
                    labelModel.AttachFace = ModelLabelAttachFace.Auto;

                    Double productLineThickness = (_settingsController.SelectionLineThickness > 0) ? _settingsController.SelectionLineThickness / 2D : 1;
                    labelModel.Margin = Math.Max(productLineThickness, 0.1);

                    //set the label attach face based on controller settings and the type of component 
                    // the position is sitting in.
                    if (this.PlanPosition.IsOnTopDownComponent)
                    {
                        if (_settingsController.ShowTopDownProductLabelsAsFront)
                        {
                            //if the component is being rotated then show top otherwise show front
                            labelModel.AttachFace =
                                (_settingsController.RotateTopDownComponents) ?
                                labelModel.AttachFace = ModelLabelAttachFace.Top :
                                labelModel.AttachFace = ModelLabelAttachFace.Auto;
                        }
                        else
                        {
                            labelModel.AttachFace = ModelLabelAttachFace.Top;
                        }
                    }
                }
                labelModel.EndEdit();

            }
            else
            {
                //remove the label
                if (_label != null)
                {
                    RemoveLabel(_label);
                    _label = null;
                }
            }
        }

        /// <summary>
        /// Updates the visibility of this model.
        /// </summary>
        private void UpdateVisibility()
        {
            if (!_isVisibleOverride
                || (!_settingsController.ShowPositions && !ShouldDrawPegs()))
            {
                //this position is set to always hide.
                // or show positions is off anyway and we dont care about pegs.
                base.IsVisible = false;
            }
            else if (_settingsController.ShowPositions)
            {
                //we are drawing positions
                base.IsVisible = true;

                //force peg and label redraw to make sure they are correct (see below)
                DrawPegs();
                UpdateProductLabel();
            }
            else if (ShouldDrawPegs())
            {
                //[CCM-18775]if we are showing pegs then we should 
                //still draw them and the labels regardless of
                // whether show positions is on

                //note that we will only hit this if we actually care about pegs on
                // this position.
                base.IsVisible = true;

                //start by hiding all child models and parts but leave labels enabled.
                foreach (var child in this.ModelChildren.ToList()) child.IsVisible = false;
                foreach (var child in this.ModelParts.ToList()) child.IsVisible = false;

                //redraw the pegs so they can make any changes that are
                // required by showing them individually.
                DrawPegs();
            }
        }

        /// <summary>
        /// Returns true if pegs are to be drawn.
        /// </summary>
        private Boolean ShouldDrawPegs()
        {
            return
           _settingsController.ShowPegs //peg display is on
           && this.PlanPosition.IsOnPegs // and the product is on a pegged component
           && this.PlanProduct.PegDepth > 0;  // and it has a peg depth
             //&& this.PlanProduct.NumberOfPegHoles > 0; // and peg holes
        }


        /// <summary>
        /// Draws the peg visual
        /// </summary>
        private void DrawPegs()
        {
            //remove the old parts
            foreach (ModelConstructPart3DData pegPart in _pegParts)
            {
                RemovePart(pegPart);
            }
            _pegParts.Clear();

            //check if we need to draw anything.
            if (!this.ShouldDrawPegs()) return;

            IPlanProductRenderable prod = this.PlanProduct;
            IPlanPositionRenderable pos = this.PlanPosition;
            IPlanSubComponentRenderable sub = this.PlanPosition.ParentSubComponent;


            WidthHeightDepthValue orientedSize = ProductOrientationHelper.GetOrientatedSize(pos.UnitOrientationType, pos.UnitWidth, pos.UnitHeight, pos.UnitDepth);

            ModelMaterial3D barMat = (_settingsController.ShowPositions) ?
                ModelMaterial3D.CreateMaterial(/*gray*/ModelColour.IntToColor(-8355712))
                : ModelMaterial3D.CreateMaterial(/*red*/ModelColour.NewColour(255, 0, 0));

            Single pegZ = (pos.Z > 0) ? -pos.Z + pos.ParentSubComponent.Depth : 0;


            //create a bar per front facing
            MeshPart3DData bar1 = new MeshPart3DData(this);
            bar1.SetMaterial(barMat);

            Single barWidth = (sub.MerchConstraintRow1Width > 0) ? sub.MerchConstraintRow1Width : prod.PegDefaultWidth;
            Single barHeight = (sub.MerchConstraintRow1Height > 0) ? sub.MerchConstraintRow1Height : prod.PegDefaultHeight;
            Single barDepth = prod.PegDepth + 0.01F;

            PointValue barCenter =
                new PointValue()
                {
                    X = pos.PegX,
                    Y = orientedSize.Height - pos.PegY,
                    Z = pegZ + (barDepth / 2F)
                };

            PointValue barSpacing = new PointValue()
            {
                X = orientedSize.Width + pos.FacingSpaceX - bar1.Size.Width,
                Y = orientedSize.Height + pos.FacingSpaceY - bar1.Size.Height
            };

            MeshBuilderInstructions meshInstructions = new MeshBuilderInstructions(_settingsController.GenerateNormals, generateTextures: true);
            meshInstructions.AddBoxFaces(barCenter, barWidth, barHeight, barDepth, BoxFaceType.All);
            meshInstructions.TileMesh(pos.FacingsHigh, pos.FacingsWide, /*deep*/1, /*hollowStart*/0, barSpacing);
            bar1.Builder = meshInstructions;

            _pegParts.Add(bar1);
            AddPart(bar1);

            if (prod.NumberOfPegHoles >= 2)
            {
                MeshPart3DData bar2 = new MeshPart3DData(this);
                bar2.SetMaterial(barMat);

                barCenter =
                    new PointValue()
                    {
                        X = pos.PegX2,
                        Y = orientedSize.Height - pos.PegY2,
                        Z = pegZ + (barDepth / 2F)
                    };

                barSpacing = new PointValue()
                {
                    X = orientedSize.Width + pos.FacingSpaceX - bar2.Size.Width,
                    Y = orientedSize.Height + pos.FacingSpaceY - bar2.Size.Height
                };

                meshInstructions = new MeshBuilderInstructions(_settingsController.GenerateNormals, generateTextures: true);
                meshInstructions.AddBoxFaces(barCenter, barWidth, barHeight, barDepth, BoxFaceType.All);
                meshInstructions.TileMesh(pos.FacingsHigh, pos.FacingsWide, /*deep*/1, /*hollowStart*/0, barSpacing);
                bar2.Builder = meshInstructions;

                _pegParts.Add(bar2);
                AddPart(bar2);
            }

            if (prod.NumberOfPegHoles == 3)
            {
                MeshPart3DData bar3 = new MeshPart3DData(this);
                bar3.SetMaterial(barMat);

                barCenter =
                    new PointValue()
                    {
                        X = pos.PegX3,
                        Y = orientedSize.Height - pos.PegY3,
                        Z = pegZ + (barDepth / 2F)
                    };

                barSpacing = new PointValue()
                {
                    X = orientedSize.Width + pos.FacingSpaceX - bar3.Size.Width,
                    Y = orientedSize.Height + pos.FacingSpaceY - bar3.Size.Height
                };

                meshInstructions = new MeshBuilderInstructions(_settingsController.GenerateNormals, generateTextures: true);
                meshInstructions.AddBoxFaces(barCenter, barWidth, barHeight, barDepth, BoxFaceType.All);
                meshInstructions.TileMesh(pos.FacingsHigh, pos.FacingsWide, /*deep*/1, /*hollowStart*/0, barSpacing);
                bar3.Builder = meshInstructions;

                _pegParts.Add(bar3);
                AddPart(bar3);
            }
        }

        /// <summary>
        /// Draws the selection outline box.
        /// </summary>
        private void UpdateSelectionOutline()
        {
            if (this.IsSelected)
            {
                IPlanRenderSettings settings = _settingsController;

                if (_selectionOutlinePart == null)
                {
                    //create the outline part
                    _selectionOutlinePart = new BoxModelPart3DData(this);
                    _selectionOutlinePart.IsWireframe = true;
                    AddPart(_selectionOutlinePart);
                }

                _selectionOutlinePart.BeginEdit();

                //update the size - making it slightly inflated
                const Single inflateBy = 0.1F;

                _selectionOutlinePart.Position = new PointValue()
                {
                    X = -inflateBy,
                    Y = -inflateBy,
                    Z = -inflateBy
                };

                _selectionOutlinePart.Size = new WidthHeightDepthValue()
                {
                    Width = ((this.PlanPosition.Width > 0) ? this.PlanPosition.Width : 1) + (inflateBy * 2),
                    Height = ((this.PlanPosition.Height > 0) ? this.PlanPosition.Height : 1) + (inflateBy * 2),
                    Depth = ((this.PlanPosition.Depth > 0) ? this.PlanPosition.Depth : 1) + (inflateBy * 2)
                };

                //update the material
                _selectionOutlinePart.SetMaterial(ModelColour.Transparent);
                _selectionOutlinePart.LineThickness = (settings.SelectionLineThickness > 0) ? settings.SelectionLineThickness : 1;

                _selectionOutlinePart.LineMaterial =
                               new ModelMaterial3D()
                               {
                                   FillColour = settings.SelectionColour,
                                   FillPattern = settings.SelectionFillPattern
                               };

                _selectionOutlinePart.EndEdit();

            }
            else if (_selectionOutlinePart != null)
            {
                RemovePart(_selectionOutlinePart);
                _selectionOutlinePart = null;
            }
        }


        #region Position Block Methods

        /// <summary>
        /// Loads the models for this position.
        /// </summary>
        private void LoadPositionModels()
        {
            //clear any existing child models and parts.
            // Leave labels in place.
            foreach (var child in this.ModelChildren.ToList())
            {
                RemoveChild(child);
            }
            //foreach (var part in this.ModelParts.ToList())
            //{
            //    if (part == _selectionOutlinePart) continue;

            //    RemovePart(part);
            //}
            _trayData.Clear();

            UpdateVisibleFaces();

            if (this.ShowUnits)
            {
                List<ModelConstructPart3DData> createdPartData = CreateUnitPartData();
                List<IModelConstructPart3DData> usedParts = new List<IModelConstructPart3DData>();
                usedParts.Add(_selectionOutlinePart);

                //add in all tray parts
                foreach (ModelConstructPart3DData part in createdPartData)
                {
                    //Tray part - just add.
                    if (Object.Equals(part.Tag, PlanPosition3DPartType.Tray))
                    {
                        AddPart(part);
                        _trayData.Add(part);
                        usedParts.Add(part);
                        continue;
                    }


                    if (part is BoxMeshPart3DData)
                    {
                        BoxMeshPart3DData existingPart =
                            this.ModelParts.FirstOrDefault(p => Object.Equals(p.Tag, part.Tag)) as BoxMeshPart3DData;
                        if (existingPart != null)
                        {
                            BoxMeshPart3DData newPart = (BoxMeshPart3DData)part;

                            //recycle the existing model:
                            existingPart.BeginEdit();
                            {
                                existingPart.Builder = newPart.Builder;
                                existingPart.MaterialTileSize = newPart.MaterialTileSize;
                                existingPart.Position = part.Position;
                                existingPart.Size = part.Size;
                            }
                            existingPart.EndEdit();
                            usedParts.Add(existingPart);

                            continue;
                        }
                    }

                    //Add the new part.
                    AddPart(part);
                    usedParts.Add(part);

                }

                //remove all unused parts:
                foreach (IModelConstructPart3DData part in this.ModelParts.ToList())
                {
                    if (!usedParts.Contains(part)) RemovePart(part);
                }


                //foreach (ModelConstructPart3DData part in partData)
                //{
                //    AddPart(part);

                //    if (Object.Equals(part.Tag, PlanPosition3DPartType.Tray))
                //    {
                //        _trayData.Add(part);
                //    }
                //}
            }
            else
            {
                foreach (var part in this.ModelParts.ToList())
                {
                    if (part == _selectionOutlinePart) continue;
                    RemovePart(part);
                }

                //draw a simple block
                // NB - Render as BoxModelPart3DData not BoxMeshPart3DData otherwise the borderlines are blurry.
                BoxModelPart3DData positionBlock = new BoxModelPart3DData(this);
                positionBlock.Tag = PlanPosition3DPartType.MainBlock;
                positionBlock.BeginEdit();
                positionBlock.Size = new WidthHeightDepthValue()
                {
                    //force this to measure something.
                    Width = (this.PlanPosition.Width > 0) ? this.PlanPosition.Width : 1,
                    Height = (this.PlanPosition.Height > 0) ? this.PlanPosition.Height : 1,
                    Depth = (this.PlanPosition.Depth > 0) ? this.PlanPosition.Depth : 1
                };
                positionBlock.EndEdit();
                AddPart(positionBlock);
            }


            //update the position of the model.
            UpdateModelPlacement();
            UpdateMaterial();
            UpdateProductLabel();
            UpdateVisibility();
            DrawPegs();
            UpdateSelectionOutline();
        }

        /// <summary>
        /// Returns a list of part data that can be used to render the position.
        /// </summary>
        /// <returns></returns>
        private List<ModelConstructPart3DData> CreateUnitPartData()
        {
            IPlanPositionRenderable pos = this.PlanPosition;
            List<ModelConstructPart3DData> partList = new List<ModelConstructPart3DData>();


            //---- Main Block ----
            PlanogramPositionBlockType blockType = PlanogramPositionBlockType.Main;
            RotationValue mainRotation = ProductOrientationHelper.GetRotation(pos.UnitOrientationType);
            _isDrawingUnitlessTrays = pos.IsMerchandisedAsTrays && this.ShowProductImages && HasTrayImage(blockType);

            if (pos.IsMerchandisedAsTrays)
            {
                if (_isDrawingUnitlessTrays)
                {
                    //tray without units 
                    //- will only be used if images are displayed and we have a tray image.
                    partList.Add(CreateUnitlessTrayBlock(blockType, pos, this));
                }
                else
                {
                    //tray with units:
                    partList.AddRange(CreateTrayBlocksWithUnits(blockType, pos, this));
                }
            }
            else
            {
                //normal unit block:
                partList.Add(CreateUnitBlock(blockType, pos, this));
            }



            //---- XBlock ----
            _isDrawingUnitlessTraysX = false;
            if (pos.FacingsXHigh > 0 && pos.FacingsXWide > 0 && pos.FacingsXDeep > 0)
            {
                blockType = PlanogramPositionBlockType.X;
                _isDrawingUnitlessTraysX = pos.IsXMerchandisedAsTrays && this.ShowProductImages && HasTrayImage(blockType);

                if (pos.IsXMerchandisedAsTrays)
                {
                    if (_isDrawingUnitlessTraysX)
                    {
                        //tray without units 
                        //- will only be used if images are displayed and we have a tray image.
                        partList.Add(CreateUnitlessTrayBlock(blockType, pos, this));
                    }
                    else
                    {
                        //tray with units:
                        partList.AddRange(CreateTrayBlocksWithUnits(blockType, pos, this));
                    }
                }
                else
                {
                    //normal unit block:
                    partList.Add(CreateUnitBlock(blockType, pos, this));
                }
            }


            //---- YBlock ----
            _isDrawingUnitlessTraysY = false;
            if (pos.FacingsYHigh > 0 && pos.FacingsYWide > 0 && pos.FacingsYDeep > 0)
            {
                blockType = PlanogramPositionBlockType.Y;
                _isDrawingUnitlessTraysY = pos.IsYMerchandisedAsTrays && this.ShowProductImages && HasTrayImage(blockType);

                if (pos.IsYMerchandisedAsTrays)
                {
                    if (_isDrawingUnitlessTraysY)
                    {
                        //tray without units 
                        //- will only be used if images are displayed and we have a tray image.
                        partList.Add(CreateUnitlessTrayBlock(blockType, pos, this));
                    }
                    else
                    {
                        //tray with units:
                        partList.AddRange(CreateTrayBlocksWithUnits(blockType, pos, this));
                    }
                }
                else
                {
                    //normal unit block:
                    partList.Add(CreateUnitBlock(blockType, pos, this));
                }
            }


            //---- ZBlock ----
            _isDrawingUnitlessTraysZ = false;
            if (pos.FacingsZHigh > 0 && pos.FacingsZWide > 0 && pos.FacingsZDeep > 0)
            {
                blockType = PlanogramPositionBlockType.Z;
                _isDrawingUnitlessTraysZ = pos.IsZMerchandisedAsTrays && this.ShowProductImages && HasTrayImage(blockType);

                if (pos.IsZMerchandisedAsTrays)
                {
                    if (_isDrawingUnitlessTraysZ)
                    {
                        //tray without units 
                        //- will only be used if images are displayed and we have a tray image.
                        partList.Add(CreateUnitlessTrayBlock(blockType, pos, this));
                    }
                    else
                    {
                        //tray with units:
                        partList.AddRange(CreateTrayBlocksWithUnits(blockType, pos, this));
                    }
                }
                else
                {
                    //normal unit block:
                    partList.Add(CreateUnitBlock(blockType, pos, this));
                }

            }

            return partList;
        }


        /// <summary>
        /// Creates a block of standard units at the given start position.
        /// </summary>
        /// <param name="blockType"></param>
        /// <param name="pos"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        private ModelConstructPart3DData CreateUnitBlock(PlanogramPositionBlockType blockType, IPlanPositionRenderable pos, ModelConstruct3DData parentConstructData)
        {
            ModelConstructPart3DData unitBlock3D;

            PlanogramProductShapeType unitShapeType = (this.ShowProductShapes) ? this.PlanProduct.ShapeType : PlanogramProductShapeType.Box;
            Boolean generateNormals = (unitShapeType != PlanogramProductShapeType.Box) || this._settingsController.GenerateNormals;


            WideHighDeepValue facings;
            WidthHeightDepthValue unitSize;
            RotationValue unitRotation;
            PointValue start;
            Object blockTag;
            PointValue facingSpace = new PointValue(pos.FacingSpaceX, pos.FacingSpaceY, pos.FacingSpaceZ);

            switch (blockType)
            {
                default:
                case PlanogramPositionBlockType.Main:
                    facings = new WideHighDeepValue(pos.FacingsWide, pos.FacingsHigh, pos.FacingsDeep);
                    unitSize = new WidthHeightDepthValue(pos.UnitWidth, pos.UnitHeight, pos.UnitDepth);
                    unitRotation = ProductOrientationHelper.GetRotation(pos.UnitOrientationType);
                    start = new PointValue(pos.BlockMainStartX, pos.BlockMainStartY, pos.BlockMainStartZ);
                    blockTag = PlanPosition3DPartType.MainBlock;
                    break;

                case PlanogramPositionBlockType.X:
                    facings = new WideHighDeepValue(pos.FacingsXWide, pos.FacingsXHigh, pos.FacingsXDeep);
                    unitSize = new WidthHeightDepthValue(pos.UnitXWidth, pos.UnitXHeight, pos.UnitXDepth);
                    unitRotation = ProductOrientationHelper.GetRotation(pos.UnitXOrientationType);
                    start = new PointValue(pos.BlockXStartX, pos.BlockXStartY, pos.BlockXStartZ);
                    blockTag = PlanPosition3DPartType.XBlock;
                    break;

                case PlanogramPositionBlockType.Y:
                    facings = new WideHighDeepValue(pos.FacingsYWide, pos.FacingsYHigh, pos.FacingsYDeep);
                    unitSize = new WidthHeightDepthValue(pos.UnitYWidth, pos.UnitYHeight, pos.UnitYDepth);
                    unitRotation = ProductOrientationHelper.GetRotation(pos.UnitYOrientationType);
                    start = new PointValue(pos.BlockYStartX, pos.BlockYStartY, pos.BlockYStartZ);
                    blockTag = PlanPosition3DPartType.YBlock;
                    break;

                case PlanogramPositionBlockType.Z:
                    facings = new WideHighDeepValue(pos.FacingsZWide, pos.FacingsZHigh, pos.FacingsZDeep);
                    unitSize = new WidthHeightDepthValue(pos.UnitZWidth, pos.UnitZHeight, pos.UnitZDepth);
                    unitRotation = ProductOrientationHelper.GetRotation(pos.UnitZOrientationType);
                    start = new PointValue(pos.BlockZStartX, pos.BlockZStartY, pos.BlockZStartZ);
                    blockTag = PlanPosition3DPartType.ZBlock;
                    break;
            }



            //force unit size to be bigger than 0.
            if (unitSize.Width == 0) unitSize.Width = 1;
            if (unitSize.Height == 0) unitSize.Height = 1;
            if (unitSize.Depth == 0) unitSize.Depth = 1;

            WidthHeightDepthValue nestSize = new WidthHeightDepthValue(this.PlanProduct.NestingWidth, this.PlanProduct.NestingHeight, this.PlanProduct.NestingDepth);

            switch (unitShapeType)
            {
                default:
                case PlanogramProductShapeType.Box:
                    {

                        if (nestSize.Height > 0 || nestSize.Width > 0 || nestSize.Depth > 0)
                        {
                            //Nested box:
                            MeshBuilderInstructions instructions = new MeshBuilderInstructions(generateNormals, /*textures*/true);
                            instructions.AddNestableBox(unitSize, nestSize);
                            instructions.RotateMesh(unitRotation);
                            instructions.TileMesh(facings.High, facings.Wide, facings.Deep, _hollowStart, facingSpace);

                            BoxMeshPart3DData blockData = new BoxMeshPart3DData(this);
                            blockData.Position = start;
                            blockData.Builder = instructions;
                            blockData.MaterialTileSize = unitSize;
                            blockData.SetMaterial(ModelColour.White, /*isTiled*/true);

                            unitBlock3D = blockData;
                        }
                        else
                        {
                            TiledBoxModelPart3DData blockData = new TiledBoxModelPart3DData(parentConstructData);
                            blockData.Position = start;
                            blockData.Tile = facings;
                            blockData.TileSize = unitSize;
                            blockData.TileRotation = unitRotation;
                            blockData.TileSpacing = facingSpace;
                            //blockData.NestSize = nestSize;

                            blockData.VisibleFaces = _visibleFaces[blockType];

                            // Amend facings tile to 1 in unseen directions.
                            OptimizeTilesForVisibleFaces(blockData, blockType);

                            blockData.SetMaterial(ModelColour.White, /*isTiled*/true);

                            unitBlock3D = blockData;
                        }
                    }
                    break;

                case PlanogramProductShapeType.Bottle:
                    {
                        MeshBuilderInstructions instructions = new MeshBuilderInstructions(generateNormals, /*textures*/true);
                        instructions.AddBottle(unitSize.Width, unitSize.Height, unitSize.Depth);
                        instructions.RotateMesh(unitRotation);
                        instructions.TileMesh(facings.High, facings.Wide, facings.Deep, _hollowStart, facingSpace);


                        BoxMeshPart3DData blockData = new BoxMeshPart3DData(this);
                        blockData.Position = start;
                        blockData.Builder = instructions;
                        blockData.MaterialTileSize = unitSize;
                        blockData.SetMaterial(ModelColour.White, /*isTiled*/true);

                        unitBlock3D = blockData;
                    }
                    break;


                    //case PlanogramProductShapeType.Jar:
                    //    instructions.AddJar(unitSize.X, unitSize.Y, unitSize.Z);
                    //    break;

                    //case PlanogramProductShapeType.Can:
                    //    instructions.AddCan(unitSize.X, unitSize.Y, unitSize.Z);
                    //    break;
            }



            //finalize the block
            unitBlock3D.Tag = blockTag;


            unitBlock3D.EndEdit();

            return unitBlock3D;

        }

        /// <summary>
        /// Creates single blocks to represent each tray. Does not draw the tray itself or the units inside.
        /// Used for displaying tray images
        /// </summary>
        private static ModelConstructPart3DData CreateUnitlessTrayBlock(PlanogramPositionBlockType blockType, IPlanPositionRenderable pos, ModelConstruct3DData parentConstructData)
        {
            WideHighDeepValue facings;
            RotationValue trayRotation;
            PointValue start;
            Object blockTag;

            switch (blockType)
            {
                default:
                case PlanogramPositionBlockType.Main:
                    facings = new WideHighDeepValue(pos.FacingsWide, pos.FacingsHigh, pos.FacingsDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitOrientationType);
                    start = new PointValue(pos.BlockMainStartX, pos.BlockMainStartY, pos.BlockMainStartZ);
                    blockTag = PlanPosition3DPartType.MainBlock;
                    break;

                case PlanogramPositionBlockType.X:
                    facings = new WideHighDeepValue(pos.FacingsXWide, pos.FacingsXHigh, pos.FacingsXDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitXOrientationType);
                    start = new PointValue(pos.BlockXStartX, pos.BlockXStartY, pos.BlockXStartZ);
                    blockTag = PlanPosition3DPartType.XBlock;
                    break;

                case PlanogramPositionBlockType.Y:
                    facings = new WideHighDeepValue(pos.FacingsYWide, pos.FacingsYHigh, pos.FacingsYDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitYOrientationType);
                    start = new PointValue(pos.BlockYStartX, pos.BlockYStartY, pos.BlockYStartZ);
                    blockTag = PlanPosition3DPartType.YBlock;
                    break;

                case PlanogramPositionBlockType.Z:
                    facings = new WideHighDeepValue(pos.FacingsZWide, pos.FacingsZHigh, pos.FacingsZDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitZOrientationType);
                    start = new PointValue(pos.BlockZStartX, pos.BlockZStartY, pos.BlockZStartZ);
                    blockTag = PlanPosition3DPartType.ZBlock;
                    break;
            }


            IPlanProductRenderable planProduct = pos.Product;

            Single trayWidth = (planProduct.TrayWidth > 0) ? planProduct.TrayWidth : 1;
            Single trayHeight = (planProduct.TrayHeight > 0) ? planProduct.TrayHeight : 1;
            Single trayDepth = (planProduct.TrayDepth > 0) ? planProduct.TrayDepth : 1;

            WidthHeightDepthValue traySize = new WidthHeightDepthValue()
            {
                Width = trayWidth,
                Height = trayHeight,
                Depth = trayDepth,
            };

            //force tray size to be bigger than 0.
            if (traySize.Width == 0) traySize.Width = 1;
            if (traySize.Height == 0) traySize.Height = 1;
            if (traySize.Depth == 0) traySize.Depth = 1;


            //create the block
            TiledBoxModelPart3DData unitBlock3D = new TiledBoxModelPart3DData(parentConstructData);

            unitBlock3D.Tile = facings;
            unitBlock3D.TileSize = traySize;
            unitBlock3D.TileRotation = trayRotation;
            unitBlock3D.TileSpacing = new PointValue(pos.FacingSpaceX, pos.FacingSpaceY, pos.FacingSpaceZ);

            unitBlock3D.SetMaterial(ModelColour.White, /*isTiled*/true);


            unitBlock3D.Position = start;

            unitBlock3D.Tag = blockTag;

            unitBlock3D.EndEdit();
            return unitBlock3D;
        }

        /// <summary>
        /// Creates a block of trays with units showing.
        /// </summary>
        /// <param name="blockType"></param>
        /// <param name="pos"></param>
        /// <param name="parentConstructData"></param>
        /// <returns></returns>
        private IEnumerable<ModelConstructPart3DData> CreateTrayBlocksWithUnits(
            PlanogramPositionBlockType blockType, IPlanPositionRenderable pos, ModelConstruct3DData parentConstructData)
        {
            List<ModelConstructPart3DData> output = new List<ModelConstructPart3DData>();
            IPlanProductRenderable planProduct = pos.Product;

            //get block specific values.
            WideHighDeepValue trayFacings;
            RotationValue trayRotation;
            PointValue trayStart;
            Object blockTag;

            switch (blockType)
            {
                default:
                case PlanogramPositionBlockType.Main:
                    trayFacings = new WideHighDeepValue(pos.FacingsWide, pos.FacingsHigh, pos.FacingsDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitOrientationType);
                    trayStart = new PointValue(pos.BlockMainStartX, pos.BlockMainStartY, pos.BlockMainStartZ);
                    blockTag = PlanPosition3DPartType.MainBlock;
                    break;

                case PlanogramPositionBlockType.X:
                    trayFacings = new WideHighDeepValue(pos.FacingsXWide, pos.FacingsXHigh, pos.FacingsXDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitXOrientationType);
                    trayStart = new PointValue(pos.BlockXStartX, pos.BlockXStartY, pos.BlockXStartZ);
                    blockTag = PlanPosition3DPartType.XBlock;
                    break;

                case PlanogramPositionBlockType.Y:
                    trayFacings = new WideHighDeepValue(pos.FacingsYWide, pos.FacingsYHigh, pos.FacingsYDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitYOrientationType);
                    trayStart = new PointValue(pos.BlockYStartX, pos.BlockYStartY, pos.BlockYStartZ);
                    blockTag = PlanPosition3DPartType.YBlock;
                    break;

                case PlanogramPositionBlockType.Z:
                    trayFacings = new WideHighDeepValue(pos.FacingsZWide, pos.FacingsZHigh, pos.FacingsZDeep);
                    trayRotation = ProductOrientationHelper.GetRotation(pos.UnitZOrientationType);
                    trayStart = new PointValue(pos.BlockZStartX, pos.BlockZStartY, pos.BlockZStartZ);
                    blockTag = PlanPosition3DPartType.ZBlock;
                    break;
            }


            //TODO: Amend tray facings tile to 1 in unseen directions.
            // i.e if front on and orth design/front camera then dont bother drawing more than 1 deep.
            // would need to remember to correct the start pos for this too tho.


            #region Create Trays Model

            MatrixValue trayRotationMatrix = MatrixValue.CreateRotationMatrix(trayRotation);

            //calculate tray values:
            WidthHeightDepthValue trayThick = new WidthHeightDepthValue()
            {
                Width = (planProduct.TrayThickWidth > 0) ? planProduct.TrayThickWidth : 0.2F,
                Height = (planProduct.TrayThickHeight > 0) ? planProduct.TrayThickHeight : 0.2F,
                Depth = (planProduct.TrayThickDepth > 0) ? planProduct.TrayThickDepth : 0.2F
            };

            Single trayWidth = (planProduct.TrayWidth > 0) ? planProduct.TrayWidth : 1;
            Single trayHeight = (planProduct.TrayHeight > 0) ? planProduct.TrayHeight : 1;
            Single trayDepth = (planProduct.TrayDepth > 0) ? planProduct.TrayDepth : 1;

            WidthHeightDepthValue traySize = new WidthHeightDepthValue()
            {
                Width = trayWidth,
                Height = (trayHeight * 0.25F) + trayThick.Height,
                Depth = trayDepth,
            };

            WidthHeightDepthValue trayUnitSize =
                new WidthHeightDepthValue(trayWidth, trayHeight, trayDepth).Transform(trayRotationMatrix).Abs();

            WidthHeightDepthValue trayNestSize = new WidthHeightDepthValue(0, trayThick.Height, 0);

            WidthHeightDepthValue rotatedTrayThick =
                new WidthHeightDepthValue(trayThick.Width, trayThick.Height, trayThick.Depth).Transform(trayRotationMatrix).Abs();


            PointValue traySpacing = new PointValue()
            {
                X = pos.FacingSpaceX,
                Y = trayHeight - trayThick.Height + pos.FacingSpaceY,
                Z = pos.FacingSpaceZ
            }
            .Transform(trayRotationMatrix).Abs();


            //create the tray block

            BoxMeshPart3DData trayModel = new BoxMeshPart3DData(parentConstructData);
            trayModel.BeginEdit();

            trayModel.Position = trayStart;

            MeshBuilderInstructions meshBuilder = new MeshBuilderInstructions(/*normals_settingsController.GenerateNormals*/ false, /*textures*/true);
            meshBuilder.AddNestableBox(traySize, trayNestSize, /*skew*/false);
            meshBuilder.RotateMesh(trayRotation);
            meshBuilder.TileMesh(trayFacings.High, trayFacings.Wide, trayFacings.Deep, _hollowStart, traySpacing);
            trayModel.Builder = meshBuilder;

            trayModel.MaterialTileSize = traySize;
            trayModel.SetMaterial(_trayColour, /*isTiled*/true);

            //tag this part as a unit block for reference.
            trayModel.Tag = PlanPosition3DPartType.Tray;

            trayModel.EndEdit();
            output.Add(trayModel);
            #endregion



            //create the unit blocks:
            WideHighDeepValue unitsInTray =
                new WideHighDeepValue(
                (Int16)Math.Max((Int32)planProduct.TrayWide, 1),
                (Int16)Math.Max((Int32)planProduct.TrayHigh, 1),
                (Int16)Math.Max((Int32)planProduct.TrayDeep, 1));

            WidthHeightDepthValue unitSize = new WidthHeightDepthValue
            {
                Width = /*innerTrayWidth*/(trayWidth - (trayThick.Width * 2)) / unitsInTray.Wide,
                Height = /*innerTrayHeight*/(trayHeight - trayThick.Height) / unitsInTray.High,
                Depth = /*innerTrayDepth*/(trayDepth - (trayThick.Depth * 2)) / unitsInTray.Deep
            };
            //force unit size to be bigger than 0.
            if (unitSize.Width == 0) unitSize.Width = 1;
            if (unitSize.Height == 0) unitSize.Height = 1;
            if (unitSize.Depth == 0) unitSize.Depth = 1;

            PlanogramProductShapeType unitShapeType = (this.ShowProductShapes) ? this.PlanProduct.ShapeType : PlanogramProductShapeType.Box;
            Boolean generateNormals = (unitShapeType != PlanogramProductShapeType.Box) || this._settingsController.GenerateNormals;

            PointValue unitSpacing = new PointValue(); //no spacing
            RotationValue unitRotation = new RotationValue(); //no rotation

            Single currY = trayStart.Y + rotatedTrayThick.Height;
            for (Int32 tHigh = 0; tHigh < trayFacings.High; tHigh++)
            {
                Single currX = trayStart.X + rotatedTrayThick.Width;
                for (Int32 tWide = 0; tWide < trayFacings.Wide; tWide++)
                {
                    Single currZ = trayStart.Z + rotatedTrayThick.Depth;
                    for (Int32 tDeep = 0; tDeep < trayFacings.Deep; tDeep++)
                    {
                        PointValue start = new PointValue(currX, currY, currZ);

                        #region Create Tray Units

                        switch (unitShapeType)
                        {
                            default:
                            case PlanogramProductShapeType.Box:
                                {
                                    WidthHeightDepthValue nestSize = new WidthHeightDepthValue(this.PlanProduct.NestingWidth, this.PlanProduct.NestingHeight, this.PlanProduct.NestingDepth);
                                    if (nestSize.Height > 0 || nestSize.Width > 0 || nestSize.Depth > 0)
                                    {
                                        //Nested box:
                                        MeshBuilderInstructions instructions = new MeshBuilderInstructions(generateNormals, /*textures*/true);
                                        instructions.AddNestableBox(unitSize, nestSize);
                                        instructions.RotateMesh(unitRotation);
                                        instructions.TileMesh(unitsInTray.High, unitsInTray.Wide, unitsInTray.Deep, _hollowStart, unitSpacing);
                                        instructions.RotateMesh(trayRotation);

                                        BoxMeshPart3DData unitBlock = new BoxMeshPart3DData(this);
                                        unitBlock.Builder = instructions;
                                        unitBlock.MaterialTileSize = unitSize;
                                        unitBlock.SetMaterial(ModelColour.White, /*isTiled*/true);

                                        unitBlock.Position = start;
                                        unitBlock.Tag = blockTag;
                                        unitBlock.EndEdit();
                                        output.Add(unitBlock);
                                    }
                                    else
                                    {

                                        TiledBoxModelPart3DData unitBlock = new TiledBoxModelPart3DData(parentConstructData);
                                        unitBlock.Tile = unitsInTray;
                                        unitBlock.TileSize = unitSize;
                                        unitBlock.TileRotation = unitRotation;
                                        unitBlock.TileSpacing = unitSpacing;
                                        //unitBlock.NestSize = nestSize;

                                        unitBlock.SetMaterial(ModelColour.White, /*isTiled*/true);
                                        unitBlock.Position = start;
                                        unitBlock.Tag = blockTag;

                                        unitBlock.VisibleFaces = _visibleFaces[blockType];

                                        //TODO: Amend tray facings tile to 1 in unseen directions.
                                        // i.e if front on and orth design/front camera then dont bother drawing more than 1 deep.
                                        // would need to remember to correct the start pos for this too tho.

                                        unitBlock.Rotation = trayRotation; //TODO check!!

                                        unitBlock.EndEdit();
                                        output.Add(unitBlock);
                                    }
                                }
                                break;

                            case PlanogramProductShapeType.Bottle:
                                {

                                    MeshBuilderInstructions instructions = new MeshBuilderInstructions(generateNormals, /*textures*/true);
                                    instructions.AddBottle(unitSize.Width, unitSize.Height, unitSize.Depth);
                                    instructions.RotateMesh(unitRotation);
                                    instructions.TileMesh(unitsInTray.High, unitsInTray.Wide, unitsInTray.Deep, _hollowStart, unitSpacing);


                                    BoxMeshPart3DData unitBlock = new BoxMeshPart3DData(this);
                                    unitBlock.Builder = instructions;
                                    unitBlock.MaterialTileSize = unitSize;
                                    unitBlock.SetMaterial(ModelColour.White, /*isTiled*/true);

                                    unitBlock.Position = start;
                                    unitBlock.Tag = blockTag;

                                    unitBlock.EndEdit();
                                    output.Add(unitBlock);
                                }
                                break;


                        }
                        #endregion


                        currZ += trayUnitSize.Depth;
                    }

                    currX += trayUnitSize.Width;
                    currX += pos.FacingSpaceX; // add on spacing.
                }

                currY += trayUnitSize.Height;
            }


            return output;
        }


        #region OLD


        /// <summary>
        /// Create the unit mesh block instructions
        /// </summary>
        private MeshBuilderInstructions CreateUnitBlockMeshInstructions(
            Int32 high, Int32 wide, Int32 deep, WidthHeightDepthValue unitSize, PointValue facingSpace, RotationValue rotation)
        {
            PlanogramProductShapeType unitShapeType = (this.ShowProductShapes) ? this.PlanProduct.ShapeType : PlanogramProductShapeType.Box;

            Boolean generateNormals = (unitShapeType != PlanogramProductShapeType.Box) || this._settingsController.GenerateNormals;
            MeshBuilderInstructions instructions = new MeshBuilderInstructions(generateNormals, /*textures*/true);

            switch (unitShapeType)
            {
                default:
                case PlanogramProductShapeType.Box:
                    instructions.AddNestableBox(unitSize,
                        new WidthHeightDepthValue(this.PlanProduct.NestingWidth, this.PlanProduct.NestingHeight, this.PlanProduct.NestingDepth));
                    break;

                case PlanogramProductShapeType.Bottle:
                    instructions.AddBottle(unitSize.Width, unitSize.Height, unitSize.Depth);
                    break;


                    //case PlanogramProductShapeType.Jar:
                    //    instructions.AddJar(unitSize.X, unitSize.Y, unitSize.Z);
                    //    break;

                    //case PlanogramProductShapeType.Can:
                    //    instructions.AddCan(unitSize.X, unitSize.Y, unitSize.Z);
                    //    break;
            }
            instructions.RotateMesh(rotation);
            instructions.TileMesh(high, wide, deep, _hollowStart, facingSpace);

            return instructions;
        }

        /// <summary>
        /// Creates a simple unit block using the given instructions
        /// </summary>
        private ModelConstructPart3DData CreateUnitBlock(
            MeshBuilderInstructions instructions, PlanogramPositionBlockType blockType, PointValue start, WidthHeightDepthValue unitSize)
        {
            BoxMeshPart3DData unitBlock3D = new BoxMeshPart3DData(this);

            unitBlock3D.Builder = instructions;
            unitBlock3D.MaterialTileSize = unitSize;
            unitBlock3D.SetMaterial(ModelColour.White, /*isTiled*/true);


            unitBlock3D.Position = start;

            //tag this part as a unit block for reference.
            switch (blockType)
            {
                case PlanogramPositionBlockType.Main:
                    unitBlock3D.Tag = PlanPosition3DPartType.MainBlock;
                    break;

                case PlanogramPositionBlockType.X:
                    unitBlock3D.Tag = PlanPosition3DPartType.XBlock;
                    break;

                case PlanogramPositionBlockType.Y:
                    unitBlock3D.Tag = PlanPosition3DPartType.YBlock;
                    break;

                case PlanogramPositionBlockType.Z:
                    unitBlock3D.Tag = PlanPosition3DPartType.ZBlock;
                    break;
            }


            unitBlock3D.EndEdit();
            return unitBlock3D;
        }

        // private ModelConstructPart3DData CreateUnitBlock(PlanogramPositionBlockType blockType,
        //Int32 high, Int32 wide, Int32 deep, PointValue start, RotationValue rotation)
        // {

        //     IPlanPositionRenderable pos = this.PlanPosition;

        //     WidthHeightDepthValue unitSize;

        //     switch (blockType)
        //     {
        //         default:
        //         case PlanogramPositionBlockType.Main:
        //             unitSize = new WidthHeightDepthValue(pos.UnitWidth, pos.UnitHeight, pos.UnitDepth);
        //             break;

        //         case PlanogramPositionBlockType.X:
        //             unitSize = new WidthHeightDepthValue(pos.UnitXWidth, pos.UnitXHeight, pos.UnitXDepth);
        //             break;

        //         case PlanogramPositionBlockType.Y:
        //             unitSize = new WidthHeightDepthValue(pos.UnitYWidth, pos.UnitYHeight, pos.UnitYDepth);
        //             break;

        //         case PlanogramPositionBlockType.Z:
        //             unitSize = new WidthHeightDepthValue(pos.UnitZWidth, pos.UnitZHeight, pos.UnitZDepth);
        //             break;
        //     }



        //     return CreateUnitBlock(this, blockType, unitSize,
        //         new PointValue(pos.FacingSpaceX, pos.FacingSpaceY, pos.FacingSpaceZ),
        //         new WideHighDeepValue((Int16)wide, (Int16)high, (Int16)deep),
        //         start, rotation);
        // }


        /// <summary>
        /// Creates a block of trays with the tray part and unit blocks inside
        /// </summary>
        //private ModelConstructPart3DData[] CreateTrayBlocksWithUnits(PlanogramPositionBlockType blockType,
        //    Int32 high, Int32 wide, Int32 deep, PointValue start, RotationValue rotation)
        //{
        //    ModelConstructPart3DData[] output = new ModelConstructPart3DData[2];

        //    IPlanPositionRenderable position = this.PlanPosition;
        //    IPlanProductRenderable planProduct = position.Product;

        //    WidthHeightDepthValue trayThick = new WidthHeightDepthValue()
        //    {
        //        Width = (planProduct.TrayThickWidth > 0) ? planProduct.TrayThickWidth : 0.2F,
        //        Height = (planProduct.TrayThickHeight > 0) ? planProduct.TrayThickHeight : 0.2F,
        //        Depth = (planProduct.TrayThickDepth > 0) ? planProduct.TrayThickDepth : 0.2F
        //    };

        //    Single trayWidth = (planProduct.TrayWidth > 0) ? planProduct.TrayWidth : 1;
        //    Single trayHeight = (planProduct.TrayHeight > 0) ? planProduct.TrayHeight : 1;
        //    Single trayDepth = (planProduct.TrayDepth > 0) ? planProduct.TrayDepth : 1;

        //    WidthHeightDepthValue traySize = new WidthHeightDepthValue()
        //    {
        //        Width = trayWidth,
        //        Height = (trayHeight * 0.25F) + trayThick.Height,
        //        Depth = trayDepth,
        //    };

        //    WidthHeightDepthValue trayUnitSize =
        //        new WidthHeightDepthValue(trayWidth, trayHeight, trayDepth)
        //        .Transform(MatrixValue.CreateRotationMatrix(rotation)).Abs();

        //    WidthHeightDepthValue trayNestSize = new WidthHeightDepthValue(0, trayThick.Height, 0);

        //    WidthHeightDepthValue rotatedTrayThick = new WidthHeightDepthValue(trayThick.Width, trayThick.Height, trayThick.Depth)
        //    .Transform(MatrixValue.CreateRotationMatrix(rotation)).Abs();


        //    PointValue traySpacing = new PointValue()
        //    {
        //        X = position.FacingSpaceX,
        //        Y = trayHeight/*trayUnitSize.Y*/ - trayThick.Height + position.FacingSpaceY,
        //        Z = position.FacingSpaceZ
        //    }
        //    .Transform(MatrixValue.CreateRotationMatrix(rotation)).Abs();


        //    //create the tray block
        //    BoxMeshPart3DData trayModel = new BoxMeshPart3DData(this);
        //    trayModel.BeginEdit();

        //    trayModel.Position = start;

        //    MeshBuilderInstructions meshBuilder = new MeshBuilderInstructions(/*normals*/_settingsController.GenerateNormals, /*textures*/true);
        //    meshBuilder.AddNestableBox(traySize, trayNestSize, /*skew*/false);
        //    meshBuilder.RotateMesh(rotation);
        //    meshBuilder.TileMesh(high, wide, deep, _hollowStart, traySpacing);
        //    trayModel.Builder = meshBuilder;

        //    trayModel.MaterialTileSize = traySize;
        //    trayModel.SetMaterial(_trayColour, /*isTiled*/true);

        //    //tag this part as a unit block for reference.
        //    trayModel.Tag = PlanPosition3DPartType.Tray;

        //    trayModel.EndEdit();
        //    output[0] = trayModel;


        //    //create the unit block
        //    MeshBuilderInstructions finalUnitBlockInstructions = new MeshBuilderInstructions(true, true);

        //    Int32 unitsInTrayHigh = Math.Max((Int32)planProduct.TrayHigh, 1);
        //    Int32 unitsInTrayWide = Math.Max((Int32)planProduct.TrayWide, 1);
        //    Int32 unitsInTrayDeep = Math.Max((Int32)planProduct.TrayDeep, 1);
        //    Single innerTrayWidth = trayWidth - (trayThick.Width * 2);
        //    Single innerTrayHeight = trayHeight - trayThick.Height;
        //    Single innerTrayDepth = trayDepth - (trayThick.Depth * 2);

        //    WidthHeightDepthValue unitSize = new WidthHeightDepthValue
        //    {
        //        Width = innerTrayWidth / unitsInTrayWide,
        //        Height = innerTrayHeight / unitsInTrayHigh,
        //        Depth = innerTrayDepth / unitsInTrayDeep
        //    };
        //    PointValue unitSpacing = new PointValue(); //no spacing
        //    RotationValue unitRotation = new RotationValue(); //no rotation

        //    Single currY = rotatedTrayThick.Height;//trayThick.Height;
        //    for (Int32 tHigh = 0; tHigh < high; tHigh++)
        //    {
        //        Single currX = rotatedTrayThick.Width;
        //        for (Int32 tWide = 0; tWide < wide; tWide++)
        //        {
        //            Single currZ = rotatedTrayThick.Depth;
        //            for (Int32 tDeep = 0; tDeep < deep; tDeep++)
        //            {
        //                MeshBuilderInstructions unitInstructions = CreateUnitBlockMeshInstructions(
        //                    unitsInTrayHigh, unitsInTrayWide, unitsInTrayDeep, unitSize, unitSpacing, unitRotation);
        //                unitInstructions.RotateMesh(rotation);

        //                finalUnitBlockInstructions.Append(unitInstructions, new PointValue(currX, currY, currZ));

        //                currZ += trayUnitSize.Depth;
        //            }

        //            currX += trayUnitSize.Width;
        //            currX += position.FacingSpaceX; // add on spacing.
        //        }

        //        currY += trayUnitSize.Height;
        //    }


        //    output[1] = CreateUnitBlock(finalUnitBlockInstructions, blockType, start, unitSize);


        //    return output;
        //}

        ///// <summary>
        ///// Creates single blocks to represent each tray. Does not draw the tray itself or the units inside.
        ///// Used for displaying tray images
        ///// </summary>
        //private ModelConstructPart3DData CreateUnitlessTrayBlock(PlanogramPositionBlockType blockType,
        //    Int32 high, Int32 wide, Int32 deep, PointValue start, RotationValue rotation)
        //{

        //    IPlanPositionRenderable position = this.PlanPosition;
        //    IPlanProductRenderable planProduct = position.Product;

        //    Single trayWidth = (planProduct.TrayWidth > 0) ? planProduct.TrayWidth : 1;
        //    Single trayHeight = (planProduct.TrayHeight > 0) ? planProduct.TrayHeight : 1;
        //    Single trayDepth = (planProduct.TrayDepth > 0) ? planProduct.TrayDepth : 1;

        //    WidthHeightDepthValue traySize = new WidthHeightDepthValue()
        //    {
        //        Width = trayWidth,
        //        Height = trayHeight,
        //        Depth = trayDepth,
        //    };

        //    return CreateUnitBlock(this, blockType,
        //        traySize, new PointValue(), new WideHighDeepValue((Int16)wide, (Int16)high, (Int16)deep),
        //        start, rotation);

        //    //MeshBuilderInstructions instructions =
        //    //    CreateUnitBlockMeshInstructions(high, wide, deep, traySize, new PointValue(), rotation);

        //    //return CreateUnitBlock(instructions, blockType, start, traySize);
        //}

        #endregion

        #endregion

        #region Image Helpers

        /// <summary>
        /// Returns true if this has any images available
        /// </summary>
        /// <returns></returns>
        public Boolean HasAvailableImages()
        {
            if (this.PlanPosition == null) return false;

            //always return false if images are off as we don't want to trigger
            // a load by accident.
            if (!_settingsController.ShowProductImages) return false;

            BoxFaceType faces = GetVisibleFaces();

            return
                 ((faces & BoxFaceType.Front) == BoxFaceType.Front && this.PlanPosition.FrontImageData != null)
                || ((faces & BoxFaceType.Back) == BoxFaceType.Back && this.PlanPosition.BackImageData != null)
                || ((faces & BoxFaceType.Left) == BoxFaceType.Left && this.PlanPosition.LeftImageData != null)
                || ((faces & BoxFaceType.Right) == BoxFaceType.Right && this.PlanPosition.RightImageData != null)
                || ((faces & BoxFaceType.Top) == BoxFaceType.Top && this.PlanPosition.TopImageData != null)
                || ((faces & BoxFaceType.Bottom) == BoxFaceType.Bottom && this.PlanPosition.BottomImageData != null);
        }


        /// <summary>
        /// Returns the images to be displayed for the given part type.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="partType"></param>
        /// <param name="frontImageData"></param>
        /// <param name="backImageData"></param>
        /// <param name="topImageData"></param>
        /// <param name="bottomImageData"></param>
        /// <param name="leftImageData"></param>
        /// <param name="rightImageData"></param>
        private void GetVisibleImageData(
            IPlanPositionRenderable pos,
            PlanogramPositionBlockType blockType,
            out Byte[] frontImageData, out Byte[] backImageData,
            out Byte[] topImageData, out Byte[] bottomImageData,
            out Byte[] leftImageData, out Byte[] rightImageData)
        {

            frontImageData = null;
            backImageData = null;
            topImageData = null;
            bottomImageData = null;
            leftImageData = null;
            rightImageData = null;

            if (!_settingsController.ShowProductImages) return;

            BoxFaceType face = _visibleFaces[blockType];
            if ((face & BoxFaceType.Front) == BoxFaceType.Front) frontImageData = GetImageData(blockType, BoxFaceType.Front);
            if ((face & BoxFaceType.Back) == BoxFaceType.Back) backImageData = GetImageData(blockType, BoxFaceType.Back);
            if ((face & BoxFaceType.Top) == BoxFaceType.Top) topImageData = GetImageData(blockType, BoxFaceType.Top);
            if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom) bottomImageData = GetImageData(blockType, BoxFaceType.Bottom);
            if ((face & BoxFaceType.Left) == BoxFaceType.Left) leftImageData = GetImageData(blockType, BoxFaceType.Left);
            if ((face & BoxFaceType.Right) == BoxFaceType.Right) rightImageData = GetImageData(blockType, BoxFaceType.Right);


            //now double check to make sure that we do not show a mix of tray and unit images.
            Boolean isDrawingTrayImages = false;
            switch (blockType)
            {
                case PlanogramPositionBlockType.Main:
                    isDrawingTrayImages = _isDrawingUnitlessTrays;
                    break;
                case PlanogramPositionBlockType.X:
                    isDrawingTrayImages = _isDrawingUnitlessTraysX;
                    break;
                case PlanogramPositionBlockType.Y:
                    isDrawingTrayImages = _isDrawingUnitlessTraysY;
                    break;
                case PlanogramPositionBlockType.Z:
                    isDrawingTrayImages = _isDrawingUnitlessTraysZ;
                    break;
            }
            if (isDrawingTrayImages)
            {
                if (frontImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Front))
                    frontImageData = null;

                if (backImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Back))
                    backImageData = null;

                if (topImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Top))
                    topImageData = null;

                if (bottomImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Bottom))
                    bottomImageData = null;

                if (leftImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Left))
                    leftImageData = null;

                if (rightImageData != null && !this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Right))
                    rightImageData = null;
            }
        }

        /// <summary>
        /// Returns the requested image holder
        /// </summary>
        /// <param name="blockType">the block type to get for</param>
        /// <param name="face">the box face requested.</param>
        /// <returns></returns>
        private Byte[] GetImageData(PlanogramPositionBlockType blockType, BoxFaceType face)
        {
            switch (blockType)
            {
                case PlanogramPositionBlockType.Main:
                    if ((face & BoxFaceType.Front) == BoxFaceType.Front) return this.PlanPosition.FrontImageData;
                    if ((face & BoxFaceType.Back) == BoxFaceType.Back) return this.PlanPosition.BackImageData;
                    if ((face & BoxFaceType.Top) == BoxFaceType.Top) return this.PlanPosition.TopImageData;
                    if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom) return this.PlanPosition.BottomImageData;
                    if ((face & BoxFaceType.Left) == BoxFaceType.Left) return this.PlanPosition.LeftImageData;
                    if ((face & BoxFaceType.Right) == BoxFaceType.Right) return this.PlanPosition.RightImageData;
                    break;

                case PlanogramPositionBlockType.X:
                    if ((face & BoxFaceType.Front) == BoxFaceType.Front) return this.PlanPosition.FrontXImageData;
                    if ((face & BoxFaceType.Back) == BoxFaceType.Back) return this.PlanPosition.BackXImageData;
                    if ((face & BoxFaceType.Top) == BoxFaceType.Top) return this.PlanPosition.TopXImageData;
                    if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom) return this.PlanPosition.BottomXImageData;
                    if ((face & BoxFaceType.Left) == BoxFaceType.Left) return this.PlanPosition.LeftXImageData;
                    if ((face & BoxFaceType.Right) == BoxFaceType.Right) return this.PlanPosition.RightXImageData;
                    break;

                case PlanogramPositionBlockType.Y:
                    if ((face & BoxFaceType.Front) == BoxFaceType.Front) return this.PlanPosition.FrontYImageData;
                    if ((face & BoxFaceType.Back) == BoxFaceType.Back) return this.PlanPosition.BackYImageData;
                    if ((face & BoxFaceType.Top) == BoxFaceType.Top) return this.PlanPosition.TopYImageData;
                    if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom) return this.PlanPosition.BottomYImageData;
                    if ((face & BoxFaceType.Left) == BoxFaceType.Left) return this.PlanPosition.LeftYImageData;
                    if ((face & BoxFaceType.Right) == BoxFaceType.Right) return this.PlanPosition.RightYImageData;
                    break;

                case PlanogramPositionBlockType.Z:
                    if ((face & BoxFaceType.Front) == BoxFaceType.Front) return this.PlanPosition.FrontZImageData;
                    if ((face & BoxFaceType.Back) == BoxFaceType.Back) return this.PlanPosition.BackZImageData;
                    if ((face & BoxFaceType.Top) == BoxFaceType.Top) return this.PlanPosition.TopZImageData;
                    if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom) return this.PlanPosition.BottomZImageData;
                    if ((face & BoxFaceType.Left) == BoxFaceType.Left) return this.PlanPosition.LeftZImageData;
                    if ((face & BoxFaceType.Right) == BoxFaceType.Right) return this.PlanPosition.RightZImageData;
                    break;
            }
            return null;
        }

        /// <summary>
        /// Returns true if we should redraw parts because of a change to
        /// the display of tray images.
        /// </summary>
        /// <returns></returns>
        private Boolean ShouldRedrawForTrayImages()
        {
            return this.PlanPosition.IsMerchandisedAsTrays
                || (this.PlanPosition.IsXMerchandisedAsTrays
                    && this.PlanPosition.FacingsXHigh > 0 && this.PlanPosition.FacingsXWide > 0 && this.PlanPosition.FacingsXDeep > 0)
                 || (this.PlanPosition.IsYMerchandisedAsTrays
                    && this.PlanPosition.FacingsYHigh > 0 && this.PlanPosition.FacingsYWide > 0 && this.PlanPosition.FacingsYDeep > 0)
                || (this.PlanPosition.IsZMerchandisedAsTrays
                    && this.PlanPosition.FacingsZHigh > 0 && this.PlanPosition.FacingsZWide > 0 && this.PlanPosition.FacingsZDeep > 0);

            //---------------------------


            ////if we are not showing images but are drawing one of the tray ones then we should redraw.
            //if(!this.ShowProductImages
            //    && 
            //    (_isDrawingUnitlessTrays || _isDrawingUnitlessTraysX 
            //    || _isDrawingUnitlessTraysY || _isDrawingUnitlessTraysZ))
            //{
            //    return true;
            //}

            ////if we are showing images, 
            //// check if we have trays for which we are either
            ////  drawing the tray image but have no longer have one
            //// or we are not drawing the tray image but we do have one.
            //if (this.ShowProductImages)
            //{
            //    if (this.PlanPosition.IsMerchandisedAsTrays)
            //    {
            //        Boolean hasTrayImg = HasTrayImage(PlanogramPositionBlockType.Main);
            //        if ((_isDrawingUnitlessTrays && !hasTrayImg)
            //            || (!_isDrawingUnitlessTrays && hasTrayImg))
            //        {
            //            return true;
            //        }
            //    }

            //    if (this.PlanPosition.IsXMerchandisedAsTrays)
            //    {
            //        Boolean hasTrayImg = HasTrayImage(PlanogramPositionBlockType.X);
            //        if ((_isDrawingUnitlessTraysX && !hasTrayImg)
            //            || (!_isDrawingUnitlessTraysX && hasTrayImg))
            //        {
            //            return true;
            //        }
            //    }

            //    if (this.PlanPosition.IsYMerchandisedAsTrays)
            //    {
            //        Boolean hasTrayImg = HasTrayImage(PlanogramPositionBlockType.Y);
            //        if ((_isDrawingUnitlessTraysY && !hasTrayImg)
            //            || (!_isDrawingUnitlessTraysY && hasTrayImg))
            //        {
            //            return true;
            //        }
            //    }

            //    if (this.PlanPosition.IsZMerchandisedAsTrays)
            //    {
            //        Boolean hasTrayImg = HasTrayImage(PlanogramPositionBlockType.Z);
            //        if ((_isDrawingUnitlessTraysZ && !hasTrayImg)
            //            || (!_isDrawingUnitlessTraysZ && hasTrayImg))
            //        {
            //            return true;
            //        }
            //    }
            //}

            //return false;
        }

        /// <summary>
        /// Returns true if we have a tray image available.
        /// </summary>
        /// <param name="blockType"></param>
        /// <returns></returns>
        private Boolean HasTrayImage(PlanogramPositionBlockType blockType)
        {
            if (this.PlanPosition == null) return false;

            //always return false if images are off as we don't want to trigger a load by accident.
            if (!_settingsController.ShowProductImages) return false;


            BoxFaceType face = _visibleFaces[blockType];

            if ((face & BoxFaceType.Front) == BoxFaceType.Front)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Front)) return true;
            }
            if ((face & BoxFaceType.Back) == BoxFaceType.Back)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Back)) return true;
            }
            if ((face & BoxFaceType.Top) == BoxFaceType.Top)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Top)) return true;
            }
            if ((face & BoxFaceType.Bottom) == BoxFaceType.Bottom)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Bottom)) return true;
            }
            if ((face & BoxFaceType.Left) == BoxFaceType.Left)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Left)) return true;
            }
            if ((face & BoxFaceType.Right) == BoxFaceType.Right)
            {
                if (this.PlanPosition.IsTrayImage(blockType, PlanogramProductFaceType.Right)) return true;
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Returns the highlight colour of this position.
        /// </summary>
        /// <returns></returns>
        private ModelColour? GetHighlightColour()
        {
            var highlights = _settingsController.PositionHighlights;
            if (highlights == null) return null;

            Int32 highlightColour;
            if (highlights.TryGetValue(this.PlanPosition.Id, out highlightColour))
            {
                ModelColour returnValue = ModelColour.IntToColor(highlightColour);
                returnValue.Alpha = 255; //force alpha
                return returnValue;
            }

            //[V8-29362] return white if the position could not be found among the highlight colours.
            // it may be that it has been filtered out.
            return ModelColour.White;
        }

        /// <summary>
        /// Returns the label text for this position.
        /// </summary>
        public String GetLabelText()
        {
            var labels = _settingsController.PositionLabelText;
            if (labels == null) return null;

            String text = null;
            labels.TryGetValue(this.PlanPosition.Id, out text);
            return text;
        }

        /// <summary>
        /// Returns the corresponding block type for the given part type.
        /// </summary>
        /// <param name="partType"></param>
        /// <returns></returns>
        private static PlanogramPositionBlockType ToBlockType(PlanPosition3DPartType partType)
        {
            switch (partType)
            {
                default:
                case PlanPosition3DPartType.MainBlock: return PlanogramPositionBlockType.Main;
                case PlanPosition3DPartType.XBlock: return PlanogramPositionBlockType.X;
                case PlanPosition3DPartType.YBlock: return PlanogramPositionBlockType.Y;
                case PlanPosition3DPartType.ZBlock: return PlanogramPositionBlockType.Z;
            }
        }

        /// <summary>
        /// Returns true if the position is using the specified block.
        /// </summary>
        /// <param name="blockType"></param>
        /// <returns></returns>
        private Boolean HasBlock(PlanogramPositionBlockType blockType)
        {
            switch (blockType)
            {
                default:
                case PlanogramPositionBlockType.Main: return true;

                case PlanogramPositionBlockType.X:
                    return (this.PlanPosition.FacingsXHigh > 0 && this.PlanPosition.FacingsXWide > 0 && this.PlanPosition.FacingsXDeep > 0);

                case PlanogramPositionBlockType.Y:
                    return (this.PlanPosition.FacingsYHigh > 0 && this.PlanPosition.FacingsYWide > 0 && this.PlanPosition.FacingsYDeep > 0);

                case PlanogramPositionBlockType.Z:
                    return (this.PlanPosition.FacingsZHigh > 0 && this.PlanPosition.FacingsZWide > 0 && this.PlanPosition.FacingsZDeep > 0);
            }
        }

        #region Visible Faces

        /// <summary>
        /// Updates the values of _visibleFaces
        /// </summary>
        /// <returns></returns>
        private Boolean UpdateVisibleFaces()
        {
            if (_settingsController.CameraType == CameraViewType.Perspective
                || this.ParentModelData == null)
                return false;


            Boolean hasChanged = false;

            BoxFaceType mainFaces = BoxFaceType.All;
            BoxFaceType xFaces = BoxFaceType.All;
            BoxFaceType yFaces = BoxFaceType.All;
            BoxFaceType zFaces = BoxFaceType.All;


            if (!HasRotation(this.ParentModelData.WorldTransform))
            {
                if (_settingsController.CameraType == CameraViewType.Front
                || _settingsController.CameraType == CameraViewType.Design)
                {
                    mainFaces = GetFace(this.PlanPosition.UnitOrientationType);
                    xFaces = GetFace(this.PlanPosition.UnitXOrientationType);
                    yFaces = GetFace(this.PlanPosition.UnitYOrientationType);
                    zFaces = GetFace(this.PlanPosition.UnitZOrientationType);
                }
            }


            //check if any value has changed.
            if (mainFaces != _visibleFaces[PlanogramPositionBlockType.Main])
            {
                _visibleFaces[PlanogramPositionBlockType.Main] = mainFaces;
                hasChanged = true;
            }
            if (xFaces != _visibleFaces[PlanogramPositionBlockType.X])
            {
                _visibleFaces[PlanogramPositionBlockType.X] = xFaces;

                if (this.PlanPosition.FacingsXHigh > 0 && this.PlanPosition.FacingsXWide > 0 && this.PlanPosition.FacingsXDeep > 0)
                    hasChanged = true;
            }
            if (yFaces != _visibleFaces[PlanogramPositionBlockType.Y])
            {
                _visibleFaces[PlanogramPositionBlockType.Y] = yFaces;

                if (this.PlanPosition.FacingsYHigh > 0 && this.PlanPosition.FacingsYWide > 0 && this.PlanPosition.FacingsYDeep > 0)
                    hasChanged = true;
            }
            if (zFaces != _visibleFaces[PlanogramPositionBlockType.Z])
            {
                _visibleFaces[PlanogramPositionBlockType.Z] = zFaces;

                if (this.PlanPosition.FacingsZHigh > 0 && this.PlanPosition.FacingsZWide > 0 && this.PlanPosition.FacingsZDeep > 0)
                    hasChanged = true;
            }

            return hasChanged;
        }

        /// <summary>
        /// Returns which box face will be displayed wjem the product is at the given orientation.
        /// </summary>
        private static BoxFaceType GetFace(PlanogramProductOrientationType orientation)
        {
            switch (orientation)
            {
                default:
                case PlanogramProductOrientationType.Front0:
                case PlanogramProductOrientationType.Front180:
                case PlanogramProductOrientationType.Front270:
                case PlanogramProductOrientationType.Front90:
                    return BoxFaceType.Front;


                case PlanogramProductOrientationType.Back0:
                case PlanogramProductOrientationType.Back180:
                case PlanogramProductOrientationType.Back270:
                case PlanogramProductOrientationType.Back90:
                    return BoxFaceType.Back;

                case PlanogramProductOrientationType.Bottom0:
                case PlanogramProductOrientationType.Bottom180:
                case PlanogramProductOrientationType.Bottom270:
                case PlanogramProductOrientationType.Bottom90:
                    return BoxFaceType.Bottom;

                case PlanogramProductOrientationType.Left0:
                case PlanogramProductOrientationType.Left180:
                case PlanogramProductOrientationType.Left270:
                case PlanogramProductOrientationType.Left90:
                    return BoxFaceType.Left;

                case PlanogramProductOrientationType.Right0:
                case PlanogramProductOrientationType.Right180:
                case PlanogramProductOrientationType.Right270:
                case PlanogramProductOrientationType.Right90:
                    return BoxFaceType.Right;

                case PlanogramProductOrientationType.Top0:
                case PlanogramProductOrientationType.Top180:
                case PlanogramProductOrientationType.Top270:
                case PlanogramProductOrientationType.Top90:
                    return BoxFaceType.Top;


            }
        }

        /// <summary>
        /// Returns a flag of which product faces will be visible to the camera.
        /// </summary>
        private BoxFaceType GetVisibleFaces()
        {
            BoxFaceType face = _visibleFaces[PlanogramPositionBlockType.Main];

            if (HasBlock(PlanogramPositionBlockType.X))
            {
                face = face | _visibleFaces[PlanogramPositionBlockType.X];
            }
            if (HasBlock(PlanogramPositionBlockType.Y))
            {
                face = face | _visibleFaces[PlanogramPositionBlockType.Y];
            }
            if (HasBlock(PlanogramPositionBlockType.Z))
            {
                face = face | _visibleFaces[PlanogramPositionBlockType.Z];
            }

            return face;
        }

        /// <summary>
        /// Reduces the number of tiles to be produced according to the visiblity of faces.
        /// i.e Stops tiles being rendered when they cannot be seen anyway.
        /// </summary>
        /// <param name="blockData"></param>
        /// <param name="blockType"></param>
        private void OptimizeTilesForVisibleFaces(TiledBoxModelPart3DData blockData, PlanogramPositionBlockType blockType)
        {
            //cannot optimize perspective camera.
            if (_settingsController.CameraType == CameraViewType.Perspective) return;

            //for now dont optimize if the parent has any rotation
            if (this.ParentModelData != null && HasRotation(this.ParentModelData.WorldTransform)) return;


            //start by only doing front/design camera and front facing for now.
            if (_settingsController.CameraType == CameraViewType.Front
                || _settingsController.CameraType == CameraViewType.Design
                && _visibleFaces[blockType] == BoxFaceType.Front
                && blockData.Tile.Deep > 1)
            {
                Int32 removedTiles = blockData.Tile.Deep - 1;
                blockData.Tile = new WideHighDeepValue(blockData.Tile.Wide, blockData.Tile.High, 1);

                //move the position start forward by the removed tiles
                blockData.Position = new PointValue()
                {
                    X = blockData.Position.X,
                    Y = blockData.Position.Y,
                    Z = blockData.Position.Z + (removedTiles * blockData.TileSize.Depth)
                };

            }

            //TODO: other camera types and facings.
        }

        #endregion

        #region OnChanged Methods

        /// <summary>
        /// Called whenever an image property change fires.
        /// </summary>
        private void OnImagesChanged(Boolean onlyWhenShowing = true)
        {
            if (ShouldRedrawForTrayImages())
            {
                OnPartChanged();
                return;
            }


            //if (onlyWhenShowing && !this.ShowProductImages) return;

            OnMaterialChanged();

            //[V8-32598] If the label availabel reload that too
            // as it could be affected
            if (this.ProductLabel != null
                && !this.ProductLabel.ShowOverImages)
            {
                OnLabelChanged();
            }
        }

        /// <summary>
        /// Flags that the model part needs updating
        /// </summary>
        private void OnPartChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPartChanged = false;
                LoadPositionModels();
            }
            else
            {
                //flag the change
                _isPartChanged = true;
            }
        }

        /// <summary>
        /// Flags that the model material needs updating
        /// </summary>
        private void OnMaterialChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isMaterialChanged = false;
                UpdateMaterial();
            }
            else
            {
                //flag the change
                _isMaterialChanged = true;
            }
        }

        /// <summary>
        /// Flags that the model placement needs updating.
        /// </summary>
        private void OnPlacementChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        /// <summary>
        /// Flags that the model visibility needs updating.
        /// </summary>
        private void OnVisibilityChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isVisibilityChanged = false;
                UpdateVisibility();
            }
            else
            {
                //flag the change
                _isVisibilityChanged = true;
            }
        }

        /// <summary>
        /// Flags that the model pegs need updating
        /// </summary>
        private void OnPegsChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPegsChanged = false;
                DrawPegs();
            }
            else
            {
                //flag the change
                _isPegsChanged = true;
            }
        }

        /// <summary>
        /// Flags that the model label needs updating
        /// </summary>
        private void OnLabelChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isLabelChanged = false;
                UpdateProductLabel();
            }
            else
            {
                //flag the change
                _isLabelChanged = true;
            }
        }

        #endregion

        #endregion

        #region IPlanPart3DData Members

        Object IPlanPart3DData.PlanPart
        {
            get { return _planPosition; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;

            //start an edit on the label to prevent it from loading.
            if (_label != null) _label.BeginEdit();

        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;
            if (!_isUpdating)
            {
                if (_label != null && _label.IsEditing) _label.EndEdit();
                EndUpdate();
            }
        }

        #endregion

        #region IUpdateable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isPartChanged)
            {
                OnPartChanged();
            }

            if (_isPegsChanged)
            {
                OnPegsChanged();
            }

            if (_isLabelChanged)
            {
                OnLabelChanged();
            }


            if (_isMaterialChanged)
            {
                OnMaterialChanged();
            }

            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }

            if (_isVisibilityChanged)
            {
                OnVisibilityChanged();
            }

        }

        private void OnIsUpdatingChanged(Boolean newValue)
        {
            if (newValue)
            {
                //if the value
                if (!_isUpdating)
                {
                    BeginUpdate();
                }
            }
            else
            {
                if (!this.PlanPosition.IsUpdating && !this.PlanProduct.IsUpdating
                    && _isUpdating)
                {
                    EndUpdate();
                }
            }
        }

        #endregion

        #region IDisposable Members

        private void UnsubscribeHandles()
        {
            _planPosition.PropertyChanged -= PlanPosition_PropertyChanged;
            _planProduct.PropertyChanged -= PlanProduct_PropertyChanged;
            _settingsController.PropertyChanged -= SettingsController_PropertyChanged;
        }

        protected override void OnFreeze()
        {
            base.OnFreeze();
            UnsubscribeHandles();
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                UnsubscribeHandles();

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        public static ModelMaterial3D.FillPatternType ToFillPatternType(PlanogramProductFillPatternType prodFillPattern)
        {
            switch (prodFillPattern)
            {
                case PlanogramProductFillPatternType.Solid: return ModelMaterial3D.FillPatternType.Solid;

                case PlanogramProductFillPatternType.Border: return ModelMaterial3D.FillPatternType.Border;

                case PlanogramProductFillPatternType.Crosshatch: return ModelMaterial3D.FillPatternType.Crosshatch;

                case PlanogramProductFillPatternType.DiagonalDown: return ModelMaterial3D.FillPatternType.DiagonalDown;

                case PlanogramProductFillPatternType.DiagonalUp: return ModelMaterial3D.FillPatternType.DiagonalUp;

                case PlanogramProductFillPatternType.Horizontal: return ModelMaterial3D.FillPatternType.Horizontal;

                case PlanogramProductFillPatternType.Vertical: return ModelMaterial3D.FillPatternType.Vertical;

                case PlanogramProductFillPatternType.Dotted: return ModelMaterial3D.FillPatternType.Dotted;

                default:
                    Debug.Fail("Product Fill Pattern type not handled");
                    return ModelMaterial3D.FillPatternType.Solid;
            }
        }

        #endregion

    }
}
