﻿// Copyright © Galleria RTS Ltd 2016
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members required to render a planogram
    /// </summary>
    public interface IPlanRenderable : INotifyPropertyChanged
    {
        Single Height { get; }
        Single Width { get; }
        Single Depth { get; }

        event NotifyCollectionChangedEventHandler FixturesCollectionChanged;
        IEnumerable<IPlanFixtureRenderable> Fixtures { get; }

    }
}
