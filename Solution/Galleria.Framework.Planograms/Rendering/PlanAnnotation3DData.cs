﻿// Copyright © Galleria RTS Ltd 2016


using Galleria.Framework.DataStructures;
using Galleria.Framework.Rendering;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Represents ModelConstruct3DData used to create a PlanogramAnnotation.
    /// </summary>
    public sealed class PlanAnnotation3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        private IPlanRenderSettings _settingsController;
        private IPlanAnnotationRenderable _annotation;

        private ModelConstructPart3DData _partData;
        private ModelLabel3DData _labelData;

        private Boolean _suppressPropertyChanged;
        private Boolean _isSelected = false;
        private Boolean _setIsVisible = true;
        private Boolean _isPlacementChanged;
        private Boolean _isPartsChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the IPlanogramAnnotation source for this data
        /// </summary>
        public IPlanAnnotationRenderable Annotation
        {
            get { return _annotation; }
        }

        /// <summary>
        /// Gets/Sets whether this model is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");

                    OnPartsChanged();
                }
            }
        }

        public new Boolean IsVisible
        {
            get { return base.IsVisible; }
            set
            {
                _setIsVisible = value;
                UpdateVisibility();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="annotationView">The source IPlanogramAnnotation</param>
        /// <param name="parentPlan3D">The parent Plan3DData</param>
        public PlanAnnotation3DData(IPlanAnnotationRenderable annotationView, IPlanRenderSettings settingsController)
        {
            _annotation = annotationView;
            _annotation.PropertyChanged += Annotation_PropertyChanged;

            _settingsController = settingsController;
            _settingsController.PropertyChanged += SettingsController_PropertyChanged;

            OnPartsChanged();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on source IPlanogramAnnotation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Annotation_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!_suppressPropertyChanged)
            {
                _suppressPropertyChanged = true;

                switch (e.PropertyName)
                {
                    case "X":
                    case "Y":
                    case "Z":
                    case "Slope":
                    case "Angle":
                    case "Roll":
                        OnPlacementChanged();
                        break;

                    case "Height":
                    case "Width":
                    case "Depth":
                    case "Text":
                    case "BackgroundColour":
                    case "BorderColour":
                    case "BorderThickness":
                    case "FontColour":
                    case "FontName":
                    case "FontSize":
                    case "CanReduceFontToFit":
                        OnPartsChanged();
                        break;
                }

                _suppressPropertyChanged = false;
            }
        }


        /// <summary>
        /// Called when a property changes on the parent plangram 3d data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                //Although this is show positions we shall set annotations using
                // the same setting for now.
                //V8-32636 - Updated to use new ShowAnnotations
                case PlanRenderSettings.ShowAnnotationsPropertyName:
                    this.IsVisible = _settingsController.ShowAnnotations;
                    break;

                case PlanRenderSettings.SelectionColourPropertyName:
                    if (this.IsSelected && !_settingsController.SuppressPerformanceIntensiveUpdates) ReloadParts();
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the position of this model.
        /// </summary>
        private void UpdateModelPlacement()
        {
            _suppressPropertyChanged = true;

            IPlanAnnotationRenderable annoView = this.Annotation;

            //in the data the z position is given as an offset from 
            //the front of the parent subcomponent.
            Single z = annoView.Z;
            //if (annoView.ParentSubComponent != null)
            //{
            //    z = annoView.ParentSubComponent.Depth - annoView.Z
            //        - ((annoView.Depth > 0) ? annoView.Depth : 1);
            //}


            this.Position = new PointValue(annoView.X, annoView.Y, z);


            //nb - rotation is not valid for this.


            _suppressPropertyChanged = false;
        }

        /// <summary>
        /// Updates the parts of this model.
        /// </summary>
        private void ReloadParts()
        {
            IPlanAnnotationRenderable anno = this.Annotation;

            if (_labelData == null)
            {
                _labelData = new ModelLabel3DData();
                _labelData.ShowBackMaterial = true;
                AddLabel(_labelData);
            }
            if (_partData == null)
            {
                _partData = new BoxModelPart3DData(this);
                AddPart(_partData);
            }


            BoxModelPart3DData partData = _partData as BoxModelPart3DData;
            partData.BeginEdit();
            {
                partData.Size = new WidthHeightDepthValue
                {
                    Width = (anno.Width > 0) ? anno.Width : 1,
                    Height = (anno.Height > 0) ? anno.Height : 1,
                    Depth = (anno.Depth > 0) ? anno.Depth : 1,
                };

                //Ensure background is opaque to keep selection easy for the user.
                partData.SetMaterial(ModelMaterial3D.CreateMaterial(ModelColour.NewColour(anno.BackgroundColour, (Byte)255)));

                if (this.IsSelected)
                {
                    Double selectionLineThickness = (_settingsController.SelectionLineThickness > 0) ?
                        _settingsController.SelectionLineThickness : 1;

                    partData.LineMaterial = ModelMaterial3D.CreateMaterial(_settingsController.SelectionColour);
                    partData.LineThickness =
                        (anno.BorderThickness > selectionLineThickness) ?
                        anno.BorderThickness : selectionLineThickness;
                }
                else
                {
                    partData.LineMaterial = ModelMaterial3D.CreateMaterial(ModelColour.IntToColor(anno.BorderColour));
                    partData.LineThickness = anno.BorderThickness;
                }

            }
            partData.EndEdit();


            ModelLabel3DData label = _labelData;
            label.BeginEdit();
            {

                this.Size = _partData.Size;

                label.MaxSize =
                    new WidthHeightDepthValue()
                    {
                        Width = _partData.Size.Width,
                        Height = _partData.Size.Height,
                        Depth = _partData.Size.Depth
                    };

                label.Text = anno.Text;
                label.TextColour = ModelColour.IntToColor(anno.FontColour);
                label.FontName = anno.FontName;
                label.FontSize = (anno.FontSize > 0) ? anno.FontSize : 8;

                //Use transparent so we don't get overlap issues with the model borders.
                //So we rely on the model part providing the colour.
                label.BackgroundColour = ModelColour.Transparent;

                label.ShrinkFontToFit = anno.CanReduceFontToFit;
                label.BorderThickness = 0;

                label.RotateFontToFit = false;
                label.Alignment = ModelTextAlignment.Left;
                label.MinFontSize = 0.1;

                //Use border thickness as a margin instead of changing the max size value.
                //This prevents the positioning of the text from messing up due to the
                //centre being incorrect.
                label.Margin = anno.BorderThickness;

                label.TextBoxFontBold = false;
                label.TextBoxFontItalic = false;
                label.TextBoxFontUnderlined = false;
                label.LabelHorizontalPlacement = ModelHorizontalAlignment.Center;
                label.LabelVerticalPlacement = ModelVerticalAlignment.Center;
                label.TextDirection = ModelTextDirection.Horizontal;

                label.AttachFace = ModelLabelAttachFace.Front;
            }
            label.EndEdit();

            OnPlacementChanged();
            UpdateVisibility();
        }

        /// <summary>
        /// Updates the visibility of this model.
        /// </summary>
        private void UpdateVisibility()
        {
            Boolean newIsVisible = true;

            //V8-32636 - Updated to use new ShowAnnotations not ShowPositions
            if (!_settingsController.ShowAnnotations)
            {
                newIsVisible = false;
            }

            if (newIsVisible)
            {
                newIsVisible = _setIsVisible;
            }

            base.IsVisible = newIsVisible;
        }

        #endregion

        #region IUpdatable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isPartsChanged)
            {
                OnPartsChanged();
            }
            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }
        }

        private void OnPlacementChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        private void OnPartsChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPartsChanged = false;
                ReloadParts();
            }
            else
            {
                //flag the change
                _isPartsChanged = true;
            }
        }

        #endregion

        #region IPlanPart3DData Members

        Object IPlanPart3DData.PlanPart
        {
            get { return _annotation; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;

            //if we arent flagged as updating then its ok
            // to trigger a refresh.
            if (!_isUpdating) EndUpdate();

            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.UnpauseUpdates();
            }
        }

        #endregion

        #region IDisposable Members

        protected override void OnFreeze()
        {
            base.OnFreeze();

            _annotation.PropertyChanged -= Annotation_PropertyChanged;
            _settingsController.PropertyChanged -= SettingsController_PropertyChanged;
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                _annotation.PropertyChanged -= Annotation_PropertyChanged;
                _settingsController.PropertyChanged -= SettingsController_PropertyChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
