﻿// Copyright © Galleria RTS Ltd 2016
using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members required to render a planogram fixture.
    /// </summary>
    public interface IPlanFixtureRenderable : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets the id of this bay.
        /// </summary>
        Object PlanogramFixtureItemId { get; }

        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }

        event NotifyCollectionChangedEventHandler AssembliesCollectionChanged;
        IEnumerable<IPlanAssemblyRenderable> Assemblies { get; }

        event NotifyCollectionChangedEventHandler ComponentsCollectionChanged;
        IEnumerable<IPlanComponentRenderable> Components { get; }

        event NotifyCollectionChangedEventHandler AnnotationsCollectionChanged;
        IEnumerable<IPlanAnnotationRenderable> Annotations { get; }
    }
}
