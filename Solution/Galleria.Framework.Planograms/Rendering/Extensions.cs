﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rendering;

namespace Galleria.Framework.Planograms.Rendering
{
    public static class Extensions
    {
        /// <summary>
        /// Converts the given PlanogramLabelHorizontalAlignment to a corresponding ModelHorizontalAlignment
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ModelHorizontalAlignment ToModelHorizontalAlignment(this PlanogramLabelHorizontalAlignment val)
        {
            switch (val)
            {
                case PlanogramLabelHorizontalAlignment.Center: return ModelHorizontalAlignment.Center;
                case PlanogramLabelHorizontalAlignment.Left: return ModelHorizontalAlignment.Left;
                case PlanogramLabelHorizontalAlignment.Right: return ModelHorizontalAlignment.Right;
                default: return ModelHorizontalAlignment.Center;
            }
        }

        /// <summary>
        /// Converts the given PlanogramLabelVerticalAlignment to a corresponding ModelVerticalAlignment
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ModelVerticalAlignment ToModelVerticalAlignment(this PlanogramLabelVerticalAlignment val)
        {
            switch (val)
            {
                case PlanogramLabelVerticalAlignment.Bottom: return ModelVerticalAlignment.Bottom;
                case PlanogramLabelVerticalAlignment.Center: return ModelVerticalAlignment.Center;
                case PlanogramLabelVerticalAlignment.Top: return ModelVerticalAlignment.Top;

                default: return ModelVerticalAlignment.Center;
            }
        }

        /// <summary>
        /// Converts the given PlanogramLabelTextAlignment to a corresponding ModelTextAlignment
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ModelTextAlignment ToModelTextAlignment(this PlanogramLabelTextAlignment val)
        {
            switch (val)
            {
                case PlanogramLabelTextAlignment.Center: return ModelTextAlignment.Center;
                case PlanogramLabelTextAlignment.Justified: return ModelTextAlignment.Justified;
                case PlanogramLabelTextAlignment.Left: return ModelTextAlignment.Left;
                case PlanogramLabelTextAlignment.Right: return ModelTextAlignment.Right;

                default: return ModelTextAlignment.Left;
            }
        }

        /// <summary>
        /// Converts the given PlanogramLabelTextDirection to a corresponding ModelTextDirection
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ModelTextDirection ToModelTextDirection(this PlanogramLabelTextDirection val)
        {
            switch (val)
            {
                case PlanogramLabelTextDirection.Horizontal: return ModelTextDirection.Horizontal;
                case PlanogramLabelTextDirection.Vertical: return ModelTextDirection.Vertical;
                default: return ModelTextDirection.Horizontal;
            }
        }

        /// <summary>
        /// Converts the given PlanogramLabelTextWrapping to a corresponding ModelTextWrapping
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ModelTextWrapping ToModelTextWrapping(this PlanogramLabelTextWrapping val)
        {
            switch (val)
            {
                case PlanogramLabelTextWrapping.Letter: return ModelTextWrapping.Letter;
                case PlanogramLabelTextWrapping.None: return ModelTextWrapping.None;
                case PlanogramLabelTextWrapping.Word: return ModelTextWrapping.Word;

                default: return ModelTextWrapping.Letter;
            }
        }


    }
}
