﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// The data required to render a planogram component in 3d.
    /// </summary>
    public sealed class PlanComponent3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        private readonly IPlanRenderSettings _settingsController;
        private readonly IPlanComponentRenderable _component;

        private readonly Dictionary<IPlanSubComponentRenderable, PlanSubComponent3DData> _modelsDict = new Dictionary<IPlanSubComponentRenderable, PlanSubComponent3DData>();
        private readonly Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData> _annoModelsDict = new Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData>();

        private ModelLabel3DData _label;

        private Boolean _isPlacementChanged; //flag to indicate that the placement needs updating.
        private Boolean _isLabelChanged; //flag to indicate that the label need updating.

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source component view.
        /// </summary>
        public IPlanComponentRenderable Component
        {
            get { return _component; }
        }

        /// <summary>
        /// Returns the planogram 3d data settings.
        /// </summary>
        public IPlanRenderSettings Settings
        {
            get { return _settingsController; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="componentView">the source component</param>
        /// <param name="settingsController">the settings controller to use</param>
        public PlanComponent3DData(IPlanComponentRenderable componentView, IPlanRenderSettings settingsController)
        {
            _component = componentView;
            _settingsController = settingsController;
            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="componentView">the source component</param>
        /// <param name="settingsController">the settings controller to use</param>
        internal PlanComponent3DData(ModelConstruct3DData parentData, IPlanComponentRenderable componentView, IPlanRenderSettings settingsController)
        {
            this.ParentModelData = parentData;
            _component = componentView;
            _settingsController = settingsController;
            Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to property changes on the main component view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Component_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    OnPlacementChanged();
                    break;

                case "Height":
                case "Width":
                case "Depth":
                    {
                        this.Size = new WidthHeightDepthValue
                        {
                            Width = this.Component.Width,
                            Height = this.Component.Height,
                            Depth = this.Component.Depth
                        };

                        if (_label != null)
                        {
                            _label.BeginEdit();
                            _label.MaxSize = this.Size;
                            _label.EndEdit();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when the subcomponents collection on the linked component changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Component_SubComponentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanSubComponentRenderable view in e.NewItems)
                        {
                            AddSubComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanSubComponentRenderable view in e.OldItems)
                        {
                            RemoveSubComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanSubComponentRenderable> views = this.Component.SubComponents.ToList();

                        //remove as required
                        foreach (IPlanSubComponentRenderable modelledView in _modelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveSubComponentModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanSubComponentRenderable v in views)
                        {
                            AddSubComponentModel(v);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the annotations collection changes on the source IPlanogramComponentRenderable
        /// </summary>
        private void Component_AnnotationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanAnnotationRenderable view in e.NewItems)
                        {
                            AddAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanAnnotationRenderable view in e.OldItems)
                        {
                            RemoveAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanAnnotationRenderable> views = this.Component.Annotations.ToList();

                        //remove as required
                        foreach (IPlanAnnotationRenderable modelledView in _annoModelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveAnnotationModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanAnnotationRenderable v in views)
                        {
                            AddAnnotationModel(v);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called when a property changes on the parent plangram 3d data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case PlanRenderSettings.FixtureLabelPropertyName:
                case PlanRenderSettings.FixtureLabelTextPropertyName:
                    OnLabelChanged();
                    break;

                case PlanRenderSettings.RotateTopDownComponentsPropertyName:
                    OnPlacementChanged();
                    break;

            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initialization actions.
        /// </summary>
        private void Initialize()
        {
            _component.PropertyChanged += Component_PropertyChanged;
            _component.SubComponentsCollectionChanged += Component_SubComponentsCollectionChanged;
            _component.AnnotationsCollectionChanged += Component_AnnotationsCollectionChanged;

            _settingsController.PropertyChanged += SettingsController_PropertyChanged;

            this.Size = new WidthHeightDepthValue
            {
                Width = _component.Width,
                Height = _component.Height,
                Depth = _component.Depth
            };

            OnPlacementChanged();


            //Add child Subcomponents
            foreach (var subComponent in _component.SubComponents)
            {
                AddSubComponentModel(subComponent);
            }

            //Add child annotations
            foreach (var anno in _component.Annotations)
            {
                AddAnnotationModel(anno);
            }

            OnLabelChanged();
        }

        /// <summary>
        /// Updates the placement of this model.
        /// </summary>
        private void UpdateModelPlacement()
        {
            IPlanComponentRenderable componentView = this.Component;

            this.Position =
                new PointValue(componentView.X, componentView.Y, componentView.Z);

            this.Rotation =
                new RotationValue(componentView.Angle, componentView.Slope, componentView.Roll);


            //If this is flagged as top down then flip it if the view setting is enabled.
            if (this.Settings.RotateTopDownComponents && this.Component.IsMerchandisedTopDown)
            {
                this.Rotation =
                     new RotationValue
                     {
                         Angle = this.Rotation.Angle,
                         Slope = Convert.ToSingle(this.Rotation.Slope - (Math.PI / 2)),
                         Roll = this.Rotation.Roll
                     };

                this.Position =
                    new PointValue()
                    {
                        X = componentView.X,
                        Y = componentView.Y + componentView.Height, //TODO - work this out properly.
                        Z = componentView.Z
                    };

            }
        }

        /// <summary>
        /// Updates the label for this component.
        /// </summary>
        private void UpdateFixtureLabel()
        {
            IPlanogramLabel labelSetting = this.Settings.FixtureLabel;

            //get the text to be displayed.
            String text = null;
            if (labelSetting != null && this.IsVisible)
            {
                //turn off the label if we are showing any images
                // and the label setting is to not show it over the top.
                if (!labelSetting.ShowOverImages
                    && _settingsController.ShowFixtureImages
                    && EnumerateSubComponentModels().Any(s => s.HasAvailableImages()))
                {
                    text = null;
                }
                else
                {
                    text = GetLabelText();
                }
            }


            if (!String.IsNullOrEmpty(text))
            {
                //add the label if required.
                if (_label == null)
                {
                    _label = new ModelLabel3DData();
                    AddLabel(_label);
                }


                ModelLabel3DData labelModel = _label;
                labelModel.BeginEdit();
                {
                    labelModel.IsAsync = _settingsController.CanRenderAsync;
                    labelModel.MaxSize = this.Size;
                    labelModel.TextColour = ModelColour.IntToColor(labelSetting.TextColour);
                    labelModel.BackgroundColour = ModelColour.IntToColor(labelSetting.BackgroundColour);
                    labelModel.BorderColour = ModelColour.IntToColor(labelSetting.BorderColour);
                    labelModel.BorderThickness = labelSetting.BorderThickness;
                    labelModel.FontSize = labelSetting.FontSize;
                    labelModel.RotateFontToFit = labelSetting.IsRotateToFitOn;
                    labelModel.ShrinkFontToFit = labelSetting.IsShrinkToFitOn;
                    labelModel.FontName = labelSetting.Font;
                    labelModel.Text = text;
                    labelModel.TextBoxFontBold = labelSetting.TextBoxFontBold;
                    labelModel.TextBoxFontItalic = labelSetting.TextBoxFontItalic;
                    labelModel.TextBoxFontUnderlined = labelSetting.TextBoxFontUnderlined;
                    labelModel.LabelHorizontalPlacement = labelSetting.LabelHorizontalPlacement.ToModelHorizontalAlignment();
                    labelModel.LabelVerticalPlacement = labelSetting.LabelVerticalPlacement.ToModelVerticalAlignment();
                    labelModel.Alignment = labelSetting.TextBoxTextAlignment.ToModelTextAlignment();
                    labelModel.TextDirection = labelSetting.TextDirection.ToModelTextDirection();
                    labelModel.TextWrapping = labelSetting.TextWrapping.ToModelTextWrapping();
                    labelModel.BackgroundTransparency = labelSetting.BackgroundTransparency;
                    labelModel.MinFontSize = 0.1;
                    labelModel.Margin = 0.1;
                    labelModel.AttachFace = ModelLabelAttachFace.Auto;
                }
                labelModel.EndEdit();
            }
            else
            {
                //remove the label
                if (_label != null)
                {
                    RemoveLabel(_label);
                    _label = null;
                }
            }
        }

        /// <summary>
        /// Adds a new child for the given Subcomponent view
        /// </summary>
        /// <param name="assemblyView"></param>
        private void AddSubComponentModel(IPlanSubComponentRenderable view)
        {
            PlanSubComponent3DData model = new PlanSubComponent3DData(this, view, Settings);
            AddChild(model);
            _modelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the child model for the given Subcomponent view.
        /// </summary>
        /// <param name="assemblyView"></param>
        private void RemoveSubComponentModel(IPlanSubComponentRenderable view)
        {
            PlanSubComponent3DData model;

            if (_modelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _modelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Enumerates through child subcomponent data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanSubComponent3DData> EnumerateSubComponentModels()
        {
            foreach (var entry in _modelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Adds a new child for the given annotation view.
        /// </summary>
        /// <param name="view"></param>
        private void AddAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model = new PlanAnnotation3DData(view, _settingsController);
            AddChild(model);
            _annoModelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the model for the given view.
        /// </summary>
        /// <param name="view"></param>
        private void RemoveAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model;

            if (_annoModelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _annoModelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Enumerates through child annotation data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanAnnotation3DData> EnumerateAnnotationModels()
        {
            foreach (var entry in _annoModelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Returns the label text for this position.
        /// </summary>
        public String GetLabelText()
        {
            if (this.Component.Id == null) return null;

            var labels = _settingsController.FixtureLabelText;
            if (labels == null) return null;

            String text = null;
            labels.TryGetValue(this.Component.Id, out text);
            return text;
        }

        #endregion

        #region IPlanPart3DData Members

        Object IPlanPart3DData.PlanPart
        {
            get { return _component; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;

            //if we arent flagged as updating then its ok
            // to trigger a refresh.
            if (!_isUpdating) EndUpdate();

            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.UnpauseUpdates();
            }
        }


        #endregion

        #region IUpdateable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isLabelChanged)
            {
                OnLabelChanged();
            }


            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }

        }

        private void OnIsUpdatingChanged(Boolean newValue)
        {
            if (newValue)
            {
                //if the value
                if (!_isUpdating)
                {
                    BeginUpdate();
                }
            }
            else
            {
                if (!this.Component.IsUpdating && _isUpdating)
                {
                    EndUpdate();
                }
            }
        }

        #region OnChanged Methods

        private void OnPlacementChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        private void OnLabelChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isLabelChanged = false;
                UpdateFixtureLabel();
            }
            else
            {
                //flag the change
                _isLabelChanged = true;
            }
        }

        #endregion


        #endregion

        #region IDisposable Members

        protected override void OnFreeze()
        {
            base.OnFreeze();

            this.Component.PropertyChanged -= Component_PropertyChanged;
            this.Component.SubComponentsCollectionChanged -= Component_SubComponentsCollectionChanged;
            this.Component.AnnotationsCollectionChanged -= Component_AnnotationsCollectionChanged;
            this.Settings.PropertyChanged -= SettingsController_PropertyChanged;
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                this.Component.PropertyChanged -= Component_PropertyChanged;
                this.Component.SubComponentsCollectionChanged -= Component_SubComponentsCollectionChanged;
                this.Component.AnnotationsCollectionChanged -= Component_AnnotationsCollectionChanged;
                this.Settings.PropertyChanged -= SettingsController_PropertyChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
