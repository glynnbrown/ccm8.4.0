﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    public sealed class PlanAssembly3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        private readonly IPlanRenderSettings _settingsController;
        private readonly IPlanAssemblyRenderable _assembly;
        private readonly Dictionary<IPlanComponentRenderable, PlanComponent3DData> _modelsDict = new Dictionary<IPlanComponentRenderable, PlanComponent3DData>();
        private Boolean _isPlacementChanged;
        #endregion

        #region Properties

        public IPlanAssemblyRenderable Assembly
        {
            get { return _assembly; }
        }

        /// <summary>
        /// Returns the planogram 3d data settings.
        /// </summary>
        public IPlanRenderSettings Settings
        {
            get { return _settingsController; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="assemblyView">the source assembly</param>
        /// <param name="settingsController">the settings controller to use</param>
        public PlanAssembly3DData(IPlanAssemblyRenderable assemblyView, IPlanRenderSettings settingsController)
        {
            _settingsController = settingsController;
            _assembly = assemblyView;

            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="assemblyView">the source assembly</param>
        /// <param name="settingsController">the settings controller to use</param>
        internal PlanAssembly3DData(PlanFixture3DData parentData, IPlanAssemblyRenderable assemblyView, IPlanRenderSettings settingsController)
        {
            this.ParentModelData = parentData;
            _settingsController = settingsController;
            _assembly = assemblyView;

            Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to property changes on the main component view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Assembly_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    OnPlacementChanged();
                    break;

                case "Height":
                case "Width":
                case "Depth":
                    this.Size = new WidthHeightDepthValue
                    {
                        Width = this.Assembly.Width,
                        Height = this.Assembly.Height,
                        Depth = this.Assembly.Depth
                    };
                    break;
            }

        }

        /// <summary>
        /// Called when the component collection changes on the source assembly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Assembly_ComponentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanComponentRenderable view in e.NewItems)
                        {
                            AddComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanComponentRenderable view in e.OldItems)
                        {
                            RemoveComponentModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanComponentRenderable> views = this.Assembly.Components.ToList();

                        //remove as required
                        foreach (IPlanComponentRenderable modelledView in _modelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveComponentModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanComponentRenderable v in views)
                        {
                            AddComponentModel(v);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initialization actions.
        /// </summary>
        private void Initialize()
        {
            _assembly.PropertyChanged += Assembly_PropertyChanged;
            _assembly.ComponentsCollectionChanged += Assembly_ComponentsCollectionChanged;

            this.Size = new WidthHeightDepthValue
            {
                Width = _assembly.Width,
                Height = _assembly.Height,
                Depth = _assembly.Depth
            };

            OnPlacementChanged();


            //Add child components
            foreach (var component in _assembly.Components)
            {
                AddComponentModel(component);
            }
        }

        private void UpdateModelPlacement()
        {
            var part = this.Assembly;

            this.Position =
                new PointValue(part.X, part.Y, part.Z);

            this.Rotation =
                new RotationValue(part.Angle, part.Slope, part.Roll);
        }

        /// <summary>
        /// Adds a new child for the given component view
        /// </summary>
        /// <param name="assemblyView"></param>
        private void AddComponentModel(IPlanComponentRenderable view)
        {
            PlanComponent3DData model = new PlanComponent3DData(this, view, Settings);
            AddChild(model);
            _modelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the child model for the given component view.
        /// </summary>
        /// <param name="assemblyView"></param>
        private void RemoveComponentModel(IPlanComponentRenderable view)
        {
            PlanComponent3DData model;

            if (_modelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _modelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Enumerates through child component data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanComponent3DData> EnumerateComponentModels()
        {
            foreach (var entry in _modelsDict)
            {
                yield return entry.Value;
            }
        }

        #endregion

        #region IUpdatable Members

        private Boolean _isUpdating;

        private void BeginUpdate()
        {
            _isUpdating = true;
        }

        private void EndUpdate()
        {
            _isUpdating = false;

            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }
        }

        private void OnPlacementChanged()
        {
            if (!_isUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        #endregion

        #region IPlanPart3DData Members

        Object IPlanPart3DData.PlanPart
        {
            get { return _assembly; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;

            //if we arent flagged as updating then its ok
            // to trigger a refresh.
            if (!_isUpdating) EndUpdate();

            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.UnpauseUpdates();
            }
        }

        #endregion

        #region IDisposable Members

        protected override void OnFreeze()
        {
            base.OnFreeze();

            this.Assembly.PropertyChanged -= Assembly_PropertyChanged;
            this.Assembly.ComponentsCollectionChanged -= Assembly_ComponentsCollectionChanged;
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                this.Assembly.PropertyChanged -= Assembly_PropertyChanged;
                this.Assembly.ComponentsCollectionChanged -= Assembly_ComponentsCollectionChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
