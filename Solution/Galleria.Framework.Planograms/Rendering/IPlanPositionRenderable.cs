﻿#region header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM 8.3.2)
// CCM-18938 : M.Pettit
//  Added PegX2,PegY2, PegX3, PegY3 properties
#endregion
#endregion

using Galleria.Framework.Planograms.Model;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members required to render a planogram position.
    /// </summary>
    public interface IPlanPositionRenderable : INotifyPropertyChanged
    {
        /// <summary>
        /// The unqiue identifier 
        /// for the planogram position
        /// Used to fetch highlights from the dictionary etc.
        /// </summary>
        Object Id { get; }

        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The rotation angle
        /// </summary>
        Single Angle { get; }
        /// <summary>
        /// The rotation slope
        /// </summary>
        Single Slope { get; }
        /// <summary>
        /// The rotation roll
        /// </summary>
        Single Roll { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }



        IPlanProductRenderable Product { get; }
        IPlanSubComponentRenderable ParentSubComponent { get; }

        /// <summary>
        /// Returns true if this object is in the process of being updated
        /// Used for performance.
        /// </summary>
        Boolean IsUpdating { get; }

        #region Main Block

        /// <summary>
        /// The number of facings high in the main block
        /// </summary>
        Int16 FacingsHigh { get; }
        /// <summary>
        /// The number of facings wide in the main block
        /// </summary>
        Int16 FacingsWide { get; }
        /// <summary>
        /// The facings deep of the main block
        /// </summary>
        Int16 FacingsDeep { get; }
        /// <summary>
        /// The orientation type of the main block
        /// </summary>
        PlanogramProductOrientationType UnitOrientationType { get; }
        /// <summary>
        /// Returns true if the main block is a tray position.
        /// </summary>
        Boolean IsMerchandisedAsTrays { get; }
        /// <summary>
        /// The unorientated height of a single product unit
        /// </summary>
        Single UnitHeight { get; }
        /// <summary>
        /// The unorientated width of a single product unit
        /// </summary>
        Single UnitWidth { get; }
        /// <summary>
        /// The unorientated depth of a single product unit.
        /// </summary>
        Single UnitDepth { get; }


        Byte[] FrontImageData { get; }
        Byte[] BackImageData { get; }
        Byte[] TopImageData { get; }
        Byte[] BottomImageData { get; }
        Byte[] LeftImageData { get; }
        Byte[] RightImageData { get; }

        Single BlockMainStartX { get; }
        Single BlockMainStartY { get; }
        Single BlockMainStartZ { get; }


        #endregion

        #region X Block

        /// <summary>
        /// The number of facings high in the X block
        /// </summary>
        Int16 FacingsXHigh { get; }
        /// <summary>
        /// The number of facings wide in the X block
        /// </summary>
        Int16 FacingsXWide { get; }
        /// <summary>
        /// The number of facings deep in the X block
        /// </summary>
        Int16 FacingsXDeep { get; }
        /// <summary>
        /// The orientation of products in the X block.
        /// </summary>
        PlanogramProductOrientationType UnitXOrientationType { get; }
        /// <summary>
        /// Returns true if the products in the X block are merchandised as trays
        /// </summary>
        Boolean IsXMerchandisedAsTrays { get; }
        /// <summary>
        /// The unorientated height of units in the X block
        /// </summary>
        Single UnitXHeight { get; }
        /// <summary>
        /// The unorientated width of units in the X block
        /// </summary>
        Single UnitXWidth { get; }
        /// <summary>
        /// The unorientated depth of units in the X block.
        /// </summary>
        Single UnitXDepth { get; }
        /// <summary>
        /// Where the x block should be placed in relation to the main block.
        /// </summary>
        Boolean IsXPlacedLeft { get; }


        Byte[] FrontXImageData { get; }
        Byte[] BackXImageData { get; }
        Byte[] TopXImageData { get; }
        Byte[] BottomXImageData { get; }
        Byte[] LeftXImageData { get; }
        Byte[] RightXImageData { get; }

        Single BlockXStartX { get; }
        Single BlockXStartY { get; }
        Single BlockXStartZ { get; }


        #endregion

        #region Y Block

        /// <summary>
        /// The number of facings high in the Y block
        /// </summary>
        Int16 FacingsYHigh { get; }
        /// <summary>
        /// The number of facings wide in the Y block
        /// </summary>
        Int16 FacingsYWide { get; }
        /// <summary>
        /// The number of facings deep in the Y block
        /// </summary>
        Int16 FacingsYDeep { get; }
        /// <summary>
        /// The orientation of products in the Y block.
        /// </summary>
        PlanogramProductOrientationType UnitYOrientationType { get; }
        /// <summary>
        /// Returns true if the products in the Y block are merchandised as trays
        /// </summary>
        Boolean IsYMerchandisedAsTrays { get; }
        /// <summary>
        /// The unorientated height of units in the Y block
        /// </summary>
        Single UnitYHeight { get; }
        /// <summary>
        /// The unorientated width of units in the Y block
        /// </summary>
        Single UnitYWidth { get; }
        /// <summary>
        /// The unorientated depth of units in the Y block.
        /// </summary>
        Single UnitYDepth { get; }
        /// <summary>
        /// Where the y block should be placed in relation to the main block.
        /// </summary>
        Boolean IsYPlacedBottom { get; }

        Byte[] FrontYImageData { get; }
        Byte[] BackYImageData { get; }
        Byte[] TopYImageData { get; }
        Byte[] BottomYImageData { get; }
        Byte[] LeftYImageData { get; }
        Byte[] RightYImageData { get; }

        Single BlockYStartX { get; }
        Single BlockYStartY { get; }
        Single BlockYStartZ { get; }

        #endregion

        #region Z Block

        /// <summary>
        /// The number of facings high in the Z block
        /// </summary>
        Int16 FacingsZHigh { get; }
        /// <summary>
        /// The number of facings wide in the Z block
        /// </summary>
        Int16 FacingsZWide { get; }
        /// <summary>
        /// The number of facings deep in the Z block
        /// </summary>
        Int16 FacingsZDeep { get; }
        /// <summary>
        /// The orientation of products in the Z block.
        /// </summary>
        PlanogramProductOrientationType UnitZOrientationType { get; }
        /// <summary>
        /// Returns true if the products in the Z block are merchandised as trays
        /// </summary>
        Boolean IsZMerchandisedAsTrays { get; }
        /// <summary>
        /// The unorientated height of units in the Z block
        /// </summary>
        Single UnitZHeight { get; }
        /// <summary>
        /// The unorientated width of units in the Z block
        /// </summary>
        Single UnitZWidth { get; }
        /// <summary>
        /// The unorientated depth of units in the Z block.
        /// </summary>
        Single UnitZDepth { get; }
        /// <summary>
        /// Where the Z block should be placed in relation to the main block.
        /// </summary>
        Boolean IsZPlacedFront { get; }


        Byte[] FrontZImageData { get; }
        Byte[] BackZImageData { get; }
        Byte[] TopZImageData { get; }
        Byte[] BottomZImageData { get; }
        Byte[] LeftZImageData { get; }
        Byte[] RightZImageData { get; }


        Single BlockZStartX { get; }
        Single BlockZStartY { get; }
        Single BlockZStartZ { get; }

        #endregion

        //helper properties

        /// <summary>
        /// Returns true if the products of this position are 
        /// placed on pegs.
        /// </summary>
        Boolean IsOnPegs { get; }

        /// <summary>
        /// Gets/Sets whether this position is currently on a top down component
        /// </summary>
        Boolean IsOnTopDownComponent { get; }

        /// <summary>
        /// The amount of x spacing between facings.
        /// </summary>
        Single FacingSpaceX { get; }

        /// <summary>
        /// The amount of y spacing between facings.
        /// </summary>
        Single FacingSpaceY { get; }

        /// <summary>
        /// The amount of z spacing between facings.
        /// </summary>
        Single FacingSpaceZ { get; }

        /// <summary>
        /// The actual peg x to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegX { get; }

        /// <summary>
        /// The actual peg y to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegY { get; }

        /// <summary>
        /// The actual peg x to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegX2 { get; }

        /// <summary>
        /// The actual peg y to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegY2 { get; }

        /// <summary>
        /// The actual peg x to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegX3 { get; }

        /// <summary>
        /// The actual peg y to use for this position.
        /// If the product value was 0 then this has been defaulted.
        /// </summary>
        Single PegY3 { get; }


        #region Methods

        /// <summary>
        /// Returns true if the image applied to the given block and facing is
        /// a tray image.
        /// </summary>
        /// <param name="blockType"></param>
        /// <param name="faceType"></param>
        /// <returns></returns>
        Boolean IsTrayImage(PlanogramPositionBlockType blockType, PlanogramProductFaceType faceType);

        #endregion

    }
}
