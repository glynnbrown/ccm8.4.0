﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.Planograms.Interfaces;
using System;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Simple implementation of IPlanogramLabel
    /// </summary>
    public struct PlanLabel : IPlanogramLabel
    {
        public Int32 BackgroundColour { get; set; }
        public Int32 BorderColour { get; set; }
        public Int32 BorderThickness { get; set; }
        public String Text { get; set; }
        public Int32 TextColour { get; set; }
        public String Font { get; set; }
        public Int32 FontSize { get; set; }
        public Boolean IsRotateToFitOn { get; set; }
        public Boolean IsShrinkToFitOn { get; set; }
        public Boolean TextBoxFontBold { get; set; }
        public Boolean TextBoxFontItalic { get; set; }
        public Boolean TextBoxFontUnderlined { get; set; }
        public PlanogramLabelHorizontalAlignment LabelHorizontalPlacement { get; set; }
        public PlanogramLabelVerticalAlignment LabelVerticalPlacement { get; set; }
        public PlanogramLabelTextAlignment TextBoxTextAlignment { get; set; }
        public PlanogramLabelTextDirection TextDirection { get; set; }
        public PlanogramLabelTextWrapping TextWrapping { get; set; }
        public Single BackgroundTransparency { get; set; }
        public Boolean ShowOverImages { get; set; }
        public Boolean ShowLabelPerFacing { get; set; }
        public Boolean HasImage { get; set; }
    }
}
