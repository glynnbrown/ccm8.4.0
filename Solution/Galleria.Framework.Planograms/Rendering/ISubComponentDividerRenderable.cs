﻿// Copyright © Galleria RTS Ltd 2016
using System;

namespace Galleria.Framework.Planograms.Rendering
{
    public interface ISubComponentDividerRenderable
    {
        Single X { get; }
        Single Y { get; }
        Single Z { get; }

        Single Height { get; }
        Single Width { get; }
        Single Depth { get; }
    }
}
