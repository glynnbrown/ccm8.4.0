﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rendering;
using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// A default implementation of IPlan3DDataSettings
    /// </summary>
    public sealed class PlanRenderSettings : IPlanRenderSettings
    {
        #region Fields

        private Boolean _canRenderAsync = true;
        private Boolean _suppressPerformanceIntensiveUpdates;
        private Boolean _generateNormals = true;
        private Boolean _generateTextureCoordiates = true;

        private Boolean _showChestWalls = true;
        private Boolean _showShelfRisers = true;
        private Boolean _showFixtureImages = false;
        private Boolean _showFixtures = true;
        private Boolean _showFixtureFillPatterns = false;
        private Boolean _showWireframeOnly = false;
        private Boolean _showNotches = false;
        private Boolean _showPegHoles = false;
        private Boolean _showDividerLines = false;

        private Double _selectionLineThickness = 0.5;
        private ModelColour _selectionColour;
        private ModelMaterial3D.FillPatternType _selectionFillPattern = ModelMaterial3D.FillPatternType.DiagonalUp;

        private Boolean _showPositions = true;
        private Boolean _showPositionUnits = false;
        private Boolean _showChestsTopDown = false;
        private Boolean _showProductImages = false;
        private Boolean _showProductShapes = false;
        private Boolean _showProductFillColours = true;
        private Boolean _showProductFillPatterns = false;
        private Boolean _showPegs = false;
        private Boolean _showDividers = false;
        private Boolean _showAnnotations = true;

        private IPlanogramLabel _productLabel = null;
        private IPlanogramLabel _fixtureLabel = null;
        private ModelLabelAttachFace _labelAttachFace = ModelLabelAttachFace.Front;
        private Boolean _showTopDownProductLabelsAsFront = true;

        private PlanogramPositionHighlightColours _productHighlights;
        private PlanogramPositionLabelTexts _positionLabelText;
        private PlanogramFixtureLabelTexts _fixtureLabelText;

        private CameraViewType _cameraType = CameraViewType.Perspective;

        #endregion

        #region PropertyNames

        //Not using property paths here because they are not supported in silverlight.
        public const String SuppressPerformanceIntensiveUpdatesPropertyName = "SuppressPerformanceIntensiveUpdates";
        public const String ShowChestWallsPropertyName = "ShowChestWalls";
        public const String ShowShelfRisersPropertyName = "ShowShelfRisers";
        public const String ShowFixtureImagesPropertyName = "ShowFixtureImages";
        public const String ShowFixturesPropertyName = "ShowFixtures";
        public const String ShowFixtureFillPatternsPropertyName = "ShowFixtureFillPatterns";
        public const String ShowWireframeOnlyPropertyName = "ShowWireframeOnly";
        public const String SelectionColourPropertyName = "SelectionColour";
        public const String SelectionFillPatternPropertyName = "SelectionFillPattern";
        public const String ShowNotchesPropertyName = "ShowNotches";
        public const String ShowPegHolesPropertyName = "ShowPegHoles";
        public const String ShowDividerLinesPropertyName = "ShowDividerLines";
        public const String ShowAnnotationsPropertyName = "ShowAnnotations";

        public const String ShowPositionsPropertyName = "ShowPositions";
        public const String ShowPositionUnitsPropertyName = "ShowPositionUnits";
        public const String RotateTopDownComponentsPropertyName = "RotateTopDownComponents";
        public const String ShowProductImagesPropertyName = "ShowProductImages";
        public const String ShowProductShapesPropertyName = "ShowProductShapes";
        public const String ShowProductFillColoursPropertyName = "ShowProductFillColours";
        public const String ShowProductFillPatternsPropertyName = "ShowProductFillPatterns";
        public const String ShowPegsPropertyName = "ShowPegs";
        public const String ShowDividersPropertyName = "ShowDividers";
        public const String ProductLabelPropertyName = "ProductLabel";
        public const String FixtureLabelPropertyName = "FixtureLabel";
        public const String PositionHighlightsPropertyName = "PositionHighlights";
        public const String PositionLabelTextPropertyName = "PositionLabelText";
        public const String FixtureLabelTextPropertyName = "FixtureLabelText";
        public const String LabelAttachFacePropertyName = "LabelAttachFace";
        public const String ShowTopDownComponentLabelsAsFrontPropertyName = "ShowTopDownComponentLabelsAsFront";

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether the plan can be rendered asyncronously.
        /// True by default.
        /// </summary>
        public Boolean CanRenderAsync
        {
            get { return _canRenderAsync; }
            set
            {
                if (value != _canRenderAsync)
                {
                    _canRenderAsync = value;
                    OnPropertyChanged("CanRenderAsync");
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether normals should be generated
        /// </summary>
        public Boolean GenerateNormals
        {
            get { return _generateNormals; }
            set { _generateNormals = value; }
        }

        /// <summary>
        /// Gets/Sets whether textures should be generated.
        /// </summary>
        public Boolean GenerateTextureCoordinates
        {
            get { return _generateTextureCoordiates; }
            set { _generateTextureCoordiates = value; }
        }

        /// <summary>
        /// Gets/Sets whether heavy updates should be suppressed.
        /// This is used while the plan is in an update mode.
        /// </summary>
        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _suppressPerformanceIntensiveUpdates; }
            set
            {
                if (value != _suppressPerformanceIntensiveUpdates)
                {
                    Object oldValue = _suppressPerformanceIntensiveUpdates;
                    _suppressPerformanceIntensiveUpdates = value;
                    OnPropertyChanged(SuppressPerformanceIntensiveUpdatesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether chest walls should be visible
        /// </summary>
        public Boolean ShowChestWalls
        {
            get { return _showChestWalls; }
            set
            {
                if (value != _showChestWalls)
                {
                    Object oldValue = _showChestWalls;
                    _showChestWalls = value;
                    OnPropertyChanged(ShowChestWallsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether shelf risers should be drawn
        /// </summary>
        public Boolean ShowShelfRisers
        {
            get { return _showShelfRisers; }
            set
            {
                if (value != _showShelfRisers)
                {
                    Object oldValue = _showShelfRisers;

                    _showShelfRisers = value;
                    OnPropertyChanged(ShowShelfRisersPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether fixture images should be shown.
        /// </summary>
        public Boolean ShowFixtureImages
        {
            get { return _showFixtureImages; }
            set
            {
                if (value != _showFixtureImages)
                {
                    _showFixtureImages = value;
                    OnPropertyChanged(ShowFixtureImagesPropertyName);
                }
            }
        }

        /// <summary>
        /// Toggles the rendering of all fixtures and components.
        /// </summary>
        public Boolean ShowFixtures
        {
            get { return _showFixtures; }
            set
            {
                if (value != _showFixtures)
                {
                    _showFixtures = value;
                    OnPropertyChanged(ShowFixturesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether fixture fill patterns should be shown.
        /// </summary>
        public Boolean ShowFixtureFillPatterns
        {
            get { return _showFixtureFillPatterns; }
            set
            {
                if (value != _showFixtureFillPatterns)
                {
                    _showFixtureFillPatterns = value;
                    OnPropertyChanged(ShowFixtureFillPatternsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the model should be shown as a wireframe.
        /// </summary>
        public Boolean ShowWireframeOnly
        {
            get { return _showWireframeOnly; }
            set
            {
                if (value != _showWireframeOnly)
                {
                    Object oldValue = _showWireframeOnly;

                    _showWireframeOnly = value;
                    OnPropertyChanged(ShowWireframeOnlyPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the thickness of selection lines.
        /// </summary>
        public Double SelectionLineThickness
        {
            get { return _selectionLineThickness; }
        }

        /// <summary>
        /// Gets/Sets the colour used for selection
        /// </summary>
        public ModelColour SelectionColour
        {
            get { return _selectionColour; }
            set
            {
                _selectionColour = value;
                OnPropertyChanged(SelectionColourPropertyName);
            }
        }

        /// <summary>
        /// Gets/Sets the fill pattern used for selection
        /// </summary>
        public ModelMaterial3D.FillPatternType SelectionFillPattern
        {
            get { return _selectionFillPattern; }
            set
            {
                _selectionFillPattern = value;
                OnPropertyChanged(SelectionFillPatternPropertyName);
            }
        }

        /// <summary>
        /// Gets/Sets whether notches should be shown
        /// </summary>
        public Boolean ShowNotches
        {
            get { return _showNotches; }
            set
            {
                if (value != _showNotches)
                {
                    Object oldValue = _showNotches;

                    _showNotches = value;
                    OnPropertyChanged(ShowNotchesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether peg holes should be shown.
        /// </summary>
        public Boolean ShowPegHoles
        {
            get { return _showPegHoles; }
            set
            {
                if (value != _showPegHoles)
                {
                    Object oldValue = _showPegHoles;

                    _showPegHoles = value;
                    OnPropertyChanged(ShowPegHolesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether slot lines should be shown.
        /// </summary>
        public Boolean ShowDividerLines
        {
            get { return _showDividerLines; }
            set
            {
                if (value != _showDividerLines)
                {
                    Object oldValue = _showDividerLines;

                    _showDividerLines = value;
                    OnPropertyChanged(ShowDividerLinesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether position models should be visible.
        /// </summary>
        public Boolean ShowPositions
        {
            get { return _showPositions; }
            set
            {
                if (value != _showPositions)
                {
                    Object oldValue = _showPositions;
                    _showPositions = value;
                    OnPropertyChanged(ShowPositionsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether position units should be shown
        /// </summary>
        public Boolean ShowPositionUnits
        {
            get { return _showPositionUnits; }
            set
            {
                if (value != _showPositionUnits)
                {
                    Object oldValue = _showPositionUnits;
                    _showPositionUnits = value;
                    OnPropertyChanged(ShowPositionUnitsPropertyName);
                }

            }
        }

        /// <summary>
        /// Gets/Sets whether components with IsMerchandisedTopDown true should be flipped
        /// </summary>
        public Boolean RotateTopDownComponents
        {
            get { return _showChestsTopDown; }
            set
            {
                if (value != _showChestsTopDown)
                {
                    Object oldValue = _showChestsTopDown;

                    _showChestsTopDown = value;
                    OnPropertyChanged(RotateTopDownComponentsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether images should be shown
        /// </summary>
        public Boolean ShowProductImages
        {
            get { return _showProductImages; }
            set
            {
                if (value != _showProductImages)
                {
                    Object oldValue = _showProductImages;

                    _showProductImages = value;
                    OnPropertyChanged(ShowProductImagesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the product shapes should be shown
        /// </summary>
        public Boolean ShowProductShapes
        {
            get { return _showProductShapes; }
            set
            {
                if (value != _showProductShapes)
                {
                    Object oldValue = _showProductShapes;

                    _showProductShapes = value;
                    OnPropertyChanged(ShowProductShapesPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the product fill colours should be shown,
        /// only used when no highlight applied.
        /// </summary>
        public Boolean ShowProductFillColours
        {
            get { return _showProductFillColours; }
            set
            {
                if (value != _showProductFillColours)
                {
                    Object oldValue = _showProductFillColours;

                    _showProductFillColours = value;
                    OnPropertyChanged(ShowProductFillColoursPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the product fill patterns should be shown
        /// </summary>
        public Boolean ShowProductFillPatterns
        {
            get { return _showProductFillPatterns; }
            set
            {
                if (value != _showProductFillPatterns)
                {
                    Object oldValue = _showProductFillPatterns;

                    _showProductFillPatterns = value;
                    OnPropertyChanged(ShowProductFillPatternsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether pegs should be shown.
        /// </summary>
        public Boolean ShowPegs
        {
            get { return _showPegs; }
            set
            {
                if (value != _showPegs)
                {
                    Object oldValue = _showPegs;

                    _showPegs = value;
                    OnPropertyChanged(ShowPegsPropertyName);
                }
            }
        }

        /// <summary>
        /// Show Dividers
        /// </summary>
        public Boolean ShowDividers
        {
            get { return _showDividers; }
            set
            {
                if (value != _showDividers)
                {
                    Object oldValue = _showDividers;

                    _showDividers = value;
                    OnPropertyChanged(ShowDividersPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the setting to use for product labels
        /// </summary>
        public IPlanogramLabel ProductLabel
        {
            get { return _productLabel; }
            set
            {
                if (value != _productLabel)
                {
                    Object oldValue = _productLabel;

                    _productLabel = value;
                    OnPropertyChanged(ProductLabelPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the setting to use for fixture labels
        /// </summary>
        public IPlanogramLabel FixtureLabel
        {
            get { return _fixtureLabel; }
            set
            {
                if (value != _fixtureLabel)
                {
                    Object oldValue = _fixtureLabel;

                    _fixtureLabel = value;
                    OnPropertyChanged(FixtureLabelPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the default face to use when attaching labels.
        /// </summary>
        public ModelLabelAttachFace LabelAttachFace
        {
            get { return _labelAttachFace; }
            set
            {
                if (_labelAttachFace != value)
                {
                    _labelAttachFace = value;
                    OnPropertyChanged(LabelAttachFacePropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets a flag which indicates whether product labels 
        /// on top down components should be drawn to the front when the component
        /// is not rotated.
        /// </summary>
        public Boolean ShowTopDownProductLabelsAsFront
        {
            get { return _showTopDownProductLabelsAsFront; }
            set
            {
                if (_showTopDownProductLabelsAsFront != value)
                {
                    _showTopDownProductLabelsAsFront = value;
                    OnPropertyChanged(ShowTopDownComponentLabelsAsFrontPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the product highlights to apply.
        /// </summary>
        public PlanogramPositionHighlightColours PositionHighlights
        {
            get { return _productHighlights; }
            set
            {
                if (value != _productHighlights)
                {
                    Object oldValue = _productHighlights;
                    _productHighlights = value;
                    OnPropertyChanged(PositionHighlightsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the label text to be displayed for each position.
        /// </summary>
        public PlanogramPositionLabelTexts PositionLabelText
        {
            get { return _positionLabelText; }
            set
            {
                if (value != _positionLabelText)
                {
                    Object oldValue = _positionLabelText;
                    _positionLabelText = value;
                    OnPropertyChanged(PositionLabelTextPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the label text to be displayed for each component.
        /// </summary>
        public PlanogramFixtureLabelTexts FixtureLabelText
        {
            get { return _fixtureLabelText; }
            set
            {
                if (value != _fixtureLabelText)
                {
                    Object oldValue = _fixtureLabelText;
                    _fixtureLabelText = value;
                    OnPropertyChanged(FixtureLabelTextPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the model should be shown as a Annotations.
        /// </summary>
        public Boolean ShowAnnotations
        {
            get { return _showAnnotations; }
            set
            {
                if (value != _showAnnotations)
                {
                    Object oldValue = _showAnnotations;

                    _showAnnotations = value;
                    OnPropertyChanged(ShowAnnotationsPropertyName);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the camera type being used to view the model.
        /// Used for performance optimisations.
        /// </summary>
        public CameraViewType CameraType
        {
            get { return _cameraType; }
            set { _cameraType = value; }
        }

        #endregion

        #region Constructor
        public PlanRenderSettings()
        {
            _selectionColour = ModelColour.NewColour(255, 165, 0); //orange
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Copies the settings from the given object
        /// </summary>
        /// <param name="settings"></param>
        public void LoadSettings(IPlanRenderSettings settings)
        {
            ShowShelfRisers = settings.ShowShelfRisers;
            ShowChestWalls = settings.ShowChestWalls;
            ShowFixtureImages = settings.ShowFixtureImages;
            ShowFixtureFillPatterns = settings.ShowFixtureFillPatterns;
            ShowWireframeOnly = settings.ShowWireframeOnly;
            ShowNotches = settings.ShowNotches;
            ShowPegHoles = settings.ShowPegHoles;
            ShowDividerLines = settings.ShowDividerLines;
            ShowPositions = settings.ShowPositions;
            ShowPositionUnits = settings.ShowPositionUnits;
            RotateTopDownComponents = settings.RotateTopDownComponents;
            ShowProductImages = settings.ShowProductImages;
            ShowProductShapes = settings.ShowProductShapes;
            ShowProductFillPatterns = settings.ShowProductFillPatterns;
            ShowPegs = settings.ShowPegs;
            ShowDividers = settings.ShowDividers;
            ShowTopDownProductLabelsAsFront = settings.ShowTopDownProductLabelsAsFront;
            ShowAnnotations = settings.ShowAnnotations;
            LabelAttachFace = ModelLabelAttachFace.Auto;
            PositionHighlights = settings.PositionHighlights;
            GenerateNormals = settings.GenerateNormals;
            GenerateTextureCoordinates = settings.GenerateTextureCoordinates;
            SelectionColour = settings.SelectionColour;


            ProductLabel = settings.ProductLabel;
            FixtureLabel = settings.FixtureLabel;
            PositionHighlights = settings.PositionHighlights;
            PositionLabelText = settings.PositionLabelText;
            FixtureLabelText = settings.FixtureLabelText;
        }

        #endregion
    }
}
