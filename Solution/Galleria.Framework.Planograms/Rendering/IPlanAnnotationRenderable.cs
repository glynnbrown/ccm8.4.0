﻿// Copyright © Galleria RTS Ltd 2016

using System;
using System.ComponentModel;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Defines members requried to render a planogram annotation.
    /// </summary>
    public interface IPlanAnnotationRenderable : INotifyPropertyChanged
    {

        /// <summary>
        /// The x position of the part (front bottom left)
        /// </summary>
        Single X { get; }
        /// <summary>
        /// The y position of the part (front bottom left)
        /// </summary>
        Single Y { get; }
        /// <summary>
        /// The z position of the part (front bottom left)
        /// </summary>
        Single Z { get; }

        /// <summary>
        /// The height of the part
        /// </summary>
        Single Height { get; }
        /// <summary>
        /// The width of the part
        /// </summary>
        Single Width { get; }
        /// <summary>
        /// The depth of the part
        /// </summary>
        Single Depth { get; }


        /// <summary>
        /// The actual text to show
        /// </summary>
        String Text { get; }

        /// <summary>
        /// The annotation background colour
        /// </summary>
        Int32 BackgroundColour { get; }

        /// <summary>
        /// The annotation border colour
        /// </summary>
        Int32 BorderColour { get; }

        /// <summary>
        /// The thickness of the border line.
        /// </summary>
        Single BorderThickness { get; }

        /// <summary>
        /// The colour of the text
        /// </summary>
        Int32 FontColour { get; }

        /// <summary>
        /// The size of the text
        /// </summary>
        Single FontSize { get; }

        /// <summary>
        /// The name of the font family to use.
        /// </summary>
        String FontName { get; }

        /// <summary>
        /// If true, the font size may be reduced to fit within the annotation bounds.
        /// </summary>
        Boolean CanReduceFontToFit { get; }
    }
}
