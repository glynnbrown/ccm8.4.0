﻿// Copyright © Galleria RTS Ltd 2016


using Galleria.Framework.DataStructures;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Class representing the data required to create a modelconstruct3d for a planogram
    /// </summary>
    public sealed class Plan3DData : ModelConstruct3DData
    {
        #region Fields

        private readonly Dictionary<IPlanFixtureRenderable, PlanFixture3DData> _fixtureModelsDict = new Dictionary<IPlanFixtureRenderable, PlanFixture3DData>();

        private IPlanRenderable _sourcePlanogram;
        private IPlanRenderSettings _settingsController;

        #endregion

        #region Properties

        public IPlanRenderSettings Settings
        {
            get { return _settingsController; }
        }

        /// <summary>
        /// Returns true if updates are paused on this model.
        /// </summary>
        public Boolean IsPaused { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="sourcePlan">The source planogram</param>
        /// <param name="settingsController">The settings controller.</param>
        public Plan3DData(IPlanRenderable sourcePlan, IPlanRenderSettings settingsController)
        {
            _sourcePlanogram = sourcePlan;
            _sourcePlanogram.FixturesCollectionChanged += SourcePlan_FixturesCollectionChanged;

            _settingsController = (settingsController != null) ? settingsController : new PlanRenderSettings();
            _settingsController.PropertyChanged += SettingsController_PropertyChanged;

            this.IsWireframe = _settingsController.ShowWireframeOnly;

            Initialize();

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the source planogram fixtures collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourcePlan_FixturesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanFixtureRenderable fixture in e.NewItems)
                        {
                            AddFixtureModel(fixture);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanFixtureRenderable fixture in e.OldItems)
                        {
                            RemoveFixtureModel(fixture);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        //load the views ordered by x so that rendering looks neater.
                        List<IPlanFixtureRenderable> fixtureViews =
                            _sourcePlanogram.Fixtures.OrderBy(f => f.X).ToList();

                        //remove as required
                        foreach (IPlanFixtureRenderable modelledFixture in _fixtureModelsDict.Keys.ToList())
                        {
                            if (!fixtureViews.Contains(modelledFixture))
                            {
                                RemoveFixtureModel(modelledFixture);
                            }
                            else
                            {
                                fixtureViews.Remove(modelledFixture);
                            }
                        }

                        //add remaining
                        foreach (IPlanFixtureRenderable fixtureView in fixtureViews)
                        {
                            AddFixtureModel(fixtureView);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a property on the settings controller changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanRenderSettings.ShowWireframeOnlyPropertyName)
            {
                this.IsWireframe = _settingsController.ShowWireframeOnly;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void Initialize()
        {
            IPlanRenderable plan = _sourcePlanogram;
            if (plan != null)
            {
                Single planX = Single.PositiveInfinity;
                Single planWidth = 0;
                Single planY = Single.PositiveInfinity;
                Single planHeight = 0;
                Single planZ = Single.PositiveInfinity;
                Single planDepth = 0;

                foreach (IPlanFixtureRenderable fixture in plan.Fixtures)
                {
                    AddFixtureModel(fixture);

                    //Determine the size of this model
                    planX = Math.Min(planX, fixture.X);
                    planY = Math.Min(planY, fixture.Y);
                    planZ = Math.Min(planZ, fixture.Z);

                    planWidth = Math.Max(planWidth, (fixture.X + fixture.Width));
                    planHeight = Math.Max(planHeight, (fixture.Y + fixture.Height));
                    planDepth = Math.Max(planDepth, (fixture.Z + fixture.Depth));
                }


                //Add annotations.
                //foreach (PlanogramAnnotationView anno in plan.Annotations)
                //{
                //    AddChild(new Annotation3D(anno));
                //}


                planWidth = planWidth - planX;
                planHeight = planHeight - planY;
                planDepth = planDepth - planZ;

                this.Size = new WidthHeightDepthValue() { Width = planWidth, Height = planHeight, Depth = planDepth };
            }
        }

        /// <summary>
        /// Adds a model child for the given fixture view.
        /// </summary>
        /// <param name="fixtureView"></param>
        private void AddFixtureModel(IPlanFixtureRenderable fixtureView)
        {
            PlanFixture3DData model = new PlanFixture3DData(fixtureView, _settingsController);
            AddChild(model);
            _fixtureModelsDict.Add(fixtureView, model);
        }

        /// <summary>
        /// Removes the model relating to the given fixture view
        /// </summary>
        /// <param name="fixtureView"></param>
        private void RemoveFixtureModel(IPlanFixtureRenderable fixtureView)
        {
            PlanFixture3DData model;

            if (_fixtureModelsDict.TryGetValue(fixtureView, out model))
            {
                RemoveChild(model);
                _fixtureModelsDict.Remove(fixtureView);
            }
        }


        /// <summary>
        /// Returns a list of all fixture models held by this planogram model
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanFixture3DData> EnumerateFixtureModels()
        {
            //only need to check direct children for this.
            foreach (var entry in _fixtureModelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Returns a list of all assembly models
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanAssembly3DData> EnumerateAssemblyModels()
        {
            foreach (IModelConstruct3DData model3D in EnumerateChildModels())
            {
                if (model3D is PlanAssembly3DData)
                {
                    yield return (PlanAssembly3DData)model3D;
                }
            }
        }

        /// <summary>
        /// Returns a list of all component models held by this
        /// planogram model.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanComponent3DData> EnumerateComponentModels()
        {
            foreach (IModelConstruct3DData model3D in EnumerateChildModels())
            {
                if (model3D is PlanComponent3DData)
                {
                    yield return (PlanComponent3DData)model3D;
                }
            }
        }

        /// <summary>
        /// Returns a list of all subcomponent models held by this
        /// planogram model.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanSubComponent3DData> EnumerateSubComponentModels()
        {
            foreach (IModelConstruct3DData model3D in EnumerateChildModels())
            {
                if (model3D is PlanSubComponent3DData)
                {
                    yield return (PlanSubComponent3DData)model3D;
                }
            }
        }

        /// <summary>
        /// Returns a list of all position models held by this
        /// planogram model.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanPosition3DData> EnumeratePositionModels()
        {
            foreach (IModelConstruct3DData model3D in EnumerateChildModels())
            {
                if (model3D is PlanPosition3DData)
                {
                    yield return (PlanPosition3DData)model3D;
                }
            }
        }

        /// <summary>
        /// Returns the model for the given component
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public PlanComponent3DData FindComponentModel(IPlanComponentRenderable component)
        {
            foreach (PlanComponent3DData c in EnumerateComponentModels())
            {
                if (c.Component == component)
                {
                    return c;
                }
            }
            return null;
        }

        /// <summary>
        /// Freezes the plan model and all of its children.
        /// All event handlers will be unsubscribed
        /// and this will no longer react to changes.
        /// </summary>
        public void Freeze()
        {
            OnFreeze();
        }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        public void PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData fixtureData in EnumerateFixtureModels())
            {
                fixtureData.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        public void UnpauseUpdates()
        {
            foreach (IPlanPart3DData fixtureData in EnumerateFixtureModels())
            {
                fixtureData.UnpauseUpdates();
            }
            IsPaused = false;
        }

        #endregion

        #region IDisposable

        protected override void OnFreeze()
        {
            base.OnFreeze();

            _sourcePlanogram.FixturesCollectionChanged -= SourcePlan_FixturesCollectionChanged;
            _settingsController.PropertyChanged -= SettingsController_PropertyChanged;

        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                _sourcePlanogram.FixturesCollectionChanged -= SourcePlan_FixturesCollectionChanged;
                _settingsController.PropertyChanged -= SettingsController_PropertyChanged;

                DisposeBase();
                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion

        #region ICloneable

        public Plan3DData Clone()
        {
            Plan3DData clone = new Plan3DData(this._sourcePlanogram, this.Settings);
            return clone;
        }

        #endregion

    }
}
