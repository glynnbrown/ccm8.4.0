﻿// Copyright © Galleria RTS Ltd 2016
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Galleria.Framework.Planograms.Rendering
{
    /// <summary>
    /// Implementation of ModelConstruct3DData for a PlanogramSubComponent.
    /// </summary>
    public sealed class PlanSubComponent3DData : ModelConstruct3DData, IPlanPart3DData
    {
        #region Fields

        const Boolean genTextures = true;

        private readonly IPlanRenderSettings _settingsController;
        private readonly IPlanSubComponentRenderable _subcomponent;

        private ModelConstructPart3DData _subComponentPart;
        private Dictionary<BoxFaceType, BoxModelPart3DData> _riserParts = new Dictionary<BoxFaceType, BoxModelPart3DData>();
        private MeshPart3DData _dividerLinePart;
        private List<ModelConstructPart3DData> _dividerParts = new List<ModelConstructPart3DData>();
        private MeshPart3DData _pegHolesPart;
        private MeshPart3DData _notchesPart;

        private readonly Dictionary<IPlanPositionRenderable, PlanPosition3DData> _posModelsDict = new Dictionary<IPlanPositionRenderable, PlanPosition3DData>();
        private readonly Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData> _annoModelsDict = new Dictionary<IPlanAnnotationRenderable, PlanAnnotation3DData>();

        private Boolean _isSelected = false;

        private Boolean _isPartChanged; //flag to indicate that the parts need updating
        private Boolean _isMaterialChanged; // flag to indicate the material needs updating.
        private Boolean _isRiserPartChanged; //flag to indicate that risers need updating;
        private Boolean _isRiserMaterialChanged; //flag to indicate that the riser material needs updating;
        private Boolean _isDividerLinesChanged; // flag to indicate that slotlines need updating.
        private Boolean _isPegholesChanged; // flag to indicate that pegholes need updating.
        private Boolean _isNotchesChanged; //flag to indicate that notches need updating.
        private Boolean _isPlacementChanged; //flag to indicate that the placement needs updating.
        private Boolean _isVisibilityChanged; //flag to indicate that the visibility needs updating.
        private Boolean _isDividersChanged; //flag to indicate that the dividers need updating.

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source component view.
        /// </summary>
        public IPlanSubComponentRenderable SubComponent
        {
            get { return _subcomponent; }
        }

        /// <summary>
        /// Returns true if performance intensive updates should be supressed.
        /// </summary>
        public Boolean SupressPerformanceIntensiveUpdates
        {
            get { return _settingsController.SuppressPerformanceIntensiveUpdates; }
        }

        /// <summary>
        /// Gets/Sets whether this model is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");

                    OnMaterialChanged();
                    OnRiserMaterialChanged();
                }
            }
        }

        /// <summary>
        /// Returns the actual subcomponent part data.
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public ModelConstructPart3DData SubComponentPart
        {
            get { return _subComponentPart; }
        }

        /// <summary>
        /// Returns the dividerline part data
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public MeshPart3DData DividerLinePart
        {
            get { return _dividerLinePart; }
        }

        /// <summary>
        /// Returns the riser part data
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public Dictionary<BoxFaceType, BoxModelPart3DData> RiserParts
        {
            get { return _riserParts; }
        }

        /// <summary>
        /// Returns the peghole part data
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public MeshPart3DData PegholesPart
        {
            get { return _pegHolesPart; }
        }

        /// <summary>
        /// Returns the notch part data
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public MeshPart3DData NotchesPart
        {
            get { return _notchesPart; }
        }

        /// <summary>
        /// Returns the currently loaded divider parts for this sub.
        /// </summary>
        /// <remarks>Exposed for unit testing</remarks>
        public IEnumerable<ModelConstructPart3DData> DividerParts
        {
            get { return _dividerParts; }
        }

        /// <summary>
        /// Returns true if chest walls should be visible.
        /// (Inherited from the parent Plan3DData)
        /// </summary>
        private Boolean ShowChestWalls
        {
            get { return _settingsController.ShowChestWalls; }
        }

        /// <summary>
        /// Returns true if shelf risers should be visible
        /// (Inherited from the parent Plan3DData)
        /// </summary>
        private Boolean ShowShelfRisers
        {
            get { return _settingsController.ShowShelfRisers; }
        }

        /// <summary>
        /// Returns true if images should be used
        /// </summary>
        private Boolean ShowImages
        {
            get
            {
                if (_settingsController != null)
                {
                    return _settingsController.ShowFixtureImages
                        && !this.SupressPerformanceIntensiveUpdates;
                }
                return true;
            }
        }

        /// <summary>
        /// Returns true if fill patterns will be shown.
        /// </summary>
        private Boolean ShowFillPatterns
        {
            get
            {
                return _settingsController.ShowFixtureFillPatterns
                && !this.SupressPerformanceIntensiveUpdates;
            }
        }

        /// <summary>
        /// Returns true if peg holes should be shown.
        /// </summary>
        private Boolean ShowPegHoles
        {
            get { return _settingsController.ShowPegHoles; }
        }

        /// <summary>
        /// Returns true if notches should be shown.
        /// </summary>
        private Boolean ShowNotches
        {
            get { return _settingsController.ShowNotches; }
        }

        /// <summary>
        /// Returns true if slot lines should be shown
        /// </summary>
        private Boolean ShowDividerLines
        {
            get { return _settingsController.ShowDividerLines; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="subcomponentView">The source IPlanogramSubComponent</param>
        /// <param name="parentPlan3D">The parent plan data.</param>
        public PlanSubComponent3DData(IPlanSubComponentRenderable subcomponentView, IPlanRenderSettings settingsController)
        {
            _settingsController = settingsController;
            _subcomponent = subcomponentView;

            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="subcomponentView">The source IPlanogramSubComponent</param>
        /// <param name="parentPlan3D">The parent plan data.</param>
        internal PlanSubComponent3DData(ModelConstruct3DData parentData, IPlanSubComponentRenderable subcomponentView, IPlanRenderSettings settingsController)
        {
            this.ParentModelData = parentData;
            _settingsController = settingsController;
            _subcomponent = subcomponentView;

            Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the source IPlanogramSubComponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsUpdating":
                    OnIsUpdatingChanged();
                    break;

                case "X":
                case "Y":
                case "Z":
                case "Slope":
                case "Angle":
                case "Roll":
                    OnPlacementChanged();
                    break;

                case "DividerObstructionFillColour":
                case "DividerObstructionFillPattern":
                    OnDividersChanged();
                    break;


                default:
                    OnPartChanged();
                    break;
            }
        }

        /// <summary>
        /// Called when a property changes on the parent plangram 3d data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case PlanRenderSettings.ShowChestWallsPropertyName:
                case PlanRenderSettings.ShowShelfRisersPropertyName:
                    OnPartChanged();
                    break;

                case PlanRenderSettings.ShowFixtureImagesPropertyName:
                case PlanRenderSettings.ShowFixtureFillPatternsPropertyName:
                    OnMaterialChanged();
                    break;

                case PlanRenderSettings.ShowNotchesPropertyName:
                    OnNotchesChanged();
                    break;

                case PlanRenderSettings.ShowPegHolesPropertyName:
                    OnPegholesChanged();
                    break;

                case PlanRenderSettings.ShowDividerLinesPropertyName:
                    OnDividerLinesChanged();
                    break;

                case PlanRenderSettings.SuppressPerformanceIntensiveUpdatesPropertyName:
                    OnSupressPerformanceIntensiveUpdatesChanged();
                    break;

                case PlanRenderSettings.ShowDividersPropertyName:
                    OnDividersChanged();
                    break;

                case PlanRenderSettings.SelectionColourPropertyName:
                    if (this.IsSelected && !this.SupressPerformanceIntensiveUpdates) OnMaterialChanged();
                    break;
            }
        }

        /// <summary>
        /// Called whenever the settings controller property value changes
        /// </summary>
        private void OnSupressPerformanceIntensiveUpdatesChanged()
        {
            if (!this.SupressPerformanceIntensiveUpdates)
            {
                if (_isNotchesChanged)
                {
                    OnNotchesChanged();
                }

                if (_isPegholesChanged)
                {
                    OnPegholesChanged();
                }
            }

            //update the material if this is a selected item
            // so that images can be hidden while performing the update.
            if (this.IsSelected)
            {
                OnMaterialChanged();
            }

        }

        /// <summary>
        /// Called whenever the positions collection changes on the source IPlanogramSubComponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponent_PositionsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanPositionRenderable view in e.NewItems)
                        {
                            AddPositionModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanPositionRenderable view in e.OldItems)
                        {
                            RemovePositionModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        //load the views ordered by x as it just looks neater when rendering
                        // things like images and labels etc.
                        List<IPlanPositionRenderable> views =
                            this.SubComponent.Positions.OrderBy(p => p.X).ToList();

                        //remove as required
                        foreach (IPlanPositionRenderable modelledView in _posModelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemovePositionModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanPositionRenderable v in views)
                        {
                            AddPositionModel(v);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the annotations collection changes on the source IPlanogramSubComponent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponent_AnnotationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanAnnotationRenderable view in e.NewItems)
                        {
                            AddAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanAnnotationRenderable view in e.OldItems)
                        {
                            RemoveAnnotationModel(view);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        List<IPlanAnnotationRenderable> views = this.SubComponent.Annotations.ToList();

                        //remove as required
                        foreach (IPlanAnnotationRenderable modelledView in _annoModelsDict.Keys.ToList())
                        {
                            if (!views.Contains(modelledView))
                            {
                                RemoveAnnotationModel(modelledView);
                            }
                            else
                            {
                                views.Remove(modelledView);
                            }
                        }

                        //add remaining
                        foreach (IPlanAnnotationRenderable v in views)
                        {
                            AddAnnotationModel(v);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the subcomponent dividers collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubComponent_DividersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnDividersChanged();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out initialization actions.
        /// </summary>
        private void Initialize()
        {
            _settingsController.PropertyChanged += SettingsController_PropertyChanged;

            _subcomponent.PropertyChanged += SubComponent_PropertyChanged;
            _subcomponent.PositionsCollectionChanged += SubComponent_PositionsCollectionChanged;
            _subcomponent.AnnotationsCollectionChanged += SubComponent_AnnotationsCollectionChanged;
            _subcomponent.DividersCollectionChanged += SubComponent_DividersCollectionChanged;

            //Add child positions
            foreach (var pos in _subcomponent.Positions)
            {
                AddPositionModel(pos);
            }

            //Add child annotations
            foreach (var anno in _subcomponent.Annotations)
            {
                AddAnnotationModel(anno);
            }

            //update position and create parts
            OnPartChanged();

            OnPlacementChanged();

            OnIsUpdatingChanged();
        }

        #region Rendering Methods

        /// <summary>
        /// Updates the position and rotation of this model.
        /// </summary>
        private void UpdateModelPlacement()
        {
            var subcomponentView = this.SubComponent;

            this.Position = new PointValue
            {
                X = subcomponentView.X,
                Y = subcomponentView.Y,
                Z = subcomponentView.Z
            };

            this.Rotation = new RotationValue
            {
                Angle = subcomponentView.Angle,
                Slope = subcomponentView.Slope,
                Roll = subcomponentView.Roll
            };
        }

        /// <summary>
        /// Updates the visiblity of this model.
        /// </summary>
        private void UpdateVisibility()
        {
            Boolean setIsVisible = true;

            foreach (IModelConstructPart3DData part in this.ModelParts)
            {
                part.IsVisible = setIsVisible;
            }
        }

        /// <summary>
        /// Updates the material used by the parts of this model.
        /// </summary>
        private void UpdateMaterial()
        {
            //if we have no model to colour then just return.
            if (_subComponentPart == null) { return; }

            var subComponent = this.SubComponent;
            var settings = _settingsController;
            ModelConstructPart3DData subPart = _subComponentPart;

            subPart.BeginEdit();
            {
                if (this.IsSelected)
                {
                    subPart.LineThickness = (subComponent.LineThickness > 0) ? subComponent.LineThickness : Double.PositiveInfinity;

                    subPart.LineMaterial =
                        new ModelMaterial3D()
                        {
                            FillColour = settings.SelectionColour,
                            FillPattern = settings.SelectionFillPattern
                        };
                }
                else
                {
                    subPart.LineMaterial = ModelMaterial3D.CreateMaterial(ModelColour.IntToColor(subComponent.LineColour));
                    subPart.LineThickness = _settingsController.ShowFixtures ? subComponent.LineThickness : 0f;
                }

                switch (subComponent.ShapeType)
                {
                    default:
                    case PlanogramSubComponentShapeType.Box:
                        ((BoxModelPart3DData)subPart).SetMaterials(GetBoxFaceMaterials());
                        break;

                        //case Model.FixtureSubComponentShapeType.Mesh:
                        //    subPart.Material = GetBoxFaceMaterials()[BoxFaceType.Front];
                        //    break;
                }
            }
            subPart.EndEdit();
        }

        /// <summary>
        /// Reloads the parts of this model.
        /// </summary>
        private void UpdateParts()
        {
            var subComponent = this.SubComponent;

            //update the size
            this.Size =
                new WidthHeightDepthValue()
                {
                    Width = subComponent.Width,
                    Height = subComponent.Height,
                    Depth = subComponent.Depth
                };

            ModelConstructPart3DData subPart = null;

            switch (subComponent.ShapeType)
            {
                #region Default / Box

                default:
                case PlanogramSubComponentShapeType.Box:
                    {
                        //check if we need to create the model
                        if (_subComponentPart != null && !(_subComponentPart is BoxModelPart3DData))
                        {
                            RemovePart(_subComponentPart);
                            _subComponentPart = null;
                        }
                        if (_subComponentPart == null)
                        {
                            _subComponentPart = new BoxModelPart3DData(this);
                            AddPart(_subComponentPart);
                        }

                        //Begin editing
                        subPart = _subComponentPart;
                        subPart.BeginEdit();


                        BoxModelPart3DData boxSubPart = (BoxModelPart3DData)_subComponentPart;

                        //set face thicknesses.
                        boxSubPart.FrontFaceThickness = subComponent.FaceThicknessFront;
                        boxSubPart.BackFaceThickness = subComponent.FaceThicknessBack;
                        boxSubPart.TopFaceThickness = subComponent.FaceThicknessTop;
                        boxSubPart.BottomFaceThickness = subComponent.FaceThicknessBottom;
                        boxSubPart.LeftFaceThickness = subComponent.FaceThicknessLeft;
                        boxSubPart.RightFaceThickness = subComponent.FaceThicknessRight;


                        //if this is a chest and the settings are set to hide walls
                        // then set all wall values to 0.
                        // V8-27202 : Showing chest walls should affect any subcomponent with face thickness,
                        // not just chests.
                        //if (subComponent.IsChest)
                        //{
                        if (!this.ShowChestWalls)
                        {
                            boxSubPart.FrontFaceThickness = 0;
                            boxSubPart.BackFaceThickness = 0;
                            boxSubPart.LeftFaceThickness = 0;
                            boxSubPart.RightFaceThickness = 0;
                        }
                        //}


                    }
                    break;

                    #endregion

                    #region Mesh

                    //case Model.FixtureSubComponentShapeType.Mesh:
                    //    {
                    //        //check if we need to create the model
                    //        if (_subComponentPart != null && !(_subComponentPart is FileModelPart3DData))
                    //        {
                    //            RemovePart(_subComponentPart);
                    //            _subComponentPart = null;
                    //        }
                    //        if (_subComponentPart == null)
                    //        {
                    //            _subComponentPart = new FileModelPart3DData(this);
                    //            AddPart(_subComponentPart);
                    //        }

                    //        //Begin editing
                    //        subPart = _subComponentPart;
                    //        subPart.BeginEdit();

                    //        FileModelPart3DData meshSubPart = (FileModelPart3DData)subPart;
                    //        meshSubPart.FileType = FileModelPartType.studio3ds;
                    //        meshSubPart.FileData = File.ReadAllBytes(@".\UserData\VWBus.3ds");

                    //    }
                    //break;

                    #endregion
            }

            subPart.Size = this.Size;

            //update related items.
            OnRiserPartChanged();
            OnNotchesChanged();
            OnDividerLinesChanged();
            OnPegholesChanged();
            OnMaterialChanged();
            OnDividersChanged();
            OnVisibilityChanged();

            subPart.EndEdit();
        }

        #region Risers

        /// <summary>
        /// Updates the riser parts of this subcomponent
        /// </summary>
        private void UpdateRiserModel()
        {
            if (_subComponentPart == null) { return; }

            var subComponent = this.SubComponent;
            WidthHeightDepthValue subComponentSize = this.Size;


            Single riserHeight = subComponent.RiserHeight;
            Single riserThickness = subComponent.RiserThickness;
            if (riserThickness == 0) riserThickness = 1;

            Boolean hasRiser =
                (subComponent.IsRiserPlacedOnFront || subComponent.IsRiserPlacedOnBack
                || subComponent.IsRiserPlacedOnLeft || subComponent.IsRiserPlacedOnRight);

            List<BoxFaceType> requiredRisers = new List<BoxFaceType>();

            if (hasRiser && this.ShowShelfRisers && riserHeight > 0)
            {
                if (subComponent.IsRiserPlacedOnFront) requiredRisers.Add(BoxFaceType.Front);
                if (subComponent.IsRiserPlacedOnBack) requiredRisers.Add(BoxFaceType.Back);
                if (subComponent.IsRiserPlacedOnLeft) requiredRisers.Add(BoxFaceType.Left);
                if (subComponent.IsRiserPlacedOnRight) requiredRisers.Add(BoxFaceType.Right);
            }


            //remove any old parts
            foreach (BoxFaceType key in _riserParts.Keys.ToList())
            {
                if (!requiredRisers.Contains(key))
                {
                    RemovePart(_riserParts[key]);
                    _riserParts.Remove(key);
                }
            }

            //now update as required
            foreach (BoxFaceType key in requiredRisers)
            {
                BoxModelPart3DData partData = null;
                _riserParts.TryGetValue(key, out partData);

                if (partData == null)
                {
                    partData = new BoxModelPart3DData(this);
                    _riserParts[key] = partData;
                    AddPart(partData);
                }

                switch (key)
                {
                    case BoxFaceType.Front:
                        partData.Position = new PointValue(0, subComponentSize.Height, subComponentSize.Depth);
                        partData.Size = new WidthHeightDepthValue(subComponentSize.Width, riserHeight, riserThickness);
                        break;

                    case BoxFaceType.Back:
                        partData.Position = new PointValue(0, subComponentSize.Height, -riserThickness);
                        partData.Size = new WidthHeightDepthValue(subComponentSize.Width, riserHeight, riserThickness);
                        break;

                    case BoxFaceType.Left:
                        partData.Position = new PointValue(-riserThickness, subComponentSize.Height, 0);
                        partData.Size = new WidthHeightDepthValue(riserThickness, riserHeight, subComponentSize.Depth);
                        break;

                    case BoxFaceType.Right:
                        partData.Position = new PointValue(subComponentSize.Width, subComponentSize.Height, 0);
                        partData.Size = new WidthHeightDepthValue(riserThickness, riserHeight, subComponentSize.Depth);
                        break;
                }
            }

            OnRiserMaterialChanged();
        }

        /// <summary>
        /// Updates the material of the riser model.
        /// </summary>
        private void UpdateRiserMaterial()
        {
            var subComponent = this.SubComponent;

            foreach (BoxModelPart3DData riserPart in _riserParts.Values)
            {
                riserPart.BeginEdit();
                {
                    ModelColour riserColour = new ModelColour() { Alpha = 255/*100*/, Red = 255, Blue = 255, Green = 255 };
                    if (subComponent.RiserColour != 0) riserColour = ModelColour.IntToColor(subComponent.RiserColour);

                    //set transparency
                    if (riserColour != ModelColour.Transparent)
                    {
                        riserPart.IsWireframe = false;

                        riserColour.Alpha = 255;
                        //riserColour.Alpha = Convert.ToByte((subComponent.RiserTransparencyPercent > 0) ?
                        //    255 - Math.Round((255 / 100.0) * subComponent.RiserTransparencyPercent) : 255);
                    }
                    else
                    {
                        //if the part is transparent then draw as wireframe only.
                        riserPart.IsWireframe = true;
                    }

                    riserPart.SetMaterial(riserColour);

                    if (subComponent != null)
                    {
                        riserPart.LineThickness = subComponent.LineThickness != 0 ? subComponent.LineThickness : 0.5F;
                        riserPart.LineMaterial = ModelMaterial3D.CreateMaterial((this.IsSelected) ? _settingsController.SelectionColour : ModelColour.IntToColor(subComponent.LineColour));
                    }
                    else
                    {
                        riserPart.LineThickness = 1;
                        riserPart.LineMaterial = ModelMaterial3D.CreateMaterial(ModelColour.NewColour(0F, 0F, 0F));
                    }


                }
                riserPart.EndEdit();
            }

        }

        #endregion

        #region Dividers

        /// <summary>
        /// Updates the divider line parts of this subcomponent
        /// </summary>
        private void UpdateDividerLines()
        {
            if (_subComponentPart == null) { return; }

            var subComponent = this.SubComponent;

            MeshBuilderInstructions mb = null;

            Boolean hasDividerLines = false;
            if (this.ShowDividerLines)
            {
                mb = CreateDividerLineInstructions(this.SubComponent.GetDividerLines());
                hasDividerLines = (mb != null);
            }


            if (hasDividerLines)
            {
                //create the divider line part if req
                if (_dividerLinePart == null)
                {
                    MeshPart3DData slotLinesPart = new MeshPart3DData(this);
                    slotLinesPart.CanWireframe = false;
                    slotLinesPart.Builder = mb;
                    slotLinesPart.SetMaterial(ModelColour.NewColour(253, 253, 172));

                    _dividerLinePart = slotLinesPart;
                    AddPart(_dividerLinePart);
                }
                else
                {
                    _dividerLinePart.Builder = mb;
                }

            }
            else if (_dividerLinePart != null)
            {
                RemovePart(_dividerLinePart);
                _dividerLinePart = null;
            }

        }

        /// <summary>
        /// Draws dividers
        /// </summary>
        private void UpdateDividers()
        {
            if (SubComponentPart == null) { return; }

            //remove existing dividers.
            if (_dividerParts.Count > 0)
            {
                foreach (ModelConstructPart3DData divider in _dividerParts)
                {
                    RemovePart(divider);
                }
                _dividerParts.Clear();
            }

            //draw in new ones
            if (_settingsController.ShowDividers && this.SubComponent.Dividers != null)
            {
                foreach (ISubComponentDividerRenderable divider in this.SubComponent.Dividers)
                {
                    BoxModelPart3DData dividerPart = new BoxModelPart3DData(this);
                    dividerPart.SetMaterial(new ModelMaterial3D(
                        ModelColour.IntToColor(this.SubComponent.DividerObstructionFillColour),
                        ToFillPatternType(this.SubComponent.DividerObstructionFillPattern), null));
                    dividerPart.Position = new PointValue(divider.X, divider.Y, divider.Z);
                    dividerPart.Size = new WidthHeightDepthValue(divider.Width, divider.Height, divider.Depth);

                    AddPart(dividerPart);
                    _dividerParts.Add(dividerPart);
                }
            }
        }

        /// <summary>
        /// Creates mesh builder instructions which can be used to create a single model part
        /// to display all divider lines.
        /// </summary>
        /// <param name="dividerLines"></param>
        /// <returns></returns>
        private static MeshBuilderInstructions CreateDividerLineInstructions(List<RectValue> dividerLines)
        {
            if (dividerLines.Count == 0) return null;

            Boolean generateNormals = false;
            Boolean generateTextures = false;
            MeshBuilderInstructions mb = new MeshBuilderInstructions(generateNormals, generateTextures);

            foreach (RectValue r in dividerLines)
            {
                mb.AddBoxFaces(new PointValue(r.X + (r.Width / 2F), r.Y + (r.Height / 2F), r.Z + (r.Depth / 2F)),
                    r.Width, r.Height, r.Depth, BoxFaceType.All);
            }

            return mb;
        }

        /// <summary>
        /// Creates mesh builder instructions which can be used to create a single model part
        /// to display all divider lines.
        /// </summary>
        /// <remarks>I have no idea why this is separate... should prob look at using this for plan version too as a common method.</remarks>
        public static MeshBuilderInstructions CreateDividerLineInstructions(
            Dictionary<BoxFaceType, Single> faceThicknesses, Single dividerWidth,
            Single divStartX, Single divSpacingX,
            Single divStartY, Single divSpacingY,
            Single divStartZ, Single divSpacingZ,
            WidthHeightDepthValue subSize)
        {
            //Dictionary<BoxFaceType, Single> faceThicknesses = GetFaceThicknesses();

            Boolean generateNormals = false;
            Boolean generateTextures = false;
            MeshBuilderInstructions mb = new MeshBuilderInstructions(generateNormals, generateTextures);

            Single lineWidth = dividerWidth;//subComponent.DividerObstructionWidth;//0.5F;
            Single lineDepth = 0.01F;

            Single xStart = faceThicknesses[BoxFaceType.Left] + divStartX;
            Single xSpacing = divSpacingX;

            if (xSpacing > 0 && xSpacing > lineWidth)
            {
                Int32 xLineCount = (Int32)((subSize.Width - xStart) / xSpacing);
                Single lineY = subSize.Height;

                //if we have any face thicknesses then we are going to place the products inside so use the bottom value.
                if (faceThicknesses.Values.Any(v => v > 0))
                {
                    lineY = faceThicknesses[BoxFaceType.Bottom];
                }

                //front
                MeshBuilderInstructions fontLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                fontLines.AddBox(lineWidth, lineY, lineDepth);
                fontLines.TileMesh(1, xLineCount, 1, 1, new PointValue(xSpacing - lineWidth, 0, 0));
                mb.Append(fontLines, new PointValue(xStart, 0, subSize.Depth));

                //top
                MeshBuilderInstructions topLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                topLines.AddBox(lineWidth, lineDepth, subSize.Depth);
                topLines.TileMesh(1, xLineCount, 1, 1, new PointValue(xSpacing - lineWidth, 0, 0));
                mb.Append(topLines, new PointValue(xStart, lineY, 0));

            }

            Single yStart = faceThicknesses[BoxFaceType.Bottom] + divStartY;
            Single ySpacing = divSpacingY;
            if (ySpacing > 0 && ySpacing > lineWidth)
            {
                Int32 lineCount = (Int32)((subSize.Height - yStart) / ySpacing);

                //front
                MeshBuilderInstructions frontLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                frontLines.AddBox(subSize.Width, lineWidth, lineDepth);
                frontLines.TileMesh(lineCount, 1, 1, 1, new PointValue(0, ySpacing - lineWidth, 0));
                mb.Append(frontLines, new PointValue(0, yStart, subSize.Depth));
            }


            Single zStart = faceThicknesses[BoxFaceType.Back] + divStartZ;
            Single zSpacing = divSpacingZ;

            if (zSpacing > 0 && zSpacing > lineWidth)
            {
                Int32 lineCount = (Int32)((subSize.Depth - zStart) / zSpacing);
                Single lineY = subSize.Height;

                //if we have any face thicknesses then we are going to place the products inside so use the bottom value.
                if (faceThicknesses.Values.Any(v => v > 0))
                {
                    lineY = faceThicknesses[BoxFaceType.Bottom];
                }

                //left
                MeshBuilderInstructions leftLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                leftLines.AddBox(lineDepth, lineY, lineWidth);
                leftLines.TileMesh(1, 1, lineCount, 1, new PointValue(0, 0, zSpacing - lineWidth));
                mb.Append(leftLines, new PointValue(0, 0, zStart));

                //right
                MeshBuilderInstructions rightLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                rightLines.AddBox(lineDepth, lineY, lineWidth);
                rightLines.TileMesh(1, 1, lineCount, 1, new PointValue(0, 0, zSpacing - lineWidth));
                mb.Append(rightLines, new PointValue(subSize.Width, 0, zStart));

                //top
                MeshBuilderInstructions topLines = new MeshBuilderInstructions(generateNormals, generateTextures);
                topLines.AddBox(subSize.Width, lineDepth, lineWidth);
                topLines.TileMesh(1, 1, lineCount, 1, new PointValue(0, 0, zSpacing - lineWidth));
                mb.Append(topLines, new PointValue(0, lineY, zStart));

            }

            return mb;
        }

        #endregion

        #region Pegholes

        /// <summary>
        /// Updates the peghole model for this subcomponent.
        /// </summary>
        private void UpdatePegholes()
        {
            if (_subComponentPart == null) { return; }
            var subComponent = this.SubComponent;

            //NB: Peg holes are on front facing only.
            PlanogramPegHoles pegHoles = subComponent.GetPegHoles();

            Boolean hasPegholes = false;
            if (this.ShowPegHoles && !this.SupressPerformanceIntensiveUpdates)
            {
                pegHoles = subComponent.GetPegHoles();

                if (pegHoles.Row1.Any() || pegHoles.Row2.Any())
                {
                    hasPegholes = true;
                }
            }

            //[GEM:28886] Check the peghole draw density
            // This is a threshold for performance reasons. 
            // Otherwise we end up drawing thousands of tiny holes.
            if (hasPegholes)
            {
                Single area = subComponent.Height * subComponent.Width;
                Single density = pegHoles.All.Count() / area;
                if (density > 50) hasPegholes = false;
            }


            if (hasPegholes)
            {
                MeshPart3DData modelPart = _pegHolesPart;

                //if the part does not exist then create it.
                if (modelPart == null)
                {
                    modelPart = new MeshPart3DData(this);
                    modelPart.Material = new ModelMaterial3D(ModelColour.Black, ModelMaterial3D.FillPatternType.Solid, null);
                    modelPart.Size = _subComponentPart.Size;
                    AddPart(modelPart);
                    _pegHolesPart = modelPart;
                }


                MeshBuilderInstructions meshInstructions = new MeshBuilderInstructions(_settingsController.GenerateNormals, genTextures);

                foreach (var p in pegHoles.Row1)
                {
                    meshInstructions.AddBoxFaces(new PointValue(p.X, p.Y, subComponent.Depth),
                       pegHoles.PegWidthRow1, pegHoles.PegHeightRow1, 0.1, BoxFaceType.Front);
                }

                foreach (var p in pegHoles.Row2)
                {
                    meshInstructions.AddBoxFaces(new PointValue(p.X, p.Y, subComponent.Depth),
                        pegHoles.PegWidthRow2, pegHoles.PegHeightRow2, 0.1, BoxFaceType.Front);
                }


                modelPart.Builder = meshInstructions;
            }
            else
            {
                //remove any existing part.
                if (_pegHolesPart != null)
                {
                    RemovePart(_pegHolesPart);
                    _pegHolesPart = null;
                }

                if (this.ShowPegHoles && this.SupressPerformanceIntensiveUpdates)
                {
                    //flag the change.
                    _isPegholesChanged = true;
                }
            }

        }

        #endregion

        #region Notches

        /// <summary>
        /// Updates the notch part for this subcomponent.
        /// </summary>
        private void UpdateNotches()
        {
            if (_subComponentPart == null) { return; }
            var subComponent = this.SubComponent;
            ModelConstructPart3DData subPart = _subComponentPart;

            MeshBuilderInstructions meshInstructions = null;

            Boolean hasNotches = (this.ShowNotches && !this.SupressPerformanceIntensiveUpdates);
            if (hasNotches)
            {
                meshInstructions =
                    CreateNotchPartInstructions(
                    _settingsController.GenerateNormals, genTextures,
                    subComponent.NotchStyleType, subComponent.IsNotchPlacedOnFront,
                    subComponent.NotchStartX, subComponent.NotchStartY,
                    subComponent.NotchSpacingX, subComponent.NotchSpacingY,
                    subComponent.NotchWidth, subComponent.NotchHeight,
                    subPart.Size);

                hasNotches = (meshInstructions != null);
            }



            if (hasNotches)
            {
                MeshPart3DData modelPart = _notchesPart;

                //if the part does not exist then create it.
                if (modelPart == null)
                {
                    modelPart = new MeshPart3DData(this);
                    modelPart.Material = new ModelMaterial3D(ModelColour.Black, ModelMaterial3D.FillPatternType.Solid, null);
                    modelPart.Size = _subComponentPart.Size;
                    AddPart(modelPart);
                    _notchesPart = modelPart;
                }

                modelPart.Builder = meshInstructions;
            }
            else
            {
                //remove any existing part
                if (_notchesPart != null)
                {
                    RemovePart(_notchesPart);
                    _notchesPart = null;
                }

                if (this.ShowNotches && this.SupressPerformanceIntensiveUpdates)
                {
                    //flag the change
                    _isNotchesChanged = true;
                }
            }

        }

        /// <summary>
        /// Returns mesh instructions to create notches based on the given values.
        /// </summary>
        public static MeshBuilderInstructions CreateNotchPartInstructions(
            Boolean generateNormals, Boolean generateTextures,
            Model.PlanogramSubComponentNotchStyleType notchStyleType,
            Boolean isNotchPlacedOnFront,
            Single notchStartX, Single notchStartY, Single notchSpacingX, Single notchSpacingY,
            Single notchWidth, Single notchHeight,
            WidthHeightDepthValue subSize)
        {
            Boolean hasNotches =
                (notchHeight > 0 && notchWidth > 0
                && notchSpacingY > 0
                   && (isNotchPlacedOnFront /*|| subComponent.IsNotchPlacedOnBack
                        || subComponent.IsNotchPlacedOnLeft || subComponent.IsNotchPlacedOnRight*/));

            if (!hasNotches) return null;


            MeshBuilderInstructions meshInstructions = new MeshBuilderInstructions(generateNormals, generateTextures);


            Boolean isDoubleNotch =
                 notchStyleType == Model.PlanogramSubComponentNotchStyleType.DoubleCircle
                 || notchStyleType == Model.PlanogramSubComponentNotchStyleType.DoubleRectangle
                 || notchStyleType == Model.PlanogramSubComponentNotchStyleType.DoubleSquare;


            Boolean[] includeNotches =
                new Boolean[6]
                        {
                            isNotchPlacedOnFront,
                            false,//subComponent.IsNotchPlacedOnBack,
                            false, //no top notches
                            false, //no bottom notches
                            false,//subComponent.IsNotchPlacedOnLeft,
                            false,//subComponent.IsNotchPlacedOnRight
                        };


            for (Int32 i = 0; i < 6; i++)
            {
                if (includeNotches[i])
                {
                    //Single notchHeight = subComponent.NotchHeight;
                    //Single notchWidth = subComponent.NotchWidth;
                    const Single notchDepth = 0.1F;

                    Single maxX = 0;
                    Single maxY = 0;

                    switch (i)
                    {
                        case 0: //front
                            maxX = subSize.Width;
                            maxY = subSize.Height;
                            break;

                        case 1: //back
                            maxX = subSize.Width;
                            maxY = subSize.Height;
                            break;

                        case 4: //left
                            maxX = subSize.Depth;
                            maxY = subSize.Height;
                            break;

                        case 5: //right
                            maxX = subSize.Depth;
                            maxY = subSize.Height;
                            break;

                        default: continue;
                    }


                    for (Int32 row = 1; row <= 2; row++)
                    {
                        Single currX = notchStartX + (notchWidth / 2.0F);
                        Single currZ = subSize.Depth;

                        switch (i)
                        {
                            case 0: //front
                                currX = notchStartX + (notchWidth / 2.0F);
                                currZ = subSize.Depth;
                                break;

                            case 1: //back
                                currX = notchStartX + (notchWidth / 2.0F);
                                currZ = 0;
                                break;

                            case 4: //left
                                currX = notchStartX + (notchWidth / 2.0F);
                                currZ = 0;
                                break;

                            case 5: //right
                                currX = notchStartX + (notchWidth / 2.0F);
                                currZ = subSize.Width;
                                break;
                        }



                        Single row1End = currX + (notchWidth / 2.0F);
                        if (isDoubleNotch)
                        {
                            row1End += (notchWidth + notchSpacingX);
                        }

                        if (row == 1)
                        {
                            if (row1End > maxX) { break; } //hangs off edge.
                        }
                        else if (row == 2)
                        {
                            //set row 2 start
                            currX = maxX - notchStartX - (notchWidth / 2.0F);
                            if (isDoubleNotch)
                            {
                                currX = currX - notchWidth - notchSpacingX;
                            }

                            if (row1End > currX) { break; } //overlaps first row
                        }


                        Single notch0 = notchStartY - notchSpacingY;
                        Int32 notchNumber = 1;
                        Single currY;

                        while (true)
                        {
                            currY = notch0 + (notchNumber * notchSpacingY);

                            if (currY < maxY && ((currY - notchHeight) < maxY))
                            {
                                //determine the notch centers
                                PointValue[] notchCenters = (isDoubleNotch) ? new PointValue[2] : new PointValue[1];

                                PointValue center1 = new PointValue(currX, currY - notchHeight / 2.0F, currZ);
                                if (i == 4 || i == 5)
                                {
                                    center1 = new PointValue(currZ, currY - notchHeight / 2.0F, currX);
                                }
                                notchCenters[0] = center1;

                                if (isDoubleNotch)
                                {
                                    Single notch2X = currX + notchWidth + notchSpacingX;
                                    PointValue center2 = new PointValue(notch2X, currY - notchHeight / 2.0F, currZ);
                                    if (i == 4 || i == 5)
                                    {
                                        center2 = new PointValue(currZ, currY - notchHeight / 2.0F, notch2X);
                                    }
                                    notchCenters[1] = center2;
                                }


                                //add the notches
                                foreach (PointValue c in notchCenters)
                                {
                                    switch (notchStyleType)
                                    {
                                        case Model.PlanogramSubComponentNotchStyleType.DoubleCircle:
                                        case Model.PlanogramSubComponentNotchStyleType.SingleCircle:
                                            meshInstructions.AddEllipsoid(c, notchWidth / 2.0, notchWidth/*notchHeight*/ / 2.0, notchDepth / 2.0);
                                            break;

                                        case Model.PlanogramSubComponentNotchStyleType.DoubleRectangle:
                                        case Model.PlanogramSubComponentNotchStyleType.SingleRectangle:
                                            meshInstructions.AddBoxFaces(c, notchWidth, notchHeight, notchDepth, BoxFaceType.All);
                                            break;

                                        case Model.PlanogramSubComponentNotchStyleType.DoubleSquare:
                                        case Model.PlanogramSubComponentNotchStyleType.SingleSquare:
                                            meshInstructions.AddBoxFaces(c, notchWidth, notchWidth/*notchHeight*/, notchDepth, BoxFaceType.All);
                                            break;

                                        case Model.PlanogramSubComponentNotchStyleType.SingleH:
                                            {
                                                PointValue v1 = new PointValue(c.X - (notchWidth / 2F) + ((notchWidth / 3F) / 2F), c.Y, c.Z);
                                                PointValue v2 = new PointValue(c.X + (notchWidth / 2F) - ((notchWidth / 3F) / 2F), c.Y, c.Z);

                                                meshInstructions.AddBoxFaces(v1, notchWidth / 3.0, notchHeight, notchDepth, BoxFaceType.All);
                                                meshInstructions.AddBoxFaces(c, notchWidth, notchHeight / 3.0, notchDepth, BoxFaceType.All);
                                                meshInstructions.AddBoxFaces(v2, notchWidth / 3.0, notchHeight, notchDepth, BoxFaceType.All);
                                            }
                                            break;

                                        default:
                                            Debug.Fail("SubComponentNotchStyleType not recognised");
                                            meshInstructions.AddBoxFaces(c, notchWidth, notchHeight, notchDepth, BoxFaceType.All);
                                            break;
                                    }
                                }


                                notchNumber++;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }

                }
            }

            return meshInstructions;
        }

        #endregion

        /// <summary>
        /// Returns an array of facing materials for this subcomponent.
        /// </summary>
        /// <returns></returns>
        private Dictionary<BoxFaceType, ModelMaterial3D> GetBoxFaceMaterials()
        {
            var subComponent = this.SubComponent;

            Byte[] frontImageData = null;
            Byte[] backImageData = null;
            Byte[] topImageData = null;
            Byte[] bottomImageData = null;
            Byte[] leftImageData = null;
            Byte[] rightImageData = null;

            if (this.ShowImages)
            {
                frontImageData = subComponent.FrontImageData;
                backImageData = subComponent.BackImageData;
                topImageData = subComponent.TopImageData;
                bottomImageData = subComponent.BottomImageData;
                leftImageData = subComponent.LeftImageData;
                rightImageData = subComponent.RightImageData;
            }

            var frontPattern = ModelMaterial3D.FillPatternType.Solid;
            var backPattern = ModelMaterial3D.FillPatternType.Solid;
            var topPattern = ModelMaterial3D.FillPatternType.Solid;
            var bottomPattern = ModelMaterial3D.FillPatternType.Solid;
            var leftPattern = ModelMaterial3D.FillPatternType.Solid;
            var rightPattern = ModelMaterial3D.FillPatternType.Solid;

            if (this.ShowFillPatterns)
            {
                frontPattern = ToFillPatternType(subComponent.FillPatternTypeFront);
                backPattern = ToFillPatternType(subComponent.FillPatternTypeBack);
                topPattern = ToFillPatternType(subComponent.FillPatternTypeTop);
                bottomPattern = ToFillPatternType(subComponent.FillPatternTypeBottom);
                leftPattern = ToFillPatternType(subComponent.FillPatternTypeLeft);
                rightPattern = ToFillPatternType(subComponent.FillPatternTypeRight);
            }


            Int32 transparency;

            ModelColour frontColour = ModelColour.IntToColor(subComponent.FillColourFront);
            transparency = subComponent.TransparencyPercentFront;
            frontColour.Alpha = CalculateTransparency(transparency);

            ModelColour backColour = ModelColour.IntToColor(subComponent.FillColourBack);
            transparency = subComponent.TransparencyPercentBack;
            backColour.Alpha = CalculateTransparency(transparency);

            ModelColour topColour = ModelColour.IntToColor(subComponent.FillColourTop);
            transparency = subComponent.TransparencyPercentTop;
            topColour.Alpha = CalculateTransparency(transparency);

            ModelColour bottomColour = ModelColour.IntToColor(subComponent.FillColourBottom);
            transparency = subComponent.TransparencyPercentBottom;
            bottomColour.Alpha = CalculateTransparency(transparency);

            ModelColour leftColour = ModelColour.IntToColor(subComponent.FillColourLeft);
            transparency = subComponent.TransparencyPercentLeft;
            leftColour.Alpha = CalculateTransparency(transparency);

            ModelColour rightColour = ModelColour.IntToColor(subComponent.FillColourRight);
            transparency = subComponent.TransparencyPercentRight;
            rightColour.Alpha = CalculateTransparency(transparency);

            Dictionary<BoxFaceType, ModelMaterial3D> faceMaterials = new Dictionary<BoxFaceType, ModelMaterial3D>()
                {
                    {BoxFaceType.Front, new ModelMaterial3D(frontColour, frontPattern, frontImageData)},
                    {BoxFaceType.Back, new ModelMaterial3D(backColour, backPattern, backImageData)},
                    {BoxFaceType.Top, new ModelMaterial3D(topColour, topPattern, topImageData)},
                    {BoxFaceType.Bottom, new ModelMaterial3D(bottomColour, bottomPattern, bottomImageData)},
                    {BoxFaceType.Left, new ModelMaterial3D(leftColour, leftPattern, leftImageData)},
                    {BoxFaceType.Right, new ModelMaterial3D(rightColour, rightPattern, rightImageData)}
                };

            return faceMaterials;
        }

        private Byte CalculateTransparency(Int32 transparency)
        {
            // If the fixtures are to be hidden, return an alpha of 0. This will ensure that the model data
            // is not rendered. We do this, rather than setting the IsVisible property, to ensure that the child
            // models of this item are still rendered (positions, labels, etc.).
            if (!_settingsController.ShowFixtures) return 0;
            return Convert.ToByte((transparency > 0) ? 255 - Math.Round((255 / 100.0) * transparency) : 255);
        }

        /// <summary>
        /// Returns a dictionary of resolved face thicknesses.
        /// </summary>
        /// <returns></returns>
        private Dictionary<BoxFaceType, Single> GetFaceThicknesses()
        {
            var sub = this.SubComponent;

            var faceThicknesses = new Dictionary<BoxFaceType, Single>()
                {
                    {BoxFaceType.Front, Math.Max(0, sub.FaceThicknessFront)},
                    {BoxFaceType.Back, Math.Max(0,sub.FaceThicknessBack)},
                    {BoxFaceType.Top, Math.Max(0,sub.FaceThicknessTop)},
                    {BoxFaceType.Bottom, Math.Max(0,sub.FaceThicknessBottom)},
                    {BoxFaceType.Left, Math.Max(0,sub.FaceThicknessLeft)},
                    {BoxFaceType.Right, Math.Max(0,sub.FaceThicknessRight)}
                };


            //if all values are over 0 then none will be applied so just reset them.
            if (faceThicknesses.Values.All(f => f > 0))
            {
                foreach (var key in faceThicknesses.Keys)
                {
                    faceThicknesses[key] = 0;
                }
            }

            return faceThicknesses;
        }

        #endregion

        #region Child Model Methods

        /// <summary>
        /// Adds a new child for the given Position view
        /// </summary>
        /// <param name="assemblyView"></param>
        private void AddPositionModel(IPlanPositionRenderable view)
        {
            PlanPosition3DData model = new PlanPosition3DData(this, view, _settingsController);
            AddChild(model);
            _posModelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the child model for the given Position view.
        /// </summary>
        /// <param name="assemblyView"></param>
        private void RemovePositionModel(IPlanPositionRenderable view)
        {
            PlanPosition3DData model;

            if (_posModelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _posModelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Adds a new child for the given annotation view.
        /// </summary>
        /// <param name="view"></param>
        private void AddAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model = new PlanAnnotation3DData(view, _settingsController);
            AddChild(model);
            _annoModelsDict.Add(view, model);
        }

        /// <summary>
        /// Removes the model for the given view.
        /// </summary>
        /// <param name="view"></param>
        private void RemoveAnnotationModel(IPlanAnnotationRenderable view)
        {
            PlanAnnotation3DData model;

            if (_annoModelsDict.TryGetValue(view, out model))
            {
                RemoveChild(model);
                _annoModelsDict.Remove(view);
            }
        }

        /// <summary>
        /// Enumerates through child annotation data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanAnnotation3DData> EnumerateAnnotationModels()
        {
            foreach (var entry in _annoModelsDict)
            {
                yield return entry.Value;
            }
        }

        /// <summary>
        /// Enumerates through child position data.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanPosition3DData> EnumeratePositionModels()
        {
            foreach (var entry in _posModelsDict)
            {
                yield return entry.Value;
            }
        }

        #endregion

        #region Helpers

        private static ModelMaterial3D.FillPatternType ToFillPatternType(PlanogramSubComponentFillPatternType prodFillPattern)
        {
            switch (prodFillPattern)
            {
                case PlanogramSubComponentFillPatternType.Solid: return ModelMaterial3D.FillPatternType.Solid;
                case PlanogramSubComponentFillPatternType.Border: return ModelMaterial3D.FillPatternType.Border;
                case PlanogramSubComponentFillPatternType.Crosshatch: return ModelMaterial3D.FillPatternType.Crosshatch;
                case PlanogramSubComponentFillPatternType.DiagonalDown: return ModelMaterial3D.FillPatternType.DiagonalDown;
                case PlanogramSubComponentFillPatternType.DiagonalUp: return ModelMaterial3D.FillPatternType.DiagonalUp;
                case PlanogramSubComponentFillPatternType.Horizontal: return ModelMaterial3D.FillPatternType.Horizontal;
                case PlanogramSubComponentFillPatternType.Vertical: return ModelMaterial3D.FillPatternType.Vertical;
                case PlanogramSubComponentFillPatternType.Dotted: return ModelMaterial3D.FillPatternType.Dotted;

                default:
                    Debug.Fail("Product Fill Pattern type not handled");
                    return ModelMaterial3D.FillPatternType.Solid;
            }
        }

        /// <summary>
        /// Returns true if this has any images available
        /// </summary>
        /// <returns></returns>
        public Boolean HasAvailableImages()
        {
            if (this.SubComponent == null) return false;

            //always return false if images are off as we don't want to trigger
            // a load by accident.
            if (!_settingsController.ShowFixtureImages) return false;

            return
                this.SubComponent.FrontImageData != null
                || this.SubComponent.BackImageData != null
                || this.SubComponent.LeftImageData != null
                || this.SubComponent.RightImageData != null
                || this.SubComponent.TopImageData != null
                || this.SubComponent.BottomImageData != null;
        }

        #endregion

        #endregion

        #region IUpdatable Members

        public Boolean IsUpdating { get; private set; }

        private void BeginUpdate()
        {
            IsUpdating = true;
        }

        private void EndUpdate()
        {
            IsUpdating = false;

            if (_isPartChanged)
            {
                OnPartChanged();
            }
            if (_isMaterialChanged)
            {
                OnMaterialChanged();
            }

            if (_isRiserPartChanged)
            {
                OnRiserPartChanged();
            }
            if (_isRiserMaterialChanged)
            {
                OnRiserMaterialChanged();
            }

            if (_isNotchesChanged)
            {
                OnNotchesChanged();
            }


            if (_isDividerLinesChanged)
            {
                OnDividerLinesChanged();
            }

            if (_isPegholesChanged)
            {
                OnPegholesChanged();
            }

            if (_isPlacementChanged)
            {
                OnPlacementChanged();
            }

            if (_isVisibilityChanged)
            {
                OnVisibilityChanged();
            }

            if (_isDividersChanged)
            {
                OnDividersChanged();
            }
        }

        private void OnIsUpdatingChanged()
        {
            if (this.SubComponent.IsUpdating && !IsUpdating)
            {
                BeginUpdate();
            }
            else if (!this.SubComponent.IsUpdating && IsUpdating)
            {
                EndUpdate();
            }
        }

        #region OnChanged Methods

        private void OnPartChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isPartChanged = false;
                UpdateParts();
            }
            else
            {
                //flag the change
                _isPartChanged = true;
            }
        }

        private void OnMaterialChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isMaterialChanged = false;
                UpdateMaterial();
            }
            else
            {
                //flag the change
                _isMaterialChanged = true;
            }
        }

        private void OnRiserPartChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isRiserPartChanged = false;
                UpdateRiserModel();
            }
            else
            {
                //flag the change
                _isRiserPartChanged = true;
            }
        }

        private void OnRiserMaterialChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isRiserMaterialChanged = false;
                UpdateRiserMaterial();
            }
            else
            {
                //flag the change
                _isRiserMaterialChanged = true;
            }
        }

        private void OnNotchesChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isNotchesChanged = false;
                UpdateNotches();
            }
            else
            {
                //flag the change
                _isNotchesChanged = true;
            }
        }

        private void OnDividerLinesChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isDividerLinesChanged = false;
                UpdateDividerLines();
            }
            else
            {
                //flag the change
                _isDividerLinesChanged = true;
            }
        }

        private void OnPegholesChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isPegholesChanged = false;
                UpdatePegholes();
            }
            else
            {
                //flag the change
                _isPegholesChanged = true;
            }
        }

        private void OnPlacementChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isPlacementChanged = false;
                UpdateModelPlacement();
            }
            else
            {
                //flag the change
                _isPlacementChanged = true;
            }
        }

        private void OnVisibilityChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isVisibilityChanged = false;
                UpdateVisibility();
            }
            else
            {
                //flag the change
                _isVisibilityChanged = true;
            }
        }

        private void OnDividersChanged()
        {
            if (!IsUpdating && !IsPaused)
            {
                _isDividersChanged = false;
                UpdateDividers();
            }
            else
            {
                //flag the change
                _isDividersChanged = true;
            }
        }


        #endregion


        #endregion

        #region IPlanPart3DData Members

        public Object PlanPart
        {
            get { return _subcomponent; }
        }

        /// <summary>
        /// Get/Sets whether this model is paused.
        /// </summary>
        private Boolean IsPaused { get; set; }

        /// <summary>
        /// Stops this model from processing updates.
        /// Instead all changes will be flagged ready to be refreshed when
        /// unpause is called.
        /// </summary>
        void IPlanPart3DData.PauseUpdates()
        {
            IsPaused = true;
            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.PauseUpdates();
            }
        }

        /// <summary>
        /// Lets this model process updates again. An immediate update
        /// will be made for any changes that ocurred during the pause.
        /// </summary>
        void IPlanPart3DData.UnpauseUpdates()
        {
            if (!IsPaused) return;
            IsPaused = false;

            //if we arent flagged as updating then its ok
            // to trigger a refresh.
            if (!IsUpdating) EndUpdate();

            foreach (IPlanPart3DData data in this.ModelChildren)
            {
                data.UnpauseUpdates();
            }
        }

        #endregion

        #region IDisposable Members

        protected override void OnFreeze()
        {
            base.OnFreeze();

            _subcomponent.PositionsCollectionChanged -= SubComponent_PositionsCollectionChanged;
            _subcomponent.AnnotationsCollectionChanged -= SubComponent_AnnotationsCollectionChanged;
            _subcomponent.DividersCollectionChanged -= SubComponent_DividersCollectionChanged;
            _subcomponent.PropertyChanged -= SubComponent_PropertyChanged;
            _settingsController.PropertyChanged -= SettingsController_PropertyChanged;
        }

        public override void Dispose()
        {
            if (!IsDisposed)
            {
                _subcomponent.PositionsCollectionChanged -= SubComponent_PositionsCollectionChanged;
                _subcomponent.AnnotationsCollectionChanged -= SubComponent_AnnotationsCollectionChanged;
                _subcomponent.DividersCollectionChanged -= SubComponent_DividersCollectionChanged;
                _subcomponent.PropertyChanged -= SubComponent_PropertyChanged;
                _settingsController.PropertyChanged -= SettingsController_PropertyChanged;

                DisposeBase();

                GC.SuppressFinalize(this);
                IsDisposed = true;
            }
        }

        #endregion
    }
}
