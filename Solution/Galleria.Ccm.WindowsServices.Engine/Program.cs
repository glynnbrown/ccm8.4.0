﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: SA201
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.ServiceProcess;

namespace Galleria.Ccm.WindowsServices.Engine
{
    /// <summary>
    /// The main application class
    /// </summary>
    internal static class Program
    {
        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(String[] args)
        {
            using (Service service = new Service())
            {
                service.Run(args);
            }
        }
        #endregion
    }
}
