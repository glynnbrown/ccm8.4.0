﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: SA201
// V8-25787 : N.Foster
//  Created
// V8-26429 : J.Pickup
//  RunInstallerAttribute Added.
#endregion
#endregion

using System;
using Galleria.Framework.WindowsServices;
using System.ComponentModel;

namespace Galleria.Ccm.WindowsServices.Engine
{
    [RunInstaller(true)]
    public partial class ServiceInstaller : GalleriaServiceInstallerBase
    {
        #region Properties
        /// <summary>
        /// Returns the service name
        /// </summary>
        public override String ServiceName
        {
            get { return Constants.ServiceName; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ServiceInstaller()
            : base()
        {
            InitializeComponent();
        }
        #endregion
    }
}
