﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM801
// V8-28917 : N.Foster
//  Fixed issue where service cannot shut itself down
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Ccm.Services;
using Galleria.Framework.Engine;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.WindowsServices;

namespace Galleria.Ccm.WindowsServices.Engine
{
    /// <summary>
    /// CCM workflow engine
    /// </summary>
    public partial class Service : GalleriaServiceBase
    {
        #region Constants
        /// <summary>
        /// The amount of time in minutes to wait before recycling the service.
        /// </summary>
        private const Int32 _recycleDelay = 240;
        #endregion

        #region Fields
        private DateTime _recycledTime; // holds the time the service was last recycled
        private Galleria.Ccm.Engine.Engine _engine; // instance of the ccm engine
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Service()
            : base(Constants.ServiceName)
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns the process poll time
        /// </summary>
        protected override Int32 ProcessTime
        {
            get { return 10000; }
        }
        #endregion

        #region Processing
        /// <summary>
        /// Returns true if the service requires recycling
        /// </summary>
        /// <returns></returns>
        protected override Boolean RecycleRequired()
        {
            return (DateTime.UtcNow - _recycledTime).TotalMinutes > _recycleDelay;
        }

        /// <summary>
        /// Called when the process is initialized
        /// </summary>
        protected override void OnProcessInitialization()
        {
            // set-up logging for the service
            if (LoggingHelper.Logger == null)
            {
                LoggingHelper.Logger = new LoggingHelper(this.ServiceName, true);
            }

            // initialize the gfs foundation services client
            FoundationServiceClient.RegisterGFSServiceClientTypes();

            // register the image renderer
            PlanogramImagesHelper.RegisterPlanogramImageRenderer<Galleria.Framework.Planograms.Controls.Wpf.Helpers.PlanogramImageRenderer>();
        }

        /// <summary>
        /// Called when the process is starting up
        /// or after a recycle has occurred
        /// </summary>
        protected override void OnProcessStartup()
        {
            try
            {
                // record the time the service was last recycled
                _recycledTime = DateTime.UtcNow;

                // attempt to authenticate the user
                if (!DomainPrincipal.Authenticate())
                {
                    throw new DomainAuthenticationException();
                }
            }
            catch (DomainAuthenticationException ex)
            {
                this.OnLogException(ex);
            }
            catch (Exception ex)
            {
                this.OnLogException(ex);
            }
        }

        /// <summary>
        /// Performs the main processing, but only if the user has been authenticated.
        /// </summary>
        protected override void OnProcess()
        {
            // attempt to authenticate the user
            // if they weren't already authenticated
            if (!ApplicationContext.User.Identity.IsAuthenticated)
            {
                DomainPrincipal.Authenticate();
            }

            // if the user has been authenticated
            if (ApplicationContext.User.Identity.IsAuthenticated)
            {
                // start the engine if it is not currently running
                if (_engine == null)
                {
                    _engine = new Ccm.Engine.Engine();
                    _engine.StartEngine();
                }
                else
                {
                    // if the engine has shut itself down
                    // then we need to stop the service
                    if (_engine.Status == EngineStatus.Stopped)
                    {
                        this.Stop();
                    }
                }
            }
        }

        /// <summary>
        /// Called when the process is shutting down
        /// or when a recyle is occurring
        /// </summary>
        protected override void OnProcessShutdown()
        {
            // ask the engine to shutdown
            if (_engine != null)
            {
                _engine.StopEngine();
                _engine = null;
            }

            // logout
            DomainPrincipal.Logout();
        }

        #endregion
    }
}
