﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: SA201
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.WindowsServices.Engine
{
    /// <summary>
    /// Internal class used to hold global constants
    /// </summary>
    internal class Constants
    {
        public const String ServiceName = "CCM Processor";
    }
}
