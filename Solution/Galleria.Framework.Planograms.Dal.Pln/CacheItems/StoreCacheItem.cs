﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class StoreCacheItem : DalCacheItem
    {
        #region Constructor

        public StoreCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Store;
            }
        }

        public static String Header
        {
            get
            {
                return Language.StoreCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.StoreCacheItem_Row;
            }
        }

        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> storeCacheSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = storeCacheSchemaDefinition;

            // Get the planogram dto from the collection.
            PlanogramDto planogramDto = GetDto<PlanogramDto>();

            // Load the default row values into an array.
            String storeCacheItemRowDefault = StoreCacheItem.Row;
            this._exportData = storeCacheItemRowDefault.Split(',');

            SetStoreValues(planogramDto);

            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the spaceman store values froma  planogramDto
        /// </summary>
        /// <param name="planogramDto"></param>
        private void SetStoreValues(PlanogramDto planogramDto)
        {
            // Set the values
            SetSpacemanFieldValue(SpacemanFieldHelper.StoreName, planogramDto.Name);

            //Eroneous fields. (Spaceman corrects to theese values more often than not, yet we can't set them from V8).
            SetSpacemanFieldValue(SpacemanFieldHelper.StoreGP_ROII_Calc, 1);
            SetSpacemanFieldValue(SpacemanFieldHelper.StoreUsingOverflowSpace, 0);
        }

        #endregion
    }
}
