﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-32468 : J.Pickup
//  Changed point of purchase to default to unit, to make similar to Apollo exports - added logging.
// V8-32672 : J.Pickup
//  The id of the fixel to which the position belongs now takes the correct id directly from the component dto and is no longer set twice.
// V8-32678 : J.Pickup
//  Peg id now calculated correctly in all scenarios.
// V8-32705  : J.Pickup
// Event log for when product dimensions have not been provided.
// V8-13992  : J.Pickup
// The position -> prod link now uses a resolver, as the mappings set by the user can sometimes effect other fields than the one set due to spaceman file srructure.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class PositionCacheItem : DalCacheItem
    {
        #region constructor

        public PositionCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Position;
            }
        }

        public static String Header
        {
            get
            {
                return Language.PositionCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.PositionCacheItem_Row;
            }
        }

        public Object subcomponentId
        {
            get
            {
                if (this.Dtos.ContainsKey(typeof(PlanogramPositionDto)))
                {
                    return (Dtos[typeof(PlanogramPositionDto)] as PlanogramPositionDto).PlanogramSubComponentId;
                }
                return null;
            }
        }

        public Object ProductId
        {
            get
            {
                if (this.Dtos.ContainsKey(typeof(PlanogramPositionDto)))
                {
                    return (Dtos[typeof(PlanogramPositionDto)] as PlanogramPositionDto).PlanogramProductId;
                }
                return null;
            }
        }

        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> positionSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = positionSchemaDefinition;

            // Load the default row values into an array.
            String positionCacheItemRowDefault = PositionCacheItem.Row;
            this._exportData = positionCacheItemRowDefault.Split(',');

            Boolean isComplex = this._dalCache.ExportMappingContext.AllowComplexPositions;

            #region GetDto's

            PlanogramPositionDto planogramPositionDto = GetDto<PlanogramPositionDto>();
            PlanogramProductDto planogramProductDto = GetDto<PlanogramProductDto>();
            PlanogramSubComponentDto planogramSubComponentDto = GetDto<PlanogramSubComponentDto>();
            PlanogramComponentDto planogramComponentDto = GetDto<PlanogramComponentDto>();

            #endregion

            //Setvalues
            if (planogramPositionDto != null)
            {
                #region Id's

                SetSpacemanFieldValue(SpacemanFieldHelper.PositionID, planogramPositionDto.Id);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionFIXLToPOS, planogramComponentDto.Id);

                //Have to use resolved id as user can map values to product id.
                Object resolvedProductId = planogramPositionDto.PlanogramProductId.ToString();
                this._dalCache.ProductIdResolver.TryGetValue(planogramPositionDto.PlanogramProductId, out resolvedProductId);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionPRODToPOS, resolvedProductId);
                
                #endregion

                #region Coordinates

                Single yOffset = 0;

                if (planogramComponentDto != null)
                {
                    SpacemanComponentType componentType = EncodeSpacemanComponentType(planogramSubComponentDto, planogramComponentDto);
                    switch (componentType)
                    {
                        case SpacemanComponentType.Basket:
                        case SpacemanComponentType.Chest:
                            yOffset = -planogramSubComponentDto.FaceThicknessBottom;
                            break;
                        case SpacemanComponentType.Pallet:
                        case SpacemanComponentType.Shelf:
                            yOffset = -planogramSubComponentDto.Height;
                            break;

                        default:
                            break;
                    }
                }

                //Need to be set based on what item is on...
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionX, planogramPositionDto.X); 
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionY, (planogramPositionDto.Y + yOffset));
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionZ, planogramPositionDto.Z);

                #endregion

                #region Rotation
                
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionSlope, planogramPositionDto.Slope);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionRotation, planogramPositionDto.Angle);

                #endregion

                #region Merch style

                //Set merch style
                if (planogramProductDto != null)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyle, (PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle));
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionMerchStyle, (byte)PlanogramPositionMerchandisingStyle.Unit);
                }

                //Validate merch style
                String merchStyleFriendly;
                SpacemanExportHelper.GetMerchStyleFriendly(planogramPositionDto, planogramProductDto, out merchStyleFriendly);

                if (SpacemanExportHelper.IsNotASpacemanType(planogramPositionDto, planogramProductDto))
                {
                    _dalCache.LogWarning(Language.ExportLog_MerchStyleNotSupported_Description, String.Format(Language.ExportLog_MerchStyleNotSupported, planogramProductDto.Name, merchStyleFriendly), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }
                
                if (!SpacemanExportHelper.MerchStyleIsAllowedInSpaceman(planogramPositionDto, planogramProductDto))
                {
                    _dalCache.LogWarning(Language.ExportLog_ProductDimensionsMissing_Description, String.Format(Language.ExportLog_ProductDimensionsMissing, planogramProductDto.Name, merchStyleFriendly), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }

                #endregion

                #region Number of Facings (inc capping).

                // Main facing
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionVertical, planogramPositionDto.FacingsHigh);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionHorizontal, planogramPositionDto.FacingsWide);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionDepthFacings, planogramPositionDto.FacingsDeep);
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionOrientation, planogramPositionDto.OrientationType);

                //Capping size
                #region  Left/Right Caps

                if (planogramPositionDto.IsXPlacedLeft)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftVertical, planogramPositionDto.FacingsXHigh);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftHorizontal, planogramPositionDto.FacingsXWide);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftDepthFacings, planogramPositionDto.FacingsXDeep);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftOrientation, planogramPositionDto.OrientationTypeX);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyleX));
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightVertical, planogramPositionDto.FacingsXHigh);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightHorizontal, planogramPositionDto.FacingsXWide);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightDepthFacings, planogramPositionDto.FacingsXDeep);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightOrientation, planogramPositionDto.OrientationTypeX);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyleX));
                }

                #endregion

                #region  Top/Bottom Caps

                if (planogramPositionDto.IsYPlacedBottom)
                {
                    //Spaceman does not support bottom capping. 
                    _dalCache.LogWarning(Language.ExportLog_NoBottomCaps_Description, String.Format(Language.ExportLog_NoBottomCaps, planogramProductDto.Name), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopVertical, planogramPositionDto.FacingsYHigh);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopHorizontal, planogramPositionDto.FacingsYWide);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopDepthFacings, planogramPositionDto.FacingsYDeep);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopOrientation, planogramPositionDto.OrientationTypeY);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyleY));

                }

                #endregion

                #region Back/Front Caps

                if (planogramPositionDto.IsZPlacedFront)
                {
                    //Spaceman does not support front capping.
                    _dalCache.LogWarning(Language.ExportLog_NoFrontCaps_Description, String.Format(Language.ExportLog_NoFrontCaps, planogramProductDto.Name), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackVertical, planogramPositionDto.FacingsZHigh);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackHorizontal, planogramPositionDto.FacingsZWide);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackDepthFacings, planogramPositionDto.FacingsZDeep);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackOrientation, planogramPositionDto.OrientationTypeZ);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyleZ));
                }

                #endregion

                #endregion

                #region Autofill Peg

                SetSpacemanFieldValue(SpacemanFieldHelper.PositionAutoFillPeg, 0);

                #endregion

                #region Orientation

                //Main Orientation
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionOrientation, (byte)SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationType, (PlanogramProductOrientationType)planogramProductDto.OrientationType));

                //Capping Orientations               
                #region  Left/Right Caps
                if (planogramPositionDto.IsXPlacedLeft)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionLeftOrientation, (byte)SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationTypeX, (PlanogramProductOrientationType)planogramPositionDto.OrientationTypeX));
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightOrientation, (byte)SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationTypeX, (PlanogramProductOrientationType)planogramPositionDto.OrientationTypeX));
                }
                #endregion

                #region  Top/Bottom Caps

                if (planogramPositionDto.IsYPlacedBottom)
                {
                    //Spaceman does not support a bottom cap.
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopOrientation, (byte)SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationTypeY, (PlanogramProductOrientationType)planogramPositionDto.OrientationTypeY));
                }

                #endregion

                #region  Back/Front Caps

                if (planogramPositionDto.IsZPlacedFront)
                {
                    //Never place at the front. 
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackOrientation, (byte)SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationTypeZ, (PlanogramProductOrientationType)planogramPositionDto.OrientationTypeZ));
                }

                #endregion

                #endregion
            }

            if (planogramSubComponentDto != null)
            {
                #region Adjust values based on type

                SpacemanComponentType componentType = EncodeSpacemanComponentType(planogramSubComponentDto, planogramComponentDto);
                if (componentType == SpacemanComponentType.Peg || componentType == SpacemanComponentType.Bar)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionX, planogramPositionDto.X + planogramProductDto.PegProngOffsetX);
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionPegID, _dalCache.PegLibrary.GetPegId(this));
                }

                if (componentType == SpacemanComponentType.Bar)
                {
                    //There is a diference between v8s placement of positions relative to the bar compared to spaceman
                    _dalCache.LogWarning(Language.ExportLog_RelativePositioning_Description, String.Format(Language.ExportLog_RelativePositioning_Description,  planogramProductDto.Name), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }

                #endregion
            }

            StringBuilder positionStringBuilder = new StringBuilder();

            #region Complex position handling.

            //Once all values are set we then evaluate non-complex position specific values and react accordingly.
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionComplex, isComplex);

            if (!isComplex && planogramPositionDto != null)
            {
                #region CappingStyle

                //Top caps we apply a capping style to achieve capping.
                if (!planogramPositionDto.IsYPlacedBottom
                    && planogramPositionDto.FacingsYHigh != 0
                    && planogramPositionDto.FacingsYWide != 0
                    && planogramPositionDto.FacingsYDeep != 0)
                {
                    if(planogramPositionDto.FacingsHigh == 1 && planogramPositionDto.FacingsYHigh > 1)
                    {
                        //max cap
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, (byte)SpacemanCappingStyleType.MaxCap);
                    }
                    else if (planogramPositionDto.FacingsYHigh == 1)
                    {
                        //Min cap
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, (byte)SpacemanCappingStyleType.MinCap);
                    }
                    else 
                    {
                        //med cap 
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, (byte)SpacemanCappingStyleType.MedCap);
                    }
                }
                else
                {
                    //No Cap
                    SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, (byte)SpacemanCappingStyleType.NoCap);
                }

                #endregion

                #region Fixed 

                //We don't apply as fixed as messes up capping availablilty.
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionFixed, false);

                #endregion

                #region Left / Right Capping Positions

                //Left/Right caps we apply as  new positions
                String[] _nonComplexCappingPosition = new String[_exportData.Length];

                if (planogramPositionDto != null && planogramProductDto != null)
                {
                    if (planogramPositionDto.FacingsXHigh != 0
                        && planogramPositionDto.FacingsXWide != 0
                        && planogramPositionDto.FacingsXDeep != 0)
                    {
                        //We have caps that need splitting into seperate positons...

                        //Take a copy of exisiting position
                        _exportData.CopyTo(_nonComplexCappingPosition, 0);

                        //Take existing position and switch x cap into the main facing. 
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionVertical, planogramPositionDto.FacingsXHigh, _nonComplexCappingPosition);
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionHorizontal, planogramPositionDto.FacingsXWide, _nonComplexCappingPosition);
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionDepthFacings, planogramPositionDto.FacingsXDeep, _nonComplexCappingPosition);

                        //Change orientation
                        SpacemanOrientationtype spacemanXOrientation = SpacemanExportHelper.EncodeOrientation((PlanogramPositionOrientationType)planogramPositionDto.OrientationTypeX, (PlanogramProductOrientationType)planogramProductDto.OrientationType); //Capping cannot have second orientation so doesn't matter what we pass.
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionOrientation, spacemanXOrientation, _nonComplexCappingPosition);

                        //Need to apply merchandising style
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionMerchStyle, SpacemanExportHelper.EncodeMerchandisingStyle((PlanogramPositionMerchandisingStyle)planogramPositionDto.MerchandisingStyleX, (PlanogramProductMerchandisingStyle)planogramProductDto.MerchandisingStyle), _nonComplexCappingPosition);

                        //Remove capping for cap
                        SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, SpacemanCappingStyleType.NoCap, _nonComplexCappingPosition);

                        //Need to either place after position if placed right. Otherwise we will need to leave in place but adjust main positions location.
                        float xCapWidth = 0;
                        if (!planogramPositionDto.IsXPlacedLeft)
                        {
                            //Set x afterwards (caps right)
                            xCapWidth = PositionPlacementExportHelper.GetRightCapBlockWidth(planogramPositionDto, planogramProductDto);
                            SetSpacemanFieldValue(SpacemanFieldHelper.PositionX, planogramPositionDto.X + xCapWidth, _nonComplexCappingPosition);
                        }
                        else
                        {
                            //Caps left. Leave in current location but adjust the x location of main facing accross by the capping size.
                            xCapWidth = PositionPlacementExportHelper.GetLeftCapBlockWidth(planogramPositionDto, planogramProductDto);
                            SetSpacemanFieldValue(SpacemanFieldHelper.PositionX, planogramPositionDto.X + xCapWidth);
                        }
                    }
                }

                #endregion

                //We do not do noncomplex back caps so throw warning log to let know they have been droped if exist.
                positionStringBuilder.AppendLine((String.Join(",", _nonComplexCappingPosition)));
            }

            #endregion

            #region Spaceman Calculated / Oddities.
            //This region contains properties that are either calculated by spaceman or are unknown.
            //Therefore try to initialise as 0 / empty values.

            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFixed, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionCapStyle, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionTotalFacings, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionTotalUnits, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionVertSpace, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionHorizSpace, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionDepthSpace, "0");
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFHeight, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFWidth, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFDepth, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFHeight, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFCaphigh, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFCaphigh, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFCapdeep, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFNestdeep, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionFNesthigh, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackDepthCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackHorizCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionBackVertCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightDepthCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightHorizCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionRightVertCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopDepthCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopHorizCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionTopVertCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionDepthCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionHorizCrush, 0.0);
            SetSpacemanFieldValue(SpacemanFieldHelper.PositionVertCrush, 0.0);


            if (planogramProductDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionFillColor, SpacemanExportHelper.EncodeColour(planogramProductDto.FillColour));
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionLabel, planogramProductDto.Name);
            }

            if (planogramComponentDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.PositionSECTToFIXL, planogramComponentDto.PlanogramId);
            }

            #endregion

            //Make sure string builder contains our main position record.
            positionStringBuilder.Append((String.Join(",", _exportData)));

            // Return the array as a single csv string.
            return positionStringBuilder.ToString();
        }
       
        #endregion
    }

}
