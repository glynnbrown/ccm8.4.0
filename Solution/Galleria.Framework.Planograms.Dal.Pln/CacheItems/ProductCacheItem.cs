﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-13992  : J.Pickup
// The position -> prod link now uses a resolver, as the mappings set by the user can sometimes effect other fields than the one set due to spaceman file srructure.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class ProductCacheItem : DalCacheItem
    {
        #region Constructor

        public ProductCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Product;
            }
        }

        public static String Header
        {
            get
            {
                return Language.ProductCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.ProductCacheItem_Row;
            }
        }

        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> productSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = productSchemaDefinition;

            // Load the default row values into an array.
            String productCacheItemRowDefault = ProductCacheItem.Row;
            this._exportData = productCacheItemRowDefault.Split(',');

            #region GetDto's

            PlanogramProductDto planogramProductDto = GetDto<PlanogramProductDto>();

            #endregion
            
            if (planogramProductDto != null)
            {
                SetProductValues(planogramProductDto);

                ApplyExportMappings(planogramProductDto);
                ApplyPerformanceDataMappings();
                this.CalculateIdResolver(planogramProductDto);
            }
            
            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Uses reflection to set the appropriate mapping for performance data.
        /// </summary>
        private void ApplyPerformanceDataMappings()
        {
            if (!this._dalCache.ExportMappingContext.IncludePerformanceData) return;

            PlanogramPerformanceDataDto performanceDto = GetDto<PlanogramPerformanceDataDto>();
            if (performanceDto != null)
            {
                foreach (var mapping in this._dalCache.ExportMappingContext.MetricMappings)
                {
                    PropertyInfo property = typeof(PlanogramPerformanceDataDto).GetProperty(String.Format("P{0}", mapping.MetricId));

                    if (property == null) continue;
                    SetSpacemanFieldValue(mapping.Source, property.GetValue(performanceDto, null));
                }
            }
        }
        
        /// <summary>
        /// Sets product values based on a CCM Product dto
        /// </summary>
        /// <param name="planogramProductDto"></param>
        private void SetProductValues(PlanogramProductDto planogramProductDto)
        {
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductID, planogramProductDto.Id);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUPC, planogramProductDto.Gtin);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductName, planogramProductDto.Name);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductHeight, planogramProductDto.Height);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductWidth, planogramProductDto.Width);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductDepth, planogramProductDto.Depth);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductDisplayHeight, planogramProductDto.DisplayHeight);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductDisplayWidth, planogramProductDto.DisplayWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductDisplayDepth, planogramProductDto.DisplayDepth);

            //Peg1Depth is not PegDepth, it's the distance from the peg hole to the back of the product
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg1Depth, planogramProductDto.Depth);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg1Right, planogramProductDto.PegX);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg2Right, planogramProductDto.PegX2);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg3Right, planogramProductDto.PegX3);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg1Down, planogramProductDto.PegY);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg2Down, planogramProductDto.PegY2);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPeg3Down, planogramProductDto.PegY3);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsCase, planogramProductDto.CasePackUnits);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsCaseHigh, planogramProductDto.CaseHigh);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsCaseWide, planogramProductDto.CaseWide);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsCaseDeep, planogramProductDto.CaseDeep);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductCaseHeight, planogramProductDto.CaseHeight);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductCaseWidth, planogramProductDto.CaseWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductCaseDepth, planogramProductDto.CaseDepth);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMaxHigh, planogramProductDto.MaxStack);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMinDeep, planogramProductDto.MinDeep);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMaxDeep, planogramProductDto.MaxDeep);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsTray, planogramProductDto.TrayPackUnits);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductStdOrient, planogramProductDto.OrientationType);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMerchStyle, planogramProductDto.MerchandisingStyle);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMaxVertCrush, (1 - planogramProductDto.SqueezeHeight));
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMaxHorizCrush, (1 - planogramProductDto.SqueezeWidth));
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductMaxDepthCrush, (1 - planogramProductDto.SqueezeDepth));

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductVertNest, planogramProductDto.NestingHeight);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductHorizNest, planogramProductDto.NestingWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductDepthNest, planogramProductDto.NestingDepth);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsTrayHigh, planogramProductDto.TrayHigh);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsTrayWide, planogramProductDto.TrayWide);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductUnitsTrayDeep, planogramProductDto.TrayDeep);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductTrayHeight, planogramProductDto.TrayHeight);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductTrayWidth, planogramProductDto.TrayWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductTrayDepth, planogramProductDto.TrayDepth);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductColour, SpacemanExportHelper.EncodeColour(planogramProductDto.FillColour));
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductFillPattern, SetFillPatternType(planogramProductDto.FillPatternType));

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductHorizGap, planogramProductDto.FingerSpaceToTheSide);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductVertGap, planogramProductDto.FingerSpaceAbove);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPegRight, planogramProductDto.PegX);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductPegDown, planogramProductDto.PegY);
            SetSpacemanFieldValue(SpacemanFieldHelper.ProductColourIsClear, false);

            SetSpacemanFieldValue(SpacemanFieldHelper.ProductCostBasis, (Byte)SpacemanCostBasisType.Unit);
        }

        /// <summary>
        /// Creates a resolved dictionary of the product id from CCM against the id the user set.
        /// </summary>
        /// <param name="planogramProductDto"></param>
        private void CalculateIdResolver(PlanogramProductDto planogramProductDto)
        {
            // We need to be able to pass the ID back up as the position will need to reference it if was altered in export mappings.
            // This is a special case due to the way id's are mapped in spaceman.
            String setIdFromMapping = GetSpacemanFieldValue(SpacemanFieldHelper.ProductID);
            setIdFromMapping = setIdFromMapping.Replace("\"", String.Empty);

            if (!_dalCache.ProductIdResolver.ContainsKey(planogramProductDto.Id))
            {
                _dalCache.ProductIdResolver.Add(planogramProductDto.Id, setIdFromMapping);
            }
        }

        #endregion
    }

}
