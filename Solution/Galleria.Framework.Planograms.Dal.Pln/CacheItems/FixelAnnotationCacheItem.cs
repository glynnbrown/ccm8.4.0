﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-32699  : J.Pickup
//  Removed uneccessary double null check and removed a unrequired check within set coordinates that unecessariliy prevented method from executing.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;
using System.Text;


namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class FixelAnnotationCacheItem : DalCacheItem
    {
        #region Contstructor

        public FixelAnnotationCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Fixel;
            }
        }

        public static String Header
        {
            get
            {
                return Language.FixelCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.FixelCacheItem_Row;
            }
        }

        public Object sectionId
        {
            get
            {
                if (this.Dtos.ContainsKey(typeof(PlanogramFixtureComponentDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId;
                }
                else if (this.Dtos.ContainsKey(typeof(PlanogramFixtureAssemblyDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureAssemblyDto)] as PlanogramFixtureAssemblyDto).PlanogramFixtureId;
                }

                return null;
            }
        }

        public Object subCompnentId
        {
            get
            {
                if (this.DtoIds.ContainsKey(typeof(PlanogramSubComponentDto)))
                {
                    return DtoIds[typeof(PlanogramSubComponentDto)];
                }
                return null;
            }
        }

        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> fixelSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            StringBuilder output = new StringBuilder();

            this._localSchemaDefinition = fixelSchemaDefinition;

            // Attempt to aquire reference to all required dtos.
            #region GetDto's

            PlanogramAnnotationDto planogramAnnotationDto = GetDto<PlanogramAnnotationDto>();
            if (planogramAnnotationDto == null) return String.Empty;

            PlanogramFixtureAssemblyDto planogramFixtureAssemblyDto = GetDto<PlanogramFixtureAssemblyDto>(planogramAnnotationDto.PlanogramFixtureAssemblyId);
            PlanogramFixtureComponentDto planogramFixtureComponentDto = GetDto<PlanogramFixtureComponentDto>(planogramAnnotationDto.PlanogramFixtureComponentId);
            PlanogramAssemblyComponentDto planogramAssemblyComponentDto = GetDto<PlanogramAssemblyComponentDto>(planogramAnnotationDto.PlanogramAssemblyComponentId);
            PlanogramSubComponentDto planogramSubComponentDto = GetDto<PlanogramSubComponentDto>(planogramAnnotationDto.PlanogramSubComponentId);
            PlanogramFixtureItemDto planogramFixtureItemDto = GetDto<PlanogramFixtureItemDto>(planogramAnnotationDto.PlanogramFixtureItemId);
            PlanogramComponentDto planogramComponentDto = GetDto<PlanogramComponentDto>();

            #endregion

            // Load the default row values into an array.
            String fixelCacheItemRowDefault = FixelCacheItem.Row;
            this._exportData = fixelCacheItemRowDefault.Split(',');

            SetBasicAnnotationProperties(planogramAnnotationDto);
            CreateNewFontRecordForFixel(planogramAnnotationDto, output);
            SetFixelCoordinates(planogramFixtureComponentDto, planogramAssemblyComponentDto, planogramSubComponentDto, planogramAnnotationDto, planogramFixtureAssemblyDto, planogramFixtureItemDto, planogramComponentDto);
            SetSpacmanComponentType();
            SetFixelName(planogramSubComponentDto);
            SetFixelSegment(planogramFixtureComponentDto, planogramComponentDto, planogramFixtureItemDto);

            output.AppendLine((String.Join(",", _exportData)));

            // Return the array as a single csv string.
            return output.ToString();
        }

        #region Methods

        private void SetFixelCoordinates(PlanogramFixtureComponentDto planogramFixtureComponentDto, 
                                        PlanogramAssemblyComponentDto planogramAssemblyComponentDto, 
                                        PlanogramSubComponentDto planogramSubComponentDto,
                                        PlanogramAnnotationDto planogramAnnotationDto,
                                        PlanogramFixtureAssemblyDto planogramFixtureAssemblyDto,
                                        PlanogramFixtureItemDto planogramFixtureItemDto,
                                        PlanogramComponentDto planogramComponentDto)
        {
            Single x = 0, y = 0, z = 0;

            if (planogramFixtureComponentDto != null && planogramSubComponentDto != null & planogramSubComponentDto != null)
            {

                x = planogramFixtureComponentDto.X + planogramSubComponentDto.X + planogramAnnotationDto.X ?? 0;
                y = planogramFixtureComponentDto.Y + planogramSubComponentDto.Y + planogramAnnotationDto.Y ?? 0;
                z = planogramFixtureComponentDto.Z + planogramSubComponentDto.Z + planogramAnnotationDto.Z ?? 0;
            }
            else if (planogramAssemblyComponentDto != null && planogramFixtureAssemblyDto != null && planogramSubComponentDto != null)
            {
                x = planogramFixtureAssemblyDto.X + planogramAssemblyComponentDto.X + planogramSubComponentDto.X + planogramAnnotationDto.X ?? 0;
                y = planogramFixtureAssemblyDto.Y + planogramAssemblyComponentDto.Y + planogramSubComponentDto.Y + planogramAnnotationDto.Y ?? 0;
                z = planogramFixtureAssemblyDto.Z + planogramAssemblyComponentDto.Z + planogramSubComponentDto.Z + planogramAnnotationDto.Z ?? 0;
            }
            else
            {
                x = planogramAnnotationDto.X ?? 0;
                y = planogramAnnotationDto.Y ?? 0;
                z = planogramAnnotationDto.Z ?? 0;
            }


            if (planogramFixtureItemDto != null)
            {
                x += planogramFixtureItemDto.X;
                y += planogramFixtureItemDto.Y;
                z += planogramFixtureItemDto.Z;
            }

            SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, x);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelY, y);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelZ, z);
        }

        private void CreateNewFontRecordForFixel(PlanogramAnnotationDto planogramAnnotationDto, StringBuilder output)
        {
            //Font needs to be a seperate record in spaceman file structure. 
            FontCacheItem emptyFontCacheItem = new FontCacheItem(this._dalCache, String.Empty);
            Tuple<String, String> myFontRecord = emptyFontCacheItem.EncodeFont(planogramAnnotationDto, this._dalCache.PackageItem.PackageSchemaDefinitions[DalCacheItemType.Font]);

            if (myFontRecord != null)
            {
                String myFontRow = myFontRecord.Item1;
                String myFontID = myFontRecord.Item2;
                output.AppendLine(myFontRow);

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelFontID, myFontID);
            }
        }

        private void SetBasicAnnotationProperties(PlanogramAnnotationDto planogramAnnotationDto)
        {
            //Ids
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelId, planogramAnnotationDto.Id);

            //Size
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelHeight, planogramAnnotationDto.Height);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, planogramAnnotationDto.Width);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, planogramAnnotationDto.Depth);

            //Textbox / Annotation Related
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelTextField, planogramAnnotationDto.Text);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelLabel, planogramAnnotationDto.Text);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelTextboxReduceToFit, planogramAnnotationDto.CanReduceFontToFit);

            //Colour
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelFillColor, SpacemanExportHelper.EncodeColour(planogramAnnotationDto.BackgroundColour));
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelColour, SpacemanExportHelper.EncodeColour(planogramAnnotationDto.BackgroundColour));
        }

        private void SetFixelName(PlanogramSubComponentDto planogramSubComponentDto)
        {
            if (planogramSubComponentDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelName, planogramSubComponentDto.Name);
            }
            else
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelName, Language.FixelAnnotationCacheItem_Textbox);
            }
        }

        private void SetFixelSegment(PlanogramFixtureComponentDto planogramFixtureComponentDto, PlanogramComponentDto planogramComponentDto, PlanogramFixtureItemDto planogramFixtureItemDto)
        {
            //Start on first segment.
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, 1);

            if (planogramFixtureComponentDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureComponentDto.PlanogramFixtureId);
            }

            if (planogramComponentDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramComponentDto.Name);
            }

            //Attempt to enforce bay sequence as most important solution to the fixel segment value.
            if (planogramFixtureItemDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureItemDto.BaySequenceNumber);
            }
        }

        private void SetSpacmanComponentType()
        {
            //Spaceman currently only has one type of annotation
            SpacemanComponentType componentType = SpacemanComponentType.TextBox;
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelType, componentType);
        }
        
        #endregion

        #endregion
    } 
}
