﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-32470  : J.Pickup
//  Change to prevent the backboard from being to tall - spaceman places differently to v8. (base).
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;
using System.Text;


namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class BayCacheItem : DalCacheItem
    {
        #region Constants

        private Int32 cNotchXIndex = 0;
        private Int32 cBayWidthIndex = 1;
        private Int32 cNotchWidthIndex = 2;
        private Int32 cNotchStartIndex = 3;
        private Int32 cNotchSpacingIndex = 4;

        #endregion

        #region Contstructor

        public BayCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Fixel;
            }
        }

        public static String Header
        {
            get
            {
                return Language.FixelCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.FixelCacheItem_Row;
            }
        }

        public Object sectionId
        {
            get
            {
                if (this.DtoIds.ContainsKey(typeof(PlanogramFixtureDto)))
                {
                    return DtoIds[typeof(PlanogramFixtureDto)];
                }
                else if (this.Dtos.ContainsKey(typeof(PlanogramFixtureItemDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureItemDto)] as PlanogramFixtureItemDto).PlanogramFixtureId;
                }

                return null;
            }
        }

        FixelCacheItem _backboard;
        public FixelCacheItem Backboard
        {
            get { return _backboard; }
            set { _backboard = value; }
        }

        FixelCacheItem _base;
        public FixelCacheItem Base
        {
            get { return _base; }
            set { _base = value; }
        }

        FixelCacheItem _firstBaseInPlan;
        public FixelCacheItem FirstBaseInPlan
        {
            get { return _firstBaseInPlan; }
            set { _firstBaseInPlan = value; }
        }

        private Single? _xPosition;
        public Single XPosition
        {
            get
            {
                if (_xPosition.HasValue)
                {
                    return _xPosition.Value;
                }

                Object objectDto = null;
                if (this.Dtos.TryGetValue(typeof(PlanogramFixtureItemDto), out objectDto))
                {
                    PlanogramFixtureItemDto planogramFixtureItemDto = objectDto as PlanogramFixtureItemDto;
                    return planogramFixtureItemDto.X; 
                }
                return 0;
            }
            set
            {
                _xPosition = value;
            }
        }

        public Single Width { get; set; }

        #endregion

        #region Encode

        /// <summary>
        /// Encodes the bay cache item.
        /// </summary>
        /// <param name="fixelSchemaDefinition"></param>
        /// <returns></returns>
        /// <remarks>Expects you to have already set the sectionBackboard, sectionBase, and FirstBaseInPlan Properties. Otherwise encode may be with issues.</remarks>
        public override string Encode(Dictionary<String, SpacemanSchemaItem> fixelSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            StringBuilder output = new StringBuilder();
            
            this._localSchemaDefinition = fixelSchemaDefinition;

            Single[] notchValues;
            output.AppendLine(EncodeBay(out notchValues));

            if (notchValues[cNotchWidthIndex] > 0)
            {
                output.AppendLine(EncodeBayNotch(notchValues, true));
                output.AppendLine(EncodeBayNotch(notchValues, false));
            }

            return output.ToString();
        }

        private string EncodeBay(out Single[] notchValues)
        {
            notchValues = new Single[5];
            notchValues[cNotchXIndex] = 0;

            // Load the default row values into an array.
            String bayCacheItemRowDefault = BayCacheItem.Row;
            this._exportData = bayCacheItemRowDefault.Split(',');

            #region GetDto's

            PlanogramFixtureDto planogramFixtureDto = GetDto<PlanogramFixtureDto>();
            PlanogramFixtureItemDto planogramFixtureItemDto = GetDto<PlanogramFixtureItemDto>();
            PlanogramFixtureComponentDto planogramFixtureComponentDto = GetDto<PlanogramFixtureComponentDto>();

            // Get backboard dto
            PlanogramSubComponentDto backboardPlanogramSubComponentDto = null;
            if (this.Backboard != null)
            {
                backboardPlanogramSubComponentDto = this.Backboard.GetDto<PlanogramSubComponentDto>();
            }

            // Get base dto
            PlanogramSubComponentDto basePlanogramSubComponentDto = null;
            if (this.Base != null)
            {
                basePlanogramSubComponentDto = this.Base.GetDto<PlanogramSubComponentDto>();
            }

            #endregion

            // SET VALUES:
            Single maxWidth = 1;

            // PlanogramFixtureDto
            if (planogramFixtureDto != null)
            {
                #region ID & Name

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelId, String.Format("ID{0}", planogramFixtureDto.Id));
                
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelName, planogramFixtureDto.Name);

                #endregion

                #region Height, Width, Depth

                maxWidth = planogramFixtureDto.Width;
                float height = planogramFixtureDto.Height;

                //Need to reduce the overall height as the baackboard is placed on top of base rather than behind it.
                //Use the first base in the plan where available, else lets just use the current base. 
                if (this.FirstBaseInPlan != null)
                {
                    var planogramSubcomponentDto = this.FirstBaseInPlan.GetDto<PlanogramSubComponentDto>();
                    if (planogramSubcomponentDto != null) height = planogramFixtureDto.Height - planogramSubcomponentDto.Height;
                }
                else if(basePlanogramSubComponentDto != null)
                {
                    height = planogramFixtureDto.Height - basePlanogramSubComponentDto.Height;
                }
                 
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelHeight, height);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, planogramFixtureDto.Depth);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, maxWidth);

                #endregion
            }
            
            // PlanogramFixtureItemDto
            if (planogramFixtureItemDto != null)
            {
                #region id's & Links

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureItemDto.BaySequenceNumber);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSECTFIXELLINK, String.Format("ID{0}", planogramFixtureItemDto.PlanogramId));

                #endregion

                #region Coordinates

                Single bayX = this.XPosition;
                if (backboardPlanogramSubComponentDto != null && backboardPlanogramSubComponentDto.IsNotchPlacedOnFront)
                {
                    bayX += backboardPlanogramSubComponentDto.NotchStartX;
                }

                float zPosition = planogramFixtureItemDto.Z; 
                if (backboardPlanogramSubComponentDto != null)
                {
                    zPosition -= backboardPlanogramSubComponentDto.Depth;
                }
                

                notchValues[cNotchXIndex] = bayX;
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, bayX);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelY, planogramFixtureItemDto.Y);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelZ, zPosition);

                #endregion
            }

            // BackboardPlanogramSubComponentDto
            if (backboardPlanogramSubComponentDto != null)
            {
                #region Backboard size

                maxWidth = Math.Max(maxWidth, backboardPlanogramSubComponentDto.Width);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, backboardPlanogramSubComponentDto.Depth);

                #endregion

                #region Colours

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelFillColor, SpacemanExportHelper.EncodeColour(backboardPlanogramSubComponentDto.FillColourFront));
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelColour, SpacemanExportHelper.EncodeColour(backboardPlanogramSubComponentDto.FillColourFront));

                #endregion

                #region Rotation/Angles

                if (planogramFixtureItemDto != null)
                {
                    //Our 'angle' is spacemans 'rotation'
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelSlope, SpacemanExportHelper.ConvertFromRadians(planogramFixtureItemDto.Slope + backboardPlanogramSubComponentDto.Slope));
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelAngle, SpacemanExportHelper.ConvertFromRadians(planogramFixtureItemDto.Roll + backboardPlanogramSubComponentDto.Roll));
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelRotation, SpacemanExportHelper.ConvertCCMAngleToSpacemanAngle(planogramFixtureItemDto.Angle + backboardPlanogramSubComponentDto.Angle));
                }

                #endregion
            }
            
            if (basePlanogramSubComponentDto != null)
            {
                #region Base Size

                //Determine which is the better fit for base size then apply it at this point.
                maxWidth = Math.Max(maxWidth, basePlanogramSubComponentDto.Width);

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, maxWidth); //We set here even though it may be adjusted later if notches are to be applied.

                #endregion
            }

            #region  Adjust width to include notches

            float baseWidthFromMaxWidthBeforeNotchWidthAccountedFor = -1;
            if (backboardPlanogramSubComponentDto != null && backboardPlanogramSubComponentDto.IsNotchPlacedOnFront)
            {
                #region Notches

                //Spaceman spacing (Y) doesnt include the base height. In V8 we do therefore adjust.
                float spacemanBaseHeight = FirstBaseInPlan.GetDto<PlanogramSubComponentDto>().Height;
                float spacemanNotchStartY = (backboardPlanogramSubComponentDto.NotchStartY - spacemanBaseHeight);
                if (spacemanNotchStartY < spacemanBaseHeight)
                {
                    spacemanNotchStartY = 0;
                }

                notchValues[cNotchWidthIndex] = backboardPlanogramSubComponentDto.NotchWidth;
                notchValues[cNotchStartIndex] = spacemanNotchStartY;
                notchValues[cNotchSpacingIndex] = backboardPlanogramSubComponentDto.NotchSpacingY;

                baseWidthFromMaxWidthBeforeNotchWidthAccountedFor = maxWidth; 


                // Alter the bay width to account for notches
                maxWidth -= (2 * notchValues[cNotchWidthIndex]);
                notchValues[cBayWidthIndex] = maxWidth;

                // Set width last as we take the maximum value from base, backboard and bay.
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, maxWidth);

                #endregion
            }
            else
            { 
                //As we arent shwoing notches ensure we rip out those values. Otherwise V8 will display them on direct import, without running through spaceman.
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertSpacing, 0.0);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertStart, 0.0);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertSpacing, 0.0);
            }

            if (baseWidthFromMaxWidthBeforeNotchWidthAccountedFor != -1)
            {
                Width = baseWidthFromMaxWidthBeforeNotchWidthAccountedFor;
            }
            else
            {
                Width = maxWidth; // Ensure width is always set at end of each encode. (total width including notches)
            }
            

            #endregion

            #region Apply user mappings

            if (planogramFixtureDto != null)
            {
                ApplyExportMappings(planogramFixtureDto);
            }

            #endregion

            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        private string EncodeBayNotch(Single[] notchValues, Boolean left)
        {
            PlanogramFixtureDto planogramFixtureDto = GetDto<PlanogramFixtureDto>();
            PlanogramFixtureItemDto planogramFixtureItemDto = GetDto<PlanogramFixtureItemDto>();

            if (planogramFixtureDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelName, planogramFixtureDto.Name);
            }

            if (planogramFixtureItemDto != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureItemDto.BaySequenceNumber);
            }

            Single notchX = notchValues[cNotchXIndex];
            Single bayWidth = notchValues[cBayWidthIndex];
            Single notchWidth = notchValues[cNotchWidthIndex];
            Single notchYSpacing = notchValues[cNotchSpacingIndex]; 
            Single notchStartY = notchValues[cNotchStartIndex]; 
            if (left)
            {
                notchX -= notchWidth;
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, FixelFrameElementType.LeftNotch);
            }
            else
            {
                notchX += bayWidth;
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, FixelFrameElementType.RightNotch);
            }

            SetSpacemanFieldValue(SpacemanFieldHelper.FixelType, SpacemanComponentType.Frame);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, notchX);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, notchWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertSpacing, notchYSpacing);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertStart, notchStartY);

            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        #endregion
    }

}
