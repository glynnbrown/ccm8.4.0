﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{
    /// <summary>
    ///     This is a special case cache item. Rather than returning a single string it returns a tuple which holds both the encoded spaceman row record, but also the id of the font. 
    ///     This should be called as part of another encode, such as from annotation, where a custom font record may be required.
    /// </summary>
    public class FontCacheItem : DalCacheItem
    {
       
        #region Constructor 

        public FontCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties
    
        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Font;
            }
        }

        public static String Header
        {
            get
            {
                return Language.FontCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.FontCacheItem_Row;
            }
        }

        String _fontId;
        public String FontID
        {
            get
            {
                if(_fontId == null) _fontId = Guid.NewGuid().ToString();
                return _fontId;
            }
            set { _fontId = value; }
        }

        #endregion

        #region Encode

        public Tuple<String, String> EncodeFont(object dto, Dictionary<String, SpacemanSchemaItem> fontSchemaDefinition)
        {
            
            PlanogramAnnotationDto planogramAnnotationDto = dto as PlanogramAnnotationDto;

            if (planogramAnnotationDto != null)
            {
                // Return a spaceman encoded body/row.
                this._localSchemaDefinition = fontSchemaDefinition;

                // Load the default row values into an array.
                String productCacheItemRowDefault = FontCacheItem.Row;
                this._exportData = productCacheItemRowDefault.Split(',');
         
                SetSpacemanFieldValue(SpacemanFieldHelper.FontCollectionID, this.FontID);

                SetSpacemanFieldValue(SpacemanFieldHelper.FontName, planogramAnnotationDto.FontName);
                SetSpacemanFieldValue(SpacemanFieldHelper.FontSize, planogramAnnotationDto.FontSize);
                SetSpacemanFieldValue(SpacemanFieldHelper.FontColour, SpacemanExportHelper.EncodeColour(planogramAnnotationDto.FontColour));

                // Return the array as a single csv string and Id of font.
                String myFontRow = String.Join(",", _exportData);
                return new Tuple<String, String>(myFontRow, this.FontID);
            }

            return null;
        }

        #endregion
    }

}
