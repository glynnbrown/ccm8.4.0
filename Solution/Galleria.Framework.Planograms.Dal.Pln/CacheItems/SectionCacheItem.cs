﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class SectionCacheItem : DalCacheItem
    {
        #region Constructor 

        public SectionCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Section;
            }

        }

        public static String Header
        {
            get
            {
                return Language.SectionCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.SectionCacheItem_Row;
            }
        }

        public Object sectionId
        {
            get
            {
                if (this.DtoIds.ContainsKey(typeof(PlanogramFixtureDto)))
                {
                    return DtoIds[typeof(PlanogramFixtureDto)];
                }
                else if (this.Dtos.ContainsKey(typeof(PlanogramFixtureItemDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureItemDto)] as PlanogramFixtureItemDto).PlanogramFixtureId;
                }

                return null;
            }
        }

        FixelCacheItem _backboard;
        public FixelCacheItem Backboard
        {
            get { return _backboard; }
            set { _backboard = value; }
        }

        FixelCacheItem _base;
        public FixelCacheItem Base
        {
            get { return _base; }
            set { _base = value; }
        }


        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> sectionSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = sectionSchemaDefinition;

            #region Get Dto's

            // Get the planogram fixture dtos from the collection.
            PackageDto packageDto = GetDto<PackageDto>();
            PlanogramDto planogramDto = GetDto<PlanogramDto>();

            // Get backboard dto
            PlanogramSubComponentDto backboardPlanogramSubComponentDto = null;
            if (this.Backboard != null)
            {
                backboardPlanogramSubComponentDto = this.Backboard.GetDto<PlanogramSubComponentDto>();
            }

            // Get base dto
            PlanogramSubComponentDto basePlanogramSubComponentDto = null;
            if (this.Base != null)
            {
                basePlanogramSubComponentDto = this.Base.GetDto<PlanogramSubComponentDto>();
            }

            #endregion

            // Load the default row values into an array.
            String sectionCacheItemRowDefault = SectionCacheItem.Row;
            this._exportData = sectionCacheItemRowDefault.Split(',');
            
            //Start variables
            Boolean isPlanogramDtoAvailable = (planogramDto != null);
            Boolean isPackageDtoAvailable = (packageDto != null);
            Boolean isBackboardPlanogramSubComponentDtoAvailable = (backboardPlanogramSubComponentDto != null);
            Boolean isBasePlanogramSubComponentDtoAvailable = (basePlanogramSubComponentDto != null);
            Single height = 0;

            SetInitialCoordinates();

            if (isPlanogramDtoAvailable)
            {
                SetPlanogramValues(planogramDto);
                height = planogramDto.Height;
            }
            
            if (isBackboardPlanogramSubComponentDtoAvailable)
            {
                SetBackboardValues(backboardPlanogramSubComponentDto);
            }
            
            if (isBasePlanogramSubComponentDtoAvailable && isPlanogramDtoAvailable)
            {
                SetBaseValues(planogramDto, basePlanogramSubComponentDto);
                height += basePlanogramSubComponentDto.Height;
            }

            SetSectionHeight(height);

            if (isPlanogramDtoAvailable)
            {
                ApplyExportMappings(planogramDto);
            }

            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        #endregion

        #region Methods

        private void SetPlanogramValues(PlanogramDto planogramDto)
        {
            // Id & name
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionId, String.Format("ID{0}", planogramDto.Id));
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionName, planogramDto.Name);

            // Sizing 
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionWidth, planogramDto.Width);
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionDepth, planogramDto.Depth);
        }
        private void SetInitialCoordinates()
        {
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionX, 0);
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionY, 0);
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionZ, 0);
        }
        private void SetBackboardValues(PlanogramSubComponentDto backboardPlanogramSubComponentDto)
        {
            //Set notch related values
            if (backboardPlanogramSubComponentDto.IsNotchPlacedOnFront)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.SectionNotchSpacing, backboardPlanogramSubComponentDto.NotchSpacingY);
                SetSpacemanFieldValue(SpacemanFieldHelper.SectionNotchStart, backboardPlanogramSubComponentDto.NotchStartY);
                SetSpacemanFieldValue(SpacemanFieldHelper.SectionNotchWidth, backboardPlanogramSubComponentDto.NotchStartX);
            }
            else
            {
                //We set to 0 when not placed. Otherwise an imediate import into V8 after exporting will result with notches being shown.
                SetSpacemanFieldValue(SpacemanFieldHelper.SectionNotchWidth, 0);
            }
            
            //Colour and depth
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionColour, SpacemanExportHelper.EncodeColour(backboardPlanogramSubComponentDto.FillColourFront));
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionBackDepth, backboardPlanogramSubComponentDto.Depth);
        }
        private void SetBaseValues(PlanogramDto planogramDto, PlanogramSubComponentDto basePlanogramSubComponentDto)
        {
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionBaseHeight, basePlanogramSubComponentDto.Height);
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionBaseDepth, basePlanogramSubComponentDto.Depth);
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionBaseWidth, planogramDto.Width);
        }
        private void SetSectionHeight(float sectionHeight)
        {
            //The height of the plan with the height off the base added on. i.e the spaceman section height.
            SetSpacemanFieldValue(SpacemanFieldHelper.SectionHeight, sectionHeight);
        }

        #endregion
    }

}
