﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31548 : M.Brumby
//  Created
#endregion
#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using System;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{
    public class PegCacheItem : DalCacheItem
    {
        #region Constructor

        public PegCacheItem(DalCache dalCache, String id)
            : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Peg;
            }

        }
        public static String Header
        {
            get
            {
                return Language.PegCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.PegCacheItem_Row;
            }
        }
        
        #endregion
        
        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> pegSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = pegSchemaDefinition;

            // Load the default row values into an array.
            String pegCacheItemRowDefault = PegCacheItem.Row;
            this._exportData = pegCacheItemRowDefault.Split(',');

            PlanogramSubComponentDto subComponentDto = GetDto<PlanogramSubComponentDto>();
            PlanogramComponentDto componentDto = GetDto<PlanogramComponentDto>();
            PlanogramProductDto productDto = GetDto<PlanogramProductDto>();
            PlanogramFixtureDto fixtureDto = GetDto<PlanogramFixtureDto>();

            SetSpacemanFieldValue(SpacemanFieldHelper.PegID, this.Id);
            SetSpacemanFieldValue(SpacemanFieldHelper.PegLength, PegLibrary.GetPegDepth(productDto.PegDepth, subComponentDto.MerchandisableDepth, fixtureDto.Depth - componentDto.Depth));
            SetSpacemanFieldValue(SpacemanFieldHelper.PegSpan, PegLibrary.EstimateSpan(productDto.PegProngOffsetX, subComponentDto.MerchConstraintRow1SpacingX));


            return String.Join(",", _exportData);
        }

        #endregion
    }
}
