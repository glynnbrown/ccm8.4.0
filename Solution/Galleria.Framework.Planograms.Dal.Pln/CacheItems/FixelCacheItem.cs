﻿#region Version History: CCM830

// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-32635 : J.Pickup
//  Added AssemblyComponent.
// V8-32632 : J.Pickup
//  Added FixelFrameElement changes - to appear in fixture list. 
// V8-32645 : J.Pickup
//  Added New information logging for how the merchandising strategies avilable in spaceman may differ from V8.
// V8-18545 : J.Pickup
// Changes to exports to ensure custom fields don't corrupt file format when contain field seperator. Changes to dividers, based on new spaceman understanding.

#endregion

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;


namespace Galleria.Framework.Planograms.Dal.Pln.CacheItems
{

    public class FixelCacheItem : DalCacheItem
    {
        #region Contstructor

        public FixelCacheItem(DalCache dalCache, String id) : base(dalCache, id)
        {
            //Do nothing atm.
        }

        #endregion

        #region Properties

        internal override DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Fixel;
            }
        }

        public static String Header
        {
            get
            {
                return Language.FixelCacheItem_Header;
            }
        }

        public static String Row
        {
            get
            {
                return Language.FixelCacheItem_Row;
            }
        }

        public Boolean IsBackBoardOrBase
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Backboard ||
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Base
                    : false);
            }
        }

        public Boolean IsBackBoard
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Backboard
                    : false);
            }
        }

        public Boolean IsBase
        {
            get
            {
                return Dtos.Any(d => d.Value is PlanogramComponentDto ?
                    (d.Value as PlanogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Base
                    : false);
            }
        }

        public Object sectionId
        {
            get
            {
                if (this.Dtos.ContainsKey(typeof(PlanogramFixtureComponentDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureComponentDto)] as PlanogramFixtureComponentDto).PlanogramFixtureId;
                }
                else if (this.Dtos.ContainsKey(typeof(PlanogramFixtureAssemblyDto)))
                {
                    return (Dtos[typeof(PlanogramFixtureAssemblyDto)] as PlanogramFixtureAssemblyDto).PlanogramFixtureId;
                }

                return null;
            }
        }

        public Object subCompnentId
        {
            get
            {
                if (this.DtoIds.ContainsKey(typeof(PlanogramSubComponentDto)))
                {
                    return DtoIds[typeof(PlanogramSubComponentDto)];
                }
                return null;
            }
        }
        
        public Single FinalBayX { get; set; }

        public Single? WidthOverride { get; set; }

        public Boolean IsValid
        {
            get
            {
                Object objectDto = null;
                return this.Dtos.TryGetValue(typeof(PlanogramSubComponentDto), out objectDto);
            }
        }
        #endregion

        #region Encode

        public override string Encode(Dictionary<String, SpacemanSchemaItem> fixelSchemaDefinition)
        {
            // Return a spaceman encoded body/row.
            this._localSchemaDefinition = fixelSchemaDefinition;
            
            // Get the planogram fixture dtos from the collection.
            #region GetDto's         
   
            PlanogramFixtureAssemblyDto planogramFixtureAssemblyDto = GetDto<PlanogramFixtureAssemblyDto>();
            PlanogramFixtureComponentDto planogramFixtureComponentDto = GetDto<PlanogramFixtureComponentDto>();
            PlanogramAssemblyComponentDto planogramAssemblyComponentDto = GetDto<PlanogramAssemblyComponentDto>();
            PlanogramComponentDto planogramComponentDto = GetDto<PlanogramComponentDto>();
            PlanogramSubComponentDto planogramSubComponentDto = GetDto<PlanogramSubComponentDto>();
            PlanogramFixtureItemDto planogramFixtureItemDto = GetDto<PlanogramFixtureItemDto>();

            #endregion

            // Load the default row values into an array.
            String fixelCacheItemRowDefault = FixelCacheItem.Row;
            this._exportData = fixelCacheItemRowDefault.Split(',');

            // Life always begins as a bay.
            SpacemanComponentType componentType = SpacemanComponentType.Bay;

            // Set the values in the array:
            if (planogramFixtureItemDto != null)
            {
                #region Fixture Id

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureItemDto.BaySequenceNumber);
                //SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, planogramFixtureComponentDto.PlanogramFixtureId);

                #endregion
            }

            if (planogramSubComponentDto != null)
            {
                #region Size

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelHeight, planogramSubComponentDto.Height);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, planogramSubComponentDto.Width);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, planogramSubComponentDto.Depth);

                #endregion

                #region Type

                if (this.IsBackBoard)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelType, (Byte)SpacemanComponentType.Bay);
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, (byte)FixelFrameElementType.Backboard);
                }
                else if (this.IsBase)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, (byte)FixelFrameElementType.Base);
                }
                else
                {
                    //Encode the spaceman component type.
                    if (planogramComponentDto != null) componentType = EncodeSpacemanComponentType(planogramSubComponentDto, planogramComponentDto);
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelType, componentType);

                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, (byte)FixelFrameElementType.Component);
                }

                #endregion

                #region Output slotwall warning

                if (planogramComponentDto.ComponentType == (byte)PlanogramComponentType.SlotWall)
                {
                    //Spaceman does not activley support slotwalls.
                    _dalCache.LogWarning(Language.ExportLog_SlotwallNotSupported_Description, String.Format(Language.ExportLog_SlotwallNotSupported, planogramSubComponentDto.Name), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }

                #endregion

                #region Type specific properties

                // Encode type unique values.
                switch (componentType)
                {
                    case SpacemanComponentType.Shelf:
                        if (planogramSubComponentDto.IsRiserPlacedOnFront) SetSpacemanFieldValue(SpacemanFieldHelper.FixelGrillHeight, planogramSubComponentDto.RiserHeight);
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelCombine, (Byte)planogramSubComponentDto.CombineType);
                        EncodeOverhangValues(planogramSubComponentDto);
                        break;

                    case SpacemanComponentType.Chest:
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelCombine, (Byte)planogramSubComponentDto.CombineType);
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelGrillHeight, (planogramSubComponentDto.Height - planogramSubComponentDto.FaceThicknessBottom));
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelHeight, planogramSubComponentDto.FaceThicknessBottom);
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelThickness, planogramSubComponentDto.FaceThicknessLeft);
                        break;

                    case SpacemanComponentType.Pallet:
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelCombine, (Byte)planogramSubComponentDto.CombineType);
                        break;

                    case SpacemanComponentType.Bar:
                        EncodeOverhangValues(planogramSubComponentDto);
                        break;

                    case SpacemanComponentType.Peg:
                        EncodeOverhangValues(planogramSubComponentDto);
                        EncodeOverhangValues(planogramSubComponentDto);
                        break;

                    case SpacemanComponentType.Divider:
                    case SpacemanComponentType.Frame:
                    case SpacemanComponentType.Rod:
                    case SpacemanComponentType.Bay:
                        break;

                    default:
                        System.Diagnostics.Debug.Fail("Unknown SpacemanComponentType when calling DecodePlanogramSubComponentDto.");
                        break;
                }

                #endregion 

                #region Colours
                
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelFillColor, SpacemanExportHelper.EncodeColour(planogramSubComponentDto.FillColourFront));
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelColour, SpacemanExportHelper.EncodeColour(planogramSubComponentDto.FillColourFront));

                #endregion

                #region Squeeze Allowed

                //set is squeeze applied
                Int32 isSqueeze = Convert.ToInt16(planogramSubComponentDto.IsProductSqueezeAllowed); 
                if (isSqueeze != null) SetSpacemanFieldValue(SpacemanFieldHelper.FixelAutoCrush, isSqueeze);

                #endregion

                #region z,y merch strategy warning logs

                if ((planogramSubComponentDto.MerchandisingStrategyY != (Byte)PlanogramSubComponentYMerchStrategyType.Bottom || planogramSubComponentDto.MerchandisingStrategyZ != (Byte)PlanogramSubComponentZMerchStrategyType.Front)
                    && !this.IsBackBoardOrBase)
                {
                    String compName = String.Empty;

                    if (planogramComponentDto != null)
                    {
                        compName = planogramComponentDto.Name;
                    }

                    //CCM supports stacked merch style in z & y however spaceman does not. Spit out a log.
                    _dalCache.LogWarning(Language.ExportLog_MerchandisingStyleNotSupported_Description, String.Format(Language.ExportLog_MerchandisingStyleNotSupported, compName), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }

                #endregion

                #region extra constraint values
                
                EncodeMerchConstraint1Values(planogramSubComponentDto, componentType);

                #endregion
            }

            #region X,Y,Z adjustment & calculation

            Single x = 0, y = 0, z = 0;
            Single slope = 0, angle = 0, roll = 0;
            if (planogramFixtureComponentDto != null)
            {
                x = planogramFixtureComponentDto.X + planogramSubComponentDto.X;
                y = planogramFixtureComponentDto.Y + planogramSubComponentDto.Y;
                z = planogramFixtureComponentDto.Z + planogramSubComponentDto.Z;
                slope = planogramFixtureComponentDto.Slope + planogramSubComponentDto.Slope;
                angle = planogramFixtureComponentDto.Angle + planogramSubComponentDto.Angle;
                roll = planogramFixtureComponentDto.Roll + planogramSubComponentDto.Roll;
            }
            else if (planogramFixtureAssemblyDto != null && planogramAssemblyComponentDto != null)
            {
                x = planogramFixtureAssemblyDto.X + planogramAssemblyComponentDto.X + planogramSubComponentDto.X;
                y = planogramFixtureAssemblyDto.Y + planogramAssemblyComponentDto.Y + planogramSubComponentDto.Y;
                z = planogramFixtureAssemblyDto.Z + planogramAssemblyComponentDto.Z + planogramSubComponentDto.Z;
                slope = planogramFixtureAssemblyDto.Slope + planogramAssemblyComponentDto.Slope + planogramSubComponentDto.Slope;
                angle = planogramFixtureAssemblyDto.Angle + planogramAssemblyComponentDto.Angle + planogramSubComponentDto.Angle;
                roll = planogramFixtureAssemblyDto.Roll + planogramAssemblyComponentDto.Roll + planogramSubComponentDto.Roll;

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelAssembly, planogramAssemblyComponentDto.PlanogramAssemblyId);
            }

            if (planogramFixtureItemDto != null)
            {
                //Coordinates are relative to the plan not the bay so we need to rotate the bay relative
                //coordinates before adding the bays coordinates to them
                MatrixValue rotate = MatrixValue.CreateRotationMatrix(planogramFixtureItemDto.Angle, planogramFixtureItemDto.Slope, planogramFixtureItemDto.Roll);
                PointValue localCoordinates = new PointValue(x, y, z);
                localCoordinates = localCoordinates.Transform(rotate);

                x = this.FinalBayX + localCoordinates.X;
                y = planogramFixtureItemDto.Y + localCoordinates.Y;
                z = planogramFixtureItemDto.Z + localCoordinates.Z;
                
                slope += planogramFixtureItemDto.Slope;
                angle += planogramFixtureItemDto.Angle;
                roll += planogramFixtureItemDto.Roll;
            }

            #endregion

            #region Coordinates

            SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, x);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelY, y);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelZ, z);

            #endregion

            #region Rotation/Angles

            //Our 'angle' is spacemans 'rotation'
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelSlope, SpacemanExportHelper.ConvertFromRadians(slope));
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelAngle, SpacemanExportHelper.ConvertFromRadians(roll));
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelRotation, SpacemanExportHelper.ConvertCCMAngleToSpacemanAngle(angle));

            #endregion

            #region Id's and Naming 

            if (planogramComponentDto != null)
            {
                
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSECTFIXELLINK, String.Format("ID{0}", planogramComponentDto.PlanogramId));
                //SetSpacemanFieldValue(SpacemanFieldHelper.FixelSECTFIXELLINK, String.Format("ID{0}", planogramComponentDto.PlanogramId));
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelId, planogramComponentDto.Id);
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelName, planogramComponentDto.Name); 
            }

            #endregion

            #region Base specific sizing 

            if (planogramComponentDto.ComponentType == (byte)PlanogramComponentType.Base)
            {
                PlanogramDto planogramDto = GetDto<PlanogramDto>();

                if (planogramDto != null)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, planogramDto.Width);
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, planogramDto.Depth);
                }

                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSegment, 0);
            }

            if (WidthOverride != null)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, WidthOverride.Value);

            }

            #endregion

            #region Merch style

            //Spaceman doesn't have any Z merch strategies. 
            if (componentType == SpacemanComponentType.Shelf || componentType == SpacemanComponentType.Chest 
                || componentType == SpacemanComponentType.Pallet || componentType == SpacemanComponentType.Basket || componentType == SpacemanComponentType.Bar)
            {
                SetSpacemanFieldValue(SpacemanFieldHelper.FixelSpreadProducts, SpacemanExportHelper.EncodeMerchandisingStrategyX((PlanogramSubComponentXMerchStrategyType)planogramSubComponentDto.MerchandisingStrategyX));
            }


            #endregion

            #region Max Merch

            if (planogramSubComponentDto != null)
            {
                switch (componentType)
                {
                    case SpacemanComponentType.Shelf:
                    case SpacemanComponentType.Chest:
                    case SpacemanComponentType.Pallet:
                    case SpacemanComponentType.Basket:
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelMaxMerch, planogramSubComponentDto.MerchandisableHeight);
                        break;

                    case SpacemanComponentType.Bar:
                    case SpacemanComponentType.Rod:
                    case SpacemanComponentType.Peg:
                        SetSpacemanFieldValue(SpacemanFieldHelper.FixelMaxMerch, planogramSubComponentDto.MerchandisableDepth);
                        break;
                        
                    case SpacemanComponentType.Frame:
                    case SpacemanComponentType.Bay:
                    case SpacemanComponentType.TextBox:
                    case SpacemanComponentType.Divider:
                    default:
                        break;
                }
            }

            #endregion

            #region Erroneous

            SetSpacemanFieldValue(SpacemanFieldHelper.FixelDraw, 1);

            #endregion 

            #region  Apply user mappings

            if (planogramComponentDto != null)
            {
                ApplyExportMappings(planogramComponentDto);
            }

            #endregion

            // Return the array as a single csv string.
            return String.Join(",", _exportData);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a divider fixel before the position item X position tat matches
        /// up to the subcomponent divider values
        /// </summary>
        /// <param name="positionItem"></param>
        /// <returns></returns>
        public String CreateXDividerForPosition(PositionCacheItem positionItem)
        {
            PlanogramSubComponentDto planogramSubComponentDto = GetDto<PlanogramSubComponentDto>();
            PlanogramPositionDto positionDto = positionItem.GetDto<PlanogramPositionDto>();
            if (planogramSubComponentDto != null && positionDto != null)
            {
                return CreateXDivider(planogramSubComponentDto, positionDto.X);
            }
            return String.Empty;
        }

        /// <summary>
        /// Creates a divider fixel a thte first available divider position from the end
        /// of the element
        /// </summary>
        /// <returns></returns>
        public String CreateXEndDivider()
        {
            PlanogramSubComponentDto planogramSubComponentDto = GetDto<PlanogramSubComponentDto>();

            if (planogramSubComponentDto != null && planogramSubComponentDto.IsDividerObstructionAtEnd)
            {
                return CreateXDivider(planogramSubComponentDto, planogramSubComponentDto.Width - planogramSubComponentDto.DividerObstructionWidth);
            }
            return String.Empty;
        }

        /// <summary>
        /// Creates the first possible divider before the provided xPosition
        /// </summary>
        /// <param name="xPosition">x position relative to the subcomponent</param>
        /// <returns></returns>
        private String CreateXDivider(PlanogramSubComponentDto planogramSubComponentDto, Single xPosition)
        {
            if (planogramSubComponentDto != null)
            {
                Single dividerStart = planogramSubComponentDto.DividerObstructionStartX;
                Single dividerSpacing = planogramSubComponentDto.DividerObstructionSpacingX;
                Single dividerWidth = planogramSubComponentDto.DividerObstructionWidth;
                Single dividerHeight = planogramSubComponentDto.DividerObstructionHeight;
                Single dividerDepth = planogramSubComponentDto.DividerObstructionDepth;

                Single yOffset = 0, zOffset = 0;
                switch ((PlanogramSubComponentMerchandisingType)planogramSubComponentDto.MerchandisingType)
                {
                    case PlanogramSubComponentMerchandisingType.Stack:
                        yOffset = planogramSubComponentDto.Height;
                        dividerDepth = planogramSubComponentDto.Depth;
                        break;
                    case PlanogramSubComponentMerchandisingType.Hang:
                        zOffset = planogramSubComponentDto.Depth;
                        dividerHeight = planogramSubComponentDto.Height;
                        dividerDepth = planogramSubComponentDto.DividerObstructionDepth;
                        break;
                    case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    case PlanogramSubComponentMerchandisingType.None:
                    default:
                        break;
                }




                if ((dividerSpacing > 0 && dividerStart <= xPosition) || planogramSubComponentDto.IsDividerObstructionByFacing)
                {
                    Single originalXPosition, originalYPosition, originalZPosition;
                    Single angle, slope, roll;
                    if (Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelX), out originalXPosition) &&
                        Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelY), out originalYPosition) &&
                        Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelZ), out originalZPosition) &&
                        Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelAngle), out roll) &&
                        Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelSlope), out slope) &&
                        Single.TryParse(GetSpacemanFieldValue(SpacemanFieldHelper.FixelRotation), out angle))
                    {
                        Single k = xPosition - dividerStart;
                        Int32 dividerCount = (Int32)(k / dividerSpacing);
                        PointValue localCoordinates = new PointValue();
                        Boolean create = false;
                        if (planogramSubComponentDto.IsDividerObstructionByFacing)
                        {
                            if (xPosition - dividerWidth > 0 || planogramSubComponentDto.IsDividerObstructionAtStart)
                            {
                                localCoordinates = new PointValue(Math.Max(0, xPosition - dividerWidth), yOffset, zOffset);
                                create = true;
                            }
                        }
                        else if (dividerCount > 0 || planogramSubComponentDto.IsDividerObstructionAtStart)
                        {
                            localCoordinates = new PointValue((dividerCount * dividerSpacing) + dividerStart, yOffset, zOffset);
                            create = true;

                        }

                        if (create)
                        {
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelType, (Byte)SpacemanComponentType.Divider);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrameElement, 0);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelId, Guid.NewGuid());

                            //Rotate the coordinates and hope for the best
                            MatrixValue rotate = MatrixValue.CreateRotationMatrix((Single)(-angle * MathHelper.RadiansPerDegree),
                                (Single)(slope * MathHelper.RadiansPerDegree),
                                (Single)(roll * MathHelper.RadiansPerDegree));

                            //transform the divider coordinates as the coordinates are not
                            //to take into consideration any existing fixel transforms.
                            localCoordinates = localCoordinates.Transform(rotate);

                            //Start the divider off where the component starts then shift it along
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, originalXPosition + localCoordinates.X);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelY, originalYPosition + localCoordinates.Y);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelZ, originalZPosition + localCoordinates.Z);

                            //Set the divider size
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelWidth, dividerWidth);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelHeight, dividerHeight);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelDepth, dividerDepth);


                            //make it red. Red is awsome and also the colour they are in CCM
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelFillColor, 255);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelColour, 255);

                            String dividerLine = String.Join(",", _exportData);

                            //reset the coordinates to orignial values so that the next divider
                            //can be placed
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelX, originalXPosition);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelY, originalYPosition);
                            SetSpacemanFieldValue(SpacemanFieldHelper.FixelZ, originalZPosition);
                            return dividerLine;
                        }
                    }
                }
            }
            return String.Empty;
        }

        private void EncodeOverhangValues(PlanogramSubComponentDto planogramSubComponentDto)
        {
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelLeftOverhang, planogramSubComponentDto.LeftOverhang);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelRightOverhang, planogramSubComponentDto.RightOverhang);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelFrontOverhang, planogramSubComponentDto.FrontOverhang);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelBackOverhang, planogramSubComponentDto.BackOverhang);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelUpperOverhang, planogramSubComponentDto.TopOverhang);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelLowerOverhang, planogramSubComponentDto.BottomOverhang);
        }

        private void EncodeMerchConstraint1Values(PlanogramSubComponentDto planogramSubComponentDto, SpacemanComponentType componentType)
        {
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertStart, planogramSubComponentDto.MerchConstraintRow1StartY);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizStart, planogramSubComponentDto.MerchConstraintRow1StartX);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelVertSpacing, planogramSubComponentDto.MerchConstraintRow1SpacingY);

            //Component slot lines
            //Removed old references as we no longer export dividers based on previous fields.
            //SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizSlotStart, planogramSubComponentDto.DividerObstructionStartX);
            //SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizSlotSpacing, planogramSubComponentDto.DividerObstructionSpacingX);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelSpacerThick, planogramSubComponentDto.DividerObstructionWidth);
            SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizSpacing, planogramSubComponentDto.DividerObstructionSpacingX);

            if (componentType == SpacemanComponentType.Peg)
            {
                if (planogramSubComponentDto.MerchConstraintRow1SpacingX == 0)
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizSpacing, "0.01"); //Never export to less than 2 dp.
                }
                else
                {
                    SetSpacemanFieldValue(SpacemanFieldHelper.FixelHorizSpacing, planogramSubComponentDto.MerchConstraintRow1SpacingX);
                }
            }
        }

        #endregion
    }

}
