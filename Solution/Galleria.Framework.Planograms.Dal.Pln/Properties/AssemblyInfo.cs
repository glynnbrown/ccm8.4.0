﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Galleria.Framework.Planograms.Dal.Pln")]
[assembly: AssemblyDescription("8.0.0.48")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Galleria RTS Ltd")]
[assembly: AssemblyProduct("Customer Centric Merchandising")]
[assembly: AssemblyCopyright("Copyright (c) Galleria RTS Ltd 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: Guid("7e0addf2-37bb-4a2c-b1a9-959235563c41")]