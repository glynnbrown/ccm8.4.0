﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
// CCM-27919 : L.Ineson
//  Added FieldMappings property.
#endregion
#region Version History: CCM801
// CCM-28622 : D.Pleasance
//  Added MetricMapping
// CCM-28790 : D.Pleasance
//  Refactored to remove static property references
#endregion

#region Version History: CCM820

// V8-31175 : A.Silva
//  Amended the Constructor so that is can determine the correct file path when a planogram name has been appended to the dal factory name.

#endregion

#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting. Dto's lookups and typed insert dto methods.
// V8-32470 : J.Pickup
//  Change to ensure that the dalcontext is loaded from the field mappings. - Need to speak to mp as to why was written like that. 
// V8-32767 : J.Pickup
//  Change to dividers to ensure that a start divider is only created once when there are stacked positions in y and z axis.
// V8-32915  : J.Pickup
//  InsertDto now handles the insert obj of type CustomAttributeDataDto, to ensure the mappings for the custom attributes can be applied.
// V8-13992  : J.Pickup
// The position -> prod link now uses a resolver.  Also set products to be written to spaceman file first, as helps programatic execution. (Spaceman doesn't mind).
// V8-18545 : J.Pickup
// Changes to exports to ensure custom fields don't corrupt file format when contain field seperator. Changes to dividers, based on new spaceman understanding.
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Helpers;
using System.Collections.ObjectModel;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.Dal.Pln.CacheItems;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.External;
using System.Text;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Class used to cache and hold information
    /// relating to a dal factory
    /// </summary>
    public class DalCache : IDisposable
    {
        #region Fields
        private Object _lock = new object(); // object used for locking
        private String _filePath; // holds the full file path
        private FileStream _fileStream; // the file stream to the file
        private DalCacheItem _packageItem; // holds the root package item
        private ReadOnlyCollection<PlanogramFieldMappingDto> _fieldMappings; //holds the customisable field mappings to be applied
        private ReadOnlyCollection<PlanogramMetricMappingDto> _metricMappings; //holds the customisable field mappings to be applied
        private Boolean _readOnly; // indicates if the file was opened read only

        //A Lookup for each type of cache item - except store as only ever one (root).
        private SectionCacheItem _sectionCacheItem;
        private Dictionary<Object, BayCacheItem>_bayCacheItem = new Dictionary<object, BayCacheItem>();
        private Dictionary<Object, FixelCacheItem> _fixelCacheItems = new Dictionary<object, FixelCacheItem>();
        private Dictionary<Object, ProductCacheItem> _productCacheItems = new Dictionary<object, ProductCacheItem>();
        private Dictionary<Object, PositionCacheItem> _positionCacheItems = new Dictionary<object, PositionCacheItem>();
        private Dictionary<Object, FixelAnnotationCacheItem> _fixelAnnotationCacheItems = new Dictionary<object, FixelAnnotationCacheItem>();
        private PegLibrary _pegLibrary = null;

        //Resolvers
        private Dictionary<Object, Object> _productIdResolver = new Dictionary<Object, Object>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the path to the cache file
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
        }

        /// <summary>
        /// Returns the path to the cache file
        /// </summary>
        public Dictionary<Object, Object> ProductIdResolver
        {
            get { return _productIdResolver; }
            set { _productIdResolver = value; }
        }
        

        /// <summary>
        /// Returns the root package item
        /// </summary>
        public DalCacheItem PackageItem
        {
            get
            {
                // create the package item if required
                if (_packageItem == null)
                    lock (_lock)
                        if (_packageItem == null)
                        {
                            _packageItem = new DalCacheItem(this, _filePath, _fileStream, _fieldMappings, _metricMappings);
                            LoadCacheData();
                        }

                // return the package item
                return _packageItem;
            }
            set
            {
                //Clear
                if (_packageItem != null) _packageItem.Dispose();
                _packageItem = value;
            }
        }

        /// <summary>
        /// Returns the collection of customisable field mappings to apply.
        /// </summary>
        /// <remarks>Type: Planogram = 0, Fixture = 1, Component = 2, Product = 3</remarks>
        public ReadOnlyCollection<PlanogramFieldMappingDto> FieldMappings
        {
            get{ return _fieldMappings; }
            set 
            {
                _fieldMappings = value; 
            }
        }

        /// <summary>
        /// Returns the collection of customisable metric mappings to apply.
        /// </summary>
        /// <remarks></remarks>
        public ReadOnlyCollection<PlanogramMetricMappingDto> MetricMappings
        {
            get { return _metricMappings; }
            set
            {
                _metricMappings = value;
            }
        }

        public ExportContext ExportMappingContext {  get; set; }

        /// <summary>
        /// Returns a library of pegs used during export
        /// </summary>
        public PegLibrary PegLibrary
        {
            get
            {
                if (_pegLibrary == null)
                {
                    _pegLibrary = new PegLibrary(this);
                }

                return _pegLibrary;
            }
        }

        public IEnumerable<DalCacheItem> AllCacheItems
        {
            get
            {
                return _bayCacheItem.Select(b => b.Value as DalCacheItem)
                    .Union(_fixelCacheItems.Select(b => b.Value as DalCacheItem))
                    .Union(_productCacheItems.Select(b => b.Value as DalCacheItem))
                    .Union(_positionCacheItems.Select(b => b.Value as DalCacheItem))
                    .Union(_fixelAnnotationCacheItems.Select(b => b.Value as DalCacheItem))
                    .Union(_fixelCacheItems.Select(b => b.Value as DalCacheItem)).Union(new List<DalCacheItem>() { _sectionCacheItem as DalCacheItem, _packageItem as DalCacheItem });
            }
        }
        #endregion

        #region Cache Properties

        #region Package
        /// <summary>
        /// PackageDtoList
        /// </summary>
        private List<PackageDto> _packageDtoList = new List<PackageDto>();
        public List<PackageDto> PackageDtoList
        {
            get { return _packageDtoList; }
        }
        
        #endregion

        #region Planogram
        /// <summary>
        /// PlanogramDtoList
        /// </summary>
        private List<PlanogramDto> _planogramDtoList = new List<PlanogramDto>();
        public List<PlanogramDto> PlanogramDtoList
        {
            get { return _planogramDtoList; }
        }
        
        #endregion

        #region PlanogramFixtureItem
        /// <summary>
        /// PlanogramFixtureItemDtoList
        /// </summary>
        private List<PlanogramFixtureItemDto> _planogramFixtureItemDtoList = new List<PlanogramFixtureItemDto>();
        public List<PlanogramFixtureItemDto> PlanogramFixtureItemDtoList
        {
            get { return _planogramFixtureItemDtoList; }
        }

        #endregion

        #region PlanogramFixture
        /// <summary>
        /// PlanogramFixtureDtoList
        /// </summary>
        private List<PlanogramFixtureDto> _planogramFixtureDtoList = new List<PlanogramFixtureDto>();
        public List<PlanogramFixtureDto> PlanogramFixtureDtoList
        {
            get { return _planogramFixtureDtoList; }
        }

        #endregion

        #region PlanogramFixtureComponent
        /// <summary>
        /// PlanogramFixtureComponentDtoList
        /// </summary>
        private List<PlanogramFixtureComponentDto> _planogramFixtureComponentDtoList = new List<PlanogramFixtureComponentDto>();
        public List<PlanogramFixtureComponentDto> PlanogramFixtureComponentDtoList
        {
            get { return _planogramFixtureComponentDtoList; }
        }

        #endregion

        #region PlanogramComponent
        /// <summary>
        /// PlanogramComponentDtoList
        /// </summary>
        private List<PlanogramComponentDto> _planogramComponentDtoList = new List<PlanogramComponentDto>();
        public List<PlanogramComponentDto> PlanogramComponentDtoList
        {
            get { return _planogramComponentDtoList; }
        }

        #endregion

        #region PlanogramSubComponent
        /// <summary>
        /// PlanogramSubComponentList
        /// </summary>
        private List<PlanogramSubComponentDto> _planogramSubComponentList = new List<PlanogramSubComponentDto>();
        public List<PlanogramSubComponentDto> PlanogramSubComponentDtoList
        {
            get { return _planogramSubComponentList; }
        }

        #endregion

        #region PlanogramProduct
        /// <summary>
        /// PlanogramDtoList
        /// </summary>
        private List<PlanogramProductDto> _planogramProductDtoList = new List<PlanogramProductDto>();
        public List<PlanogramProductDto> PlanogramProductDtoList
        {
            get { return _planogramProductDtoList; }
        }

        #endregion

        #region PlanogramPerformanceData
        /// <summary>
        /// PlanogramPerformanceData
        /// </summary>
        private List<PlanogramPerformanceMetricDto> _planogramPerformanceMetricDtoList = new List<PlanogramPerformanceMetricDto>();
        public List<PlanogramPerformanceMetricDto> PlanogramPerformanceMetricDtoList
        {
            get { return _planogramPerformanceMetricDtoList; }
        }

        #endregion

        #region PlanogramPerformanceData
        /// <summary>
        /// PlanogramPerformanceData
        /// </summary>
        private List<PlanogramPerformanceDataDto> _planogramPerformanceDataDtoList = new List<PlanogramPerformanceDataDto>();
        public List<PlanogramPerformanceDataDto> PlanogramPerformanceDataDtoList
        {
            get { return _planogramPerformanceDataDtoList; }
        }

        #endregion

        #region PlanogramPosition
        /// <summary>
        /// PlanogramPositionDtoList
        /// </summary>
        private List<PlanogramPositionDto> _planogramPositionDtoList = new List<PlanogramPositionDto>();
        public List<PlanogramPositionDto> PlanogramPositionDtoList
        {
            get { return _planogramPositionDtoList; }
        }

        #endregion

        #region PlanogramAnnotation
        /// <summary>
        /// PlanogramAnnotationDtoList
        /// </summary>
        private List<PlanogramAnnotationDto> _planogramAnnotationDtoList = new List<PlanogramAnnotationDto>();
        public List<PlanogramAnnotationDto> PlanogramAnnotationDtoList
        {
            get { return _planogramAnnotationDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTree
        /// <summary>
        /// PlanogramConsumerDecisionTreeDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeDto> _planogramConsumerDecisionTreeDtoList = new List<PlanogramConsumerDecisionTreeDto>();
        public List<PlanogramConsumerDecisionTreeDto> PlanogramConsumerDecisionTreeDtoList
        {
            get { return _planogramConsumerDecisionTreeDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTreeLevel
        /// <summary>
        /// PlanogramConsumerDecisionTreeLevelDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeLevelDto> _planogramConsumerDecisionTreeLevelDtoList = new List<PlanogramConsumerDecisionTreeLevelDto>();
        public List<PlanogramConsumerDecisionTreeLevelDto> PlanogramConsumerDecisionTreeLevelDtoList
        {
            get { return _planogramConsumerDecisionTreeLevelDtoList; }
        }
        #endregion

        #region PlanogramConsumerDecisionTreeNode
        /// <summary>
        /// PlanogramConsumerDecisionTreeNodeDtoList
        /// </summary>
        private List<PlanogramConsumerDecisionTreeNodeDto> _planogramConsumerDecisionTreeNodeDtoList = new List<PlanogramConsumerDecisionTreeNodeDto>();
        public List<PlanogramConsumerDecisionTreeNodeDto> PlanogramConsumerDecisionTreeNodeDtoList
        {
            get { return _planogramConsumerDecisionTreeNodeDtoList; }
        }
        #endregion

        #region PlanogramEventLog
        /// <summary>
        /// PlanogramEventLogDtoList
        /// </summary>
        private List<PlanogramEventLogDto> _planogramEventLogDtoList = new List<PlanogramEventLogDto>();
        public List<PlanogramEventLogDto> PlanogramEventLogDtoList
        {
            get { return _planogramEventLogDtoList; }
        }
        #endregion

        #region CustomAttributeData
        /// <summary>
        /// CustomAttributeDataDtoList
        /// </summary>
        private List<CustomAttributeDataDto> _customAttributeDataDtoList = new List<CustomAttributeDataDto>();
        public List<CustomAttributeDataDto> CustomAttributeDataDtoList
        {
            get { return _customAttributeDataDtoList; }
        }
        #endregion

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCache(DalFactoryConfigElement dalFactoryConfig)
        {
            _filePath = dalFactoryConfig.Name;
            Int32 planNameIndex = _filePath.IndexOf("::", StringComparison.InvariantCultureIgnoreCase);
            if (planNameIndex == -1) return;

            //  Split the plan name and the real file path.
            //_planName = _filePath.Substring(planNameIndex + 2, _filePath.Length - planNameIndex - 2);
            _filePath = _filePath.Substring(0, planNameIndex);
        }
        #endregion

        #region Methods

        private void LoadCacheData()
        {
            this.PackageDtoList.Add((PackageDto)_packageItem.Dto);

            foreach (var planogramItem in _packageItem.Children[DalCacheItemType.Section])
            {
                this.PlanogramDtoList.Add((PlanogramDto)planogramItem.Dto);
            }
            if (_packageItem.Children.ContainsKey(DalCacheItemType.Fixel))
            {
                foreach (var fixelItem in _packageItem.Children[DalCacheItemType.Fixel])
                {
                    foreach (object fixelDto in (List<object>)fixelItem.Dto)
                    {
                        if (fixelDto.GetType() == typeof(PlanogramFixtureItemDto))
                        {
                            this.PlanogramFixtureItemDtoList.Add((PlanogramFixtureItemDto)fixelDto);
                        }
                        else if (fixelDto.GetType() == typeof(PlanogramFixtureDto))
                        {
                            this.PlanogramFixtureDtoList.Add((PlanogramFixtureDto)fixelDto);
                        }
                        else if (fixelDto.GetType() == typeof(PlanogramFixtureComponentDto))
                        {
                            this.PlanogramFixtureComponentDtoList.Add((PlanogramFixtureComponentDto)fixelDto);
                        }
                        else if (fixelDto.GetType() == typeof(PlanogramComponentDto))
                        {
                            this.PlanogramComponentDtoList.Add((PlanogramComponentDto)fixelDto);
                        }
                        else if (fixelDto.GetType() == typeof(PlanogramSubComponentDto))
                        {
                            this.PlanogramSubComponentDtoList.Add((PlanogramSubComponentDto)fixelDto);
                        }
                        else if (fixelDto.GetType() == typeof(PlanogramAnnotationDto))
                        {
                            this.PlanogramAnnotationDtoList.Add((PlanogramAnnotationDto)fixelDto);
                        }
                    }
                }
            }
            if (_packageItem.Children.ContainsKey(DalCacheItemType.Product))
            {
                foreach (var productItem in _packageItem.Children[DalCacheItemType.Product])
                {
                    this.PlanogramProductDtoList.Add((PlanogramProductDto)productItem.Dto);
                }
                if (_packageItem.Children.ContainsKey(DalCacheItemType.Position))
                {
                    foreach (var positionItem in _packageItem.Children[DalCacheItemType.Position])
                    {
                        foreach (PlanogramPositionDto positionDto in (List<PlanogramPositionDto>)positionItem.Dto)
                        {
                            this.PlanogramPositionDtoList.Add(positionDto);
                        }
                    }
                }
            }
            this.PlanogramConsumerDecisionTreeDtoList.Add(new PlanogramConsumerDecisionTreeDto()
            {
                Id = IdentityHelper.GetNextInt32(),
                Name = Language.PlanogramConsumerDecisionTree_Name_Default,
                PlanogramId = 1
            });
            this.PlanogramConsumerDecisionTreeLevelDtoList.Add(new PlanogramConsumerDecisionTreeLevelDto()
            {
                Id = IdentityHelper.GetNextInt32(),
                Name = Language.PlanogramConsumerDecisionTreeLevel_RootLevel_DefaultName,
                PlanogramConsumerDecisionTreeId = PlanogramConsumerDecisionTreeDtoList[0].Id
            });
            this.PlanogramConsumerDecisionTreeNodeDtoList.Add(new PlanogramConsumerDecisionTreeNodeDto()
            {
                Id = IdentityHelper.GetNextInt32(),
                Name = Language.PlanogramConsumerDecisionTreeNode_RootNode_DefaultName,
                PlanogramConsumerDecisionTreeId = PlanogramConsumerDecisionTreeDtoList[0].Id,
                PlanogramConsumerDecisionTreeLevelId = PlanogramConsumerDecisionTreeLevelDtoList[0].Id
            });
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            this.Close();
        }

        /// <summary>
        /// Opens and locks the file ready for reading
        /// </summary>
        public void Open(Boolean readOnly)
        {
            lock (_lock)
            {
                // attempt open the file stream
                if (_fileStream == null)
                {
                    // determine if the file already exists
                    Boolean isNew = !File.Exists(_filePath);

                    // check if the file is readonly
                    // and set the correct file access mode
                    FileAccess fileAccess = FileAccess.ReadWrite;
                    if (readOnly)
                    {
                        fileAccess = FileAccess.Read;
                    }
                    else if (!isNew)
                    {
                        FileInfo fileInfo = new FileInfo(_filePath);
                        if (fileInfo.IsReadOnly) fileAccess = FileAccess.Read;
                    }

                    if (fileAccess == FileAccess.ReadWrite)
                    {
                        //create a backup of the existing file, if any
                        SpacePlanningExportHelper.CreateExportFileBackup(_filePath);
                    }

                    // open the file stream
                    _fileStream = new FileStream(_filePath, FileMode.OpenOrCreate, fileAccess, FileShare.ReadWrite);

                    // remember that the file is readonly
                    _readOnly = readOnly;
                }
                else
                {
                    _fileStream.Close();

                    if (readOnly)
                    {
                        _fileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    }
                    else
                    {
                        //create a backup of the existing file, if any
                        SpacePlanningExportHelper.CreateExportFileBackup(_filePath);

                        _fileStream = new FileStream(_filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }
                }
            }
        }

        /// <summary>
        /// Closes and releases the cached file
        /// </summary>
        public void Close()
        {
            lock (_lock)
            {
                // dispose of the file stream
                if (_fileStream != null) _fileStream.Dispose();
                _fileStream = null;

                // dispose of the cache items
                if (_packageItem != null) _packageItem.Dispose();
                _packageItem = null;

                 if (_packageItem != null)_sectionCacheItem.Dispose();
                _sectionCacheItem = null;
                _fixelCacheItems.Clear();
                _positionCacheItems.Clear();
                _productCacheItems.Clear();
                _fixelAnnotationCacheItems.Clear();

                _packageDtoList.Clear();
                _planogramDtoList.Clear();
                _planogramFixtureItemDtoList.Clear();
                _planogramFixtureDtoList.Clear();
                _planogramFixtureComponentDtoList.Clear();
                _planogramComponentDtoList.Clear();
                _planogramSubComponentList.Clear();
                _planogramProductDtoList.Clear();
                _planogramPositionDtoList.Clear();
                _planogramAnnotationDtoList.Clear();
                _planogramConsumerDecisionTreeDtoList.Clear();
                _planogramConsumerDecisionTreeLevelDtoList.Clear();
                _planogramConsumerDecisionTreeNodeDtoList.Clear();
                _planogramEventLogDtoList.Clear();
                _customAttributeDataDtoList.Clear();
                _planogramPerformanceDataDtoList.Clear();
                _planogramPerformanceMetricDtoList.Clear();

                _pegLibrary = null;

                if (this.ExportMappingContext != null && this.ExportMappingContext.ExportEvents.Any(e => (PlanogramEventLogEntryType)e.EntryType == PlanogramEventLogEntryType.Error))
                {
                    //Restore the original file, if any, to its location
                    SpacePlanningExportHelper.RestoreExportFileBackup(_filePath);
                }
                else
                {
                    //Delete the old backup file as it is no longer required
                    SpacePlanningExportHelper.DeleteExportFileBackup(_filePath);
                }

                _productIdResolver.Clear();
                _productIdResolver = null;
            }
        }

        public void RefreshCache()
        {
            // open the file stream
            if (_packageItem != null)
            {
                this.Close();
                _fileStream = new FileStream(_filePath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
            }
        }
        #endregion

        #region JPExport

        /// <summary>
        /// Calls the relevant insertDto method depening on type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId"></param>
        /// <param name="thisId"></param>
        public void InsertDto<T>(T obj, Object parentId, Object thisId)
        {
            try
            {
                //Root
                if (obj is PackageDto || obj is PlanogramDto)
                {
                    this.InsertStoreCacheItemDto<T>(obj, parentId, thisId);
                    this.InsertSectionCacheItemDto<T>(obj, parentId, thisId);
                }

                //BayFixelCahceItem
                if (obj is PlanogramFixtureDto || obj is PlanogramFixtureItemDto)
                {
                    this.InsertBayCacheItemDto<T>(obj, parentId, thisId);
                }

                //Fixels.
                if (obj is PlanogramFixtureAssemblyDto || obj is PlanogramAssemblyComponentDto ||
                    obj is PlanogramFixtureComponentDto || obj is PlanogramComponentDto ||
                    obj is PlanogramSubComponentDto) this.InsertFixelCacheItemDto<T>(obj, parentId, thisId);

                //PlanogramAssemblyDto - no idea what to do
                //Products
                if (obj is PlanogramProductDto || obj is PlanogramPerformanceDataDto) this.InsertProductCacheItemDto<T>(obj, parentId, thisId);

                //Positions
                if (obj is PlanogramPositionDto) this.InsertPositionCacheItemDto<T>(obj, parentId, thisId);

                //Positions
                if (obj is PlanogramAnnotationDto) this.InsertPlanogramAnnotationDto<T>(obj, parentId, thisId);

                //Custom Attribute
                if (obj is CustomAttributeDataDto) this.InsertCustomAttribute<T>(obj, parentId, thisId);

            }
            catch (Exception ex)
            {
                //rethow the error if we don't log it
                if (!LogException<T>(ex))
                {
                    //kill off the file stream just to be sure.
                    if (_fileStream != null) _fileStream.Dispose();
                    _fileStream = null;

                    //Restore the original file, if any, to its location
                    SpacePlanningExportHelper.RestoreExportFileBackup(_filePath);

                    throw;
                }
            }
        }

        /// <summary>
        ///     Inserts a store cache item dto.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PackageDto id</param>
        /// <param name="thisId">dto id</param>
        private void InsertStoreCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            if (obj is PackageDto)
            {
                PackageDto packageDto = obj as PackageDto;
                if (packageDto != null)
                {
                    this.PackageItem = new StoreCacheItem(this, parentId.ToString());
                    this.PackageItem.InsertDto<PackageDto>(packageDto, thisId);

                    this.ExportMappingContext = (packageDto.FieldMappings as ExportContext) ?? new ExportContext();
                   
                }
            }
            else
            {
                this.PackageItem.InsertDto<T>(obj, thisId);
            }
        }
        
        /// <summary>
        ///     Inserts a section cache item dto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramFixtureDto Id</param>
        /// <param name="thisId">dto id</param>
        private void InsertSectionCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            if (obj is PackageDto)
            {
                PackageDto packageDto = obj as PackageDto;
                if (packageDto != null)
                {
                    this._sectionCacheItem = new SectionCacheItem(this, parentId.ToString());
                    this._sectionCacheItem.InsertDto<PackageDto>(packageDto, thisId);

                    this.ExportMappingContext = (packageDto.FieldMappings as ExportContext) ?? new ExportContext();
                }
            }
            else
            {
                this._sectionCacheItem.InsertDto<T>(obj, thisId);
            }
        }

        /// <summary>
        ///     Inserts a bay cache item dto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramFixtureDto Id</param>
        /// <param name="thisId">dto id</param>
        private void InsertBayCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            //Find bayCacheItem by parent id
            BayCacheItem bayCacheItem = null;
            if (!_bayCacheItem.TryGetValue(parentId, out bayCacheItem))
            {
                bayCacheItem = new BayCacheItem(this, parentId.ToString());
                _bayCacheItem.Add(parentId, bayCacheItem);
            }
            bayCacheItem.InsertDto<T>(obj, thisId);
        }

        /// <summary>
        ///     inserts a fixel cache item dto.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramComponentDto id</param>
        /// <param name="thisId">dto id</param>
        private void InsertFixelCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            FixelCacheItem sectionItem = null;
            Boolean addToCache = false;
            if (obj is PlanogramFixtureAssemblyDto)
            {
                PlanogramFixtureAssemblyDto packageDto = obj as PlanogramFixtureAssemblyDto;
                IEnumerable<FixelCacheItem> items = _fixelCacheItems.Values.Where(s => 
                        s.Dtos.ContainsKey(typeof(PlanogramAssemblyComponentDto)) &&
                        Equals(s.GetDto<PlanogramAssemblyComponentDto>().PlanogramAssemblyId, parentId));
                foreach (FixelCacheItem item in items)
                {
                    item.InsertDto<T>(obj, thisId);
                }
                return;
            }
            else
            {
                //Find Section cache item by parent id                
                if (!_fixelCacheItems.TryGetValue(parentId, out sectionItem))
                {
                    sectionItem = new FixelCacheItem(this, parentId.ToString());
                    addToCache = true;
                }
            }

            if (sectionItem != null)
            {
                if (!sectionItem.InsertDto<T>(obj, thisId))
                {
                    String newId = Guid.NewGuid().ToString();
                    FixelCacheItem copyItem = new FixelCacheItem(this, newId);
                    copyItem.InsertDto<T>(obj, thisId);
                    copyItem.InsertDtos(sectionItem);
                    _fixelCacheItems.Add(newId, copyItem);
                }
                else
                {
                   if(addToCache) _fixelCacheItems.Add(parentId, sectionItem);
                }
            }
        }

        /// <summary>
        ///     Inserts a position cache item dto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramFixtureDto Id</param>
        /// <param name="thisId">dto id</param>
        private void InsertPositionCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            PositionCacheItem item = new PositionCacheItem(this, thisId.ToString());
            item.InsertDto<T>(obj, thisId);
            _positionCacheItems.Add(thisId, item);
        }

        /// <summary>
        ///     Inserts a product cache item dto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramFixtureDto Id</param>
        /// <param name="thisId">dto id</param>
        private void InsertProductCacheItemDto<T>(T obj, Object parentId, Object thisId)
        {
            ProductCacheItem item = null;
            if (!_productCacheItems.TryGetValue(parentId, out item))
            {
                item = new ProductCacheItem(this, thisId.ToString());
                _productCacheItems.Add(thisId, item);
            }

            item.InsertDto<T>(obj, thisId);           
        }

        /// <summary>
        ///     Inserts a FixelAnnotationCacheItem dto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId">PlanogramFixtureDto Id</param>
        /// <param name="thisId">dto id</param>
        private void InsertPlanogramAnnotationDto<T>(T obj, Object parentId, Object thisId)
        {
            FixelAnnotationCacheItem item = new FixelAnnotationCacheItem(this, thisId.ToString());
            item.InsertDto<T>(obj, thisId);
            _fixelAnnotationCacheItems.Add(thisId, item);
        }

        /// <summary>
        ///     Inserts a custom attribute into the relavent cache items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parentId"></param>
        /// <param name="thisId"></param>
        private void InsertCustomAttribute<T>(T obj, Object parentId, Object thisId) 
        {
            if (obj is CustomAttributeDataDto)
            {
                CustomAttributeDataDto dto = obj as CustomAttributeDataDto;
                if (dto != null)
                {
                    switch ((CustomAttributeDataPlanogramParentType)dto.ParentType)
                    {
                        case CustomAttributeDataPlanogramParentType.Planogram:
                            InsertSectionCacheItemDto<T>(obj, parentId, thisId);
                            InsertStoreCacheItemDto<T>(obj, parentId, thisId);
                            break;
                        case CustomAttributeDataPlanogramParentType.PlanogramComponent:
                            InsertFixelCacheItemDto<T>(obj, parentId, thisId);
                            break;
                        case CustomAttributeDataPlanogramParentType.PlanogramProduct:
                            InsertProductCacheItemDto<T>(obj, parentId, thisId);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the planogram name of the first source planogram to export
        /// </summary>
        /// <returns>The planogram name</returns>
        private String GetSourcePlanogramName()
        {
            if (_sectionCacheItem.Dtos.ContainsKey(typeof(PlanogramDto)))
            {
                PlanogramDto planDto = _sectionCacheItem.Dtos[typeof(PlanogramDto)] as PlanogramDto;
                if (planDto != null)
                {

                    return planDto.Name;
                }
            }
            //if we didn't find it, default to the exported plan file name
            return System.IO.Path.GetFileNameWithoutExtension(_filePath); 
        }

        public void ExportPackage()
        {
            try
            {
                //Log start
                String sourcePlanogramName = GetSourcePlanogramName();
                LogInformation(
                    Language.ExportLog_StartingExport_Description, String.Format(Language.ExportLog_StartingExport_Content, sourcePlanogramName),
                    PlanogramEventLogAffectedType.Planogram);


                this.PackageItem.InitializeSchema(StoreCacheItem.Header);
                this.PackageItem.InitializeSchema(SectionCacheItem.Header);
                this.PackageItem.InitializeSchema(FixelCacheItem.Header);
                this.PackageItem.InitializeSchema(PositionCacheItem.Header);
                this.PackageItem.InitializeSchema(ProductCacheItem.Header);
                this.PackageItem.InitializeSchema(PegCacheItem.Header);
                this.PackageItem.InitializeSchema(FixelAnnotationCacheItem.Header);
                this.PackageItem.InitializeSchema(FontCacheItem.Header);
                
                using (TextWriter textWriter = new StreamWriter(_fileStream))
                {
                    //Headers here:
                    textWriter.WriteLine(StoreCacheItem.Header);
                    textWriter.WriteLine(SectionCacheItem.Header);
                    textWriter.WriteLine(FixelCacheItem.Header);
                    textWriter.WriteLine(PositionCacheItem.Header);
                    textWriter.WriteLine(ProductCacheItem.Header);
                    textWriter.WriteLine(PegCacheItem.Header);
                    textWriter.WriteLine(FontCacheItem.Header);

                    // After the headers there must be a new line. Otherwise spaceman won't read the remaining structure of the file.
                    textWriter.WriteLine(Environment.NewLine);

                    // Package/Planogram (Store)
                    textWriter.WriteLine(_packageItem.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Store]));

                    FixelCacheItem backboard = _fixelCacheItems.Values.Where(f => f.IsBackBoard).FirstOrDefault();
                    FixelCacheItem baseboard = _fixelCacheItems.Values.Where(f => f.IsBase).FirstOrDefault();

                    if (backboard != null) _sectionCacheItem.Backboard = backboard;
                    if (baseboard != null) _sectionCacheItem.Base = baseboard;

                    //Need to do something about base and backboard.
                    // Package/Planogram (Store)
                    textWriter.WriteLine(_sectionCacheItem.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Section]));

                    //Do products first - important for product id mappings on position linkage
                    foreach (ProductCacheItem product in _productCacheItems.Values)
                    {
                        textWriter.WriteLine(product.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Product]));
                    }

                    if (baseboard != null)
                    {
                        baseboard.WidthOverride = 0;
                    }

                    FixelCacheItem firstBaseInPlan = null;
                    Single xPosition = 0;
                    // Section
                    foreach (BayCacheItem section in _bayCacheItem.Values.OrderBy(v => v.XPosition))
                    {
                        // Get the relevant fixel cache items & set property before calling encode.
                        FixelCacheItem sectionBackboard = _fixelCacheItems.Values.Where(f => Equals(f.sectionId, section.sectionId) && f.IsBackBoard).FirstOrDefault();
                        FixelCacheItem sectionBase = _fixelCacheItems.Values.Where(f => Equals(f.sectionId, section.sectionId) && f.IsBase).FirstOrDefault();

                        if (sectionBackboard != null) section.Backboard = sectionBackboard;
                        if (sectionBase != null) section.Base = sectionBase;
                        //first base only wants updating first time.
                        if (firstBaseInPlan == null && sectionBase != null) firstBaseInPlan = sectionBase;
                        if (firstBaseInPlan != null) section.FirstBaseInPlan = firstBaseInPlan;
                        

                        section.XPosition = xPosition;
                        textWriter.WriteLine(section.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Fixel]));
                        xPosition += section.Width; //width is populated during encode
                        if (baseboard != null) baseboard.WidthOverride += section.Width;

                        // Fixel
                        foreach (FixelCacheItem fixel in _fixelCacheItems.Values.Where(f => Equals(f.sectionId, section.sectionId) && !f.IsBackBoardOrBase && f.IsValid).OrderByDescending(v => v.IsBase).ThenByDescending(v => v.IsBackBoard))
                        {
                            //Set the xposition that was actually used for the section
                            fixel.FinalBayX = section.XPosition;

                            //Insert if not already exists
                            fixel.InsertDtos(section);

                            //Base must be output first
                            //Then backboard?
                            //Then the rest
                            textWriter.WriteLine(fixel.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Fixel]));

                            //StringBuilder dividers = new StringBuilder();
                            List<PositionCacheItem> fixelPositions = _positionCacheItems.Values.Where(f => Equals(f.subcomponentId, fixel.subCompnentId)).ToList();

                            foreach (PositionCacheItem position in fixelPositions)
                            {
                                ProductCacheItem product = null;
                                if (_productCacheItems.TryGetValue(position.ProductId, out product))
                                {
                                    position.InsertDtos(fixel);
                                    position.InsertDtos(product);
                                    textWriter.WriteLine(position.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Position]));
                                }
                            }

                            //We group by x as in ccm we can stack deep. i.e. One divider may serve several positions.
                            //foreach (var positions in fixelPositions.GroupBy(p => p.GetDto<PlanogramPositionDto>().X))
                            //{
                            //    String divider = fixel.CreateXDividerForPosition(positions.First());
                            //    if (!String.IsNullOrWhiteSpace(divider))
                            //    {
                            //        dividers.AppendLine(divider);
                            //    }
                            //}

                            //Create the end divider if there is one.
                            //String end = fixel.CreateXEndDivider();
                            //if (!String.IsNullOrWhiteSpace(end))
                            //{
                            //    dividers.AppendLine(end);
                            //}

                            //if (dividers.Length > 0)
                            //{
                            //    textWriter.WriteLine(dividers.ToString());
                            //}
                        }
                    }

                    if (baseboard != null)
                    {
                        baseboard.InsertDtos(_sectionCacheItem);
                        textWriter.WriteLine(baseboard.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Fixel]));
                    }

                    foreach (FixelAnnotationCacheItem annotation in _fixelAnnotationCacheItems.Values)
                    {
                        textWriter.WriteLine(annotation.Encode(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Fixel]));
                    }

                    //write out the pegs
                    textWriter.WriteLine(this.PegLibrary.EncodeLibrary(PackageItem.PackageSchemaDefinitions[DalCacheItemType.Peg]));

                    //Ensure we add at least two new lines to the end of the file. Otherwise spaceman won't open.
                    textWriter.WriteLine(Environment.NewLine);
                }

                //Log end
                LogInformation(
                    Language.ExportLog_ExportComplete_Description, String.Format(Language.ExportLog_ExportComplete_Content, sourcePlanogramName, this.FilePath),
                    PlanogramEventLogAffectedType.Planogram);

            }
            catch (Exception ex)
            {
                //rethow the error if we don't log it
                if (!LogError(ex.Source, ex.Message, PlanogramEventLogAffectedType.Planogram))
                {
                    //Kill off the file stream just to be sure
                    if (_fileStream != null) _fileStream.Dispose();
                    _fileStream = null;

                    //Restore the original file, if any, to its location
                    SpacePlanningExportHelper.RestoreExportFileBackup(_filePath);
                    throw;
                }
            }
            finally
            {
                Close();
            }
        }

        #region Logging
        /// <summary>
        ///  Logs a warning to the ExportMappingContext
        /// </summary>
        /// <param name="description"></param>
        /// <param name="content"></param>
        /// <param name="affectedType"></param>
        /// <returns>true if a log was made</returns>
        internal Boolean LogWarning(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            return Log(description, content, PlanogramEventLogEntryType.Warning, affectedType);
        }

        /// <summary>
        /// Logs an error to the ExportMappingContext
        /// </summary>
        /// <param name="description"></param>
        /// <param name="content"></param>
        /// <param name="affectedType"></param>
        /// <returns>true if a log was made</returns>
        internal Boolean LogError(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            return Log(description, content, PlanogramEventLogEntryType.Error, affectedType);
        }

        /// <summary>
        /// Logs an information message to the ExportMappingContext
        /// </summary>
        /// <param name="description"></param>
        /// <param name="content"></param>
        /// <param name="affectedType"></param>
        /// <returns>true if a log was made</returns>
        internal Boolean LogInformation(String description, String content, PlanogramEventLogAffectedType affectedType)
        {
            return Log(description, content, PlanogramEventLogEntryType.Information, affectedType);
        }

        /// <summary>
        /// Logs a typed exception to the ExportMappingContext
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ex"></param>
        /// <returns>true if a log was made</returns>
        internal Boolean LogException<T>(Exception ex)
        {
            if (this.ExportMappingContext != null && this.ExportMappingContext.ViewErrorLogsOnCompletion)
            {
                //log exception
                SpacePlanningExportHelper.CreateErrorEventLogEntry<T>(this.PackageItem.Id, this.ExportMappingContext.ExportEvents, ex.Message);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Logs to the ExportMappingContext
        /// </summary>
        /// <param name="description"></param>
        /// <param name="content"></param>
        /// <param name="affectedType"></param>
        /// <returns>true if a log was made</returns>
        private Boolean Log(String description, String content, PlanogramEventLogEntryType logType, PlanogramEventLogAffectedType affectedType)
        {
            if (this.ExportMappingContext != null && this.ExportMappingContext.ViewErrorLogsOnCompletion)
            {
                SpacePlanningExportHelper.CreateEventLogEntry(this.PackageItem.Id, this.ExportMappingContext.ExportEvents, description, content, logType, affectedType);
                return true;
            }

            return false;
        }
        #endregion

        #endregion


    }
}
