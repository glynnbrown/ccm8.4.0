﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31548 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pln.CacheItems;
using Galleria.Framework.Planograms.External;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    public class PegLibrary
    {
        #region Fields
        private DalCache _parentCache;
        private Dictionary<String, PegCacheItem> _pegItems = new Dictionary<String, PegCacheItem>();
        #endregion

        #region Constructor
        public PegLibrary(DalCache dalCache)
        {
            _parentCache = dalCache;
        }
        #endregion

        #region Methods

        public String GetPegId(PositionCacheItem positionItem)
        {
            PlanogramSubComponentDto subComponentDto = positionItem.GetDto<PlanogramSubComponentDto>();
            PlanogramComponentDto componentDto = positionItem.GetDto<PlanogramComponentDto>();
            PlanogramProductDto productDto = positionItem.GetDto<PlanogramProductDto>();
            PlanogramFixtureDto fixtureDto = positionItem.GetDto<PlanogramFixtureDto>();

            if (subComponentDto != null &&
                componentDto != null &&
                fixtureDto != null &&
                productDto != null)
            {
                Single pegDepth = GetPegDepth(productDto.PegDepth, subComponentDto.MerchandisableDepth, fixtureDto.Depth - componentDto.Depth);
                Int32 span = EstimateSpan(productDto.PegProngOffsetX, subComponentDto.MerchConstraintRow1SpacingX);
                String pegId = CreatePegId(componentDto.Name, pegDepth, span);

                PegCacheItem pegItem = null;
                if (!_pegItems.TryGetValue(pegId, out pegItem))
                {
                    pegItem = new PegCacheItem(_parentCache, pegId);
                    pegItem.InsertDto<PlanogramSubComponentDto>(subComponentDto, subComponentDto.Id);
                    pegItem.InsertDto<PlanogramComponentDto>(componentDto, componentDto.Id);
                    pegItem.InsertDto<PlanogramProductDto>(productDto, productDto.Id);
                    pegItem.InsertDto<PlanogramFixtureDto>(fixtureDto, fixtureDto.Id);
                    
                    _pegItems[pegId] = pegItem;
                }

                return pegItem.Id;
            }

            return "****"; //default spaceman peg id
        }

        public String EncodeLibrary(Dictionary<String, SpacemanSchemaItem> pegSchemaDefinition)
        {
            StringBuilder output = new StringBuilder();

            foreach (PegCacheItem item in _pegItems.Values)
            {
                output.AppendLine(item.Encode(pegSchemaDefinition));
            }

            return output.ToString();
        }

        #endregion
        #region Static Methods
        /// <summary>
        /// Estimate how many peg holes the peg should span
        /// </summary>
        public static Int32 EstimateSpan(Single pegProngOffsetX, Single pegGap)
        {
            if (pegProngOffsetX != 0)
            {
                return (Int32)(((Math.Abs(pegProngOffsetX) * 2) / pegGap) + 1);
            }

            return 1;            
        }

        /// <summary>
        /// Get the correct peg depth
        /// </summary>
        /// <param name="pegDepth"></param>
        /// <param name="merchDepth"></param>
        /// <returns></returns>
        public static Single GetPegDepth(Single pegDepth, Single merchDepth, Single bayDepth)
        {
            return pegDepth > 0 ? pegDepth : merchDepth > 0 ? merchDepth : bayDepth;
        }

        public static String CreatePegId(String componentName, Single pegDepth, Int32 span)
        {
            return String.Format("{0}/{1}/{2}", componentName, pegDepth, span);
        }

        #endregion
    }
}
