﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
// V8-18545 : J.Pickup
// Changes to exports to ensure custom fields don't corrupt file format when contain field seperator. Changes to dividers, based on new spaceman understanding.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.Dal.Pln
{

    public enum SpacemanFieldType
    {
        Unknown = -99,
        Id = -1,
        StringId = 1,
        String = 2
    }

    public class SpacemanSchemaItem
    {
        public String fieldName;
        public Int32 fieldIndex;
        public SpacemanFieldType fieldType;

        public SpacemanSchemaItem(String fieldName, Int32 fieldIndex, String fieldType)
        {
            this.fieldName = fieldName;
            this.fieldIndex = fieldIndex;

            switch (fieldType)
            {
                case "-1":
                    this.fieldType = SpacemanFieldType.Id;
                    break;
                case "1":
                    this.fieldType = SpacemanFieldType.StringId;
                    break;
                case "2":
                    this.fieldType = SpacemanFieldType.String;
                    break;

                default:
                    this.fieldType = SpacemanFieldType.Unknown;
                    break;
            }
        }
    }
}
