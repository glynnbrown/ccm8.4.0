﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramPerformanceDataDal : DalBase, IPlanogramPerformanceDataDal
    {
        #region Fetch

        public IEnumerable<PlanogramPerformanceDataDto> FetchByPlanogramPerformanceId(object planogramPerformanceId)
        {
            List<PlanogramPerformanceDataDto> dtoList = new List<PlanogramPerformanceDataDto>();
            foreach (PlanogramPerformanceDataDto dto in this.DalContext.DalCache.PlanogramPerformanceDataDtoList)
            {
                if (dto.PlanogramPerformanceId.Equals(planogramPerformanceId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceDataDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramPerformanceDataDto>(dto, dto.PlanogramProductId, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
           foreach(PlanogramPerformanceDataDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPerformanceDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
