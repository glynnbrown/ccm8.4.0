﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssemblyComponentDal : DalBase, IPlanogramAssemblyComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssemblyComponentDto> FetchByPlanogramAssemblyId(object planogramAssemblyId)
        {
            return new List<PlanogramAssemblyComponentDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssemblyComponentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAssemblyComponentDto>(dto, dto.PlanogramComponentId, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            foreach (PlanogramAssemblyComponentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAssemblyComponentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssemblyComponentDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssemblyComponentDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
