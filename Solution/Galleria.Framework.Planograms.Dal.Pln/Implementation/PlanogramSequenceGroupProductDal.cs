﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramSequenceGroupProductDal : DalBase, IPlanogramSequenceGroupProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramSequenceGroupProductDto> FetchByPlanogramSequenceGroupId(object planogramSequenceGroupId)
        {
            return new List<PlanogramSequenceGroupProductDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramSequenceGroupProductDto dto)
        {
            
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            
        }

        #endregion

        #region Update

        public void Update(PlanogramSequenceGroupProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramSequenceGroupProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramSequenceGroupProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
