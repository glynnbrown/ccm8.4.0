﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramConsumerDecisionTreeDal : DalBase, IPlanogramConsumerDecisionTreeDal
    {
        #region Fetch

        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(object id)
        {
            PlanogramConsumerDecisionTreeDto dto = this.DalContext.DalCache.PlanogramConsumerDecisionTreeDtoList.Where(p => p.Id.Equals(id)).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return Clone(dto);
        }

        #endregion

        #region Insert

        public void Insert(PlanogramConsumerDecisionTreeDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramConsumerDecisionTreeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramConsumerDecisionTreeDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramConsumerDecisionTreeDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
