﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion

#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssortmentDal : DalBase, IPlanogramAssortmentDal
    {
        #region Fetch

        public PlanogramAssortmentDto FetchByPlanogramId(object id)
        {
            return new PlanogramAssortmentDto()
            {
                Id = 1,
                PlanogramId = id,
                Name = Message.PlanogramAssortment_Name_Default
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramAssortmentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
