﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31999 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramComparisonResultDal : DalBase, IPlanogramComparisonResultDal
    {
        #region Fetch

        public IEnumerable<PlanogramComparisonResultDto> FetchByPlanogramComparisonId(object id)
        {
            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComparisonResultDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramComparisonResultDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramComparisonResultDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramComparisonResultDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
