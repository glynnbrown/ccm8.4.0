﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-27919 : L.Ineson
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM802
// V8-29183 : D.Pleasance
//  Added default for DaysOfPerformance.
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramInventoryDal : DalBase, IPlanogramInventoryDal
    {
        #region Fetch

        public PlanogramInventoryDto FetchByPlanogramId(object id)
        {
            return new PlanogramInventoryDto()
            {
                Id = 1,
                PlanogramId = id,
                DaysOfPerformance = 7
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramInventoryDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramInventoryDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramInventoryDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramInventoryDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
