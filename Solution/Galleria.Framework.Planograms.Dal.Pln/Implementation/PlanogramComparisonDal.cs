﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31999 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramComparisonDal : DalBase, IPlanogramComparisonDal
    {
        #region Fetch

        public PlanogramComparisonDto FetchByPlanogramId(object id)
        {
            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComparisonDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramComparisonDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramComparisonDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramComparisonDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramComparisonDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramComparisonDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
