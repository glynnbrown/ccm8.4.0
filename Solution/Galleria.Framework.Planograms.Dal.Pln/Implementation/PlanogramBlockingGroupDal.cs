﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramBlockingGroupDal : DalBase, IPlanogramBlockingGroupDal
    {
        #region Fetch

        public IEnumerable<PlanogramBlockingGroupDto> FetchByPlanogramBlockingId(object id)
        {
            return new List<PlanogramBlockingGroupDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramBlockingGroupDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramBlockingGroupDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramBlockingGroupDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramBlockingGroupDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}