﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramPositionDal : DalBase, IPlanogramPositionDal
    {
        #region Fetch

        public IEnumerable<PlanogramPositionDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramPositionDto> dtoList = new List<PlanogramPositionDto>();
            foreach (PlanogramPositionDto dto in this.DalContext.DalCache.PlanogramPositionDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPositionDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramPositionDto>(dto, dto.Id, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramPositionDto> dtos)
        {
            foreach (PlanogramPositionDto dto in dtos)
            {
                Insert(dto);

            }
        }

        #endregion

        #region Update

        public void Update(PlanogramPositionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPositionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPositionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPositionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
