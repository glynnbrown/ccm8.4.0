﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM803
// V8-29404 : D.Pleasance
//  Amended defaults
#endregion 
#region Version History: CCM810
// V8-29818 : L.Ineson
//  Amended defaults.
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramRenumberingStrategyDal : DalBase, IPlanogramRenumberingStrategyDal
    {
        #region Fetch

        public PlanogramRenumberingStrategyDto FetchByPlanogramId(object planogramId)
        {
            return new PlanogramRenumberingStrategyDto()
            {
                Id = 1,
                PlanogramId = planogramId,
                Name = Language.PlanogramRenumberingStrategy_Name_Default,
                BayComponentXRenumberStrategyPriority = 1,
                BayComponentYRenumberStrategyPriority = 2,
                BayComponentZRenumberStrategyPriority = 3,
                PositionXRenumberStrategyPriority = 1,
                PositionYRenumberStrategyPriority = 2,
                PositionZRenumberStrategyPriority = 3,
                BayComponentXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                BayComponentYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                BayComponentZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                PositionXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                PositionYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                PositionZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                RestartComponentRenumberingPerBay = true,
                IgnoreNonMerchandisingComponents = true,
                RestartPositionRenumberingPerComponent = true,
                UniqueNumberMultiPositionProductsPerComponent = false,
                ExceptAdjacentPositions= false
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramRenumberingStrategyDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramRenumberingStrategyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramRenumberingStrategyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramRenumberingStrategyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
