﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramPerformanceDal : DalBase, IPlanogramPerformanceDal
    {
        #region Fetch

        public PlanogramPerformanceDto FetchByPlanogramId(object id)
        {
            return new PlanogramPerformanceDto()
            {
                Id = 1,
                PlanogramId = id,
                Name = Message.PlanogramPerformance_Name_Default
            };
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(System.Collections.Generic.IEnumerable<PlanogramPerformanceDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
