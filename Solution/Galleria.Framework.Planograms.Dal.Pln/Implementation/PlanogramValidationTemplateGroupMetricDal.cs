﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramValidationTemplateGroupMetricDal : DalBase, IPlanogramValidationTemplateGroupMetricDal
    {
        #region Fetch

        public IEnumerable<PlanogramValidationTemplateGroupMetricDto> FetchByPlanogramValidationTemplateGroupId(object planogramValidationTemplateGroupId)
        {
            return new List<PlanogramValidationTemplateGroupMetricDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramValidationTemplateGroupMetricDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramValidationTemplateGroupMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramValidationTemplateGroupMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramValidationTemplateGroupMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}