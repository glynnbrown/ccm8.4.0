﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramSubComponentDal : DalBase, IPlanogramSubComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(object planogramComponentId)
        {
            List<PlanogramSubComponentDto> dtoList = new List<PlanogramSubComponentDto>();
            foreach (PlanogramSubComponentDto dto in this.DalContext.DalCache.PlanogramSubComponentDtoList)
            {
                if (dto.PlanogramComponentId.Equals(planogramComponentId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramSubComponentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramSubComponentDto>(dto, dto.PlanogramComponentId, dto.Id);

        }

        public void Insert(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            foreach (PlanogramSubComponentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramSubComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSubComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramSubComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
