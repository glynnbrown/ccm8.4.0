﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramFixtureComponentDal : DalBase, IPlanogramFixtureComponentDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureComponentDto> FetchByPlanogramFixtureId(object planogramFixtureId)
        {
            List<PlanogramFixtureComponentDto> dtoList = new List<PlanogramFixtureComponentDto>();
            foreach (PlanogramFixtureComponentDto dto in this.DalContext.DalCache.PlanogramFixtureComponentDtoList)
            {
                if (dto.PlanogramFixtureId.Equals(planogramFixtureId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureComponentDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureComponentDto>(dto, dto.PlanogramComponentId, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            foreach (PlanogramFixtureComponentDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureComponentDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureComponentDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
