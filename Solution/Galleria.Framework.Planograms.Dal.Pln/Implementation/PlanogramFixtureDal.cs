﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramFixtureDal : DalBase, IPlanogramFixtureDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramFixtureDto> dtoList = new List<PlanogramFixtureDto>();
            foreach (PlanogramFixtureDto dto in this.DalContext.DalCache.PlanogramFixtureDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureDto>(dto, dto.Id, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramFixtureDto> dtos)
        {
            foreach(PlanogramFixtureDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramFixtureDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
