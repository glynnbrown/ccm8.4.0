﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
// CCM-28622 : D.Pleasance
//  Amended so that default metrics are no longer created they come from the specified metric mappings
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramPerformanceMetricDal : DalBase, IPlanogramPerformanceMetricDal
    {
        #region Fetch

        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(object planogramPerformanceId)
        {
            List<PlanogramPerformanceMetricDto> dtoList = new List<PlanogramPerformanceMetricDto>();
            foreach (PlanogramPerformanceMetricDto dto in this.DalContext.DalCache.PlanogramPerformanceMetricDtoList)
            {
                if (dto.PlanogramPerformanceId.Equals(planogramPerformanceId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramPerformanceMetricDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramPerformanceMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramPerformanceMetricDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramPerformanceMetricDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
