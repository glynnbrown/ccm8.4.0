﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attributes
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class CustomAttributeDataDal : DalBase, ICustomAttributeDataDal
    {
        #region Fetch

        public CustomAttributeDataDto FetchByParentTypeParentId(byte parentType, object parentId)
        {
            foreach (CustomAttributeDataDto dto in this.DalContext.DalCache.CustomAttributeDataDtoList)
            {
                if (dto.ParentType == parentType && dto.ParentId.Equals(parentId))
                {
                    return Clone(dto);
                }
            }

            return new CustomAttributeDataDto() { Id = IdentityHelper.GetNextInt32(), ParentType = parentType, ParentId = parentId };
        }

        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            HashSet<Object> parentLookup = new HashSet<Object>();
            foreach (Object parentId in parentIds) parentLookup.Add(parentId);

            List<CustomAttributeDataDto> dtos = new List<CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in this.DalContext.DalCache.CustomAttributeDataDtoList)
            {
                if ((dto.ParentType == parentType) && (parentLookup.Contains(dto.ParentId)))
                {
                    dtos.Add(Clone(dto));
                }
            }
            return dtos;
        }

        #endregion

        #region Insert

        public void Insert(CustomAttributeDataDto dto)
        {
            this.DalContext.DalCache.InsertDto<CustomAttributeDataDto>(dto, dto.ParentId, dto.Id);
        }

        public void Insert(IEnumerable<CustomAttributeDataDto> dtos)
        {
            foreach (CustomAttributeDataDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(CustomAttributeDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<CustomAttributeDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Upsert

        public void Upsert(IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomAttributeDataDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<CustomAttributeDataDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
