﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramEventLogDal : DalBase, IPlanogramEventLogDal
    {
        #region Fetch

        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramEventLogDto> dtoList = new List<PlanogramEventLogDto>();
            foreach (PlanogramEventLogDto dto in this.DalContext.DalCache.PlanogramEventLogDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramEventLogDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<PlanogramEventLogDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramEventLogDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramEventLogDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramEventLogDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramEventLogDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
