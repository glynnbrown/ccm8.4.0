﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-31550 : A.Probyn
//  Created
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssortmentLocationBuddyDal : DalBase, IPlanogramAssortmentLocationBuddyDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentLocationBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            return new List<PlanogramAssortmentLocationBuddyDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentLocationBuddyDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentLocationBuddyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentLocationBuddyDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssortmentLocationBuddyDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
