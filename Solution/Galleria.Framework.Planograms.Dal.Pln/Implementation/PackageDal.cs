﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
// CCM-27919 : L.Ineson
//  Added field mappings fetch argument to FetchById
#endregion
#region Version History: CCM801
// CCM-28622 : D.Pleasance
//  Added MetricMapping
#endregion

#region Version History: CCM820

// V8-31175 : A.Silva
//  Amended FetchById to not care about the Package ID parameter as there is only one package in the dalcache.

#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.External;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    /// <summary>
    /// PackageDal implementation
    /// </summary>
    public class PackageDal : DalBase, IPackageDal
    {
        #region Lock
        /// <summary>
        /// Locks the specified item
        /// </summary>
        public Byte LockById(Object id, Object userId, Byte lockType, Boolean readOnly)
        {
            Boolean successful = true;
            try
            {
                DalContext.DalCache.Open(readOnly);
            }
            catch
            {
                successful = false;
            }
            return (Byte)(successful ? 1 : 0);
        }
        #endregion

        #region Unlock
        /// <summary>
        /// Unlocks the specified item
        /// </summary>
        public Byte UnlockById(Object id, Object lockUserId, Byte lockType, Boolean lockReadOnly)
        {
            // ensure that the id matches the file
            if (!id.Equals(this.DalContext.DalCache.FilePath)) throw new DtoDoesNotExistException();

            // open the file in the cache
            this.DalContext.DalCache.Close();

            // reutrn success
            return 1; // Success
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        public PackageDto FetchById(Object id)
        {
            return FetchById(id, new List<PlanogramFieldMappingDto>());
        }

        /// <summary>
        /// Returns the specified dto from the data source
        /// </summary>
        public PackageDto FetchById(Object id, Object fetchArgument)
        {
            // refresh cache if package is being loaded again, the mapping template may be different
            DalContext.DalCache.RefreshCache();

            //set the optional field mappings
            //  Get the Custom Field Mappings from the argument if there are any...
            var args = fetchArgument as Object[];
            ImportContext context = null;

            //  If there are fetch argunments, and contain an Import Context, use it.
            if (args != null) context = args.OfType<ImportContext>().FirstOrDefault();

            //  If there is no Import Context, create an empty one.
            if (context == null) context = new ImportContext();

            DalContext.DalCache.FieldMappings = context.FieldMappings;
            DalContext.DalCache.MetricMappings = context.MetricMappings;

            // NB this call is just to initialize the dto.
            Object package = DalContext.DalCache.PackageItem.Dto;

            PackageDto dto = DalContext.DalCache.PackageDtoList.FirstOrDefault();
            if (dto != null) return Clone(dto);
            throw new DtoDoesNotExistException();
        }

        public IList<Object> FetchIdsExceptFor(IEnumerable<string> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastModified)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        #endregion

        #region IPackageDal Members

        public void Insert(PackageDto dto)
        {
            this.DalContext.DalCache.InsertDto<PackageDto>(dto, dto.Id, dto.Id);
        }

        public void Update(PackageDto dto)
        {
            throw new NotImplementedException();
        }

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public byte UpdatePlanogramAttributes(IEnumerable<PlanogramAttributesDto> planAttributesDtoList)
        {
            throw new NotImplementedException();
        } 

        #endregion
    }
}