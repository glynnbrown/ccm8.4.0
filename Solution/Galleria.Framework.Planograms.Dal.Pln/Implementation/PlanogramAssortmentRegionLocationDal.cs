﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssortmentRegionLocationDal : DalBase, IPlanogramAssortmentRegionLocationDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentRegionLocationDto> FetchByPlanogramAssortmentRegionId(object id)
        {
            return new List<PlanogramAssortmentRegionLocationDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentRegionLocationDto dto)
        {
            //
        }

        public void Insert(IEnumerable<PlanogramAssortmentRegionLocationDto> dtos)
        {
            //
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentRegionLocationDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentRegionLocationDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentRegionLocationDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentRegionLocationDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
