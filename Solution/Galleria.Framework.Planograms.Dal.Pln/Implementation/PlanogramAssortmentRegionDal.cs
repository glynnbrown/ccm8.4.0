﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssortmentRegionDal : DalBase, IPlanogramAssortmentRegionDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentRegionDto> FetchByPlanogramAssortmentId(object id)
        {
            return new List<PlanogramAssortmentRegionDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentRegionDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentRegionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAssortmentRegionDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAssortmentRegionDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
