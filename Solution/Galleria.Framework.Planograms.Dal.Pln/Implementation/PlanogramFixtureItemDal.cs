﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramFixtureItemDal : DalBase, IPlanogramFixtureItemDal
    {
        #region Fetch

        public IEnumerable<PlanogramFixtureItemDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramFixtureItemDto> dtoList = new List<PlanogramFixtureItemDto>();
            foreach (PlanogramFixtureItemDto dto in this.DalContext.DalCache.PlanogramFixtureItemDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramFixtureItemDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramFixtureItemDto>(dto, dto.PlanogramFixtureId, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            foreach (PlanogramFixtureItemDto dto in dtos)
            {
                Insert(dto);
            }
        }

        public void Update(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(PlanogramFixtureItemDto dto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramFixtureItemDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramFixtureItemDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
