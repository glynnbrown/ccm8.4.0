﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAnnotationDal : DalBase, IPlanogramAnnotationDal
    {
        #region Fetch

        public IEnumerable<PlanogramAnnotationDto> FetchByPlanogramId(object planogramId)
        {
            List<PlanogramAnnotationDto> dtoList = new List<PlanogramAnnotationDto>();
            foreach (PlanogramAnnotationDto dto in this.DalContext.DalCache.PlanogramAnnotationDtoList)
            {
                if (dto.PlanogramId.Equals(planogramId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAnnotationDto dto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramAnnotationDto>(dto, dto.Id, dto.Id);
        }

        public void Insert(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            foreach (PlanogramAnnotationDto dto in dtos)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update

        public void Update(PlanogramAnnotationDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramAnnotationDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<PlanogramAnnotationDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
