﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramDal : DalBase, IPlanogramDal
    {
        #region Fetch

        public IEnumerable<PlanogramDto> FetchByPackageId(object packageId)
        {
            List<PlanogramDto> dtoList = new List<PlanogramDto>();
            foreach (PlanogramDto dto in this.DalContext.DalCache.PlanogramDtoList)
            {
                if (dto.PackageId.Equals(packageId))
                {
                    dtoList.Add(Clone(dto));
                }
            }
            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PackageDto packageDto, PlanogramDto planogramDto)
        {
            this.DalContext.DalCache.InsertDto<PlanogramDto>(planogramDto, planogramDto.PackageId, planogramDto.Id);
        }

        public void Insert(PlanogramDto planogramDto)
        {
            Insert(null, planogramDto);
        }

        #endregion

        #region Update

        public void Update(PackageDto packageDto, PlanogramDto planogramDto)
        {
            throw new NotImplementedException();
        }

        public void Update(PlanogramDto planogramDto)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
