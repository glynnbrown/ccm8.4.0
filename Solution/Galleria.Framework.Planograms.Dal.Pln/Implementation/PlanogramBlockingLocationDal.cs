﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramBlockingLocationDal : DalBase, IPlanogramBlockingLocationDal
    {
        #region Fetch

        public IEnumerable<PlanogramBlockingLocationDto> FetchByPlanogramBlockingId(object id)
        {
            return new List<PlanogramBlockingLocationDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramBlockingLocationDto dto)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        public void Insert(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            //Apollo does not use this data so no need to insert this into the DalCache
        }

        #endregion

        #region Update

        public void Update(PlanogramBlockingLocationDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramBlockingLocationDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramBlockingLocationDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}

