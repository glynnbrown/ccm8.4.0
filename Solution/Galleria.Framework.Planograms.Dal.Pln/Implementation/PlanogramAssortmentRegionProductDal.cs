﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31548 : J.Pickup
//  PCR01419 Export to Spaceman - created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramAssortmentRegionProductDal : DalBase, IPlanogramAssortmentRegionProductDal
    {
        #region Fetch

        public IEnumerable<PlanogramAssortmentRegionProductDto> FetchByPlanogramAssortmentRegionId(object id)
        {
            return new List<PlanogramAssortmentRegionProductDto>();
        }

        #endregion

        #region Insert

        public void Insert(PlanogramAssortmentRegionProductDto dto)
        {
            
        }

        public void Insert(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {

        }

        #endregion

        #region Update

        public void Update(PlanogramAssortmentRegionProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Update(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(PlanogramAssortmentRegionProductDto dto)
        {
            Debug.Assert(false, "Not Implemented");
        }

        public void Delete(IEnumerable<PlanogramAssortmentRegionProductDto> dtos)
        {
            Debug.Assert(false, "Not Implemented");
        }

        #endregion
    }
}
