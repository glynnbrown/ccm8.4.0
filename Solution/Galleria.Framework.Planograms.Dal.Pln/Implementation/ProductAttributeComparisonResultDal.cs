﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31999 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class ProductAttributeComparisonResultDal : DalBase, IProductAttributeComparisonResultDal
    {
        #region Fetch

        public IEnumerable<ProductAttributeComparisonResultDto> FetchByPlanogramId(object id)
        {
            return new List<ProductAttributeComparisonResultDto>();
        }

        #endregion

        #region Insert

        public void Insert(ProductAttributeComparisonResultDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Update

        public void Update(ProductAttributeComparisonResultDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Delete

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }

        public void Delete(ProductAttributeComparisonResultDto dto)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<ProductAttributeComparisonResultDto> dtos)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
