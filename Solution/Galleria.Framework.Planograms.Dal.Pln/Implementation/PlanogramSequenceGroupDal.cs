﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28996 : A.Silva
//		Created
#endregion
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Pln.Implementation
{
    public class PlanogramSequenceGroupDal : DalBase, IPlanogramSequenceGroupDal
    {
        #region Fetch
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(object planogramSequenceId)
        {
            return new List<PlanogramSequenceGroupDto>();
        }
        #endregion

        #region Insert
        public void Insert(PlanogramSequenceGroupDto dto)
        {
            //throw new NotImplementedException();
        }

        public void Insert(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            //throw new NotImplementedException();
        }
        #endregion

        #region Update
        public void Update(PlanogramSequenceGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        public void Delete(IEnumerable<PlanogramSequenceGroupDto> dtos)
        {
            throw new NotImplementedException();
        }

        public void Delete(PlanogramSequenceGroupDto dto)
        {
            throw new NotImplementedException();
        }

        public void DeleteById(object id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}