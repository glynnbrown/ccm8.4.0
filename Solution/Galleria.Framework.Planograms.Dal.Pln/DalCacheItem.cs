﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM801
// V8-28622 : D.Pleasance
//  Added MetricMapping
// V8-28790 : D.Pleasance
//  Refactored to remove static property references
// V8-28792 : D.Pleasance
//  Amended so that backboard now sits to the floor and base is placed in front of backboard.
// V8-28821 : D.Pleasance
//  Amended fixture depth calculations, this is now set as either backboard depth or base depth depending with is greater.
// V8-28871 : D.Pleasance
//  Removed all rounding as this was skewing values so process now returns raw values.
#endregion
#region Version History: CCM802
// V8-28893 : D.Pleasance
//  Added functionality to load peg data and use this info to calculate peg depth \ facings deep
// V8-28994 : D.Pleasance
//  Added business rule validation for planogram product.
// V8-29207 : D.Pleasance
//  Amended notchStartY so that base height is factored in. This follows earlier changes in 801, V8-28792
#endregion
#region Version History: CCM803
// V8-29338 : D.Pleasance
//  Amended min cap logic so that if max stac is applied then the 1 min cap will only be applied if max stack allows it (max stack > front facing high) 
// V8-29323 : D.Pleasance
//  Amended CalculateShelfMerchandisingArea so that calculated merch heights take into account if the next element 
//  is a bar (hanging products) when calculating merchandising height.
#endregion
#region Version History: CCM810
// V8-29665 : L.Ineson
//  Added set of Planogram Product Placement settings.
// V8-29713 : D.Pleasance
//  Added CultureInfo.InvariantCulture so that data isnt converted to locale, should be the raw data from within the import file.
// V8-30006 : D.Pleasance
//  Corrected fill pattern mappings
// V8-29985 : D.Pleasance
//  Amended merchandisingStrategyZ for Bar to Back.
// V8-29533 : D.Pleasance
//  Added functionality to check for duplicate product gtins, if the exist known exception is thrown
// V8-30125 : D.Pleasance
//  Amended to capture distinct list of event log messages.
// V8-30143 : D.Pleasance
//  Added defaults for Position orientationTypeY, orientationTypeX.
// V8-30215 : M.Brumby
//  Added AggregationType
#endregion
#region Version History: CCM811
// V8-30534 : D.Pleasance
//  Amended decoding of product nesting information. It seems that if there is nest height data then this is the nesting stack that is 
//  applied even if there is nest height, width, depth data. CCM will attempt to adhere to all the nesting values where spaceman will stack to the first that has data.
// V8-30537 : D.Pleasance
//  Amended SubComponent setting isProductOverlapAllowed to always be true for all component types. 
// V8-30576 : N.Haywood
//  Changed default annotation to text box rather than bay annotation
#endregion
#region Version History: CCM820
// V8-30663 : D.Pleasance
//  Since V8 now has PlanogramSubComponent.IsProductSqueezeAllowed, this setting has now been mapped across. 
// V8-30664 : J.Pickup
//  Spaceman files with clear product colour (No Colour) now default over to white. 
// V8-30809 : A.Silva
//  Refactored MapSetValue to parse some default values, and made to use the common parser in the Import Helper.
// V8-30661 : D.Pleasance
//  Corrected mappings so that base depth isnt mapped as depth for creating a bay. This should be the fixel depth.
// V8-31189 : D.Pleasance
//  Added file version check and warning to DecodePackageDto().
// V8-31183 : D.Pleasance
//  Amended to also calculate peg depth for bar \ rod.
// V8-31499 : D.Pleasance
//  Amended if position is peg \ bar product and autoFillPeg is enabled and we havent got peg detail in the file 
//  then we take the units deep value from the file rather than defaulting deep to 1.
// V8-31528 : M.Brumby, D.Pleasance
//  Extra logic for handling break characters. Generate unique position Id to prevent copy errors in the workflow
#endregion
#region Version History: CCM830
// V8-31647 : D.Pleasance
//  Amended to ensure Planogram Name has a value, defaulted to the filename if mapping doesnt apply a value.
//V8-31672 : L.Ineson
//  Ensured that max top cap gets set.
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31694 : D.Pleasance
//  Amended so that for Complex positions default Max Right Stack \ Max Top Cap get applied where required. 
//  Non complex positions dont have Max Right Cap applied as in Spaceman non complex positions can never have right caps.
// V8-31855 : D.Pleasance
//  Added new method GetShelfMerchHeight so that correct merchandising height is obtained for positions that are on combined shelves.
// V8-31959 : D.Pleasance
//  Amended CalculateFacingsYHigh() so that caps can exceed max stack, Max stack is just for front facings.
// V8-31993 : D.Pleasance
//  Complex positions are set as manually placed items.
// V8-32000 : D.Pleasance
//  Cap style max stack now defaults max stack to 1 and max top cap 99.
// V8-32002 : L.Ineson
//  Bar merch strategy y is now even.
// V8-31548 : J.Pickup
//  Divider = 16 added to enum. 
// V8-32462 : J.Pickup
//  Fix to ensure that when importing a product with a facings higher than the max stack, we adhere to the max stack.
// V8-32628 : J.Pickup
//  Pallets now always have a manual x strategy. (Pallets always manual in spaceman). 
// V8-18545 : J.Pickup
// Changes to exports to ensure custom fields don't corrupt file format when contain field seperator. Changes to dividers, based on new spaceman understanding.
// CCM-13578 : A.Silva
//  Amended GetSingleValue to reproduce the apparent process done by Spaceplan when using these values (truncate at three decimal places, then round up to two).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Reflection;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using System.Globalization;
using Galleria.Framework.Planograms.Resources.Language;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Items used to cache data within the dal
    /// </summary>
    public class DalCacheItem : IDisposable
    {
        #region Enums
        enum PlanogramFixtureType
        {
            FixtureItem,
            Fixture,
            FixtureComponent,
            Component,
            SubComponent
        }

        internal enum SpacemanComponentType
        {
            Shelf = 0,
            Peg = 1,
            Chest = 2,
            Bar = 3,
            Pallet = 6,
            Frame = 7,
            Bay = 8,
            Basket = 11,
            Rod = 12,
            TextBox = 13, 
            Divider = 16
        }
        #endregion

        #region Fields
        private static Object _nextIdLock = new Object(); // object used for locking
        private static Int32 _nextId = 0; // holds the next id number
        private String _id; // holds the item id
        private String _data; // holds the file data
        private Object _dto; // holds the translated dto        
        private Dictionary<DalCacheItemType, List<DalCacheItem>> _children = new Dictionary<DalCacheItemType, List<DalCacheItem>>();
        private Dictionary<DalCacheItemType, Dictionary<String, SpacemanSchemaItem>> _schemaDefinitions = new Dictionary<DalCacheItemType, Dictionary<String, SpacemanSchemaItem>>();
        internal DalCache _dalCache;
        internal Dictionary<String, SpacemanSchemaItem> _localSchemaDefinition = new Dictionary<String, SpacemanSchemaItem>();
        internal String[] _exportData;


        private readonly Dictionary<Type, Object> _dtos = new Dictionary<Type, Object>();
        private readonly Dictionary<Type, Object> _dtoIds = new Dictionary<Type, Object>();

        #endregion

        #region Properties
        /// <summary>
        /// Returns the item id
        /// </summary>
        public String Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Returns the data for this item
        /// </summary>
        public String Data
        {
            get
            {
                return _data;
            }
        }

        /// <summary>
        /// Gets or sets the dto for this item
        /// </summary>
        public Object Dto
        {
            get
            {
                return _dto;
            }
            set
            {
                _dto = value;
            }
        }

        /// <summary>
        /// Returns the children of this
        /// </summary>
        public Dictionary<DalCacheItemType, List<DalCacheItem>> Children
        {
            get { return _children; }
        }
        
        /// <summary>
        ///     Holds the dtos per type of dto associated with this cache item.
        /// </summary>
        public Dictionary<Type, Object> Dtos { get { return _dtos; } }

        public Dictionary<Type, Object> DtoIds { get { return _dtoIds; } }

        internal Dictionary<DalCacheItemType, Dictionary<String, SpacemanSchemaItem>> PackageSchemaDefinitions
        {
            get { return _dalCache.PackageItem._schemaDefinitions; }
        }

        internal virtual DalCacheItemType CacheType
        {
            get
            {
                return DalCacheItemType.Unknown;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type. 
        /// </summary>
        internal DalCacheItem(DalCache dalCache, String id)
        {
            _dalCache = dalCache;
            _id = id;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCacheItem(DalCache dalCache, String id, FileStream fileStream, ReadOnlyCollection<PlanogramFieldMappingDto> fieldMappings, ReadOnlyCollection<PlanogramMetricMappingDto> metricMappings)
        {
            _dalCache = dalCache;

            using (EndOfLineStreamReader textReader = new EndOfLineStreamReader(fileStream))
            {
                this.Initialize(null, id, textReader, null);
            }

            this.loadDtos(id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalCacheItem(DalCache dalCache, DalCacheItem parent, EndOfLineStreamReader textReader, String data)
        {
            _dalCache = dalCache;
            this.Initialize(parent, null, textReader, data);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalCache"></param>
        /// <param name="dalCacheItem"></param>
        /// <param name="data"></param>
        public DalCacheItem(DalCache dalCache, DalCacheItem dalCacheItem, String data)
        {
            _dalCache = dalCache;
            _data = data;
        }
        #endregion

        #region Methods

        #region Initialization
        /// <summary>
        /// Initializes the dal cache item
        /// loading its data and its children
        /// </summary>
        private void Initialize(DalCacheItem parent, String id, EndOfLineStreamReader textReader, String data)
        {
            // store a reference to the parent

            // generate an id for this item
            if (id != null)
                _id = id;
            else
                _id = GetNextId();

            Boolean schemaLoaded = false;

            // now parse the data 
            while ((data != null) || (textReader.Peek() >= 0))
            {
                // read a line from the file
                if (data == null) data = ReadCompleteLineData(textReader);
                
                // get the item type from the data
                DalCacheItemType itemType = DalCacheItemTypeHelper.GetDalCacheItemType(data);

                if (!schemaLoaded)
                {
                    if (data.Length == 0) // Loading of schema complete
                    {
                        schemaLoaded = true;
                    }
                    else
                    {
                        this.InitializeSchema(data);
                    }
                }
                else
                {
                    if (itemType != DalCacheItemType.Unknown)
                    {
                        this.LoadData(itemType, data);
                    }
                }

                data = null;
            }
        }

        private void LoadData(DalCacheItemType itemType, String data)
        {
            if (itemType == DalCacheItemType.Store)
            {
                _data = data;
            }
            else
            {
                // verify we have created a list to hold the children
                if (!_children.ContainsKey(itemType))
                {
                    _children.Add(itemType, new List<DalCacheItem>());
                }

                // Add plan data
                _children[itemType].Add(new DalCacheItem(_dalCache, this, data));
            }
        }

        /// <summary>
        /// Initialises the data row schema definitions
        /// </summary>
        /// <param name="data"></param>
        internal void InitializeSchema(String data)
        {
            String[] dataItems = data.Split(',');

            if (dataItems.Length > 0)
            {
                DalCacheItemType itemType = DalCacheItemTypeHelper.GetDalCacheItemType(dataItems[0]);
                if (!_schemaDefinitions.ContainsKey(itemType))
                {
                    _schemaDefinitions.Add(itemType, new Dictionary<String, SpacemanSchemaItem>());
                    Int32 position = 1;

                    for (Int32 i = 1; i < dataItems.Length; i += 2)
                    {
                        String val = dataItems[i];
                        val = val.Replace("\"", String.Empty).Trim();

                        if (!String.IsNullOrEmpty(val))
                        {
                            if (!_schemaDefinitions[itemType].ContainsKey(val))
                            {
                                Int32 index = i + 1;
                                if (dataItems.Length > index)
                                {
                                    _schemaDefinitions[itemType].Add(val, new SpacemanSchemaItem(val, position, dataItems[i + 1]));
                                }
                                else
                                {
                                    _schemaDefinitions[itemType].Add(val, new SpacemanSchemaItem(val, position, String.Empty));
                                }
                            }
                            else
                            {

                            }
                        }
                        position++;
                    }                    
                }
            }
        }
        
        /// <summary>
        /// Initializes all children of the specified type
        /// </summary>
        private void InitializeChildren(DalCacheItem parent, DalCacheItemType itemType, EndOfLineStreamReader textReader, ref String data)
        {
            // verify we have created a list to hold the children
            if (!_children.ContainsKey(itemType))
            {
                _children.Add(itemType, new List<DalCacheItem>());
            }

            // now process all items in the file until we hit an item type
            // that is not what we are expecting
            while ((data != null) || (textReader.Peek() >= 0))
            {
                // read the next line from the file
                if (data == null) data = textReader.ReadLine();

                // get the line item type
                DalCacheItemType nextItemType = DalCacheItemTypeHelper.GetDalCacheItemType(data);

                // determine if the item type matches
                // that which we are expecting
                if (nextItemType == itemType)
                {
                    // the item matches the type we are expecting
                    // so create a new item and add to the child list
                    _children[itemType].Add(new DalCacheItem(_dalCache, parent, textReader, data));

                    // clear the data so that we read the next line
                    data = null;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Loads all dtos from file
        /// </summary>
        /// <param name="id"></param>
        private void loadDtos(String id)
        {
            if (id == this.Id)
            {
                IEnumerable<PlanogramFieldMappingDto> planogramFieldMappings = _dalCache.FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Planogram).AsEnumerable();
                IEnumerable<PlanogramFieldMappingDto> fixtureFieldMappings = _dalCache.FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Fixture).AsEnumerable();
                IEnumerable<PlanogramFieldMappingDto> componentFieldMappings = _dalCache.FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Component).AsEnumerable();
                IEnumerable<PlanogramFieldMappingDto> productFieldMappings = _dalCache.FieldMappings.Where(p => p.Type == (Byte)PlanogramFieldMappingType.Product).AsEnumerable();
                IEnumerable<PlanogramMetricMappingDto> planogramMetricMappings = _dalCache.MetricMappings.AsEnumerable();

                String[] sectionData = null;
                Boolean hasBase = false;

                foreach (var planogramItem in this.Children[DalCacheItemType.Section])
                {
                    sectionData = GetData(planogramItem.Data).ToArray();
                    planogramItem.Dto = DecodePlanogramDto(id, sectionData, this._schemaDefinitions[DalCacheItemType.Section], planogramFieldMappings, ref hasBase);
                }

                this.Dto = DecodePackageDto(id, this.Data, sectionData, this._schemaDefinitions[DalCacheItemType.Store], this._schemaDefinitions[DalCacheItemType.Section]);

                List<PlanogramAnnotationDto> availableFontDetails = new List<PlanogramAnnotationDto>();

                if (this.Children.ContainsKey(DalCacheItemType.Font))
                {
                    foreach (var fontItem in this.Children[DalCacheItemType.Font])
                    {
                        availableFontDetails.Add(DecodeAvailableFonts(fontItem.Data, this._schemaDefinitions[DalCacheItemType.Font]));
                    }
                }

                List<PegData> pegDataList = new List<PegData>();

                if (this.Children.ContainsKey(DalCacheItemType.Peg))
                {
                    foreach (var pegItem in this.Children[DalCacheItemType.Peg])
                    {
                        pegDataList.Add(DecodeAvailablePegs(pegItem.Data, this._schemaDefinitions[DalCacheItemType.Peg]));
                    }
                }

                List<Int16> segments = new List<Int16>();
                Dictionary<String, String> componentSegmentLookup = new Dictionary<String, String>();
                Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups = new Dictionary<String, Dictionary<PlanogramFixtureType, object>>();
                List<PlanogramFixtureItemDto> bayPositions = new List<PlanogramFixtureItemDto>();
                List<PlanogramFixtureDto> bays = new List<PlanogramFixtureDto>();
                List<PlanogramFixtureComponentDto> bayComponents = new List<PlanogramFixtureComponentDto>();
                List<Tuple<String, Int16, Single, Single, Single, Single>> frameDetails = new List<Tuple<String, Int16, Single, Single, Single, Single>>();
                Dictionary<String, Single> subComponentMerchandisingArea = new Dictionary<String, Single>();
                Dictionary<String, PlanogramProductDto> productLookup = new Dictionary<String, PlanogramProductDto>();
                Dictionary<String, List<ElementPositionDetail>> elementPositionLookupDetail = new Dictionary<string, List<ElementPositionDetail>>();

                if (this.Children.ContainsKey(DalCacheItemType.Fixel))
                {
                    foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
                    {
                        String[] fixelData = GetData(fixelItem.Data).ToArray();
                        List<object> fixelDtos = new List<object>();

                        Byte componentType = GetByteValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelType, DalCacheItemType.Fixel);
                        Int16 segment = GetInt16Value(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel);
                        fixelItem._id = GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel);

                        switch (componentType)
                        {
                            case (Byte)SpacemanComponentType.Frame:
                                Single xPos = GetSingleValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelX, DalCacheItemType.Fixel);
                                Single width = GetSingleValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel);
                                Single notchStart = GetSingleValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelVertStart, DalCacheItemType.Fixel);
                                if (hasBase) notchStart += GetSingleValue(sectionData, this._schemaDefinitions[DalCacheItemType.Section], SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
                                Single notchSpacing = GetSingleValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelVertSpacing, DalCacheItemType.Fixel);
                                if (!frameDetails.Select(p => p.Item1).ToList().Contains(fixelItem._id)) // if we havent already obtained the frame detail add, we kick out duplicates
                                {
                                    frameDetails.Add(new Tuple<String, Int16, Single, Single, Single, Single>(fixelItem._id, segment, xPos, width, notchStart, notchSpacing));
                                }
                                else
                                {
                                    String fixelName = GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel);
                                    CreateErrorEventLogEntry(String.Format(Language.DuplicateFixelIds, fixelItem._id), String.Format(Language.DuplicateFixelIds_Description, fixelItem._id, fixelName), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, DalCacheItemType.Fixel);
                                }
                                break;
                            case (Byte)SpacemanComponentType.TextBox:
                                fixelDtos.Add(DecodePlanogramAnnotationDto(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], availableFontDetails));
                                break;
                            default:
                                if ((componentType == (Byte)SpacemanComponentType.Bay && segment > 0) || componentType != (Byte)SpacemanComponentType.Bay)
                                {
                                    Dictionary<PlanogramFixtureType, object> fixelLookup = new Dictionary<PlanogramFixtureType, object>();

                                    if (!componentSegmentLookup.ContainsKey(fixelItem._id)) // dont add duplicate fixel ids
                                    {
                                        if (segment > 0)
                                        {
                                            if (!segments.Contains(segment)) { segments.Add(segment); }
                                        }

                                        if (componentType == (Byte)SpacemanComponentType.Bay) // Bay
                                        {
                                            fixelDtos.Add(DecodePlanogramFixtureItemDto(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel]));
                                            fixelLookup.Add(PlanogramFixtureType.FixtureItem, fixelDtos[fixelDtos.Count - 1]);
                                            bayPositions.Add((PlanogramFixtureItemDto)fixelDtos[fixelDtos.Count - 1]);
                                            fixelDtos.Add(DecodePlanogramFixtureDto(fixelData, sectionData, this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section], fixtureFieldMappings));
                                            fixelLookup.Add(PlanogramFixtureType.Fixture, fixelDtos[fixelDtos.Count - 1]);
                                            bays.Add((PlanogramFixtureDto)fixelDtos[fixelDtos.Count - 1]);
                                        }

                                        fixelDtos.Add(DecodePlanogramFixtureComponentDto(fixelData, componentType, this._schemaDefinitions[DalCacheItemType.Fixel]));
                                        fixelLookup.Add(PlanogramFixtureType.FixtureComponent, fixelDtos[fixelDtos.Count - 1]);
                                        bayComponents.Add((PlanogramFixtureComponentDto)fixelDtos[fixelDtos.Count - 1]);
                                        fixelDtos.Add(DecodePlanogramComponentDto(fixelData, sectionData, this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section], componentFieldMappings));
                                        fixelLookup.Add(PlanogramFixtureType.Component, fixelDtos[fixelDtos.Count - 1]);
                                        fixelDtos.Add(DecodePlanogramSubComponentDto(fixelData, sectionData, this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section]));
                                        fixelLookup.Add(PlanogramFixtureType.SubComponent, fixelDtos[fixelDtos.Count - 1]);

                                        fixelLookups.Add(GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel), fixelLookup);

                                        if ((componentType == (Byte)SpacemanComponentType.Bay && segment > 0) && hasBase)
                                        {
                                            fixelDtos.Add(CreateBasePlanogramFixtureComponentDto(fixelData, sectionData, segment.ToString(), this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section]));
                                            fixelDtos.Add(CreateBasePlanogramComponentDto(fixelData, sectionData, segment.ToString(), this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section]));
                                            fixelDtos.Add(CreateBasePlanogramSubComponentDto(fixelData, sectionData, segment.ToString(), this._schemaDefinitions[DalCacheItemType.Fixel], this._schemaDefinitions[DalCacheItemType.Section]));
                                        }

                                        componentSegmentLookup.Add(GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel), GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel));
                                    }
                                    else
                                    {
                                        String fixelName = GetValue(fixelData, this._schemaDefinitions[DalCacheItemType.Fixel], SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel);
                                        CreateErrorEventLogEntry(String.Format(Language.DuplicateFixelIds, fixelItem._id), String.Format(Language.DuplicateFixelIds_Description, fixelItem._id, fixelName), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, DalCacheItemType.Fixel);
                                    }
                                }
                                break;
                        }

                        fixelItem.Dto = fixelDtos;
                    }

                    // The segments that are to be created may not have been of type segment. A peg can be used as a type of segment also. Here we capture these instances
                    ValidateSegmentsCreated(segments, bays, bayComponents, fixelLookups, bayPositions, sectionData, this._schemaDefinitions[DalCacheItemType.Section], hasBase);

                    // Missing identifiers are created when the entire plan is loaded so we can ensure a unique identity.
                    CreateMissingFixelIdentifiers(fixelLookups);

                    // Applies the rotations relative to the segment they belong
                    ApplyRelativeSegmentRotation(bayPositions, bayComponents);

                    // Apply the fixture frame detail to created bay data
                    ApplyFixtureFrameDetails(frameDetails);

                    List<String> productGtins = new List<String>();
                    List<String> duplicateGtins = new List<String>();

                    if (this.Children.ContainsKey(DalCacheItemType.Product))
                    {
                        foreach (var productItem in this.Children[DalCacheItemType.Product])
                        {
                            productItem.Dto = DecodePlanogramProductDto(id, productItem.Data, this._schemaDefinitions[DalCacheItemType.Product], productLookup, productFieldMappings, productGtins, duplicateGtins);
                        }

                        if (duplicateGtins.Any())
                        {
                            FailPlanWithDuplicateGtinException(duplicateGtins);
                        }

                        if (this.Children.ContainsKey(DalCacheItemType.Position))
                        {
                            elementPositionLookupDetail = CreateElementPositionLookups(this.Children[DalCacheItemType.Position], this._schemaDefinitions[DalCacheItemType.Position], productLookup);
                        }
                    }

                    // calculate missing merchandising heights, required for calculating cap styles
                    subComponentMerchandisingArea = CalculateShelfMerchandisingArea(bayPositions, bayComponents, fixelLookups, elementPositionLookupDetail);

                    // calculate relative x positions (now all fixel data has been loaded we can update fixture component x positions relative to there fixture items)
                    CalculateFixtureComponentXAndZpositions(bayPositions);
                }

                CreatePlanogramPerformanceMetrics();

                List<PlanogramPositionDto> positions = new List<PlanogramPositionDto>();
                if (this.Children.ContainsKey(DalCacheItemType.Product))
                {
                    foreach (var productItem in this.Children[DalCacheItemType.Product])
                    {
                        DecodePlanogramPerformanceDto((PlanogramProductDto)productItem.Dto, productItem.Data, this._schemaDefinitions[DalCacheItemType.Product]);
                    }

                    if (this.Children.ContainsKey(DalCacheItemType.Position))
                    {
                        foreach (var positionItem in this.Children[DalCacheItemType.Position])
                        {
                            positionItem.Dto = DecodePlanogramPositionDto(positionItem.Data, this._schemaDefinitions[DalCacheItemType.Position], componentSegmentLookup, fixelLookups, bayPositions, productLookup, subComponentMerchandisingArea, pegDataList);
                            positions.AddRange((List<PlanogramPositionDto>)positionItem.Dto);
                        }
                    }
                }

                CalculatePositionSequence(fixelLookups, positions);
            }
        }
                                                                
        #endregion

        #region Encoding/Decoding
        
        #region Package
        /// <summary>
        /// Decodes a package dto
        /// </summary>
        private object DecodePackageDto(String id, String data, String[] sectionData, Dictionary<String, SpacemanSchemaItem> schema, Dictionary<String, SpacemanSchemaItem> sectionSchema)
        {
            String[] fields = GetData(data).ToArray();

            String fileVersion = GetValue(fields, schema, SpacemanFieldHelper.StoreFileVersion, DalCacheItemType.Store);

            //  Check the version is the right one...
            if (!SpacemanFieldHelper.SupportedVersions.Any(version => String.Equals(version, fileVersion)))
                CreateErrorEventLogEntry(String.Format(Message.Import_VersionMismatch_Description, fileVersion),
                                 String.Format(Message.Import_VersionMismatch_Content, SpacemanFieldHelper.SupportedVersions.Last()),
                                 PlanogramEventLogEntryType.Information, PlanogramEventLogEventType.Import, DalCacheItemType.Store);

            String name = GetValue(fields, schema, SpacemanFieldHelper.StoreName, DalCacheItemType.Store);
            if (name == null || name.Length == 0)
            {
                name = GetValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionPlanName, DalCacheItemType.Section);
            }
            if (name == null || name.Length == 0)
            {
                name = Language.DefaultPlanName;
            }
            
            return new PackageDto()
            {
                Id = id,
                Name = name
            };
        }

        /// <summary>
        /// Encodes a package dto
        /// </summary>
        private static String EncodePackageDto(PackageDto dto)
        {
            return null;
        }
        #endregion

        #region Planogram
        /// <summary>
        /// Decodes a planogram dto
        /// </summary>
        private PlanogramDto DecodePlanogramDto(String id, String[] sectionData, Dictionary<String, SpacemanSchemaItem> schema, IEnumerable<PlanogramFieldMappingDto> fieldMappings, ref Boolean hasBase)
        {
            Single baseDepth = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section);
            Single baseWidth = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionBaseWidth, DalCacheItemType.Section);
            Single baseHeight = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);

            if (baseDepth > 0 && baseWidth > 0 && baseHeight > 0)
            {
                hasBase = true;
            }

            PlanogramDto planogramDto = new PlanogramDto()
            {
                Id = 1,
                PackageId = id,
                UniqueContentReference = Guid.NewGuid().ToString(),
                Height = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionHeight, DalCacheItemType.Section),
                Width = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionWidth, DalCacheItemType.Section),
                Depth = GetSingleValue(sectionData, schema, SpacemanFieldHelper.SectionDepth, DalCacheItemType.Section),
                ProductPlacementX = (Byte)PlanogramProductPlacementXType.Manual,
                ProductPlacementY = (Byte)PlanogramProductPlacementYType.Manual,
                ProductPlacementZ = (Byte)PlanogramProductPlacementZType.Manual,
                SourcePath = id
            };

            MapCustomAttributes(typeof(PlanogramDto), planogramDto, fieldMappings, sectionData, schema, CustomAttributeDataPlanogramParentType.Planogram, planogramDto.Id);

            if (String.IsNullOrEmpty(planogramDto.Name))
            {
                planogramDto.Name = Path.GetFileNameWithoutExtension(id);
            }

            return planogramDto;
        }
        
        /// <summary>
        /// Encodes a planogram dto
        /// </summary>
        private static String EncodePlanogramDto(PlanogramDto dto)
        {
            return null;
        }
        #endregion

        #region Available Fonts

        private PlanogramAnnotationDto DecodeAvailableFonts(String data, Dictionary<String, SpacemanSchemaItem> schema)
        {
            String[] fontData = GetData(data).ToArray();

            return new PlanogramAnnotationDto()
            {
                Id = GetValue(fontData, schema, SpacemanFieldHelper.FontCollectionID, DalCacheItemType.Font),
                FontSize = GetSingleValue(fontData, schema, SpacemanFieldHelper.FontSize, DalCacheItemType.Font),
                FontName = GetValue(fontData, schema, SpacemanFieldHelper.FontName, DalCacheItemType.Font),
                FontColour = ConvertColour((Int32)GetSingleValue(fontData, schema, SpacemanFieldHelper.FontColour, DalCacheItemType.Font))
            };
        }
                
        #endregion

        #region Available Pegs

        private PegData DecodeAvailablePegs(String data, Dictionary<String, SpacemanSchemaItem> schema)
        {
            String[] pegData = GetData(data).ToArray();

            return new PegData()
            {
                Id = GetValue(pegData, schema, SpacemanFieldHelper.PegID, DalCacheItemType.Peg),
                Source = GetValue(pegData, schema, SpacemanFieldHelper.PegSource, DalCacheItemType.Peg),
                Type = GetByteValue(pegData, schema, SpacemanFieldHelper.PegType, DalCacheItemType.Peg),
                Span = GetByteValue(pegData, schema, SpacemanFieldHelper.PegSpan, DalCacheItemType.Peg),
                Length = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegLength, DalCacheItemType.Peg),
                Rise = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegRise, DalCacheItemType.Peg),
                Headroom = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegHeadroom, DalCacheItemType.Peg),
                Activity = GetByteValue(pegData, schema, SpacemanFieldHelper.PegActivity, DalCacheItemType.Peg),
                PegTagWidth = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegTagWidth, DalCacheItemType.Peg),
                PegTagHeight = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegTagHeight, DalCacheItemType.Peg),
                PegTagLocRight = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegTagLocRight, DalCacheItemType.Peg),
                PegTagLocDown = GetSingleValue(pegData, schema, SpacemanFieldHelper.PegTagLocDown, DalCacheItemType.Peg)
            };
        }

        #endregion

        #region PlanogramFixtureItem
        /// <summary>
        /// Decodes a planogram Fixture Item dto
        /// </summary>
        private PlanogramFixtureItemDto DecodePlanogramFixtureItemDto(String[] fixelData, Dictionary<String, SpacemanSchemaItem> schema)
        {
            return new PlanogramFixtureItemDto()
            {
                Id = GetValue(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel),
                PlanogramId = 1,
                PlanogramFixtureId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel),
                X = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelX, DalCacheItemType.Fixel),
                Y = 0, //GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelY, DalCacheItemType.Fixel),
                Z = 0, // GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelZ, DalCacheItemType.Fixel)
                Angle = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRotation, DalCacheItemType.Fixel),
                Slope = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelSlope, DalCacheItemType.Fixel),
                BaySequenceNumber = GetInt16Value(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel)
            };
        }

        /// <summary>
        /// Encodes a planogram dto
        /// </summary>
        private static String EncodePlanogramFixtureItemDto(PlanogramDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramFixture
        /// <summary>
        /// Decodes a planogram Fixture dto
        /// </summary>
        private PlanogramFixtureDto DecodePlanogramFixtureDto(String[] fixelData, String[] sectionData, Dictionary<String, SpacemanSchemaItem> schema, Dictionary<String, SpacemanSchemaItem> sectionSchema, IEnumerable<PlanogramFieldMappingDto> fieldMappings)
        {
            Single depth = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepth, DalCacheItemType.Fixel);
            Single baseDepth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section);

            depth = baseDepth > depth ? baseDepth : depth;

            PlanogramFixtureDto planogramFixtureDto = new PlanogramFixtureDto()
            {
                Id = GetValue(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel),
                PlanogramId = 1,
                Name = GetValue(fixelData, schema, SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel),
                Height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel) + GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                Width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel),
                Depth = depth
            };

            MapCustomAttributes(typeof(PlanogramFixtureDto), planogramFixtureDto, fieldMappings, fixelData, schema, CustomAttributeDataPlanogramParentType.PlanogramComponent, planogramFixtureDto.Id);

            return planogramFixtureDto;
        }

        /// <summary>
        /// Encodes a planogram fixture dto
        /// </summary>
        private static String EncodePlanogramFixtureDto(PlanogramFixtureDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramFixtureComponent
        /// <summary>
        /// Decodes a planogram fixture component dto
        /// </summary>
        private PlanogramFixtureComponentDto DecodePlanogramFixtureComponentDto(String[] fixelData, Byte componentType, Dictionary<String, SpacemanSchemaItem> schema)
        {
            Single zPos = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelZ, DalCacheItemType.Fixel);
            Single yPos = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelY, DalCacheItemType.Fixel);
            Single slope = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelSlope, DalCacheItemType.Fixel);
            Single angle = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRotation, DalCacheItemType.Fixel);
            String segment = GetValue(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel);
            if (segment == "-1") { segment = "1"; }

            switch (componentType)
            {
                case (Byte)SpacemanComponentType.Bay:
                    yPos = 0;
                    break;
                default:
                    break;
            }
            
            return new PlanogramFixtureComponentDto()
            {
                Id = GetValue(fixelData, schema, SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel),
                PlanogramComponentId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel),
                PlanogramFixtureId = segment,
                X = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelX, DalCacheItemType.Fixel),
                Y = yPos,
                Z = zPos,
                Slope = slope != 0 ? ConvertToRadians(slope) : 0,
                Angle = angle,
                Roll = 0
            };
        }

        private PlanogramFixtureComponentDto CreateBasePlanogramFixtureComponentDto(String[] fixelData, String[] sectionData, String segment, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String,SpacemanSchemaItem> sectionSchema)
        {
            return new PlanogramFixtureComponentDto()
            {
                Id = null,
                PlanogramComponentId = null,
                PlanogramFixtureId = segment,
                X = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelX, DalCacheItemType.Fixel),
                Y = 0,
                Z = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelZ, DalCacheItemType.Fixel) + GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepth, DalCacheItemType.Fixel),
                Slope = 0,
                Angle = 0,
                Roll = 0
            };
        }

        /// <summary>
        /// Encodes a planogram fixture component dto
        /// </summary>
        private static String EncodePlanogramFixtureComponentDto(PlanogramFixtureComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramComponent
        /// <summary>
        /// Decodes a planogram component dto
        /// </summary>
        private PlanogramComponentDto DecodePlanogramComponentDto(String[] fixelData, String[] sectionData, Dictionary<String, SpacemanSchemaItem> schema, Dictionary<String, SpacemanSchemaItem> sectionSchema, IEnumerable<PlanogramFieldMappingDto> fieldMappings)
        {
            String fixelId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel);
            Byte componentType = GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelType, DalCacheItemType.Fixel);
            Single height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
            Single width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel);
            Single depth = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepth, DalCacheItemType.Fixel);
            String name = GetValue(fixelData, schema, SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel).Trim();
            
            switch (componentType)
            {
                case (Byte)SpacemanComponentType.Bay:
                    height += GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
                    break;
                default:
                    break;
            }

            PlanogramComponentDto planogramComponentDto = new PlanogramComponentDto()
            {
                Id = fixelId,
                PlanogramId = 1,
                Name = name,
                Height = height,
                Width = width,
                Depth = depth,
                ComponentType = ConvertComponentType(componentType),
                IsMerchandisedTopDown = GetIsMerchandisedTopDown(componentType) 
            };

            MapCustomAttributes(typeof(PlanogramComponentDto), planogramComponentDto, fieldMappings, fixelData, schema, CustomAttributeDataPlanogramParentType.PlanogramComponent, planogramComponentDto.Id);

            planogramComponentDto.Name = planogramComponentDto.Name.Trim();

            if (planogramComponentDto.Name.Length == 0)
            {
                planogramComponentDto.Name = fixelId;
            }

            return planogramComponentDto;
        }
        
        private PlanogramComponentDto CreateBasePlanogramComponentDto(String[] fixelData, String[] sectionData, String segment, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String,SpacemanSchemaItem> sectionSchema)
        {
            Single height = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
            Single width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel);
            Single depth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section);

            return new PlanogramComponentDto()
            {
                Id = null,
                PlanogramId = 1,
                Name = GetValue(fixelData, schema, SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel).Trim() + segment,
                Height = height,
                Width = width,
                Depth = depth,
                ComponentType = (Byte)PlanogramComponentType.Base,
            };
        }
        
        private static byte ConvertComponentType(Byte componentType)
        {
            switch (componentType)
            {
                case (Byte)SpacemanComponentType.Shelf:
                    return (Byte)PlanogramComponentType.Shelf;
                case (Byte)SpacemanComponentType.Peg:
                    return (Byte)PlanogramComponentType.Peg;
                case (Byte)SpacemanComponentType.Chest:
                    return (Byte)PlanogramComponentType.Chest;
                case (Byte)SpacemanComponentType.Bar:
                    return (Byte)PlanogramComponentType.Bar;
                case (Byte)SpacemanComponentType.Pallet:
                    return (Byte)PlanogramComponentType.Pallet;
                case (Byte)SpacemanComponentType.Bay:
                    return (Byte)PlanogramComponentType.Backboard;
                case (Byte)SpacemanComponentType.Rod:
                    return (Byte)PlanogramComponentType.Rod;
                default:
                    return 0;
            }
        }

        private static Boolean GetIsMerchandisedTopDown(Byte componentType)
        {
            return (componentType == (Byte)SpacemanComponentType.Chest);
        }

        /// <summary>
        /// Encodes a planogram fixture component dto
        /// </summary>
        private static String EncodePlanogramComponentDto(PlanogramComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramSubComponent
        /// <summary>
        /// Decodes a planogram sub component dto
        /// </summary>
        private PlanogramSubComponentDto DecodePlanogramSubComponentDto(String[] fixelData, String[] sectionData, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String,SpacemanSchemaItem> sectionSchema)
        {
            const Int32 defaultShelfFillColour = -16777216;
            const Int32 defaultPegFillColour = -3308225;
            const Int32 defaultChestFillColour = -10185235;
            const Int32 defaultBarFillColour = -11179217;
            const Int32 defaultPalletFillColour = -7650029;
            const Int32 defaultBayFillColour = -2894893;
            const Int32 defaultBasketFillColour = -3308225;
            const Int32 defaultRodFillColour = -5171;
            const Int32 defaultBayLineColour = -16777216;

            String fixelId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel);
            Byte componentType = GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelType, DalCacheItemType.Fixel);
            String name = GetValue(fixelData, schema, SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel).Trim();

            if (name.Length == 0)
            {
                name = fixelId;
            }

            Single height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
            Single width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel);
            Single depth = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepth, DalCacheItemType.Fixel);
            Single merchandisableHeight = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelMaxMerch, DalCacheItemType.Fixel);
            Byte fillPattern = GetComponentFillPatternType(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelFillPattern, DalCacheItemType.Fixel));            
            Int16 colourIsClear = GetInt16Value(fixelData, schema, SpacemanFieldHelper.FixelColourIsClear, DalCacheItemType.Fixel);
            Int32 fillColour = -1;

            if (colourIsClear >= 0)
            {
                fillColour = GetInt32Value(fixelData, schema, SpacemanFieldHelper.FixelColour, DalCacheItemType.Fixel);
                fillColour = ConvertColour(fillColour);
            }

            Int32 lineColour = -16777216;
            Single lineThickness = 0.5F;
            Single riserHeight = 0;
            Single riserThickness = 0;
            Int32 riserTransparencyPercent = 0;
            Boolean IsRiserPlacedOnFront = false;            
            Single notchStartX = 0;
            Single notchSpacingX = 0;
            Single notchStartY = 0;
            Single notchSpacingY = 0;
            Single notchHeight = 0;
            Single notchWidth = 0;
            Boolean isNotchPlacedOnFront = false;
            Byte notchStyleType = 0;
            Single merchConstraintRow1StartX = 0;
            Single merchConstraintRow1SpacingX = 0;
            Single merchConstraintRow1StartY = 0;
            Single merchConstraintRow1SpacingY = 0;
            Single merchConstraintRow1Height = 0; 
            Single merchConstraintRow1Width = 0; 
            Byte merchandisingType = GetMerchandisingType(componentType);
            Byte combineType = 0;
            Boolean isProductOverlapAllowed = true;
            Byte merchandisingStrategyX = 0;
            Byte merchandisingStrategyY = 0;
            Byte merchandisingStrategyZ = 0;
            Single leftOverhang = 0;
            Single rightOverhang = 0;
            Single frontOverhang = 0;
            Single backOverhang = 0;
            Single topOverhang = 0;
            Single bottomOverhang = 0;
            Single faceThicknessFront = 0;
            Single faceThicknessBack=0;
            Single faceThicknessBottom = 0;
            Single faceThicknessLeft = 0;
            Single faceThicknessRight = 0;
            Single faceThicknessTop = 0;
            Single dividerObstructionWidth = 0;
            Single dividerObstructionHeight = 0;
            Single dividerObstructionDepth = 0;
            Single dividerObstructionStartX = 0;
            Single dividerObstructionStartY = 0;
            Single dividerObstructionStartZ = 0;
            Single dividerObstructionSpacingX= 0;
            Single dividerObstructionSpacingY = 0;
            Single dividerObstructionSpacingZ = 0;
            Boolean isDividerObstructionByFacing = false;
            Boolean isProductSqueezeAllowed = false;

            switch (componentType)
            {
                case (Byte)SpacemanComponentType.Shelf:
                    if (fillColour == -1) fillColour = defaultShelfFillColour;
                    riserHeight = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelGrillHeight, DalCacheItemType.Fixel);
                    riserThickness = 1;
                    riserTransparencyPercent = 50;
                    if (riserHeight > 0) IsRiserPlacedOnFront = true;
                    leftOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelLeftOverhang, DalCacheItemType.Fixel);
                    rightOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRightOverhang, DalCacheItemType.Fixel);
                    dividerObstructionStartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotStart, DalCacheItemType.Fixel);
                    dividerObstructionSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotSpacing, DalCacheItemType.Fixel);
                    if (dividerObstructionStartX <= 0 && dividerObstructionSpacingX <= 0)
                    {
                        dividerObstructionSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSpacing, DalCacheItemType.Fixel);
                    }
                    dividerObstructionWidth = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelSpacerThick, DalCacheItemType.Fixel);
                    if (dividerObstructionWidth > 0)
                    {
                        dividerObstructionDepth = depth;
                        dividerObstructionHeight = 2;
                    }
                    if (GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizGap, DalCacheItemType.Fixel) > 0)
                    {
                        isDividerObstructionByFacing = true;
                    }
                    merchandisingStrategyX = GetMerchandisingStrategyX(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel));
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Front;
                    combineType = GetCombineType(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelCombine, DalCacheItemType.Fixel));
                    isProductSqueezeAllowed = Convert.ToBoolean(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelAutoCrush, DalCacheItemType.Fixel));
                    break;

                case (Byte)SpacemanComponentType.Peg:
                    if (fillColour == -1) fillColour = defaultPegFillColour;
                    notchStartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizStart, DalCacheItemType.Fixel);
                    notchSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSpacing, DalCacheItemType.Fixel);
                    notchStartY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertStart, DalCacheItemType.Fixel);
                    notchSpacingY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertSpacing, DalCacheItemType.Fixel);
                    merchConstraintRow1StartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizStart, DalCacheItemType.Fixel);
                    merchConstraintRow1SpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSpacing, DalCacheItemType.Fixel);
                    merchConstraintRow1StartY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertStart, DalCacheItemType.Fixel);
                    merchConstraintRow1SpacingY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertSpacing, DalCacheItemType.Fixel);
                    leftOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelLeftOverhang, DalCacheItemType.Fixel);
                    rightOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRightOverhang, DalCacheItemType.Fixel);
                    topOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelUpperOverhang, DalCacheItemType.Fixel);
                    bottomOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelLowerOverhang, DalCacheItemType.Fixel);
                    merchConstraintRow1Height = 0.2F; // defaulted for peg
                    merchConstraintRow1Width = 0.2F; // defaulted for peg
                    notchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Back;
                    isProductSqueezeAllowed = Convert.ToBoolean(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelAutoCrush, DalCacheItemType.Fixel));
                    break;

                case (Byte)SpacemanComponentType.Chest:
                    if (fillColour == -1) fillColour = defaultChestFillColour;
                    height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelGrillHeight, DalCacheItemType.Fixel) + GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
                    faceThicknessBottom = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
                    faceThicknessFront = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelThickness, DalCacheItemType.Fixel);
                    faceThicknessLeft = faceThicknessFront; 
                    faceThicknessRight = faceThicknessFront; 
                    faceThicknessBack = faceThicknessFront; 
                    dividerObstructionStartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotStart, DalCacheItemType.Fixel);
                    dividerObstructionStartZ = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepthSlotStart, DalCacheItemType.Fixel);
                    dividerObstructionSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotSpacing, DalCacheItemType.Fixel);
                    dividerObstructionSpacingZ = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepthSlotSpacing, DalCacheItemType.Fixel);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;

                case (Byte)SpacemanComponentType.Bar:
                    if (fillColour == -1) fillColour = defaultBarFillColour;
                    leftOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelLeftOverhang, DalCacheItemType.Fixel);
                    rightOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRightOverhang, DalCacheItemType.Fixel);
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Even;
                    merchandisingStrategyX = GetMerchandisingStrategyX(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel));
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Back;
                    combineType = GetCombineType(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelCombine, DalCacheItemType.Fixel));
                    isProductSqueezeAllowed = Convert.ToBoolean(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelAutoCrush, DalCacheItemType.Fixel));

                    break;

                case (Byte)SpacemanComponentType.Pallet:
                    if (fillColour == -1) fillColour = defaultPalletFillColour;
                    leftOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelLeftOverhang, DalCacheItemType.Fixel);
                    rightOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelRightOverhang, DalCacheItemType.Fixel);
                    frontOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelFrontOverhang, DalCacheItemType.Fixel);
                    backOverhang = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelBackOverhang, DalCacheItemType.Fixel);
                    dividerObstructionStartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotStart, DalCacheItemType.Fixel);
                    dividerObstructionStartZ = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepthSlotStart, DalCacheItemType.Fixel);
                    dividerObstructionSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSlotSpacing, DalCacheItemType.Fixel);
                    dividerObstructionSpacingZ = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepthSlotSpacing, DalCacheItemType.Fixel);
                    if (GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizGap, DalCacheItemType.Fixel) > 0)
                    {
                        isDividerObstructionByFacing = true;
                    }
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Front;
                    break;
                                    
                case (Byte)SpacemanComponentType.Bay:
                    merchandisableHeight = 0;
                    height += GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
                    fillColour = defaultBayFillColour;
                    fillPattern = (Byte)PlanogramSubComponentFillPatternType.Solid;
                    lineColour = defaultBayLineColour;
                    lineThickness = 0.3F;
                    notchStartX = 0;
                    notchSpacingX = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchSpacing, DalCacheItemType.Section);
                    notchStartY = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchStart, DalCacheItemType.Section) + GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
                    notchSpacingY = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchSpacing, DalCacheItemType.Section);
                    notchHeight = 0.1F;
                    notchWidth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchWidth, DalCacheItemType.Section);
                    if (notchWidth > 0) { isNotchPlacedOnFront = true; }
                    notchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle;
                    isProductOverlapAllowed = false;
                    break;

                case (Byte)SpacemanComponentType.Basket:
                    if (fillColour == -1) fillColour = defaultBasketFillColour;
                    height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelGrillHeight, DalCacheItemType.Fixel) + GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
                    faceThicknessBottom = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel);
                    faceThicknessFront = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelThickness, DalCacheItemType.Fixel);
                    faceThicknessBack = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelThickness, DalCacheItemType.Fixel);
                    faceThicknessRight = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelThickness, DalCacheItemType.Fixel);
                    faceThicknessLeft = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelThickness, DalCacheItemType.Fixel);
                    GetBasketMerchandisingStrategy(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel), ref merchandisingStrategyX, ref merchandisingStrategyY, ref merchandisingStrategyZ);
                    break;

                case (Byte)SpacemanComponentType.Rod:
                    if (fillColour == -1) fillColour = defaultRodFillColour;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Top;
                    merchandisingStrategyZ = GetMerchandisingStrategyZ(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelSpreadProducts, DalCacheItemType.Fixel));
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Even;
                    break;

                default:
                    riserHeight = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelGrillHeight, DalCacheItemType.Fixel);
                    riserThickness = 1;
                    if (riserHeight > 0) IsRiserPlacedOnFront = true;
                    notchStartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizStart, DalCacheItemType.Fixel);
                    notchSpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSpacing, DalCacheItemType.Fixel);
                    notchStartY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertStart, DalCacheItemType.Fixel);
                    notchSpacingY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertSpacing, DalCacheItemType.Fixel);
                    merchConstraintRow1StartX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizStart, DalCacheItemType.Fixel);
                    merchConstraintRow1SpacingX = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHorizSpacing, DalCacheItemType.Fixel);
                    merchConstraintRow1StartY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertStart, DalCacheItemType.Fixel);
                    merchConstraintRow1SpacingY = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelVertSpacing, DalCacheItemType.Fixel);
                    notchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle;
                    break;
            }

            return new PlanogramSubComponentDto()
            {
                Id = fixelId,
                PlanogramComponentId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelId, DalCacheItemType.Fixel),
                Name = name,
                Height = height,
                Width = width,
                Depth = depth,
                X = 0,
                Y = 0,
                Z = 0,
                Slope = 0,
                Angle = 0,
                Roll = 0,
                ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                MerchandisableHeight = merchandisableHeight,
                IsVisible = true,
                HasCollisionDetection = true,
                FillPatternTypeFront = fillPattern,
                FillPatternTypeBack = fillPattern,
                FillPatternTypeTop = fillPattern,
                FillPatternTypeBottom = fillPattern,
                FillPatternTypeLeft = fillPattern,
                FillPatternTypeRight = fillPattern,
                FillColourFront = fillColour,
                FillColourBack = fillColour,
                FillColourTop = fillColour,
                FillColourBottom = fillColour,
                FillColourLeft = fillColour,
                FillColourRight = fillColour,
                LineColour = lineColour,
                LineThickness = lineThickness,
                RiserHeight = riserHeight,
                RiserThickness = riserThickness,
                RiserTransparencyPercent = riserTransparencyPercent,
                IsRiserPlacedOnFront = IsRiserPlacedOnFront,
                NotchStartX = notchStartX,
                NotchSpacingX = notchSpacingX,
                NotchStartY = notchStartY,
                NotchSpacingY = notchSpacingY,
                NotchHeight = notchHeight,
                NotchWidth = notchWidth,
                IsNotchPlacedOnFront = isNotchPlacedOnFront,
                NotchStyleType = notchStyleType,
                MerchConstraintRow1StartX = merchConstraintRow1StartX,
                MerchConstraintRow1SpacingX = merchConstraintRow1SpacingX,
                MerchConstraintRow1StartY = merchConstraintRow1StartY,
                MerchConstraintRow1SpacingY = merchConstraintRow1SpacingY,
                MerchConstraintRow1Height = merchConstraintRow1Height,
                MerchConstraintRow1Width = merchConstraintRow1Width,
                MerchandisingType = merchandisingType,
                CombineType = combineType,
                IsProductOverlapAllowed = isProductOverlapAllowed,
                MerchandisingStrategyX = merchandisingStrategyX,
                MerchandisingStrategyY = merchandisingStrategyY,
                MerchandisingStrategyZ = merchandisingStrategyZ,
                LeftOverhang = leftOverhang,
                RightOverhang = rightOverhang,
                FrontOverhang = frontOverhang,
                BackOverhang = backOverhang,
                TopOverhang = topOverhang,
                BottomOverhang = bottomOverhang,
                FaceThicknessFront = faceThicknessFront,
                FaceThicknessBack = faceThicknessBack,
                FaceThicknessBottom = faceThicknessBottom,
                FaceThicknessLeft = faceThicknessLeft,
                FaceThicknessRight = faceThicknessRight,
                FaceThicknessTop = faceThicknessTop,
                DividerObstructionWidth = dividerObstructionWidth,
                DividerObstructionHeight = dividerObstructionHeight,
                DividerObstructionDepth = dividerObstructionDepth,
                DividerObstructionStartX = dividerObstructionStartX,
                DividerObstructionStartY = dividerObstructionStartY,
                DividerObstructionStartZ = dividerObstructionStartZ,
                DividerObstructionSpacingX = dividerObstructionSpacingX,
                DividerObstructionSpacingY = dividerObstructionSpacingY,
                DividerObstructionSpacingZ = dividerObstructionSpacingZ,
                IsDividerObstructionByFacing = isDividerObstructionByFacing,
                DividerObstructionFillColour = -65536, //red
                DividerObstructionFillPattern = 0,
                IsProductSqueezeAllowed = isProductSqueezeAllowed
            };
        }

        private static void GetBasketMerchandisingStrategy(Byte merchandisingStrategy, ref Byte merchandisingStrategyX, ref Byte merchandisingStrategyY, ref Byte merchandisingStrategyZ)
        {
            switch (merchandisingStrategy)
            {
                case 0: // left to right
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.LeftStacked;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;
                case 5: // front to back
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.Bottom;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.FrontStacked;
                    break;
                case 7: // Bottom to top
                    merchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                    merchandisingStrategyY = (Byte)PlanogramSubComponentYMerchStrategyType.BottomStacked;
                    merchandisingStrategyZ = (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
                    break;
                default:
                    break;
            }
        }

        private static Byte GetCombineType(Byte combineType)
        {
            switch (combineType)
            {
                case 0:
                    return (Byte)PlanogramSubComponentCombineType.None;
                case 1:
                    return (Byte)PlanogramSubComponentCombineType.Both;
                case 2:
                    return (Byte)PlanogramSubComponentCombineType.LeftOnly;
                case 3:
                    return (Byte)PlanogramSubComponentCombineType.RightOnly;
                default:
                    return (Byte)PlanogramSubComponentCombineType.None;
            }
        }

        private static Byte GetMerchandisingStrategyX(Byte merchandisingStrategyX)
        {
            switch (merchandisingStrategyX)
            {
                case 0: // Left
                    return (Byte)PlanogramSubComponentXMerchStrategyType.LeftStacked;
                case 1: // Right
                    return (Byte)PlanogramSubComponentXMerchStrategyType.RightStacked;
                case 3: // Even
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Even;
                case 4: // Facings
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Even;
                default:
                    return (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
            }
        }

        private static Byte GetMerchandisingStrategyZ(Byte merchandisingStrategyZ)
        {
            switch (merchandisingStrategyZ)
            {
                case 3: // Even
                    return (Byte)PlanogramSubComponentZMerchStrategyType.Even;
                case 4: // Facings
                    return (Byte)PlanogramSubComponentZMerchStrategyType.Even;
                case 5: // Front
                    return (Byte)PlanogramSubComponentZMerchStrategyType.FrontStacked;
                case 6: // Back
                    return (Byte)PlanogramSubComponentZMerchStrategyType.BackStacked;
                default:
                    return (Byte)PlanogramSubComponentZMerchStrategyType.Manual;
            }
        }

        private static Byte GetComponentFillPatternType(Byte fillPatternType)
        {
            switch (fillPatternType)
            {
                case 0:
                    return (Byte)PlanogramSubComponentFillPatternType.Solid;
                case 1:
                    return (Byte)PlanogramSubComponentFillPatternType.DiagonalDown;
                case 2:
                    return (Byte)PlanogramSubComponentFillPatternType.DiagonalUp;
                case 3:
                case 6:
                    return (Byte)PlanogramSubComponentFillPatternType.Crosshatch;
                case 4:
                    return (Byte)PlanogramSubComponentFillPatternType.Border;
                case 11:
                    return (Byte)PlanogramSubComponentFillPatternType.Horizontal;
                case 12:
                    return (Byte)PlanogramSubComponentFillPatternType.Vertical;
                default:
                    return (Byte)PlanogramSubComponentFillPatternType.Solid;
            }
        }

        private PlanogramSubComponentDto CreateBasePlanogramSubComponentDto(String[] fixelData, String[] sectionData, String segment, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String,SpacemanSchemaItem> sectionSchema)
        {
            Single height = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section);
            Single width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel);
            Single depth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section);   
            Int32 fillColour = -5658199;
            Int32 lineColour = -16777216;
            Single lineThickness = 0.3F;

            return new PlanogramSubComponentDto()
            {
                Id = null,
                PlanogramComponentId = null,
                Name = GetValue(fixelData, schema, SpacemanFieldHelper.FixelName, DalCacheItemType.Fixel).Trim() + segment,
                Height = height,
                Width = width,
                Depth = depth,
                X = 0,
                Y = 0,
                Z = 0,
                FillColourFront = fillColour,
                FillColourBack = fillColour,
                FillColourTop = fillColour,
                FillColourBottom = fillColour,
                FillColourLeft = fillColour,
                FillColourRight = fillColour,
                LineColour = lineColour,
                LineThickness = lineThickness,
                MerchandisingType = (Byte)PlanogramSubComponentMerchandisingType.None,
                DividerObstructionFillColour = -65536,
            };
        }
        
        private static Byte GetMerchandisingType(Byte componentType)
        {
            switch (componentType)
            {
                case (Byte)SpacemanComponentType.Shelf:
                case (Byte)SpacemanComponentType.Chest:
                case (Byte)SpacemanComponentType.Pallet:
                case (Byte)SpacemanComponentType.Basket:
                    return (Byte)PlanogramSubComponentMerchandisingType.Stack;
                case (Byte)SpacemanComponentType.Peg:
                case (Byte)SpacemanComponentType.Bar:
                    return (Byte)PlanogramSubComponentMerchandisingType.Hang;
                case (Byte)SpacemanComponentType.Rod:
                    return (Byte)PlanogramSubComponentMerchandisingType.HangFromBottom;                
                case (Byte)SpacemanComponentType.Bay:
                    return (Byte)PlanogramSubComponentMerchandisingType.None;
                default:
                    return (Byte)PlanogramSubComponentMerchandisingType.None;
            }
        }
        
        /// <summary>
        /// Encodes a planogram sub component dto
        /// </summary>
        private static String EncodePlanogramSubComponentDto(PlanogramSubComponentDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramProduct
        /// <summary>
        /// Decodes a planogram product dto
        /// </summary>
        private PlanogramProductDto DecodePlanogramProductDto(String id, String data, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String, PlanogramProductDto> productLookup, IEnumerable<PlanogramFieldMappingDto> fieldMappings, List<String> productGtins, List<String> duplicateGtins)
        {
            String[] fields = GetData(data).ToArray();

            Single height = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductHeight, DalCacheItemType.Product);
            Single width = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductWidth, DalCacheItemType.Product);
            Single depth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductDepth, DalCacheItemType.Product);

            Byte trayHigh = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsTrayHigh, DalCacheItemType.Product);
            Byte trayWide = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsTrayWide, DalCacheItemType.Product);
            Byte trayDeep = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsTrayDeep, DalCacheItemType.Product);
            Single trayHeight = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductTrayHeight, DalCacheItemType.Product);
            Single trayWidth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductTrayWidth, DalCacheItemType.Product);
            Single trayDepth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductTrayDepth, DalCacheItemType.Product);
            Single trayThickHeight = 0;
            Single trayThickWidth = 0;
            Single trayThickDepth = 0;
            Boolean createNewProductId = false;

            if (!(trayHigh == 1 && trayWide == 1 && trayDeep == 1))
            {
                if (trayHigh > 0 && trayHeight > 0 && height > 0)
                {
                    trayThickHeight = (height * trayHigh > trayHeight) ? 0 : (trayHeight - (height * trayHigh));
                }
                if (trayWide > 0 && trayWidth > 0 && width > 0)
                {
                    trayThickWidth = (width * trayWide > trayWidth) ? 0 : ((trayWidth - (width * trayWide)) / 2);
                }
                if (trayDeep > 0 && trayDepth > 0 && depth > 0)
                {
                    trayThickDepth = (depth * trayDeep > trayDepth) ? 0 : ((trayDepth - (depth * trayDeep)) / 2);
                }
            }
            
            Single SqueezeHeight = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxVertCrush, DalCacheItemType.Product);
            Single SqueezeWidth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxHorizCrush, DalCacheItemType.Product);
            Single SqueezeDepth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductMaxDepthCrush, DalCacheItemType.Product);

            if (SqueezeHeight > 0)
            {
                SqueezeHeight = (1 - SqueezeHeight);
            }
            else
            {
                SqueezeHeight = 1;
            }
            if (SqueezeWidth > 0)
            {
                SqueezeWidth = (1 - SqueezeWidth);
            }
            else
            {
                SqueezeWidth = 1;
            }
            if (SqueezeDepth > 0)
            {
                SqueezeDepth = (1 - SqueezeDepth);
            }
            else
            {
                SqueezeDepth = 1;
            }

            Single nestingHeight = 0;
            Single nestingWidth = 0;
            Single nestingDepth = 0;

            nestingHeight = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductVertNest, DalCacheItemType.Product);            
            // can be negative in Spaceman changes the way that the nest is rendered (always invert to positive for CCM)
            nestingHeight = (nestingHeight < 0) ? -nestingHeight : nestingHeight; // (positive nest above, negative nest below)

            if (nestingHeight == 0)
            {
                nestingWidth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductHorizNest, DalCacheItemType.Product);
                nestingWidth = (nestingWidth < 0) ? -nestingWidth : nestingWidth;

                if (nestingWidth == 0)
                {
                    nestingDepth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductDepthNest, DalCacheItemType.Product);
                    nestingDepth = (nestingDepth < 0) ? -nestingDepth : nestingDepth;
                }
            }
            
            String productId = GetValue(fields, schema, SpacemanFieldHelper.ProductID, DalCacheItemType.Product);

            Int16 colourIsClear = GetInt16Value(fields, schema, SpacemanFieldHelper.ProductColourIsClear, DalCacheItemType.Product);
            Int32 fillColour = 16777215;

            if (colourIsClear >= 0) { fillColour = ConvertColour(GetInt32Value(fields, schema, SpacemanFieldHelper.ProductColour, DalCacheItemType.Product)); }

            PlanogramProductDto planogramProductDto = new PlanogramProductDto()
            {
                Id = productId,
                PlanogramId = 1,
                Gtin = GetValue(fields, schema, SpacemanFieldHelper.ProductUPC, DalCacheItemType.Product),
                Name = GetValue(fields, schema, SpacemanFieldHelper.ProductName, DalCacheItemType.Product),
                Height = height,
                Width = width,
                Depth = depth,

                DisplayHeight = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductDisplayHeight, DalCacheItemType.Product),
                DisplayWidth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductDisplayWidth, DalCacheItemType.Product),
                DisplayDepth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductDisplayDepth, DalCacheItemType.Product),

                NumberOfPegHoles = 0, // overriden by peg position decode but always defaulted to 1 if on a peg!

                PegX = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg1Right, DalCacheItemType.Product),
                PegX2 = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg2Right, DalCacheItemType.Product),
                PegX3 = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg3Right, DalCacheItemType.Product),
                PegY = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg1Down, DalCacheItemType.Product),
                PegY2 = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg2Down, DalCacheItemType.Product),
                PegY3 = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductPeg3Down, DalCacheItemType.Product),

                PegDepth = 0, // overriden by peg position decode, the actual peg depth is contained within peg library which isnt always contained within Spaceman file

                SqueezeHeight = SqueezeHeight,
                SqueezeWidth = SqueezeWidth,
                SqueezeDepth = SqueezeDepth,

                NestingHeight = nestingHeight,
                NestingWidth = nestingWidth,
                NestingDepth = nestingDepth,
                CasePackUnits = GetInt16Value(fields, schema, SpacemanFieldHelper.ProductUnitsCase, DalCacheItemType.Product),

                CaseHigh = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsCaseHigh, DalCacheItemType.Product),
                CaseWide = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsCaseWide, DalCacheItemType.Product),
                CaseDeep = GetByteValue(fields, schema, SpacemanFieldHelper.ProductUnitsCaseDeep, DalCacheItemType.Product),
                CaseHeight = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductCaseHeight, DalCacheItemType.Product),
                CaseWidth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductCaseWidth, DalCacheItemType.Product),
                CaseDepth = GetSingleValue(fields, schema, SpacemanFieldHelper.ProductCaseDepth, DalCacheItemType.Product),
                MaxStack = GetByteValue(fields, schema, SpacemanFieldHelper.ProductMaxHigh, DalCacheItemType.Product),

                MinDeep = GetByteValue(fields, schema, SpacemanFieldHelper.ProductMinDeep, DalCacheItemType.Product),
                MaxDeep = GetByteValue(fields, schema, SpacemanFieldHelper.ProductMaxDeep, DalCacheItemType.Product),
                TrayPackUnits = GetInt16Value(fields, schema, SpacemanFieldHelper.ProductUnitsTray, DalCacheItemType.Product),
                TrayHigh = trayHigh,
                TrayWide = trayWide,
                TrayDeep = trayDeep,
                TrayHeight = trayHeight,
                TrayWidth = trayWidth,
                TrayDepth = trayDepth,
                TrayThickHeight = trayThickHeight,
                TrayThickWidth = trayThickWidth,
                TrayThickDepth = trayThickDepth,

                StatusType = (Byte)PlanogramProductStatusType.Active,
                OrientationType = GetOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.ProductStdOrient, DalCacheItemType.Product)),
                MerchandisingStyle = GetMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.ProductMerchStyle, DalCacheItemType.Product)),
                IsFrontOnly = GetIsFrontOnly(fields, schema), //seems that its not possible to calculate this from the legal orientations within Spaceman as it seems to break these rules to get top caps on 
              
                CanBreakTrayUp = true,
                CanBreakTrayDown = true,
                CanBreakTrayBack = true,
                CanBreakTrayTop = true,
                StyleNumber = 0,
                ShapeType = (Byte)PlanogramProductShapeType.Box,
                FillPatternType = GetFillPatternType(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFillPattern, DalCacheItemType.Product)),
                FillColour = fillColour,
            };

            MapCustomAttributes(typeof(PlanogramProductDto), planogramProductDto, fieldMappings, fields, schema, CustomAttributeDataPlanogramParentType.PlanogramProduct, planogramProductDto.Id);

            if (!productLookup.ContainsKey(productId))
            {
                productLookup.Add(productId, planogramProductDto);
            }
            else
            {
                createNewProductId = true;
            }

            SpacePlanningImportHelper.ValidatePlanogramProductBusinessRules(_dalCache.PlanogramEventLogDtoList, planogramProductDto, productGtins);
            
            if (createNewProductId) // ensure the spaceman idenfifier is unique
            {
                Int32 uniqueKey = 1;
                String newIdentifier = String.Format("{0}{1}{2}", productId, "~", uniqueKey);

                while (productLookup.ContainsKey(newIdentifier))
                {
                    uniqueKey++;
                    newIdentifier = String.Format("{0}{1}{2}", productId, "~", uniqueKey);
                }

                planogramProductDto.Id = newIdentifier;
            }

            return planogramProductDto;
        }

        ///// <summary>
        ///// Validates the business rule data for the planogram product object
        ///// </summary>
        ///// <param name="planogramProductDto"></param>
        //private void ValidatePlanogramProductBusinessRules(PlanogramProductDto planogramProductDto)
        //{
        //    #region GTIN Business Rule
        //    if (planogramProductDto.Gtin != null && planogramProductDto.Gtin.Length > 14)
        //    {
        //        String oldValue = planogramProductDto.Gtin;
        //        planogramProductDto.Gtin = planogramProductDto.Gtin.Substring(0, 14);

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_Gtin_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_Gtin_Error_Description, planogramProductDto.Gtin), PlanogramEventLogEntryType.Warning, 
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    if (planogramProductDto.Gtin.Length == 0 && planogramProductDto.Id != null)
        //    {
        //        planogramProductDto.Gtin = planogramProductDto.Id.ToString();
        //    }
        //    #endregion

        //    #region Product Name Business Rule
        //    if (planogramProductDto.Name != null && planogramProductDto.Name.Length > 100)
        //    {
        //        String oldValue = planogramProductDto.Name;
        //        planogramProductDto.Name = planogramProductDto.Name.Substring(0, 100);

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_Name_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_Name_Error_Description, planogramProductDto.Name), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    if (planogramProductDto.Name.Length == 0 && planogramProductDto.Id != null)
        //    {
        //        planogramProductDto.Name = planogramProductDto.Id.ToString();
        //    }
        //    #endregion

        //    #region Brand Business Rule
        //    if (planogramProductDto.Brand != null && planogramProductDto.Brand.Length > 20)
        //    {
        //        String oldValue = planogramProductDto.Brand;
        //        planogramProductDto.Brand = planogramProductDto.Brand.Substring(0, 20);

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_Brand_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_Brand_Error_Description, planogramProductDto.Brand), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion

        //    #region Financial Group Code Business Rule
        //    if (planogramProductDto.FinancialGroupCode != null && planogramProductDto.FinancialGroupCode.Length > 50)
        //    {
        //        String oldValue = planogramProductDto.FinancialGroupCode;
        //        planogramProductDto.FinancialGroupCode = planogramProductDto.FinancialGroupCode.Substring(0, 50);

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_FinancialGroupCode_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_FinancialGroupCode_Error_Description, planogramProductDto.FinancialGroupCode), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion

        //    #region Financial Group Name Business Rule
        //    if (planogramProductDto.FinancialGroupName != null && planogramProductDto.FinancialGroupName.Length > 100)
        //    {
        //        String oldValue = planogramProductDto.FinancialGroupName;
        //        planogramProductDto.FinancialGroupName = planogramProductDto.FinancialGroupName.Substring(0, 100);

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_FinancialGroupName_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_FinancialGroupName_Error_Description, planogramProductDto.FinancialGroupName), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion

        //    #region Nesting Height Business Rule
        //    if (planogramProductDto.NestingHeight > planogramProductDto.Height)
        //    {
        //        Single oldValue = planogramProductDto.NestingHeight;
        //        planogramProductDto.NestingHeight = planogramProductDto.Height;

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_NestingHeight_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_NestingHeight_Error_Description, planogramProductDto.NestingHeight), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion

        //    #region Nesting Width Business Rule
        //    if (planogramProductDto.NestingWidth > planogramProductDto.Width)
        //    {
        //        Single oldValue = planogramProductDto.NestingWidth;
        //        planogramProductDto.NestingWidth = planogramProductDto.Width;

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_NestingWidth_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_NestingWidth_Error_Description, planogramProductDto.NestingWidth), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion

        //    #region Nesting Depth Business Rule
        //    if (planogramProductDto.NestingDepth > planogramProductDto.Depth)
        //    {
        //        Single oldValue = planogramProductDto.NestingDepth;
        //        planogramProductDto.NestingDepth = planogramProductDto.Depth;

        //        CreateErrorEventLogEntry(String.Format(Language.PlanogramProduct_NestingDepth_Error, oldValue),
        //                                    String.Format(Language.PlanogramProduct_NestingDepth_Error_Description, planogramProductDto.NestingDepth), PlanogramEventLogEntryType.Warning,
        //                                    PlanogramEventLogEventType.Import, DalCacheItemType.Product);
        //    }
        //    #endregion
        //}

        private void CreateMissingProductIdentifiers(Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String, PlanogramProductDto> productLookup)
        {
            List<String> identifiers = productLookup.Keys.ToList();

            foreach (var productItem in this.Children[DalCacheItemType.Product])
            {
                if (((PlanogramProductDto)productItem.Dto).Id == null)
                {
                    String[] fields = GetData(productItem.Data).ToArray();

                    String productId = GetValue(fields, schema, SpacemanFieldHelper.ProductID, DalCacheItemType.Product);
                    String productUpc = GetValue(fields, schema, SpacemanFieldHelper.ProductUPC, DalCacheItemType.Product);
                    String productName = GetValue(fields, schema, SpacemanFieldHelper.ProductName, DalCacheItemType.Product);

                    Int32 uniqueKey = 1;
                    String newIdentifier = String.Format("{0}{1}{2}", productId, "~", uniqueKey);

                    while (identifiers.Contains(newIdentifier)) // ensure the idenfifier is unique
                    {
                        uniqueKey++;
                        newIdentifier = String.Format("{0}{1}{2}", productId, "~", uniqueKey);
                    }
                    // update references
                    ((PlanogramProductDto)productItem.Dto).Id = newIdentifier;

                    if (((PlanogramProductDto)productItem.Dto).Gtin.Length == 0)
                    {
                        ((PlanogramProductDto)productItem.Dto).Gtin = productId;
                    }

                    if (((PlanogramProductDto)productItem.Dto).Name.Length == 0)
                    {
                        ((PlanogramProductDto)productItem.Dto).Name = productId;
                    }

                    // log duplicate product ids
                    CreateErrorEventLogEntry(String.Format(Language.DuplicateProductIds, productId), String.Format(Language.DuplicateProductIds_Description, productName, productUpc), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, DalCacheItemType.Product);
                    identifiers.Add(newIdentifier);
                }
            }
        }

        private Byte GetOrientationType(Byte orientationType)
        {
            switch (orientationType)
            {
                case 0:
                    return (Byte)PlanogramProductOrientationType.Front0;
                case 1:
                    return (Byte)PlanogramProductOrientationType.Front90;
                case 2:
                    return (Byte)PlanogramProductOrientationType.Front180;
                case 3:
                    return (Byte)PlanogramProductOrientationType.Front270;
                case 4:
                    return (Byte)PlanogramProductOrientationType.Top0;
                case 5:
                    return (Byte)PlanogramProductOrientationType.Top90;
                case 6:
                    return (Byte)PlanogramProductOrientationType.Top180;
                case 7:
                    return (Byte)PlanogramProductOrientationType.Top270;
                case 8:
                    return (Byte)PlanogramProductOrientationType.Left0;
                case 9:
                    return (Byte)PlanogramProductOrientationType.Left90;
                case 10:
                    return (Byte)PlanogramProductOrientationType.Left180;
                case 11:
                    return (Byte)PlanogramProductOrientationType.Left270;
                case 12:
                    return (Byte)PlanogramProductOrientationType.Back0;
                case 13:
                    return (Byte)PlanogramProductOrientationType.Back90;
                case 14:
                    return (Byte)PlanogramProductOrientationType.Back180;
                case 15:
                    return (Byte)PlanogramProductOrientationType.Back270;
                case 16:
                    return (Byte)PlanogramProductOrientationType.Bottom0;
                case 17:
                    return (Byte)PlanogramProductOrientationType.Bottom90;
                case 18:
                    return (Byte)PlanogramProductOrientationType.Bottom180;
                case 19:
                    return (Byte)PlanogramProductOrientationType.Bottom270;
                case 20:
                    return (Byte)PlanogramProductOrientationType.Right0;
                case 21:
                    return (Byte)PlanogramProductOrientationType.Right90;
                case 22:
                    return (Byte)PlanogramProductOrientationType.Right180;
                case 23:
                    return (Byte)PlanogramProductOrientationType.Right270;
                default:
                    return (Byte)PlanogramProductOrientationType.Front0;
            }
        }

        private Byte GetMerchandisingStyle(Byte merchandisingStyleType)
        {
            switch (merchandisingStyleType)
            {
                case 0:
                    return (Byte)PlanogramProductMerchandisingStyle.Unit;
                case 1:
                    return (Byte)PlanogramProductMerchandisingStyle.Tray;
                case 2:
                    return (Byte)PlanogramProductMerchandisingStyle.Case;
                case 3:
                    return (Byte)PlanogramProductMerchandisingStyle.Display;
                default:
                    return (Byte)PlanogramProductMerchandisingStyle.Unit;
            }
        }

        private Boolean GetIsFrontOnly(String[] fields, Dictionary<String,SpacemanSchemaItem> schema)
        {
            Boolean isFrontOnly = Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFront0, DalCacheItemType.Product));

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFront90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFront180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductFront270, DalCacheItemType.Product))) { isFrontOnly = false; }

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductTop0, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductTop90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductTop180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductTop270, DalCacheItemType.Product))) { isFrontOnly = false; }

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductLeft0, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductLeft90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductLeft180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductLeft270, DalCacheItemType.Product))) { isFrontOnly = false; }

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBack0, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBack90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBack180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBack270, DalCacheItemType.Product))) { isFrontOnly = false; }

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBottom0, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBottom90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBottom180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductBottom270, DalCacheItemType.Product))) { isFrontOnly = false; }

            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductRight0, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductRight90, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductRight180, DalCacheItemType.Product))) { isFrontOnly = false; }
            if (isFrontOnly) if (Convert.ToBoolean(GetByteValue(fields, schema, SpacemanFieldHelper.ProductRight270, DalCacheItemType.Product))) { isFrontOnly = false; }

            return isFrontOnly;
        }

   
        internal static Byte GetFillPatternType(Byte fillPatternType)
        {
            switch (fillPatternType)
            {
                case 0:
                    return (Byte)PlanogramProductFillPatternType.Solid;
                case 1:
                    return (Byte)PlanogramProductFillPatternType.DiagonalDown;
                case 2:
                    return (Byte)PlanogramProductFillPatternType.DiagonalUp;
                case 3:
                case 6:
                    return (Byte)PlanogramProductFillPatternType.Crosshatch;
                case 4:
                    return (Byte)PlanogramProductFillPatternType.Border;
                case 11:
                    return (Byte)PlanogramProductFillPatternType.Horizontal;
                case 12:
                    return (Byte)PlanogramProductFillPatternType.Vertical;
                default:
                    return (Byte)PlanogramProductFillPatternType.Solid;
            }
        }

        internal static Byte SetFillPatternType(Byte fillPatternType)
        {
            switch (fillPatternType)
            {
                case (Byte)PlanogramProductFillPatternType.Solid:
                    return 0;
                case (Byte)PlanogramProductFillPatternType.DiagonalDown:
                    return 1;
                case (Byte)PlanogramProductFillPatternType.DiagonalUp:
                    return 2;
                case (Byte)PlanogramProductFillPatternType.Crosshatch:
                    return 3;
                case (Byte)PlanogramProductFillPatternType.Border:
                    return 4;
                case (Byte)PlanogramProductFillPatternType.Horizontal:
                    return 11;
                case (Byte)PlanogramProductFillPatternType.Vertical:
                    return 12;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Encodes a planogram product dto
        /// </summary>
        private static String EncodePlanogramProductDto(PlanogramProductDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramPosition
        /// <summary>
        /// Decodes a planogram position dto
        /// </summary>
        private List<PlanogramPositionDto> DecodePlanogramPositionDto(String data, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String, String> componentSegmentLookup,
                Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups, List<PlanogramFixtureItemDto> fixtureItems, Dictionary<String, PlanogramProductDto> productLookup, Dictionary<String, Single> subComponentMerchandisingArea, List<PegData> pegDataList)
        {
            List<PlanogramPositionDto> planogramPositionDtos = new List<PlanogramPositionDto>();
            String[] fields = GetData(data).ToArray();

            Single xPos = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionX, DalCacheItemType.Position);
            Single yPos = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionY, DalCacheItemType.Position);

            Byte facingsHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionVertical, DalCacheItemType.Position);
            Byte facingsWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionHorizontal, DalCacheItemType.Position);
            Byte facingsDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionDepthFacings, DalCacheItemType.Position);
            Byte merchandisingStyle = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionMerchStyle, DalCacheItemType.Position));

            Byte facingsYHigh = 0;
            Byte facingsYWide = 0;
            Byte facingsYDeep = 0;
            Byte merchandisingStyleY = 0;
            Byte orientationTypeY = (Byte)PlanogramPositionOrientationType.Top0;

            Byte facingsXHigh = 0;
            Byte facingsXWide = 0;
            Byte facingsXDeep = 0;
            Byte merchandisingStyleX = 0;
            Byte orientationTypeX = (Byte)PlanogramPositionOrientationType.Right0;
            Boolean isXPlacedLeft = false;

            Byte facingsZHigh = 0;
            Byte facingsZWide = 0;
            Byte facingsZDeep = 0;
            Byte merchandisingStyleZ = 0;
            Byte orientationTypeZ = 0;
            
            Byte IsComplexPosition = GetByteValue(fields, schema, SpacemanFieldHelper.PositionComplex, DalCacheItemType.Position);
            Byte IsFixedPosition = GetByteValue(fields, schema, SpacemanFieldHelper.PositionFixed, DalCacheItemType.Position);
            String positionFixtureLink = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position);
            object planogramSubComponentDto = null;
            object planogramComponentDto = null;
            Single merchandisingArea = 0;
            Boolean isManuallyPlaced = false;

            if (IsComplexPosition == 0)
            {
                // calculate cap styles
                Byte capStyle = GetByteValue(fields, schema, SpacemanFieldHelper.PositionCapStyle, DalCacheItemType.Position);
                                
                if (fixelLookups.ContainsKey(positionFixtureLink))
                {
                    fixelLookups[positionFixtureLink].TryGetValue(PlanogramFixtureType.Component, out planogramComponentDto);
                    fixelLookups[positionFixtureLink].TryGetValue(PlanogramFixtureType.SubComponent, out planogramSubComponentDto);
                }

                if (planogramComponentDto != null && ((PlanogramComponentDto)planogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Shelf) // capping style only applied to shelf element type
                {
                    PlanogramProductDto planogramProductDto = null;
                    productLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position), out planogramProductDto);
                    merchandisingArea = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionVertSpace, DalCacheItemType.Position);
                    if (merchandisingArea.LessOrEqualThan(0))
                    {
                        merchandisingArea = GetShelfMerchHeight(subComponentMerchandisingArea, fixelLookups, positionFixtureLink, xPos + (facingsWide * planogramProductDto.Width), fixtureItems);
                    }

                    Byte frontOrientation = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));
                    Single productFrontdepth = GetOrientatedProductPresentationDepth((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);
                    orientationTypeY = GetPositionOrientationCapType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));
                    Single productCapDepth = GetOrientatedProductPresentationDepth((PlanogramPositionOrientationType)orientationTypeY, planogramProductDto, merchandisingStyle);

                    switch (capStyle)
                    {
                        case 1: // No Cap: No products are stacked to fill vertical merchandising space.
                            if (planogramProductDto.MaxStack > 0 && planogramProductDto.MaxStack < facingsHigh)
                            {
                                facingsHigh = planogramProductDto.MaxStack;
                            }
                            break;

                        case 2: // Min Cap: Stack units as high as possible in a front orientation, and then use only one capped unit on top. 
                            if (merchandisingArea.GreaterThan(0))
                            {
                                if (IsTopCapValid(productFrontdepth, facingsDeep, productCapDepth))
                                {
                                    if (planogramProductDto.MaxStack == 0 || planogramProductDto.MaxStack > facingsHigh)
                                    {
                                        planogramProductDto.MaxTopCap = 1;

                                        Single productFrontHeight = GetOrientatedProductPresentationHeight((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);
                                        Single productCapHeight = GetOrientatedProductPresentationHeight((PlanogramPositionOrientationType)orientationTypeY, planogramProductDto, merchandisingStyle);
                                        if (((productFrontHeight * facingsHigh) + productCapHeight).LessOrEqualThan(merchandisingArea))
                                        {
                                            facingsYHigh = 1;
                                            facingsYWide = facingsWide;
                                            facingsYDeep = CalculateFacingsYDeep(orientationTypeY, (PlanogramSubComponentDto)planogramSubComponentDto, planogramProductDto, facingsDeep, merchandisingStyle);
                                            merchandisingStyleY = merchandisingStyle;
                                        }
                                    }
                                }
                            }
                            break;

                        case 3: // Med Cap: Stack units as high as possible in a front orientation, and then use all the remaining vertical space for capped units. 
                            if (merchandisingArea.GreaterThan(0))
                            {                                
                                if (IsTopCapValid(productFrontdepth, facingsDeep, productCapDepth))
                                {
                                    if (planogramProductDto.MaxStack == 0 || planogramProductDto.MaxStack > facingsHigh)
                                    {
                                        planogramProductDto.MaxTopCap = 99;

                                        Single productFrontHeight = GetOrientatedProductPresentationHeight((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);
                                        orientationTypeY = GetPositionOrientationCapType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));

                                        facingsYWide = facingsWide;
                                        facingsYHigh = CalculateFacingsYHigh(orientationTypeY, planogramProductDto, (merchandisingArea - (facingsHigh * productFrontHeight)), facingsHigh, merchandisingStyle);
                                        facingsYDeep = CalculateFacingsYDeep(orientationTypeY, (PlanogramSubComponentDto)planogramSubComponentDto, planogramProductDto, facingsDeep, merchandisingStyle);
                                        merchandisingStyleY = merchandisingStyle;
                                    }
                                }
                            }
                            break;

                        case 4: // Max Cap: One unit is placed in a front orientation and the remaining units are capped. 
                            if (merchandisingArea.GreaterThan(0))
                            {
                                if (IsTopCapValid(productFrontdepth, facingsDeep, productCapDepth))
                                {
                                    if (planogramProductDto.MaxStack == 0 || planogramProductDto.MaxStack > 1)
                                    {
                                        planogramProductDto.MaxTopCap = 99;
                                        planogramProductDto.MaxStack = 1;

                                        Single productFrontHeight = GetOrientatedProductPresentationHeight((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);

                                        facingsYWide = facingsWide;
                                        facingsYHigh = CalculateFacingsYHigh(orientationTypeY, planogramProductDto, (merchandisingArea - (1 * productFrontHeight)), facingsHigh, merchandisingStyle);
                                        facingsYDeep = CalculateFacingsYDeep(orientationTypeY, (PlanogramSubComponentDto)planogramSubComponentDto, planogramProductDto, facingsDeep, merchandisingStyle);
                                        merchandisingStyleY = merchandisingStyle;
                                    }
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }
                else if (planogramComponentDto != null && 
                        (((PlanogramComponentDto)planogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Peg || 
                         ((PlanogramComponentDto)planogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Bar)) 
                {
                    // calculate position depth and peg depth
                    PlanogramProductDto planogramProductDto = null;
                    productLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position), out planogramProductDto);

                    String positionPegId = GetValue(fields, schema, SpacemanFieldHelper.PositionPegID, DalCacheItemType.Position);
                    Byte autoFillPeg = GetByteValue(fields, schema, SpacemanFieldHelper.PositionAutoFillPeg, DalCacheItemType.Position);
                    PegData pegData = pegDataList.Where(p => p.Id == positionPegId).FirstOrDefault();
                    Byte frontOrientation = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));
                    Single productDepth = GetOrientatedProductPresentationDepth((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);

                    if (autoFillPeg == 1)
                    {
                        if (pegData != null && pegData.Length > 0)
                        {
                            // calculate merchandising area to peg depth or the applied merch depth
                            merchandisingArea = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionVertSpace, DalCacheItemType.Position);
                            if (merchandisingArea.LessOrEqualThan(0))
                            {
                                subComponentMerchandisingArea.TryGetValue(((PlanogramSubComponentDto)planogramSubComponentDto).Id.ToString(), out merchandisingArea);
                            }

                            if (merchandisingArea > 0)
                            {
                                merchandisingArea = merchandisingArea < pegData.Length ? merchandisingArea : pegData.Length;
                            }
                            else
                            {
                                merchandisingArea = pegData.Length;
                            }

                            if (productDepth > 0)
                            {
                                facingsDeep = (Byte)Math.Floor((merchandisingArea / productDepth));
                            }
                            facingsDeep = facingsDeep == 0 ? (Byte)1 : facingsDeep; 
                        }
                        else
                        {
                            // we cant calulate the auto fill deep value without peg library info and data 
                            // within file cannot be trusted as Spaceman calulates this upon loading.

                            // V8-31499 : Rather than defaulting to 1 we will take the deep value from the file as in some cases this value is correct.
                            //facingsDeep = 1; 
                        }
                    }
                    if (planogramProductDto != null)
                    {
                        if (pegData != null)
                        {
                            planogramProductDto.PegDepth = pegData.Length;
                        }
                        else
                        {
                            planogramProductDto.PegDepth = (facingsDeep * productDepth);
                        }
                        planogramProductDto.NumberOfPegHoles = 1;
                    }
                }
                else if (planogramComponentDto != null && ((PlanogramComponentDto)planogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Rod)
                {
                    // calculate position depth and peg depth
                    PlanogramProductDto planogramProductDto = null;
                    productLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position), out planogramProductDto);

                    String positionPegId = GetValue(fields, schema, SpacemanFieldHelper.PositionPegID, DalCacheItemType.Position);
                    PegData pegData = pegDataList.Where(p => p.Id == positionPegId).FirstOrDefault();
                    
                    if (planogramProductDto != null)
                    {
                        if (pegData != null)
                        {
                            planogramProductDto.PegDepth = pegData.Length;
                        }
                        else
                        {
                            Byte frontOrientation = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));
                            Single productDepth = GetOrientatedProductPresentationDepth((PlanogramPositionOrientationType)frontOrientation, planogramProductDto, merchandisingStyle);

                            planogramProductDto.PegDepth = (facingsDeep * productDepth);
                        }
                        planogramProductDto.NumberOfPegHoles = 1;
                    }
                }
            }
            else
            {
                facingsYHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopVertical, DalCacheItemType.Position);
                facingsYWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopHorizontal, DalCacheItemType.Position);
                facingsYDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopDepthFacings, DalCacheItemType.Position);
                merchandisingStyleY = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopMerchStyle, DalCacheItemType.Position));
                orientationTypeY = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopOrientation, DalCacheItemType.Position));

                facingsXHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightVertical, DalCacheItemType.Position);
                facingsXWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightHorizontal, DalCacheItemType.Position);
                facingsXDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightDepthFacings, DalCacheItemType.Position);
                merchandisingStyleX = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightMerchStyle, DalCacheItemType.Position));
                orientationTypeX = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightOrientation, DalCacheItemType.Position));
                isXPlacedLeft = false;

                // If complex position has right and left presentation then we have to split into 2 positions
                if (facingsXHigh > 0 && GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftVertical, DalCacheItemType.Position) > 0)
                {
                    return CalculateMultiplePlanogramPositionDtos(fields, schema, componentSegmentLookup, productLookup);
                }

                if (facingsXHigh == 0) // There is no right presentation, need to check left
                {
                    facingsXHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftVertical, DalCacheItemType.Position);
                    facingsXWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftHorizontal, DalCacheItemType.Position);
                    facingsXDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftDepthFacings, DalCacheItemType.Position);
                    merchandisingStyleX = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftMerchStyle, DalCacheItemType.Position));
                    orientationTypeX = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftOrientation, DalCacheItemType.Position));
                    isXPlacedLeft = true;
                }

                if (facingsXWide > 0 || facingsYWide > 0)
                {
                    PlanogramProductDto planogramProductDto = null;
                    productLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position), out planogramProductDto);
                    if (planogramProductDto != null)
                    {
                        planogramProductDto.MaxRightCap = (facingsXWide > 0 && facingsXWide <= 99) ? (Byte)99 : facingsXWide;
                        planogramProductDto.MaxTopCap = (facingsYWide > 0 && facingsYWide <= 99) ? (Byte)99 : facingsYWide;
                    }
                }

                facingsZHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackVertical, DalCacheItemType.Position);
                facingsZWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackHorizontal, DalCacheItemType.Position);
                facingsZDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackDepthFacings, DalCacheItemType.Position);
                merchandisingStyleZ = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackMerchStyle, DalCacheItemType.Position));
                orientationTypeZ = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackOrientation, DalCacheItemType.Position));
                isManuallyPlaced = true;
            }

            if (planogramSubComponentDto == null || planogramComponentDto == null)
            {
                if (fixelLookups.ContainsKey(positionFixtureLink))
                {
                    fixelLookups[positionFixtureLink].TryGetValue(PlanogramFixtureType.Component, out planogramComponentDto);
                    fixelLookups[positionFixtureLink].TryGetValue(PlanogramFixtureType.SubComponent, out planogramSubComponentDto);
                }
            }

            if (planogramComponentDto !=null && ((PlanogramComponentDto)planogramComponentDto).ComponentType == (Byte)PlanogramComponentType.Shelf && planogramSubComponentDto != null)
            {
                yPos += ((PlanogramSubComponentDto)planogramSubComponentDto).Height;
            }

            if (IsFixedPosition > 0) // position is fixed within spaceman, update sub component reference if not already manual merchandising strategy
            {
                if (planogramSubComponentDto != null)
                {
                    ((PlanogramSubComponentDto)planogramSubComponentDto).MerchandisingStrategyX = (Byte)PlanogramSubComponentXMerchStrategyType.Manual;
                }
            }

            String segment = "0";
            componentSegmentLookup.TryGetValue(positionFixtureLink, out segment);
            if (segment == "-1") { segment = "1"; }

            planogramPositionDtos.Add(new PlanogramPositionDto()
            {
                Id = GetNextId(),//GetValue(fields, schema, SpacemanFieldHelper.PositionID, DalCacheItemType.Position),
                PlanogramId = 1,
                PlanogramFixtureItemId = segment,
                PlanogramFixtureComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramSubComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramProductId = GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position),
                X = xPos,
                Y = yPos,
                Z = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionZ, DalCacheItemType.Position),
                Slope = 0,
                Angle = 0,
                Roll = 0,
                FacingsHigh = facingsHigh,
                FacingsWide = facingsWide,
                FacingsDeep = facingsDeep,
                MerchandisingStyle = merchandisingStyle,
                OrientationType = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position)),
                FacingsXHigh = facingsXHigh,
                FacingsXWide = facingsXWide,
                FacingsXDeep = facingsXDeep,
                MerchandisingStyleX = merchandisingStyleX,
                OrientationTypeX = orientationTypeX,
                IsXPlacedLeft = isXPlacedLeft,
                FacingsYHigh = facingsYHigh,
                FacingsYWide = facingsYWide,
                FacingsYDeep = facingsYDeep,
                MerchandisingStyleY = merchandisingStyleY,
                OrientationTypeY = orientationTypeY,
                IsYPlacedBottom = false,
                FacingsZHigh = facingsZHigh,
                FacingsZWide = facingsZWide,
                FacingsZDeep = facingsZDeep,
                MerchandisingStyleZ = merchandisingStyleZ,
                OrientationTypeZ = orientationTypeZ,
                IsZPlacedFront = false,
                Sequence = 1,
                SequenceX = 1,
                SequenceY = 1,
                SequenceZ = 1,
                UnitsHigh = 1,
                UnitsWide = 1,
                UnitsDeep = 1,
                TotalUnits = 1,
                DepthSqueeze = 1,
                DepthSqueezeX = 1,
                DepthSqueezeY = 1,
                DepthSqueezeZ = 1,
                HorizontalSqueeze = 1,
                HorizontalSqueezeX = 1,
                HorizontalSqueezeY = 1,
                HorizontalSqueezeZ = 1,
                VerticalSqueeze = 1,
                VerticalSqueezeX = 1,
                VerticalSqueezeY = 1,
                VerticalSqueezeZ = 1,
                IsManuallyPlaced = isManuallyPlaced
            });

            return planogramPositionDtos;
        }

        /// <summary>
        /// The size of the front block must be >= size of the cap depth
        /// </summary>
        /// <returns>True if top cap can be added</returns>
        private bool IsTopCapValid(Single productFrontdepth, Byte facingsDeep, Single productCapDepth)
        {
            return ((productFrontdepth * facingsDeep).GreaterOrEqualThan(productCapDepth));
        }

        /// <summary>
        /// Gets the merchandising height of the shelf that the position belongs too
        /// </summary>
        /// <param name="subComponentMerchandisingArea">Merchandising height lookups by sub components</param>
        /// <param name="fixelLookups">The fixel lookups</param>
        /// <param name="positionFixtureLink">The current position fixel link</param>
        /// <param name="xPosition">The positions furthest x position</param>
        /// <param name="bayPositions">The plans fixtureItems</param>
        /// <returns>The shelf merchandising height relative to the position</returns>
        private Single GetShelfMerchHeight(Dictionary<String, Single> subComponentMerchandisingArea, Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups, String positionFixtureLink, Single xPosition, List<PlanogramFixtureItemDto> fixtureItems)
        {
            Single merchandisingArea = 0;
            Dictionary<PlanogramFixtureType, object> fixelItems = fixelLookups[positionFixtureLink];

            if (fixelItems.ContainsKey(PlanogramFixtureType.SubComponent) && fixelItems.ContainsKey(PlanogramFixtureType.FixtureComponent))
            {
                // validate that the position location refernce in the spaceman file corresponds to the shelf that the position sits upon

                PlanogramFixtureComponentDto positionFixtureComponent = (PlanogramFixtureComponentDto)fixelItems[PlanogramFixtureType.FixtureComponent];
                PlanogramSubComponentDto positionSubComponent = (PlanogramSubComponentDto)fixelItems[PlanogramFixtureType.SubComponent];
                PlanogramFixtureItemDto positionFixtureItem = fixtureItems.FirstOrDefault(p => p.Id.Equals(positionFixtureComponent.PlanogramFixtureId));

                Single currentAssignedComponentLocation = positionFixtureComponent.X + positionSubComponent.X;

                if (positionSubComponent.CombineType == (Byte)PlanogramSubComponentCombineType.None || currentAssignedComponentLocation.LessOrEqualThan(xPosition) && ((currentAssignedComponentLocation + positionSubComponent.Width).GreaterOrEqualThan(xPosition)))
                {
                    subComponentMerchandisingArea.TryGetValue((positionSubComponent).Id.ToString(), out merchandisingArea);
                }
                else
                {
                    foreach (String fixel in fixelLookups.Keys)
                    {
                        if (positionFixtureLink != fixel)
                        {
                            fixelItems = fixelLookups[fixel];

                            if (fixelItems.ContainsKey(PlanogramFixtureType.SubComponent) && fixelItems.ContainsKey(PlanogramFixtureType.FixtureComponent))
                            {
                                PlanogramFixtureComponentDto fixtureComponent = (PlanogramFixtureComponentDto)fixelItems[PlanogramFixtureType.FixtureComponent];
                                Single shelfTollerance = Math.Abs(positionFixtureComponent.Y - fixtureComponent.Y);

                                if (fixtureComponent.Y == positionFixtureComponent.Y || shelfTollerance.LessThan(0.1)) 
                                {
                                    PlanogramSubComponentDto subComponent = (PlanogramSubComponentDto)fixelItems[PlanogramFixtureType.SubComponent];
                                    PlanogramFixtureItemDto fixtureItem = fixtureItems.FirstOrDefault(p => p.Id.Equals(fixtureComponent.PlanogramFixtureId));

                                    // get the relative position location
                                    Single componentLocation = (fixtureItem.X - positionFixtureItem.X) + fixtureComponent.X + subComponent.X;

                                    if (componentLocation.LessOrEqualThan(xPosition) && ((componentLocation + subComponent.Width).GreaterOrEqualThan(xPosition)))
                                    {
                                        subComponentMerchandisingArea.TryGetValue((subComponent).Id.ToString(), out merchandisingArea);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return merchandisingArea;
        }

        private List<PlanogramPositionDto> CalculateMultiplePlanogramPositionDtos(String[] fields, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String, String> componentSegmentLookup,
                                                                                    Dictionary<String, PlanogramProductDto> productLookup)
        {
            // Complex position that consists of left, front, right has to be split into 2 positions
            // The first position is the left presentation
            // The second position is the front, top, back, right presentation
            
            List<PlanogramPositionDto> planogramPositionDtos = new List<PlanogramPositionDto>();

            Byte facingsHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftVertical, DalCacheItemType.Position);
            Byte facingsWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftHorizontal, DalCacheItemType.Position);
            Byte facingsDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftDepthFacings, DalCacheItemType.Position);
            Byte merchandisingStyle = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftMerchStyle, DalCacheItemType.Position));
            Byte orientationType = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionLeftOrientation, DalCacheItemType.Position));

            Byte facingsYHigh = 0;
            Byte facingsYWide = 0;
            Byte facingsYDeep = 0;
            Byte merchandisingStyleY = 0;
            Byte orientationTypeY = 0;

            Byte facingsXHigh = 0;
            Byte facingsXWide = 0;
            Byte facingsXDeep = 0;
            Byte merchandisingStyleX = 0;
            Byte orientationTypeX = 0;
            Boolean isXPlacedLeft = true;

            Byte facingsZHigh = 0;
            Byte facingsZWide = 0;
            Byte facingsZDeep = 0;
            Byte merchandisingStyleZ = 0;
            Byte orientationTypeZ = 0;

            String segment = "0";
            componentSegmentLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position), out segment);
            if (segment == "-1") { segment = "1"; }

            PlanogramProductDto planogramProductDto = null;
            productLookup.TryGetValue(GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position), out planogramProductDto);

            // Obtain detail of how much space the left presentation takes up
            Single x = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionX, DalCacheItemType.Position);
            Byte frontOrientation = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));
            Single orientatedProductWidth = GetProductWidth((PlanogramPositionOrientationType)frontOrientation, planogramProductDto);
            
            #region Create Left presentation
            planogramPositionDtos.Add(new PlanogramPositionDto()
            {
                Id = GetValue(fields, schema, SpacemanFieldHelper.PositionID, DalCacheItemType.Position),
                PlanogramId = 1,
                PlanogramFixtureItemId = segment,
                PlanogramFixtureComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramSubComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramProductId = GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position),
                X = x,
                Y = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionY, DalCacheItemType.Position),
                Z = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionZ, DalCacheItemType.Position),
                Slope = 0,
                Angle = 0,
                Roll = 0,
                FacingsHigh = facingsHigh,
                FacingsWide = facingsWide,
                FacingsDeep = facingsDeep,
                MerchandisingStyle = merchandisingStyle,
                OrientationType = orientationType,
                FacingsXHigh = facingsXHigh,
                FacingsXWide = facingsXWide,
                FacingsXDeep = facingsXDeep,
                MerchandisingStyleX = merchandisingStyleX,
                OrientationTypeX = orientationTypeX,
                IsXPlacedLeft = isXPlacedLeft,
                FacingsYHigh = facingsYHigh,
                FacingsYWide = facingsYWide,
                FacingsYDeep = facingsYDeep,
                MerchandisingStyleY = merchandisingStyleY,
                OrientationTypeY = orientationTypeY,
                IsYPlacedBottom = false,
                FacingsZHigh = facingsZHigh,
                FacingsZWide = facingsZWide,
                FacingsZDeep = facingsZDeep,
                MerchandisingStyleZ = merchandisingStyleZ,
                OrientationTypeZ = orientationTypeZ,
                IsZPlacedFront = false,
                Sequence = 1,
                SequenceX = 1,
                SequenceY = 1,
                SequenceZ = 1,
                UnitsHigh = 1,
                UnitsWide = 1,
                UnitsDeep = 1,
                TotalUnits = 1,
                IsManuallyPlaced = true
            });
            #endregion

            // Calculate the amount of space the left presentation occupies
            x += (facingsXWide * orientatedProductWidth);

            #region Create remaining position presentation (Front, Top, Back, Right)
            facingsHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionVertical, DalCacheItemType.Position);
            facingsWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionHorizontal, DalCacheItemType.Position);
            facingsDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionDepthFacings, DalCacheItemType.Position);
            merchandisingStyle = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionMerchStyle, DalCacheItemType.Position));
            orientationType = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionOrientation, DalCacheItemType.Position));

            facingsYHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopVertical, DalCacheItemType.Position);
            facingsYWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopHorizontal, DalCacheItemType.Position);
            facingsYDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopDepthFacings, DalCacheItemType.Position);
            merchandisingStyleY = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopMerchStyle, DalCacheItemType.Position));
            orientationTypeY = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionTopOrientation, DalCacheItemType.Position));

            facingsXHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightVertical, DalCacheItemType.Position);
            facingsXWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightHorizontal, DalCacheItemType.Position);
            facingsXDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightDepthFacings, DalCacheItemType.Position);
            merchandisingStyleX = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightMerchStyle, DalCacheItemType.Position));
            orientationTypeX = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionRightOrientation, DalCacheItemType.Position));
            isXPlacedLeft = false;

            facingsZHigh = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackVertical, DalCacheItemType.Position);
            facingsZWide = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackHorizontal, DalCacheItemType.Position);
            facingsZDeep = GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackDepthFacings, DalCacheItemType.Position);
            merchandisingStyleZ = GetPositionMerchandisingStyle(GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackMerchStyle, DalCacheItemType.Position));
            orientationTypeZ = GetPositionOrientationType(GetByteValue(fields, schema, SpacemanFieldHelper.PositionBackOrientation, DalCacheItemType.Position));

            planogramPositionDtos.Add(new PlanogramPositionDto()
            {
                Id = GetValue(fields, schema, SpacemanFieldHelper.PositionID, DalCacheItemType.Position) + "-2",
                PlanogramId = 1,
                PlanogramFixtureItemId = segment,
                PlanogramFixtureComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramSubComponentId = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position),
                PlanogramProductId = GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position),
                X = x,
                Y = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionY, DalCacheItemType.Position),
                Z = GetSingleValue(fields, schema, SpacemanFieldHelper.PositionZ, DalCacheItemType.Position),
                Slope = 0,
                Angle = 0,
                Roll = 0,
                FacingsHigh = facingsHigh,
                FacingsWide = facingsWide,
                FacingsDeep = facingsDeep,
                MerchandisingStyle = merchandisingStyle,
                OrientationType = orientationType,
                FacingsXHigh = facingsXHigh,
                FacingsXWide = facingsXWide,
                FacingsXDeep = facingsXDeep,
                MerchandisingStyleX = merchandisingStyleX,
                OrientationTypeX = orientationTypeX,
                IsXPlacedLeft = isXPlacedLeft,
                FacingsYHigh = facingsYHigh,
                FacingsYWide = facingsYWide,
                FacingsYDeep = facingsYDeep,
                MerchandisingStyleY = merchandisingStyleY,
                OrientationTypeY = orientationTypeY,
                IsYPlacedBottom = false,
                FacingsZHigh = facingsZHigh,
                FacingsZWide = facingsZWide,
                FacingsZDeep = facingsZDeep,
                MerchandisingStyleZ = merchandisingStyleZ,
                OrientationTypeZ = orientationTypeZ,
                IsZPlacedFront = false,
                Sequence = 1,
                SequenceX = 1,
                SequenceY = 1,
                SequenceZ = 1,
                UnitsHigh = 1,
                UnitsWide = 1,
                UnitsDeep = 1,
                TotalUnits = 1,
                IsManuallyPlaced = true
            });
            #endregion

            if (planogramProductDto != null)
            {
                planogramProductDto.MaxRightCap = (facingsXWide > 0 && facingsXWide <= 99) ? (Byte)99 : facingsXWide;
                planogramProductDto.MaxTopCap = (facingsYWide > 0 && facingsYWide <= 99) ? (Byte)99 : facingsYWide;
            }

            return planogramPositionDtos;
        }

        private static Byte CalculateFacingsYHigh(Byte orientationTypeY, PlanogramProductDto planogramProductDto, Single remainingSpace, Byte facingsHigh, Byte merchandisingStyle)
        {
            Byte facingsYHigh = 0;
            Single productCapHeight = GetOrientatedProductPresentationHeight((PlanogramPositionOrientationType)orientationTypeY, planogramProductDto, merchandisingStyle);

            while ((remainingSpace - productCapHeight).GreaterOrEqualThan(0))
            {
                remainingSpace -= productCapHeight;
                facingsYHigh++;
            }
            return facingsYHigh;
        }

        private static Byte CalculateFacingsYDeep(Byte orientationTypeY, PlanogramSubComponentDto planogramSubComponentDto, PlanogramProductDto planogramProductDto, Byte facingsDeep, Byte merchandisingStyle)
        {
            Byte facingsYDeep = 0;
            Single productCapDepth = GetOrientatedProductPresentationDepth((PlanogramPositionOrientationType)orientationTypeY, planogramProductDto, merchandisingStyle);
            Single remainingDepthSpace = planogramSubComponentDto.Depth;

            while ((remainingDepthSpace - productCapDepth).GreaterOrEqualThan(0))
            {
                remainingDepthSpace -= productCapDepth;
                facingsYDeep++;
            }
            return facingsYDeep;
        }

        private static Single GetOrientatedProductPresentationHeight(PlanogramPositionOrientationType orientationType, PlanogramProductDto planogramProductDto, Byte merchandisingStyle)
        {
            switch (orientationType)
            {
                default:
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Height;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayHeight;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseHeight;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayHeight;
                        default:
                            return planogramProductDto.Height;
                    }
                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Width;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayWidth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseWidth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayWidth;
                        default:
                            return planogramProductDto.Width;
                    }
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Depth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayDepth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseDepth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayDepth;
                        default:
                            return planogramProductDto.Depth;
                    }
            }
        }

        private static Single GetOrientatedProductPresentationDepth(PlanogramPositionOrientationType orientationType, PlanogramProductDto planogramProductDto, Byte merchandisingStyle)
        {
            switch (orientationType)
            {
                default:
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Depth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayDepth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseDepth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayDepth;
                        default:
                            return planogramProductDto.Depth;
                    }
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Width;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayWidth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseWidth;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayWidth;
                        default:
                            return planogramProductDto.Width;
                    }
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    switch (merchandisingStyle)
                    {
                        case (Byte)PlanogramPositionMerchandisingStyle.Unit:
                            return planogramProductDto.Height;
                        case (Byte)PlanogramPositionMerchandisingStyle.Tray:
                            return planogramProductDto.TrayHeight;
                        case (Byte)PlanogramPositionMerchandisingStyle.Case:
                            return planogramProductDto.CaseHeight;
                        case (Byte)PlanogramPositionMerchandisingStyle.Display:
                            return planogramProductDto.DisplayHeight;
                        default:
                            return planogramProductDto.Height;
                    }
            }
        }

        private static Single GetProductWidth(PlanogramPositionOrientationType orientationType, PlanogramProductDto planogramProductDto)
        {
            switch (orientationType)
            {
                default:
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                    return planogramProductDto.Width;
                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                    return planogramProductDto.Height;
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    return planogramProductDto.Depth;
            }
        }
        
        private static Byte GetPositionOrientationType(Byte orientationType)
        {
            switch (orientationType)
            {
                case 0:
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case 1:
                    return (Byte)PlanogramPositionOrientationType.Front90;
                case 2:
                    return (Byte)PlanogramPositionOrientationType.Front180;
                case 3:
                    return (Byte)PlanogramPositionOrientationType.Front270;
                case 4:
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case 5:
                    return (Byte)PlanogramPositionOrientationType.Top90;
                case 6:
                    return (Byte)PlanogramPositionOrientationType.Top180;
                case 7:
                    return (Byte)PlanogramPositionOrientationType.Top270;
                case 8:
                    return (Byte)PlanogramPositionOrientationType.Left0;
                case 9:
                    return (Byte)PlanogramPositionOrientationType.Left90;
                case 10:
                    return (Byte)PlanogramPositionOrientationType.Left180;
                case 11:
                    return (Byte)PlanogramPositionOrientationType.Left270;
                case 12:
                    return (Byte)PlanogramPositionOrientationType.Back0;
                case 13:
                    return (Byte)PlanogramPositionOrientationType.Back90;
                case 14:
                    return (Byte)PlanogramPositionOrientationType.Back180;
                case 15:
                    return (Byte)PlanogramPositionOrientationType.Back270;
                case 16:
                    return (Byte)PlanogramPositionOrientationType.Bottom0;
                case 17:
                    return (Byte)PlanogramPositionOrientationType.Bottom90;
                case 18:
                    return (Byte)PlanogramPositionOrientationType.Bottom180;
                case 19:
                    return (Byte)PlanogramPositionOrientationType.Bottom270;
                case 20:
                    return (Byte)PlanogramPositionOrientationType.Right0;
                case 21:
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case 22:
                    return (Byte)PlanogramPositionOrientationType.Right180;
                case 23:
                    return (Byte)PlanogramPositionOrientationType.Right270;
                default:
                    return (Byte)PlanogramPositionOrientationType.Front0;
            }
        }

        private static Byte GetPositionOrientationCapType(Byte orientationType)
        {
            switch (orientationType)
            {
                case 0: // Front0
                    return (Byte)PlanogramPositionOrientationType.Top0;
                case 1: // Front90
                    return (Byte)PlanogramPositionOrientationType.Left90;
                case 2: // Front180
                    return (Byte)PlanogramPositionOrientationType.Top180;
                case 3: // Front270
                    return (Byte)PlanogramPositionOrientationType.Right270;
                case 4: // Top0
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case 5: // Top90
                    return (Byte)PlanogramPositionOrientationType.Left180;
                case 6: // Top180
                    return (Byte)PlanogramPositionOrientationType.Front180;
                case 7: // Top270
                    return (Byte)PlanogramPositionOrientationType.Right180;
                case 8: // Left0
                    return (Byte)PlanogramPositionOrientationType.Top270;
                case 9: // Left90
                    return (Byte)PlanogramPositionOrientationType.Back90;
                case 10: // Left180
                    return (Byte)PlanogramPositionOrientationType.Bottom270;
                case 11: // Left270
                    return (Byte)PlanogramPositionOrientationType.Front270;
                case 12: // Back0
                    return (Byte)PlanogramPositionOrientationType.Bottom180;
                case 13: // Back90
                    return (Byte)PlanogramPositionOrientationType.Right90;
                case 14: // Back180
                    return (Byte)PlanogramPositionOrientationType.Bottom0;
                case 15: // Back270
                    return (Byte)PlanogramPositionOrientationType.Right270;
                case 16: // Bottom0
                    return (Byte)PlanogramPositionOrientationType.Front0;
                case 17: // Bottom90
                    return (Byte)PlanogramPositionOrientationType.Left0;
                case 18: // Bottom180
                    return (Byte)PlanogramPositionOrientationType.Back0;
                case 19: // Bottom270
                    return (Byte)PlanogramPositionOrientationType.Right0;
                case 20: // Right0
                    return (Byte)PlanogramPositionOrientationType.Top90;
                case 21: // Right90
                    return (Byte)PlanogramPositionOrientationType.Front90;
                case 22: // Right180
                    return (Byte)PlanogramPositionOrientationType.Bottom90;
                case 23: // Right270
                    return (Byte)PlanogramPositionOrientationType.Front270;          
                default:
                    return (Byte)PlanogramPositionOrientationType.Top0;
            }
        }

        private static Byte GetPositionMerchandisingStyle(Byte merchandisingStyleType)
        {
            switch (merchandisingStyleType)
            {
                case 0:
                    return (Byte)PlanogramPositionMerchandisingStyle.Unit;
                case 1:
                    return (Byte)PlanogramPositionMerchandisingStyle.Tray;
                case 2:
                    return (Byte)PlanogramPositionMerchandisingStyle.Case;
                case 3:
                    return (Byte)PlanogramPositionMerchandisingStyle.Display;
                default:
                    return (Byte)PlanogramPositionMerchandisingStyle.Unit;
            }
        }

        /// <summary>
        /// Encodes a planogram position dto
        /// </summary>
        private static String EncodePlanogramPositionDto(PlanogramPositionDto dto)
        {
            return null;
        }
        #endregion

        #region PlanogramAnnotation
        /// <summary>
        /// Decodes a planogram annotation dto
        /// </summary>
        private object DecodePlanogramAnnotationDto(String[] fixelData, Dictionary<String, SpacemanSchemaItem> schema, List<PlanogramAnnotationDto> availableFontDetails)
        {
            String fontId = GetValue(fixelData, schema, SpacemanFieldHelper.FixelFontID, DalCacheItemType.Fixel);
            Int32 fontColour = -16777216;
            String fontName = "Arial";
            Single fontSize = 8;

            PlanogramAnnotationDto fontDetail = availableFontDetails.Where(p => p.Id.Equals(fontId)).FirstOrDefault();

            if (fontDetail == null )
            {
                fontDetail = availableFontDetails.FirstOrDefault();
            }

            if (fontDetail != null)
            {
                fontColour = fontDetail.FontColour;
                fontName = fontDetail.FontName;
                fontSize = fontDetail.FontSize;
            }

            String segment = GetValue(fixelData, schema, SpacemanFieldHelper.FixelSegment, DalCacheItemType.Fixel);
            if (segment == "-1") { segment = "1"; }

            return new PlanogramAnnotationDto()
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramId = 1,
                PlanogramFixtureItemId = segment,
                AnnotationType = (Byte)PlanogramAnnotationType.TextBox,
                X = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelX, DalCacheItemType.Fixel),
                Y = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelY, DalCacheItemType.Fixel),
                Z = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelZ, DalCacheItemType.Fixel),
                Height = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelHeight, DalCacheItemType.Fixel),
                Width = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelWidth, DalCacheItemType.Fixel),
                Depth = GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelDepth, DalCacheItemType.Fixel),
                Text = GetValue(fixelData, schema, SpacemanFieldHelper.FixelLabel, DalCacheItemType.Fixel),
                FontColour = fontColour,
                BackgroundColour = ConvertColour((Int32)GetSingleValue(fixelData, schema, SpacemanFieldHelper.FixelColour, DalCacheItemType.Fixel)),
                CanReduceFontToFit = Convert.ToBoolean(GetByteValue(fixelData, schema, SpacemanFieldHelper.FixelTextboxReduceToFit, DalCacheItemType.Fixel)),
                FontName = fontName,
                FontSize = fontSize,
                BorderColour = -16777216,
                BorderThickness = 0.1F
            };
        }
        #endregion

        #region PlanogramPerformance

        private void DecodePlanogramPerformanceDto(PlanogramProductDto planogramProductDto, String data, Dictionary<String,SpacemanSchemaItem> schema)
        {
            String[] fields = GetData(data).ToArray();

            PlanogramPerformanceDataDto planogramPerformanceDataDto = new PlanogramPerformanceDataDto()
            {
                Id = IdentityHelper.GetNextInt32(),
                PlanogramPerformanceId = 1,
                PlanogramProductId = planogramProductDto.Id
            };

            foreach (PlanogramMetricMappingDto mapping in _dalCache.MetricMappings)
            {
                String propertyName = "P" + mapping.MetricId.ToString();

                PropertyInfo property = typeof(PlanogramPerformanceDataDto).GetProperty(propertyName);
                if (property != null)
                {
                    MapSetValue(planogramPerformanceDataDto, fields, schema, DalCacheItemType.Product, mapping.Source, propertyName, property);
                }
            }

            if (planogramPerformanceDataDto != null)
            {
                _dalCache.PlanogramPerformanceDataDtoList.Add(planogramPerformanceDataDto);
            }
        }

        #endregion

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates list of Spaceman specific data format
        /// </summary>
        /// <param name="data">The spaceman data</param>
        /// <returns>A list of data items</returns>
        internal static List<String> GetData(String data)
        {
            const Char doubleQuote = '"';
            const Char comma = ','; 

            String dataItem = String.Empty;
            List<String> dataList = new List<String>();

            for (Int32 c = 0; c < data.Length; c++)
            {
                char ch = data[c];

                if (ch == doubleQuote) // char starts with double quote continue to end of the correct double quote for complete data
                {
                    if (dataItem.Length > 0)
                    {
                        dataList.Add(dataItem);
                    }

                    c++;
                    Boolean completeDataItem = false;
                    while (!completeDataItem && c < data.Length)
                    {
                        if (data[c] == comma)
                        {
                            completeDataItem = IsCorrectComma(data, c);
                        }
                        if (!completeDataItem)
                        {
                            dataItem += data[c];
                            c++;
                        }
                    }

                    if (dataItem.Length > 0)
                    {
                        dataItem = dataItem.Substring(0, dataItem.Length - 1);
                    }

                    //remove any break characters from the data item
                    //NOTE: The removal of break chars infront of back slashes must
                    //always come last.
                    dataItem = dataItem.Replace(@"\""", @"""").Replace(@"\\", @"\");

                    dataList.Add(dataItem);
                    dataItem = String.Empty;
                }
                else
                {
                    if (ch != comma) // ignore commas
                    {
                        dataItem += ch;
                    }
                    else
                    {
                        dataList.Add(dataItem);
                        dataItem = String.Empty;
                    }
                }
            }

            if (dataItem.Length > 0)
            {
                dataList.Add(dataItem);
            }
            return dataList;
        }

        /// <summary>
        /// Determines whether a comma found in a comma-separated string actually represents the next 
        /// separating comma or just a comma in the string being read.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        private static Boolean IsCorrectComma(string data, int c)
        {
            const Char doubleQuote = '"';

            if (c <= 2) return true;
            Boolean returnVal = false;

            // If we did find a comma, it's the right comma if it has a
            // double quote right before it that is not escaped.

            if (!IsBreakCharacter(data, c - 2))
            {
                returnVal = (data.Substring(c - 1, 1)[0] == doubleQuote);
            }
            return returnVal;
        }

        /// <summary>
        /// Check to see if the char at index c is a break character 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        private static Boolean IsBreakCharacter(string data, int c)
        {
            Int32 breakCount = 0;
            //make a count of how many break characters we
            //have in a consecutive streak
            for (Int32 x = c; x > 0; x--)
            {
                //increment if we are a break char
                if (data[x] == '\\')
                {
                    breakCount++;
                }
                else
                {
                    break;
                }
            }

            //If we have an odd number of break counts
            //then the character at index c is a break
            //char. Otherwise it is either not
            //a break char or it is a backslash
            return breakCount % 2 != 0;
        }

        /// <summary>
        /// Reads complete section data
        /// </summary>
        /// <param name="textReader">The text reader</param>
        /// <returns></returns>
        private String ReadCompleteLineData(TextReader textReader)
        {
            String line = textReader.ReadLine();
            String completeLine = String.Empty;

            while (line.EndsWith("&", StringComparison.InvariantCultureIgnoreCase))
            {
                //If the file being read in is not created in the same language format as
                //the current language then we could end up with some odd characters e.g.
                //蓋 can become »\ which contains a break character that is not a break
                //character. This can't be handled later and must be removed now.
                if (line.EndsWith("\\\"&", StringComparison.InvariantCultureIgnoreCase) &&
                    !line.EndsWith("\\\\\"&", StringComparison.InvariantCultureIgnoreCase))
                {
                    completeLine += line.Substring(0, line.Length - 3) + "\",";  // remove three chars '\"&' and replace with '",'
                }
                else
                {
                    completeLine += line.TrimEnd(line[line.Length - 1]) + ","; // remove last char '&' and replace with ','
                }
                line = textReader.ReadLine();
            }

            completeLine += line; 
            
            return completeLine;
        }

        /// <summary>
        /// Returns the next id
        /// </summary>
        private static String GetNextId()
        {
            lock (_nextIdLock)
            {
                _nextId++;
                return _nextId.ToString();
            }
        }

        private void MapCustomAttributes(Type p, object dto, IEnumerable<PlanogramFieldMappingDto> fieldMappings, String[] data, Dictionary<String, SpacemanSchemaItem> schema, CustomAttributeDataPlanogramParentType customAttributeDataPlanogramParentType, object id)
        {
            CustomAttributeDataDto customAttributes = null;
            DalCacheItemType itemType;

            switch (customAttributeDataPlanogramParentType)
            {
                case CustomAttributeDataPlanogramParentType.Planogram:
                    itemType = DalCacheItemType.Section;
                    break;
                case CustomAttributeDataPlanogramParentType.PlanogramProduct:
                    itemType = DalCacheItemType.Product;
                    break;
                case CustomAttributeDataPlanogramParentType.PlanogramComponent:
                    itemType = DalCacheItemType.Fixel;
                    break;
                default:
                    itemType = DalCacheItemType.Section;
                    break;
            }

            foreach (PlanogramFieldMappingDto mapping in fieldMappings)
            {
                PropertyInfo property = p.GetProperty(mapping.Target);
                if (property != null)
                {
                    MapSetValue(dto, data, schema, itemType, mapping.Source, mapping.Target, property);
                }
                else
                {
                    if (mapping.Target.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                    {
                        String[] columnMapping = mapping.Target.Split('.');

                        if (columnMapping.Any())
                        {
                            property = typeof(CustomAttributeDataDto).GetProperty(columnMapping[1]);
                            if (property != null)
                            {
                                if (customAttributes == null)
                                {
                                    customAttributes = new CustomAttributeDataDto()
                                    {
                                        Id = IdentityHelper.GetNextInt32(),
                                        ParentType = (Byte)customAttributeDataPlanogramParentType,
                                        ParentId = id
                                    };
                                }

                                MapSetValue(customAttributes, data, schema, itemType, mapping.Source, mapping.Target, property);
                            }
                        }
                    }
                }
            }

            if (customAttributes != null)
            {
                _dalCache.CustomAttributeDataDtoList.Add(customAttributes);
            }
        }

        private void MapSetValue(Object dto, IList<String> data, IDictionary<String,SpacemanSchemaItem> schema, DalCacheItemType itemType, String source, String target, PropertyInfo property)
        {
            try
            {
                String stringValue = data[schema[source].fieldIndex];
                Boolean success;
                Object value = SpacePlanningImportHelper.ParseValue(stringValue, property.PropertyType, out success);

                if (!success)
                {
                    CreateErrorEventLogEntry(String.Format(Language.CustomMappingError, source, target),
                                             String.Format(Language.CustomMappingError_Description, source, target, stringValue),
                                             PlanogramEventLogEntryType.Warning,
                                             PlanogramEventLogEventType.Import,
                                             itemType);
                }

                property.SetValue(dto, value, null);
            }
            catch
            {
                CreateErrorEventLogEntry(String.Format(Language.CustomMappingError, source, target), String.Format(Language.CustomMappingError_Description, source, target, null), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
        }

        private void ValidateSegmentsCreated(List<Int16> segments, List<PlanogramFixtureDto> bays, List<PlanogramFixtureComponentDto> bayComponents, Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups, List<PlanogramFixtureItemDto> bayPositions,
                                        String[] sectionData, Dictionary<String,SpacemanSchemaItem> sectionSchema, Boolean hasBase)
        {
            foreach (Int16 segment in segments)
            {
                PlanogramFixtureDto planogramFixtureDto = bays.Where(p => p.Id.Equals(segment.ToString())).FirstOrDefault();

                // segemnt is a peg in Spaceman, create the fixture item
                if (planogramFixtureDto == null)
                {
                    PlanogramComponentDto planogramComponentDto = null;
                    PlanogramSubComponentDto planogramSubComponentDto = null;
                    PlanogramFixtureComponentDto segmentFixtureComponentDto = null;

                    foreach (PlanogramFixtureComponentDto planogramFixtureComponentDto in bayComponents.Where(p => p.PlanogramFixtureId.Equals(segment.ToString())))
                    {
                        object fixelLookupItem = null;

                        if (fixelLookups.ContainsKey(planogramFixtureComponentDto.Id.ToString()))
                        {
                            fixelLookups[planogramFixtureComponentDto.Id.ToString()].TryGetValue(PlanogramFixtureType.Component, out fixelLookupItem);
                            planogramComponentDto = (PlanogramComponentDto)fixelLookupItem;

                            fixelLookups[planogramFixtureComponentDto.Id.ToString()].TryGetValue(PlanogramFixtureType.SubComponent, out fixelLookupItem);
                            planogramSubComponentDto = (PlanogramSubComponentDto)fixelLookupItem;
                        }
                        if (planogramComponentDto != null)
                        {
                            if (planogramComponentDto.ComponentType == 3 && planogramFixtureComponentDto.Z < 0)
                            {
                                DalCacheItem FixelItem = this.Children[DalCacheItemType.Fixel].Where(p => p.Id == planogramComponentDto.Id.ToString()).FirstOrDefault();

                                PlanogramFixtureItemDto planogramFixtureItemDto = new PlanogramFixtureItemDto()
                                {
                                    Id = segment.ToString(),
                                    PlanogramId = 1,
                                    PlanogramFixtureId = segment.ToString(),
                                    X = planogramFixtureComponentDto.X,
                                    Y = planogramFixtureComponentDto.Y,
                                    Z = 0,
                                    Angle = planogramFixtureComponentDto.Angle,
                                    Slope = planogramFixtureComponentDto.Slope,
                                    BaySequenceNumber = segment
                                };

                                ((List<object>)FixelItem.Dto).Add(planogramFixtureItemDto);
                                bayPositions.Add(planogramFixtureItemDto);

                                Single depth = (planogramComponentDto.Depth / 2);
                                
                                ((List<object>)FixelItem.Dto).Add(new PlanogramFixtureDto()
                                {
                                    Id = segment.ToString(),
                                    PlanogramId = 1,
                                    Name = planogramSubComponentDto.Name,
                                    Height = planogramComponentDto.Height,
                                    Width = planogramComponentDto.Width,
                                    Depth = depth
                                });

                                segmentFixtureComponentDto = new PlanogramFixtureComponentDto()
                                {
                                    Id = null,
                                    PlanogramComponentId = null,
                                    PlanogramFixtureId = segment.ToString(),
                                    X = planogramFixtureComponentDto.X,
                                    Y = 0,
                                    Z = planogramFixtureComponentDto.Z,
                                    Slope = planogramFixtureComponentDto.Slope,
                                    Angle = planogramFixtureComponentDto.Angle,
                                    Roll = 0
                                };

                                ((List<object>)FixelItem.Dto).Add(segmentFixtureComponentDto);

                                ((List<object>)FixelItem.Dto).Add(new PlanogramComponentDto()
                                {
                                    Id = null,
                                    PlanogramId = 1,
                                    Name = planogramComponentDto.Name,
                                    Height = planogramComponentDto.Height + GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                                    Width = planogramComponentDto.Width,
                                    Depth = (planogramComponentDto.Depth / 2),
                                    ComponentType = (Byte)PlanogramComponentType.Backboard,
                                });

                                ((List<object>)FixelItem.Dto).Add(new PlanogramSubComponentDto()
                                {
                                    Id = null,
                                    PlanogramComponentId = null,
                                    Name = planogramSubComponentDto.Name,
                                    ShapeType = (Byte)PlanogramSubComponentShapeType.Box,
                                    Height = planogramSubComponentDto.Height + GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                                    Width = planogramSubComponentDto.Width,
                                    Depth = (planogramSubComponentDto.Depth / 2),
                                    MerchandisableHeight = 0,
                                    FillPatternTypeFront = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillPatternTypeBack = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillPatternTypeTop = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillPatternTypeBottom = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillPatternTypeLeft = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillPatternTypeRight = (Byte)PlanogramSubComponentFillPatternType.Solid,
                                    FillColourFront = -2894893,
                                    FillColourBack = -2894893,
                                    FillColourTop = -2894893,
                                    FillColourBottom = -2894893,
                                    FillColourLeft = -2894893,
                                    FillColourRight = -2894893,
                                    LineColour = -16777216,
                                    LineThickness = 0.3F,
                                    NotchStartX = 0,
                                    NotchSpacingX = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchSpacing, DalCacheItemType.Section),
                                    NotchStartY = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchStart, DalCacheItemType.Section) + GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                                    NotchSpacingY = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchSpacing, DalCacheItemType.Section),
                                    NotchHeight = 0.1F,
                                    NotchWidth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchWidth, DalCacheItemType.Section),
                                    IsNotchPlacedOnFront = (GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionNotchWidth, DalCacheItemType.Section) > 0),
                                    NotchStyleType = (Byte)PlanogramSubComponentNotchStyleType.SingleRectangle,
                                    MerchandisingType = (Byte)PlanogramSubComponentMerchandisingType.None
                                });

                                if (hasBase)
                                {
                                    ((List<object>)FixelItem.Dto).Add(new PlanogramFixtureComponentDto()
                                    {
                                        Id = null,
                                        PlanogramComponentId = null,
                                        PlanogramFixtureId = segment.ToString(),
                                        X = planogramFixtureComponentDto.X, 
                                        Y = 0,
                                        Z = (planogramFixtureComponentDto.Z / 2),
                                        Slope = 0,
                                        Angle = 0,
                                        Roll = 0
                                    });

                                    ((List<object>)FixelItem.Dto).Add(new PlanogramComponentDto()
                                    {
                                        Id = null,
                                        PlanogramId = 1,
                                        Name = planogramComponentDto.Name + segment,
                                        Height = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                                        Width = planogramComponentDto.Width,
                                        Depth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section),
                                        ComponentType = (Byte)PlanogramComponentType.Base
                                    });

                                    ((List<object>)FixelItem.Dto).Add(new PlanogramSubComponentDto()
                                    {
                                        Id = null,
                                        PlanogramComponentId = null,
                                        Name = planogramSubComponentDto.Name,
                                        Height = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseHeight, DalCacheItemType.Section),
                                        Width = planogramSubComponentDto.Width,
                                        Depth = GetSingleValue(sectionData, sectionSchema, SpacemanFieldHelper.SectionBaseDepth, DalCacheItemType.Section),
                                        X = 0,
                                        Y = 0,
                                        Z = 0,
                                        FillColourFront = -5658199,
                                        FillColourBack = -5658199,
                                        FillColourTop = -5658199,
                                        FillColourBottom = -5658199,
                                        FillColourLeft = -5658199,
                                        FillColourRight = -5658199,
                                        LineColour = -16777216,
                                        LineThickness = 0.3F,
                                        MerchandisingType = (Byte)PlanogramSubComponentMerchandisingType.None
                                    });
                                }

                                planogramFixtureComponentDto.Z += (planogramComponentDto.Depth / 2);
                                planogramComponentDto.Depth = (planogramComponentDto.Depth / 2);
                                planogramSubComponentDto.Depth = (planogramSubComponentDto.Depth / 2);
                                break;
                            }
                        }
                    }

                    if (segmentFixtureComponentDto != null)
                    {
                        bayComponents.Add(segmentFixtureComponentDto);
                    }
                }
            }
        }

        /// <summary>
        /// Missing identifiers are created when the entire plan is loaded so we can ensure a unique identity.
        /// </summary>
        /// <param name="fixelLookups"></param>
        private void CreateMissingFixelIdentifiers(Dictionary<string, Dictionary<PlanogramFixtureType, object>> fixelLookups)
        {
            List<String> keys = fixelLookups.Keys.ToList();
            foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
            {
                PlanogramFixtureComponentDto planogramFixtureComponentDto = null;
                PlanogramComponentDto planogramComponentDto = null;
                PlanogramSubComponentDto planogramSubComponentDto = null;
                foreach (object fixelDto in (List<object>)fixelItem.Dto)
                {
                    if (fixelDto.GetType() == typeof(PlanogramFixtureComponentDto))
                    {
                        planogramFixtureComponentDto = (PlanogramFixtureComponentDto)fixelDto;
                    }
                    if (planogramFixtureComponentDto != null && planogramFixtureComponentDto.Id == null)
                    {
                        if (fixelDto.GetType() == typeof(PlanogramComponentDto))
                        {
                            planogramComponentDto = (PlanogramComponentDto)fixelDto;
                        }
                        if (fixelDto.GetType() == typeof(PlanogramSubComponentDto))
                        {
                            planogramSubComponentDto = (PlanogramSubComponentDto)fixelDto;
                        }
                        if (planogramComponentDto != null && planogramSubComponentDto != null)
                        {
                            Int32 uniqueKey = 1;
                            String newIdentifier = String.Format("{0}{1}{2}", planogramFixtureComponentDto.PlanogramFixtureId, "~", uniqueKey);

                            while (keys.Contains(newIdentifier)) // ensure the idenfifier is unique
                            {
                                uniqueKey++;
                                newIdentifier = String.Format("{0}{1}{2}", planogramFixtureComponentDto.PlanogramFixtureId, "~", uniqueKey);
                            }
                            // update references
                            planogramFixtureComponentDto.Id = newIdentifier;
                            planogramFixtureComponentDto.PlanogramComponentId = newIdentifier;
                            planogramComponentDto.Id = newIdentifier;
                            planogramSubComponentDto.Id = newIdentifier;
                            planogramSubComponentDto.PlanogramComponentId = newIdentifier;

                            keys.Add(newIdentifier);
                            planogramComponentDto = null;
                            planogramSubComponentDto = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Applies the rotations relative to the segment they belong
        /// </summary>
        /// <param name="bayPositions">The fixture items</param>
        /// <param name="bayComponents">The fixture components</param>
        private void ApplyRelativeSegmentRotation(List<PlanogramFixtureItemDto> bayPositions, List<PlanogramFixtureComponentDto> bayComponents)
        {
            foreach (PlanogramFixtureItemDto planogramFixtureItemDto in bayPositions)
            {
                if (planogramFixtureItemDto.Angle > 0)
                {
                    foreach (PlanogramFixtureComponentDto planogramFixtureComponentDto in bayComponents.Where(p => p.PlanogramFixtureId.Equals(planogramFixtureItemDto.PlanogramFixtureId)))
                    {
                        // calculate fixture component items relative to fixture
                        planogramFixtureComponentDto.Angle = ConvertToRadians((planogramFixtureItemDto.Angle - planogramFixtureComponentDto.Angle));
                    }

                    planogramFixtureItemDto.Angle = ConvertToRadians((360 - planogramFixtureItemDto.Angle));
                }
            }
        }

        /// <summary>
        /// calculate relative x positions (now all fixel data has been loaded we can update fixture component x,z positions relative to there fixture items)
        /// </summary>
        /// <param name="bayPositions">The fixture items</param>
        /// <param name="frameDetails"></param>
        private void CalculateFixtureComponentXAndZpositions(List<PlanogramFixtureItemDto> bayPositions)
        {
            foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
            {
                foreach (object fixelDto in (List<object>)fixelItem.Dto)
                {
                    if (fixelDto.GetType() == typeof(PlanogramFixtureComponentDto))
                    {
                        PlanogramFixtureComponentDto planogramFixtureComponentDto = (PlanogramFixtureComponentDto)fixelDto;
                        PlanogramFixtureItemDto planogramFixtureItemDto = bayPositions.Where(p => p.Id.Equals(planogramFixtureComponentDto.PlanogramFixtureId)).FirstOrDefault();

                        if (planogramFixtureItemDto != null)
                        {
                            planogramFixtureComponentDto.X = planogramFixtureComponentDto.X - planogramFixtureItemDto.X;
                            //planogramFixtureComponentDto.Z = planogramFixtureComponentDto.Z - planogramFixtureItemDto.Z;
                        }
                    }
                    if (fixelDto.GetType() == typeof(PlanogramAnnotationDto))
                    {
                        PlanogramAnnotationDto planogramAnnotationDto = (PlanogramAnnotationDto)fixelDto;
                        PlanogramFixtureItemDto planogramFixtureItemDto = bayPositions.Where(p => p.Id.Equals(planogramAnnotationDto.PlanogramFixtureItemId)).FirstOrDefault();

                        if (planogramFixtureItemDto != null)
                        {
                            planogramAnnotationDto.X = planogramAnnotationDto.X - planogramFixtureItemDto.X;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Applies frame data to fixture and fixture components. (When frames are applied the segment data doesnt doesnt include the 
        /// frame detail so apply here, reset start x position of the segment and also apply the frame widths to the segment)
        /// </summary>
        /// <param name="frameDetails">The frame details</param>
        private void ApplyFixtureFrameDetails(List<Tuple<String, Int16, Single, Single, Single, Single>> frameDetails)
        {
            foreach (Tuple<String, Int16, Single, Single, Single, Single> frameDetail in frameDetails.OrderBy(p => p.Item2).OrderBy(p => p.Item3))
            {
                foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
                {
                    PlanogramFixtureItemDto planogramFixtureItemDto = null;
                    PlanogramComponentDto planogramComponentDto = null;
                    foreach (object fixelDto in (List<object>)fixelItem.Dto)
                    {
                        if (fixelDto.GetType() == typeof(PlanogramFixtureItemDto))
                        {
                            planogramFixtureItemDto = (PlanogramFixtureItemDto)fixelDto;
                        }

                        if (fixelDto.GetType() == typeof(PlanogramComponentDto))
                        {
                            planogramComponentDto = (PlanogramComponentDto)fixelDto;
                        }
                    }
                    if (planogramFixtureItemDto != null && planogramFixtureItemDto.PlanogramFixtureId.Equals(frameDetail.Item2.ToString()) && (planogramComponentDto.ComponentType == 5 || planogramComponentDto.ComponentType == 7))
                    {
                        foreach (object fixelDto in (List<object>)fixelItem.Dto)
                        {
                            if (fixelDto.GetType() == typeof(PlanogramFixtureItemDto))
                            {
                                if (((PlanogramFixtureItemDto)fixelDto).X > frameDetail.Item3)
                                {
                                    ((PlanogramFixtureItemDto)fixelDto).X = frameDetail.Item3;
                                }
                            }
                            if (fixelDto.GetType() == typeof(PlanogramFixtureDto))
                            {
                                ((PlanogramFixtureDto)fixelDto).Width += frameDetail.Item4;
                            }
                            if (fixelDto.GetType() == typeof(PlanogramFixtureComponentDto))
                            {
                                if (((PlanogramFixtureComponentDto)fixelDto).X > frameDetail.Item3)
                                {
                                    ((PlanogramFixtureComponentDto)fixelDto).X = frameDetail.Item3;
                                }
                            }
                            if (fixelDto.GetType() == typeof(PlanogramComponentDto))
                            {
                                ((PlanogramComponentDto)fixelDto).Width += frameDetail.Item4;
                            }
                            if (fixelDto.GetType() == typeof(PlanogramSubComponentDto))
                            {
                                PlanogramSubComponentDto planogramSubComponentDto = (PlanogramSubComponentDto)fixelDto;

                                planogramSubComponentDto.Width += frameDetail.Item4;

                                if (frameDetail.Item6 > 0) // specific notch detail is obtained from frame detail
                                {
                                    planogramSubComponentDto.NotchSpacingX = frameDetail.Item6;
                                    planogramSubComponentDto.NotchStartY = frameDetail.Item5;
                                    planogramSubComponentDto.NotchSpacingY = frameDetail.Item6;
                                    planogramSubComponentDto.NotchWidth = frameDetail.Item4;
                                    if (planogramSubComponentDto.NotchWidth > 0) { planogramSubComponentDto.IsNotchPlacedOnFront = true; }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Creates an element position lookup.
        /// </summary>
        /// <param name="positions">The plans positions</param>
        /// <param name="schema">The position schema</param>
        /// <param name="productLookup">The product lookup dictionary</param>
        /// <returns>A dictionary of elements position details</returns>
        private Dictionary<String, List<ElementPositionDetail>> CreateElementPositionLookups(List<DalCacheItem> positions, Dictionary<String,SpacemanSchemaItem> schema, Dictionary<String, PlanogramProductDto> productLookup)
        {
            Dictionary<String, List<ElementPositionDetail>> returnVal = new Dictionary<String, List<ElementPositionDetail>>();

            foreach (DalCacheItem position in positions)
            {
                String[] fields = GetData(position.Data).ToArray();
                String positionFixtureLink = GetValue(fields, schema, SpacemanFieldHelper.PositionFIXLToPOS, DalCacheItemType.Position);
                String positionProductLink = GetValue(fields, schema, SpacemanFieldHelper.PositionPRODToPOS, DalCacheItemType.Position);

                List<ElementPositionDetail> elementPositionDetails = null;
                returnVal.TryGetValue(positionFixtureLink, out elementPositionDetails);

                if (elementPositionDetails == null)
                {
                    elementPositionDetails = new List<ElementPositionDetail>();
                    returnVal.Add(positionFixtureLink, elementPositionDetails);
                }

                PlanogramProductDto planogramProductDto = null;
                productLookup.TryGetValue(positionProductLink, out planogramProductDto);

                if (planogramProductDto != null)
                {
                    elementPositionDetails.Add(new ElementPositionDetail()
                    {
                        ProductHeight = planogramProductDto.Height
                    });
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Creates a dictionary of shelf merchandising heights, calculates the merchandising height if not already applied (required for calculating cappings based upon cap style)
        /// </summary>
        /// <param name="bays">The fixture items</param>
        /// <param name="bayComponents">The fixture components</param>
        /// <param name="fixelLookups">The fixture lookups</param>
        /// <param name="elementPositionLookupDetail">The element position lookups</param>
        /// <returns>A dictionary of shelf merchandising heights</returns>
        private Dictionary<String, Single> CalculateShelfMerchandisingArea(List<PlanogramFixtureItemDto> bays, List<PlanogramFixtureComponentDto> bayComponents,
                                    Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups, Dictionary<String, List<ElementPositionDetail>> elementPositionLookupDetail)
        {
            Dictionary<String, Single> subComponentMerchandisingArea = new Dictionary<String, Single>();

            foreach (PlanogramFixtureItemDto bay in bays.OrderBy(p => p.Id))
            {
                PlanogramFixtureDto planogramFixtureDto = null;

                // Get fixture detail
                foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
                {
                    PlanogramFixtureDto planogramFixDto = null;

                    foreach (object fixelDto in (List<object>)fixelItem.Dto)
                    {
                        if (fixelDto.GetType() == typeof(PlanogramFixtureDto))
                        {
                            planogramFixDto = (PlanogramFixtureDto)fixelDto;
                            if (planogramFixDto.Id.Equals(bay.PlanogramFixtureId)) { planogramFixtureDto = planogramFixDto; }
                        }
                    }
                    if (planogramFixtureDto != null) { break; }
                }

                //obtain the bay components that intersect the bay position, a component may span multiple bays
                List<PlanogramFixtureComponentDto> selectedBayComponents = bayComponents.Where(p => p.PlanogramFixtureId.Equals(bay.Id)).ToList();
                foreach (PlanogramFixtureComponentDto fixtureComponent in bayComponents)
                {
                    object fixelLookupItem = null;
                    PlanogramComponentDto planogramComponentDto = null;

                    if (!selectedBayComponents.Contains(fixtureComponent))
                    {
                        if (fixelLookups.ContainsKey(fixtureComponent.Id.ToString()))
                        {
                            fixelLookups[fixtureComponent.Id.ToString()].TryGetValue(PlanogramFixtureType.Component, out fixelLookupItem);
                            planogramComponentDto = (PlanogramComponentDto)fixelLookupItem;
                        }

                        if (planogramComponentDto != null)
                        {
                            if (fixtureComponent.X.GreaterOrEqualThan(bay.X) && (fixtureComponent.X + planogramComponentDto.Width).LessOrEqualThan(bay.X + planogramFixtureDto.Width))
                            {
                                // within bounds of bay
                                selectedBayComponents.Add(fixtureComponent);
                            }
                            else if (fixtureComponent.X.LessOrEqualThan(bay.X) &&
                                ((fixtureComponent.X + planogramComponentDto.Width).GreaterThan(bay.X) || (fixtureComponent.X + planogramComponentDto.Width).GreaterThan(bay.X + planogramFixtureDto.Width)))
                            {
                                // spans into bay or spans the entire bay
                                selectedBayComponents.Add(fixtureComponent);
                            }
                            else if (fixtureComponent.X.GreaterOrEqualThan(bay.X) && (bay.X + planogramFixtureDto.Width).GreaterThan(fixtureComponent.X))
                            {
                                // start of component is within bay boundry
                                selectedBayComponents.Add(fixtureComponent);
                            }
                        }
                    }
                }

                // calculate merch heights for components associated with the bay but include all possible bay components when checking for intersects
                foreach (PlanogramFixtureComponentDto fixtureComponent in bayComponents.Where(p=> p.PlanogramFixtureId.Equals(bay.Id)).OrderBy(p => p.Y))
                {
                    object fixelLookupItem = null;
                    PlanogramComponentDto planogramComponentDto = null;
                    PlanogramSubComponentDto planogramSubComponentDto = null;

                    if (fixelLookups.ContainsKey(fixtureComponent.Id.ToString()))
                    {
                        fixelLookups[fixtureComponent.Id.ToString()].TryGetValue(PlanogramFixtureType.Component, out fixelLookupItem);
                        planogramComponentDto = (PlanogramComponentDto)fixelLookupItem;

                        fixelLookups[fixtureComponent.Id.ToString()].TryGetValue(PlanogramFixtureType.SubComponent, out fixelLookupItem);
                        planogramSubComponentDto = (PlanogramSubComponentDto)fixelLookupItem;
                    }

                    if (planogramComponentDto != null)
                    {
                        if (planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Shelf)
                        {
                            // find next fixture component that intersects fixture component
                            PlanogramFixtureComponentDto nextFixtureComponent = selectedBayComponents.Where(p => p.Y.GreaterThan(fixtureComponent.Y)
                                            && p.Z.LessThan(fixtureComponent.Z + planogramComponentDto.Depth) && (p.Z + planogramComponentDto.Depth).GreaterThan(fixtureComponent.Z)).OrderBy(p => p.Y).FirstOrDefault();

                            Single merchandisingHeight = 0;

                            if (nextFixtureComponent != null)
                            {
                                PlanogramComponentDto nextPlanogramComponentDto = null;
                                // lookup next component type to obtain what type the next element is
                                fixelLookups[nextFixtureComponent.Id.ToString()].TryGetValue(PlanogramFixtureType.Component, out fixelLookupItem);
                                nextPlanogramComponentDto = (PlanogramComponentDto)fixelLookupItem;
                                Single hangingProducts = 0;

                                if (nextPlanogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Bar)
                                {
                                    if (elementPositionLookupDetail.ContainsKey(nextFixtureComponent.Id.ToString()))
                                    {
                                        // get the largest hanging product, factoring in the height of the bar, hangs from top
                                        hangingProducts = elementPositionLookupDetail[nextFixtureComponent.Id.ToString()].Max(p => p.ProductHeight) - nextPlanogramComponentDto.Height;
                                    }                                    
                                }

                                merchandisingHeight = (nextFixtureComponent.Y - hangingProducts)  - (fixtureComponent.Y + planogramComponentDto.Height);
                                                                
                                // to merch height or next element start
                                if (planogramSubComponentDto.MerchandisableHeight.GreaterThan(0) && (planogramSubComponentDto.MerchandisableHeight.LessThan(merchandisingHeight)))
                                {
                                    merchandisingHeight = planogramSubComponentDto.MerchandisableHeight;
                                }                                
                            }
                            else
                            {
                                if (planogramSubComponentDto.MerchandisableHeight.GreaterThan(0))
                                {
                                    merchandisingHeight = planogramSubComponentDto.MerchandisableHeight;
                                }
                                else
                                {
                                    // to merch height \ top of fixture
                                    merchandisingHeight = planogramFixtureDto.Height - (fixtureComponent.Y + planogramComponentDto.Height);
                                }
                            }
                            if (!subComponentMerchandisingArea.ContainsKey(fixtureComponent.Id.ToString()))
                            {
                                subComponentMerchandisingArea.Add(fixtureComponent.Id.ToString(), merchandisingHeight);
                            }
                            else
                            {
                                if (subComponentMerchandisingArea[fixtureComponent.Id.ToString()].GreaterThan(merchandisingHeight))
                                {
                                    subComponentMerchandisingArea[fixtureComponent.Id.ToString()] = merchandisingHeight;
                                }
                            }
                        }
                        else if (planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Peg ||
                                    planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Bar)
                        {
                            subComponentMerchandisingArea.Add(fixtureComponent.Id.ToString(), planogramSubComponentDto.MerchandisableHeight);
                        }
                    }
                }
            }

            return subComponentMerchandisingArea;
        }

        private void CalculatePositionSequence(Dictionary<String, Dictionary<PlanogramFixtureType, object>> fixelLookups, List<PlanogramPositionDto> positions)
        {
            foreach (var fixelItem in this.Children[DalCacheItemType.Fixel])
            {
                foreach (object fixelDto in (List<object>)fixelItem.Dto)
                {
                    if (fixelDto.GetType() == typeof(PlanogramComponentDto))
                    {
                        PlanogramComponentDto planogramComponentDto = (PlanogramComponentDto)fixelDto;

                        if (planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Shelf ||
                            planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Bar ||
                            planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Chest ||
                            planogramComponentDto.ComponentType == (Byte)PlanogramComponentType.Pallet)
                        {
                            Int16 sequence = 1;
                            foreach (PlanogramPositionDto planogramPositionDto in positions.Where(p => p.PlanogramFixtureComponentId.Equals(planogramComponentDto.Id)).OrderBy(p => p.X))
                            {
                                planogramPositionDto.Sequence = sequence;
                                planogramPositionDto.SequenceX = sequence;
                                sequence++;
                            }

                            // V8-29557 : Commented out currently but may come in again when Component type Chest \ Peg are changed from manual strategy
                            //var subPositions = positions.Where(p => p.PlanogramFixtureComponentId.Equals(planogramComponentDto.Id)).ToList();

                            //Int16 seqX = 1;
                            //foreach (var group in subPositions.OrderBy(p => p.X).GroupBy(p => MathHelper.RoundToNearestIncrement(p.X, 1)))
                            //{
                            //    foreach (var pos in group)
                            //    {
                            //        pos.SequenceX = seqX;
                            //    }
                            //    seqX++;
                            //}

                            //Int16 seqY = 1;
                            //foreach (var group in subPositions.OrderBy(p => p.Y).GroupBy(p => MathHelper.RoundToNearestIncrement(p.Y, 1)))
                            //{
                            //    foreach (var pos in group)
                            //    {
                            //        pos.SequenceY = seqY;
                            //    }
                            //    seqY++;
                            //}

                            //Int16 seqZ = 1;
                            //foreach (var group in subPositions.OrderBy(p => p.Z).GroupBy(p => MathHelper.RoundToNearestIncrement(p.Z, 1)))
                            //{
                            //    foreach (var pos in group)
                            //    {
                            //        pos.SequenceZ = seqZ;
                            //    }
                            //    seqZ++;
                            //}

                            //IOrderedEnumerable<PlanogramPositionDto> orderedPositions;

                            //orderedPositions = subPositions.OrderBy(p => p.SequenceX);
                            //orderedPositions = orderedPositions.ThenBy(p => p.SequenceY);
                            //orderedPositions = orderedPositions.ThenBy(p => p.SequenceZ);

                            //Int16 seq = 1;
                            //orderedPositions.ToList().ForEach(P => P.Sequence = seq++); 
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Converts spaceman colour to CCM colour
        /// </summary>
        /// <param name="intValue">The colour value</param>
        /// <returns>CCM Colour</returns>
        private static Int32 ConvertColour(Int32 intValue)
        {
            const Int32 cTransparentColour = 16777215;
            if (intValue == -1 )
            { 
                return cTransparentColour; 
            }

            Byte b = (Byte)((intValue >> 0x10) & 0xff);
            Byte g = (Byte)((intValue >> 8) & 0xff);
            Byte r = (Byte)(intValue & 0xff);
            Byte a = (Byte)((intValue >> 0x18) & 0xff);

            if (a == 0) { a = 255; }

            var uint32Color = (((UInt32)a) << 24) |
                    (((UInt32)r) << 16) |
                    (((UInt32)g) << 8) |
                    (UInt32)b;

            return (Int32)uint32Color;
        }
        
        private static Single ConvertToRadians(Single degrees)
        {
            Single d = degrees;
            IFormatProvider prov = CultureInfo.InvariantCulture;
            
            if (d > 360 || degrees < -360)
            {
                d = d % 360;
            }

            if (d < 0)
            {
                d = 360 + d;
            }

            return System.Convert.ToSingle(d * (Math.PI / 180f), prov);
        }

        private String GetValue(String[] fields, Dictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            if (schema.ContainsKey(schemaKey))
            {
                if ((fields.Count() - 1) >= schema[schemaKey].fieldIndex)
                {
                    String returnVal = fields[schema[schemaKey].fieldIndex];
                    if (returnVal.Length > 0) { returnVal = returnVal.Trim(); }
                    return returnVal;
                }
                else
                {
                    CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType.ToString(), schemaKey), String.Format(Language.UnableToReadData_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                }
            }
            else
            {
                // report missing key
                CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType.ToString(), schemaKey), String.Format(Language.MissingKeyError_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
            return null;
        }

        private Byte GetByteValue(String[] fields, Dictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            if (schema.ContainsKey(schemaKey))
            {
                if ((fields.Count() - 1) >= schema[schemaKey].fieldIndex)
                {
                    try
                    {
                        return Convert.ToByte(fields[schema[schemaKey].fieldIndex], prov);
                    }
                    catch
                    {
                        // report failed conversion
                        CreateErrorEventLogEntry(String.Format(Language.ConversionError, itemType.ToString(), schemaKey), String.Format(Language.ConversionError_Description, itemType.ToString(), schemaKey, fields[schema[schemaKey].fieldIndex]), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                    }
                }
                else
                {
                    CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType.ToString(), schemaKey), String.Format(Language.UnableToReadData_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                }
            }
            else
            {
                // report missing key
                CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType.ToString(), schemaKey), String.Format(Language.MissingKeyError_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
            return 0;
        }

        private Single GetSingleValue(IList<String> fields, IDictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            if (!CheckSchemaKey(schema, schemaKey, itemType)) return 0;

            Int32 fieldIndex = schema[schemaKey].fieldIndex;
            if (!CheckFieldIndex(fieldIndex, fields, schemaKey, itemType)) return 0;

            String value = fields[fieldIndex];
            Single parsedValue;

            if (Single.TryParse(value, NumberStyles.Float | NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out parsedValue))

                if (parsedValue <= 0.004 && parsedValue > 0)
                    {
                        return 0.01F;
                    }

                return (Single) Math.Round(Math.Truncate(parsedValue*1000)/1000, 2);

            // report failed conversion
            CreateErrorEventLogEntry(
                String.Format(Language.ConversionError, itemType, schemaKey),
                String.Format(Language.ConversionError_Description, itemType, schemaKey, value),
                PlanogramEventLogEntryType.Warning,
                PlanogramEventLogEventType.Import,
                itemType);
            return 0;
        }

        /// <summary>
        ///     Checks that the index of the <paramref name="schemaKey"/> field is within bounds of the provided source <paramref name="fields"/>.
        /// </summary>
        /// <param name="fieldIndex"></param>
        /// <param name="fields"></param>
        /// <param name="schemaKey"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private Boolean CheckFieldIndex(
            Int32 fieldIndex,
            ICollection<String> fields,
            String schemaKey, DalCacheItemType itemType)
        {
            if (fields.Count - 1 >= fieldIndex) return true;

            CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType, schemaKey),
                                     String.Format(Language.UnableToReadData_Description, itemType, schemaKey),
                                     PlanogramEventLogEntryType.Warning,
                                     PlanogramEventLogEventType.Import,
                                     itemType);
            return false;
        }

        /// <summary>
        ///     Checks whether the provided <paramref name="schemaKey"/> exists in the given <paramref name="schema"/>.
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="schemaKey"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private Boolean CheckSchemaKey(
            IDictionary<String, SpacemanSchemaItem> schema,
            String schemaKey,
            DalCacheItemType itemType)
        {
            if (schema.ContainsKey(schemaKey)) return true;

            // report missing key
            CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType, schemaKey),
                                     String.Format(Language.MissingKeyError_Description, itemType, schemaKey),
                                     PlanogramEventLogEntryType.Warning,
                                     PlanogramEventLogEventType.Import,
                                     itemType);
            return false;
        }

        private Int16 GetInt16Value(String[] fields, Dictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            if (schema.ContainsKey(schemaKey))
            {
                if ((fields.Count() - 1) >= schema[schemaKey].fieldIndex)
                {
                    try
                    {
                        return Convert.ToInt16(fields[schema[schemaKey].fieldIndex], prov);
                    }
                    catch
                    {
                        // report failed conversion
                        CreateErrorEventLogEntry(String.Format(Language.ConversionError, itemType.ToString(), schemaKey), String.Format(Language.ConversionError_Description, itemType.ToString(), schemaKey, fields[schema[schemaKey].fieldIndex]), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                    }
                }
                else
                {
                    CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType.ToString(), schemaKey), String.Format(Language.UnableToReadData_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                }
            }
            else
            {
                // report missing key
                CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType.ToString(), schemaKey), String.Format(Language.MissingKeyError_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
            return 0;
        }

        private Int32 GetInt32Value(String[] fields, Dictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            if (schema.ContainsKey(schemaKey))
            {
                if ((fields.Count() - 1) >= schema[schemaKey].fieldIndex)
                {
                    try
                    {
                        return Convert.ToInt32(fields[schema[schemaKey].fieldIndex], prov);
                    }
                    catch
                    {
                        // report failed conversion
                        CreateErrorEventLogEntry(String.Format(Language.ConversionError, itemType.ToString(), schemaKey), String.Format(Language.ConversionError_Description, itemType.ToString(), schemaKey, fields[schema[schemaKey].fieldIndex]), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                    }
                }
                else
                {
                    CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType.ToString(), schemaKey), String.Format(Language.UnableToReadData_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                }
            }
            else
            {
                // report missing key
                CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType.ToString(), schemaKey), String.Format(Language.MissingKeyError_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
            return 0;
        }

        private DateTime? GetDateTimeValue(String[] fields, Dictionary<String, SpacemanSchemaItem> schema, String schemaKey, DalCacheItemType itemType)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            if (schema.ContainsKey(schemaKey))
            {
                if ((fields.Count() - 1) >= schema[schemaKey].fieldIndex)
                {
                    try
                    {
                        return Convert.ToDateTime(fields[schema[schemaKey].fieldIndex], prov);
                    }
                    catch
                    {
                        // report failed conversion
                        CreateErrorEventLogEntry(String.Format(Language.ConversionError, itemType.ToString(), schemaKey), String.Format(Language.ConversionError_Description, itemType.ToString(), schemaKey, fields[schema[schemaKey].fieldIndex]), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                    }
                }
                else
                {
                    CreateErrorEventLogEntry(String.Format(Language.UnableToReadData, itemType.ToString(), schemaKey), String.Format(Language.UnableToReadData_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
                }
            }
            else
            {
                // report missing key
                CreateErrorEventLogEntry(String.Format(Language.MissingKeyError, itemType.ToString(), schemaKey), String.Format(Language.MissingKeyError_Description, itemType.ToString(), schemaKey), PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Import, itemType);
            }
            return null;
        }

        private void CreateErrorEventLogEntry(String description, String content, PlanogramEventLogEntryType entryType, PlanogramEventLogEventType eventType, DalCacheItemType itemType)
        {
            Byte affectedType = 0;

            switch (itemType)
            {
                case DalCacheItemType.Store:
                case DalCacheItemType.Section:
                case DalCacheItemType.Fixel:
                case DalCacheItemType.Font:
                    affectedType = (Byte)PlanogramEventLogAffectedType.Components;
                    break;
                case DalCacheItemType.Position:
                    affectedType = (Byte)PlanogramEventLogAffectedType.Positions;
                    break;
                case DalCacheItemType.Product:
                case DalCacheItemType.Peg:
                    affectedType = (Byte)PlanogramEventLogAffectedType.Products;
                    break;
                default:
                    break;
            }

            if (!_dalCache.PlanogramEventLogDtoList.Where(p => p.Description == description).Any())
            {
                _dalCache.PlanogramEventLogDtoList.Add(new PlanogramEventLogDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramId = 1,
                    EntryType = (Byte)entryType,
                    EventType = (Byte)eventType,
                    AffectedType = affectedType,
                    AffectedId = 1,
                    Description = description,
                    Content = content,
                    DateTime = DateTime.UtcNow
                });
            }
        }

        /// <summary>
        /// Creates the planogram metric mappings
        /// </summary>
        private void CreatePlanogramPerformanceMetrics()
        {
            foreach (PlanogramMetricMappingDto metric in _dalCache.MetricMappings)
            {
                _dalCache.PlanogramPerformanceMetricDtoList.Add(new PlanogramPerformanceMetricDto()
                {
                    Id = IdentityHelper.GetNextInt32(),
                    PlanogramPerformanceId = 1,
                    Name = metric.Name,
                    Description = metric.Description,
                    MetricId = metric.MetricId,
                    Direction = metric.Direction,
                    MetricType = metric.MetricType,
                    SpecialType = metric.SpecialType,
                    AggregationType = metric.AggregationType
                });
            }
        }

        /// <summary>
        /// Builds up list of duplicate product gtins and throws exception
        /// </summary>
        /// <param name="duplicateGtins">The duplicate GTINs</param>
        private void FailPlanWithDuplicateGtinException(List<String> duplicateGtins)
        {
            StringBuilder duplicates = new StringBuilder();

            foreach (String duplicate in duplicateGtins)
            {
                duplicates.AppendFormat("{0}, ", duplicate);
            }

            // Remove trailing comma and space.
            duplicates.Remove(duplicates.Length - 2, 2);

            throw new PackageDuplicateProductGtinException(duplicates.ToString());
        }

        #endregion

        #region Export Helpers

        /// <summary>
        /// Inserts a dto into this dal cache item if the type does not exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dto"></param>
        /// <returns>true on success, false if the dto type is already loaded</returns>
        public Boolean InsertDto<T>(T dto, Object id)
        {
            if (this.Dtos.Any(d => d.Value is T))
            {
                return false;
            }
            else
            {
                this.Dtos.Add(typeof(T), dto);
                this.DtoIds.Add(typeof(T), id);
                return true;
            }
        }

        /// <summary>
        /// Gets the dto of type T if it exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetDto<T>()
        {
            Object objectDto = null;
            if (this.Dtos.TryGetValue(typeof(T), out objectDto))
            {
                return (T)objectDto;
            }
            return default(T);
        }
        public Object GetDtoId<T>()
        {
            Object id = null;
            if (this.DtoIds.TryGetValue(typeof(T), out id))
            {
                return id;
            }
            return null;
        }

        /// <summary>
        /// Gets the dto of type T if it exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetDto<T>(Object id)
        {
            if (id != null)
            {
                foreach (DalCacheItem item in _dalCache.AllCacheItems)
                {
                    Object a = item.GetDtoId<T>();
                    if (Equals(a, id))
                    {
                        return item.GetDto<T>();
                    }
                }
            }

            return default(T);
        }

        /// <summary>
        /// Inserts the dtos from a cache item into this one, ignoring
        /// any types that already exist.
        /// </summary>
        /// <param name="sourceItem"></param>
        public void InsertDtos(DalCacheItem sourceItem)
        {
            foreach (var dtoCopy in sourceItem.Dtos)
            {
                if (!this.Dtos.Any(d => d.Key.Equals(dtoCopy.Key)))
                {
                    Dtos[dtoCopy.Key] = dtoCopy.Value;
                }
            }
            foreach (var dtoCopy in sourceItem.DtoIds)
            {
                if (!this.DtoIds.Any(d => d.Key.Equals(dtoCopy.Key)))
                {
                    DtoIds[dtoCopy.Key] = dtoCopy.Value;
                }
            }
        }

        public virtual String Encode(Dictionary<String, SpacemanSchemaItem> schemaDefinition)
        {
            return String.Empty;
        }

        internal void SetSpacemanFieldValue(String fieldKey, Object value, String[] dataToSet = null)
        {
            SpacemanSchemaItem schemaItem;

            String[] dataToApplyChangesTo = _exportData;
            if (dataToSet != null) dataToApplyChangesTo = dataToSet;
            
            //Get the index to set
            if (_localSchemaDefinition.TryGetValue(fieldKey, out schemaItem))
            {
                if (schemaItem.fieldIndex < dataToApplyChangesTo.Length)
                {
                    //Handle datatypes to string here:
                    if (value is Boolean) value = (Boolean)value ? 1 : 0;
                    if (value is Enum) value = (Int32)value;

                    //Set the index based on data type.
                    switch (schemaItem.fieldType)
                    {
                        case SpacemanFieldType.Id:
                        case SpacemanFieldType.StringId:
                        case SpacemanFieldType.String:
                            String myStringValue = Convert.ToString(value);
                            myStringValue = myStringValue.Replace(@"\", @"\\");
                            //Apparently new lines are too hard for spaceman to do
                            myStringValue = myStringValue.Replace(Environment.NewLine, @" ");
                            try
                            {
                                dataToApplyChangesTo[schemaItem.fieldIndex] = String.Format("\"{0}\"", myStringValue);
                            }
                            catch (Exception ex)
                            {
                                _dalCache.LogError(ex.Source, ex.Message, DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                            }
                            break;

                        case SpacemanFieldType.Unknown:
                        default:
                            dataToApplyChangesTo[schemaItem.fieldIndex] = Convert.ToString(value);
                            break;
                    }
                }
                else
                {
                    _dalCache.LogError(Language.SetSpacemanFieldValue_Error, String.Format(Language.SetSpacemanFieldValue_Error, fieldKey, DalCacheItemTypeHelper.GetFriendlyName(this.CacheType)), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
                }
            }
            else
            {
                _dalCache.LogError(Language.SetSpacemanFieldValue_Error, String.Format(Language.SetSpacemanFieldValue_Error, fieldKey, DalCacheItemTypeHelper.GetFriendlyName(this.CacheType)), DalCacheItemTypeHelper.GetEventLogAffectedType(this.CacheType));
            }
        }

        internal String GetSpacemanFieldValue(String fieldKey)
        {
            SpacemanSchemaItem schemaItem;

            //Get the index to set
            if (_localSchemaDefinition.TryGetValue(fieldKey, out schemaItem))
            {
                return _exportData[schemaItem.fieldIndex];
            }

            return String.Empty;
        }

        internal SpacemanComponentType EncodeSpacemanComponentType(PlanogramSubComponentDto sub, PlanogramComponentDto com)
        {
            switch ((PlanogramSubComponentMerchandisingType)sub.MerchandisingType)
            {
                case PlanogramSubComponentMerchandisingType.Stack:
                    if (sub.FaceThicknessBottom > 0)
                    {
                        return SpacemanComponentType.Chest;
                    }
                    else
                    {
                        if (com.ComponentType == (Byte)PlanogramComponentType.Pallet)
                        {
                            return SpacemanComponentType.Pallet;
                        }
                        return SpacemanComponentType.Shelf;
                    }

                case PlanogramSubComponentMerchandisingType.HangFromBottom:
                    return SpacemanComponentType.Rod;



                //Not sure about this.
                case PlanogramSubComponentMerchandisingType.Hang:
                    if (sub.MerchConstraintRow1SpacingX == 0 && sub.MerchConstraintRow1SpacingY == 0)
                    {
                        return SpacemanComponentType.Bar;
                    }
                    return SpacemanComponentType.Peg;

                case PlanogramSubComponentMerchandisingType.None:
                    if (com.ComponentType == (Byte)PlanogramComponentType.Backboard ||
                        com.ComponentType == (Byte)PlanogramComponentType.Base)
                    {
                        return SpacemanComponentType.Bay;
                    }
                    return SpacemanComponentType.Divider;
                default: return SpacemanComponentType.Divider;
            }
        }

        /// <summary>
        /// Applies user mappings based on dto type
        /// </summary>
        /// <param name="dto"></param>
        internal void ApplyExportMappings(Object dto)
        {
            if (this._dalCache.ExportMappingContext != null)
            {
                Type type = dto.GetType();

                IEnumerable<PlanogramFieldMappingDto> mappings = null;
                if (dto is PlanogramDto)
                {
                    mappings = this._dalCache.ExportMappingContext.PlanogramMappings;
                }
                else
                if (dto is PlanogramFixtureDto)
                {
                    mappings = this._dalCache.ExportMappingContext.FixtureMappings;
                }
                else
                if (dto is PlanogramComponentDto)
                {
                    mappings = this._dalCache.ExportMappingContext.ComponentMappings;
                }
                else
                if (dto is PlanogramProductDto)
                {
                    mappings = this._dalCache.ExportMappingContext.ProductMappings;
                }
                else
                if (dto is PlanogramPerformanceDataDto)
                {
                    foreach (var mapping in this._dalCache.ExportMappingContext.MetricMappings)
                    {
                        PropertyInfo property = typeof(PlanogramPerformanceDataDto).GetProperty(String.Format("P{0}", mapping.MetricId));

                        if (property == null) continue;
                        SetSpacemanFieldValue(mapping.Source, property.GetValue(dto, null));
                    }
                }
                else
                {
                    return;                    
                }

                foreach (PlanogramFieldMappingDto mapping in mappings)
                {

                    PropertyInfo property = type.GetProperty(mapping.Source);
                    if (property == null &&
                        mapping.Source.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                    {
                        String[] columnMapping = mapping.Source.Split('.');
                        property = typeof(CustomAttributeDataDto).GetProperty(columnMapping[1]);

                        if (property == null) continue;
                        Object attribute;
                        if (this.Dtos.TryGetValue(typeof(CustomAttributeDataDto), out attribute))
                        {

                            SetSpacemanFieldValue(mapping.Target, property.GetValue(attribute, null));
                        }
                    }
                    else
                    {
                        if (property == null) continue;
                        SetSpacemanFieldValue(mapping.Target, property.GetValue(dto, null));
                    }
                }
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Called when this instance is disposing
        /// </summary>
        public void Dispose()
        {
            foreach (List<DalCacheItem> childList in _children.Values)
            {
                foreach (DalCacheItem child in childList)
                {
                    child.Dispose();
                }
            }
            _id = null;
            _data = null;
            _dto = null;
            _children = null;

            _schemaDefinitions.Clear();
            _schemaDefinitions = null;
            _localSchemaDefinition.Clear();
            _localSchemaDefinition = null;
            _exportData = null;
            _dalCache = null;
            _dtos.Clear();
            _dtoIds.Clear();
        }
        #endregion

        #endregion
    }

    /// <summary>
    /// Helper Peg Data class containing Spaceman Peg data
    /// </summary>
    public class PegData
    {
        public String Id { get; set; }
        public String  Source { get; set; }
        public Byte Type { get; set; }
        public Byte Span { get; set; }
        public Single Length { get; set; }
        public Single Rise { get; set; }
        public Single Headroom { get; set; }
        public Byte Activity { get; set; }
        public Single PegTagWidth { get; set; }
        public Single PegTagHeight { get; set; }
        public Single PegTagLocRight { get; set; }
        public Single PegTagLocDown { get; set; }
    }

    /// <summary>
    /// Helper Element Position class 
    /// </summary>
    public class ElementPositionDetail
    {
        public Single ProductHeight { get; set; }        
    }
}