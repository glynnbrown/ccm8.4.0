﻿#region Version History: CCM830
// V8-31548 : J.Pickup
// created
#endregion

using System.Collections.Generic;
using System;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Indicates the dal cache item type
    /// </summary>
    public enum SpacemanCostBasisType
    {
        Unit = 1
    }
}