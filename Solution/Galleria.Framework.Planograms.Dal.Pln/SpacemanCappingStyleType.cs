﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
#endregion

using System.Collections.Generic;
using System;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Indicates the spaceman orientation type.
    /// </summary>
    public enum SpacemanCappingStyleType
    {
        Unknown = 0,
        NoCap = 1,
        MinCap = 2, 
        MedCap = 3, 
        MaxCap = 4
    }
}