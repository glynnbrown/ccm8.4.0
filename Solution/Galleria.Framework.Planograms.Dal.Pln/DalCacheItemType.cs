﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM802
// V8-28893 : D.Pleasance
//  Added Peg
#endregion 
#endregion

using System.Collections.Generic;
using System;
using Galleria.Framework.Planograms.Dal.Pln.Resources;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Indicates the dal cache item type
    /// </summary>
    public enum DalCacheItemType
    {
        Store,
        Section,
        Fixel,
        Font,
        Position,
        Product,
        Peg,
        Unknown
    }

    /// <summary>
    /// Helper class to provide internationaalized strings for each DalCacheItemType
    /// </summary>
    public static class DalCacheItemTypeHelper
    {
        public static readonly Dictionary<DalCacheItemType, String> FriendlyNames =
            new Dictionary<DalCacheItemType, String>()
            {
                {DalCacheItemType.Store, "\"[2.141]Store\""},
                {DalCacheItemType.Section, "\"[2.143]Section\""},
                {DalCacheItemType.Fixel, "\"[2.144]Fixel\""},
                {DalCacheItemType.Font, "\"[50.0003]Font\""},
                {DalCacheItemType.Position, "\"[2.146]Position\""},
                {DalCacheItemType.Product, "\"[2.149]Product\""},
                {DalCacheItemType.Peg, "\"[2.147]PEG\""},
                {DalCacheItemType.Unknown, "\"Unknown\""}
            };

        public static DalCacheItemType GetDalCacheItemType(String description)
        {
            foreach (KeyValuePair<DalCacheItemType, String> keyPair in DalCacheItemTypeHelper.FriendlyNames)
            {
                if (description.StartsWith(keyPair.Value, StringComparison.InvariantCultureIgnoreCase))
                {
                    return keyPair.Key;
                }
            }
            return DalCacheItemType.Unknown;
        }


        public static String GetFriendlyName(DalCacheItemType dalCacheItemType)
        {
            switch (dalCacheItemType)   
            {
                case DalCacheItemType.Store:
                    return Language.DalCacheItemType_FriendlyName_Store;
                case DalCacheItemType.Section:
                    return Language.DalCacheItemType_FriendlyName_Section;
                case DalCacheItemType.Fixel:
                    return Language.DalCacheItemType_FriendlyName_Fixel;
                case DalCacheItemType.Font:
                    return Language.DalCacheItemType_FriendlyName_Font;
                case DalCacheItemType.Position:
                   return Language.DalCacheItemType_FriendlyName_Position;
                case DalCacheItemType.Product:
                    return Language.DalCacheItemType_FriendlyName_Product;
                case DalCacheItemType.Peg:
                    return Language.DalCacheItemType_FriendlyName_Peg;
                case DalCacheItemType.Unknown:
                    return Language.DalCacheItemType_FriendlyName_Unknown;
                default:
                    break;
            }

            return Language.DalCacheItemType_FriendlyName_Unknown;
        }

        public static PlanogramEventLogAffectedType GetEventLogAffectedType(DalCacheItemType dalCacheItemType)
        {
            switch (dalCacheItemType)
            {
                case DalCacheItemType.Store:
                    return PlanogramEventLogAffectedType.Planogram;
                case DalCacheItemType.Section:
                    return PlanogramEventLogAffectedType.Planogram;
                case DalCacheItemType.Fixel:
                    return PlanogramEventLogAffectedType.Components;
                case DalCacheItemType.Font:
                    return PlanogramEventLogAffectedType.Planogram;
                case DalCacheItemType.Position:
                    return PlanogramEventLogAffectedType.Positions;
                case DalCacheItemType.Product:
                    return PlanogramEventLogAffectedType.Products;
                case DalCacheItemType.Peg:
                    return PlanogramEventLogAffectedType.Components;
                case DalCacheItemType.Unknown:
                    return PlanogramEventLogAffectedType.Planogram;
                default:
                    break;
            }

            return PlanogramEventLogAffectedType.Planogram;
        }
    }
}