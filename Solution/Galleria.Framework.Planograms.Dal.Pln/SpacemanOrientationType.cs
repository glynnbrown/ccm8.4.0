﻿//Move dto helper = remove this class.

//#region Version History: CCM830
//// V8-31548 : J.Pickup
////  Work to facilitate exporting - created.
//#endregion

//using System.Collections.Generic;
//using System;

//namespace Galleria.Framework.Planograms.Dal.Pln
//{
//    /// <summary>
//    /// Indicates the spaceman orientation type.
//    /// </summary>
//    public enum SpacemanOrientationtype
//    {
//        Front0 = 0,
//        Front90 = 1,
//        Front180 = 2,
//        Front270 = 3,
//        Top0 = 4,
//        Top90 = 5,
//        Top180 = 6,
//        Top270 = 7,
//        Left0 = 8,
//        Left90 = 9,
//        Left180 = 10,
//        Left270 = 11,
//        Back0 = 12,
//        Back90 = 13,
//        Back180 = 14,
//        Back270 = 15,
//        Bottom0 = 16,
//        Bottom90 = 17,
//        Bottom180 = 18,
//        Bottom270 = 19,
//        Right0 = 20,
//        Right90 = 21,
//        Right180 = 22,
//        Right270 = 23,
//    }
//}