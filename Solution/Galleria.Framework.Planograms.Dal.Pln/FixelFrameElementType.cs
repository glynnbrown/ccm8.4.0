﻿#region Version History: CCM830
// V8-31548 : J.Pickup
//  Work to facilitate exporting - created.
// V8-32635 : J.Pickup
// Added AssemblyComponent.
// V8-32632 : J.Pickup
// Added Changed assembly to base - after further testing enum was wrong.




#endregion

using System.Collections.Generic;
using System;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Indicates the dal cache item type
    /// </summary>
    public enum FixelFrameElementType
    {
        Component = 0,
        Base = 1,
        Backboard = 2,
        LeftNotch = 3,
        RightNotch = 4        
    }
}