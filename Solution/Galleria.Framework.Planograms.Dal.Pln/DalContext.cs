﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24779 : D.Pleasance
//  Created
#endregion
#region Version History: CCM820
// V8-31439 : N.Foster
//  Added TransactionInProgress property
#region Version History: CCM830
// V8-31548 : J.Pickup
//  Commit now makes a call to export.
#endregion



#endregion
#endregion

using System;
using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.Dal.Pln
{
    /// <summary>
    /// Class that represents a single
    /// connection to a file
    /// </summary>
    public class DalContext : DalContextBase
    {
        #region Fields
        private DalCache _dalCache; // holds a reference to the dal cache object
        private Boolean _transactionInProgress; // indicates if a transaction is in progress on this context
        #endregion

        #region Properties
        /// <summary>
        /// Returns the shared cache object
        /// </summary>
        public DalCache DalCache
        {
            get { return _dalCache; }
        }

        /// <summary>
        /// Indicates if a transaction is in progress on this context
        /// </summary>
        public override bool TransactionInProgress
        {
            get { return _transactionInProgress; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalContext(DalCache dalCache)
        {
            _dalCache = dalCache;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Begins a transaction within this context
        /// </summary>
        public override void Begin()
        {
            _transactionInProgress = true;
        }

        /// <summary>
        /// Commits a transaction within this context
        /// </summary>
        public override void Commit()
        {
            _transactionInProgress = false;

            this.DalCache.ExportPackage();
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDispose()
        {
            base.OnDispose();
            if (_dalCache != null) _dalCache = null;
            _transactionInProgress = false;
        }
        #endregion
    }
}
