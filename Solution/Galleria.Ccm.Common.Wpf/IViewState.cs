﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26281 : L.Ineson 
//  Created.
#endregion
#region Version History: (CCM 8.1.1)
// V8-30078 : I.George
//  Added EntityIdChanged event
#endregion
#region Version History: (CCM 8.2.0)
//V8-28861 : L.Ineson
//  Added session directory methods.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32810 : L.Ineson
//  Added Settings.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Common.Wpf
{
    /// <summary>
    /// Defines the expected members of a viewstate class
    /// </summary>
    public interface IViewState
    {
        /// <summary>
        /// Gets the current entity id that we are connected to
        /// in the repository.
        /// </summary>
        Int32 EntityId { get; }

        /// <summary>
        /// Returns true if we are connected to the repository.
        /// </summary>
        Boolean IsConnectedToRepository { get; }

        /// <summary>
        /// Gets the location of the current help file.
        /// </summary>
        String HelpFilePath { get; }


        /// <summary>
        /// Returns the vm for the user editor settings.
        /// </summary>
        UserEditorSettingsViewModel Settings { get; }

        /// <summary>
        /// Notifies that the entity id has changed.
        /// </summary>
        event EventHandler EntityIdChanged;

        /// <summary>
        /// Gets the initial directory to use for the given type.
        /// </summary>
        String GetSessionDirectory(SessionDirectory type);

        /// <summary>
        /// Sets the new session directory for the given type.
        /// </summary>
        void SetSessionDirectory(SessionDirectory type, String directory);


        
    }

}
