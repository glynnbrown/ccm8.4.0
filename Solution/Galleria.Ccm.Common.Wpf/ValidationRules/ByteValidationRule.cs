﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28864 : A.Silva
//      Created

#endregion

#endregion

using System;
using System.Globalization;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.ValidationRules
{
    /// <summary>
    ///     Custom <see cref="ValidationRule"/> to check the validity of <see cref="byte"/> values.
    /// </summary>
    public class ByteValidationRule : ValidationRule
    {
        /// <summary>
        ///     Checks the value to see if it is a valid <see cref="Byte"/> value.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Windows.Controls.ValidationResult"/> object.
        /// </returns>
        /// <param name="value">The value from the binding target to check.</param><param name="cultureInfo">The culture to use in this rule.</param>
        public override ValidationResult Validate(Object value, CultureInfo cultureInfo)
        {
            Byte convertedvalue;
            return value != null && Byte.TryParse(value.ToString(), out convertedvalue)
                       ? new ValidationResult(true, null)
                       : new ValidationResult(false, String.Format(Message.ByteValidationRule_OutOfRangeError, Byte.MinValue, Byte.MaxValue));
        }
    }
}