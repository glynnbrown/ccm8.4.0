﻿using Fluent;
using System;
using System.Windows;

namespace Galleria.Ccm.Common.Wpf.Controls
{
    /// <summary>
    /// Sub class inheriting from Fluent.MenuItem
    /// so that we may deal with quick access items being created.
    /// </summary>
    public class DynamicMenuItem : Fluent.MenuItem, IQuickAccessItemProvider
    {
        public class CreateQuickAccessItemArgs : EventArgs
        {
            public FrameworkElement Element { get; set; }
        }

        public event EventHandler<CreateQuickAccessItemArgs> CreatingQuickAccessItem;

        private FrameworkElement RaiseCreatingQuickAccessItem(FrameworkElement element)
        {
            if (CreatingQuickAccessItem == null) return element;

            CreateQuickAccessItemArgs args = new CreateQuickAccessItemArgs();
            args.Element = element;
            CreatingQuickAccessItem(this, args);
            return args.Element;
        }

        public new FrameworkElement CreateQuickAccessItem()
        {
            FrameworkElement element = RaiseCreatingQuickAccessItem(base.CreateQuickAccessItem());
            return element;
        }
    }
}
