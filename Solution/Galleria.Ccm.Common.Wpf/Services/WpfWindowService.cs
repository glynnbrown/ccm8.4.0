﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 801)

// V8-28755 : L.Ineson 
//   Created.
// V8-27897 : A.Kuszyk
//  Added information icon to information message in ShowMessage.
// V8-28676 : A.Silva
//      Added implementation for ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler).
// V8-28882 : A.Silva
//      Amended implementation of GetCurrentActiveWindow to ignore AvalonDock panels as possible active windows.

#endregion
#region Version History : CCM820
// V8-29968 : A.Kuszyk
//  Added 4 button ShowMessage method.
#endregion

#region Version History : CCM830
// V8-32255 : A.Silva
//  Added implementation for ContinueWithItemChange allowing to pass a parameter to the save command.
// V8-32361 : L.Ineson
//  Added activate window.
// V8-32810 : L.Ineson
//  Added ShowSelectDirectoryDialog
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using Local = Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;
using System.Diagnostics;
using Fluent;
using System.Windows.Input;

namespace Galleria.Ccm.Common.Wpf.Services
{
    /// <summary>
    /// WPF implementation of IWindowService
    /// Used to control the display of windows in the ui.
    /// </summary>
    public sealed class WpfWindowService : IWindowService
    {
        #region General Windows

        /// <summary>
        /// Creates a new window with the given parameters and shows it as a non-modal window
        /// </summary>
        /// <typeparam name="T">The type of window control to create</typeparam>
        /// <param name="windowArgs">The window constructor parameters. Usually the viewmodel. 
        /// Can also except the window control itself however this is not recommended.</param>
        public void ShowWindow<T>(params Object[] windowArgs)
            where T : Window
        {
            Window window = CreateNewWindow(typeof(T), windowArgs);
            ShowWindow(window, false);
        }

        /// <summary>
        /// Creates a new window with the given parameters and shows it as a modal
        /// dialog.
        /// </summary>
        /// <typeparam name="T">The type of window control to create</typeparam>
        /// <param name="windowArgs">The window constructor parameters. Usually the viewmodel. 
        /// Can also except the window control itself however this is not recommended.</param>
        public void ShowDialog<T>(params Object[] windowArgs)
            where T : Window
        {
            Window window = CreateNewWindow(typeof(T), windowArgs);
            ShowWindow(window, /*isModal*/true);
        }

        /// <summary>
        /// Closes the given window.
        /// </summary>
        /// <param name="window">The window to close</param>
        public void CloseWindow(Window window)
        {
            window.Close();
        }

        /// <summary>
        /// Opens the ribbon control backstage on the given window.
        /// </summary>
        /// <param name="window">the window to open the ribbon on</param>
        public void OpenRibbonBackstage(Window window)
        {
            if (window == null) return;
            SetRibbonBackstageIsOpen(window as Fluent.RibbonWindow, true);
        }

        /// <summary>
        /// Closes the ribbon control backstage on the given window.
        /// </summary>
        /// <param name="window">the window to find the ribbon on</param>
        public void CloseRibbonBackstage(Window window)
        {
            if (window == null) return;
            SetRibbonBackstageIsOpen(window as Fluent.RibbonWindow, false);
        }


        /// <summary>
        /// Searches for an existing window of the given type and activates it.
        /// </summary>
        /// <typeparam name="T">the window type to find</typeparam>
        /// <returns>A reference to the existing window or null if none found.</returns>
        public T ActivateWindow<T>() where T : Window
        {
            T win = Framework.Controls.Wpf.Helpers.GetWindow<T>();

            if (win != null) 
            {
                win.Activate();
            }

            return win;
        }

        #endregion

        #region Messages/Prompts

        /// <summary>
        /// Shows the requested error message to the user.
        /// </summary>
        public void ShowErrorMessage(String header, String description)
        {
            ShowMessage(MessageWindowType.Error, header, description, 1, Message.Generic_Ok, null, null);
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            String button1Content, String button2Content, String button3Content)
        {
            return ShowMessage(messageType, header, description, 3, button1Content, button2Content, button3Content);
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content)
        {
            ModalMessage msg = new ModalMessage()
            {
                Header = header,
                Description = description,
                ButtonCount = buttonCount,
                Button1Content = button1Content,
                Button2Content = button2Content,
                Button3Content = button3Content,
                MessageIcon = GetMessageIcon(messageType)
            };

            ShowWindow(msg, /*isModal*/true);

            return msg.Result;
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content, 
            ModalMessageButton defaultButton, ModalMessageButton cancelButton)
        {
            ModalMessage msg = new ModalMessage()
            {
                Header = header,
                Description = description,
                ButtonCount = buttonCount,
                Button1Content = button1Content,
                Button2Content = button2Content,
                Button3Content = button3Content,
                MessageIcon = GetMessageIcon(messageType),
                DefaultButton = defaultButton,
                CancelButton = cancelButton
            };

            ShowWindow(msg, /*isModal*/true);

            return msg.Result;
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content, String button4Content)
        {
            ModalMessage msg = new ModalMessage()
            {
                Header = header,
                Description = description,
                ButtonCount = buttonCount,
                Button1Content = button1Content,
                Button2Content = button2Content,
                Button3Content = button3Content,
                Button4Content = button4Content,
                MessageIcon = GetMessageIcon(messageType)
            };

            ShowWindow(msg, /*isModal*/true);

            return msg.Result;
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType,
            String header, String description, String button1Content, String button2Content)
        {
            return ShowMessage(messageType, header, description, 2, button1Content, button2Content, null);
        }

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        public ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content, 
            String hideFutureWarningsText, out Boolean hideFutureWarningsResult)
        {
            ModalMessage msg = new ModalMessage()
            {
                Header = header,
                Description = description,
                ButtonCount = buttonCount,
                Button1Content = button1Content,
                Button2Content = button2Content,
                Button3Content = button3Content,
                MessageIcon = GetMessageIcon(messageType),
                IsHideFutureWarningsCheckVisible = true,
                HideFutureWarningsText = hideFutureWarningsText
            };

            ShowWindow(msg, /*isModal*/true);

            hideFutureWarningsResult = msg.IsHideFutureWarningsRequested;
            return msg.Result;
        }

        /// <summary>
        /// Shows the requested ok message to the user.
        /// </summary>
        public void ShowOkMessage(MessageWindowType messageType, String header, String description)
        {
            ShowMessage(messageType, header, description, 1, Message.Generic_Ok, null, null);
        }

        private static System.Windows.Media.ImageSource GetMessageIcon(MessageWindowType messageType)
        {
            System.Windows.Media.ImageSource messageIcon = null;
            switch (messageType)
            {
                case MessageWindowType.Error:
                    messageIcon = ImageResources.CriticalError_32;
                    break;

                case MessageWindowType.Warning:
                    messageIcon = Galleria.Ccm.Common.Wpf.Resources.ImageResources.Warning_32;
                    break;

                case MessageWindowType.Info:
                    messageIcon = Galleria.Ccm.Common.Wpf.Resources.ImageResources.Dialog_Information;
                    break;

                case MessageWindowType.Question:
                    messageIcon = Galleria.Ccm.Common.Wpf.Resources.ImageResources.DialogQuestion;
                    break;
            }
            return messageIcon;
        }

        public Boolean ConfirmDeleteWithUser(String itemName)
        {
            return FrameworkHelpers.ConfirmDeleteWithUser(itemName);
        }

        public void ShowErrorOccurredMessage(String itemName, OperationType actionType)
        {
            FrameworkHelpers.ShowErrorOccurredMessage(itemName, actionType);
        }

        public Boolean ContinueWithItemChange(Csla.Core.BusinessBase modelObject, RelayCommand saveCommand)
        {
            return FrameworkHelpers.ContinueWithItemChange(modelObject, saveCommand);
        }

        public Boolean ContinueWithItemChange(Csla.Core.BusinessBase modelObject, RelayCommand saveCommand, Object commandParameter)
        {
            return FrameworkHelpers.ContinueWithItemChange(modelObject, saveCommand, commandParameter);
        }

        public Boolean PromptIfNameIsNotUnique(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue)
        {
            return FrameworkHelpers.PromptIfNameIsNotUnique(isUniqueCheck, proposedValue, out selectedValue);
        }

        public Boolean ShowConcurrencyReloadPrompt(String itemName)
        {
            return FrameworkHelpers.ShowConcurrencyReloadPrompt(itemName);
        }

        public Boolean PromptForSaveAsName(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue)
        {
            return FrameworkHelpers.PromptForSaveAsName(isUniqueCheck, proposedValue, out selectedValue);
        }

        public Boolean PromptUserForValue(String dialogTitle, String forceFirstShowDescription, String notUniqueDescription, 
            String inputLabel, Boolean forceFirstShow, Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue)
        {
            return FrameworkHelpers.PromptUserForValue(dialogTitle, forceFirstShowDescription, notUniqueDescription,
            inputLabel, forceFirstShow, isUniqueCheck, proposedValue, out selectedValue);
        }

        #endregion

        #region Modal Busy

        /// <summary>
        /// Shows a determinate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        public void ShowDeterminateBusy(String header, String desc)
        {
            ModalBusy busy = new ModalBusy()
            {
                Header = header,
                Description = desc,
                IsDeterminate = true,
                IsCancelAvailable = false,
            };
            ShowWindow(busy, /*isModal*/true);
        }

        /// <summary>
        /// Shows a determinate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="cancelHandler">The cancel event handler</param>
        public void ShowDeterminateBusy(String header, String desc, EventHandler cancelHandler)
        {
            ModalBusy busy = new ModalBusy()
            {
                Header = header,
                Description = desc,
                IsDeterminate = true,
                IsCancelAvailable = true,
            };
            busy.CancelRequested += cancelHandler;

            ShowWindow(busy, /*isModal*/true);
        }

        /// <summary>
        /// Shows an indeterminate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        public void ShowIndeterminateBusy(String header, String desc)
        {
            ModalBusy busy = new ModalBusy()
            {
                Header = header,
                Description = desc,
                IsDeterminate = false,
                IsCancelAvailable = false,
            };
            ShowWindow(busy, /*isModal*/true);
        }

        /// <summary>
        /// Shows an indeterminate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="cancelHandler">the cancel event handler</param>
        public void ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler)
        {
            ModalBusy busy = new ModalBusy()
            {
                Header = header,
                Description = desc,
                IsDeterminate = false,
                IsCancelAvailable = true,
            };
            busy.CancelRequested += cancelHandler;

            ShowWindow(busy, /*isModal*/true);
        }

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="progressPercentage">the new percentage</param>
        public void UpdateBusy(String header, String description, Int32 progressPercentage)
        {
            ModalBusy busy = GetCurrentBusy();
            if (busy == null) return;

            busy.Header = header;
            busy.Description = description;
            busy.ProgressPercentage = progressPercentage;
        }

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="desc">the window description text</param>
        /// <param name="progressPercentage">the new percentage</param>
        public void UpdateBusy(String description, Int32 progressPercentage)
        {
            ModalBusy busy = GetCurrentBusy();
            if (busy == null) return;

            busy.Description = description;
            busy.ProgressPercentage = progressPercentage;
        }

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="progressPercentage">the new percentage</param>
        public void UpdateBusy(Int32 progressPercentage)
        {
            ModalBusy busy = GetCurrentBusy();
            if (busy == null) return;

            busy.ProgressPercentage = progressPercentage;
        }

        /// <summary>
        /// Closes the current busy window
        /// </summary>
        public void CloseCurrentBusy()
        {
            ModalBusy busy = GetCurrentBusy();
            if (busy == null) return;

            busy.Close();
        }

        /// <summary>
        /// Closes the current busy window
        /// </summary>
        /// <param name="cancelHandler">the cancel event handler</param>
        public void CloseCurrentBusy(EventHandler cancelHandler)
        {
            ModalBusy busy = GetCurrentBusy();
            if (busy == null) return;

            busy.CancelRequested -= cancelHandler;
            busy.Close();
        }

        /// <summary>
        /// Overrides the mouse cursor with the wait cursor.
        /// </summary>
        /// <returns>True if the cursor was not already applied.</returns>
        public Boolean ShowWaitCursor()
        {
            if (Mouse.OverrideCursor == Cursors.Wait) return false;
            Mouse.OverrideCursor = Cursors.Wait;
            return true;
        }

        /// <summary>
        /// Hides the wait cursor override.
        /// </summary>
        public void HideWaitCursor()
        {
            Mouse.OverrideCursor = null;
        }

        #endregion

        #region File dialogs

        /// <summary>
        /// Show the standard open file dialog
        /// </summary>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="fileName">the name of the selected file</param>
        /// <returns>True if the action was completed.</returns>
        public Boolean ShowOpenFileDialog(String intialDirectory, String filter, out String fileName)
        {
            var dialog = new System.Windows.Forms.OpenFileDialog()
            {
                Filter = filter,
                CheckFileExists = true,
                InitialDirectory = intialDirectory,
                Multiselect = false, // use other method to open multiple
                CheckPathExists = true
            };

            Boolean result = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            fileName = dialog.FileName;

            return result;
        }

        /// <summary>
        /// Show the standard open file dialog to select multiple files.
        /// </summary>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="fileNames">An array containing selected filenames</param>
        /// <returns>True if the action was completed.</returns>
        public Boolean ShowOpenMultiFileDialog(String intialDirectory, String filter, out String[] fileNames)
        {
            var dialog = new System.Windows.Forms.OpenFileDialog()
            {
                Filter = filter,
                CheckFileExists = true,
                InitialDirectory = intialDirectory,
                Multiselect = true, 
                CheckPathExists = true
            };

            Boolean result = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            fileNames = dialog.FileNames;

            return result;
        }

        /// <summary>
        /// Shows the standard save file dialog.
        /// </summary>
        /// <param name="fileName">the default file name</param>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="selectedFileName">the name of the selected file</param>
        /// <returns>True if the action was completed.</returns>
        public Boolean ShowSaveFileDialog(String fileName, String intialDirectory, String filter, out String selectedFileName)
        {
            var dialog = new System.Windows.Forms.SaveFileDialog
            {
                FileName = fileName,
                InitialDirectory = intialDirectory,
                Filter = filter,
                OverwritePrompt = true
            };

            Boolean result = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            selectedFileName = dialog.FileName;

            return result;
        }

        /// <summary>
        /// Shows the standard directory selection dialog.
        /// </summary>
        /// <param name="intialDirectory"></param>
        /// <returns></returns>
        public Boolean ShowSelectDirectoryDialog(String intialDirectory, out String selectedDirectory)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog
            {
                SelectedPath = intialDirectory,
            };

            Boolean result = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            selectedDirectory = dialog.SelectedPath;

            return result;
        }

        #endregion

        #region Helpers

        [DllImport("user32.dll")]
        static extern IntPtr GetActiveWindow();

        /// <summary>
        ///     Gets the <see cref="Window" /> currently active in the application.
        /// </summary>
        /// <param name="ignoreWin"><see cref="Window" /> that should be ignored as a candidate to return.</param>
        /// <returns>The <see cref="Window" /> instance that is currently active or null if none.</returns>
        /// <remarks>
        ///     [V8-28882] There is an issue with AvalonDock panels in which having several will make them ALL appear as active
        ///     window if the application does not have the focus, and none should really be considered a window.
        ///     Any AvalonDock panels will be ignored by this method.
        /// </remarks>
        private static Window GetCurrentActiveWindow(Window ignoreWin)
        {
            IntPtr active = GetActiveWindow();

            //  Get the current collection of windows for the application, except the ignored window and any Avalon panels
            //  (if there are any might all appear as active and none is valid as an owner window).
            IEnumerable<Window> allWindows =
                Application.Current.Windows.OfType<Window>().Where(
                    window =>
                    !Equals(window, ignoreWin) && !(window.GetType().Namespace ?? String.Empty).Contains("Avalon"))
                           .ToList();

            //  Get the active window in the current collection.
            Window activeWindow = allWindows.SingleOrDefault(window => new WindowInteropHelper(window).Handle == active);

            //  Return the active window or if there is none, the last window in the collection (the most recently added).
            return activeWindow ?? allWindows.LastOrDefault();
        }

        /// <summary>
        /// Shows the requested window.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="isModal"></param>
        private static void ShowWindow(Window window, Boolean isModal)
        {
            //force the parent
            if (window.Owner == null
                && window != Application.Current.MainWindow)
            {
                window.Owner = GetCurrentActiveWindow(window);
            }

            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Icon = Local.ImageResources.CCMIcon;
            window.ShowInTaskbar = true;
            //window.Closed += new EventHandler(ChildWindow_Closed);

            //validate the size of the window to ensure that it is within the screen limits.
            if (window.Height > 0 || window.Width > 0)
            {
                System.Windows.Forms.Screen curScreen =
                    System.Windows.Forms.Screen.FromHandle(new WindowInteropHelper(window).Handle);
                if (window.Height > 0 && window.Height > curScreen.Bounds.Height) window.Height = curScreen.Bounds.Height;
                if (window.Width > 0 && window.Width > curScreen.Bounds.Width) window.Width = curScreen.Bounds.Width;
            }



            //show it.
            if (isModal)
            {
                window.ShowDialog();
            }
            else
            {
                window.Show();
            }
        }

        private static Window CreateNewWindow(Type windowType, Object[] windowArgs)
        {
            //check that we were not passed a window as a parameter
            if (windowArgs.Count() == 1 && windowArgs[0] != null && windowArgs[0] is Window)
            {
                if (windowArgs[0].GetType() == windowType)
                {
                    return (Window)windowArgs[0];
                }
            }


            Type[] paramTypes = null;
            if (windowArgs != null)
            {
                paramTypes = windowArgs.Select(a => a.GetType()).ToArray();
            }

            ConstructorInfo ctr = windowType.GetConstructor(paramTypes);
            Window window = (Window)ctr.Invoke(windowArgs);

            return window;
        }

        private ModalBusy GetCurrentBusy()
        {
            ModalBusy busy = FrameworkHelpers.GetWindow<ModalBusy>();
            if (busy == null)
            {
                Debug.Fail("busy not found");
                return null;
            }
            return busy;
        }

        private static void SetRibbonBackstageIsOpen(RibbonWindow ribbonWin, Boolean isBackstageOpen)
        {
            if (ribbonWin == null) return;
            if (ribbonWin.Content == null) return;

            //get the ribbon belonging to the window
            Ribbon ownerRibbon = (((DependencyObject)ribbonWin.Content)).FindVisualDescendent<Ribbon>();
            if (ownerRibbon == null) return;

            //set the backstage is open state
            Backstage ribbonBstg = ownerRibbon.Menu.FindVisualDescendent<Backstage>();
            if (ribbonBstg == null) return;
            ribbonBstg.IsOpen = isBackstageOpen;
        }

        #endregion

    }
}