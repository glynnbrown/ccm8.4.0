﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 801)
// V8-28755 : L.Ineson 
//   Created.
// V8-28716 : L.Ineson
//  Added user state overload to report progress.
//  Amended to use normal background worker
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;

namespace Galleria.Ccm.Common.Wpf.Services
{
    public sealed class WpfModalBusyWorkerService : IModalBusyWorkerService
    {
        #region Fields
        private BackgroundWorker _worker;
        private IModalBusyWork _currentWork;
        private Boolean _isBusy;
        #endregion

        #region Properties

        public Boolean IsBusy
        {
            get { return _isBusy; }
            private set { _isBusy = value; }
        }

        public IModalBusyWork CurrentWork
        {
            get { return _currentWork; }
        }

        public Boolean CancellationPending
        {
            get
            {
                if (_worker == null) return false;
                return _worker.CancellationPending;
            }
        }

        #endregion

        #region Constuctor
        public WpfModalBusyWorkerService() { }
        #endregion

        #region Methods

        public void RunWorker(IModalBusyWork work)
        {
            if (IsBusy)
            {
                Debug.Fail("Already busy");
                return;
            }

            this.IsBusy = true;
            _currentWork = work;

            //create the worker
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = work.ReportsProgress;
            worker.WorkerSupportsCancellation = work.SupportsCancellation;

            worker.DoWork += Worker_DoWork;
            if (work.ReportsProgress) worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

            _worker = worker;
            worker.RunWorkerAsync(work.GetWorkerArgs());


            work.ShowModalBusy(this, Worker_CancelRequested);
        }

        private IWindowService GetWindowService()
        {
            return Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>();
        }

        public void ReportProgress(Int32 progress)
        {
            ReportProgress(progress, null);
        }

        public void ReportProgress(Int32 progress, Object userState)
        {
            Debug.Assert(_worker != null, "Trying to report progress without a worker.");
            if (_worker == null) return;
            _worker.ReportProgress(progress, userState);
        }

        #endregion

        #region Event Handlers

        private void Worker_CancelRequested(object sender, EventArgs e)
        {
            _worker.CancelAsync();
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.CurrentWork.OnDoWork(this, e);
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.CurrentWork.OnProgressChanged(this, e);
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Tidy up.
            BackgroundWorker worker = (BackgroundWorker)sender;
            Debug.Assert(_worker == worker, "worked completed is not the current one");
            _worker = null;

            worker.ProgressChanged -= Worker_ProgressChanged;
            worker.DoWork -= Worker_DoWork;
            worker.RunWorkerCompleted -= Worker_RunWorkerCompleted;

            //close the busy window, passing through the cancel handler to unsubscribe.
            GetWindowService().CloseCurrentBusy(Worker_CancelRequested);

            IModalBusyWork work = this.CurrentWork;
            _currentWork = null;

            IsBusy = false;

            work.OnWorkCompleted(this, e);
        }

        #endregion


    }
}
