﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 801)
// V8-28755 : L.Ineson 
//   Created.
// V8-28716 : L.Ineson
//  Added user state overload to report progress.
//  Added Getworkerargs to work interface.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Common.Wpf.Services
{
    public interface IModalBusyWorkerService
    {
        void RunWorker(IModalBusyWork work);
        void ReportProgress(Int32 progress);
        void ReportProgress(Int32 progress, Object userState);

        Boolean CancellationPending { get; }
    }

    public interface IModalBusyWork
    {
        Boolean ReportsProgress { get; }
        Boolean SupportsCancellation { get; }

        Object GetWorkerArgs();

        void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e);

        void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e);

        void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e);

        void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler);
    }

}
