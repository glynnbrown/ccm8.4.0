﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 801)
// V8-28755 : L.Ineson 
//   Created.
// V8-28676 : A.Silva
//      Added ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler).

#endregion
#region Version History : CCM820
// V8-29968 : A.Kuszyk
//  Added 4 button ShowMessage method.
#endregion

#region Version History : CCM830

// V8-32255 : A.Silva
//  Added overload for ContinueWithItemChange to allow passing parameters to the save command.
// V8-32361 : L.Ineson
//  Added activate window.
// V8-32589 : L.Ineson
//  Added methods for show hide wait cursor.
// V8-32810 : L.Ineson
//  Added ShowSelectDirectoryDialog
#endregion

#endregion

using System;
using System.Windows;
using Csla.Core;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Services
{
    public enum MessageWindowType
    {
        None,
        Warning,
        Error,
        Info,
        Question,
    }

    public interface IWindowService
    {
        /// <summary>
        /// Creates a new window with the given parameters and shows it as a non-modal window
        /// </summary>
        /// <typeparam name="T">The type of window control to create</typeparam>
        /// <param name="windowArgs">The window constructor parameters. Usually the viewmodel. 
        /// Can also except the window control itself however this is not recommended.</param>
        void ShowWindow<T>(params Object[] windowArgs) where T : Window;

        /// <summary>
        /// Creates a new window with the given parameters and shows it as a modal
        /// dialog.
        /// </summary>
        /// <typeparam name="T">The type of window control to create</typeparam>
        /// <param name="windowArgs">The window constructor parameters. Usually the viewmodel. 
        /// Can also except the window control itself however this is not recommended.</param>
        void ShowDialog<T>(params Object[] windowArgs) where T : Window;

        /// <summary>
        /// Closes the given window.
        /// </summary>
        /// <param name="window">The window to close</param>
        void CloseWindow(Window window);

        /// <summary>
        /// Searches for an existing window of the given type and activates it.
        /// </summary>
        /// <typeparam name="T">the window type to find</typeparam>
        /// <returns>A reference to the existing window or null if none found.</returns>
        T ActivateWindow<T>() where T : Window;

        /// <summary>
        /// Opens the ribbon control backstage on the given window.
        /// </summary>
        /// <param name="window">the window to open the ribbon on</param>
        void OpenRibbonBackstage(Window window);

        /// <summary>
        /// Closes the ribbon control backstage on the given window.
        /// </summary>
        /// <param name="window">the window to find the ribbon on</param>
        void CloseRibbonBackstage(Window window);

        #region  Messages / Prompts

        /// <summary>
        /// Shows the requested error message to the user.
        /// </summary>
        void ShowErrorMessage(String header, String description);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content,
            ModalMessageButton defaultButton, ModalMessageButton cancelButton);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
           Int32 buttonCount, String button1Content, String button2Content, String button3Content);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
           String button1Content, String button2Content, String button3Content);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
            Int32 buttonCount, String button1Content, String button2Content, String button3Content, String button4Content);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
             String button1Content, String button2Content);

        /// <summary>
        /// Shows the requested message to the user and returns a result.
        /// </summary>
        ModalMessageResult ShowMessage(MessageWindowType messageType, String header, String description,
           Int32 buttonCount, String button1Content, String button2Content, String button3Content,
           String hideFutureWarningsText, out Boolean hideFutureWarningsResult);

        /// <summary>
        /// Shows the requested ok message to the user.
        /// </summary>
        void ShowOkMessage(MessageWindowType messageType, String header, String description);

        /// <summary>
        /// Shows the requested error message to the user.
        /// </summary>
        /// <param name="itemName">the name of the item against which the action was performed.</param>
        /// <param name="actionType">The type of action that failed.</param>
        void ShowErrorOccurredMessage(String itemName, OperationType actionType);

        /// <summary>
        /// Shows a prompt asking the user to confirm the delete action.
        /// </summary>
        /// <param name="itemName">The name of the item to be deleted.</param>
        /// <returns>True if the delete action may proceed.</returns>
        Boolean ConfirmDeleteWithUser(String itemName);

        /// <summary>
        /// Shows a prompt asking the user to confirm the item change action when the given model
        /// object is dirty.
        /// </summary>
        /// <param name="modelObject">The model object to check</param>
        /// <param name="saveCommand">The save command to execute if required</param>
        /// <returns>True if the change action may proceed.</returns>
        Boolean ContinueWithItemChange(BusinessBase modelObject, RelayCommand saveCommand);

        /// <summary>
        ///     Prompt the user to continue or not if the item has changes.
        /// </summary>
        /// <param name="modelObject">The instance that might be changed.</param>
        /// <param name="saveCommand">The command used to save the instance.</param>
        /// <param name="commandParameter">The parameter to pass to the command.</param>
        /// <returns>True if the change action may proceed.</returns>
        Boolean ContinueWithItemChange(BusinessBase modelObject, RelayCommand saveCommand, Object commandParameter);

        /// <summary>
        /// Shows a prompt asking the user for a new name if the selected name is not unique.
        /// </summary>
        /// <param name="isUniqueCheck">the unique check to call</param>
        /// <param name="proposedValue">the proposed name</param>
        /// <param name="selectedValue">the selected unique name</param>
        /// <returns>True if the action was not cancelled.</returns>
        Boolean PromptIfNameIsNotUnique(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue);

        Boolean PromptUserForValue(String dialogTitle, String forceFirstShowDescription, String notUniqueDescription, String inputLabel, Boolean forceFirstShow, Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue);

        /// <summary>
        /// Shows a prompt informing the user that a concurrency error has occured
        /// </summary>
        /// <param name="itemName">The name of the item which failed</param>
        /// <returns>True if the user has selected to reload.</returns>
        Boolean ShowConcurrencyReloadPrompt(String itemName);


        /// <summary>
        /// Shows a prompt asking the user for a new name. The window will always be displayed at least once.
        /// </summary>
        /// <param name="isUniqueCheck">the unique check to call</param>
        /// <param name="proposedValue">the proposed name</param>
        /// <param name="selectedValue">the selected unique name</param>
        /// <returns>True if the action was not cancelled.</returns>
        Boolean PromptForSaveAsName(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue);

        #endregion

        #region ModalBusy

        /// <summary>
        /// Shows a determinate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        void ShowDeterminateBusy(String header, String desc);

        /// <summary>
        /// Shows a determinate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="cancelHandler">The cancel event handler</param>
        void ShowDeterminateBusy(String header, String desc, EventHandler cancelHandler);

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="progressPercentage">the new percentage</param>
        void UpdateBusy(String header, String description, Int32 progressPercentage);

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="desc">the window description text</param>
        /// <param name="progressPercentage">the new percentage</param>
        void UpdateBusy(String description, Int32 progressPercentage);

        /// <summary>
        /// Updates the existing busy windpw process percentage
        /// </summary>
        /// <param name="progressPercentage">the new percentage</param>
        void UpdateBusy(Int32 progressPercentage);

        /// <summary>
        /// Closes the current busy window
        /// </summary>
        void CloseCurrentBusy();

        /// <summary>
        /// Closes the current busy window
        /// </summary>
        /// <param name="cancelHandler">the cancel event handler</param>
        void CloseCurrentBusy(EventHandler cancelHandler);

        /// <summary>
        /// Shows an indeterminate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        void ShowIndeterminateBusy(String header, String desc);

        /// <summary>
        /// Shows an indeterminate busy window
        /// </summary>
        /// <param name="header">the window header</param>
        /// <param name="desc">the window description text</param>
        /// <param name="cancelHandler">the cancel event handler</param>
        void ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler);

        /// <summary>
        /// Overrides the mouse cursor with the wait cursor.
        /// </summary>
        /// <returns>True if the cursor was not already applied.</returns>
        Boolean ShowWaitCursor();

        /// <summary>
        /// Hides the wait cursor override.
        /// </summary>
        void HideWaitCursor();

        #endregion

        #region file dialogs

        /// <summary>
        /// Show the standard open file dialog
        /// </summary>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="fileName">the name of the selected file</param>
        /// <returns>True if the action was completed.</returns>
        Boolean ShowOpenFileDialog(String intialDirectory, String filter, out String fileName);

        /// <summary>
        /// Show the standard open file dialog to select multiple files.
        /// </summary>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="fileNames">An array containing selected filenames</param>
        /// <returns>True if the action was completed.</returns>
        Boolean ShowOpenMultiFileDialog(String intialDirectory, String filter, out String[] fileNames);

        /// <summary>
        /// Shows the standard save file dialog.
        /// </summary>
        /// <param name="fileName">the default file name</param>
        /// <param name="intialDirectory">the directory to show</param>
        /// <param name="filter">the file type filter</param>
        /// <param name="selectedFileName">the name of the selected file</param>
        /// <returns>True if the action was completed.</returns>
        Boolean ShowSaveFileDialog(String fileName, String intialDirectory, String filter, out String selectedFileName);

        /// <summary>
        /// Shows the standard directory selection dialog.
        /// </summary>
        /// <param name="intialDirectory"></param>
        /// <returns></returns>
        Boolean ShowSelectDirectoryDialog(String intialDirectory, out String selectedDirectory);

        #endregion
    }
}
