﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class LocationProductIllegalListViewModel : ViewStateObject<LocationProductIllegalList>
    {
        #region Constructor
        public LocationProductIllegalListViewModel() { }
        #endregion

        #region Fetch

        /// <summary>
        /// Fetches all objects by current entityId and loads the model synchronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new LocationProductIllegalList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion

        #region Save

        /// <summary>
        /// Save the current model synchronously
        /// </summary>
        public void Save()
        {
            Update();
        }

        #endregion
    }
}
