﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
// V8-27058 : A.Probyn
//  Changed over to use PlanogramMetadataImage for thumbnail
// V8-27411 : M.Pettit
//   Changed so that package is fetched as unlocked read-only object
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Processes;
using Galleria.Framework.Planograms.Processes.RenderPlanogramImage;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Helpers;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Viewmodel for the PlanogramMetadataImage model object
    /// </summary>
    public sealed class PlanogramMetadataImageViewModel : ViewStateObject<PlanogramMetadataImage>
    {
        #region Constructor
        public PlanogramMetadataImageViewModel() { }
        #endregion

        #region Methods

        #region FetchThumbnailByPlanogramId

        /// <summary>
        /// Fetches the planogram thumbnail
        /// </summary>
        public void FetchThumbnailByPlanogramId(Int32 planogramId)
        {
            if (!CCMClient.IsUnitTesting)
            {
                Package package = Package.FetchById(planogramId);
                Planogram plan = package.Planograms[0];


                PlanogramMetadataImage img = null;
                if (plan.PlanogramMetadataImages.Count > 0)
                {
                    img = plan.PlanogramMetadataImages.FirstOrDefault();
                }
                
                this.Model = img;

            }
            else
            {
                this.Model = PlanogramMetadataImage.NewPlanogramMetadataImage();
            }
        }

        /// <summary>
        /// Fetches the planogram thumbnail asyncronously.
        /// </summary>
        public void FetchThumbnailByPlanogramIdAsync(Int32 planogramId, Boolean renderIfNotAvailable = false)
        {
            if (!CCMClient.IsUnitTesting)
            {
                Csla.Threading.BackgroundWorker worker = new Csla.Threading.BackgroundWorker();
                worker.DoWork += worker_DoWork;
                worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                worker.RunWorkerAsync(new Object[] { planogramId, renderIfNotAvailable });
            }
            else
            {
                FetchThumbnailByPlanogramId(planogramId);
            }
        }
        void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Fetch package as read-only
            Object[] args = (Object[])e.Argument;
            Package package = Package.FetchById((Int32)args[0]);
            Planogram plan = package.Planograms[0];

            PlanogramMetadataImage img = plan.PlanogramMetadataImages.FirstOrDefault();

            if (img == null && ((Boolean)args[1]))
            {
                try
                {
                    //force immediate calc:
                    IPlanogramMetadataHelper metadataHelper = PlanogramMetadataHelper.NewPlanogramMetadataHelper((Int32)package.EntityId, package);
                    package.CalculateMetadata(processMetadataImages: true, metadataHelper: metadataHelper);
                    img = plan.PlanogramMetadataImages.FirstOrDefault();
                }
                catch (Exception) { }
            }

            //take a clone of the image so that we dont hold the plan open.
            if (img != null)
            {
                img = img.Clone();
            }

            package = null;
            plan = null;
            
            e.Result = img;
        }

        void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            var worker = (Csla.Threading.BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;

            PlanogramMetadataImage img  = null;
            if (e.Error == null)
            {
                img = e.Result as PlanogramMetadataImage;
            }
            this.Model = img;
        }

        #endregion


        #endregion
    }
}
