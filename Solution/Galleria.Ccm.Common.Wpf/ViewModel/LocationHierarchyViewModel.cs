﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25554 : L.Hodson
//  Copied from GFS
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of a LocationHierarchy model object.
    /// </summary>
    public sealed class LocationHierarchyViewModel : ViewStateObject<LocationHierarchy>
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public LocationHierarchyViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model property with 
        /// the hierarchy for the current entity
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new LocationHierarchy.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Saves the current model
        /// </summary>
        public void Save()
        {
            Update();
        }


        #endregion


    }
}
