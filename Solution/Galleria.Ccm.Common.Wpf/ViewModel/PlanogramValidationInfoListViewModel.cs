﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27045 : A.Silva ~ Added FetchByPlanogramId.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    ///     This class holds the viewmodel for an instance of the <see cref="PlanogramValidationInfoList"/> class.
    /// </summary>
    public sealed class PlanogramValidationInfoListViewModel : ViewStateListObject<PlanogramValidationInfoList, PlanogramValidationInfo>
    {
        /// <summary>
        ///     Fetches <see cref="PlanogramValidationInfo"/> for the given planogram Ids.
        /// </summary>
        /// <param name="planogramIds"></param>
        public void FetchByPlanogramIds(IEnumerable<Int32> planogramIds)
        {
            var criteria = new PlanogramValidationInfoList.FetchByPlanogramIdsCriteria(planogramIds);
            Fetch(criteria);
        }
    }
}
