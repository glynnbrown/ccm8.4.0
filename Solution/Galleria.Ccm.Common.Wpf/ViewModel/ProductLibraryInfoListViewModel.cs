﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM830)
// CCM-32361 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for ProductUniverseInfoList
    /// </summary>
    public sealed class ProductLibraryInfoListViewModel : ViewStateListObject<ProductLibraryInfoList, ProductLibraryInfo>
    {
        #region Constructor

        public ProductLibraryInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all Infos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchAllForEntity()
        {
            Fetch(new ProductLibraryInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Fetches the list of all Infos by the current entity id
        /// and loads the model asyncronously
        /// </summary>
        public void FetchAllForEntityAsync()
        {
            BeginFetch(new ProductLibraryInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
