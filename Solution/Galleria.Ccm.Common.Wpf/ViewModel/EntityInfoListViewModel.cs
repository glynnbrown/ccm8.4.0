﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class EntityInfoListViewModel : ViewStateListObject<EntityInfoList, EntityInfo>
    {
        #region Constructor

        public EntityInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all entityInfos by the current id
        /// and loads the model syncronously
        /// </summary>
        public void FetchAll()
        {
            Fetch(new EntityInfoList.FetchAllCriteria());
        }

        /// <summary>
        /// Fetches all entity for current entity
        /// asynchronously.
        /// </summary>
        public void FetchAsync()
        {
            var criteria = new EntityInfoList.FetchAllCriteria();

            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

    }
}
