﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25452 : N.Haywood
//	Created
// CCM-25395 : A.Probyn 
//  Created.
// CCM-26281 : L.Ineson
//  Merged togther versions from both clients.
#endregion
#endregion


using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for ProductUniverseInfoList
    /// </summary>
    public sealed class ProductUniverseInfoListViewModel : ViewStateListObject<ProductUniverseInfoList, ProductUniverseInfo>
    {
        #region Constructor

        public ProductUniverseInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all ProductUniverseInfos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchAllForEntity()
        {
            Fetch(new ProductUniverseInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
