﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson
//  Moved from editor.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System;
using System.IO;
using System.Diagnostics;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Provides a view of the UserEditorSettings model
    /// </summary>
    public sealed class UserEditorSettingsViewModel: ViewStateObject<UserEditorSettings>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserEditorSettingsViewModel()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model
        /// </summary>
        public new void Fetch()
        {
            base.Fetch(new UserEditorSettings.FetchCriteria());
        }

        /// <summary>
        /// Saves the current model.
        /// </summary>
        public void Save()
        {
            Update();
        }

        /// <summary>
        /// Creates the directories specifed by the current settings model.
        /// </summary>
        public void CreateDirectories()
        {
            //if (this.Model == null) return;

            //UserEditorSettings settings = this.Model;

            ////try to create the directories if they do not exist
            
            ////Labels:
            //if (!Directory.Exists(settings.LabelLocation))
            //{
            //    try
            //    {
            //        Directory.CreateDirectory(settings.LabelLocation);

            //        //Add a default product label.
            //        using (Label defaultProductLabel = Label.NewLabel(LabelType.Product))
            //        {
            //            defaultProductLabel.Name = "Product Default";
            //            defaultProductLabel.Text = "[PlanogramProduct.Gtin] [PlanogramProduct.Name]";
            //            defaultProductLabel.SaveAsFile(Path.ChangeExtension(Path.Combine(settings.LabelLocation, defaultProductLabel.Name), Label.FileExtension));
            //        }

            //    }
            //    catch (Exception) { }
            //}

            ////others
            //String[] directories = new String[] 
            //{ 
            //    settings.ImageLocation, 
            //    settings.FixtureLibraryLocation, 
            //    settings.HighlightLocation,
            //    settings.ValidationTemplateLocation,
            //    settings.ColumnLayoutLocation,
            //    settings.PlanogramFileTemplateLocation,
            //};
            //foreach (String path in directories)
            //{
            //    if (!Directory.Exists(path))
            //    {
            //        try 
            //        { 
            //            Directory.CreateDirectory(path); 
            //        }
            //        catch (Exception) { }
            //    }
            //}
        }

        #endregion

        #region Overrides

        protected override void OnDataPortalException(Exception error)
        {
            //something has gone wrong but as this is only a settings file its not the end of the world
            // so just keep calm and carry on.
        }

        #endregion
    }

    
}
