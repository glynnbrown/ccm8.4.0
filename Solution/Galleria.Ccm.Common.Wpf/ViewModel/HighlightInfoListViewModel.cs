﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 : J.Pickup
//    Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class HighlightInfoListViewModel : ViewStateListObject<HighlightInfoList, HighlightInfo>
    {
        #region Fields

        private readonly String _folderPath;

        #endregion

        #region Constructor

        public HighlightInfoListViewModel(String fileSystemPath)
        {
            _folderPath = fileSystemPath;
        }

        public HighlightInfoListViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model with a list of highlight infos
        /// </summary>
        public void FetchAll()
        {
            String folderPath = _folderPath;
            String ext = String.Format("{0}{1}", "*", Highlight.FileExtension);

            List<Object> ids = new List<Object>();
            if (Directory.Exists(folderPath))
            {
                foreach (String fileName in
                    Directory.GetFiles(folderPath, ext, SearchOption.TopDirectoryOnly))
                {
                    ids.Add(fileName);
                }
            }

            Fetch(new HighlightInfoList.FetchByIdsCriteria(HighlightDalFactoryType.FileSystem, ids));
        }

        /// <summary>
        /// Fetches the list of all <see cref="HighlightInfo"/> objects with the current entity Id and loads the model synchronously./>
        /// </summary>
        public void FetchAllForEntity()
        {
            var criteria = new HighlightInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        #endregion
    }
}
