﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of the ProductHierarchy model object
    /// </summary>
    public sealed class ProductHierarchyViewModel : ViewStateObject<ProductHierarchy>
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductHierarchyViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model with the merchandising hierarchy for the current entity
        /// </summary>
        public void FetchMerchandisingHierarchyForCurrentEntity()
        {
            Fetch(new ProductHierarchy.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Saves the current model
        /// </summary>
        public void Save()
        {
            Update();
        }

        #endregion

    }
}
