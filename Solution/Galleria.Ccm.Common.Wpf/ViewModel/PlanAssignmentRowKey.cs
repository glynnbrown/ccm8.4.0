﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.2)
// V8-29071 : M.Shelley
//  Initial version - Add a fix for the auto-refresh clearing the user's selection
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class PlanAssignmentRowKey
    {
        public Int32 KeyId { get; set; }
        public Int32 LocationPlanAssignmentId { get; set; }

        public PlanAssignmentRowKey(PlanAssignmentStoreSpaceInfo planAssignItem, LocationPlanAssignment locationPlanItem, 
                PlanAssignmentViewType viewType)
        {   
            if (planAssignItem != null)
            {
                if (viewType == PlanAssignmentViewType.ViewCategory)
                {
                    KeyId = (Int32) planAssignItem.LocationId;
                }
                else
                {
                    KeyId = planAssignItem.ProductGroupId;
                }
            }

            if (locationPlanItem != null)
            {
                LocationPlanAssignmentId = locationPlanItem.Id;
            }
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.KeyId.GetHashCode() ^
                   this.LocationPlanAssignmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanAssignmentRowKey other = obj as PlanAssignmentRowKey;
            if (other != null)
            {
                if (other.KeyId != this.KeyId) { return false; }
                if (other.LocationPlanAssignmentId != this.LocationPlanAssignmentId) { return false; }
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
