﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-28986 : M.Shelley
//  Created
#endregion

#region Version History: (CCM 8.2.0)
// V8-30508 : M.Shelley
//  Split the class and moved the PlanAssignmentPublishStatusType enum definition to the Galleria.Ccm.Model namespace
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public static class PlanAssignmentPublishStatusTypeHelper
    {
        public static readonly Dictionary<PlanAssignmentPublishStatusType, ImageSource> Images =
            new Dictionary<PlanAssignmentPublishStatusType, ImageSource>()
            {
                {PlanAssignmentPublishStatusType.NoPlan, null},
                {PlanAssignmentPublishStatusType.PlanNotPublished, null},
                {PlanAssignmentPublishStatusType.ProjectLinkNotPublished, null},
                {PlanAssignmentPublishStatusType.Successful, ImageResources.PlanAssign_PublishSuccessful},
                {PlanAssignmentPublishStatusType.ProjectLinkExceptions, ImageResources.PlanAssign_PublishWarning},
                {PlanAssignmentPublishStatusType.Failed, ImageResources.PlanAssign_PublishFailed},
                {PlanAssignmentPublishStatusType.ProjectLinkNotPublishedExternally, null},
                {PlanAssignmentPublishStatusType.PlanogramAltered, ImageResources.PlanAssign_PublishWarning},
            };

        public static readonly Dictionary<PlanAssignmentPublishStatusType, String> ToolTip =
            new Dictionary<PlanAssignmentPublishStatusType, String>()
            {
                {PlanAssignmentPublishStatusType.NoPlan, Message.PlanAssignmentPublishStatusType_NoPlan},
                {PlanAssignmentPublishStatusType.PlanNotPublished, Message.PlanAssignmentPublishStatusType_PlanNotPublished},
                {PlanAssignmentPublishStatusType.ProjectLinkNotPublished, Message.PlanAssignmentPublishStatusType_ProjectLinkNotPublished},
                {PlanAssignmentPublishStatusType.Successful, Message.PlanAssignmentPublishStatusType_PublishSuccessful},
                {PlanAssignmentPublishStatusType.ProjectLinkExceptions, Message.PlanAssignmentPublishStatusType_PublishProjectLinkExceptions},
                {PlanAssignmentPublishStatusType.Failed, Message.PlanAssignmentPublishStatusType_PublishFailed},
                {PlanAssignmentPublishStatusType.ProjectLinkNotPublishedExternally, Message.PlanAssignmentPublishStatusType_NotPublished},
                {PlanAssignmentPublishStatusType.PlanogramAltered, Message.PlanAssignmentPublishStatusType_PlanogramAltered},
          };
    }
}
