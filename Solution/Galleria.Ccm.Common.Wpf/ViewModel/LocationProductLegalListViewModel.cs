﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class LocationProductLegalListViewModel : ViewStateObject<LocationProductLegalList>
    {
        #region Constructor
        public LocationProductLegalListViewModel() { }
        #endregion

        #region Fetch

        /// <summary>
        /// Fetches all objects by current entityId and loads the model synchronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new LocationProductLegalList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion

        #region Save

        /// <summary>
        /// Save the current model synchronously
        /// </summary>
        public void Save()
        {
            Update();
        }

        #endregion
    }
}
