﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// CCM-25460 : L.Ineson
//  Created
// CCM-26281 : L.Ineson
//  Merged together both versions.
// V8-26284 : A.Kuszyk
//  Added AllGroups and RootGroup properties and MovePlanogramGroup method.
// V8-27966 : A.Silva
//      Added polling support.
//      Added CopyPlanogramGroup.
// V8-28209 : A.Silva
//      Amended OnModelChanged to clear _rootGroup, so it gets initialized again with the new model.
// V8-27280 : A.Silva
//      Added GetRecentWork to retrieve the latest modified Planogram Groups.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of the PlanogramHierarchy model object
    /// </summary>
    public sealed class PlanogramHierarchyViewModel : ViewStateObject<PlanogramHierarchy>
    {
        /// <summary>
        /// Whether the worker thread should continue after a data portal exception has been raised.
        /// </summary>
        private const Boolean ContinueAfterDataPortalException = true;

        #region Nested Enum PlanogramHierarchyPollingType

        private enum PlanogramHierarchyPollingType
        {
            None,
            FetchForCurrentEntity
        }

        #endregion

        #region Fields

        private PlanogramGroupViewModel _rootGroup;
        private PlanogramHierarchyPollingType _pollingType;

        #endregion

        #region Properties

        /// <summary>
        /// The view model for the Root Group in this hierarchy.
        /// </summary>
        public PlanogramGroupViewModel RootGroup
        {
            get
            {
                if (Model?.RootGroup == null) return null;

                return _rootGroup ?? (_rootGroup = new PlanogramGroupViewModel(Model.RootGroup, true));
            }
        }

        /// <summary>
        /// Returns true if this viewmodel is currently polling for data.
        /// </summary>
        public Boolean IsPolling { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramHierarchyViewModel()
        {
            ModelChanged += OnModelChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the model for this view is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnModelChanged(Object sender, EventArgs e)
        {
            _rootGroup = null;
        }

        #endregion

        #region Methods

        #region FetchForCurrentEntity

        /// <summary>
        /// Fetches the model for the currently connected entity.
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Int32 entityId = CCMClient.ViewState.EntityId;
            if (!CCMClient.ViewState.IsConnectedToRepository || entityId == 0) return;

            var criteria = new PlanogramHierarchy.FetchByEntityIdCriteria(entityId);
            try
            {
                Fetch(criteria);
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                // If there is an issue fetching the data, just ignore it for now as if we did not have a connection.
            }
        }

        /// <summary>
        /// Starts polling the fecth for current entity method.
        /// </summary>
        public void BeginPollingFetchForCurrentEntity()
        {
            if (!CCMClient.IsUnitTesting)
            {
                BeginPolling(PlanogramHierarchyPollingType.FetchForCurrentEntity, CCMClient.ViewState.EntityId);
            }
            else
            {
                //just update
                FetchForCurrentEntity();
            }
        }

        #endregion

        #region Planogram Group related

        /// <summary>
        /// Enumerates through all group views for the current hierarchy model.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramGroupViewModel> EnumerateAllGroupViews()
        {
            PlanogramGroupViewModel rootView = RootGroup;
            if (rootView == null) yield break;

            foreach (PlanogramGroupViewModel groupView in rootView.EnumerateAllChildGroups())
            {
                yield return groupView;
            }
        }

        public PlanogramGroupViewModel CopyPlanogramGroup(PlanogramGroupViewModel source, PlanogramGroupViewModel target)
        {
            if (source == null || target == null || Equals(source, target) || RootGroup == null) return null;

            PlanogramGroupViewModel copy = null;
            try
            {
                copy = new PlanogramGroupViewModel(source.PlanogramGroup.Copy(), true);
                target.AddGroup(copy);
            }
            catch (DataPortalException e)
            {
                CommonHelper.RecordException(e);
            }

            return copy;
        }

        public IEnumerable<PlanogramGroupViewModel> GetRecentWork(Int32 maxItems = 10)
        {
            return RootGroup?.EnumerateAllChildGroups().Except(new List<PlanogramGroupViewModel> { RootGroup })
                .OrderByDescending(o =>
                    o.PlanogramGroup.DateLastModified)
                .Take(maxItems)
                .Select(o =>
                    PlanogramGroupViewModel.NewRecentWorkGroup(o.PlanogramGroup)) ?? new List<PlanogramGroupViewModel>();
        }

        #endregion

        #region Polling Support

        /// <summary>
        /// Stops any ongoing polling operation 
        /// asynchronously.
        /// </summary>
        public void StopPollingAsync()
        {
            IsPolling = false;
            _pollingType = PlanogramHierarchyPollingType.None;
        }

        private void BeginPolling(PlanogramHierarchyPollingType pollingType, Object argument)
        {
            if (IsPolling) return;

            IsPolling = true;
            _pollingType = pollingType;

            BackgroundWorker pollingWorker = new BackgroundWorker();
            pollingWorker.WorkerReportsProgress = true;
            pollingWorker.DoWork += PollingWorker_DoWork;
            pollingWorker.ProgressChanged += PollingWorker_ProgressChanged;
            pollingWorker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
            pollingWorker.RunWorkerAsync(argument);
        }

        private void PollingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker pollingWorker = (BackgroundWorker)sender;

            while (IsPolling)
            {
                //update
                PlanogramHierarchy model = null;
                switch (_pollingType)
                {
                    case PlanogramHierarchyPollingType.None:
                        break;

                    case PlanogramHierarchyPollingType.FetchForCurrentEntity:
                        {
                            Int32? entityId = e.Argument as Int32?;
                            try
                            {
                                if (entityId.HasValue && entityId != 0)
                                    model = PlanogramHierarchy.FetchByEntityId(entityId.Value);
                            }
                            catch (DataPortalException)
                            {
                                CommonHelper.GetWindowService().ShowErrorMessage(Message.Application_Database_ConnectionLostHeader, Message.Application_Database_ConnectionLostDescription);
                                return;
                            }
                        }
                        break;

                    default:
                        Debug.Fail("Not handled");
                        break;
                }

                //report progress to send model
                pollingWorker.ReportProgress(100, model);
                model = null;

                //force a garbage collection for this thread.
                GC.Collect();

                //Wait
                Thread.Sleep(Constants.PollingPeriod);
            }
        }

        private void PollingWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PlanogramHierarchy newmodel = e.UserState as PlanogramHierarchy;
            if (newmodel != Model)
            {
                SetModel(ViewStateObjectModelChangeReason.Fetch, newmodel, null);
            }
        }

        private void PollingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker pollingWorker = (BackgroundWorker)sender;
            pollingWorker.DoWork -= PollingWorker_DoWork;
            pollingWorker.ProgressChanged -= PollingWorker_ProgressChanged;
            pollingWorker.RunWorkerCompleted -= PollingWorker_RunWorkerCompleted;
        }

        #endregion

        #endregion

        #region Overrides

        protected override void OnDataPortalException(Exception error)
        {
            //just log the error
            Debug.WriteLine(error.GetBaseException().Message);

            //notify gibraltar
            Gibraltar.Agent.Log.RecordException(error, null, ContinueAfterDataPortalException);
        }

        #endregion
    }
}