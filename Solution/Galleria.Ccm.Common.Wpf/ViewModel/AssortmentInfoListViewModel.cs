﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created
// CCM-26281 : L.Ineson
//  Merged from both clients.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for ProductUniverseInfoList
    /// </summary>
    public class AssortmentInfoListViewModel : ViewStateListObject<AssortmentInfoList, AssortmentInfo>
    {
        #region Constructor

        public AssortmentInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all AssortmentInfos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new AssortmentInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
