﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public class ConsumerDecisionTreeInfoListViewModel : ViewStateListObject<ConsumerDecisionTreeInfoList, ConsumerDecisionTreeInfo>
    {
        #region Constructors
        public ConsumerDecisionTreeInfoListViewModel() : base() { }
        #endregion

        #region Methods

        /// <summary>
        /// Populates this view with a list of all infos for the current entity
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new ConsumerDecisionTreeInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
