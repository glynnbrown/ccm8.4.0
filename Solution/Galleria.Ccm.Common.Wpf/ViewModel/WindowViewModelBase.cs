﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.01)
//V8-28878 : L.Ineson
// Initial Version
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Gibraltar.Agent;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// An abstract base class from which view model controllers are inherited
    /// </summary>
    public abstract class ControlViewModelBase : INotifyPropertyChanged, IDisposable
    {
        #region Fields
        private bool _isDisposed; // indicates if this instance has been disposed
        private FrameworkElement _attachedControl; // the attached control
        private readonly ObservableCollection<IRelayCommand> _viewModelCommands = new ObservableCollection<IRelayCommand>(); // a collection of relay commands associated with this view model object
        #endregion

        #region Events
        /// <summary>
        /// Notifies of a property value change on this object
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifies that the viewmodel has been disposed
        /// </summary>
        public event EventHandler Disposed;
        #endregion

        #region Properties
        /// <summary>
        /// Returns true if this instance has been disposed.
        /// </summary>
        public Boolean IsDisposed
        {
            get
            {
                return _isDisposed;
            }

            protected set
            {
                _isDisposed = value;
                if (value)
                {
                    if (this.Disposed != null)
                    {
                        this.Disposed(this, EventArgs.Empty);
                    }
                }
            }
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the attached control changes
        /// </summary>
        protected virtual void OnAttachedControlChanged(FrameworkElement oldControl, FrameworkElement newControl)
        {
            if (oldControl != null)
            {
                oldControl.KeyDown -= AttachedControl_KeyDown;
            }

            if (newControl != null)
            {
                newControl.KeyDown += AttachedControl_KeyDown;
            }
        }

        /// <summary>
        /// Responds to input gestures
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachedControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (_viewModelCommands == null) return;

            Key keyPressed = e.Key;
            ModifierKeys modifiers = Keyboard.Modifiers;

            //cycle through commands and execute as required
            foreach (IRelayCommand registeredCommand in _viewModelCommands.ToList())
            {
                if (registeredCommand.InputGestureModifiers == modifiers)
                {
                    if (registeredCommand.InputGestureKey == keyPressed)
                    {
                        if (registeredCommand.CanExecute(null))
                        {
                            registeredCommand.Execute(null);
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows/hide the wait cursor
        /// </summary>
        /// <param name="show"></param>
        protected void ShowWaitCursor(Boolean show)
        {
            if (_attachedControl == null) return;
            Mouse.OverrideCursor = (show) ? Cursors.Wait : null;
        }

        #region Attached Control Methods

        [Obsolete("Temporary, code should be refactored to not require this")]
        protected Boolean HasAttachedControl()
        {
            return (_attachedControl != null);
        }

        [Obsolete("Temporary, code should be refactored to not require this")]
        protected FrameworkElement GetAttachedControl()
        {
            return _attachedControl;
        }

        /// <summary>
        /// Extension method which returns true if there are no binding validation errors
        /// on the given window.
        /// </summary>
        protected Boolean AreBindingsValid()
        {
            if (_attachedControl == null) return true;
            return CommonHelper.AreBindingsValid(_attachedControl);
        }

        /// <summary>
        /// Register the given window with this viewmodel
        /// </summary>
        public virtual void RegisterControl(FrameworkElement control)
        {
            Debug.Assert(_attachedControl == null, "Control has already been registered!");

            FrameworkElement oldControl = _attachedControl;
            _attachedControl = control;
            OnAttachedControlChanged(oldControl, control);
        }

        /// <summary>
        /// Unregister the window attached to this viewmodel.
        /// </summary>
        public void UnregisterControl()
        {
            if (_attachedControl == null) return;

            FrameworkElement oldControl = _attachedControl;
            _attachedControl = null;

            OnAttachedControlChanged(oldControl, null);
        }

        #endregion

        #region Command Methods

        /// <summary>
        /// Registers the given command with this base.
        /// So it can be called using keyboard shortcuts.
        /// </summary>
        /// <param name="command"></param>
        protected void RegisterCommand(IRelayCommand command)
        {
            Debug.Assert(!_viewModelCommands.Contains(command),
                "{0} Command has already been registered", command.FriendlyName);

            _viewModelCommands.Add(command);
        }

        /// <summary>
        /// Creates a new command and registers it with this vm.
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        /// <param name="friendlyName"></param>
        /// <param name="friendlyDescription"></param>
        /// <param name="icon"></param>
        /// <param name="smallIcon"></param>
        /// <returns></returns>
        protected RelayCommand RegisterCommand(
            Action<object> execute, Predicate<object> canExecute,
            String friendlyName = null,
            String friendlyDescription = null,
            ImageSource icon = null,
             ImageSource smallIcon = null,
            Key? inputGestureKey = null,
            ModifierKeys modifierKeys = ModifierKeys.None)
        {
            RelayCommand command =
                (canExecute != null) ?
                new RelayCommand(execute, canExecute) :
                new RelayCommand(execute);

            command.FriendlyName = friendlyName;
            command.FriendlyDescription = friendlyDescription;
            command.Icon = icon;
            command.SmallIcon = smallIcon;
            command.InputGestureKey = inputGestureKey;
            command.InputGestureModifiers = modifierKeys;

            _viewModelCommands.Add(command);

            return command;
        }

        /// <summary>
        /// Creates a new command and registers it with this vm.
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        /// <param name="friendlyName"></param>
        /// <param name="friendlyDescription"></param>
        /// <param name="icon"></param>
        /// <param name="smallIcon"></param>
        /// <returns></returns>
        protected RelayCommand RegisterCommand(Action<object> execute,
            String friendlyName = null,
            String friendlyDescription = null,
            ImageSource icon = null,
             ImageSource smallIcon = null)
        {
            RelayCommand command =
                new RelayCommand(execute)
                {
                    FriendlyName = friendlyName,
                    FriendlyDescription = friendlyDescription,
                    Icon = icon,
                    SmallIcon = smallIcon
                };

            _viewModelCommands.Add(command);

            return command;
        }

        public enum CommonCommand
        {
            New,
            Save,
            Delete,
            CloseWindow,
            CancelDialog
        }

        protected RelayCommand RegisterCommand(CommonCommand type,
            Action<object> execute, Predicate<object> canExecute = null)
        {
            switch (type)
            {
                case CommonCommand.CancelDialog:
                    return RegisterCommand(execute, canExecute,
                        friendlyName: Message.Generic_Cancel);

                case CommonCommand.CloseWindow:
                    return RegisterCommand(execute, canExecute,
                    friendlyName: Message.Generic_Close,
                    smallIcon: ImageResources.Close_16,
                    inputGestureKey: Key.F4,
                    modifierKeys: ModifierKeys.Control);

                case CommonCommand.Delete:
                    return RegisterCommand(execute, canExecute,
                    friendlyName: Message.Generic_Delete,
                    smallIcon: ImageResources.Delete_16);

                case CommonCommand.New:
                    return RegisterCommand(execute, canExecute,
                    friendlyName: Message.Generic_New,
                    friendlyDescription: Message.Generic_New_Tooltip,
                    icon: ImageResources.NewDocument_32,
                    smallIcon: ImageResources.NewDocument_16,
                    inputGestureKey: Key.N,
                    modifierKeys: ModifierKeys.Control);

                case CommonCommand.Save:
                    return RegisterCommand(execute, canExecute,
                    friendlyName: Message.Generic_Save,
                    friendlyDescription: Message.Generic_Save_Tooltip,
                    icon: ImageResources.Save_32,
                    smallIcon: ImageResources.Save_16,
                    inputGestureKey: Key.S,
                    modifierKeys: ModifierKeys.Control);

                default: throw new NotImplementedException();
            }

        }


        #endregion

        /// <summary>
        /// Returns the active window service.
        /// </summary>
        protected IWindowService GetWindowService()
        {
            return Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>();
        }

        /// <summary>
        /// Records that the given exception has occurred
        /// </summary>
        /// <param name="ex"></param>
        protected void RecordException(Exception ex, String exceptionCategory = "Handled")
        {
            Debug.WriteLine(ex.Message);

            //notify gibraltar
            Log.RecordException(ex, exceptionCategory, /*canContinue*/true);

            //[TODO] place the exception message in some sort of local event log?
        }

        /// <summary>
        /// Returns the property path
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        protected static PropertyPath GetPropertyPath<TViewModel>(Expression<Func<TViewModel, object>> expression)
        {
            return WpfHelper.GetPropertyPath<TViewModel>(expression);
        }

        #endregion

        #region INotifyPropertyChanged

        /// <summary>
        /// INotify Property changed interface member
        /// </summary>
        /// <param name="propertyPath"></param>
        /// <remarks>Should this be protected rather than public?</remarks>
        public void OnPropertyChanged(PropertyPath propertyPath)
        {
            OnPropertyChanged(propertyPath.Path);
        }
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDisposable
        /// <summary>
        /// IDisposable interface member
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            UnregisterControl();

            IsDisposed = true;
        }

        [Obsolete]
        protected void DisposeBase()
        {
            UnregisterControl();
        }

        #endregion
    }

    /// <summary>
    /// Abstract class intended to be a base for all window controller viewmodels.
    /// </summary>
    public abstract class WindowViewModelBase : ControlViewModelBase
    {
        #region Fields
        private Window _attachedWindow; // the attached window
        private Boolean _isWindowCloseHandled;
        #endregion

        #region Methods

        public override void RegisterControl(FrameworkElement control)
        {
            Window window = control as Window;
            if (window == null) throw new ArgumentException();

            base.RegisterControl(control);
        }

        /// <summary>
        /// Register the given window with this viewmodel
        /// </summary>
        public void RegisterWindowControl(Window window)
        {
            RegisterControl(window);
        }

        /// <summary>
        /// Unregister the window attached to this viewmodel.
        /// </summary>
        public void UnregisterWindowControl()
        {
            UnregisterControl();
        }

        /// <summary>
        /// Closes the ribbon backstage of the window control
        /// attached to this viewmodel.
        /// </summary>
        protected void CloseRibbonBackstage()
        {
            GetWindowService().CloseRibbonBackstage(_attachedWindow);
        }

        /// <summary>
        /// Opens the ribbon backstage of the window control
        /// attached to this viewmodel.
        /// </summary>
        protected void OpenRibbonBackstage()
        {
            GetWindowService().OpenRibbonBackstage(_attachedWindow);
        }

        /// <summary>
        /// Closes the attached window.
        /// </summary>
        protected void CloseWindow()
        {
            CancelEventArgs cancelArgs = new CancelEventArgs();
            OnWindowClosing(cancelArgs, /*isCrossClick*/false);

            if (!cancelArgs.Cancel)
            {
                _isWindowCloseHandled = true;
                GetWindowService().CloseWindow(_attachedWindow);
            }
        }

        /// <summary>
        /// Closes the attached dialog and sets the dialog result.
        /// </summary>
        /// <param name="dialogResult"></param>
        protected void CloseDialog(Boolean? dialogResult)
        {
            if (_attachedWindow != null)
            {
                _attachedWindow.DialogResult = dialogResult;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the attached control changes
        /// </summary>
        protected override void OnAttachedControlChanged(FrameworkElement oldControl, FrameworkElement newControl)
        {
            base.OnAttachedControlChanged(oldControl, newControl);

            Window oldWin = oldControl as Window;
            if (oldWin != null)
            {
                oldWin.Closing -= AttachedControl_Closing;
            }

            Window newWin = newControl as Window;
            _attachedWindow = newWin;
            if (newWin != null)
            {
                newWin.Closing += AttachedControl_Closing;
            }
        }

        /// <summary>
        /// Called when the attached window notifies that it is closing.
        /// </summary>
        private void AttachedControl_Closing(object sender, CancelEventArgs e)
        {
            if (_isWindowCloseHandled) return;

            OnWindowClosing(e, /*isCrossClick*/true);

            if (!e.Cancel) _isWindowCloseHandled = true;
        }

        /// <summary>
        /// Called when the window is about to close.
        /// </summary>
        protected virtual void OnWindowClosing(CancelEventArgs e, Boolean isCrossClick)
        {
            // override as required
        }

        #endregion

    }
}
