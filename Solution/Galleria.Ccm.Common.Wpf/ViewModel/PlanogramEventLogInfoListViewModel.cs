﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 810)
// V8-29861 : A.Probyn
//  Created
#endregion

#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents  a view of a list of planogram event log infos.
    /// </summary>
    public sealed class PlanogramEventLogInfoListViewModel : ViewStateListObject<PlanogramEventLogInfoList, PlanogramEventLogInfo>
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramEventLogInfoListViewModel() { }
        #endregion

        #region Methods

        #region FetchByWorkpackageId

        /// <summary>
        /// Fetches plan event log infos for all plans belonging to the given workpackage id.
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByWorkpackageId(Int32 workpackageId)
        {
            Fetch(new PlanogramEventLogInfoList.FetchByWorkpackageIdCriteria(workpackageId));
        }

        /// <summary>
        /// Fetches plan event log infos for all plans belonging to the given workpackage id.
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByWorkpackageIdAsync(Int32 workpackageId)
        {
            var criteria = new PlanogramEventLogInfoList.FetchByWorkpackageIdCriteria(workpackageId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #endregion

    }
}
