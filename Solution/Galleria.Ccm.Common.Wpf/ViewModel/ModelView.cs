﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// V8-26170 : A.Silva ~ Ported from Editor.Client.Wpf.

#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;


namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Represents a view of a model object
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class ModelView<TModel> : INotifyPropertyChanged, IDisposable
    {
        #region Fields

        #endregion

        #region Properties

        public TModel Model { get; private set; }

        #endregion

        #region Constructor

        public ModelView(TModel model)
        {
            Model = model;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyPath property)
        {
            OnPropertyChanged(property.Path);
        }

        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public abstract void Dispose();

        #endregion
    }
}
