﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25460 : L.Hodson ~ Created.
// V8-25664 : A.Kuszyk ~ Added IsExpanded, IsMyCategories and ToolTip properties
// V8-26284 : A.Kuszyk ~ Moved to Galleria.Ccm.Common.Wpf.
// V8-26533 : A.Kuszyk ~ Added NotifyPropertyChanged to Name property.
// V8-27280 : A.Silva  ~ Amended Id Property to return 0 if there is no underlying Planogram Group.
// V8-28286 : A.Silva
//  Amended NewRecentWorkGroup so that a fake planogram with Id 0 is returned as the root of Recent Work Groups.
// V8-28337 : A.Silva
//  Amended Move to ensure no copying over oneself.
// V8-28597 : D.Pleasance
//  Amended constructor so that childViews is initialised even if child groups are not added.

#endregion

#region Version History : CCM 802
// V8-27433 : A.Kuszyk
//  Removed Tooltip property (moved functionality to PlanogramGroup.GetFriendlyFullPath().
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// A view of an available planogram group
    /// </summary>
    public sealed class PlanogramGroupViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isRecentWorkGroup;
        private PlanogramGroup _planogramGroup;
        private String _name;
        private readonly BulkObservableCollection<PlanogramGroupViewModel> _childViews; //holds child views, only created if required.
        private Boolean _isExpanded;

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this group is a My Categories favourite group.
        /// </summary>
        public Boolean IsMyCategoriesGroup { get; private set; }

        /// <summary>
        /// Returns true if this is the
        /// recent work group.
        /// </summary>
        public Boolean IsRecentWorkGroup
        {
            get { return _isRecentWorkGroup; }
        }

        /// <summary>
        /// Returns the planogram group
        /// </summary>
        public PlanogramGroup PlanogramGroup
        {
            get { return _planogramGroup; }
        }

        /// <summary>
        /// Gets the group name
        /// </summary>
        public String Name
        {
            get { return _name; }
            set
            {
                _name = value;
                PlanogramGroup.Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The Id of the Planogram Group.
        /// </summary>
        public Int32 Id
        {
            get
            {
                return _planogramGroup != null 
                    ? _planogramGroup.Id 
                    : 0;
            }
        }

        /// <summary>
        /// Returns a list of the children of this group.
        /// </summary>
        public BulkObservableCollection<PlanogramGroupViewModel> ChildViews
        {
            get { return _childViews; }
        }

        /// <summary>
        /// Indicates whether or not the Tree View Item this model represents
        /// is expanded or not. This property is bound to the control.
        /// </summary>
        public Boolean IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                NotifyPropertyChanged("IsExpanded");
            }
        }

        #endregion

        #region Constructors

        private PlanogramGroupViewModel() { }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="addChildGroups"></param>
        public PlanogramGroupViewModel(PlanogramGroup group, Boolean addChildGroups)
        {
            _planogramGroup = group;
            _name = group.Name;
            _childViews = new BulkObservableCollection<PlanogramGroupViewModel>();

            if (addChildGroups)
            {
                foreach (PlanogramGroup child in group.ChildList.OrderBy(s => s.Name))
                {
                    _childViews.Add(new PlanogramGroupViewModel(child, addChildGroups));
                }
            }
        }

        #endregion

        #region FactoryMethods

        /// <summary>
        /// Creates a new My Categories Group.
        /// </summary>
        /// <param name="group">The Planogram Group to use to create a View Model for.</param>
        /// <param name="addChildGroups">True indicates child groups should be added.</param>
        /// <returns>A new PlanogramGroupSelectorView.</returns>
        public static PlanogramGroupViewModel NewMyCategoriesGroup(PlanogramGroup group, Boolean addChildGroups)
        {
            var view = new PlanogramGroupViewModel(group, addChildGroups);
            view.IsMyCategoriesGroup = true;
            return view;
        }

        /// <summary>
        /// Creates a recent work group.
        /// </summary>
        /// <returns></returns>
        public static PlanogramGroupViewModel NewRecentWorkGroup(PlanogramGroup group = null)
        {
            if (group != null) return new PlanogramGroupViewModel(group, true) { _isRecentWorkGroup = true };

            // If no group was supplied, return the root Recent Work Group.
            group = PlanogramGroup.NewPlanogramGroup();
            group.Id = 0;
            group.Name = Message.PlanogramHierarchySelector_RecentWorkGroup;
            return new PlanogramGroupViewModel(group, true) {_isRecentWorkGroup = true};
        }

        /// <summary>
        /// Creates a new display only group
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static PlanogramGroupViewModel NewDisplayGroup(String name, Boolean isMyCategoriesGroup = false)
        {
            var view = new PlanogramGroupViewModel();
            view._name = name;
            view.IsMyCategoriesGroup = isMyCategoriesGroup;
            return view;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Moves this Planogram Group to a new parent, ensuring that both the Models and
        /// View Models are updated appropriately.
        /// </summary>
        /// <param name="oldParent">The current parent of this Group.</param>
        /// <param name="newParent">The target parent for this Group.</param>
        /// <returns>True if operation was a success.</returns>
        public Boolean Move(PlanogramGroupViewModel oldParent, PlanogramGroupViewModel newParent)
        {
            if (oldParent == null || Equals(oldParent, newParent)) return false;

            oldParent.RemoveGroup(this,false);
            _planogramGroup = this.PlanogramGroup.Move<PlanogramGroupList>(oldParent.PlanogramGroup.ChildList, newParent.PlanogramGroup.ChildList);
            newParent.AddGroup(this,false);

            newParent.SortChildViews();
            return true;
        }

        /// <summary>
        /// Adds an item to the ChildViews list, maintaining the list in sorted
        /// name order.
        /// </summary>
        public void AddGroup(PlanogramGroupViewModel item, Boolean addModel=true)
        {
            Debug.Assert(_childViews != null, "Trying to add a group when child views not loaded");
            if (_childViews == null) return;
            if (_childViews.Contains(item)) return;
            if (addModel)
            {
                PlanogramGroup.ChildList.Add(item.PlanogramGroup);
            }
            _childViews.Add(item);
        }


        public void SortChildViews()
        {
            Debug.Assert(_childViews != null, "Trying to sort when child views not loaded");
            if (_childViews == null) return;

            var orderedList = _childViews.OrderBy(g => g.Name).ToList();
            _childViews.Clear();
            _childViews.AddRange(orderedList);
        }

        /// <summary>
        /// Removes an item from the ChildViews list, maintaining control of the
        /// expansion state of this item.
        /// </summary>
        public void RemoveGroup(PlanogramGroupViewModel item, Boolean removeModel=true)
        {
            Debug.Assert(_childViews != null, "Trying to remove a group when child views not loaded");
            if (_childViews == null) return;

            if (!_childViews.Contains(item)) return;

            if (removeModel)
            {
                PlanogramGroup.ChildList.Remove(item.PlanogramGroup);
            }
            _childViews.Remove(item);

            if (_childViews.Count == 0)
            {
                IsExpanded = false;
            }
        }

        /// <summary>
        /// Enumerates through this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramGroupViewModel> EnumerateAllChildGroups()
        {
            if (_childViews == null) yield break;

            yield return this;

            foreach (var child in this.ChildViews)
            {
                foreach (var g in child.EnumerateAllChildGroups())
                {
                    yield return g;
                }
            }
        }

        /// <summary>
        /// Returns the name of the PlanogramGroup.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return PlanogramGroup.Name;
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Equals Override

        public override bool Equals(object obj)
        {
            PlanogramGroupViewModel other = obj as PlanogramGroupViewModel;
            if (other == null) return false;

            if (other.PlanogramGroup != this.PlanogramGroup) return false;
            if (other.IsRecentWorkGroup != this.IsRecentWorkGroup) return false;
            if (other.IsMyCategoriesGroup != this.IsMyCategoriesGroup) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return 
                (this.PlanogramGroup != null)? this.PlanogramGroup.GetHashCode(): 0 ^ 
                this.IsRecentWorkGroup.GetHashCode() ^
                this.IsMyCategoriesGroup.GetHashCode();
        }

        #endregion

    }

    /// <summary>
    /// Container for the PlanogramGroupView transferred during a drag & drop.
    /// </summary>
    public class PlanogramGroupDropEventArgs : EventArgs
    {
        public PlanogramGroupViewModel PlanogramGroupView { get; set; }

        public PlanogramGroupDropEventArgs(PlanogramGroupViewModel planogramGroupView)
        {
            PlanogramGroupView = planogramGroupView;
        }
    }
}
