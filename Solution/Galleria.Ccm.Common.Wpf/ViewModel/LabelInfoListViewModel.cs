﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27940 : L.Luong
//    Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.IO;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class LabelInfoListViewModel : ViewStateListObject<LabelInfoList, LabelInfo>
    {
        #region Fields
        private readonly String _folderPath;
        #endregion

        #region Constructor
        public LabelInfoListViewModel(String fileSystemPath)
            : this()
        {
            _folderPath = fileSystemPath;
        }

        public LabelInfoListViewModel(){}
        #endregion

        #region Methods

        /// <summary>
        /// Populates the model with a list of label infos
        /// </summary>
        public void FetchAll()
        {
            String folderPath = _folderPath;
            String ext = String.Format("{0}{1}", "*", Label.FileExtension);

            List<Object> ids = new List<Object>();
            if (Directory.Exists(folderPath))
            {
                foreach (String fileName in
                    Directory.GetFiles(folderPath, ext, SearchOption.TopDirectoryOnly))
                {
                    ids.Add(fileName);
                }
            }

            Fetch(new LabelInfoList.FetchByIdsCriteria(LabelDalFactoryType.FileSystem, ids));
        }

        /// <summary>
        /// Populates the model with a list of label infos
        /// </summary>
        public void FetchAllByType(LabelType type)
        {
            String folderPath = _folderPath;
            String ext = String.Format("{0}{1}", "*", Label.FileExtension);

            List<Object> ids = new List<Object>();
            if (Directory.Exists(folderPath))
            {
                foreach (String fileName in
                    Directory.GetFiles(folderPath, ext, SearchOption.TopDirectoryOnly))
                {
                    ids.Add(fileName);
                }
            }

            Fetch(new LabelInfoList.FetchByTypeCriteria(LabelDalFactoryType.FileSystem, ids, type));
        }

        /// <summary>
        ///     Fetches the list of all <see cref="LabelInfo"/> objects with the current entity Id and loads the model synchronously./>
        /// </summary>
        public void FetchAllForEntity()
        {
            var criteria = new LabelInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        #endregion
    }    
}
