﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for LocationInfoList
    /// </summary>
    public sealed class LocationInfoListViewModel : ViewStateListObject<LocationInfoList, LocationInfo>
    {
        #region Constructor

        public LocationInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all locationInfos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchAllForEntity()
        {
            Fetch(new LocationInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Fetches all locations for current entity
        /// asynchronously.
        /// </summary>
        public void FetchForCurrentEntityAsync()
        {
            var criteria = new LocationInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion
    }
}
