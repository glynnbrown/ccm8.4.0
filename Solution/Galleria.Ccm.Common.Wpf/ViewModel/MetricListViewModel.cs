﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26158 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Metric List View Model
    /// </summary>
    public class MetricListViewModel : ViewStateListObject<MetricList, Metric>
    {
        #region Constructor

        public MetricListViewModel()
        { }
         #endregion

        #region Methods
        /// <summary>
        /// Populate the model for the current enitty synchronously
        /// </summary>
        public void FetchForEntity()
        {
            MetricList.FetchByEntityIdCriteria criteria = 
                new MetricList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);

            Fetch(criteria);
        }
        /// <summary>
        /// save the model synchronously
        /// </summary>
        public void Save()
        {
            Update();
        }
        #endregion

    }
}
