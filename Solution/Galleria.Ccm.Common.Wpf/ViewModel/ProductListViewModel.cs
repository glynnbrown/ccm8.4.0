﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-27150 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for ProductList
    /// </summary>
    public sealed class ProductListViewModel : ViewStateListObject<ProductList, Product>
    {
        #region Constructor
        public ProductListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Performs an async search for products based on the given criteria.
        /// </summary>
        /// <param name="criteria"></param>
        public void FetchForCurrentEntityBySearchCriteriaAsync(String searchCriteria)
        {
            var criteria = new ProductList.FetchByEntityIdSearchTextCriteria(CCMClient.ViewState.EntityId, searchCriteria);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        /// <summary>
        /// Performs an async search for products based on the given criteria.
        /// </summary>
        /// <param name="criteria"></param>
        public void FetchForCurrentEntityByMultipleSearchCriteriaAsync(String searchCriteria)
        {
            List<String> searches = new List<String>();

            foreach (String s in searchCriteria.Split(new String[] { System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator }, StringSplitOptions.None).ToList())
            {
                if (!String.IsNullOrWhiteSpace(s))
                {
                    searches.Add(s.Trim());
                }
            }
            
            var criteria = new ProductList.FetchByEntityIdMultipleSearchTextCriteria(CCMClient.ViewState.EntityId, searches);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        /// <summary>
        /// Returns all products for the given category id
        /// As a category can have multiple product universes this will return a distinct list from all of them.
        /// </summary>
        /// <param name="merchGroupId"></param>
        public void FetchByCategoryIdAsync(Int32 productGroupId)
        {
            var criteria = new ProductList.FetchByMerchandisingGroupIdCriteria(productGroupId);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        #endregion
    }
}
