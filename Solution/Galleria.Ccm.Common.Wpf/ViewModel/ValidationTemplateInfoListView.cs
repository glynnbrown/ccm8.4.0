﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    ///     View holder for the <see cref="ValidationTemplateInfoList"/> type.
    /// </summary>
    public sealed class ValidationTemplateInfoListView : ViewStateListObject<ValidationTemplateInfoList, ValidationTemplateInfo>
    {
        /// <summary>
        ///     Fetches the list of all <see cref="ValidationTemplateInfo"/> objects with the current entity Id and loads the model synchronously./>
        /// </summary>
        public void FetchAllForEntity()
        {
            var criteria = new ValidationTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        /// <summary>
        ///     Fetches the list of all <see cref="ValidationTemplateInfo"/> objects with the current entity Id and loads the model asynchronously./>
        /// </summary>
        public void FetchAllForEntityAsync()
        {
            var criteria = new ValidationTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }
    }
}
