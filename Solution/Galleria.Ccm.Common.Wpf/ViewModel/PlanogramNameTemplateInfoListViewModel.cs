﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//	Created
#endregion
#endregion


using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for PlanogramNameTemplateInfo
    /// </summary>
    public sealed class PlanogramNameTemplateInfoListViewModel : ViewStateListObject<PlanogramNameTemplateInfoList, PlanogramNameTemplateInfo>
    {
        #region Constructor

        public PlanogramNameTemplateInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all PlanogramNameTemplateInfos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchAllForEntity()
        {
            Fetch(new PlanogramNameTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}