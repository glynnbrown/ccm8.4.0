﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-26157 : L.Ineson
//  Created
#endregion

#region Version History: (CCM 8.0.1)
// V8-27937 : M.Shelley
//  Added code to allow a programmatic check to test if there is a valid GFS connection, 
//  without any windows or popup dialogs.
//  This has been modified so that if changes are made in the systems administration page
//  the connection is re-checked
#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of an Entity model object
    /// </summary>
    public sealed class EntityViewModel : ViewStateObject<Entity>
    {
        #region Nested classes
        /// <summary>
        /// Denotes the possible connection states
        /// </summary>
        public enum GFSConnectionState
        {
            Unknown,
            PathNotSet,
            EntityNotSet,
            CouldNotConnect,
            EntityNotFound,
            Successful
        }

        /// <summary>
        /// Simple class used to pass arguments 
        /// when checking the gfs connection.
        /// </summary>
        public class ConnectionCheckWorkArgs
        {
            public Int32 EntityId { get; set; }
            public Entity Entity { get; set; }
            public GFSConnectionState Result { get; set; }
            public Exception Error { get; set; }

            public ConnectionCheckWorkArgs(Int32 entityId, Entity entityModel)
            {
                EntityId = entityId;
                Entity = entityModel;
            }
        }

        #endregion

        #region Fields
        private GFSConnectionState _servicesConnectionState;
        private ModalBusy _busy;
        private Boolean? _isValidGFSConnection = null;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the state of the GFS connection.
        /// </summary>
        public GFSConnectionState ServicesConnectionState
        {
            get { return _servicesConnectionState; }
            private set { _servicesConnectionState = value; }
        }

        #endregion

        public Boolean? IsValidGFSConnection
        {
            get { return _isValidGFSConnection;  }
            set { _isValidGFSConnection = value; }
        }

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public EntityViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the current entity
        /// </summary>
        public void FetchCurrent()
        {
            Int32 entityId = CCMClient.ViewState.EntityId;
            if (entityId != 0)
            {
                Fetch(new Entity.FetchByIdCriteria(entityId));
            }
            else
            {
                SetModel(ViewStateObjectModelChangeReason.Fetch, null, null);
            }
        }

        /// <summary>
        /// Saves the entity
        /// </summary>
        public void Save()
        {
            Update();
        }

        #endregion

        #region GFS Connection Checking

        /// <summary>
        /// Starts checking the connection to the given path.
        /// </summary>
        /// <param name="connectionPath"></param>
        public void CheckGFSConnectionState()
        {
            this.ServicesConnectionState = GFSConnectionState.Unknown;

            ModalBusy msg = new ModalBusy()
            {
                Header = Message.GFSConnectionHelper_CheckingHeader,
                Description = Message.GFSConnectionHelper_CheckingDescription,
                IsDeterminate = false
            };
            _busy = msg;

            var args = new ConnectionCheckWorkArgs(CCMClient.ViewState.EntityId, this.Model);

            Csla.Threading.BackgroundWorker worker = new Csla.Threading.BackgroundWorker();
            worker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync(args);

            WindowHelper.ShowWindow(msg, true);
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            ConnectionCheckWorkArgs args = (ConnectionCheckWorkArgs)e.Argument;

            //if the entity has not yet been populated then fetch it.
            if (args.Entity == null)
            {
                args.Entity = Entity.FetchById(args.EntityId);
            }

            //check that a gfs entity id is linked.
            if (args.Entity.GFSId.HasValue)
            {
                String connectionPath = args.Entity.SystemSettings.FoundationServicesEndpoint;
                if (String.IsNullOrEmpty(connectionPath))
                {
                    //No connection path has been provided.
                    args.Result = GFSConnectionState.PathNotSet;
                }
                else
                {
                    try
                    {
                        //try to fetch the list of GFS entities as a connection test.
                        GFSModel.EntityList gfsEntities = GFSModel.EntityList.FetchAll(CCMClient.ViewState.EntityId);

                        if (gfsEntities.Count == 0 || !gfsEntities.Select(g => g.Id).Contains(args.Entity.GFSId.Value))
                        {
                            //The gfs entity did not exist at the given path.
                            args.Result = GFSConnectionState.EntityNotFound;
                        }
                        else
                        {
                            args.Result = GFSConnectionState.Successful;
                        }
                    }
                    catch (Exception ex)
                    {
                        args.Result = GFSConnectionState.CouldNotConnect;
                        args.Error = ex.GetBaseException();
                    }

                }
            }
            else
            {
                //No GFS entity has been linked to this one.
                args.Result = GFSConnectionState.EntityNotSet;
            }


            e.Result = args;
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            var worker = (Csla.Threading.BackgroundWorker)sender;

            String connectionError = null;

            if (e.Error != null)
            {
                this.ServicesConnectionState = GFSConnectionState.CouldNotConnect;
                connectionError = e.Error.GetBaseException().Message;
            }
            else
            {
                ConnectionCheckWorkArgs args = (ConnectionCheckWorkArgs)e.Result;
                
                this.ServicesConnectionState = args.Result;

                switch (this.ServicesConnectionState)
                {
                    case GFSConnectionState.EntityNotFound:
                        connectionError = Message.GFSConnectionHelper_EntityNotFound;
                        break;

                    case GFSConnectionState.EntityNotSet:
                        connectionError = Message.GFSConnectionHelper_EntityNotSet;
                        break;

                    case GFSConnectionState.PathNotSet:
                        connectionError = Message.GFSConnectionHelper_PathNotSet;
                        break;

                    case GFSConnectionState.CouldNotConnect:
                        connectionError = args.Error.Message;
                        break;
                }

                //update the entity model if required
                if (this.Model == null && args.Entity != null)
                {
                    SetModel(ViewStateObjectModelChangeReason.Fetch, args.Entity, null);
                }

            }

            _busy.Close();
            _busy = null;

            //show the error dialog
            if (!String.IsNullOrEmpty(connectionError))
            {

                WindowHelper.ShowWindow(
                    new ModalMessage()
                    {
                        Header = Message.GFSConnectionHelper_ConnectionFailed,
                        Description = connectionError,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_Ok,
                        MessageIcon = ImageResources.CriticalError_32

                    }, true);

            }

        }

        public delegate void EntityViewmodelDelegate(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e);

        /// <summary>
        /// A helper method that allows the use of an external callback delegate to determine if there
        /// is a current valid GFS connection.
        /// </summary>
        /// <param name="completedHandler"></param>
        public void ValidateGFSConnectionState(EntityViewmodelDelegate completedHandler)
        {
            this.ServicesConnectionState = GFSConnectionState.Unknown;

            var args = new ConnectionCheckWorkArgs(CCMClient.ViewState.EntityId, this.Model);

            Csla.Threading.BackgroundWorker worker = new Csla.Threading.BackgroundWorker();
            worker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(completedHandler);
            worker.RunWorkerAsync(args);
        }

        #endregion
    }
}
