﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System.ComponentModel;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for UserInfoList
    /// </summary>
    public sealed class UserInfoListViewModel : ViewStateListObject<UserInfoList, UserInfo>
    {
        #region Constructor

        public UserInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all UserInfos
        /// and loads the model syncronously
        /// </summary>
        public void FetchAll()
        {
            Fetch(new UserInfoList.FetchAllCriteria());
        }

        #endregion

    }
}
