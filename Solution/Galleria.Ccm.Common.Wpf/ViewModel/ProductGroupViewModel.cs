﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//  Copied from SA
// CCM-26281 : L.Ineson
//  Merged together versions from both clients.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// View of a product hierarchy group 
    /// - simpler then product group viewmodel as has no event handlers so does not need disposing.
    /// </summary>
    public sealed class ProductGroupViewModel : INotifyPropertyChanged
    {
        #region Fields
        private String _groupLabelOverride = null;
        private ProductLevel _associatedLevel;
        private Dictionary<String, ProductGroup> _levelPathValues;
        #endregion

        #region Binding Property paths

        public static readonly PropertyPath ProductGroupProperty = WpfHelper.GetPropertyPath<ProductGroupViewModel>(p => p.ProductGroup);
        public static readonly PropertyPath ProductGroupLabelProperty = WpfHelper.GetPropertyPath<ProductGroupViewModel>(p => p.ProductGroupLabel);
        public static readonly PropertyPath AssociatedLevelProperty = WpfHelper.GetPropertyPath<ProductGroupViewModel>(p => p.AssociatedLevel);
        public static readonly PropertyPath LevelPathValuesProperty = WpfHelper.GetPropertyPath<ProductGroupViewModel>(p => p.LevelPathValues);
        public static readonly PropertyPath AssociatedLevelNumberProperty = WpfHelper.GetPropertyPath<ProductGroupViewModel>(p => p.AssociatedLevelNumber);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the original product group model
        /// </summary>
        public ProductGroup ProductGroup
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns a string containing of format Code : Name + overrides for null productGroup, Root etc
        /// </summary>
        public String ProductGroupLabel
        {
            get
            {
                if (!String.IsNullOrEmpty(_groupLabelOverride))
                {
                    return _groupLabelOverride;
                }

                else if (this.ProductGroup != null)
                {
                    return this.ProductGroup.GroupLabel;
                }

                else
                {
                    //return uncategorized label
                    return Message.ProductGroup_NullProductGroupLabel;
                }
            }
            set
            {
                _groupLabelOverride = value;
                OnPropertyChanged(ProductGroupLabelProperty);
            }
        }

        /// <summary>
        /// Returns the level object associated to the source group
        /// </summary>
        public ProductLevel AssociatedLevel
        {
            get { return _associatedLevel; }
            private set
            {
                _associatedLevel = value;
                OnPropertyChanged(AssociatedLevelProperty);
                OnPropertyChanged(AssociatedLevelNumberProperty);
            }
        }

        /// <summary>
        /// Returns the level number to which the group is associated
        /// </summary>
        public Int32 AssociatedLevelNumber
        {
            get
            {
                if (this.AssociatedLevel != null)
                {
                    ProductHierarchy parentHierarchy = this.AssociatedLevel.ParentHierarchy;
                    if (parentHierarchy != null)
                    {
                        Int32 levelNo = 1;
                        ProductLevel level = parentHierarchy.RootLevel;

                        while (level != null && level != this.AssociatedLevel)
                        {
                            level = level.ChildLevel;
                            levelNo++;
                        }

                        return levelNo;
                    }
                }
                return 0;
            }
        }

        /// <summary>
        /// Returns the dictionary collection of level path values
        /// </summary>
        public Dictionary<String, ProductGroup> LevelPathValues
        {
            get { return _levelPathValues; }
            private set
            {
                _levelPathValues = value;
                OnPropertyChanged(LevelPathValuesProperty);
            }
        }

        #endregion

        #region Constructor

        public ProductGroupViewModel(ProductGroup sourceGroup)
        {
            this.ProductGroup = sourceGroup;
            RefreshAll();
        }

        #endregion

        #region Methods

        private static Dictionary<String, ProductGroup> ConstructLevelPath(ProductGroup sourceGroup)
        {
            Dictionary<String, ProductGroup> levelPathDict = new Dictionary<String, ProductGroup>();

            if (sourceGroup != null)
            {
                ProductGroup group = sourceGroup;
                ProductHierarchy structure = group.ParentHierarchy;

                List<ProductGroup> pathPartList = group.GetParentPath();
                pathPartList.Add(group);

                //construct the dictionary
                // add all levels so we dont get binding exceptions slowing things down.
                List<ProductLevel> levelList = structure.EnumerateAllLevels().ToList();
                for (Int32 i = 0; i < levelList.Count; i++)
                {
                    levelPathDict.Add(levelList[i].Name, pathPartList.ElementAtOrDefault(i));
                }
            }
            else
            {
                levelPathDict.Add("No Specific Category", null);
            }

            return levelPathDict;

        }

        /// <summary>
        /// Forces an update of all values
        /// </summary>
        public void RefreshAll()
        {
            ProductGroup sourceGroup = this.ProductGroup;

            if (sourceGroup != null)
            {
                this.LevelPathValues = ConstructLevelPath(sourceGroup);

                this.AssociatedLevel = sourceGroup.GetAssociatedLevel();
            }

            OnPropertyChanged(ProductGroupLabelProperty);
        }

        /// <summary>
        /// Returns a list  of this unit and  all child units below this one
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductGroupViewModel> GetAllChildUnits()
        {
            IEnumerable<ProductGroupViewModel> returnList =
                new List<ProductGroupViewModel>() { this };

            foreach (ProductGroup child in this.ProductGroup.ChildList)
            {
                ProductGroupViewModel childView = new ProductGroupViewModel(child);
                returnList = returnList.Union(childView.GetAllChildUnits());
            }

            return returnList;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns a null group
        /// </summary>
        /// <param name="levels"></param>
        /// <returns></returns>
        public static ProductGroupViewModel CreateNullGroup(IEnumerable<ProductLevel> levels)
        {
            ProductGroupViewModel group = new ProductGroupViewModel(null);

            Dictionary<String, ProductGroup> levelPathDict = new Dictionary<String, ProductGroup>();
            foreach (ProductLevel level in levels)
            {
                levelPathDict.Add(level.Name, null);
            }
            group._levelPathValues = levelPathDict;

            return group;
        }

        #endregion
    }
}
