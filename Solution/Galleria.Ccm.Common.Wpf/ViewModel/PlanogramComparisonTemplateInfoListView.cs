﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32041 : A.Silva
//  Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    ///     View holder for the <see cref="PlanogramComparisonTemplateInfoList"/> type.
    /// </summary>
    public class PlanogramComparisonTemplateInfoListView : ViewStateListObject<PlanogramComparisonTemplateInfoList, PlanogramComparisonTemplateInfo>
    {
        /// <summary>
        ///     Fetch the list of all <see cref="PlanogramComparisonTemplateInfo"/> objects with the current entity Id and loads the model synchronously./>
        /// </summary>
        public void FetchAllForEntity()
        {
            var criteria = new PlanogramComparisonTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        /// <summary>
        ///     Fetches the list of all <see cref="PlanogramComparisonTemplateInfo"/> objects with the current entity Id and loads the model asynchronously./>
        /// </summary>
        public void FetchAllForEntityAsync()
        {
            var criteria = new PlanogramComparisonTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }
    }
}