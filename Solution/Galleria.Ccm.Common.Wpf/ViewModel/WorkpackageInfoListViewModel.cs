﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 801)
// CCM-28690 : D.Pleasance
//  Amended to prevent multiple worker processes being created for polling workpackage data.
#endregion
#endregion

using System;
using System.Threading;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of a WorkpackageInfoList model object
    /// </summary>
    public sealed class WorkpackageInfoListViewModel : ViewStateListObject<WorkpackageInfoList, WorkpackageInfo>
    {
        #region Fields
        private Boolean _isPolling;
        private String _pollingMethod;
        private Csla.Threading.BackgroundWorker _worker = null;
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this viewmodel is polling.
        /// </summary>
        public Boolean IsPolling
        {
            get { return _isPolling; }
            private set { _isPolling = value; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance
        /// </summary>
        public WorkpackageInfoListViewModel() { }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Error.
        /// </summary>
        /// <param name="error"></param>
        protected override void OnDataPortalException(Exception error)
        {
            //just log 
            CommonHelper.RecordException(error);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches a list of all workpackages infos for the current entity.
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new WorkpackageInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Fetches a list of all workpackages infos for the current entity.
        /// Asynchronously.
        /// </summary>
        public void FetchForCurrentEntityAsync()
        {
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(new WorkpackageInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
            }
            else
            {
                Fetch(new WorkpackageInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
            }
        }

        /// <summary>
        /// Starts polling the fetch for current entity method.
        /// </summary>
        public void BeginPollingFetchForCurrentEntity()
        {
            StartPolling("FetchForCurrentEntity");
        }

        /// <summary>
        /// Stops any ongoing polling operation 
        /// asynchronously.
        /// </summary>
        public void StopPollingAsync()
        {
            _isPolling = false;
            _pollingMethod = null;
        }

        #region Polling

        private void StartPolling(String pollingMethod)
        {
            if (!IsPolling)
            {
                IsPolling = true;
                _pollingMethod = pollingMethod;

                this.ModelChanged += OnModelChangedWhenPolling;
                ExecutePollingMethod();
            }
        }

        private void ExecutePollingMethod()
        {
            switch (_pollingMethod)
            {
                case "FetchForCurrentEntity":
                    // Check if there is a current database connection
                    try
                    {
                        var entityData = Entity.FetchById(CCMClient.ViewState.EntityId);
                    }
                    catch (Csla.DataPortalException dataExc)
                    {
                        // there was a database error so warn the user
                        CommonHelper.GetWindowService().ShowErrorMessage(Message.Application_Database_ConnectionLostHeader, Message.Application_Database_ConnectionLostDescription);
                        return;
                    }

                    FetchForCurrentEntityAsync();
                    break;
                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Called when the model changes and we are polling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnModelChangedWhenPolling(object sender, ViewStateObjectModelChangedEventArgs<WorkpackageInfoList> e)
        {
            if (IsPolling)
            {
                BeingPollingWait();
            }
            else
            {
                this.ModelChanged -= OnModelChangedWhenPolling;
            }
        }

        private void BeingPollingWait()
        {
            if (_worker == null)
            {
                _worker = new Csla.Threading.BackgroundWorker();
                _worker.DoWork += PollingWorker_DoWork;
                _worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
                _worker.RunWorkerAsync();
            }
            else
            {
                if (!_worker.IsBusy)
                {
                    _worker.DoWork += PollingWorker_DoWork;
                    _worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
                    _worker.RunWorkerAsync();
                }
            }
        }
        private void PollingWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Thread.Sleep(Constants.PollingPeriod);
        }
        private void PollingWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker worker = (Csla.Threading.BackgroundWorker)sender;
            worker.DoWork -= PollingWorker_DoWork;
            worker.RunWorkerCompleted -= PollingWorker_RunWorkerCompleted;

            if (IsPolling)
            {
                ExecutePollingMethod();
            }
            else
            {
                this.ModelChanged -= OnModelChangedWhenPolling;
                _worker = null;
            }
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean isDisposing)
        {
            StopPollingAsync();
            base.Dispose(isDisposing);
        }

        #endregion

    }

}
