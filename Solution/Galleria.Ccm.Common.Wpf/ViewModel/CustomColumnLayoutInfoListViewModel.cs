﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Modified FetchAll and FetchByType so that they become basically overloads.
// V8-26170 : A.Silva ~ Ported from Galleria.Ccm.Editor.Client.Wpf
// V8-26351 : A.Silva ~ Some refactoring to tidy up Custom Column Layout classes.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    ///     ViewModel to access the Custom Column Layout settings files.
    /// </summary>
    public sealed class CustomColumnLayoutInfoListViewModel :
        ViewStateListObject<CustomColumnLayoutInfoList, CustomColumnLayoutInfo>
    {
        #region Fields

        private readonly String _folderPath;

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomColumnLayoutInfoListViewModel" /> class using the provided
        ///     <paramref name="fileSystemPath" /> when accessing files.
        /// </summary>
        /// <param name="fileSystemPath">Path to the folder containing the Custom Column Layout settings files.</param>
        public CustomColumnLayoutInfoListViewModel(String fileSystemPath)
        {
            _folderPath = fileSystemPath;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Populates the model with a list of <see cref="CustomColumnLayoutInfo" />.
        /// </summary>
        public void FetchAll()
        {
            FetchAllByType(CustomColumnLayoutType.Undefined);
        }

        /// <summary>
        ///     Populates the model with a list of <see cref="CustomColumnLayoutInfo" /> of the given <paramref name="type" />.
        /// </summary>
        /// <param name="type"><see cref="CustomColumnLayoutType" /> of the infos that will be fetched.</param>
        /// <remarks>If the <paramref name="type" /> is <c>Undefined</c> then ALL existing infos are fetched.</remarks>
        public void FetchAllByType(CustomColumnLayoutType type)
        {
            var ids = GetIds();
            if (type == CustomColumnLayoutType.Undefined)
            {
                Fetch(
                    new CustomColumnLayoutInfoList.FetchByIdsCriteria(
                        CustomColumnLayoutDalFactoryType.FileSystem, ids));
            }
            else
            {
                Fetch(
                    new CustomColumnLayoutInfoList.FetchByTypeCriteria(
                        CustomColumnLayoutDalFactoryType.FileSystem, ids, type));
            }
        }

        private List<Object> GetIds()
        {
            var folderPath = _folderPath;
            var ext = String.Format("{0}{1}", "*", CustomColumnLayout.FileExtension);
            var ids = new List<Object>();

            if (Directory.Exists(folderPath))
            {
                ids.AddRange(Directory.GetFiles(folderPath, ext, SearchOption.TopDirectoryOnly));
            }
            return ids;
        }

        #endregion
    }
}