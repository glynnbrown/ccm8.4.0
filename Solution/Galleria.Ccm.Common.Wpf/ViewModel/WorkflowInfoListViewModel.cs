﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents of a view of a WorkflowInfoList model object
    /// </summary>
    public sealed class WorkflowInfoListViewModel : ViewStateListObject<WorkflowInfoList, WorkflowInfo>
    {
        #region Constructor
        public WorkflowInfoListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Populates the model syncronously with a list
        /// of all infos for the current entity.
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new WorkflowInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Populates the model asyncronously with a list
        /// of all infos for the current entity.
        /// </summary>
        public void FetchForCurrentEntityAsync()
        {
            var criteria = new WorkflowInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion
    }
}
