﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (GFS 2.11)
// GFS-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for AssortmentMinorRevisionInfoInfoList
    /// </summary>
    public sealed class AssortmentMinorRevisionInfoListViewModel : ViewStateListObject<AssortmentMinorRevisionInfoList, AssortmentMinorRevisionInfo>
    {
        #region Constructor

        public AssortmentMinorRevisionInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all AssortmentInfos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchForEntity(Int32 entityId)
        {
            Fetch(new AssortmentMinorRevisionInfoList.FetchByEntityIdCriteria(entityId));
        }

        #endregion
    }
}
