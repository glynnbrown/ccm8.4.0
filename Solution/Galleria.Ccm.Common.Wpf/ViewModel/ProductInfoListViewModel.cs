﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// CCM-25452 : N.Haywood
//  Created
// V8-25453 : A.Kuszyk
//  Added BeginFetchByProductUniverseId.
// V8-25455 : J.Pickup
//  Added BeginFetchByEntityIdConsumerDecisionTree & BeginFetchByProductGroupId
// V8-26281 : L.Ineson
//  Merged together versions from both clients and tidied up
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for ProductInfoList
    /// </summary>
    public sealed class ProductInfoListViewModel : ViewStateListObject<ProductInfoList, ProductInfo>
    {
        #region Constructor
        public ProductInfoListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Returns all products with the given search criteria asynchronously.
        /// </summary>
        /// <param name="criteria"></param>
        //public void BeginFetchBySearchCriteria(String searchCriteria)
        //{
        //    var criteria = new ProductInfoList.FetchByEntityIdSearchCriteriaCriteria(CCMClient.ViewState.EntityId, searchCriteria);

        //    if (!CCMClient.IsUnitTesting)
        //    {
        //        BeginFetch(criteria);
        //    }
        //    else
        //    {
        //        Fetch(criteria);
        //    }

        //}


        [Obsolete("Use BeginFetchForCurrentEntityBySearchCriteria")]
        public void BeginFetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            var criteria = new ProductInfoList.FetchByEntityIdSearchCriteriaCriteria(entityId, searchCriteria);

            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        /// <summary>
        /// Performs an async search for products based on the given criteria.
        /// </summary>
        /// <param name="criteria"></param>
        public void BeginFetchForCurrentEntityBySearchCriteria(String searchCriteria)
        {
            var criteria = new ProductInfoList.FetchByEntityIdSearchCriteriaCriteria(CCMClient.ViewState.EntityId, searchCriteria);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        /// <summary>
        /// Performs an async search for products based on the given product universe id
        /// </summary>
        /// <param name="productUniverseId"></param>
        public void BeginFetchByProductUniverseId(Int32 productUniverseId)
        {
            var criteria = new ProductInfoList.FetchByProductUniverseIdCriteria(productUniverseId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        /// <summary>
        /// Performs an async search for products based on the given consumerDecisionTreeId
        /// </summary>
        public void BeginFetchByConsumerDecisionTree(Int32 consumerDecisionTreeId)
        {
            List<Int32> productIds = new List<Int32>();
            if (consumerDecisionTreeId > 0)
            {
                ConsumerDecisionTree consumerDecisionTree = ConsumerDecisionTree.FetchById(consumerDecisionTreeId);

                foreach (ConsumerDecisionTreeNode consumerDecisionNode in consumerDecisionTree.GetAllNodes())
                {
                    foreach (ConsumerDecisionTreeNodeProduct consumerDecisionProduct in consumerDecisionNode.Products)
                    {
                        productIds.Add(consumerDecisionProduct.ProductId);
                    }
                }
            }

            var criteria = new ProductInfoList.FetchByProductIdsCriteria(productIds);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }


        ///// <summary>
        ///// Performs an async search for products based on the given product group id 
        ///// and any child product groups
        ///// </summary>
        ///// <param name="productGroupId"></param>
        //public void BeginFetchByProductGroupId(Int32 productGroupId)
        //{
        //    var criteria = new ProductInfoList.FetchByParentProductGroupIdCriteria(productGroupId);
        //      if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            //else Fetch(criteria);
        //}


        /// <summary>
        /// Populates the model with infos for the given product ids
        /// asynchronously
        /// </summary>
        public void FetchByProductIdsAsync(IEnumerable<Int32> productIds)
        {
            var criteria = new ProductInfoList.FetchByProductIdsCriteria(productIds);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        /// <summary>
        /// Populates the model with infos for the given gtins asynchronously.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="productGtins"></param>
        public void FetchByEntityIdProductGtinsAsync(Int32 entityId, IEnumerable<String> productGtins)
        {
            var criteria = new ProductInfoList.FetchByEntityIdProductGtinsCriteria(entityId, productGtins);
            if (!CCMClient.IsUnitTesting) BeginFetch(criteria);
            else Fetch(criteria);
        }

        #endregion
    }
}
