﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using System;
using Galleria.Ccm.GFSModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of the GFSTimelineGroupList model.
    /// </summary>
    public sealed class GFSTimelineGroupListViewModel : ViewStateListObject<TimelineGroupList, TimelineGroup>
    {
        #region Constructor

        public GFSTimelineGroupListViewModel() { }

        #endregion

        #region Methods
        /// <summary>
        /// Fetches a list of timeline groups from gfs based on the given entity name.
        /// </summary>
        public void BeginFetchByPerformanceSource(GFSModel.Entity entity, GFSModel.PerformanceSource performanceSource)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = DateTime.UtcNow;
            var criteria = new TimelineGroupList.FetchByDateRangeCriteria(entity, performanceSource, startDate, endDate);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }
        #endregion
    }
}
