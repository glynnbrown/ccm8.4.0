﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// CCM-32790 : M.Pettit
//  Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Planogram Export Template Info List View Model
    /// </summary>
    public sealed class PlanogramExportTemplateInfoListViewModel : ViewStateListObject<PlanogramExportTemplateInfoList, PlanogramExportTemplateInfo>
    {
        #region Constructor

        public PlanogramExportTemplateInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model for the current entity synchronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new PlanogramExportTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
