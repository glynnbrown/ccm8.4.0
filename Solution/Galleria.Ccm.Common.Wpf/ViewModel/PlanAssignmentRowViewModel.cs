﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : M.Shelley
//	Created
// V8-28276 : A.Kuszyk
//  Made StoreSpaceBayTotalWidth nullable.
#endregion

#region Version History: (CCM 8.0.1)
// V8-28481 : M.Shelley
//  Added published status and location published display column
// V8-28986 : M.Shelley
//  Added a new constructor to display plans assigned against product groups for a given location
#endregion

#region Version History: (CCM 8.0.2)
// V8-29071 : M.Shelley
//  Add a fix for the auto-refresh clearing the user's selection
#endregion

#region Version History: (CCM 8.0.2)
// V8-28242 : M.Shelley
//  Add functionality to allow the plan assignment "Date Communicated" and "Date Live" 
//  values to be changed
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added Height
// V8-29173 : N.Haywood
//  Removed published date check in GetPlanPublishStatus() as the publish date is only set on a successful publish
#endregion

#region Version History: (CCM 8.1.1)
// V8-30463 : L.Ineson
//  Now takes in user infos improve performance.
// V8-30621 : D.Pleasance
//  Amended SetPlanogram to clear PlanPublishDate and PlanPublishUser
#endregion

#region Version History: (CCM 8.2.0)
// V8-30508 : M.Shelley
//  Added code to use the plan publish status field to display the publish status and 
//  code to detect if the planogram modified date is different from the last publish date and
//  if so, change the publish status icon to reflect this as a "PlanogramAltered" type.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32711 : L.Ineson
//  All calculated field values are now returned as nullable for this view.
#endregion

#endregion

using System;
using System.Linq;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using Galleria.Framework.Model;
using System.Text;
using ModelMessage = Galleria.Ccm.Resources.Language.Message;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Runtime.CompilerServices;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of a record to be used when displaying planogram assignment rows.
    /// </summary>
    public sealed class PlanAssignmentRowViewModel : ViewModelObject
    {
        #region Constants

        const Int16 PublishTimeTimeAllowance = 1;

        #endregion

        #region Fields

        private Type _status;
        private Int16 _locationId;
        private Int32? _storeSpaceBayCount;
        private Double? _storeSpaceBayTotalWidth;
        private Double? _storeSpaceBayHeight;
        private Int32 _productGroupId;
        private Int32? _planogramId;
        private String _planName;
        private Int32? _planBayCount = null;
        private Single? _planTotalWidth = null;
        private Single? _planHeight = null;
        private PlanAssignmentType _planType;
        private String _planAssignedByUser;
        private String _planPublishUser;
        private DateTime? _planAssignedDate;
        private DateTime? _planPublishDate;

        private PlanogramInfo _planogram;
        private LocationInfo _location;
        private ProductGroupInfo _productGroup;
        private PlanAssignmentStoreSpaceInfo _planAssignmentStoreSpaceInfo;
        private LocationPlanAssignment _locationPlanAssignment;

        private DateTime? _dateCommunicated;
        private DateTime? _dateLive;

        private PlanAssignmentRowKey _rowKey;
        private CalculatedValueResolver _calculatedValueResolver;

        #endregion

        #region Property Path Bindings

        public static readonly PropertyPath StatusProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.Status);
        public static readonly PropertyPath StoreSpaceBayCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.StoreSpaceBayCount);
        public static readonly PropertyPath StoreSpaceBayTotalWidthProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.StoreSpaceBayTotalWidth);
        public static readonly PropertyPath StoreSpaceBayHeightProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.StoreSpaceBayHeight);
        public static readonly PropertyPath PlanNameProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanName);
        public static readonly PropertyPath PlanBayCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanBayCount);
        public static readonly PropertyPath PlanTotalWidthProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanTotalWidth);
        public static readonly PropertyPath PlanHeightProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanHeight);
        public static readonly PropertyPath PlanInfoProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanogramInfo);
        public static readonly PropertyPath PlanTypeProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanType);
        public static readonly PropertyPath PlanAssignedByUserProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanAssignedByUser);
        public static readonly PropertyPath PlanAssignedDateProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanAssignedDate);
        public static readonly PropertyPath PlanPublishDateProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanPublishDate);
        public static readonly PropertyPath PlanPublishUserProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanPublishUser);
        public static readonly PropertyPath PlanAssignStatusProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanAssignStatus);
        public static readonly PropertyPath FriendlyAssignStatusProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.FriendlyAssignStatus);
        public static readonly PropertyPath LocationPlanAssignmentProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.LocationPlanAssignment);
        public static readonly PropertyPath FriendlyPlanPublishTypeProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.FriendlyPlanPublishType);
        public static readonly PropertyPath PlanPublishTooltipProperty = WpfHelper.GetPropertyPath<PlanAssignmentRowViewModel>(p => p.PlanPublishTooltip);

        #endregion

        #region Properties

        public Type Status
        {
            get { return _status; }
            private set
            {
                _status = value;
                OnPropertyChanged(StatusProperty);
            }
        }

        public Int16 LocationId
        {
            get { return _locationId; }
        }

        public Int32? StoreSpaceBayCount
        {
            get { return _storeSpaceBayCount; }
            private set
            {
                _storeSpaceBayCount = value;
                OnPropertyChanged(StoreSpaceBayCountProperty);
            }
        }

        public Double? StoreSpaceBayTotalWidth
        {
            get { return _storeSpaceBayTotalWidth; }
            private set
            {
                _storeSpaceBayTotalWidth = value;
                OnPropertyChanged(StoreSpaceBayTotalWidthProperty);
            }
        }

        public Double? StoreSpaceBayHeight
        {
            get { return _storeSpaceBayHeight; }
            private set
            {
                _storeSpaceBayHeight = value;
                OnPropertyChanged(StoreSpaceBayHeightProperty);
            }
        }

        public Int32 ProductGroupId
        {
            get { return _productGroupId; }
        }

        public ProductGroupInfo ProductGroup
        {
            get { return _productGroup; }
        }

        public PlanogramInfo PlanogramInfo
        {
            get { return _planogram; }
            private set
            {
                _planogram = value;
                OnPropertyChanged(PlanInfoProperty);
            }
        }

        public String PlanName
        {
            get { return _planName; }
            private set
            {
                _planName = value;
                OnPropertyChanged(PlanNameProperty);
            }
        }

        public Int32? PlanBayCount
        {
            get { return _planBayCount; }
            private set
            {
                _planBayCount = value;
                OnPropertyChanged(PlanBayCountProperty);
            }
        }

        public Single? PlanTotalWidth
        {
            get { return _planTotalWidth; }
            private set
            {
                _planTotalWidth = value;
                OnPropertyChanged(PlanTotalWidthProperty);
            }
        }

        public Single? PlanHeight
        {
            get { return _planHeight; }
            private set
            {
                _planHeight = value;
                OnPropertyChanged(PlanHeightProperty);
            }
        }

        public PlanAssignmentType PlanType
        {
            get { return _planType; }
            private set
            {
                _planType = value;
                OnPropertyChanged(PlanTypeProperty);
            }
        }

        public String PlanAssignedByUser
        {
            get { return _planAssignedByUser; }
            private set
            {
                _planAssignedByUser = value;
                OnPropertyChanged(PlanAssignedByUserProperty);
            }
        }

        public DateTime? PlanAssignedDate
        {
            get { return _planAssignedDate; }
            private set
            {
                _planAssignedDate = value;
                OnPropertyChanged(PlanAssignedDateProperty);
            }
        }

        public DateTime? PlanPublishDate
        {
            get { return _planPublishDate; }
            private set
            {
                _planPublishDate = value;
                OnPropertyChanged(PlanPublishDateProperty);
            }
        }

        public String PlanPublishUser
        {
            get { return _planPublishUser; }
            private set
            {
                _planPublishUser = value;
                OnPropertyChanged(PlanPublishUserProperty);
            }
        }

        public PlanAssignmentPublishStatusType? PlanAssignStatus
        {
            get { return GetPlanPublishStatus(); }
        }

        /// <summary>
        /// Gets the current publishing status for this item and store in a user readable way
        /// Also used for sorting on the status column header
        /// </summary>
        public String FriendlyAssignStatus
        {
            get 
            {
                return (this.PlanAssignStatus == null ? String.Empty : 
                        PlanAssignmentPublishStatusTypeHelper.ToolTip[(PlanAssignmentPublishStatusType) this.PlanAssignStatus]);
            }
        }

        public LocationInfo Location
        {
            get { return _location; }
        }

        public PlanAssignmentStoreSpaceInfo PlanAssignmentStoreSpaceInfo
        {
            get { return _planAssignmentStoreSpaceInfo; }
        }

        public LocationPlanAssignment LocationPlanAssignment
        {
            get { return _locationPlanAssignment; }
            private set
            {
                _locationPlanAssignment = value;
                OnPropertyChanged(LocationPlanAssignmentProperty);
            }
        }

        public PlanAssignmentRowKey RowKey
        {
            get { return _rowKey; }
        }

        public String FriendlyPlanPublishType
        {
            get
            {
                if (_locationPlanAssignment == null || _locationPlanAssignment.PublishType == null)
                {
                    return String.Empty;
                }

                return PlanPublishTypeHelper.FriendlyNames[(PlanPublishType)_locationPlanAssignment.PublishType];
            }
        }

        public String PlanPublishTooltip
        {
            get { return GetPublishTooltip(); }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = new CalculatedValueResolver(GetCalculatedValue, true);

                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="planAssignItem"></param>
        /// <param name="locationPlanItem"></param>
        /// <param name="planInfo"></param>
        /// <param name="selectedLocation"></param>
        /// <param name="selectedProductGroup"></param>
        /// <param name="currentViewType"></param>
        /// <param name="assignedByUserInfo"></param>
        /// <param name="publishedByUserInfo"></param>
        public PlanAssignmentRowViewModel(
            PlanAssignmentStoreSpaceInfo planAssignItem, LocationPlanAssignment locationPlanItem, PlanogramInfo planInfo,
            LocationInfo selectedLocation, ProductGroupInfo selectedProductGroup, PlanAssignmentViewType currentViewType,
            UserInfo assignedByUserInfo, UserInfo publishedByUserInfo)
        {

            LoadDetails(planAssignItem, locationPlanItem,
                planInfo, selectedLocation, selectedProductGroup, currentViewType,
                assignedByUserInfo, publishedByUserInfo);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Utility method to return a formatted user name
        /// </summary>
        /// <param name="userRef">An instance of the User class</param>
        /// <returns>A string of the format "'first name' 'last name'"</returns>
        private String FormatUsername(UserInfo userRef)
        {
            if (userRef != null)
            {
                return String.Format(CultureInfo.CurrentCulture, "{0} {1}", userRef.FirstName, userRef.LastName);
            }
            return String.Empty;
        }

        /// <summary>
        /// Sets the planogram assigned to this row.
        /// </summary>
        public void SetPlanogram(PlanogramInfo newPlanogram, DateTime currentLocalDate, String userFriendlyName)
        {
            this.PlanogramInfo = newPlanogram;
            this.PlanName = newPlanogram.Name;
            this.PlanBayCount = newPlanogram.MetaBayCount;
            this.PlanTotalWidth = newPlanogram.Width;
            this.PlanHeight = newPlanogram.Height;
            this.PlanAssignedByUser = userFriendlyName;
            this.PlanAssignedDate = currentLocalDate;
            this.PlanPublishDate = null;
            this.PlanPublishUser = null;
        }

        /// <summary>
        /// Clears the planogram assigned to this row.
        /// </summary>
        public void ClearPlanogram()
        {
            this.PlanogramInfo = null;
            this.PlanAssignedByUser = null;
            this.PlanAssignedDate = null;
            this.PlanName = "";
            this.PlanBayCount = null;
            this.PlanTotalWidth = null;
            this.PlanHeight = null;
        }

        /// <summary>
        /// Method to determine the publish status of the current plan location row item
        /// </summary>
        private PlanAssignmentPublishStatusType? GetPlanPublishStatus()
        {
            PlanAssignmentPublishStatusType? localStatus = PlanAssignmentPublishStatusType.PlanNotPublished;

            // Check the plan assignment is not null 
            if (this.LocationPlanAssignment != null)
            {
                localStatus = LocationPlanAssignment.PublishStatus;
                if (this.PlanogramInfo != null)
                {
                    switch (this.LocationPlanAssignment.PublishStatus)
                    {
                        case PlanAssignmentPublishStatusType.Successful:
                            // Check if the plan modified date is after the plan publish date
                            // i.e. the plan has been edited after publishing
                            // if the plan has publish successfully then the PublishDate should not be null
                            var publishDateDiff = ((DateTime) this.PlanPublishDate - this.PlanogramInfo.DateLastModified).Minutes;

                            // Check if the time difference in minutes is less than a specified value
                            // as the work package that published the plans could also have changed the
                            // last modified date of the plan

                            if (publishDateDiff < 0 && Math.Abs(publishDateDiff) > PublishTimeTimeAllowance)
                            {
                                localStatus = PlanAssignmentPublishStatusType.PlanogramAltered;
                            }
                            break;

                        default:
                            localStatus = (this.LocationPlanAssignment.PublishStatus == null ? null : this.LocationPlanAssignment.PublishStatus);
                            break;
                    }
                }
            }

            return localStatus;
        }

        /// <summary>
        /// Helper method to generate the plan publish status tooltip text depending on the plan publish status
        /// </summary>
        /// <returns>The tooltip string</returns>
        private String GetPublishTooltip()
        {
            String newLine = System.Environment.NewLine;
            String tooltip = String.Empty;

            if (this.LocationPlanAssignment != null)
            {
                if (LocationPlanAssignment.PublishStatus != null)
                {
                    var localPublishStatus = GetPlanPublishStatus();
                    tooltip = 
                        PlanAssignmentPublishStatusTypeHelper.ToolTip[(PlanAssignmentPublishStatusType) localPublishStatus] + newLine;

                    switch (localPublishStatus)
                    {
                        case PlanAssignmentPublishStatusType.Successful:
                            tooltip += ModelMessage.LocationPlanAssignment_PublishedByUser + " " + PlanPublishUser + newLine +
                                       ModelMessage.LocationPlanAssignment_DatePublished + " " + LocationPlanAssignment.DatePublished;
                            break;

                        case PlanAssignmentPublishStatusType.PlanogramAltered:
                            var localPlan = this.PlanogramInfo;
                            tooltip += ModelMessage.LocationPlanAssignment_PublishedByUser + " " + PlanPublishUser + newLine +
                                   ModelMessage.LocationPlanAssignment_DatePublished + " " + LocationPlanAssignment.DatePublished + newLine +
                                   ModelMessage.PlanogramInfo_DateLastModified + " " + localPlan.DateLastModified;
                            break;

                        default:
                            break;
                    }
                }
            }

            return tooltip;
        }

        public void UpdateRow(
            PlanAssignmentStoreSpaceInfo planAssignItem, LocationPlanAssignment locationPlanItem, PlanogramInfo planInfo,
            LocationInfo selectedLocation, ProductGroupInfo selectedProductGroup, PlanAssignmentViewType currentViewType,
            UserInfo assignedByUserInfo, UserInfo publishedByUserInfo)
        {
            Debug.Assert((new PlanAssignmentRowKey(planAssignItem, locationPlanItem, currentViewType) != _rowKey), "row key does not match");

            LoadDetails(planAssignItem, locationPlanItem, 
                planInfo, selectedLocation, selectedProductGroup, currentViewType,
                assignedByUserInfo, publishedByUserInfo);
        }


        private void LoadDetails(
            PlanAssignmentStoreSpaceInfo planAssignItem, LocationPlanAssignment locationPlanItem, PlanogramInfo planInfo,
            LocationInfo selectedLocation, ProductGroupInfo selectedProductGroup, PlanAssignmentViewType currentViewType,
            UserInfo assignedByUserInfo, UserInfo publishedByUserInfo)
        {
            _planAssignmentStoreSpaceInfo = planAssignItem;
            this.LocationPlanAssignment = locationPlanItem;


            //Store Space
            if (planAssignItem != null)
            {
                this.StoreSpaceBayCount = (planAssignItem.BayCount < 1 ? null : (Int32?)planAssignItem.BayCount);
                this.StoreSpaceBayTotalWidth = planAssignItem.BayTotalWidth.EqualTo(0) ? null : (Double?)planAssignItem.BayTotalWidth;
                this.StoreSpaceBayHeight = planAssignItem.BayHeight.EqualTo(0) ? null : (Double?)planAssignItem.BayHeight;
            }

            // Assigned planogram details
            if (locationPlanItem != null)
            {
                this.PlanAssignedByUser = FormatUsername(assignedByUserInfo);
                this.PlanAssignedDate = locationPlanItem.DateAssigned;

                this.PlanPublishUser = FormatUsername(publishedByUserInfo);
                this.PlanPublishDate = locationPlanItem.DatePublished;

                //fire off related property changes
                OnPropertyChanged(PlanAssignStatusProperty);
                OnPropertyChanged(FriendlyAssignStatusProperty);
                OnPropertyChanged(FriendlyPlanPublishTypeProperty);
            }

            //Product group details
            if (selectedProductGroup != null)
            {
                _productGroup = selectedProductGroup;
                _productGroupId = selectedProductGroup.Id;
            }

            //Location details
            if (selectedLocation != null)
            {
                _location = selectedLocation;
                _locationId = selectedLocation.Id;
            }

            //Planogram details
            if (planInfo != null)
            {
                _planogram = planInfo;
                _planogramId = planInfo.Id;
                _planName = planInfo.Name;
                this.PlanBayCount = planInfo.MetaBayCount;
                this.PlanTotalWidth = planInfo.Width;
                this.PlanHeight = planInfo.Height;
            }

            _rowKey = new PlanAssignmentRowKey(planAssignItem, locationPlanItem, currentViewType);

            //force dynamic values to update.
            OnPropertyChanged(String.Empty);

            //refresh calculated values
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();
        }

        /// <summary>
        /// Enumerates through the list of fields that can be displayed for this view. 
        /// Used by PlanAssignmentColumnLayoutFactory
        /// </summary>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            //PlanAssignmentStoreSpaceInfo
            foreach (ObjectFieldInfo fieldInfo in PlanAssignmentStoreSpaceInfo.EnumerateDisplayableFieldInfosLocation())
            {
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            // Location
            foreach (ObjectFieldInfo fieldInfo in Model.Location.EnumerateDisplayableFieldInfos())
            {
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            // PlanAssignmentStoreSpaceInfo
            foreach (ObjectFieldInfo fieldInfo in PlanAssignmentStoreSpaceInfo.EnumerateDisplayableFieldInfos())
            {
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

             // ProductGroup
            foreach (ObjectFieldInfo fieldInfo in Model.ProductGroup.EnumerateDisplayableFieldInfos())
            {
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            // PlanogramInfo
            String ownerFriendlyName = Framework.Planograms.Model.Planogram.FriendlyName;
            Type ownerType = typeof(PlanogramInfo);

            foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePropertyInfos())
            {
                var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, propertyInfo);
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }

            // LocationPlanAssignment
            foreach (ObjectFieldInfo fieldInfo in LocationPlanAssignment.EnumerateDisplayableFieldInfos())
            {
                //Inject in the user name columns:
                if (fieldInfo.PropertyName == LocationPlanAssignment.DateAssignedProperty.Name)
                {
                    //assigned user
                    ObjectFieldInfo assignedUser = ObjectFieldInfo.NewObjectFieldInfo(
                        typeof(LocationPlanAssignment), LocationPlanAssignment.FriendlyName,
                        PlanAssignmentRowViewModel.PlanAssignedByUserProperty.Path, LocationPlanAssignment.AssignedByUserIdProperty.FriendlyName,
                        typeof(String), ModelPropertyDisplayType.None, false, LocationPlanAssignment.FriendlyName);

                    yield return assignedUser;
                }
                else if (fieldInfo.PropertyName == LocationPlanAssignment.DatePublishedProperty.Name)
                {
                    //published user
                    ObjectFieldInfo publishedUser = ObjectFieldInfo.NewObjectFieldInfo(
                        typeof(LocationPlanAssignment), LocationPlanAssignment.FriendlyName,
                        PlanAssignmentRowViewModel.PlanPublishUserProperty.Path, LocationPlanAssignment.PublishedByUserIdProperty.FriendlyName,
                        typeof(String), ModelPropertyDisplayType.None, false, LocationPlanAssignment.FriendlyName);

                    yield return publishedUser;
                }
                
                if (fieldInfo.PropertyName == LocationPlanAssignment.PublishTypeProperty.Name)
                {
                    // Plan publish type
                    ObjectFieldInfo planPublishField = ObjectFieldInfo.NewObjectFieldInfo(
                        typeof(LocationPlanAssignment), LocationPlanAssignment.FriendlyName,
                        PlanAssignmentRowViewModel.FriendlyPlanPublishTypeProperty.Path, LocationPlanAssignment.PublishTypeProperty.FriendlyName,
                        typeof(String), ModelPropertyDisplayType.None, false, LocationPlanAssignment.FriendlyName);

                    yield return planPublishField;
                }
                else
                {
                    fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                    fieldInfo.PropertyName = fieldInfo.OwnerType.Name + "." + fieldInfo.PropertyName;
                    yield return fieldInfo;
                }
            }
        }

        /// <summary>
        /// Returns true if this row has valid location space information.
        /// </summary>
        /// <returns></returns>
        public Boolean HasValidLocationSpace()
        {
            if (!this.StoreSpaceBayCount.HasValue 
                || !this.StoreSpaceBayTotalWidth.HasValue 
                || !this.StoreSpaceBayHeight.HasValue)
            {
                return false;
            }
            return true;
        }

        #region CalculatedValueResolver

        /// <summary>
        /// Calculates a value from the given expression text.
        /// </summary>
        private object GetCalculatedValue(String expressionText)
        {
            //Return all calculated fields as a nullable type for this view.
            Type returnType =
                ObjectFieldExpression.GetExpectedValueType(expressionText, EnumerateDisplayableFieldInfos());
            returnType = CommonHelper.ToNullableType(returnType);



            ObjectFieldExpression expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(EnumerateDisplayableFieldInfos());

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;

            Object returnValue = expression.Evaluate(null, returnType);

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;

            return returnValue;
        }

        /// <summary>
        /// Called when resolving a calculated parameter.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            ObjectFieldInfo field = e.Parameter.FieldInfo;
            e.Result = field.GetValue(this);
        }


        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }

 
}
