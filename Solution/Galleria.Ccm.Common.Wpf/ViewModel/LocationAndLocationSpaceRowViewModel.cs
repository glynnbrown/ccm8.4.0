﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811
// V8-30357 : M.Brumby
//  Create
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Row view model for use with Location and location space column layout factory
    /// </summary>
    public sealed class LocationAndLocationSpaceRowViewModel : ViewModelObject
    {

        #region Property Paths
        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<LocationAndLocationSpaceRowViewModel>(p => p.Location);
        public static readonly PropertyPath StoreSpaceInfoProperty = WpfHelper.GetPropertyPath<LocationAndLocationSpaceRowViewModel>(p => p.StoreSpaceInfo);
        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<LocationAndLocationSpaceRowViewModel>(p => p.Name);
        #endregion

        #region Properties

        /// <summary>
        /// Location name
        /// </summary>
        /// <remarks>Required for use in the workpackage 
        /// "go store specific" assigned grid</remarks>
        public String Name
        {
            get
            {
                if (this.Location != null)
                {
                    return this.Location.Name;
                }
                return null;
            }
        }

        /// <summary>
        /// Location property
        /// </summary>
        public Location Location
        {
            get;
            private set;
        }

        /// <summary>
        /// Store space info property
        /// </summary>
        public PlanAssignmentStoreSpaceInfo StoreSpaceInfo
        {
            get;
            private set;
        }

        #endregion

        #region Constructor
        public LocationAndLocationSpaceRowViewModel(Location location, PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            Location = location;
            StoreSpaceInfo = storeSpaceInfo;
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a list of row view models linking locations with store space information
        /// for the supplied product group id.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="productGroupId"></param>
        /// <param name="locations"></param>
        /// <returns></returns>
        public static IEnumerable<LocationAndLocationSpaceRowViewModel> GetRowViewModels(Int32 entityId, Int32 productGroupId, LocationList locations)
        {
            Dictionary<Int16, PlanAssignmentStoreSpaceInfo>  storeSpaceDic = PlanAssignmentStoreSpaceInfoList.FetchByEntityIdProductGroupId(entityId, productGroupId).ToDictionary(d => d.LocationId);

            foreach (Location loc in locations)
            {
                PlanAssignmentStoreSpaceInfo info;
                if (storeSpaceDic.TryGetValue(loc.Id, out info))
                {
                    yield return new LocationAndLocationSpaceRowViewModel(loc, info);
                }
            }
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
