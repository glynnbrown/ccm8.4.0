﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using Galleria.Ccm.GFSModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of the GFSPerformanceSourceList model.
    /// </summary>
    public sealed class GFSPerformanceSourceListViewModel : ViewStateListObject<PerformanceSourceList, PerformanceSource>
    {
        #region Constructor

        public GFSPerformanceSourceListViewModel() { }

        #endregion

        #region Methods
        /// <summary>
        /// Fetches a list of performance sources from gfs based on the given entity name.
        /// </summary>
        public void FetchAll(GFSModel.Entity entity)
        {
            var criteria = new PerformanceSourceList.FetchAllCriteria(entity);
            Fetch(criteria);
        }
        #endregion
    }
}
