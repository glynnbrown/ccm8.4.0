﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Processes;
using Galleria.Framework.Planograms.Processes.RenderPlanogramImage;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    //TODO: Sort out to fetch the image properly.

    /// <summary>
    /// Viewmodel for the PlanogramImage model object
    /// </summary>
    public sealed class PlanogramImageViewModel : ViewStateObject<PlanogramImage>
    {
        #region Fields
        private Dictionary<Int32, PlanogramImage> _cache = new Dictionary<Int32, PlanogramImage>();
        #endregion

        #region Constructor
        public PlanogramImageViewModel() { }
        #endregion

        #region Methods

        #region FetchThumbnailByPlanogramId

        public void FetchThumbnailByPlanogramId(Int32 planogramId)
        {
            if (!CCMClient.IsUnitTesting)
            {
                Package package = Package.FetchById(planogramId);
                Planogram plan = package.Planograms[0];


                //if (!plan.MetaThumbnailId.HasValue)
                //{
                //    //force immediate image load.
                //    var images = plan.Images;
                //}

                //PlanogramImage img = null;
                //if (plan.MetaThumbnailId.HasValue)
                //{
                //    img = plan.Images.FindById(plan.MetaThumbnailId.Value);
                //}
                //else
                //{
                //    //TEMP: create immediately
                //    img = PlanogramImage.NewPlanogramImage();
                //    img.ImageData = plan.CreatePlanogramThumbnailData(false);
                //}

                //this.Model = img;

            }
            else
            {
                this.Model = PlanogramImage.NewPlanogramImage();
            }
        }

        #endregion

        #region FetchThumbnailByPlanogramIdAsync
        /// <summary>
        /// Temp method to generate a planogram thumbnail.
        /// </summary>
        /// <param name="planogramId"></param>
        public void FetchThumbnailByPlanogramIdAsync(Int32 planogramId)
        {
            if (!CCMClient.IsUnitTesting)
            {
                PlanogramImage img = FindCachedThumbnail(planogramId);
                if (img != null)
                {
                    this.Model = img;
                }
                else
                {
                    ProcessFactory<Process> processFactory = new ProcessFactory<Process>();
                    processFactory.ProcessCompleted +=RenderPlanogramImage_ProcessCompleted;
                    processFactory.Execute(new Process(planogramId, /*isSoftware*/false));
                }
            }
            else
            { 
                this.Model = PlanogramImage.NewPlanogramImage();
            }
        }

        private void RenderPlanogramImage_ProcessCompleted(object sender, ProcessCompletedEventArgs<Process> e)
        {
            // unsubscribe events
            ProcessFactory<Process> processFactory = (ProcessFactory<Process>)sender;
            processFactory.ProcessCompleted -= RenderPlanogramImage_ProcessCompleted;

            if (e.Error == null)
            {
                //set the image
                PlanogramImage img = PlanogramImage.NewPlanogramImage();
                img.ImageData = e.Process.ImageData;
                this.Model = img;
            }
        }
        #endregion

        private void AddToCache(Int32 planId, PlanogramImage img)
        {
            if (_cache.Count > 4)
            {
                _cache.Remove(_cache.Keys.First());
            }

            _cache.Add(planId, img);
        }

        private PlanogramImage FindCachedThumbnail(Int32 planId)
        {
            PlanogramImage img = null;
            _cache.TryGetValue(planId, out img);

            return img;
        }

        #endregion
    }
}
