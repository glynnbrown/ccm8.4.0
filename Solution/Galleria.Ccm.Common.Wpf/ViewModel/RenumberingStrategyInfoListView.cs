﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    ///     View holder for a <see cref="RenumberingStrategyInfoList"/> model.
    /// </summary>
    public sealed class RenumberingStrategyInfoListView : ViewStateListObject<RenumberingStrategyInfoList, RenumberingStrategyInfo>
    {
        /// <summary>
        ///     Fetches the list of all <see cref="RenumberingStrategyInfo"/> objects with the current entity Id and loads the model synchronously./>
        /// </summary>
        public void FetchAllForEntity()
        {
            var criteria = new RenumberingStrategyInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        /// <summary>
        ///     Fetches the list of all <see cref="RenumberingStrategyInfo"/> objects with the current entity Id and loads the model asynchronously./>
        /// </summary>
        public void FetchAllForEntityAsync()
        {
            var criteria = new RenumberingStrategyInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }
    }
}
