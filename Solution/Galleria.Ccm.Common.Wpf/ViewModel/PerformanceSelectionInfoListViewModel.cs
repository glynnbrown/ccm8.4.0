﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents  a view of a list of PerformanceSelectionInfos.
    /// </summary>
    public sealed class PerformanceSelectionInfoListViewModel : ViewStateListObject<PerformanceSelectionInfoList, PerformanceSelectionInfo>
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PerformanceSelectionInfoListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list for the current entity synchronously.
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new PerformanceSelectionInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
