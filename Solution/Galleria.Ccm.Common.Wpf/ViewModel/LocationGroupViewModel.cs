﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25554 : L.Hodson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of a LocationGroup model object
    /// </summary>
    public sealed class LocationGroupViewModel : INotifyPropertyChanged
    {
        #region Fields
        private String _groupLabelOverride = null;
        private LocationLevel _associatedLevel;
        private Dictionary<String, LocationGroup> _levelPathValues;
        #endregion

        #region Binding Property paths

        public static readonly PropertyPath LocationGroupProperty = WpfHelper.GetPropertyPath<LocationGroupViewModel>(p => p.LocationGroup);
        public static readonly PropertyPath LocationGroupLabelProperty = WpfHelper.GetPropertyPath<LocationGroupViewModel>(p => p.LocationGroupLabel);
        public static readonly PropertyPath AssociatedLevelProperty = WpfHelper.GetPropertyPath<LocationGroupViewModel>(p => p.AssociatedLevel);
        public static readonly PropertyPath LevelPathValuesProperty = WpfHelper.GetPropertyPath<LocationGroupViewModel>(p => p.LevelPathValues);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the original product group model
        /// </summary>
        public LocationGroup LocationGroup
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns the level object associated to the source group
        /// </summary>
        public LocationLevel AssociatedLevel
        {
            get { return _associatedLevel; }
            private set
            {
                _associatedLevel = value;
                OnPropertyChanged(AssociatedLevelProperty);
            }
        }

        /// <summary>
        /// Returns the dictionary collection of level path values
        /// </summary>
        public Dictionary<String, LocationGroup> LevelPathValues
        {
            get { return _levelPathValues; }
            private set
            {
                _levelPathValues = value;
                OnPropertyChanged(LevelPathValuesProperty);
            }
        }


        /// <summary>
        /// Returns a string containing of format Code : Name + overrides for null group, Root etc
        /// </summary>
        public String LocationGroupLabel
        {
            get
            {
                if (!String.IsNullOrEmpty(_groupLabelOverride))
                {
                    return _groupLabelOverride;
                }

                else if (this.LocationGroup != null)
                {
                    return this.LocationGroup.ToString();
                }

                else
                {
                    //return uncategorized label
                    return Message.ProductGroup_NullProductGroupLabel;
                }
            }
            set
            {
                _groupLabelOverride = value;
                OnPropertyChanged(LocationGroupLabelProperty);
            }
        }

        #endregion

        #region Constructor

        public LocationGroupViewModel(LocationGroup sourceGroup)
        {
            this.LocationGroup = sourceGroup;
            RefreshAll();
        }

        #endregion

        #region Methods

        private static Dictionary<String, LocationGroup> ConstructLevelPath(LocationGroup sourceGroup)
        {
            Dictionary<String, LocationGroup> levelPathDict = new Dictionary<String, LocationGroup>();

            if (sourceGroup != null)
            {
                LocationGroup group = sourceGroup;
                LocationHierarchy structure = group.ParentHierarchy;

                List<LocationGroup> pathPartList = group.GetParentPath();
                pathPartList.Add(group);

                //construct the dictionary
                // add all levels so we dont get binding exceptions slowing things down.
                List<LocationLevel> levelList = structure.EnumerateAllLevels().ToList();
                for (Int32 i = 0; i < levelList.Count; i++)
                {
                    levelPathDict.Add(levelList[i].Name, pathPartList.ElementAtOrDefault(i));
                }
            }
            else
            {
                levelPathDict.Add(String.Empty, null);
            }

            return levelPathDict;

        }

        /// <summary>
        /// Forces an update of all values
        /// </summary>
        public void RefreshAll()
        {
            LocationGroup sourceGroup = this.LocationGroup;

            if (sourceGroup != null)
            {
                this.LevelPathValues = ConstructLevelPath(sourceGroup);
                this.AssociatedLevel = sourceGroup.GetAssociatedLevel();
            }

            OnPropertyChanged(LocationGroupLabelProperty);
        }

        /// <summary>
        /// Returns a list  of this unit and  all child units below this one
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationGroupViewModel> GetAllChildUnits()
        {
            IEnumerable<LocationGroupViewModel> returnList =
                new List<LocationGroupViewModel>() { this };

            foreach (LocationGroup child in this.LocationGroup.ChildList)
            {
                LocationGroupViewModel childView = new LocationGroupViewModel(child);
                returnList = returnList.Union(childView.GetAllChildUnits());
            }

            return returnList;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns a null group
        /// </summary>
        /// <param name="levels"></param>
        /// <returns></returns>
        public static LocationGroupViewModel CreateNullGroup(IEnumerable<LocationLevel> levels)
        {
            LocationGroupViewModel group = new LocationGroupViewModel(null);

            Dictionary<String, LocationGroup> levelPathDict = new Dictionary<String, LocationGroup>();
            foreach (LocationLevel level in levels)
            {
                Object levelRef = level.Id;
                levelPathDict.Add(level.Name, null);
            }
            group._levelPathValues = levelPathDict;

            return group;
        }

        #endregion

    }
}
