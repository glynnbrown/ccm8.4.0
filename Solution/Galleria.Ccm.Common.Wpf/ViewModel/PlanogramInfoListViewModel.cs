﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
// CCM-26281 : L.Ineson
//  Merged versions from both clients
// CCM-26520 : J.Pickup
//  FetchByNullCategoryCode
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteria
// V8-28493 : L.Ineson
//  Added FetchByWorkpackageIdAutomationStatus
#endregion

#region Version History: (CCM 801)
// V8-28507 : D.Pleasance
//  Amended methods FetchByWorkpackageId \ FetchByWorkpackageIdAutomationStatus to make use of paging functionality
#endregion

#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
#endregion

#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents  a view of a list of planogram infos.
    /// </summary>
    public sealed class PlanogramInfoListViewModel : ViewStateListObject<PlanogramInfoList, PlanogramInfo>
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramInfoListViewModel() { }
        #endregion

        #region Methods

        #region FetchByPlanogramGroupId

        /// <summary>
        /// Fetches plan infos for all plans belonging to the given group id.
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByPlanogramGroupId(Int32 planGroupId)
        {
            Fetch(new PlanogramInfoList.FetchByPlanogramGroupIdCriteria(planGroupId));
        }

        /// <summary>
        /// Fetches plan infos for all plans belonging to the given group id.
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByPlanogramGroupIdAsync(Int32 planGroupId)
        {
            var criteria = new PlanogramInfoList.FetchByPlanogramGroupIdCriteria(planGroupId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #region FetchByCategoryCode

        /// <summary>
        /// Fetches plan infos for all plans belonging to the given catgeory code
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByCategoryCode(String categoryCode)
        {
            Fetch(new PlanogramInfoList.FetchByCategoryCodeCriteria(categoryCode));
        }

        #endregion

        #region FetchNonDebugByCategoryCode

        /// <summary>
        /// Fetches non debug lan infos for all plans belonging to the given catgeory code
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchNonDebugByCategoryCode(String categoryCode)
        {
            Fetch(new PlanogramInfoList.FetchNonDebugByCategoryCodeCriteria(categoryCode));
        }

        #endregion


        #region FetchByNullCategoryCode

        /// <summary>
        /// Fetches plan infos for all plans with a null catgeory code
        /// </summary>
        /// <param name="planGroupId"></param>
        public void FetchByNullCategoryCode()
        {
            Fetch(new PlanogramInfoList.FetchByNullCategoryCodeCriteria());
        }

        #endregion

        #region FetchByWorkpackagePagingCriteriaId

        /// <summary>
        /// Fetches infos for all plans belonging to the given workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdPagingCriteria(Int32 workpackageId, Byte pageNumber, Int16 pageSize)
        {
            Fetch(new PlanogramInfoList.FetchByWorkpackageIdPagingCriteria(workpackageId, pageNumber, pageSize));
        }

        /// <summary>
        /// Fetches infos for all plans belonging to the given workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdPagingCriteriaAsync(Int32 workpackageId, Byte pageNumber, Int16 pageSize)
        {
            var criteria = new PlanogramInfoList.FetchByWorkpackageIdPagingCriteria(workpackageId, pageNumber, pageSize);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #region FetchByWorkpackageIdAutomationStatusPagingCriteria

        /// <summary>
        /// Fetches infos for all plans belonging to the given workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdAutomationStatusPagingCriteria(Int32 workpackageId, List<ProcessingStatus> automationStatusList, Byte pageNumber, Int16 pageSize)
        {
            Fetch(new PlanogramInfoList.FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(workpackageId, automationStatusList, pageNumber, pageSize));
        }

        /// <summary>
        /// Fetches infos for all plans belonging to the given workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(Int32 workpackageId, ProcessingStatus automationStatus, Byte pageNumber, Int16 pageSize)
        {
            FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId, new List<ProcessingStatus> { automationStatus }, pageNumber, pageSize);
        }


        /// <summary>
        /// Fetches infos for all plans belonging to the given workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(Int32 workpackageId, List<ProcessingStatus> automationStatusList, Byte pageNumber, Int16 pageSize)
        {
            var criteria = new PlanogramInfoList.FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(workpackageId, automationStatusList, pageNumber, pageSize);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #region FetchBySearchCriteria

        /// <summary>
        /// Fetches plan infos that match the given criteria
        /// asynchronously.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="planogramGroupId"></param>
        public void FetchBySearchCriteriaAsync(String name, Int32? planogramGroupId, Int32 entityId)
        {
            var criteria = new PlanogramInfoList.FetchBySearchCriteriaCriteria(name, planogramGroupId, entityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #endregion

    }

    public sealed class PlanogramInfoView : INotifyPropertyChanged
    {
        #region Fields
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Properties

        public Int32 Id
        {
            get { return Model.Id; }
        }

        public String Name
        {
            get { return Model.Name; }
        }

        public PlanogramInfo Model
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                {
                    _calculatedValueResolver =
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCachine*/true);
                }
                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        public PlanogramInfoView(PlanogramInfo model)
        {
            this.Model = model;
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when resolving a calculated parameter.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            e.Result = e.Parameter.FieldInfo.GetValue(this.Model);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates a value from the given expression text.
        /// </summary>
        private object GetCalculatedValue(String expressionText)
        {
            ObjectFieldExpression expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(PlanogramInfo.EnumerateDisplayableFieldInfos());

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;

            //evaluate
            Object returnValue = expression.Evaluate();

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;

            return returnValue;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }

   
}
