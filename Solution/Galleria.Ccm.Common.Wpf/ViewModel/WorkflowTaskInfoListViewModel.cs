﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel for the WorkflowTaskInfoList model object
    /// </summary>
    public sealed class WorkflowTaskInfoListViewModel : ViewStateListObject<WorkflowTaskInfoList, WorkflowTaskInfo>
    {
        #region Constructors
        public WorkflowTaskInfoListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of task infos for the given workpackage
        /// asynchronously.
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdAsync(Int32 workpackageId)
        {
            var criteria = new WorkflowTaskInfoList.FetchByWorkpackageIdCriteria(workpackageId);

            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        /// <summary>
        /// Fetches the list of task infos for the given workflow id.
        /// </summary>
        /// <param name="workflowId"></param>
        public void FetchByWorkflowId(Int32 workflowId)
        {
            Fetch(new WorkflowTaskInfoList.FetchByWorkflowIdCriteria(workflowId));
        }

        #endregion

    }
}
