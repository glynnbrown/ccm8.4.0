﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#region Version History: (CCM 8.0)
// CCM-28023 : L.Ineson
//  Added additional constructors and deny all perm.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Simple class holding object permissions
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ModelPermission<T>
    {
        #region Fields

        private Boolean _canCreate;
        private Boolean _canFetch;
        private Boolean _canEdit;
        private Boolean _canDelete;

        private Boolean _canRefresh;
        #endregion

        #region Properties

        public Boolean CanCreate
        {
            get { return _canCreate; }
        }

        public Boolean CanFetch
        {
            get { return _canFetch; }
        }

        public Boolean CanEdit
        {
            get { return _canEdit; }
        }

        public Boolean CanDelete
        {
            get { return _canDelete; }
        }

        #endregion

        #region Constructor

        public ModelPermission() 
        {
            _canRefresh = true;
            Refresh();
        }

        public ModelPermission(IDictionary<AuthorizationActions, Boolean> permissions)
        {
            _canCreate = permissions[AuthorizationActions.CreateObject];
            _canFetch = permissions[AuthorizationActions.GetObject];
            _canEdit = permissions[AuthorizationActions.EditObject];
            _canDelete = permissions[AuthorizationActions.DeleteObject];
        }

        private ModelPermission(Boolean create, Boolean fetch, Boolean edit, Boolean delete)
        {
            _canCreate = create;
            _canFetch = fetch;
            _canEdit = edit;
            _canDelete = delete;
        }

        #endregion

        #region Methods

        public void Refresh()
        {
            if (!_canRefresh) return;

            //create
            _canCreate = BusinessRules.HasPermission(AuthorizationActions.CreateObject, typeof(T));

            //fetch
            _canFetch = BusinessRules.HasPermission(AuthorizationActions.GetObject, typeof(T));

            //edit
            _canEdit = BusinessRules.HasPermission(AuthorizationActions.EditObject, typeof(T));

            //delete
            _canDelete = BusinessRules.HasPermission(AuthorizationActions.DeleteObject, typeof(T));
        }

        public static ModelPermission<T> DenyAll()
        {
            return new ModelPermission<T>(false, false, false, false);
        }

        #endregion
    }
}
