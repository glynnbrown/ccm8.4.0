﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26124 : I.George
//	Initial version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class InventoryProfileInfoListViewModel : ViewStateListObject<InventoryProfileInfoList, InventoryProfileInfo>
    {
        #region Constructor

        public InventoryProfileInfoListViewModel()
        { }

        #endregion

        #region Methods

        public void FetchCurrentEntity()
        {
            InventoryProfileInfoList.FetchByEntityIdCriteria criteria =
                new InventoryProfileInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            Fetch(criteria);
        }

        #endregion
    }
}
