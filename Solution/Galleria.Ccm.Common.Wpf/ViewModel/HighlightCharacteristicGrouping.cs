﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;
using System.Collections.Specialized;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Collection of HighlightEditorCharacteristicGroupings
    /// </summary>
    public sealed class HighlightCharacteristicGroupingCollection
        : ModelViewCollection<HighlightCharacteristicGrouping, HighlightCharacteristicList, HighlightCharacteristic>
    {
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightCharacteristicGroupingCollection()
        {
            this.CreateViewMethod = CreateView;
        }

        /// <summary>
        /// Creates a new view from the given model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private HighlightCharacteristicGrouping CreateView(HighlightCharacteristic model)
        {
            return new HighlightCharacteristicGrouping(model);
        }
    }

    /// <summary>
    /// Row view for a characteristic grouping.
    /// </summary>
    public sealed class HighlightCharacteristicGrouping : ModelView<HighlightCharacteristic>
    {
        #region Fields

        private readonly BulkObservableCollection<HighlightCharacteristicRule> _rules = new BulkObservableCollection<HighlightCharacteristicRule>();
        private ReadOnlyBulkObservableCollection<HighlightCharacteristicRule> _rulesRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of rules for this row.
        /// Includes a blank.
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightCharacteristicRule> Rules
        {
            get
            {
                if (_rulesRO == null)
                {
                    _rulesRO = new ReadOnlyBulkObservableCollection<HighlightCharacteristicRule>(_rules);
                }
                return _rulesRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="model"></param>
        public HighlightCharacteristicGrouping(HighlightCharacteristic model)
            : base(model)
        {
            _rules.BulkCollectionChanged += Rules_BulkCollectionChanged;

            model.Rules.BulkCollectionChanged += Characteristic_RulesBulkCollectionChanged;

            Characteristic_RulesBulkCollectionChanged(model,
                new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the CurrentCharacteristicRules collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Rules_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightCharacteristicRule r in e.ChangedItems)
                    {
                        r.PropertyChanged += HighlightCharacteristicRule_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightCharacteristicRule r in e.ChangedItems)
                    {
                        r.PropertyChanged -= HighlightCharacteristicRule_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (HighlightCharacteristicRule r in e.ChangedItems)
                    {
                        r.PropertyChanged -= HighlightCharacteristicRule_PropertyChanged;
                    }
                    foreach (HighlightCharacteristicRule r in _rules)
                    {
                        r.PropertyChanged += HighlightCharacteristicRule_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the selected characteristic rules collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Characteristic_RulesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightCharacteristicRule h in e.ChangedItems)
                    {
                        if (!_rules.Contains(h))
                        {
                            _rules.Add(h);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightCharacteristicRule h in e.ChangedItems) _rules.Remove(h);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _rules.Clear();
                    if (sender != null)
                    {
                        foreach (HighlightCharacteristicRule h in ((HighlightCharacteristic)sender).Rules)
                            _rules.Add(h);
                    }
                    break;
            }

            //ensure there is still a blank row in the collection
            EnsureBlankRow();
        }

        /// <summary>
        /// Called whenever a property changes on a highlight filter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighlightCharacteristicRule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HighlightCharacteristicRule rule = (HighlightCharacteristicRule)sender;
            if (rule.Parent == null)
            {
                if (!String.IsNullOrEmpty(rule.Field))
                {
                    //the filter was the blank row which is now no longer blank
                    // so add to the main collection.
                    this.Model.Rules.Add(rule);
                }
            }
        }

        #endregion

        #region Methods

        private void EnsureBlankRow()
        {
            if (_rules.Count == 0 || !_rules.Any(c => c.Parent == null))
            {
                _rules.Add(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Model.Rules.BulkCollectionChanged -= Characteristic_RulesBulkCollectionChanged;
                    _rules.Clear();
                    _rules.BulkCollectionChanged -= Rules_BulkCollectionChanged;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
