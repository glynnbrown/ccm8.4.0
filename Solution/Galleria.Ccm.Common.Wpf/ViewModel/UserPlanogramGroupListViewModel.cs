﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Ineson
//  Created.
// V8-25664 : A.Kuszyk 
//  Removed temporary classes.
// V8-26281  : L.Ineson
//  Merged together versions from both clients and corrected base type.
// V8-26284 : A.Kuszyk
//  Added Favourites list of View Models.
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents a view of the UserPlanogramGroupList moedl object
    /// </summary>
    public sealed class UserPlanogramGroupListViewModel : ViewStateListObject<UserPlanogramGroupList, UserPlanogramGroup>
    {

        //#region Fields
        //private List<PlanogramGroupViewModel> _favourites;
        //#endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public UserPlanogramGroupListViewModel() 
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of groups for the current user and entity.
        /// </summary>
        public void FetchForCurrentUserAndEntity()
        {
            if (CCMClient.ViewState.EntityId == 0) return;

            //Get the id of the current user.
            // Defaulting to 0 so that this doesn't end up crashing out
            // if the model gets set to null.
            Int32 curUserId = 0;
            var domainUserId = DomainPrincipal.CurrentUserId;
            if (domainUserId != null)
            {
                curUserId = (Int32)domainUserId;
            }

            Fetch(new UserPlanogramGroupList.FetchByUserIdCriteria(curUserId));
        }

        /// <summary>
        /// Saves the underlying model.
        /// </summary>
        public void Save()
        {
            Update();
        }

        /// <summary>
        /// Adds the given Planogram Group the favourites list.
        /// </summary>
        /// <param name="group">The Group to add.</param>
        public void AddGroupToFavourites(PlanogramGroup group, Boolean saveImmediately = true)
        {
            //if the group is null then do nothing.
            if (group == null) return;
            if (this.Model == null) throw new ArgumentNullException("Model");

            //create the new group and add it
            this.Model.Add(
                UserPlanogramGroup.NewUserPlanogramGroup((Int32)DomainPrincipal.CurrentUserId, group.Id));

            //save if required.
            if (saveImmediately)
            {
                Save();
            }
        }

        /// <summary>
        /// Removes the record for the given planogram group.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="saveImmediately"></param>
        public void RemoveGroupFromFavourites(PlanogramGroup group, Boolean saveImmediately = true)
        {
            //if the group is null then do nothing.
            if (group == null) return;
            if (this.Model == null) throw new ArgumentNullException("Model");

            //get the item to remove
            UserPlanogramGroup item = this.Model.FirstOrDefault(u => u.PlanogramGroupId == group.Id);
            if (item == null) return;

            //remove it.
            this.Model.Remove(item);

            //save if required.
            if (saveImmediately)
            {
                Save();
            }
        }




        //---------- TODO: REVIEW METHODS BELOW

        ///// <summary>
        ///// Gets the list of Favourites View Models, in name order.
        ///// </summary>
        ///// <param name="planHierarchy">A Planogram Hierarchy View Model, for use if the Favourites list has to be built.</param>
        ///// <returns>The list of Favourites View Models, in name order.</returns>
        ///// <remarks>
        ///// If this is the first time Favourites has been requested, the list is built from scratch
        ///// by retrieving Groups from the Plan Hierarchy that match the groups stored by this
        ///// View's model.
        ///// If not, the private list is returned, which is maintained by the Add and Remove
        ///// methods.
        ///// </remarks>
        //public IEnumerable<PlanogramGroupViewModel> GetFavourites(PlanogramHierarchyViewModel planHierarchy)
        //{
        //    if (planHierarchy == null || planHierarchy.Model == null) throw new ArgumentNullException();

        //    //if (_favourites != null) return _favourites;

        //    var favourites = new List<PlanogramGroupViewModel>();
        //    foreach (var model in this.Model)
        //    {
        //        var planGroup = planHierarchy
        //            .Model
        //            .EnumerateAllGroups()
        //            .FirstOrDefault(g => g.Id == model.PlanogramGroupId);
        //        if (planGroup == null) continue;
        //        favourites.Add(PlanogramGroupViewModel.NewMyCategoriesGroup(planGroup, true));
        //    }
        //    return favourites.OrderBy(g => g.Name);
        //}

        //public void ClearFavourites()
        //{
        //    _favourites = null;
        //}

        ///// <summary>
        ///// Adds the given Planogram Group the favourites list.
        ///// </summary>
        ///// <param name="group">The Group to add.</param>
        //public void AddGroupToFavourites(PlanogramGroupViewModel group)
        //{
        //    if (group == null) throw new ArgumentNullException("group");

        //    // Add group to Model.
        //    var userPlanogramGroup = UserPlanogramGroup.NewUserPlanogramGroup(
        //        (Int32)DomainPrincipal.CurrentUserId, group.Id);
        //    Model.Add(userPlanogramGroup);

        //    // Add group to View.
        //    //if (_favourites == null) return;
        //    //_favourites.Add(PlanogramGroupViewModel.NewMyCategoriesGroup(group.PlanogramGroup, true));
        //    //_favourites = _favourites.OrderBy(g => g.Name).ToList();
        //}

        ///// <summary>
        ///// Removes the given Planogram Group from the favourites list.
        ///// </summary>
        ///// <param name="group">The Group to remove.</param>
        //public void RemoveGroupFromFavourites(PlanogramGroupViewModel group)
        //{
        //    if (group == null) throw new ArgumentNullException("group");

        //    // Remove from Model.
        //    var userPlanogramGroup = Model
        //            .Where(fav => fav.PlanogramGroupId == group.Id)
        //            .FirstOrDefault();
        //    if (userPlanogramGroup != null)
        //    {
        //        Model.Remove(userPlanogramGroup);
        //    }

        //    // Remove from View.
        //    //if (_favourites == null) return;
        //    //if(_favourites.Any(g=>g.Id==group.Id))
        //    //{
        //    //    _favourites.RemoveAll(g=>g.Id==group.Id);
        //    //}
        //}


        #endregion

    }
}
