﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-24779 : D.Pleasance
//  Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Planogram Import Template Info List View Model
    /// </summary>
    public sealed class PlanogramImportTemplateInfoListViewModel : ViewStateListObject<PlanogramImportTemplateInfoList, PlanogramImportTemplateInfo>
    {
        #region Constructor

        public PlanogramImportTemplateInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model for the current entity synchronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new PlanogramImportTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}