﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: CCM800
// V8-26941 : A.Silva 
//  Created.
// V8-27146 : A.Silva 
//  Implemented Group Validation info column.
// V8-27196 : A.Silva 
//  Amended mapping of validation data to column data (dependant on property name, but not on the actual object).
// V8-27393 : L.Ineson
//  Brought up to standard to stop memory leak.
// V8-27411 : M.Pettit
//  Added Lock information properties
//  Updated status tooltip and icons during processing
// V8-27912 : M.Pettit
//  Update valdiation icons to match squaregrid status icons
#endregion
#region Version History: CCM810
//  V8-29802 : M.Brumby
//      Removed MetricValidationResultTypes as it was unused and causing errors.
//  V8-30029 : M.Brumby
//      Made the validation tooltip type TextBox instead of String to accomodate more
//      complex tooltip messages.
// V8-29590 : A.Probyn
//      Added new PlanogramStatusTooltip
//      Updated tooltip layouts
// V8-30079 : L.Ineson
//  Updated to use new CompleteWithWarnings status type.
// V8-30177 : N.Foster
//  Correctly calculate time remaining
#endregion
#region Version History: CCM811
// V8-30410 : N.Haywood
//  Added local time conversion for planogram lock date
// V8-30599 : A.Probyn
//  Added new ValidationTooltip to summarise the tooltip for export purposes
#endregion
#region Version History: CCM820
// V8-30410 : J.Pickup
//  Added local time conversion for planogram validation and metadata refresh. (Was missing from 811).
#endregion
#region Version History: (CCM 8.3.0)
// V8-31832 : L.Ineson
//  Added support for calculated columns.
// CCM-13673 : G.Richards
//  If a workpackage info reference exists and the workpackage status is either pending or queued 
//  then set the corresponding planogram's automation status as pending.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf.Converters;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Image = System.Windows.Controls.Image;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Runtime.CompilerServices;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// A row viewmodel used to display info for a single planogram in a grid.
    /// </summary>
    public sealed class PlanogramRepositoryRow : INotifyPropertyChanged
    {
        #region Static Properties

        private static Dictionary<String, String> _fieldLookup;
        public static Dictionary<String, String> FieldInfoLookup
        {
            get
            {
                if (_fieldLookup == null)
                {
                    _fieldLookup =
                        PlanogramFieldHelper.GetPlanogramObjectFieldInfos()[PlanogramItemType.Planogram]
                        .ToDictionary(info => info.FieldPlaceholder, info => info.PropertyName);

                    //CommonHelper.EnumerateObjectFieldInfos(
                    //typeof(Planogram)).ToDictionary(info => info.FieldPlaceholder, info => info.PropertyName);
                }
                return _fieldLookup;
            }
        }

        #endregion

        #region Fields
        private PlanogramValidationInfo _validationInfo;
        private PlanogramInfo _info;

        private ImageSource _lockedIcon = null;
        private String _lockedToolTip = String.Empty;
        private PlanogramRepositoryRowStatusType _planogramStatusType;

        private ImageSource _automationStatusIcon = null;
        private String _automationStatusText = String.Empty;
        private String _automationStatusEstimatedTimeLeft = String.Empty;
        private String _automationStatusToolTip = String.Empty;

        private string _planogramStatusTooltip = String.Empty;

        private ImageSource _metaDataProcessingIcon = null;
        private String _metaDataProcessingToolTip = String.Empty;

        private Visibility _validationProcessingIconVisibility = Visibility.Collapsed;
        private ImageSource _validationProcessingIcon = null;
        private String _validationProcessingToolTip = String.Empty;

        private ReadOnlyCollection<PlanogramValidationResult> _groupValidationResultTypes;

        private Boolean _isLoadingThumbnail;
        private PlanogramMetadataImageViewModel _thumbnailView;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Property Paths

        public static readonly PropertyPath GroupValidationResultTypesProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.GroupValidationResultTypes);
        public static readonly PropertyPath InfoProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.Info);
        public static readonly PropertyPath LockedIconProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.LockedIcon);
        public static readonly PropertyPath LockedToolTipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.LockedToolTip);
        public static readonly PropertyPath AutomationStatusIconProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.AutomationStatusIcon);
        public static readonly PropertyPath AutomationStatusTextProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.AutomationStatusText);
        public static readonly PropertyPath AutomationStatusToolTipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.AutomationStatusToolTip);
        public static readonly PropertyPath AutomationStatusEstimatedTimeLeftProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.AutomationStatusEstimatedTimeLeft);
        public static readonly PropertyPath PlanogramStatusTypeProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.PlanogramStatusType);
        public static readonly PropertyPath MetaDataProcessingIconProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.MetaDataProcessingIcon);
        public static readonly PropertyPath MetaDataProcessingToolTipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.MetaDataProcessingToolTip);
        public static readonly PropertyPath PlanogramStatusTooltipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.PlanogramStatusTooltip);
        public static readonly PropertyPath ValidationProcessingIconProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationProcessingIcon);
        public static readonly PropertyPath ValidationProcessingVisibilityProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationProcessingVisibility);
        public static readonly PropertyPath ValidationProcessingToolTipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationProcessingToolTip);
        public static readonly PropertyPath ValidationInfoProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationInfo);
        public static readonly PropertyPath ValidationResultsVisibilityProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationResultsVisibility);
        public static readonly PropertyPath ThumbnailProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.Thumbnail);
        public static readonly PropertyPath IsLoadingThumbnailProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.IsLoadingThumbnail);
        public static readonly PropertyPath ValidationIsOutOfDateProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationIsOutOfDate);
        public static readonly PropertyPath ValidationTooltipProperty = WpfHelper.GetPropertyPath<PlanogramRepositoryRow>(p => p.ValidationTooltip);
        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the value of the <see cref="Info" /> property, notifying when the value changes.
        /// </summary>
        public PlanogramInfo Info
        {
            get { return _info; }
            private set
            {
                _info = value;
                OnPropertyChanged(InfoProperty);
            }
        }

        public ImageSource Thumbnail
        {
            get
            {
                if (_thumbnailView == null
                    || (_thumbnailView.Model == null
                        && Info.DateMetadataCalculated.HasValue
                        && !IsLoadingThumbnail))
                {
                    FetchThumbnailAsync();
                }

                if (_thumbnailView != null &&
                    _thumbnailView.Model != null)
                {
                    return ImageHelper.GetBitmapImage(_thumbnailView.Model.ImageData);
                }

                return null;
            }
        }

        public Boolean IsLoadingThumbnail
        {
            get { return _isLoadingThumbnail; }
            private set
            {
                _isLoadingThumbnail = value;
                OnPropertyChanged(IsLoadingThumbnailProperty);
            }
        }

        public ImageSource AutomationStatusIcon
        {
            get { return _automationStatusIcon; }
            private set
            {
                if (_automationStatusIcon != value)
                {
                    _automationStatusIcon = value;
                    OnPropertyChanged(AutomationStatusIconProperty);
                }
            }
        }

        public String AutomationStatusText
        {
            get { return _automationStatusText; }
            private set
            {
                if (_automationStatusText != value)
                {
                    _automationStatusText = value;
                    OnPropertyChanged(AutomationStatusTextProperty);
                }
            }
        }

        public String AutomationStatusEstimatedTimeLeft
        {
            get { return _automationStatusEstimatedTimeLeft; }
            private set
            {
                if (_automationStatusEstimatedTimeLeft != value)
                {
                    _automationStatusEstimatedTimeLeft = value;
                    OnPropertyChanged(AutomationStatusEstimatedTimeLeftProperty);
                }
            }
        }

        public String AutomationStatusToolTip
        {
            get { return _automationStatusToolTip; }
            private set
            {
                if (_automationStatusToolTip != value)
                {
                    _automationStatusToolTip = value;
                    OnPropertyChanged(AutomationStatusToolTipProperty);
                }
            }
        }

        public PlanogramRepositoryRowStatusType PlanogramStatusType
        {
            get { return _planogramStatusType; }
        }

        public String PlanogramStatusTooltip
        {
            get { return _planogramStatusTooltip; }
        }

        public ImageSource LockedIcon
        {
            get { return _lockedIcon; }
            private set
            {
                if (_lockedIcon != value)
                {
                    _lockedIcon = value;
                    OnPropertyChanged(LockedIconProperty);
                }
            }
        }

        public String LockedToolTip
        {
            get { return _lockedToolTip; }
            private set
            {
                if (_lockedToolTip != value)
                {
                    _lockedToolTip = value;
                    OnPropertyChanged(LockedToolTipProperty);
                }
            }
        }

        public ImageSource MetaDataProcessingIcon
        {
            get { return _metaDataProcessingIcon; }
            private set
            {
                if (_metaDataProcessingIcon != value)
                {
                    _metaDataProcessingIcon = value;
                    OnPropertyChanged(MetaDataProcessingIconProperty);
                }
            }
        }

        public String MetaDataProcessingToolTip
        {
            get { return _metaDataProcessingToolTip; }
            private set
            {
                if (_metaDataProcessingToolTip != value)
                {
                    _metaDataProcessingToolTip = value;
                    OnPropertyChanged(MetaDataProcessingToolTipProperty);
                }
            }
        }

        public ImageSource ValidationProcessingIcon
        {
            get { return _validationProcessingIcon; }
            private set
            {
                if (_validationProcessingIcon != value)
                {
                    _validationProcessingIcon = value;
                    OnPropertyChanged(ValidationProcessingIconProperty);
                }
            }
        }

        public Boolean ValidationIsOutOfDate
        {
            get
            {
                if (Info != null)
                {
                    if (Info.ValidationIsOutOfDate)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public Visibility ValidationProcessingVisibility
        {
            //get { return ValidationProcessingIcon != null ? Visibility.Visible : Visibility.Collapsed; }
            get { return ValidationIsOutOfDate ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility ValidationResultsVisibility
        {
            //get { return ValidationProcessingIcon == null ? Visibility.Visible : Visibility.Collapsed; }
            get { return !ValidationIsOutOfDate ? Visibility.Visible : Visibility.Collapsed; }
        }

        public String ValidationProcessingToolTip
        {
            get { return _validationProcessingToolTip; }
            private set
            {
                if (_validationProcessingToolTip != value)
                {
                    _validationProcessingToolTip = value;
                    OnPropertyChanged(ValidationProcessingToolTipProperty);
                }
            }
        }

        /// <summary>
        /// Returns a validation tooltip summary depending on status
        /// </summary>
        public String ValidationTooltip
        {
            get
            {
                if (this.ValidationProcessingVisibility == Visibility.Visible)
                {
                    return this.ValidationProcessingToolTip;
                }
                else
                {
                    if (this.GroupValidationResultTypes.Count > 0)
                    { 
                        return this.GroupValidationResultTypes.Last().TooltipText;
                    }
                }
                return null;
            }
        }

        /// <summary>
        ///     Gets or sets the value of the <see cref="ValidationInfo" /> property, notifying when the value changes.
        /// </summary>
        public PlanogramValidationInfo ValidationInfo
        {
            get { return _validationInfo; }
            private set
            {
                _validationInfo = value;
                OnPropertyChanged(ValidationInfoProperty);
            }
        }

        /// <summary>
        ///     Gets or sets the value of the <see cref="GroupValidationResultTypes" /> property, notifying when the value changes.
        /// </summary>
        public ReadOnlyCollection<PlanogramValidationResult> GroupValidationResultTypes
        {
            get { return _groupValidationResultTypes; }
            private set
            {
                _groupValidationResultTypes = value;
                OnPropertyChanged(GroupValidationResultTypesProperty);
            }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = 
                        new CalculatedValueResolver(GetCalculatedValue, /*enableCaching*/true);

                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanogramRepositoryRow(PlanogramInfo info, PlanogramValidationInfo validationInfo)
        {
            UpdateInfo(info, validationInfo);
        }

        public PlanogramRepositoryRow(WorkpackageInfo workpackageInfo, PlanogramInfo info, PlanogramValidationInfo validationInfo)
        {
            UpdateInfo(workpackageInfo, info, validationInfo);
        }

        #endregion

        #region Methods

        public void UpdateInfo(PlanogramInfo newInfo, PlanogramValidationInfo validationInfo)
        {
            UpdateInfo(null, newInfo, validationInfo);
        }
        
        public void UpdateInfo(WorkpackageInfo workpackageInfo, PlanogramInfo newInfo, PlanogramValidationInfo validationInfo)
        {
            Debug.Assert(newInfo != null && (this.Info == null || (newInfo.Id == this.Info.Id)), "Incorrect info passed");

            if (newInfo == null) return;

            DateTime? oldDateLastModified = (this.Info != null) ? (DateTime?)this.Info.DateLastModified : null;

            this.Info = newInfo;
            
            //Set the lock icon and tooltip
            ImageSource lockedIcon;
            String lockedToolTip;
            SetLockedProperties(_info, out lockedIcon, out lockedToolTip);

            this.LockedIcon = lockedIcon;
            this.LockedToolTip = lockedToolTip;

            //Set the automation status icon, text and tooltip
            ImageSource automationStatusIcon;
            String automationStatusText;
            String automationStatusToolTip;
            String automationStatusEstimatedTimeLeft;
            PlanogramRepositoryRowStatusType statusType;
            SetAutomationProperties(workpackageInfo, _info, out statusType, out automationStatusIcon, out automationStatusText, out automationStatusToolTip, out automationStatusEstimatedTimeLeft);

            this.AutomationStatusIcon = automationStatusIcon;
            this.AutomationStatusText = automationStatusText;
            this.AutomationStatusToolTip = automationStatusToolTip;
            this.AutomationStatusEstimatedTimeLeft = automationStatusEstimatedTimeLeft;

            //Set the metadata processing icon and tooltip
            ImageSource metaDataProcessingIcon;
            String metaDataProcessingToolTip;
            SetMetaDataProcessingProperties(_info, out metaDataProcessingIcon, out metaDataProcessingToolTip);

            this.MetaDataProcessingIcon = metaDataProcessingIcon;
            this.MetaDataProcessingToolTip = metaDataProcessingToolTip;

            //Set the validation processing icon and tooltip
            ImageSource validationProcessingIcon;
            String validationProcessingToolTip;
            SetValidationProcessingProperties(_info, out validationProcessingIcon, out validationProcessingToolTip);

            this.ValidationProcessingIcon = validationProcessingIcon;
            this.ValidationProcessingToolTip = validationProcessingToolTip;

            //Set planogram status toolip
            SetPlanogramStatusString(_info, out _planogramStatusTooltip);

            if (validationInfo != null)
            {
                this.ValidationInfo = validationInfo;

                this.GroupValidationResultTypes = GetGroupValidationResults(validationInfo);
            }

            OnPropertyChanged(ValidationIsOutOfDateProperty);
            OnPropertyChanged(ValidationProcessingVisibilityProperty);
            OnPropertyChanged(ValidationResultsVisibilityProperty);

            //null off the thumbnail if it has changed
            if (_thumbnailView != null &&
                newInfo.DateLastModified != oldDateLastModified)
            {
                _thumbnailView = null;
                OnPropertyChanged(ThumbnailProperty);
            }

            //refresh calculated values
            if (_calculatedValueResolver != null) _calculatedValueResolver.RefreshValues();
        }

        /// <summary>
        /// Sets the locekd icon and tooltip properties for the passed Planogram Info
        /// </summary>
        /// <param name="info">The planogram info to interrogate</param>
        /// <param name="icon">The returned icon</param>
        /// <param name="toolTip">The returned tooltip</param>
        private static void SetLockedProperties(PlanogramInfo info, out ImageSource icon, out String toolTip)
        {
            if (info.IsLocked)
            {
                //Set the tooltip
                String lockOwner = info.LockUserDisplayName;
                if (info.LockType == PackageLockType.System)
                {
                    lockOwner = Message.PlanogramRepository_LockedProcessToolTip;
                }

                String lockType = info.LockType == PackageLockType.System ?
                    PackageLockTypeHelper.FriendlyNames[(PackageLockType)info.LockType] + " " : String.Empty;

                toolTip = String.Format("{0} {1}{2}\r\n{3} {4}",
                    Message.PlanogramRepository_LockedByToolTip,
                    lockType,
                    lockOwner,
                    Message.PlanogramRepository_LockedDateToolTip,
                    String.Format("{0:g}", ((DateTime)info.LockDate).ToLocalTime()));

                //set the icon
                if (info.LockUserId == DomainPrincipal.CurrentUserId)
                {
                    icon = ImageResources.LockedByCurrentUser_16;
                }
                else
                {
                    icon = ImageResources.Locked_16;
                }
            }
            else
            {
                icon = null;
                toolTip = String.Empty;
            }
        }

        /// <summary>
        /// Sets the automation display properties for the passed planogram info object
        /// </summary>
        /// <param name="workpackageInfo">The workpackage info to interrogate</param>
        /// <param name="info">The planogram info to interrogate</param>
        /// <param name="icon">The returned icon</param>
        /// <param name="description">The returned description</param>
        /// <param name="toolTip">The returned tooltip</param>
        private static void SetAutomationProperties(WorkpackageInfo workpackageInfo, PlanogramInfo info, out PlanogramRepositoryRowStatusType statusType, out ImageSource icon, out String description, out String toolTip, out String estimatedTimeRemaining)
        {
            bool workpackgeReference = false;

            if (workpackageInfo != null)
            {                
                if (workpackageInfo.ProcessingStatus == WorkpackageProcessingStatusType.Pending || workpackageInfo.ProcessingStatus == WorkpackageProcessingStatusType.Queued)
                {
                    workpackgeReference = true;
                }
            }

            if (workpackgeReference)
            {
                icon = null;
                description = Message.PlanogramRepository_AutomationStatusPendingDescription;
                toolTip = Message.PlanogramRepository_AutomationStatusPendingToolTip;
                estimatedTimeRemaining = String.Empty;
                statusType = PlanogramRepositoryRowStatusType.Incomplete;
            }
            else
            {
                switch (info.AutomationProcessingStatus)
                {
                    case ProcessingStatus.Pending:
                        icon = null;
                        description = Message.PlanogramRepository_AutomationStatusPendingDescription;
                        toolTip = Message.PlanogramRepository_AutomationStatusPendingToolTip;
                        estimatedTimeRemaining = String.Empty;
                        statusType = PlanogramRepositoryRowStatusType.Incomplete;
                        break;

                    case ProcessingStatus.Queued:
                        icon = null;
                        //TODO : Add Queued timing information - adfter X minutes, add 
                        //"This job has been waiting for X minutes" to the current description
                        description = Message.PlanogramRepository_AutomationStatusQueuedDescription;
                        toolTip = Message.PlanogramRepository_AutomationStatusQueuedToolTip;
                        estimatedTimeRemaining = String.Empty;
                        statusType = PlanogramRepositoryRowStatusType.Incomplete;
                        break;

                    #region Processing
                    case ProcessingStatus.Processing:
                        {
                            icon = null;
                            description = Message.PlanogramRepository_AutomationStatusProcessingDescription;
                            estimatedTimeRemaining = String.Empty;
                            statusType = PlanogramRepositoryRowStatusType.Incomplete;


                            //Tooltip Line 1:
                            //The job is being processed and is estimated to complete in {0}.
                            String minsRemaining = CalculateAutomationProcessMinutesRemaining(info, out estimatedTimeRemaining);
                            String msgLine1 = String.Format(Message.PlanogramRepository_AutomationStatusProcessingToolTipLine1, minsRemaining);

                            //Tooltip Line 2:
                            String msgLine2 = String.Empty;
                            if (info.AutomationDateStarted != null)
                            {
                                //This job was started on {0}.
                                DateTime displayTime = ((DateTime)info.AutomationDateStarted).ToLocalTime();
                                msgLine2 = String.Format(
                                            Message.PlanogramRepository_AutomationStatusProcessingToolTipLine2,
                                            displayTime.ToString("F"));
                            }
                            else
                            {
                                //This job's start date has not yet been set.
                                msgLine2 = Message.PlanogramRepository_AutomationStatusProcessingToolTipLine2UnknownDate;
                            }

                            //Tooltip Line 3:
                            //Current step : {0}.
                            String msgLine3 = String.Format(
                                Message.PlanogramRepository_AutomationStatusProcessingToolTipLine3,
                                info.AutomationProcessingStatusDescription.ToString());

                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine(msgLine1);
                            builder.AppendLine(msgLine2);
                            builder.AppendLine(msgLine3);
                            toolTip = builder.ToString();
                        }
                        break;
                    #endregion

                    case ProcessingStatus.Complete:
                        statusType = PlanogramRepositoryRowStatusType.Complete;
                        icon = PlanogramRepositoryRowStatusTypeHelper.FriendlyIcons[statusType];
                        description = PlanogramRepositoryRowStatusTypeHelper.FriendlyNames[statusType];
                        toolTip = Message.PlanogramRepository_AutomationStatusCompleteToolTip;
                        estimatedTimeRemaining = String.Empty;
                        break;

                    case ProcessingStatus.CompleteWithWarnings:
                        statusType = PlanogramRepositoryRowStatusType.CompleteWithWarnings;
                        icon = PlanogramRepositoryRowStatusTypeHelper.FriendlyIcons[statusType];
                        description = PlanogramRepositoryRowStatusTypeHelper.FriendlyNames[statusType];
                        toolTip = Message.PlanogramRepository_AutomationStatusCompleteToolTip;
                        estimatedTimeRemaining = String.Empty;
                        break;

                    case ProcessingStatus.Failed:
                        statusType = PlanogramRepositoryRowStatusType.Failed;
                        icon = PlanogramRepositoryRowStatusTypeHelper.FriendlyIcons[statusType];
                        description = PlanogramRepositoryRowStatusTypeHelper.FriendlyNames[statusType];
                        toolTip = Message.PlanogramRepository_AutomationStatusFailedToolTip;
                        estimatedTimeRemaining = String.Empty;
                        break;

                    default:
                        statusType = PlanogramRepositoryRowStatusType.Incomplete;
                        icon = null;
                        description = String.Empty;
                        toolTip = String.Empty;
                        estimatedTimeRemaining = String.Empty;
                        break;
                }
            }
        }

        /// <summary>
        /// Calculate the time remaining for an automation processing task
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static String CalculateAutomationProcessMinutesRemaining(PlanogramInfo info, out String estimatedTimeRemaining)
        {
            estimatedTimeRemaining = String.Empty;
            try
            {
                if (info.AutomationDateStarted != null && info.AutomationDateLastUpdated != null &&
                info.AutomationProgressMax > 0)
                {
                    //We can only calculate the remaining time if some progress has already been made
                    if (info.AutomationProgressCurrent > 0 && (DateTime)info.AutomationDateLastUpdated > (DateTime)info.AutomationDateStarted)
                    {
                        //have to use utcnow so that a stuck job's processing time increases
                        Double minutesSoFar = (DateTime.UtcNow).Subtract((DateTime)info.AutomationDateStarted).TotalMinutes;
                        Double minsRemaining = minutesSoFar * (((Double)info.AutomationProgressMax / (Double)info.AutomationProgressCurrent) - 1);

                        if (minsRemaining < 1)
                        {
                            // return "less than 1 minute"
                            estimatedTimeRemaining = "< 1m";
                            return Message.PlanogramRepository_AutomationStatusProcessingLessThanOneMinute;
                        }
                        else if (minsRemaining < 1.5 && minsRemaining > 1)
                        {
                            // return "1 minute"
                            estimatedTimeRemaining = "1m";
                            return String.Format("1 {0}", Message.PlanogramRepository_AutomationStatusProcessingMinute);
                        }
                        else
                        {
                            // return "XX minutes"
                            estimatedTimeRemaining = String.Format("{0}m", minsRemaining.ToString("F0"));
                            return String.Format("{0} {1}", minsRemaining.ToString("F0"), Message.PlanogramRepository_AutomationStatusProcessingMinutes);
                        }
                    }
                }
            }
            catch
            {
                //don't cry over spilt milk
            }
            // return amibiguous "a few minutes"
            return Message.PlanogramRepository_AutomationStatusProcessingMinutesUnknown;
        }

        /// <summary>
        /// Sets the metadata display properties for the passed planogram info object
        /// </summary>
        /// <param name="info">The planogram info to interrogate</param>
        /// <param name="icon">The returned icon</param>
        /// <param name="toolTip">The returned tooltip</param>
        private static void SetMetaDataProcessingProperties(PlanogramInfo info, out ImageSource icon, out String toolTip)
        {
            icon = null;
            toolTip = String.Empty;

            if (info.DateMetadataCalculated == null)
            {
                //There is no calculated meta data so it needs updating.
                // We only display the icon if the MetaData process is updating the data

                //Set the icon
                icon = ImageResources.MetaDataStatusProcessing_16;

                //Set the ToolTip
                String dateLastUpdated = info.MetaDataDateLastUpdated != null ? String.Format("{0:f}", ((DateTime)info.MetaDataDateLastUpdated).ToLocalTime()) : Message.PlanogramRepository_GenericStatus_DateUnknown;

                switch (info.MetaDataProcessingStatus)
                {
                    case ProcessingStatus.Processing:
                        try
                        {
                            //Plan Information is currently being updated.
                            //{0}% complete as of {1}.
                            toolTip = String.Format(Message.PlanogramRepository_MetaDataProcessingToolTip,
                                ((Single)info.MetaDataProgress).ToString("P0"),
                                dateLastUpdated);
                        }
                        catch
                        {
                            //don't cry over spilt milk 
                        }
                        break;

                    default:
                        String msgLine1 = Message.PlanogramRepository_MetaDataOutOfDateToolTipLine1;
                        String msgLine2 = String.Format(Message.PlanogramRepository_MetaDataOutOfDateToolTipLine2, String.Format("{0:f}", ((DateTime)info.DateLastModified).ToLocalTime()));
                        String msgLine3 = String.Format(Message.PlanogramRepository_MetaDataOutOfDateToolTipLine3, dateLastUpdated);

                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(msgLine1);
                        builder.AppendLine(msgLine2);
                        builder.Append(msgLine3);
                        toolTip = builder.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the validation display properties for the passed planogram info object
        /// </summary>
        /// <param name="info">The planogram info to interrogate</param>
        /// <param name="icon">The returned icon</param>
        /// <param name="toolTip">The returned tooltip</param>
        private static void SetValidationProcessingProperties(PlanogramInfo info, out ImageSource icon, out String toolTip)
        {
            icon = null;
            toolTip = String.Empty;

            if (info.ValidationIsOutOfDate)
            {
                //Set the icon
                icon = ImageResources.ValidationStatusProcessing_16;

                //Set the ToolTip
                String dateLastUpdated = info.ValidationDateLastUpdated != null ? String.Format("{0:f}", ((DateTime)info.ValidationDateLastUpdated).ToLocalTime()): Message.PlanogramRepository_GenericStatus_DateUnknown;

                switch (info.ValidationProcessingStatus)
                {
                    case ProcessingStatus.Processing:
                        try
                        {
                            //The validation results are currently being updated.
                            //{0}% complete as of {1}.
                            toolTip = String.Format(Message.PlanogramRepository_ValidationResultsProcessingToolTip,
                                ((Single)info.ValidationProgress).ToString("P0"),
                                dateLastUpdated);
                        }
                        catch
                        {
                            //don't cry over spilt milk 
                        }
                        break;

                    default:
                        //The validation results are out of date and will be updated shortly.
                        //Last Planogram Edit: {0}
                        //Last Validation Data Update: {0}
                        String msgLine1 = Message.PlanogramRepository_ValidationResultsOutOfDateToolTipLine1;
                        String msgLine2 = String.Format(Message.PlanogramRepository_ValidationResultsOutOfDateToolTipLine2, String.Format("{0:f}", ((DateTime)info.DateLastModified).ToLocalTime()));
                        String msgLine3 = String.Format(Message.PlanogramRepository_ValidationResultsOutOfDateToolTipLine3, dateLastUpdated);

                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(msgLine1);
                        builder.AppendLine(msgLine2);
                        builder.Append(msgLine3);
                        toolTip = builder.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Returns the list of group results for the given source.
        /// </summary>
        private static ReadOnlyCollection<PlanogramValidationResult> GetGroupValidationResults(PlanogramValidationInfo source)
        {
            if (source == null) return new ReadOnlyCollection<PlanogramValidationResult>(new List<PlanogramValidationResult>());

            List<PlanogramValidationResult> results = new List<PlanogramValidationResult>(source.Groups.Count);

            List<PlanogramValidationGroupInfo> orderedGroups = source.Groups.OrderBy(g => g.Id).ToList();
            //TEMP: Cycle through in id order to make sure that they dont come out muddled.
            // should really add an order number but this will do short term.
            foreach (var group in orderedGroups)
            {
                results.Add(new PlanogramValidationResult(ToIcon16x16(group.ResultType), group.Id, orderedGroups));
            }


            return new ReadOnlyCollection<PlanogramValidationResult>(results);
        }

        /// <summary>
        /// Static method to construct the error status string
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static void SetPlanogramStatusString(PlanogramInfo info, out String tooltip)
        {
            StringBuilder builder = new StringBuilder();

            if (info.MetaNoOfErrors.HasValue && info.MetaNoOfErrors.Value > 0)
            {
                builder.AppendLine(String.Format(Message.PlanogramRepository_StatusToolTip_NoOfErrorsLine, info.MetaNoOfErrors));
            }
            if (info.MetaNoOfWarnings.HasValue && info.MetaNoOfWarnings.Value > 0)
            {
                builder.AppendLine(String.Format(Message.PlanogramRepository_StatusToolTip_NoOfWarningsLine, info.MetaNoOfWarnings));
            }
            if (info.MetaNoOfInformationEvents.HasValue && info.MetaNoOfInformationEvents.Value > 0)
            {
                builder.AppendLine(String.Format(Message.PlanogramRepository_StatusToolTip_NoOfInformationEventsLine, info.MetaNoOfInformationEvents));
            }

            tooltip = builder.ToString().TrimEnd('\r', '\n'); ;
        }

        /// <summary>
        ///     This extension method maps the <see cref="PlanogramValidationTemplateResultType"/> values to their icons.
        /// </summary>
        /// <param name="type">The value to retrieve the icon for.</param>
        /// <returns>An <see cref="ImageSource"/> with the icon to be used for the given <paramref name="type"/>.</returns>
        private static ImageSource ToIcon16x16(PlanogramValidationTemplateResultType type)
        {
            switch (type)
            {
                case PlanogramValidationTemplateResultType.Green:
                    return ImageResources.ValidationResult_Ok_16;
                case PlanogramValidationTemplateResultType.Yellow:
                    return ImageResources.ValidationResult_Warning_16;
                case PlanogramValidationTemplateResultType.Red:
                    return ImageResources.ValidationResult_Fail_16;
                default:
                    return null;
            }
        }

        ///// <summary>
        /////     This extension method maps the <see cref="PlanogramValidationTemplateResultType"/> values to their icons.
        ///// </summary>
        ///// <param name="type">The value to retrieve the icon for.</param>
        ///// <returns>An <see cref="ImageSource"/> with the icon to be used for the given <paramref name="type"/>.</returns>
        //public static ImageSource ToIcon32x32(this PlanogramValidationTemplateResultType type)
        //{
        //    switch (type)
        //    {
        //        case PlanogramValidationTemplateResultType.Green:
        //            return ImageResources.ValidationResult_Ok_32;
        //        case PlanogramValidationTemplateResultType.Yellow:
        //            return ImageResources.ValidationResult_Warning_32;
        //        case PlanogramValidationTemplateResultType.Red:
        //            return ImageResources.ValidationResult_Fail_32;
        //        default:
        //            return null;
        //    }
        //}

        #region FetchThumbnailAsync

        private void FetchThumbnailAsync()
        {
            IsLoadingThumbnail = true;

            _thumbnailView = new PlanogramMetadataImageViewModel();
            _thumbnailView.ModelChanged += ThumbnailView_ModelChanged;

            _thumbnailView.FetchThumbnailByPlanogramIdAsync(this.Info.Id);
        }

        private void ThumbnailView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramMetadataImage> e)
        {
            ((PlanogramMetadataImageViewModel)sender).ModelChanged -= ThumbnailView_ModelChanged;
            this.IsLoadingThumbnail = false;
            OnPropertyChanged(ThumbnailProperty);
        }

        #endregion

        #region CalculatedValueResolver

        /// <summary>
        /// Called when resolving a calculated parameter.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            e.Result = e.Parameter.FieldInfo.GetValue(this.Info);
        }


        private object GetCalculatedValue(String expressionText)
        {
            ObjectFieldExpression expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(PlanogramInfo.EnumerateDisplayableFieldInfos());

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;

            //evaluate
            Object returnValue = expression.Evaluate();

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;

            return returnValue;
        }

        #endregion

        #endregion

        #region Static Helpers

        //TODO: Move these to code behind.

        public static DataTemplate CreateDataTemplate(ObjectFieldInfo field, DisplayUnitOfMeasureCollection uomCol)
        {
            var dataTemplate = new DataTemplate(typeof(PlanogramRepositoryRow));
            var panelElement = new FrameworkElementFactory(typeof(DockPanel));
            panelElement.SetValue(DockPanel.LastChildFillProperty, true);
            panelElement.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
            panelElement.AppendChild(CreateIconElement(field));
            panelElement.AppendChild(CreateTextBlockElement(field, uomCol));
            dataTemplate.VisualTree = panelElement;

            return dataTemplate;
        }

        private static FrameworkElementFactory CreateIconElement(ObjectFieldInfo field)
        {
            var elementFactory = new FrameworkElementFactory(typeof(Image));
            var elementSize = new Size(16, 16);
            var marginThickness = new Thickness(3, 0, 3, 0);
            var multiBinding = new MultiBinding { Converter = new DictionaryKeyToValueConverter() };
            multiBinding.Bindings.Add(new Binding { Source = field.PropertyName });
            multiBinding.Bindings.Add(new Binding("MetricValidationResultTypes"));

            elementFactory.SetBinding(Image.SourceProperty, multiBinding);
            elementFactory.SetValue(FrameworkElement.WidthProperty, elementSize.Width);
            elementFactory.SetValue(FrameworkElement.HeightProperty, elementSize.Height);
            elementFactory.SetValue(FrameworkElement.MarginProperty, marginThickness);
            return elementFactory;
        }

        private static FrameworkElementFactory CreateTextBlockElement(ObjectFieldInfo field,
            DisplayUnitOfMeasureCollection uomCol)
        {
            var valueBinding = new Binding("Info." + field.PropertyName);
            var marginThickness = new Thickness(3, 0, 3, 0);
            var elementFactory = new FrameworkElementFactory(typeof(TextBlock));
            elementFactory.SetBinding(TextBlock.TextProperty, valueBinding);
            elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            elementFactory.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
            elementFactory.SetValue(FrameworkElement.MarginProperty, marginThickness);
            uomCol.ApplyConverter(valueBinding, field.PropertyDisplayType, field.PropertyType);
            return elementFactory;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));

                if (property.Path == "Info")
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(LockedIconProperty.Path));
                    PropertyChanged(this, new PropertyChangedEventArgs(LockedToolTipProperty.Path));
                }
            }
        }

        #endregion

    }

    #region PlanogramValidationResult
    public class PlanogramValidationResult
    {
        public PlanogramValidationResult(ImageSource icon, Int32 groupId, IEnumerable<PlanogramValidationGroupInfo> groups)
        {
            Icon = icon;

            this.ToolTip = new TextBlock();
            if (groups.Any())
            {
                foreach (PlanogramValidationGroupInfo group in groups)
                {
                    if (group.Id == groupId)
                    {
                        //Make the validation group line we are hovering over bold
                        //so that it can be easily seen amid the other validation
                        //groups
                        ToolTip.Inlines.Add(new Bold(new Run(group.ToString())));
                    }
                    else
                    {
                        ToolTip.Inlines.Add(group.ToString());
                    }
                    ToolTip.Inlines.Add(Environment.NewLine);
                }
                //we don't need the last new line statement
                ToolTip.Inlines.Remove(ToolTip.Inlines.Last());

                TooltipText = String.Join(", ", groups);
            }
        }

        public ImageSource Icon { get; private set; }
        public TextBlock ToolTip { get; private set; }
        public String TooltipText { get; private set; }
    }
    #endregion

    #region PlanogramRepositoryRowStatusType

    /// <summary>
    /// UI Planogram row status enum
    /// </summary>
    public enum PlanogramRepositoryRowStatusType
    {
        Complete,
        CompleteWithWarnings,
        Failed,
        Incomplete
    }

    public static class PlanogramRepositoryRowStatusTypeHelper
    {
        public static readonly Dictionary<PlanogramRepositoryRowStatusType, String> FriendlyNames =
            new Dictionary<PlanogramRepositoryRowStatusType, String>()
            {
                {PlanogramRepositoryRowStatusType.Complete, Message.Enum_PlanogramRepositoryRowStatusType_Complete},
                {PlanogramRepositoryRowStatusType.CompleteWithWarnings, ProcessingStatusTypeHelper.FriendlyNames[ProcessingStatus.CompleteWithWarnings]},
                {PlanogramRepositoryRowStatusType.Failed, Message.Enum_PlanogramRepositoryRowStatusType_Failed},
                {PlanogramRepositoryRowStatusType.Incomplete, String.Empty},
            };

        public static readonly Dictionary<PlanogramRepositoryRowStatusType, ImageSource> FriendlyIcons =
            new Dictionary<PlanogramRepositoryRowStatusType, ImageSource>()
            {
                {PlanogramRepositoryRowStatusType.Complete, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusComplete_16},
                {PlanogramRepositoryRowStatusType.CompleteWithWarnings, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusCompleteWithErrorsWarnings_16},
                {PlanogramRepositoryRowStatusType.Failed, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusFailed_16},
                {PlanogramRepositoryRowStatusType.Incomplete, null},
            };
    }
    #endregion
}