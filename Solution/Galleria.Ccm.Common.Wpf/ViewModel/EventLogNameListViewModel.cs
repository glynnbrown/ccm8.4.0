﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class EventLogNameListViewModel : ViewStateObject<EventLogNameList>
    {
        #region Constructor

        public EventLogNameListViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches infos for all available entities
        /// </summary>
        public void FetchAll()
        {
            Fetch(new EventLogNameList.FetchAllCriteria());
        }

        #endregion
    }
}
