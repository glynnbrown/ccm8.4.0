﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Location Space List view model
    /// </summary>
    public sealed class LocationSpaceInfoListViewModel : ViewStateListObject<LocationSpaceInfoList, LocationSpaceInfo>
    {
        #region Constructor

        public LocationSpaceInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the model for the current entity synchronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new LocationSpaceInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
