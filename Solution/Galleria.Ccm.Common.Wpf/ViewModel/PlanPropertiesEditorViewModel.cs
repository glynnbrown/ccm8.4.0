﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//  Added an editor window for the planogram properties
// V8-29860 : M.Shelley
//  Added the PlanogramType property
// V8-30059 : L.Ineson
//  Amended edit properties to make setting individual values optional.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Viewmodel for the planogram properties editor
    /// </summary>
    public sealed class PlanPropertiesEditorViewModel : WindowViewModelBase
    {
        #region Fields

        private Int32 _entityId;
        private Int32 _selectedCount = 0;
        private Boolean? _dialogResult;

        private PlanogramAttributes _attributes;

        private readonly ClusterSchemeInfoListViewModel _clusterSchemeInfoListViewModel = new ClusterSchemeInfoListViewModel();

        #endregion

        #region Property Binding Paths

        // Properties
        public static readonly PropertyPath SelectedCountProperty = GetPropertyPath<PlanPropertiesEditorViewModel>(p => p.SelectedCount);
        public static readonly PropertyPath AttributesProperty = GetPropertyPath<PlanPropertiesEditorViewModel>(p => p.Attributes);

        // Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PlanPropertiesEditorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanPropertiesEditorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the count of the number of selected rows.
        /// </summary>
        public Int32 SelectedCount
        {
            get { return _selectedCount; }
        }

        /// <summary>
        /// Gets/Sets the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        /// <summary>
        /// Returns the attributes model.
        /// </summary>
        public PlanogramAttributes Attributes
        {
            get { return _attributes; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="selectedRowItems"></param>
        public PlanPropertiesEditorViewModel(Int32 entityId, IEnumerable<PlanogramRepositoryRow> selectedRowItems)
        {
            if (selectedRowItems == null)
            {
                return;
            }

            _entityId = entityId;
            _selectedCount = selectedRowItems.Count();

            // Set initial display values
            PlanogramAttributes att = PlanogramAttributes.NewPlanogramAttributes(null);
            _attributes = att;

            // Configure the status
            var tmpStatusList = selectedRowItems.Select(x => x.Info.Status).Distinct();
            if (tmpStatusList.Count() == 1)
            {
                att.Status = tmpStatusList.First();
            }

            // Configure the dates
            var tmpDateApprovedList = selectedRowItems.Select(x => x.Info.DateApproved).Distinct();
            if (tmpDateApprovedList.Count() == 1)
            {
                att.DateApproved = tmpDateApprovedList.First();
            }

            var tmpDateArchivedList = selectedRowItems.Select(x => x.Info.DateArchived).Distinct();
            if (tmpDateArchivedList.Count() == 1)
            {
                att.DateArchived = tmpDateArchivedList.First();
            }

            var tmpDateWipList = selectedRowItems.Select(x => x.Info.DateWip).Distinct();
            if (tmpDateWipList.Count() == 1)
            {
                att.DateWip = tmpDateWipList.First();
            }

            // Configure the location details
            var tmpLocationsList = selectedRowItems.Select(x => x.Info.LocationCode).Distinct();
            if (tmpLocationsList.Count() == 1)
            {
                att.LocationCode = tmpLocationsList.First();
                att.LocationName = selectedRowItems.Select(x => x.Info.LocationName).Distinct().First();
            }

            // Configure the category
            var tmpCategoryList = selectedRowItems.Select(x => x.Info.CategoryCode).Distinct();
            if (tmpCategoryList.Count() == 1)
            {
                att.CategoryCode = tmpCategoryList.First();
                att.CategoryName = selectedRowItems.Select(x => x.Info.CategoryName).Distinct().First();
            }

            // Configure the cluster details
            var tmpClusterSchemaList = selectedRowItems.Select(x => x.Info.ClusterSchemeName).Distinct();
            if (tmpClusterSchemaList.Count() == 1)
            {
                att.ClusterSchemeName = tmpClusterSchemaList.First();
            }

            var tmpClusterList = selectedRowItems.Select(x => x.Info.ClusterName).Distinct();
            if (tmpClusterList.Count() == 1)
            {
                att.ClusterName = tmpClusterList.First();
            }

            // Configure the planogram type 
            var tmpPlanType = selectedRowItems.Select(x => x.Info.PlanogramType).Distinct();
            if (tmpPlanType.Count() == 1)
            {
                att.PlanogramType = tmpPlanType.First();
            }

            att.PropertyChanged += Attributes_PropertyChanged;
        }

       

        #endregion

        #region Commands

        #region SelectCategoryCommand

        private RelayCommand _selectCategoryCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SelectCategoryCommand
        {
            get
            {
                if (_selectCategoryCommand == null)
                {
                    _selectCategoryCommand = new RelayCommand(
                        p => SelectCategoryCommand_Executed(),
                        p => SelectCategoryCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_selectCategoryCommand);
                }
                return _selectCategoryCommand;
            }
        }

        private bool SelectCategoryCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _selectCategoryCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void SelectCategoryCommand_Executed()
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(_entityId);
            MerchGroupSelectionWindow dialog =
                new MerchGroupSelectionWindow(hierarchy, null, DataGridSelectionMode.Single);

            // var result = dialog.ShowDialog();
            WindowHelper.ShowWindow(dialog, true);

            if (dialog.DialogResult == true)
            {
                ProductGroup selectedGroup = dialog.SelectionResult.FirstOrDefault();
                if (selectedGroup != null)
                {
                    this.Attributes.CategoryCode = selectedGroup.Code;
                    this.Attributes.CategoryName = selectedGroup.Name;
                }
            }
        }

        #endregion
				
        #region SelectLocationCommand

        private RelayCommand _selectLocationCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SelectLocationCommand
        {
            get
            {
                if (_selectLocationCommand == null)
                {
                    _selectLocationCommand = new RelayCommand(
                        p => SelectLocationCommand_Executed(),
                        p => SelectLocationCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_selectLocationCommand);
                }
                return _selectLocationCommand;
            }
        }

        private bool SelectLocationCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _selectLocationCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void SelectLocationCommand_Executed()
        {
            LocationHierarchyViewModel locationHierarchyViewModel = new LocationHierarchyViewModel();
            locationHierarchyViewModel.FetchForCurrentEntity();
            LocationInfoListViewModel locationInfoListView = new LocationInfoListViewModel();
            locationInfoListView.FetchAllForEntity();

            var locations = new ObservableCollection<LocationInfo>();
            foreach (var locItem in locationInfoListView.BindableCollection)
            {
                locations.Add(locItem);
            }
            var selectedLocations = new ObservableCollection<LocationInfo>();

            Galleria.Ccm.Common.Wpf.Selectors.LocationSelectionViewModel viewModel =
                new Galleria.Ccm.Common.Wpf.Selectors.LocationSelectionViewModel(locations, selectedLocations, locationHierarchyViewModel.Model,
                                                                                    System.Windows.Controls.DataGridSelectionMode.Single);
            CommonHelper.GetWindowService().ShowDialog<LocationSelectionWindow>(viewModel);

            // if a location is selected, set the name and code of that location
            if (viewModel.SelectedAvailableLocations.Count > 0 && viewModel.DialogResult == true)
            {
                this.Attributes.LocationCode = viewModel.SelectedAvailableLocations.First().Location.Code;
                this.Attributes.LocationName = viewModel.SelectedAvailableLocations.First().Location.Name;
            }
        }

        #endregion

        #region SelectClusterSchemeCommand

        private RelayCommand _selectClusterSchemeCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SelectClusterSchemeCommand
        {
            get
            {
                if (_selectClusterSchemeCommand == null)
                {
                    _selectClusterSchemeCommand = new RelayCommand(
                        p => SelectClusterSchemeCommand_Executed(),
                        p => SelectClusterSchemeCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_selectClusterSchemeCommand);
                }
                return _selectClusterSchemeCommand;
            }
        }

        private bool SelectClusterSchemeCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _selectClusterSchemeCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void SelectClusterSchemeCommand_Executed()
        {
            ClusterSchemeSelectionViewModel viewModel = new ClusterSchemeSelectionViewModel();
            CommonHelper.GetWindowService().ShowDialog<ClusterSchemeSelectionWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                // Fetch clusterscheme using the selected clusterSchemeInfo
                ClusterScheme selectedClusterScheme = ClusterScheme.FetchById(viewModel.SelectedClusterSchemeInfo.Id);

                // set the selected clusterScheme name
                this.Attributes.ClusterSchemeName = selectedClusterScheme.Name;
               
                // Try to populate the cluster if the set location code is in the selected ClusterScheme
                if (this.Attributes.LocationCode != String.Empty)
                {
                    foreach (Cluster cluster in selectedClusterScheme.Clusters)
                    {
                        foreach (ClusterLocation clusterLocation in cluster.Locations)
                        {
                            if (this.Attributes.LocationCode == clusterLocation.Code)
                            {
                                this.Attributes.ClusterName = cluster.Name;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region SelectClusterCommand

        private RelayCommand _selectClusterCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SelectClusterCommand
        {
            get
            {
                if (_selectClusterCommand == null)
                {
                    _selectClusterCommand = new RelayCommand(
                        p => SelectClusterCommand_Executed(),
                        p => SelectClusterCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_selectClusterCommand);
                }
                return _selectClusterCommand;
            }
        }

        private bool SelectClusterCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _selectClusterCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void SelectClusterCommand_Executed()
        {
            // Load up the available cluster scheme
            _clusterSchemeInfoListViewModel.FetchForCurrentEntity();

            PlanogramAttributes att = this.Attributes;

            // Check that the cluster Scheme name is not empty
            if (att.ClusterSchemeName != String.Empty)
            {
                // check that the clusterScheme name entered exists
                if (_clusterSchemeInfoListViewModel.Model.Any(s => s.Name == att.ClusterSchemeName))
                {
                    // if name exists then get the clusterScheme with the id
                    ClusterSchemeInfo matchingInfo = _clusterSchemeInfoListViewModel.Model.Where(p => p.Name == att.ClusterSchemeName).FirstOrDefault();
                    if (matchingInfo != null)
                    {
                        ClusterScheme clusterScheme = ClusterScheme.FetchById(matchingInfo.Id);
                        LocationClusterSelectorViewModel viewModel = new LocationClusterSelectorViewModel(clusterScheme, true);
                        CommonHelper.GetWindowService().ShowDialog<LocationClusterSelectionWindow>(viewModel);

                        //set the cluster name
                        if (viewModel.DialogResult == true)
                        {
                            att.ClusterName = viewModel.SelectedCluster.Name;
                        }
                    }
                }
                else
                {
                    // Load up window and display a message that the cluster scheme does not exists
                    LocationClusterSelectorViewModel viewModel = new LocationClusterSelectorViewModel(null, false);
                    CommonHelper.GetWindowService().ShowDialog<LocationClusterSelectionWindow>(viewModel);
                }
            }

        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Plan editor window Ok command
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                    };
                    RegisterCommand(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            //must have at least one property set
            if (!this.Attributes.IsAnyValueSet())
            {
                return false;
            }

            return true;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Plan editor window cancel command
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed(),
                        p => CancelCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private bool CancelCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _cancelCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Event Handlers

        private void Attributes_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //If a property value changes then automatically mark it as set.
            if (!e.PropertyName.EndsWith("IsSet"))
            {
                var isSetProperty = typeof(PlanogramAttributes).GetProperty(e.PropertyName + "IsSet");
                if (isSetProperty != null)
                {
                    isSetProperty.SetValue(this.Attributes, true, null);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the list of planogram attributes to use when updating.
        /// </summary>
        public IEnumerable<PlanogramAttributes> CreatePlanogramAttributes(IEnumerable<Object> planogramIds)
        {
            List<PlanogramAttributes> attributeList = new List<PlanogramAttributes>();

            foreach (Object planId in planogramIds)
            {
                PlanogramAttributes att = _attributes.Copy();
                att.PlanogramId = planId;
                attributeList.Add(att);
            }

            return attributeList;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.Attributes.PropertyChanged -= Attributes_PropertyChanged;
                    _clusterSchemeInfoListViewModel.Dispose();
                    base.DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
