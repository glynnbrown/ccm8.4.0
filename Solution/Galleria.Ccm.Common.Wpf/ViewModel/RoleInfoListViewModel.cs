﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class RoleInfoListViewModel : ViewStateListObject<RoleInfoList, RoleInfo>
    {
        #region Constructor

        public RoleInfoListViewModel() { }

        #endregion

        #region Methods

        public void FetchAll()
        {
            Fetch(new RoleInfoList.FetchAllCriteria());
        }

        #endregion
    }

    public class RoleMemberListViewModel : ViewStateObject<RoleMemberList>
    {
        #region Constructor

        public RoleMemberListViewModel() { }

        #endregion
    }
}
