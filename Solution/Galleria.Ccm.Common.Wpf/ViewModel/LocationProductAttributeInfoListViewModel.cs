﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class LocationProductAttributeInfoListViewModel : ViewStateListObject<LocationProductAttributeInfoList, LocationProductAttributeInfo>
    {
        #region Constructor

        public LocationProductAttributeInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Performs an async search for products based on the given search criteria.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="searchCriteria"></param>
        public void BeginFetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            BeginFetch(new LocationProductAttributeInfoList.FetchByEntityIdSearchCriteriaCriteria(entityId, searchCriteria));
        }

        #endregion
    }
}
