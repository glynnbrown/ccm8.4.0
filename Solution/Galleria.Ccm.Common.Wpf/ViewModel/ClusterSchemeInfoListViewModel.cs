﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class ClusterSchemeInfoListViewModel : ViewStateListObject<ClusterSchemeInfoList, ClusterSchemeInfo>
    {
        #region Constructors
        public ClusterSchemeInfoListViewModel() : base() { }
        #endregion

        #region Methods

        /// <summary>
        /// Populates this view with a list of all infos for the current entity
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new ClusterSchemeInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        #endregion
    }
}
