﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion
#region Version History: (CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property
#endregion
#region Version History: (CCM810)
// V8-29861 : A.Probyn
//  Extended for FetchByWorkpackageId
#endregion

#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    public sealed class EventLogListViewModel : ViewStateObject<EventLogList>
    {
        #region Constructor

        public EventLogListViewModel() { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches infos for all available entities
        /// </summary>
        public void FetchAll()
        {
            Fetch(new EventLogList.FetchAllCriteria());
        }

        /// <summary>
        /// Fetches infos for all available entities
        /// </summary>
        public void FetchByEventLogNameId(Int32 Id)
        {
            Fetch(Id);
        }

        /// <summary>
        /// Fetches infos for all available entities
        /// </summary>
        public void FetchByEventLogNameIdEntryTypeEntityId(Int16 TopRowCount, Nullable<Int32> EventLogNameID, Nullable<Int16> EntryType, Nullable<Int32> EntityID)
        {
            Fetch(new EventLogList.FetchByEventLogNameIdEntryTypeEntityIdCriteria(TopRowCount, EventLogNameID, EntryType, EntityID));

        }

        /// <summary>
        /// Fetches logs for a mixed set of selected criteria
        /// </summary>
        public void FetchByMixedCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source, Nullable<Int32> eventId, Nullable<Int16> entryType,
                Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> datetime, String process, String computerName, String uRLHelperLink,
                String description, String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName, Int32? workpackageId)
        {
            Fetch(new EventLogList.FetchBySelectionOfCriteria(topRowCount, eventLogNameId, source, eventId, entryType, userId,
                entityId, datetime, process, computerName, uRLHelperLink, description, content, gibraltarSessionId, startDate, endDate, workpackageName, workpackageId));

        }

        #region FetchByworkpackageId

        /// <summary>
        /// Fetch the event log records for the specified work package id
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageId(Int32 workpackageId)
        {
            Fetch(new EventLogList.FetchByWorkpackageIdCriteria(workpackageId));
        }

        /// <summary>
        /// Fetch the event log records for the specified work package id
        /// </summary>
        /// <param name="workpackageId"></param>
        public void FetchByWorkpackageIdAsync(Int32 workpackageId)
        {
            var criteria = new EventLogList.FetchByWorkpackageIdCriteria(workpackageId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion

        #endregion
    }
}
