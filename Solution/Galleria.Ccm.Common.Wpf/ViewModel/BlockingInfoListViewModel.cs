﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26123 : L.Ineson
//  Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// ViewModel holder for BlockingInfoList
    /// </summary>
    public sealed class BlockingInfoListViewModel : ViewStateListObject<BlockingInfoList, BlockingInfo>
    {
        #region Constructor

        public BlockingInfoListViewModel()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the list of all infos by the current entity id
        /// and loads the model syncronously
        /// </summary>
        public void FetchForCurrentEntity()
        {
            Fetch(new BlockingInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Fetches all infos for current entity
        /// asynchronously.
        /// </summary>
        public void FetchForCurrentEntityAsync()
        {
            var criteria = new BlockingInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion
    }
}
