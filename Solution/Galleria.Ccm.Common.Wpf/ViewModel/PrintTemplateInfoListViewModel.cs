﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-30738 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Presents of a view of a PrintTemplateInfoList model object
    /// </summary>
    public sealed class PrintTemplateInfoListViewModel : ViewStateListObject<PrintTemplateInfoList, PrintTemplateInfo>
    {
        #region Constructor
        public PrintTemplateInfoListViewModel() { }
        #endregion

        #region Methods

        /// <summary>
        /// Populates the model syncronously with a list
        /// of all infos for the current entity.
        /// </summary>
        public void FetchForCurrentEntity()
        {
            if (CCMClient.ViewState.EntityId == 0) return;

            Fetch(new PrintTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId));
        }

        /// <summary>
        /// Populates the model asyncronously with a list
        /// of all infos for the current entity.
        /// </summary>
        public void FetchForCurrentEntityAsync()
        {
            if (CCMClient.ViewState.EntityId == 0) return;

            var criteria = new PrintTemplateInfoList.FetchByEntityIdCriteria(CCMClient.ViewState.EntityId);
            if (!CCMClient.IsUnitTesting)
            {
                BeginFetch(criteria);
            }
            else
            {
                Fetch(criteria);
            }
        }

        #endregion
    }
}
