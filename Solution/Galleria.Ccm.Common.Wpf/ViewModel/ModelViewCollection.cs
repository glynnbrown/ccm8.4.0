﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-26170 : A.Silva ~ Ported from Editor.Client.Wpf.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Common.Wpf.ViewModel
{
    /// <summary>
    /// Represents a collection containing views based on a model list.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TList"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public class ModelViewCollection<TView, TList, TModel> : BulkObservableCollection<TView>, IDisposable
        where TView : ModelView<TModel>, IDisposable
        where TList : IList<TModel>, IEnumerable<TModel>
    {
        #region Fields

        private TList _modelList;
        private CreateViewDelegate _createViewDel;
        private Boolean _suppressCollectionChangedHandler;

        #endregion

        #region Properties

        public delegate TView CreateViewDelegate(TModel model);

        /// <summary>
        /// Gets/Sets the method used to create a new view.
        /// </summary>
        protected CreateViewDelegate CreateViewMethod
        {
            get { return _createViewDel; }
            set { _createViewDel = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ModelViewCollection()
        {
            _createViewDel = CreateView;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="createViewMethod"></param>
        public ModelViewCollection(CreateViewDelegate createViewMethod)
        {
            _createViewDel = createViewMethod;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever an item changes in this collection
        /// </summary>
        /// <param name="e"></param>
        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            base.OnBulkCollectionChanged(e);

            if (!_suppressCollectionChangedHandler)
            {
                _suppressCollectionChangedHandler = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            if (_modelList != null)
                            {
                                foreach (TView view in e.ChangedItems)
                                {
                                    if (view.Model != null && !_modelList.Contains(view.Model))
                                    {
                                        _modelList.Add(view.Model);
                                    }
                                }
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            foreach (TView view in e.ChangedItems)
                            {
                                if (_modelList != null)
                                {
                                    _modelList.Remove(view.Model);
                                }
                                view.Dispose();
                            }

                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        {
                            //remove old
                            if (e.ChangedItems != null)
                            {
                                foreach (TView view in e.ChangedItems)
                                {
                                    if (_modelList != null)
                                    {
                                        _modelList.Remove(view.Model);
                                    }
                                    view.Dispose();
                                }
                            }

                            //re-add new
                            if (_modelList != null)
                            {
                                foreach (TView view in this)
                                {
                                    if (!_modelList.Contains(view.Model))
                                    {
                                        _modelList.Add(view.Model);
                                    }
                                }
                            }
                        }
                        break;

                }

                _suppressCollectionChangedHandler = false;
            }
            else
            {
                //otherwise just make sure that any old views have been disposed of.

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Remove:
                    case NotifyCollectionChangedAction.Reset:
                        if (e.ChangedItems != null)
                        {
                            foreach (TView view in e.ChangedItems)
                            {
                                view.Dispose();
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Called whenever the source model list collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModelList_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_suppressCollectionChangedHandler)
            {
                _suppressCollectionChangedHandler = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            List<TView> newViews = new List<TView>();
                            foreach (TModel item in e.ChangedItems)
                            {
                                TView view = _createViewDel.Invoke(item);
                                if (view != null)
                                {
                                    newViews.Add(view);
                                }
                            }
                            AddRange(newViews);
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            List<TView> oldViews = new List<TView>();
                            foreach (TModel item in e.ChangedItems)
                            {
                                TView view = FindView(item);
                                if (view != null)
                                {
                                    oldViews.Add(view);
                                }
                            }
                            RemoveRange(oldViews);
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        {
                            if (this.Count > 0) { Clear(); }

                            List<TView> newViews = new List<TView>();
                            foreach (TModel item in _modelList)
                            {
                                TView view = _createViewDel.Invoke(item);
                                if (view != null)
                                {
                                    newViews.Add(view);
                                }
                            }
                            AddRange(newViews);
                        }
                        break;
                }

                _suppressCollectionChangedHandler = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attaches this to the given modellist
        /// </summary>
        /// <param name="modelList"></param>
        public void Attach(TList modelList)
        {
            Dettach();

            if (modelList != null)
            {
                _modelList = modelList;

                INotifyBulkCollectionChanged bulkCollectionModel = _modelList as INotifyBulkCollectionChanged;
                if (bulkCollectionModel != null)
                {
                    bulkCollectionModel.BulkCollectionChanged += ModelList_BulkCollectionChanged;
                }

                this.Clear();
                ModelList_BulkCollectionChanged(_modelList,
                    new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new List<TModel>(_modelList)));

            }
        }

        /// <summary>
        /// Dettaches this view from the current modellist
        /// </summary>
        public void Dettach()
        {
            if (_modelList != null)
            {
                _suppressCollectionChangedHandler = true;

                INotifyBulkCollectionChanged bulkCollectionModel = _modelList as INotifyBulkCollectionChanged;
                if (bulkCollectionModel != null)
                {
                    bulkCollectionModel.BulkCollectionChanged -= ModelList_BulkCollectionChanged;
                }

                Clear();

                _modelList = default(TList);

                _suppressCollectionChangedHandler = false;
            }
        }

        /// <summary>
        /// Creates a new view from the given model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected virtual TView CreateView(TModel model)
        {
            TView view = null;

            ConstructorInfo constuctor = typeof(TView).GetConstructor(new Type[] { typeof(TModel) });
            if (constuctor != null)
            {
                view = (TView)constuctor.Invoke(new Object[] { model });
            }

            return view;
        }

        /// <summary>
        /// Finds the view for the given model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TView FindView(TModel model)
        {
            return this.FirstOrDefault(v => Object.Equals(v.Model, model));
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Dettach();
                }

                _isDisposed = true;
            }
        }

        #endregion
    }
}
