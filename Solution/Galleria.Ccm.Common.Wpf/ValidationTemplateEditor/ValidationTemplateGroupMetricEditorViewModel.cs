﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy and localization.
// V8-26721 : A.Silva ~ Refactored to use Validation Template interfaces.
// V8-26782 : A.Silva ~ Moved over from Editor Client.
// V8-26817 : A.Silva ~ Ok command enabled only if valid and dirty (but not new);
// V8-27196 : A.Silva ~ Amended to use ObjectFieldInfoSelector instead of PlanItemFieldSelector.
// V8-28553 : A.Silva
//      Amended OkCommand_CanExecute() so that the metric will not be saved if the parent would be invalid.

#endregion
#region Version History: (CCM v8.0.2)
// V8-29054 : M.Pettit
//  Extended GetPlanogramModelFieldSelectorGroups() method to include numeric and boolean source fields
#endregion
#region Version History: CCM803
// V8-29596 : L.Luong
//  Amended to now be able to validated Boolean and String fields
#endregion
#region Version History: CCM810
// V8-29739 : M.Brumby
//  Stopped everything from always changing to their default types.
#endregion
#region Version History: (CCM 8.2.0)
// V8-31533 : A.Probyn
//  Added defensive code to field selectors SelectedField value
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    public class ValidationTemplateGroupMetricEditorViewModel : ViewModelAttachedControlObject<ValidationTemplateGroupMetricEditorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;

        private IValidationTemplateGroupMetric _validationTemplateGroupMetric;

        private PlanogramValidationAggregationType _aggregationType;

        private String _title;

        private Boolean _isFieldTypeString = false;
        private Boolean _isFieldTypeBoolean = false;
        private Boolean _isFieldTypeNumeric = false;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ValidationTemplateGroupMetricProperty =
            WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.ValidationTemplateGroupMetric);

        public static readonly PropertyPath TitleProperty =
            WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.Title);

        public static readonly PropertyPath AggregationTypeProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.AggregationType);

        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.CancelCommand);

        public static readonly PropertyPath IsAggregableProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.IsAggregable);
        public static readonly PropertyPath IsFieldTypeStringProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.IsFieldTypeString);
        public static readonly PropertyPath IsFieldTypeNumericProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.IsFieldTypeNumeric);
        public static readonly PropertyPath IsFieldTypeBooleanProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.IsFieldTypeBoolean);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        public IValidationTemplateGroupMetric ValidationTemplateGroupMetric
        {
            get { return _validationTemplateGroupMetric; }
            set
            {
                _validationTemplateGroupMetric = value;
                SetFieldTypeFlags(false);
                OnPropertyChanged(ValidationTemplateGroupMetricProperty);
            }
        }

        public Boolean IsFieldTypeString
        {
            get
            {
                return _isFieldTypeString;
            }
        }

        public Boolean IsFieldTypeBoolean
        {
            get
            {
                return _isFieldTypeBoolean;
            }
        }

        public Boolean IsFieldTypeNumeric
        {
            get
            {
                return _isFieldTypeNumeric;
            }
        }

        public String Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(TitleProperty);
            }
        }

        public PlanogramValidationAggregationType AggregationType
        {
            get { return _aggregationType; }
            set
            {
                _aggregationType = value;
                OnAggregationTypeChange();
                OnPropertyChanged(AggregationTypeProperty);
            }
        }

        private void OnAggregationTypeChange()
        {
            if (ValidationTemplateGroupMetric == null) return;
            ValidationTemplateGroupMetric.AggregationType = _aggregationType;
        }

        public Boolean IsAggregable
        {
            get
            {
                var fields = EnumerateObjectFieldInfos().ToList();
                var info = ObjectFieldInfo.ExtractFieldsFromText(ValidationTemplateGroupMetric.Field,fields).FirstOrDefault();
                return info != null && IsAggregableField(info);
            }
        }

        #endregion

        #region Constructor

        public ValidationTemplateGroupMetricEditorViewModel(ValidationTemplateGroupMetricEditorWindow validationTemplateGroupMetricEditorWindow, String title, IValidationTemplateGroupMetric validationTemplateGroupMetric)
        {
            AttachedControl = validationTemplateGroupMetricEditorWindow;
            Title = title;
            ValidationTemplateGroupMetric = validationTemplateGroupMetric;
            AggregationType = ValidationTemplateGroupMetric.AggregationType;
            SetFieldTypeFlags(false);
        }

        #endregion

        #region Commands

        #region OkCommand

        private IRelayCommand _okCommand;

        /// <summary>
        ///     Accepts the changes and closes the window.
        /// </summary>
        public IRelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => OkCommand_Executed(), o => OkCommand_CanExecute())
                {
                    FriendlyName = Message.Generic_Ok,
                    InputGestureKey = Key.Enter
                };
                ViewModelCommands.Add(_okCommand);

                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            if (!ValidationTemplateGroupMetric.IsValid)
                OkCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            else if (!ValidationTemplateGroupMetric.Parent.IsValid)
                OkCommand.DisabledReason = ValidationTemplateGroupMetric.Parent.BuildErrorsDescription();
            else if (!ValidationTemplateGroupMetric.IsDirty && !ValidationTemplateGroupMetric.IsNew)
                OkCommand.DisabledReason = Message.Generic_ItemIsNotDirty;
            else
                return true;
            return false;
        }

        private void OkCommand_Executed()
        {
            if (!IsFieldTypeNumeric) ValidationTemplateGroupMetric.Score2 = 0;
            DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private IRelayCommand _cancelCommand;

        /// <summary>
        ///     Cancels the change and closes the window.
        /// </summary>
        public IRelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => CancelCommand_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                ViewModelCommands.Add(_cancelCommand);

                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Sets the field type flags based on the current validation template group metric
        /// if the current field type has changed then it will set default values if allowed.
        /// </summary>
        /// <param name="allowTypeReset"></param>
        private void SetFieldTypeFlags(Boolean allowTypeReset = true)
        {
            Boolean originalIsFieldTypeString = _isFieldTypeString;
            Boolean originalIsFieldTypeBoolean = _isFieldTypeBoolean;
            Boolean originalIsFieldTypeNumeric = _isFieldTypeNumeric;

            _isFieldTypeString = false;
            _isFieldTypeBoolean = false;
            _isFieldTypeNumeric = false;
            var fields = EnumerateObjectFieldInfos().ToList();
            var info = ObjectFieldInfo.ExtractFieldsFromText(ValidationTemplateGroupMetric.Field, fields).FirstOrDefault();

            //find out what we are!
            if (info != null)
            {
                if (info.PropertyType == typeof(String))
                {
                    _isFieldTypeString = true;
                }
                else if (info.PropertyType == typeof(Boolean) || info.PropertyType == typeof(Boolean?))
                {
                    _isFieldTypeBoolean = true;
                }
                else
                {
                    _isFieldTypeNumeric = true;
                }
            }

            //If we are allowed (essentially if we are not loading an exisitng ValidationTemplateGroupMetric
            //then we need to see if we have changed field type. If we have then the validation type and 
            //aggregation types need to be changed to defaults so that they are set to something available
            //in that types helper object.
            if (allowTypeReset)
            {
                if (!originalIsFieldTypeString && _isFieldTypeString)
                {
                    ValidationTemplateGroupMetric.ValidationType = PlanogramValidationType.Contains;
                    AggregationType = PlanogramValidationAggregationType.Count;
                }
                if (!originalIsFieldTypeBoolean && _isFieldTypeBoolean)
                {
                    ValidationTemplateGroupMetric.ValidationType = PlanogramValidationType.True;
                    AggregationType = PlanogramValidationAggregationType.Count;
                    ValidationTemplateGroupMetric.Criteria = null;
                }
                if (!originalIsFieldTypeNumeric && _isFieldTypeNumeric)
                {
                    ValidationTemplateGroupMetric.ValidationType = PlanogramValidationType.GreaterThan;
                    AggregationType = PlanogramValidationAggregationType.Avg;
                    ValidationTemplateGroupMetric.Criteria = null;
                }
            }
            OnPropertyChanged(IsAggregableProperty);
            OnPropertyChanged(IsFieldTypeStringProperty);
            OnPropertyChanged(IsFieldTypeNumericProperty);
            OnPropertyChanged(IsFieldTypeBooleanProperty);
        }

        //TODO: Put this into a command.
        public void EditValidationTemplateGroupMetricField()
        {
            String fieldValue = this.ValidationTemplateGroupMetric.Field;

            FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                       FieldSelectorInputType.SingleField, FieldSelectorResolveType.SingleValue,
                       fieldValue, GetPlanogramModelFieldSelectorGroups());
            
            viewModel.ShowDialog(this.AttachedControl);
            if (viewModel.DialogResult != true) return;

            if (viewModel.SelectedField != null)
            {
                this.ValidationTemplateGroupMetric.Field = viewModel.SelectedField.FieldPlaceholder;
            }

            SetFieldTypeFlags();
        }

        /// <summary>
        /// Enumerates through all fields used by validation templates.
        /// </summary>
        private IEnumerable<ObjectFieldInfo> EnumerateObjectFieldInfos()
        {
            foreach(var group in GetPlanogramModelFieldSelectorGroups())
            {
                foreach (var field in group.Fields)
                {
                    yield return field;
                }
            }
        }

        /// <summary>
        /// Returns a list of field selector groups for the planogram structure.
        /// Note that this is a list from the planogram model which
        /// includes meta data fields but does not include ui fields.
        /// </summary>
        /// <returns></returns>
        public static List<FieldSelectorGroup> GetPlanogramModelFieldSelectorGroups()
        {
            //Note that this is currently filtering out the list of available fields
            // to those that are numeric only - this is as required for first release.

            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            var fieldDict = PlanogramFieldHelper.GetPlanogramObjectFieldInfos();

            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramProductGroupName,
                fieldDict[PlanogramItemType.Product].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));
            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramPositionGroupName,
                fieldDict[PlanogramItemType.Position].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));
            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramSubComponentGroupName,
                fieldDict[PlanogramItemType.SubComponent].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));
            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramComponentGroupName,
                fieldDict[PlanogramItemType.Component].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));
            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramFixtureGroupName,
                fieldDict[PlanogramItemType.Fixture].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));
            groups.Add(new FieldSelectorGroup(Message.FieldSelector_PlanogramGroupName,
                fieldDict[PlanogramItemType.Planogram].Where(f => f.IsNumeric || f.PropertyType == typeof(Boolean) || f.PropertyType == typeof(Boolean?) || f.PropertyType == typeof(String))));

            return groups;
        }

        /// <summary>
        ///     Checks wether the <paramref name="info"/> can be aggregated or not in a summary column or metric.
        /// </summary>
        /// <param name="info">The value to have its aggregability checked.</param>
        /// <returns><c>True</c> if the <paramref name="info"/> is aggregable, <c>false</c> otherwise.</returns>
        private static Boolean IsAggregableField(ObjectFieldInfo info)
        {
            return info.OwnerType != typeof(Planogram);

            //var ownerType = info.OwnerType;

            //return ownerType == typeof(PlanogramProduct)
            //    || ownerType == typeof(PlanogramPosition)
            //    || ownerType == typeof(PlanogramSubComponent)
            //    || ownerType == typeof(PlanogramComponent)
            //    || ownerType == typeof(PlanogramFixtureComponent)
            //    || ownerType == typeof(PlanogramFixture)
            //    || ownerType == typeof(PlanogramItem);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                // dispose here of any resources.
            }

            IsDisposed = true;
        }

        #endregion
    }
}