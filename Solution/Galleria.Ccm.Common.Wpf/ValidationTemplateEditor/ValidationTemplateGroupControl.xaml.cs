﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy up and localization.
// V8-26634 : A.Silva ~ Added delete functionality for group metric fields.
// V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.
// V8-26782 : A.Silva ~ Moved over from Editor Client.
// V8-27316 : A.Silva
//      Amended to refresh the metrics count even if the group is null.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    /// <summary>
    ///     Interaction logic for ValidationTemplateGroupControl.xaml
    /// </summary>
    public partial class ValidationTemplateGroupControl
    {
        #region Properties

        #region ValidationTemplateGroup

        public static readonly DependencyProperty ValidationTemplateGroupProperty = DependencyProperty.Register(
            "ValidationTemplateGroup", typeof (IValidationTemplateGroup), typeof (ValidationTemplateGroupControl),
            new PropertyMetadata(null, ValidationTemplateGroup_PropertyChanged));

        public IValidationTemplateGroup ValidationTemplateGroup
        {
            get { return (IValidationTemplateGroup) GetValue(ValidationTemplateGroupProperty); }
            set { SetValue(ValidationTemplateGroupProperty, value); }
        }

        private static void ValidationTemplateGroup_PropertyChanged(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as ValidationTemplateGroupControl;
            if (senderControl == null) return;

            var newValue = e.NewValue as IValidationTemplateGroup;
            senderControl.MetricsCount = newValue == null ? 0 : newValue.Metrics.Count();
        }

        #endregion

        private static readonly DependencyProperty MetricsCountProperty =
            DependencyProperty.Register("MetricsCount", typeof (int), typeof (ValidationTemplateGroupControl),
                new PropertyMetadata(null));

        private int MetricsCount
        {
            get { return (int)GetValue(MetricsCountProperty); }
            set { SetValue(MetricsCountProperty, value); }
        }

        #endregion

        #region Events

        #region Delete

        /// <summary>
        ///     Raised whenever the button is clicked.
        /// </summary>
        private static readonly RoutedEvent FieldDeletingEvent = EventManager.RegisterRoutedEvent("FieldDeleting",
            RoutingStrategy.Bubble, typeof (ValidationTemplateGroupMetricFieldEventHandler),
            typeof (ValidationTemplateGroupControl));

        public event ValidationTemplateGroupMetricFieldEventHandler FieldDeleting
        {
            add { AddHandler(FieldDeletingEvent, value); }
            remove { RemoveHandler(FieldDeletingEvent, value); }
        }

        private void OnDelete(ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            RaiseDeleteEvent(e);
        }

        private void RaiseDeleteEvent(ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            e.RoutedEvent = FieldDeletingEvent;
            e.Source = this;
            RaiseEvent(e);
            MetricsCount = ValidationTemplateGroup != null ? ValidationTemplateGroup.Metrics.Count() : 0;
        }

        #endregion

        #region Edit

        /// <summary>
        ///     Raised whenever the button is clicked.
        /// </summary>
        private static readonly RoutedEvent FieldEditingEvent = EventManager.RegisterRoutedEvent("FieldEditing",
            RoutingStrategy.Bubble, typeof (ValidationTemplateGroupMetricFieldEventHandler),
            typeof (ValidationTemplateGroupControl));

        public event ValidationTemplateGroupMetricFieldEventHandler FieldEditing
        {
            add { AddHandler(FieldEditingEvent, value); }
            remove { RemoveHandler(FieldEditingEvent, value); }
        }

        private void OnClick(ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            RaiseClickEvent(e);
        }

        private void RaiseClickEvent(ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            e.RoutedEvent = FieldEditingEvent;
            e.Source = this;
            RaiseEvent(e);
            MetricsCount = ValidationTemplateGroup != null ? ValidationTemplateGroup.Metrics.Count() : 0;
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateGroupControl()
        {
            InitializeComponent();

            xLayoutRoot.DataContext = this;
        }

        #endregion

        #region Event Handlers

        private void XScoringMetricField_Click(Object sender,
            ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            OnClick(e);
        }

        private void xScoringMetricField_Delete(object sender,
            ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            OnDelete(e);
        }

        #endregion
    }
}