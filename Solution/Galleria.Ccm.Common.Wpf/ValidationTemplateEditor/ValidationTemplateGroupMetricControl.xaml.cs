﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy up and localization.
// V8-26634 : A.Silva ~ Added delete functionality for group metric fields.
// V8-26686 : A.Silva ~ Copied over from Workflow Editor.
// V8-26721 : A.Silva ~ Refactored to use the Validation Template interfaces.
// V8-26782 : A.Silva ~ Moved over from Editor Client.
// V8-27196 : A.Silva ~ Amended GetDisplayText so that it uses ObjectFieldInfo.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    /// <summary>
    ///     Interaction logic for ValidationTemplateGroupMetricControl.xaml
    /// </summary>
    public sealed partial class ValidationTemplateGroupMetricControl
    {
        #region Properties

        #region AllowDelete

        public static readonly DependencyProperty AllowDeleteProperty = DependencyProperty.Register(
            "AllowDelete", typeof(Boolean), typeof(ValidationTemplateGroupMetricControl), new PropertyMetadata(false));

        public Boolean AllowDelete
        {
            get { return (Boolean)GetValue(AllowDeleteProperty); }
            set { SetValue(AllowDeleteProperty, value); }
        }

        #endregion

        #region DeleteButtonVisibility

        public static readonly DependencyProperty DeleteButtonVisibilityProperty = DependencyProperty.Register("DeleteButtonVisibility",
            typeof(Visibility), typeof(ValidationTemplateGroupMetricControl),
            new PropertyMetadata(Visibility.Collapsed));

        private Visibility DeleteButtonVisibility
        {
            get { return (Visibility)GetValue(DeleteButtonVisibilityProperty); }
            set { SetValue(DeleteButtonVisibilityProperty, value); }
        }

        #endregion

        #region DisplayText

        public static readonly DependencyProperty DisplayTextProperty = DependencyProperty.Register("DisplayText",
            typeof(String), typeof(ValidationTemplateGroupMetricControl),
            new PropertyMetadata(null));

        private String DisplayText
        {
            get { return (String)GetValue(DisplayTextProperty); }
            set { SetValue(DisplayTextProperty, value); }
        }

        #endregion

        #region Label

        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register(
            "Label", typeof(String), typeof(ValidationTemplateGroupMetricControl), new PropertyMetadata(null));

        /// <summary>
        ///     Gets or sets the label of the field.
        /// </summary>
        public String Label
        {
            get { return (String)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        #endregion

        #region ValidationTemplateGroupMetric

        public static readonly DependencyProperty ValidationTemplateGroupMetricProperty = DependencyProperty.Register(
            "ValidationTemplateGroupMetric", typeof(IValidationTemplateGroupMetric), typeof(ValidationTemplateGroupMetricControl), new PropertyMetadata(null, ValidationTemplateGroupMetric_PropertyChanged));

        private static void ValidationTemplateGroupMetric_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as ValidationTemplateGroupMetricControl;
            if (senderControl == null) return;

            senderControl.DisplayText = GetDisplayText(senderControl.ValidationTemplateGroupMetric);
            senderControl.DeleteButtonVisibility = senderControl.AllowDelete && senderControl.ValidationTemplateGroupMetric != null
                ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        ///     Gets or sets the value selected in the field.
        /// </summary>
        public IValidationTemplateGroupMetric ValidationTemplateGroupMetric
        {
            get { return (IValidationTemplateGroupMetric)GetValue(ValidationTemplateGroupMetricProperty); }
            set { SetValue(ValidationTemplateGroupMetricProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        #region Deleting

        /// <summary>
        ///     Raised whenever the button is clicked.
        /// </summary>
        public static readonly RoutedEvent DeletingEvent = EventManager.RegisterRoutedEvent("Deleting",
            RoutingStrategy.Bubble, typeof(ValidationTemplateGroupMetricFieldEventHandler), typeof(ValidationTemplateGroupMetricControl));

        public event ValidationTemplateGroupMetricFieldEventHandler Deleting
        {
            add
            {
                AddHandler(DeletingEvent, value);
            }
            remove
            {
                RemoveHandler(DeletingEvent, value);
            }
        }

        private void OnDeleting()
        {
            RaiseDeletingEvent();
        }

        private void RaiseDeletingEvent()
        {
            var newEventArgs = new ValidationTemplateGroupMetricFieldEditingEventHandlerArgs(DeletingEvent)
            {
                Title = Label,
                ValidationTemplateGroupMetric = ValidationTemplateGroupMetric
            };
            RaiseEvent(newEventArgs);
            DisplayText = GetDisplayText(ValidationTemplateGroupMetric);
        }

        #endregion

        #region Editing

        /// <summary>
        ///     Raised whenever the button is clicked.
        /// </summary>
        public static readonly RoutedEvent EditingEvent = EventManager.RegisterRoutedEvent("Editing",
            RoutingStrategy.Bubble, typeof(ValidationTemplateGroupMetricFieldEventHandler), typeof(ValidationTemplateGroupMetricControl));

        public event ValidationTemplateGroupMetricFieldEventHandler Editing
        {
            add { AddHandler(EditingEvent, value); }
            remove { RemoveHandler(EditingEvent, value); }
        }

        private void OnEditing()
        {
            RaiseEditingEvent();
        }

        private void RaiseEditingEvent()
        {
            var newEventArgs = new ValidationTemplateGroupMetricFieldEditingEventHandlerArgs(EditingEvent)
            {
                Title = Label,
                ValidationTemplateGroupMetric = ValidationTemplateGroupMetric
            };
            RaiseEvent(newEventArgs);
            DisplayText = GetDisplayText(ValidationTemplateGroupMetric);
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateGroupMetricControl()
        {
            InitializeComponent();

            xLayoutRoot.DataContext = this;
        }

        #endregion

        #region Event Handlers

        private void XBeginDeleteButton_Click(Object sender, RoutedEventArgs e)
        {
            OnDeleting();
        }

        private void XBeginEditButton_Click(Object sender, RoutedEventArgs e)
        {
            OnEditing();
        }

        #endregion

        #region Static Helper Methods

        private static String GetDisplayText(IValidationTemplateGroupMetric metric)
        {
            if (metric == null) return String.Empty;
            var masterFieldList = PlanogramFieldHelper.GetPlanogramObjectFieldInfos().SelectMany(v=> v.Value);

            var info = ObjectFieldInfo.ExtractFieldsFromText(metric.Field, masterFieldList).FirstOrDefault();
            return info != null ? info.FieldFriendlyName : String.Empty;
        }

        #endregion
    }

    public delegate void ValidationTemplateGroupMetricFieldEventHandler(Object sender, ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e);

    public class ValidationTemplateGroupMetricFieldEditingEventHandlerArgs : RoutedEventArgs
    {
        public ValidationTemplateGroupMetricFieldEditingEventHandlerArgs(RoutedEvent routedEvent)
            : base(routedEvent)
        {
        }

        public IValidationTemplateGroupMetric ValidationTemplateGroupMetric { get; set; }
        public String Title { get; set; }
    }
}