﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26401 : A.Silva ~ Created.
// V8-26721 : A.Silva ~ Refactored to use the Validation Template interface.
// V8-26782 : A.Silva ~ Moved over from Editor Client.

#endregion

#endregion

using System;
using System.Windows;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    /// <summary>
    /// Interaction logic for ValidationTemplateGroupMetricEditorWindow.xaml
    /// </summary>
    public partial class ValidationTemplateGroupMetricEditorWindow
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ValidationTemplateGroupMetricEditorViewModel), typeof(ValidationTemplateGroupMetricEditorWindow),
            new PropertyMetadata(null));

        public ValidationTemplateGroupMetricEditorViewModel ViewModel
        {
            get { return (ValidationTemplateGroupMetricEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateGroupMetricEditorWindow(String title, IValidationTemplateGroupMetric metric)
        {
            InitializeComponent();

            ViewModel = new ValidationTemplateGroupMetricEditorViewModel(this, title, metric);
        }

        #endregion

        #region Event Handlers

        private void XShowScoreMetricSelectorButton_Click(Object sender,
            ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            ViewModel.EditValidationTemplateGroupMetricField();
        }


        #endregion
    }
}
