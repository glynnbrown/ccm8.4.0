﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26401 : A.Silva ~ Created.
// V8-26686 : A.Silva ~ Copied over from Workflow Editor.
// V8-26782 : A.Silva ~ Moved over from Editor Client.
// V8-27909 : A.Silva
//      Amended empty groups allowing to enter scoring data.

#endregion

#endregion

using System;
using System.Windows;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    /// <summary>
    /// Interaction logic for ValidationTemplateScoringControl.xaml
    /// </summary>
    public partial class ValidationTemplateScoringControl
    {
        #region Properties

        #region Source

        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("Source",
            typeof(Object), typeof(ValidationTemplateScoringControl), new PropertyMetadata(null, SourceOnPropertyChanged));

        private static void SourceOnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ValidationTemplateScoringControl senderControl = sender as ValidationTemplateScoringControl;
            if (senderControl == null) return;

            senderControl.IsEnabled = e.NewValue != null;
        }

        public Object Source
        {
            get { return GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        #endregion

        #region HeaderVisibility

        public static readonly DependencyProperty HeaderVisibilityProperty = DependencyProperty.Register(
            "HeaderVisibility", typeof(Visibility), typeof(ValidationTemplateScoringControl), new PropertyMetadata(Visibility.Visible));

        public Visibility HeaderVisibility
        {
            get { return (Visibility)GetValue(HeaderVisibilityProperty); }
            set { SetValue(HeaderVisibilityProperty, value); }
        }

        #endregion

        #region ScoreVisibility

        public static readonly DependencyProperty ScoreVisibilityProperty = DependencyProperty.Register(
            "ScoreVisibility", typeof(Visibility), typeof(ValidationTemplateScoringControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility ScoreVisibility
        {
            get { return (Visibility)GetValue(ScoreVisibilityProperty); }
            set { SetValue(ScoreVisibilityProperty, value); }
        }

        #endregion

        #region FieldTypeStringVisibility

        public static readonly DependencyProperty FieldTypeStringVisibilityProperty = DependencyProperty.Register(
            "FieldTypeStringVisibility", typeof(Visibility), typeof(ValidationTemplateScoringControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility FieldTypeStringVisibility
        {
            get { return (Visibility)GetValue(FieldTypeStringVisibilityProperty); }
            set { SetValue(FieldTypeStringVisibilityProperty, value); }
        }

        #endregion

        #region FieldTypeNumericVisibility

        public static readonly DependencyProperty FieldTypeNumericVisibilityProperty = DependencyProperty.Register(
            "FieldTypeNumericVisibility", typeof(Visibility), typeof(ValidationTemplateScoringControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility FieldTypeNumericVisibility
        {
            get { return (Visibility)GetValue(FieldTypeNumericVisibilityProperty); }
            set { SetValue(FieldTypeNumericVisibilityProperty, value); }
        }

        #endregion

        #region FieldTypeBooleanVisibility

        public static readonly DependencyProperty FieldTypeBooleanVisibilityProperty = DependencyProperty.Register(
            "FieldTypeBooleanVisibility", typeof(Visibility), typeof(ValidationTemplateScoringControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility FieldTypeBooleanVisibility
        {
            get { return (Visibility)GetValue(FieldTypeBooleanVisibilityProperty); }
            set { SetValue(FieldTypeBooleanVisibilityProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateScoringControl()
        {
            InitializeComponent();

            xLayoutRoot.DataContext = this;
        }

        #endregion
    }
}
