﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26686 : A.Silva ~ Created.
// V8-26782 : A.Silva ~ Moved over from Editor Client.
// V8-26817 : A.Silva ~ Updated IValidationTemplateEditorViewModel.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Globalization;

namespace Galleria.Ccm.Common.Wpf.ValidationTemplateEditor
{
    /// <summary>
    ///     Interaction logic for ValidationTemplateEditorControl.xaml
    /// </summary>
    public partial class ValidationTemplateEditorControl
    {
        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (IValidationTemplateEditorViewModel), typeof (ValidationTemplateEditorControl),
            new PropertyMetadata(null, ViewModel_PropertyChanged ));

        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as ValidationTemplateEditorControl;
            if (senderControl == null) return;

            var oldViewModel = e.OldValue as IValidationTemplateEditorViewModel;
            var newViewModel = e.NewValue as IValidationTemplateEditorViewModel;
        }

        /// <summary>
        /// Gets/Sets the viewmodel context controller.
        /// </summary>
        public IValidationTemplateEditorViewModel ViewModel
        {
            get { return (IValidationTemplateEditorViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationTemplateEditorControl"/> class.
        /// </summary>
        public ValidationTemplateEditorControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked whenever the user requests that a metric be edited.
        /// </summary>
        private void XValidationTemplateGroup_MetricEdit(Object sender, ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            var senderControl = sender as ValidationTemplateGroupControl;
            if (senderControl == null) return;

            ViewModel.EditGroupMetric(senderControl.ValidationTemplateGroup, e.ValidationTemplateGroupMetric, e.Title);
        }

        /// <summary>
        ///     Invoked whenever the user requests that a metric is deleted.
        /// </summary>
        private void XValidationTemplateGroup_MetricDelete(Object sender, ValidationTemplateGroupMetricFieldEditingEventHandlerArgs e)
        {
            var senderControl = sender as ValidationTemplateGroupControl;
            if (senderControl == null) return;

            ViewModel.DeleteGroupMetric(senderControl.ValidationTemplateGroup, e.ValidationTemplateGroupMetric);
        }

        #endregion

    }

    /// <summary>
    ///     Describes common functionality expected from any viewmodel which uses a <see cref="ValidationTemplateEditorControl"/>.
    /// </summary>
    public interface IValidationTemplateEditorViewModel
    {
        /// <summary>
        ///     Code invoked when editing a metric inside a group.
        /// </summary>
        /// <param name="validationTemplateGroup">The instance implementing <see cref="IValidationTemplateGroup"/> whose metric is being edited.</param>
        /// <param name="validationTemplateGroupMetric">The instance implementing <see cref="IValidationTemplateGroupMetric"/> which is the one being edited.</param>
        /// <param name="title">The label used to ideintify the metric to the user.</param>
        void EditGroupMetric(IValidationTemplateGroup validationTemplateGroup, IValidationTemplateGroupMetric validationTemplateGroupMetric, string title);

        /// <summary>
        ///     Code invoked when deleting a metric from a group.
        /// </summary>
        /// <param name="validationTemplateGroup">The instance implementing <see cref="IValidationTemplateGroup"/> whose metric is being deleted.</param>
        /// <param name="validationTemplateGroupMetric">The instance implementing <see cref="IValidationTemplateGroupMetric"/> which is the one being deleted.</param>
        void DeleteGroupMetric(IValidationTemplateGroup validationTemplateGroup, IValidationTemplateGroupMetric validationTemplateGroupMetric);
   
        /// <summary>
        ///     The edited version of the instance implementing <see cref="IValidationTemplate"/> that is being edited.
        /// </summary>
        IValidationTemplate ValidationTemplateEdit { get; }
    }

    public static class IValidationTemplateEditorViewModelHelper
    {
        /// <summary>
        /// Preps the validation template ready for editing.
        /// </summary>
        public static void PrepTemplateForEdit(this IValidationTemplate validationTemplate)
        {
            //ensure that the validation template has at least 5 groups
            if (!validationTemplate.Groups.Any())
            {
                //no groups created yet so add all
                for (Int32 i = 1; i <= 5; i++)
                {
                    String newGroupName;

                    newGroupName = String.Format(CultureInfo.CurrentCulture, Message.ValidationTemplate_GroupXName, i);

                    //add the new group
                    validationTemplate.AddNewGroup(newGroupName);
                }
            }
            else
            {
                Int32 groupNo = validationTemplate.Groups.Count();
                while (groupNo < 5)
                {
                    //add the new group
                    validationTemplate.AddNewGroup(String.Format(CultureInfo.CurrentCulture, Message.ValidationTemplate_GroupXName, groupNo));
                    groupNo++;
                }
            }
        }
    }
}