﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)

// V8-26170 : A.Silva ~ Created

#endregion

#region Version History: (CCM 8.1)

// V8-29434 : Added ConverterToPercentage

#endregion

#region Version History: (CCM 8.1.1)
// V8-30455 : D.Pleasance
//  Added ConverterEnumValueToFriendlyName
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Galleria.Ccm.Common.Wpf.Resources
{
    public static class ResourceKeys
    {
        #region Converters
        public static readonly String ConverterAreEqual = "ConverterAreEqual";
        public static readonly String ConverterRadiansToDegrees = "ConverterRadiansToDegrees";
        public static readonly String ConverterEnumTypeToFriendlyNames = "ConverterEnumTypeToFriendlyNames";
        public static readonly String ConverterEnumValueToFriendlyName = "ConverterEnumValueToFriendlyName";
        public static readonly String ConverterToPercentage = "ConverterToPercentage";
        public static readonly String ConverterPlanogramFieldTextToDisplay = "ConverterPlanogramFieldTextToDisplay";
        #endregion

        #region Main
        public static readonly String Main_tmpBulletContent = "Main_tmpBulletContent";
        public static readonly String Main_StyMaintenanceWindowButton = "Main_StyMaintenanceWindowButton";
        public static readonly String Main_StyLightDatagrid = "Main_StyLightDatagrid";

        public static readonly String Main_StyTopRightTabItem = "Main_StyTopRightTabItem";
        public static readonly String Main_StyTopRightTabControl = "Main_StyTopRightTabControl";
        public static readonly String Main_StyTopLeftTabControl = "Main_StyTopLeftTabControl";

        public static readonly String PlanInfoPanel_Expander = "PlanInfoPanel_Expander";
        public static readonly String Main_StySetPropFieldButton = "Main_StySetPropFieldButton";
        public static readonly String Main_StySplitButton = "Main_StySplitButton";
        #endregion
    }

}
