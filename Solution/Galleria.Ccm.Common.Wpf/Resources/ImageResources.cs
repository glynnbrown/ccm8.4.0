﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26170 : A.Silva ~ Created.
// V8-26172 : A.Silva ~ Added resources for ColumnLayoutEditor.
// V8-26318 : A.Silva ~ some new icons added for ColumnLayoutEditor.
// V8-26534 : A.Kuszyk ~ Added Dialog images.
// V8-27528 : M.Shelley ~ Added Desaware product licensing dialog images
// V8-27411 : M.Pettit
//  Added Locked-16.png icon
//  Added LockedByCurrentUser-16.png icon
//  Added SquareTick-16.png, SquareCross-16.png icons
// V8-27912 : M.Pettit
//  Update valdiation icons to match squaregrid status icons
// V8-28986 : M.Shelley
//	Moved images from the Workflow client project
#endregion


#region Version History: (CCM 8.1)
// V8-29742 : L.Ineson
//  Corrected column layout editor resources.
// V8-29590 : A.Probyn
//  Added WorkpackageStatusCompleteWithErrorsWarnings_16
// V8-30171 : A.Probyn
//  Updated ValidationResult_Ok_32, ValidationResult_Warning_32, ValidationResult_Fail_32
#endregion

#region Version History: CCM830

// V8-32041 : A.Silva
//  Added PlanogramComparisonSetup.
// V8-32157 : A.Silva
//  Amended some and removed unnecessary constant image paths for Column Layout Editor.
// V8-32356 : N.Haywood
//  Added add using clipboard
// V8-32245 : N.Haywood
//  Added generic up/down selection
// V8-32652 : A.Silva
//  Cleaned up some unused image names and unified the Add Remove Selected ones.
// V8-32790 : M.Pettit
//  Added Open-48 
// V8-32810 : M.Pettit
//  Added Print template icons
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Galleria.Ccm.Common.Wpf.Resources
{
    public static class ImageResources
    {
        const Boolean IsImageCachingOn = false; //Flag to toggle if images should be cached for later use

        #region Helpers

        private static readonly Dictionary<String, ImageSource> _sourceDict = new Dictionary<String, ImageSource>();

        private static readonly String _iconFolderPath = String.Format(
            CultureInfo.InvariantCulture, "pack://application:,,,/{0};component/Resources/Graphics/{{0}}", Assembly.GetExecutingAssembly().GetName().Name);

        /// <summary>
        /// Helper method to create a frozen bitmap image source
        /// This prevents issue where static sources can memory leak
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private static BitmapImage CreateAsFrozen(String imagePath)
        {
            BitmapImage src = null;

            try
            {
                src = new BitmapImage(new Uri(imagePath));
                src.Freeze();
            }
            catch (Exception)
            {
                Console.WriteLine("Image source not found: " + imagePath);
            }

            return src;
        }

        private static ImageSource GetSource(String key)
        {
            if (IsImageCachingOn)
            {
                ImageSource src;
                if (!_sourceDict.TryGetValue(key, out src))
                {
                    src = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, key));
                    _sourceDict[key] = src;
                }
                return src;
            }
            else
            {
                return CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, key));
            }
        }

        #endregion

        #region Constant Image Paths

        //A
        const String _accept_32 = "Accept-32-Office.png";
        const String _apply32Security = "Apply-32-Security.png";
        const String _apply16Security = "Apply-16-Security.png";
        const String _alignCentre_16 = "AlignCentre-16.png";
        const String _alignLeft_16 = "AlignLeft-16.png";
        const String _alignRight_16 = "AlignRight-16.png";
        const String _add_16 = "Add-16-Database.png";
        private const String _add_32 = "Add-32-Database.png";

        //B
        const String _back24Toolbar = "Back-24-Toolbar.png";

        //C
        const String _ccmAboutScreen = "CCM-AboutScreen.png";
        const String _ccmIcon = "CCMIcon.ico";
        const String _close_16 = "ClosedFolder-16-File.png";
        const String _fontBold_16 = "FontBold-16.png";
        const String _fontItalic_16 = "FontItalic-16.png";
        const String _fontUnderline_16 = "FontUnderline-16.png";
        const String _copy_16 = "Copy-16-Toolbar.png";
        const String _colorLayers_32 = "ColorLayers-32-Design.png"; //Highlight Manager

        //D
        const String _reportGridEdit_32 = "ReportGridEdit-32.png";
        const String _danger_32 = "Danger-32-Office.png";
        const String _downArrow16 = "DownArrow-16.png";
        const String _delete_16 = "Delete-16-Database.png";
        const String _delete_32 = "Delete-32-Database.png";
        const String _dialogTopBar = "DialogTopBar.png";
        const String _dialogSideBar = "DialogSideBar.png";
        const String _database_32 = "Database-32-Database.png";
        const String _downToobar_16 = "Down-Toolbar-16.png";
        const String _downAllToobar_16 = "DownAll-Toolbar-16.png";
        
        //E
        const String _email_48 = "Email-48-Office.png";
        const String _edit_16 = "Edit-16-Computer.png";

        //F
        const String _forward24Multimedia = "Forward-24-Multimedia.png";
        const String _forward24Toolbar = "Forward-24-Toolbar.png";
        const String _favouritesAdd_32 = "FavouritesAdd-32.png";
        const String _favouritesAdd_16 = "FavouritesAdd-16-Toolbar.png";
        const String _favouritesDelete_32 = "FavouritesDelete-32.png";
        const String _favouritesDelete_16 = "FavouritesDelete-16-Toolbar.png";
        const String _filter_32 = "Filter-32.png";
        const String _folderClosed_14 = "FolderClosed-14.png";
        const String _folderOpen_14 = "FolderOpen-14.png";
        const String _folderOpen_48 = "FolderOpen-48.png";
        const String _fixtureLabel_32 = "FixtureLabel-32.png";
        const String _fixtureBack_16 = "FixtureBack-16.png";
        const String _fixtureBottom_16 = "FixtureBottom-16.png";
        const String _fixtureSideLeft_16 = "FixtureSideLeft-16.png";
        const String _fixtureSideRight_16 = "FixtureSideRight-16.png";
        const String _fixtureTop_16 = "FixtureTop-16.png";
        const String _formula_16 = "Formula-16-Office.png";
        
        //G
        const String _grid16Database = "Grid-16-Database.png";
        const String _grid_32 = "Grid-32.png";
        const String _gridGroupByColumns_32 = "GridGroupColumns-32.png";
        const String _gridProperties_32 = "GridProperties-32.png";
        const String _gridShowTotals_32 = "GridShowTotals-32.png";
        const String _gridSort_32 = "GridSort-32.png";

        //H
        const String _hq_48 = "HQ-48-Office.png";

        //I
        const String _info_32 = "Info-32-Toolbar.png";

        //L
        const String _locked_16 = "Locked-16.png";
        const String _lockedByCurrentUser_16 = "LockedByCurrentUser-16.png";
        

        //M
        const String _meeting_32 = "Meeting-32-People.png";

        //N
        const String _newDocument_16 = "NewDocument-16-Toolbar.png";
        const String _newDocument_32 = "NewDocument-32-Toolbar.png";

        //O
        const String _open_16 = "Open-16-Toolbar.png";
        const String _open_32 = "Open-32-Toolbar.png";
        const String _orientation_16 = "Orientation-16.png";
        const String _orientation_32 = "Orientation-32.png";

        //P
        const String _planAssign_PublishSuccessful_16 = "SquareTick-16.png";
        const String _planAssign_PublishFailed_16 = "SquareCross-16.png";
        const String _planAssign_PublishException_16 = "SquareWarning-16.png";
        const String _planogramCompare_16 = "PlanogramCompare-16.png";
        const String _planogramCompare_32 = "PlanogramCompare-32.png";
        const String _printOptions_32 = "PrintOptions-32.png";
        const String _printOptions_AddLine_32 = "PrintOptionsAddLine-32.png";
        const String _printOptions_AddPlanogram_32 = "PrintOptionsAddPlanogram-32.png";
        const String _printOptions_AddSection_32 = "PrintOptionsAddSection-32.png";
        const String _printOptions_AddTextBox_32 = "PrintOptionsAddTextBox-32.png";
        const String _printOptions_GridStyleFull_32 = "PrintOptionsGridStyle-Full-32.png";
        const String _printOptions_GridStyleLight_32 = "PrintOptionsGridStyle-Light-32.png";
        const String _printOptions_GridStyleNone_32 = "PrintOptionsGridStyle-None-32.png";
        const String _printOptions_GridStyleStandard_32 = "PrintOptionsGridStyle-Standard-32.png";
        const String _printOptions_PageSize_32 = "PrintOptionsPageSize-32.png";
        const String _printOptions_RemoveSection_32 = "PrintOptionsRemoveSection-32.png";
        const String _printOptions_SectionGroupAdd_32 = "PrintOptionsSectionGroupAdd-32.png";
        const String _printOptions_SectionGroupRemove_32 = "PrintOptionsSectionGroupRemove-32.png";
        const String _printOptions_RemoveComponent_32 = "PrintOptionsRemoveComponent-32.png";
        const String _printOptions_Header_32 = "PrintOptionsHeader-32.png";
        const String _printOptions_Footer_32 = "PrintOptionsFooter-32.png";
        const String _printOptions_SelectComponent_32 = "PrintOptionsSelectComponent-32.png";
        const String _printOptions_InsertAutoText_32 = "PrintOptions_InsertAutoText-32.png";
        const String _printPreview_32 = "PrintPreview-32.png";
        const String _printTemplate_16 = "PrintTemplate-16.png";
        const String _printTemplate_32 = "PrintTemplate-32.png";
        const String _planMirror_32 = "PlanMirror-32.png";
        const String _pasteDocument_16 = "PasteDocument-16-Toolbar.png";
        const String _printOptionsPlanogramNoColour = "PrintOptionsPlanogramNoColour.png";
        const String _pageOrientationPortrait_32 = "PageOrientation_Portrait.png";
        const String _pageOrientationPortraitLandscape_32 = "PageOrientation_Landscape.png";
        const String _productLabel_32 = "ProductLabel-32.png"; //Product Label Manager
        const String _productFillPatterns_16 = "ProductFillPatterns-16.png";
        const String _productUnits_16 = "ProductUnits-16.png";
        const String _productShapes_16 = "ProductShapes-16.png";
        const String _planogram_16 = "Planogram-16.png";
        const String _planogram_32 = "Planogram-32.png";
        const String _planogramRealImages_16 = "PlanogramRealImages-16.png";
        const String _planogramRealImages_32 = "PlanogramRealImages-32.png";
        const String _planogram3D_16 = "Planogram3D-16.png";
        const String _planogram3D_32 = "Planogram3D-32.png";
        const String _planogramNoColour_16 = "PlanogramNoColour-16.png";
        const String _planogramNoColour_32 = "PlanogramNoColour-32.png";
        const String _productFillColours_16 = "ProductFillColours-16.png";
        const String _printTemplatePlanogramLeftView = "PrintTemplatePlanogramLeftView.png";
        const String _printTemplatePlanogramRightView = "PrintTemplatePlanogramRightView.png";
        const String _printTemplatePlanogram3DView = "PrintTemplatePlanogram3DView.png";

        //R
        const String _rewind24Multimedia = "Rewind-24-Multimedia.png";
        const String _refresh_16 = "Refresh-16-Toolbar.png";
        const String _retailShop_32 = "RetailShop-32-City.png";

        //S
        const String _saveAs_16 = "SaveAs-16-Toolbar.png";
        const String _saveAs_32 = "SaveAs-32-Toolbar.png";
        const String _save_16 = "Save-16-Toolbar.png";
        const String _save_32 = "Save-32-Toolbar.png";
        const String _squareCross_16 = "SquareCross-16.png";
        const String _squareCross_32 = "SquareCross-32.png";
        const String _squareTick_16 = "SquareTick-16.png";
        const String _squareTick_32 = "SquareTick-32.png";
        const String _squareUpdating_16 = "SquareUpdating-16.png";
        const String _squareWarning_16 = "SquareWarning-16.png";
        const String _squareWarning_32 = "SquareWarning-32.png";
        const String _support_32 = "Support-32-Office.png";
        const String _saveAndClose_16 = "SaveAndClose-16.png";
        const String _saveAndNew_16 = "SaveAndNew-16.png";
        const String _selectUsingClipboard_16 = "SelectUsingClipboard-16.png";
        const String _saveToFile16 = "SaveToFile-16.png";
        const String _saveToFile32 = "SaveToFile-32.png";

        //T
        const String _turnOff_16 = "TurnOff-16-Toolbar.png";
        const String _telephone_48 = "Telephone-48-Office.png";
        

        //U
        const String _upArrow16 = "UpArrow-16.png";
        const String _undo_16 = "Undo-16-Design.png";
        const String _up_Toolbar_16 = "Up-Toolbar-16.png";
        const String _upAll_Toolbar_16 = "UpAll-Toolbar-16.png";

        //V
        const String _validationOk_16 = "ValidationOk-16.png";
        const String _validationWarning_16 = "ValidationWarning-16.png";
        const String _validationFail_16 = "ValidationFail-16.png";
        const String _validationOk_32 = "ValidationOk-32.png";
        const String _validationWarning_32 = "ValidationWarning-32.png";
        const String _validationFail_32 = "ValidationFail-32.png";

        //W
        const String _warning_32 = "Warning-32-Toolbar.png";
        const String _world_32_Toolbar = "World-32-Toolbar.png";
        const String _web_48 = "Web-48-Office.png";

        #endregion

        #region General

        #region Icon, Splash and About page backgrounds

        public static ImageSource CCMAboutScreen
        {
            get { return GetSource(_ccmAboutScreen); }
        }

        public static ImageSource CCMIcon
        {
            get { return GetSource(_ccmIcon); }
        }

        #endregion

        #region Model Object Screen icons

        public static ImageSource ProductLabel_32{get{return GetSource(_productLabel_32);}}
        public static ImageSource FixtureLabel_32 { get { return GetSource(_fixtureLabel_32); } }
        public static ImageSource Highlight_32 { get { return GetSource(_colorLayers_32); } }
        public static ImageSource DataSheet_32 { get { return GetSource(_reportGridEdit_32); } }

        #endregion

        #region File Icons

        public static ImageSource New_16
        {
            get { return GetSource(_newDocument_16); }
        }

        public static ImageSource New_32
        {
            get { return GetSource(_newDocument_32); }
        }

        public static ImageSource Open_16
        {
            get { return GetSource(_open_16); }
        }

        public static ImageSource Open_32
        {
            get { return GetSource(_open_32); }
        }

        public static ImageSource SaveAs_16
        {
            get { return GetSource(_saveAs_16); }
        }

        public static ImageSource SaveAs_32
        {
            get { return GetSource(_saveAs_32); }
        }

        public static ImageSource Save_16
        {
            get { return GetSource(_save_16); }
        }

        public static ImageSource Save_32
        {
            get { return GetSource(_save_32); }
        }

        public static ImageSource SaveToFile_16
        {
            get { return GetSource(_saveToFile16); }
        }

        public static ImageSource SaveToFile_32
        {
            get { return GetSource(_saveToFile32); }
        }

        public static ImageSource SaveAndClose_16
        {
            get { return GetSource(_saveAndClose_16); }
        }

        public static ImageSource SaveAndNew_16
        {
            get { return GetSource(_saveAndNew_16); }
        }

        public static ImageSource Add_16
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource Delete_16
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource Delete_32
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource Close_16
        {
            get { return GetSource(_turnOff_16); }
        }


        public static ImageSource BackstageOpen_Repository
        {
            get { return GetSource(_database_32); }
        }

        public static ImageSource BackstageSave_Invalid
        {
            get { return GetSource(_squareCross_16); }
        }
        #endregion

        #region Dialog

        public static ImageSource DialogError { get { return GetSource(_danger_32); } }
        public static ImageSource DialogQuestion { get { return TODO; } }
        public static ImageSource DialogWarning { get { return GetSource(_warning_32); } }

        public static ImageSource Warning_32
        {
            get { return GetSource(_warning_32); }
        }

        #endregion

        public static ImageSource Open_48
        {
            get { return GetSource(_folderOpen_48); }
        }
        
        public static ImageSource NewDocument_16
        {
            get { return GetSource(_newDocument_16); }
        }

        public static ImageSource NewDocument_32
        {
            get { return GetSource(_newDocument_32); }
        }

        public static ImageSource TreeViewItem_ClosedFolder { get { return GetSource(_folderClosed_14); } }
        public static ImageSource TreeViewItem_OpenFolder { get { return GetSource(_folderOpen_14); } }

        //Selectors
        public static ImageSource AddSelectedItems { get { return GetSource(_forward24Toolbar); } }

        public static ImageSource SelectUsingClipboard { get { return GetSource(_selectUsingClipboard_16); } }


        public static ImageSource TODO
        {
            get { return GetSource(_support_32); }
        }

        #endregion

        #region PlanogramViewTypes

        public static ImageSource PlanogramViewTypeDesign16 { get { return GetSource(_planogramNoColour_16); } }
        public static ImageSource PlanogramViewTypeDesign32 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource PlanogramViewTypeFront16 { get { return GetSource(_planogramNoColour_16); } }
        public static ImageSource PlanogramViewTypeFront32 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource PlanogramViewTypeLeft16 { get { return GetSource(_fixtureSideLeft_16); } }
        //public static ImageSource PlanogramViewTypeLeft32 { get { return GetSource(_fixtureSideLeft_16); } }
        public static ImageSource PlanogramViewTypeRight16 { get { return GetSource(_fixtureSideRight_16); } }
        //public static ImageSource PlanogramViewTypeRight32 { get { return GetSource(_fixtureSideRight_16); } }
        public static ImageSource PlanogramViewTypeTop16 { get { return GetSource(_fixtureTop_16); } }
        //public static ImageSource PlanogramViewTypeTop32 { get { return GetSource(_fixtureTop_16); } }
        public static ImageSource PlanogramViewTypeBottom16 { get { return GetSource(_fixtureBottom_16); } }
        //public static ImageSource PlanogramViewTypeBottom32 { get { return GetSource(_fixtureBottom_16); } }
        public static ImageSource PlanogramViewTypeBack16 { get { return GetSource(_fixtureBack_16); } }
        //public static ImageSource PlanogramViewTypeBack32 { get { return GetSource(_fixtureBack_16); } }
        public static ImageSource PlanogramViewType3D16 { get { return GetSource(_planogram3D_16); } }
        public static ImageSource PlanogramViewType3D32 { get { return GetSource(_planogram3D_32); } }

        #endregion
        
        #region FieldSelector

        public static ImageSource FieldSelector_RemoveLastFormulaItem { get { return GetSource(_undo_16); } }
        public static ImageSource FieldSelector_ClearFormula { get { return GetSource(_delete_16); } }
        public static ImageSource Formula_16 { get { return GetSource(_formula_16); } }

        #endregion

        #region PlanogramHierarchySelector

        public static ImageSource PlanogramHierarchySelector_AddToFavourites32 { get { return GetSource(_favouritesAdd_32); } }
        public static ImageSource PlanogramHierarchySelector_AddToFavourites16 { get { return GetSource(_favouritesAdd_16); } }
        public static ImageSource PlanogramHierarchySelector_DeleteFromFavourites32 { get { return GetSource(_favouritesDelete_32); } }
        public static ImageSource PlanogramHierarchySelector_DeleteFromFavourites16 { get { return GetSource(_favouritesDelete_16); } }

        #endregion

        #region Planogram Selector

        public static ImageSource PlanogramSelector_Refresh16 { get { return GetSource(_refresh_16); } }

        #endregion

        #region PlanogramRepository Locking, Status icons

        public static ImageSource Locked_16
        {
            get { return GetSource(_locked_16); }
        }

        public static ImageSource LockedByCurrentUser_16
        {
            get { return GetSource(_lockedByCurrentUser_16); }
        }

        public static ImageSource AutomationStatusComplete_16
        {
            get { return GetSource(_squareTick_16); }
        }

        public static ImageSource AutomationStatusFailed_16
        {
            get { return GetSource(_squareCross_16); }
        }

        public static ImageSource MetaDataStatusProcessing_16
        {
            get { return GetSource(_squareUpdating_16); }
        }

        public static ImageSource ValidationStatusProcessing_16
        {
            get { return GetSource(_squareUpdating_16); }
        }

        public static ImageSource PlanRepositoryRenamePlanogram_16
        {
            get { return GetSource(_edit_16); }
        }

        #endregion

        #region Workpackage Status icons

        public static ImageSource WorkpackageStatusComplete_16
        {
            get { return GetSource(_squareTick_16); }
        }

        public static ImageSource WorkpackageStatusFailed_16
        {
            get { return GetSource(_squareCross_16); }
        }

        public static ImageSource WorkpackageStatusCompleteWithErrorsWarnings_16
        {
            get { return GetSource(_squareWarning_16); }
        }
        #endregion

        #region Location
        public static ImageSource MasterDataManagement_LocationSetup
        {
            get { return GetSource(_retailShop_32); }
        }
        #endregion

        #region Backstage Help

        public static ImageSource BackstageHelp_ContactUs_32 { get { return GetSource(_meeting_32); } }
        public static ImageSource BackstageHelp_Help_32 { get { return GetSource(_support_32); } }
        public static ImageSource BackstageHelp_Email_48 { get { return GetSource(_email_48); } }
        public static ImageSource BackstageHelp_GalleriaAddress_48 { get { return GetSource(_hq_48); } }
        public static ImageSource BackstageHelp_Web_48 { get { return GetSource(_web_48); } }
        public static ImageSource BackstageHelp_Telephone_48 { get { return GetSource(_telephone_48); } }

        #endregion

        #region Validation Template Maintenance

        public static ImageSource ValidationResult_Ok_32 { get { return GetSource(_validationOk_32); } }
        public static ImageSource ValidationResult_Warning_32 { get { return GetSource(_validationWarning_32); } }
        public static ImageSource ValidationResult_Fail_32 { get { return GetSource(_validationFail_32); } }

        //public static ImageSource ValidationResult_Ok_16 { get { return GetSource(_squareTick_16); } }
        //public static ImageSource ValidationResult_Warning_16 { get { return GetSource(_squareWarning_16); } }
        //public static ImageSource ValidationResult_Fail_16 { get { return GetSource(_squareCross_16); } }
        public static ImageSource ValidationResult_Ok_16 { get { return GetSource(_validationOk_16); } }
        public static ImageSource ValidationResult_Warning_16 { get { return GetSource(_validationWarning_16); } }
        public static ImageSource ValidationResult_Fail_16 { get { return GetSource(_validationFail_16); } }

        #endregion

        #region ColumnLayoutEditor

        public static ImageSource ColumnLayoutEditor_EditCommand
        {
            get { return GetSource(_reportGridEdit_32); }
        }

        public static ImageSource ColumnLayoutEditor_MoveColumnDown
        {
            get { return GetSource(_downArrow16); }
        }

        public static ImageSource ColumnLayoutEditor_MoveColumnUp
        {
            get { return GetSource(_upArrow16); }
        }

        public static ImageSource ColumnLayoutEditor_Apply
        {
            get { return GetSource(_apply16Security); }
        }
        
        public static ImageSource ColumnLayoutEditor_Filter_32{get { return GetSource(_filter_32); }}

        #endregion

        #region Dialogs

        public static ImageSource Dialog_Error
        {
            get { return GetSource(_danger_32); }
        }

        public static ImageSource Dialog_Warning
        {
            get { return GetSource(_warning_32); }
        }

        public static ImageSource Dialog_TopBar
        {
            get { return GetSource(_dialogTopBar); }
        }

        public static ImageSource Dialog_SideBar
        {
            get { return GetSource(_dialogSideBar); }
        }

        public static ImageSource Dialog_Information
        {
            get { return GetSource(_info_32); }
        }

        #endregion

        #region Licensing

        public static ImageSource LicenseWindowIcon
        {
            get { return GetSource(_world_32_Toolbar); }
        }

        #endregion

        #region Plan Assignment

        public static ImageSource PlanAssign_PublishSuccessful
        {
            get { return GetSource(_planAssign_PublishSuccessful_16); }
        }

        public static ImageSource PlanAssign_PublishFailed
        {
            get { return GetSource(_planAssign_PublishFailed_16); }
        }

        public static ImageSource PlanAssign_PublishWarning
        {
            get { return GetSource(_planAssign_PublishException_16); }
        }

        #endregion

        #region PrintTemplateSetup

        public static ImageSource PrintTemplateSetup { get { return GetSource(_printTemplate_32); } }
        public static ImageSource PrintTemplateSetup_16 { get { return GetSource(_printTemplate_16); } }
        public static ImageSource PrintTemplateSetup_Orientation { get { return GetSource(_orientation_16); } }
        public static ImageSource PrintTemplateSetup_Orientation_Portrait { get { return GetSource(_pageOrientationPortrait_32); } }
        public static ImageSource PrintTemplateSetup_Orientation_Landscape { get { return GetSource(_pageOrientationPortraitLandscape_32); } }
        public static ImageSource PrintTemplateSetup_PaperSize { get { return GetSource(_printOptions_PageSize_32); } }
        public static ImageSource PrintTemplateSetup_PaperSizeMenuItem { get { return GetSource(_newDocument_32); } }
        public static ImageSource PrintTemplateSetup_AddSection { get { return GetSource(_printOptions_AddSection_32); } }
        public static ImageSource PrintTemplateSetup_RemoveSection { get { return GetSource(_printOptions_RemoveSection_32); } }
        public static ImageSource PrintTemplateSetup_SmallRemoveSection { get { return GetSource(_delete_16); } }
        public static ImageSource PrintTemplateSetup_AddSectionGroup { get { return GetSource(_printOptions_SectionGroupAdd_32); } }
        public static ImageSource PrintTemplateSetup_RemoveSectionGroup { get { return GetSource(_printOptions_SectionGroupRemove_32); } }
        public static ImageSource PrintTemplateSetup_Header { get { return GetSource(_printOptions_Header_32); } }
        public static ImageSource PrintTemplateSetup_Footer { get { return GetSource(_printOptions_Footer_32); } }
        public static ImageSource PrintTemplateSetup_TextBox { get { return GetSource(_printOptions_AddTextBox_32); } }
        public static ImageSource PrintTemplateSetup_Planogram { get { return GetSource(_printOptions_AddPlanogram_32); } }
        public static ImageSource PrintTemplateSetup_DataSheet { get { return GetSource(_reportGridEdit_32); } }
        public static ImageSource PrintTemplateSetup_SelectComponent { get { return GetSource(_printOptions_SelectComponent_32); } }
        public static ImageSource PrintTemplateSetup_RemoveComponent { get { return GetSource(_printOptions_RemoveComponent_32); } }
        public static ImageSource PrintTemplateSetup_SmallRemoveComponent { get { return GetSource(_delete_16); } }
        public static ImageSource PrintTemplateSetup_SmallCopyComponent { get { return GetSource(_copy_16); } }
        public static ImageSource PrintTemplateSetup_SmallPasteComponent { get { return GetSource(_pasteDocument_16); } }
        public static ImageSource PrintTemplateSetup_SmallEditColumnWidths { get { return TODO; } }
        public static ImageSource PrintTemplateSetup_AlignLeft { get { return GetSource(_alignLeft_16); } }
        public static ImageSource PrintTemplateSetup_AlignCentre { get { return GetSource(_alignCentre_16); } }
        public static ImageSource PrintTemplateSetup_AlignRight { get { return GetSource(_alignRight_16); } }
        public static ImageSource PrintTemplateSetup_FontBold { get { return GetSource(_fontBold_16); } }
        public static ImageSource PrintTemplateSetup_FontItalic { get { return GetSource(_fontItalic_16); } }
        public static ImageSource PrintTemplateSetup_FontUnderline { get { return GetSource(_fontUnderline_16); } }
        public static ImageSource PrintTemplateSetup_HeaderFooter_Settings { get { return TODO; } }
        public static ImageSource PrintTemplateSetup_GridStyle_Full { get { return GetSource(_printOptions_GridStyleFull_32); } }
        public static ImageSource PrintTemplateSetup_GridStyle_Light { get { return GetSource(_printOptions_GridStyleLight_32); } }
        public static ImageSource PrintTemplateSetup_GridStyle_None { get { return GetSource(_printOptions_GridStyleNone_32); } }
        public static ImageSource PrintTemplateSetup_GridStyle_Standard { get { return GetSource(_printOptions_GridStyleStandard_32); } }
        public static ImageSource PrintTemplateSetup_TextBox_InsertText { get { return GetSource(_printOptions_InsertAutoText_32); } }
        public static ImageSource PrintTemplateSetup_Line { get { return GetSource(_printOptions_AddLine_32); } }
        public static ImageSource PrintTemplateSetup_PrintPreview { get { return GetSource(_printPreview_32); } }
        public static ImageSource PrintTemplateSetup_MovePreviewUp { get { return GetSource(_upArrow16); } }
        public static ImageSource PrintTemplateSetup_MovePreviewDown { get { return GetSource(_downArrow16); } }
        public static ImageSource PrintTemplateSetup_MoveUp { get { return GetSource(_upArrow16); } }
        public static ImageSource PrintTemplateSetup_MoveDown { get { return GetSource(_downArrow16); } }
        public static ImageSource PrintTemplateSetup_MirrorPlan { get { return GetSource(_planMirror_32); } }
        
        public static ImageSource PrintTemplateSetup_PlanogramComponentFront { get { return GetSource(_printOptionsPlanogramNoColour); } }
        public static ImageSource PrintTemplateSetup_PlanogramComponentLeft { get { return GetSource(_printTemplatePlanogramLeftView); } }
        public static ImageSource PrintTemplateSetup_PlanogramComponentRight { get { return GetSource(_printTemplatePlanogramRightView); } }
        public static ImageSource PrintTemplateSetup_PlanogramComponent3D { get { return GetSource(_printTemplatePlanogram3DView); } }


        public static ImageSource PrintTemplateSetup_Positions_16 { get { return GetSource(_planogram_16); } }
        public static ImageSource PrintTemplateSetup_RealImages_16 { get { return GetSource(_planogramRealImages_16); } }
        public static ImageSource PrintTemplateSetup_ProductUnits_16 { get { return GetSource(_productUnits_16); } }
        public static ImageSource PrintTemplateSetup_ProductShapes_16 { get { return GetSource(_productShapes_16); } }
        public static ImageSource PrintTemplateSetup_ProductFillPatterns_16 { get { return GetSource(_productFillPatterns_16); } }
        public static ImageSource PrintTemplateSetup_ProductFillColours_16 { get { return GetSource(_productFillColours_16); } }

        public static ImageSource PrintTemplateSetup_SetFixtureLabel { get { return FixtureLabel_32; } }
        public static ImageSource PrintTemplateSetup_SetProductLabel { get { return ProductLabel_32; } }
        public static ImageSource PrintTemplateSetup_SetHighlight { get { return Highlight_32; } }
        public static ImageSource PrintTemplateSetup_SetDataSheet { get { return DataSheet_32; } }

        public static ImageSource PrintTemplateSetup_LinkedResourceNotAvailable { get { return GetSource(_validationFail_16); } }

        #endregion

        #region LabelSetupContent
        public static ImageSource LabelSetupContent_AlignLeft { get { return GetSource(_alignLeft_16); } }
        public static ImageSource LabelSetupContent_AlignCentre { get { return GetSource(_alignCentre_16); } }
        public static ImageSource LabelSetupContent_AlignRight { get { return GetSource(_alignRight_16); } }
        public static ImageSource LabelSetupContent_FontBold { get { return GetSource(_fontBold_16); } }
        public static ImageSource LabelSetupContent_FontItalic { get { return GetSource(_fontItalic_16); } }
        public static ImageSource LabelSetupContent_FontUnderline { get { return GetSource(_fontUnderline_16); } }
        #endregion

        #region PlanogramComparisonSetup

        public static ImageSource PlanogramComparisonSetup { get { return GetSource(_planogramCompare_32); } }

        #endregion

        public static ImageSource ItemConflictDialog_OptionIcon { get { return GetSource(_printOptions_AddPlanogram_32); } }

    }
}
