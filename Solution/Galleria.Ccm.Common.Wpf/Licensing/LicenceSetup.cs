﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8 Beta)
//	V8-27528 : M.Shelley ~	Copied licensing code from GFS and abstracted to the Common.WPF 
//							project to allow generic use by multiple clients
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using Desaware.MachineLicense40;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Common.Wpf.Licensing
{
    public class LicenceSetup
    {
        #region Constants

        private const Boolean isDebug = true; 

        #endregion

        #region Fields

        private SecurityViewModel _securityViewModel; // the security view model
        private SecurityDialogViewModel _securityDialogViewModel; // the security view model
        private static LicenseState _licenseState; //holds the state of the license
        private DomainLicense _securityLicense; //the security license methods 
        private SecurityWindow _securityWindow; // the security window if required
        private SecurityDialog _securityDialog; // the security dialog if required
        private DemoDialog _demoDialog; // the Demo dialog if required
        private Boolean _showDialog; //Has the demo dialog already been displayed
        private Boolean _applicationShutdown; // Set to true when the application is being shutdown

        #endregion

        #region Properties

        /// <summary>
        /// The Application LicenseState
        /// </summary>
        public LicenseState LicenseState
        {
            get
            {
                if (_licenseState == null)
                {
                    _licenseState = new LicenseState();
                }
                return _licenseState;
            }
        }

        public Boolean ApplicationShutdown
        {
            get { return _applicationShutdown; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Setup or verify the application licence
        /// </summary>
        /// <param name="runAsync">True to perform the online licence check asynchronously, false for a synchronous check</param>
        public LicenceSetup(Boolean runAsync)
        {
            DebugLog("Started Licence Setup");

            // Setup background worker with event handlers attached
            if (runAsync)
            {
                BackgroundWorker _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(LicensingBackgroundWorker_DoWork);
                _backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(LicensingBackgroundWorker_RunWorkerCompleted);
                _backgroundWorker.RunWorkerAsync();
            }
            else
            {
                LicensingInitialise();
                LicensingUpdate();
            }
        }

        #endregion

        #region Methods

        private void DebugLog(String logString)
        {
            if (isDebug)
            {
                Console.WriteLine(logString);
            }
        }

        /// <summary>
        /// DoWork handler for the background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LicensingBackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            LicensingInitialise();
        }

        private void LicensingInitialise()
        {
            // Create New License
            _securityLicense = new DomainLicense();
            _securityViewModel = new SecurityViewModel(_securityLicense);
            _securityDialogViewModel = new SecurityDialogViewModel(_securityLicense);

            // Initalize the license component
            _securityLicense.LicenseInitalize();

            if (isDebug)
            {
                DebugLog(String.Format("LicensingInitialise : {0} ({1} days left)",
                    _securityLicense.ValidationStatus, _securityLicense.LicenseDaysRemaining()));
            }
        }

        /// <summary>
        /// Event handler for the background work completing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LicensingBackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            LicensingUpdate();
        }

        private void LicensingUpdate()
        {
            if (_securityLicense.DemoVersion())
            {
                // Display the demo results of the License
                DebugLog("LicensingUpdate : Demo licence");
                ShowDemoDialog();
            }
            else
            {
                if (_securityViewModel.SecurityWindowRequired())
                {
                    DebugLog("LicensingUpdate : ShowSecurityWindow");
                    // Application is not licensed Show Registration Window
                    ShowSecurityWindow();
                }
                else
                {
                    if (_securityDialogViewModel.SecurityDialogWindowRequired())
                    {
                        DebugLog("LicensingUpdate : ShowSecurityDialog");
                        // Display The results of the License
                        ShowSecurityDialog();
                    }
                }
            }

            //refresh the license state
            LicenseState.Refresh(_securityLicense);
        }

        /// <summary>
        /// Show the Security window
        /// </summary>
        private void ShowSecurityWindow()
        {
            // hook up to Security completed event and show Security window

            _securityWindow = new SecurityWindow(_securityViewModel);
            _securityWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            var windowOwner = _securityWindow.Owner;
            var windowParent = _securityWindow.Parent;

            _securityViewModel.LoadRegistrationDetails();
            bool? _dialogResult = _securityWindow.ShowDialog();

            DebugLog("ShowSecurityWindow - _dialogResult : " +
                (_dialogResult == null ? "null" : _dialogResult.ToString()));

            if (_dialogResult == true)
            {
                //Close Security window
                CloseSecurityWindow();

                //Display The results of the License
                ShowSecurityDialog();
            }
            else
            {
                DebugLog("ShowSecurityWindow - Licence dialog cancelled - shutdown");

                Application.Current.Shutdown();
                _applicationShutdown = true;
            }
        }

        /// <summary>
        /// Security completed, close window
        /// </summary>
        private void CloseSecurityWindow()
        {
            _securityWindow.Close();
        }

        /// <summary>
        /// Show the Security window
        /// </summary>
        private void ShowSecurityDialog()
        {
            // hook up to Security completed event and show Security window
            _securityDialog = new SecurityDialog(_securityDialogViewModel);
            _securityDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            var windowOwner = _securityDialog.Owner;
            var windowParent = _securityDialog.Parent;

            var mainWindow = Application.Current.MainWindow;

            if (mainWindow != null)
            {
                DebugLog("ShowSecurityDialog - main window type : " + mainWindow.GetType());
            }

            if (mainWindow != null &&
                !mainWindow.GetType().Equals(typeof(SecurityDialog)))
            {
                DebugLog("ShowSecurityDialog - set window Owner");
                _securityDialog.Owner = mainWindow;
            }

            bool? _dialogResult = _securityDialog.ShowDialog();

            DebugLog("ShowSecurityDialog - _dialogResult : " + 
                (_dialogResult == null ? "null" : _dialogResult.ToString()));

            if (_dialogResult == false)
            {
                if (_securityDialogViewModel.IsReturnButtonPressed == true)
                {
                    //Application is not licensed Show Registration Window
                    DebugLog("ShowSecurityDialog - ShowSecurityWindow()");
                    ShowSecurityWindow();
                }
                else
                {
                    //If application is no longer valid
                    if (!_securityDialogViewModel.IsApplicationUseValid)
                    {
                        // ToDo: Uncomment
                        //App.Current.Shutdown();
                        DebugLog("ShowSecurityDialog - IsApplicationUseValid : false");
                        Application.Current.Shutdown();
                        _applicationShutdown = true;
                    }
                    else
                    {
                        //Check if security dialog is showing
                        Window securityDialog = Framework.Controls.Wpf.Helpers.GetWindow<SecurityDialog>();
                        if (securityDialog != null)
                        {
                            //Hide any existing one
                            _securityDialog.Hide();

                            _securityDialog.Owner = Application.Current.MainWindow;

                            _securityDialog.ShowDialog();
                        }
                        else
                        {
                            //Ensure the window is displayed showing anything message
                            ShowSecurityDialog();
                        }
                    }
                    return;
                }
            }
            else
            {
                if (_securityDialogViewModel.IsValidationError == true)
                {
                    DebugLog("ShowSecurityDialog - InstallType.None");

                    //Reset the install type to none to show the validation error
                    _securityLicense.securityInstallType = InstallType.None;

                    //Application has a validation error show dialog
                    ShowSecurityDialog();
                }
                else
                {
                    DebugLog("ShowSecurityDialog - _securityLicense.DemoVersion : " + _securityLicense.DemoVersion());

                    if (_securityLicense.DemoVersion() == true)
                    {
                        ShowDemoDialog();
                    }
                }
            }
        }

        /// <summary>
        /// Show the Demo Dialog window
        /// </summary>
        private void ShowDemoDialog()
        {
            DebugLog("ShowDemoDialog - _showDialog : " + _showDialog);

            if (_showDialog == true)
            {
                //Reset the variable so demo dialog screen is not shown again
                _showDialog = false;

                //If it is a demo version then check it is valid
                if (_securityLicense.IsServerConnection())
                {
                    if (_securityLicense.DemoValid() == false)
                    {
                        DebugLog("ShowDemoDialog - ValidationStatus.DemoExpired");
                        _securityLicense.ValidationStatus = ValidationStatus.DemoExpired;
                    }
                }
                else
                {
                    DebugLog("ShowDemoDialog - No licence server connection");
                }

                if (_securityLicense.ValidationStatus == ValidationStatus.DemoExpired)
                {
                    DebugLog("ShowDemoDialog - ShowSecurityDialog");
                    ShowSecurityDialog();
                }
                else
                {
                    // hook up to Security completed event and show Security window
                    _demoDialog = new DemoDialog(_securityLicense.DemoDaysRemaining());
                    _demoDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    bool? _dialogResult = _demoDialog.ShowDialog();

                    DebugLog("ShowDemoDialog - _dialogResult : " +
                        (_dialogResult == null ? "null" : _dialogResult.ToString()));

                    if (_dialogResult == false)
                    {
                        //Application is demo Show Registration Window
                        ShowSecurityWindow();
                    }
                }
            }
        }

        #endregion
    }
}
