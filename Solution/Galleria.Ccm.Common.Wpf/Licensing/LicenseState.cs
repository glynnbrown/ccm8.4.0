﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8 Beta)
//	V8-27528 : M.Shelley ~	Copied licensing code from GFS and abstracted to the Common.WPF 
//							project to allow generic use by multiple clients
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Security;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.Licensing
{
    /// <summary>
    /// Holds information on the current state of the application license
    /// </summary>
    public class LicenseState : ViewModelObject
    {
        #region Fields

        private Boolean _isLoaded = false;

        private String _isCEIPParticipate = String.Empty;
        private String _licenceStatus = String.Empty;
        private String _demoDaysRemaining = String.Empty;
        private String _activationDaysRemaining = String.Empty;
        private String _licenceGUID = String.Empty;
        private Boolean _isFullLicense = false;
        private Boolean _isInDemoMode = false;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsLoadedProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.IsLoaded);
        public static readonly PropertyPath IsCEIPParticipateProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.IsCEIPParticipate);
        public static readonly PropertyPath LicenceStatusProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.LicenceStatus);
        public static readonly PropertyPath DemoDaysRemainingProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.DemoDaysRemaining);
        public static readonly PropertyPath ActivationDaysRemainingProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.ActivationDaysRemaining);
        public static readonly PropertyPath IsFullLicenseProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.IsFullLicense);
        public static readonly PropertyPath IsInDemoModeProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.IsInDemoMode);
        public static readonly PropertyPath LicenceGUIDProperty = WpfHelper.GetPropertyPath<LicenseState>(p => p.LicenceGUID);

        #endregion

        #region Properties

        /// <summary>
        /// Flag to indicate that the information here
        /// has been retrieved.
        /// </summary>
        public Boolean IsLoaded
        {
            get { return _isLoaded; }
            private set
            {
                _isLoaded = value;
                OnPropertyChanged(IsLoadedProperty);
            }
        }

        /// <summary>
        /// Is the user participating in CEIP?
        /// </summary>
        public String IsCEIPParticipate
        {
            get { return _isCEIPParticipate; }
            private set
            {
                _isCEIPParticipate = value;
                OnPropertyChanged(IsCEIPParticipateProperty);
            }
        }

        /// <summary>
        /// The status of the current licence (licenced, demo mode, not licenced)
        /// </summary>
        public String LicenceStatus
        {
            get { return _licenceStatus; }
            private set
            {
                _licenceStatus = value;
                OnPropertyChanged(LicenceStatusProperty);
            }
        }

        /// <summary>
        /// How many days are left on the demo licence
        /// </summary>
        public String DemoDaysRemaining
        {
            get { return _demoDaysRemaining; }
            private set
            {
                _demoDaysRemaining = value;
                OnPropertyChanged(DemoDaysRemainingProperty);
            }
        }

        /// <summary>
        /// How many days are left on the product main licence
        /// </summary>
        public String ActivationDaysRemaining
        {
            get { return _activationDaysRemaining; }
            private set
            {
                _activationDaysRemaining = value;
                OnPropertyChanged(ActivationDaysRemainingProperty);
            }
        }

        /// <summary>
        /// Does the product have a full activation licence
        /// </summary>
        public Boolean IsFullLicense
        {
            get { return _isFullLicense; }
            private set
            {
                _isFullLicense = value;
                OnPropertyChanged(IsFullLicenseProperty);
            }
        }

        /// <summary>
        /// Does the product have a demo licence
        /// </summary>
        public Boolean IsInDemoMode
        {
            get { return _isInDemoMode; }
            private set
            {
                _isInDemoMode = value;
                OnPropertyChanged(IsInDemoModeProperty);
            }
        }

        /// <summary>
        /// The licence guid
        /// </summary>
        public String LicenceGUID
        {
            get { return _licenceGUID; }
            private set
            {
                _licenceGUID = value;
                OnPropertyChanged(LicenceGUIDProperty);
            }
        }

        #endregion

        #region Constructor

        public LicenseState()
        {
        }

        public LicenseState(DomainLicense securityLicense)
        {
            this.Refresh(securityLicense);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the property values held here
        /// </summary>
        /// <param name="securityLicense"></param>
        public void Refresh(DomainLicense securityLicense)
        {
            //Get the Settings info
            //SettingsList settingsList = SettingsList.Instance;
            //settingsList.LoadSettingsList();

            //Get settings info
            UserSystemSetting settings = UserSystemSetting.FetchUserSettings();

            //Get Licence Status
            this.IsFullLicense = securityLicense.Licensed();
            this.IsInDemoMode = securityLicense.DemoVersion();
            this.LicenceStatus = GetLicenceStatusString();

            //Get days remaining strings
            Int32 numActivationDaysRemaining = securityLicense.LicenseDaysRemaining();
            this.ActivationDaysRemaining = GetDaysRemainingAsString(numActivationDaysRemaining);
            Int32 numDemoDaysRemaining = securityLicense.DemoDaysRemaining();
            this.DemoDaysRemaining = GetDaysRemainingAsString(numDemoDaysRemaining);

            //Get the licence Guid String
            this.LicenceGUID = securityLicense.LicenseInstallationSerial();

            //Get CEIP participation as a string
            this.IsCEIPParticipate = GetIsCEIPParticipateString(settings.SendCEIPInformation);

            //set the loaded flag
            this.IsLoaded = true;
        }

        /// <summary>
        /// Gets the licence status as a string to display to the user
        /// </summary>
        /// <returns></returns>
        private String GetLicenceStatusString()
        {
            if (_isFullLicense)
            {
                return Message.Generic_Licenced;
            }
            if (_isInDemoMode)
            {
                return Message.Generic_InDemoMode;
            }
            return Message.Generic_NotLicenced;
        }

        /// <summary>
        /// Gets the licence status as a string to display to the user
        /// </summary>
        /// <returns></returns>
        private String GetDaysRemainingAsString(Int32 numDays)
        {
            switch (numDays)
            {
                case -1:
                    return Message.Generic_UnlimitedDaysRemaining;
                case 1:
                    return String.Format("{0} {1}", numDays.ToString(), Message.Generic_Day);
                default:
                    return String.Format("{0} {1}", numDays.ToString(), Message.Generic_Days);
            }
        }

        /// <summary>
        /// Gets the CEIP status to display to the user
        /// </summary>
        /// <returns></returns>
        private String GetIsCEIPParticipateString(Boolean isParticipate)
        {
            if (isParticipate)
            {
                return Message.Generic_Participating;
            }
            return Message.Generic_NotParticipating;
        }

        #endregion

        #region IDisposeable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                //nothing to dispose of

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
