﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8 Beta)
//	V8-27528 : M.Shelley ~	Copied licensing code from GFS and abstracted to the Common.WPF 
//							project to allow generic use by multiple clients
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Globalization;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.Licensing
{
    /// <summary>
    /// Interaction logic for DemoDialog.xaml
    /// </summary>
    public partial class DemoDialog : ExtendedRibbonWindow
    {
        public DemoDialog(Int32 demodays)
        {
            InitializeComponent();

            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description1)));
            xMessageDescription.Inlines.Add(new Bold(new Run(demodays.ToString())));
            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description2)));
            xMessageDescription.Inlines.Add(new Bold(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description3))));
            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description4)));
            xMessageDescription.Inlines.Add(new Bold(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description5))));
            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description6)));
            xMessageDescription.Inlines.Add(new Bold(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description7))));
            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description8)));
            xMessageDescription.Inlines.Add(new Bold(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description9))));
            xMessageDescription.Inlines.Add(new Run(String.Format(CultureInfo.CurrentCulture, Message.DemoDialog_Description10)));
        }
   
        #region Event Handlers

        /// <summary>
        /// Handles the ok\continue click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdContinue_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Handles the ok\return click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdReturn_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion
    }
}
