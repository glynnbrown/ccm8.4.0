﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 820
//V8-28861 : L.Ineson
//  Created
#endregion

#region Version History : CCM 830

//V8-31546 : M.Pettit
//  Added PlanogramExportFileTemplate
// V8-31945 : A.Silva
//  Added SessionDirectory.Comparison.

#endregion

#endregion

using System;
using Galleria.Ccm.Model;
using System.Diagnostics;
using System.IO;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Denotes the different Session directory types.
    /// </summary>
    /// <remarks>Remember to add to SessionDirectoryHelper method </remarks>
    public enum SessionDirectory
    {
        Planogram,
        Images,
        Label,
        Highlight,
        DataSheet,
        PrintTemplate,
        CustomColumnLayout,
        FixtureLibrary,
        ValidationTemplate,
        PlanogramFileTemplate,
        ProductUniverse,
        PlanogramExportFileTemplate,
        PlanogramComparisonTemplate,
        ProductLibrary,
        PlanogramExport
    }

    /// <summary>
    /// Helper methods for use with SessionDirectory enum.
    /// </summary>
    public static class SessionDirectoryHelper
    {
        /// <summary>
        /// Gets the current session directory path to use for the given type.
        /// The directory will be created if it does not already exist.
        /// </summary>
        public static String GetOrCreateSettingsDirectory(SessionDirectory type, UserEditorSettings settings)
        {
            //Try to find the directory from the user settings file.
            Debug.Assert(settings != null, "Settings are not loaded");
            if (settings == null) return null;

            String settingsDir = null;
            switch (type)
            {
                case SessionDirectory.Planogram:
                    settingsDir = settings.PlanogramLocation;
                    break;

                case SessionDirectory.Images:
                    settingsDir = settings.ImageLocation;
                    break;

                case SessionDirectory.DataSheet:
                    settingsDir = settings.DataSheetLayoutLocation;
                    break;

                case SessionDirectory.Highlight:
                    settingsDir = settings.HighlightLocation;
                    break;

                case SessionDirectory.Label:
                    settingsDir = settings.LabelLocation;
                    break;

                case SessionDirectory.PrintTemplate:
                    settingsDir = settings.PrintTemplateLocation;
                    break;

                case SessionDirectory.CustomColumnLayout:
                    settingsDir = settings.ColumnLayoutLocation;
                    break;

                case SessionDirectory.FixtureLibrary:
                    settingsDir = settings.FixtureLibraryLocation;
                    break;

                case SessionDirectory.ValidationTemplate:
                    settingsDir = settings.ValidationTemplateLocation;
                    break;

                case SessionDirectory.PlanogramFileTemplate:
                    settingsDir = settings.PlanogramFileTemplateLocation;
                    break;

                case SessionDirectory.PlanogramExportFileTemplate:
                    settingsDir = settings.PlanogramExportFileTemplateLocation;
                    break;

                case SessionDirectory.ProductUniverse:
                    if(settings.DefaultProductLibrarySourceType == DefaultProductLibrarySourceType.ProductLibrary)
                    {
                        settingsDir =
                            (!String.IsNullOrWhiteSpace(settings.DefaultProductLibrarySource)) ?
                            Path.GetDirectoryName(settings.DefaultProductLibrarySource) : null;
                    }
                    break;

                case SessionDirectory.PlanogramComparisonTemplate:
                    settingsDir = settings.PlanogramComparisonTemplateLocation;
                    break;

                case SessionDirectory.PlanogramExport:
                    return Environment.GetFolderPath(Environment.SpecialFolder.Personal);

                default:
                    return null;
            }

            //if no settings dir was supplied just return null
            if (String.IsNullOrWhiteSpace(settingsDir)) return null;

            //if the settings dir already exists return it.
            if (Directory.Exists(settingsDir)) return settingsDir;

            //if not then try to create it
            try
            {
                Directory.CreateDirectory(settingsDir);
            }
            catch (Exception ex)
            {
                //failed so record and return my documents.
                CommonHelper.RecordException(ex);
                return null;
            }

            return settingsDir;
        }

    }

}
