﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion
#region Version History: (CCM 8.3.0)
// CCM-32810 : M.Pettit
//	Added SelectRepositoryLabel
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Contains helper methods for label setup ui.
    /// </summary>
    public static class LabelUIHelper
    {
        /// <summary>
        /// Shows the dialog allowing the user to select a repository label.
        /// </summary>
        /// <returns>the selected highlight info</returns>
        public static LabelInfo SelectRepositoryLabel()
        {
            LabelInfoList labelinfos;
            try
            {
                labelinfos = LabelInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                return null;
            }

            GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            win.ItemSource = labelinfos;

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return null;

            LabelInfo selectedItem = win.SelectedItems.Cast<LabelInfo>().FirstOrDefault();
            if (selectedItem != null) return selectedItem.Clone();
            else return null;
        }
        
        /// <summary>
         /// Shows the open file for loading a label from file.
         /// </summary>
         /// <returns>the selected file name or null</returns>
        public static String ShowOpenFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowOpenFileDialog(
                CCMClient.ViewState.GetSessionDirectory(SessionDirectory.Label),
                String.Format(CultureInfo.InvariantCulture, Message.LabelSetupContent_ImportFilter, Label.FileExtension),
                out fileName);

            //update the session directory
            if (result)
            {
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.Label, Path.GetDirectoryName(fileName));
            }


            return fileName;
        }

        /// <summary>
        /// Shows the SaveFileDialog for saving a label to file.
        /// </summary>
        /// <returns>the saved file name or null</returns>
        public static String ShowSaveAsFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowSaveFileDialog(
            null,
            CCMClient.ViewState.GetSessionDirectory(SessionDirectory.Label),
            String.Format(CultureInfo.InvariantCulture, Message.LabelSetupContent_ImportFilter, Label.FileExtension),
            out fileName);

            //update the session directory
            if (result)
            {
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.Label, Path.GetDirectoryName(fileName));
            }

            return fileName;
        }

        /// <summary>
        /// Fetches a readonly version of the label whereever it comes from
        /// </summary>
        public static Label FetchLabelAsReadonly(Object labelId)
        {
            if (labelId == null) return null;

            Int32 repositoryId;
            if(labelId is Int32)
            {
                return Label.FetchById((Int32)labelId, /*asReadonly*/true);
            }
            else if (Int32.TryParse(labelId as String, out repositoryId))
            {
                return Label.FetchById(repositoryId, /*asReadonly*/true);
            }
            else if (labelId is String)
            {
                return Label.FetchByFilename((String)labelId, /*asReadonly*/true);
            }

            return null;
        }

        /// <summary>
        /// Shows the field selector to set the label context
        /// </summary>
        public static void SetFieldText(Label labelContext)
        {
            SetFieldText(labelContext, null);
        }

        /// <summary>
        /// Shows the field selector to set the label context according to the given planogram.
        /// </summary>
        public static void SetFieldText(Label labelContext, Planogram planogramContext)
        {
            String fieldValue = labelContext.Text;

            List<FieldSelectorGroup> groups;
                 
            if(labelContext.Type == LabelType.Product)
            {
                groups = CommonHelper.GetAllPlanogramFieldSelectorGroups(planogramContext);
            }
            else
            {
                groups = CommonHelper.GetPlanogramFixtureFieldSelectorGroups();
            }


            FieldSelectorViewModel fieldSelectorView =
                new FieldSelectorViewModel(FieldSelectorInputType.Formula,
                    FieldSelectorResolveType.Text, fieldValue,
                    groups);

            CommonHelper.GetWindowService().ShowDialog<FieldSelectorWindow>(fieldSelectorView);

            if (fieldSelectorView.DialogResult == true)
            {
                labelContext.Text = fieldSelectorView.FieldText;
            }
        }
  
    }

        
}
