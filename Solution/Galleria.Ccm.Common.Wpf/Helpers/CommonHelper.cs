﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.00)

// V8-26170 : A.Silva ~ Created.
// V8-26172 : A.Silva ~ Some refactoring to easy the use of the Custom Layout Editor.
// V8-26281 : L.Ineson
//  Changed name to CommonHelper to avoid conficts with client LocalHelper class.
// V8-26408 : L.Ineson
//  Added column helper methods.
// V8-26560 : A.Probyn
//  Added visibility to CreateReadOnlyColumn
// V8-26782 : A.Silva ~ Change reference to namespace Galleria.Ccm.Common.Wpf.Model to access PlanItemType.
// V8-26944 : A.Silva ~ Added extension method ToIcon.
//                    ~ Added CreateReadOnlyColumn for ExtendedTemplateColumns.
// V8-27045 : A.Silva ~ Added ColumnCellAlignment = HorizontalAlignment.Stretch to the data grid template columns.
// V8-27196 : A.Silva ~ Added some ObjectFieldInfo related helper methods.
// V8-27250 : A.Silva ~ Amended DataGridExtendedTemplateColumn CreateReadOnlyColumn() so that it sets the sort member path.
// V8-27912 : M.Pettit
//  Update valdiation icons to match squaregrid status icons
// V8-27900 : L.Ineson
//  Ensured create datagrid column methods set the column ClipboardContentBinding property.
// V8-28522 : D.Pleasance
//  Amended CreateReadOnlyColumn implementations to check if pathPrefix is set and how the path is constructed
#endregion

#region Version History: (CCM v8.10)
// V8-30074 : L.Ineson
//  Added ToLookupDictionary
#endregion

#region Version History: (CCM v8.11)
// V8-29544 : L.Ineson
//  Added support for colour columns.
// V8-30422 : L.Ineson
//  Swapped over date column converter to datetime
#endregion

#region Version History: (CCM v8.20)
// V8-30968 : A.Probyn
//  Updated creation of color columns to remove the color converter from the clipboard binding for export purposes.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Fluent;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Gibraltar.Agent;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    public static class CommonHelper
    {
        #region Nested Classes

        public enum PropertyType
        {
            String,
            Integer,
            Decimal,
            Boolean,
            DateTime,
            Enum
        }

        #endregion

        #region Extensions

        /// <summary>
        /// Sets the isopen state of the backstage on the given ribbon window.
        /// TODO: Make private.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="isBackstageOpen"></param>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void SetRibbonBackstageState(this RibbonWindow sender, bool isBackstageOpen)
        {
            if (sender == null) return;
            if (sender.Content == null) return;

            //get the ribbon belonging to the window
            Ribbon ownerRibbon = (((DependencyObject)sender.Content)).FindVisualDescendent<Ribbon>();
            if (ownerRibbon == null) return;

            //set the backstage is open state
            Backstage ribbonBstg = ownerRibbon.Menu.FindVisualDescendent<Backstage>();
            if (ribbonBstg == null) return;
            ribbonBstg.IsOpen = isBackstageOpen;
        }

        /// <summary>
        ///     Adds the items from a given <see cref="IEnumerable{T}"/> to an <see cref="ObservableCollection{T}"/>.
        /// </summary>
        /// <typeparam name="T">Type of the items in the collection.</typeparam>
        /// <param name="collection">The <see cref="IEnumerable{T}"/> containing the items to be added into an <see cref="ObservableCollection{T}"/></param>
        /// <returns>A new <see cref="ObservableCollection{T}"/> with the items from the original <paramref name="collection"/>.</returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection)
        {
            if (collection == null) return null;

            //create an emtpy observable collection object  
            var observableCollection = new ObservableCollection<T>();

            //loop through all the records and add to observable collection object  
            foreach (var item in collection)
            {
                observableCollection.Add(item);
            }

            //return the populated observable collection  
            return observableCollection;
        }

        #endregion

        #region General

        /// <summary>
        /// Returns the basic property display type for the given type.
        /// </summary>
        public static PropertyType GetBasicPropertyType(Type propertyType)
        {
            if (propertyType == typeof(String))
            {
                return PropertyType.String;
            }
            else if (propertyType.IsEnum)
            {
                return PropertyType.Enum;
            }
            else if (propertyType == typeof(Single) || propertyType == typeof(Single?)
            || propertyType == typeof(Decimal) || propertyType == typeof(Decimal?)
                || propertyType == typeof(Double) || propertyType == typeof(Double?))
            {
                return PropertyType.Decimal;
            }
            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?)
                || propertyType == typeof(Int16) || propertyType == typeof(Int16?)
                || propertyType == typeof(Byte) || propertyType == typeof(Byte?))
            {
                return PropertyType.Integer;
            }
            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
            {
                return PropertyType.Boolean;
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return PropertyType.DateTime;
            }

            return PropertyType.String;
        }

        /// <summary>
        /// Finds the friendly names dictionary for the given enum type.
        /// </summary>
        public static IDictionary GetEnumFriendlyNames(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentOutOfRangeException("enumType", "This is not an enum type");
            }


            IDictionary helperDictionary = null;

            //find the helper
            String enumTypeName = enumType.AssemblyQualifiedName;
            String helperTypeName = enumTypeName.Replace(enumType.Name, enumType.Name + "Helper");

            Type helperType = Type.GetType(helperTypeName);
            if (helperType != null)
            {
                FieldInfo fInfo = helperType.GetField("FriendlyNames");
                if (fInfo != null)
                {
                    helperDictionary = fInfo.GetValue(null) as IDictionary;
                }
            }


            return helperDictionary;
        }

        /// <summary>
        /// Finds the friendly names dictionary for the given enum type.
        /// </summary>
        public static String GetEnumFriendlyName(Object enumValue)
        {
            if (enumValue == null) return null;

            Type enumType = enumValue.GetType();
            if (!enumType.IsEnum) return null;

            IDictionary helperDictionary = GetEnumFriendlyNames(enumType);
            if (!helperDictionary.Contains(enumValue)) return null;

            return helperDictionary[enumValue] as String;
        }

        /// <summary>
        /// Returns the property description for this field.
        /// </summary>
        public static PropertyItemDescription GetPropertyDescription(IModelPropertyInfo propertyInfo,
            DisplayUnitOfMeasureCollection uomValues,
            String pathPrefix = null, String category = null)
        {
            return GetPropertyDescription(propertyInfo, uomValues.GetUomConverter(), uomValues, pathPrefix, category);
        }


        /// <summary>
        /// Returns the property description for this field.
        /// </summary>
        public static PropertyItemDescription GetPropertyDescription(IModelPropertyInfo propertyInfo,
            IValueConverter uomConverter, DisplayUnitOfMeasureCollection uomValues,
            String pathPrefix = null, String category = null)
        {
            PropertyItemDescription propertyDesc = new PropertyItemDescription()
            {
                DisplayName = propertyInfo.FriendlyName,
                PropertyName = (pathPrefix == null) ? propertyInfo.Name : pathPrefix + "." + propertyInfo.Name,
                CategoryName = category,
            };

            //set the converter
            switch (propertyInfo.DisplayType)
            {
                case ModelPropertyDisplayType.None:
                case ModelPropertyDisplayType.Angle:
                    // do nothing
                    break;

                case ModelPropertyDisplayType.Percentage:
                    propertyDesc.Converter =
                        Application.Current.Resources[Galleria.Ccm.Common.Wpf.Resources.ResourceKeys.ConverterToPercentage] as IValueConverter;
                    break;

                case ModelPropertyDisplayType.Length:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Length;
                    break;

                case ModelPropertyDisplayType.Currency:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Currency;
                    break;

                case ModelPropertyDisplayType.Area:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Area;
                    break;

                case ModelPropertyDisplayType.Volume:
                    propertyDesc.Converter = uomConverter;
                    propertyDesc.ConverterParameter = uomValues.Volume;
                    break;
            }


            return propertyDesc;
        }

        /// <summary>
        /// Records that the given exception has occurred
        /// </summary>
        /// <param name="ex"></param>
        public static void RecordException(Exception ex, String exceptionCategory = "Handled")
        {
            Debug.WriteLine(ex.Message);

            //notify gibraltar
            Log.RecordException(ex, exceptionCategory, /*canContinue*/true);

            //[TODO] place the exception message in some sort of local event log?
        }

        public static IWindowService GetWindowService()
        {
            return Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>();
        }

        public static IModalBusyWorkerService GetModalBusyWorkerService()
        {
            return Galleria.Ccm.Services.ServiceContainer.GetService<IModalBusyWorkerService>();
        }

        /// <summary>
        /// Returns the Rexex pattern to use when finding matches for 
        /// input keywords.
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public static String GetRexexKeywordPattern(String searchText)
        {
            String pattern = searchText;
            if (!String.IsNullOrEmpty(pattern))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append('^');

                foreach (String keyword in pattern.Split(' '))
                {
                    if (keyword != "*" && keyword != "+")
                    {
                        String word = keyword.Replace("*", String.Empty);
                        word = word.Replace("+", String.Empty);

                        sb.AppendFormat(CultureInfo.InvariantCulture, "(?=.*?{0})", word);
                    }
                }

                sb.Append(".*$");

                pattern = sb.ToString();
            }
            return pattern;
        }

        #endregion

        #region DataBinding

        /// <summary>
        /// Returns true if there are no binding validation errors
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Boolean AreBindingsValid(DependencyObject obj)
        {
            if (obj != null)
            {
                return !System.Windows.Controls.Validation.GetHasError(obj) &&
                    LogicalTreeHelper.GetChildren(obj).OfType<DependencyObject>().All(child => AreBindingsValid(child));
            }
            else
            {
                //unit testing so just return true
                return true;
            }
        }

        #endregion

        #region Conversions

        #region Colours

        /// <summary>
        /// Converts the given integer to a color value.
        /// </summary>
        public static Color IntToColor(Int32 intValue, Boolean forceAlpha = false)
        {
            Color returnColour = Colors.White;

            Int32 colorAsInt = intValue;

            returnColour = new Color()
            {
                R = Clamp((Byte)((colorAsInt >> 16) & 0xff)),
                G = Clamp((Byte)((colorAsInt >> 8) & 0xff)),
                B = Clamp((Byte)((colorAsInt) & 0xff)),
                A = Clamp((Byte)((colorAsInt >> 24) & 0xff))
            };

            if (forceAlpha)
            {
                returnColour.A = 255;
            }

            return returnColour;
        }

        /// <summary>
        /// Converts the given color to an integer value.
        /// </summary>
        /// <param name="colorVal"></param>
        /// <returns></returns>
        public static Int32 ColorToInt(Color colorVal)
        {
            var uint32Color = (((UInt32)colorVal.A) << 24) |
                    (((UInt32)colorVal.R) << 16) |
                    (((UInt32)colorVal.G) << 8) |
                    (UInt32)colorVal.B;

            return (Int32)uint32Color;
        }

        /// <summary>
        /// Clamps the supplied value bettween 0 and 255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Byte Clamp(Single value)
        {
            Single output = Math.Max(value, 0);
            output = Math.Min(value, 255);
            return (Byte)(output);
        }


        #endregion

        #region Angles

        public static Double ToDegrees(Double radians)
        {
            return RadiansToDegreesConverter.ToDegrees(radians);
        }

        public static Single ToDegrees(Single radians)
        {
            return RadiansToDegreesConverter.ToDegrees(radians);
        }


        public static Double ToRadians(Double degrees)
        {
            return RadiansToDegreesConverter.ToRadians(degrees);
        }

        public static Single ToRadians(Single degrees)
        {
            return RadiansToDegreesConverter.ToRadians(degrees);
        }

        #endregion

        #region Property Type

        public static Type ToNullableType(Type propertyType)
        {
            if (propertyType == typeof(Single))
            {
                return typeof(Single?);
            }
            if (propertyType == typeof(Double))
            {
                return typeof(Double?);
            }
            if (propertyType == typeof(Decimal))
            {
                return typeof(Decimal?);
            }
            if (propertyType == typeof(Int32))
            {
                return typeof(Int32?);
            }
            if (propertyType == typeof(Int16))
            {
                return typeof(Int16?);
            }
            if (propertyType == typeof(Byte))
            {
                return typeof(Byte?);
            }
            if (propertyType == typeof(Byte))
            {
                return typeof(Byte?);
            }
            if (propertyType == typeof(Boolean))
            {
                return typeof(Boolean?);
            }
            if (propertyType == typeof(DateTime))
            {
                return typeof(DateTime?);
            }

            return propertyType;
        }

        #endregion

        #endregion

        #region Images

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        public static Byte[] CreateImageBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }

        /// <summary>
        /// Gets a bitmap image from a memory stream
        /// </summary>
        /// <param name="dataStream">memory stream</param>
        /// <returns>BitmapImage</returns>
        public static BitmapImage GetBitmapImage(MemoryStream dataStream)
        {
            BitmapImage img = new BitmapImage();
            dataStream.Position = 0;
            img.BeginInit();
            img.StreamSource = new MemoryStream(dataStream.ToArray());
            img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            img.CacheOption = BitmapCacheOption.Default;
            img.EndInit();
            img.Freeze();
            return img;
        }

        #endregion

        #region ExtendedDataGrid

        public static DataGridColumn CreateReadOnlyColumn(ObjectFieldInfo fieldInfo, DisplayUnitOfMeasureCollection uomValues,
            String columnGroup = null, Boolean isVisible = true, String pathPrefix = null)
        {
            String path = (pathPrefix == null) ? fieldInfo.PropertyName : pathPrefix + "." + fieldInfo.PropertyName;

            return CreateReadOnlyColumn(path, fieldInfo.PropertyType, fieldInfo.PropertyFriendlyName,
                fieldInfo.PropertyDisplayType, uomValues, columnGroup, isVisible);
        }

        public static DataGridColumn CreateReadOnlyColumn(
           IModelPropertyInfo propertyInfo, DisplayUnitOfMeasureCollection uomValues,
            String columnGroup = null, Boolean isVisible = true, String pathPrefix = null)
        {
            String path = (pathPrefix == null) ? propertyInfo.Name : pathPrefix + "." + propertyInfo.Name;

            return CreateReadOnlyColumn(path, propertyInfo.Type, propertyInfo.FriendlyName,
                propertyInfo.DisplayType, uomValues, columnGroup, isVisible);
        }

        public static DataGridColumn CreateReadOnlyColumn(
            String path, Type propertyType, String header, ModelPropertyDisplayType propertyDisplayType,
            DisplayUnitOfMeasureCollection uomValues, String columnGroup = null, Boolean isVisible = true)
        {
            PropertyType pType = GetBasicPropertyType(propertyType);

            DataGridColumn col = null;

            Binding colBinding = new Binding(path)
            {
                Mode = BindingMode.OneWay,
            };

            //set the converter for the binding.
            if (uomValues != null)
            {
                uomValues.ApplyConverter(colBinding, propertyDisplayType, pType);
            }


            switch (pType)
            {
                #region String
                default:
                case PropertyType.String:
                    {
                        col = new DataGridExtendedTextColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            ClipboardContentBinding = colBinding,
                            IsReadOnly = true,
                            ColumnGroupName = columnGroup
                        };
                    }
                    break;
                #endregion

                #region Integer/Colour
                case PropertyType.Integer:
                    {
                        if (propertyDisplayType == ModelPropertyDisplayType.Colour)
                        {
                            //With no converter
                            Binding clipboardBinding = new Binding(path);
                            clipboardBinding.Mode = colBinding.Mode;
                            clipboardBinding.UpdateSourceTrigger = colBinding.UpdateSourceTrigger;
                            clipboardBinding.ValidatesOnDataErrors = colBinding.ValidatesOnDataErrors;

                            col = new DataGridColorPickerBoxColumn()
                            {
                                Header = header,
                                Binding = colBinding,
                                ClipboardContentBinding = clipboardBinding,
                                IsReadOnly = true,
                                ColumnGroupName = columnGroup,
                                CanUserFilter = false
                            };
                        }
                        else
                        {
                            col = new DataGridExtendedNumericColumn()
                            {
                                Header = header,
                                IsReadOnly = true,
                                ColumnGroupName = columnGroup,
                                ColumnCellAlignment = HorizontalAlignment.Right,
                                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer,
                                Binding = colBinding,
                            };
                        }
                    }
                    break;
                #endregion

                #region Decimal
                case PropertyType.Decimal:
                    {
                        colBinding.StringFormat = "{0:N2}";

                        col = new DataGridExtendedNumericColumn()
                        {
                            Header = header,
                            IsReadOnly = true,
                            ColumnGroupName = columnGroup,
                            ColumnCellAlignment = HorizontalAlignment.Right,
                            Binding = colBinding,
                        };

                        if (propertyDisplayType == ModelPropertyDisplayType.Angle)
                        {
                            col.ClipboardContentBinding = colBinding;
                        }
                    }
                    break;
                #endregion

                #region Enum
                case PropertyType.Enum:
                    {
                        //get the enum helper
                        var enumDictionary = GetEnumFriendlyNames(propertyType);

                        colBinding.Converter = (Application.Current != null) ?
                            Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue] as IValueConverter
                            : new Galleria.Framework.Controls.Wpf.Converters.DictionaryKeyToValueConverter();

                        colBinding.ConverterParameter = enumDictionary;

                        col = new DataGridExtendedTextColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            ClipboardContentBinding = colBinding,
                            IsReadOnly = true,
                            ColumnGroupName = columnGroup,
                            ColumnCellAlignment = HorizontalAlignment.Left,
                        };
                    }
                    break;
                #endregion

                #region Boolean
                case PropertyType.Boolean:
                    {
                        col = new DataGridExtendedCheckBoxColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            IsReadOnly = true,
                            ColumnGroupName = columnGroup,
                        };
                    }
                    break;
                #endregion

                #region DateTime
                case PropertyType.DateTime:
                    {
                        //just use a normal text column.
                        col = new DataGridExtendedTextColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            ClipboardContentBinding = colBinding,
                            IsReadOnly = true,
                            ColumnGroupName = columnGroup
                        };

                        //col = new DataGridDateTimeColumn()
                        //{
                        //    Header = header,
                        //    Binding = colBinding,
                        //    SelectedDateFormat = DatePickerFormat.Long,
                        //    ColumnGroupName = columnGroup,
                        //    ClipboardContentBinding = colBinding
                        //};
                    };
                    break;
                #endregion
            }



            col.Visibility = (isVisible) ? Visibility.Visible : Visibility.Hidden;

            return col;
        }

        public static DataGridExtendedTemplateColumn CreateReadOnlyColumn(DataTemplate dataTemplate, String sortMemberPath, String header, String columnGroup)
        {
            var column = new DataGridExtendedTemplateColumn
            {
                CellTemplate = dataTemplate,
                SortMemberPath = sortMemberPath,
                Header = header,
                ColumnGroupName = columnGroup,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            return column;
        }

        public static DataGridColumn CreateEditableColumn(
            ObjectFieldInfo fieldInfo, DisplayUnitOfMeasureCollection uomValues,
            String category = null, Boolean isVisible = true, String pathPrefix = null)
        {
            String path = (pathPrefix == null) ? fieldInfo.PropertyName : pathPrefix + "." + fieldInfo.PropertyName;

            return CreateEditableColumn(path, fieldInfo.PropertyType, fieldInfo.PropertyFriendlyName,
                fieldInfo.PropertyDisplayType, uomValues, category, isVisible);
        }


        public static DataGridColumn CreateEditableColumn(
            IModelPropertyInfo propertyInfo, DisplayUnitOfMeasureCollection uomValues,
            String category = null, Boolean isVisible = true, String pathPrefix = null)
        {
            String path = (pathPrefix == null) ? propertyInfo.Name : pathPrefix + "." + propertyInfo.Name;

            return CreateEditableColumn(path, propertyInfo.Type, propertyInfo.FriendlyName,
                propertyInfo.DisplayType, uomValues, category, isVisible);
        }


        public static DataGridColumn CreateEditableColumn(
            String path, Type propertyType, String header, ModelPropertyDisplayType propertyDisplayType,
            DisplayUnitOfMeasureCollection uomValues,
            String category = null, Boolean isVisible = true)
        {
            PropertyType pType = GetBasicPropertyType(propertyType);

            DataGridColumn col = null;

            Binding colBinding = new Binding(path)
            {
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                ValidatesOnDataErrors = true,
            };

            //set the converter
            uomValues.ApplyConverter(colBinding, propertyDisplayType, pType);


            switch (pType)
            {
                #region String
                default:
                case PropertyType.String:
                    {
                        col = new DataGridExtendedTextColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            ClipboardContentBinding = colBinding,
                            ColumnGroupName = category,
                        };
                    }
                    break;
                #endregion

                #region Integer/Colour
                case PropertyType.Integer:
                    {
                        if (propertyDisplayType == ModelPropertyDisplayType.Colour)
                        {
                            //With no converter
                            Binding clipboardBinding = new Binding(path);
                            clipboardBinding.Mode = colBinding.Mode;
                            clipboardBinding.UpdateSourceTrigger = colBinding.UpdateSourceTrigger;
                            clipboardBinding.ValidatesOnDataErrors = colBinding.ValidatesOnDataErrors;

                            col = new DataGridColorPickerBoxColumn()
                            {
                                Header = header,
                                Binding = colBinding,
                                ClipboardContentBinding = clipboardBinding,
                                ColumnGroupName = category,
                                CanUserFilter = false,
                            };
                        }
                        else
                        {
                            col = new DataGridExtendedNumericColumn()
                            {
                                Header = header,
                                ColumnGroupName = category,
                                ColumnCellAlignment = HorizontalAlignment.Right,
                                Binding = colBinding,
                                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer,
                                AllowsNullValue = false,
                            };
                        }
                    }
                    break;
                #endregion

                #region Decimal
                case PropertyType.Decimal:
                    {
                        col = new DataGridExtendedNumericColumn()
                        {
                            Header = header,
                            ColumnGroupName = category,
                            ColumnCellAlignment = HorizontalAlignment.Right,
                            Binding = colBinding,
                            TextInputType = Galleria.Framework.Controls.Wpf.InputType.Decimal,
                            AllowsNullValue = false,
                        };

                        if (propertyDisplayType == ModelPropertyDisplayType.Angle)
                        {
                            col.ClipboardContentBinding = colBinding;
                        }
                    }
                    break;
                #endregion

                #region Enum
                case PropertyType.Enum:
                    {
                        //get the enum helper
                        var enumDictionary = GetEnumFriendlyNames(propertyType);

                        col = new DataGridExtendedComboBoxColumn()
                        {
                            Header = header,
                            ItemsSource = enumDictionary,
                            SelectedValueBinding = colBinding,
                            SelectedValuePath = "Key",
                            DisplayMemberPath = "Value",
                            ColumnGroupName = category,
                            ClipboardContentBinding =
                                new Binding(colBinding.Path.Path)
                                {
                                    Converter = Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue] as IValueConverter,
                                    ConverterParameter = enumDictionary
                                }
                        };
                    }
                    break;
                #endregion

                #region Boolean
                case PropertyType.Boolean:
                    {
                        col = new DataGridExtendedCheckBoxColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            ColumnGroupName = category,
                        };
                    }
                    break;
                #endregion

                #region DateTime
                case PropertyType.DateTime:
                    {
                        col = new DataGridDateTimeColumn()
                        {
                            Header = header,
                            Binding = colBinding,
                            SelectedDateFormat = DatePickerFormat.Short,
                            ColumnGroupName = category,
                            ClipboardContentBinding = colBinding
                        };
                    };
                    break;
                #endregion
            }


            col.Visibility = (isVisible) ? Visibility.Visible : Visibility.Hidden;


            return col;
        }

        #endregion

        #region Planogram

        /// <summary>
        /// Returns a list of all field selector groups for a planogram.
        /// </summary>
        /// <param name="planogram">the context to use for performance fields.</param>
        /// <returns></returns>
        public static List<FieldSelectorGroup> GetAllPlanogramFieldSelectorGroups(Planogram planogram = null)
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            groups.Add(new FieldSelectorGroup(PlanogramProduct.FriendlyName,
                PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMeta*/true, /*incCustom*/true, /*incPerformance*/true,
                ((planogram != null) ? planogram.Performance : null))));

            groups.Add(new FieldSelectorGroup(PlanogramPosition.FriendlyName, PlanogramPosition.EnumerateDisplayableFieldInfos()));

            groups.Add(new FieldSelectorGroup(PlanogramSubComponent.FriendlyName, PlanogramSubComponent.EnumerateDisplayableFieldInfos()));

            groups.Add(new FieldSelectorGroup(PlanogramComponent.FriendlyName,
                PlanogramComponent.EnumerateDisplayableFieldInfos(/*incMeta*/true)
                .Union(PlanogramFixtureComponent.EnumerateDisplayableFieldInfos())));

            groups.Add(new FieldSelectorGroup(PlanogramFixture.FriendlyName,
                PlanogramFixture.EnumerateDisplayableFieldInfos(/*incMetadata*/true)
                .Union(PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*incMetadata*/true))));

            groups.Add(new FieldSelectorGroup(Planogram.FriendlyName,
               Planogram.EnumerateDisplayableFieldInfos(/*incMetadata*/true)));

            return groups;
        }

        /// <summary>
        /// Returns a list of fixture field selector groups for a planogram.
        /// </summary>
        public static List<FieldSelectorGroup> GetPlanogramFixtureFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            groups.Add(new FieldSelectorGroup(PlanogramSubComponent.FriendlyName, PlanogramSubComponent.EnumerateDisplayableFieldInfos()));

            groups.Add(new FieldSelectorGroup(PlanogramComponent.FriendlyName,
                PlanogramComponent.EnumerateDisplayableFieldInfos(/*incMeta*/true)
                .Union(PlanogramFixtureComponent.EnumerateDisplayableFieldInfos())));

            groups.Add(new FieldSelectorGroup(PlanogramFixture.FriendlyName,
                PlanogramFixture.EnumerateDisplayableFieldInfos(/*incMetadata*/true)
                .Union(PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*incMetadata*/true))));

            groups.Add(new FieldSelectorGroup(Planogram.FriendlyName,
               Planogram.EnumerateDisplayableFieldInfos(/*incMetadata*/true)));

            return groups;
        }

        #endregion
    }

}


