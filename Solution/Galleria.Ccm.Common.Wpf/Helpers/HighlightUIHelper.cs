﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion
#region Version History: (CCM 8.3.0)
// CCM-32810 : M.Pettit
//	Added SelectRepositoryHighlight
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Contains helper methods for highlight ui screens.
    /// </summary>
    public static class HighlightUIHelper
    {
        /// <summary>
        /// Shows the dialog allowing the user to select a repository highlight.
        /// </summary>
        /// <returns>the selected highlight info</returns>
        public static HighlightInfo SelectRepositoryHighlight()
        {
            HighlightInfoList highlightInfos;
            try
            {
                highlightInfos = HighlightInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                return null;
            }

            GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            win.ItemSource = highlightInfos;

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return null;

            HighlightInfo selectedItem = win.SelectedItems.Cast<HighlightInfo>().FirstOrDefault();
            if (selectedItem != null) return selectedItem.Clone();
            else return null;
        }
        
        /// <summary>
         /// Shows the open file for loading a highlight from file.
         /// </summary>
         /// <returns>the selected file name or null</returns>
        public static String ShowOpenFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowOpenFileDialog(
                CCMClient.ViewState.GetSessionDirectory(SessionDirectory.Highlight),
                String.Format(CultureInfo.InvariantCulture, Message.HighlightSetupContent_ImportFilter, Highlight.FileExtension),
                out fileName);

            if (result)
            {
                //update the session directory
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.Highlight, Path.GetDirectoryName(fileName));
            }


            return fileName;
        }

        /// <summary>
        /// Shows the SaveFileDialog for saving a highlight to file.
        /// </summary>
        public static String ShowSaveAsFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowSaveFileDialog(
            null,
            CCMClient.ViewState.GetSessionDirectory(SessionDirectory.Highlight),
            String.Format(CultureInfo.InvariantCulture, Message.HighlightSetupContent_ImportFilter, Highlight.FileExtension),
            out fileName);

            if (result)
            {
                //update the session directory
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.Highlight, Path.GetDirectoryName(fileName));
            }

            return fileName;
        }

        /// <summary>
        /// Fetches a readonly version of the highlight whereever it comes from
        /// </summary>
        public static Highlight FetchHighlightAsReadonly(Object highlightId)
        {
            if (highlightId == null) return null;

            Int32 repositoryId;
            if (highlightId is Int32)
            {
                return Highlight.FetchById((Int32)highlightId, /*asReadonly*/true);
            }
            else if (Int32.TryParse(highlightId as String, out repositoryId))
            {
                return Highlight.FetchById(repositoryId, /*asReadonly*/true);
            }
            else if (highlightId is String)
            {
                return Highlight.FetchByFilename((String)highlightId, /*asReadonly*/true);
            }

            return null;
        }


        /// <summary>
        /// Processes the settings groups for the given setting.
        /// </summary>
        public static void CreateSettingGroups(Highlight setting, Planogram plan, Boolean clearExisting)
        {
            //only process if we have a setting and an active plan.
            if (setting != null && plan != null)
            {
                if (clearExisting)
                {
                    setting.Groups.Clear();
                }


                //TODO: Derive what items we are highlighting from the setting.
                // For now however we shall just do positions.
                List<PlanogramPosition> processItems = plan.Positions.ToList();

                List<PlanogramHighlightResultGroup> expectedGroups = PlanogramHighlightHelper.DetermineHighlightGroups(processItems, setting);

                //Create group
                List<HighlightGroup> oldGroups = setting.Groups.ToList();

                Int32 orderNumber = 1;
                Int32 unspecifiedLabelCount = 0;

                foreach (PlanogramHighlightResultGroup entry in expectedGroups)
                {
                    HighlightGroup group = HighlightGroup.NewHighlightGroup();

                    //Ensure group labels are unique. First is called "Unspecified", subsequent are "Unspecified (1)" etc
                    if (String.IsNullOrEmpty(entry.Label.Trim()))
                    {
                        String unspecifiedLabel = Message.HighlightSetupContent_UnspecifiedGroupLabel;
                        if (unspecifiedLabelCount > 0)
                        {
                            unspecifiedLabel = String.Format("{0} ({1})", unspecifiedLabel, unspecifiedLabelCount);
                        }
                        group.Name = unspecifiedLabel;
                        unspecifiedLabelCount++;
                    }
                    else
                    {
                        group.Name = entry.Label;
                    }
                    group.FillColour = entry.Colour;
                    setting.Groups.Add(group);

                    group.Order = orderNumber;
                    orderNumber++;
                }

                setting.Groups.RemoveList(oldGroups);
            }
        }

    }

   
}
