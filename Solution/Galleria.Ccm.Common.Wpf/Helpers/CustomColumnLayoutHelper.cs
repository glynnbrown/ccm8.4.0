﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-24124 : A.Silva ~ Enhanced how binding was created for default columns.
// V8-26170 : A.Silva ~ Ported from Editor.Client.
// V8-26351 : A.Silva ~ Refactored some code to make it more standard.
// V8-26370 : A.Silva ~ No longer setting ColumnCellBackgroundBrush for DataGridColumns to test impact on performance. It generates an error when rendering.
// V8-26369 : A.Silva ~ Added the mappings to DateTime and ComboBox (for enums) plus support for the friendly names collections.
// V8-26597 : A.Silva ~ Corrected the loading of the path from the current grid, to avoid storing extra information.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27022 : A.Silva 
//  Changed slightly the implementation of ToDataGridColumnOf<T>, so that it uses the custom column path IF there is one, if not, it will use the property name.
// V8-26944 : A.Silva ~ Added overload of ToDataGridColumn to allow creating custom template columns.
// V8-27250 : A.Silva
//      Amended ToDataGridColumn() so that it sets Sort Member Path for Template columns.
// V8-27504 : A.Silva
//      Amended new signature for CustomColumn.NewCustomColumn.
// V8-27898 : L.Ineson
//  Amended to pass through field infos instead of just property names. Reduced amount of duplicated code.
// V8-28040 : A.Probyn
//  Added defensive code to Load method when trying to apply filters.
// V8-28373 : A.Silva
//      Amended Load so that PathMask is taken into account if there is one.

#endregion

#region Version History: (CCM 8.1.0)
// V8-29681 : A.Probyn
// Implemented new RegisterPlanogram
#endregion

#region Version History: (CCM 8.2.0)
// V8-30870 : L.Ineson
//  Added use of new CustomColumn DisplayName property.
// V8-30958 : L.Ineson
//  Tidied up after column manager changes.
#endregion

#endregion

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Provides helper methods for use with the CustomColumnManager
    /// </summary>
    public static class CustomColumnLayoutHelper
    {
       


        /// <summary>
        /// Shows the open file for loading a label from file.
        /// </summary>
        /// <returns>the selected file name or null</returns>
        public static String ShowDataSheetOpenFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowOpenFileDialog(
                CCMClient.ViewState.GetSessionDirectory(SessionDirectory.DataSheet),
                String.Format(CultureInfo.InvariantCulture, Message.DataSheet_ImportFilter, CustomColumnLayout.DataSheetFileExtension),
                out fileName);

            if (result)
            {
                //update session directory
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.DataSheet,
                    System.IO.Path.GetDirectoryName(fileName));
            }

            return fileName;
        }

        /// <summary>
        /// Shows the SaveFileDialog for saving a label to file.
        /// </summary>
        /// <returns>the saved file name or null</returns>
        public static String ShowDataSheetSaveAsFileDialog()
        {
            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowSaveFileDialog(
            null,
            CCMClient.ViewState.GetSessionDirectory(SessionDirectory.DataSheet),
            String.Format(CultureInfo.InvariantCulture, Message.DataSheet_ImportFilter, CustomColumnLayout.DataSheetFileExtension),
            out fileName);

            //update the session directory
            if (result)
            {
                //update session directory
                CCMClient.ViewState.SetSessionDirectory(
                    SessionDirectory.DataSheet,
                    System.IO.Path.GetDirectoryName(fileName));
            }

            return fileName;
        }

        /// <summary>
        /// Shows the open file for loading a cust column layout from file.
        /// </summary>
        /// <returns>the selected file name or null</returns>
        public static String ShowCustomColumnOpenFileDialog(String initialDirectory)
        {
            //TODO: Implement session directory.

            String fileName = null;

            Boolean result =
            CommonHelper.GetWindowService().ShowOpenFileDialog(
                initialDirectory,
                String.Format(CultureInfo.InvariantCulture,
                Message.DataSheet_ImportFilter, CustomColumnLayout.FileExtension),
                out fileName);


            return fileName;
        }
    }
}