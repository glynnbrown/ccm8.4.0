﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created (merged some methodss from data mangement and other related code into one file helper class)
// V8-2681 : L.Ineson
//  Copied over and merged versions from both clients.
// V8-27931 : N.Haywood
//  Fixed tab delimiter
#endregion

#region Version History: (CCM 8.1.0)
// V8-30130 : A.Probyn
//  Extended LoadSpecificWorksheet to throw any unhandled exceptions up. 
#endregion

#region Version History: CCM820

// V8-31023 : A.Silva
//  Added GetPsaPlanogramsFromFile to get the list of plan names in a psa file.
// V8-31017 : M.Pettit
//	Amended IsFileLocked() to take account of files open in other applications
// V8-31360 : A.Silva
//  Amended GetPsaPlanogramsFromFile so that it opens locked files as the access is read only.

#endregion

#region Version History: CCM830

// V8-31878 : A.Silva
//  Amended GetPsaPlanogramsFromFile so that escaped characters are dealt with correctly.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Aspose.Cells;
using System.Data;
using Galleria.Framework.Logging;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using Galleria.Framework.Imports;
using System.Globalization;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Helper class containing helper methods relating to files and importing etc
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Converts the import aligment to a datagrid column one
        /// </summary>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public static HorizontalAlignment ConvertAlignment(ImportFileDataHorizontalAlignment alignment)
        {
            switch (alignment)
            {
                case ImportFileDataHorizontalAlignment.Center: return HorizontalAlignment.Center;
                case ImportFileDataHorizontalAlignment.Left: return HorizontalAlignment.Left;
                case ImportFileDataHorizontalAlignment.Right: return HorizontalAlignment.Right;
                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Method to open the file and extract the worksheets
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<String> LoadSelectedFileWorksheets(String filePath)
        {
            using (UICodeMetric metric = new UICodeMetric("File Load - Aspose loading file worksheet properties:" + filePath))
            {
                ObservableCollection<String> worksheetNames = new ObservableCollection<String>();

                try
                {
                    //Load workbook and worksheet at index 0
                    LoadOptions loadOptions = new LoadOptions(LoadFormat.Auto);
                    LoadDataOption dataOption = new LoadDataOption();
                    dataOption.SheetIndexes = new int[] { };
                    dataOption.ImportFormula = false;
                    loadOptions.LoadDataOptions = dataOption;
                    loadOptions.LoadDataOnly = true;

                    //Open file using aspose cells
                    Workbook workBook = new Workbook(filePath, loadOptions);

                    foreach (Worksheet sheet in workBook.Worksheets)
                    {
                        //Add to collection
                        worksheetNames.Add(sheet.Name);
                    }

                    workBook = null;
                }
                //catch out of memory exception too?
                catch (IOException)
                {
                    CommonHelper.GetWindowService().ShowOkMessage(
                    Services.MessageWindowType.Error,
                    Message.FileHelper_FileInUse,
                    Message.FileHelper_FileInUse_Desc);
                }
                return worksheetNames;
            }
        }

        /// <summary>
        /// Provides worksheet index for the given workbook sheet name 
        /// </summary>
        /// <returns>
        /// ID of the source work sheet to retreive the data from. 
        /// </returns>
        public static Int32 ResolveWorksheetIndex(String filePath, String excelWorksheet)
        {
            Int32 workbookIndex = 0;

            if (!String.IsNullOrEmpty(filePath))
            {
                //Load available worksheets
                ObservableCollection<String> worksheets = FileHelper.LoadSelectedFileWorksheets(filePath);

                if (excelWorksheet == null && worksheets.Count > 0)
                {
                    excelWorksheet = worksheets.FirstOrDefault();
                }

                //find index of the worksheet containing data
                String workbookSheetName = excelWorksheet;
                if (!String.IsNullOrEmpty(workbookSheetName))
                {

                    if (!String.IsNullOrEmpty(workbookSheetName))
                    {
                        if (worksheets.Contains(workbookSheetName))
                        {
                            workbookIndex = worksheets.IndexOf(workbookSheetName);
                        }
                    }
                }
            }
            return workbookIndex;
        }

        /// <summary>
        /// Method to open the file and load specific worksheet data
        /// </summary>
        /// <returns></returns>
        public static DataTable LoadSpecificWorksheet(String filePath, Int32 worksheetIndex, Boolean headersInFirstRow,
            ref Boolean hasOutOfMemoryException, ref Boolean hasIOException)
        {
            using (UICodeMetric metric = new UICodeMetric("File Load - Aspose loading specific worksheet data:" + filePath))
            {
                DataTable loadedWorksheetData = null;
                Workbook workBook = null;

                try
                {
                    //Load workbook and worksheet
                    LoadOptions loadOptions = new LoadOptions(LoadFormat.Auto);
                    LoadDataOption dataOption = new LoadDataOption();
                    //dataOption.SheetNames = new string[] { worksheetName };
                    dataOption.SheetIndexes = new int[] { worksheetIndex };
                    dataOption.ImportFormula = false;
                    loadOptions.LoadDataOptions = dataOption;
                    loadOptions.LoadDataOnly = true;

                    //Open file using aspose cells
                    workBook = new Workbook(filePath, loadOptions);

                    foreach (Worksheet sheet in workBook.Worksheets)
                    {
                        if (sheet.Index == worksheetIndex)
                        {

                            //Calculate how many rows exist, which is the number we need to fetch
                            int numRowsToFetch = (sheet.Cells.MaxDataRow + 1);

                            if (numRowsToFetch != 0)
                            {
                                //[ISO-13483] Extract data into data table and return (always fetch all, starting at 0 and whether
                                //the headers are in first row comes from paramter).
                                loadedWorksheetData = sheet.Cells.ExportDataTableAsString(0,
                                    0, numRowsToFetch, (sheet.Cells.MaxDataColumn + 1), headersInFirstRow);
                            }
                            break;
                        }
                    }

                    workBook = null;
                }
                //[ISO-13031] Added catch for the out of memory exception when files are v.large.
                catch (OutOfMemoryException)
                {
                    //[ISO-13514] Clear workbook just incase this is still holding memory
                    workBook = null;
                    hasOutOfMemoryException = true;
                }
                catch (IOException)
                {
                    workBook = null;
                    hasIOException = true;
                }
                catch (Exception ex)
                {
                    //General catch all so it doesn't silently fail
                    workBook = null;

                    //Throw error up
                    throw ex;
                }

                return loadedWorksheetData;
            }
        }


        /// <summary>
        /// Method to open the file and load specific worksheet data
        /// </summary>
        /// <returns></returns>
        public static DataTable LoadWorksheetFirstRowOnly(String filePath, String worksheetName, Boolean headersInFirstRow)
        {
            DataTable loadedWorksheetData = null;
            Workbook workBook = null;

            try
            {
                Int32 worksheetIndex = ResolveWorksheetIndex(filePath, worksheetName);

                //Load workbook and worksheet
                LoadOptions loadOptions = new LoadOptions(LoadFormat.Auto);
                LoadDataOption dataOption = new LoadDataOption();
                //dataOption.SheetNames = new string[] { worksheetName };
                dataOption.SheetIndexes = new int[] { worksheetIndex };
                dataOption.ImportFormula = false;
                loadOptions.LoadDataOptions = dataOption;
                loadOptions.LoadDataAndFormatting = true;

                //Open file using aspose cells
                workBook = new Workbook(filePath, loadOptions);

                foreach (Worksheet sheet in workBook.Worksheets)
                {
                    if (sheet.Index != worksheetIndex) continue;

                    Int32 numRowsToFetch = Math.Min((sheet.Cells.MaxDataRow + 1), (headersInFirstRow) ? 1 : 2);
                    if (numRowsToFetch != 0)
                    {
                        //[ISO-13483] Extract data into data table and return (always fetch all, starting at 0 and whether
                        //the headers are in first row comes from paramter).
                        loadedWorksheetData = sheet.Cells.ExportDataTableAsString(0,
                            0, numRowsToFetch, (sheet.Cells.MaxDataColumn + 1), headersInFirstRow);
                    }
                    break;

                }

                workBook = null;
            }
            catch (Exception ex)
            {
                //General catch all so it doesn't silently fail
                workBook = null;

                //Throw error up
                throw ex;
            }

            return loadedWorksheetData;

        }

        /// <summary>
        /// Returns the suspected Encoding for a text / csv file.
        /// The detection is based on Byte Order Mark.
        /// If file cannot be read from, the Encoding.Default is returned.
        /// </summary>
        /// <param name="loadFilePath">The full file path</param>
        /// <returns>The file character encoding</returns>
        public static Encoding DetectFileEncoding(String loadFilePath)
        {
            Boolean useDefault = false;
            // create default encoding (Ansi CodePage)
            Encoding encoding = Encoding.Default;

            // detect Byte Order Mark if any, default will be used otherwise
            Int32 bufferSize = 5;
            byte[] buffer = new byte[bufferSize];
            byte[] fileBytes = null;
            try
            {
                FileStream file = new FileStream(loadFilePath, FileMode.Open);
                file.Read(buffer, 0, bufferSize);
                file.Close();
            }
            catch (IOException)
            {
                // use default encoding
                useDefault = true;
            }

            if (!useDefault)
            {
                if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                {
                    encoding = Encoding.UTF8;
                }
                else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                {
                    encoding = Encoding.Unicode;
                }
                else if ((buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xFE && buffer[3] == 0xFF)  //UTF-32 (BE)
                    || (buffer[0] == 0xFF && buffer[1] == 0xFE && buffer[2] == 0 && buffer[3] == 0)) //UTF-32 (LE)
                {
                    encoding = Encoding.UTF32;
                }
                else if ((buffer[0] == 0x2B && buffer[1] == 0x2F && buffer[2] == 0x76 && buffer[3] == 0x38)
                    || (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76 && buffer[3] == 0x39)
                    || (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76 && buffer[3] == 0x2B)
                    || (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76 && buffer[3] == 0x2F)
                    || (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76 && buffer[3] == 0x2D))
                {
                    encoding = Encoding.UTF7;
                }
                else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                {
                    encoding = Encoding.GetEncoding(1201); //UTF-16 (BE)
                }
                else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                {
                    encoding = Encoding.GetEncoding(1200); //UTF-16 (LE)
                }
                else
                {
                    //attempt a costly check if the encoding is UTF-8 but without BOM
                    fileBytes = System.IO.File.ReadAllBytes(loadFilePath);
                    //amend encoding fallback character as the default '?' can be present in the original text
                    Encoding utf8Encoding = Encoding.GetEncoding(Encoding.UTF8.HeaderName,
                        new EncoderReplacementFallback("[UTF8EncodingError]"),
                        new DecoderReplacementFallback("[UTF8DecodingError]"));
                    String loadFile = utf8Encoding.GetString(fileBytes);
                    if (!loadFile.Contains("[UTF8DecodingError]"))
                    {
                        //conversion was successful, override default encoding
                        encoding = Encoding.UTF8;
                    }
                }
            }

            return encoding;
        }

        /// <summary>
        /// Deletes a file and displays friendly error messages for known exceptions
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Boolean DeleteExistingFile(String fileName)
        {
            try
            {
                System.IO.File.Delete(fileName);
                return true;
            }
            catch (IOException)
            {
                CommonHelper.GetWindowService().ShowOkMessage(
                    Services.MessageWindowType.Error,
                    Message.FileHelper_ErrorHeader,
                    String.Format(CultureInfo.CurrentUICulture, Message.FileHelper_FileInUse, fileName));
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                CommonHelper.GetWindowService().ShowOkMessage(
                    Services.MessageWindowType.Error,
                    Message.FileHelper_ErrorHeader,
                    String.Format(CultureInfo.CurrentUICulture, Message.FileHelper_InsufficientPermissions, fileName));
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Gets the delimiter string for the given text delmiter type value
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static String GetDelimiterString(ImportDefinitionTextDelimiterType delimiter)
        {
            switch (delimiter)
            {
                case ImportDefinitionTextDelimiterType.Semicolon: return ";";
                case ImportDefinitionTextDelimiterType.Pipe: return "|";
                case ImportDefinitionTextDelimiterType.Comma: return ",";
                case ImportDefinitionTextDelimiterType.Tab: return "\t";
                default: return ",";
            }
        }

        /// <summary>
        /// Helper method to return file type from file paths extension
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static ImportDefinitionFileType GetFileTypeFromExtension(String filePath)
        {
            //Extract file type
            String ext = Path.GetExtension(filePath);
            if (ext == ".txt" || ext == ".csv")
            {
                return ImportDefinitionFileType.Text;
            }
            else if (ext == ".xlsx" || ext == ".xls")
            {
                return ImportDefinitionFileType.Excel;
            }
            return ImportDefinitionFileType.None;
        }

        /// <summary>
        /// Checks to see if a file is open
        /// </summary>
        public static Boolean IsFileLocked(System.IO.FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                //required for psa files open in JDA application
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        /// <summary>
        ///     Examine the given <paramref name="planFile"/> and return a list of all planograms found in it.
        /// </summary>
        /// <param name="planFile"></param>
        /// <returns></returns>
        public static List<String> GetPsaPlanogramsFromFile(String planFile)
        {
            var planNames = new List<String>();

            //  NB Even if the file is locked, we are accesing it in read only mode.
            //if (IsFileLocked(new System.IO.FileInfo(planFile))) return planNames;

            FileStream fileStream;
            try
            {
                fileStream = new FileStream(planFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            catch
            {
                return planNames;
            }

            using (fileStream)
            using (TextReader streamReader = new StreamReader(fileStream))
            {
                //  Read the file one line at a time.
                //  Store the names of the plans in the file to return them later.
                while (streamReader.Peek() >= 0)
                {
                    String rawLine = streamReader.ReadLine();
                    if (rawLine == null)
                    {
                        Debug.Fail("Line could not be read and there was supposed to be one.");
                        break;
                    }

                    String[] line = rawLine.SplitBy(',').ToArray();
                    if (String.Equals(line[0], "Planogram", StringComparison.InvariantCultureIgnoreCase))
                    {
                        planNames.Add(line[ProSpaceImportHelper.GetIndex(ProSpaceImportHelper.PlanogramName)]);
                    }
                }
            }

            return planNames;
        }

        /// <summary>
        /// Returns the next available unique file name based on the path given.
        /// Used for renaming when there is an existing item of the same name.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static String GetNextUniqueFilePath(String filePath)
        {
            //return straight out if the path is already unique.
            if (!System.IO.File.Exists(filePath)) return filePath;

            String origName = Path.GetFileNameWithoutExtension(filePath);
            String dir = Path.GetDirectoryName(filePath);
            String ext = Path.GetExtension(filePath);


            //keep incrementing the number until we find a unique path.
            Int32 renamePostfix = 1;
            String newPath;
            do
            {
                newPath = Path.ChangeExtension(Path.Combine(dir, String.Format("{0} ({1})", origName, renamePostfix)), ext);
                renamePostfix++;

            } while (System.IO.File.Exists(newPath));


            return newPath;
        }
    }
}