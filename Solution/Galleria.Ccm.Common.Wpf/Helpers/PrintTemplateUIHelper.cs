﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32810 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Globalization;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Model;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Provides helper methods for ui operations related to print templates.
    /// </summary>
    public static class PrintTemplateUIHelper
    {
        /// <summary>
        /// Shows the dialog allowing the user to select a repository print template.
        /// </summary>
        /// <returns>the selected print template info</returns>
        public static PrintTemplateInfo SelectRepositoryPrintTemplate()
        {
            PrintTemplateInfoList printTemplateInfos;
            try
            {
                printTemplateInfos = PrintTemplateInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                return null;
            }


            GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            win.ItemSource = printTemplateInfos;

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return null;

            PrintTemplateInfo selectedItem = win.SelectedItems.Cast<PrintTemplateInfo>().FirstOrDefault();
            if (selectedItem != null) return selectedItem.Clone();
            else return null;
        }

        /// <summary>
        /// Shows the dialog allowing the user to select a file print template.
        /// </summary>
        /// <returns>the selected print template info</returns>
        public static PrintTemplateInfo SelectPrintTemplateFromFile()
        {
            //show the open file dialog.
            String file = null;
            if (String.IsNullOrEmpty(file))
            {
                Boolean result =
                CommonHelper.GetWindowService().ShowOpenFileDialog(
                    CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PrintTemplate),
                    String.Format(CultureInfo.InvariantCulture, CommonMessage.PrintTemplateSetup_ImportFilter, PrintTemplate.FileExtension),
                    out file);

                if (result)
                {
                    //update the session directory
                    CCMClient.ViewState.SetSessionDirectory(SessionDirectory.PrintTemplate, System.IO.Path.GetDirectoryName(file));
                }

                if (!result) return null;
            }


            PrintTemplateInfo selectedItem;
            try
            {
                selectedItem = PrintTemplateInfoList.FetchByFileNames(new String[] { file }).FirstOrDefault();
                if (selectedItem != null) selectedItem =selectedItem.Clone();
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
                return null;
            }

            return selectedItem;
        }

        /// <summary>
        /// Returns the default print template
        /// </summary>
        public static PrintTemplateInfo GetDefaultPrintTemplateInfo()
        {
            String defaultTemplateId = CCMClient.ViewState.Settings.Model.DefaultPrintTemplateId;

            PrintTemplateInfo defaultTemplate = null;
            if (!String.IsNullOrEmpty(defaultTemplateId))
            {
                try
                {
                    Int32 repositoryId;
                    if (Int32.TryParse(defaultTemplateId, out repositoryId) && CCMClient.ViewState.EntityId > 0)
                    {
                        defaultTemplate = PrintTemplateInfoList.FetchByIds(new Object[] { repositoryId }).FirstOrDefault();
                    }
                    else
                    {
                        defaultTemplate = PrintTemplateInfoList.FetchByFileNames(new String[] { defaultTemplateId }).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }
            if (defaultTemplate == null && CCMClient.ViewState.EntityId > 0)
            {
                try
                {
                    defaultTemplate = PrintTemplateInfoList.FetchByEntityId(CCMClient.ViewState.EntityId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }

            //clone the template so that the rest of the info list can be dropped from memory.
            if (defaultTemplate != null) defaultTemplate.Clone();

            return defaultTemplate;
        }
    }

    
}
