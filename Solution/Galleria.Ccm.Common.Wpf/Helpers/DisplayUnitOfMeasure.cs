﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson 
//  Created.
// V8-26472 : A.Probyn
//  Added new overloaded new method.
// V8-24767 : L.Ineson
//  Added try catch to EntityId param overload to stop
// it falling over when we are not connected to a repository.
// V8-28066 : A.Kuszyk
//  Added PercentageInstance for DisplayUnitOfMeasure instances for percentages.
// V8-28270 : L.ineson
//  Added AdditionalValueConverter
// V8-28006 : J.Pickup
//  Added PercentageNoMultiply as a temporary solution to duplicate converter issues.

#endregion

#region Version History: (CCM 8.1)
// V8-29434: L.Luong
//  Temporary changed percentage converter until framework fix
// V8-30241 : M.Brumby
//  Added a field for PercentageNoMultiply as using the property was
//  causing PercentageInstance to not multiply!
#endregion

#region Version History: (CCM 8.11)
// V8-27521 : L.Ineson
//  Applying the angle display type converter now resets the binding updatesourcetrigger to lost
//  focus so that it does not interfere with text input.
#endregion

#region Version History: (CCM 8.2)
// V8-30980 : L.Ineson
//  Added ForceDecimalDisplay
#endregion

#endregion

using System;
using System.Security;
using System.Windows;
using System.Windows.Data;
using Csla;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Simple class for configuring a display unit of measure
    /// </summary>
    public sealed class DisplayUnitOfMeasure
    {
        #region Static

        /// <summary>
        /// Returns and instance of DisplayUnitOfMeasure that represents a percentage.
        /// </summary>
        public static DisplayUnitOfMeasure PercentageInstance
        {
            get
            {
                if (_percentageInstance == null)
                {
                    _percentageInstance = new DisplayUnitOfMeasure(
                        Message.DisplayUnitOfMeasure_Percentage_DisplayName,
                        Message.DisplayUnitOfMeasure_Percentage_Abbreviation,
                        /*isPrefix*/false,
                        (Application.Current != null) ? Application.Current.Resources[Resources.ResourceKeys.ConverterToPercentage] as IValueConverter : null,
                        /*forceDecimalDisplay*/false);
                }
                return _percentageInstance;
            }
        }

        /// <summary>
        /// Returns and instance of DisplayUnitOfMeasure that represents a percentage without multiplying the value.
        /// </summary>
        public static DisplayUnitOfMeasure PercentageNoMultiply
        {
            get
            {
                if (_percentageNoMultiply == null)
                {
                    _percentageNoMultiply = new DisplayUnitOfMeasure(
                        Message.DisplayUnitOfMeasure_Percentage_DisplayName,
                        Message.DisplayUnitOfMeasure_Percentage_Abbreviation,
                        false);
                }

                //Force the additional value converter away to prevent multiplication. (Already multiplied when coming throgh this property).
                _percentageNoMultiply.AdditionalValueConverter = null;
                return _percentageNoMultiply;
            }
        }


        #endregion

        #region Fields
        private static DisplayUnitOfMeasure _percentageInstance;
        private static DisplayUnitOfMeasure _percentageNoMultiply;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the display name of this uom
        /// </summary>
        public String DisplayName
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Return the abbreviation to display for values using this uom
        /// </summary>
        public String Abbreviation
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns true if the abbreviation is a prefix to the value.
        /// </summary>
        public Boolean IsPrefix
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the additional converter to be applied when processing the value.
        /// </summary>
        public IValueConverter AdditionalValueConverter
        {
            get;
            private set;
        }

        /// <summary>
        /// If true, all dp places will be displayed.
        /// Used for currency.
        /// </summary>
        public Boolean ForceDecimalDisplay
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public DisplayUnitOfMeasure(String displayName, String abbreviation, Boolean isPrefix)
            :this(displayName, abbreviation, isPrefix, null, false)
        {
            
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public DisplayUnitOfMeasure(String displayName, String abbreviation, Boolean isPrefix, Boolean forceDecimalDisplay)
            : this(displayName, abbreviation, isPrefix, null, forceDecimalDisplay)
        {

        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public DisplayUnitOfMeasure(String displayName, String abbreviation, Boolean isPrefix, IValueConverter valueConverter, Boolean forceDecimalDisplay)
        {
            DisplayName = displayName;
            Abbreviation = abbreviation;
            IsPrefix = isPrefix;
            ForceDecimalDisplay = forceDecimalDisplay;
            AdditionalValueConverter = valueConverter;
        }

        #endregion
    }

    /// <summary>
    /// A collection containing all display uom information required
    /// </summary>
    public sealed class DisplayUnitOfMeasureCollection
    {
        #region Fields
        private static DisplayUnitOfMeasureCollection _empty;
        private IValueConverter _converter;
        #endregion

        #region Properties
        
        public static DisplayUnitOfMeasureCollection Empty
        {
            get
            {
                if (_empty == null)
                {
                    _empty = new DisplayUnitOfMeasureCollection();
                }
                return _empty;
            }
        }

        public DisplayUnitOfMeasure Percentage
        {
            get { return DisplayUnitOfMeasure.PercentageInstance; }
        }

        public DisplayUnitOfMeasure PercentageNoMultiply
        {
            get { return DisplayUnitOfMeasure.PercentageNoMultiply; }
        }

        public DisplayUnitOfMeasure Length
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure Area
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure Currency
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure Volume
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure Weight
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure Angle
        {
            get;
            private set;
        }

        public DisplayUnitOfMeasure LengthCubed
        {
            get;
            private set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Private constructor.
        /// </summary>
        private DisplayUnitOfMeasureCollection(){}

        #endregion

        #region Methods

        /// <summary>
        /// Returns the uom description to use for the given property display type.
        /// </summary>
        public DisplayUnitOfMeasure GetUom(ModelPropertyDisplayType displayType)
        {
            switch (displayType)
            {
                default:
                case ModelPropertyDisplayType.None:
                    return null;
                
                case ModelPropertyDisplayType.Percentage:
                    return this.Percentage;

                case ModelPropertyDisplayType.Angle:
                    return this.Angle;

                case ModelPropertyDisplayType.Length:
                    return this.Length;

                case ModelPropertyDisplayType.Currency:
                    return this.Currency;

                case ModelPropertyDisplayType.Area:
                    return this.Area;

                case ModelPropertyDisplayType.Volume:
                    return this.Volume;

                case ModelPropertyDisplayType.LengthCubed:
                    return this.LengthCubed;
            }
        }

        /// <summary>
        /// Returns the active uom converter instance.
        /// </summary>
        public IValueConverter GetUomConverter()
        {
            if (_converter == null)
            {
                _converter = new UnitDisplayConverter();
            }
            return _converter;
        }

        /// <summary>
        /// Applies any required converter to the given binding.
        /// </summary>
        /// <param name="binding">the binding to apply the convert to</param>
        /// <param name="displayType">the display type of the property.</param>
        /// <param name="propertyType">the type of property being bound to.</param>
        public void ApplyConverter(Binding binding, ModelPropertyDisplayType displayType, Type propertyType)
        {
            ApplyConverter(binding, displayType, CommonHelper.GetBasicPropertyType(propertyType));
        }

        /// <summary>
        /// Applies any required converter to the given binding.
        /// </summary>
        /// <param name="binding">the binding to apply the convert to</param>
        /// <param name="displayType">the display type of the property.</param>
        /// <param name="propertyType">the type of property being bound to.</param>
        public void ApplyConverter(Binding binding, ModelPropertyDisplayType displayType, CommonHelper.PropertyType propertyType)
        {
            #region DateTime
            if (propertyType == CommonHelper.PropertyType.DateTime)
            {
                switch (displayType)
                {
                    case ModelPropertyDisplayType.DateTime:
                        binding.Converter =  Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDateTime] as IValueConverter;
                        break;

                    case ModelPropertyDisplayType.Time:
                        binding.Converter = Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterUTCToLocalTime] as IValueConverter;
                        break;

                    default:
                        binding.Converter = Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDate] as IValueConverter;
                        break;
                        
                }
            }
            #endregion

            else
            {
                switch (displayType)
                {
                    case ModelPropertyDisplayType.None:
                        //do nothing
                        break;

                    #region Angle
                    case ModelPropertyDisplayType.Angle:
                        {
                            binding.Converter = Application.Current.Resources[Resources.ResourceKeys.ConverterRadiansToDegrees] as IValueConverter;

                            //[V8-27521] force the binding trigger to lost focus otherwise the values get changed as the user types.
                            binding.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
                        }
                        break;
                    #endregion

                    #region Colour
                    case ModelPropertyDisplayType.Colour:
                        {
                            binding.Converter = Application.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.ConverterIntToColor]  as IValueConverter;
                        }
                        break;
                    #endregion

                    default:
                        {
                            binding.Converter = GetUomConverter();
                            binding.ConverterParameter = GetUom(displayType);
                        }
                        break;
                }
            }

        }

        #endregion

        #region FactoryMethods

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given planogram.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(Planogram plan)
        {
            DisplayUnitOfMeasureCollection item = new DisplayUnitOfMeasureCollection();
            
            item.Length = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.FriendlyNames[plan.LengthUnitsOfMeasure],
                PlanogramLengthUnitOfMeasureTypeHelper.Abbreviations[plan.LengthUnitsOfMeasure],
                /*isPrefix*/false);

            item.LengthCubed = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedFriendlyName(plan.LengthUnitsOfMeasure),
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedAbbreviation(plan.LengthUnitsOfMeasure),
                /*isPrefix*/false);

            item.Area = new DisplayUnitOfMeasure(
                PlanogramAreaUnitOfMeasureTypeHelper.FriendlyNames[plan.AreaUnitsOfMeasure],
                PlanogramAreaUnitOfMeasureTypeHelper.Abbreviations[plan.AreaUnitsOfMeasure],
                /*isPrefix*/false);

            item.Volume = new DisplayUnitOfMeasure(
                PlanogramVolumeUnitOfMeasureTypeHelper.FriendlyNames[plan.VolumeUnitsOfMeasure],
                PlanogramVolumeUnitOfMeasureTypeHelper.Abbreviations[plan.VolumeUnitsOfMeasure],
                /*isPrefix*/false);

            item.Currency = new DisplayUnitOfMeasure(
                PlanogramCurrencyUnitOfMeasureTypeHelper.FriendlyNames[plan.CurrencyUnitsOfMeasure],
                PlanogramCurrencyUnitOfMeasureTypeHelper.Abbreviations[plan.CurrencyUnitsOfMeasure],
                /*isPrefix*/true, /*forceDecimalDisplay*/true);

            item.Weight = new DisplayUnitOfMeasure(
                PlanogramWeightUnitOfMeasureTypeHelper.FriendlyNames[plan.WeightUnitsOfMeasure],
                PlanogramWeightUnitOfMeasureTypeHelper.Abbreviations[plan.WeightUnitsOfMeasure],
                /*isPrefix*/false);


            return item;
         }

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given PlanogramInfo.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(PlanogramInfo plan)
        {
            DisplayUnitOfMeasureCollection item = new DisplayUnitOfMeasureCollection();

            item.Length = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.FriendlyNames[plan.LengthUnitsOfMeasure],
                PlanogramLengthUnitOfMeasureTypeHelper.Abbreviations[plan.LengthUnitsOfMeasure],
                /*isPrefix*/false);

            item.LengthCubed = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedFriendlyName(plan.LengthUnitsOfMeasure),
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedAbbreviation(plan.LengthUnitsOfMeasure),
                /*isPrefix*/false);

            item.Area = new DisplayUnitOfMeasure(
                PlanogramAreaUnitOfMeasureTypeHelper.FriendlyNames[plan.AreaUnitsOfMeasure],
                PlanogramAreaUnitOfMeasureTypeHelper.Abbreviations[plan.AreaUnitsOfMeasure],
                /*isPrefix*/false);

            item.Volume = new DisplayUnitOfMeasure(
                PlanogramVolumeUnitOfMeasureTypeHelper.FriendlyNames[plan.VolumeUnitsOfMeasure],
                PlanogramVolumeUnitOfMeasureTypeHelper.Abbreviations[plan.VolumeUnitsOfMeasure],
                /*isPrefix*/false);

            item.Currency = new DisplayUnitOfMeasure(
                PlanogramCurrencyUnitOfMeasureTypeHelper.FriendlyNames[plan.CurrencyUnitsOfMeasure],
                PlanogramCurrencyUnitOfMeasureTypeHelper.Abbreviations[plan.CurrencyUnitsOfMeasure],
                /*isPrefix*/true, /*forceDecimalDisplay*/true);

            item.Weight = new DisplayUnitOfMeasure(
                PlanogramWeightUnitOfMeasureTypeHelper.FriendlyNames[plan.WeightUnitsOfMeasure],
                PlanogramWeightUnitOfMeasureTypeHelper.Abbreviations[plan.WeightUnitsOfMeasure],
                /*isPrefix*/false);


            return item;
        }

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given FixturePackage.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(FixturePackage fixturePackage)
        {
            DisplayUnitOfMeasureCollection item = new DisplayUnitOfMeasureCollection();

            item.Length = new DisplayUnitOfMeasure(
                FixtureLengthUnitOfMeasureTypeHelper.FriendlyNames[fixturePackage.LengthUnitsOfMeasure],
                FixtureLengthUnitOfMeasureTypeHelper.Abbreviations[fixturePackage.LengthUnitsOfMeasure],
                /*isPrefix*/false);

            PlanogramLengthUnitOfMeasureType lengthCubedType = 
                FixtureLengthUnitOfMeasureTypeHelper.ToPlanogramLengthUnitOfMeasureType(fixturePackage.LengthUnitsOfMeasure);
            item.LengthCubed = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedFriendlyName(lengthCubedType),
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedAbbreviation(lengthCubedType),
                /*isPrefix*/false);

            item.Area = new DisplayUnitOfMeasure(
                FixtureAreaUnitOfMeasureTypeHelper.FriendlyNames[fixturePackage.AreaUnitsOfMeasure],
                FixtureAreaUnitOfMeasureTypeHelper.Abbreviations[fixturePackage.AreaUnitsOfMeasure],
                /*isPrefix*/false);

            item.Volume = new DisplayUnitOfMeasure(
                FixtureVolumeUnitOfMeasureTypeHelper.FriendlyNames[fixturePackage.VolumeUnitsOfMeasure],
                FixtureVolumeUnitOfMeasureTypeHelper.Abbreviations[fixturePackage.VolumeUnitsOfMeasure],
                /*isPrefix*/false);

            item.Currency = new DisplayUnitOfMeasure(
                FixtureCurrencyUnitOfMeasureTypeHelper.FriendlyNames[fixturePackage.CurrencyUnitsOfMeasure],
                FixtureCurrencyUnitOfMeasureTypeHelper.Abbreviations[fixturePackage.CurrencyUnitsOfMeasure],
                /*isPrefix*/true, /*forceDecimalDisplay*/true);

            item.Weight = new DisplayUnitOfMeasure(
                FixtureWeightUnitOfMeasureTypeHelper.FriendlyNames[fixturePackage.WeightUnitsOfMeasure],
                FixtureWeightUnitOfMeasureTypeHelper.Abbreviations[fixturePackage.WeightUnitsOfMeasure],
                /*isPrefix*/false);


            return item;
        }

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given Entity.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(Entity entity)
        {
            DisplayUnitOfMeasureCollection item = new DisplayUnitOfMeasureCollection();

            item.Length = new DisplayUnitOfMeasure(
                UnitOfMeasureLengthTypeHelper.FriendlyNames[entity.SystemSettings.LengthUnitsOfMeasure],
                UnitOfMeasureLengthTypeHelper.Abbreviations[entity.SystemSettings.LengthUnitsOfMeasure],
                /*isPrefix*/false);

            item.LengthCubed = new DisplayUnitOfMeasure(
               UnitOfMeasureLengthTypeHelper.GetLengthCubedFriendlyName(entity.SystemSettings.LengthUnitsOfMeasure),
               UnitOfMeasureLengthTypeHelper.GetLengthCubedAbbreviation(entity.SystemSettings.LengthUnitsOfMeasure),
                /*isPrefix*/false);

            item.Area = new DisplayUnitOfMeasure(
                UnitOfMeasureAreaTypeHelper.FriendlyNames[entity.SystemSettings.AreaUnitsOfMeasure],
                UnitOfMeasureAreaTypeHelper.Abbreviations[entity.SystemSettings.AreaUnitsOfMeasure],
                /*isPrefix*/false);

            item.Volume = new DisplayUnitOfMeasure(
                UnitOfMeasureVolumeTypeHelper.FriendlyNames[entity.SystemSettings.VolumeUnitsOfMeasure],
                UnitOfMeasureVolumeTypeHelper.Abbreviations[entity.SystemSettings.VolumeUnitsOfMeasure],
                /*isPrefix*/false);

            item.Currency = new DisplayUnitOfMeasure(
                UnitOfMeasureCurrencyTypeHelper.FriendlyNames[entity.SystemSettings.CurrencyUnitsOfMeasure],
                UnitOfMeasureCurrencyTypeHelper.Abbreviations[entity.SystemSettings.CurrencyUnitsOfMeasure],
                /*isPrefix*/true, /*forceDecimalDisplay*/true);

            item.Weight = new DisplayUnitOfMeasure(
                UnitOfMeasureWeightTypeHelper.FriendlyNames[entity.SystemSettings.WeightUnitsOfMeasure],
                UnitOfMeasureWeightTypeHelper.Abbreviations[entity.SystemSettings.WeightUnitsOfMeasure],
                /*isPrefix*/false);


            return item;

        }

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given Entity.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(Int32 entityId)
        {
            DisplayUnitOfMeasureCollection item;

            Entity entity = null;
            try
            {
                entity = Entity.FetchById(entityId);
            }
            catch (DataPortalException) { }
            catch (SecurityException) { }

            if (entity != null)
            {
                item = new DisplayUnitOfMeasureCollection();

                item.Length = new DisplayUnitOfMeasure(
                    UnitOfMeasureLengthTypeHelper.FriendlyNames[entity.SystemSettings.LengthUnitsOfMeasure],
                    UnitOfMeasureLengthTypeHelper.Abbreviations[entity.SystemSettings.LengthUnitsOfMeasure],
                    /*isPrefix*/false);

                item.LengthCubed = new DisplayUnitOfMeasure(
                   UnitOfMeasureLengthTypeHelper.GetLengthCubedFriendlyName(entity.SystemSettings.LengthUnitsOfMeasure),
                   UnitOfMeasureLengthTypeHelper.GetLengthCubedAbbreviation(entity.SystemSettings.LengthUnitsOfMeasure),
                    /*isPrefix*/false);

                item.Area = new DisplayUnitOfMeasure(
                    UnitOfMeasureAreaTypeHelper.FriendlyNames[entity.SystemSettings.AreaUnitsOfMeasure],
                    UnitOfMeasureAreaTypeHelper.Abbreviations[entity.SystemSettings.AreaUnitsOfMeasure],
                    /*isPrefix*/false);

                item.Volume = new DisplayUnitOfMeasure(
                    UnitOfMeasureVolumeTypeHelper.FriendlyNames[entity.SystemSettings.VolumeUnitsOfMeasure],
                    UnitOfMeasureVolumeTypeHelper.Abbreviations[entity.SystemSettings.VolumeUnitsOfMeasure],
                    /*isPrefix*/false);

                item.Currency = new DisplayUnitOfMeasure(
                    UnitOfMeasureCurrencyTypeHelper.FriendlyNames[entity.SystemSettings.CurrencyUnitsOfMeasure],
                    UnitOfMeasureCurrencyTypeHelper.Abbreviations[entity.SystemSettings.CurrencyUnitsOfMeasure],
                    /*isPrefix*/true, /*forceDecimalDisplay*/true);

                item.Weight = new DisplayUnitOfMeasure(
                    UnitOfMeasureWeightTypeHelper.FriendlyNames[entity.SystemSettings.WeightUnitsOfMeasure],
                    UnitOfMeasureWeightTypeHelper.Abbreviations[entity.SystemSettings.WeightUnitsOfMeasure],
                    /*isPrefix*/false);
            }
            else
            {
                item = DisplayUnitOfMeasureCollection.Empty;
            }


            return item;

        }

        /// <summary>
        /// Creates a new instance of DisplayUnitOfMeasureCollection for the given UserEditorSettings.
        /// </summary>
        public static DisplayUnitOfMeasureCollection NewDisplayUnitOfMeasureCollection(UserEditorSettings settings)
        {
            DisplayUnitOfMeasureCollection item = new DisplayUnitOfMeasureCollection();

            item.Length = new DisplayUnitOfMeasure(
               PlanogramLengthUnitOfMeasureTypeHelper.FriendlyNames[settings.LengthUnitOfMeasure],
               PlanogramLengthUnitOfMeasureTypeHelper.Abbreviations[settings.LengthUnitOfMeasure],
                /*isPrefix*/false);

            item.LengthCubed = new DisplayUnitOfMeasure(
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedFriendlyName(settings.LengthUnitOfMeasure),
                PlanogramLengthUnitOfMeasureTypeHelper.GetLengthCubedAbbreviation(settings.LengthUnitOfMeasure),
                /*isPrefix*/false);

            PlanogramAreaUnitOfMeasureType areaUnit;
            switch (settings.LengthUnitOfMeasure)
            {
                case PlanogramLengthUnitOfMeasureType.Inches: areaUnit = PlanogramAreaUnitOfMeasureType.SquareInches;break;
                case PlanogramLengthUnitOfMeasureType.Centimeters: areaUnit = PlanogramAreaUnitOfMeasureType.SquareCentimeters;break;
                default:  areaUnit = PlanogramAreaUnitOfMeasureType.Unknown; break;
            }

            item.Area = new DisplayUnitOfMeasure(
                PlanogramAreaUnitOfMeasureTypeHelper.FriendlyNames[areaUnit],
                PlanogramAreaUnitOfMeasureTypeHelper.Abbreviations[areaUnit],
                /*isPrefix*/false);

            item.Volume = new DisplayUnitOfMeasure(
                PlanogramVolumeUnitOfMeasureTypeHelper.FriendlyNames[settings.VolumeUnitOfMeasure],
                PlanogramVolumeUnitOfMeasureTypeHelper.Abbreviations[settings.VolumeUnitOfMeasure],
                /*isPrefix*/false);

            item.Currency = new DisplayUnitOfMeasure(
                PlanogramCurrencyUnitOfMeasureTypeHelper.FriendlyNames[settings.CurrencyUnitOfMeasure],
                PlanogramCurrencyUnitOfMeasureTypeHelper.Abbreviations[settings.CurrencyUnitOfMeasure],
                /*isPrefix*/true, /*forceDecimalDisplay*/true);

            item.Weight = new DisplayUnitOfMeasure(
                PlanogramWeightUnitOfMeasureTypeHelper.FriendlyNames[settings.WeightUnitOfMeasure],
                PlanogramWeightUnitOfMeasureTypeHelper.Abbreviations[settings.WeightUnitOfMeasure],
                /*isPrefix*/false);

            return item;

        }

        #endregion
    }
}
