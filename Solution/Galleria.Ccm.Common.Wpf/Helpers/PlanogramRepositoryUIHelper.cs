﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM801
//V8-28251 : L.Ineson
// Created
// V8-28716 : L.Ineson
//  Moved code to copy planograms to here.
#endregion
#region Version History: CCM802
// V8-28840 : L.Luong
//  Added EntityId to saves
#endregion
#region Version History: CCM810
// V8-29746 : A.Probyn
//  Updated OverwritePackage on CopyPlansToGroupWork to deal with content lookup.
// V8-29811 : A.Probyn
//  Moved some OverwritePackage and CopyPackage into the model project for reuse by the engine.
//  Updated FindWorkpackagePlanogramGroupId as Workpakcages folder should now no longer exists
// V8-29973 : N.Foste
// Removed FindWorkpackagePlanogramGroupId as no longer required due to changes made by Alex
// V8-29996 : L.Ineson
//  CreateNewGroup now only checks that name is unique under the parent group.
#endregion
#region Version History : CCM 811
// V8-30344 : D.Pleasance
//  Amended ConfirmPlanogramOverwriteWithUser to include PlanogramGroupName so we can see the target folder of the plan being overwritten.
// V8-30415 : M.Pettit
//  Copying a plan into the same folder threw an unhandled exception
// V8-28935 : D.Pleasance
//  Amended ShowPlanogramGroupDeleteWarning to validate for locked plans that require alternate messages to the user.
#endregion
#region Version History: CCM 830
// V8-32525 : M.Brumby
//  Added delete permission check to group move and delete functionality
// CCM-18522 : L.Ineson
// Added a retry attempt to plans that fail in CopyPlansToGroupWork and a neater error message if they still have issues.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using Csla;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Provides helper methods for the planogram repository screens.
    /// </summary>
    public static class PlanogramRepositoryUIHelper
    {
        #region Planogram Group Methods
        /// <summary>
        /// Creates a new planogram group child under the given parent and saves it immediately.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static Int32? CreateNewPlanogramGroup(PlanogramGroup parentGroup)
        {
            Debug.Assert(!parentGroup.IsNew, "Cannot add to a group that has not yet been inserted");
            if (parentGroup.IsNew) return null;

            PlanogramHierarchy parentHierarchy = parentGroup.ParentHierarchy;

            //Ask the user for the name of the new group.
            String groupName;

            Boolean promptResult =
                CommonHelper.GetWindowService().PromptUserForValue(
                  Message.PlanogramHierarchySelector_NewGroupPrompt,
                    Message.PlanogramHierarchySelector_NewGroupPrompt_Description,
                    Message.PlanogramHierarchySelector_NewGroupPrompt_NotUniqueDescription,
                    Message.Generic_Name,
                    true,
                    (s) => !parentGroup.ChildList.Any(g => String.Compare(g.Name, s, true) == 0),
                    String.Empty,
                    out groupName);
            if (!promptResult) return null;

            Int32? newGroupId = null;

            try
            {
                newGroupId = PlanogramGroup.InsertPlanogramGroup(parentGroup.ParentHierarchy.Id, parentGroup.Id, groupName, null);
            }
            catch (DataPortalException e)
            {
                Console.WriteLine(e);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(Framework.Model.ModelObject<PlanogramGroup>.FriendlyName, OperationType.Save);
                return null;
            }

            return newGroupId;
        }

        /// <summary>
        ///     Displays a screen to edit the Planogram Group properties.
        /// </summary>
        /// <param name="planogramGroupView">The view containing the planogram group to be edited.</param>
        /// <param name="showMerchGroupSelector">Whether to show or not the Merchandising Group selector (hidden by default).</param>
        /// <returns>True if the re-name operation was successful.</returns>
        public static void ShowPlanogramGroupEditWindow(PlanogramGroup planogramGroup, Boolean showMerchGroupSelector)
        {
            if (planogramGroup == null) return;

            PlanogramGroupEditViewModel editorViewModel = new PlanogramGroupEditViewModel(planogramGroup) { ShowMerchGroupSelector = showMerchGroupSelector };

            CommonHelper.GetWindowService().ShowDialog<PlanogramGroupEditWindow>(editorViewModel);

            if (editorViewModel.DialogResult == true)
            {
                try
                {
                    planogramGroup.UpdatePlanogramGroup();
                }
                catch (DataPortalException ex)
                {
                    CommonHelper.RecordException(ex);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(planogramGroup.Name, OperationType.Save);
                }
            }
        }

        #region Planogram Group Deletion

        /// <summary>
        /// Returns true if the given planogram group can be deleted.
        /// </summary>
        public static Boolean CanDeletePlanogramGroup(PlanogramGroup groupToDelete)
        {
            if (groupToDelete == null) return false;

            //must not be the root
            if (groupToDelete.ParentGroup == null) return false;

            //must have permission to delete the planogram group
            if (!Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, typeof(PlanogramGroup))) return false;

            return true;
        }

        /// <summary>
        /// Method to carry out the actions when the user requests to delete a planogram group.
        /// Inc. building the deletion list, confirming the delete with the user and then actually performing the delete.
        /// </summary>
        /// <param name="groupToDelete">the group to be deleted</param>
        /// <param name="itemDeletedHandler">handler to call each time an item is deleted.</param>
        public static void DeletePlanogramGroup(PlanogramGroup groupToDelete, EventHandler<EventArgs<Object>> itemDeletedHandler)
        {
            //build the list of items to be deleted
            Dictionary<PlanogramGroup, List<PlanogramInfo>> deletionList = PlanogramRepositoryUIHelper.BuildPlanogramGroupDeletionList(groupToDelete);
            if (deletionList == null || deletionList.Count == 0) return;

            //confirm the delete with the user
            if (!PlanogramRepositoryUIHelper.ShowPlanogramGroupDeleteWarning(groupToDelete, deletionList)) return;

            //perform the deletion
            PlanogramRepositoryUIHelper.DeletePlanogramGroupItems(deletionList, itemDeletedHandler);
        }

        /// <summary>
        /// Shows a warning dialog to the user asking them to confirm the 
        /// deletion of the given planogram group
        /// </summary>
        /// <param name="groupToDelete">the group to be deleted</param>
        /// <param name="parentWindow">the parent window to associate the dialog with.</param>
        /// <returns></returns>
        private static Boolean ShowPlanogramGroupDeleteWarning(PlanogramGroup groupToDelete, Dictionary<PlanogramGroup, List<PlanogramInfo>> deleteList)
        {
            if (groupToDelete == null) return false; //return false if we have no group.

            Int32 childGroupCount = deleteList.Keys.Count - 1;
            Int32 childPlanCount = deleteList.Sum(p => p.Value.Count);
            Int32 lockedPlanCount = 0;
            Int32 totalLockedPlanCount = 0;

            //check to see if we are allowed to delete planograms
            if(!ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString()))
            {
                //if we arn't allowed to delete planograms and there are planograms
                //in the delete list then don't delete
                if (deleteList.Any(p => p.Value.Any()))
                {
                    // no delete can occur so display ok message box and always return false for this case. (all plans are locked)
                    CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Warning,
                        String.Format(CultureInfo.CurrentCulture, Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, groupToDelete.Name),
                        String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningDescriptionNoPlanogramDeletePermission, deleteList.Sum(p => p.Value.Count)));

                    return false;
                }
            }

            foreach (PlanogramGroup planGroup in deleteList.Keys)
            {
                if (deleteList[planGroup].Any())
                {
                    if (deleteList[planGroup].All(p => p.IsLocked)) 
                    {
                        // all plans within the group are locked, remove from number of groups \ plans that can be deleted
                        childGroupCount--;
                        lockedPlanCount = deleteList[planGroup].Count;
                        childPlanCount = childPlanCount - lockedPlanCount;
                        totalLockedPlanCount = totalLockedPlanCount + lockedPlanCount;
                    }
                    else
                    {
                        // check if any are locked
                        if (deleteList[planGroup].Any(p => p.IsLocked))
                        {
                            // remove from plan count totals any plans that are locked
                            lockedPlanCount = deleteList[planGroup].Where(p => p.IsLocked).Count();
                            childPlanCount = childPlanCount - lockedPlanCount;
                            totalLockedPlanCount = totalLockedPlanCount + lockedPlanCount;
                        }
                    }
                }
            }

            Boolean hasContinueCancel = true;
            String description = Message.PlanogramHierarchySelector_DeleteGroup_WarningDescription;

            if ((childGroupCount > 0 || childPlanCount > 0) && totalLockedPlanCount > 0)
            {
                description = String.Format(CultureInfo.CurrentCulture,
                    Message.PlanogramHierarchySelector_DeleteGroup_WarningDescriptionHasChildrenAndLockedPlanograms,
                    childGroupCount, childPlanCount, totalLockedPlanCount);
            }
            else if (childGroupCount > 0 || childPlanCount > 0)
            {
                description = String.Format(CultureInfo.CurrentCulture,
                    Message.PlanogramHierarchySelector_DeleteGroup_WarningDescriptionHasChildren,
                    childGroupCount, childPlanCount);
            }
            else if (totalLockedPlanCount > 0)
            {
                hasContinueCancel = false;
                description = String.Format(CultureInfo.CurrentCulture,
                    Message.PlanogramHierarchySelector_DeleteGroup_WarningDescriptionHasLockedPlanograms,
                    totalLockedPlanCount);
            }

            if (hasContinueCancel)
            {
                ModalMessageResult result =
                    CommonHelper.GetWindowService().ShowMessage(
                     MessageWindowType.Warning,
                     String.Format(CultureInfo.CurrentCulture, Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, groupToDelete.Name),
                     description,
                     Message.Generic_Continue,
                     Message.Generic_Cancel);

                return (result == ModalMessageResult.Button1);
            }
            else
            {
                // no delete can occur so display ok message box and always return false for this case. (all plans are locked)
                CommonHelper.GetWindowService().ShowOkMessage(
                    MessageWindowType.Warning,
                    String.Format(CultureInfo.CurrentCulture, Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, groupToDelete.Name),
                    description);

                return false;
            }
        }

        /// <summary>
        /// Builds a list of planogram groups child planograms to be deleted.
        /// </summary>
        private static Dictionary<PlanogramGroup, List<PlanogramInfo>> BuildPlanogramGroupDeletionList(PlanogramGroup groupToDelete)
        {
            BuildPlanogramGroupDeletionListWork work = new BuildPlanogramGroupDeletionListWork(groupToDelete);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            return work.Result;
        }

        private static Dictionary<PlanogramGroup, List<PlanogramInfo>> BuildPlanogramGroupMoveList(PlanogramGroup groupToDelete)
        {
            BuildPlanogramGroupDeletionListWork work = new BuildPlanogramGroupDeletionListWork(groupToDelete, Message.PlanogramRepository_MovePlanogramGroup_MoveListHeader, Message.PlanogramRepository_MovePlanogramGroup_MoveListDescription);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            return work.Result;
        }

        /// <summary>
        /// Performs the actual deletion.
        /// </summary>
        /// <param name="deletionList">the list of groups and plans to delete</param>
        /// <param name="itemDeletedHandler">handler to be called when an item is deleted.</param>
        private static void DeletePlanogramGroupItems(Dictionary<PlanogramGroup, List<PlanogramInfo>> deletionList, EventHandler<EventArgs<Object>> itemDeletedHandler)
        {
            DeletePlanogramGroupItemsWork work = new DeletePlanogramGroupItemsWork(deletionList);
            work.ItemDeleted += itemDeletedHandler;
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            work.ItemDeleted -= itemDeletedHandler;
        }


        /// <summary>
        /// Worker to build up the list of planogram groups and planograms
        /// to be deleted whilst showing a modal busy.
        /// </summary>
        private sealed class BuildPlanogramGroupDeletionListWork : IModalBusyWork
        {
            #region Nested classes

            private sealed class WorkArgs
            {
                public Int32 PlanogramHierarchyId { get; set; }
                public Int32 PlanogramGroupId { get; set; }
                public String PlanogramGroupName { get; set; }
            }

            #endregion

            #region Fields
            private Int32 _hierarchyId;
            private Int32 _sourceGroupId;
            private String _sourceGroupName;
            private String _header;
            private String _description;
            private Dictionary<PlanogramGroup, List<PlanogramInfo>> _result;
            #endregion

            #region Properties

            public Dictionary<PlanogramGroup, List<PlanogramInfo>> Result
            {
                get { return _result; }
                private set { _result = value; }
            }

            public Boolean ReportsProgress
            {
                get { return false; }
            }

            public Boolean SupportsCancellation
            {
                get { return false; }
            }

            #endregion

            #region Constructor
            public BuildPlanogramGroupDeletionListWork(PlanogramGroup group)
                :this(group,  Message.PlanogramRepository_DeletePlanogramGroup_DeleteListHeader,
                Message.PlanogramRepository_DeletePlanogramGroup_DeleteListDesc)
            {
            }

            public BuildPlanogramGroupDeletionListWork(PlanogramGroup group, String header, String description)
            {
                _hierarchyId = group.ParentHierarchy.Id;
                _sourceGroupId = group.Id;
                _sourceGroupName = group.Name;
                _header = header;
                _description = description;
            }
            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowIndeterminateBusy(_header, _description);
            }

            public object GetWorkerArgs()
            {
                return new WorkArgs()
                {
                    PlanogramHierarchyId = _hierarchyId,
                    PlanogramGroupId = _sourceGroupId,
                    PlanogramGroupName = _sourceGroupName
                };
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                WorkArgs args = (WorkArgs)e.Argument;
                var result = new Dictionary<PlanogramGroup, List<PlanogramInfo>>();


                PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchById(args.PlanogramHierarchyId);
                PlanogramGroup startGroup = hierarchy.EnumerateAllGroups().FirstOrDefault(g => g.Id == args.PlanogramGroupId);

                if (startGroup != null)
                {
                    AddToDeleteList(startGroup, result);
                }


                e.Result = result;
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                //nothing to report.
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(PlanogramGroup.FriendlyName, OperationType.Delete);
                }
                else if (!e.Cancelled)
                {
                    this.Result = e.Result as Dictionary<PlanogramGroup, List<PlanogramInfo>>;
                }

            }

            private void AddToDeleteList(PlanogramGroup group, Dictionary<PlanogramGroup, List<PlanogramInfo>> list)
            {
                //recurse down so leaf nodes are added first.
                foreach (PlanogramGroup child in group.ChildList)
                {
                    AddToDeleteList(child, list);
                }

                //add the results for this group
                list[group] = PlanogramInfoList.FetchByPlanogramGroupId(group.Id).ToList();
            }


            #endregion
        }

        /// <summary>
        /// Worker to delete a list of planogram groups and planograms
        /// whilst showing a modal busy.
        /// </summary>
        private sealed class DeletePlanogramGroupItemsWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkArgs
            {
                public Dictionary<PlanogramGroup, List<PlanogramInfo>> DeletionList { get; set; }
                public Int32? UserId { get; set; }
            }

            #endregion

            #region Fields
            private Dictionary<PlanogramGroup, List<PlanogramInfo>> _deletionList; //the full list of plans to be deleted.
            private Queue<PlanogramGroup> _groupQueue = new Queue<PlanogramGroup>();
            private Queue<PlanogramInfo> _planQueue; //the queue of plans left to delete.

            private Int32 _currentGroupDeleteNo;

            private List<PlanogramGroup> _deleteFailedList;
            #endregion

            #region Event

            public event EventHandler<EventArgs<Object>> ItemDeleted;

            private void RaiseItemDeleted(Object item)
            {
                if (ItemDeleted != null)
                    ItemDeleted(this, new EventArgs<Object>(item));
            }

            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Constructor
            public DeletePlanogramGroupItemsWork(Dictionary<PlanogramGroup, List<PlanogramInfo>> deleteList)
            {
                _deletionList = deleteList;

                _groupQueue = new Queue<PlanogramGroup>(deleteList.Keys);
                _planQueue = new Queue<PlanogramInfo>(deleteList.First().Value);
                _currentGroupDeleteNo = 1;
                _deleteFailedList = new List<PlanogramGroup>();
            }
            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowDeterminateBusy(
                   String.Format(CultureInfo.CurrentCulture,
                                Message.PlanogramRepository_DeletePlanogramGroup_Header, _deletionList.Keys.Last().Name),
                   String.Format(CultureInfo.CurrentCulture,
                        Message.PlanogramRepository_DeletePlans_Desc, _currentGroupDeleteNo, _groupQueue.Count, _deletionList.Keys.First().Name),
                               cancelEventHandler);
            }

            public object GetWorkerArgs()
            {
                return new WorkArgs()
                {
                    DeletionList = _deletionList,
                    UserId = DomainPrincipal.CurrentUserId
                };
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                if (_deletionList == null || _deletionList.Count == 0) return;

                WorkArgs args = (WorkArgs)e.Argument;
                var deleteList = args.DeletionList;

                //determine the percentage per item
                Double itemPercent = 100 / (Double)(deleteList.Sum(d => d.Value.Count) + deleteList.Count);
                Double curPercent = 0;

                Queue<PlanogramGroup> groupQueue = new Queue<PlanogramGroup>(deleteList.Keys);

                while (!sender.CancellationPending && groupQueue.Count > 0)
                {
                    PlanogramGroup currentGroup = groupQueue.Dequeue();
                    Queue<PlanogramInfo> groupPlans = new Queue<PlanogramInfo>(deleteList[currentGroup]);
                    Boolean hasLockedPlans = false;

                    //delete each child planogram.
                    while (!sender.CancellationPending && groupPlans.Count > 0)
                    {
                        PlanogramInfo currentPlan = groupPlans.Dequeue();

                        try
                        {
                            Package.DeletePackageById(currentPlan.Id, args.UserId, PackageLockType.System);
                        }
                        catch (DataPortalException)
                        {
                            hasLockedPlans = true;
                        }

                        //report progress
                        curPercent += itemPercent;
                        sender.ReportProgress((Int32)curPercent);
                    }

                    //delete the group itself.
                    //need to check again that there are no child plans and no child groups before deleting.
                    Boolean canDelete = !hasLockedPlans || PlanogramInfoList.FetchByPlanogramGroupId(currentGroup.Id).Count == 0;
                    if (canDelete) canDelete = currentGroup.ParentHierarchy != null;
                    if (canDelete)
                    {
                        PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchById(currentGroup.ParentHierarchy.Id);
                        PlanogramGroup group = hierarchy.EnumerateAllGroups().FirstOrDefault(g => g.Id == currentGroup.Id);
                        canDelete = (group != null) && group.ChildList.Count == 0;
                    }

                    if (canDelete)
                    {
                        PlanogramGroup.DeletePlanogramGroup(currentGroup.Id);
                    }

                    //report progress
                    curPercent += itemPercent;
                    sender.ReportProgress((Int32)curPercent);
                }
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                if (_planQueue.Count == 0)
                {
                    //group deleted
                    PlanogramGroup lastGroup = _groupQueue.Dequeue();
                    PlanogramGroup nextGroup = (_groupQueue.Count > 0) ? _groupQueue.Peek() : null;

                    _currentGroupDeleteNo++;

                    //set up the next plan queue
                    if (nextGroup != null)
                    {
                        _planQueue = new Queue<PlanogramInfo>(_deletionList[nextGroup]);

                    }

                    //update the modal busy with the new percentage
                    if (nextGroup != null)
                    {
                        CommonHelper.GetWindowService().UpdateBusy
                            (
                                String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlans_Desc,
                                    _currentGroupDeleteNo, _deletionList.Keys.Count, nextGroup.Name),

                                e.ProgressPercentage
                            );
                    }
                    else
                    {
                        CommonHelper.GetWindowService().UpdateBusy(String.Empty, 100);
                    }


                    RaiseItemDeleted(lastGroup);
                }
                else
                {
                    PlanogramInfo lastPlan = _planQueue.Dequeue();
                    PlanogramGroup currentGroup = (_groupQueue.Count > 0) ? _groupQueue.Peek() : null;

                    //update the modal busy with the new percentage
                    CommonHelper.GetWindowService().UpdateBusy(e.ProgressPercentage);


                    RaiseItemDeleted(lastPlan);
                }
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    String nextGroupName = (_groupQueue.Count > 0) ? _groupQueue.Peek().Name : null;

                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(nextGroupName, OperationType.Delete);
                }
            }


            #endregion
        }

        #endregion

        #region Move Planogram Group

        /// <summary>
        /// Moves the planogram group to be a child of the given parent.
        /// </summary>
        public static void MovePlanogramGroup(PlanogramGroup groupToMove, PlanogramGroup newParent)
        {
            //if already belongs to same parent don't bother.
            if (groupToMove == null || newParent == null) return;
            if (groupToMove.ParentGroup == newParent) return;

            //check to see if we are allowed to delete planograms (which also covers moving them)
            if (!ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString()))
            {
                Dictionary<PlanogramGroup, List<PlanogramInfo>> moveList = PlanogramRepositoryUIHelper.BuildPlanogramGroupMoveList(groupToMove);

                //if we arn't allowed to delete planograms and there are planograms
                //in the delete list then don't delete
                if (moveList.Any(p => p.Value.Any()))
                {
                    // no delete can occur so display ok message box and always return false for this case. (all plans are locked)
                    CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Warning,
                        String.Format(CultureInfo.CurrentCulture, Message.PlanogramHierarchySelector_MoveGroup_WarningTitle, groupToMove.Name),
                        String.Format(Message.PlanogramHierarchySelector_MoveGroup_WarningDescriptionNoPlanogramDeletePermission, moveList.Sum(p => p.Value.Count)));

                    return;
                }
            }

            //check if a child group with the same name already exists
            if (newParent.ChildList.Any(g => g.Name == groupToMove.Name))
            {
                CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Error,
                    Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Title,
                    String.Format(Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Description, groupToMove.Name, newParent.Name));
                return;
            }

            //confirm action with user
            ModalMessageResult confirmResult =
                CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                Message.PlanogramHierarchySelector_MoveGroup_WarningTitle,
                String.Format(Message.PlanogramHierarchySelector_MoveGroup_WarningDescription, groupToMove.Name, newParent.Name),
                Message.Generic_Continue, Message.Generic_Cancel);
            if (confirmResult != ModalMessageResult.Button1) return;

            //move the group.
            try
            {
                PlanogramGroup.UpdatePlanogramGroup(groupToMove.Id, groupToMove.ParentHierarchy.Id, newParent.Id, groupToMove.Name, groupToMove.ProductGroupId);
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(groupToMove.Name, OperationType.Save);
            }
        }

        #endregion

        #endregion

        #region Planogram Methods

        /// <summary>
        /// Shows a dialog confirming the plan overwrite with the user.
        /// </summary>
        public static Boolean ConfirmPlanogramOverwriteWithUser(String planogramName, String planogramGroupName)
        {
            ModalMessageResult result = 
                CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                      Message.PlanRepository_CopyPlanograms_DuplicateHeader,
                       String.Format(CultureInfo.CurrentCulture,
                       Message.PlanRepository_CopyPlanograms_DuplicateDesc,
                       planogramName, planogramGroupName),
                       Message.PlanRepository_CopyPlanograms_DuplicateOverwrite,
                       Message.Generic_Cancel);

            return (result == ModalMessageResult.Button1);
        }

        #region Planogram Deletion

        ///// <summary>
        ///// Returns a friendly error string if the user cannot delete the given planograms.
        ///// </summary>
        //public static String CanUserDeletePlanograms(IEnumerable<PlanogramInfo> plansToDelete)
        //{
        //    //  Must have planogram delete permission.
        //    if (!ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString()))
        //    {
        //        return Message.Generic_NoDeletePermission;
        //    }

        //    // Must have selected planograms.
        //    if (plansToDelete.Count() == 0)
        //    {
        //        return Message.PlanRepository_DeletePlanogram_DisabledReason_NoPlanogramSelected;
        //    }

        //    // Selected plan must not be locked.
        //    if (plansToDelete.Any(r => r.IsLocked))
        //    {
        //        return Message.PlanoRepository_DeletePlanogram_DisbaledReason_LockedPlans;
        //    }

        //    return null;
        //}

        /// <summary>
        /// Carries out actions to handle a user initiated request to delete a list of plans,
        /// inc. confirming the delete with the user and carrying it out.
        /// </summary>
        /// <param name="plansToDelete">the list of plans to delete</param>
        /// <param name="itemDeletedHandler">handler to call each time a single plan delete is completed.</param>
        public static void DeletePlanograms(IEnumerable<PlanogramInfo> plansToDelete, EventHandler<EventArgs<PlanogramInfo>> itemDeletedHandler)
        {

            //Prompt the user with a dialog that includes information about the consequences of deleting planograms assigned to store if any
            PlanogramDeletionDialogViewModel editorViewModel = new PlanogramDeletionDialogViewModel(plansToDelete);
            CommonHelper.GetWindowService().ShowDialog<PlanogramDeletionDialogWindow>(editorViewModel);
            if (editorViewModel.DialogResult == false) return;
            plansToDelete = editorViewModel.PlansToDelete;

            //execute the deletion
            PlanogramDeleteWork work = new PlanogramDeleteWork(plansToDelete);
            work.ItemDeleted += itemDeletedHandler;
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            work.ItemDeleted -= itemDeletedHandler;

        }

        private sealed class PlanogramDeleteWork : IModalBusyWork
        {
            #region Fields
            private List<PlanogramInfo> _planogramsToBeDeleted; //the full list of plans to be deleted.
            private Queue<PlanogramInfo> _deletionQueue; //the queue of plans left to delete.
            private Int32 _currentPlanDeleteNo; //the current plan number.
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Events

            public event EventHandler<EventArgs<PlanogramInfo>> ItemDeleted;

            private void RaiseItemDeleted(PlanogramInfo item)
            {
                if (ItemDeleted != null)
                    ItemDeleted(this, new EventArgs<PlanogramInfo>(item));
            }

            #endregion

            #region Constructor

            public PlanogramDeleteWork(IEnumerable<PlanogramInfo> plansToDelete)
            {
                //Initialise references
                _planogramsToBeDeleted = plansToDelete.ToList();
                _deletionQueue = new Queue<PlanogramInfo>(_planogramsToBeDeleted);
                _currentPlanDeleteNo = 1;
            }

            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    Message.PlanogramRepository_DeletePlans_Header,
                    String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlans_Desc,
                                _currentPlanDeleteNo, _planogramsToBeDeleted.Count, _planogramsToBeDeleted.First().Name),
                                cancelEventHandler);
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                Int32? userId = DomainPrincipal.CurrentUserId;
                Queue<Int32> planIds = new Queue<Int32>(_planogramsToBeDeleted.Select(p => p.Id));

                if (planIds.Count == 0) return;

                //determine the percentage per plan
                Double planPercent = 100 / (Double)planIds.Count;
                Double curPercent = 0;

                while (!sender.CancellationPending && planIds.Count > 0)
                {
                    Int32 nextPlanId = planIds.Dequeue();

                    //delete the package.
                    Package.DeletePackageById(nextPlanId, userId, PackageLockType.System);

                    //report progress
                    curPercent += planPercent;
                    sender.ReportProgress((Int32)curPercent);
                }
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                PlanogramInfo lastPlan = _deletionQueue.Dequeue();
                PlanogramInfo nextPlan = (_deletionQueue.Count > 0) ? _deletionQueue.Peek() : null;

                _currentPlanDeleteNo++;


                //update the modal busy with the new percentage
                if (nextPlan != null)
                {
                    CommonHelper.GetWindowService().UpdateBusy(
                        Message.PlanogramRepository_DeletePlans_Header,
                        String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlans_Desc,
                        _currentPlanDeleteNo, _planogramsToBeDeleted.Count, nextPlan.Name),
                        e.ProgressPercentage);
                }
                else
                {
                    CommonHelper.GetWindowService().UpdateBusy(Message.PlanogramRepository_DeletePlans_Header, String.Empty, 100);
                }


                //Raise the progress changed event
                RaiseItemDeleted(lastPlan);
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                PlanogramInfo nextPlan = (_deletionQueue.Count > 0) ? _deletionQueue.Peek() : null;
                _deletionQueue = null;

                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(nextPlan.Name, OperationType.Delete);
                }
            }

            #endregion


            public object GetWorkerArgs()
            {
                return null;
            }
        }

        #endregion

        #region Planogram Move

        public static void MovePlanogramsToGroup(IEnumerable<PlanogramInfo> plansToMove, PlanogramGroup targetGroup)
        {
            //just execute - no need to confirm with the user.
            MovePlansToGroupWork work = new MovePlansToGroupWork(plansToMove, targetGroup);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
        }

        /// <summary>
        /// Work class which copies the given planograms to a another planogram group
        /// Checking for overwrites as it goes.
        /// </summary>
        private sealed class MovePlansToGroupWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkerArgs
            {
                public List<PlanCopyRequest> ItemsToCopy { get; set; }
                public Int32? UserId { get; set; }
                public Int32 TargetGroupId { get; set; }
                public String TargetGroupName { get; set; }
                public Int32 TargetEntityId { get; set; }
            }

            private sealed class WorkerProgressArgs
            {
                public Int32 RequestNo { get; set; }
                public String PlanName { get; set; }
                public Boolean IsExistingLocked { get; set; }
                public PlanCopyDuplicateAction Action { get; set; }
                public Boolean IsComplete { get; set; }

                public Boolean ApplyActionToAllRemaining { get; set; }

                public String NextPlanName { get; set; }
            }

            private enum PlanCopyDuplicateAction
            {
                None = 0,
                Overwrite = 1,
                CopyAndRename = 2,
                Ignore = 3,
                Cancel = 4
            }

            private sealed class PlanCopyRequest
            {
                public Int32 PlanId { get; set; }
                public String PlanName { get; set; }
                public Int32 TargetGroupId { get; set; }
                public PlanCopyDuplicateAction Action { get; set; }

                public PlanCopyRequest() { }
            }
            #endregion

            #region Fields
            private Int32 _targetGroupId;
            private String _targetGroupName;
            private Int32 _targetEntityId;
            private List<PlanCopyRequest> _copyItems;
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }


            #endregion

            #region Constructor

            public MovePlansToGroupWork(IEnumerable<PlanogramInfo> planInfos, PlanogramGroup targetGroup)
            {
                _targetGroupId = targetGroup.Id;
                _targetGroupName = targetGroup.Name;
                _targetEntityId = targetGroup.ParentHierarchy.EntityId;

                //create the copy requests
                _copyItems = new List<PlanCopyRequest>(planInfos.Count());
                foreach (PlanogramInfo p in planInfos)
                {
                    _copyItems.Add(
                        new PlanCopyRequest()
                        {
                            PlanId = p.Id,
                            PlanName = p.Name,
                            TargetGroupId = targetGroup.Id
                        });
                }
            }

            #endregion

            #region Methods

            public Object GetWorkerArgs()
            {
                return new WorkerArgs()
                {
                    ItemsToCopy = _copyItems,
                    UserId = DomainPrincipal.CurrentUserId,
                    TargetGroupId = _targetGroupId,
                    TargetGroupName = _targetGroupName,
                    TargetEntityId = _targetEntityId,
                };
            }

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                PlanCopyRequest firstReq = _copyItems.First();

                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    Message.PlanRepository_MovePlanograms_BusyHeader,
                    String.Format(CultureInfo.CurrentCulture, "{0}/{1} - {2}", 1, _copyItems.Count, firstReq.PlanName),
                    cancelEventHandler);
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                WorkerArgs args = (WorkerArgs)e.Argument;
                Int32? userId = args.UserId;
                Int32 groupId = args.TargetGroupId;
                Int32 entityId = args.TargetEntityId;
                Queue<PlanCopyRequest> copyItems = new Queue<PlanCopyRequest>(args.ItemsToCopy);

                PlanogramInfoList plansInTarget = PlanogramInfoList.FetchByPlanogramGroupId(groupId);

                Double progPerItem = 100 / (Double)copyItems.Count;
                Double currentProgress = 0;


                //cycle through the items to be copied.
                Int32 itemNo = 1;
                while (copyItems.Count > 0)
                {
                    if (sender.CancellationPending) return;

                    PlanCopyRequest request = copyItems.Dequeue();

                    WorkerProgressArgs progressArgs = new WorkerProgressArgs()
                    {
                        PlanName = request.PlanName,
                        RequestNo = itemNo,
                        Action = PlanCopyDuplicateAction.None
                    };

                    Int32? movedId = null;
                    PlanCopyDuplicateAction duplicateAction = PlanCopyDuplicateAction.None;

                    //check if a plan with the same name already exists in the group.
                    PlanogramInfo existingPlan = plansInTarget.FirstOrDefault(p => String.Compare(p.Name, request.PlanName, true) == 0);
                    if (existingPlan != null)
                    {
                        //if the plan is moving to the same place dont bother
                        if (existingPlan.Id == request.PlanId)
                        {
                            duplicateAction = PlanCopyDuplicateAction.Ignore;
                        }
                        else
                        {
                            progressArgs.IsExistingLocked = existingPlan.IsLocked;
                            duplicateAction = request.Action;

                            //if action is overwrite but plan is locked - rename.
                            if (duplicateAction == PlanCopyDuplicateAction.Overwrite && existingPlan.IsLocked)
                            {
                                duplicateAction = PlanCopyDuplicateAction.CopyAndRename;
                            }

                            //if no action set, ask user
                            if (duplicateAction == PlanCopyDuplicateAction.None)
                            {
                                sender.ReportProgress((Int32)currentProgress, progressArgs);
                                while (progressArgs.Action == PlanCopyDuplicateAction.None)
                                {
                                    //force this to wait for a reply.
                                    Thread.Sleep(50);
                                }
                                duplicateAction = progressArgs.Action;

                                //if apply to all was set then default the remaining items.
                                if (progressArgs.ApplyActionToAllRemaining)
                                {
                                    foreach (var r in copyItems)
                                    {
                                        r.Action = duplicateAction;
                                    }
                                }
                            }
                        }
                    }


                    switch (duplicateAction)
                    {
                        case PlanCopyDuplicateAction.Cancel:
                            e.Cancel = true;
                            return;

                        case PlanCopyDuplicateAction.Ignore:
                            break;

                        case PlanCopyDuplicateAction.Overwrite:
                            {
                                movedId = request.PlanId;

                                //just try to delete the plan to be overwritten
                                Boolean deleteFailed = false;
                                try
                                {
                                    Package.DeletePackageById(existingPlan.Id, userId, PackageLockType.User);
                                }
                                catch (DataPortalException)
                                {
                                    deleteFailed = true;
                                }

                                if (deleteFailed)
                                {
                                    //could not delete the plan to be overwritten so rename this one.
                                    movedId = RenamePackage(request.PlanId, request.PlanName, userId, plansInTarget, entityId);
                                }

                                //associate the copy with the target group.
                                PlanogramGroup.AssociatePlanograms(groupId, new List<Int32> { movedId.Value });
                            }
                            break;

                        case PlanCopyDuplicateAction.CopyAndRename:
                            movedId = RenamePackage(request.PlanId, request.PlanName, userId, plansInTarget, entityId);
                            PlanogramGroup.AssociatePlanograms(groupId, new List<Int32> { movedId.Value });
                            break;

                        case PlanCopyDuplicateAction.None:
                            PlanogramGroup.AssociatePlanograms(groupId, new List<Int32> { request.PlanId });
                            break;
                    }


                    //send progress update
                    progressArgs.NextPlanName = (copyItems.Count > 0) ? copyItems.Peek().PlanName : null;
                    progressArgs.IsComplete = true;
                    currentProgress += progPerItem;
                    sender.ReportProgress((Int32)currentProgress, progressArgs);
                    itemNo++;
                }
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                WorkerProgressArgs args = (WorkerProgressArgs)e.UserState;


                //if the current request is complete then just update progress.
                if (args.IsComplete)
                {
                    CommonHelper.GetWindowService().UpdateBusy(
                            String.Format(CultureInfo.CurrentCulture, "{0}/{1} - {2}",
                            args.RequestNo + 1, _copyItems.Count, args.NextPlanName),
                             e.ProgressPercentage);
                }
                else
                {
                    //the report progress requires input from the user.
                    Boolean applyToAll = true;

                    ModalMessageResult result = CommonHelper.GetWindowService()
                        .ShowMessage(MessageWindowType.Warning,
                       Message.PlanRepository_CopyPlanograms_DuplicateHeader,
                        String.Format(CultureInfo.CurrentCulture,
                        Message.PlanRepository_CopyPlanograms_DuplicateDescNoTarget,
                        args.PlanName),
                        3,
                        Message.PlanRepository_CopyPlanograms_DuplicateRename,
                        Message.PlanRepository_CopyPlanograms_DuplicateOverwrite,
                        Message.Generic_Cancel,
                        Message.PlanRepository_CopyPlanograms_DuplicateApplyToAll, out applyToAll);

                    PlanCopyDuplicateAction action = PlanCopyDuplicateAction.CopyAndRename;
                    switch (result)
                    {
                        case ModalMessageResult.Button1:
                            action = PlanCopyDuplicateAction.CopyAndRename;
                            break;

                        case ModalMessageResult.Button2:
                            action = PlanCopyDuplicateAction.Overwrite;
                            break;


                        case ModalMessageResult.Button3:
                        case ModalMessageResult.CrossClicked:
                            action = PlanCopyDuplicateAction.Cancel;
                            break;
                    }


                    //set the reponse.
                    if (applyToAll) args.ApplyActionToAllRemaining = true;
                    args.Action = action;
                }
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {

                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorMessage(Message.PlanRepository_MovePlanograms_BusyHeader, Message.PlanRepository_MovePlanograms_Error);
                }
            }

            private static Int32 RenamePackage(Int32 planId, String planName, Int32? userId, PlanogramInfoList plansInTarget, Int32 entityId)
            {
                //get initial copy name
                String newName = GetPlanCopyName(planName, plansInTarget);

                Int32 copyId = planId;
                Boolean renameFailed = false;
                try
                {
                    using (Package pk = Package.FetchById(planId, userId, PackageLockType.System))
                    {
                        pk.Name = newName;
                        if (pk.Planograms.Count > 0)
                        {
                            pk.Planograms[0].Name = newName;
                        }
                        pk.Save();
                    }
                }
                catch (DataPortalException)
                {
                    renameFailed = true;
                }

                if (renameFailed)
                {
                    //take a copy instead.
                    copyId = CopyPackage(planId, userId, newName, entityId, String.Empty, String.Empty, false);
                }
                return copyId;
            }

            #endregion
        }

        #endregion

        #region Planogram Copy

        public static void CopyPlanogramsToGroup(IEnumerable<PlanogramInfo> plansToCopy, PlanogramGroup targetGroup)
        {
            //just execute - no need to confirm with the user.
            CopyPlansToGroupWork work = new CopyPlansToGroupWork(plansToCopy, targetGroup);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
        }

        /// <summary>
        /// Work class which copies the given planograms to a another planogram group
        /// Checking for overwrites as it goes.
        /// </summary>
        private sealed class CopyPlansToGroupWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkerArgs
            {
                public List<PlanCopyRequest> ItemsToCopy { get; set; }
                public Int32? UserId { get; set; }
                public Int32 TargetGroupId { get; set; }
                public String TargetGroupName { get; set; }
                public Int32 EntityId { get; set; }
            }

            private sealed class WorkerProgressArgs
            {
                public Int32 RequestNo { get; set; }
                public String PlanName { get; set; }
                public Boolean IsExistingLocked { get; set; }
                public PlanCopyDuplicateAction Action { get; set; }
                public Boolean IsComplete { get; set; }

                public Boolean ApplyActionToAllRemaining { get; set; }

                public String NextPlanName { get; set; }
            }


            private enum PlanCopyDuplicateAction
            {
                None = 0,
                Overwrite = 1,
                CopyAndRename = 2,
                Ignore = 3,
                Cancel = 4
            }

            private sealed class PlanCopyRequest
            {
                public Int32 PlanId { get; set; }
                public String PlanName { get; set; }
                public Int32 TargetGroupId { get; set; }
                public PlanCopyDuplicateAction Action { get; set; }

                public PlanCopyRequest() { }
            }
            #endregion

            #region Fields
            private Int32 _targetGroupId;
            private String _targetGroupName;
            private List<PlanCopyRequest> _copyItems;
            private Int32 _targetentityId;
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }


            #endregion

            #region Constructor

            public CopyPlansToGroupWork(IEnumerable<PlanogramInfo> planInfos, PlanogramGroup targetGroup)
            {
                _targetGroupId = targetGroup.Id;
                _targetGroupName = targetGroup.Name;
                _targetentityId = targetGroup.ParentHierarchy.EntityId;

                //create the copy requests
                _copyItems = new List<PlanCopyRequest>(planInfos.Count());
                foreach (PlanogramInfo p in planInfos)
                {
                    _copyItems.Add(
                        new PlanCopyRequest()
                        {
                            PlanId = p.Id,
                            PlanName = p.Name,
                            TargetGroupId = targetGroup.Id
                        });
                }
            }

            #endregion

            #region Methods

            public Object GetWorkerArgs()
            {
                return new WorkerArgs()
                {
                    ItemsToCopy = _copyItems,
                    UserId = DomainPrincipal.CurrentUserId,
                    TargetGroupId = _targetGroupId,
                    TargetGroupName = _targetGroupName,
                    EntityId = _targetentityId,
                };
            }

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                PlanCopyRequest firstReq = _copyItems.First();

                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    String.Format(CultureInfo.CurrentCulture, Message.PlanRepository_CopyPlanograms_BusyHeader, _targetGroupName),
                    String.Format(CultureInfo.CurrentCulture, "{0}/{1} - {2}", 1, _copyItems.Count, firstReq.PlanName),
                    cancelEventHandler);
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                WorkerArgs args = (WorkerArgs)e.Argument;
                Int32? userId = args.UserId;
                Int32 groupId = args.TargetGroupId;
                Int32 entityId = args.EntityId;
                Queue<PlanCopyRequest> copyItems = new Queue<PlanCopyRequest>(args.ItemsToCopy);

                PlanogramInfoList plansInTarget = PlanogramInfoList.FetchByPlanogramGroupId(groupId);

                Double progPerItem = 100 / (Double)copyItems.Count;
                Double currentProgress = 0;

                List<PlanCopyRequest> failedItems = new List<PlanCopyRequest>();

                //cycle through the items to be copied.
                Int32 itemNo = 1;
                while (copyItems.Count > 0)
                {
                    if (sender.CancellationPending) return;

                    PlanCopyRequest request = copyItems.Dequeue();

                    WorkerProgressArgs progressArgs = new WorkerProgressArgs()
                    {
                        PlanName = request.PlanName,
                        RequestNo = itemNo,
                        Action = PlanCopyDuplicateAction.None
                    };

                    Int32? copyId = null;
                    PlanCopyDuplicateAction duplicateAction = PlanCopyDuplicateAction.None;

                    //check if a plan with the same name already exists in the group.
                    PlanogramInfo existingPlan = plansInTarget.FirstOrDefault(p => String.Compare(p.Name, request.PlanName, true) == 0);
                    if (existingPlan != null)
                    {
                        progressArgs.IsExistingLocked = existingPlan.IsLocked;
                        duplicateAction = request.Action;

                        //if action is overwrite but plan is locked - rename.
                        if (duplicateAction == PlanCopyDuplicateAction.Overwrite && existingPlan.IsLocked)
                        {
                            duplicateAction = PlanCopyDuplicateAction.CopyAndRename;
                        }

                        //if no action set, ask user
                        if (duplicateAction == PlanCopyDuplicateAction.None)
                        {
                            sender.ReportProgress((Int32)currentProgress, progressArgs);
                            while (progressArgs.Action == PlanCopyDuplicateAction.None)
                            {
                                //force this to wait for a reply.
                                Thread.Sleep(50);
                            }
                            duplicateAction = progressArgs.Action;

                            //if apply to all was set then default the remaining items.
                            if (progressArgs.ApplyActionToAllRemaining)
                            {
                                foreach (var r in copyItems)
                                {
                                    r.Action = duplicateAction;
                                }
                            }
                        }
                    }

                    try
                    {
                        switch (duplicateAction)
                        {
                            case PlanCopyDuplicateAction.Cancel:
                                e.Cancel = true;
                                return;

                            case PlanCopyDuplicateAction.Ignore:
                                break;

                            case PlanCopyDuplicateAction.Overwrite:
                                copyId = OverwritePackage(existingPlan, request.PlanId, request.PlanName, userId, plansInTarget, entityId);
                                break;

                            case PlanCopyDuplicateAction.CopyAndRename:
                                String copyName = GetPlanCopyName(request.PlanName, plansInTarget);
                                copyId = CopyPackage(request.PlanId, userId, copyName, entityId, String.Empty, String.Empty, false);
                                break;

                            case PlanCopyDuplicateAction.None:
                                copyId = CopyPackage(request.PlanId, userId, request.PlanName, entityId, String.Empty, String.Empty, false);
                                break;
                        }
                    }
                    catch(Exception ex)
                    {
                        CommonHelper.RecordException(ex);

                        //if the item has not failed before then queue it to be retried.
                        if (!failedItems.Contains(request))
                        {
                            failedItems.Add(request);
                            copyItems.Enqueue(request);//readd to the end of the queue to try again.

                            //update progress
                            progressArgs.NextPlanName = (copyItems.Count > 0) ? copyItems.Peek().PlanName : null;
                            progressArgs.IsComplete = true;
                            sender.ReportProgress((Int32)currentProgress, progressArgs); //resend the same progress
                        }
                        else
                        {
                            //update progress but do not requeue as we have retried this already.
                            progressArgs.NextPlanName = (copyItems.Count > 0) ? copyItems.Peek().PlanName : null;
                            progressArgs.IsComplete = true;
                            currentProgress += progPerItem;
                            sender.ReportProgress((Int32)currentProgress, progressArgs); //resend the same progress
                            itemNo++;
                        }
                        continue;
                    }


                    if (copyId.HasValue)
                    {
                        //associate the copy with the target group.
                        PlanogramGroup.AssociatePlanograms(groupId, new List<Int32> { copyId.Value });
                    }

                    //remove the item from the failed list if it is a retry
                    if (failedItems.Contains(request)) failedItems.Remove(request);

                    //send progress update
                    progressArgs.NextPlanName = (copyItems.Count > 0) ? copyItems.Peek().PlanName : null;
                    progressArgs.IsComplete = true;
                    currentProgress += progPerItem;
                    sender.ReportProgress((Int32)currentProgress, progressArgs);
                    itemNo++;
                }

                //if we still have failed items then throw a neat error message.
                if(failedItems.Any())
                {
                    throw new ArgumentException(String.Format(Message.PlanRepository_PlansFailedToCopy, failedItems.Count));
                }
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                WorkerProgressArgs args = (WorkerProgressArgs)e.UserState;


                //if the current request is complete then just update progress.
                if (args.IsComplete)
                {
                    CommonHelper.GetWindowService().UpdateBusy(
                            String.Format(CultureInfo.CurrentCulture, "{0}/{1} - {2}",
                            args.RequestNo + 1, _copyItems.Count, args.NextPlanName),
                             e.ProgressPercentage);
                }
                else
                {
                    //the report progress requires input from the user.
                    Boolean applyToAll = true;

                    ModalMessageResult result = CommonHelper.GetWindowService()
                        .ShowMessage(
                        MessageWindowType.Warning,
                        Message.PlanRepository_CopyPlanograms_DuplicateHeader,
                        String.Format(CultureInfo.CurrentCulture, Message.PlanRepository_CopyPlanograms_DuplicateDescNoTarget, args.PlanName),
                        3,
                        Message.PlanRepository_CopyPlanograms_DuplicateRename,
                        Message.PlanRepository_CopyPlanograms_DuplicateOverwrite,
                        Message.Generic_Cancel,
                        Message.PlanRepository_CopyPlanograms_DuplicateApplyToAll, out applyToAll);

                    PlanCopyDuplicateAction action = PlanCopyDuplicateAction.CopyAndRename;
                    switch (result)
                    {
                        case ModalMessageResult.Button1:
                            action = PlanCopyDuplicateAction.CopyAndRename;
                            break;

                        case ModalMessageResult.Button2:
                            action = PlanCopyDuplicateAction.Overwrite;
                            break;


                        case ModalMessageResult.Button3:
                        case ModalMessageResult.CrossClicked:
                            action = PlanCopyDuplicateAction.Cancel;
                            break;
                    }


                    //set the reponse.
                    if (applyToAll) args.ApplyActionToAllRemaining = true;
                    args.Action = action;
                }
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorMessage("Error", e.Error.Message);
                }
            }

            private static Int32? OverwritePackage(PlanogramInfo planToOverwrite, Int32 planId, String planName, Int32? userId, PlanogramInfoList plansInTarget, Int32 entityId)
            {
                return PackageHelper.OverwritePackage(planToOverwrite, planId, planName, userId, plansInTarget, entityId);
            }

            #endregion
        }

        private static String GetPlanCopyName(String currentName, PlanogramInfoList plansInTarget)
        {
            return PackageHelper.GetPlanCopyName(currentName, plansInTarget);
        }

        private static Int32 CopyPackage(Int32 packageToCopyId, Int32? userId, String copyName, Int32 entityId, String catName, String catCode, Boolean shouldCopyCatgeory = false)
        {
            return PackageHelper.CopyPackage(packageToCopyId, userId, copyName, entityId, false, catName, catCode, false);
        }

        #endregion

        #region Planogram Rename

        public static void RenamePlanogram(PlanogramInfo plan)
        {
            String newPlanName = plan.Name;

            CommonHelper.GetWindowService().ShowWaitCursor();
            PlanogramInfoList existingPlans;
            try
            {
                existingPlans = PlanogramInfoList.FetchByPlanogramGroupId((Int32)plan.PlanogramGroupId);
            }
            catch (Exception ex)
            {
                CommonHelper.GetWindowService().HideWaitCursor();
                CommonHelper.GetWindowService().ShowErrorMessage(String.Empty, ex.Message);
                return;
            }
            CommonHelper.GetWindowService().HideWaitCursor();


            Predicate<String> isUniqueCheck =
                new Predicate<String>( (s) =>
                {
                    return !existingPlans.Any(p => String.Compare(p.Name, s) == 0);
                });

            //Prompt the user for a name.
            Boolean continueWithRename =
            CommonHelper.GetWindowService().PromptUserForValue(
                dialogTitle: Message.RenamePlanogram_Title,
                forceFirstShowDescription: Message.RenamePlanogram_Desc,
                notUniqueDescription: Message.RenamePlanogram_DescNotUnique,
                inputLabel: String.Empty,
                forceFirstShow: true,
                isUniqueCheck: isUniqueCheck,
                proposedValue: plan.Name,
                selectedValue: out newPlanName
                );

            if (!continueWithRename) return;


            CommonHelper.GetWindowService().ShowWaitCursor();
            try
            {
                using (Package pk = Package.FetchById(plan.Id, DomainPrincipal.CurrentUserId, PackageLockType.User))
                {
                    pk.Name = newPlanName;
                    pk.Planograms.First().Name = newPlanName;
                    pk.Save(clearMetadata: false, updateDateLastModified: false);
                }
            }
            catch(Exception ex)
            {
                CommonHelper.GetWindowService().HideWaitCursor();
                CommonHelper.GetWindowService().ShowErrorMessage(String.Empty, ex.Message);
                return;
            }
            CommonHelper.GetWindowService().HideWaitCursor();
        } 

        #endregion

        #endregion
    }
}
