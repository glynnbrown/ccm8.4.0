﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-26281 : L.Ineson 
//   Created.
// V8-28337 : A.Silva
//      Added GetParentWindow extension method.

#endregion

#endregion

using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Local = Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Collections.Generic;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Provides window related helpers.
    /// </summary>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [Obsolete("Use the new window service!")]
    public static class WindowHelper
    {

        #region Event

        /// <summary>
        /// Requests that a new window be launched.
        /// </summary>
        [Obsolete("Use window service instead")]
        public static event EventHandler<ShowWindowRequestArgs> ShowWindowRequest;

        #endregion

        #region ShowWindow Methods

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void ShowWindow(Window window, Boolean isModal)
        {
            ProcessShowWindow(
            new ShowWindowRequestArgs()
            {
                Window = window,
                IsModal = isModal
            });
        }

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void ShowWindow(Window window, Window parentWindow, Boolean isModal)
        {
            ProcessShowWindow(
            new ShowWindowRequestArgs()
            {
                Window = window,
                ParentWindow = parentWindow,
                IsModal = isModal
            });
        }

        /// <summary>
        /// Launches the window of the given type with the given parameters.
        /// </summary>
        /// <param name="windowType"></param>
        /// <param name="args"></param>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void ShowWindow(ShowWindowRequestArgs requestArgs)
        {
            ProcessShowWindow(requestArgs);
        }

     
      

        /// <summary>
        /// Launches the modal dialog for the given viewmodel.
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        [Obsolete("Use the new window service!")]
        public static void ShowDialog<T>(this ViewModelAttachedControlObject<T> viewModel)
            where T : Window
        {
            ProcessShowWindow(new ShowWindowRequestArgs(typeof(T), /*isModel*/true, null, new Object[] { viewModel }));
        }

        /// <summary>
        /// Launches the modal dialog for the given viewmodel.
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        [Obsolete("Use the new window service!")]
        public static void ShowDialog<T>(this ViewModelAttachedControlObject<T> viewModel, Window parentWindow)
            where T : Window
        {
            ProcessShowWindow(new ShowWindowRequestArgs(typeof(T), /*isModel*/true, parentWindow, new Object[] { viewModel }));
        }

        #endregion

        /// <summary>
        ///     Gets the <see cref="Window"/> instance that contains this <see cref="DependencyObject"/> instance.
        /// </summary>
        /// <param name="dependencyObject">The instance to get the containing <see cref="Window"/> from.</param>
        /// <returns>The instance of <see cref="Window"/> that contains the <paramref name="dependencyObject"/> or <c>null</c> if there is none.</returns>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static Window GetParentWindow(this DependencyObject dependencyObject)
        {
            return dependencyObject == null
                ? null
                : dependencyObject.FindVisualAncestor<Window>();
        }

        ///// <summary>
        ///// Returns the active window of the current application.
        ///// </summary>
        //[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        //public static Window GetActiveWindow()
        //{
        //    if (Application.Current == null) return null;

        //    foreach (Window win in Application.Current.Windows)
        //    {
        //        if (win.IsActive) return win;
        //    }

        //    return Application.Current.MainWindow;
        //}

        #region Private methods

        private static void ProcessShowWindow(ShowWindowRequestArgs e)
        {
            //Raise out the request
            if (ShowWindowRequest != null)
            {
                ShowWindowRequest(Application.Current, e);
            }

            if (Application.Current != null)
            {
                Window window;
                if (e.Window != null)
                {
                    window = e.Window;
                }
                else
                {
                    //create the window.
                    Type[] paramTypes = null;
                    if (e.Args != null)
                    {
                        paramTypes = e.Args.Select(a => a.GetType()).ToArray();
                    }

                    ConstructorInfo ctr = e.WindowType.GetConstructor(paramTypes);
                    window = (Window)ctr.Invoke(e.Args);
                }

                //determine the parent window.
                Window parent = e.ParentWindow;
                if (parent != null)
                {
                    window.Owner = parent;
                }
                else if (window.Owner == null
                    && window != Application.Current.MainWindow)
                {
                    window.Owner = Application.Current.MainWindow;
                }

                window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                window.Icon = Local.ImageResources.CCMIcon;
                window.ShowInTaskbar = true;
                window.Closed += new EventHandler(ChildWindow_Closed);

                //show it.
                if (e.IsModal)
                {
                    window.ShowDialog();
                }
                else
                {
                    window.Show();
                }
            }
        }

        /// <summary>
        /// Cleans up when a child window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ChildWindow_Closed(object sender, EventArgs e)
        {
            //Activates the owning window after its child has closed
            //to fix the issue of it hiding.

            Window senderControl = (Window)sender;
            senderControl.Closed -= ChildWindow_Closed;

            Dispatcher.CurrentDispatcher.BeginInvoke(
                (Action)(() =>
                {
                    senderControl.Owner.Activate();

                }), priority: System.Windows.Threading.DispatcherPriority.Loaded);
        }

        #endregion

        #region Nested classes

        /// <summary>
        /// Event args used to launch a new window.
        /// </summary>
        public class ShowWindowRequestArgs : EventArgs
        {
            #region Fields

            private Window _window;

            private Window _parentWindow;
            private Boolean _isModal;

            private Type _windowType;
            private Object[] _args;

            #endregion

            #region Properties

            /// <summary>
            /// TEMP -
            /// Gets/Sets the actual window to show.
            /// </summary>
            public Window Window
            {
                get { return _window; }
                set { _window = value; }
            }

            /// <summary>
            /// Gets/Sets the window type.
            /// </summary>
            public Type WindowType
            {
                get { return _windowType; }
                set { _windowType = value; }
            }

            /// <summary>
            /// Gets/Sets the constructor paramters to use when
            /// creating the window.
            /// </summary>
            public Object[] Args
            {
                get { return _args; }
                set { _args = value; }
            }

            /// <summary>
            /// Gets/Sets the parent window.
            /// </summary>
            public Window ParentWindow
            {
                get { return _parentWindow; }
                set { _parentWindow = value; }
            }

            /// <summary>
            /// Gets/Sets whether the window should be launched 
            /// as modal.
            /// </summary>
            public Boolean IsModal
            {
                get { return _isModal; }
                set { _isModal = value; }
            }



            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            public ShowWindowRequestArgs() { }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="windowType"></param>
            /// <param name="args"></param>
            public ShowWindowRequestArgs(Type windowType, Boolean isModel, Window parent, Object[] args)
            {
                this.WindowType = windowType;
                this.IsModal = isModel;
                this.ParentWindow = parent;
                this.Args = args;
            }


            #endregion
        }
        #endregion
    }


   

    

    
}
