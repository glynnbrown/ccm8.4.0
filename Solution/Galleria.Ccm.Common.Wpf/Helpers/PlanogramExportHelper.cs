﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32810 : L.Ineson
//  Created
// V8-32810 : M.Pettit
//  Now saves selected print template to the related ContentLookup for each plnaogram before exporting
//  Added Override values to PdfExportItem for highlights, labels and show real images
// V8-33002 : M.Pettit
//  Handles situation where CreateShowExportWindowCommand method planograms collection parameter can contain nulls
// V8-33001 : M.Pettit
//  ExportPlanogramsToPdf now correctly handles file and repository based planograms when called from Space Planning or Automation
//  (When called from Space Planning, the Planogram property is set, when called from Automation the PlanofgramInfo property is set)
// V8-32996 : M.Pettit
//  Plan counts now correct if user cancels part-way through processing multiple plans
// CCM-14039 : M.Pettit
//  Planograms that were referenced more than once but has content lookups that did not previously exist now save/update the content lookup correctly
// CCM-14014 : M.Pettit
//  When exporting the plan, file Print templates are now fetched as readonly
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using System.Threading;
using Galleria.Framework.Planograms.Rendering;

namespace Galleria.Ccm.Common.Wpf.Helpers
{
    /// <summary>
    /// Static class providing helper methods for exporting planograms.
    /// </summary>
    public static class PlanogramExportHelper
    {
        /// <summary>
        /// Exports the planograms as pdfs to the output directory.
        /// </summary>
        /// <param name="exportItems"></param>
        /// <param name="printTemplate"></param>
        /// <param name="outputDir"></param>
        public static void ExportPlanogramsToPdf(List<PdfExportItem> exportItems, String outputDir)
        {
            //first check that the output directory does actually exist
            if (!Directory.Exists(outputDir))
            {
                CommonHelper.GetWindowService().ShowErrorMessage(null,
                    String.Format(CultureInfo.CurrentCulture, Message.PrintTemplateUIHelper_ExportPlans_OutputDirDoesNotExist, outputDir));
                return;
            }

            // save selected print templates to the associated content lookup:

            //save any print templates to the associated content lookup.
            //Note: Only saves if the print template is from the repository and the planogram is in the repository
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                try
                {
                    //Get the Repository plans and their content lookups
                    List<PdfExportItem> repositoryExportItems = new List<PdfExportItem>();
                    List<Int32> repositoryPlanIds = new List<Int32>();
                    foreach (PdfExportItem item in exportItems)
                    {
                        Int32? planId = null;
                        if (IsRepositoryPlanogram(item, out planId))
                        {
                            repositoryExportItems.Add(item);
                            repositoryPlanIds.Add((Int32)planId);
                        }
                    }
                    //Fetch all content lookups for the exported planograms in a single fetch
                    ContentLookupList contentLookups = ContentLookupList.FetchByPlanogramIds(repositoryPlanIds);
                    List<ContentLookup> newContentLookups = new List<ContentLookup>();

                    //update the related contentlookup with the new template id
                    foreach (PdfExportItem exportItem in repositoryExportItems)
                    {
                        if (exportItem.TemplateInfo.Id is Int32)
                        {
                            //we can save as it as a repository template so
                            //get the planogram id
                            Int32? planId = null;
                            if (IsRepositoryPlanogram(exportItem, out planId))
                            {
                                String planogramName = exportItem.Planogram != null ?
                                    exportItem.Planogram.Name :
                                    exportItem.PlanogramInfo.Name;

                                //get the related content lookup if one already exists in the database
                                ContentLookup contentLookup = contentLookups.FirstOrDefault(c => c.PlanogramId.Equals(planId));

                                //Any newly created Content lookups in this process will not be saved as child objects
                                //and must be stored separately - they may be referenced more than once in this repositoryExportItems list
                                //ContentLookup newContentLookup = newContentLookups.FirstOrDefault(c => c.PlanogramId.Equals(planId));
                                if (contentLookup == null)
                                {
                                    contentLookup = newContentLookups.FirstOrDefault(c => c.PlanogramId.Equals(planId));
                                }

                                if (contentLookup == null)
                                {
                                    //it does not exist in the database, nor has it been created in this process by a previous export item
                                    //create a new empty lookup
                                    contentLookup = ContentLookup.NewContentLookup(CCMClient.ViewState.EntityId);
                                    contentLookup.PlanogramId = (Int32)planId;
                                    contentLookup.PlanogramName = planogramName;
                                    contentLookup.UserId = (Int32)DomainPrincipal.CurrentUserId;
                                    contentLookup.PrintTemplateId = (Int32)exportItem.TemplateInfo.Id;
                                    contentLookup = contentLookup.Save();
                                    newContentLookups.Add(contentLookup);
                                }
                                else
                                {
                                    //set the print template id
                                    contentLookup.PrintTemplateId = (Int32)exportItem.TemplateInfo.Id;
                                }

                                if (!contentLookup.IsChild && contentLookup.IsDirty)
                                {
                                    contentLookup = contentLookup.Save();
                                }
                            }
                        }
                    }
                    if (contentLookups.Any()) contentLookups.Save();
                }
                catch (Exception ex)
                {
                    CommonHelper.GetWindowService().ShowErrorMessage(
                            Message.PrintTemplateUIHelper_ExportPlans_FailPrintTemplateSaveHeader,
                            Message.PrintTemplateUIHelper_ExportPlans_FailPrintTemplateSaveDesc);
                }
            }


            //execute the export
            ExportPlanogramsToPdfWork work = new ExportPlanogramsToPdfWork(exportItems, outputDir);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);
        }

        /// <summary>
        /// Determines whether an exportItem referes to a repository planogram or a file-based pog.
        /// </summary>
        /// <param name="item">The export item to test</param>
        /// <param name="planogramId">The returned PlanogramId if valid</param>
        /// <returns>True if the exportItem refereneces a Repository planogram</returns>
        private static Boolean IsRepositoryPlanogram(PdfExportItem item, out Int32? planogramId)
        {
            //Notes: When called from CCM Automation, the PlanogramInfo is set
            //When called from CCM Space Planning the Planogram property is set
            if (item.PlanogramInfo != null)
            {
                planogramId = item.PlanogramInfo.Id;
                return true;
            }
            if (item.Planogram != null)
            {
                //the planogram will have an int id whether file or repository, but its parent package does not
                if (item.Planogram.Parent.Id is Int32)
                {
                    //parent package is a repository package so planogram must be also
                    planogramId = (Int32)item.Planogram.Id;
                    return true;
                }
            }
            planogramId = null;
            return false;
        }

        /// <summary>
        /// Carries out the asyncronous work of exporting out pdfs.
        /// </summary>
        private sealed class ExportPlanogramsToPdfWork : IModalBusyWork
        {
            #region Nested Classes

            /// <summary>
            /// Argument used to report progress
            /// </summary>
            private sealed class ReportProgressArgs
            {
                public Int32 TotalCount { get; set; }
                public Int32 NextItemNo { get; set; }
                public PdfExportItem NextItem { get; set; }

                public String OutputDir { get; set; }

                /// <summary>
                /// Flag to indictate if the conflict dialog should be
                /// shown.
                /// </summary>
                public Boolean ShowConflictPrompt { get; private set; }
                /// <summary>
                /// The number of items after this which also have a
                /// conflict to resolve.
                /// </summary>
                public Int32 RemainingConflictCount { get; private set; }
                /// <summary>
                /// The action to take in resolving a conflict
                /// </summary>
                public ItemConflictAction ConflictAction { get; private set; }

                /// <summary>
                /// Flag to indicate if the conflict action should be applied to all
                /// remaining conflicts
                /// </summary>
                public Boolean ApplyConflictActionToAll { get; private set; }

                /// <summary>
                /// The new name of the item if the user choses to resolve the conflict by
                /// renaming.
                /// </summary>
                public String NewName { get; private set; }


                public void RequireConflictPrompt(Int32 remainingConflictsCount, String newNameIfRenamed)
                {
                    ShowConflictPrompt = true;
                    RemainingConflictCount = remainingConflictsCount;
                    NewName = newNameIfRenamed;
                }

                public void SetConflictPromptResult(ItemConflictAction action, Boolean applyToAll)
                {
                    ConflictAction = action;
                    ApplyConflictActionToAll = applyToAll;
                }
            }

            /// <summary>
            /// The result of the ExportPlanogramsToPdf process work.
            /// </summary>
            private sealed class WorkCompletedResult
            {
                /// <summary>
                /// List containing items which were skipped by the user.
                /// </summary>
                public List<PdfExportItem> SkippedItems { get; private set; }

                /// <summary>
                /// List containing items which exported successfully.
                /// </summary>
                public List<PdfExportItem> SucessfulItems { get; private set; }

                /// <summary>
                /// List containing items which failed to export.
                /// </summary>
                public List<PdfExportItem> FailedItems { get; private set; }

                /// <summary>
                /// Flag to indicate if the process
                /// was cancelled by the yser
                /// </summary>
                public Boolean IsCancelledByUser { get; set; }

                /// <summary>
                /// Constructor
                /// </summary>
                public WorkCompletedResult()
                {
                    this.SkippedItems = new List<PdfExportItem>();
                    this.SucessfulItems = new List<PdfExportItem>();
                    this.FailedItems = new List<PdfExportItem>();
                }
            }

            #endregion

            #region Fields
            private List<PdfExportItem> _planograms;
            private String _outputDirectory;
            private Int32 _itemNumber = 1;
            #endregion

            #region Properties

            Boolean IModalBusyWork.ReportsProgress
            {
                get { return true; }
            }

            Boolean IModalBusyWork.SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            public ExportPlanogramsToPdfWork(List<PdfExportItem> planograms, String outputDir)
            {
                _planograms = planograms.ToList();
                _outputDirectory = outputDir;
            }
            #endregion

            #region Methods

            public object GetWorkerArgs()
            {
                return new Object[] { _planograms, _outputDirectory };
            }

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                if (this._planograms.Count > 1)
                {
                    CommonHelper.GetWindowService().ShowDeterminateBusy(
                        Message.PrintTemplateUIHelper_ExportPlans_Header,
                        Message.PrintTemplateUIHelper_ExportPlans_Desc,
                                    cancelEventHandler);
                }
                else
                {
                    CommonHelper.GetWindowService().ShowIndeterminateBusy(
                        Message.PrintTemplateUIHelper_ExportPlans_Header,
                        Message.PrintTemplateUIHelper_ExportPlans_Desc,
                                    cancelEventHandler);
                }

            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                Object[] args = (Object[])e.Argument;
                String outputDir = (String)args[1];
                Queue<PdfExportItem> plans = new Queue<PdfExportItem>((List<PdfExportItem>)args[0]);

                if (!plans.Any()) return;

                WorkCompletedResult result = new WorkCompletedResult();
                Int32 totalCount = plans.Count;

                //determine the percentage per plan
                Double planPercent = 100 / ((Double)plans.Count);
                Double curPercent = 0;


                Dictionary<Object, PrintTemplate> printTemplateCache = new Dictionary<Object, PrintTemplate>();
                ItemConflictAction conflictAction = ItemConflictAction.Unset;

                _itemNumber = 1;
                while (!sender.CancellationPending && plans.Any())
                {
                    PdfExportItem exportItem = plans.Dequeue();

                    //create the report progress args.
                    ReportProgressArgs progressArgs = new ReportProgressArgs();
                    progressArgs.NextItem = exportItem;
                    progressArgs.NextItemNo = _itemNumber;
                    progressArgs.TotalCount = totalCount;
                    progressArgs.OutputDir = outputDir;

                    //Check if we are going to have a conflict so that we can prompt the user.
                    String exportFilePath = Path.ChangeExtension(Path.Combine(outputDir, exportItem.ExportAsName), ".pdf");
                    Boolean hasConflict = false;
                    if (System.IO.File.Exists(exportFilePath))
                    {
                        hasConflict = true;

                        if (conflictAction == ItemConflictAction.Unset)
                        {
                            //Tell the args to show the prompt to the user
                            String[] newFileNames = plans.Select(p => p.ExportAsName).ToArray();
                            Int32 conflictCount = System.IO.Directory.EnumerateFiles(outputDir).Count(f =>
                            newFileNames.Contains(Path.GetFileNameWithoutExtension(f)));

                            progressArgs.RequireConflictPrompt(conflictCount,
                                Path.GetFileName(FileHelper.GetNextUniqueFilePath(exportFilePath)));
                        }
                        else
                        {
                            //copy the action.
                            progressArgs.SetConflictPromptResult(conflictAction, true);
                        }
                    }

                    //report progress
                    sender.ReportProgress((Int32)curPercent, progressArgs);

                    if (hasConflict)
                    {

                        if (progressArgs.ShowConflictPrompt)
                        {
                            //force this to wait for a reply.
                            while (progressArgs.ConflictAction == ItemConflictAction.Unset)
                            {
                                Thread.Sleep(100);
                            }
                        }


                        switch (progressArgs.ConflictAction)
                        {
                            case ItemConflictAction.Skip:
                                //if the user chose to skip then just move on.
                                result.SkippedItems.Add(exportItem);
                                continue;


                            case ItemConflictAction.Cancel:
                                //if they cancelled then add everything left over to skipped and stop.
                                result.SkippedItems.Add(exportItem);
                                result.SkippedItems.AddRange(plans);
                                plans.Clear();
                                result.IsCancelledByUser = true;
                                continue;

                            case ItemConflictAction.DontAction:
                                if (progressArgs.ApplyConflictActionToAll) conflictAction = progressArgs.ConflictAction;

                                //just move onto the next item.
                                result.SkippedItems.Add(exportItem);
                                continue;

                            case ItemConflictAction.ContinueAndReplace:
                                //no action code required, code below will do this automagically.
                                if (progressArgs.ApplyConflictActionToAll) conflictAction = progressArgs.ConflictAction;
                                break;


                            case ItemConflictAction.ContinueButKeepBoth:
                                if (progressArgs.ApplyConflictActionToAll) conflictAction = progressArgs.ConflictAction;
                                //change the new name.
                                exportFilePath = FileHelper.GetNextUniqueFilePath(exportFilePath);
                                break;

                        }

                    }


                    try
                    {
                        //first get the print template
                        PrintTemplate template;
                        if (!printTemplateCache.TryGetValue(exportItem.TemplateInfo.Id, out template))
                        {
                            template = (exportItem.TemplateInfo.Id is Int32) ?
                                PrintTemplate.FetchById((Int32)exportItem.TemplateInfo.Id)
                                : PrintTemplate.FetchByFilename((String)exportItem.TemplateInfo.Id, true);

                            ApplyOverrides(exportItem, template);

                            //add the mnodified template to the cache
                            printTemplateCache.Add(exportItem.TemplateInfo.Id, template);
                        }

                        //save the pdf
                        OutputPdf(exportItem, template, exportFilePath);

                        //add to successful list
                        result.SucessfulItems.Add(exportItem);

                        //Save the last template used as the default template
                        CCMClient.ViewState.Settings.Model.DefaultPrintTemplateId = template.Id.ToString();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.RecordException(ex);
                        result.FailedItems.Add(exportItem);
                    }


                    //report progress
                    curPercent += planPercent;
                    _itemNumber++;
                }

                if (sender.CancellationPending) result.IsCancelledByUser = true;

                e.Result = result;
            }

            /// <summary>
            /// Apply the overrides to the template but only if they have a value
            /// </summary>
            /// <param name="exportItem"></param>
            /// <param name="template"></param>
            private static void ApplyOverrides(PdfExportItem exportItem, PrintTemplate template)
            {
                if (exportItem.OverrideHighlight != null || exportItem.OverrideLabel != null || exportItem.OverrideShowRealImages != null)
                {
                    foreach (PrintTemplateSectionGroup sectionGroup in template.SectionGroups)
                    {
                        foreach (PrintTemplateSection section in sectionGroup.Sections)
                        {
                            foreach (PrintTemplateComponent component in section.Components.
                                Where(c => c.Type == PrintTemplateComponentType.Planogram))
                            {
                                if (exportItem.OverrideHighlight != null)
                                {
                                    component.ComponentDetails.PlanogramHighlightId = exportItem.OverrideHighlight.Id;
                                }
                                if (exportItem.OverrideLabel != null)
                                {
                                    switch (exportItem.OverrideLabel.Type)
                                    {
                                        case LabelType.Product:
                                            component.ComponentDetails.PlanogramProductLabelId = exportItem.OverrideLabel.Id;
                                            break;

                                        case LabelType.Fixture:
                                            component.ComponentDetails.PlanogramFixtureLabelId = exportItem.OverrideLabel.Id;
                                            break;
                                    }
                                }
                                if (exportItem.OverrideShowRealImages != null)
                                {
                                    component.ComponentDetails.PlanogramProductImages = (Boolean)exportItem.OverrideShowRealImages;
                                }
                            }
                        }
                    }
                }
            }

            private static void OutputPdf(PdfExportItem exportItem, PrintTemplate template, String pdfPath)
            {
                //get the planogram if we do not already have it.
                Planogram plan = exportItem.Planogram;
                if (plan == null) plan = Package.FetchById(exportItem.PlanogramInfo.Id).Planograms.First();


                //create the pdf
                var process = new Processes.Reports.Planogram.Process(template, plan);

                //Set up PlanogramModelData to save time
                process.PlanogramModelDataList = exportItem.PlanogramModelDataList;

                //we are working off the client so turn off the software render.
                process.IsSoftwareRender = false;

                //now execute
                process = ProcessFactory.Execute<Processes.Reports.Planogram.Process>(process);

                //save pdf down
                //String pdfPath = Path.ChangeExtension(Path.Combine(outputDir, exportItem.ExportAsName), ".pdf");
                if (System.IO.File.Exists(pdfPath)) System.IO.File.Delete(pdfPath);
                System.IO.File.WriteAllBytes(pdfPath, process.ReportData);
            }



            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                ReportProgressArgs args = (ReportProgressArgs)e.UserState;

                //Update the busy window
                CommonHelper.GetWindowService().UpdateBusy(
                  String.Format(CultureInfo.CurrentCulture, "({0}/{1}) - {2}",
                    args.NextItemNo, args.TotalCount, args.NextItem.ExportAsName),
                    e.ProgressPercentage);


                //if we have a conflict and no resolution then prompt the user.
                if (args.ShowConflictPrompt)
                {
                    String pdfPath = Path.ChangeExtension(Path.Combine(args.OutputDir, args.NextItem.ExportAsName), ".pdf");

                    ItemConflictDialogViewModel vm = new ItemConflictDialogViewModel();
                    vm.ContinueAndReplaceHeader = Message.PlanogramExportHelper_OverwriteDialogExportAndReplace;
                    vm.DontActionHeader = Message.PlanogramExportHelper_OverwriteDialogDontExport;
                    vm.ContinueButKeepBothHeader = Message.PlanogramExportHelper_OverwriteDialogExportButKeepBoth;

                    vm.ItemName = Path.GetFileName(args.NextItem.ExportAsName);
                    vm.ItemRenameName = args.NewName;
                    vm.ExistingItemDateModified = System.IO.File.GetLastWriteTime(pdfPath);
                    vm.RemainingConflictCount = args.RemainingConflictCount;

                    //show the dialog
                    CommonHelper.GetWindowService().ShowDialog<ItemConflictDialog>(vm);

                    //set the result.
                    args.SetConflictPromptResult(vm.Result, vm.ApplyActionToAllConflicts);
                }
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorMessage(null, e.Error.GetBaseException().Message);
                }
                else
                {
                    WorkCompletedResult result = (WorkCompletedResult)e.Result;

                    if (result.FailedItems.Any())
                    {
                        CommonHelper.GetWindowService().ShowErrorMessage(
                            Message.PrintTemplateUIHelper_ExportPlans_FailHeader,
                            Message.PrintTemplateUIHelper_ExportPlans_FailDesc);
                    }
                    else if (result.IsCancelledByUser)
                    {
                        CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Info,
                            Message.PrintTemplateUIHelper_ExportPlans_CancelledHeader,
                            String.Format(CultureInfo.CurrentCulture, Message.PrintTemplateUIHelper_ExportPlans_CancelDesc,
                            result.SucessfulItems.Count, result.SkippedItems.Count));
                    }
                    else
                    {
                        //show a success message with the correct planogram count displayed
                        //if the user has cenclled part-way through
                        CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Info,
                            Message.PrintTemplateUIHelper_ExportPlans_SuccessHeader,
                            String.Format(CultureInfo.CurrentCulture,
                            Message.PrintTemplateUIHelper_ExportPlans_SuccessDesc, result.SucessfulItems.Count, _outputDirectory));
                    }

                }
            }

            #endregion
        }


        /// <summary>
        /// Returns the command to use for showing the export window.
        /// </summary>
        public static RelayCommand CreateShowExportWindowCommand(IEnumerable<PlanogramInfo> planogramsCollection)
        {
            return new RelayCommand(
                //Executed:
                p =>
                {
                    //Source collection may contain nulls if no planogram assigned
                    CommonHelper.GetWindowService().ShowDialog<ExportPlanogramsWindow>(
                        new ExportPlanogramsViewModel(planogramsCollection.Where(i => i != null).ToList()));
                },

                //CanExecute:
                p =>
                {
                    if (!planogramsCollection.Any()) return false;
                    return true;
                })
            {
                FriendlyName = Message.ExportPlanograms_Title,
                FriendlyDescription = Message.ExportPlanograms_CommandDesc,
                Icon = ImageResources.SaveToFile_32,
                SmallIcon = ImageResources.SaveToFile_16
            };
        }

    }

    #region PdfExportItem

    /// <summary>
    /// Simple class to represent a planogram which can be selected for pdf export.
    /// </summary>
    public sealed class PdfExportItem : INotifyPropertyChanged
    {
        #region Fields
        private Boolean _isSelected = true;
        private String _exportAsName;
        private PrintTemplateInfo _templateInfo;

        private Boolean? _overrideShowRealImages = null;
        private Highlight _overrideHighlight = null;
        private Label _overrideLabel = null;
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether this row is selected
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Returns the planogramInfo context
        /// </summary>
        public PlanogramInfo PlanogramInfo { get; private set; }

        /// <summary>
        /// Returns the planogram context
        /// </summary>
        public Planogram Planogram { get; private set; }

        /// <summary>
        /// Returns the planogram model data
        /// </summary>
        public List<Plan3DData> PlanogramModelDataList { get; set; }

        public String Name
        {
            get { return (Planogram != null) ? Planogram.Name : PlanogramInfo.Name; }
        }

        public String CategoryCode
        {
            get { return (Planogram != null) ? Planogram.CategoryCode : PlanogramInfo.CategoryCode; }
        }

        public String CategoryName
        {
            get { return (Planogram != null) ? Planogram.CategoryName : PlanogramInfo.CategoryName; }
        }

        /// <summary>
        /// Gets/Sets the name to export the planogram as.
        /// </summary>
        public String ExportAsName
        {
            get { return _exportAsName; }
            set
            {
                _exportAsName = value;
                OnPropertyChanged("ExportAsName");
            }
        }

        /// <summary>
        /// Gets/Sets the print template to use.
        /// </summary>
        public PrintTemplateInfo TemplateInfo
        {
            get { return _templateInfo; }
            set
            {
                _templateInfo = value;
                OnPropertyChanged("TemplateInfo");
            }
        }

        /// <summary>
        /// Gets/Sets the override value for Show real images to apply. If null, no override is set and the print template value should be used
        /// </summary>
        public Boolean? OverrideShowRealImages
        {
            get { return _overrideShowRealImages; }
            set { _overrideShowRealImages = value; }
        }

        /// <summary>
        /// Gets/Sets the override value for Highlight to apply. If null, no override is set and the print template value should be used
        /// </summary>
        public Highlight OverrideHighlight
        {
            get { return _overrideHighlight; }
            set { _overrideHighlight = value; }
        }

        /// <summary>
        /// Gets/Sets the override value for Label to apply. If null, no override is set and the print template value should be used
        /// </summary>
        public Label OverrideLabel
        {
            get { return _overrideLabel; }
            set { _overrideLabel = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="plan"></param>
        public PdfExportItem(Planogram plan, PrintTemplateInfo template, Boolean? overrideShowRealImages, Highlight overrideHighlight, Label overrideLabel, List<Plan3DData> planogramModelDataList)
        {
            this.Planogram = plan;
            this.ExportAsName = plan.Name;
            this.TemplateInfo = template;

            //optional parameters for overrides to the template values
            this.OverrideShowRealImages = overrideShowRealImages;
            this.OverrideHighlight = overrideHighlight;
            this.OverrideLabel = overrideLabel;
            this.PlanogramModelDataList = planogramModelDataList;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="plan"></param>
        public PdfExportItem(PlanogramInfo planInfo, PrintTemplateInfo template, Boolean? overrideShowRealImages, Highlight overrideHighlight, Label overrideLabel, List<Plan3DData> planogramModelDataList)
        {
            this.PlanogramInfo = planInfo;
            this.ExportAsName = planInfo.Name;
            this.TemplateInfo = template;

            //optional parameters for overrides to the template values
            this.OverrideShowRealImages = overrideShowRealImages;
            this.OverrideHighlight = overrideHighlight;
            this.OverrideLabel = overrideLabel;
            this.PlanogramModelDataList = planogramModelDataList;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    #endregion
}
