﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26281 : L.Ineson 
//  Created.
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added Location
// V8-28986 : M.Shelley
//  Added PlanAssignment
#endregion

#region Version History: (CCM 8.1.1)
// V8-30357 : M.Brumby
//  Added LocationAndLocationSpaceColumnLayoutFactory
#endregion

#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn
//  Added ColumnScreenKeyAssortmentProducts & ColumnScreenKeyAssortmentLocations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;

namespace Galleria.Ccm.Common.Wpf
{
    /// <summary>
    /// Provides static access to common client application properties
    /// </summary>
    public static class CCMClient
    {
        #region Fields
        private static IViewState _viewState;
        private static Boolean _isUnitTesting;
        #endregion

        #region Properties

        public static IViewState ViewState
        {
            get { return _viewState; }
        }

        public static Boolean IsUnitTesting
        {
            get { return _isUnitTesting; }
        }

        #endregion

        #region Methods

        public static void Register(IViewState viewstate)
        {
            _viewState = viewstate;
        }


        public static void MarkAsUnitTesting()
        {
            _isUnitTesting = true;
        }

        #endregion

        #region Column ScreenKeys
        /// <summary>
        /// A screen key to use for general product screens (e.g. selectors etc)
        /// </summary>
        public static readonly String ColumnScreenKeyProducts = "Products";

        /// <summary>
        /// A screen key to use for general assortment product rules/buddies screens (e.g. selectors etc)
        /// </summary>
        public static readonly String ColumnScreenKeyAssortmentProducts = "Assortment Products";
        
        /// <summary>
        /// A screen key to use for general assortment location rules/buddies screens (e.g. selectors etc)
        /// </summary>
        public static readonly String ColumnScreenKeyAssortmentLocations= "Assortment Locations";
        #endregion
    }
}
