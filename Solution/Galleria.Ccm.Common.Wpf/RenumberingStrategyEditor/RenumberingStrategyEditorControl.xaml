﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2014.-->
<!-- Version History : (8.0)
    V8-27153 : A.Silva ~ Created.
-->
<!-- Version History : (8.1)
    V8-28878 : A.Silva ~ Amended name of RestartComponentRenumberingPerBayProperty to make it consistent with the database field name.
-->
<UserControl x:Class="Galleria.Ccm.Common.Wpf.RenumberingStrategyEditor.RenumberingStrategyEditorControl"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
             xmlns:lg="clr-namespace:Galleria.Ccm.Common.Wpf.Resources.Language"
             xmlns:gplanogram="clr-namespace:Galleria.Framework.Planograms.Model;assembly=Galleria.Framework.Planograms"
             x:Name="xUserControl">
    <Grid DataContext="{Binding ElementName=xUserControl, Path=RenumberingStrategy}"
          Background="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushWindowContentAreaBackground}}">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*" />
            <ColumnDefinition Width="*" />
        </Grid.ColumnDefinitions>

        <!--region Left panel (bay & component settings)-->
        <Border Grid.Column="0" Margin="2" Padding="4" BorderThickness="0,0,1,0" BorderBrush="LightGray"
                HorizontalAlignment="Stretch" VerticalAlignment="Stretch">
            <Grid HorizontalAlignment="Stretch">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <!--region Panel title-->
                <Border Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2" BorderThickness="0,0,1,1" Padding="4"
                        BorderBrush="LightGray">
                    <TextBlock Margin="8,4,2,4" HorizontalAlignment="Left"
                               Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_BayAndComponent}" />
                </Border>
                <!--endregion Panel title-->

                <!--region Renumber priority column title-->
                <Border Grid.Row="0" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <TextBlock x:Name="lblRenumberPriority" Margin="8,4,2,4" HorizontalAlignment="Center" TextWrapping="Wrap"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RenumberPriority}" />
                </Border>
                <!--endregion Renumber priority column title-->

                <!--region X renumber strategy (horizontal)-->
                <Border Grid.Row="1" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">
                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_XRenumberStrategy}" />
                </Border>

                <Border Grid.Row="1" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentXRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentXRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:LeftToRight}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberType.LeftToRight}" />
                                <Binding
                                    Source="{x:Static  gplanogram:PlanogramRenumberingStrategyXRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentXRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentXRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:RightToLeft}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberType.RightToLeft}" />
                                <Binding Source="{x:Static  gplanogram:PlanogramRenumberingStrategyXRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="1" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center"
                                       Text="{Binding Path=BayComponentXRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}" 
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion X renumber strategy (horizontal)-->

                <!--region Y renumber strategy (vertical)-->
                <Border Grid.Row="2" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">
                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_YRenumberStrategy}" />
                </Border>

                <Border Grid.Row="2" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentYRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentYRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:TopToBottom}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyYRenumberType.TopToBottom}" />
                                <Binding Source="{x:Static  gplanogram:PlanogramRenumberingStrategyYRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentYRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentYRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:BottomToTop}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyYRenumberType.BottomToTop}" />
                                <Binding
                                    Source="{x:Static  gplanogram:PlanogramRenumberingStrategyYRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="2" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" Text="{Binding Path=BayComponentYRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion Y renumber strategy (vertical)-->

                <!--region Z renumber strategy (depth)-->
                <Border Grid.Row="3" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">

                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_ZRenumberStrategy}" />
                </Border>

                <Border Grid.Row="3" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentZRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentZRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:FrontToBack}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyZRenumberType.FrontToBack}" />
                                <Binding Source="{x:Static  gplanogram:PlanogramRenumberingStrategyZRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="BayComponentZRenumberStrategy"
                                     IsChecked="{Binding Path=BayComponentZRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:BackToFront}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyZRenumberType.BackToFront}" />
                                <Binding
                                    Source="{x:Static  gplanogram:PlanogramRenumberingStrategyZRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="3" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center"
                                       Text="{Binding Path=BayComponentZRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion Z renumber strategy (depth)-->

                <!--region Other settings-->
                <StackPanel Grid.Row="4" Grid.Column="0" Grid.ColumnSpan="3" Margin="0,16,0,0" Orientation="Vertical">

                    <TextBlock Margin="8,3,2,3" HorizontalAlignment="Left"
                               Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_AdditionalSettings}" />

                    <CheckBox Margin="16,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=RestartComponentRenumberingPerBay, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RestartComponentRenumberingPerBay}" />
                    </CheckBox>

                    <CheckBox Margin="16,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=RestartComponentRenumberingPerComponentType, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RestartComponentRenumberingPerComponentType}" />
                    </CheckBox>

                    <CheckBox Margin="16,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=IgnoreNonMerchandisingComponents, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_IgnoreNonMerchandisingComponents}" />
                    </CheckBox>

                </StackPanel>
                <!--endregion Other settings-->

            </Grid>
        </Border>
        <!--endregion Left panel (bay & component settings)-->

        <!--region Right panel (position settings)-->
        <Border Grid.Column="1" Margin="2" Padding="4" BorderThickness="1,0,0,0" BorderBrush="LightGray"
                HorizontalAlignment="Stretch" VerticalAlignment="Stretch">
            <Grid HorizontalAlignment="Stretch">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <!--region Panel title-->
                <Border Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2" BorderThickness="0,0,1,1" Padding="4"
                        BorderBrush="LightGray">
                    <TextBlock Margin="8,4,2,4" HorizontalAlignment="Left"
                               Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_Position}" />
                </Border>
                <!--endregion Panel title-->

                <!--region Renumber priority column title-->
                <Border Grid.Row="0" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <TextBlock Margin="8,4,2,4" HorizontalAlignment="Center" TextWrapping="Wrap"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RenumberPriority}" />
                </Border>
                <!--endregion Renumber priority column title-->

                <!--region X renumber strategy (horizontal)-->
                <Border Grid.Row="1" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">

                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_XRenumberStrategy}" />
                </Border>

                <Border Grid.Row="1" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionXRenumberStrategy"
                                     IsChecked="{Binding Path=PositionXRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:LeftToRight}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberType.LeftToRight}" />
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionXRenumberStrategy"
                                     IsChecked="{Binding Path=PositionXRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:RightToLeft}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberType.RightToLeft}" />
                                <Binding
                                    Source="{x:Static gplanogram:PlanogramRenumberingStrategyXRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="1" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" Text="{Binding Path=PositionXRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion X renumber strategy (horizontal)-->

                <!--region Y renumber strategy (vertical)-->
                <Border Grid.Row="2" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">
                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_YRenumberStrategy}" />
                </Border>

                <Border Grid.Row="2" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionYRenumberStrategy"
                                     IsChecked="{Binding Path=PositionYRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:TopToBottom}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyYRenumberType.TopToBottom}" />
                                <Binding Source="{x:Static  gplanogram:PlanogramRenumberingStrategyYRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionYRenumberStrategy"
                                     IsChecked="{Binding Path=PositionYRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:BottomToTop}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyYRenumberType.BottomToTop}" />
                                <Binding
                                    Source="{x:Static  gplanogram:PlanogramRenumberingStrategyYRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="2" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" Text="{Binding Path=PositionYRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}" 
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion Y renumber strategy (vertical)-->

                <!--region Z renumber strategy (depth)-->
                <Border Grid.Row="3" Grid.Column="0" BorderThickness="0,0,0,1" Padding="2" BorderBrush="LightGray">

                    <TextBlock Margin="2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_ZRenumberStrategy}" />
                </Border>

                <Border Grid.Row="3" Grid.Column="1" BorderThickness="0,0,1,1" Padding="4" BorderBrush="LightGray">
                    <StackPanel Margin="8,0,8,0" Orientation="Vertical">

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionZRenumberStrategy"
                                     IsChecked="{Binding Path=PositionZRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:FrontToBack}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyZRenumberType.FrontToBack}" />
                                <Binding Source="{x:Static  gplanogram:PlanogramRenumberingStrategyZRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                        <RadioButton Margin="3" HorizontalAlignment="Left" GroupName="PositionZRenumberStrategy"
                                     IsChecked="{Binding Path=PositionZRenumberStrategy, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}, ConverterParameter=equals:BackToFront}">
                            <MultiBinding
                                Converter="{StaticResource {x:Static g:ResourceKeys.ConverterDictionaryKeyToValue}}"
                                Mode="OneTime">
                                <Binding Source="{x:Static gplanogram:PlanogramRenumberingStrategyZRenumberType.BackToFront}" />
                                <Binding
                                    Source="{x:Static  gplanogram:PlanogramRenumberingStrategyZRenumberTypeHelper.FriendlyNames}" />
                            </MultiBinding>
                        </RadioButton>

                    </StackPanel>
                </Border>

                <Border Grid.Row="3" Grid.Column="2" BorderThickness="0,0,0,1" Padding="4" BorderBrush="LightGray">
                    <g:ExtendedTextBox Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" Text="{Binding Path=PositionZRenumberStrategyPriority, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}" 
                                       AutomationProperties.LabeledBy="{Binding ElementName=lblRenumberPriority}"/>
                </Border>
                <!--endregion Z renumber strategy (depth)-->

                <!--region Other settings-->
                <StackPanel Grid.Row="4" Grid.Column="0" Grid.ColumnSpan="3" Margin="0,16,0,0" Orientation="Vertical">

                    <TextBlock Margin="8,4,2,4" HorizontalAlignment="Left"
                               Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                               Text="{x:Static lg:Message.RenumberingStrategyEditorControl_AdditionalSettings}" />

                    <CheckBox Margin="16,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=RestartPositionRenumberingPerBay, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RestartPositionRenumberingPerBay}" />
                    </CheckBox>

                    <CheckBox Margin="24,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=RestartPositionRenumberingPerComponent, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_RestartPositionRenumberingPerComponent}" />
                    </CheckBox>

                    <CheckBox Margin="16,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=UniqueNumberMultiPositionProductsPerComponent, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock
                            Text="{x:Static lg:Message.RenumberingStrategyEditorControl_UniqueNumberMultiProductPerComponent}" />
                    </CheckBox>

                    <CheckBox Margin="24,3,2,3" HorizontalAlignment="Left"
                              IsChecked="{Binding Path=ExceptAdjacentPositions, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}">
                        <TextBlock Text="{x:Static lg:Message.RenumberingStrateEditorControl_ExceptAdjacentPositions}" />
                    </CheckBox>

                </StackPanel>
                <!--endregion Other settings-->

            </Grid>
        </Border>
        <!--endregion Right panel (position settings)-->

    </Grid>
</UserControl>