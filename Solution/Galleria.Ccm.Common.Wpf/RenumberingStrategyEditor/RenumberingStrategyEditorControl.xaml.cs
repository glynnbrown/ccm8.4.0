﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System.Windows;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Common.Wpf.RenumberingStrategyEditor
{
    /// <summary>
    ///     Interaction logic for RenumberingStrategyEditorControl.xaml
    /// </summary>
    public partial class RenumberingStrategyEditorControl
    {
        #region Properties

        #region RenumberingStrategy

        public static readonly DependencyProperty RenumberingStrategyProperty = DependencyProperty.Register(
            "RenumberingStrategy", typeof(IPlanogramRenumberingStrategy), typeof(RenumberingStrategyEditorControl), new PropertyMetadata(null));

        public IPlanogramRenumberingStrategy RenumberingStrategy
        {
            get { return (IPlanogramRenumberingStrategy)GetValue(RenumberingStrategyProperty); }
            set { SetValue(RenumberingStrategyProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="RenumberingStrategyEditorControl" /> class.
        /// </summary>
        public RenumberingStrategyEditorControl()
        {
            InitializeComponent();
        }

        #endregion
    }
}