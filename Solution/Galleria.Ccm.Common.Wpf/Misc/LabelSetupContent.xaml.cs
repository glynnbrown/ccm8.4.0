﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for LabelSetupContent.xaml
    /// </summary>
    public sealed partial class LabelSetupContent : UserControl
    {
        #region Fields
        private readonly ReadOnlyCollection<String> _availableFonts;
        #endregion

        #region Properties

        #region ViewModel

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
             DependencyProperty.Register("ViewModel", typeof(ILabelSetupViewModel), typeof(LabelSetupContent),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public ILabelSetupViewModel ViewModel
        {
            get { return (ILabelSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LabelSetupContent()
        {
            InitializeComponent();

            //fonts
            List<String> fonts = new List<String>();
            foreach (System.Windows.Media.FontFamily fam in System.Windows.Media.Fonts.SystemFontFamilies)
            {
                fonts.Add(fam.ToString());
            }
            _availableFonts = fonts.OrderBy(f => f).ToList().AsReadOnly();

            this.FontNameComboBox.ItemsSource = _availableFonts;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the label content textbox gets double clicked.
        /// </summary>
        private void LabelContentTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.SetFieldCommand.Execute();
        }

        #endregion
    }
    
    /// <summary>
    /// Defines a viewmodel which acts as a label setup controller.
    /// </summary>
    public interface ILabelSetupViewModel
    {
        Model.Label SelectedLabel { get; }
        RelayCommand SetFieldCommand { get; }
        String DisplayText { get; set;  }
    }
}
