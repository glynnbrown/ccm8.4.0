﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Model;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for PlanogramInfoPanel.xaml
    /// </summary>
    public partial class PlanogramInfoPanel : UserControl
    {
        #region Properties

        /// <summary>
        /// Viewmodel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramRepositoryRow), typeof(PlanogramInfoPanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel property value changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanogramInfoPanel)obj).OnViewModelChanged();
        }

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public PlanogramRepositoryRow ViewModel
        {
            get { return (PlanogramRepositoryRow)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanogramInfoPanel()
        {
            InitializeComponent();

            if (this.IsVisible) UpdateDataContextBinding();

            this.IsVisibleChanged += PlanogramInfoPanel_IsVisibleChanged;
        }

        #region Event Handlers

        /// <summary>
        /// Called whenever the visibility state of the panel changes.
        /// </summary>
        private void PlanogramInfoPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateDataContextBinding();
        }

        /// <summary>
        /// Called when the viewmodel property changes.
        /// </summary>
        private void OnViewModelChanged()
        {
            UpdatePropertyDefinitions();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Controls the datacontext binding based on the panel visibility.
        /// This is to stop data fetches bein triggered unnecessarily when the panel is not visible.
        /// </summary>
        private void UpdateDataContextBinding()
        {
            if (this.planInfoLayoutRoot == null) return;
            if (this.IsVisible)
            {
                BindingOperations.SetBinding(this.planInfoLayoutRoot,
                    DataContextProperty, new Binding("ViewModel") { ElementName = "planInfoPanel" });

                UpdatePropertyDefinitions();
            }
            else
            {
                BindingOperations.ClearBinding(this.planInfoLayoutRoot, DataContextProperty);
            }
        }

        /// <summary>
        ///     Adds the property descriptions to the property grid.
        /// </summary>
        private void UpdatePropertyDefinitions()
        {
            if (xAdditionalPropertyEditor == null) return;

            var propertyItemDescriptions = xAdditionalPropertyEditor.PropertyDescriptions;

            // Clear any past property item descriptions.
            if (propertyItemDescriptions.Count > 0) propertyItemDescriptions.Clear();

            if (ViewModel != null && ViewModel.Info != null && this.IsVisible)
            {
                propertyItemDescriptions.AddRange(GetPropertyDescriptions());
            }
        }

        /// <summary>
        ///     Get the collection of <see cref="PropertyItemDescription" /> to present on the <see cref="PropertyEditor" />.
        /// </summary>
        /// <returns>A list of <see cref="PropertyItemDescription" /> to be used on a <see cref="PropertyEditor" />.</returns>
        private IEnumerable<PropertyItemDescription> GetPropertyDescriptions()
        {
            //properties already displayed in other section so can be excluded here:
            List<IModelPropertyInfo> excludedDefs =
                new List<IModelPropertyInfo>()
                {
                    PlanogramInfo.NameProperty,
                    PlanogramInfo.MetaBayCountProperty,
                    PlanogramInfo.StatusProperty,
                    PlanogramInfo.MetaUniqueProductCountProperty,
                    PlanogramInfo.CategoryCodeProperty,
                    PlanogramInfo.CategoryNameProperty,
                    PlanogramInfo.MetaTotalMerchandisableLinearSpaceProperty,
                    PlanogramInfo.DateLastModifiedProperty
                };


            DisplayUnitOfMeasureCollection uomValues =
                (this.ViewModel != null && this.ViewModel.Info != null) ?
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(this.ViewModel.Info)
                : DisplayUnitOfMeasureCollection.Empty;


            List<PropertyItemDescription> properties = new List<PropertyItemDescription>();

            foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePropertyInfos())
            {
                if (!excludedDefs.Contains(propertyInfo))
                {
                    properties.Add(CommonHelper.GetPropertyDescription(propertyInfo, uomValues));
                }
            }

            return properties;
        }


        #endregion
    }
}
