﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for HighlightSetupContent.xaml
    /// </summary>
    public sealed partial class HighlightSetupContent : UserControl
    {
        #region Properties

        #region ViewModel

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
             DependencyProperty.Register("ViewModel", typeof(IHighlightSetupViewModel), typeof(HighlightSetupContent),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public IHighlightSetupViewModel ViewModel
        {
            get { return (IHighlightSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region ShowPreviewGroups

        /// <summary>
        /// ShowPreviewGroups dependency property definition
        /// </summary>
        public static readonly DependencyProperty ShowPreviewGroupsProperty =
            DependencyProperty.Register("ShowPreviewGroups", typeof(Boolean), typeof(HighlightSetupContent),
            new PropertyMetadata(true));

        /// <summary>
        /// Gets/Sets whether the preview groups content should be displayed.
        /// Default is true.
        /// </summary>
        public Boolean ShowPreviewGroups
        {
            get { return (Boolean)GetValue(ShowPreviewGroupsProperty); }
            set { SetValue(ShowPreviewGroupsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public HighlightSetupContent()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reselect the filters tab.
        /// </summary>
        private void MethodType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.xSettingsTab.IsSelected = true;
        }

        /// <summary>
        /// Called whenever a mouse down event occurs on a characteristic name textbox.
        /// </summary>
        private void CharacteristicTextBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem parentItem = ((DependencyObject)sender).FindVisualAncestor<ListBoxItem>();
            if (parentItem != null)
            {
                parentItem.IsSelected = true;
            }
        }

        /// <summary>
        /// Called whenever the characteristic expander is collapsed.
        /// </summary>
        private void CharacteristicExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            Expander ex = (Expander)sender;
            ListBoxItem li = ex.FindVisualAncestor<ListBoxItem>();
            li.Measure(new Size(1000, 1000));
        }

        #endregion

        private void Expander_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && sender.GetType() == typeof(Expander))
            {
                var expand = sender as Expander;
                if (expand != null) expand.IsExpanded = !expand.IsExpanded;
                e.Handled = true;
            }
        }
    }

    /// <summary>
    /// Denotes the expected members of a viewmodel for use with
    /// the HighlightSetupContent control.
    /// </summary>
    public interface IHighlightSetupViewModel
    {
        Highlight SelectedHighlight { get; }
        ReadOnlyBulkObservableCollection<HighlightFilter> CurrentHighlightFilters { get; }
        HighlightCharacteristicGroupingCollection CharacteristicGroups { get; }
        RelayCommand SetFieldCommand { get; }
        RelayCommand RemoveFilterCommand { get; }
        RelayCommand AddCharacteristicCommand { get; }
        RelayCommand SetCharacteristicRuleFieldCommand { get; }
        RelayCommand RemoveCharacteristicCommand { get; }
        RelayCommand RemoveCharacteristicRuleCommand { get; }
    }
}
