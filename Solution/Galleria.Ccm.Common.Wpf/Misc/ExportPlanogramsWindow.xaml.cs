﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32810 : L.Ineson
//  Created
//V8-32810 M.Pettit
//  Now selects the planogram's related Content Lookup Print Template intially or the efault if none set previously.
// V8-33002 : M.Pettit
//  Export to pdf command now disabled if a selected plan has no template
// CCM-18326 : M.Pettit
//  Export CanExecute now checks for valid overide settings
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for ExportPlanogramsWindow.xaml
    /// </summary>
    public sealed partial class ExportPlanogramsWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ExportPlanogramsViewModel), typeof(ExportPlanogramsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called when the value of the viewmodel dependency property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ExportPlanogramsWindow senderControl = (ExportPlanogramsWindow)obj;

            if (e.OldValue != null)
            {
                ExportPlanogramsViewModel oldModel = (ExportPlanogramsViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();

                senderControl.Resources.Remove("SetPdfPrintTemplateCommand");
                senderControl.Resources.Remove("SetPdfPrintTemplateFromRepositoryCommand");
                senderControl.Resources.Remove("SetPdfPrintTemplateFromFileCommand");
            }

            if (e.NewValue != null)
            {
                ExportPlanogramsViewModel newModel = (ExportPlanogramsViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);

                senderControl.Resources.Add("SetPdfPrintTemplateCommand", newModel.SetPdfPrintTemplateCommand);
                senderControl.Resources.Add("SetPdfPrintTemplateFromRepositoryCommand", newModel.SetPdfPrintTemplateFromRepositoryCommand);
                senderControl.Resources.Add("SetPdfPrintTemplateFromFileCommand", newModel.SetPdfPrintTemplateFromFileCommand);
            }

        }

        /// <summary>
        /// Gets the viewmodel controller.
        /// </summary>
        public ExportPlanogramsViewModel ViewModel
        {
            get { return (ExportPlanogramsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="viewModel">the window vm controller.</param>
        public ExportPlanogramsWindow(ExportPlanogramsViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }
        #endregion

        #region Window Close

        /// <summary>
        /// Called when the window has completed its close action.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;
                   if (disposableViewModel != null) disposableViewModel.Dispose();

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }

    /// <summary>
    /// ViewModel controller for ExportPlanogramsWindow
    /// </summary>
    public sealed class ExportPlanogramsViewModel : WindowViewModelBase
    {
        #region Fields
        private String _pdfOutputDirectory;
        private readonly BulkObservableCollection<PdfExportItem> _pdfExportPlanograms = new BulkObservableCollection<PdfExportItem>();
        private readonly ObservableCollection<PdfExportItem> _highlightedPdfExportRows = new ObservableCollection<PdfExportItem>();
        private Boolean _exportAllPdfPlanograms = true;

        private Boolean _canOverridePrintTemplateShowRealImages = false;
        private Boolean _canOverridePrintTemplateLabels = false;
        private Boolean _canOverridePrintTemplateHighlights = false;
        private Highlight _overridePrintTemplateHighlight = null;
        private Label _overridePrintTemplatLabel = null;
        private Boolean _overridePrintTemplatShowImages = false;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath PdfOutputDirectoryProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.PdfOutputDirectory);
        public static readonly PropertyPath HighlightedPdfExportRowsProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.HighlightedPdfExportRows);
        public static readonly PropertyPath PdfExportPlanogramsProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.PdfExportPlanograms);
        public static readonly PropertyPath ExportAllPdfPlanogramsProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.ExportAllPdfPlanograms);
        public static readonly PropertyPath PdfExportCountProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.PdfExportCount);

        public static readonly PropertyPath CanOverrideShowRealImagesProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.CanOverridePrintTemplateShowRealImages);
        public static readonly PropertyPath CanOverridePrintTemplateLabelsProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.CanOverridePrintTemplateLabels);
        public static readonly PropertyPath CanOverridePrintTemplateHighlightsProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.CanOverridePrintTemplateHighlights);
        public static readonly PropertyPath OverridePrintTemplateShowRealImagesProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.OverridePrintTemplateShowRealImages);
        public static readonly PropertyPath OverridePrintTemplateLabelProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.OverridePrintTemplateLabel);
        public static readonly PropertyPath OverridePrintTemplateHighlightProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.OverridePrintTemplateHighlight);

        //Commands
        public static readonly PropertyPath SetPdfPrintTemplateCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetPdfPrintTemplateCommand);
        public static readonly PropertyPath SetPdfOutputDirectoryCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetPdfOutputDirectoryCommand);
        public static readonly PropertyPath ExportToPdfCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.ExportCommand);
        public static readonly PropertyPath ExportCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.ExportCommand);
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.CancelCommand);

        public static readonly PropertyPath SetLabelCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetLabelCommand);
        public static readonly PropertyPath SetLabelFromFileCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetLabelFromFileCommand);
        public static readonly PropertyPath SetLabelFromRepositoryCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetLabelFromRepositoryCommand);
        public static readonly PropertyPath SetHighlightCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetHighlightCommand);
        public static readonly PropertyPath SetHighlightFromFileCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetHighlightFromFileCommand);
        public static readonly PropertyPath SetHighlightFromRepositoryCommandProperty = GetPropertyPath<ExportPlanogramsViewModel>(p => p.SetHighlightFromRepositoryCommand);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of plans that can be selected for the pdf export.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PdfExportItem> PdfExportPlanograms
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns the collection indicating which rows the user has selected in the pdf export grid.
        /// </summary>
        public ObservableCollection<PdfExportItem> HighlightedPdfExportRows
        {
            get { return _highlightedPdfExportRows; }
        }

        /// <summary>
        /// Gets/Sets the pdf directory to export to.
        /// </summary>
        public String PdfOutputDirectory
        {
            get { return _pdfOutputDirectory; }
            set
            {
                _pdfOutputDirectory = value;
                OnPropertyChanged(PdfOutputDirectoryProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether all plans are selected.
        /// </summary>
        public Boolean ExportAllPdfPlanograms
        {
            get { return _exportAllPdfPlanograms; }
            set
            {
                _exportAllPdfPlanograms = value;
                OnPropertyChanged(ExportAllPdfPlanogramsProperty);

                foreach (var row in this.PdfExportPlanograms)
                    row.IsSelected = value;
            }
        }

        /// <summary>
        /// Returns a count of export plans that are selected.
        /// </summary>
        public Int32 PdfExportCount
        {
            get { return this.PdfExportPlanograms.Count(p => p.IsSelected); }
        }

        #endregion

        #region Override Properties

        public Boolean CanOverridePrintTemplateShowRealImages
        {
            get { return _canOverridePrintTemplateShowRealImages; }
            set
            {
                _canOverridePrintTemplateShowRealImages = value;
                OnPropertyChanged(CanOverrideShowRealImagesProperty);
            }
        }

        public Boolean CanOverridePrintTemplateLabels
        {
            get { return _canOverridePrintTemplateLabels; }
            set
            {
                _canOverridePrintTemplateLabels = value;
                OnPropertyChanged(CanOverridePrintTemplateLabelsProperty);
            }
        }

        public Boolean CanOverridePrintTemplateHighlights
        {
            get { return _canOverridePrintTemplateHighlights; }
            set
            {
                _canOverridePrintTemplateHighlights = value;
                OnPropertyChanged(CanOverridePrintTemplateHighlightsProperty);
            }
        }

        public Boolean OverridePrintTemplateShowRealImages
        {
            get { return _overridePrintTemplatShowImages; }
            set
            {
                _overridePrintTemplatShowImages = value;
                OnPropertyChanged(OverridePrintTemplateShowRealImagesProperty);
            }
        }

        public Label OverridePrintTemplateLabel
        {
            get { return _overridePrintTemplatLabel; }
            set
            {
                _overridePrintTemplatLabel = value;
                OnPropertyChanged(OverridePrintTemplateLabelProperty);
            }
        }

        public Highlight OverridePrintTemplateHighlight
        {
            get { return _overridePrintTemplateHighlight; }
            set
            {
                _overridePrintTemplateHighlight = value;
                OnPropertyChanged(OverridePrintTemplateHighlightProperty);
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="planograms">list of planograms selected for export.</param>
        public ExportPlanogramsViewModel(List<PlanogramInfo> planograms)
        {
            this.PdfExportPlanograms = new ReadOnlyBulkObservableCollection<PdfExportItem>(_pdfExportPlanograms);

            this.PdfOutputDirectory = CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PlanogramExport);

            //update the export plans collection
            if (_pdfExportPlanograms.Any()) _pdfExportPlanograms.Clear();
            this.HighlightedPdfExportRows.Clear();

            PrintTemplateInfoList templates = PrintTemplateInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
            PrintTemplateInfo defaultTemplate = PrintTemplateUIHelper.GetDefaultPrintTemplateInfo();
            
            
            List<Int32> planIds = planograms.Select(p => p.Id).ToList();
            ContentLookupList contentLookups = ContentLookupList.FetchByPlanogramIds(planIds);

            foreach (PlanogramInfo planInfo in planograms)
            {
                //See if a template is already assigned in the content lookup for the planogram.
                //if not, use the default
                PrintTemplateInfo contentTemplate = null;
                ContentLookup planContentLookup = contentLookups.FirstOrDefault(c => c.PlanogramId.Equals(planInfo.Id));
                if (planContentLookup != null)
                {
                    contentTemplate = templates.FirstOrDefault(t => t.Id.Equals(planContentLookup.PrintTemplateId));
                }
                if (contentTemplate == null)
                {
                    contentTemplate = defaultTemplate;
                }

                _pdfExportPlanograms.Add(new PdfExportItem(planInfo, contentTemplate, null, null, null, null));
            }
            this.ExportAllPdfPlanograms = true;
        }

        #endregion

        #region Commands

        #region Set Template Commands

        #region SetPdfPrintTemplateCommand

        private RelayCommand _setPrintTemplateCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateCommand
        {
            get
            {
                if (_setPrintTemplateCommand == null)
                {
                    _setPrintTemplateCommand = 
                        RegisterCommand(p => SetPdfPrintTemplate_Executed(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setPrintTemplateCommand;
            }
        }

        private void SetPdfPrintTemplate_Executed()
        {
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetPdfPrintTemplateFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetPdfPrintTemplateFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetPdfPrintTemplateFromFileCommand

        private RelayCommand _setPrintTemplateFromFileCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateFromFileCommand
        {
            get
            {
                if (_setPrintTemplateFromFileCommand == null)
                {
                    _setPrintTemplateFromFileCommand = 
                        RegisterCommand(p => SetPdfPrintTemplateFromFile_Executed(),
                        friendlyName:Message.Generic_OpenFromFile,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setPrintTemplateFromFileCommand;
            }
        }

        private void SetPdfPrintTemplateFromFile_Executed()
        {
            PrintTemplateInfo selectedItem = PrintTemplateUIHelper.SelectPrintTemplateFromFile();
            if (selectedItem != null)
            {
                foreach (var row in this.HighlightedPdfExportRows)
                    row.TemplateInfo = selectedItem;
            }
        }

        #endregion

        #region SetPdfPrintTemplateFromRepositoryCommand

        private RelayCommand _setPrintTemplateFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateFromRepositoryCommand
        {
            get
            {
                if (_setPrintTemplateFromRepositoryCommand == null)
                {
                    _setPrintTemplateFromRepositoryCommand = 
                        RegisterCommand(
                        p => SetPdfPrintTemplateFromRepository_Executed(),
                        p => SetPdfPrintTemplateFromRepository_CanExecute(),
                        friendlyName:  Message.Generic_OpenFromRepository,
                        icon:  CommonImageResources.Open_16,
                        smallIcon:  CommonImageResources.Open_16);
                }
                return _setPrintTemplateFromRepositoryCommand;
            }
        }

        private Boolean SetPdfPrintTemplateFromRepository_CanExecute()
        {
            if (!CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetPdfPrintTemplateFromRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }
            return true;
        }

        private void SetPdfPrintTemplateFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;
            PrintTemplateInfo selectedItem = PrintTemplateUIHelper.SelectRepositoryPrintTemplate();
            if (selectedItem != null)
            {
                foreach (var row in this.HighlightedPdfExportRows)
                    row.TemplateInfo = selectedItem;
            }
        }

        #endregion

        #endregion

        #region Set Override Label Commands

        #region SetLabelCommand

        private RelayCommand _setLabelCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetLabelCommand
        {
            get
            {
                if (_setLabelCommand == null)
                {
                    _setLabelCommand =
                        RegisterCommand(
                            p => SetLabel_Executed(),
                            p => SetLabel_CanExecute(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelCommand;
            }
        }

        private Boolean SetLabel_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            return true;
        }

        private void SetLabel_Executed()
        {
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetLabelFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetLabelFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetLabelFromFileCommand

        private RelayCommand _setLabelFromFileCommand;

        /// <summary>
        /// Show the dialog to select a label from the repository.
        /// </summary>
        public RelayCommand SetLabelFromFileCommand
        {
            get
            {
                if (_setLabelFromFileCommand == null)
                {
                    _setLabelFromFileCommand =
                        RegisterCommand(
                            p => SetLabelFromFile_Executed(),
                            p => SetLabelFromFile_CanExecute(),
                            friendlyName: Message.Generic_OpenFromFile,
                            icon: CommonImageResources.Open_16,
                            smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelFromFileCommand;
            }
        }

        private Boolean SetLabelFromFile_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            return true;
        }

        private void SetLabelFromFile_Executed()
        {
            String file = LabelUIHelper.ShowOpenFileDialog();

            if (String.IsNullOrEmpty(file)) return;

            Label selectedItem = LabelUIHelper.FetchLabelAsReadonly(file);
            if (selectedItem != null)
            {
                this.OverridePrintTemplateLabel = selectedItem;
            }
        }

        #endregion

        #region SetLabelFromRepositoryCommand

        private RelayCommand _setLabelFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a Label from the repository.
        /// </summary>
        public RelayCommand SetLabelFromRepositoryCommand
        {
            get
            {
                if (_setLabelFromRepositoryCommand == null)
                {
                    _setLabelFromRepositoryCommand =
                        RegisterCommand(
                        p => SetLabelFromRepository_Executed(),
                        p => SetLabelFromRepository_CanExecute(),
                        friendlyName: Message.Generic_OpenFromRepository,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelFromRepositoryCommand;
            }
        }

        private Boolean SetLabelFromRepository_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            if (!CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetLabelFromRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }
            this.SetLabelFromRepositoryCommand.DisabledReason = String.Empty;
            return true;
        }

        private void SetLabelFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;
            LabelInfo selectedItemInfo = LabelUIHelper.SelectRepositoryLabel();
            if (selectedItemInfo != null)
            {
                this.OverridePrintTemplateLabel = LabelUIHelper.FetchLabelAsReadonly(selectedItemInfo.Id);
            }
        }

        #endregion

        #endregion

        #region Set Override Highlight Commands

        #region SetHighlightCommand

        private RelayCommand _setHighlightCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightCommand
        {
            get
            {
                if (_setHighlightCommand == null)
                {
                    _setHighlightCommand =
                        RegisterCommand(
                        p => SetHighlight_Executed(),
                        p => SetHighlight_CanExecute(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightCommand;
            }
        }

        private Boolean SetHighlight_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            return true;
        }

        private void SetHighlight_Executed()
        {
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetHighlightFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetHighlightFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetHighlightFromFileCommand

        private RelayCommand _setHighlightFromFileCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightFromFileCommand
        {
            get
            {
                if (_setHighlightFromFileCommand == null)
                {
                    _setHighlightFromFileCommand =
                        RegisterCommand(
                        p => SetHighlightFromFile_Executed(),
                        p => SetHighlightFromFile_CanExecute(),
                        friendlyName: Message.Generic_OpenFromFile,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightFromFileCommand;
            }
        }

        private Boolean SetHighlightFromFile_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            return true;
        }

        private void SetHighlightFromFile_Executed()
        {
            String file = HighlightUIHelper.ShowOpenFileDialog();

            if (String.IsNullOrEmpty(file)) return;

            Highlight selectedItem = HighlightUIHelper.FetchHighlightAsReadonly(file);
            if (selectedItem != null)
            {
                this.OverridePrintTemplateHighlight = selectedItem;
            }
        }

        #endregion

        #region SetHighlightFromRepositoryCommand

        private RelayCommand _setHighlightFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightFromRepositoryCommand
        {
            get
            {
                if (_setHighlightFromRepositoryCommand == null)
                {
                    _setHighlightFromRepositoryCommand =
                        RegisterCommand(
                        p => SetHighlightFromRepository_Executed(),
                        p => SetHighlightFromRepository_CanExecute(),
                        friendlyName: Message.Generic_OpenFromRepository,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightFromRepositoryCommand;
            }
        }

        private Boolean SetHighlightFromRepository_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            if (!CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetHighlightFromRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }
            this.SetHighlightFromRepositoryCommand.DisabledReason = String.Empty;
            return true;
        }

        private void SetHighlightFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;
            HighlightInfo selectedItemInfo = HighlightUIHelper.SelectRepositoryHighlight();
            if (selectedItemInfo != null)
            {
                this.OverridePrintTemplateHighlight = HighlightUIHelper.FetchHighlightAsReadonly(selectedItemInfo.Id);
            }
        }

        #endregion

        #endregion

        #region SetPdfOutputDirectoryCommand

        private IRelayCommand _setPdfOutputDirectoryCommand;

        /// <summary>
        /// Show the dialog to select the directory to output the pdf files.
        /// </summary>
        public IRelayCommand SetPdfOutputDirectoryCommand
        {
            get
            {
                if (_setPdfOutputDirectoryCommand == null)
                {
                    _setPdfOutputDirectoryCommand = 
                        RegisterCommand(p => SetPdfOutputDirectory_Executed(),
                        friendlyName: "...",
                        icon: CommonImageResources.Open_16,
                        smallIcon:CommonImageResources.Open_16);
                }
                return _setPdfOutputDirectoryCommand;
            }
        }

        private void SetPdfOutputDirectory_Executed()
        {
            String selectedDir;
            if (CommonHelper.GetWindowService()
                .ShowSelectDirectoryDialog(this.PdfOutputDirectory, out selectedDir))
            {
                this.PdfOutputDirectory = selectedDir;
            }
        }

        #endregion

        #region ExportToPdfCommand

        private RelayCommand _exportCommand;

        /// <summary>
        /// Performs the export.
        /// </summary>
        public RelayCommand ExportCommand
        {
            get
            {
                if (_exportCommand == null)
                {
                    _exportCommand = RegisterCommand(
                        p => Export_Executed(),
                        p => Export_CanExecute(),
                        friendlyName: Message.ExportPlanograms_Title);
                }
                return _exportCommand;
            }
        }

        private Boolean Export_CanExecute()
        {
            OnPropertyChanged(PdfExportCountProperty);
            _exportAllPdfPlanograms = this.PdfExportPlanograms.All(p => p.IsSelected);
            OnPropertyChanged(ExportAllPdfPlanogramsProperty);

            _exportCommand.DisabledReason = String.Empty;
            //check all planograms are valid
            if (this.PdfOutputDirectory == null) return false;
            if (!this.PdfExportPlanograms.Any(p => p.IsSelected)) return false;
            if (this.PdfExportPlanograms.Any(p => p.IsSelected && p.TemplateInfo == null))
            {
                _exportCommand.DisabledReason = Message.ExportPlanograms_DisabledReason_NoTemplate;
                 return false;
            }
            //check overrides
            if (this.CanOverridePrintTemplateLabels)
            {
                if (this.OverridePrintTemplateLabel == null)
                {
                    _exportCommand.DisabledReason = Message.ExportPlanograms_DisabledReason_NoOverrideLabel;
                    return false;
                }
            }
            if (this.CanOverridePrintTemplateHighlights)
            {
                if (this.OverridePrintTemplateHighlight == null)
                {
                    _exportCommand.DisabledReason = Message.ExportPlanograms_DisabledReason_NoOverrideHighlight;
                    return false;
                }
            }
            return true;
        }

        private void Export_Executed()
        {
            //apply any override values tot he export items
            Boolean? showRealImages = this.CanOverridePrintTemplateShowRealImages == true ? (Boolean?)this.OverridePrintTemplateShowRealImages : null;
            Highlight overrideHighlight = this.CanOverridePrintTemplateHighlights == true ? this.OverridePrintTemplateHighlight : null;
            Label overrideLabel = this.CanOverridePrintTemplateLabels == true ? this.OverridePrintTemplateLabel : null;

             foreach (PdfExportItem item in this.PdfExportPlanograms.Where(p => p.IsSelected))
             {
                item.OverrideShowRealImages = showRealImages;
                item.OverrideHighlight = overrideHighlight;
                item.OverrideLabel = overrideLabel;
             }

            PlanogramExportHelper.ExportPlanogramsToPdf(
                this.PdfExportPlanograms.Where(p => p.IsSelected).ToList(),
                this.PdfOutputDirectory);

            CloseDialog(true);
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels this window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand =
                        RegisterCommand(
                        CommonCommand.CancelDialog,
                        p => Cancel_Executed());
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            CloseDialog(false);
        }

        #endregion

        #endregion
    }
}
