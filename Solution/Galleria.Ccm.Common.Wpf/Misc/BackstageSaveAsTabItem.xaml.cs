﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fluent;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for BackstageSaveAsTabItem.xaml
    /// </summary>
    public partial class BackstageSaveAsTabItem : BackstageTabItem
    {
        #region Properties

        #region ShowOptionsPanel

        /// <summary>
        /// ShowOptionsPanel dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty ShowOptionsPanelProperty =
             DependencyProperty.Register("ShowOptionsPanel", typeof(Boolean), typeof(BackstageSaveAsTabItem),
             new PropertyMetadata(true));

        /// <summary>
        /// Gets/Sets whether the user is allowed to change the selected option.
        /// </summary>
        public Boolean ShowOptionsPanel
        {
            get { return (Boolean)GetValue(ShowOptionsPanelProperty); }
            set { SetValue(ShowOptionsPanelProperty, value); }
        }


        #endregion

        #region SelectedOption

        /// <summary>
        /// SelectedOption dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty SelectedOptionProperty =
            DependencyProperty.Register("SelectedOption", typeof(BackstageOpenOption), typeof(BackstageSaveAsTabItem),
            new PropertyMetadata(BackstageOpenOption.OpenFromRepository, OnSelectedOptionPropertyChanged));

        /// <summary>
        /// Called whenever the value of the SelectedOptionProperty changes.
        /// </summary>
        private static void OnSelectedOptionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //((BackstageSaveAsTabItem)obj).RaiseSelectedOptionChanged();

            //if (((BackstageOpenOption)e.NewValue) == BackstageOpenOption.OpenFromRepository)
            //{
            //    ((BackstageSaveAsTabItem)obj).RaiseRefreshRepositoryItems();
            //}
        }

        /// <summary>
        /// Gets/Sets which option is selected on the tab
        /// </summary>
        public BackstageOpenOption SelectedOption
        {
            get { return (BackstageOpenOption)GetValue(SelectedOptionProperty); }
            set { SetValue(SelectedOptionProperty, value); }
        }

        #endregion

        #region SaveAsToRepositoryCommand

        /// <summary>
        /// SaveAsToRepositoryCommand dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty SaveAsToRepositoryCommandProperty =
            DependencyProperty.Register("SaveAsToRepositoryCommand", typeof(IRelayCommand), typeof(BackstageSaveAsTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the command to call when opening from repository
        /// </summary>
        public IRelayCommand SaveAsToRepositoryCommand
        {
            get { return (IRelayCommand)GetValue(SaveAsToRepositoryCommandProperty); }
            set { SetValue(SaveAsToRepositoryCommandProperty, value); }
        }

        #endregion

        #region SaveAsToFileCommand

        /// <summary>
        /// SaveAsToFileCommand dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty SaveAsToFileCommandProperty =
            DependencyProperty.Register("SaveAsToFileCommand", typeof(IRelayCommand), typeof(BackstageSaveAsTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the command to call when browsing for a file to open.
        /// </summary>
        public IRelayCommand SaveAsToFileCommand
        {
            get { return (IRelayCommand)GetValue(SaveAsToFileCommandProperty); }
            set { SetValue(SaveAsToFileCommandProperty, value); }
        }

        #endregion

        #region RepositoryItemTemplate

        /// <summary>
        /// RepositoryItemTemplate dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty RepositoryItemTemplateProperty =
            DependencyProperty.Register("RepositoryItemTemplate", typeof(DataTemplate), typeof(BackstageSaveAsTabItem));

        /// <summary>
        /// Gets/Sets the template to use for the new repository item details.
        /// </summary>
        public DataTemplate RepositoryItemTemplate
        {
            get { return (DataTemplate)GetValue(RepositoryItemTemplateProperty); }
            set { SetValue(RepositoryItemTemplateProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public BackstageSaveAsTabItem()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(BackstageSaveAsTabItem_Loaded);
        }

        #endregion

        #region Event Handlers

        void BackstageSaveAsTabItem_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateRecentFolders();
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SaveAsToFileCommand != null)
            {
                this.SaveAsToFileCommand.Execute(((ToggleButton)sender).Tag);
            }
        }

        #endregion


        #region Methods

        /// <summary>
        /// Sets the itemsource of the recent folders list
        /// </summary>
        private void PopulateRecentFolders()
        {
            String[] paths = new String[]
            {
                System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                System.Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            };

            RecentFolder[] folders = new RecentFolder[]
            {
                new RecentFolder()
                    {
                        Name = "My Documents",
                        Path = paths[0]
                    },

                    new RecentFolder()
                    {
                        Name = "Desktop",
                        Path = paths[1]
                    },

                    new RecentFolder()
                    {
                        Name = "My Computer",
                        Path = paths[1]
                    },

            };


            this.RecentFolders.ItemsSource = folders;
        }

        #endregion

    }

    public enum BackstageOpenOption
    {
        OpenFromRepository,
        OpenFromFile
    }

    public class RecentFolder
    {
        public String Name { get; set; }
        public String Path { get; set; }
    }
}
