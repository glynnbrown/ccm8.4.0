﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.ViewModel;
using System.Collections;
using Galleria.Framework.Controls.Wpf;
using System.IO;
using System.Windows.Controls.Primitives;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for BackstageOpenTabItem.xaml
    /// </summary>
    public partial class BackstageOpenTabItem : Fluent.BackstageTabItem
    {
        #region Events

        public event EventHandler SelectedOptionChanged;

        private void RaiseSelectedOptionChanged()
        {
            if (SelectedOptionChanged != null)
                SelectedOptionChanged(this, EventArgs.Empty);
        }

        public event EventHandler RefreshRepositoryItems;

        private void RaiseRefreshRepositoryItems()
        {
            if (RefreshRepositoryItems != null)
                RefreshRepositoryItems(this, EventArgs.Empty);
        }

        #endregion

        #region Properties

        #region OpenFromRepositoryCommand

        /// <summary>
        /// OpenFromRepositoryCommand dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty OpenFromRepositoryCommandProperty =
            DependencyProperty.Register("OpenFromRepositoryCommand", typeof(IRelayCommand), typeof(BackstageOpenTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the command to call when opening from repository
        /// </summary>
        public IRelayCommand OpenFromRepositoryCommand
        {
            get { return (IRelayCommand)GetValue(OpenFromRepositoryCommandProperty); }
            set { SetValue(OpenFromRepositoryCommandProperty, value); }
        }

        #endregion

        #region OpenFromFileCommand

        /// <summary>
        /// OpenFromFileCommand dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty OpenFromFileCommandProperty =
            DependencyProperty.Register("OpenFromFileCommand", typeof(IRelayCommand), typeof(BackstageOpenTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the command to call when browsing for a file to open.
        /// </summary>
        public IRelayCommand OpenFromFileCommand
        {
            get { return (IRelayCommand)GetValue(OpenFromFileCommandProperty); }
            set { SetValue(OpenFromFileCommandProperty, value); }
        }

        #endregion

        #region RepositoryItems

        /// <summary>
        /// RepositoryItems dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty RepositoryItemsProperty =
            DependencyProperty.Register("RepositoryItems", typeof(IList), typeof(BackstageOpenTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the collection of available repository items to display.
        /// </summary>
        public IList RepositoryItems
        {
            get { return (IList)GetValue(RepositoryItemsProperty); }
            set { SetValue(RepositoryItemsProperty, value); }
        }

        #endregion

        #region RepositoryGridColumns

        /// <summary>
        /// RepositoryGridColumns dependency property defintion.
        /// </summary>
        public static readonly DependencyProperty RepositoryGridColumnsProperty =
            DependencyProperty.Register("RepositoryGridColumns", typeof(DataGridColumnCollection), typeof(BackstageOpenTabItem),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the collection of columns to be displayed by the repository grid, 
        /// if null then only name is displayed.
        /// </summary>
        public DataGridColumnCollection RepositoryGridColumns
        {
            get { return (DataGridColumnCollection)GetValue(RepositoryItemsProperty); }
            set { SetValue(RepositoryItemsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BackstageOpenTabItem()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(BackstageOpenTabItem_Loaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial actions on laod.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackstageOpenTabItem_Loaded(object sender, RoutedEventArgs e)
        {
            RaiseRefreshRepositoryItems();
        }

        /// <summary>
        /// Called when an item on the OpenFromRepositoryGrid is double clicked.
        /// </summary>
        private void OpenFromRepositoryGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.OpenFromRepositoryCommand != null)
            {
                this.OpenFromRepositoryCommand.Execute(e.RowItem);
            }
        }

        #endregion

    }


}
