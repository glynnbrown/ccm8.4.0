﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//  Added an editor window for the planogram properties
#endregion

#endregion

using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    /// Interaction logic for PlanPropertiesEditorWindow.xaml
    /// </summary>
    public sealed partial class PlanPropertiesEditorWindow : ExtendedRibbonWindow
    {
        #region Property

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanPropertiesEditorViewModel), typeof(PlanPropertiesEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanPropertiesEditorWindow senderControl = (PlanPropertiesEditorWindow)obj;

            if (e.OldValue != null)
            {
                PlanPropertiesEditorViewModel oldModel = (PlanPropertiesEditorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                PlanPropertiesEditorViewModel newModel = (PlanPropertiesEditorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public PlanPropertiesEditorViewModel ViewModel
        {
            get { return (PlanPropertiesEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanPropertiesEditorWindow(PlanPropertiesEditorViewModel propsViewModel)
        {
            InitializeComponent();

            this.ViewModel = propsViewModel;
        }

        #endregion
    }
}
