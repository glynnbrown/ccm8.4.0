﻿// Copyright © Galleria RTS Ltd 2016

using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using System;
using System.Globalization;
using System.Windows;

namespace Galleria.Ccm.Common.Wpf.Misc
{
    /// <summary>
    ///Dialog window used to confirm the action to take when a
    ///item copy conflict occurs.
    /// </summary>
    public partial class ItemConflictDialog : Window
    {
        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ItemConflictDialogViewModel), typeof(ItemConflictDialog),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ItemConflictDialog senderControl = (ItemConflictDialog)obj;

            ItemConflictDialogViewModel oldModel = e.OldValue as ItemConflictDialogViewModel;
            if (oldModel != null)
            {
                oldModel.UnregisterWindowControl();
            }

            ItemConflictDialogViewModel newModel = e.NewValue as ItemConflictDialogViewModel;
            if (newModel != null)
            {
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        /// The controller for this window.
        /// </summary>
        public ItemConflictDialogViewModel ViewModel
        {
            get { return (ItemConflictDialogViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="vm">the window controller</param>
        public ItemConflictDialog(ItemConflictDialogViewModel vm)
        {
            InitializeComponent();

            this.ViewModel = vm;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the skip button is clicked
        /// </summary>
        private void SkipButton_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetResult(ItemConflictAction.Skip);
        }

        /// <summary>
        /// Called when the cancel button is clicked
        /// </summary>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetResult(ItemConflictAction.Cancel);
        }

        /// <summary>
        /// Called when the ContinueAndReplace button is clicked
        /// </summary>
        private void ContinueAndReplace_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetResult(ItemConflictAction.ContinueAndReplace);
        }

        /// <summary>
        /// Called when the ContinueButKeepBoth button is clicked
        /// </summary>
        private void ContinueButKeepBoth_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetResult(ItemConflictAction.ContinueButKeepBoth);
        }

        /// <summary>
        /// Called when the DontAction button is clicked
        /// </summary>
        private void DontAction_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetResult(ItemConflictAction.DontAction);
        }

        /// <summary>
        /// Called when the window is closing to dispose
        /// of the viewmodel.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disVm = this.ViewModel as IDisposable;
                    this.ViewModel = null;
                    if (disVm != null) disVm.Dispose();

                   }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

    }

    /// <summary>
    /// Denotes the available conflict result values.
    /// </summary>
    public enum ItemConflictAction
    {
        Unset,
        ContinueAndReplace,
        DontAction,
        ContinueButKeepBoth,
        Skip,
        Cancel
    }

    /// <summary>
    /// ViewModel controller for the item conflict dialog.
    /// </summary>
    public sealed class ItemConflictDialogViewModel : WindowViewModelBase
    {
        #region Fields
        private ItemConflictAction _result;
        private String _continueAndReplaceHeader;
        private String _dontActionHeader;
        private String _continueButKeepBothHeader;
        private bool _applyActionToAllConflicts;
        private Int32 _remainingConflictCount;
        private String _itemName;
        private String _destinationPath;
        private DateTime _newItemDateModified;
        private DateTime _existingItemDateModified;
        private String _itemRenameName;
        #endregion

        #region Properties

        /// <summary>
        /// The result selected by the user.
        /// </summary>
        public ItemConflictAction Result
        {
            get { return _result; }
            private set
            {
                _result = value;
                OnPropertyChanged("Result");
            }
        }

        /// <summary>
        /// The header string to display 
        /// on the ContinueAndReplace option button
        /// </summary>
        public String ContinueAndReplaceHeader
        {
            get
            {
                return _continueAndReplaceHeader;
            }
            set
            {
                _continueAndReplaceHeader = value;
                OnPropertyChanged("ContinueAndReplaceHeader");
            }
        }

        /// <summary>
        /// The header string to display on the DontAction
        /// button
        /// </summary>
        public String DontActionHeader
        {
            get { return _dontActionHeader; }
            set
            {
                _dontActionHeader = value;
                OnPropertyChanged("DontActionHeader");
            }
        }

        /// <summary>
        /// The header string to display on the ContinueButKeepBoth
        /// button
        /// </summary>
        public String ContinueButKeepBothHeader
        {
            get { return _continueButKeepBothHeader; }
            set
            {
                _continueButKeepBothHeader = value;
                OnPropertyChanged("ContinueButKeepBothHeader");
            }
        }

        /// <summary>
        /// Flag to indicate whether the selected action should be
        /// applied to all remaining conflicts.
        /// </summary>
        public Boolean ApplyActionToAllConflicts
        {
            get { return _applyActionToAllConflicts; }
            set
            {
                _applyActionToAllConflicts = value;
                OnPropertyChanged("ApplyActionToAllConflicts");
            }
        }

        /// <summary>
        /// The number of conflicts left to be resolved after this one.
        /// </summary>
        public Int32 RemainingConflictCount
        {
            get { return _remainingConflictCount; }
            set
            {
                _remainingConflictCount = value;
                OnPropertyChanged("RemainingConflictCount");
            }
        }

        /// <summary>
        /// The name of the item being copied.
        /// </summary>
        public String ItemName
        {
            get { return _itemName; }
            set
            {
                _itemName = value;
                OnPropertyChanged("ItemName");
            }
        }

        /// <summary>
        /// Gets/Sets the name that the item will assume
        /// if it is renamed.
        /// </summary>
        public String ItemRenameName
        {
            get { return _itemRenameName; }
            set
            {
                _itemRenameName = value;
                OnPropertyChanged("ItemRenameName");
            }
        }

        /// <summary>
        /// The destination path that the item
        /// is being copied to.
        /// </summary>
        public String DestinationPath
        {
            get { return _destinationPath; }
            set
            {
                _destinationPath = value;
                OnPropertyChanged("DestinationPath");
            }
        }

        /// <summary>
        /// The date at which the new item was last modified.
        /// </summary>
        public DateTime NewItemDateModified
        {
            get { return _newItemDateModified; }
            set
            {
                _newItemDateModified = value;
                OnPropertyChanged("NewItemDateModified");
            }
        }

        /// <summary>
        /// Returns the text to display showing the modified date,
        /// </summary>
        public String NewItemDateModifiedText
        {
            get
            {
                DateTime local = NewItemDateModified.ToLocalTime();

                return
                  (NewItemDateModified > ExistingItemDateModified) ?
                  String.Format(CultureInfo.CurrentCulture, Message.ItemConflictDialog_DateModifiedNewer,
                  local.ToShortDateString(), local.ToShortTimeString())
                  : String.Format(CultureInfo.CurrentCulture, Message.ItemConflictDialog_DateModified,
                  local.ToShortDateString(), local.ToShortTimeString());
            }
        }

        /// <summary>
        /// The last modified date of the existing item.
        /// </summary>
        public DateTime ExistingItemDateModified
        {
            get { return _existingItemDateModified; }
            set
            {
                _existingItemDateModified = value;
                OnPropertyChanged("ExistingItemDateModified");
            }
        }

        /// <summary>
        /// Returns the text to display showing the modified date,
        /// </summary>
        public String ExistingItemDateModifiedText
        {
            get
            {
                DateTime local = ExistingItemDateModified.ToLocalTime();

                return
                  (ExistingItemDateModified > NewItemDateModified) ?
                  String.Format(CultureInfo.CurrentCulture, Message.ItemConflictDialog_DateModifiedNewer,
                  local.ToShortDateString(), local.ToShortTimeString())
                  : String.Format(CultureInfo.CurrentCulture, Message.ItemConflictDialog_DateModified,
                  local.ToShortDateString(), local.ToShortTimeString());
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ItemConflictDialogViewModel()
        {
            //Initialize.
            _result = ItemConflictAction.Unset;
            _newItemDateModified = DateTime.Now;
            _existingItemDateModified = DateTime.Now;
            _continueAndReplaceHeader = Message.ItemConflictDialog_ContinueAndReplace;
            _dontActionHeader = Message.ItemConflictDialog_DontAction;
            _continueButKeepBothHeader = Message.ItemConflictDialog_ContinueButKeepBoth;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the result and closes the dialog.
        /// </summary>
        public void SetResult(ItemConflictAction value)
        {
            if (value == ItemConflictAction.Unset)
                throw new ArgumentOutOfRangeException();

            this.Result = value;

            CloseDialog((value != ItemConflictAction.Cancel));
        }

        #endregion

    }


}
