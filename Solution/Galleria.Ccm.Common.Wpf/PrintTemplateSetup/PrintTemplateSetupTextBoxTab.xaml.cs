﻿
#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System.Linq;
using System.Windows;
using Fluent;
using System.Collections.Generic;
using System;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateSetupTextBoxTab.xaml
    /// </summary>
    public sealed partial class PrintTemplateSetupTextBoxTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PrintTemplateSetupViewModel), typeof(PrintTemplateSetupTextBoxTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the value of the viewmodel dependency property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PrintTemplateSetupTextBoxTab)obj).LoadInsertTextMenu();
        }

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public PrintTemplateSetupViewModel ViewModel
        {
            get { return (PrintTemplateSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PrintTemplateSetupTextBoxTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
       /// Loads the list of available fields into the insert text menu.
       /// </summary>
        private void LoadInsertTextMenu()
        {
            if (this.ViewModel == null || this.xInsertTextDropDownButton == null
                || this.xInsertTextDropDownButton.Items.Count > 0) return;

            //add Custom Attributes sub menu to the existing "insert" menuItem
            MenuItem subMenuTexts = new MenuItem();
            subMenuTexts.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Text;
            subMenuTexts.MaxDropDownHeight = 300;
            MenuItem subMenuText_More = new MenuItem();
            subMenuText_More.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Text_More;
            subMenuText_More.MaxDropDownHeight = 300;
            MenuItem subMenuValues = new MenuItem();
            subMenuValues.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Value;
            subMenuValues.MaxDropDownHeight = 300;
            MenuItem subMenuValue_More = new MenuItem();
            subMenuValue_More.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Value_More;
            subMenuValue_More.MaxDropDownHeight = 300;
            MenuItem subMenuFlags = new MenuItem();
            subMenuFlags.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Flag;
            subMenuFlags.MaxDropDownHeight = 300;
            MenuItem subMenuDates = new MenuItem();
            subMenuDates.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Date;
            subMenuDates.MaxDropDownHeight = 300;
            MenuItem subMenuNotes = new MenuItem();
            subMenuNotes.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Note;
            subMenuNotes.MaxDropDownHeight = 300;

            //load the insert text menu
            foreach (var group in this.ViewModel.AvailableInsertFields.GroupBy(f=> f.GroupName))
            {
                MenuItem groupItem = new MenuItem();
                groupItem.Header = group.Key;
                
                foreach (var field in group)
                {
                    MenuItem menuItem = new MenuItem();
                    menuItem.Header = field.PropertyFriendlyName;
                    menuItem.Command = this.ViewModel.InsertTextCommand;
                    menuItem.CommandParameter = field;

                    if (group.Key.ToString().Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramComponent_PropertyGroup_Custom))
                    {
                        if (field.ToString().Contains("Text"))
                        {
                            if (field.ToString().Contains(CustomAttributeData.Text26Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text27Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text28Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text29Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text30Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text31Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text32Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text33Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text34Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text35Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text36Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text37Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text38Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text39Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text40Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text41Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text42Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text43Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text44Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text45Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text46Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text47Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text48Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text49Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Text50Property.Name.ToString()))
                            {                                
                                subMenuText_More.Items.Add(menuItem);
                            }
                            else {
                                subMenuTexts.Items.Add(menuItem);
                            }

                        }
                        else if (field.ToString().Contains("Value"))
                        {
                            if (field.ToString().Contains(CustomAttributeData.Value26Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value27Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value28Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value29Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value30Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value31Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value32Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value33Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value34Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value35Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value36Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value37Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value38Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value39Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value40Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value41Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value42Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value43Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value44Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value45Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value46Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value47Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value48Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value49Property.Name.ToString())
                                || field.ToString().Contains(CustomAttributeData.Value50Property.Name.ToString()))
                            {
                                subMenuValue_More.Items.Add(menuItem);
                            }
                            else {
                                subMenuValues.Items.Add(menuItem);
                            }
                        }
                        else if (field.ToString().Contains("Flag"))
                        {
                            subMenuFlags.Items.Add(menuItem);
                        }
                        else if (field.ToString().Contains("Date"))
                        {
                            subMenuDates.Items.Add(menuItem);
                        }
                        else if (field.ToString().Contains("Note"))
                        {
                            subMenuNotes.Items.Add(menuItem);
                        }
                    }
                    else {
                        groupItem.Items.Add(menuItem);
                        groupItem.MaxDropDownHeight = 300;
                    }
                        
                }

                if (group.Key.ToString().Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramComponent_PropertyGroup_Custom))
                {
                    subMenuTexts.Items.Add(subMenuText_More);
                    groupItem.Items.Add(subMenuTexts);
                    subMenuValues.Items.Add(subMenuValue_More);
                    groupItem.Items.Add(subMenuValues);
                    groupItem.Items.Add(subMenuFlags);
                    groupItem.Items.Add(subMenuDates);
                    groupItem.Items.Add(subMenuNotes);
                }

                this.xInsertTextDropDownButton.Items.Add(groupItem);
            }
        }

        #endregion
    }
}
