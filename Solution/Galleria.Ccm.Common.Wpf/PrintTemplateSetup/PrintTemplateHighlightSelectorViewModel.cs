﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// ViewModel controller for PrintTemplateHighlightSelector
    /// </summary>
    public sealed class PrintTemplateHighlightSelectorViewModel : WindowViewModelBase, IHighlightSetupViewModel
    {
        #region Fields
        private readonly Boolean _hasRepositoryConnection;
        private readonly ModelPermission<Model.Highlight> _itemRepositoryPerms;
        private Model.Highlight _selectedHighlight; //Currently selected highlight
        private PrintTemplateComponentDetail _componentDetail;
        private String _selectedHighlightFullName;

        private readonly BulkObservableCollection<HighlightFilter> _currentHighlightFilters = new BulkObservableCollection<HighlightFilter>();
        private ReadOnlyBulkObservableCollection<HighlightFilter> _currentHighlightFiltersRO;
        private readonly HighlightCharacteristicGroupingCollection _characteristicGroups = new HighlightCharacteristicGroupingCollection();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedHighlightProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SelectedHighlight);
        public static readonly PropertyPath SelectedHighlightFullNameProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SelectedHighlightFullName);
        public static readonly PropertyPath CurrentHighlightFiltersProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.CurrentHighlightFilters);
        public static readonly PropertyPath CharacteristicGroupsProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.CharacteristicGroups);

        //Commands
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath SetFieldCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SetFieldCommand);
        public static readonly PropertyPath ClearSelectedHighlightCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.ClearSelectedHighlightCommand);
        public static readonly PropertyPath SetFilterFieldCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SetFilterFieldCommand);
        public static readonly PropertyPath RemoveFilterCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.RemoveFilterCommand);
        public static readonly PropertyPath AddCharacteristicCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.AddCharacteristicCommand);
        public static readonly PropertyPath SetCharacteristicRuleFieldCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.SetCharacteristicRuleFieldCommand);
        public static readonly PropertyPath RemoveCharacteristicCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.RemoveCharacteristicCommand);
        public static readonly PropertyPath RemoveCharacteristicRuleCommandProperty = GetPropertyPath<PrintTemplateHighlightSelectorViewModel>(p => p.RemoveCharacteristicRuleCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the print template component detail.
        /// </summary>
        public PrintTemplateComponentDetail ComponentDetail
        {
            get { return _componentDetail; }
        }

        /// <summary>
        /// Returns the selected highlight model.
        /// </summary>
        public Model.Highlight SelectedHighlight
        {
            get { return _selectedHighlight; }
            private set
            {
                _selectedHighlight = value;

                OnPropertyChanged(SelectedHighlightProperty);
                OnSelectedHighlightChanged(value, SelectedHighlight);
            }
        }

        /// <summary>
        /// Gets the name of the selected highlight.
        /// </summary>
        public String SelectedHighlightFullName
        {
            get { return _selectedHighlightFullName; }
            private set
            {
                _selectedHighlightFullName = value;
                OnPropertyChanged(SelectedHighlightFullNameProperty);
            }
        }

        /// <summary>
        /// Returns the collection of filters for the current highlight.
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightFilter> CurrentHighlightFilters
        {
            get
            {
                if (_currentHighlightFiltersRO == null)
                {
                    _currentHighlightFiltersRO = new ReadOnlyBulkObservableCollection<HighlightFilter>(_currentHighlightFilters);
                }
                return _currentHighlightFiltersRO;
            }
        }

        /// <summary>
        /// Returns the collection of characteristics for the current highlight.
        /// </summary>
        public HighlightCharacteristicGroupingCollection CharacteristicGroups
        {
            get { return _characteristicGroups; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateHighlightSelectorViewModel(PrintTemplateComponentDetail componentDetail)
        {
            _componentDetail = componentDetail;

            Int32 entityId = CCMClient.ViewState.EntityId;
            _hasRepositoryConnection = entityId > 0;

            //get permissions for current repository.
            _itemRepositoryPerms = (_hasRepositoryConnection) ?
                new ModelPermission<Model.Highlight>(Model.Highlight.GetUserPermissions())
                : ModelPermission<Model.Highlight>.DenyAll();


            //load the current highlight - will be a readonly copy.
            _selectedHighlightFullName = Message.PrintTemplateHighlightSelector_NoHighlightName;

            Object highlightId =componentDetail.PlanogramHighlightId;
            if (highlightId != null)
            {
                try
                {
                    this.SelectedHighlight = HighlightUIHelper.FetchHighlightAsReadonly(highlightId);
                }
                catch (DataPortalException ex)
                {
                    RecordException(ex);

                    GetWindowService().ShowOkMessage(Services.MessageWindowType.Warning,
                        Message.PrintTemplateHighlightSelector_HighlightNotFoundHeader,
                        Message.PrintTemplateHighlightSelector_HighlightNotFoundDesc);
                }
            }
        }

        #endregion

        #region Commands

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens an existing highlight
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private void Open_Executed(object args)
        {
            if (this.OpenFromRepositoryCommand.CanExecute())
            {
                this.OpenFromRepositoryCommand.Execute();
            }
            else
            {
                this.OpenFromFileCommand.Execute();
            }
        }

        #endregion

        #region OpenFromRepository

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        o => OpenFromRepository_Executed(o),
                        o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_OpenFromRepository
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!_hasRepositoryConnection)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null)
            {
                GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
                win.ItemSource = HighlightInfoList.FetchByEntityId(CCMClient.ViewState.EntityId).ToList();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
                if (win.DialogResult != true) return;

                id = win.SelectedItems.Cast<HighlightInfo>().First().Id;
            }


            //load the template
            base.ShowWaitCursor(true);
            try
            {
                this.SelectedHighlight = Model.Highlight.FetchById(id, /*asReadonly*/true);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.Generic_OpenFromFile
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(Object args)
        {
            //show the file dialog if no args passed in.
            String file = args as String;
            if (String.IsNullOrEmpty(file)) file = HighlightUIHelper.ShowOpenFileDialog();
            if (String.IsNullOrEmpty(file)) return;

            //Fetch the highlight as readonly.
            base.ShowWaitCursor(true);
            try
            {
                this.SelectedHighlight = Model.Highlight.FetchByFilename(file, /*asReadOnly*/true);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves changes made to a new highlight setting and loads new as current
        /// /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        Icon = ImageResources.SaveAs_32
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //must have a highlight selected
            if (this.SelectedHighlight == null)
            {
                this.SaveAsToFileCommand.DisabledReason = Message.Generic_NoItemSelected;
                this.SaveAsCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            if (this.SaveAsToRepositoryCommand.CanExecute())
            {
                this.SaveAsToRepositoryCommand.Execute();
            }
            else
            {
                this.SaveAsToFileCommand.Execute();
            }
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveToRepository
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!_hasRepositoryConnection)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must have a highlight selected
            if (this.SelectedHighlight == null)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }


        private void SaveAsToRepository_Executed()
        {
            if (this.SelectedHighlight != null)
            {
                String copyName;

                HighlightInfoList existingItems;
                try
                {
                    existingItems = HighlightInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
                }
                catch (DataPortalException ex)
                {
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }
                Predicate<String> isUniqueCheck =
                   (s) => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

                Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
                if (!nameAccepted) return;

                ShowWaitCursor(true);

                //copy the item and rename
                Model.Highlight itemCopy = this.SelectedHighlight.Copy();
                itemCopy.Name = copyName;

                //Perform the save.
                ShowWaitCursor(true);

                try
                {
                    //Make sure the entityId is set
                    itemCopy.EntityId = CCMClient.ViewState.EntityId;

                    this.SelectedHighlight = itemCopy.SaveAs();
                }
                catch (Exception ex)
                {
                    base.ShowWaitCursor(false);
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveToFile

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(p => SaveAsToFile_Executed(p), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveToFile
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private void SaveAsToFile_Executed(Object args)
        {
            Model.Highlight itemToSave = this.SelectedHighlight;

            if (itemToSave != null)
            {
                //get the file path to save to.
                String filePath = args as String;
                if (String.IsNullOrEmpty(filePath)) filePath = HighlightUIHelper.ShowSaveAsFileDialog();
                if (String.IsNullOrEmpty(filePath)) return;


                //save
                base.ShowWaitCursor(true);
                try
                {
                    itemToSave = itemToSave.SaveAsFile(filePath);

                    //unlock the file immediately.
                    itemToSave.Dispose();
                }
                catch (Exception ex)
                {
                    base.ShowWaitCursor(false);
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }
                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits the selection
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    RegisterCommand(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (this.SelectedHighlight != null
                && this.SelectedHighlight.IsDirty)
            {
                this.OKCommand.DisabledReason = Message.PrintTemplateHighlightSelector_OKDisabledNotSaved;
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            this.ComponentDetail.PlanogramHighlightId =
                    (this.SelectedHighlight != null) ? 
                    this.SelectedHighlight.Id : null;

            //close the window.
            CloseWindow();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region ClearSelectedHighlightCommand

        private RelayCommand _clearSelectedHighlightCommand;

        /// <summary>
        /// Commits the selection
        /// </summary>
        public RelayCommand ClearSelectedHighlightCommand
        {
            get
            {
                if (_clearSelectedHighlightCommand == null)
                {
                    _clearSelectedHighlightCommand = new RelayCommand(
                        p => ClearSelectedHighlight_Executed(),
                        p => ClearSelectedHighlight_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateHighlightSelector_ClearSelectedHighlight,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_clearSelectedHighlightCommand);
                }
                return _clearSelectedHighlightCommand;
            }
        }

        private Boolean ClearSelectedHighlight_CanExecute()
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }

            return true;
        }

        private void ClearSelectedHighlight_Executed()
        {
            this.SelectedHighlight = null;
        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// PARM: The field property info.
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed(p),
                        p => SetField_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        //FriendlyDescription = Message.HighlightSetupContent_SetFieldCommand_Desc
                    };
                    RegisterCommand(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private Boolean SetField_CanExecute(Object args)
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }

            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetField_Executed(Object args)
        {
            Csla.Core.IPropertyInfo fieldPropertyInfo = args as Csla.Core.IPropertyInfo;
            if (fieldPropertyInfo != null)
            {

                String fieldValue = null;
                if (fieldPropertyInfo == Highlight.Field1Property)
                {
                    fieldValue = this.SelectedHighlight.Field1;
                }
                else if (fieldPropertyInfo == Highlight.Field2Property)
                {
                    fieldValue = this.SelectedHighlight.Field2;
                }

                FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                    FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue,
                    fieldValue, CommonHelper.GetAllPlanogramFieldSelectorGroups());

                GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);


                if (viewModel.DialogResult == true)
                {
                    if (fieldPropertyInfo == Highlight.Field1Property)
                    {
                        this.SelectedHighlight.Field1 = viewModel.FieldText;
                    }
                    else if (fieldPropertyInfo == Highlight.Field2Property)
                    {
                        this.SelectedHighlight.Field2 = viewModel.FieldText;
                    }
                }

            }
        }

        #endregion

        #region SetFilterFieldCommand

        private RelayCommand _setFilterFieldCommand;

        /// <summary>
        /// Shows the window to populate the group field
        /// </summary>
        public RelayCommand SetFilterFieldCommand
        {
            get
            {
                if (_setFilterFieldCommand == null)
                {
                    _setFilterFieldCommand = new RelayCommand(
                        p => SetFilterField_Executed(p),
                        p => SetFilterField_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.HighlightSetupContent_SetField_Desc
                    };
                    RegisterCommand(_setFilterFieldCommand);
                }
                return _setFilterFieldCommand;

            }
        }

        private Boolean SetFilterField_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetFilterField_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                       FieldSelectorInputType.Formula,
                       FieldSelectorResolveType.SingleValue,
                       filter.Field, CommonHelper.GetAllPlanogramFieldSelectorGroups());

                GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    filter.Field = viewModel.FieldText;
                }

            }
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null)
                {
                    _removeFilterCommand = new RelayCommand(
                        p => RemoveFilter_Executed(p),
                        p => RemoveFilter_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removeFilterCommand);
                }
                return _removeFilterCommand;
            }
        }

        private Boolean RemoveFilter_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveFilter_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                Highlight parentSetting = filter.Parent;
                if (parentSetting != null)
                {
                    parentSetting.Filters.Remove(filter);
                }
            }

            EnsureBlankRows();
        }

        #endregion

        #region AddCharacteristicCommand

        private RelayCommand _addCharacteristicCommand;

        /// <summary>
        /// Adds a new item for characteristic analysis.
        /// </summary>
        public RelayCommand AddCharacteristicCommand
        {
            get
            {
                if (_addCharacteristicCommand == null)
                {
                    _addCharacteristicCommand = new RelayCommand(
                        p => AddCharacteristic_Executed(),
                        p => AddCharacteristic_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        SmallIcon = ImageResources.Add_16
                    };
                    RegisterCommand(_addCharacteristicCommand);
                }
                return _addCharacteristicCommand;
            }
        }

        private Boolean AddCharacteristic_CanExecute()
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }
            return true;
        }

        private void AddCharacteristic_Executed()
        {
            Int32 groupNum = this.SelectedHighlight.Characteristics.Count + 1;

            HighlightCharacteristic newCharacteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            newCharacteristic.Name = String.Format(CultureInfo.CurrentCulture, Message.HighlightSetupContent_NewCharacteristicNameFormat, groupNum);
            this.SelectedHighlight.Characteristics.Add(newCharacteristic);
        }

        #endregion

        #region SetCharacteristicRuleFieldCommand

        private RelayCommand _setCharacteristicRuleFieldCommand;

        /// <summary>
        /// Sets the field value for the given aspect rule
        /// PARAM: HighlightCharacteristicRule
        /// </summary>
        public RelayCommand SetCharacteristicRuleFieldCommand
        {
            get
            {
                if (_setCharacteristicRuleFieldCommand == null)
                {
                    _setCharacteristicRuleFieldCommand = new RelayCommand(
                        p => SetCharacteristicRuleField_Executed(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.HighlightSetupContent_SetField_Desc
                    };
                    RegisterCommand(_setCharacteristicRuleFieldCommand);
                }
                return _setCharacteristicRuleFieldCommand;

            }
        }

        private void SetCharacteristicRuleField_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {

                FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                    FieldSelectorInputType.Formula,
                    FieldSelectorResolveType.SingleValue,
                    rule.Field, CommonHelper.GetAllPlanogramFieldSelectorGroups());

                GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    rule.Field = viewModel.FieldText;
                }

            }
        }

        #endregion

        #region RemoveCharacteristicCommand

        private RelayCommand _removeCharacteristicCommand;

        /// <summary>
        /// Removes the given aspect
        ///  PARAM: HighlightCharacteristic
        /// </summary>
        public RelayCommand RemoveCharacteristicCommand
        {
            get
            {
                if (_removeCharacteristicCommand == null)
                {
                    _removeCharacteristicCommand = new RelayCommand(
                        p => RemoveCharacteristic_Executed(p),
                        p => RemoveCharacteristic_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removeCharacteristicCommand);
                }
                return _removeCharacteristicCommand;
            }
        }

        private Boolean RemoveCharacteristic_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            //Cannot remove the last group
            if (this.SelectedHighlight.Characteristics.Count == 1)
            {
                return false;
            }

            return true;
        }

        private void RemoveCharacteristic_Executed(Object args)
        {
            HighlightCharacteristic modelToRemove = null;

            HighlightCharacteristicGrouping view = args as HighlightCharacteristicGrouping;
            if (view != null)
            {
                modelToRemove = view.Model;
            }
            else
            {
                modelToRemove = args as HighlightCharacteristic;
            }

            if (modelToRemove != null)
            {
                modelToRemove.Parent.Characteristics.Remove(modelToRemove);
            }
        }

        #endregion

        #region RemoveCharacteristicRuleCommand

        private RelayCommand _removeCharacteristicRuleCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveCharacteristicRuleCommand
        {
            get
            {
                if (_removeCharacteristicRuleCommand == null)
                {
                    _removeCharacteristicRuleCommand = new RelayCommand(
                        p => RemoveCharacteristicRule_Executed(p),
                        p => RemoveCharacteristicRule_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removeCharacteristicRuleCommand);
                }
                return _removeCharacteristicRuleCommand;
            }
        }

        private Boolean RemoveCharacteristicRule_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveCharacteristicRule_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {
                HighlightCharacteristic parentSetting = rule.Parent;
                if (parentSetting != null) parentSetting.Rules.Remove(rule);
            }

            EnsureBlankRows();
        }

        #endregion

        #endregion

        #region Event Handler

        /// <summary>
        /// Responds to a change of selected setting.
        /// </summary>
        private void OnSelectedHighlightChanged(Model.Highlight oldValue, Model.Highlight newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedHighlight_PropertyChanged;
                oldValue.ChildChanged -= SelectedHighlight_ChildChanged;
                oldValue.Filters.BulkCollectionChanged -= SelectedHighlight_FiltersBulkCollectionChanged;
                _characteristicGroups.Dettach();
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedHighlight_PropertyChanged;
                newValue.ChildChanged += SelectedHighlight_ChildChanged;
                newValue.Filters.BulkCollectionChanged += SelectedHighlight_FiltersBulkCollectionChanged;
                _characteristicGroups.Attach(newValue.Characteristics);

                //HighlightUIHelper.CreateSettingGroups(newValue, _activePlanogram, /*clearExisting*/false);

            }

            SelectedHighlight_FiltersBulkCollectionChanged(newValue, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

            EnsureBlankRows();



            //Set the full name of the item.
            String highlightFullName = Message.PrintTemplateHighlightSelector_NoHighlightName;
            if (newValue != null)
            {
                if (newValue.Id is String)
                {
                    //display the file path.
                    highlightFullName = newValue.Id.ToString();
                }
                else
                {
                    //repository highlight so just display its name.
                    highlightFullName = newValue.Name;
                }

            }
            this.SelectedHighlightFullName = highlightFullName;

        }

        /// <summary>
        /// Reponds to a property change on the selected setting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Highlight.NameProperty.Name)
            {
                if (e.PropertyName == Highlight.MethodTypeProperty.Name)
                {
                    this.SelectedHighlight.Groups.Clear();
                    EnsureBlankRows();
                }
                //CreateSettingGroups(this.SelectedHighlight, _activePlanogram, /*ClearExisting*/false);
            }
        }

        /// <summary>
        /// Called when a child of the selected setting changes.
        /// </summary>
        private void SelectedHighlight_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            Boolean updateGroups = false;

            if (e.PropertyChangedArgs != null)
            {
                if (e.ChildObject is HighlightFilter)
                {
                    updateGroups = true;

                    if (e.PropertyChangedArgs.PropertyName == HighlightFilter.FieldProperty.Name)
                    {
                        EnsureBlankRows();
                    }
                }
                else if (e.ChildObject is HighlightCharacteristicRule)
                {
                    updateGroups = true;

                    if (e.PropertyChangedArgs.PropertyName == HighlightCharacteristicRule.FieldProperty.Name)
                    {
                        EnsureBlankRows();
                    }
                }
                
            }
            else if (e.CollectionChangedArgs != null)
            {
                if (e.ChildObject is HighlightFilterList
                    || e.ChildObject is HighlightCharacteristicList
                    || e.ChildObject is HighlightCharacteristicRuleList)
                {
                    updateGroups = true;
                }

            }

            if (e.ChildObject is HighlightCharacteristic //HighlightCharacteristicGrouping
                && e.PropertyChangedArgs != null && SelectedHighlight != null && SelectedHighlight.MethodType == HighlightMethodType.Characteristic)
            {
                if (e.PropertyChangedArgs.PropertyName == HighlightCharacteristic.NameProperty.Name)
                {
                    Int32 idx = SelectedHighlight.Characteristics.IndexOf((HighlightCharacteristic)e.ChildObject);
                    HighlightGroup hg = SelectedHighlight.Groups[idx];

                    hg.DisplayName = e.ChildObject.ToString();
                }
            }

            if (e.ChildObject is HighlightGroup //HighlightGrouping
                && e.PropertyChangedArgs != null && SelectedHighlight != null && SelectedHighlight.MethodType == HighlightMethodType.Characteristic)
            {
                if (e.PropertyChangedArgs.PropertyName == HighlightGroup.DisplayNameProperty.Name)
                {
                    Int32 idx = SelectedHighlight.Groups.IndexOf((HighlightGroup)e.ChildObject);

                    HighlightGroup hg = SelectedHighlight.Groups[idx];
                    HighlightCharacteristicGrouping hcg = CharacteristicGroups.Count >= (idx + 1) ? CharacteristicGroups[idx] : null;

                    if (hcg != null)
                    {
                        hcg.Model.Name = ((HighlightGroup)e.ChildObject).DisplayName;
                    }

                    //Make sure we update displayed preview object also
                    hg.Name = ((HighlightGroup)e.ChildObject).DisplayName;
                }
            }

            //update the preview
            //if (updateGroups)
            //{
            //    CreateSettingGroups(this.SelectedHighlight, _activePlanogram, /*ClearExisting*/false);
            //}
        }

        /// <summary>
        /// Called whenever the current highlight filters collection changes.
        /// </summary>
        private void SelectedHighlight_FiltersBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        if (!_currentHighlightFilters.Contains(h))
                        {
                            _currentHighlightFilters.Add(h);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        _currentHighlightFilters.Remove(h);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        _currentHighlightFilters.Clear();
                        if (sender != null)
                        {
                            foreach (HighlightFilter h in ((Highlight)sender).Filters)
                            {
                                _currentHighlightFilters.Add(h);
                            }
                        }
                    }
                    break;
            }

            //ensure there is still a blank row in the collection
            EnsureBlankRows();
        }

        /// <summary>
        /// Called whenever a property changes on a highlight filter.
        /// </summary>
        private void HighlightFilter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            HighlightFilter filter = (HighlightFilter)sender;
            if (filter.Parent == null)
            {
                if (!String.IsNullOrEmpty(filter.Field))
                {
                    //the filter was the blank row which is now no longer blank
                    // so add to the main collection.
                    this.SelectedHighlight.Filters.Add(filter);
                }
            }
        }

        /// <summary>
        /// Called whenever the CurrentHighlightFilters collection changes
        /// </summary>
        private void CurrentHighlightFilters_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    foreach (HighlightFilter h in _currentHighlightFilters)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the CharacteristicGroups collection changes
        /// </summary>
        private void CharacteristicGroups_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (HighlightCharacteristicGrouping group in e.ChangedItems)
                        {
                            group.Dispose();
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a blank rows if required.
        /// </summary>
        private void EnsureBlankRows()
        {
            Highlight highlight = this.SelectedHighlight;
            if (highlight != null)
            {
                //add a new blank filter if required.
                if (_currentHighlightFilters.Count == 0 ||
                    !_currentHighlightFilters.Any(c => c.Parent == null))
                {
                    _currentHighlightFilters.Add(HighlightFilter.NewHighlightFilter());
                }

                if (highlight.MethodType == HighlightMethodType.Characteristic)
                {
                    if (highlight.Characteristics.Count == 0)
                    {
                        this.AddCharacteristicCommand.Execute();
                    }

                }
            }
        }


        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                DisposeBase();
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
