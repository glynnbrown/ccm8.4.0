﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
// V8-31403 : A.Probyn
//  Updated for help ids
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateSetupWindow.xaml
    /// </summary>
    public sealed partial class PrintTemplateSetupWindow : ExtendedRibbonWindow
    {
        #region Fields
        PrintTemplateSection _oldPrintTemplateSection;
        private PrintTemplateSection _attachedSection;
        private Boolean _suppressSelectionChangedHandling;
        private const Single _scale = 2.835F;
        #endregion

        #region Properties

        public static String HelpFileKey { get; set; }


        #region View Model Property

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(PrintTemplateSetupViewModel), typeof(PrintTemplateSetupWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the viewmodel context.
        /// </summary>
        public PrintTemplateSetupViewModel ViewModel
        {
            get { return (PrintTemplateSetupViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Called whenever the value of the viewmodel dependency property changes.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateSetupWindow senderControl = (PrintTemplateSetupWindow)obj;

            if (e.OldValue != null)
            {
                PrintTemplateSetupViewModel oldModel = (PrintTemplateSetupViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

                //oldModel.EditColumnWidthsCommand.Executed -= senderControl.EditColumnWidthsCommand_Executed;
                oldModel.OpenFromRepositoryCommand.Executed -= senderControl.ViewModel_OpenCommandExecuted;
                oldModel.OpenFromFileCommand.Executed -= senderControl.ViewModel_OpenCommandExecuted;
                oldModel.OpenFromRepositoryCommand.Executed -= senderControl.ViewModel_OpenCommandExecuted;
            }

            if (e.NewValue != null)
            {
                PrintTemplateSetupViewModel newModel = (PrintTemplateSetupViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                //newModel.EditColumnWidthsCommand.Executed += senderControl.EditColumnWidthsCommand_Executed;
                newModel.OpenFromRepositoryCommand.Executed += senderControl.ViewModel_OpenCommandExecuted;
                newModel.OpenFromFileCommand.Executed += senderControl.ViewModel_OpenCommandExecuted;
                newModel.OpenFromRepositoryCommand.Executed += senderControl.ViewModel_OpenCommandExecuted;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateSetupWindow(PrintTemplateSetupViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Set bitmap scaling mode as default for .Net 4 is low quality
            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.Fant);

            //set the help file key for this window.
            Galleria.Framework.Controls.Wpf.Help.SetFilename(this, CCMClient.ViewState.HelpFilePath);
            if (!String.IsNullOrEmpty(HelpFileKey)) Galleria.Framework.Controls.Wpf.Help.SetKeyword(this, HelpFileKey);
            
            this.ViewModel = viewModel;


            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        /// <summary>
        /// Called on initial load of the window.
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(Window_Loaded);

            //load the visual thumb
            OnViewModelSeletedSectionChanged();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        #region ViewModel Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PrintTemplateSetupViewModel.SelectedSectionProperty.Path)
            {
                OnViewModelSeletedSectionChanged();
            }
        }

        /// <summary>
        /// Responds to the Open command executing
        /// </summary>
        private void ViewModel_OpenCommandExecuted(object sender, EventArgs e)
        {
            if (this.ViewModel.CurrentPrintTemplate == null) return;

            //loop through all the section groups
            foreach (PrintTemplateSectionGroup group in this.ViewModel.CurrentPrintTemplate.SectionGroups)
            {

                //loop through all the sections and create the preview image
                foreach (PrintTemplateSection section in group.Sections)
                {
                    //create visual
                    PrintTemplateSectionCanvas render = new PrintTemplateSectionCanvas();
                    this.xPrintTemplateSectionPreviewImageGrid.Children.Add(render);

                    //load all components
                    foreach (PrintTemplateComponent component in section.Components)
                    {
                        PrintTemplateComponentNode componentNode = new PrintTemplateComponentNode(component);
                        render.Components.Add(componentNode);
                    }

                    //draw the section preview grid
                    this.xPrintTemplateSectionPreviewImageGrid.InvalidateVisual();
                    this.xPrintTemplateSectionPreviewImageGrid.Arrange(new Rect(new System.Windows.Point(0, 0),
                        new System.Windows.Point(section.ScaledPageWidth, section.ScaledPageHeight)));
                    this.xPrintTemplateSectionPreviewImageGrid.UpdateLayout();

                    //create new render target bitmap
                    RenderTargetBitmap renderTargetBitmap =
                        new RenderTargetBitmap(
                            section.ScaledPageWidth,
                            section.ScaledPageHeight,
                            96,
                            96,
                            PixelFormats.Default);

                    //create bitmap of the section preview
                    renderTargetBitmap.Render(render);

                    //create image brush from the bitmap
                    ImageBrush imageBrush = new ImageBrush(renderTargetBitmap);

                    //set the preview image 
                    section.PreviewImage = imageBrush;

                    //remove the canvas from the grid
                    this.xPrintTemplateSectionPreviewImageGrid.Children.Remove(render);
                }
            }

            //ensure there is a selected section group
            if (this.ViewModel.SelectedSectionGroup == null)
            {
                this.ViewModel.SelectedSectionGroup = (this.ViewModel.CurrentPrintTemplate.SectionGroups.Count > 0) ?
                    this.ViewModel.CurrentPrintTemplate.SectionGroups.FirstOrDefault(p => p.Number == 1)
                    : PrintTemplateSectionGroup.NewEmpty();
            }

            //ensure there is a selected section
            if (this.ViewModel.SelectedSection == null)
            {
                this.ViewModel.SelectedSection = (this.ViewModel.SelectedSectionGroup.Sections.Count > 0) ?
                    this.ViewModel.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == 1)
                    : PrintTemplateSection.NewEmpty();
            }

            //if there is a selected section then convert to visual brush
            if (this.ViewModel.SelectedSection != null)
            {
                //create a new visual brush of the section
                VisualBrush visualBrush = new VisualBrush(printTemplateSection);

                //set the visual brush as the preview image
                this.ViewModel.SelectedSection.PreviewImage = visualBrush;
            }

        }

        #endregion

        #region Override Handlers

        /// <summary>
        /// Called on key presses
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (this.ViewModel == null) return;

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage & select the open tab
                CommonHelper.SetRibbonBackstageState(this, true);
                this.xBackstageOpen.IsSelected = true;
            }
            else if (e.Key == Key.Delete && Keyboard.Modifiers == ModifierKeys.None)
            {
                this.ViewModel.RemoveComponentCommand.Execute();

                //UIElement focusedElement = FocusManager.GetFocusedElement(this) as UIElement;
                //if (focusedElement != null)
                //{
                //    ZoomBox mainDisplay = focusedElement.FindVisualAncestor<ZoomBox>();
                //    if (mainDisplay != null)
                //    {
                //        this.ViewModel.RemoveComponentCommand.Execute();
                //    }
                //    else if (this.xSectionGroups.IsKeyboardFocusWithin)
                //    {
                //        this.ViewModel.RemoveSectionCommand.Execute();
                //    }
                //}

            }
        }

        #region Side Panel Previews

        /// <summary>
        /// Responds to section selection changed
        /// </summary>
        private void xSections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                PrintTemplateSection selectedSection = e.AddedItems[0] as PrintTemplateSection;

                if (selectedSection != null)
                {
                    if (this.ViewModel != null
                        && this.ViewModel.SelectedSection != selectedSection)
                    {
                        //Set selected section
                        this.ViewModel.SelectedSection = selectedSection;

                        //find the parent group and select it
                        PrintTemplateSectionGroup group = (PrintTemplateSectionGroup)selectedSection.Parent;

                        if (group != null && group != this.ViewModel.SelectedSectionGroup)
                        {
                            this.ViewModel.SelectedSectionGroup = group;
                        }

                        // reset the selected component
                        this.ViewModel.SelectedComponent = null;

                        // set the component type to select
                        this.ViewModel.SelectedComponentToolType = PrintTemplateComponentToolType.Select;
                    }
                }
            }
        }

        /// <summary>
        /// Responds to section group selection changed
        /// </summary>
        private void xSectionGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                PrintTemplateSectionGroup selectedSectionGroup = e.AddedItems[0] as PrintTemplateSectionGroup;

                if (selectedSectionGroup != null)
                {
                    //Set selected section group
                    this.ViewModel.SelectedSectionGroup = selectedSectionGroup;

                    //if the selected section is not in the selected group then select the 1st section
                    if (this.ViewModel.SelectedSection != null && this.ViewModel.SelectedSection.Parent != null)
                    {
                        PrintTemplateSectionGroup group = (PrintTemplateSectionGroup)this.ViewModel.SelectedSection.Parent;

                        if (group != selectedSectionGroup)
                        {
                            //Find the child section and select it
                            PrintTemplateSection selectedSection = selectedSectionGroup.Sections.FirstOrDefault(s => s.Number == 1);

                            if (selectedSection != null)
                            {
                                this.ViewModel.SelectedSection = selectedSection;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Responds to right mouse button down on list box item
        /// </summary>
        private void SectionPreviewDisplay_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem senderControl = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (senderControl == null) return;

            //see if the event came from the print option section
            PrintTemplateSection section = senderControl.DataContext as PrintTemplateSection;
            if (section != null)
            {
                PrintTemplateSection_RightClickEvent(section);
            }
            else
            {
                //see if the event came from the print option section group
                PrintTemplateSectionGroup sectionGroup = senderControl.DataContext as PrintTemplateSectionGroup;
                if (sectionGroup != null)
                {
                    PrintTemplateSectionGroup_RightClickEvent(sectionGroup);
                }
            }

        }

        #endregion

        #region Main Display

        /// <summary>
        /// Event handler for the component list bulk collection changing
        /// </summary>
        private void ComponentList_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PrintTemplateComponent item in e.ChangedItems)
                    {
                        AddComponentControl(item);
                    }
                   break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PrintTemplateComponent item in e.ChangedItems)
                    {
                        RemoveComponentControl(item);
                    }
                    break;

                    //TODO: What about reset?
            }

        }

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        private void printTemplateSection_ComponentSelectionChanged(object sender, EventArgs e)
        {
            //return out if this is suppressed
            if (_suppressSelectionChangedHandling) { return; }

            _suppressSelectionChangedHandling = true;

            if (printTemplateSection.SelectedItem != null)
            {
                //update the property to match the printTemplateSection
                PrintTemplateComponentNode printTemplateSectionSelectedNode = printTemplateSection.SelectedItem as PrintTemplateComponentNode;

                if (printTemplateSectionSelectedNode != null)
                {
                    this.ViewModel.SelectedComponent = printTemplateSectionSelectedNode.UnitContext;
                }
                else
                {
                    PrintTemplateComponentLine printTemplateSectionSelectedLine = printTemplateSection.SelectedItem as PrintTemplateComponentLine;
                    if (printTemplateSectionSelectedLine != null)
                    {
                        this.ViewModel.SelectedComponent = printTemplateSectionSelectedLine.UnitContext;
                    }
                    else
                    {
                        this.ViewModel.SelectedComponent = null;
                    }
                }
            }
            else
            {
                this.ViewModel.SelectedComponent = null;
            }

            _suppressSelectionChangedHandling = false;
        }

        /// <summary>
        /// Responds to left mouse button event
        /// </summary>
        private void printTemplateSection_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PrintTemplateSectionCanvas selectionCanvas = sender as PrintTemplateSectionCanvas;
            if (selectionCanvas == null || this.ViewModel == null || this.ViewModel.CurrentPrintTemplate == null) return;


            PrintTemplateComponent printTemplateComponent;

            switch (this.ViewModel.SelectedComponentToolType)
            {
                #region Select
                case PrintTemplateComponentToolType.Select:

                    // reset all the selections
                    ResetPrintTemplateSectionSelections(selectionCanvas);

                    break;
                    #endregion

                #region TextBox
                case PrintTemplateComponentToolType.TextBox:

                    // create a new print option component
                    printTemplateComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);

                    // position the component
                    PositionComponent(printTemplateComponent);

                    // create the component
                    this.ViewModel.SelectedSection.Components.Add(printTemplateComponent);

                    break;
                    #endregion

                #region Planogram
                case PrintTemplateComponentToolType.Planogram:

                    // create a new print option component
                    printTemplateComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Planogram);

                    // position the component
                    PositionComponent(printTemplateComponent);

                    // create the component
                    this.ViewModel.SelectedSection.Components.Add(printTemplateComponent);

                    break;
                    #endregion

                #region DataSheet
                case PrintTemplateComponentToolType.DataSheet:

                    // create a new print option component
                    printTemplateComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.DataSheet);

                    // position the component
                    PositionComponent(printTemplateComponent);

                    // create the merch list component
                    this.ViewModel.SelectedSection.Components.Add(printTemplateComponent);

                    // show the select datasheet window
                    this.ViewModel.SetDataSheetCommand.Execute();

                    //if the id is null then remove it again
                    if (this.ViewModel.SelectedComponent.ComponentDetails.DataSheetId == null)
                    {
                        this.ViewModel.RemoveComponentCommand.Execute();
                    }

                    break;
                #endregion

                #region Line
                case PrintTemplateComponentToolType.Line:

                    // create a new print option component
                    printTemplateComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);

                    // position the component
                    PositionComponent(printTemplateComponent);

                    // create the line component
                    this.ViewModel.SelectedSection.Components.Add(printTemplateComponent);

                    break;
                #endregion
           }

            //Finally set the selected component type to Select
            this.ViewModel.SelectedComponentToolType = PrintTemplateComponentToolType.Select;

        }

        /// <summary>
        /// Responds to right mouse button event
        /// </summary>
        private void printTemplateSection_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel.SelectedComponent != null)
            {
                //create the context menu
                ContextMenu contextMenu = new ContextMenu();
                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;

                //add remove item
                MenuItem removePrintTemplateComponent = new MenuItem();
                removePrintTemplateComponent.Header = Message.PrintTemplateSetup_ContextMenu_RemoveComponent;
                removePrintTemplateComponent.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.RemoveComponentCommand.SmallIcon };
                removePrintTemplateComponent.Command = this.ViewModel.RemoveComponentCommand;
                contextMenu.Items.Add(removePrintTemplateComponent);

                //add copy item
                MenuItem copyPrintTemplateComponent = new MenuItem();
                copyPrintTemplateComponent.Header = Message.PrintTemplateSetup_ContextMenu_CopyComponent;
                copyPrintTemplateComponent.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.CopyComponentCommand.SmallIcon };
                copyPrintTemplateComponent.Command = this.ViewModel.CopyComponentCommand;
                contextMenu.Items.Add(copyPrintTemplateComponent);


                //Add component type specific options
                switch (this.ViewModel.SelectedComponent.Type)
                {
                    case PrintTemplateComponentType.TextBox:
                        {
                            //if textbox then add insert fields

                            //seperator
                            Separator seperator = new Separator();
                            contextMenu.Items.Add(seperator);

                            //add insert item
                            MenuItem insertMenu = new MenuItem();
                            insertMenu.Header = Message.PrintTemplateSetup_ContextMenu_InsertText;

                            //add Custom Attributes sub menu to the existing "insert" menuItem
                            MenuItem subMenuTexts = new MenuItem();
                            subMenuTexts.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Text;
                            MenuItem subMenuText_More = new MenuItem();
                            subMenuText_More.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Text_More;
                            MenuItem subMenuValues = new MenuItem();
                            subMenuValues.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Value;
                            MenuItem subMenuValue_More = new MenuItem();
                            subMenuValue_More.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Value_More;
                            MenuItem subMenuFlags = new MenuItem();
                            subMenuFlags.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Flag;
                            MenuItem subMenuDates = new MenuItem();
                            subMenuDates.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Date;
                            MenuItem subMenuNotes = new MenuItem();
                            subMenuNotes.Header = Message.PrintTemplateSetup_ContextMenu_CustomAttributes_Note;

                            foreach (var group in this.ViewModel.AvailableInsertFields.GroupBy(f => f.GroupName))
                            {
                                MenuItem groupItem = new MenuItem();
                                groupItem.Header = group.Key;

                                foreach (var field in group)
                                {
                                    MenuItem menuItem = new MenuItem();
                                    menuItem.Header = field.PropertyFriendlyName;
                                    menuItem.Command = this.ViewModel.InsertTextCommand;
                                    menuItem.CommandParameter = field;

                                    if (group.Key.ToString().Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramComponent_PropertyGroup_Custom))
                                    {
                                        if (field.ToString().Contains("Text"))
                                        {
                                            if (field.ToString().Contains(CustomAttributeData.Text26Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text27Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text28Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text29Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text30Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text31Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text32Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text33Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text34Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text35Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text36Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text37Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text38Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text39Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text40Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text41Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text42Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text43Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text44Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text45Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text46Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text47Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text48Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text49Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Text50Property.Name.ToString()))
                                            {
                                                subMenuText_More.Items.Add(menuItem);
                                            }
                                            else {
                                                subMenuTexts.Items.Add(menuItem);
                                            }

                                        }
                                        else if (field.ToString().Contains("Value"))
                                        {
                                            if (field.ToString().Contains(CustomAttributeData.Value26Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value27Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value28Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value29Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value30Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value31Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value32Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value33Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value34Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value35Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value36Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value37Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value38Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value39Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value40Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value41Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value42Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value43Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value44Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value45Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value46Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value47Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value48Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value49Property.Name.ToString())
                                                || field.ToString().Contains(CustomAttributeData.Value50Property.Name.ToString()))
                                            {
                                                subMenuValue_More.Items.Add(menuItem);
                                            }
                                            else {
                                                subMenuValues.Items.Add(menuItem);
                                            }
                                        }
                                        else if (field.ToString().Contains("Flag"))
                                        {
                                            subMenuFlags.Items.Add(menuItem);
                                        }
                                        else if (field.ToString().Contains("Date"))
                                        {
                                            subMenuDates.Items.Add(menuItem);
                                        }
                                        else if (field.ToString().Contains("Note"))
                                        {
                                            subMenuNotes.Items.Add(menuItem);
                                        }
                                    }
                                    else {
                                        groupItem.Items.Add(menuItem);
                                    }
                                }

                                if (group.Key.ToString().Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramComponent_PropertyGroup_Custom))
                                {
                                    subMenuTexts.Items.Add(subMenuText_More);
                                    groupItem.Items.Add(subMenuTexts);
                                    subMenuValues.Items.Add(subMenuValue_More);
                                    groupItem.Items.Add(subMenuValues);
                                    groupItem.Items.Add(subMenuFlags);
                                    groupItem.Items.Add(subMenuDates);
                                    groupItem.Items.Add(subMenuNotes);
                                }

                                insertMenu.Items.Add(groupItem);
                            }

                            //finally add to context menu
                            contextMenu.Items.Add(insertMenu);
                        }
                        break;

                    case PrintTemplateComponentType.DataSheet:
                        {
                            ////seperator
                            //Separator seperator = new Separator();
                            //contextMenu.Items.Add(seperator);

                            ////add Edit Columns
                            //MenuItem editPrintTemplateComponentColumns = new MenuItem();
                            //editPrintTemplateComponentColumns.Header = "TODO";//Message.PrintTemplateSetup_EditColumns;
                            ////editPrintTemplateComponentColumns.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.EditColumnsCommand.SmallIcon };
                            ////editPrintTemplateComponentColumns.Command = this.ViewModel.EditColumnsCommand;
                            //contextMenu.Items.Add(editPrintTemplateComponentColumns);


                            ////if merch list and atleast 1 column is set then add edit column widths:

                            ////add Edit Column Widths
                            //MenuItem editMerchListColumnWidths = new MenuItem();
                            //editMerchListColumnWidths.Header = "TODO";// Message.PrintTemplateSetup_ContextMenu_EditColumnWidths;
                            ////editMerchListColumnWidths.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.EditColumnWidthsCommand.SmallIcon };
                            ////editMerchListColumnWidths.Command = this.ViewModel.EditColumnWidthsCommand;
                            //contextMenu.Items.Add(editMerchListColumnWidths);
                        }
                        break;
                }

                //show
                contextMenu.IsOpen = true;
            }
            else
            {
                //create the context menu
                ContextMenu contextMenu = new ContextMenu();
                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;

                //add paste item
                MenuItem pastePrintTemplateComponent = new MenuItem();
                pastePrintTemplateComponent.Header = Message.PrintTemplateSetup_ContextMenu_PasteComponent;
                pastePrintTemplateComponent.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.PasteComponentCommand.SmallIcon };
                pastePrintTemplateComponent.Command = this.ViewModel.PasteComponentCommand;
                pastePrintTemplateComponent.CommandParameter = Mouse.GetPosition(this);
                contextMenu.Items.Add(pastePrintTemplateComponent);

                //show
                contextMenu.IsOpen = true;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Responds to the Edit Column Widths command executing
        /// </summary>
        private void EditColumnWidthsCommand_Executed(object sender, EventArgs e)
        {
            //find the component
            foreach (var component in printTemplateSection.Components)
            {
                PrintTemplateComponentNode componentNode = component as PrintTemplateComponentNode;
                if (componentNode != null)
                {
                    if (componentNode.UnitContext == this.ViewModel.SelectedComponent)
                    {
                        //set the is hit test visible value
                        componentNode.IsHitTestVisible = true;
                        break;
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Methods

        private void OnViewModelSeletedSectionChanged()
        {
            //carry out main window actions first.
            #region OnSelectedSectionChanged from window
            //ensure the selected section is set
            if (this.ViewModel != null
                && this.ViewModel.SelectedSection != null)
            {
                //ensure that the previous section is set, is not deleted, 
                //the preview image is a visual brush and the height and width are set
                if (_oldPrintTemplateSection != null
                    && !_oldPrintTemplateSection.IsDeleted
                    && _oldPrintTemplateSection.PreviewImage is VisualBrush
                    && _oldPrintTemplateSection.ScaledPageHeight > 0
                    && _oldPrintTemplateSection.ScaledPageWidth > 0)
                {
                    //get the visual brush of the preview image
                    VisualBrush visualBrush = (VisualBrush)_oldPrintTemplateSection.PreviewImage;

                    //create new render target bitmap
                    RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(
                        _oldPrintTemplateSection.ScaledPageWidth,
                        _oldPrintTemplateSection.ScaledPageHeight,
                        96,
                        96,
                        PixelFormats.Default);

                    //create bitmap of the visual brush
                    renderTargetBitmap.Render(visualBrush.Visual);

                    //create image brush from the bitmap
                    ImageBrush imageBrush = new ImageBrush(renderTargetBitmap);

                    //set the preview image 
                    _oldPrintTemplateSection.PreviewImage = imageBrush;
                }

                //set the selected section preview image to a visual brush of the section canvas
                if (printTemplateSection != null)
                {
                    //create a new visual brush of the section
                    VisualBrush visualBrush = new VisualBrush(printTemplateSection);

                    //set the visual brush as the preview image
                    this.ViewModel.SelectedSection.PreviewImage = visualBrush;
                }

                //set the previously selected print option section as the currently selected.
                //This is so that when the selected section changes we can create a image brush for the preview image
                _oldPrintTemplateSection = this.ViewModel.SelectedSection;
            }
            #endregion

            #region Preview display change actions
            //through to preview display actions.

            if (_attachedSection != null)
            {
                _attachedSection.Components.BulkCollectionChanged -= ComponentList_BulkCollectionChanged;
            }
            _attachedSection = null;

            if (this.ViewModel != null && this.ViewModel.SelectedSection != null)
            {
                _attachedSection = this.ViewModel.SelectedSection;
                _attachedSection.Components.BulkCollectionChanged += ComponentList_BulkCollectionChanged;
            }

            RefreshPrintTemplateSectionCanvas(_attachedSection);
            #endregion

        }

        #region Side Panel Previews

        /// <summary>
        /// Loads Print Template Section Context Menu
        /// </summary>
        private void PrintTemplateSection_RightClickEvent(PrintTemplateSection item)
        {
            //create the context menu
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;

            //add remove item
            MenuItem removePrintTemplateSection = new MenuItem();
            removePrintTemplateSection.Header = Message.PrintTemplateSetup_ContextMenu_RemoveSection;
            removePrintTemplateSection.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.RemoveSectionCommand.SmallIcon };
            removePrintTemplateSection.Command = this.ViewModel.RemoveSectionPreviewCommand;
            removePrintTemplateSection.CommandParameter = item;
            contextMenu.Items.Add(removePrintTemplateSection);

            //if more than 1 section in the group add move up/down commands
            if (item.Parent.Sections.Count > 1)
            {
                if (item.Number == 1)
                {
                    //move section down
                    MenuItem movePrintTemplateSectionDown = new MenuItem();
                    movePrintTemplateSectionDown.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionDown;
                    movePrintTemplateSectionDown.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionPreviewDownCommand.SmallIcon };
                    movePrintTemplateSectionDown.Command = this.ViewModel.MoveSectionPreviewDownCommand;
                    movePrintTemplateSectionDown.CommandParameter = item;
                    contextMenu.Items.Add(movePrintTemplateSectionDown);
                }
                else
                {
                    //move section up
                    MenuItem movePrintTemplateSectionUp = new MenuItem();
                    movePrintTemplateSectionUp.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionUp;
                    movePrintTemplateSectionUp.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionGroupPreviewUpCommand.SmallIcon };
                    movePrintTemplateSectionUp.Command = this.ViewModel.MoveSectionPreviewUpCommand;
                    movePrintTemplateSectionUp.CommandParameter = item;
                    contextMenu.Items.Add(movePrintTemplateSectionUp);

                    if (item.Number != item.Parent.Sections.Count)
                    {
                        //move section down
                        MenuItem movePrintTemplateSectionDown = new MenuItem();
                        movePrintTemplateSectionDown.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionDown;
                        movePrintTemplateSectionDown.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionGroupPreviewDownCommand.SmallIcon };
                        movePrintTemplateSectionDown.Command = this.ViewModel.MoveSectionPreviewDownCommand;
                        movePrintTemplateSectionDown.CommandParameter = item;
                        contextMenu.Items.Add(movePrintTemplateSectionDown);
                    }
                }

                //if there is more than 1 section group then allow the section to move to another group
                if (this.ViewModel.CurrentPrintTemplate.SectionGroups.Count > 1)
                {
                    //seperator
                    Separator seperator = new Separator();
                    contextMenu.Items.Add(seperator);

                    //move to section group
                    MenuItem movePrintTemplateSectionTo = new MenuItem();
                    movePrintTemplateSectionTo.Header = Message.PrintTemplateSetup_ContextMenu_MoveToSectionGroup;

                    //loop through all the section groups and add in sub menu
                    PrintTemplateSectionGroup itemGroup = (PrintTemplateSectionGroup)item.Parent;
                    foreach (PrintTemplateSectionGroup sectionGroup in this.ViewModel.CurrentPrintTemplate.SectionGroups)
                    {
                        //if not equal to the items current group
                        if (sectionGroup != itemGroup)
                        {
                            MenuItem printTemplateSectionGroup = new MenuItem();
                            String sectionGroupName = (String.IsNullOrEmpty(sectionGroup.Name)) ? Message.PrintTemplateSetup_ContextMenu_SectionGroupNoName : sectionGroup.Name;
                            printTemplateSectionGroup.Header = String.Format("{0} - {1}", sectionGroup.Number, sectionGroupName);
                            printTemplateSectionGroup.Command = this.ViewModel.MoveSectionPreviewToSectionGroupCommand;
                            printTemplateSectionGroup.CommandParameter = new PrintTemplateMoveSectionToGroupCommandParam(item, sectionGroup);
                            movePrintTemplateSectionTo.Items.Add(printTemplateSectionGroup);
                        }
                    }

                    contextMenu.Items.Add(movePrintTemplateSectionTo);
                }
            }

            //show
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Loads Print Template Section Group Context Menu
        /// </summary>
        private void PrintTemplateSectionGroup_RightClickEvent(PrintTemplateSectionGroup item)
        {
            //create the context menu
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;

            //add remove item
            MenuItem removePrintTemplateSectionGroup = new MenuItem();
            removePrintTemplateSectionGroup.Header = Message.PrintTemplateSetup_ContextMenu_RemoveSectionGroup;
            removePrintTemplateSectionGroup.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.RemoveSectionGroupPreviewCommand.SmallIcon };
            removePrintTemplateSectionGroup.Command = this.ViewModel.RemoveSectionGroupPreviewCommand;
            removePrintTemplateSectionGroup.CommandParameter = item;
            contextMenu.Items.Add(removePrintTemplateSectionGroup);

            //if more than 1 section group in the print option add move up/down commands
            if (item.Parent.SectionGroups.Count > 1)
            {
                if (item.Number == 1)
                {
                    //move section group down
                    MenuItem movePrintTemplateSectionDown = new MenuItem();
                    movePrintTemplateSectionDown.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionGroupDown;
                    movePrintTemplateSectionDown.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionGroupPreviewDownCommand.SmallIcon };
                    movePrintTemplateSectionDown.Command = this.ViewModel.MoveSectionGroupPreviewDownCommand;
                    movePrintTemplateSectionDown.CommandParameter = item;
                    contextMenu.Items.Add(movePrintTemplateSectionDown);
                }
                else
                {
                    //move section group up
                    MenuItem movePrintTemplateSectionUp = new MenuItem();
                    movePrintTemplateSectionUp.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionGroupUp;
                    movePrintTemplateSectionUp.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionGroupPreviewUpCommand.SmallIcon };
                    movePrintTemplateSectionUp.Command = this.ViewModel.MoveSectionGroupPreviewUpCommand;
                    movePrintTemplateSectionUp.CommandParameter = item;
                    contextMenu.Items.Add(movePrintTemplateSectionUp);

                    if (item.Number != item.Parent.SectionGroups.Count)
                    {
                        //move section group down
                        MenuItem movePrintTemplateSectionDown = new MenuItem();
                        movePrintTemplateSectionDown.Header = Message.PrintTemplateSetup_ContextMenu_MoveSectionGroupDown;
                        movePrintTemplateSectionDown.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.MoveSectionGroupPreviewDownCommand.SmallIcon };
                        movePrintTemplateSectionDown.Command = this.ViewModel.MoveSectionGroupPreviewDownCommand;
                        movePrintTemplateSectionDown.CommandParameter = item;
                        contextMenu.Items.Add(movePrintTemplateSectionDown);
                    }
                }
            }

            //show
            contextMenu.IsOpen = true;
        }


        #endregion

        #region Main Display

        /// <summary>
        /// Adds Print Template Component
        /// </summary>
        private void AddComponentControl(PrintTemplateComponent printTemplateComponent)
        {
            //draw the component
            if (printTemplateComponent.Type == PrintTemplateComponentType.Line)
            {
                PrintTemplateComponentLine componentNode = new PrintTemplateComponentLine(printTemplateComponent);
                printTemplateSection.Components.Add(componentNode);
            }
            else
            {
                PrintTemplateComponentNode componentNode = new PrintTemplateComponentNode(printTemplateComponent);
                printTemplateSection.Components.Add(componentNode);
            }
        }

        /// <summary>
        /// Removes Print Template Component
        /// </summary>
        private void RemoveComponentControl(PrintTemplateComponent printTemplateComponent)
        {
            //find the component
            foreach (var component in printTemplateSection.Components)
            {
                PrintTemplateComponentNode node = component as PrintTemplateComponentNode;
                if (node != null)
                {
                    if (node.UnitContext == printTemplateComponent)
                    {
                        // remove the component
                        printTemplateSection.Components.Remove(node);
                        break;
                    }
                }
                else
                {
                    PrintTemplateComponentLine line = component as PrintTemplateComponentLine;
                    if (line != null)
                    {
                        if (line.UnitContext == printTemplateComponent)
                        {
                            // remove the component
                            printTemplateSection.Components.Remove(line);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the print option section canvas to show current selection
        /// </summary>
        private void RefreshPrintTemplateSectionCanvas(PrintTemplateSection selectedPrintTemplateSection)
        {
            // Loop through each print option component and deselect it
            DeselectAllComponents();

            // clear the grid
            printTemplateSection.Components.Clear();

            // loop through each component
            foreach (PrintTemplateComponent printTemplateComponent in selectedPrintTemplateSection.Components)
            {
                //draw the component
                AddComponentControl(printTemplateComponent);
            }

            // Loop through each print option component and deselect it
            DeselectAllComponents();

            // reset the selected component
            this.ViewModel.SelectedComponent = null;

        }

        /// <summary>
        /// Position the component to mouse point location
        /// </summary>
        public void PositionComponent(PrintTemplateComponent printTemplateComponent)
        {
            // get the mouse position
            Point mousePoint = Mouse.GetPosition(this.printTemplateSection);

            // position the component to the mouse position
            printTemplateComponent.X = Convert.ToSingle(mousePoint.X / _scale);
            printTemplateComponent.Y = Convert.ToSingle(mousePoint.Y / _scale);

            // if it's a line then set it's end point too
            if (printTemplateComponent.Type == PrintTemplateComponentType.Line)
            {
                printTemplateComponent.ComponentDetails.LineEndX = printTemplateComponent.X + 100;
                printTemplateComponent.ComponentDetails.LineEndY = printTemplateComponent.Y;
            }
        }

        /// <summary>
        /// Clear selections on the section canvas
        /// </summary>
        private void ResetPrintTemplateSectionSelections(PrintTemplateSectionCanvas selectionCanvas)
        {
            // Loop through each print option component and deselect it
            DeselectAllComponents();

            // Loop through each component node and disable it from being edited unless it's a textbox.
            foreach (var component in selectionCanvas.Components)
            {
                PrintTemplateComponentNode componentNode = component as PrintTemplateComponentNode;

                if (componentNode != null)
                {
                    // Get the model object & Check component type
                    PrintTemplateComponent printTemplateComponent = (PrintTemplateComponent)componentNode.UnitContext;
                    componentNode.IsHitTestVisible = (printTemplateComponent.Type == PrintTemplateComponentType.TextBox);
                }
            }

            // reset the selected component
            this.ViewModel.SelectedComponent = null;
        }

        /// <summary>
        /// Deselects all components.
        /// </summary>
        private void DeselectAllComponents()
        {
            foreach (var child in printTemplateSection.Children)
            {
                PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                if (container != null)
                {
                    container.IsSelected = false;
                }
                else
                {
                    PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                    if (line != null) line.IsSelected = false;
                }
            }
        }

        #endregion

        #endregion

        #region Window Close

        /// <summary>
        /// On the window being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        /// <summary>
        /// Called when the window has completed its close action.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (_attachedSection != null)
                   {
                       _attachedSection.Components.BulkCollectionChanged -= ComponentList_BulkCollectionChanged;
                       _attachedSection = null;
                   }

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
        
    }

}
