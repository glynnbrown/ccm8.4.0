﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2.0)
// V8-30738 : L.Ineson
//  Created.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32340 : L.Ineson
//  Made sure that file is locked on open.
// V8-32443 : L.Ineson
//  Ammended preview command to show planogram selector when no current plan is available.
// V8-32790 : M.Pettit
//  Added ImportFromSourceType for compatibility with new backstage UI
// CCM-18454 : L.Ineson
//  Corrected CreatePlanogramPreviewWork to turn software render off as intended.
// CCM-18454 : L.Ineson
//  Print preview now ensures that the current planogram images collection is loaded if required
//  so as to prevent threading issues.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Csla.Server;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Viewmodel controller for PrintTemplateSetupWindow
    /// </summary>
    public sealed class PrintTemplateSetupViewModel : WindowViewModelBase
    {
        #region Fields

        private readonly String _previewFilePath;

        private readonly ModelPermission<PrintTemplate> _itemRepositoryPerms = new ModelPermission<PrintTemplate>();

        private readonly PrintTemplateInfoListViewModel _masterPrintTemplateInfoListView = new PrintTemplateInfoListViewModel();
        private PrintTemplate _currentPrintTemplate;

        private PrintTemplateSectionGroup _selectedSectionGroup;
        private PrintTemplateSection _selectedSection;
        private PrintTemplateComponent _selectedComponent;

        private PrintTemplateComponentToolType _selectedComponentToolType = PrintTemplateComponentToolType.Select;

        private readonly ReadOnlyCollection<Byte> _availableFontSizes;
        private readonly ReadOnlyCollection<FontFamily> _availableFonts;
        private readonly ReadOnlyCollection<ObjectFieldInfo> _availableInsertFields;

        private Int32 _entityId;
        private Planogram _currentPlanogram = null;


        private Dictionary<PrintTemplateComponent, PrintTemplateSection> _printTemplateComponentCopy = new Dictionary<PrintTemplateComponent, PrintTemplateSection>();
        const Single _scale = 2.835F;
        const Byte _cSectionNumber = 1;

        private OpenFromSourceType _openSourceType = OpenFromSourceType.OpenFromRepository;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailablePrintTemplatesProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AvailablePrintTemplates);
        public static readonly PropertyPath CurrentPrintTemplateProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.CurrentPrintTemplate);
        public static readonly PropertyPath SelectedSectionGroupProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SelectedSectionGroup);
        public static readonly PropertyPath SelectedSectionProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SelectedSection);
        public static readonly PropertyPath SelectedComponentProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SelectedComponent);
        public static readonly PropertyPath AvailableFontSizesProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AvailableFontSizes);
        public static readonly PropertyPath AvailableFontsProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AvailableFonts);
        public static readonly PropertyPath AvailableInsertFieldsProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AvailableInsertFields);
        public static readonly PropertyPath SelectedComponentToolTypeProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SelectedComponentToolType);
        public static readonly PropertyPath OpenFromSourceTypeProperty = GetPropertyPath<PrintTemplateSetupViewModel>(p => p.OpenFromSourceType);

        //Commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath RemoveComponentCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.RemoveComponentCommand);
        public static readonly PropertyPath MoveSectionGroupUpCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.MoveSectionGroupUpCommand);
        public static readonly PropertyPath MoveSectionGroupDownCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.MoveSectionGroupDownCommand);
        public static readonly PropertyPath MoveSectionUpCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.MoveSectionUpCommand);
        public static readonly PropertyPath MoveSectionDownCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.MoveSectionDownCommand);
        public static readonly PropertyPath AddSectionGroupCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AddSectionGroupCommand);
        public static readonly PropertyPath RemoveSectionGroupCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.RemoveSectionGroupCommand);
        public static readonly PropertyPath RemoveSectionGroupAndSectionsCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.RemoveSectionGroupAndSectionsCommand);
        public static readonly PropertyPath RemoveSectionGroupOnlyCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.RemoveSectionGroupOnlyCommand);
        public static readonly PropertyPath AddSectionCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.AddSectionCommand);
        public static readonly PropertyPath RemoveSectionCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.RemoveSectionCommand);
        public static readonly PropertyPath PrintPreviewCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.PrintPreviewCommand);
        public static readonly PropertyPath SetProductLabelCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SetProductLabelCommand);
        public static readonly PropertyPath SetFixtureLabelCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SetFixtureLabelCommand);
        public static readonly PropertyPath SetHighlightCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SetHighlightCommand);
        public static readonly PropertyPath SetDataSheetCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.SetDataSheetCommand);
        public static readonly PropertyPath ShowCameraSettingsCommandProperty = WpfHelper.GetPropertyPath<PrintTemplateSetupViewModel>(p => p.ShowCameraSettingsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Determnines the source of the open method selected by the user
        /// </summary>
        public OpenFromSourceType OpenFromSourceType
        {
            get { return _openSourceType; }
            set
            {
                _openSourceType = value;
                OnPropertyChanged(OpenFromSourceTypeProperty);
            }
        }

        /// <summary>
        /// Returns a readonly collection of print options available for selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<PrintTemplateInfo> AvailablePrintTemplates
        {
            get { return _masterPrintTemplateInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Returns the current selected print option
        /// </summary>
        public PrintTemplate CurrentPrintTemplate
        {
            get { return _currentPrintTemplate; }
            private set
            {
                if (_currentPrintTemplate != value)
                {
                    PrintTemplate oldValue = _currentPrintTemplate;
                    _currentPrintTemplate = value;
                    OnPropertyChanged(CurrentPrintTemplateProperty);

                    OnCurrentPrintTemplateChanged(oldValue, value);
                }
            }
        }

        /// <summary>
        /// Returns the current selected print option section group
        /// </summary>
        public PrintTemplateSectionGroup SelectedSectionGroup
        {
            get { return _selectedSectionGroup; }
            set
            {
                _selectedSectionGroup = value;
                OnPropertyChanged(SelectedSectionGroupProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current selected print option section
        /// </summary>
        public PrintTemplateSection SelectedSection
        {
            get { return _selectedSection; }
            set
            {
                _selectedSection = value;
                OnPropertyChanged(SelectedSectionProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current selected print option component
        /// </summary>
        public PrintTemplateComponent SelectedComponent
        {
            get { return _selectedComponent; }
            set
            {
                _selectedComponent = value;
                OnPropertyChanged(SelectedComponentProperty);

                OnSelectedComponentChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of available Font Sizes
        /// </summary>
        public ReadOnlyCollection<Byte> AvailableFontSizes
        {
            get { return _availableFontSizes; }
        }

        /// <summary>
        /// Returns the collection of Available Fonts
        /// </summary>
        public ReadOnlyCollection<FontFamily> AvailableFonts
        {
            get { return _availableFonts; }
        }

        /// <summary>
        /// Returns the list of fields that are available to insert into
        /// a textbox component.
        /// </summary>
        public ReadOnlyCollection<ObjectFieldInfo> AvailableInsertFields
        {
            get { return _availableInsertFields; }
        }

        /// <summary>
        /// Gets/Sets the currently selected tool type.
        /// </summary>
        public PrintTemplateComponentToolType SelectedComponentToolType
        {
            get { return _selectedComponentToolType; }
            set
            {
                _selectedComponentToolType = value;
                OnPropertyChanged(SelectedComponentToolTypeProperty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateSetupViewModel(Int32 entityId)
            : this(entityId, null)
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateSetupViewModel(Int32 entityId, Planogram currentPlanogram = null)
        {
            //Initialise fixed collections.
            _availableFontSizes = new ReadOnlyCollection<Byte>(new Byte[] { 6, 7, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 });

            _availableFonts = new List<FontFamily>() 
            { 
                new FontFamily("Arial"), 
                new FontFamily("Courier New"), 
                new FontFamily("Times New Roman"),
                new FontFamily("Symbol"), 
                new FontFamily("Arial Unicode MS") 
            }.AsReadOnly();

            _availableInsertFields =
                PrintTemplateComponentDetail.EnumerateSpecialTextFields()
                .Union(Planogram.EnumerateDisplayableFieldInfos(/*incMeta*/true))
                .ToList().AsReadOnly();

            //set the preview file path.
            _previewFilePath = Path.Combine(Path.GetTempPath(), "CCM Print Preview.pdf");

            //set perms
            _itemRepositoryPerms = (entityId > 0) ?
                new ModelPermission<PrintTemplate>(PrintTemplate.GetUserPermissions())
                : ModelPermission<PrintTemplate>.DenyAll();


            _entityId = entityId;
            _currentPlanogram = currentPlanogram;

            //available items
            if (entityId > 0) _masterPrintTemplateInfoListView.FetchForCurrentEntity();

            //Load a new item.
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new item
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //nb - do not check create permission as we 
            // can always save to file.

            return true;
        }

        private void New_Executed()
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            //load the new template
            this.CurrentPrintTemplate = PrintTemplate.NewPrintTemplate(_entityId);

            //close the backstage
            CloseRibbonBackstage();
        }

        #endregion

        #region OpenFromRepositoryCommand

        private RelayCommand<Int32?> _openFromRepositoryCommand;

        /// <summary>
        /// Loads the requested item from the repository
        /// parameter = the id of the item to open
        /// </summary>
        public RelayCommand<Int32?> OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand<Int32?>(
                        p => OpenFromRepository_Executed(p),
                        p => OpenFromRepository_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute(Int32? itemId)
        {
            //must be connected
            if (_entityId == 0)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.PrintTemplateSetup_DisabledNoRepository;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //must have a value
            if (!itemId.HasValue)
            {
                this.OpenFromRepositoryCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Int32? itemId)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;


            ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.CurrentPrintTemplate = PrintTemplate.FetchById(itemId.Value);
                this.CurrentPrintTemplate.MarkGraphAsInitialized();
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }


            //close the backstage
            CloseRibbonBackstage();

            ShowWaitCursor(false);

        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.Generic_OpenFromFile,
                        Icon = ImageResources.Open_48,
                        SmallIcon = ImageResources.Open_16
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(Object args)
        {
            String file = args as String;
            if (String.IsNullOrEmpty(file))
            {
                Boolean result =
                GetWindowService().ShowOpenFileDialog(
                CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PrintTemplate),
                    String.Format(CultureInfo.InvariantCulture, Message.PrintTemplateSetup_ImportFilter, PrintTemplate.FileExtension),
                    out file);

                if (!result) return;

                //update the session directory
                CCMClient.ViewState.SetSessionDirectory(SessionDirectory.PrintTemplate, Path.GetDirectoryName(file));
            }


            base.ShowWaitCursor(true);
            try
            {
                this.CurrentPrintTemplate = PrintTemplate.FetchByFilename(file, /*asReadOnly*/false);
                this.CurrentPrintTemplate.MarkGraphAsInitialized();
            }
            catch (Exception ex)
            {
                //make sure the file is not left locked.
                PrintTemplate.UnlockPrintTemplateByFileName(file);

                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);

        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.CurrentPrintTemplate == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //if the item would be saved to the repository then we 
            // need to check permissions.
            if (!this.CurrentPrintTemplate.IsFile
                && !this.CurrentPrintTemplate.IsNew
                && _entityId > 0)
            {

                //if item is new, must have create
                if (this.CurrentPrintTemplate.IsNew && !_itemRepositoryPerms.CanCreate)
                {
                    this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                    return false;
                }

                //if item is old, user must have edit permission
                if (!this.CurrentPrintTemplate.IsNew && !_itemRepositoryPerms.CanEdit)
                {
                    this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                    return false;
                }
            }

            //must be valid
            if (!this.CurrentPrintTemplate.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            ////bindings must be valid
            //if (!this.AreBindingsValid())
            //{
            //    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //    this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //    return false;
            //}


            return true;
        }

        private void Save_Executed()
        {
            OnSaveCommandExecute();
        }

        private Boolean OnSaveCommandExecute()
        {
            //if the template is new:
            if (this.CurrentPrintTemplate.IsNew)
            {
                //if repository is connected save there
                if (_entityId > 0)
                {
                    return SaveExistingItem();
                }
                else
                {
                    //otherwise save to file
                    return SaveAsToFile();
                }

            }
            else
            {
                //otherwise just save
                return SaveExistingItem();
            }
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveExistingItem()
        {
            //If its not a file item then check the name is still unique.
            if (!this.CurrentPrintTemplate.IsFile)
            {
                Object itemId = this.CurrentPrintTemplate.Id;

                //** check the item unique value
                String newName;


                Predicate<String> isUniqueCheck =
                       (s) =>
                       {
                           Boolean returnValue = true;

                           ShowWaitCursor(true);

                           foreach (PrintTemplateInfo info in _masterPrintTemplateInfoListView.Model)
                           {
                               if (!Object.Equals(info.Id, itemId)
                                   && info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                               {
                                   returnValue = false;
                                   break;
                               }
                           }

                           ShowWaitCursor(false);

                           return returnValue;
                       };


                Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.CurrentPrintTemplate.Name, out newName);
                if (!nameAccepted) return false;

                //set the name
                if (this.CurrentPrintTemplate.Name != newName) this.CurrentPrintTemplate.Name = newName;
                this.CurrentPrintTemplate.EntityId = _entityId;
            }

            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.CurrentPrintTemplate = this.CurrentPrintTemplate.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                RecordException(rootException);

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.CurrentPrintTemplate.Name);
                    if (itemReloadRequired)
                    {
                        this.CurrentPrintTemplate = Model.PrintTemplate.FetchById(this.CurrentPrintTemplate.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.CurrentPrintTemplate.Name, OperationType.Save);
                }

                return false;
            }

            //refresh the info list
            _masterPrintTemplateInfoListView.FetchForCurrentEntity();

            //raise the event
            RaisePrintTemplateSaved(this.CurrentPrintTemplate.Id);

            ShowWaitCursor(false);

            return true;
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SaveAsToRepository,
                        FriendlyDescription = Message.PrintTemplateSetup_SaveAsToRepository_Desc,
                        SmallIcon = ImageResources.SaveAs_16,
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            //must have a repository connection.
            if (_entityId == 0)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.PrintTemplateSetup_DisabledNoRepository;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.CurrentPrintTemplate == null)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentPrintTemplate.IsValid)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAsToRepository_Executed()
        {
            //** confirm the name to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);
                       foreach (PrintTemplateInfo info in _masterPrintTemplateInfoListView.Model)
                       {
                           if (info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }
                       base.ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            //update the name.
            this.CurrentPrintTemplate.Name = copyName;

            //** save the item
            ShowWaitCursor(true);
            try
            {
                this.CurrentPrintTemplate = this.CurrentPrintTemplate.SaveAsToRepository(_entityId);
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex.GetBaseException());
                GetWindowService().ShowErrorOccurredMessage(this.CurrentPrintTemplate.Name, OperationType.Save);
                return;
            }

            //refresh the info list
            _masterPrintTemplateInfoListView.FetchForCurrentEntity();

            //raise the event
            RaisePrintTemplateSaved(this.CurrentPrintTemplate.Id);

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(
                        p => SaveAsToFile_Executed(p),
                        p => SaveAsToFile_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SaveAsToFile,
                        FriendlyDescription = Message.PrintTemplateSetup_SaveAsToFile_Desc,
                        SmallIcon = ImageResources.SaveAs_16,
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private Boolean SaveAsToFile_CanExecute()
        {
            //may not be null
            if (this.CurrentPrintTemplate == null)
            {
                this.SaveAsToFileCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentPrintTemplate.IsValid)
            {
                this.SaveAsToFileCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAsToFile_Executed(Object args)
        {
            SaveAsToFile(args as String);
        }

        private Boolean SaveAsToFile(String filePath = null)
        {
            PrintTemplate itemToSave = this.CurrentPrintTemplate;

            if (String.IsNullOrEmpty(filePath))
            {
                //show dialog to get path
                Boolean result =
                GetWindowService().ShowSaveFileDialog(
                    this.CurrentPrintTemplate.Name,
                    CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PrintTemplate),
                    String.Format(CultureInfo.InvariantCulture, Message.PrintTemplateSetup_ImportFilter, PrintTemplate.FileExtension),
                    out filePath);

                if (!result) return false;

                //update the session directory.
                CCMClient.ViewState.SetSessionDirectory(SessionDirectory.PrintTemplate, Path.GetDirectoryName(filePath));
            }

            base.ShowWaitCursor(true);

            //save
            try
            {
                //update the item name to the chosen file one.
                itemToSave.Name = Path.GetFileNameWithoutExtension(filePath);

                this.CurrentPrintTemplate = itemToSave.SaveAsFile(filePath);

                //unlock immediately.
                PrintTemplate.UnlockPrintTemplateByFileName(filePath);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return false;
            }
            base.ShowWaitCursor(false);

            //raise the event
            RaisePrintTemplateSaved(this.CurrentPrintTemplate.Id);

            return true;
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = OnSaveCommandExecute();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = OnSaveCommandExecute();

            //if save completed close the window.
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.CurrentPrintTemplate == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.CurrentPrintTemplate.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            //user must have delete permission if a repository item.
            if (!this.CurrentPrintTemplate.IsFile && !_itemRepositoryPerms.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.CurrentPrintTemplate.ToString())) return;

            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            Model.PrintTemplate itemToDelete = this.CurrentPrintTemplate;
            itemToDelete.Delete();

            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);
                return;
            }

            //update the available items
            _masterPrintTemplateInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);

        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                        SmallIcon = ImageResources.Close_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region AddSectionCommand

        private RelayCommand _addSectionCommand;

        /// <summary>
        /// Adds a new Section beneath to the current selected Section
        /// </summary>
        public RelayCommand AddSectionCommand
        {
            get
            {
                if (_addSectionCommand == null)
                {
                    _addSectionCommand = new RelayCommand(
                        p => AddSection_Executed(), p => AddSection_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_AddSection,
                        FriendlyDescription = Message.PrintTemplateSetup_AddSection_Description,
                        Icon = ImageResources.PrintTemplateSetup_AddSection
                    };
                    RegisterCommand(_addSectionCommand);
                }
                return _addSectionCommand;
            }
        }

        private Boolean AddSection_CanExecute()
        {
            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.AddSectionCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void AddSection_Executed()
        {
            AddNewSection();
        }

        #endregion

        #region RemoveSectionCommand

        private RelayCommand _removeSectionComamnd;

        /// <summary>
        /// Removes the selected Section
        /// </summary>
        public RelayCommand RemoveSectionCommand
        {
            get
            {
                if (_removeSectionComamnd == null)
                {
                    _removeSectionComamnd = new RelayCommand(
                        p => RemoveSection_Executed(), p => RemoveSection_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSection,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSection_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSection,
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallRemoveSection
                    };
                    RegisterCommand(_removeSectionComamnd);
                }
                return _removeSectionComamnd;
            }
        }

        private Boolean RemoveSection_CanExecute()
        {
            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.RemoveSectionCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            //must have a Section selected
            if (this.SelectedSectionGroup == null || this.SelectedSection == null)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_Selected;
                return false;
            }

            //must be more than 1 section
            if (this.SelectedSectionGroup.Sections.Count == 1)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_1Section;
                return false;
            }

            return true;
        }

        private void RemoveSection_Executed()
        {
            RemoveSelectedSection(this.SelectedSection);
        }
        #endregion

        #region MoveSectionUpCommand

        private RelayCommand _moveSectionUpComamnd;

        /// <summary>
        /// Moves the Section up 1 position
        /// </summary>
        public RelayCommand MoveSectionUpCommand
        {
            get
            {
                if (_moveSectionUpComamnd == null)
                {
                    _moveSectionUpComamnd = new RelayCommand(
                        p => MoveSectionUp_Executed(),
                        p => MoveSectionUp_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_MoveSectionUp,
                        FriendlyDescription = Message.PrintTemplateSetup_MoveSectionUp_Decription,
                        SmallIcon = ImageResources.PrintTemplateSetup_MoveUp
                    };
                    RegisterCommand(_moveSectionUpComamnd);
                }
                return _moveSectionUpComamnd;
            }
        }


        private Boolean MoveSectionUp_CanExecute()
        {
            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.MoveSectionUpCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            //must have a Section selected
            if (this.SelectedSection == null)
            {
                this.MoveSectionUpCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_Selected;
                return false;
            }

            //must have more than 1 section to move anywhere
            if (this.SelectedSection.Parent.Sections.Count == 1)
            {
                this.MoveSectionUpCommand.DisabledReason = Message.PrintTemplateSetup_MoveDisabledReason_1Section;
                return false;
            }

            //can't move section 1 up
            if (this.SelectedSection.Number == 1)
            {
                this.MoveSectionUpCommand.DisabledReason = Message.PrintTemplateSetup_MoveUpDisabledReason_1stSection;
                return false;
            }

            return true;
        }

        private void MoveSectionUp_Executed()
        {
            MoveSection(this.SelectedSection, (Byte)(this.SelectedSection.Number - 1));
        }
        #endregion

        #region MoveSectionDownCommand

        private RelayCommand _moveSectionDownComamnd;

        /// <summary>
        /// Moves the Section Down 1 position
        /// </summary>
        public RelayCommand MoveSectionDownCommand
        {
            get
            {
                if (_moveSectionDownComamnd == null)
                {
                    _moveSectionDownComamnd = new RelayCommand(
                        p => MoveSectionDown_Executed(),
                        p => MoveSectionDown_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_MoveSectionDown,
                        FriendlyDescription = Message.PrintTemplateSetup_MoveSectionDown_Decription,
                        SmallIcon = ImageResources.PrintTemplateSetup_MoveDown
                    };
                    RegisterCommand(_moveSectionDownComamnd);
                }
                return _moveSectionDownComamnd;
            }
        }


        private Boolean MoveSectionDown_CanExecute()
        {
            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.MoveSectionDownCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            //must have a Section selected
            if (this.SelectedSection == null)
            {
                this.MoveSectionDownCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_Selected;
                return false;
            }

            if (this.SelectedSection.Parent == null)
            {
                return false;
            }

            //must have more than 1 section to move anywhere
            if (this.SelectedSection.Parent.Sections.Count == 1)
            {
                this.MoveSectionDownCommand.DisabledReason = Message.PrintTemplateSetup_MoveDisabledReason_1Section;
                return false;
            }

            //can't move section last section down
            if (this.SelectedSection.Number == this.SelectedSection.Parent.Sections.Count)
            {
                this.MoveSectionDownCommand.DisabledReason = Message.PrintTemplateSetup_MoveDownDisabledReason_LastSection;
                return false;
            }

            return true;
        }

        private void MoveSectionDown_Executed()
        {
            MoveSection(this.SelectedSection, (Byte)(this.SelectedSection.Number + 1));
        }
        #endregion

        #region RemoveSectionPreviewCommand

        private RelayCommand _removeSectionPreviewComamnd;

        /// <summary>
        /// Removes the Section
        /// </summary>
        public RelayCommand RemoveSectionPreviewCommand
        {
            get
            {
                if (_removeSectionPreviewComamnd == null)
                {
                    _removeSectionPreviewComamnd = new RelayCommand(
                        p => RemoveSectionPreview_Executed((PrintTemplateSection)p),
                        p => RemoveSectionPreview_CanExecute(p))
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSection,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSection_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSection,
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallRemoveSection
                    };
                    RegisterCommand(_removeSectionPreviewComamnd);
                }
                return _removeSectionPreviewComamnd;
            }
        }


        private Boolean RemoveSectionPreview_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            PrintTemplateSection sectionToRemove = (PrintTemplateSection)parameter;

            //must be more than 1 section in the group
            if (sectionToRemove.Parent.Sections.Count == 1)
            {
                this.RemoveSectionPreviewCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_1Section;
                return false;
            }

            return true;
        }

        private void RemoveSectionPreview_Executed(PrintTemplateSection sectionToRemove)
        {
            base.ShowWaitCursor(true);

            //if the section preview to be removed is the selected section,
            //then the action is the same as the ribbon command
            if (sectionToRemove == this.SelectedSection)
            {
                RemoveSelectedSection(sectionToRemove);
            }
            else
            {
                //find the section group the section is child of
                foreach (PrintTemplateSectionGroup sectionGroup in this.CurrentPrintTemplate.SectionGroups)
                {
                    if (sectionGroup.Sections.Contains(sectionToRemove))
                    {
                        Byte selectedSectionNumber = sectionToRemove.Number;

                        // remove section from the print option
                        sectionGroup.Sections.Remove(sectionToRemove);

                        // loop through all the other sections and decrease the section number
                        foreach (PrintTemplateSection section in sectionGroup.Sections)
                        {
                            // if section number is equal to or more than the removed section then decrease
                            if (section.Number > selectedSectionNumber)
                            {
                                section.Number--;
                            }
                        }
                    }
                }
            }

            base.ShowWaitCursor(false);
        }
        #endregion

        #region MoveSectionPreviewUpCommand

        private RelayCommand _moveSectionPreviewUpComamnd;

        /// <summary>
        /// Moves the Section Preview up 1 position
        /// </summary>
        public RelayCommand MoveSectionPreviewUpCommand
        {
            get
            {
                if (_moveSectionPreviewUpComamnd == null)
                {
                    _moveSectionPreviewUpComamnd = new RelayCommand(
                        p => MoveSectionPreviewUp_Executed((PrintTemplateSection)p),
                        p => MoveSectionPreviewUp_CanExecute(p))
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_MovePreviewUp
                    };
                    RegisterCommand(_moveSectionPreviewUpComamnd);
                }
                return _moveSectionPreviewUpComamnd;
            }
        }

        private Boolean MoveSectionPreviewUp_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }


            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.MoveSectionPreviewUpCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void MoveSectionPreviewUp_Executed(PrintTemplateSection sectionToMove)
        {
            MoveSectionPreview(sectionToMove, (Byte)(sectionToMove.Number - 1));
        }
        #endregion

        #region MoveSectionPreviewDownCommand

        private RelayCommand _moveSectionPreviewDownComamnd;

        /// <summary>
        /// Moves the Section Preview Down 1 position
        /// </summary>
        public RelayCommand MoveSectionPreviewDownCommand
        {
            get
            {
                if (_moveSectionPreviewDownComamnd == null)
                {
                    _moveSectionPreviewDownComamnd = new RelayCommand(
                        p => MoveSectionPreviewDown_Executed((PrintTemplateSection)p),
                        p => MoveSectionPreviewDown_CanExecute(p))
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_MovePreviewDown
                    };
                    RegisterCommand(_moveSectionPreviewDownComamnd);
                }
                return _moveSectionPreviewDownComamnd;
            }
        }


        private Boolean MoveSectionPreviewDown_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.MoveSectionPreviewDownCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void MoveSectionPreviewDown_Executed(PrintTemplateSection sectionToMove)
        {
            MoveSectionPreview(sectionToMove, (Byte)(sectionToMove.Number + 1));
        }
        #endregion

        #region MoveSectionPreviewToSectionGroupCommand

        private RelayCommand _moveSectionPreviewToSectionGroupComamnd;

        /// <summary>
        /// Moves the Section Preview to another section group
        /// </summary>
        public RelayCommand MoveSectionPreviewToSectionGroupCommand
        {
            get
            {
                if (_moveSectionPreviewToSectionGroupComamnd == null)
                {
                    _moveSectionPreviewToSectionGroupComamnd = new RelayCommand(
                        p => MoveSectionPreviewToSectionGroup_Executed((PrintTemplateMoveSectionToGroupCommandParam)p),
                        p => MoveSectionPreviewToSectionGroup_CanExecute(p));
                    RegisterCommand(_moveSectionPreviewToSectionGroupComamnd);
                }
                return _moveSectionPreviewToSectionGroupComamnd;
            }
        }


        private Boolean MoveSectionPreviewToSectionGroup_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.MoveSectionPreviewToSectionGroupCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void MoveSectionPreviewToSectionGroup_Executed(PrintTemplateMoveSectionToGroupCommandParam parameter)
        {
            PrintTemplateSection sectionToMove = parameter.PrintTemplateSection as PrintTemplateSection;
            PrintTemplateSectionGroup sectionGroup = parameter.PrintTemplateSectionGroup as PrintTemplateSectionGroup;
            MoveSectionPreviewToGroup(sectionToMove, sectionGroup);
        }
        #endregion

        #region AddSectionGroupCommand

        private RelayCommand _addSectionGroupCommand;

        /// <summary>
        /// Adds a new Section Group beneath to the current selected Section
        /// </summary>
        public RelayCommand AddSectionGroupCommand
        {
            get
            {
                if (_addSectionGroupCommand == null)
                {
                    _addSectionGroupCommand = new RelayCommand(
                        p => AddSectionGroup_Executed(), p => AddSectionGroup_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_AddSectionGroup,
                        FriendlyDescription = Message.PrintTemplateSetup_AddSectionGroup_Description,
                        Icon = ImageResources.PrintTemplateSetup_AddSectionGroup
                    };
                    RegisterCommand(_addSectionGroupCommand);
                }
                return _addSectionGroupCommand;
            }
        }

        private Boolean AddSectionGroup_CanExecute()
        {
            //must have an option loaded.
            if (this.CurrentPrintTemplate == null)
            {
                this.AddSectionGroupCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void AddSectionGroup_Executed()
        {
            AddNewSectionGroup(this.SelectedSection);
        }

        #endregion

        #region RemoveSectionGroupCommand

        private RelayCommand _removeSectionGroupComamnd;

        /// <summary>
        /// Removes the selected Section Group
        /// </summary>
        public RelayCommand RemoveSectionGroupCommand
        {
            get
            {
                if (_removeSectionGroupComamnd == null)
                {
                    _removeSectionGroupComamnd = new RelayCommand(
                        p => RemoveSectionGroupAndSections_Executed(),
                        p => RemoveSectionGroup_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSectionGroup,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSectionGroup_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSectionGroup
                    };
                    RegisterCommand(_removeSectionGroupComamnd);
                }
                return _removeSectionGroupComamnd;
            }
        }


        private Boolean RemoveSectionGroup_CanExecute()
        {
            //must have a Section selected
            if (this.SelectedSectionGroup == null)
            {
                this.RemoveSectionGroupCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_Selected;
                return false;
            }

            //must be more than 1 section Group
            if (this.CurrentPrintTemplate.SectionGroups.Count == 1)
            {
                this.RemoveSectionGroupCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_1SectionGroup;
                return false;
            }

            return true;
        }

        #endregion

        #region RemoveSectionGroupAndSectionsCommand

        private RelayCommand _removeSectionGroupAndSectionsComamnd;

        /// <summary>
        /// Removes the selected Section Group and relating sections
        /// </summary>
        public RelayCommand RemoveSectionGroupAndSectionsCommand
        {
            get
            {
                if (_removeSectionGroupAndSectionsComamnd == null)
                {
                    _removeSectionGroupAndSectionsComamnd = new RelayCommand(
                        p => RemoveSectionGroupAndSections_Executed(), p => RemoveSectionGroupAndSections_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSectionGroup,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSectionGroupAndSections_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSectionGroup
                    };
                    RegisterCommand(_removeSectionGroupAndSectionsComamnd);
                }
                return _removeSectionGroupAndSectionsComamnd;
            }
        }


        private Boolean RemoveSectionGroupAndSections_CanExecute()
        {
            //must have a Section selected
            if (this.SelectedSectionGroup == null)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_Selected;
                return false;
            }

            //must be more than 1 section Group
            if (this.CurrentPrintTemplate.SectionGroups.Count == 1)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_1SectionGroup;
                return false;
            }

            return true;
        }

        private void RemoveSectionGroupAndSections_Executed()
        {
            RemoveSelectedSectionGroupAndSections(this.SelectedSectionGroup);
        }
        #endregion

        #region RemoveSectionGroupOnlyCommand

        private RelayCommand _removeSectionGroupOnlyCommand;

        /// <summary>
        /// Removes the selected Section Group and moves the relating sections to another section
        /// </summary>
        public RelayCommand RemoveSectionGroupOnlyCommand
        {
            get
            {
                if (_removeSectionGroupOnlyCommand == null)
                {
                    _removeSectionGroupOnlyCommand = new RelayCommand(
                        p => RemoveSectionGroupOnly_Executed(), p => RemoveSectionGroupOnly_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSectionGroup,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSectionGroupOnly_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSectionGroup
                    };
                    RegisterCommand(_removeSectionGroupOnlyCommand);
                }
                return _removeSectionGroupOnlyCommand;
            }
        }


        private Boolean RemoveSectionGroupOnly_CanExecute()
        {
            //must have a Section selected
            if (this.SelectedSectionGroup == null)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_Selected;
                return false;
            }

            //must be more than 1 section Group
            if (this.CurrentPrintTemplate.SectionGroups.Count == 1)
            {
                this.RemoveSectionCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_1SectionGroup;
                return false;
            }

            return true;
        }

        private void RemoveSectionGroupOnly_Executed()
        {
            RemoveSelectedSectionGroupOnly(this.SelectedSectionGroup);
        }
        #endregion

        #region MoveSectionGroupUpCommand

        private RelayCommand _moveSectionGroupUpComamnd;

        /// <summary>
        /// Moves the Section Group up 1 position
        /// </summary>
        public RelayCommand MoveSectionGroupUpCommand
        {
            get
            {
                if (_moveSectionGroupUpComamnd == null)
                {
                    _moveSectionGroupUpComamnd = new RelayCommand(
                        p => MoveSectionGroupUp_Executed(),
                        p => MoveSectionGroupUp_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_MoveSectionUp,
                        FriendlyDescription = Message.PrintTemplateSetup_MoveSectionGroupUp_Decription,
                        Icon = ImageResources.PrintTemplateSetup_MoveUp
                    };
                    RegisterCommand(_moveSectionGroupUpComamnd);
                }
                return _moveSectionGroupUpComamnd;
            }
        }


        private Boolean MoveSectionGroupUp_CanExecute()
        {
            //must have a Section selected
            if (this.SelectedSectionGroup == null)
            {
                this.MoveSectionGroupUpCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_Selected;
                return false;
            }

            if (this.SelectedSectionGroup.Parent == null)
            {
                return false;
            }

            //must have more than 1 section group to move anywhere
            if (this.SelectedSectionGroup.Parent.SectionGroups.Count == 1)
            {
                this.MoveSectionGroupUpCommand.DisabledReason = Message.PrintTemplateSetup_MoveDisabledReason_1SectionGroup;
                return false;
            }

            //can't move section group 1 up
            if (this.SelectedSectionGroup.Number == 1)
            {
                this.MoveSectionGroupUpCommand.DisabledReason = Message.PrintTemplateSetup_MoveUpDisabledReason_1stSectionGroup;
                return false;
            }

            return true;
        }

        private void MoveSectionGroupUp_Executed()
        {
            MoveSectionGroup(this.SelectedSectionGroup, (Byte)(this.SelectedSectionGroup.Number - 1));
        }
        #endregion

        #region MoveSectionGroupDownCommand

        private RelayCommand _moveSectionGroupDownComamnd;

        /// <summary>
        /// Moves the Section Group Down 1 position
        /// </summary>
        public RelayCommand MoveSectionGroupDownCommand
        {
            get
            {
                if (_moveSectionGroupDownComamnd == null)
                {
                    _moveSectionGroupDownComamnd = new RelayCommand(
                        p => MoveSectionGroupDown_Executed(),
                        p => MoveSectionGroupDown_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_MoveSectionDown,
                        FriendlyDescription = Message.PrintTemplateSetup_MoveSectionGroupDown_Decription,
                        Icon = ImageResources.PrintTemplateSetup_MoveDown
                    };
                    RegisterCommand(_moveSectionGroupDownComamnd);
                }
                return _moveSectionGroupDownComamnd;
            }
        }


        private Boolean MoveSectionGroupDown_CanExecute()
        {
            //must have a Section group selected
            if (this.SelectedSectionGroup == null)
            {
                this.MoveSectionGroupDownCommand.DisabledReason = Message.PrintTemplateSetup_SectionGroupCommandDisabledReason_Selected;
                return false;
            }

            if (this.SelectedSectionGroup.Parent == null)
            {
                return false;
            }

            //must have more than 1 section group to move anywhere
            if (this.SelectedSectionGroup.Parent.SectionGroups.Count == 1)
            {
                this.MoveSectionGroupDownCommand.DisabledReason = Message.PrintTemplateSetup_MoveDisabledReason_1SectionGroup;
                return false;
            }

            //can't move section last section down
            if (this.SelectedSectionGroup.Number == this.SelectedSectionGroup.Parent.SectionGroups.Count)
            {
                this.MoveSectionGroupDownCommand.DisabledReason = Message.PrintTemplateSetup_MoveDownDisabledReason_LastSectionGroup;
                return false;
            }

            return true;
        }

        private void MoveSectionGroupDown_Executed()
        {
            MoveSectionGroup(this.SelectedSectionGroup, (Byte)(this.SelectedSectionGroup.Number + 1));
        }
        #endregion

        #region RemoveSectionGroupPreviewCommand

        private RelayCommand _removeSectionGroupPreviewComamnd;

        /// <summary>
        /// Removes the Section Group
        /// </summary>
        public RelayCommand RemoveSectionGroupPreviewCommand
        {
            get
            {
                if (_removeSectionGroupPreviewComamnd == null)
                {
                    _removeSectionGroupPreviewComamnd = new RelayCommand(
                        p => RemoveSectionGroupPreview_Executed((PrintTemplateSectionGroup)p),
                        p => RemoveSectionGroupPreview_CanExecute(p))
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveSectionGroup,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveSectionGroup_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveSectionGroup,
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallRemoveSection
                    };
                    RegisterCommand(_removeSectionGroupPreviewComamnd);
                }
                return _removeSectionGroupPreviewComamnd;
            }
        }


        private Boolean RemoveSectionGroupPreview_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            PrintTemplateSectionGroup sectionGroupToRemove = (PrintTemplateSectionGroup)parameter;

            //must be more than 1 section in the group
            if (sectionGroupToRemove.Parent.SectionGroups.Count == 1)
            {
                this.RemoveSectionGroupPreviewCommand.DisabledReason = Message.PrintTemplateSetup_SectionCommandDisabledReason_1Section;
                return false;
            }

            return true;
        }

        private void RemoveSectionGroupPreview_Executed(PrintTemplateSectionGroup sectionGroupToRemove)
        {
            RemoveSectionGroupPreview(sectionGroupToRemove);
        }
        #endregion

        #region MoveSectionGroupPreviewUpCommand

        private RelayCommand _moveSectionGroupPreviewUpComamnd;

        /// <summary>
        /// Moves the Section Group Preview up 1 position
        /// </summary>
        public RelayCommand MoveSectionGroupPreviewUpCommand
        {
            get
            {
                if (_moveSectionGroupPreviewUpComamnd == null)
                {
                    _moveSectionGroupPreviewUpComamnd = new RelayCommand(
                        p => MoveSectionGroupPreviewUp_Executed((PrintTemplateSectionGroup)p),
                        p => MoveSectionGroupPreviewUp_CanExecute(p))
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_MovePreviewUp
                    };
                    RegisterCommand(_moveSectionGroupPreviewUpComamnd);
                }
                return _moveSectionGroupPreviewUpComamnd;
            }
        }


        private Boolean MoveSectionGroupPreviewUp_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return true;
        }

        private void MoveSectionGroupPreviewUp_Executed(PrintTemplateSectionGroup sectionGroupToMove)
        {
            MoveSectionGroupPreview(sectionGroupToMove, (Byte)(sectionGroupToMove.Number - 1));
        }
        #endregion

        #region MoveSectionGroupPreviewDownCommand

        private RelayCommand _moveSectionGroupPreviewDownComamnd;

        /// <summary>
        /// Moves the Section Group Preview Down 1 position
        /// </summary>
        public RelayCommand MoveSectionGroupPreviewDownCommand
        {
            get
            {
                if (_moveSectionGroupPreviewDownComamnd == null)
                {
                    _moveSectionGroupPreviewDownComamnd = new RelayCommand(
                        p => MoveSectionGroupPreviewDown_Executed((PrintTemplateSectionGroup)p),
                        p => MoveSectionGroupPreviewDown_CanExecute(p))
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_MovePreviewDown
                    };
                    RegisterCommand(_moveSectionGroupPreviewDownComamnd);
                }
                return _moveSectionGroupPreviewDownComamnd;
            }
        }


        private Boolean MoveSectionGroupPreviewDown_CanExecute(Object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return true;
        }

        private void MoveSectionGroupPreviewDown_Executed(PrintTemplateSectionGroup sectionGroupToMove)
        {
            MoveSectionGroupPreview(sectionGroupToMove, (Byte)(sectionGroupToMove.Number + 1));
        }
        #endregion

        #region RemoveComponentCommand

        private RelayCommand _removeComponentComamnd;

        /// <summary>
        /// Removes the selected Component
        /// </summary>
        public RelayCommand RemoveComponentCommand
        {
            get
            {
                if (_removeComponentComamnd == null)
                {
                    _removeComponentComamnd = new RelayCommand(
                        p => RemoveComponent_Executed(), p => RemoveComponent_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_RemoveComponent,
                        FriendlyDescription = Message.PrintTemplateSetup_RemoveComponent_Description,
                        Icon = ImageResources.PrintTemplateSetup_RemoveComponent,
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallRemoveComponent
                    };
                    RegisterCommand(_removeComponentComamnd);
                }
                return _removeComponentComamnd;
            }
        }


        private Boolean RemoveComponent_CanExecute()
        {
            //must have a Component selected
            if (this.SelectedComponent == null)
            {
                this.RemoveComponentCommand.DisabledReason = Message.PrintTemplateSetup_ComponentCommandDisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveComponent_Executed()
        {
            base.ShowWaitCursor(true);

            PrintTemplateComponent ComponentToRemove = this.SelectedComponent;

            //remove the Component and select its parent
            this.SelectedSection.Components.Remove(ComponentToRemove);

            //select the parent Component of that removed
            this.SelectedComponent = null;

            base.ShowWaitCursor(false);
        }
        #endregion

        #region CopyComponentCommand

        private RelayCommand _copyComponentComamnd;

        /// <summary>
        /// Copy the selected Component
        /// </summary>
        public RelayCommand CopyComponentCommand
        {
            get
            {
                if (_copyComponentComamnd == null)
                {
                    _copyComponentComamnd = new RelayCommand(
                        p => CopyComponent_Executed(), p => CopyComponent_CanExecute())
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallCopyComponent
                    };
                    RegisterCommand(_copyComponentComamnd);
                }
                return _copyComponentComamnd;
            }
        }


        private Boolean CopyComponent_CanExecute()
        {
            //must have a Component selected
            if (this.SelectedComponent == null)
            {
                this.CopyComponentCommand.DisabledReason = Message.PrintTemplateSetup_ComponentCommandDisabledReason;
                return false;
            }

            return true;
        }

        private void CopyComponent_Executed()
        {
            //Clear the currently copied components
            _printTemplateComponentCopy.Clear();

            //Add the selected component to the dictionary
            _printTemplateComponentCopy.Add(this.SelectedComponent.Copy(), this.SelectedSection);
        }
        #endregion

        #region PasteComponentCommand

        private RelayCommand _pasteComponentComamnd;

        /// <summary>
        /// Paste the copied Component
        /// </summary>
        public RelayCommand PasteComponentCommand
        {
            get
            {
                if (_pasteComponentComamnd == null)
                {
                    _pasteComponentComamnd = new RelayCommand(
                        p => PasteComponent_Executed(p), p => PasteComponent_CanExecute())
                    {
                        SmallIcon = ImageResources.PrintTemplateSetup_SmallPasteComponent
                    };
                    RegisterCommand(_pasteComponentComamnd);
                }
                return _pasteComponentComamnd;
            }
        }


        private Boolean PasteComponent_CanExecute()
        {
            //must have a Component copied
            if (_printTemplateComponentCopy.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void PasteComponent_Executed(object param)
        {
            Dictionary<PrintTemplateComponent, PrintTemplateSection> copiedDictionary = new Dictionary<PrintTemplateComponent, PrintTemplateSection>();

            // Loop through the copied components and add to canvas
            foreach (KeyValuePair<PrintTemplateComponent, PrintTemplateSection> copiedComponent in _printTemplateComponentCopy)
            {
                // Set the variables
                PrintTemplateSection sectionOfCopiedComponent = copiedComponent.Value;
                PrintTemplateComponent componentCopy = copiedComponent.Key;

                // Create a new copy of the component for the clipboard
                PrintTemplateComponent newCopiedComponent = componentCopy.Copy();

                // See whether the selected section is the same as the copied component,
                // if it is then position to mouse point
                if (sectionOfCopiedComponent == this.SelectedSection)
                {
                    // Position the component in the mouse location
                    Point mousePoint = (Point)param;
                    componentCopy.X = Convert.ToSingle(mousePoint.X / _scale);
                    componentCopy.Y = Convert.ToSingle(mousePoint.Y / _scale);
                }

                // Add the copied component to the selected section
                this.SelectedSection.Components.Add(componentCopy);

                // Add to copy of dictionary
                copiedDictionary.Add(newCopiedComponent, sectionOfCopiedComponent);
            }

            //Finally clear the dictionary and re-populate
            _printTemplateComponentCopy.Clear();

            //Loop through each of the copied components and re-populate
            foreach (KeyValuePair<PrintTemplateComponent, PrintTemplateSection> copiedComponent in copiedDictionary)
            {
                _printTemplateComponentCopy.Add(copiedComponent.Key, copiedComponent.Value);
            }
        }
        #endregion

        #region InsertTextCommand

        private RelayCommand _insertTextComamnd;

        /// <summary>
        /// Insert text placeholders to textbox
        /// </summary>
        public RelayCommand InsertTextCommand
        {
            get
            {
                if (_insertTextComamnd == null)
                {
                    _insertTextComamnd = new RelayCommand(
                        p => InsertText_Executed(p), p => InsertText_CanExecute());
                    RegisterCommand(_insertTextComamnd);
                }
                return _insertTextComamnd;
            }
        }

        private Boolean InsertText_CanExecute()
        {
            //must have a Component selected
            if (this.SelectedComponent == null)
            {
                return false;
            }

            //selected component must be a textbox
            if (this.SelectedComponent.Type != PrintTemplateComponentType.TextBox)
            {
                return false;
            }

            return true;
        }

        private void InsertText_Executed(object param)
        {
            ObjectFieldInfo fieldToInsert = param as ObjectFieldInfo;
            if (fieldToInsert == null) return;

            this.SelectedComponent.ComponentDetails.TextBoxText =
                String.Format("{0}{1}", this.SelectedComponent.ComponentDetails.TextBoxText,
                fieldToInsert.FieldPlaceholder);
        }
        #endregion

        #region PrintPreviewCommand

        private RelayCommand _printPreviewComamnd;

        /// <summary>
        /// Shows a print preview using the current print option
        /// </summary>
        public RelayCommand PrintPreviewCommand
        {
            get
            {
                if (_printPreviewComamnd == null)
                {
                    _printPreviewComamnd = new RelayCommand(
                        p => PrintPreview_Executed(), p => PrintPreview_CanExecute())
                    {
                        FriendlyName = (_currentPlanogram != null) ?
                            Message.PrintTemplateSetup_PrintPreview_Current : Message.PrintTemplateSetup_PrintPreview,
                        FriendlyDescription = Message.PrintTemplateSetup_PrintPreview_Description,
                        Icon = ImageResources.PrintTemplateSetup_PrintPreview
                    };
                    RegisterCommand(_printPreviewComamnd);
                }
                return _printPreviewComamnd;
            }
        }

        private Boolean PrintPreview_CanExecute()
        {
            if (this.CurrentPrintTemplate == null)
            {
                this.PrintPreviewCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            //must have a current plangram or be connected to the repository.
            if (_currentPlanogram == null && _entityId == 0)
            {
                this.PrintPreviewCommand.DisabledReason = Message.PrintTemplateSetup_PrintPreviewCommand_DisabledReason;
                return false;
            }

            return true;
        }

        private void PrintPreview_Executed()
        {
            Planogram previewPlanogram = _currentPlanogram;

            if(_currentPlanogram != null)
            {
                //if we are using the current planogram and need to display images
                // then force the collection to load first.
                if(this.CurrentPrintTemplate.RequiresPlanogramImages())
                {
                    base.ShowWaitCursor(true);
                    PlanogramImageList forceImmediateImageLoad = _currentPlanogram.Images;
                    base.ShowWaitCursor(false);
                }
            }


            //if there is no current planogram then show the repository selector.
            if (previewPlanogram == null && _entityId != 0)
            {
                using(PlanogramSelectorViewModel planSelectorVm = 
                    new PlanogramSelectorViewModel(CCMClient.ViewState.GetSessionDirectory(SessionDirectory.CustomColumnLayout), _entityId))
                {

                    GetWindowService().ShowDialog<PlanogramSelectorWindow>(planSelectorVm);
                    if (planSelectorVm.DialogResult != true || planSelectorVm.SelectedPlanogramInfo == null) return;

                    //fetch the planogram
                    try
                    {
                        Package pk = Package.FetchById(planSelectorVm.SelectedPlanogramInfo.Id);
                        if(pk != null) previewPlanogram = pk.Planograms.FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        RecordException(ex);
                        GetWindowService().ShowErrorOccurredMessage(planSelectorVm.SelectedPlanogramInfo.Name, OperationType.Open);
                    }
                }
            }
            if (previewPlanogram == null) return;


            //check if file has been generated before and delete if it has
            String pdfPath = _previewFilePath;
            if (!TryDeletePreviewFile(/*showUserWarning*/true)) return;

            //Run the work to create the preview.
            CreatePlanogramPreviewWork work = new CreatePlanogramPreviewWork(this.CurrentPrintTemplate, previewPlanogram, pdfPath);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);

        }

        /// <summary>
        /// Carries out the asynchronous work of creating a planogram preview
        /// </summary>
        private sealed class CreatePlanogramPreviewWork : IModalBusyWork
        {
            #region Fields
            private PrintTemplate _template;
            private Planogram _planogram;
            private String _pdfPath;
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return false; }
            }

            public Boolean SupportsCancellation
            {
                get { return false; }
            }

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            public CreatePlanogramPreviewWork(PrintTemplate template, Planogram plan, String path)
            {
                _template = template;
                _planogram = plan;
                _pdfPath = path;

            }
            #endregion

            #region Methods

            public object GetWorkerArgs()
            {
                return new Object[] { _template, _planogram, _pdfPath };
            }

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                Object[] args = (Object[])e.Argument;
                PrintTemplate template = (PrintTemplate)args[0];
                Planogram plan = (Planogram)args[1];
                String pdfPath = (String)args[2];

                //create the pdf
                var process = new Processes.Reports.Planogram.Process(template, plan);

                //we are working off the client so turn off the software render.
                process.IsSoftwareRender = false;

                process = ProcessFactory.Execute<Processes.Reports.Planogram.Process>(process);

                //save pdf down
                System.IO.File.WriteAllBytes(pdfPath, process.ReportData);
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                //nothing to report.
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorMessage(null, e.Error.GetBaseException().Message);
                }
                else if (!e.Cancelled)
                {
                    //open the pdf
                    if (!String.IsNullOrWhiteSpace(_pdfPath))
                    {
                        //check it was saved
                        if (System.IO.File.Exists(_pdfPath))
                        {
                            System.Diagnostics.Process.Start(_pdfPath);
                        }
                    }
                }
            }

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowIndeterminateBusy(
                      Message.PrintTemplateSetup_PrintPreviewBusy_Header,
                      Message.PrintTemplateSetup_PrintPreviewBusy_Desc);
            }

            #endregion
        }

        #endregion


        #region SetProductLabelCommand

        private RelayCommand _setProductLabelComamnd;

        /// <summary>
        /// Shows the dialog to allow the user to set the 
        /// linked ProductLabel for the selected
        /// planogram component.
        /// </summary>
        public RelayCommand SetProductLabelCommand
        {
            get
            {
                if (_setProductLabelComamnd == null)
                {
                    _setProductLabelComamnd = new RelayCommand(
                        p => SetProductLabel_Executed(),
                        p => SetProductLabel_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SetProductLabel,
                        FriendlyDescription = Message.PrintTemplateSetup_SetProductLabel_Desc,
                        Icon = ImageResources.PrintTemplateSetup_SetProductLabel
                    };
                    RegisterCommand(_setProductLabelComamnd);
                }
                return _setProductLabelComamnd;
            }
        }

        private Boolean SetProductLabel_CanExecute()
        {
            if (this.SelectedComponent == null
                || this.SelectedComponent.Type != PrintTemplateComponentType.Planogram)
            {
                return false;
            }


            return true;
        }

        private void SetProductLabel_Executed()
        {
            PrintTemplateLabelSelectorViewModel vm =
                new PrintTemplateLabelSelectorViewModel(this.SelectedComponent.ComponentDetails, LabelType.Product);

            GetWindowService().ShowDialog<PrintTemplateLabelSelector>(vm);
        }

        #endregion

        #region SetFixtureLabelCommand

        private RelayCommand _setFixtureLabelComamnd;

        /// <summary>
        /// Shows the dialog to allow the user to set the 
        /// linked FixtureLabel for the selected
        /// planogram component.
        /// </summary>
        public RelayCommand SetFixtureLabelCommand
        {
            get
            {
                if (_setFixtureLabelComamnd == null)
                {
                    _setFixtureLabelComamnd = new RelayCommand(
                        p => SetFixtureLabel_Executed(),
                        p => SetFixtureLabel_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SetFixtureLabel,
                        FriendlyDescription = Message.PrintTemplateSetup_SetFixtureLabel_Desc,
                        Icon = ImageResources.PrintTemplateSetup_SetFixtureLabel
                    };
                    RegisterCommand(_setFixtureLabelComamnd);
                }
                return _setFixtureLabelComamnd;
            }
        }

        private Boolean SetFixtureLabel_CanExecute()
        {
            if (this.SelectedComponent == null
                || this.SelectedComponent.Type != PrintTemplateComponentType.Planogram)
            {
                return false;
            }

            return true;
        }

        private void SetFixtureLabel_Executed()
        {
            PrintTemplateLabelSelectorViewModel vm =
                new PrintTemplateLabelSelectorViewModel(this.SelectedComponent.ComponentDetails, LabelType.Fixture);

            GetWindowService().ShowDialog<PrintTemplateLabelSelector>(vm);
        }

        #endregion

        #region SetHighlightCommand

        private RelayCommand _setHighlightComamnd;

        /// <summary>
        /// Shows the dialog to allow the user to set the 
        /// linked HighlightLabel for the selected
        /// planogram component.
        /// </summary>
        public RelayCommand SetHighlightCommand
        {
            get
            {
                if (_setHighlightComamnd == null)
                {
                    _setHighlightComamnd = new RelayCommand(
                        p => SetHighlight_Executed(),
                        p => SetHighlight_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SetHighlight,
                        FriendlyDescription = Message.PrintTemplateSetup_SetHighlight_Desc,
                        Icon = ImageResources.PrintTemplateSetup_SetHighlight
                    };
                    RegisterCommand(_setHighlightComamnd);
                }
                return _setHighlightComamnd;
            }
        }

        private Boolean SetHighlight_CanExecute()
        {
            if (this.SelectedComponent == null
                || this.SelectedComponent.Type != PrintTemplateComponentType.Planogram)
            {
                return false;
            }

            return true;
        }

        private void SetHighlight_Executed()
        {
            PrintTemplateHighlightSelectorViewModel vm =
                new PrintTemplateHighlightSelectorViewModel(this.SelectedComponent.ComponentDetails);

            GetWindowService().ShowDialog<PrintTemplateHighlightSelector>(vm);
        }

        #endregion

        #region SetDataSheetCommand

        private RelayCommand _setDataSheetComamnd;

        /// <summary>
        /// Shows the dialog to allow the user to set the 
        /// linked DataSheetLabel for the selected
        /// planogram component.
        /// </summary>
        public RelayCommand SetDataSheetCommand
        {
            get
            {
                if (_setDataSheetComamnd == null)
                {
                    _setDataSheetComamnd = new RelayCommand(
                        p => SetDataSheet_Executed(p),
                        p => SetDataSheet_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateSetup_SetDataSheet,
                        FriendlyDescription = Message.PrintTemplateSetup_SetDataSheet_Desc,
                        Icon = ImageResources.PrintTemplateSetup_SetDataSheet
                    };
                    RegisterCommand(_setDataSheetComamnd);
                }
                return _setDataSheetComamnd;
            }
        }

        private Boolean SetDataSheet_CanExecute()
        {
            if (this.SelectedComponent == null
                || this.SelectedComponent.Type != PrintTemplateComponentType.DataSheet)
            {
                return false;
            }

            return true;
        }

        private void SetDataSheet_Executed(Object args)
        {
            //show the file dialog if no args passed in.
            String file = args as String;
            if (String.IsNullOrEmpty(file)) file = CustomColumnLayoutHelper.ShowDataSheetOpenFileDialog();
            if (String.IsNullOrEmpty(file)) return;

            //just set against the component details.
            this.SelectedComponent.ComponentDetails.DataSheetId = file;
        }

        #endregion

        #region ShowCameraSettingsCommand

        private RelayCommand _showCameraSettingsCommand;

        /// <summary>
        /// Shows the camera settings window.
        /// </summary>
        public RelayCommand ShowCameraSettingsCommand
        {
            get
            {
                if (_showCameraSettingsCommand == null)
                {
                    _showCameraSettingsCommand = new RelayCommand(
                        p => ShowCameraSettings_Executed(),
                        p => ShowCameraSettings_CanExecute())
                        {
                            FriendlyName = Message.PrintTemplateSetup_CameraSettings
                        };
                    RegisterCommand(_showCameraSettingsCommand);
                }
                return _showCameraSettingsCommand;
            }
        }

        private Boolean ShowCameraSettings_CanExecute()
        {
            if (this.SelectedComponent == null)
                return false;

            if (this.SelectedComponent.Type != PrintTemplateComponentType.Planogram)
                return false;

            if (this.SelectedComponent.ComponentDetails.PlanogramViewType != PrintTemplatePlanogramViewType.Perspective)
                return false;

            return true;
        }

        private void ShowCameraSettings_Executed()
        {
            PrintTemplateCameraSettingsViewModel vm = new PrintTemplateCameraSettingsViewModel(
                this.SelectedComponent.ComponentDetails, _currentPlanogram);

            GetWindowService().ShowDialog<PrintTemplateCameraSettings>(vm);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current print template is changed.
        /// </summary>
        private void OnCurrentPrintTemplateChanged(PrintTemplate oldValue, PrintTemplate newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= CurrentPrintTemplate_PropertyChanged;

                //must dispose of the old item so that if it was a file then it gets unlocked.
                if (oldValue.IsFile && (newValue == null || !(newValue.IsFile && newValue.Id == oldValue.Id)))
                {
                    oldValue.Dispose();
                }
            }

            if (newValue != null)
            {


                newValue.PropertyChanged += CurrentPrintTemplate_PropertyChanged;

                //make sure the template has at least one section group and section
                Boolean isInitialized = newValue.IsInitialized;

                if (!newValue.SectionGroups.Any()) newValue.SectionGroups.AddNewSectionGroup();
                this.SelectedSectionGroup = newValue.SectionGroups.First();

                if (!this.SelectedSectionGroup.Sections.Any()) this.SelectedSectionGroup.Sections.AddNewSection();
                this.SelectedSection = this.SelectedSectionGroup.Sections.First();

                //reinitialize if required
                if (isInitialized && !newValue.IsInitialized) newValue.MarkGraphAsInitialized();
            }
            else
            {
                this.SelectedSectionGroup = null;
                this.SelectedSection = null;
            }


            //OnPaperSizeChanged();
        }

        /// <summary>
        /// Called whenever a property changes on the current print template.
        /// </summary>
        private void CurrentPrintTemplate_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PrintTemplate.PaperSizeProperty.Name)
            {
                this.CurrentPrintTemplate.UpdateChildPageSizes();
            }
        }

        /// <summary>
        /// Called whenever the selected section changes.
        /// </summary>
        private void OnSelectedSectionChanged(PrintTemplateSection newValue)
        {
            //TODO:

            //OnPropertyChanged(CurrentPrintTemplateSectionOrientationProperty);
            //OnPaperSizeChanged();
        }

        /// <summary>
        /// Called whenever the selected component changes.
        /// </summary>
        private void OnSelectedComponentChanged(PrintTemplateComponent newValue)
        {
            //if (this.SelectedComponent != null)
            //{
            //    // set the context tab
            //    this.SelectedComponentContextualTab = this.SelectedComponent.Type;
            //}
            //else
            //{
            //    this.SelectedComponentContextualTab = PrintTemplateComponentType.Select;
            //}
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.CurrentPrintTemplate, this.SaveCommand);
        }

        #region Section Methods

        /// <summary>
        /// Adds a new section to the given parent
        /// </summary>
        /// <param name="parentLevel"></param>
        private void AddNewSection()
        {
            base.ShowWaitCursor(true);

            //Add a new section after the one selected
            PrintTemplateSection newPrintTemplateSection =
                 (this.SelectedSection != null) ?
                 this.SelectedSectionGroup.Sections.AddNewSection((Byte)(this.SelectedSection.Number + 1))
                 : this.SelectedSectionGroup.Sections.AddNewSection();


            ////create new child section
            //PrintTemplateSection newPrintTemplateSection = PrintTemplateSection.NewPrintTemplateSection();

            //// ensure there is a print section selected
            //if (this.SelectedSection != null)
            //{
            //    // increment the section number so the new section is a child of the selected.
            //    newPrintTemplateSection.Number = (Byte)(this.SelectedSection.Number + 1);

            //    // loop through all the other sections and increment the section number
            //    foreach (PrintTemplateSection section in this.SelectedSectionGroup.Sections)
            //    {
            //        // if section number is equal to or more than the new section then increment
            //        if (section.Number >= newPrintTemplateSection.Number)
            //        {
            //            section.Number++;
            //        }
            //    }
            //}
            //else
            //{
            //    // if there isn't a selected section then this must be the first one
            //    newPrintTemplateSection.Number = 1;
            //}

            ////add the new section to the print option section group
            //this.SelectedSectionGroup.Sections.Add(newPrintTemplateSection);

            //select new section
            this.SelectedSection = newPrintTemplateSection;

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the selected section
        /// </summary>
        /// <param name="selectedSection"></param>
        private void RemoveSelectedSection(PrintTemplateSection selectedSection)
        {
            base.ShowWaitCursor(true);

            PrintTemplateSection newSelectedPrintTemplateSection;

            // set the selected print option to a temporary new one to stop null exceptions on 
            // dependency properties by removing the currently selected
            this.SelectedSection = PrintTemplateSection.NewEmpty();

            Byte selectedSectionNumber = selectedSection.Number;

            // remove section from the print option
            this.SelectedSectionGroup.Sections.Remove(selectedSection);

            // loop through all the other sections and decrease the section number
            foreach (PrintTemplateSection section in this.SelectedSectionGroup.Sections)
            {
                // if section number is equal to or more than the removed section then decrease
                if (section.Number > selectedSectionNumber)
                {
                    section.Number--;
                }
            }

            // find the new selected section by looking below for a child
            newSelectedPrintTemplateSection = this.SelectedSectionGroup.Sections.FirstOrDefault(u => u.Number == selectedSectionNumber);

            // ensure the section is found
            if (newSelectedPrintTemplateSection != null)
            {
                // select section
                this.SelectedSection = newSelectedPrintTemplateSection;
            }
            else
            {
                // try and find the section above, if possible
                if ((selectedSectionNumber - 1) > 0)
                {
                    newSelectedPrintTemplateSection = this.SelectedSectionGroup.Sections.FirstOrDefault(u => u.Number == (selectedSectionNumber - 1));

                    // ensure a section is found
                    if (newSelectedPrintTemplateSection != null)
                    {
                        // select section
                        this.SelectedSection = newSelectedPrintTemplateSection;
                    }
                }

                // set print option section to the 1st one
                newSelectedPrintTemplateSection = this.SelectedSectionGroup.Sections.FirstOrDefault(u => u.Number == _cSectionNumber);

                // ensure the 1st section is found
                if (newSelectedPrintTemplateSection != null)
                {
                    // select 1st section
                    this.SelectedSection = newSelectedPrintTemplateSection;
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Moves the selected section 1 position
        /// </summary>
        private void MoveSection(PrintTemplateSection sectionToMove, Byte targetSectionNumber)
        {
            base.ShowWaitCursor(true);

            //set selected section to a new one to avoid exceptions
            this.SelectedSection = PrintTemplateSection.NewEmpty();

            //move the section
            this.SelectedSectionGroup.MoveSectionWithinGroup(sectionToMove, targetSectionNumber);

            //select the moved section
            this.SelectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == targetSectionNumber);

            //reset to force the ui to update.. shouldnt really need to do this but resort is not triggering.
            this.SelectedSectionGroup.Sections.Reset();

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Moves the selected section Preview 1 position
        /// </summary>
        /// <param name="selectedSection"></param>
        private void MoveSectionPreview(PrintTemplateSection sectionToMove, Byte targetSectionNumber)
        {
            base.ShowWaitCursor(true);

            //see if the section is selected
            Boolean sectionToMoveIsSelected = (sectionToMove == this.SelectedSection) ? true : false;

            //find the section group it belongs to
            PrintTemplateSectionGroup sectionGroup = sectionToMove.Parent.Sections.Parent as PrintTemplateSectionGroup;

            if (sectionToMoveIsSelected)
            {
                //set selected section to a new one to avoid exceptions
                this.SelectedSection = PrintTemplateSection.NewEmpty();

                //move the section
                sectionGroup.MoveSectionWithinGroup(sectionToMove, targetSectionNumber);

                //select the moved section
                this.SelectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == targetSectionNumber);
            }
            else
            {
                //move the section
                sectionGroup.MoveSectionWithinGroup(sectionToMove, targetSectionNumber);
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Moves the selected section Preview 1 position
        /// </summary>
        /// <param name="selectedSection"></param>
        private void MoveSectionPreviewToGroup(PrintTemplateSection sectionToMove, PrintTemplateSectionGroup targetSectionGroup)
        {
            base.ShowWaitCursor(true);

            //see if the section is selected
            Boolean sectionToMoveIsSelected = (sectionToMove == this.SelectedSection) ? true : false;

            //find the section group it belongs to
            PrintTemplateSectionGroup sectionGroup = sectionToMove.Parent.Sections.Parent as PrintTemplateSectionGroup;

            //target section number
            Byte targetSectionNumber = (Byte)(targetSectionGroup.Sections.Count + 1);

            if (sectionToMoveIsSelected)
            {
                //set selected section to a new one to avoid exceptions
                this.SelectedSection = PrintTemplateSection.NewEmpty();

                //move the section
                sectionGroup.MoveSectionToNewGroup(targetSectionGroup, sectionToMove, targetSectionNumber);

                //select the target section group
                this.SelectedSectionGroup = targetSectionGroup;

                //select the moved section
                this.SelectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == targetSectionNumber);
            }
            else
            {
                //move the section
                sectionGroup.MoveSectionToNewGroup(targetSectionGroup, sectionToMove, targetSectionNumber);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Section Group Methods

        /// <summary>
        /// Adds a new section to the given parent
        /// </summary>
        /// <param name="parentLevel"></param>
        private void AddNewSectionGroup(PrintTemplateSection parentSection)
        {
            base.ShowWaitCursor(true);

            //Add the new group
            PrintTemplateSectionGroup newPrintTemplateSectionGroup =
                (this.SelectedSectionGroup != null) ?
                this.CurrentPrintTemplate.SectionGroups.AddNewSectionGroup((Byte)(this.SelectedSectionGroup.Number + 1))
                : this.CurrentPrintTemplate.SectionGroups.AddNewSectionGroup((Byte)1);

            //Add a new initial section
            PrintTemplateSection newPrintTemplateSection = newPrintTemplateSectionGroup.Sections.AddNewSection();



            ////create new child section group
            //PrintTemplateSectionGroup newPrintTemplateSectionGroup = PrintTemplateSectionGroup.NewPrintTemplateSectionGroup();

            ////create a new section
            //PrintTemplateSection newPrintTemplateSection = PrintTemplateSection.NewPrintTemplateSection();

            ////Add section to the group
            //newPrintTemplateSectionGroup.Sections.Add(newPrintTemplateSection);

            //// ensure there is a print section group selected
            //if (this.SelectedSectionGroup != null)
            //{
            //    // increment the section group number so the new section group is a child of the selected.
            //    newPrintTemplateSectionGroup.Number = (Byte)(this.SelectedSectionGroup.Number + 1);

            //    // loop through all the other section groups and increment the section group number
            //    foreach (PrintTemplateSectionGroup sectionGroup in this.CurrentPrintTemplate.SectionGroups)
            //    {
            //        // if section group number is equal to or more than the new section group then increment
            //        if (sectionGroup.Number >= newPrintTemplateSectionGroup.Number)
            //        {
            //            sectionGroup.Number++;
            //        }
            //    }
            //}
            //else
            //{
            //    // if there isn't a selected section group then this must be the first one
            //    newPrintTemplateSectionGroup.Number = 1;
            //}

            ////add the new section to the print option section group
            //this.CurrentPrintTemplate.SectionGroups.Add(newPrintTemplateSectionGroup);

            //select new section group
            this.SelectedSectionGroup = newPrintTemplateSectionGroup;

            //select new section
            this.SelectedSection = newPrintTemplateSection;

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the selected section Group and sections
        /// </summary>
        /// <param name="selectedSection"></param>
        private void RemoveSelectedSectionGroupAndSections(PrintTemplateSectionGroup selectedSectionGroup)
        {
            base.ShowWaitCursor(true);

            PrintTemplateSectionGroup newSelectedPrintTemplateSectionGroup;

            // set the selected print option section group to a temporary new one to stop null exceptions on 
            // dependency properties by removing the currently selected
            this.SelectedSectionGroup = PrintTemplateSectionGroup.NewEmpty();

            Byte selectedSectionGroupNumber = selectedSectionGroup.Number;

            // remove section group from the print option
            this.CurrentPrintTemplate.SectionGroups.Remove(selectedSectionGroup);

            // loop through all the other section groups and decrease the section group number
            foreach (PrintTemplateSectionGroup sectionGroup in this.CurrentPrintTemplate.SectionGroups)
            {
                // if section group number is equal to or more than the removed section then decrease
                if (sectionGroup.Number > selectedSectionGroupNumber)
                {
                    sectionGroup.Number--;
                }
            }

            // find the new selected section group by looking below for a child
            newSelectedPrintTemplateSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(u => u.Number == selectedSectionGroupNumber);

            // ensure the section is found
            if (newSelectedPrintTemplateSectionGroup != null)
            {
                // select section group
                this.SelectedSectionGroup = newSelectedPrintTemplateSectionGroup;
            }
            else
            {
                // try and find the section group above, if possible
                if ((selectedSectionGroupNumber - 1) > 0)
                {
                    newSelectedPrintTemplateSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(u => u.Number == (selectedSectionGroupNumber - 1));

                    // ensure a section group is found
                    if (newSelectedPrintTemplateSectionGroup != null)
                    {
                        // select section group
                        this.SelectedSectionGroup = newSelectedPrintTemplateSectionGroup;
                    }
                }

                // set print option section group to the 1st one
                newSelectedPrintTemplateSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(u => u.Number == _cSectionNumber);

                // ensure the 1st section group is found
                if (newSelectedPrintTemplateSectionGroup != null)
                {
                    // select 1st section group
                    this.SelectedSectionGroup = newSelectedPrintTemplateSectionGroup;
                }
            }

            //Select the first sectoin in the group
            if (this.SelectedSectionGroup != null)
            {
                if (this.SelectedSectionGroup.Sections.Count > 0)
                {
                    this.SelectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(u => u.Number == _cSectionNumber);
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the selected section Group
        /// </summary>
        /// <param name="selectedSection"></param>
        private void RemoveSelectedSectionGroupOnly(PrintTemplateSectionGroup selectedSectionGroup)
        {
            base.ShowWaitCursor(true);

            PrintTemplateSectionGroup newSelectedPrintTemplateSectionGroup;

            // set the selected print option section group to a temporary new one to stop null exceptions on 
            // dependency properties by removing the currently selected
            this.SelectedSectionGroup = PrintTemplateSectionGroup.NewEmpty();

            Byte selectedSectionGroupNumber = selectedSectionGroup.Number;
            Byte selectedSectionNumber = this.SelectedSection.Number;

            //get the sections from the section group
            ObservableCollection<PrintTemplateSection> sections = selectedSectionGroup.Sections;

            // remove section group from the print option
            this.CurrentPrintTemplate.SectionGroups.Remove(selectedSectionGroup);

            // loop through all the other section groups and decrease the section group number
            foreach (PrintTemplateSectionGroup sectionGroup in this.CurrentPrintTemplate.SectionGroups)
            {
                // if section group number is equal to or more than the removed section then decrease
                if (sectionGroup.Number > selectedSectionGroupNumber)
                {
                    sectionGroup.Number--;
                }
            }

            //if the deleted section group was number 1 then need to add 
            //sections to the new group 1 else go to the section group above
            if (selectedSectionGroupNumber == 1)
            {
                newSelectedPrintTemplateSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(p => p.Number == _cSectionNumber);

                if (newSelectedPrintTemplateSectionGroup != null)
                {
                    //increment the section numbers to the amount of sections in the deleted group
                    foreach (PrintTemplateSection section in newSelectedPrintTemplateSectionGroup.Sections)
                    {
                        section.Number = (Byte)(section.Number + sections.Count);
                    }

                    //move the sections to the newly selected group
                    selectedSectionGroup.MoveAllSectionsToNewGroup(newSelectedPrintTemplateSectionGroup);

                    //select the group
                    this.SelectedSectionGroup = newSelectedPrintTemplateSectionGroup;

                    //select the section
                    PrintTemplateSection selectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == selectedSectionNumber);
                    if (selectedSection != null)
                    {
                        this.SelectedSection = selectedSection;
                    }
                }
            }
            else
            {
                //set the new selected section group number to the section group above
                Byte newSelectedSectionGroupNumber = (Byte)(selectedSectionGroupNumber - 1);

                newSelectedPrintTemplateSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(p => p.Number == newSelectedSectionGroupNumber);

                if (newSelectedPrintTemplateSectionGroup != null)
                {
                    //increment the section numbers to the amount of sections in the newly selected group,
                    //then add to the group
                    foreach (PrintTemplateSection section in sections)
                    {
                        section.Number = (Byte)(section.Number + newSelectedPrintTemplateSectionGroup.Sections.Count);
                    }

                    //increment the selected section count
                    selectedSectionNumber = (Byte)(selectedSectionNumber + newSelectedPrintTemplateSectionGroup.Sections.Count);

                    //move the sections to the newly selected group
                    selectedSectionGroup.MoveAllSectionsToNewGroup(newSelectedPrintTemplateSectionGroup);

                    //select the group
                    this.SelectedSectionGroup = newSelectedPrintTemplateSectionGroup;

                    //select the section
                    PrintTemplateSection selectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == selectedSectionNumber);
                    if (selectedSection != null)
                    {
                        this.SelectedSection = selectedSection;
                    }
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Moves the selected section group 1 position
        /// </summary>
        /// <param name="selectedSectionGroup"></param>
        private void MoveSectionGroup(PrintTemplateSectionGroup sectionGroupToMove, Byte targetSectionGroupNumber)
        {
            base.ShowWaitCursor(true);

            //set the selected section number
            Byte selectedSectionNumber = this.SelectedSection.Number;

            //set selected section group to a new one to avoid exceptions
            this.SelectedSectionGroup = PrintTemplateSectionGroup.NewEmpty();

            //move the section group
            this.CurrentPrintTemplate.MoveSectionGroup(sectionGroupToMove, targetSectionGroupNumber);

            //select the moved section group
            this.SelectedSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(p => p.Number == targetSectionGroupNumber);

            //select the section
            PrintTemplateSection selectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == selectedSectionNumber);
            if (selectedSection != null)
            {
                this.SelectedSection = selectedSection;
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the selected section
        /// </summary>
        /// <param name="selectedSectionGroup"></param>
        private void RemoveSectionGroupPreview(PrintTemplateSectionGroup sectionGroupToRemove)
        {
            base.ShowWaitCursor(true);

            //if the section Group preview to be removed is the selected section Group,
            //then the action is the same as the ribbon command
            if (sectionGroupToRemove == this.SelectedSectionGroup)
            {
                RemoveSelectedSectionGroupAndSections(sectionGroupToRemove);
            }
            else
            {
                //find the section group
                if (this.CurrentPrintTemplate.SectionGroups.Contains(sectionGroupToRemove))
                {
                    Byte selectedSectionGroupNumber = sectionGroupToRemove.Number;

                    // remove section group from the print option
                    this.CurrentPrintTemplate.SectionGroups.Remove(sectionGroupToRemove);

                    // loop through all the other section groups and decrease the section group number
                    foreach (PrintTemplateSectionGroup sectionGroup in this.CurrentPrintTemplate.SectionGroups)
                    {
                        // if section group number is equal to or more than the removed section group then decrease
                        if (sectionGroup.Number > selectedSectionGroupNumber)
                        {
                            sectionGroup.Number--;
                        }
                    }
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Moves the selected section group Preview 1 position
        /// </summary>
        /// <param name="selectedSectionGroup"></param>
        private void MoveSectionGroupPreview(PrintTemplateSectionGroup sectionGroupToMove, Byte targetSectionGroupNumber)
        {
            base.ShowWaitCursor(true);

            //see if the section group is selected
            if (sectionGroupToMove == this.SelectedSectionGroup)
            {
                //set the selected section number
                Byte selectedSectionNumber = this.SelectedSection.Number;

                //set selected section group to a new one to avoid exceptions
                this.SelectedSectionGroup = PrintTemplateSectionGroup.NewEmpty();

                //move the section group
                this.CurrentPrintTemplate.MoveSectionGroup(sectionGroupToMove, targetSectionGroupNumber);

                //select the moved section group
                this.SelectedSectionGroup = this.CurrentPrintTemplate.SectionGroups.FirstOrDefault(p => p.Number == targetSectionGroupNumber);

                //select the section
                PrintTemplateSection selectedSection = this.SelectedSectionGroup.Sections.FirstOrDefault(p => p.Number == selectedSectionNumber);
                if (selectedSection != null)
                {
                    this.SelectedSection = selectedSection;
                }
            }
            else
            {
                //move the section group
                this.CurrentPrintTemplate.MoveSectionGroup(sectionGroupToMove, targetSectionGroupNumber);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        /// <summary>
        /// Checks to see if pdf is open
        /// </summary>
        private static Boolean IsFileLocked(System.IO.FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        /// <summary>
        /// Tries to delete the preview file.
        /// </summary>
        /// <param name="showUserWarning"></param>
        /// <returns></returns>
        private Boolean TryDeletePreviewFile(Boolean showUserWarning)
        {
            //check if file has been generated before and delete if it has
            String pdfPath = _previewFilePath;
            if (System.IO.File.Exists(pdfPath))
            {
                Boolean fileRemoved = true;
                try
                {
                    //check if file is already open
                    if (!IsFileLocked(new System.IO.FileInfo(pdfPath)))
                    {
                        //delete the file
                        System.IO.File.Delete(pdfPath);
                    }
                    else
                    {
                        fileRemoved = false;
                    }
                }
                catch
                {
                    fileRemoved = false;
                }

                if (!fileRemoved)
                {
                    if (showUserWarning)
                    {
                        //tell the user to close the pdf
                        GetWindowService().ShowErrorMessage(
                            Message.PrintTemplateSetup_PrintPreview_FileInUse,
                            String.Format(CultureInfo.CurrentCulture, Message.PrintTemplateSetup_PrintPreview_FileInUse_Description, pdfPath));
                    }
                    return false;
                }
            }

            return true;
        }


        #endregion

        #region Events

        public event EventHandler<EventArgs<Object>> PrintTemplateSaved;

        private void RaisePrintTemplateSaved(Object id)
        {
            if (PrintTemplateSaved != null)
            {
                PrintTemplateSaved(this, new EventArgs<object>(id));
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentPrintTemplate = null;
                    _masterPrintTemplateInfoListView.Dispose();

                    TryDeletePreviewFile(false);

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }


    #region Supporting Classes


    /// <summary>
    /// Enum denoting the different component types.
    /// </summary>
    public enum PrintTemplateComponentToolType
    {
        Select,
        TextBox,
        Planogram,
        DataSheet,
        Line,
    }

    public sealed class PrintTemplateMoveSectionToGroupCommandParam
    {
        #region Fields
        private PrintTemplateSection _printTemplateSection;
        private PrintTemplateSectionGroup _printTemplateSectionGroup;
        #endregion

        #region Properties

        public PrintTemplateSection PrintTemplateSection
        {
            get { return _printTemplateSection; }
        }

        public PrintTemplateSectionGroup PrintTemplateSectionGroup
        {
            get { return _printTemplateSectionGroup; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of the print option section to move parameters
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="splitProperties"></param>
        public PrintTemplateMoveSectionToGroupCommandParam(PrintTemplateSection section, PrintTemplateSectionGroup sectionGroup)
        {
            _printTemplateSection = section;
            _printTemplateSectionGroup = sectionGroup;
        }

        #endregion
    }

    /// <summary>
    /// An enum which defines the fields available to insert in to a textbox
    /// </summary>
    public enum PrintTemplateComponentInsertText
    {
        //CommunicatedDate = 0,
        //ChangesFromPreviousCount,
        //ContentName,
        //ContentOwner,
        //ContentVersion,
        Date,
        DateTime,
        //LiveDate,
        //LocationCode,
        //LocationName,
        PageNumber,
        //ProductCount,
        //ProductGroupCode,
        //ProductGroupName,
        //SOWUser,
        TotalPages,
        //UCR
    }

    public static class PrintTemplateComponentInsertTextHelper
    {
        public static readonly Dictionary<PrintTemplateComponentInsertText, String> FriendlyNames =
            new Dictionary<PrintTemplateComponentInsertText, String>()
            {
                //{PrintTemplateComponentInsertText.CommunicatedDate, "Message.Enum_PrintTemplateComponentInsertText_CommunicatedDate"},
                //{PrintTemplateComponentInsertText.ChangesFromPreviousCount, "Message.Enum_PrintTemplateComponentInsertText_ChangesFromPreviousCount"},
                //{PrintTemplateComponentInsertText.ContentName, Message.Enum_PrintTemplateComponentInsertText_ContentName"},
                //{PrintTemplateComponentInsertText.ContentOwner, Message.Enum_PrintTemplateComponentInsertText_ContentOwner"},
                //{PrintTemplateComponentInsertText.ContentVersion, Message.Enum_PrintTemplateComponentInsertText_ContentVersion},
                {PrintTemplateComponentInsertText.Date, "Message.Enum_PrintTemplateComponentInsertText_Date"},
                {PrintTemplateComponentInsertText.DateTime, "Message.Enum_PrintTemplateComponentInsertText_DateTime"},
                //{PrintTemplateComponentInsertText.LiveDate, Message.Enum_PrintTemplateComponentInsertText_LiveDate},
                //{PrintTemplateComponentInsertText.LocationCode, Message.Enum_PrintTemplateComponentInsertText_LocationCode},
                //{PrintTemplateComponentInsertText.LocationName, Message.Enum_PrintTemplateComponentInsertText_LocationName},
                {PrintTemplateComponentInsertText.PageNumber, "Message.Enum_PrintTemplateComponentInsertText_PageNumber"},
                //{PrintTemplateComponentInsertText.ProductCount, Message.Enum_PrintTemplateComponentInsertText_ProductCount},
                //{PrintTemplateComponentInsertText.ProductGroupCode, Message.Enum_PrintTemplateComponentInsertText_ProductGroupCode},
                //{PrintTemplateComponentInsertText.ProductGroupName, Message.Enum_PrintTemplateComponentInsertText_ProductGroupName},
                //{PrintTemplateComponentInsertText.SOWUser, Message.Enum_PrintTemplateComponentInsertText_SOWUser},
                {PrintTemplateComponentInsertText.TotalPages, "Message.Enum_PrintTemplateComponentInsertText_TotalPages"},
                //{PrintTemplateComponentInsertText.UCR, Message.Enum_PrintTemplateComponentInsertText_UCR}
            };

        /// <summary>
        /// Dictionary with Friendly Name as key and PrintTemplateComponentInsertText as value
        /// </summary>
        public static readonly Dictionary<String, PrintTemplateComponentInsertText> EnumFromFriendlyName =
            new Dictionary<String, PrintTemplateComponentInsertText>()
            {
                //{Message.Enum_PrintTemplateComponentInsertText_CommunicatedDate, PrintTemplateComponentInsertText.CommunicatedDate},
                //{Message.Enum_PrintTemplateComponentInsertText_ChangesFromPreviousCount, PrintTemplateComponentInsertText.ChangesFromPreviousCount},
                //{Message.Enum_PrintTemplateComponentInsertText_ContentName, PrintTemplateComponentInsertText.ContentName},
                //{Message.Enum_PrintTemplateComponentInsertText_ContentOwner, PrintTemplateComponentInsertText.ContentOwner},
                //{Message.Enum_PrintTemplateComponentInsertText_ContentVersion, PrintTemplateComponentInsertText.ContentVersion},
                {"Message.Enum_PrintTemplateComponentInsertText_Date", PrintTemplateComponentInsertText.Date},
                {"Message.Enum_PrintTemplateComponentInsertText_DateTime", PrintTemplateComponentInsertText.DateTime},
                //{Message.Enum_PrintTemplateComponentInsertText_LiveDate, PrintTemplateComponentInsertText.LiveDate},
                //{Message.Enum_PrintTemplateComponentInsertText_LocationCode, PrintTemplateComponentInsertText.LocationCode},
                //{Message.Enum_PrintTemplateComponentInsertText_LocationName, PrintTemplateComponentInsertText.LocationName},
                {"Message.Enum_PrintTemplateComponentInsertText_PageNumber", PrintTemplateComponentInsertText.PageNumber},
                //{Message.Enum_PrintTemplateComponentInsertText_ProductCount, PrintTemplateComponentInsertText.ProductCount},
                //{Message.Enum_PrintTemplateComponentInsertText_ProductGroupCode, PrintTemplateComponentInsertText.ProductGroupCode},
                //{Message.Enum_PrintTemplateComponentInsertText_ProductGroupName, PrintTemplateComponentInsertText.ProductGroupName},
                //{Message.Enum_PrintTemplateComponentInsertText_SOWUser, PrintTemplateComponentInsertText.SOWUser},
                {"Message.Enum_PrintTemplateComponentInsertText_TotalPages", PrintTemplateComponentInsertText.TotalPages},
                //{Message.Enum_PrintTemplateComponentInsertText_UCR, PrintTemplateComponentInsertText.UCR}
            };

        public static PrintTemplateComponentInsertText? PrintTemplateComponentInsertTextGetEnum(String description)
        {
            foreach (KeyValuePair<PrintTemplateComponentInsertText, String> keyPair in PrintTemplateComponentInsertTextHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }

        public static String PrintTemplateComponentInsertTextGetFriendlyName(PrintTemplateComponentInsertText printTemplateComponentInsertText)
        {
            String friendlyName = String.Empty;
            FriendlyNames.TryGetValue(printTemplateComponentInsertText, out friendlyName);

            return friendlyName;
        }

        /// <summary>
        /// Converts the placeholder text with the enum value
        /// </summary>
        public static String ConvertPlaceholderToEnum(String text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                foreach (KeyValuePair<String, PrintTemplateComponentInsertText> insertText in EnumFromFriendlyName)
                {
                    text = text.Replace("<" + insertText.Key + ">", "<" + insertText.Value.ToString() + ">");
                }
            }

            return text;
        }

        /// <summary>
        /// Converts the enum to a placeholder value
        /// </summary>
        public static String ConvertEnumToPlaceholder(String text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                foreach (KeyValuePair<PrintTemplateComponentInsertText, String> insertText in FriendlyNames)
                {
                    text = text.Replace("<" + insertText.Key.ToString() + ">", "<" + insertText.Value + ">");
                }
            }

            return text;
        }
    }

    #endregion
}
