﻿
#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateSetupHomeTab.xaml
    /// </summary>
    public sealed partial class PrintTemplateSetupHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PrintTemplateSetupViewModel), typeof(PrintTemplateSetupHomeTab),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public PrintTemplateSetupViewModel ViewModel
        {
            get { return (PrintTemplateSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PrintTemplateSetupHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
