﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateSetupBackstageOpen.xaml
    /// </summary>
    public sealed partial class PrintTemplateSetupBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PrintTemplateSetupViewModel), typeof(PrintTemplateSetupBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public PrintTemplateSetupViewModel ViewModel
        {
            get { return (PrintTemplateSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PrintTemplateSetupBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void XSearchResults_OnMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                PrintTemplateInfo itemToOpen = (PrintTemplateInfo)senderControl.SelectedItem;

                if (itemToOpen != null)
                {
                    this.ViewModel.OpenFromRepositoryCommand.Execute(itemToOpen.Id);
                }
            }
        }

        #endregion
    }
}
