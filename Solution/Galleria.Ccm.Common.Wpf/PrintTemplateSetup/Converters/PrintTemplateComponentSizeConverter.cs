﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Globalization;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Converters
{
    public class PrintTemplateComponentSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // convert the value to a double
            double d = (double)value;
            // Divide the value by 2 to get the mm value as canvas is twice the scale
            d = (d / 2.835);
            // Round the result to a whole mm
            return Math.Round(d);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
