﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Converters
{
    /// <summary>
    /// Converter for changing font names to a valid font family.
    /// </summary>
    public sealed class PrintTemplateFontNameToFontFamilyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String fontName = value as String;

            if (String.IsNullOrEmpty(fontName)) return value;

            FontFamily font = (FontFamily)new FontFamilyConverter().ConvertFromString(fontName);
            if (font != null) return font;


            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToString(value, CultureInfo.InvariantCulture);
        }
    }
}
