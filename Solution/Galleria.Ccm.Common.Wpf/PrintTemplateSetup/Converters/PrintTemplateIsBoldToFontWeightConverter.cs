﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Converters
{
    public sealed class PrintTemplateIsBoldToFontWeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Boolean? isBold = value as Boolean?;

            if (!isBold.HasValue) return FontWeights.Normal;

            return (isBold.Value) ? FontWeights.Bold : FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
