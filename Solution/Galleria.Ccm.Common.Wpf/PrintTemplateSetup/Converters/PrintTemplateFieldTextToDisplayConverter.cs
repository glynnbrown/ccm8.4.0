﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Linq;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Converters
{
    /// <summary>
    /// Converters the given field text to a display friendly version
    /// </summary>
    public sealed class PrintTemplateFieldTextToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(value as String,
                PrintTemplateComponentDetail.EnumerateSpecialTextFields().Union(Planogram.EnumerateDisplayableFieldInfos(/*incMeta*/true)));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value as String,
                PrintTemplateComponentDetail.EnumerateSpecialTextFields().Union(Planogram.EnumerateDisplayableFieldInfos(/*incMeta*/true)));
        }
    }
}
