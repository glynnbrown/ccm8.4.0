﻿// Copyright © Galleria RTS Ltd 2016

using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentSelection : Control
    {
        #region Fields
        private Adorner _printTemplateComponentAdorner;
        #endregion

        #region Properties

        #region ShowPrintTemplateComponentSelection
        public bool ShowPrintTemplateComponentSelection
        {
            get { return (bool)GetValue(ShowPrintTemplateComponentSelectionProperty); }
            set { SetValue(ShowPrintTemplateComponentSelectionProperty, value); }
        }

        public static readonly DependencyProperty ShowPrintTemplateComponentSelectionProperty =
            DependencyProperty.Register("ShowPrintTemplateComponentSelection", typeof(bool), typeof(PrintTemplateComponentSelection),
            new FrameworkPropertyMetadata(false, new PropertyChangedCallback(ShowPrintTemplateComponentSelectionProperty_Changed)));

        public PrintTemplateComponentSelection()
        {
            Unloaded += new RoutedEventHandler(this.PrintTemplateComponentSelection_Unloaded);
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Sets the visibility of the print option component adorner to hidden
        /// </summary>
        private void HidePrintTemplateComponentAdorner()
        {
            if (this._printTemplateComponentAdorner != null)
            {
                this._printTemplateComponentAdorner.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Shows print option component adorner
        /// </summary>
        private void ShowPrintTemplateComponentAdorner()
        {
            // Create the adorner for the print option component adorner or make visible if already created
            if (this._printTemplateComponentAdorner == null)
            {
                // Get the adorner layer
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);

                // If the adorner is set
                if (adornerLayer != null)
                {
                    // Set the print option component
                    PrintTemplateComponentContainer selectedPrintTemplateComponent = this.DataContext as PrintTemplateComponentContainer;

                    // Get the print option canvas the component is on
                    Canvas canvas = VisualTreeHelper.GetParent(selectedPrintTemplateComponent) as Canvas;

                    // Create a new resize adorner for the print option component
                    this._printTemplateComponentAdorner = new PrintTemplateComponentResizeAdorner(selectedPrintTemplateComponent);

                    // Add this component to the adorner layer
                    adornerLayer.Add(this._printTemplateComponentAdorner);

                    // show the adorner based on the boolean value
                    if (this.ShowPrintTemplateComponentSelection)
                    {
                        this._printTemplateComponentAdorner.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        this._printTemplateComponentAdorner.Visibility = Visibility.Hidden;
                    }
                }
            }
            else
            {
                // show the adorner based on the boolean value
                if (this.ShowPrintTemplateComponentSelection)
                {
                    this._printTemplateComponentAdorner.Visibility = Visibility.Visible;
                }
                else
                {
                    this._printTemplateComponentAdorner.Visibility = Visibility.Hidden;
                }
            }
        }

        /// <summary>
        /// Remove the print option component selection adorner from the adorner layer
        /// </summary>
        private void PrintTemplateComponentSelection_Unloaded(object sender, RoutedEventArgs e)
        {
            // If the print option component adorner is set
            if (this._printTemplateComponentAdorner != null)
            {
                // Get the adorner layer
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);

                // If the adorner is set
                if (adornerLayer != null)
                {
                    // Remove the component adorner from the layer
                    adornerLayer.Remove(this._printTemplateComponentAdorner);
                }

                // reset the component adorner layer
                this._printTemplateComponentAdorner = null;
            }
        }

        /// <summary>
        /// Property changed event of the print option component selection property
        /// </summary>
        private static void ShowPrintTemplateComponentSelectionProperty_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Set the print option component selection
            PrintTemplateComponentSelection printTemplateComponentSelection = (PrintTemplateComponentSelection)d;

            // Set the boolean show value
            bool showPrintTemplateComponentSelection = (bool)e.NewValue;

            // Show or Hide the component adorner
            if (showPrintTemplateComponentSelection)
            {
                printTemplateComponentSelection.ShowPrintTemplateComponentAdorner();
            }
            else
            {
                printTemplateComponentSelection.HidePrintTemplateComponentAdorner();
            }
        }
        #endregion
    }
}
