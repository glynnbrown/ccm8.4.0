﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentMoveThumb : Thumb
    {
        #region Fields
        private RotateTransform _rotateTransform;
        private PrintTemplateComponentContainer _printTemplateComponent;
        #endregion

        #region Constructor

        public PrintTemplateComponentMoveThumb()
        {
            DragStarted += new DragStartedEventHandler(this.MovePrintTemplateComponentThumb_DragStarted);
            DragDelta += new DragDeltaEventHandler(this.MovePrintTemplateComponentThumb_DragDelta);
        }
        #endregion

        #region Internal Event Handlers
        /// <summary>
        /// Move the print option component thumb drag started event
        /// </summary>
        private void MovePrintTemplateComponentThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            // set the print opttion component
            this._printTemplateComponent = DataContext as PrintTemplateComponentContainer;

            if (this._printTemplateComponent != null)
            {
                this._printTemplateComponent.IsSelected = true;
                this._rotateTransform = this._printTemplateComponent.RenderTransform as RotateTransform;
            }
        }

        /// <summary>
        /// Move the print option component
        /// </summary>
        private void MovePrintTemplateComponentThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            // ensure the print option component is set
            if (_printTemplateComponent != null)
            {
                // set the component change
                Point dragDelta = new Point(e.HorizontalChange, e.VerticalChange);

                if (this._rotateTransform != null)
                {
                    dragDelta = this._rotateTransform.Transform(dragDelta);
                }

                // Move the print option component to the new co-ordinates
                double newLeft = Canvas.GetLeft(this._printTemplateComponent) + dragDelta.X;
                double newTop = Canvas.GetTop(this._printTemplateComponent) + dragDelta.Y;

                _printTemplateComponent.Move(newLeft, newTop, this._printTemplateComponent.Height, this._printTemplateComponent.Width);
            }
        }
        #endregion
    }
}
