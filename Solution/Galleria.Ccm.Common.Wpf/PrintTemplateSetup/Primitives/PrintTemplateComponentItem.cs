﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Windows;
namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentItem
    {
        #region Attached Properties

        #region IsSelectedProperty

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.RegisterAttached("IsSelected", typeof(bool), typeof(PrintTemplateComponentItem));

        public static bool GetIsSelected(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            return (bool)obj.GetValue(IsSelectedProperty);
        }

        public static void SetIsSelected(DependencyObject obj, bool value)
        {
            obj.SetValue(IsSelectedProperty, value);
        }

        #endregion

        #region XPositionProperty

        public static readonly DependencyProperty XPositionProperty =
            DependencyProperty.RegisterAttached("XPosition", typeof(Double), typeof(PrintTemplateComponentItem),
            new PropertyMetadata(0D, OnXPositionPropertyChanged));

        public static void OnXPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement ele = obj as UIElement;
            if (ele != null)
            {
                RaisePrintTemplateComponentPropertyChangedEvent(ele);
            }
        }

        public static Double GetXPosition(DependencyObject obj)
        {
            return (Double)obj.GetValue(XPositionProperty);
        }

        public static void SetXPosition(DependencyObject obj, Double newValue)
        {
            obj.SetValue(XPositionProperty, newValue);
        }

        #endregion

        #region YPositionProperty

        public static readonly DependencyProperty YPositionProperty =
            DependencyProperty.RegisterAttached("YPosition", typeof(Double), typeof(PrintTemplateComponentItem),
            new PropertyMetadata(0D, OnYPositionPropertyChanged));

        public static void OnYPositionPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement ele = obj as UIElement;
            if (ele != null)
            {
                RaisePrintTemplateComponentPropertyChangedEvent(ele);
            }
        }

        public static Double GetYPosition(DependencyObject obj)
        {
            return (Double)obj.GetValue(YPositionProperty);
        }

        public static void SetYPosition(DependencyObject obj, Double newValue)
        {
            obj.SetValue(YPositionProperty, newValue);
        }

        #endregion

        #region HeightProperty

        public static readonly DependencyProperty HeightProperty =
            DependencyProperty.RegisterAttached("Height", typeof(Double), typeof(PrintTemplateComponentItem),
            new PropertyMetadata(0D, OnHeightPropertyChanged));

        public static void OnHeightPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement ele = obj as UIElement;
            if (ele != null)
            {
                RaisePrintTemplateComponentPropertyChangedEvent(ele);
            }
        }

        public static Double GetHeight(DependencyObject obj)
        {
            return (Double)obj.GetValue(HeightProperty);
        }

        public static void SetHeight(DependencyObject obj, Double newValue)
        {
            obj.SetValue(HeightProperty, newValue);
        }

        #endregion

        #region WidthProperty

        public static readonly DependencyProperty WidthProperty =
            DependencyProperty.RegisterAttached("Width", typeof(Double), typeof(PrintTemplateComponentItem),
            new PropertyMetadata(0D, OnWidthPropertyChanged));

        public static void OnWidthPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement ele = obj as UIElement;
            if (ele != null)
            {
                RaisePrintTemplateComponentPropertyChangedEvent(ele);
            }
        }

        public static Double GetWidth(DependencyObject obj)
        {
            return (Double)obj.GetValue(WidthProperty);
        }

        public static void SetWidth(DependencyObject obj, Double newValue)
        {
            obj.SetValue(WidthProperty, newValue);
        }

        #endregion

        #endregion

        #region Attached Events

        #region PrintTemplateComponentPropertyChanged

        public static readonly RoutedEvent PrintTemplateComponentPropertyChangedEvent =
           EventManager.RegisterRoutedEvent("PrintTemplateComponentPropertyChanged", RoutingStrategy.Bubble,
           typeof(RoutedEventHandler), typeof(PrintTemplateComponentItem));

        public static void AddPrintTemplateComponentPropertyChangedHandler(DependencyObject obj, RoutedEventHandler handler)
        {
            UIElement uie = obj as UIElement;
            if (uie != null)
            {
                uie.AddHandler(PrintTemplateComponentItem.PrintTemplateComponentPropertyChangedEvent, handler);
            }
        }

        public static void RemovePrintTemplateComponentPropertyChangedHandler(DependencyObject obj, RoutedEventHandler handler)
        {
            UIElement uie = obj as UIElement;
            if (uie != null)
            {
                uie.RemoveHandler(PrintTemplateComponentItem.PrintTemplateComponentPropertyChangedEvent, handler);
            }
        }

        private static void RaisePrintTemplateComponentPropertyChangedEvent(UIElement uie)
        {
            uie.RaiseEvent(new RoutedEventArgs(PrintTemplateComponentItem.PrintTemplateComponentPropertyChangedEvent));
        }

        #endregion

        #endregion
    }
}
