﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using Galleria.Framework.Controls.Wpf;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentContainer : ContentControl
    {
        #region Fields
        private bool _suppressPositionChangedEvent;
        #endregion

        #region Events

        #region Selected

        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentContainer));

        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        protected virtual void OnSelected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PrintTemplateComponentContainer.SelectedEvent));
        }

        #endregion

        #region Deselected

        public static readonly RoutedEvent DeselectedEvent =
            EventManager.RegisterRoutedEvent("Deselected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentContainer));

        public event RoutedEventHandler Deselected
        {
            add { AddHandler(DeselectedEvent, value); }
            remove { RemoveHandler(DeselectedEvent, value); }
        }

        protected virtual void OnDeselected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PrintTemplateComponentContainer.DeselectedEvent));
        }

        #endregion

        #region PositionChanged

        public static readonly RoutedEvent PositionChangedEvent =
            EventManager.RegisterRoutedEvent("PositionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentContainer));

        public event RoutedEventHandler PositionChanged
        {
            add { AddHandler(PositionChangedEvent, value); }
            remove { RemoveHandler(PositionChangedEvent, value); }
        }

        private static void PositionChangedHandler(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentContainer senderControl = obj as PrintTemplateComponentContainer;
            if (senderControl != null)
            {
                senderControl.OnPositionChanged();
            }
        }

        protected virtual void OnPositionChanged()
        {
            if (!_suppressPositionChangedEvent)
            {
                _suppressPositionChangedEvent = true;

                PrintTemplateComponentItem.SetXPosition(this.UIElement, Canvas.GetLeft(this));
                PrintTemplateComponentItem.SetYPosition(this.UIElement, Canvas.GetTop(this));
                PrintTemplateComponentItem.SetHeight(this.UIElement, this.Height);
                PrintTemplateComponentItem.SetWidth(this.UIElement, this.Width);

                RaiseEvent(new RoutedEventArgs(PrintTemplateComponentContainer.PositionChangedEvent));

                _suppressPositionChangedEvent = false;
            }
        }

        #endregion

        #endregion

        #region IsSelectedProperty

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(PrintTemplateComponentContainer),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        /// <summary>
        /// Gets/Sets if the item is selected
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!((bool)e.NewValue == (bool)e.OldValue))
            {
                //show/hide the template parts accordingly
                PrintTemplateComponentContainer printTemplateComponent = obj as PrintTemplateComponentContainer;

                //raise the appropriate event back to the item
                if ((bool)e.NewValue)
                {
                    PrintTemplateComponentItem.SetIsSelected((UIElement)printTemplateComponent.UIElement, true);
                    printTemplateComponent.OnSelected();
                }
                else
                {
                    PrintTemplateComponentItem.SetIsSelected((UIElement)printTemplateComponent.UIElement, false);
                    printTemplateComponent.OnDeselected();
                }
            }
        }

        #endregion

        #region UIElementProperty

        public static readonly DependencyProperty UIElementProperty =
            DependencyProperty.Register("UIElement", typeof(FrameworkElement), typeof(PrintTemplateComponentContainer),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the contained ui element
        /// </summary>
        public FrameworkElement UIElement
        {
            get { return (FrameworkElement)GetValue(UIElementProperty); }
            private set { SetValue(UIElementProperty, value); }
        }

        #endregion

        #region Constructor
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
        static PrintTemplateComponentContainer()
        {
            // Style override
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PrintTemplateComponentContainer), new FrameworkPropertyMetadata(typeof(PrintTemplateComponentContainer)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateComponentContainer(FrameworkElement element)
        {
            this.AddHandler(PrintTemplateComponentItem.PrintTemplateComponentPropertyChangedEvent, (RoutedEventHandler)OnPrintTemplateComponentPropertyChanged);

            this.UIElement = element;
        }
        #endregion

        #region Internal Overrides

        /// <summary>
        /// Catches the mouse button down event
        /// Preview so it can handle before anything else
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            // Loop through each print option component and deselect it
            PrintTemplateSectionCanvas printTemplateCanvas = this.FindVisualAncestor<PrintTemplateSectionCanvas>();
            if (printTemplateCanvas != null)
            {
                foreach (var child in printTemplateCanvas.Children)
                {
                    PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                    if (container != null)
                    {
                        container.IsSelected = false;
                    }
                    else
                    {
                        PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                        if (line != null)
                        {
                            line.IsSelected = false;
                        }
                    }
                }
            }

            // Select the textbox
            this.IsSelected = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Moves the print option component only firing position changed once
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        public void Move(double left, double top, double height, double width)
        {
            _suppressPositionChangedEvent = true;

            Canvas.SetLeft(this, left);
            Canvas.SetTop(this, top);
            this.Height = height;
            this.Width = width;

            _suppressPositionChangedEvent = false;

            //raise position changed event
            OnPositionChanged();
        }

        /// <summary>
        /// Resizes the print option component only firing position changed once
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        public void Resize(double height, double width)
        {
            _suppressPositionChangedEvent = true;

            this.Height = height;
            this.Width = width;

            _suppressPositionChangedEvent = false;
        }

        /// <summary>
        /// Returns the bounds for this item
        /// </summary>
        /// <returns></returns>
        public Rect GetDesiredBounds()
        {
            if (this.UIElement != null)
            {
                this.Height = this.UIElement.Height;
                this.Width = this.UIElement.Width;

                return
                        new Rect(
                            Canvas.GetLeft(this), Canvas.GetTop(this),
                            this.UIElement.Width, this.UIElement.Height);

            }
            else
            {
                return new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), 0, 0);
            }

        }

        #endregion

        #region Event Handlers

        private void OnPrintTemplateComponentPropertyChanged(object sender, RoutedEventArgs e)
        {
            if (!_suppressPositionChangedEvent)
            {
                Move(PrintTemplateComponentItem.GetXPosition(this.UIElement), PrintTemplateComponentItem.GetYPosition(this.UIElement),
                    PrintTemplateComponentItem.GetHeight(this.UIElement), PrintTemplateComponentItem.GetWidth(this.UIElement));
            }
        }

        #endregion
    }
}
