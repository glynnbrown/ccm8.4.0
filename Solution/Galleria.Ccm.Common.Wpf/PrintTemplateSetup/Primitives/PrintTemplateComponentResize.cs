﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentResize : Control
    {

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
        static PrintTemplateComponentResize()
        {
            // Style override
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PrintTemplateComponentResize), new FrameworkPropertyMetadata(typeof(PrintTemplateComponentResize)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateComponentResize()
        {
        }

     }
}
