﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentSizeAdorner : Adorner
    {
        #region Fields
        private PrintTemplateComponentSize _printTemplateComponentSize;
        private VisualCollection _visuals;
        private PrintTemplateComponentContainer _printTemplateComponent;
        #endregion

        #region Properties
        protected override int VisualChildrenCount
        {
            get
            {
                return this._visuals.Count;
            }
        }
        #endregion

        #region Constructor
        public PrintTemplateComponentSizeAdorner(PrintTemplateComponentContainer printTemplateComponent)
            : base(printTemplateComponent)
        {
            this.SnapsToDevicePixels = true;
            this._printTemplateComponent = printTemplateComponent;
            this._printTemplateComponentSize = new PrintTemplateComponentSize();
            this._printTemplateComponentSize.DataContext = printTemplateComponent;
            this._visuals = new VisualCollection(this);
            this._visuals.Add(this._printTemplateComponentSize);
        }
        #endregion

        #region Internal Event Handlers
        protected override Visual GetVisualChild(int index)
        {
            return this._visuals[index];
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this._printTemplateComponentSize.Arrange(new Rect(new Point(0.0, 0.0), arrangeBounds));
            return arrangeBounds;
        }
        #endregion
    }
}
