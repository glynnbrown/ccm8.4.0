﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentResizeAdorner : Adorner
    {
        #region Fields
        private VisualCollection _visuals;
        private PrintTemplateComponentResize _printTemplateComponentResize;
        #endregion

        #region Properties
        /// <summary>
        /// Count of visual children
        /// </summary>
        protected override int VisualChildrenCount
        {
            get
            {
                return this._visuals.Count;
            }
        }
        #endregion

        #region Constructor
        public PrintTemplateComponentResizeAdorner(PrintTemplateComponentContainer printTemplateComponent)
            : base(printTemplateComponent)
        {
            SnapsToDevicePixels = true;
            this._printTemplateComponentResize = new PrintTemplateComponentResize();
            this._printTemplateComponentResize.DataContext = printTemplateComponent;
            this._visuals = new VisualCollection(this);
            this._visuals.Add(this._printTemplateComponentResize);
        }
        #endregion

        #region Internal Event Handlers
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this._printTemplateComponentResize.Arrange(new Rect(arrangeBounds));
            return arrangeBounds;
        }

        protected override Visual GetVisualChild(int index)
        {
            return this._visuals[index];
        }
        #endregion
    }
}
