﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentResizeThumb : Thumb
    {
        #region Fields
        private double _resizeAngle;
        private double _newTop;
        private double _newLeft;
        private double _verticalChange;
        private double _horizontalChange;
        private Adorner _printTemplateComponentAdorner;
        private Point _transformStartPoint;
        private PrintTemplateComponentContainer _printTemplateComponent;
        private Canvas _printTemplateCanvas;
        #endregion

        #region Constructor

        public PrintTemplateComponentResizeThumb()
        {
            DragStarted += new DragStartedEventHandler(this.ResizePrintTemplateComponentThumb_DragStarted);
            DragDelta += new DragDeltaEventHandler(this.ResizePrintTemplateComponentThumb_DragDelta);
            DragCompleted += new DragCompletedEventHandler(this.ResizePrintTemplateComponentThumb_DragCompleted);
        }
        #endregion

        #region Internal Event Handlers
        /// <summary>
        /// Resize the print option component drag started
        /// </summary>
        private void ResizePrintTemplateComponentThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            // Set the print option component
            this._printTemplateComponent = this.DataContext as PrintTemplateComponentContainer;

            // ensure print Template Component is set
            if (this._printTemplateComponent != null)
            {
                // Set the print option canvas
                this._printTemplateCanvas = VisualTreeHelper.GetParent(this._printTemplateComponent) as Canvas;

                // ensure the canvas is found
                if (this._printTemplateCanvas != null)
                {
                    // set the start point of the resize
                    this._transformStartPoint = this._printTemplateComponent.RenderTransformOrigin;

                    AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this._printTemplateCanvas);
                    if (adornerLayer != null)
                    {
                        this._printTemplateComponentAdorner = new PrintTemplateComponentSizeAdorner(this._printTemplateComponent);
                        adornerLayer.Add(this._printTemplateComponentAdorner);
                    }
                }
            }
        }

        /// <summary>
        /// Resize the print option component drag event
        /// </summary>
        private void ResizePrintTemplateComponentThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            // ensure print Template Component is set
            if (_printTemplateComponent != null)
            {
                switch (VerticalAlignment)
                {
                    case System.Windows.VerticalAlignment.Bottom:
                        _verticalChange = Math.Min(-e.VerticalChange, this._printTemplateComponent.ActualHeight - this._printTemplateComponent.MinHeight);
                        _newTop = Canvas.GetTop(this._printTemplateComponent) + (this._transformStartPoint.Y * _verticalChange * (1 - Math.Cos(-this._resizeAngle)));
                        _newLeft = Canvas.GetLeft(this._printTemplateComponent) - _verticalChange * this._transformStartPoint.Y * Math.Sin(-this._resizeAngle);
                        if ((_printTemplateComponent.Height - _verticalChange) > 0)
                        {
                            _printTemplateComponent.Height -= _verticalChange;
                        }
                        _printTemplateComponent.Move(_newLeft, _newTop, _printTemplateComponent.Height, _printTemplateComponent.Width);
                        break;
                    case System.Windows.VerticalAlignment.Top:
                        _verticalChange = Math.Min(e.VerticalChange, this._printTemplateComponent.ActualHeight - this._printTemplateComponent.MinHeight);
                        _newTop = Canvas.GetTop(this._printTemplateComponent) + _verticalChange * Math.Cos(-this._resizeAngle) + (this._transformStartPoint.Y * _verticalChange * (1 - Math.Cos(-this._resizeAngle)));
                        _newLeft = Canvas.GetLeft(this._printTemplateComponent) + _verticalChange * Math.Sin(-this._resizeAngle) - (this._transformStartPoint.Y * _verticalChange * Math.Sin(-this._resizeAngle));
                        if ((_printTemplateComponent.Height - _verticalChange) > 0)
                        {
                            _printTemplateComponent.Height -= _verticalChange;
                        }
                        _printTemplateComponent.Move(_newLeft, _newTop, _printTemplateComponent.Height, _printTemplateComponent.Width);
                        break;
                    default:
                        break;
                }

                switch (HorizontalAlignment)
                {
                    case System.Windows.HorizontalAlignment.Left:
                        _horizontalChange = Math.Min(e.HorizontalChange, this._printTemplateComponent.ActualWidth - this._printTemplateComponent.MinWidth);
                        _newTop = Canvas.GetTop(this._printTemplateComponent) + _horizontalChange * Math.Sin(this._resizeAngle) - (this._transformStartPoint.X * _horizontalChange * Math.Sin(this._resizeAngle));
                        _newLeft = Canvas.GetLeft(this._printTemplateComponent) + _horizontalChange * Math.Cos(this._resizeAngle) + (this._transformStartPoint.X * _horizontalChange * (1 - Math.Cos(this._resizeAngle)));
                        if ((_printTemplateComponent.Width - _horizontalChange) > 0)
                        {
                            _printTemplateComponent.Width -= _horizontalChange;
                        }
                        _printTemplateComponent.Move(_newLeft, _newTop, _printTemplateComponent.Height, _printTemplateComponent.Width);
                        break;
                    case System.Windows.HorizontalAlignment.Right:
                        _horizontalChange = Math.Min(-e.HorizontalChange, this._printTemplateComponent.ActualWidth - this._printTemplateComponent.MinWidth);
                        _newTop = Canvas.GetTop(this._printTemplateComponent) - this._transformStartPoint.X * _horizontalChange * Math.Sin(this._resizeAngle);
                        _newLeft = Canvas.GetLeft(this._printTemplateComponent) + (_horizontalChange * this._transformStartPoint.X * (1 - Math.Cos(this._resizeAngle)));
                        if ((_printTemplateComponent.Width - _horizontalChange) > 0)
                        {
                            _printTemplateComponent.Width -= _horizontalChange;
                        }
                        _printTemplateComponent.Move(_newLeft, _newTop, _printTemplateComponent.Height, _printTemplateComponent.Width);
                        break;
                    default:
                        break;
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// Resize the print option component
        /// </summary>
        private void ResizePrintTemplateComponentThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            // ensure the adorner is set
            if (this._printTemplateComponentAdorner != null)
            {
                // get the adorner layer on the canvas
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this._printTemplateCanvas);

                // ensure the adorner layer is set
                if (adornerLayer != null)
                {
                    // remove the print option component adorner from adorner layer
                    adornerLayer.Remove(this._printTemplateComponentAdorner);
                }

                // reset the print option component
                this._printTemplateComponentAdorner = null;
            }
        }
        #endregion
    }
}
