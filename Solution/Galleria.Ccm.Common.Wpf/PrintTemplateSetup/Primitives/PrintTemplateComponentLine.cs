﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentLine : Shape
    {
        #region Fields
        private const Double _scale = 2.835;
        private bool _suppressPropertyChangedEvent;
        private Point _startPoint = new Point(0, 0);
        private Point _endPoint = new Point(100, 0);
        private Double _x1;
        private Double _y1;
        private Double _x2;
        private Double _y2;
        private Double _x1DragStart;
        private Double _y1DragStart;
        private Double _x2DragStart;
        private Double _y2DragStart;
        private Double _lineWeight;
        private Color _lineColour;
        private PrintTemplateComponentLineMoveThumb _moveThumb;
        private PrintTemplateComponentLineResizeThumb _startPointResizeThumb;
        private PrintTemplateComponentLineResizeThumb _endPointResizeThumb;

        #endregion

        #region Properties

        #region Is Selected

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(PrintTemplateComponentLine),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        /// <summary>
        /// Gets/Sets if the item is selected
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            Boolean newValue = (Boolean)e.NewValue;
            if (!(newValue == (Boolean)e.OldValue))
            {
                //show/hide the template parts accordingly
                PrintTemplateComponentLine printTemplateComponent = obj as PrintTemplateComponentLine;

                //raise the appropriate event back to the item
                if (newValue)
                {
                    printTemplateComponent.OnSelectedChanged(newValue);
                    printTemplateComponent.OnSelected();
                }
                else
                {
                    printTemplateComponent.OnSelectedChanged(newValue);
                    printTemplateComponent.OnDeselected();
                }
            }
        }

        #endregion

        #region Unit Context Property

        public static readonly DependencyProperty UnitContextProperty =
            DependencyProperty.Register("UnitContext", typeof(PrintTemplateComponent),
            typeof(PrintTemplateComponentLine),
            new PropertyMetadata(null, OnUnitContextPropertyChanged));

        private static void OnUnitContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentLine senderControl = (PrintTemplateComponentLine)obj;

            if (e.OldValue != null)
            {
                PrintTemplateComponent oldValue = (PrintTemplateComponent)e.OldValue;
                oldValue.PropertyChanged -= senderControl.PrintTemplateComponentLine_PropertyChanged;
                oldValue.ComponentDetails.PropertyChanged += senderControl.PrintTemplateComponentLine_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PrintTemplateComponent newValue = (PrintTemplateComponent)e.NewValue;
                newValue.ComponentDetails.PropertyChanged += senderControl.PrintTemplateComponentLine_PropertyChanged;
                newValue.PropertyChanged += senderControl.PrintTemplateComponentLine_PropertyChanged;
                senderControl.SetLineProperties();
            }
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public PrintTemplateComponent UnitContext
        {
            get { return (PrintTemplateComponent)GetValue(UnitContextProperty); }
            set { SetValue(UnitContextProperty, value); }
        }

        #endregion

        /// <summary>
        /// Gets/ Sets the move thumb
        /// </summary>
        public PrintTemplateComponentLineMoveThumb MoveThumb
        {
            get { return _moveThumb; }
            set { _moveThumb = value; }
        }

        /// <summary>
        /// Gets/ Sets the start point resize thumb
        /// </summary>
        public PrintTemplateComponentLineResizeThumb StartPointResizeThumb
        {
            get { return _startPointResizeThumb; }
            set { _startPointResizeThumb = value; }
        }

        /// <summary>
        /// Gets/ Sets the end point resize thumb
        /// </summary>
        public PrintTemplateComponentLineResizeThumb EndPointResizeThumb
        {
            get { return _endPointResizeThumb; }
            set { _endPointResizeThumb = value; }
        }

        /// <summary>
        /// Gets/Sets the X1
        /// </summary>
        public Double X1
        {
            get { return _x1; }
            set { _x1 = value; }
        }

        /// <summary>
        /// Gets/Sets the Y1
        /// </summary>
        public Double Y1
        {
            get { return _y1; }
            set { _y1 = value; }
        }

        /// <summary>
        /// Gets/Sets the X2
        /// </summary>
        public Double X2
        {
            get { return _x2; }
            set { _x2 = value; }
        }

        /// <summary>
        /// Gets/Sets the Y2
        /// </summary>
        public Double Y2
        {
            get { return _y2; }
            set { _y2 = value; }
        }

        /// <summary>
        /// Gets/Sets the line colour
        /// </summary>
        private Color LineColour
        {
            get { return _lineColour; }
            set { _lineColour = value; }
        }

        /// <summary>
        /// Gets/Sets the line weight
        /// </summary>
        private Double LineWeight
        {
            get { return _lineWeight; }
            set { _lineWeight = value; }
        }

        /// <summary>
        /// Point used to create this shape
        /// </summary>
        private Point StartPoint
        {
            get { return _startPoint; }
            set { _startPoint = value; }
        }

        /// <summary>
        /// Point used to create this shape
        /// </summary>
        private Point EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateComponentLine(PrintTemplateComponent unitContext)
        {
            this.UnitContext = unitContext;
        }
        #endregion

        #region Event Handlers

        #region Selected

        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentLine));

        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        protected virtual void OnSelected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PrintTemplateComponentLine.SelectedEvent));
        }

        #endregion

        #region Deselected

        public static readonly RoutedEvent DeselectedEvent =
            EventManager.RegisterRoutedEvent("Deselected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentLine));

        public event RoutedEventHandler Deselected
        {
            add { AddHandler(DeselectedEvent, value); }
            remove { RemoveHandler(DeselectedEvent, value); }
        }

        protected virtual void OnDeselected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(PrintTemplateComponentLine.DeselectedEvent));
        }

        #endregion

        #region PositionChanged

        public static readonly RoutedEvent PositionChangedEvent =
            EventManager.RegisterRoutedEvent("PositionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PrintTemplateComponentLine));

        public event RoutedEventHandler PositionChanged
        {
            add { AddHandler(PositionChangedEvent, value); }
            remove { RemoveHandler(PositionChangedEvent, value); }
        }

        private static void PositionChangedHandler(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentLine senderControl = obj as PrintTemplateComponentLine;
            if (senderControl != null)
            {
                senderControl.OnPositionChanged();
            }
        }

        protected virtual void OnPositionChanged()
        {
            _suppressPropertyChangedEvent = true;

            this.UnitContext.X = Convert.ToSingle(this.X1 / _scale);
            this.UnitContext.Y = Convert.ToSingle(this.Y1 / _scale);
            this.UnitContext.ComponentDetails.LineEndX = this.X2 / _scale;
            this.UnitContext.ComponentDetails.LineEndY = this.Y2 / _scale;

            _suppressPropertyChangedEvent = false;

            RaiseEvent(new RoutedEventArgs(PrintTemplateComponentLine.PositionChangedEvent));
        }

        #endregion

        #region PropertyChanged

        /// <summary>
        /// Event handler for the component changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintTemplateComponentLine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SetLineProperties();
        }

        #endregion

        #region OnSelectedChanged

        private void OnSelectedChanged(Boolean isSelected)
        {
            // Get the print option canvas the line is on
            Canvas canvas = VisualTreeHelper.GetParent(this) as Canvas;

            if (isSelected)
            {
                //create the move thumb if not already created
                CreateMoveThumb(canvas);

                //make thumb visible
                this.MoveThumb.Visibility = Visibility.Visible;

                //Create the start point thumb if not already created
                CreateStartPointThumb(canvas);

                //place the start point thumb
                this.StartPointResizeThumb.RenderTransform = new TranslateTransform(this.X1, this.Y1);

                //make thumb visible
                this.StartPointResizeThumb.Visibility = Visibility.Visible;

                //Create the end point thumb if not already created
                CreateEndPointThumb(canvas);

                //place the End point thumb
                this.EndPointResizeThumb.RenderTransform = new TranslateTransform(this.X2, this.Y2);

                //make endpoint resize thumb visible
                this.EndPointResizeThumb.Visibility = Visibility.Visible;
            }
            else
            {
                //hide the start point resize thumb
                if (this.StartPointResizeThumb != null)
                {
                    this.StartPointResizeThumb.Visibility = Visibility.Hidden;
                }
                //hide the end point resize thumb
                if (this.EndPointResizeThumb != null)
                {
                    this.EndPointResizeThumb.Visibility = Visibility.Hidden;
                }
            }
        }

        #endregion

        #region StartPointResizeThumb_DragDelta
        /// <summary>
        /// Resize the Print Template Component Line drag event
        /// </summary>
        private void StartPointResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            // Set the print option canvas
            Canvas printTemplateCanvas = VisualTreeHelper.GetParent(this) as Canvas;

            // ensure the canvas is found
            if (printTemplateCanvas != null)
            {
                Point mousePosition = Mouse.GetPosition(printTemplateCanvas);

                this.Move(mousePosition.X, mousePosition.Y, this.X2, this.Y2);
            }
        }
        #endregion

        #region EndPointResizeThumb_DragDelta
        /// <summary>
        /// Resize the Print Template Component Line drag event
        /// </summary>
        private void EndPointResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            // Set the print option canvas
            Canvas printTemplateCanvas = VisualTreeHelper.GetParent(this) as Canvas;

            // ensure the canvas is found
            if (printTemplateCanvas != null)
            {
                Point mousePosition = Mouse.GetPosition(printTemplateCanvas);

                this.Move(this.X1, this.Y1, mousePosition.X, mousePosition.Y);
                this.EndPointResizeThumb.RenderTransform = new TranslateTransform(mousePosition.X, mousePosition.Y);
            }
        }
        #endregion

        #region Move Thumb Drag Events
        /// <summary>
        /// Resize the Print Template Component Line drag Ended
        /// </summary>
        private void MoveThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            // Loop through each print option component and deselect it
            PrintTemplateSectionCanvas printTemplateCanvas = this.FindVisualAncestor<PrintTemplateSectionCanvas>();
            if (printTemplateCanvas != null)
            {
                foreach (var child in printTemplateCanvas.Children)
                {
                    PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                    if (container != null)
                    {
                        container.IsSelected = false;
                    }
                    else
                    {
                        PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                        if (line != null)
                        {
                            line.IsSelected = false;
                        }
                    }
                }
            }

            // Select the line
            this.IsSelected = true;

            this._x1DragStart = this.X1;
            this._y1DragStart = this.Y1;
            this._x2DragStart = this.X2;
            this._y2DragStart = this.Y2;
        }

        /// <summary>
        /// Resize the Print Template Component Line drag event
        /// </summary>
        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Double newX1 = this._x1DragStart + e.HorizontalChange;
            Double newY1 = this._y1DragStart + e.VerticalChange;
            Double newX2 = this._x2DragStart + e.HorizontalChange;
            Double newY2 = this._y2DragStart + e.VerticalChange;
            this.Move(newX1, newY1, newX2, newY2);
        }
        #endregion

        #endregion

        #region Methods

        private void CreateStartPointThumb(Canvas canvas)
        {
            //if start point thumb is not created
            if (this._startPointResizeThumb == null)
            {
                //Create new start point resize thumb
                this._startPointResizeThumb = new PrintTemplateComponentLineResizeThumb();

                //Add to drag events
                this.StartPointResizeThumb.DragDelta += new DragDeltaEventHandler(StartPointResizeThumb_DragDelta);
                this.StartPointResizeThumb.Visibility = Visibility.Hidden;

                //Add to canvas
                canvas.Children.Add(this.StartPointResizeThumb);
            }
        }

        private void CreateEndPointThumb(Canvas canvas)
        {
            //if End point thumb is not created
            if (this._endPointResizeThumb == null)
            {
                //Create new End point resize thumb
                this._endPointResizeThumb = new PrintTemplateComponentLineResizeThumb();

                //Add to drag events
                this.EndPointResizeThumb.DragDelta += new DragDeltaEventHandler(EndPointResizeThumb_DragDelta);
                this.EndPointResizeThumb.Visibility = Visibility.Hidden;

                //Add to canvas
                canvas.Children.Add(this.EndPointResizeThumb);
            }
        }

        private void CreateMoveThumb(Canvas canvas)
        {
            //if Move thumb is not created
            if (this._moveThumb == null)
            {
                //Create new move thumb
                this._moveThumb = new PrintTemplateComponentLineMoveThumb();

                //Add to drag events
                this.MoveThumb.DragStarted += new DragStartedEventHandler(MoveThumb_DragStarted);
                this.MoveThumb.DragDelta += new DragDeltaEventHandler(MoveThumb_DragDelta);
                this.MoveThumb.Visibility = Visibility.Hidden;

                //Position the Move thumb
                this.MoveTheMoveThumb(this.X1, this.Y1, this.X2, this.Y2);

                //Add to canvas
                canvas.Children.Add(this.MoveThumb);
            }
        }

        private void SetLineProperties()
        {
            if (!_suppressPropertyChangedEvent)
            {
                this.X1 = this.UnitContext.X * _scale;
                this.Y1 = this.UnitContext.Y * _scale;
                this.X2 = this.UnitContext.ComponentDetails.LineEndX * _scale;
                this.Y2 = this.UnitContext.ComponentDetails.LineEndY * _scale;
                this.LineColour = ColorHelper.IntToColor(this.UnitContext.ComponentDetails.LineForeground);
                this.LineWeight = this.UnitContext.ComponentDetails.LineWeight;
                this.Stroke = new SolidColorBrush(this.LineColour);
                this.StrokeThickness = this.UnitContext.ComponentDetails.LineWeight;
                this.StrokeStartLineCap = PenLineCap.Flat;
                this.StrokeEndLineCap = PenLineCap.Flat;
                this.IsHitTestVisible = true;

                //Move the line without firing position changed as unit context has changed the position
                this.Move(this.X1, this.Y1, this.X2, this.Y2, false);
            }
        }

        private void SetLinePoints()
        {
            this._startPoint = new Point(0, 0);
            this._endPoint = new Point(100, 0);
        }

        /// <summary>
        /// Override Defining Gerometry to draw the shape
        /// </summary>
        protected override Geometry DefiningGeometry
        {
            get
            {
                // Create a StreamGeometry for describing the shape
                StreamGeometry geometry = new StreamGeometry();
                geometry.FillRule = FillRule.EvenOdd;

                using (StreamGeometryContext context = geometry.Open())
                {
                    InternalDrawGeometry(context);
                }

                // Freeze the geometry for performance benefits
                geometry.Freeze();

                return geometry;
            }
        }

        /// <summary>
        /// Draws the shape
        /// </summary>
        private void InternalDrawGeometry(StreamGeometryContext context)
        {
            context.BeginFigure(this.StartPoint, true, true);
            context.LineTo(this.EndPoint, true, true);
        }

        /// <summary>
        /// Moves the print option component only firing position changed once
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        public void Move(double x1, double y1, double x2, double y2, Boolean updateUnitContext = true)
        {
            //Set variables
            this.X1 = x1;
            this.Y1 = y1;
            this.X2 = x2;
            this.Y2 = y2;

            //Set new start and end point of the line
            this.StartPoint = new Point(X1, Y1);
            this.EndPoint = new Point(X2, Y2);

            //redraw the line
            this.InvalidateVisual();

            //move the move thumb
            MoveTheMoveThumb(x1, y1, x2, y2);

            //move the start resize thumb
            if (this.StartPointResizeThumb != null)
            {
                this.StartPointResizeThumb.RenderTransform = new TranslateTransform(x1, y1);
            }
            //move the end resize thumb
            if (this.EndPointResizeThumb != null)
            {
                this.EndPointResizeThumb.RenderTransform = new TranslateTransform(x2, y2);
            }

            //Don't need to update unit context if property changed source is the unit context
            if (updateUnitContext)
            {
                OnPositionChanged();
            }
        }

        /// <summary>
        /// Moves the print option line Move Thumb
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        public void MoveTheMoveThumb(double x1, double y1, double x2, double y2)
        {
            //ensure the move thumb is set
            if (this.MoveThumb != null)
            {
                this.MoveThumb.X1 = x1;
                this.MoveThumb.Y1 = y1;
                this.MoveThumb.X2 = x2;
                this.MoveThumb.Y2 = y2;
            }
        }

        /// <summary>
        /// Removes the lines thumbs from the canvas before itself gets deleted
        /// </summary>
        public void DisposeChildren()
        {
            // Get the print option canvas the line is on
            Canvas canvas = VisualTreeHelper.GetParent(this) as Canvas;

            //Find the start thumb
            if (this.StartPointResizeThumb != null)
            {
                //Unsubscribe from the drag events
                this.StartPointResizeThumb.DragDelta -= StartPointResizeThumb_DragDelta;

                //Remove from the canvas
                if (canvas.Children.Contains(this.StartPointResizeThumb))
                {
                    canvas.Children.Remove(this.StartPointResizeThumb);
                }
            }

            //Find the end thumb
            if (this.EndPointResizeThumb != null)
            {
                //Unsubscribe from the drag events
                this.EndPointResizeThumb.DragDelta -= EndPointResizeThumb_DragDelta;

                //Remove from the canvas
                if (canvas.Children.Contains(this.EndPointResizeThumb))
                {
                    canvas.Children.Remove(this.EndPointResizeThumb);
                }
            }

            //Find the move thumb
            if (this.MoveThumb != null)
            {
                //Unsubscribe from the drag events
                this.MoveThumb.DragStarted -= MoveThumb_DragStarted;
                this.MoveThumb.DragDelta -= MoveThumb_DragDelta;

                //Remove from the canvas
                if (canvas.Children.Contains(this.MoveThumb))
                {
                    canvas.Children.Remove(this.MoveThumb);
                }
            }
        }

        #endregion

        #region Internal Overrides

        /// <summary>
        /// Catches the mouse button down event
        /// Preview so it can handle before anything else
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            // Loop through each print option component and deselect it
            PrintTemplateSectionCanvas printTemplateCanvas = this.FindVisualAncestor<PrintTemplateSectionCanvas>();
            if (printTemplateCanvas != null)
            {
                foreach (var child in printTemplateCanvas.Children)
                {
                    PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                    if (container != null)
                    {
                        container.IsSelected = false;
                    }
                    else
                    {
                        PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                        if (line != null)
                        {
                            line.IsSelected = false;
                        }
                    }
                }
            }

            // Select the line
            this.IsSelected = true;

            e.Handled = true;
        }

        #endregion
    }
}
