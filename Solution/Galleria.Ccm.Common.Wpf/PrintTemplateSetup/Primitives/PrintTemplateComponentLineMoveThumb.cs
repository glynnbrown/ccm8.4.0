﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentLineMoveThumb : Thumb
    {
        #region Fields

        private Double _x1;
        private Double _y1;
        private Double _x2;
        private Double _y2;

        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the X1
        /// </summary>
        public Double X1
        {
            get { return _x1; }
            set
            {
                _x1 = value;
                OnLinePropertyChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the Y1
        /// </summary>
        public Double Y1
        {
            get { return _y1; }
            set { _y1 = value; }
        }

        /// <summary>
        /// Gets/Sets the X2
        /// </summary>
        public Double X2
        {
            get { return _x2; }
            set { _x2 = value; }
        }

        /// <summary>
        /// Gets/Sets the Y2
        /// </summary>
        public Double Y2
        {
            get { return _y2; }
            set { _y2 = value; }
        }

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
        static PrintTemplateComponentLineMoveThumb()
        {
            // Style override
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PrintTemplateComponentLineMoveThumb), new FrameworkPropertyMetadata(typeof(PrintTemplateComponentLineMoveThumb)));
        }

        public PrintTemplateComponentLineMoveThumb()
        {
        }
        #endregion

        #region Methods
        private void OnLinePropertyChanged()
        {
            if (this.Template != null)
            {
                ControlTemplate tmpTemplate = this.Template;
                this.Template = null;
                this.Template = tmpTemplate;
            }
        }
        #endregion
    }
}
