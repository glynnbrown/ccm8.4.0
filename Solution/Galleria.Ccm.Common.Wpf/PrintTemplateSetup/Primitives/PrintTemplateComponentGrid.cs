﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateComponentGrid : DataGrid
    {
        #region Fields
        private const Byte _defaultRowCount = 50;
        private const Int32 _defaultColumnWidth = 80;
        #endregion

        #region Properties

        #region DataSheetId

        /// <summary>
        /// DataSheetId dependency property definition
        /// </summary>
        public static readonly DependencyProperty DataSheetIdProperty =
            DependencyProperty.Register("DataSheetId", typeof(String), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(null, OnDataSheetIdPropertyChanged));

        /// <summary>
        /// Called whenever the data sheet id changes.
        /// </summary>
        private static void OnDataSheetIdPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PrintTemplateComponentGrid)obj).BeginUpdateFromDataSheet();
        }

        /// <summary>
        /// Gets/Sets the id of the linked datasheet
        /// </summary>
        public Object DataSheetId
        {
            get { return GetValue(DataSheetIdProperty); }
            set { SetValue(DataSheetIdProperty, value); }
        }

        #endregion

        #region IsDataSheetAvailable

        /// <summary>
        /// IsDataSheetAvailable dependency property definition
        /// </summary>
        public static readonly DependencyProperty IsDataSheetAvailableProperty =
            DependencyProperty.Register("IsDataSheetAvailable", typeof(Boolean?), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns true if the data sheet is avaialble, false if not,
        /// or null if still loading.
        /// </summary>
        public Boolean? IsDataSheetAvailable
        {
            get { return (Boolean?)GetValue(IsDataSheetAvailableProperty); }
            private set { SetValue(IsDataSheetAvailableProperty, value); }
        }

        #endregion


        #region Font Size

        public static readonly DependencyProperty GridFontSizeProperty =
            DependencyProperty.Register("GridFontSize", typeof(Byte), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata((Byte)11));

        /// <summary>
        /// Gets/Sets the grid font size
        /// </summary>
        public Byte GridFontSize
        {
            get { return (Byte)GetValue(GridFontSizeProperty); }
            set { SetValue(GridFontSizeProperty, value); }
        }
        #endregion

        #region Font Family

        public static readonly DependencyProperty GridFontFamilyProperty =
            DependencyProperty.Register("GridFontFamily", typeof(FontFamily), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(new FontFamily("Arial")));

        /// <summary>
        /// Gets/Sets the grid font Family
        /// </summary>
        public FontFamily GridFontFamily
        {
            get { return (FontFamily)GetValue(GridFontFamilyProperty); }
            set { SetValue(GridFontFamilyProperty, value); }
        }

        #endregion

        #region Font Weight

        public static readonly DependencyProperty GridFontWeightProperty =
            DependencyProperty.Register("GridFontWeight", typeof(FontWeight), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(FontWeights.Normal));

        /// <summary>
        /// Gets/Sets the grid font Family
        /// </summary>
        public FontWeight GridFontWeight
        {
            get { return (FontWeight)GetValue(GridFontWeightProperty); }
            set { SetValue(GridFontWeightProperty, value); }
        }

        #endregion

        #region Font Style

        public static readonly DependencyProperty GridFontStyleProperty =
            DependencyProperty.Register("GridFontStyle", typeof(FontStyle), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(FontStyles.Normal));

        /// <summary>
        /// Gets/Sets the grid font Style
        /// </summary>
        public FontStyle GridFontStyle
        {
            get { return (FontStyle)GetValue(GridFontStyleProperty); }
            set { SetValue(GridFontStyleProperty, value); }
        }
        #endregion

        #region Font Underlined

        public static readonly DependencyProperty GridFontUnderlinedProperty =
            DependencyProperty.Register("GridFontUnderlined", typeof(Boolean), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(false, OnGridFontUnderlinedPropertyChanged));

        /// <summary>
        /// Gets/Sets the grid font Underlined
        /// </summary>
        public Boolean GridFontUnderlined
        {
            get { return (Boolean)GetValue(GridFontUnderlinedProperty); }
            set { SetValue(GridFontUnderlinedProperty, value); }
        }

        private static void OnGridFontUnderlinedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentGrid grid = (PrintTemplateComponentGrid)obj;

            if (grid != null)
            {
                Boolean fontUnderlined = (Boolean)e.NewValue;

                grid.GridFontDecorations = (fontUnderlined) ? TextDecorations.Underline : new TextDecorationCollection();
            }
        }
        #endregion

        #region Font Decorations

        public static readonly DependencyProperty GridFontDecorationsProperty =
            DependencyProperty.Register("GridFontDecorations", typeof(TextDecorationCollection), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata());

        /// <summary>
        /// Gets/Sets the grid font Decorations
        /// </summary>
        public TextDecorationCollection GridFontDecorations
        {
            get { return (TextDecorationCollection)GetValue(GridFontDecorationsProperty); }
            set { SetValue(GridFontDecorationsProperty, value); }
        }

        #endregion

        #region Grid Foreground

        public static readonly DependencyProperty GridForegroundProperty =
            DependencyProperty.Register("GridForeground", typeof(Brush), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata());

        /// <summary>
        /// Gets/Sets the grid foreground
        /// </summary>
        public Brush GridForeground
        {
            get { return (Brush)GetValue(GridForegroundProperty); }
            set { SetValue(GridForegroundProperty, value); }
        }
        #endregion

        #region Row Count

        public static readonly DependencyProperty RowCountProperty =
            DependencyProperty.Register("RowCount", typeof(Int32), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(0, OnRowCountPropertyChanged));

        /// <summary>
        /// Gets/Sets the grid row count
        /// </summary>
        public Int32 RowCount
        {
            get { return (Int32)GetValue(RowCountProperty); }
            set { SetValue(RowCountProperty, value); }
        }

        private static void OnRowCountPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            DataGrid grid = (DataGrid)obj;

            if (grid != null && (Int32)e.NewValue > 0)
            {
                ObservableCollection<String> items = new ObservableCollection<String>();

                //Loop through the new number of rows and add to grid
                for (int i = 0; i < (Int32)e.NewValue; i++)
                {
                    String row = String.Empty;
                    items.Add(row);
                }

                grid.ItemsSource = items;
            }
        }
        #endregion

        #region Grid Style
        public static readonly DependencyProperty GridStyleProperty =
            DependencyProperty.Register("GridStyle", typeof(PrintTemplateComponentGridStyle), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata());

        /// <summary>
        /// Gets/Sets if the item is selected
        /// </summary>
        public PrintTemplateComponentGridStyle GridStyle
        {
            get { return (PrintTemplateComponentGridStyle)GetValue(GridStyleProperty); }
            set { SetValue(GridStyleProperty, value); }
        }
        #endregion

        #region Grid Column Widths

        public static readonly DependencyProperty GridColumnWidthsProperty =
            DependencyProperty.Register("GridColumnWidths", typeof(String), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(null, OnGridColumnWidthsPropertyChanged));

        /// <summary>
        /// Gets/Sets the column List
        /// </summary>
        public String GridColumnWidths
        {
            get { return (String)GetValue(GridColumnWidthsProperty); }
            private set { SetValue(GridColumnWidthsProperty, value); }
        }

        private static void OnGridColumnWidthsPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentGrid grid = (PrintTemplateComponentGrid)obj;

            if (grid != null)
            {
                String columnWidths = (String)e.NewValue;

                if (columnWidths != null && columnWidths.Length > 0)
                {
                    String[] columnWidthsArray = columnWidths.Split('|');
                    Int32 index = 0;

                    foreach (String column in columnWidthsArray)
                    {
                        //GFS-24024 : Added defensive coding to cope with locale-formatted decimal values in colwidths psp string
                        Int32 newWidth = PrintTemplateComponentGrid.GetColumnWidthFromDoubleAsString(column);
                        if (grid.Columns.Count >= index + 1)
                        {
                            if (newWidth != 0)
                            {
                                grid.Columns[index].Width = newWidth;
                            }
                        }

                        index++;
                    }
                }
            }
        }
        #endregion

        #region Grid Column List

        public static readonly DependencyProperty GridColumnListProperty =
            DependencyProperty.Register("GridColumnList", typeof(String), typeof(PrintTemplateComponentGrid),
            new PropertyMetadata(null, OnGridColumnListPropertyChanged));

        /// <summary>
        /// Gets/Sets the column List
        /// </summary>
        public String GridColumnList
        {
            get { return (String)GetValue(GridColumnListProperty); }
            private set { SetValue(GridColumnListProperty, value); }
        }

        private static void OnGridColumnListPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentGrid grid = (PrintTemplateComponentGrid)obj;

            if (grid != null)
            {
                //Clear out the columns
                grid.Columns.Clear();

                String columnList = (String)e.NewValue;

                if (columnList != null && columnList.Length > 0)
                {
                    String[] columnListArray = columnList.Split('|');

                    //Create new columns
                    foreach (String column in columnListArray)
                    {
                        DataGridTextColumn dataGridColumn = new DataGridTextColumn();
                        dataGridColumn.IsReadOnly = true;
                        dataGridColumn.Header = column;
                        dataGridColumn.Width = _defaultColumnWidth;

                        grid.Columns.Add(dataGridColumn);
                    }
                }

                if (grid.GridColumnWidths != null && grid.GridColumnWidths.Length > 0)
                {
                    String[] columnWidthsArray = grid.GridColumnWidths.Split('|');
                    Int32 index = 0;

                    //Set the column widths
                    foreach (String column in columnWidthsArray)
                    {
                        //GFS-24024 : Added defensive coding to cope with locale-formatted decimal values in colwidths psp string
                        Int32 newWidth = PrintTemplateComponentGrid.GetColumnWidthFromDoubleAsString(column);
                        if (grid.Columns.Count >= index + 1)
                        {
                            grid.Columns[index].Width = newWidth;
                        }

                        index++;
                    }
                }
            }
        }
        #endregion

        #region  Can User Select Rows

        public static readonly DependencyProperty CanUserSelectRowsProperty =
            DependencyProperty.RegisterAttached("CanUserSelectRows", typeof(Boolean), typeof(PrintTemplateComponentGrid),
            new FrameworkPropertyMetadata(false));
        /// <summary>
        /// Get/Set whether rows can be selected
        /// </summary>
        public Boolean CanUserSelectRows
        {
            get { return (Boolean)GetValue(CanUserSelectRowsProperty); }
            set { SetValue(CanUserSelectRowsProperty, value); }
        }

        /// <summary>
        /// If row selection is not allowed then clear the selection and 
        /// cancel
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (!this.CanUserSelectRows)
            {
                ClearSelectedItems();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Clears selected items
        /// </summary>
        public void ClearSelectedItems()
        {
            if (this.SelectionMode == DataGridSelectionMode.Single)
            {
                this.SelectedItem = null;
            }
            else
            {
                this.SelectedItems.Clear();
            }
        }

        #endregion

        #endregion

        #region Constructor
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
        static PrintTemplateComponentGrid()
        {
            // Style override
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PrintTemplateComponentGrid), new FrameworkPropertyMetadata(typeof(PrintTemplateComponentGrid)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateComponentGrid()
        {
            this.RowCount = _defaultRowCount;

            Mouse.AddPreviewMouseUpHandler(this, PrintTemplateComponentGrid_MouseButtonUp);
        }
        #endregion

        #region Overrides

        // Capture the column widths changing
        void PrintTemplateComponentGrid_MouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            ColumnWidthsChanged();
        }
        #endregion

        #region Methods

        private void ColumnWidthsChanged()
        {
            if (this.Columns.Count > 0)
            {
                String columnWidths = String.Empty;

                foreach (DataGridTextColumn column in this.Columns)
                {
                    columnWidths = columnWidths + Convert.ToInt32(column.ActualWidth).ToString() + "|";
                }

                if (!String.IsNullOrEmpty(columnWidths))
                {
                    columnWidths = columnWidths.TrimEnd('|');
                }

                this.GridColumnWidths = columnWidths;
            }
        }

        /// <summary>
        /// Convert a string representation of a double to an integer. String value may be in current
        /// culture notation, but also may be in a different culture
        /// </summary>
        /// <param name="columnWidth">The double value to parse</param>
        /// <returns></returns>
        public static Int32 GetColumnWidthFromDoubleAsString(String columnWidth)
        {
            Int32 returnValue = _defaultColumnWidth;

            //we may be a uk user on lithuanian server so cannot use cultureinfo to parse this
            //For column widths it is ok to truncate to the nearest integer

            //Strip any numeric values after first decimal point
            Int32 sepPos = columnWidth.IndexOf(".");
            if (sepPos > -1)
            {
                columnWidth = columnWidth.Substring(0, sepPos);
            }
            //Strip any numeric values after first comma
            sepPos = columnWidth.IndexOf(",");
            if (sepPos > -1)
            {
                columnWidth = columnWidth.Substring(0, sepPos);
            }
            //Try to convert to an int
            if (Int32.TryParse(columnWidth, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out returnValue))
            {
                return returnValue;
            }
            return _defaultColumnWidth;
        }

        /// <summary>
        /// Updates the properties of this node from the attached datasheet.
        /// </summary>
        private void BeginUpdateFromDataSheet()
        {
            //clear down.
            this.GridColumnWidths = null;
            this.GridColumnList = null;

            if (this.DataSheetId == null)
            {
                this.IsDataSheetAvailable = false;
                return;
            }
            else if(this.DataSheetId is String && !System.IO.File.Exists(this.DataSheetId as String))
            {
                String alternativeId = null;

                var parentCtrl = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ContentControl>((DependencyObject)this);
                if(parentCtrl != null)
                {
                    PrintTemplateComponent component = parentCtrl.Content as PrintTemplateComponent;
                    if(component != null)
                    {
                        PrintTemplate template = component.Parent.Parent.Parent;
                        if(template != null && template.IsFile)
                        {
                            String templateFolder = System.IO.Path.GetDirectoryName((String)template.Id);
                            alternativeId = System.IO.Path.Combine(templateFolder, System.IO.Path.GetFileName(((String)this.DataSheetId)));
                            if (!System.IO.File.Exists(alternativeId)) alternativeId = null;
                        }
                    }
                }

                if(String.IsNullOrEmpty(alternativeId))
                {
                    this.IsDataSheetAvailable = false;
                    return;
                }
                else
                {
                    this.DataSheetId = alternativeId;
                }
                
            }
            this.IsDataSheetAvailable = null;

            Csla.Threading.BackgroundWorker getDataSheetWorker = new Csla.Threading.BackgroundWorker();
            getDataSheetWorker.DoWork += GetDataSheetWorker_DoWork;
            getDataSheetWorker.RunWorkerCompleted += GetDataSheetWorker_RunWorkerCompleted;

            getDataSheetWorker.RunWorkerAsync(this.DataSheetId);
        }

        /// <summary>
        /// Carries out the work to fetch the datasheet information
        /// asynchronously.
        /// </summary>
        private void GetDataSheetWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Get the datasheet
            CustomColumnLayout dataSheet
                = CustomColumnLayout.FetchByFilename(e.Argument as String, /*asReadOnly*/true);

            //set the result.
            e.Result = dataSheet;
        }

        /// <summary>
        /// Processes the result from the datasheet.
        /// </summary>
        private void GetDataSheetWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker getDataSheetWorker = (Csla.Threading.BackgroundWorker)sender;
            getDataSheetWorker.DoWork -= GetDataSheetWorker_DoWork;
            getDataSheetWorker.RunWorkerCompleted -= GetDataSheetWorker_RunWorkerCompleted;

            if (e.Error != null)
            {
                this.IsDataSheetAvailable = false;
                return;
            }


            CustomColumnLayout dataSheet = e.Result as CustomColumnLayout;

            //build the list of column names
            StringBuilder nameSb = new StringBuilder();
            StringBuilder widthSb = new StringBuilder();

            Int32 autoColWidth = 0;
            if (this.ActualWidth > 0)
            {
                Int32 colsCount = dataSheet.Columns.Count(c => c.Width == 0);
                if (colsCount > 0)
                {
                    autoColWidth =
                       (Int32)((this.ActualWidth - dataSheet.Columns.Sum(c => c.Width))
                        / colsCount);
                }
            }


            foreach (var col in dataSheet.Columns)
            {
                //Column header:
                String columnName = col.DisplayName;
                if (String.IsNullOrWhiteSpace(columnName))
                {
                    //try to get the name fomr the field infos.
                    ObjectFieldInfo fieldInfo =
                    ObjectFieldInfo.ExtractFieldsFromText(col.Path,
                    PlanogramFieldHelper.EnumerateAllFields()).FirstOrDefault();

                    if (fieldInfo != null) columnName = fieldInfo.FieldFriendlyName;
                }
                if (nameSb.Length > 0) nameSb.AppendFormat("|{0}", columnName);
                else nameSb.Append(columnName);


                //Column width:
                Int32 colWidth = (col.Width > 0) ? col.Width : autoColWidth;
                if (widthSb.Length > 0) widthSb.AppendFormat("|{0}", colWidth);
                else widthSb.Append(colWidth);
            }

            this.GridColumnList = nameSb.ToString();
            this.GridColumnWidths = widthSb.ToString();



            this.IsDataSheetAvailable = true;
        }

        #endregion
    }
}
