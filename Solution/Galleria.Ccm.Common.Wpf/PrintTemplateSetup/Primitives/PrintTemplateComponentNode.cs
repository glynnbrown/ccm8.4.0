﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using Galleria.Ccm.Model;
using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    /// <summary>
    /// Print Template Component node content depicting a print option component
    /// </summary>
    public sealed class PrintTemplateComponentNode : ContentControl
    {
        #region Fields
        private bool _suppressComponentChangedHandlers;
        private const Single _scale = 2.835F;
        #endregion

        #region Properties

        #region Unit Context Property

        public static readonly DependencyProperty UnitContextProperty =
            DependencyProperty.Register("UnitContext", typeof(PrintTemplateComponent),
            typeof(PrintTemplateComponentNode),
            new PropertyMetadata(null, OnUnitContextPropertyChanged));

        private static void OnUnitContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateComponentNode senderControl = (PrintTemplateComponentNode)obj;

            if (e.OldValue != null)
            {
                PrintTemplateComponent oldValue = (PrintTemplateComponent)e.OldValue;
                oldValue.PropertyChanged -= senderControl.PrintTemplateComponent_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PrintTemplateComponent newValue = (PrintTemplateComponent)e.NewValue;
                senderControl.Content = newValue;
                newValue.PropertyChanged += senderControl.PrintTemplateComponent_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public PrintTemplateComponent UnitContext
        {
            get { return (PrintTemplateComponent)GetValue(UnitContextProperty); }
            set { SetValue(UnitContextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static PrintTemplateComponentNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(PrintTemplateComponentNode), new FrameworkPropertyMetadata(typeof(PrintTemplateComponentNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public PrintTemplateComponentNode(PrintTemplateComponent unitContext)
        {
            this.AddHandler(PrintTemplateComponentItem.PrintTemplateComponentPropertyChangedEvent, (RoutedEventHandler)OnPrintTemplateComponentPropertyChanged);

            this.UnitContext = unitContext;

            _suppressComponentChangedHandlers = true;

            PrintTemplateComponentItem.SetXPosition(this, this.UnitContext.X * _scale);
            PrintTemplateComponentItem.SetYPosition(this, this.UnitContext.Y * _scale);
            PrintTemplateComponentItem.SetHeight(this, this.UnitContext.Height * _scale);
            PrintTemplateComponentItem.SetWidth(this, this.UnitContext.Width * _scale);

            _suppressComponentChangedHandlers = false;
        }

        #endregion

        #region Event Handlers

        private void OnPrintTemplateComponentPropertyChanged(object sender, RoutedEventArgs e)
        {
            if (!_suppressComponentChangedHandlers)
            {
                this.UnitContext.X = Convert.ToSingle(PrintTemplateComponentItem.GetXPosition((PrintTemplateComponentNode)sender) / _scale);
                this.UnitContext.Y = Convert.ToSingle(PrintTemplateComponentItem.GetYPosition((PrintTemplateComponentNode)sender) / _scale);
                this.UnitContext.Height = Convert.ToSingle(PrintTemplateComponentItem.GetHeight((PrintTemplateComponentNode)sender) / _scale);
                this.UnitContext.Width = Convert.ToSingle(PrintTemplateComponentItem.GetWidth((PrintTemplateComponentNode)sender) / _scale);
            }
        }

        /// <summary>
        /// Event handler for the component changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintTemplateComponent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _suppressComponentChangedHandlers = true;

            PrintTemplateComponentItem.SetXPosition(this, this.UnitContext.X * _scale);
            PrintTemplateComponentItem.SetYPosition(this, this.UnitContext.Y * _scale);
            PrintTemplateComponentItem.SetHeight(this, this.UnitContext.Height * _scale);
            PrintTemplateComponentItem.SetWidth(this, this.UnitContext.Width * _scale);

            _suppressComponentChangedHandlers = false;
        }

        #endregion
    }
}
