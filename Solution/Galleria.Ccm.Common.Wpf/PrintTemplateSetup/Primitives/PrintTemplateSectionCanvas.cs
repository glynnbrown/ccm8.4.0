﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Copied from GFS & amended
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup.Primitives
{
    public class PrintTemplateSectionCanvas : Canvas
    {
        #region Fields
        private ObservableCollection<FrameworkElement> _components = new ObservableCollection<FrameworkElement>(); //the content items to be displayed on the canvas
        private bool _suppressSelectionChangedHandlers;
        #endregion

        #region Properties

        #region SelectedItem

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(FrameworkElement), typeof(PrintTemplateSectionCanvas),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public FrameworkElement SelectedItem
        {
            get { return (FrameworkElement)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        #endregion

        #region Components
        /// <summary>
        /// Returns the collection of components displayed by this control
        /// </summary>
        public ObservableCollection<FrameworkElement> Components
        {
            get { return _components; }
        }
        #endregion

        #endregion

        #region Constructor
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Dependency properties are initialized in-line.")]
        static PrintTemplateSectionCanvas()
        {
            // Style override
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PrintTemplateSectionCanvas), new FrameworkPropertyMetadata(typeof(PrintTemplateSectionCanvas)));
        }

        public PrintTemplateSectionCanvas()
        {
            _components.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Components_CollectionChanged);

            this.AddHandler(PrintTemplateComponentContainer.SelectedEvent, (RoutedEventHandler)OnComponentSelected);
            this.AddHandler(PrintTemplateComponentContainer.DeselectedEvent, (RoutedEventHandler)OnComponentDeselected);
            this.AddHandler(PrintTemplateComponentLine.SelectedEvent, (RoutedEventHandler)OnComponentLineSelected);
            this.AddHandler(PrintTemplateComponentLine.DeselectedEvent, (RoutedEventHandler)OnComponentLineDeselected);
        }
        #endregion

        #region Event Handlers

        #region Components Collection Changed

        /// <summary>
        /// Responds to a change in the items to be held on the diagram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Components_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                    {
                        // deselect the other components
                        foreach (var child in this.Children)
                        {
                            PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                            if (container != null)
                            {
                                container.IsSelected = false;
                            }
                            else
                            {
                                PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                                if (line != null)
                                {
                                    line.IsSelected = false;
                                }
                            }
                        }

                        PrintTemplateComponentNode addedPrintTemplateComponentNode = item as PrintTemplateComponentNode;
                        if (addedPrintTemplateComponentNode != null)
                        {
                            // create a new component
                            PrintTemplateComponentContainer newContainer = new PrintTemplateComponentContainer(addedPrintTemplateComponentNode);

                            // add the item as the content of the container
                            newContainer.Content = addedPrintTemplateComponentNode;

                            // add the component to the canvas
                            this.Children.Add(newContainer);

                            // if loading then the item will already have co-ordinates
                            Double xPosition = PrintTemplateComponentItem.GetXPosition(addedPrintTemplateComponentNode);
                            Double yPosition = PrintTemplateComponentItem.GetYPosition(addedPrintTemplateComponentNode);

                            // position the component where it was
                            newContainer.Move(xPosition, yPosition, PrintTemplateComponentItem.GetHeight(addedPrintTemplateComponentNode), PrintTemplateComponentItem.GetWidth(addedPrintTemplateComponentNode));

                            // select the new component
                            newContainer.IsSelected = true;
                        }
                        else
                        {
                            PrintTemplateComponentLine addedPrintTemplateComponentLine = item as PrintTemplateComponentLine;
                            if (addedPrintTemplateComponentLine != null)
                            {
                                // add the line to the canvas
                                this.Children.Add(addedPrintTemplateComponentLine);

                                // position the component where it was
                                addedPrintTemplateComponentLine.Move(addedPrintTemplateComponentLine.X1, addedPrintTemplateComponentLine.Y1, addedPrintTemplateComponentLine.X2, addedPrintTemplateComponentLine.Y2);

                                // select the new component
                                addedPrintTemplateComponentLine.IsSelected = true;
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:

                    foreach (FrameworkElement item in e.OldItems)
                    {
                        foreach (var child in this.Children)
                        {
                            PrintTemplateComponentContainer container = child as PrintTemplateComponentContainer;
                            if (container != null)
                            {
                                if (container.UIElement == item)
                                {
                                    //remove the container
                                    this.Children.Remove(container);
                                    break;
                                }
                            }
                            else
                            {
                                PrintTemplateComponentLine line = child as PrintTemplateComponentLine;
                                if (line != null)
                                {
                                    if (line == item)
                                    {
                                        //remove the lines children
                                        line.DisposeChildren();

                                        //remove the line
                                        this.Children.Remove(line);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:

                    foreach (PrintTemplateComponentContainer child in this.Children.OfType<PrintTemplateComponentContainer>().ToList())
                    {
                        //remove the component container
                        this.Children.Remove(child);
                    }

                    foreach (PrintTemplateComponentLine child in this.Children.OfType<PrintTemplateComponentLine>().ToList())
                    {
                        //remove the lines children
                        child.DisposeChildren();

                        //remove the line
                        this.Children.Remove(child);
                    }

                    break;
            }
        }

        #endregion

        #region On Component Selected
        /// <summary>
        /// Responds to the selection of a component
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnComponentSelected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;

                PrintTemplateComponentContainer senderComponent = (PrintTemplateComponentContainer)e.Source;
                if (senderComponent != null)
                {
                    //process the selection
                    senderComponent.IsSelected = true;

                    //set the selected item
                    this.SelectedItem = senderComponent.UIElement;

                    //raise out the event
                    if (this.ComponentSelectionChanged != null)
                    {
                        this.ComponentSelectionChanged(this, EventArgs.Empty);
                    }
                }

                _suppressSelectionChangedHandlers = false;
            }
        }
        #endregion

        #region On Component Deselected

        private void OnComponentDeselected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;

                PrintTemplateComponentContainer senderComponent = (PrintTemplateComponentContainer)e.Source;
                if (senderComponent != null)
                {
                    //process the selection
                    senderComponent.IsSelected = false;
                }

                _suppressSelectionChangedHandlers = false;
            }
        }
        #endregion

        #region On Component Line Selected
        /// <summary>
        /// Responds to the selection of a component line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnComponentLineSelected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;

                PrintTemplateComponentLine senderComponent = (PrintTemplateComponentLine)e.Source;
                if (senderComponent != null)
                {
                    //process the selection
                    senderComponent.IsSelected = true;

                    //set the selected item
                    this.SelectedItem = senderComponent;

                    //raise out the event
                    if (this.ComponentSelectionChanged != null)
                    {
                        this.ComponentSelectionChanged(this, EventArgs.Empty);
                    }
                }

                _suppressSelectionChangedHandlers = false;
            }
        }
        #endregion

        #region On Component Line Deselected

        private void OnComponentLineDeselected(Object sender, RoutedEventArgs e)
        {
            if (!_suppressSelectionChangedHandlers)
            {
                _suppressSelectionChangedHandlers = true;

                PrintTemplateComponentLine senderComponent = (PrintTemplateComponentLine)e.Source;
                if (senderComponent != null)
                {
                    //process the selection
                    senderComponent.IsSelected = false;
                }

                _suppressSelectionChangedHandlers = false;
            }
        }
        #endregion

        #endregion

        #region Events
        public event EventHandler ComponentSelectionChanged;
        #endregion

        #region Methods

        /// <summary>
        /// Returns the container for the given component item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal PrintTemplateComponentContainer GetContainerFromItem(FrameworkElement item)
        {
            return
            this.Children.OfType<PrintTemplateComponentContainer>()
                .FirstOrDefault(c => c.UIElement == item);
        }

        #endregion
    }
}
