﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Fluent;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateLabelSelector.xaml
    /// </summary>
    public sealed partial class PrintTemplateLabelSelector : ExtendedRibbonWindow
    {
        #region ViewModel

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
             DependencyProperty.Register("ViewModel", typeof(PrintTemplateLabelSelectorViewModel), typeof(PrintTemplateLabelSelector),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateLabelSelector senderControl = (PrintTemplateLabelSelector)obj;

            if (e.OldValue != null)
            {
                PrintTemplateLabelSelectorViewModel oldModel = (PrintTemplateLabelSelectorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                PrintTemplateLabelSelectorViewModel newModel = (PrintTemplateLabelSelectorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public PrintTemplateLabelSelectorViewModel ViewModel
        {
            get { return (PrintTemplateLabelSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateLabelSelector(PrintTemplateLabelSelectorViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;

            this.Title =
                (ViewModel.LabelType == Model.LabelType.Product) ?
                Message.PrintTemplateLabelSelector_ProductTitle
                : Message.PrintTemplateLabelSelector_FixtureTitle;

            this.Loaded += OnLoaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        private void SplitButton_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            DropDownButton activeButton = sender as DropDownButton;
            if (activeButton == null) return;

            //Expands/collapse the dropdown when the return key is pressed and
            //gives keyboard focus to the first item to allow navigation
            if (activeButton.HasItems && e.Key == Key.Return)
            {
                activeButton.IsDropDownOpen = !activeButton.IsDropDownOpen;
                e.Handled = true;
            }

            //Makes the dropdown menu items navigable with arrow keys
            else if (e.Key == Key.Down || e.Key == Key.Up)
            {
                Boolean up = e.Key == Key.Up;

                if (up && activeButton.Items.CurrentPosition != 0)
                {
                    activeButton.Items.MoveCurrentToPrevious();
                }
                else if (!up && activeButton.Items.CurrentPosition != activeButton.Items.Count - 1)
                {
                    activeButton.Items.MoveCurrentToNext();
                }
                else
                {
                    if (up) activeButton.Items.MoveCurrentToLast();
                    else activeButton.Items.MoveCurrentToFirst();
                }

                Keyboard.Focus(activeButton.Items.CurrentItem as MenuItem);
                e.Handled = true;
            }

            //Execute menu item's bound command on pressing space bar
            else if (e.Key == Key.Space && activeButton.IsDropDownOpen)
            {
                MenuItem selectedMenuItem = activeButton.Items.CurrentItem as MenuItem;
                if (selectedMenuItem == null) return;
                if (selectedMenuItem.Command.CanExecute(null)) selectedMenuItem.Command.Execute(null);
                e.Handled = true;
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }

   
}
