﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// Interaction logic for PrintTemplateCameraSettings.xaml
    /// </summary>
    public sealed partial class PrintTemplateCameraSettings : ExtendedRibbonWindow
    {
        #region Fields
        private ModelConstruct3D _model;
        #endregion

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PrintTemplateCameraSettingsViewModel), typeof(PrintTemplateCameraSettings),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the value of the viewmodel dependency property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PrintTemplateCameraSettings senderControl = (PrintTemplateCameraSettings)obj;

            if (e.OldValue != null)
            {
                PrintTemplateCameraSettingsViewModel oldModel = (PrintTemplateCameraSettingsViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                PrintTemplateCameraSettingsViewModel newModel = (PrintTemplateCameraSettingsViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel context.
        /// </summary>
        public PrintTemplateCameraSettingsViewModel ViewModel
        {
            get { return (PrintTemplateCameraSettingsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PrintTemplateCameraSettings(PrintTemplateCameraSettingsViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the position viewer initially loads
        /// </summary>
        private void ViewPortControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewPortControl.Loaded -= ViewPortControl_Loaded;

            //setup the camera
            PlanCameraHelper.SetupForCameraType(this.ViewPortControl, CameraViewType.Perspective);

            this.ViewPortControl.ZoomExtents();

            //add the model
            ModelConstruct3DData modelData = this.ViewModel.RenderData;
            _model = new ModelConstruct3D(modelData);

            modelData.Position = new PointValue(-modelData.Size.Width / 2, -modelData.Size.Height / 2, -modelData.Size.Depth / 2);

            this.ViewPortControl.Children.Add(_model);

            this.ViewPortControl.IsZoomEnabled = true;
            PlanCameraHelper.ZoomToFit(this.ViewPortControl, _model);
            PlanCameraHelper.ZoomOut(this.ViewPortControl);
            this.ViewPortControl.IsZoomEnabled = false;

            PerspectiveCamera cam = this.ViewPortControl.Camera as PerspectiveCamera;

            if (this.ViewModel.CameraLookDirection == new Vector3D())
            {
                this.ViewModel.CameraLookDirection = cam.LookDirection;
            }
            if (this.ViewModel.CameraPosition == new Point3D())
            {
                this.ViewModel.CameraPosition = cam.Position;
            }


            BindingOperations.SetBinding(cam, PerspectiveCamera.PositionProperty,
                new Binding(PrintTemplateCameraSettingsViewModel.CameraPositionProperty.Path)
                {
                    Source = this.ViewModel,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });

            BindingOperations.SetBinding(cam, PerspectiveCamera.LookDirectionProperty,
                new Binding(PrintTemplateCameraSettingsViewModel.CameraLookDirectionProperty.Path)
                {
                    Source = this.ViewModel,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });

            this.ViewPortControl.ZoomExtents();
        }

        private void ViewPortControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.ViewPortControl.IsLoaded)
            {
                this.ViewPortControl.ZoomExtents();
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            this.ViewPortControl.Children.Clear();
            _model.Dispose();
            _model = null;

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }

    /// <summary>
    /// ViewModel controller for PrintTemplateCameraSettings
    /// </summary>
    public sealed class PrintTemplateCameraSettingsViewModel : WindowViewModelBase
    {
        #region Fields

        private PrintTemplateComponentDetail _componentDetail;
        private Plan3DData _renderData;

        private Boolean? _dialogResult;

        private Vector3D _cameraLookDirection;
        private Point3D _cameraPosition;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CameraLookDirectionProperty = GetPropertyPath<PrintTemplateCameraSettingsViewModel>(p => p.CameraLookDirection);
        public static readonly PropertyPath CameraPositionProperty = GetPropertyPath<PrintTemplateCameraSettingsViewModel>(p => p.CameraPosition);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        /// <summary>
        /// Returns the viewdata to render.
        /// </summary>
        public Plan3DData RenderData
        {
            get { return _renderData; }
            private set
            {
                _renderData = value;
            }
        }

        /// <summary>
        /// Gets/Sets the camera look direction
        /// </summary>
        public Vector3D CameraLookDirection
        {
            get { return _cameraLookDirection; }
            set
            {
                _cameraLookDirection = value;
                OnPropertyChanged(CameraLookDirectionProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the camera position.
        /// </summary>
        public Point3D CameraPosition
        {
            get { return _cameraPosition; }
            set
            {
                _cameraPosition = value;
                OnPropertyChanged(CameraPositionProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PrintTemplateCameraSettingsViewModel(PrintTemplateComponentDetail componentDetail, Planogram planogramContext = null)
        {
            _componentDetail = componentDetail;
            this.RenderData = GenerateRenderData(planogramContext);

            if (!String.IsNullOrWhiteSpace(componentDetail.PlanogramCameraLookDirection))
            {
                try
                {
                    this.CameraLookDirection = Vector3D.Parse(componentDetail.PlanogramCameraLookDirection);
                }
                catch (Exception) { }
            }

            if (!String.IsNullOrWhiteSpace(componentDetail.PlanogramCameraPosition))
            {
                try
                {
                    this.CameraPosition = Point3D.Parse(componentDetail.PlanogramCameraPosition);
                }
                catch (Exception) { }
            }

        }

        #endregion

        #region Commands

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Applys changes and closes the dialog.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    RegisterCommand(_okCommand);
                }
                return _okCommand;
            }
        }

        private void OK_Executed()
        {
            _componentDetail.PlanogramCameraLookDirection = this.CameraLookDirection.ToString();
            _componentDetail.PlanogramCameraPosition = this.CameraPosition.ToString();

            this.DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the dialog.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        private static Plan3DData GenerateRenderData(Planogram context)
        {
            if (context != null)
            {
                return new Plan3DData(new PlanogramViewModel(context), new PlanRenderSettings());
            }
            else
            {
                SystemSettings defaultSettings = SystemSettings.NewSystemSettings();

                Package pk = Package.NewPackage(0, PackageLockType.Unknown);
                Planogram defaultPlan = Planogram.NewPlanogram();
                defaultPlan.FixtureItems.Add(defaultSettings);

                //add a backboard and base
                defaultPlan.Fixtures[0].Components.Add(PlanogramComponentType.Base, defaultSettings);
                defaultPlan.Fixtures[0].Components.Add(PlanogramComponentType.Backboard, defaultSettings);

                //and a couple of shelves
                var shelf1 = defaultPlan.Fixtures[0].Components.Add(PlanogramComponentType.Shelf, defaultSettings);
                shelf1.Y = defaultPlan.Fixtures[0].Height / 3;
                
                var shelf2 = defaultPlan.Fixtures[0].Components.Add(PlanogramComponentType.Shelf, defaultSettings);
                shelf2.Y = (defaultPlan.Fixtures[0].Height / 3) *2;


                defaultPlan.UpdateSizeFromFixtures();

                return new Plan3DData(new PlanogramViewModel(defaultPlan), new PlanRenderSettings());
            }

        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                this.RenderData.Dispose();
                this.RenderData = null;

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
