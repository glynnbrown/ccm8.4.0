﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// CCM-30738 : L.Ineson
//	Created
// V8-31026 : A.Probyn
//  Added defensive code to Save As commands as a label MUST be selected. When you open the window, it's null.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.PrintTemplateSetup
{
    /// <summary>
    /// ViewModel controller for PrintTemplateLabelSelector
    /// </summary>
    public sealed class PrintTemplateLabelSelectorViewModel : WindowViewModelBase, ILabelSetupViewModel
    {
        #region Fields
        private readonly Boolean _hasRepositoryConnection;
        private readonly ModelPermission<Model.Label> _itemRepositoryPerms;
        private Model.Label _selectedLabel; //Currently selected label
        private PrintTemplateComponentDetail _componentDetail;
        private LabelType _labelType;
        private String _selectedLabelFullName;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedLabelProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SelectedLabel);
        public static readonly PropertyPath SelectedLabelFullNameProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SelectedLabelFullName);
        public static readonly PropertyPath DisplayTextProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.DisplayText);

        //Commands
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath SetFieldCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.SetFieldCommand);
        public static readonly PropertyPath ClearSelectedLabelCommandProperty = GetPropertyPath<PrintTemplateLabelSelectorViewModel>(p => p.ClearSelectedLabelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the print template component detail.
        /// </summary>
        public PrintTemplateComponentDetail ComponentDetail
        {
            get { return _componentDetail; }
        }

        /// <summary>
        /// Returns the type of label this screen is selecting.
        /// </summary>
        public LabelType LabelType
        {
            get { return _labelType; }
        }

        /// <summary>
        /// Returns the selected label model.
        /// </summary>
        public Model.Label SelectedLabel
        {
            get { return _selectedLabel; }
            private set
            {
                _selectedLabel = value;

                OnPropertyChanged(SelectedLabelProperty);
                OnSelectedLabelChanged(value, SelectedLabel);
            }
        }

        /// <summary>
        /// Gets the name of the selected label.
        /// </summary>
        public String SelectedLabelFullName
        {
            get { return _selectedLabelFullName; }
            private set
            {
                _selectedLabelFullName = value;
                OnPropertyChanged(SelectedLabelFullNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the setting label content.
        /// </summary>
        public String DisplayText
        {
            get
            {
                return
                    (this.SelectedLabel != null) ?
                    ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(this.SelectedLabel.Text, PlanogramFieldHelper.EnumerateAllFields())
                    : String.Empty;
            }
            set
            {
                if (this.SelectedLabel != null)
                {
                    this.SelectedLabel.Text = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value, PlanogramFieldHelper.EnumerateAllFields());
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PrintTemplateLabelSelectorViewModel(PrintTemplateComponentDetail componentDetail, LabelType labelType)
        {
            _componentDetail = componentDetail;
            _labelType = labelType;

            Int32 entityId = CCMClient.ViewState.EntityId;
            _hasRepositoryConnection = entityId > 0;

            //get permissions for current repository.
            _itemRepositoryPerms = (_hasRepositoryConnection) ?
                new ModelPermission<Model.Label>(Model.Label.GetUserPermissions())
                : ModelPermission<Model.Label>.DenyAll();


            //load the current label - will be a readonly copy.
            _selectedLabelFullName = Message.PrintTemplateLabelSelector_NoLabelName;

            Object labelId = (labelType == Model.LabelType.Product) ?
                componentDetail.PlanogramProductLabelId
                : componentDetail.PlanogramFixtureLabelId;

            if (labelId != null)
            {
                try
                {
                    this.SelectedLabel = LabelUIHelper.FetchLabelAsReadonly(labelId);
                }
                catch (DataPortalException ex)
                {
                    RecordException(ex);

                    GetWindowService().ShowOkMessage(Services.MessageWindowType.Warning,
                        Message.PrintTemplateHighlightSelector_LabelNotFoundHeader,
                        Message.PrintTemplateHighlightSelector_LabelNotFoundDesc);
                }
            }
        }

        #endregion

        #region Commands

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens an existing label
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private void Open_Executed(object args)
        {
            if (this.OpenFromRepositoryCommand.CanExecute())
            {
                this.OpenFromRepositoryCommand.Execute();
            }
            else
            {
                this.OpenFromFileCommand.Execute();
            }
        }

        #endregion

        #region OpenFromRepository

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        o => OpenFromRepository_Executed(o),
                        o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_OpenFromRepository
                    };
                    RegisterCommand(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!_hasRepositoryConnection)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null)
            {
                GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
                win.ItemSource = LabelInfoList.FetchByEntityId(CCMClient.ViewState.EntityId).Where(l => l.Type == this.LabelType).ToList();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
                if (win.DialogResult != true) return;

                id = win.SelectedItems.Cast<LabelInfo>().First().Id;
            }


            //load the template
            base.ShowWaitCursor(true);
            try
            {
                this.SelectedLabel = Model.Label.FetchById(id, /*asReadonly*/true);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.Generic_OpenFromFile
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(Object args)
        {
            //show the file dialog if no args passed in.
            String file = args as String;
            if (String.IsNullOrEmpty(file)) file = LabelUIHelper.ShowOpenFileDialog();
            if (String.IsNullOrEmpty(file)) return;

            //Fetch the label as readonly.
            base.ShowWaitCursor(true);
            try
            {
                this.SelectedLabel = Model.Label.FetchByFilename(file, /*asReadOnly*/true);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves changes made to a new label setting and loads new as current
        /// /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.LabelSetupContent_SaveAs_Desc,
                        Icon = ImageResources.SaveAs_32
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }


        private Boolean SaveAs_CanExecute()
        {
            //must have a label selected
            if (this.SelectedLabel == null)
            {
                this.SaveAsToFileCommand.DisabledReason = Message.Generic_NoItemSelected;
                this.SaveAsCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            if (this.SaveAsToRepositoryCommand.CanExecute())
            {
                this.SaveAsToRepositoryCommand.Execute();
            }
            else
            {
                this.SaveAsToFileCommand.Execute();
            }
        }

        #endregion

        #region SaveAsToRepository

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                            o => SaveAsToRepository_Executed(),
                        o => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveToRepository
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);

                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            if (!_hasRepositoryConnection)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoRepositoryConnection;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must have a label selected
            if (this.SelectedLabel == null)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }


        private void SaveAsToRepository_Executed()
        {
            if (this.SelectedLabel != null)
            {
                String copyName;

                LabelInfoList existingItems;
                try
                {
                    existingItems = LabelInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
                }
                catch (DataPortalException ex)
                {
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }
                Predicate<String> isUniqueCheck =
                   (s) => { return !existingItems.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

                Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
                if (!nameAccepted) return;

                ShowWaitCursor(true);

                //copy the item and rename
                Model.Label itemCopy = this.SelectedLabel.Copy();
                itemCopy.Name = copyName;

                //Perform the save.
                ShowWaitCursor(true);

                try
                {
                    //Make sure the entityId is set
                    itemCopy.EntityId = CCMClient.ViewState.EntityId;

                    this.SelectedLabel = itemCopy.SaveAs();
                }
                catch (Exception ex)
                {
                    base.ShowWaitCursor(false);
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveToFile

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(p => SaveAsToFile_Executed(p), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveToFile
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private void SaveAsToFile_Executed(Object args)
        {
            Model.Label itemToSave = this.SelectedLabel;

            if (itemToSave != null)
            {
                //get the file path to save to.
                String filePath = args as String;
                if (String.IsNullOrEmpty(filePath)) filePath = LabelUIHelper.ShowSaveAsFileDialog();
                if (String.IsNullOrEmpty(filePath)) return;


                //save
                base.ShowWaitCursor(true);
                try
                {
                    itemToSave = itemToSave.SaveAsFile(filePath);

                    //unlock the file immediately.
                    itemToSave.Dispose();
                }
                catch (Exception ex)
                {
                    base.ShowWaitCursor(false);
                    RecordException(ex);
                    GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    return;
                }
                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits the selection
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    RegisterCommand(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (this.SelectedLabel != null
                && this.SelectedLabel.IsDirty)
            {
                this.OKCommand.DisabledReason = Message.PrintTemplateLabelSelector_OKDisabledNotSaved;
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            //update the component detail id reference.
            if (this.LabelType == Model.LabelType.Product)
            {
                this.ComponentDetail.PlanogramProductLabelId =
                    (this.SelectedLabel != null) ? this.SelectedLabel.Id : null;
            }
            else if (this.LabelType == Model.LabelType.Fixture)
            {
                this.ComponentDetail.PlanogramFixtureLabelId =
                    (this.SelectedLabel != null) ? this.SelectedLabel.Id : null;
            }

            //close the window.
            CloseWindow();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    RegisterCommand(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// PARM: The field property info.
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.LabelSetupContent_SetFieldCommand_Desc
                    };
                    RegisterCommand(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private void SetField_Executed()
        {
            LabelUIHelper.SetFieldText(this.SelectedLabel);
        }

        #endregion

        #region ClearSelectedLabelCommand

        private RelayCommand _clearSelectedLabelCommand;

        /// <summary>
        /// Commits the selection
        /// </summary>
        public RelayCommand ClearSelectedLabelCommand
        {
            get
            {
                if (_clearSelectedLabelCommand == null)
                {
                    _clearSelectedLabelCommand = new RelayCommand(
                        p => ClearSelectedLabel_Executed(),
                        p => ClearSelectedLabel_CanExecute())
                    {
                        FriendlyName = Message.PrintTemplateLabelSelector_ClearSelectedLabel,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_clearSelectedLabelCommand);
                }
                return _clearSelectedLabelCommand;
            }
        }

        private Boolean ClearSelectedLabel_CanExecute()
        {
            if (this.SelectedLabel == null)
            {
                return false;
            }

            return true;
        }

        private void ClearSelectedLabel_Executed()
        {
            this.SelectedLabel = null;
        }

        #endregion

        #endregion

        #region Event Handler

        /// <summary>
        /// Responds to a change of selected setting.
        /// </summary>
        private void OnSelectedLabelChanged(Model.Label oldValue, Model.Label newValue)
        {
            OnPropertyChanged(DisplayTextProperty);

            //Set the full name of the label.
            String labelFullName = Message.PrintTemplateLabelSelector_NoLabelName;
            if (newValue != null)
            {
                if (newValue.Id is String)
                {
                    //display the file path.
                    labelFullName = newValue.Id.ToString();
                }
                else
                {
                    //repository label so just display its name.
                    labelFullName = newValue.Name;
                }

            }
            this.SelectedLabelFullName = labelFullName;

        }

        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                DisposeBase();
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
