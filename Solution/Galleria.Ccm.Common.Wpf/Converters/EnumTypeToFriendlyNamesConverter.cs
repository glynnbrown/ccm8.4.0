﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-27426 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Returns the friendly names dictionary for the given enum type.
    /// </summary>
    public sealed class EnumTypeToFriendlyNamesConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is Type)) return null;

            Type enumType = value as Type;

            if (!enumType.IsEnum) return null;

            return CommonHelper.GetEnumFriendlyNames(enumType);

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
