﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-28066 : A.Kuszyk
//  Fixed ConvertBack return value for IValueConverter.
// V8-28270 : L.Ineson
//  Added ability to process a secondary converter from the uom definition.
// V8-28605 : A.Silva
//      Amended ConvertBack to fix an issue when returning values as String (it would return the type, not the value).
#endregion

#region Version History: (CCM 8.1.0)
// V8-29028 : L.Ineson
//  Numbers now get rounded before display.
#endregion

#region Version History: (CCM 8.2.0)
// V8-29055 : I.George
//  Now returns null for empty strings with nullable types
// V8-29251 : L.Ineson
//  Added check to stop valtype use throwing null exception.
// V8-30980 : L.Ineson
//  Added ForceDecimalDisplay
// V8-30973 : M.Shelley
//  Set Percentage converter decimal places to 2 DP
// V8-31390 : D.Pleasance
//  Amended ConvertBack so that its culture aware for NumberDecimalSeparator.
#endregion


#endregion

using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Converter used to tag the display uom abbreviation onto a text string.
    /// </summary>
    public sealed class UnitDisplayConverter : IValueConverter, IMultiValueConverter
    {
        /// <summary>
        /// Default precision for displaying numbers, it is shorter than the default precision for calculations.
        /// </summary>
        private const Byte DefaultPrecision = 2;

        #region Methods

        private static Object Convert(Object fieldValue, DisplayUnitOfMeasure uom, CultureInfo culture)
        {
            //don't apply the abbreviation 
            //if the value is blank.
            if (fieldValue == null || fieldValue as String == String.Empty)
            {
                return null;
            }

            //if we have an additional fieldvalue converter set then process that
            Object value = fieldValue;
            if (uom != null && uom.AdditionalValueConverter != null)
            {
                String decimalPlaces = null;
                try
                {
                    // V8-30973 : Set Percentage converter decimal places to 2 DP
                    if (uom.AdditionalValueConverter is ToPercentageConverter)
                    {
                        decimalPlaces = "2";
                    }

                    value = uom.AdditionalValueConverter.Convert(value, typeof(String), decimalPlaces, culture);
                }
                catch (Exception) { }
            }

            //if the value is numerical then round the number to sort out
            // precision issues.
            if (value != null && (value is Single || value is Single?))
            {
                value = Math.Round(System.Convert.ToDecimal(value), DefaultPrecision, MidpointRounding.AwayFromZero);
            }
            

            if (uom != null)
            {
                //force dp display if required.
                if (uom.ForceDecimalDisplay)
                {
                    value = String.Format(culture, "{0:F2}", value);
                }

                if (uom.IsPrefix)
                {
                    return String.Format(culture, "{0}{1}", uom.Abbreviation, value);
                }
                else
                {
                    return String.Format(culture, "{0} {1}", value, uom.Abbreviation);
                }
            }
            else
            {
                return System.Convert.ToString(value, culture);
            }
        }

        private static Object ConvertBack(String textString, Type valType, CultureInfo culture, DisplayUnitOfMeasure displayUom, Boolean isSingle)
        {
            // strip anything that isnt a valid decimal character
            var doubleArray = Regex.Split(textString, String.Format(@"[^0-9\{0}]+", culture.NumberFormat.NumberDecimalSeparator)).Where(c => c != culture.NumberFormat.NumberDecimalSeparator && c.Trim() != "");
            if (doubleArray.Any())
            {
                Object returnValue = null;

                String valString = doubleArray.ElementAt(0);
                if (textString.StartsWith("-"))
                {
                    valString = valString.Insert(0, "-");
                }

                if (!String.IsNullOrEmpty(valString))
                {
                    if (valType.IsGenericType && valType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        valType = Nullable.GetUnderlyingType(valType);
                    }

                    try
                    {
                        if (valType == typeof(Single) || valType == typeof(Single?))
                        {
                            Single val;
                            if (Single.TryParse(valString, NumberStyles.Float, culture, out val))
                            {
                                returnValue = val;
                            }
                            else if (valType == typeof(Single?))
                            {
                                returnValue = null;
                            }
                            else
                            {
                                returnValue = 0;
                            }
                        }
                        else if (valType == typeof(String))
                        {
                            returnValue = valString;
                        }
                        else
                        {
                            returnValue = System.Convert.ChangeType(valString, valType, culture);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }

                //if we have an additional fieldvalue converter set then process that
                if (displayUom != null && displayUom.AdditionalValueConverter != null)
                {
                    try
                    {
                        returnValue = displayUom.AdditionalValueConverter.ConvertBack(returnValue, valType, null, culture);
                    }
                    catch (Exception) { }
                    
                }

                return returnValue;
            }
            else
            {
                // If the input value is null and the property allows nulls then
                // just return it
                if (valType != null)
                {
                    if (String.IsNullOrEmpty(textString) && valType.IsGenericType
                        && valType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return null;
                    }
                }

                //otherwise don't process
                if (isSingle)
                {
                    return Binding.DoNothing;
                }
                else
                {
                    return new Object[] { Binding.DoNothing, Binding.DoNothing };
                }
            }
        }

        #endregion

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, parameter as DisplayUnitOfMeasure, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ConvertBack(System.Convert.ToString(value, culture), targetType, culture, parameter as DisplayUnitOfMeasure, true);
        }

        #endregion

        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Count() != 2) throw new ArgumentException();


            Object fieldValue = values[0];
            if (fieldValue == DependencyProperty.UnsetValue)
            {
                return Binding.DoNothing;
            }


            DisplayUnitOfMeasure uom = null;
            if (values[1] is DisplayUnitOfMeasure)
            {
                uom = (DisplayUnitOfMeasure)values[1];
            }


            return Convert(fieldValue, uom, culture);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            Object[] returnValues = new Object[2];
            returnValues[0] = ConvertBack(System.Convert.ToString(value, culture), targetTypes[0], culture, parameter as DisplayUnitOfMeasure, false);
            returnValues[1] = Binding.DoNothing;
            return returnValues;
        }

        #endregion

    }
}
