﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson
//  Created
// V8-26530 : L.Ineson
//  Corrected rounding for degative degrees.
// V8-26495 : L.Ineson
// Copied to common.
// V8-27518 : A.Probyn
//  Added defensive code to converter when converter to handle '-' input.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    public sealed class RadiansToDegreesConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Double radians = 0;
            Double.TryParse(System.Convert.ToString(value), out radians);
            Double degrees = ToDegrees(radians);

            if (targetType == typeof(Single))
            {
                return System.Convert.ToSingle(degrees);
            }
            else
            {
                return degrees;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {            
            Double degrees = 0;
            Double.TryParse(System.Convert.ToString(value), out degrees);
            Double radians = ToRadians(degrees);

            if (targetType == typeof(Single))
            {
                return System.Convert.ToSingle(radians);
            }
            else
            {
                return radians;
            }
        }

        #endregion

        #region Helpers

        public static Double ToDegrees(Double radians)
        {
            Double degrees = radians * (180f / Math.PI);

            if (degrees > 360 || degrees < -360)
            {
                degrees = degrees % 360;
            }

            if (degrees < 0)
            {
                degrees = 360 + degrees;
            }

            return Math.Round(degrees);
        }

        public static Single ToDegrees(Single radians)
        {
            Double degrees = radians * (180f / Math.PI);

            if (degrees > 360 || degrees < -360)
            {
                degrees = degrees % 360;
            }

            if (degrees < 0)
            {
                degrees = 360 + degrees;
            }

            return System.Convert.ToSingle(Math.Round(degrees));
        }


        public static Double ToRadians(Double degrees)
        {
            Double d = degrees;

            if (d > 360 || degrees < -360)
            {
                d = d % 360;
            }

            if (d < 0)
            {
                d = 360 + d;
            }

            return d * (Math.PI / 180f);
        }

        public static Single ToRadians(Single degrees)
        {
            Single d = degrees;

            if (d > 360 || degrees < -360)
            {
                d = d % 360;
            }

            if (d < 0)
            {
                d = 360 + d;
            }

            return System.Convert.ToSingle(d * (Math.PI / 180f));
        }


        #endregion
    }
}
