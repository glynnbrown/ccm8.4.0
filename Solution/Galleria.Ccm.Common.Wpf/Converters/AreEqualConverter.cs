﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V9-25462 : A.Silva ~ Copied over from Editor.Client

#endregion

#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    ///     Returns true if the bound Objects are equal.
    /// </summary>
    public sealed class AreEqualConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public Object Convert(Object[] values, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!values.Any()) return true;
            var val1 = values[0];
            return values.All(val => Equals(val1, val));
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
