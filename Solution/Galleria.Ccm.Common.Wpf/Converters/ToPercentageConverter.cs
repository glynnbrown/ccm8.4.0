﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.1)
// V8:29434 : L.Luong
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows.Data;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Temp Converter until framework converter is fixed, should be removed once it has been fixed
    /// </summary>
    public sealed class ToPercentageConverter : IValueConverter
    {
        /// <summary>
        /// Called when converting a value
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="targetType">The values target type</param>
        /// <param name="parameter">A conversion parameter: if specified, represents the number of decimal places to
        /// round the final percentage to.</param>
        /// <param name="culture">The conversion culture</param>
        /// <returns>The converted value</returns>
        public Object Convert(object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                Int16 decimalPlaces = 0;
                Int16.TryParse(parameter as String, out decimalPlaces);
                Single actualValue = System.Convert.ToSingle(value, CultureInfo.InvariantCulture);
                return (Single)Math.Round((double)actualValue * 100, decimalPlaces, MidpointRounding.AwayFromZero);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Converts a value back
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="targetType">The value target type</param>
        /// <param name="parameter">A conversion parameter</param>
        /// <param name="culture">The conversion culture</param>
        /// <returns>The converted value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value is String)
            {
                String valueString = (String)value;

                if (!String.IsNullOrEmpty(valueString))
                {
                    //ISO-12955 - check for letters
                    char[] charValues = valueString.ToCharArray();
                    bool isCorrect = true;
                    foreach (Char c in charValues)
                    {
                        if (!Char.IsNumber(c))
                        {
                            isCorrect = false;
                            break;
                        }
                    }

                    if (isCorrect)
                    {
                        float actualValue = Single.Parse(valueString, CultureInfo.InvariantCulture);
                        return actualValue / 100;
                    }
                }

                //value could not be converted - return null or 0 depending on the target type
                Boolean returnNull = false;
                if (!targetType.IsValueType) { returnNull = true; }
                else if (Nullable.GetUnderlyingType(targetType) != null) { returnNull = true; }

                if (returnNull) { return null; }
                else { return 0; }
            }

            if (value is decimal || value is float || value is int)
            {
                decimal actualValue = System.Convert.ToDecimal(value, CultureInfo.InvariantCulture);
                return actualValue / 100M;
            }

            return null;
        }
    }
}
