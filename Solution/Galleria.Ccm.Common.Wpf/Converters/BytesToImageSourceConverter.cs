﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Converter to create an image source from a byte array.
    /// </summary>
    public sealed class BytesToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ImageSource src = null;

            Double maxWidth = 0;
            if (parameter != null)
            {
                if (parameter is String)
                {
                    Double.TryParse((String)parameter, out maxWidth);
                }
                else if (parameter is Double)
                {
                    maxWidth = (Double)parameter;
                }
            }

            if (value is Byte[])
            {
                BitmapImage fullImg = CommonHelper.GetBitmapImage(new MemoryStream((Byte[])value));
                src = fullImg;

                if (maxWidth > 0)
                {
                    var scale = maxWidth / fullImg.PixelWidth;
                    ScaleTransform transform = new ScaleTransform(scale, scale);
                    TransformedBitmap thumb = new TransformedBitmap(fullImg, transform);
                    WriteableBitmap newBitmap = new WriteableBitmap(thumb);
                    newBitmap.Freeze();
                    src = newBitmap;
                }
            }


            return src;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
