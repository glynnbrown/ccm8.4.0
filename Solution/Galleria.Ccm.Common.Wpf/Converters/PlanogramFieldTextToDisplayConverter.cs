﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// L.Ineson ~ Created.
#endregion
#endregion

using System;
using System.Windows.Data;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Converters the given field text to a display friendly version
    /// </summary>
    public sealed class PlanogramFieldTextToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(value as String, PlanogramFieldHelper.EnumerateAllFields(null));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value as String, PlanogramFieldHelper.EnumerateAllFields(null));
        }
    }
}
