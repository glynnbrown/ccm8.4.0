﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.1.1)
// CCM-30455 : D.Pleasance
//	Created
#endregion

#endregion

using System;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections;

namespace Galleria.Ccm.Common.Wpf.Converters
{
    /// <summary>
    /// Returns the friendly name for the given enum value.
    /// </summary>
    public sealed class EnumValueToFriendlyNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;

            Type enumType = value.GetType();
            if (!enumType.IsEnum) return null;

            IDictionary source = CommonHelper.GetEnumFriendlyNames(enumType);

            if (source != null && value != null)
            {
                return source[value];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
