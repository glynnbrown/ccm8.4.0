﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson 
//  Created.
// V8-26170 : A.Silva 
//  Ported from Editor.Client.Wpf.
// V8-26690 : L.Ineson
//  Corrected build version to get from entry assembly.
// V8-27528 : M.Shelley 
//  Added licensing details to the Help
// V8-28265 : L.ineson
//  Added link to help file path from viewstate. Added contact us window.
// V8-27996 : N.Haywood
//  Added database info
#endregion

#region Version History: (CCM 8.1.1)
// V8-30078 : I.George
//  Added ViewState_EntityIdChanged event
#endregion

#endregion

using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Dal;
using System.Configuration;
using Galleria.Ccm.Model;
using System.Linq;
using System.Linq.Expressions;


namespace Galleria.Ccm.Common.Wpf.Help
{
    public sealed class BackstageHelpViewModel : ViewModelAttachedControlObject<BackstageHelp>
    {
        #region enums
        public enum DbConnectionType
        {
            None = 0,
            Sql = 1,
        }
        #endregion

        #region Fields

        private String _buildVersion = String.Empty;
        private String _keyword = String.Empty;
        private LicenseState _state;
        private DbConnectionType _connectionType;
        private String _databaseName = "";
        private String _serverName = "";

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath BuildVersionProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.BuildVersion);
        public static readonly PropertyPath StateProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.State);
        public static readonly PropertyPath ServerNameProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.ServerName);
        public static readonly PropertyPath DatabaseNameProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.DatabaseName);
        public static readonly PropertyPath ConnectionTypeProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.ConnectionType);


        //Commands
        public static readonly PropertyPath OnlineHelpCommandProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.OnlineHelpCommand);
        public static readonly PropertyPath ContactUsCommandProperty = WpfHelper.GetPropertyPath<BackstageHelpViewModel>(p => p.ContactUsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The version of the current build
        /// </summary>
        public String BuildVersion
        {
            get { return _buildVersion; }
            private set
            {
                _buildVersion = value;
                OnPropertyChanged(BuildVersionProperty);
            }
        }

        /// <summary>
        /// Holds details of the product licence
        /// </summary>
        public LicenseState State
        {
            get { return _state; }
        }

        public DbConnectionType ConnectionType
        {
            get
            {
                return _connectionType;
            }
            private set
            {
                _connectionType = value;
                OnPropertyChanged(ConnectionTypeProperty);
            }
        }

        #region ServerName

        /// <summary>
        ///     Gets the selected entity.
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            private set
            {
                _serverName = value;
                OnPropertyChanged(ServerNameProperty);
            }
        }
        #endregion

        #region DatabaseName

        /// <summary>
        ///     Gets the selected entity.
        /// </summary>
        public String DatabaseName
        {
            get
            {
                return _databaseName;
            }
            private set
            {
                _databaseName = value;
                OnPropertyChanged(DatabaseNameProperty);
            }

        }

        #endregion

        #endregion

        #region Constructor

        public BackstageHelpViewModel()
            : this(Application.Current.Properties["LicenseState"])
        {
        }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public BackstageHelpViewModel(Object stateObject)
        {
            //Get Version Information
            BuildVersion = Assembly.GetEntryAssembly().GetName().Version.ToString();

            // Get the LicenseState Application reference
            var licenceState = stateObject as LicenseState;
            if (licenceState != null)
            {
                _state = licenceState;
            }

            Refresh();


            CCMClient.ViewState.EntityIdChanged += ViewState_EntityIdChanged;
        }



        #endregion

        #region Commands

        #region OnlineHelpCommand

        private RelayCommand _onlineHelpCommand;

        public RelayCommand OnlineHelpCommand
        {
            get
            {
                if (_onlineHelpCommand == null)
                {
                    _onlineHelpCommand = new RelayCommand(p => OnlineHelp_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_OnlineHelp_Button,
                        Icon = ImageResources.BackstageHelp_Help_32
                    };
                    ViewModelCommands.Add(_onlineHelpCommand);
                }
                return _onlineHelpCommand;
            }
        }

        private void OnlineHelp_Executed()
        {

            if (this.AttachedControl != null)
            {
                String fileToOpen = CCMClient.ViewState.HelpFilePath;

                if (System.IO.File.Exists(fileToOpen))
                {
                    Process.Start(fileToOpen);
                }
                else
                {
                    #region Inform User
                    var msg = new Galleria.Framework.Controls.Wpf.ModalMessage();
                    msg.Title = System.Windows.Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                    msg.Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.BackstageHelp_CannotFindFile_Header;
                    msg.Description = String.Format(Galleria.Ccm.Common.Wpf.Resources.Language.Message.BackstageHelp_CannotFindHelpFile_Message, fileToOpen);
                    msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    msg.ButtonCount = 1;
                    msg.Button1Content = Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_Ok;
                    msg.DefaultButton = Galleria.Framework.Controls.Wpf.ModalMessageButton.Button1;
                    msg.MessageIcon = ImageResources.Warning_32;
                    msg.ShowDialog();
                    #endregion
                }

            }

        }

        #endregion

        #region ContactUsCommand

        private RelayCommand _contactUsCommand;

        public RelayCommand ContactUsCommand
        {
            get
            {
                if (_contactUsCommand == null)
                {
                    _contactUsCommand = new RelayCommand(p => ContactUs_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_ContactUs_Button,
                        Icon = ImageResources.BackstageHelp_ContactUs_32
                    };
                    ViewModelCommands.Add(_contactUsCommand);
                }
                return _contactUsCommand;
            }
        }

        private void ContactUs_Executed()
        {
            if (this.AttachedControl != null)
            {
                WindowHelper.ShowWindow(new ContactUsWindow(), true);
            }
        }

        #endregion

        #region OpenLicenseScreenCommand

        private RelayCommand _openLicenseScreenCommand;

        /// <summary>
        /// Shows the licensing screen
        /// </summary>
        public RelayCommand OpenLicenseScreenCommand
        {
            get
            {
                if (_openLicenseScreenCommand == null)
                {
                    _openLicenseScreenCommand = new RelayCommand(
                        p => OpenLicenseScreen_Executed())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openLicenseScreenCommand);
                }
                return _openLicenseScreenCommand;
            }
        }

        private void OpenLicenseScreen_Executed()
        {
            // Create and populate license
            SecurityDialogViewModel.OpenLicenceScreen();
        }

        #endregion

        #endregion

        #region Event Handlers

        private void ViewState_EntityIdChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        #endregion

        #region Methods

        private void Refresh()
        {
            //db info
            UserSystemSetting _currentSettings = UserSystemSetting.FetchUserSettings();
            Connection currentConnection =
                _currentSettings.Connections.FirstOrDefault(connection => connection.IsCurrentConnection) ??
                _currentSettings.Connections.FirstOrDefault(connection => connection.IsAutoConnect) ??
                null;

            this.ConnectionType = (currentConnection != null) ? DbConnectionType.Sql : DbConnectionType.None;

            this.ServerName = (currentConnection != null) ? currentConnection.ServerName : "";
            this.DatabaseName = (currentConnection != null) ? currentConnection.DatabaseName : Message.BackstageHelp_NotConnectedToDatabase;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                CCMClient.ViewState.EntityIdChanged -= ViewState_EntityIdChanged;
            }

            IsDisposed = true;
        }

        #endregion
    }
}
