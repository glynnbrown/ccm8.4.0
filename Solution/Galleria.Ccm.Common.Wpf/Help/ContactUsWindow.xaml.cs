﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-28265 : L.ineson
//  Copied from GFS
#endregion

#endregion


using System;
using System.Diagnostics;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Help
{
    /// <summary>
    /// Interaction logic for ContactUsWindow.xaml
    /// </summary>
    public partial class ContactUsWindow : ExtendedRibbonWindow
    {
        #region Constructor
        public ContactUsWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Navigate to the webpage described by the xaml link
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleRequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            if (e.Uri != null)
            {
                //get the webpage from the uri
                String url = e.Uri.AbsoluteUri;
                //Launch default browser
                Process.Start(new ProcessStartInfo(url));
            }
            e.Handled = true;
        }
        #endregion

        #region Window Close
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                }), priority: DispatcherPriority.Background);

        }
        #endregion

    }
}
