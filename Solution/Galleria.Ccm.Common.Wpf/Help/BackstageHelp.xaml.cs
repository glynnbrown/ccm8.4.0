﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Common.Wpf.Help
{
    /// <summary>
    /// Interaction logic for BackstageHelp.xaml
    /// </summary>
    public sealed partial class BackstageHelp
    {
        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageHelpViewModel), typeof(BackstageHelp),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BackstageHelpViewModel ViewModel
        {
            get { return (BackstageHelpViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageHelp senderControl = (BackstageHelp)obj;

            if (e.OldValue != null)
            {
                BackstageHelpViewModel oldModel = (BackstageHelpViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackstageHelpViewModel newModel = (BackstageHelpViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion



        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstageHelp()
        {
            //dont initialize until we become visible.
            this.IsVisibleChanged += BackstageHelp_IsVisibleChanged;
        }

        /// <summary>
        /// Called when this first becomes visible.
        /// </summary>
        private void BackstageHelp_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                //Unsubscribe
                this.IsVisibleChanged -= BackstageHelp_IsVisibleChanged;

                //Initialize
                InitializeComponent();
                this.ViewModel = new BackstageHelpViewModel();
            }
        }


    }
}
