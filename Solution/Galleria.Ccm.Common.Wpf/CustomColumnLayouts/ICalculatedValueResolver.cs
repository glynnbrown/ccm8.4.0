﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31832 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class CalculatedValueResolver : INotifyPropertyChanged, IDisposable
    {
        #region Fields
        private GetCalculatedValue _getCalculatedValue;
        private Boolean _isCachingEnabled;
        private Object _cacheLock = new Object();
        private readonly Dictionary<Int32, Object> _resolvedValuesCache = new Dictionary<Int32, Object>();
        #endregion

        #region Properties

        /// <summary>
        /// Property to bind to to retrieve a calculated value
        /// by its registered key.
        /// </summary>
        public Object this[Int32 expressionKey]
        {
            get{ return GetValue(expressionKey);}
        }

        /// <summary>
        /// Gets/Sets whether values should be cached.
        /// </summary>
        public Boolean IsCachingEnabled
        {
            get { return _isCachingEnabled; }
            private set { _isCachingEnabled = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="getCalculatedValue">The method to invoke to resolve the calculated value</param>
        /// <param name="isCachingEnabled">If true, resolved values will be cached.</param>
        public CalculatedValueResolver(GetCalculatedValue getCalculatedValue, Boolean isCachingEnabled = false)
        {
            _getCalculatedValue = getCalculatedValue;
            this.IsCachingEnabled = isCachingEnabled;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delegate to define the method used to return the calculated value.
        /// </summary>
        public delegate Object GetCalculatedValue(String expressionText);

        /// <summary>
        /// Invokes the get calculated value delegate to return the calculated value
        /// for the given key.
        /// </summary>
        /// <param name="key">the expression text registered key</param>
        /// <returns>the resolved value</returns>
        private Object GetValue(Int32 key)
        {
            if (_getCalculatedValue == null || _isDisposed) return null;

            //if we are caching then check if a value is already available.
            if (IsCachingEnabled)
            {
                Object cachedValue;
                if (_resolvedValuesCache.TryGetValue(key, out cachedValue))
                {
                    return cachedValue;
                }
            }

            String expressionText = ColumnLayoutManager.GetExpressionTextByKey(key);
            if (expressionText == null) return null;

            Object value = _getCalculatedValue(expressionText);

            if (IsCachingEnabled)
            {
                lock (_cacheLock)
                {
                    _resolvedValuesCache[key] = value;
                }
            }

            return value;
        }

        /// <summary>
        /// Clears the cache if active, 
        /// and fire a property change notification
        /// so that values are refreshed.
        /// </summary>
        public void RefreshValues()
        {
            if (IsCachingEnabled)
            {
                lock (_cacheLock)
                {
                    _resolvedValuesCache.Clear();
                }
            }
            
            //fire a blank property change notification.
            OnPropertyChanged("");
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if(PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;
            _isDisposed = true;

            _resolvedValuesCache.Clear();
            _getCalculatedValue = null;
        }

        ~CalculatedValueResolver()
        {
            Dispose(false);
        }

        #endregion

    }
}
