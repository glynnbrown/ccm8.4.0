﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// V8-27442 : L.Ineson
// Split out and updated.

#endregion

#region Version History: (CCM 8.03)

// V8-29341 : J.Pickup
// ComponentSequenceNumberProperty is now defaulted for this cc type. 

#endregion

#region Version History: (CCM 8.2)
// V8-31428 : L.Ineson
//  Have made sure that subcomponent generic properties are not made available.
#endregion
#region Version History: (CCM 8.3)
// V8-31594 : L.Ineson
//  Excluded x,y,z from subcomponent properties too.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///  Column layout factory used by the fixture list view when displaying
    ///  PlanogramComponentView rows.
    /// </summary>
    public sealed class PlanogramComponentColumnLayoutFactory : BaseColumnLayoutFactory
    {
        const String fixturePrefix = "Fixture.";
        const String assemblyPrefix = "Assembly.";
        const String subcomponentsPrefix = "MergedSubComponentView.";

        #region Constructors

        public PlanogramComponentColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            :base(rowSourceType, CustomColumnLayoutType.PlanogramComponent, planogramContext)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //Add a list of properties visible by default.
            base.RegisterVisibleFields(
                new List<String>
            {
                PlanogramComponent.NameProperty.Name,
                PlanogramComponent.ComponentTypeProperty.Name,
                PlanogramFixtureComponent.ComponentSequenceNumberProperty.Name,
                PlanogramComponent.HeightProperty.Name,
                PlanogramComponent.WidthProperty.Name,
                PlanogramComponent.DepthProperty.Name,
                PlanogramFixtureComponent.MetaWorldXProperty.Name,
                PlanogramFixtureComponent.MetaWorldYProperty.Name,
                PlanogramFixtureComponent.MetaWorldZProperty.Name,
                fixturePrefix + PlanogramFixture.NameProperty.Name,
            });
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns all field infos available for this layout.
        /// </summary>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            //Subcomponent fields
            String groupName = PlanogramSubComponent.FriendlyName;
            foreach (var field in PlanogramSubComponent.EnumerateDisplayableFieldInfos())
            {
                //don't include the duplicated generic properties as these will either be the same
                // or blank.
                if (field.PropertyName == PlanogramSubComponent.NameProperty.Name
                    || field.PropertyName == PlanogramSubComponent.HeightProperty.Name
                    || field.PropertyName == PlanogramSubComponent.WidthProperty.Name
                    || field.PropertyName == PlanogramSubComponent.DepthProperty.Name
                    || field.PropertyName == PlanogramSubComponent.SlopeProperty.Name
                    || field.PropertyName == PlanogramSubComponent.RollProperty.Name
                    || field.PropertyName == PlanogramSubComponent.AngleProperty.Name
                    || field.PropertyName == PlanogramSubComponent.XProperty.Name
                    || field.PropertyName == PlanogramSubComponent.YProperty.Name
                    || field.PropertyName == PlanogramSubComponent.ZProperty.Name)
                    continue;


                field.PropertyName = subcomponentsPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Component fields
            groupName = PlanogramComponent.FriendlyName;
            foreach (var field in PlanogramComponent.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.GroupName = groupName;
                yield return field;
            }

            foreach (var field in PlanogramFixtureComponent.EnumerateDisplayableFieldInfos())
            {
                field.GroupName = groupName;
                yield return field;
            }

            //Assembly
            groupName = PlanogramAssembly.FriendlyName;
            foreach (var field in PlanogramAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.PropertyName = assemblyPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixtureAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.PropertyName = assemblyPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Fixture
            groupName = PlanogramFixture.FriendlyName;
            foreach (var field in PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixture.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

        }

        #endregion
    }
}
