﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30762 : I.George
//  Created
#endregion
#region Version History: CCM830
// V8-32718 : D.Pleasance
//  Amended so that position attributes can also be selected
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class AssortmentCustomColumnLayoutFactory : BaseColumnLayoutFactory
    {
        const String productPrefix = "Product.";

        #region Contructors

        public AssortmentCustomColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        {
        }

        public AssortmentCustomColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            : base(rowSourceType, CustomColumnLayoutType.Assortment, planogramContext)
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(new List<String>
            {
               productPrefix + PlanogramProduct.NameProperty.Name,
               productPrefix + PlanogramProduct.GtinProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.IsRangedProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.RankProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.SegmentationProperty.Name,
               productPrefix +  PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.FacingsProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.UnitsProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.ProductTreatmentTypeProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.ProductLocalizationTypeProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.IsPrimaryRegionalProductProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.CommentsProperty.Name,
               productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.MetaIsProductPlacedOnPlanogramProperty.Name,
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            List<ObjectFieldInfo> fieldInfoList = new List<ObjectFieldInfo>();

            Planogram plan = base.GetPlanogramContext();
            PlanogramPerformance performance = (plan != null) ? plan.Performance : null;

            //ProductFields
            String groupName = PlanogramProduct.FriendlyName;
            foreach (var field in PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true, performance, true))
            {
                field.PropertyName = productPrefix + field.PropertyName;
                field.GroupName = groupName;
                fieldInfoList.Add(field);
            }

            //Position fields
            groupName = PlanogramPosition.FriendlyName;
            foreach (var field in PlanogramPosition.EnumerateDisplayableFieldInfos(/*appliedMerch*/false))
            {
                field.GroupName = groupName;
                fieldInfoList.Add(field);
            }

            return fieldInfoList;
        }

        public override bool IsPropertyReadOnly(Galleria.Framework.ViewModel.ObjectFieldInfo fieldInfo, System.Type sourceRowType)
        {
            //don't allow edit of the gtin or name, but everthing else is ok.
            if (fieldInfo.PropertyName == PlanogramProduct.NameProperty.Name
                || fieldInfo.PropertyName == PlanogramProduct.GtinProperty.Name
                || fieldInfo.PropertyName.StartsWith("Meta")
                || fieldInfo.PropertyName.Contains(".Meta"))
            {
                return true;
            }
                
            return base.IsPropertyReadOnly(fieldInfo, sourceRowType);
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }
}
