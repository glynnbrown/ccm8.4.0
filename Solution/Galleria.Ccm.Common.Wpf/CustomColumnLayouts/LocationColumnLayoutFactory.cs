﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for <see cref="Location"/>.
    /// </summary>
    public sealed class LocationColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constructors

        /// <summary>
        ///     Instantiates a new <see cref="ProdColumnLayoutFactory"/>.
        /// </summary>
        public LocationColumnLayoutFactory(Type rowSourceType)
            :base(rowSourceType, CustomColumnLayoutType.Location)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(new List<String>
            {
                Location.CodeProperty.Name,
                Location.NameProperty.Name,
                Location.Address1Property.Name,
                Location.RegionProperty.Name,
                Location.CityProperty.Name,
                Location.PostalCodeProperty.Name,
                //Add any new properties that should be visible on load here.
            });
        }


        #endregion


        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            foreach (ObjectFieldInfo field in Location.EnumerateDisplayableFieldInfos())
            {
                field.GroupName = Ccm.Resources.Language.Message.CustomColumnsLocation_General;
                yield return field;
            }
        }

        #endregion
    }
}