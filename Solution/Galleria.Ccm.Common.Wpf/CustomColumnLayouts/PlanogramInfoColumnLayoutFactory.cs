﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26351 : A.Silva 
//  Created.
// V8-26372 : A.Silva 
//  Added missing columns to Planogram type, and created WorkPacakge type definitions.
// V8-26369 : A.Silva 
//  All fields in product list made writable by default.
// V8-26408 : L.Ineson
//  Split out this class into its own file and added PlanogramComponent type.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27022 : A.Silva 
//  Added Default Visible Columns for PlanogramFixture Components.
// V8-27160 : L.Ineson
//  Removed workpackage layout type.
// Split out remaining types into separate factories which contain all methods
// needed for that type.
// V8-27504 : A.Silva
//      Amended uses of CustomColumn.NewCustomColumn() factory method to account for the new HeaderGroupNumber property.
//      Added BaseColumnLayoutFactory<T> and moved the factory sub types to inherit from it.
// V8-27898 : L.Ineson
//  Corrected check methods to take field info not just property path.
//  Updated product column layout factory to use new EnumerateDisplayableFieldInfos method.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-27442 : L.Ineson
// Split out implementations into separate files.
// V8-28093 : L.Ineson
//  Removed Planogram name from this as it is now a fixed column for all planogram grids.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30359 : A.Probyn
//  Added MostRecentLinkedWorkpackageNameProperty to the visible property paths.
#endregion

#region Version History: (CCM 8.2.0)
// V8-31266 : A.Probyn
//  Added new constructor with specificExcludedProperties parameter allowing us to specific other default
//  columns that may be forced onto a grid e.g PlanogramRepositoryOrganiser forces UCR.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for <see cref="PlanogramInfo"/>.
    /// </summary>
    public sealed class PlanogramInfoColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Fields
        private readonly List<IModelPropertyInfo> _excludedProperties = new List<IModelPropertyInfo>();
        #endregion

        #region Properties

        public List<IModelPropertyInfo> ExcludedProperties
        {
            get { return _excludedProperties; }
        }

        #endregion


        #region Constructors

        public PlanogramInfoColumnLayoutFactory()
            : this(typeof(PlanogramInfo), null)
        {
        }

        public PlanogramInfoColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        { }

        public PlanogramInfoColumnLayoutFactory(Type rowSourceType, List<IModelPropertyInfo> specificExcludedProperties)
            : base(rowSourceType, CustomColumnLayoutType.Planogram, null)
            
        {
            //exclude planogram name from this as it should always be displayed.
            _excludedProperties.Add(PlanogramInfo.NameProperty);

            //If any other specific properties need excluding, do so here
            if (specificExcludedProperties != null)
            {
                foreach (IModelPropertyInfo property in specificExcludedProperties)
                {
                    if (!_excludedProperties.Contains(property))
                    {
                        _excludedProperties.Add(property);
                    }
                }
            }


            base.RegisterGroupNames(
              new List<String> { Ccm.Resources.Language.Message.CustomColumnsPlanogram_General });

            base.RegisterVisibleFields(new List<String>
            {
                //PlanogramInfo.NameProperty.Name,
                PlanogramInfo.AutomationProcessingStatusProperty.Name,
                PlanogramInfo.AutomationProgressProperty.Name,
                PlanogramInfo.CategoryCodeProperty.Name,
                PlanogramInfo.CategoryNameProperty.Name,
                PlanogramInfo.PlanogramGroupNameProperty.Name,
                PlanogramInfo.WidthProperty.Name,
                PlanogramInfo.HeightProperty.Name,
                PlanogramInfo.DepthProperty.Name,
                PlanogramInfo.MetaBayCountProperty.Name,
                PlanogramInfo.MetaUniqueProductCountProperty.Name,
                PlanogramInfo.MetaTotalLinearWhiteSpaceProperty.Name,
                PlanogramInfo.DateLastModifiedProperty.Name,
                PlanogramInfo.MostRecentLinkedWorkpackageNameProperty.Name
                //Add any new properties that should be visible on load here.
            });
        }


        #endregion


        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            String ownerFriendlyName = Planogram.FriendlyName;
            Type ownerType = typeof(PlanogramInfo);

            foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePropertyInfos())
            {
                //if property is excluded then ignore it.
                if (_excludedProperties.Contains(propertyInfo)) continue;

                var field = ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, propertyInfo);
                field.GroupName = Ccm.Resources.Language.Message.CustomColumnsPlanogram_General;

                yield return field;
            }
        }

        /// <summary>
        /// Sets the visible field list, there is a default list specified already, 
        /// but this can be used to override it.
        /// </summary>
        public void SetVisibleFields(List<String> visibleFieldList)
        {
            RegisterVisibleFields(visibleFieldList);
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }
}
