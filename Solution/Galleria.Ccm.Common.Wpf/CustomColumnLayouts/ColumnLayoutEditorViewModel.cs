﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24700 : A.Silva ~ Created.
// V8-25921 : A.Silva ~ Recreated.
// V8-25907 : A.Silva ~ Localised.
// V8-26076 : A.Silva ~ Modified how columns are saved, now, all columns are saved, with whether they are visible or not.
// V8-26164 : A.Silva ~ Added constraint to SaveAs_CanExecute so that now at least one visible column is required.
// V8-26165 : A.Silva ~ Corrections to Save, SaveAs and Delete commands to avoid changing the restricted shown type of layout.
// V8-26172 : A.Silva ~ Minor refactorig due to CustomColumnLayoutManager.
// V8-26318 : A.Silva ~ Removed the ribbon.
//                    ~ Added lower button panel and hooked up the commands.
// V8-26329 : A.Silva ~ Corrected SetupSortByProperties so that the direction is updated correctly.
// V8-26351 : A.Silva ~ Some refactoring to tidy up Custom Column Layout classes.
// V8-26281 : A.Silva ~ Some tidying up. Moved some classes to their own files.
// V8-26343 : A.Silva ~ The save as dialog now shows the proposed file name, and the open dialog makes sure the file exists on disk.
// V8-26513 : L.Luong ~ Removed the Open prompt and accepting changes will now close window 
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27174 : A.Silva ~ Amended implementation of Add/Remove(all)columns so that the column order is without gaps.
// V8-27504 : A.Silva
//      Amended ordering of custom columns to account for HeaderGroupNumber before the column's actual number.
//      Amended ExchangeGroupOrder.
//      Implemented reordering of header groups.
// V8-28058 : A.Silva
//      Fixed drag and drop for column groups.
//      Fixed column group selection/expansion, so that a group needs to be selected prior to being expandable/collapsable.
//      Removed obsolete and unused property AvailableColumnLayouts and _columnLayoutInfos.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30402 : N.Haywood
//  Set header group number for newly added columns
// V8-30005 : N.Haywood
//  changed removing assigned columns so it doesn't refresh the whole unassigned grid
// V8-30532 : M.Shelley
//  Prevent an InvalidOperationException thrown by the ModelList class in the framework
#endregion

#region Version History: (CCM 8.2.0)
//V8-30870 : L.Ineson
//  Design change.
#endregion

#region Version History: (CCM 8.3.0)
//V8-31832 : L.Ineson
//  Changes to support calculated columns.
//  Have also moved customcolumnview out to own class.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
// V8-32652 : A.Silva
//  Refactored Add Remove Selected commands to use the common icons.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///  View model context providing the functionality for the <see cref="ColumnLayoutEditorOrganiser" />.
    /// </summary>
    public sealed class ColumnLayoutEditorViewModel : WindowViewModelBase
    {
        #region Nested Classes

        /// <summary>
        /// Represents a group of fields available to the column layout editor.
        /// </summary>
        public sealed class ColumnLayoutEditorFieldGroup
        {
            public String GroupName { get; set; }
            public Boolean IsAllGroup { get; set; }

            public override string ToString()
            {
                return GroupName;
            }
        }

        /// <summary>
        /// Represents a field available to the column layout editor.
        /// </summary>
        public sealed class ColumnLayoutEditorField
        {
            public String GroupName { get; set; }
            public ObjectFieldInfo FieldInfo { get; set; }


            public String Header
            {
                get { return FieldInfo.FieldFriendlyName; }
            }
            public String Path
            {
                get { return FieldInfo.FieldPlaceholder; }
            }


            public override string ToString()
            {
                return FieldInfo.FieldFriendlyName;
            }
        }

        #endregion

        #region Fields

        private Boolean? _dialogResult;
        private readonly String _layoutsFolderPath;
        private readonly IColumnLayoutFactory _layoutFactory;
        private CustomColumnLayout _currentLayout;

        private readonly ReadOnlyCollection<ColumnLayoutEditorField> _masterFields;
        private readonly ReadOnlyCollection<ColumnLayoutEditorFieldGroup> _availableFieldGroups;
        private ColumnLayoutEditorFieldGroup _selectedFieldGroup;
        private readonly BulkObservableCollection<ColumnLayoutEditorField> _selectedGroupFields = new BulkObservableCollection<ColumnLayoutEditorField>();
        private ReadOnlyBulkObservableCollection<ColumnLayoutEditorField> _selectedGroupFieldsRO;
        private readonly ObservableCollection<ColumnLayoutEditorField> _selectedFields = new ObservableCollection<ColumnLayoutEditorField>();

        private readonly BulkObservableCollection<CustomColumnView> _assignedColumnViews = new BulkObservableCollection<CustomColumnView>();
        private ReadOnlyBulkObservableCollection<CustomColumnView> _assignedColumnViewsRO;
        private readonly ObservableCollection<CustomColumnView> _selectedAssignedColumnViews = new ObservableCollection<CustomColumnView>();

        private ObservableCollection<CustomColumnView> _availableFilterByColumns = new ObservableCollection<CustomColumnView>();
        private ObservableCollection<CustomColumnView> _availableGroupSortByColumns = new ObservableCollection<CustomColumnView>();

        private String _groupByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelOne = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;

        private String _sortByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelFourAscending;
        private Boolean _sortByLevelFourDescending = true;
        private String _sortByLevelOne = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelOneAscending;
        private Boolean _sortByLevelOneDescending = true;
        private String _sortByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelThreeAscending;
        private Boolean _sortByLevelThreeDescending = true;
        private String _sortByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelTwoAscending;
        private Boolean _sortByLevelTwoDescending = true;

        private CustomColumnFilter _selectedFiltersSelection;
        private CustomColumnView _selectedNewFilterColumn;
        private String _selectedNewFilterText = String.Empty;

        private Boolean _canUserAddCalculatedColumns = false; //indicates if calculated columns can be created.

        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath AllowEditionProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AllowEdition);
        public static readonly PropertyPath CurrentLayoutProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.CurrentLayout);
        public static readonly PropertyPath AvailableFieldGroupsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath SelectedGroupFieldsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.SelectedGroupFields);
        public static readonly PropertyPath SelectedFieldsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.SelectedFields);
        public static readonly PropertyPath AssignedColumnViewsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.AssignedColumnViews);
        public static readonly PropertyPath SelectedAssignedColumnViewsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(p => p.SelectedAssignedColumnViews);

        public static readonly PropertyPath AddFilterCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AddFilterCommand);
        public static readonly PropertyPath AvailableFilterByColumnsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableFilterByColumns);
        public static readonly PropertyPath RemoveFilterCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.RemoveFilterCommand);
        public static readonly PropertyPath SelectedFiltersProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SelectedFilters);
        public static readonly PropertyPath SelectedFiltersSelectionProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SelectedFiltersSelection);
        public static readonly PropertyPath SelectedNewFilterColumnProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SelectedNewFilterColumn);
        public static readonly PropertyPath SelectedNewFilterTextProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SelectedNewFilterText);

        public static readonly PropertyPath AvailableGroupByColumnsFourProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsFour);
        public static readonly PropertyPath AvailableGroupByColumnsThreeProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsThree);
        public static readonly PropertyPath AvailableGroupByColumnsTwoProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsTwo);
        public static readonly PropertyPath AvailableGroupSortByColumnsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableGroupSortByColumns);
        public static readonly PropertyPath AvailableSortByColumnsFourProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsFour);
        public static readonly PropertyPath AvailableSortByColumnsThreeProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsThree);
        public static readonly PropertyPath AvailableSortByColumnsTwoProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsTwo);

        public static readonly PropertyPath GroupByLevelFourProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.GroupByLevelFour);
        public static readonly PropertyPath GroupByLevelOneProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.GroupByLevelOne);
        public static readonly PropertyPath GroupByLevelThreeProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.GroupByLevelThree);
        public static readonly PropertyPath GroupByLevelTwoProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.GroupByLevelTwo);

        public static readonly PropertyPath SortByLevelFourAscendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelFourAscending);
        public static readonly PropertyPath SortByLevelFourDescendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelFourDescending);
        public static readonly PropertyPath SortByLevelFourProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelFour);
        public static readonly PropertyPath SortByLevelOneAscendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelOneAscending);
        public static readonly PropertyPath SortByLevelOneDescendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelOneDescending);
        public static readonly PropertyPath SortByLevelOneProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelOne);
        public static readonly PropertyPath SortByLevelThreeAscendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelThreeAscending);
        public static readonly PropertyPath SortByLevelThreeDescendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelThreeDescending);
        public static readonly PropertyPath SortByLevelThreeProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelThree);
        public static readonly PropertyPath SortByLevelTwoAscendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelTwoAscending);
        public static readonly PropertyPath SortByLevelTwoDescendingProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelTwoDescending);
        public static readonly PropertyPath SortByLevelTwoProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SortByLevelTwo);
        public static readonly PropertyPath CanUserAddCalculatedColumnsProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.CanUserAddCalculatedColumns);

        //Commands
        public static readonly PropertyPath ApplyCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.ApplyCommand);
        public static readonly PropertyPath AddSelectedColumnsCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AddSelectedColumnsCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.CloseCommand);
        public static readonly PropertyPath MoveColumnDownCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.MoveColumnDownCommand);
        public static readonly PropertyPath MoveColumnUpCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.MoveColumnUpCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.OpenCommand);
        public static readonly PropertyPath RemoveSelectedColumnsCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.RemoveSelectedColumnsCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.SaveAsCommand);
        public static readonly PropertyPath AddCalculatedColumnCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.AddCalculatedColumnCommand);
        public static readonly PropertyPath EditCalculatedColumnCommandProperty = GetPropertyPath<ColumnLayoutEditorViewModel>(o => o.EditCalculatedColumnCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the result and closes the 
        /// attached dialog.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        /// <summary>
        /// Gets the currently loaded custom column layout.
        /// </summary>
        public CustomColumnLayout CurrentLayout
        {
            get { return _currentLayout; }
            private set
            {
                CustomColumnLayout oldValue = _currentLayout;

                _currentLayout = value;
                OnPropertyChanged(CurrentLayoutProperty);

                OnCurrentLayoutChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Gets the list of available item types
        /// </summary>
        public ReadOnlyCollection<ColumnLayoutEditorFieldGroup> AvailableFieldGroups
        {
            get { return _availableFieldGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected item type
        /// </summary>
        public ColumnLayoutEditorFieldGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected item type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ColumnLayoutEditorField> SelectedGroupFields
        {
            get
            {
                if (_selectedGroupFieldsRO == null)
                {
                    _selectedGroupFieldsRO = new ReadOnlyBulkObservableCollection<ColumnLayoutEditorField>(_selectedGroupFields);
                }
                return _selectedGroupFieldsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of the 
        /// </summary>
        public ObservableCollection<ColumnLayoutEditorField> SelectedFields
        {
            get { return _selectedFields; }
        }

        /// <summary>
        /// Returns the collection of views for columns that have been assigned.
        /// </summary>
        public ReadOnlyBulkObservableCollection<CustomColumnView> AssignedColumnViews
        {
            get
            {
                if (_assignedColumnViewsRO == null)
                {
                    _assignedColumnViewsRO = new ReadOnlyBulkObservableCollection<CustomColumnView>(_assignedColumnViews);
                }
                return _assignedColumnViewsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of column views that are selected.
        /// </summary>
        public ObservableCollection<CustomColumnView> SelectedAssignedColumnViews
        {
            get { return _selectedAssignedColumnViews; }
        }

        /// <summary>
        ///     Gets whether the properties are editable or not.
        /// </summary>
        public Boolean AllowEdition
        {
            get
            {
                return this.CurrentLayout != null && this.CurrentLayout.Type != CustomColumnLayoutType.Undefined;
            }
        }

        #region Filtering Properties

        /// <summary>
        ///     Gets/Sets the selected filters
        /// </summary>
        public CustomColumnFilterList SelectedFilters
        {
            get { return this.CurrentLayout.FilterList; }
            set
            {
                //value.ToList().ForEach(CurrentLayout.FilterList.Add);
                foreach (var filter in value) this.CurrentLayout.FilterList.Add(filter);

                OnPropertyChanged(SelectedFiltersProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected filter item
        /// </summary>
        public CustomColumnFilter SelectedFiltersSelection
        {
            get { return _selectedFiltersSelection; }
            set
            {
                _selectedFiltersSelection = value;
                OnPropertyChanged(SelectedFiltersSelectionProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected filter column
        /// </summary>
        public CustomColumnView SelectedNewFilterColumn
        {
            get { return _selectedNewFilterColumn; }
            set
            {
                _selectedNewFilterColumn = value;
                OnPropertyChanged(SelectedNewFilterColumnProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected new filter text
        /// </summary>
        public String SelectedNewFilterText
        {
            get { return _selectedNewFilterText; }
            set
            {
                _selectedNewFilterText = value;
                OnPropertyChanged(SelectedNewFilterTextProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the available filter by columns collection
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableFilterByColumns
        {
            get { return _availableFilterByColumns; }
            set
            {
                _availableFilterByColumns = value;
                OnPropertyChanged(AvailableFilterByColumnsProperty);
            }
        }

        #endregion

        #region Group By Properties

        /// <summary>
        ///     returns the available group by column collection for level four
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableGroupByColumnsFour
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsFourProperty); }
        }

        /// <summary>
        ///     returns the available group by column collection for level three
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableGroupByColumnsThree
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsThreeProperty); }
        }

        /// <summary>
        ///     returns the available group by column collection for level two
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableGroupByColumnsTwo
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsTwoProperty); }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level four
        /// </summary>
        public String GroupByLevelFour
        {
            get { return _groupByLevelFour; }
            set
            {
                _groupByLevelFour = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;

                OnPropertyChanged(GroupByLevelFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level one
        /// </summary>
        public String GroupByLevelOne
        {
            get { return _groupByLevelOne; }
            set
            {
                _groupByLevelOne = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelOneProperty);

                OnPropertyChanged(GroupByLevelOneProperty);
                OnPropertyChanged(AvailableGroupByColumnsTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level three
        /// </summary>
        public String GroupByLevelThree
        {
            get { return _groupByLevelThree; }
            set
            {
                _groupByLevelThree = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelThreeProperty);

                OnPropertyChanged(GroupByLevelThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level two
        /// </summary>
        public String GroupByLevelTwo
        {
            get { return _groupByLevelTwo; }
            set
            {
                _groupByLevelTwo = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelTwoProperty);

                OnPropertyChanged(GroupByLevelTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);
            }
        }

        #endregion

        #region Sort By properties

        /// <summary>
        ///     Gets/Sets the available group & sort by columns collection
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableGroupSortByColumns
        {
            get { return _availableGroupSortByColumns; }
            set
            {
                _availableGroupSortByColumns = value;
                //fire property changed for the all collections
                OnPropertyChanged(AvailableGroupSortByColumnsProperty);
                OnPropertyChanged(AvailableGroupByColumnsTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);
                OnPropertyChanged(AvailableSortByColumnsTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     returns the available sort by column collection for level four
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableSortByColumnsFour
        {
            get { return GetFilteredCollection(AvailableSortByColumnsFourProperty); }
        }

        /// <summary>
        ///     returns the available sort by column collection for level three
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableSortByColumnsThree
        {
            get { return GetFilteredCollection(AvailableSortByColumnsThreeProperty); }
        }

        /// <summary>
        ///     returns the available sort by column collection for level two
        /// </summary>
        public ObservableCollection<CustomColumnView> AvailableSortByColumnsTwo
        {
            get { return GetFilteredCollection(AvailableSortByColumnsTwoProperty); }
        }

        /// <summary>
        ///     Gets/Sets the sort by level four value
        /// </summary>
        public String SortByLevelFour
        {
            get { return _sortByLevelFour; }
            set
            {
                _sortByLevelFour = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                OnPropertyChanged(SortByLevelFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level four is ascending
        /// </summary>
        public Boolean SortByLevelFourAscending
        {
            get { return _sortByLevelFourAscending; }
            set
            {
                _sortByLevelFourAscending = value;
                OnPropertyChanged(SortByLevelFourAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level four is descending
        /// </summary>
        public Boolean SortByLevelFourDescending
        {
            get { return _sortByLevelFourDescending; }
            set
            {
                _sortByLevelFourDescending = value;
                OnPropertyChanged(SortByLevelFourDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level one value
        /// </summary>
        public String SortByLevelOne
        {
            get { return _sortByLevelOne; }
            set
            {
                _sortByLevelOne = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelOneProperty);

                OnPropertyChanged(SortByLevelOneProperty);
                OnPropertyChanged(AvailableSortByColumnsTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level one is ascending
        /// </summary>
        public Boolean SortByLevelOneAscending
        {
            get { return _sortByLevelOneAscending; }
            set
            {
                _sortByLevelOneAscending = value;
                OnPropertyChanged(SortByLevelOneAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level one is descending
        /// </summary>
        public Boolean SortByLevelOneDescending
        {
            get { return _sortByLevelOneDescending; }
            set
            {
                _sortByLevelOneDescending = value;
                OnPropertyChanged(SortByLevelOneDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level three value
        /// </summary>
        public String SortByLevelThree
        {
            get { return _sortByLevelThree; }
            set
            {
                _sortByLevelThree = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelThreeProperty);

                OnPropertyChanged(SortByLevelThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level three is ascending
        /// </summary>
        public Boolean SortByLevelThreeAscending
        {
            get { return _sortByLevelThreeAscending; }
            set
            {
                _sortByLevelThreeAscending = value;
                OnPropertyChanged(SortByLevelThreeAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level three is descending
        /// </summary>
        public Boolean SortByLevelThreeDescending
        {
            get { return _sortByLevelThreeDescending; }
            set
            {
                _sortByLevelThreeDescending = value;
                OnPropertyChanged(SortByLevelThreeDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level two value
        /// </summary>
        public String SortByLevelTwo
        {
            get { return _sortByLevelTwo; }
            set
            {
                _sortByLevelTwo = value ?? Message.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelTwoProperty);

                OnPropertyChanged(SortByLevelTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level two is ascending
        /// </summary>
        public Boolean SortByLevelTwoAscending
        {
            get { return _sortByLevelTwoAscending; }
            set
            {
                _sortByLevelTwoAscending = value;
                OnPropertyChanged(SortByLevelTwoAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level two is descending
        /// </summary>
        public Boolean SortByLevelTwoDescending
        {
            get { return _sortByLevelTwoDescending; }
            set
            {
                _sortByLevelTwoDescending = value;
                OnPropertyChanged(SortByLevelTwoDescendingProperty);
            }
        }

        #endregion

        /// <summary>
        /// Gets/Sets whether the formula button should be displayed to allow the
        /// user to add calculated columns.
        /// This is off by default. If setting to true you must implement ICalculatedColumnResolver
        /// against the row object!
        /// </summary>
        public Boolean CanUserAddCalculatedColumns
        {
            get { return _canUserAddCalculatedColumns; }
            set
            {
                _canUserAddCalculatedColumns = value;
                OnPropertyChanged(CanUserAddCalculatedColumnsProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor
        /// </summary>
        public ColumnLayoutEditorViewModel(CustomColumnLayout layout, IColumnLayoutFactory factory, String layoutsFolderPath)
        {
            //Set folders path
            _layoutsFolderPath = layoutsFolderPath;
            _layoutFactory = factory;

            CustomColumnLayoutType layoutType = factory.LayoutType;

            //Setup master field lookup
            Dictionary<String, ObjectFieldInfo> masterFieldLookup = factory.GetFieldLookupCache();

            //generate the master column views collection
            List<ColumnLayoutEditorField> allColumnFields = new List<ColumnLayoutEditorField>();
            foreach (CustomColumn col in factory.GetFullCustomColumnList())
            {
                ObjectFieldInfo fieldInfo;
                if (masterFieldLookup.TryGetValue(col.Path, out fieldInfo))
                {
                    allColumnFields.Add(
                    new ColumnLayoutEditorField()
                    {
                        FieldInfo = fieldInfo,
                        GroupName = factory.GetHeaderGroupName(fieldInfo),
                    });
                }
            }
            _masterFields = allColumnFields.AsReadOnly();

            //set the field groups
            List<ColumnLayoutEditorFieldGroup> columnGroups = new List<ColumnLayoutEditorFieldGroup>();
            columnGroups.Add(new ColumnLayoutEditorFieldGroup() { GroupName = Message.ColumnLayoutEditor_AllFields, IsAllGroup = true });
            columnGroups.AddRange(_masterFields.GroupBy(g => g.GroupName).Select(c => new ColumnLayoutEditorFieldGroup() { GroupName = c.Key }));
            _availableFieldGroups = columnGroups.AsReadOnly();


            //Set initial column layout
            this.CurrentLayout = (layout != null) ? layout.Clone() :
                CustomColumnLayout.NewCustomColumnLayout(layoutType, factory.NewCustomColumnList(/*default*/true));


            //select the first field group
            this.SelectedFieldGroup = this.AvailableFieldGroups.First();
        }

        #endregion

        #region Commands

        #region Open

        private RelayCommand _openCommand;

        /// <summary>
        ///   Opens the selected <see cref="CustomColumnLayout" /> for edition.
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand != null) return _openCommand;

                _openCommand = new RelayCommand(
                    p => Open_Executed(p))
                {
                    FriendlyName = Message.Generic_Open,
                    FriendlyDescription = Message.ColumnLayoutEditor_OpenColumnLayout_Description,
                    Icon = ImageResources.Open_16,
                    SmallIcon = ImageResources.Open_16
                };
                RegisterCommand(_openCommand);

                return _openCommand;
            }
        }


        private void Open_Executed(Object args)
        {
            //Show the open file dialog
            String fileName = args as String;
            if (String.IsNullOrEmpty(fileName))
            {
                fileName = CustomColumnLayoutHelper.ShowCustomColumnOpenFileDialog(_layoutsFolderPath);
            }
            if (String.IsNullOrEmpty(fileName)) return;


            //load the file.
            base.ShowWaitCursor(true);
            try
            {
                this.CurrentLayout = CustomColumnLayout.FetchByFilename(fileName, /*asReadOnly*/true);

                //unlock the file immediately.
                CustomColumnLayout.UnlockCustomColumnLayoutByFileName(fileName);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAs

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Saves the current <see cref="CustomColumnLayout" /> as a new layout.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                    };
                    RegisterCommand(_saveAsCommand);
                }

                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            if (this.CurrentLayout.Type == CustomColumnLayoutType.Undefined)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_LayoutLacksType;
                return false;
            }
            else if (!this.CurrentLayout.IsValid)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_LayoutIsNotValid;
                return false;
            }
            else if (this.CurrentLayout.Name.IndexOfAny(Path.GetInvalidPathChars()) >= 0 &&
                     !this.CurrentLayout.IsInitialized)
            {
                SaveAsCommand.DisabledReason =
                    String.Format(Message.ColumnLayoutEditor_DisabledReason_LayoutNameContainsInvalidChars,
                        new String(Path.GetInvalidPathChars()));
                return false;
            }
            else if (this.CurrentLayout.Columns.Count <= 0)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoVisibleColumns;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed(Object args)
        {
            String targetFolder = _layoutsFolderPath;
            if (!Directory.Exists(targetFolder)) Directory.CreateDirectory(targetFolder);

            //get the file name to save to.
            String fileName = args as String;
            if (String.IsNullOrEmpty(fileName))
            {
                if (!GetWindowService().ShowSaveFileDialog(
                    /*fileName*/this.CurrentLayout.Name,
                    targetFolder,
                    String.Format("Custom Column Layout files|*{0}", CustomColumnLayout.FileExtension),
                    out fileName))
                    return;
            }
            if (String.IsNullOrEmpty(fileName)) return;


            //save it
            base.ShowWaitCursor(true);
            try
            {
                this.CurrentLayout.Name = Path.GetFileNameWithoutExtension(fileName);

                //Build group by list
                BuildGroupByList(this.CurrentLayout.GroupList);

                //Build sort by list
                BuildSortByList(this.CurrentLayout.SortList);

                //Call save on the current view
                this.CurrentLayout = this.CurrentLayout.SaveAsFile(fileName);

                //unlock the file immediately.
                CustomColumnLayout.UnlockCustomColumnLayoutByFileName(fileName);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Save);
                return;
            }
            base.ShowWaitCursor(false);

        }

        #endregion

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        ///  Applies and closes the window.
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed())
                    {
                        FriendlyName = Message.ColumnLayoutEditor_Apply,
                        FriendlyDescription = Message.ColumnLayoutEditor_Apply_Description,
                        Icon = ImageResources.ColumnLayoutEditor_Apply,
                        SmallIcon = ImageResources.ColumnLayoutEditor_Apply,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoVisibleColumns
                    };
                    RegisterCommand(_applyCommand);
                }

                return _applyCommand;
            }
        }

        private void Apply_Executed()
        {
            ShowWaitCursor(true);

            //if there's been a change, sort out the lists
            if (this.CurrentLayout.IsDirty)
            {
                //Build group by list
                BuildGroupByList(this.CurrentLayout.GroupList);

                //Build sort by list
                BuildSortByList(this.CurrentLayout.SortList);
            }

            ShowWaitCursor(false);

            //close the window
            this.DialogResult = true;
        }

        #endregion

        #region Close

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the window
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                        FriendlyDescription = Message.ColumnLayoutEditor_CloseWindow_Description,
                        Icon = ImageResources.Close_16,
                        SmallIcon = ImageResources.Close_16
                    };
                    RegisterCommand(_closeCommand);
                }

                return _closeCommand;
            }
        }


        private void Close_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedColumnsCommand

        private RelayCommand _addSelectedColumnsCommand;

        /// <summary>
        ///     Adds any selected available columns to the current view column list
        /// </summary>
        public RelayCommand AddSelectedColumnsCommand
        {
            get
            {
                if (_addSelectedColumnsCommand != null) return _addSelectedColumnsCommand;

                _addSelectedColumnsCommand =
                    new RelayCommand(p => AddSelectedColumns_Executed(), p => AddSelectedColumns_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_AddSelectedColumns_Description,
                        SmallIcon = ImageResources.Add_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_addSelectedColumnsCommand);

                return _addSelectedColumnsCommand;
            }
        }

        private Boolean AddSelectedColumns_CanExecute()
        {
            if (this.SelectedFields.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void AddSelectedColumns_Executed()
        {
            List<ColumnLayoutEditorField> columnsToAdd = this.SelectedFields.ToList();
            this.SelectedFields.Clear();

            AddColumns(columnsToAdd);
        }

        #endregion

        #region RemoveSelectedColumnsCommand

        private RelayCommand _removeSelectedColumnsCommand;

        /// <summary>
        ///     Remove any selected available columns from the current view column list
        /// </summary>
        public RelayCommand RemoveSelectedColumnsCommand
        {
            get
            {
                if (_removeSelectedColumnsCommand != null) return _removeSelectedColumnsCommand;

                _removeSelectedColumnsCommand =
                    new RelayCommand(p => RemoveSelectedColumns_Executed(), p => RemoveSelectedColumns_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_RemoveSelectedColumns_Description,
                        SmallIcon = ImageResources.Delete_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_removeSelectedColumnsCommand);

                return _removeSelectedColumnsCommand;
            }
        }

        private void RefreshProperties()
        {
            //fire property changed on group/sort properties to ensure
            // - available column collections are updated to reflect removal
            OnPropertyChanged(GroupByLevelOneProperty);
            OnPropertyChanged(GroupByLevelTwoProperty);
            OnPropertyChanged(GroupByLevelThreeProperty);
            OnPropertyChanged(GroupByLevelFourProperty);
            OnPropertyChanged(SortByLevelOneProperty);
            OnPropertyChanged(SortByLevelTwoProperty);
            OnPropertyChanged(SortByLevelThreeProperty);
            OnPropertyChanged(SortByLevelFourProperty);
        }

        private Boolean RemoveSelectedColumns_CanExecute()
        {
            return SelectedAssignedColumnViews.Any();
        }

        private void RemoveSelectedColumns_Executed()
        {
            List<CustomColumnView> colsToRemove = this.SelectedAssignedColumnViews.ToList();
            this.SelectedAssignedColumnViews.Clear();
            RemoveColumns(colsToRemove);
        }

        #endregion

        #region MoveColumnUpCommand

        private RelayCommand _moveColumnUpCommand;

        /// <summary>
        ///     Move column up in the column order
        /// </summary>
        public RelayCommand MoveColumnUpCommand
        {
            get
            {
                if (_moveColumnUpCommand != null) return _moveColumnUpCommand;

                _moveColumnUpCommand = new RelayCommand(
                    p => MoveColumnUp_Executed(),
                    p => MoveColumnUp_CanExecute())
                {
                    FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnUp_Description,
                    SmallIcon = ImageResources.ColumnLayoutEditor_MoveColumnUp,
                    DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                };
                RegisterCommand(_moveColumnUpCommand);

                return _moveColumnUpCommand;
            }
        }

        private Boolean MoveColumnUp_CanExecute()
        {
            if (this.SelectedAssignedColumnViews == null)
                return false;

            if (this.SelectedAssignedColumnViews.Count == 0)
            {
                MoveColumnUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }
            if (this.SelectedAssignedColumnViews.Count != 1)
            {
                MoveColumnUpCommand.DisabledReason =
                    Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }
            if (this.SelectedAssignedColumnViews[0].Number == 1)
            {
                MoveColumnUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsTopAlready;
                return false;
            }

            return true;
        }

        private void MoveColumnUp_Executed()
        {
            ExchangeSelectionOrder(-1);
        }

        #endregion

        #region MoveColumnDownCommand

        private RelayCommand _moveColumnDownCommand;

        /// <summary>
        ///     Move column down in the column order
        /// </summary>
        public RelayCommand MoveColumnDownCommand
        {
            get
            {
                if (_moveColumnDownCommand != null) return _moveColumnDownCommand;

                _moveColumnDownCommand = new RelayCommand(
                    p => MoveColumnDown_Executed(),
                    p => MoveColumnDown_CanExecute())
                {
                    FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnDown_Description,
                    SmallIcon = ImageResources.ColumnLayoutEditor_MoveColumnDown,
                    DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                };
                RegisterCommand(_moveColumnDownCommand);

                return _moveColumnDownCommand;
            }
        }

        private Boolean MoveColumnDown_CanExecute()
        {
            if (this.SelectedAssignedColumnViews == null) return false;

            if (this.SelectedAssignedColumnViews.Count == 0)
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }

            if (this.SelectedAssignedColumnViews.Count != 1)
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }

            if (this.SelectedAssignedColumnViews.Count > 0
              && this.SelectedAssignedColumnViews[0].Number == this.AssignedColumnViews.Max(o => o.Number))
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsLastAlready;
                return false;
            }


            return true;
        }

        private void MoveColumnDown_Executed()
        {
            ExchangeSelectionOrder(1);
        }

        #endregion

        #region AddFilterCommand

        private RelayCommand _addFilterCommand;

        /// <summary>
        ///     Adds a new filter to the view
        /// </summary>
        public RelayCommand AddFilterCommand
        {
            get
            {
                if (_addFilterCommand != null) return _addFilterCommand;

                _addFilterCommand = new RelayCommand(
                    p => AddFilter_Executed(),
                    p => AddFilter_CanExecute())
                {
                    FriendlyName = Message.ColumnLayoutEditor_AddFilter,
                    FriendlyDescription = Message.ColumnLayoutEditor_AddFilter_Description,
                    SmallIcon = ImageResources.ColumnLayoutEditor_Filter_32
                };
                RegisterCommand(_addFilterCommand);

                return _addFilterCommand;
            }
        }


        private Boolean AddFilter_CanExecute()
        {
            return (SelectedNewFilterColumn != null & (SelectedNewFilterText.Length > 0));
        }

        private void AddFilter_Executed()
        {
            //Take copy of the selected new filter column
            CustomColumnView selectedFilterColumn = SelectedNewFilterColumn;

            //Remove from available list
            this.AvailableFilterByColumns.Remove(selectedFilterColumn);

            //Add to view filters
            this.CurrentLayout.FilterList.Add(
                CustomColumnFilter.NewCustomColumnFilter(SelectedNewFilterText,
                selectedFilterColumn.Path, selectedFilterColumn.DisplayName));

            //Clear text and select new column
            this.SelectedNewFilterText = String.Empty;
            this.SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        ///     Removes an existing filter from a view
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand != null) return _removeFilterCommand;

                _removeFilterCommand = new RelayCommand(
                    p => RemoveFilter_Executed(),
                    p => RemoveFilter_CanExecute())
                {
                    FriendlyName = Message.ColumnLayoutEditor_RemoveFilter,
                    FriendlyDescription = Message.ColumnLayoutEditor_RemoveFilter_Description,
                    SmallIcon = ImageResources.Delete_16
                };
                RegisterCommand(_removeFilterCommand);

                return _removeFilterCommand;
            }
        }

        private Boolean RemoveFilter_CanExecute()
        {
            return (SelectedFiltersSelection != null);
        }

        private void RemoveFilter_Executed()
        {
            //Take copy of the selected filter
            CustomColumnFilter selectedFilter = SelectedFiltersSelection;

            //Remove filter
            this.CurrentLayout.FilterList.Remove(selectedFilter);

            //Get corresponding column
            var column = this.CurrentLayout.Columns.FirstOrDefault(o => o.Path == selectedFilter.Path);
            if (column == null) return;

            //Add back to available list
            ColumnLayoutEditorField colField = _masterFields.FirstOrDefault(f => f.Path == column.Path);
            CustomColumnView colView = CreateColumnView(column, colField);
            this.AvailableFilterByColumns.Add(colView);
            this.SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        #endregion

        #region AddCalculatedColumnCommand

        private RelayCommand _addCalculatedColumnCommand;

        /// <summary>
        /// Launches the window to allow the user to add a new calculated column.
        /// </summary>
        public RelayCommand AddCalculatedColumnCommand
        {
            get
            {
                if (_addCalculatedColumnCommand == null)
                {
                    _addCalculatedColumnCommand = new RelayCommand(
                        p => AddCalculatedColumn_Executed())
                    {
                        FriendlyName = Message.DataSheetEditor_AddCalculatedColumn,
                        SmallIcon = ImageResources.Formula_16,
                    };
                    RegisterCommand(_addCalculatedColumnCommand);
                }
                return _addCalculatedColumnCommand;
            }
        }

        private void AddCalculatedColumn_Executed()
        {
            //Create the field groups
            List<FieldSelectorGroup> fieldGroups = new List<FieldSelectorGroup>(this.AvailableFieldGroups.Count);
            foreach (var group in _masterFields.GroupBy(f => f.GroupName))
            {
                fieldGroups.Add(new FieldSelectorGroup(group.Key, group.Select(f => f.FieldInfo)));
            }

            //Show the field selector window
            FieldSelectorViewModel fieldVm = new FieldSelectorViewModel(
                FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue, String.Empty, fieldGroups);
            GetWindowService().ShowDialog<FieldSelectorWindow>(fieldVm);
            if (fieldVm.DialogResult != true) return;

            String path = fieldVm.FieldText;

            //first check if the selected patch matches up to an available column
            // if it does then add it normally.
            ColumnLayoutEditorField existingField = _masterFields.FirstOrDefault(f => f.Path == path);
            if (existingField != null)
            {
                AddColumns(new List<ColumnLayoutEditorField> { existingField });
                return;
            }


            //Determine the next header number
            Int32 calcNo = 0;
            String header = Message.ColumnLayoutEditor_CalculatedColumnHeader;
            foreach (String existing in this.AssignedColumnViews.Select(c => c.DisplayName))
            {
                Match m = Regex.Match(existing, header + " [0-9+]");
                if (!m.Success) continue;

                Int32 number = Int32.Parse(m.Value.Replace(header + " ", null));
                if (number > calcNo) calcNo = number;
            }
            calcNo++;

            //Add the column
            CustomColumn column = CustomColumn.NewCustomColumn(path, 0);
            column.DisplayName = header + " " + calcNo;
            this.CurrentLayout.Columns.Add(column);

            CustomColumnView colView = _assignedColumnViews.FirstOrDefault(c => c.Column == column);

            this.AvailableFilterByColumns.Add(colView);
            this.AvailableGroupSortByColumns.Add(colView);

            AdjustNewColumnsOrder(new List<CustomColumnView> { colView });
            RefreshProperties();
        }

        #endregion

        #region EditCalculatedColumnCommand

        private RelayCommand _editCalculatedColumnCommand;

        public RelayCommand EditCalculatedColumnCommand
        {
            get
            {
                if (_editCalculatedColumnCommand == null)
                {
                    _editCalculatedColumnCommand =
                        new RelayCommand(
                            p => EditCalculatedColumn_Executed(p as CustomColumnView),
                            p => EditCalculatedColumn_CanExecute(p as CustomColumnView))
                            {
                                FriendlyName = "...",
                                FriendlyDescription = Message.ColumnLayoutEditor_EditCalculatedColumn_Desc
                            };
                    RegisterCommand(_editCalculatedColumnCommand);
                }
                return _editCalculatedColumnCommand;
            }
        }

        private Boolean EditCalculatedColumn_CanExecute(CustomColumnView colView)
        {
            if (colView == null || !colView.IsCalculated)
            {
                return false;
            }
            return true;
        }

        private void EditCalculatedColumn_Executed(CustomColumnView colView)
        {
            //Create the field groups
            List<FieldSelectorGroup> fieldGroups = new List<FieldSelectorGroup>(this.AvailableFieldGroups.Count);
            foreach (var group in _masterFields.GroupBy(f => f.GroupName))
            {
                fieldGroups.Add(new FieldSelectorGroup(group.Key, group.Select(f => f.FieldInfo)));
            }

            //Show the field selector window
            FieldSelectorViewModel fieldVm = new FieldSelectorViewModel(
                FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue, colView.Path, fieldGroups);
            GetWindowService().ShowDialog<FieldSelectorWindow>(fieldVm);
            if (fieldVm.DialogResult != true) return;

            colView.Path = fieldVm.FieldText;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds new columns for the given fields.
        /// </summary>
        /// <param name="columnsToAdd"></param>
        private void AddColumns(IEnumerable<ColumnLayoutEditorField> columnsToAdd)
        {
            //remove from the available list
            _selectedGroupFields.RemoveRange(columnsToAdd);

            //add them to the selected columns 
            List<CustomColumnView> newColumns = new List<CustomColumnView>();
            foreach (ColumnLayoutEditorField field in columnsToAdd)
            {
                CustomColumn column = CustomColumn.NewCustomColumn(field.FieldInfo.FieldPlaceholder, 0);
                if (field.FieldInfo != null && field.FieldInfo.IsNumeric) column.TotalType = CustomColumnTotalType.Sum;
                this.CurrentLayout.Columns.Add(column);

                CustomColumnView colView = _assignedColumnViews.FirstOrDefault(c => c.Column == column);
                newColumns.Add(colView);

                this.AvailableFilterByColumns.Add(colView);
                this.AvailableGroupSortByColumns.Add(colView);
            }


            AdjustNewColumnsOrder(newColumns);
            RefreshProperties();
        }

        /// <summary>
        /// Removes the given columns
        /// </summary>
        private void RemoveColumns(IEnumerable<CustomColumnView> items)
        {
            foreach (var col in items.ToList())
            {
                this.CurrentLayout.Columns.Remove(col.Column);

                AvailableFilterByColumns.Remove(AvailableFilterByColumns.FirstOrDefault(o => o.Path == col.Path));
                SelectedFilters.Remove(SelectedFilters.FirstOrDefault(o => o.Path == col.Path));
                AvailableGroupSortByColumns.Remove(AvailableGroupSortByColumns.FirstOrDefault(o => o.Path == col.Path));
            }


            OnSelectedFieldGroupChanged(this.SelectedFieldGroup);
            RefreshProperties();

            //CloneCustomColumns(SelectedColumns, this.AssignedColumnViews, CurrentLayout.Type, MasterFieldLookup);
            RemoveRowGaps(this.AssignedColumnViews);
        }

        /// <summary>
        /// Creates a new view for the given CustomColumn
        /// </summary>
        private CustomColumnView CreateColumnView(CustomColumn col, ColumnLayoutEditorField field = null)
        {
            if (field == null) field = _masterFields.FirstOrDefault(f => f.Path == col.Path);

            if (field == null && col.Path == Message.ColumnLayoutEditor_EmptyGroupSelection)
            {
                return new CustomColumnView(col, Message.ColumnLayoutEditor_EmptyGroupSelection);
            }

            CustomColumnView colView = new CustomColumnView(col,
                _layoutFactory.GetDefaultHeaderName((field != null) ? field.FieldInfo : null),
                (field == null));

            return colView;
        }

        /// <summary>
        ///     Method to cascade through lower property and clear them as necessary
        /// </summary>
        /// <param name="property"></param>
        private void CascadePropertyClear(PropertyPath property)
        {
            if (property.Path == GroupByLevelOneProperty.Path)
            {
                //if group one is empty, clear group two
                if (_groupByLevelOne == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower group has the same value, clear the lower group
                else if (_groupByLevelTwo == _groupByLevelOne)
                {
                    GroupByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelThree == _groupByLevelOne)
                {
                    GroupByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelFour == _groupByLevelOne)
                {
                    GroupByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == GroupByLevelTwoProperty.Path)
            {
                //if group two is empty, clear group three
                if (_groupByLevelTwo == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower group has the same value, clear the lower group
                else if (_groupByLevelThree == _groupByLevelTwo)
                {
                    GroupByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelFour == _groupByLevelTwo)
                {
                    GroupByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == GroupByLevelThreeProperty.Path)
            {
                //if group three is empty, clear group four
                if (_groupByLevelThree == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if group four holds the same value, clear group four
                else if (_groupByLevelFour == _groupByLevelThree)
                {
                    GroupByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelOneProperty.Path)
            {
                //if sort one is empty, clear sort two
                if (_sortByLevelOne == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower sort has the same value, clear the lower sort
                else if (_sortByLevelTwo == _sortByLevelOne)
                {
                    SortByLevelTwo = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelThree == _sortByLevelOne)
                {
                    SortByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelFour == _sortByLevelOne)
                {
                    SortByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelTwoProperty.Path)
            {
                //if sort two is empty, clear sort three
                if (_sortByLevelTwo == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower sort has the same value, clear the lower sort
                else if (_sortByLevelThree == _sortByLevelTwo)
                {
                    SortByLevelThree = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelFour == _sortByLevelTwo)
                {
                    SortByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelThreeProperty.Path)
            {
                //if sort three is empty, clear sort four
                if (_sortByLevelThree == Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if sort four has the same value, clear sort four
                else if (_sortByLevelFour == _sortByLevelThree)
                {
                    SortByLevelFour = Message.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
        }

        /// <summary>
        ///     Finds the <see cref="CustomColumn" /> with the provided <paramref name="path" /> and selects it.
        /// </summary>
        /// <param name="path">The path of the <see cref="CustomColumn" /> to locate in the <see cref="SelectedColumns" />.</param>
        private void SelectColumnByPath(String path)
        {
            var match = this.AssignedColumnViews.FirstOrDefault(p => p.Path == path);
            if (match == null) return;
            this.SelectedAssignedColumnViews.Clear();
            this.SelectedAssignedColumnViews.Add(match);
        }

        #region Filtering

        /// <summary>
        ///     Adds, ordered by Header, the columns in the <see cref="CurrentLayout" /> as FilterBy columns.
        /// </summary>
        /// <param name="selectedFilters">Collection containing any already selected filter by columns.</param>
        private void AddAvailableFilterByColumns(IEnumerable<CustomColumnFilter> selectedFilters)
        {
            AvailableFilterByColumns.Clear();

            foreach (var col in this.CurrentLayout.Columns
                .OrderBy(o => o.Number)
                .Where(col => selectedFilters.All(o => o.Path != col.Path)))
            {
                CustomColumnView view = CreateColumnView(col);
                this.AvailableFilterByColumns.Add(view);
            }

            SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        /// <summary>
        ///     Method to filter the AvailableGroupSortByColumns based upon previous levels
        /// </summary>
        /// <param name="property">property to indicate current level</param>
        /// <returns>filtered ObservableCollection</returns>
        private ObservableCollection<CustomColumnView> GetFilteredCollection(PropertyPath property)
        {
            //group by two
            if (property.Path == AvailableGroupByColumnsTwoProperty.Path)
            {
                if (GroupByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one
                    return AvailableGroupSortByColumns.Where(r => r.Path != GroupByLevelOne).ToObservableCollection();
                }
            }
            //group by three
            if (property.Path == AvailableGroupByColumnsThreeProperty.Path)
            {
                //if group one is not empty & group two is not empty
                if (GroupByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    GroupByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one or two
                    return AvailableGroupSortByColumns.Where(r => r.Path != GroupByLevelOne &&
                                                                  r.Path != GroupByLevelTwo).ToObservableCollection();
                }
            }
            //group by four
            if (property.Path == AvailableGroupByColumnsFourProperty.Path)
            {
                //if group one is not empty & group two is not empty & group three is not empty
                if (GroupByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    GroupByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    GroupByLevelThree != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one or two or three
                    return AvailableGroupSortByColumns.Where(r => r.Path != GroupByLevelOne &&
                                                                  r.Path != GroupByLevelTwo &&
                                                                  r.Path != GroupByLevelThree).ToObservableCollection();
                }
            }
            //sort by two
            if (property.Path == AvailableSortByColumnsTwoProperty.Path)
            {
                //if sort one is not empty
                if (SortByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one
                    return AvailableGroupSortByColumns.Where(r => r.Path != SortByLevelOne).ToObservableCollection();
                }
            }
            //sort by three
            if (property.Path == AvailableSortByColumnsThreeProperty.Path)
            {
                //if sort one is not empty & sort two is not empty
                if (SortByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    SortByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one or two
                    return AvailableGroupSortByColumns.Where(r => r.Path != SortByLevelOne &&
                                                                  r.Path != SortByLevelTwo).ToObservableCollection();
                }
            }
            //sort by four
            if (property.Path == AvailableSortByColumnsFourProperty.Path)
            {
                //if sort one is not empty & sort two is not empty & sort three is not empty
                if (SortByLevelOne != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    SortByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection &&
                    SortByLevelThree != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    //return the collection where the item is not group one or two or three
                    return AvailableGroupSortByColumns.Where(r => r.Path != SortByLevelOne &&
                                                                  r.Path != SortByLevelTwo &&
                                                                  r.Path != SortByLevelThree).ToObservableCollection();
                }
            }
            //if at this point there hasn't been a list returned, just return the default one so there isn't an empty combo box.
            return AvailableGroupSortByColumns;
        }

        #endregion

        #region Grouping

        /// <summary>
        ///     Adds the <see cref="CustomColumnGroup" /> items according to the selected groups.
        /// </summary>
        /// <param name="columnGroupList">The collection of <see cref="CustomColumnGroup" /> items to be built.</param>
        private void BuildGroupByList(CustomColumnGroupList columnGroupList)
        {
            columnGroupList.Clear();

            if (GroupByLevelOne == Message.ColumnLayoutEditor_EmptyGroupSelection) return;

            var list = CustomColumnGroupList.NewCustomColumnGroupList();
            // Initialize a temporary list to hold the grouping selections.

            list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelOne)); // Add the first grouping selection.
            if (GroupByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection)
            {
                list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelTwo));
                // Add the second grouping selection.
                if (GroupByLevelThree != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelThree));
                    // Add the third grouping selection.
                    if (GroupByLevelFour != Message.ColumnLayoutEditor_EmptyGroupSelection)
                    {
                        list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelFour));
                        // Add the fourth grouping selection.
                    }
                }
            }

            columnGroupList.AddList(list); // Update the column grouping list with the selections.
        }

        /// <summary>
        ///     Assign the selected column groupings to the properties bound on the UI.
        /// </summary>
        /// <param name="groups">Collection of already selected column groupings.</param>
        private void SetupGroupByProperties(IList<CustomColumnGroup> groups)
        {
            GroupByLevelOne = groups.Count > 0 ? groups[0].Grouping : null;
            GroupByLevelTwo = groups.Count > 1 ? groups[1].Grouping : null;
            GroupByLevelThree = groups.Count > 2 ? groups[2].Grouping : null;
            GroupByLevelFour = groups.Count > 3 ? groups[3].Grouping : null;
        }

        #endregion

        #region Sorting

        /// <summary>
        ///     Adds, ordered by Header, the available columns in the SelectedLayoutCopy.
        /// </summary>
        private void AddAvailableGroupSortByColumns()
        {
            AvailableGroupSortByColumns.Clear();

            AvailableGroupSortByColumns.Add(CreateColumnView(
                CustomColumn.NewCustomColumn(Message.ColumnLayoutEditor_EmptyGroupSelection, 0)));

            CurrentLayout.Columns
                .OrderBy(o => o.Number).ToList()
                .ForEach(column => AvailableGroupSortByColumns.Add(CreateColumnView(column)));
        }

        /// <summary>
        ///     Assign the selected column sortings to the properties bound on the UI.
        /// </summary>
        /// <param name="sorts">Collection of already selected column sortings.</param>
        private void SetupSortByProperties(IList<CustomColumnSort> sorts)
        {
            SortByLevelOne = sorts.Count > 0 ? sorts[0].Path : null;
            SortByLevelOneAscending = sorts.Count > 0 && sorts[0].Direction == ListSortDirection.Ascending;
            SortByLevelOneDescending = !SortByLevelOneAscending;
            SortByLevelTwo = sorts.Count > 1 ? sorts[1].Path : null;
            SortByLevelTwoAscending = sorts.Count > 1 && sorts[1].Direction == ListSortDirection.Ascending;
            SortByLevelTwoDescending = !SortByLevelTwoAscending;
            SortByLevelThree = sorts.Count > 2 ? sorts[2].Path : null;
            SortByLevelThreeAscending = sorts.Count > 2 && sorts[2].Direction == ListSortDirection.Ascending;
            SortByLevelThreeDescending = !SortByLevelThreeAscending;
            SortByLevelFour = sorts.Count > 3 ? sorts[3].Path : null;
            SortByLevelFourAscending = sorts.Count > 3 && sorts[3].Direction == ListSortDirection.Ascending;
            SortByLevelFourDescending = !SortByLevelFourAscending;
        }

        /// <summary>
        ///     Adds the <see cref="CustomColumnSort" /> items according to the selected filters.
        /// </summary>
        /// <param name="columnSortList">The collection of <see cref="CustomColumnSort" /> items to be built.</param>
        private void BuildSortByList(CustomColumnSortList columnSortList)
        {
            columnSortList.Clear(); // Initialize the sort selection list.

            if (SortByLevelOne == Message.ColumnLayoutEditor_EmptyGroupSelection) return;

            var list = CustomColumnSortList.NewCustomColumnSortList();

            list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelOne,
                CastToListSortDirection(SortByLevelOneAscending))); // Add the first sort selection.
            if (SortByLevelTwo != Message.ColumnLayoutEditor_EmptyGroupSelection)
            {
                list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelTwo,
                    CastToListSortDirection(SortByLevelTwoAscending))); // Add the second sort selection.
                if (SortByLevelThree != Message.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelThree,
                        CastToListSortDirection(SortByLevelThreeAscending))); // Add the third sort selection.
                    if (SortByLevelFour != Message.ColumnLayoutEditor_EmptyGroupSelection)
                    {
                        list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelFour,
                            CastToListSortDirection(SortByLevelFourAscending))); // Add the fourth sort selection.
                    }
                }
            }

            columnSortList.AddList(list); // Update the column sorting list with the selections.
        }

        /// <summary>
        ///     Casts <paramref name="value" /> from a <see cref="Boolean" /> to a <see cref="ListSortDirection" /> value.
        /// </summary>
        /// <param name="value">The value to be cast into a type of <see cref="ListSortDirection" />.</param>
        /// <returns>
        ///     Returns the appropriate <see cref="ListSortDirection" /> based on <paramref name="value" /> (<c>Ascending</c>
        ///     if <c>True</c>, <c>Descending</c> if <c>False</c>.
        /// </returns>
        private static ListSortDirection CastToListSortDirection(Boolean value)
        {
            return value
                ? ListSortDirection.Ascending
                : ListSortDirection.Descending;
        }

        #endregion

        #region Column Ordering


        /// <summary>
        ///     Moves the selected column (or group columns if a group is selected) the provided offser rows.
        /// </summary>
        /// <param name="offset">Number of positions to move the selection.</param>
        /// <remarks>
        ///     Negative offsets move the selection up, possitive offsets move the selection down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeSelectionOrder(Int32 offset)
        {
            var source = this.SelectedAssignedColumnViews.FirstOrDefault();
            if (source == null) return;


            ExchangeOrder(source, offset);

            SelectColumnByPath(source.Path);

            OnColumnOrderChanged();
        }

        /// <summary>
        ///  Swaps column order within the group.
        /// </summary>
        /// <param name="source">The <see cref="CustomColumn" /> that will be moved.</param>
        /// <param name="offset">Offset to the desired position.</param>
        /// <param name="itemCollection">The collection containing the items to be exchanged.</param>
        /// <remarks>
        ///     Negative offsets move the column up, possitive offsets move the column down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeOrder(CustomColumnView source, Int32 offset)
        {
            if (source == null || offset == 0) return; // Nothing to exchange.

            Int32 targetOrder = source.Number + offset;
            CustomColumnView target = this.AssignedColumnViews.FirstOrDefault(o => o.Number == targetOrder);

            if (target != null)
            {

                target.Number = source.Number;
                source.Number = targetOrder;
            }
        }

        /// <summary>
        ///     Removes missing rows in <paramref name="CustomColumns" /> so that all the row numbers are consecutive.
        /// </summary>
        /// <param name="CustomColumns"><see cref="List{T}" /> of <see cref="CustomColumn" /> from which to remove gaps.</param>
        private void RemoveRowGaps(IEnumerable<CustomColumnView> colList)
        {
            Int32 colNumber = 1;
            foreach (CustomColumnView col in colList.OrderBy(p => p.Number))
            {
                col.Number = colNumber++; // Set the column order, and then advance the counter .
            }

            OnColumnOrderChanged();
        }

        /// <summary>
        ///     Adjusts the order number of each <see cref="CustomColumn" /> from <paramref name="newItems" /> so that they
        ///     can be appended at the end of the <paramref name="itemCollection" />.
        /// </summary>
        /// <param name="newItems">
        ///     The collection of <see cref="CustomColumn" /> that need reordering before appending to
        ///     <paramref name="itemCollection" />.
        /// </param>
        /// <param name="itemCollection">
        ///     The collection of already selected <see cref="CustomColumn" /> which order will be
        ///     preserved.
        /// </param>
        /// <remarks>Each group header's <see cref="CustomColumn" /> collection has its own order within the group.</remarks>
        private void AdjustNewColumnsOrder(List<CustomColumnView> newItems)
        {
            if (this.AssignedColumnViews.Count == 0) return;

            List<String> newPaths = newItems.Select(c => c.Path).ToList();


            //first check if the header group already exists
            IEnumerable<CustomColumnView> existingGroupCols = this.AssignedColumnViews.Where(o => !newPaths.Contains(o.Path));

            Int32 startColNumber = (existingGroupCols.Any()) ? existingGroupCols.Max(c => c.Number) + 1 : 1;

            foreach (CustomColumnView col in newItems)
            {
                col.Number = startColNumber;

                startColNumber++;
            }

            OnColumnOrderChanged();
        }


        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current layout changes.
        /// </summary>
        private void OnCurrentLayoutChanged(CustomColumnLayout oldValue, CustomColumnLayout newValue)
        {
            if (oldValue != null)
            {
                oldValue.Columns.BulkCollectionChanged -= CurrentLayout_ColumnsBulkCollectionChanged;
            }


            if (newValue != null)
            {
                CurrentLayout_ColumnsBulkCollectionChanged(newValue, new Framework.Model.BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                newValue.Columns.BulkCollectionChanged += CurrentLayout_ColumnsBulkCollectionChanged;

                //from OnSelectedLayoutChanged
                AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;

                AddAvailableGroupSortByColumns();
                AddAvailableFilterByColumns(newValue.FilterList);
                SetupGroupByProperties(newValue.GroupList);
                SetupSortByProperties(newValue.SortList);

                //subscribe to collection change events
                AvailableFilterByColumns.CollectionChanged += AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged += AvailableGroupSortByColumns_CollectionChanged;
            }

        }

        /// <summary>
        /// Called whenever the columns collection on the current layout changes.
        /// </summary>
        private void CurrentLayout_ColumnsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (CustomColumn col in e.ChangedItems)
                    {
                        _assignedColumnViews.Add(CreateColumnView(col));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (CustomColumn col in e.ChangedItems)
                    {
                        CustomColumnView colView =
                            _assignedColumnViews.FirstOrDefault(v => v.Column == col);

                        _assignedColumnViews.Remove(colView);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _assignedColumnViews.Clear();
                    foreach (CustomColumn col in this.CurrentLayout.Columns)
                    {
                        _assignedColumnViews.Add(CreateColumnView(col));
                    }
                    break;
            }
            OnColumnOrderChanged();
        }

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        private void OnSelectedFieldGroupChanged(ColumnLayoutEditorFieldGroup newValue)
        {
            _selectedGroupFields.Clear();

            List<String> takenPaths = this.CurrentLayout.Columns.Select(c => c.Path).ToList();


            _selectedGroupFields.AddRange(
                     _masterFields
                     .Where(f => newValue.IsAllGroup || newValue.GroupName == f.GroupName)
                     .Except(_masterFields.Where(f => takenPaths.Contains(f.Path))));

        }


        /// <summary>
        ///     Event handler for the available filter columns changing
        /// </summary>
        private void AvailableFilterByColumns_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;

            //Reorder collection
            AvailableFilterByColumns = AvailableFilterByColumns.OrderBy(o => o.Number).ToObservableCollection();
            //Reselect first
            SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();

            AvailableFilterByColumns.CollectionChanged += AvailableFilterByColumns_CollectionChanged;
        }

        /// <summary>
        ///     Event handler for the available group/sort by columns changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableGroupSortByColumns_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;

            //Reorder collection
            AvailableGroupSortByColumns = AvailableGroupSortByColumns.ToObservableCollection();

            AvailableGroupSortByColumns.CollectionChanged += AvailableGroupSortByColumns_CollectionChanged;
        }



        #endregion

        #region Events

        public event EventHandler ColumnOrderChanged;

        private void OnColumnOrderChanged()
        {
            if (ColumnOrderChanged != null)
            {
                ColumnOrderChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IDisposable Support

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                this.CurrentLayout = null;

                AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;
            }
            IsDisposed = true;
        }

        #endregion
    }

}