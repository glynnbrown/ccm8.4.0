﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014


#region Version History: (CCM 8.3.0)
//V8-31832 : L.Ineson
//  Moved customcolumnview to here
#endregion

#endregion

using System;
using System.ComponentModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    /// Wrapper object for the custom column that includes any lookups
    /// </summary>
    public sealed class CustomColumnView : INotifyPropertyChanged
    {
        #region Fields

        private readonly CustomColumn _column;
        private readonly String _defaultHeader;

        #endregion

        #region Properties

        public CustomColumn Column
        {
            get { return _column; }
        }

        public String Path
        {
            get { return _column.Path; }
            set
            {
                _column.Path = value;
                OnPropertyChanged("Path");
            }
        }

        public Int32 Number
        {
            get { return _column.Number; }
            set
            {
                if (!_column.IsDeleted)
                {
                    _column.Number = value;
                    OnPropertyChanged("Number");
                    OnPropertyChanged("ColumnNumberAndHeader");
                }
            }
        }

        public String Header
        {
            get { return _defaultHeader; }
        }

        public String ColumnNumberAndHeader
        {
            get { return Number.ToString() + ". " + Header; }
        }

        /// <summary>
        /// Gets/Sets the column display name
        /// </summary>
        public String DisplayName
        {
            get
            {
                if (!String.IsNullOrEmpty(_column.DisplayName))
                {
                    return _column.DisplayName;
                }
                return this.Header;
            }
            set
            {
                _column.DisplayName = value;
                OnPropertyChanged("DisplayName");
                OnPropertyChanged("FullDisplayName");
            }
        }

        /// <summary>
        /// Gets/Sets the width of the column.
        /// </summary>
        public Int32 Width
        {
            get { return _column.Width; }
            set
            {
                _column.Width = value;
                OnPropertyChanged("Width");
            }
        }

        /// <summary>
        /// Gets/Sets the totals type of the column.
        /// </summary>
        public CustomColumnTotalType TotalType
        {
            get { return _column.TotalType; }
            set
            {
                _column.TotalType = value;
                OnPropertyChanged("TotalType");
            }
        }

        /// <summary>
        /// Returns true if this is a calculated column
        /// </summary>
        public Boolean IsCalculated { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public CustomColumnView(CustomColumn column, String header, Boolean isCalculated = false)
        {
            _column = column;
            _defaultHeader = header;

            this.IsCalculated = isCalculated;
        }

        #endregion

        #region Methods

        public override String ToString()
        {
            return Header;
        }

        #endregion

        #region INotifyProperty Changed

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

    }

}
