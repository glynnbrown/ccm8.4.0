﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-28986 : M.Shelley
//  Initial version - added custom columns to the plan assignment tab
#endregion

#region Version History: (CCM 8.0.3)
// V8-29326 : M.Shelley
//  Add the cluster location details to the assignment grid data 
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added Height
// V8-30055 : M.Shelley
//  Changed Group Name to be a default visible column
#endregion

#region Version History: CCM830

// V8-32385 : A.Silva
//  Removed Cluster Schema Name column from default visible columns.

#endregion

#region Version History: CCM840

// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class PlanAssignmentColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constructors

        public PlanAssignmentColumnLayoutFactory()
            : base(typeof(PlanAssignmentRowViewModel), CustomColumnLayoutType.PlanAssignment)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(new List<String>
            {
                // Add any properties that should be visible on load here.
                "Location." + Location.CodeProperty.Name,
                "Location." + Location.NameProperty.Name,
                //"PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.ClusterSchemeNameProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.ClusterNameProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.BayCountProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.BayTotalWidthProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.BayHeightProperty.Name,
                "ProductGroup." + ProductGroup.CodeProperty.Name,
                "ProductGroup." + ProductGroup.NameProperty.Name,
                "PlanogramInfo." + PlanogramInfo.NameProperty.Name,
                "PlanogramInfo." + PlanogramInfo.MetaBayCountProperty.Name,
                "PlanogramInfo." + PlanogramInfo.WidthProperty.Name,
                "PlanogramInfo." + PlanogramInfo.HeightProperty.Name,
                "PlanogramInfo." + PlanogramInfo.PlanogramGroupNameProperty.Name,
                PlanAssignmentRowViewModel.PlanAssignedByUserProperty.Path,
                "LocationPlanAssignment." + LocationPlanAssignment.DateAssignedProperty.Name,
                PlanAssignmentRowViewModel.PlanPublishUserProperty.Path,
                "LocationPlanAssignment." + LocationPlanAssignment.DatePublishedProperty.Name,
                PlanAssignmentRowViewModel.FriendlyPlanPublishTypeProperty.Path,
                "LocationPlanAssignment." + LocationPlanAssignment.OutputDestinationProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.ProductCountProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.AverageBayWidthProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.AisleNameProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.ValleyNameProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.ZoneNameProperty.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.CustomAttribute01Property.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.CustomAttribute02Property.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.CustomAttribute03Property.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.CustomAttribute04Property.Name,
                "PlanAssignmentStoreSpaceInfo." + PlanAssignmentStoreSpaceInfo.CustomAttribute05Property.Name,
                });
        }

        #endregion

        #region Methods

        public override IEnumerable<Framework.ViewModel.ObjectFieldInfo> GetModelObjectFields()
        {
            return PlanAssignmentRowViewModel.EnumerateDisplayableFieldInfos().Distinct().ToList();
        }

        #endregion
    }
}
