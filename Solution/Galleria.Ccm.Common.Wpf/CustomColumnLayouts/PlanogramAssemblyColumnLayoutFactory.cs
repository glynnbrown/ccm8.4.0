﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2)
// V8-30991 : L.Ineson
// Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///  Column layout factory used by the fixture list view when displaying
    ///  PlanogramAssemblyView rows.
    /// </summary>
    public sealed class PlanogramAssemblyColumnLayoutFactory : BaseColumnLayoutFactory
    {
        const String fixturePrefix = "Fixture.";

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramAssemblyColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            :base(rowSourceType, CustomColumnLayoutType.PlanogramAssembly, planogramContext)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //Add a list of properties visible by default.
            base.RegisterVisibleFields(
                new List<String>
            {
               PlanogramAssembly.NameProperty.Name,
               PlanogramAssembly.HeightProperty.Name,
               PlanogramAssembly.WidthProperty.Name,
               PlanogramAssembly.DepthProperty.Name,
               PlanogramFixtureAssembly.XProperty.Name,
               PlanogramFixtureAssembly.YProperty.Name,
               PlanogramFixtureAssembly.ZProperty.Name,
               PlanogramFixtureAssembly.MetaComponentCountProperty.Name,
               fixturePrefix + PlanogramFixture.NameProperty.Name,
            });
        }


        #endregion

        #region Methods

        /// <summary>
        /// Returns all field infos available for this layout.
        /// </summary>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            //Assembly
            String groupName = PlanogramAssembly.FriendlyName;
            foreach (var field in PlanogramAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixtureAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.GroupName = groupName;
                yield return field;
            }

            //Fixture
            groupName = PlanogramFixture.FriendlyName;
            foreach (var field in PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixture.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

        }

        #endregion
    }
}
