﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25172 : A.Silva ~ Created.
// V8-25170 : A.Silva ~ Added PathMask property to allow for path customization by the user.
// V8-26329 : A.Silva ~ Added UpdateColumnLayout and some refactoring.
// V8-26344 : A.Silva ~ Added OrderCustomColumns to allow reordering of columns and header groups.
// V8-26251 : A.Silva ~ Added NormalizeLayout to make sure the loaded layout is compatible with the current default.
// V8-26281 : A.Silva ~ moved out some classes to their own files.
// V8-26343 : A.Silva ~ Changed the folder path to retrieve the existing layout files.
// V8-26370 : A.Silva ~ Added some temporary performance tracing code.
// V8-26408 : L.Ineson
//  Split out CustomColumnLayoutFactory class
// V8-26597 : A.Silva ~ Added InitialColumnCollection so that new columns, not normally present in the layout.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27022 : A.Silva ~ Modified CurrentColumnSet to make sure it passes any existing PathMasks when constructing the data grid columns.
// V8-26944 : A.Silva ~ Added support for CustomColumnTemplates.
// V8-27196 : A.Silva ~ Amended DataTemplate Field registration to be linked to the FieldPlaceholder.
// V8-26513 : A.Silva
//      Changed AvailableLayoutInfos property to a method GetAvailableLayoutInfos, now it is refreshed every time the list is required.
// V8-27504 : A.Silva
//      Amended sorting by HeaderGroupNumber before the column's Number when creating the columns.
//      Amended resorting of columns to account for HeaderGroupNumber.
// V8-27898 : L.Ineson
//  Added ability to pass through the row item type to use when checking if properties should be readonly.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-28040 : A.Probyn
//  ~ Added defensive code to UpdateColumnLayoutFromGrid when column set is null.
//  ~ Added new ApplyFiltersIntoGrid method so that filters can be manually applied once linkage 
//    between DataGridColumns and grid is made.
//  ~ Added missing language resource to replace '[LG]Change current layout'
// V8-27520 : A.Silva
//      Amended and refactored CreateColumnSet so that each header group's hidden columns are added right after the group's visible ones.
// V8-28175 : A.Kuszyk
//  Add optional parameters to constructor for decorating field list with Planogram specific content.
// V8-28373 : A.Silva
//      Amended LoadLayoutIntoGrid and UpdateColumnLayoutFromGrid so that the path mask is taken into account if there is one.

#endregion

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added method UpdateLayoutTypeAndScreenKey so that CustomColumnLayoutType can be amended 
//  along with screen key so that different column sets can be dynamically loaded into the grid. 
#endregion

#region Version History: (CCM 8.1.0)
// V8-29681 : A.Probyn
// Updated to use CustomColumnLayoutHelper.RegisterPlanogram
#endregion

#region Version History: (CCM 8.2.0)
// V8-31449 : M.Brumby
//  Changed order of items being cleared to prevent virtualisation errors.
#endregion
#region Version History: (CCM 8.3.0)
// V8-31832 : L.Ineson
//  Changes to support calculated columns.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
//V8-32139 : L.Ineson
//  Changes to make sure that calculated columns support totals, sorting & filtering.
//V8-32155 : L.Ineson
//  Removed the change current layout context menu option.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     This class helps managing custom layouts for view models requiring them.
    /// </summary>
    public sealed class ColumnLayoutManager : IDisposable
    {
        #region Nested Classes
        public sealed class ColumnLayoutColumnSetEventArgs : EventArgs
        {
            public DataGridColumnCollection ColumnSet { get; private set; }

            public ColumnLayoutColumnSetEventArgs(DataGridColumnCollection set)
            {
                this.ColumnSet = set;
            }
        }
        #endregion

        #region Fields

        private ExtendedDataGrid _dataGrid; //the attached datagrid control.
        private IColumnLayoutFactory _layoutFactory; //the layout factory to use.
        private CustomColumnLayout _currentColumnLayout; // the currently selected layout
        private DisplayUnitOfMeasureCollection _displayUomCollection; // the uom collection to use.
        private readonly Dictionary<String, ObjectFieldInfo> _fieldsByPlaceholder; //lookup cache for finding a field by placeholder
        private readonly Dictionary<String, ObjectFieldInfo> _fieldsByPropertyName;//lookup cache for finding a field by propertyname
        private String _screenKey; // the key for the screen the layout is for.
        private readonly String _columnLayoutsFolderPath; //The path to the folder containing the column layout settings files that this instance should use.

        private readonly Dictionary<Type, Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate>> _dataTemplates = new Dictionary<Type, Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate>>();
        private readonly Dictionary<String, Type> _dataTemplateFields = new Dictionary<String, Type>();

        private String _calculatedColumnPath = "CalculatedValueResolver";

        #endregion

        #region Properties

        /// <summary>
        /// Gets the attached datagrid control.
        /// </summary>
        public ExtendedDataGrid DataGrid
        {
            get { return _dataGrid; }
            private set
            {
                if (_dataGrid == value) return;

                ExtendedDataGrid oldValue = _dataGrid;
                _dataGrid = value;
                OnDataGridControlChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Gets the layout factory used by this manager.
        /// </summary>
        public IColumnLayoutFactory LayoutFactory
        {
            get { return _layoutFactory; }
            private set { _layoutFactory = value; }
        }

        /// <summary>
        /// Gets/Sets the uom collection to use for column creation.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUomCollection
        {
            get { return _displayUomCollection; }
            set
            {
                if (_displayUomCollection == value) return;

                _displayUomCollection = value;

                //update the grid to reflect the change
                RefreshGrid(true);
            }
        }

        /// <summary>
        /// Gets/Sets the current layout.
        /// </summary>
        public CustomColumnLayout CurrentColumnLayout
        {
            get { return _currentColumnLayout ?? (_currentColumnLayout = GetUserColumnLayout()); }
            set
            {
                if (_currentColumnLayout == value) return; // Do not update when there were really no changes.
                _currentColumnLayout = value;

                OnCurrentColumnLayoutChanged(value);
            }
        }

        /// <summary>
        ///     If the path mask is set, all paths for created datagridcolumns will have it applied.
        /// </summary>
        public String PathMask { get; set; }

        /// <summary>
        ///  Setting to specify whether to include missing columns in the grid to add later through the right click menu
        /// </summary>
        public Boolean IncludeUnseenColumns { get; set; }

        /// <summary>
        /// Setting to specify whether the edit command should be added to the column
        /// context menu.
        /// </summary>
        public Boolean AddEditToColumnContextMenu { get; set; }

        /// <summary>
        /// Gets/Sets the path to use for calculated columns.
        /// </summary>
        public String CalculatedColumnPath
        {
            get { return _calculatedColumnPath; }
            set { _calculatedColumnPath = value; }
        }

        #endregion

        #region Events

        #region ColumnSetChanging

        /// <summary>
        /// Notifies that the columnset is about to change to that given.
        /// Allows for it to be changed before it is applied.
        /// </summary>
        public event EventHandler<ColumnLayoutColumnSetEventArgs> ColumnSetChanging;

        private void RaiseColumnSetChanging(DataGridColumnCollection columnSet)
        {
            if (ColumnSetChanging != null)
            {
                ColumnSetChanging(this, new ColumnLayoutColumnSetEventArgs(columnSet));
            }
        }

        #endregion

        #region CurrentLayoutChanged

        /// <summary>
        /// Event to notify that the layout has changed.
        /// </summary>
        public event EventHandler CurrentLayoutChanged;

        private void RaiseCurrentLayoutChanged()
        {
            if (CurrentLayoutChanged != null)
            {
                CurrentLayoutChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dataGridControl">the datagrid control to attach to</param>
        /// <param name="layoutFactory">the layout factory to use when generating columns</param>
        /// <param name="displayUomCollection">the uom collection.</param>
        public ColumnLayoutManager(
            IColumnLayoutFactory layoutFactory,
            DisplayUnitOfMeasureCollection displayUomCollection,
            String screenKey, String pathMask = null)
        {
            _columnLayoutsFolderPath = CCMClient.ViewState.GetSessionDirectory(SessionDirectory.CustomColumnLayout);
            _displayUomCollection = displayUomCollection;
            _screenKey = screenKey;
            this.PathMask = pathMask;
            this.LayoutFactory = layoutFactory;

            _fieldsByPlaceholder = layoutFactory.GetFieldLookupCache();
            _fieldsByPropertyName = _fieldsByPlaceholder.Values.ToDictionary(f => f.PropertyName);

            this.IncludeUnseenColumns = true;
            this.AddEditToColumnContextMenu = true;
        }

        #endregion

        #region Commands

        #region Edit

        private RelayCommand _editCommand;

        /// <summary>
        /// Returns the command to 
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(
                    p => EditCustomColumnLayouts_Executed())
                    {
                        FriendlyName = Message.ColumnLayoutEditor_ContextMenu_EditCommand,
                        FriendlyDescription = Message.ColumnLayoutEditor_ContextMenu_EditCommand_Description,
                        Icon = ImageResources.ColumnLayoutEditor_EditCommand,
                        SmallIcon = ImageResources.ColumnLayoutEditor_EditCommand
                    };
                }
                return _editCommand;
            }
        }

        private void EditCustomColumnLayouts_Executed()
        {
            //update the column layout from the attached grid.
            UpdateColumnLayoutFromGrid();

            //show the editor dialog.
            ColumnLayoutEditorViewModel vm = new ColumnLayoutEditorViewModel(
                this.CurrentColumnLayout, this.LayoutFactory,
                GetLayoutTypePath());

            vm.CanUserAddCalculatedColumns = !String.IsNullOrEmpty(this.CalculatedColumnPath);

            CommonHelper.GetWindowService().ShowDialog<ColumnLayoutEditorOrganiser>(vm);
            if (vm.DialogResult != true) return;

            //update the layout
            this.CurrentColumnLayout = vm.CurrentLayout;
        }

        #endregion

        #endregion

        #region EventHandlers

        /// <summary>
        /// Called whenever the column context menu opens on the attached datagrid.
        /// </summary>
        private void DataGrid_ColumnContextMenuOpening(object sender, ExtendedDataGridContextMenuOpeningEventArgs e)
        {
            if (!AddEditToColumnContextMenu) return;

            //tag the edit option for this manager onto the end.
            ItemsControl contextMenu = e.ContextMenu;

            contextMenu.Items.Add(new Separator());

            contextMenu.Items.Add(new MenuItem
            {
                Header = EditCommand.FriendlyName,
                Command = EditCommand,
                CommandParameter = CurrentColumnLayout,
                HorizontalAlignment = HorizontalAlignment.Left
            });

        }

        /// <summary>
        /// Called whenever one of the available layouts is clicked on the context menu.
        /// </summary>
        private void ContextMenuAvailableLayout_Click(object sender, RoutedEventArgs e)
        {
            MenuItem selectedItem = sender as MenuItem;
            if (selectedItem == null) return;

            CustomColumnLayoutInfo layoutInfo = selectedItem.Tag as CustomColumnLayoutInfo;
            if (layoutInfo == null) return;

            //update the selected layout.
            SetCurrentColumnLayoutPreference(layoutInfo);
        }

        /// <summary>
        /// Called whenever the current column layout changes.
        /// </summary>
        private void OnCurrentColumnLayoutChanged(CustomColumnLayout newValue)
        {
            UpdateGridFromColumnLayout();

            RaiseCurrentLayoutChanged();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attaches to and initializes the given datagrid.
        /// </summary>
        public void AttachDataGrid(ExtendedDataGrid dataGridControl)
        {
            this.DataGrid = dataGridControl;
        }

        /// <summary>
        /// Attaches and dettaches from the datagrid controls.
        /// </summary>
        private void OnDataGridControlChanged(ExtendedDataGrid oldValue, ExtendedDataGrid newValue)
        {
            if (oldValue != null)
            {
                oldValue.ColumnContextMenuOpening -= DataGrid_ColumnContextMenuOpening;
            }


            if (newValue != null)
            {
                newValue.ColumnContextMenuOpening += DataGrid_ColumnContextMenuOpening;
                UpdateGridFromColumnLayout();
            }
        }

        /// <summary>
        /// Updates the column layout held in this manager from the given grid.
        /// </summary>
        public void UpdateColumnLayoutFromGrid()
        {
            ExtendedDataGrid extendedDataGrid = this.DataGrid;
            CustomColumnLayout layout = this.CurrentColumnLayout;

            Dictionary<String, ObjectFieldInfo> fieldByPropertyNameLookup = _fieldsByPropertyName;

            layout.Columns.Clear();
            layout.SortList.Clear();
            layout.GroupList.Clear();
            layout.FilterList.Clear();

            //TODO: Don't clear list, look for existing if there.

            // Update current column state with visible columns only
            if (extendedDataGrid.ColumnSet != null)
            {
                foreach (DataGridColumn column in extendedDataGrid.ColumnSet)
                {
                    //only add if visible and valid.
                    if (column.Visibility != Visibility.Visible) continue;

                    String colPath = RemovePathMask(ExtendedDataGrid.GetBindingProperty(column));
                    CustomColumn customColumn = null;

                    //first check if this is a calculated column.
                    Boolean isCalculated = false;
                    if (!String.IsNullOrEmpty(this.CalculatedColumnPath))
                    {
                        String calcPath = ExtendedDataGrid.GetBindingProperty(column);
                        if (calcPath != null)
                        {
                            Int32 expressionKey = GetCalculatedExpressionKeyFromPath(calcPath);
                            if (expressionKey != 0)
                            {
                                String expression = GetExpressionTextByKey(expressionKey);
                                if (expression != null)
                                {
                                    colPath = expression;
                                    isCalculated = true;

                                    customColumn = MapToCustomColumn(column, this.PathMask);
                                    customColumn.Path = colPath;
                                }
                            }
                        }
                    }


                    //if its not calculated and doesnt exist in the lookup then skip on 
                    if (!isCalculated && !fieldByPropertyNameLookup.ContainsKey(colPath)) continue;

                    //create the column
                    if (customColumn == null) customColumn = MapToCustomColumn(column, this.PathMask);
                    if (customColumn == null) continue;
                    layout.Columns.Add(customColumn);


                    if (!isCalculated)
                    {
                        //Correct path to use placeholder field
                        ObjectFieldInfo field = fieldByPropertyNameLookup[RemovePathMask(ExtendedDataGrid.GetBindingProperty(column))];
                        customColumn.Path = field.FieldPlaceholder;

                        //blank off the column display name if its the same as the default.
                        if (field.PropertyName == customColumn.DisplayName) customColumn.DisplayName = null;
                    }

                    //add the filter
                    String filter = ExtendedDataGrid.GetColumnFilterValue(column) as String;
                    if (!String.IsNullOrEmpty(filter))
                    {
                        layout.FilterList.Add(CustomColumnFilter.NewCustomColumnFilter(filter, customColumn.Path, column.Header.ToString()));
                    }

                }
            }


            //reorder the columns
            Int32 columnNumber = 1;
            foreach (CustomColumn column in layout.Columns.ToList())
            {
                column.Number = columnNumber++;
            }


            // Update current sort by descriptions.
            foreach (SortDescription sortByDescription in extendedDataGrid.SortByDescriptions)
            {
                //Lookup placeholder name from property name
                ObjectFieldInfo field;
                if (fieldByPropertyNameLookup.TryGetValue(RemovePathMask(sortByDescription.PropertyName), out field))
                {
                    layout.SortList.Add(CustomColumnSort.NewCustomColumnSort(field.FieldPlaceholder, sortByDescription.Direction));
                }
                else if (sortByDescription.PropertyName.StartsWith(this.CalculatedColumnPath))
                {
                    //must be calculated
                    Int32 key = GetCalculatedExpressionKeyFromPath(sortByDescription.PropertyName);
                    if (key != 0)
                    {
                        layout.SortList.Add(CustomColumnSort.NewCustomColumnSort(GetExpressionTextByKey(key), sortByDescription.Direction));
                    }
                }
            }

            // Update current group by descriptions.
            foreach (PropertyGroupDescription groupByDescription in
                extendedDataGrid.GroupByDescriptions.Cast<PropertyGroupDescription>())
            {
                //Lookup placeholder name from property name
                ObjectFieldInfo field;
                if (fieldByPropertyNameLookup.TryGetValue(RemovePathMask(groupByDescription.PropertyName), out field))
                {
                    layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(field.FieldPlaceholder));
                }
                else if (groupByDescription.PropertyName.StartsWith(this.CalculatedColumnPath))
                {
                    //must be calculated
                    Int32 key = GetCalculatedExpressionKeyFromPath(groupByDescription.PropertyName);
                    if (key != 0)
                    {
                        layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(GetExpressionTextByKey(key)));
                    }
                }
            }
        }

        /// <summary>
        ///     Set the <see cref="CurrentColumnLayout" /> and update the user's settings to persist their preference..
        /// </summary>
        private void SetCurrentColumnLayoutPreference(CustomColumnLayoutInfo info)
        {
            //get the layout
            CustomColumnLayout layout = null;
            try
            {
                layout = (info != null) ?
                    CustomColumnLayout.FetchByFilename(info.FileName,/*asReadOnly*/true)
                    : GetUserColumnLayout();
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
            }

            this.CurrentColumnLayout = layout;

            //update the user setting.
            String fileName = info != null ? info.FileName : String.Empty;
            ColumnLayoutSetting.UpdateUserSetting(_screenKey, fileName);
        }

        /// <summary>
        ///     Fetches the user's <see cref="CustomColumnLayout" /> setting for this instance, <c>or</c> the default one for the
        ///     screen's type, if missing.
        /// </summary>
        /// <returns>New instance with the <see cref="CurrentColumnLayout" /> to use.</returns>
        /// <remarks>
        ///  If the retrieved column layout settings' type is not valid for this instance, a default valid value is  returned.
        /// </remarks>
        private CustomColumnLayout GetUserColumnLayout()
        {
            CustomColumnLayoutType layoutType = this.LayoutFactory.LayoutType;

            //try to fetch the user layout
            CustomColumnLayout columnLayout = null;
            try
            {
                columnLayout = CustomColumnLayout.FetchUserColumnLayoutByKey(_screenKey);
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
            }
            if (columnLayout != null && columnLayout.Type == layoutType) return columnLayout;


            //if no layout was loaded then get the first available.
            IEnumerable<CustomColumnLayoutInfo> availableColumnLayoutInfos = GetAvailableColumnLayoutInfos();
            if (availableColumnLayoutInfos.Any())
            {
                columnLayout = CustomColumnLayout.FetchByFilename(availableColumnLayoutInfos.First().FileName, /*asReadOnly*/true);
            }
            if (columnLayout != null && columnLayout.Type == layoutType) return columnLayout;


            //if still nothing then just create a new one.
            columnLayout = CustomColumnLayout.NewCustomColumnLayout(layoutType, this.LayoutFactory.NewCustomColumnList(/*defaultOnly*/true));


            return columnLayout;
        }

        /// <summary>
        /// Updates the grid from the current column layout
        /// </summary>
        private void UpdateGridFromColumnLayout()
        {
            ExtendedDataGrid grid = this.DataGrid;
            if (grid == null) return;

            //change the cursor to a wait
            Boolean cursorSet = false;
            try
            {
                if (Mouse.OverrideCursor == null)
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                    cursorSet = true;
                }
            }
            catch { }

            CustomColumnLayout customColumnLayout = this.CurrentColumnLayout;
            Dictionary<String, ObjectFieldInfo> fieldLookup = _fieldsByPlaceholder;

            //start generating the column set.
            DataGridColumnCollection columnSet = CreateColumnSet();

            //fire the event to allow this to be amended before it is applied to the grid.
            RaiseColumnSetChanging(columnSet);


            //clear the grid completely
            ClearGrid();


            //apply the new set to the grid.
            grid.ColumnSet = columnSet;

            // Load all the filters on the grid.
            foreach (CustomColumnFilter item in customColumnLayout.FilterList)
            {
                //Lookup property name
                ObjectFieldInfo field;
                if (fieldLookup.TryGetValue(item.Path, out field))
                {
                    DataGridColumn col = ExtendedDataGrid.GetColumnByBindingProperty(grid, AddPathMask(field.PropertyName));
                    if (col != null)
                    {
                        ExtendedDataGrid.SetColumnFilterValue(col, item.Text);
                    }
                }
                else
                {
                    //check if this is calculated.
                    Int32? expressionKey = GetRegisteredExpressionKey(item.Path);
                    if (expressionKey.HasValue)
                    {
                        String colPath = CreateCalculatedColumnPath(expressionKey.Value);
                        foreach (DataGridColumn col in columnSet)
                        {
                            String property = ExtendedDataGrid.GetBindingProperty(col);
                            if (property == colPath)
                            {
                                ExtendedDataGrid.SetColumnFilterValue(col, item.Text);
                                break;
                            }
                        }
                    }
                }
            }

            // Load all group by descriptions on the grid.
            foreach (var item in customColumnLayout.GroupList)
            {
                //Lookup property name
                ObjectFieldInfo field;
                if (fieldLookup.TryGetValue(item.Grouping, out field))
                {
                    grid.GroupByDescriptions.Add(new PropertyGroupDescription(AddPathMask(field.PropertyName)));
                }
                else
                {
                    Int32? expressionKey = GetRegisteredExpressionKey(item.Grouping);
                    if (expressionKey.HasValue)
                    {
                        //must be a calculated column
                        grid.GroupByDescriptions.Add(new PropertyGroupDescription(CreateCalculatedColumnPath(expressionKey.Value)));
                    }
                }
            }

            // Load all sort by descriptions on the grid.
            foreach (var item in customColumnLayout.SortList)
            {
                //Lookup property name
                ObjectFieldInfo field;
                if (fieldLookup.TryGetValue(item.Path, out field))
                {
                    grid.SortByDescriptions.Add(new SortDescription(AddPathMask(field.PropertyName), item.Direction));
                }
                else
                {
                    Int32? expressionKey = GetRegisteredExpressionKey(item.Path);
                    if (expressionKey.HasValue)
                    {
                        //must be a calculated column
                        grid.SortByDescriptions.Add(new SortDescription(CreateCalculatedColumnPath(expressionKey.Value), item.Direction));
                    }
                }
            }

            //set the grid totals visibility
            grid.DisplayColumnTotals = customColumnLayout.HasTotals;

            //cancel wait cursor.
            if (cursorSet)
            {
                try { Mouse.OverrideCursor = null; }
                catch { }
            }
        }

        /// <summary>
        /// Returns a new collection of columns for the current layout.
        /// </summary>
        /// <returns></returns>
        private DataGridColumnCollection CreateColumnSet()
        {
            DataGridColumnCollection collection = new DataGridColumnCollection();

            //Load the column set from the current layout:

            // Group by header number all visible columns.
            List<DataGridColumn> visibleDataGridColumns =
                CurrentColumnLayout.Columns.Copy().OrderBy(o => o.Number)
                .Select(c => NewDataGridColumn(c)).ToList();


            // Group by header number all hidden columns.
            List<DataGridColumn> hiddenDataGridColumns;
            if (this.IncludeUnseenColumns)
            {
                hiddenDataGridColumns =
                this.GetNonVisibleColumns().OrderBy(view => view.Number).Select(c => NewDataGridColumn(c)).ToList();
            }
            else
            {
                hiddenDataGridColumns = new List<DataGridColumn>();
            }

            // Add the hidden columns if there are no visible columns to add.
            foreach (DataGridColumn column in visibleDataGridColumns.Where(o => o != null))
            {
                collection.Add(column);
            }

            // Add any hidden columns at the end of the group.
            foreach (DataGridColumn column in hiddenDataGridColumns.Where(o => o != null))
            {
                collection.Add(column);
            }

            //Apply filters
            //foreach (CustomColumnFilter filterItem in this.CurrentColumnLayout.FilterList)
            //{
            //    //Find column
            //    DataGridColumn col = collection.FirstOrDefault(p => ExtendedDataGrid.GetBindingProperty(p).Equals(filterItem.Path));
            //    if (col != null)
            //    {
            //        ExtendedDataGrid.SetColumnFilterValue(col, filterItem.Text);
            //    }
            //}

            return collection;
        }

        /// <summary>
        /// Helper method to fetch the columns that are not loaded into the custom column layout, making them
        /// available but not visible.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<CustomColumnView> GetNonVisibleColumns()
        {
            List<CustomColumnView> nonVisibleColumns = new List<CustomColumnView>();

            //Get lookup of existing columns
            List<String> visibleColumns = this.CurrentColumnLayout.Columns.Select(p => p.Path).ToList();

            //Get all column layout type columns
            IEnumerable<CustomColumn> allColumns = this.LayoutFactory.GetFullCustomColumnList();
            IColumnLayoutFactory factory = this.LayoutFactory;
            Dictionary<String, ObjectFieldInfo> masterFieldLookup = _fieldsByPlaceholder;

            //Enumerate through all and add missing
            foreach (CustomColumn customColumn in allColumns)
            {
                //If not already visible
                if (!visibleColumns.Contains(customColumn.Path))
                {
                    ObjectFieldInfo fieldInfo;
                    if (masterFieldLookup.TryGetValue(customColumn.Path, out fieldInfo))
                    {
                        //Add to the collection
                        nonVisibleColumns.Add(new CustomColumnView(customColumn, fieldInfo.FieldFriendlyName));
                    }
                }
            }

            return nonVisibleColumns;
        }

        /// <summary>
        /// Returns the path for the layout directory for the current layout type.
        /// </summary>
        /// <returns></returns>
        private String GetLayoutTypePath()
        {
            String layoutTypePath =
                Path.Combine(_columnLayoutsFolderPath,
                this.LayoutFactory.LayoutType.ToString());

            try
            {
                if (!Directory.Exists(layoutTypePath)) Directory.CreateDirectory(layoutTypePath);
            }
            catch
            {
                return CCMClient.ViewState.GetSessionDirectory(SessionDirectory.CustomColumnLayout);
            }

            return layoutTypePath;
        }

        /// <summary>
        ///     Gets the collection of available <see cref="CustomColumnLayoutInfo" />.
        /// </summary>
        private IEnumerable<CustomColumnLayoutInfo> GetAvailableColumnLayoutInfos()
        {
            CustomColumnLayoutType layoutType = this.LayoutFactory.LayoutType;

            // Refresh the availableColumnLayoutInfos to account for changes in other parts of the application.
            CustomColumnLayoutInfoListViewModel infosView = new CustomColumnLayoutInfoListViewModel(GetLayoutTypePath());
            infosView.FetchAllByType(layoutType);
            return infosView.Model;
        }

        private String AddPathMask(String propertyName)
        {
            return String.IsNullOrEmpty(this.PathMask) ? propertyName : String.Format(this.PathMask, propertyName);
        }

        /// <summary>
        /// Removes mask from path
        /// </summary>
        /// <param name="maskedPath"></param>
        /// <returns></returns>
        private String RemovePathMask(String maskedPath)
        {
            if (String.IsNullOrWhiteSpace(maskedPath) || String.IsNullOrWhiteSpace(this.PathMask))
                return maskedPath;

            String replace = this.PathMask.Replace("{0}", String.Empty);
            if(String.IsNullOrWhiteSpace(replace)) return maskedPath;

            return maskedPath.Replace(replace, String.Empty);


            //var removePathMask = String.IsNullOrEmpty(PathMask) ?
            //    maskedPath :
            //    maskedPath.Substring(maskedPath.LastIndexOf('.') + 1);
            //return removePathMask;
        }

        /// <summary>
        ///     Registers a particular type to a field so that the datatemplate associated with that type can be obtained later.
        /// </summary>
        /// <typeparam name="T">Type of data that has been registered to a particular data template creator.</typeparam>
        /// <param name="field">The field to which the type is registered.</param>
        public void RegisterDataTemplateField<T>(ObjectFieldInfo field)
        {
            if (!_dataTemplates.ContainsKey(typeof(T)))
            {
                throw new ArgumentException("The Type provided has no data template registered.");
            }

            _dataTemplateFields[field.PropertyName] = typeof(T);
        }

        /// <summary>
        ///     Gets the data template creator, if any, registered to be used by a given <paramref name="field"/>.
        /// </summary>
        /// <param name="field">The field for which to retrieve a data template creator.</param>
        /// <returns>A function registered to be used with any field of the type that the given <paramref name="field"/> is registered to.</returns>
        private Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate> GetDataTemplate(ObjectFieldInfo field)
        {
            // Try to obtain the type this field is registered to, if there is no registry for it, then return null.
            Type type;
            if (!this._dataTemplateFields.TryGetValue(field.PropertyName, out type))
            {
                return null;
            }

            // Try to obtain the registered data template creator for the type registered to the given field.
            Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate> dataTemplate;
            this._dataTemplates.TryGetValue(type, out dataTemplate);

            return dataTemplate;
        }

        /// <summary>
        ///     Registers a data template creator function to be used by any fields registerd to the given type.
        /// </summary>
        /// <typeparam name="T">The type to be associated with the data template creator.</typeparam>
        /// <param name="dataTemplateCreator">The function that will be used when generating columns for any field associated with the given type.</param>
        public void RegisterDataTemplate<T>(Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate> dataTemplateCreator)
        {
            _dataTemplates[typeof(T)] = dataTemplateCreator;
            var keysToRemove =
                _dataTemplateFields.Where(pair => pair.Value == typeof(T)).Select(pair => pair.Key).ToList();
            foreach (var key in keysToRemove)
            {
                _dataTemplateFields.Remove(key);
            }
        }

        /// <summary>
        /// Creates a new datagrid column for the given custom column model.
        /// </summary>
        private DataGridColumn NewDataGridColumn(CustomColumn column)
        {
            ObjectFieldInfo field;
            if (_fieldsByPlaceholder.TryGetValue(column.Path, out field))
            {

                // V8-27022 : Applied the mask to the custom column Path (based on the property name), if there is one.
                column.Path = AddPathMask(field.PropertyName);

                return ToDataGridColumn(column, this.LayoutFactory, this.LayoutFactory.RowSourceType, field, _displayUomCollection,
                    GetDataTemplate(field));
            }


            //otherwise return a calcuated column
            return ToCalculatedDataGridColumn(column, column.Path, Message.ColumnLayoutEditor_CalculatedColumnHeader, this.LayoutFactory);
        }

        /// <summary>
        /// Creates a new datagrid column for the given custom column view
        /// </summary>
        private DataGridColumn NewDataGridColumn(CustomColumnView columnView)
        {
            ObjectFieldInfo field;
            if (!_fieldsByPlaceholder.TryGetValue(columnView.Path, out field)) return null;

            // V8-27022 : Applied the mask to the custom column Path (based on the property name), if there is one.
            columnView.Path = AddPathMask(field.PropertyName);

            DataGridColumn newDataGridColumn = ToDataGridColumn(columnView.Column, this.LayoutFactory, this.LayoutFactory.RowSourceType, field,
                _displayUomCollection, GetDataTemplate(field));

            if (newDataGridColumn != null) newDataGridColumn.Visibility = Visibility.Collapsed;
            return newDataGridColumn;
        }

        /// <summary>
        /// Reloads the current layout into the grid.
        /// </summary>
        public void RefreshGrid(Boolean keepExisting)
        {
            if (this.DataGrid == null) return;

            if (keepExisting)
            {
                UpdateColumnLayoutFromGrid();
            }

            UpdateGridFromColumnLayout();
        }

        /// <summary>
        /// Clears all layout settings from the
        /// atatched datagrid.
        /// </summary>
        public void ClearGrid()
        {
            ExtendedDataGrid grid = this.DataGrid;
            if (grid == null) return;

            //clear the grid completely
            grid.DisplayColumnTotals = false;
            grid.ClearFilterValues();

            //remove groups first otherwise if we have any virtualised columns
            //the grid could error in an unrecoverable way.
            if (grid.GroupByDescriptions.Any()) grid.GroupByDescriptions.Clear();
            if (grid.SortByDescriptions.Any()) grid.SortByDescriptions.Clear();
            if (grid.Columns.Any()) grid.Columns.Clear();
        }

        #region Column Helpers

        /// <summary>
        ///     Creates a column using the provided parameters. If a Data Template Creator function is passed it will override normal column creation.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="type"></param>
        /// <param name="field"></param>
        /// <param name="uomCollection"></param>
        /// <param name="dataTemplateCreator"></param>
        /// <returns></returns>
        private static DataGridColumn ToDataGridColumn(CustomColumn source, IColumnLayoutFactory columnFactory, Type rowItemType,
            ObjectFieldInfo field, DisplayUnitOfMeasureCollection uomCollection, Func<ObjectFieldInfo, DisplayUnitOfMeasureCollection, DataTemplate> dataTemplateCreator)
        {
            if (dataTemplateCreator == null)
            {
                return ToDataGridColumn(source, columnFactory, rowItemType, field, uomCollection);
            }
            else
            {
                var dataTemplate = dataTemplateCreator.Invoke(field, uomCollection);
                //Create basic column
                String groupName = columnFactory.GetHeaderGroupName(field);
                String header = (!String.IsNullOrEmpty(source.DisplayName)) ? source.DisplayName : columnFactory.GetDefaultHeaderName(field);

                DataGridExtendedTemplateColumn column = CommonHelper.CreateReadOnlyColumn(dataTemplate, source.Path ?? field.PropertyName, header, groupName);
                column.SelectedTotalType = (ExtendedDataGridColumnTotalType)source.TotalType;
                if (source.Width > 0) column.Width = source.Width;
                return column;
            }
        }

        /// <summary>
        ///     Determines the type of column to create and initializes it as indicated in the <paramref name="source" />.
        /// </summary>
        /// <param name="source"><see cref="CustomColumn" /> containing the definition of the column.</param>
        /// <returns>A <see cref="DataGridColumn" /> with the correct underlying type for the column.</returns>
        private static DataGridColumn ToDataGridColumn(CustomColumn source, IColumnLayoutFactory columnFactory, Type rowItemType, ObjectFieldInfo field, DisplayUnitOfMeasureCollection uomCollection)
        {
            //IColumnLayoutFactory columnFactory = GetFactory(type);
            Boolean isReadonly = columnFactory.IsPropertyReadOnly(field, rowItemType);
            String columnGroupName = columnFactory.GetHeaderGroupName(field);

            //Get the header string.
            String columnHeader = (!String.IsNullOrEmpty(source.DisplayName)) ? source.DisplayName : columnFactory.GetDefaultHeaderName(field);

            //Create the column
            DataGridColumn column;
            if (isReadonly)
            {
                column = CommonHelper.CreateReadOnlyColumn(source.Path ?? field.PropertyName, field.PropertyType,
                    columnHeader, field.PropertyDisplayType, uomCollection, columnGroupName);
            }
            else
            {
                column = CommonHelper.CreateEditableColumn(source.Path ?? field.PropertyName, field.PropertyType,
                    columnHeader, field.PropertyDisplayType, uomCollection, columnGroupName);
            }

            //width
            if (source.Width > 0) column.Width = source.Width;

            IGalleriaDataGridColumn gCol = column as IGalleriaDataGridColumn;
            if (gCol != null)
            {
                //totals type.
                gCol.SelectedTotalType = (ExtendedDataGridColumnTotalType)source.TotalType;
            }


            return column;
        }

        /// <summary>
        /// Creates a new data grid column for the calculated custom column.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="columnFactory"></param>
        /// <param name="rowItemType"></param>
        /// <param name="expressionText"></param>
        /// <param name="columnGroupName"></param>
        /// <param name="uomValues"></param>
        /// <returns></returns>
        private DataGridColumn ToCalculatedDataGridColumn(CustomColumn source, String expressionText, String columnGroupName, IColumnLayoutFactory columnFactory)
        {
            DataGridColumn column = null;

            //try to determine the property type from the expression and rowitemtype.
            CommonHelper.PropertyType propertyType =
                CommonHelper.GetBasicPropertyType(ObjectFieldExpression.GetExpectedValueType(expressionText, columnFactory.GetModelObjectFields()));

            Int32 expressionKey = RegisterExpressionText(expressionText);
            String colPath = CreateCalculatedColumnPath(expressionKey);
            Binding colBinding = new Binding(colPath);

            switch (propertyType)
            {
                default:
                case CommonHelper.PropertyType.DateTime:
                case CommonHelper.PropertyType.String:
                case CommonHelper.PropertyType.Enum:
                    column = new DataGridExtendedTextColumn() { Binding = colBinding, };
                    break;

                case CommonHelper.PropertyType.Boolean:
                    column = new DataGridExtendedCheckBoxColumn() { Binding = colBinding, };
                    break;

                case CommonHelper.PropertyType.Integer:
                    column = new DataGridExtendedNumericColumn()
                    {
                        Binding = colBinding,
                        TextInputType = Framework.Controls.Wpf.InputType.Integer,
                    };
                    break;

                case CommonHelper.PropertyType.Decimal:
                    column = new DataGridExtendedNumericColumn()
                    {
                        Binding = colBinding,
                        TextInputType = Framework.Controls.Wpf.InputType.Decimal,
                    };
                    break;
            }

            column.Header = source.DisplayName;
            column.ClipboardContentBinding = colBinding;
            column.IsReadOnly = true;
            column.SortMemberPath = colPath;


            //width
            if (source.Width > 0) column.Width = source.Width;

            IGalleriaDataGridColumn gCol = column as IGalleriaDataGridColumn;
            if (gCol != null)
            {
                //totals type.
                gCol.SelectedTotalType = (ExtendedDataGridColumnTotalType)source.TotalType;

                //group name
                gCol.ColumnGroupName = columnGroupName;
            }

            return column;

        }

        /// <summary>
        /// Map the source datagrid column to a custom one.
        /// </summary>
        /// <param name="source">the source column to extract the binding from</param>
        /// <param name="pathMask">the path mask in use.</param>
        /// <returns>a new custom column</returns>
        private static CustomColumn MapToCustomColumn(DataGridColumn source, String pathMask)
        {
            //don't support template columns atm..we could do though so need
            // to check why this was the case.
            if (source is DataGridTemplateColumn) return null;

            String colBindingPath = ExtendedDataGrid.GetBindingProperty(source);
            if (String.IsNullOrWhiteSpace(colBindingPath)) return null;

            //remove the path mask
            if (!String.IsNullOrWhiteSpace(colBindingPath) && !String.IsNullOrWhiteSpace(pathMask)
                && pathMask != "{0}")
            {
                colBindingPath = colBindingPath.Replace(pathMask.Replace("{0}", String.Empty), String.Empty);
            }

            Int32 number = source.DisplayIndex + 1;

            CustomColumn column = CustomColumn.NewCustomColumn(colBindingPath, number);
            column.DisplayName = source.Header as String;
            column.Width = (!source.Width.IsAuto) ? Convert.ToInt32(source.Width.Value) : 0;

            IGalleriaDataGridColumn galCol = source as IGalleriaDataGridColumn;
            column.TotalType = (CustomColumnTotalType)((galCol != null) ? galCol.SelectedTotalType : ExtendedDataGridColumnTotalType.None);


            return column;
        }

        #endregion

        #endregion

        #region IDisposable Support

        private Boolean _disposed;

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, 
        ///     releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                this.DataGrid = null;
                _fieldsByPlaceholder.Clear();
                _fieldsByPropertyName.Clear();
                _dataTemplates.Clear();
                _dataTemplateFields.Clear();
            }

            _disposed = true;
        }

        #endregion

        #region Calculated Expression Registering

        private static Object _expressionRegisterLock = new object();
        private static Int32 _nextIdentity = 0;
        private static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }
        private static readonly Dictionary<Int32, String> _idToExpressionText = new Dictionary<Int32, String>();

        /// <summary>
        /// Registers the given expression text and returns its key.
        /// </summary>
        public static Int32 RegisterExpressionText(String expressionText)
        {
            lock (_expressionRegisterLock)
            {
                Int32? existingKey = GetRegisteredExpressionKey(expressionText);
                if (existingKey.HasValue) return existingKey.Value;

                Int32 key = GetNextId();
                _idToExpressionText[key] = expressionText;
                return key;
            }
        }

        /// <summary>
        /// Returns the key currently registered for the given expression text.
        /// </summary>
        /// <param name="expressionText"></param>
        /// <returns></returns>
        public static Int32? GetRegisteredExpressionKey(String expressionText)
        {
            foreach (var entry in _idToExpressionText)
            {
                if (entry.Value == expressionText) return entry.Key;
            }
            return null;
        }

        /// <summary>
        /// Returns the calculated column expression text registered for the given key.
        /// </summary>
        public static String GetExpressionTextByKey(Int32 key)
        {
            String entry;
            if (_idToExpressionText.TryGetValue(key, out entry)) return entry;
            return null;
        }

        /// <summary>
        /// Returns the expression key contained within the given column path.
        /// </summary>
        private Int32 GetCalculatedExpressionKeyFromPath(String colPath)
        {
            if (colPath.StartsWith(this.CalculatedColumnPath))
            {
                String keyString =
                    colPath.Substring(this.CalculatedColumnPath.Length + 1,
                    colPath.Length - this.CalculatedColumnPath.Length - 2);

                Int32 key;
                if (Int32.TryParse(keyString, out key)) return key;
            }

            return 0;
        }

        /// <summary>
        /// Returns the formatted calculated column path.
        /// </summary>
        private String CreateCalculatedColumnPath(Int32 key)
        {
            return this.CalculatedColumnPath + "[" + key.ToString() + "]";
        }

        /// <summary>
        /// Clears out all currently registered keys.
        /// </summary>
        public static void ClearAllExpressionTextKeys()
        {
            _idToExpressionText.Clear();
        }

        #endregion
    }

}