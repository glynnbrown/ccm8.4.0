﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31881 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class ComparisonColumnLayoutFactory : BaseColumnLayoutFactory
    {
        private readonly PlanogramItemType _comparedItemType;

        #region Constructors

        public ComparisonColumnLayoutFactory(Type rowSourceType, PlanogramItemType comparedItemType, Planogram planogramContext = null)
            : base(rowSourceType, CustomColumnLayoutType.PlanogramComparison, planogramContext)
        {
            _comparedItemType = comparedItemType;
            //  Get the field infos.
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>> infos = PlanogramFieldHelper.GetPlanogramObjectFieldInfos(GetPlanogramContext());
            switch (_comparedItemType)
            {
                case PlanogramItemType.Annotation:
                    break;
                case PlanogramItemType.Position:
                    return infos.SelectMany(pair => pair.Value).Select(PrefixInfo);
                case PlanogramItemType.Product:
                    return infos.Where(pair => pair.Key == PlanogramItemType.Planogram || pair.Key == PlanogramItemType.Product).SelectMany(pair => pair.Value).Select(PrefixInfo);
                case PlanogramItemType.SubComponent:
                    break;
                case PlanogramItemType.Component:
                    break;
                case PlanogramItemType.Assembly:
                    break;
                case PlanogramItemType.Fixture:
                    break;
                case PlanogramItemType.Planogram:
                    break;
                case PlanogramItemType.Unknown:
                    break;
                default:
                    Debug.Fail("Unsupported Planogram Item Type when calling ComparisonColumnLayoutFactory.GetModelObjectFields.");
                    break;
            }

            return new List<ObjectFieldInfo>();
        }

        private static ObjectFieldInfo PrefixInfo(ObjectFieldInfo field)
        {
            field.GroupName = field.OwnerFriendlyName;
            field.PropertyName = String.Format("{0}.{1}", field.OwnerType.Name, field.PropertyName);
            return field;
        }

        #endregion
    }
}
