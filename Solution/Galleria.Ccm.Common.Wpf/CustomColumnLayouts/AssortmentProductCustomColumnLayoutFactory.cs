﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: CCM830
// V8-18515 : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    /// Column factory used for Masterdata assortment product rows.
    /// </summary>
    public sealed class AssortmentProductCustomColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Fields
        private readonly List<String> _excludedProperties = new List<String>();
        const String productPrefix = "Product.";
        const String assortmentProductPrefix = "AssortmentProduct.";
        #endregion

        #region Properties

        public List<String> ExcludedProperties
        {
            get { return _excludedProperties; }
        }

        #endregion

        #region Contructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rowSourceType"></param>
        public AssortmentProductCustomColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rowSourceType"></param>
        /// <param name="specificExcludedProperties"></param>
        /// <param name="visibleColumns"></param>
        public AssortmentProductCustomColumnLayoutFactory(Type rowSourceType, List<IModelPropertyInfo> specificExcludedProperties,  List<String> visibleColumns = null)
            : base(rowSourceType, CustomColumnLayoutType.AssortmentProductMasterData, null)
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //If any other specific properties need excluding, do so here
            if (specificExcludedProperties != null)
            {
                foreach (IModelPropertyInfo property in specificExcludedProperties)
                {
                    if (!_excludedProperties.Contains(property.Name))
                    {
                        _excludedProperties.Add(property.Name);
                    }
                }
            }

            base.RegisterVisibleFields(visibleColumns != null ?
                visibleColumns :
                new List<String>
                {
                    productPrefix + Product.GtinProperty.Name,
                    productPrefix + Product.NameProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.IsRangedProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.RankProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.SegmentationProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.FacingsProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.UnitsProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.ProductTreatmentTypeProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.ProductLocalizationTypeProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.IsPrimaryRegionalProductProperty.Name,
                        assortmentProductPrefix + AssortmentProduct.CommentsProperty.Name
                });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            Planogram plan = this.GetPlanogramContext();

            //assortment product fields
            foreach (ObjectFieldInfo field in AssortmentProduct.EnumerateDisplayableFieldInfos())
            {
                field.PropertyName = assortmentProductPrefix + field.PropertyName;
                if (_excludedProperties.Contains(field.PropertyName)) continue;
                
                yield return field;
            }

            //product fields
            foreach (ObjectFieldInfo field in Product.EnumerateDisplayableFieldInfos())
            {
                field.PropertyName = productPrefix + field.PropertyName;
                if (_excludedProperties.Contains(field.PropertyName)) continue;
                yield return field;
            }

        }

        /// <summary>
        /// Returns true if the property should be displayed as readonly.
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <param name="sourceRowType"></param>
        /// <returns></returns>
        public override Boolean IsPropertyReadOnly(ObjectFieldInfo fieldInfo, Type sourceRowType)
        {
            //don't allow edit of the gtin or name, but everthing else is ok.
            if (fieldInfo.PropertyName == Product.NameProperty.Name
                || fieldInfo.PropertyName == Product.GtinProperty.Name)
            {
                return true;
            }

            return base.IsPropertyReadOnly(fieldInfo, sourceRowType);
        }

        /// <summary>
        /// Returns the default column header name for the field.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }

    /// <summary>
    /// Wrapper for data to represent a single assortment product row
    /// </summary>
    public sealed class AssortmentProductRow : INotifyPropertyChanged
    {
        #region Fields
        private readonly AssortmentProduct _assortmentProduct;
        private readonly Product _product;
        #endregion

        #region Binding Property Paths

        public static PropertyPath AssortmentProductProperty { get { return new PropertyPath("AssortmentProduct"); } }
        public static PropertyPath ProductProperty { get { return new PropertyPath("Product"); } }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the assortment product 
        /// </summary>
        public AssortmentProduct AssortmentProduct
        {
            get { return _assortmentProduct; }
        }

        /// <summary>
        /// Returns the masterdata product
        /// </summary>
        public Product Product
        {
            get { return _product; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortmentProduct"></param>
        /// <param name="product"></param>
        public AssortmentProductRow(AssortmentProduct assortmentProduct, Product product)
        {
            _assortmentProduct = assortmentProduct;
            _product = product;
        }

        #endregion

        #region INotifyPropertyChanged

        //implemented so that wpf does not create dependency property descriptors.

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
