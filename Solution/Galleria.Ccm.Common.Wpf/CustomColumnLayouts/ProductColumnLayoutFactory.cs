﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26351 : A.Silva 
//  Created.
// V8-26372 : A.Silva 
//  Added missing columns to Planogram type, and created WorkPacakge type definitions.
// V8-26369 : A.Silva 
//  All fields in product list made writable by default.
// V8-26408 : L.Ineson
//  Split out this class into its own file and added PlanogramComponent type.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27022 : A.Silva 
//  Added Default Visible Columns for PlanogramFixture Components.
// V8-27160 : L.Ineson
//  Removed workpackage layout type.
// Split out remaining types into separate factories which contain all methods
// needed for that type.
// V8-27504 : A.Silva
//      Amended uses of CustomColumn.NewCustomColumn() factory method to account for the new HeaderGroupNumber property.
//      Added BaseColumnLayoutFactory<T> and moved the factory sub types to inherit from it.
// V8-27898 : L.Ineson
//  Corrected check methods to take field info not just property path.
//  Updated product column layout factory to use new EnumerateDisplayableFieldInfos method.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-27442 : L.Ineson
// Split out implementations into separate files.
#endregion

#region Version History: (CCM 8.0)
// V8-27636 : L.Ineson
//  Amended to use viewmodel base returned field infos.
#endregion

#region Version History: (CCM 8.1.0)
// V8-29681 : A.Probyn
// Updated to use new GetPlanogramContext
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for <see cref="PlanogramProduct"/>.
    /// </summary>
    public sealed class ProductColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constructors

        public ProductColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        { }

        public ProductColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            :base(rowSourceType, CustomColumnLayoutType.Product, planogramContext)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(
                new List<String>
            {
                PlanogramProduct.GtinProperty.Name,
                PlanogramProduct.NameProperty.Name,
                PlanogramProduct.SqueezeHeightProperty.Name,
                PlanogramProduct.SqueezeWidthProperty.Name,
                PlanogramProduct.SqueezeDepthProperty.Name,
                PlanogramProduct.TrayHighProperty.Name,
                PlanogramProduct.TrayWideProperty.Name,
                PlanogramProduct.TrayDeepProperty.Name,
                PlanogramProduct.OrientationTypeProperty.Name,
                PlanogramProduct.MerchandisingStyleProperty.Name,
                //Add any new properties that should be visible on load here.
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            //just return all fields for the product viewmodel
            foreach (ObjectFieldInfo fieldInfo in PlanogramProductViewModelBase.EnumerateDisplayableFields(base.GetPlanogramContext()))
            {
                yield return fieldInfo;
            }
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion

    }
}
