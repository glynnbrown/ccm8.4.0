﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2)
// V8-30991 : L.Ineson
// Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///  Column layout factory used by the fixture list view when displaying
    ///  PlanogramFixtureView rows.
    /// </summary>
    public sealed class PlanogramFixtureColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constants
        const String backboardPrefix = "BackboardComponent.";
        const String basePrefix = "BaseComponent.";
        const String subcomponentsPrefix = "MergedSubComponentView.";
        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramFixtureColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            :base(rowSourceType, CustomColumnLayoutType.PlanogramFixture, planogramContext)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //Add a list of properties visible by default.
            base.RegisterVisibleFields(
                new List<String>
            {
               PlanogramFixture.NameProperty.Name,
                PlanogramFixtureItem.BaySequenceNumberProperty.Name,
                PlanogramFixture.HeightProperty.Name,
                PlanogramFixture.WidthProperty.Name,
                PlanogramFixture.DepthProperty.Name,
                PlanogramFixtureItem.XProperty.Name,
                PlanogramFixtureItem.YProperty.Name,
                PlanogramFixtureItem.ZProperty.Name,
                PlanogramFixtureItem.AngleProperty.Name,
            });
        }


        #endregion

        #region Methods

        /// <summary>
        /// Returns all field infos available for this layout.
        /// </summary>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            //Fixture
            String groupName = PlanogramFixture.FriendlyName;
            foreach (var fieldInfo in PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                fieldInfo.GroupName = groupName;
                yield return fieldInfo;
            }
            foreach (var fieldInfo in PlanogramFixture.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                fieldInfo.GroupName = groupName;
                yield return fieldInfo;
            }

            //add component properties related to backboard:
            groupName = Message.PlanogramFixtureColumnLayoutFactory_Backboard;

            //HasBackboard
            yield return ObjectFieldInfo.NewObjectFieldInfo(
                typeof(PlanogramFixture), PlanogramFixture.FriendlyName,
                "HasBackboard", Message.PlanogramFixtureColumnLayoutFactory_HasBackboard,
                typeof(Boolean), Framework.Model.ModelPropertyDisplayType.None,
                true, groupName);

            IModelPropertyInfo[] properties = new IModelPropertyInfo[]
            {
                PlanogramComponent.HeightProperty,
                PlanogramComponent.WidthProperty,
                PlanogramComponent.DepthProperty,
            };
            foreach (IModelPropertyInfo property in properties)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(
                   typeof(PlanogramFixture), groupName,
                   backboardPrefix + property.Name,
                   property.FriendlyName,
                   property.Type,
                   property.DisplayType,
                   true, groupName);
            }

            properties = new IModelPropertyInfo[]
            {
                PlanogramSubComponent.IsNotchPlacedOnFrontProperty,
                PlanogramSubComponent.NotchStyleTypeProperty,
                PlanogramSubComponent.NotchHeightProperty,
                PlanogramSubComponent.NotchWidthProperty,
                PlanogramSubComponent.NotchStartXProperty,
                PlanogramSubComponent.NotchSpacingXProperty,
                PlanogramSubComponent.NotchStartYProperty,
                PlanogramSubComponent.NotchSpacingYProperty,
            };
            foreach (IModelPropertyInfo property in properties)
            {
                ObjectFieldInfo field = ObjectFieldInfo.NewObjectFieldInfo(
                   typeof(PlanogramFixture), groupName,
                   backboardPrefix + property.Name,
                   property.FriendlyName,
                   property.Type,
                   property.DisplayType,
                   true, groupName);

                field.PropertyName = field.PropertyName.Replace(backboardPrefix, backboardPrefix + subcomponentsPrefix);
                yield return field;
            }


            //HasBase
            groupName = Message.PlanogramFixtureColumnLayoutFactory_Base;

            yield return ObjectFieldInfo.NewObjectFieldInfo(
               typeof(PlanogramFixture), PlanogramFixture.FriendlyName,
               "HasBase", Message.PlanogramFixtureColumnLayoutFactory_HasBase,
               typeof(Boolean), Framework.Model.ModelPropertyDisplayType.None,
               true, groupName);


            properties = new IModelPropertyInfo[]
            {
                PlanogramComponent.HeightProperty,
                PlanogramComponent.WidthProperty,
                PlanogramComponent.DepthProperty,
            };
            foreach (IModelPropertyInfo property in properties)
            {
                yield return ObjectFieldInfo.NewObjectFieldInfo(
                   typeof(PlanogramFixture), groupName,
                   basePrefix + property.Name,
                   property.FriendlyName,
                   property.Type,
                   property.DisplayType,
                   true, groupName);
            }
        }

        #endregion

    }
}
