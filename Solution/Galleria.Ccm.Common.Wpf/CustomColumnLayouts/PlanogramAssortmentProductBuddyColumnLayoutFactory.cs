﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// CCM-14014 : M.Pettit
//  Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class PlanogramAssortmentProductBuddyColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Fields
        private readonly List<String> _excludedProperties = new List<String>();
        private Type _rowSourceType;
        const String productPrefix = "Product.";
        const String _assortmentProductPrefix = "AssortmentProduct.";
        #endregion

        #region Properties
 
        public List<String> ExcludedProperties
        {
            get { return _excludedProperties; }
        }

        #endregion

        #region Constructor

        public PlanogramAssortmentProductBuddyColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        {
        }

        public PlanogramAssortmentProductBuddyColumnLayoutFactory(Type rowSourceType, List<IModelPropertyInfo> specificExcludedProperties)
            : base(rowSourceType, CustomColumnLayoutType.PlanogramAssortmentProduct)
        {
            _rowSourceType = rowSourceType;

            //get the field infos
            List<ObjectFieldInfo> infos = new List<ObjectFieldInfo>();
            infos = GetModelObjectFields().ToList();

            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //If any other specific properties need excluding, do so here
            if (specificExcludedProperties != null)
            {
                foreach (IModelPropertyInfo property in specificExcludedProperties)
                {
                    if (!_excludedProperties.Contains(property.Name))
                    {
                        _excludedProperties.Add(property.Name);
                    }
                }
            }

            base.RegisterVisibleFields(new List<String>
            {
                //no default visible fields, those are set by the assortment rules window this is used by
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            foreach (ObjectFieldInfo field in PlanogramAssortmentProduct.EnumerateDisplayableFieldInfos())
            {
                //if property is excluded then ignore it.
                if (_excludedProperties.Contains(field.PropertyName)) continue;

                field.PropertyName = "TargetProduct." + field.PropertyName;
                yield return field;
            }

            //ProductFields
            String groupName = Product.FriendlyName;
            foreach (var field in PlanogramProduct.EnumerateDisplayableFieldInfos(false,false,false,null,false))
            {
                field.PropertyName = productPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
        }

        public override bool IsPropertyReadOnly(Galleria.Framework.ViewModel.ObjectFieldInfo fieldInfo, System.Type sourceRowType)
        {
            //don't allow edit of the gtin or name, but everthing else is ok.
            if (fieldInfo.PropertyName == PlanogramProduct.NameProperty.Name
                || fieldInfo.PropertyName == PlanogramProduct.GtinProperty.Name)
            {
                return true;
            }

            return base.IsPropertyReadOnly(fieldInfo, sourceRowType);
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }
}
