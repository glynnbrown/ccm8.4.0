﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32718 : A.Probyn
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class AssortmentLocationCustomColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Fields
        private readonly List<String> _excludedProperties = new List<String>();
        #endregion

        #region Properties

        public List<String> ExcludedProperties
        {
            get { return _excludedProperties; }
        }

        #endregion

        #region Contructors

        public AssortmentLocationCustomColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        {
        }

        public AssortmentLocationCustomColumnLayoutFactory(Type rowSourceType, List<IModelPropertyInfo> specificExcludedProperties)
            : base(rowSourceType, CustomColumnLayoutType.AssortmentLocation)
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //If any other specific properties need excluding, do so here
            if (specificExcludedProperties != null)
            {
                foreach (IModelPropertyInfo property in specificExcludedProperties)
                {
                    if (!_excludedProperties.Contains(property.Name))
                    {
                        _excludedProperties.Add(property.Name);
                    }
                }
            }

            base.RegisterVisibleFields(new List<String>
            {
                //no default visible fields, those are set by the assortment rules window this is used by
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            Planogram plan = this.GetPlanogramContext();

            //just return all of the Location fields, assortment ones will be included.
            foreach (ObjectFieldInfo field in Location.EnumerateDisplayableFieldInfos())
            {
                //if property is excluded then ignore it.
                if (_excludedProperties.Contains(field.PropertyName)) continue;

                yield return field;
            }
        }

        public override bool IsPropertyReadOnly(Galleria.Framework.ViewModel.ObjectFieldInfo fieldInfo, System.Type sourceRowType)
        {
            //don't allow edit of the gtin or name, but everthing else is ok.
            if (fieldInfo.PropertyName == Location.NameProperty.Name
                || fieldInfo.PropertyName == Location.CodeProperty.Name
                || fieldInfo.PropertyName.StartsWith("Meta")
                || fieldInfo.PropertyName.Contains(".Meta"))
            {
                return true;
            }

            return base.IsPropertyReadOnly(fieldInfo, sourceRowType);
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }
}
