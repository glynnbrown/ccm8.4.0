﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26351 : A.Silva 
//  Created.
// V8-26372 : A.Silva 
//  Added missing columns to Planogram type, and created WorkPacakge type definitions.
// V8-26369 : A.Silva 
//  All fields in product list made writable by default.
// V8-26408 : L.Ineson
//  Split out this class into its own file and added PlanogramComponent type.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27022 : A.Silva 
//  Added Default Visible Columns for PlanogramFixture Components.
// V8-27160 : L.Ineson
//  Removed workpackage layout type.
// Split out remaining types into separate factories which contain all methods
// needed for that type.
// V8-27504 : A.Silva
//      Amended uses of CustomColumn.NewCustomColumn() factory method to account for the new HeaderGroupNumber property.
//      Added BaseColumnLayoutFactory<T> and moved the factory sub types to inherit from it.
// V8-27898 : L.Ineson
//  Corrected check methods to take field info not just property path.
//  Updated product column layout factory to use new EnumerateDisplayableFieldInfos method.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-27442 : L.Ineson
// Split out implementations into separate files.
#endregion

#region Version History: (CCM 8.1.0)
// V8-29681 : A.Probyn
// Implemented new RegisterPlanogram
#endregion

#region Version History: (CCM 8.2)
// V8-30991 : L.Ineson
//  NewCustomColumnList(defaultOnly = true) now outputs based on the visible fields order.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Reflection;


namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column layout factory base.
    /// </summary>
    public abstract class BaseColumnLayoutFactory : IColumnLayoutFactory
    {
        #region Fields
        private readonly Dictionary<String, String> _headerGroupNameByFieldPlaceholder = new Dictionary<String, String>();
        private readonly Dictionary<String, Int32> _orderNumberByHeaderGroupName = new Dictionary<String, Int32>();
        private readonly List<String> _visibleFields = new List<String>();
        private Planogram _planogramContext = null;
        #endregion

        #region Properties

        public Type RowSourceType { get; private set; }
        public CustomColumnLayoutType LayoutType { get; private set; }

        #endregion

        #region Constructors

        public BaseColumnLayoutFactory(Type rowSourceType, CustomColumnLayoutType layoutType)
            : this(rowSourceType, layoutType, null)
        {
        }

        public BaseColumnLayoutFactory(Type rowSourceType, CustomColumnLayoutType layoutType, Planogram planogramContext)
        {
            this.RowSourceType = rowSourceType;
            this.LayoutType = layoutType;
            if (planogramContext != null) RegisterPlanogram(planogramContext);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="defaultOnly"></param>
        /// <returns></returns>
        public CustomColumnList NewCustomColumnList(Boolean defaultOnly)
        {
            List<ObjectFieldInfo> fields = new List<ObjectFieldInfo>();
            if (defaultOnly)
            {
                var lookup = GetModelObjectFields().ToDictionary(f => f.PropertyName);

                //output in visible field order:
                foreach (String visibleField in _visibleFields)
                {
                    ObjectFieldInfo f;
                    if (lookup.TryGetValue(visibleField, out f))
                    {
                        fields.Add(f);
                    }
                }
            }
            else
            {
                fields = GetModelObjectFields().ToList();
            }

            //Return columns
            return CustomColumnList.NewCustomColumnList(MapFieldsToCustomColumns(fields));
        }

        /// <summary>
        /// Returns the header group name of the given field.
        /// </summary>
        public String GetHeaderGroupName(ObjectFieldInfo field)
        {
            String headerGroupName;
            return _headerGroupNameByFieldPlaceholder.TryGetValue(field.FieldPlaceholder, out headerGroupName)
                ? headerGroupName
                : field.GroupName;
        }

        public Planogram GetPlanogramContext()
        {
            return _planogramContext;
        }

        /// <summary>
        ///     Gets whether the property matching the given <paramref name="path"/> is editable or not.
        /// </summary>
        /// <param name="path">Property binding path to match.</param>
        /// <returns>Whether the matching property is editable or not.</returns>;
        public virtual Boolean IsPropertyReadOnly(ObjectFieldInfo fieldInfo, Type sourceRowType)
        {
            //if the property is marked explicity as readonly then take its word for it.
            if (fieldInfo.IsReadOnly.HasValue && fieldInfo.IsReadOnly.Value) return true;
            

            //Cycle down through the path parts to find the property source type.
            String[] propertyParts = fieldInfo.PropertyName.Split('.');

            Type sourceType = sourceRowType;
            System.Reflection.PropertyInfo pInfo = null;
            foreach (String pathPart in propertyParts)
            {
                pInfo = sourceType.GetProperties().FirstOrDefault(p => p.Name == pathPart);
                if (pInfo == null) break;
                sourceType = pInfo.PropertyType;
            }


            //var pInfo = sourceRowType.GetProperty(fieldInfo.PropertyName);
            if (pInfo == null) return true;

            // Allow editing if the property has a public set method.
            return (pInfo.GetSetMethod() == null);
        }

        /// <summary>
        ///     Registers a header group name for future look up of name and order number by path.
        /// </summary>
        /// <param name="headerGroupName">The name of the header group being registered.</param>
        /// <param name="headerGroupNumber">The order number to assign to the header group being registered.</param>
        /// <param name="fields">The property paths for the fields belonging to the header group being registered.</param>
        /// <remarks>Re-registering a header group name will clear any previous registrations for it and add the new ones.</remarks>
        protected void RegisterGroup(String headerGroupName, Int32 headerGroupNumber, IEnumerable<String> fields)
        {
            if (_orderNumberByHeaderGroupName.ContainsKey(headerGroupName))
            {
                foreach (
                    var pathToRemove in
                        _headerGroupNameByFieldPlaceholder.Where(pair => pair.Value == headerGroupName).Select(pair => pair.Key))
                {
                    _headerGroupNameByFieldPlaceholder.Remove(pathToRemove);
                }
                _orderNumberByHeaderGroupName.Remove(headerGroupName);
            }
            foreach (var path in fields)
            {
                _headerGroupNameByFieldPlaceholder.Add(path, headerGroupName);
            }
            _orderNumberByHeaderGroupName.Add(headerGroupName, headerGroupNumber);
        }

        protected void RegisterGroupNames(IEnumerable<String> orderedGroups)
        {
            Int32 headerGroupNumber = 1;
            foreach (String groupName in orderedGroups)
            {
                _orderNumberByHeaderGroupName.Add(groupName, headerGroupNumber);
                headerGroupNumber++;
            }
        }

        /// <summary>
        ///     Registers the visible fields
        /// </summary>
        /// <param name="fields">The property paths for the fields that should be visible by default.</param>
        /// <remarks>Re-registering visible fields will cler the previous ones and replace them with the new ones.</remarks>
        protected void RegisterVisibleFields(IEnumerable<String> fields)
        {
            _visibleFields.Clear();
            _visibleFields.AddRange(fields);
        }

        /// <summary>
        /// Registers the planogram context for related method to make use of in the factory
        /// </summary>
        /// <param name="context"></param>
        public void RegisterPlanogram(Planogram context)
        {
            _planogramContext = context;
        }


        /// <summary>
        ///     Maps a list of <see cref="ObjectFieldInfo"/> instances to their equivalent <see cref="CustomColumn"/> instances.
        /// </summary>
        /// <param name="fields">The list of fields to map.</param>
        /// <param name="headerGroupNumber">The order number for the group these fields belong to.</param>
        /// <returns></returns>
        private static IEnumerable<CustomColumn> MapFieldsToCustomColumns(IEnumerable<ObjectFieldInfo> fields)
        {
            Int32 fieldNumber = 1;
            return fields.Select(field => CustomColumn.NewCustomColumn(field.FieldPlaceholder, fieldNumber++));
        }

        public Dictionary<String, ObjectFieldInfo> GetFieldLookupCache()
        {
            return GetModelObjectFields().ToDictionary(p => p.FieldPlaceholder);
        }

        public IEnumerable<CustomColumn> GetFullCustomColumnList()
        {
            return NewCustomColumnList(/*defaultOnly*/false);
        }


        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public abstract IEnumerable<ObjectFieldInfo> GetModelObjectFields();


        public virtual String GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field == null) return Message.ColumnLayoutEditor_CalculatedColumnHeader;

            return field.FieldFriendlyName;
        }

        #endregion




    }

}