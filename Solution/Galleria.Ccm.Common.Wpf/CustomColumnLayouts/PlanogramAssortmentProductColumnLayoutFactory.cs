﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32718 : A.Probyn
//  Created
// V8-32718 : D.Pleasance
//  Amended so that position attributes can also be selected
// CCM-18480 : D.Pleasance
//  Added optional parameter to constructor to specify which columns should be visible.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    public sealed class PlanogramAssortmentProductColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Fields
        private readonly List<String> _excludedProperties = new List<String>();
        private Boolean _isPlanItemRow = false;
        const String productPrefix = "Product.";
        #endregion

        #region Properties

        public List<String> ExcludedProperties
        {
            get { return _excludedProperties; }
        }

        #endregion

        #region Contructors

        public PlanogramAssortmentProductColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null, null)
        {
        }

        public PlanogramAssortmentProductColumnLayoutFactory(Type rowSourceType, Planogram planogramContext, List<IModelPropertyInfo> specificExcludedProperties, Boolean isPlanItemRow = false, List<String> visibleColumns = null)
            : base(rowSourceType, CustomColumnLayoutType.AssortmentProduct, planogramContext)
        {
            _isPlanItemRow = isPlanItemRow;

            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            //If any other specific properties need excluding, do so here
            if (specificExcludedProperties != null)
            {
                foreach (IModelPropertyInfo property in specificExcludedProperties)
                {
                    if (!_excludedProperties.Contains(property.Name))
                    {
                        _excludedProperties.Add(property.Name);
                    }
                }
            }

            base.RegisterVisibleFields(visibleColumns != null ?
                visibleColumns :
                //no default visible fields, those are set by the assortment rules window this is used by
                new List<String> { });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            Planogram plan = this.GetPlanogramContext();

            if (!_isPlanItemRow)
            {
                //just return all of the planogram product fields, assortment ones will be included.
                foreach (ObjectFieldInfo field in PlanogramProduct.EnumerateDisplayableFieldInfos(
                    /*incMeta*/false, /*incCustom*/true, /*incPerformance*/false,
                    ((plan != null) ? plan.Performance : null), /*incAssortmentData*/false))
                {
                    //if property is excluded then ignore it.
                    if (_excludedProperties.Contains(field.PropertyName)) continue;

                    yield return field;
                }
            }
            else
            {
                PlanogramPerformance performance = (plan != null) ? plan.Performance : null;

                //ProductFields
                String groupName = PlanogramProduct.FriendlyName;
                foreach (var field in PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true, performance, true))
                {
                    field.PropertyName = productPrefix + field.PropertyName;
                    field.GroupName = groupName;
                    yield return field;
                }

                //Position fields
                groupName = PlanogramPosition.FriendlyName;
                foreach (var field in PlanogramPosition.EnumerateDisplayableFieldInfos(/*appliedMerch*/false))
                {
                    field.GroupName = groupName;
                    yield return field;
                }
            }
        }

        public override bool IsPropertyReadOnly(Galleria.Framework.ViewModel.ObjectFieldInfo fieldInfo, System.Type sourceRowType)
        {
            //don't allow edit of the gtin or name, but everthing else is ok.
            if (fieldInfo.PropertyName == PlanogramProduct.NameProperty.Name
                || fieldInfo.PropertyName == PlanogramProduct.GtinProperty.Name
                || fieldInfo.PropertyName.StartsWith("Meta")
                || fieldInfo.PropertyName.Contains(".Meta"))
            {
                return true;
            }

            return base.IsPropertyReadOnly(fieldInfo, sourceRowType);
        }

        public override string GetDefaultHeaderName(ObjectFieldInfo field)
        {
            if (field != null) return field.PropertyFriendlyName;

            return base.GetDefaultHeaderName(field);
        }

        #endregion
    }
}
