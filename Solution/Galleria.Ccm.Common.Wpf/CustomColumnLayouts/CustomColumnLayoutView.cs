#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-25921 : A.Silva ~ Recreated.
// V8-26281 : A.Silva ~ Moved to its own file.

#endregion

#endregion

using System;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     This class represents a view of a <see cref="CustomColumnLayout" /> model.
    /// </summary>
    public sealed class CustomColumnLayoutView : ModelView<CustomColumnLayout>
    {
        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnLayoutView" /> from the provided
        ///     <paramref name="model" />.
        /// </summary>
        /// <param name="model"><see cref="CustomColumnLayout" /> instance from which to create the view.</param>
        public CustomColumnLayoutView(CustomColumnLayout model)
            : base(model)
        {
            model.PropertyChanged += ModelOnPropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Whenever a property in the model changes, notify it, and IsDirty.
        /// </summary>
        private void ModelOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
            OnPropertyChanged("IsDirty");
        }

        #endregion

        #region IDisposable Support

        private Boolean _isDisposed;

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            if (isDisposing)
            {
                if (Model != null) Model.PropertyChanged -= ModelOnPropertyChanged;
            }

            _isDisposed = true;
        }

        #endregion
    }
}