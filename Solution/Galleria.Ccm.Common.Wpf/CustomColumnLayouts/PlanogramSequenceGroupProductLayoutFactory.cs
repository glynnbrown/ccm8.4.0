﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32089 : N.Haywood
// Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for <see cref="PlanogramProduct"/>.
    /// </summary>
    public sealed class PlanogramSequenceGroupProductLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constructors

        public PlanogramSequenceGroupProductLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        { }

        public PlanogramSequenceGroupProductLayoutFactory(Type rowSourceType, Planogram planogramContext)
            : base(rowSourceType, CustomColumnLayoutType.BlockingPlan, planogramContext)
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(
                new List<String>
            {
                PlanogramProduct.GtinProperty.Name,
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            //just return all fields for the product viewmodel
            foreach (ObjectFieldInfo fieldInfo in PlanogramProductViewModelBase.EnumerateDisplayableFields(base.GetPlanogramContext()))
            {
                yield return fieldInfo;
            }
        }

        #endregion

    }
}
