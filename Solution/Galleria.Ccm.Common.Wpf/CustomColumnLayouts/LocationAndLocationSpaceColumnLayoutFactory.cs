﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811
// V8-30357 : M.Brumby
//  Create
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for <see cref="LocationAndLocationSpaceRowViewModel"/>.
    /// </summary>
    public sealed class LocationAndLocationSpaceColumnLayoutFactory : BaseColumnLayoutFactory
    {
        #region Constructors

        public LocationAndLocationSpaceColumnLayoutFactory()
            :this(typeof(LocationAndLocationSpaceRowViewModel))
        {
        }

        public LocationAndLocationSpaceColumnLayoutFactory(Type rowSourceType)
            : base(rowSourceType, CustomColumnLayoutType.LocationAndLocationSpace)
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(new List<String>
            {
                // Add any properties that should be visible on load here.
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.CodeProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.NameProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.StoreSpaceInfoProperty.Path, PlanAssignmentStoreSpaceInfo.BayCountProperty.Name),            
                //String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.StoreSpaceInfoProperty.Path, PlanAssignmentStoreSpaceInfo.BayHeightProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.StoreSpaceInfoProperty.Path, PlanAssignmentStoreSpaceInfo.BayTotalWidthProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.Address1Property.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.RegionProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.CityProperty.Name),
                String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, Location.PostalCodeProperty.Name),
            });        
        }

        #endregion

        #region Methods

        public override IEnumerable<Framework.ViewModel.ObjectFieldInfo> GetModelObjectFields()
        {
            List<ObjectFieldInfo> fieldInfoList = new List<ObjectFieldInfo>();

            Type type = typeof(Location);
            String typeFriendly = Location.FriendlyName;
            ObjectFieldInfo tempFieldInfo;
            
            //Location Code
            tempFieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CodeProperty);
            tempFieldInfo.PropertyName = String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, tempFieldInfo.PropertyName);
            fieldInfoList.Add(tempFieldInfo);

            //Location Name
            tempFieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.NameProperty);
            tempFieldInfo.PropertyName = String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, tempFieldInfo.PropertyName);
            fieldInfoList.Add(tempFieldInfo);

            // PlanAssignmentStoreSpaceInfo
            foreach (ObjectFieldInfo fieldInfo in PlanAssignmentStoreSpaceInfo.EnumerateDisplayableFieldInfos())
            {
                fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                fieldInfo.PropertyName = String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.StoreSpaceInfoProperty.Path, fieldInfo.PropertyName);
                fieldInfoList.Add(fieldInfo);
            }

            // Other Location Properties
            foreach (ObjectFieldInfo fieldInfo in Location.EnumerateDisplayableFieldInfos())
            {
                if (fieldInfo.PropertyName != Location.NameProperty.Name &&
                    fieldInfo.PropertyName != Location.CodeProperty.Name)
                {
                    fieldInfo.GroupName = fieldInfo.OwnerFriendlyName;
                    fieldInfo.PropertyName = String.Format("{0}.{1}", LocationAndLocationSpaceRowViewModel.LocationProperty.Path, fieldInfo.PropertyName);
                    fieldInfoList.Add(fieldInfo);
                }
                
            }
            
            return fieldInfoList;
        }

        #endregion
    }
}
