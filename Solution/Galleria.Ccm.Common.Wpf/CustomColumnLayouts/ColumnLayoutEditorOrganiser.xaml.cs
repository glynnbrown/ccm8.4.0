﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-24700 : A.Silva ~ Created.
// V8-25921 : A.Silva ~ Recreated.
// V8-26076 : A.Silva ~ Corrected how the columns are saved (now all are saved, but each indicates if it is visible or not).
// V8-26170 : A.Silva ~ Ported from Editor.Client.Wpf.
// V8-26172 : A.Silva ~ Minor refactoring due to use of CustomColumnLayoutManager.
// V8-26318 : A.Silva ~ Modified to remove Ribbon and use lower button panel.
// V8-26329 : A.Silva ~ Added ChangesAppliedAction and SaveWhenChangesApplied.
// V8-26472 : A.Probyn
//  Removed obsolete CustomColumn properties and added relevant code to allow lookup
// V8-27504 : A.Silva
//      Amended to allow reordering of Header Groups.
// V8-28058 : A.Silva
//      Tweaked drag and drop so it works both with columns and groups.
// V8-27513 : L.Ineson
//  Made sure grid preview mouse move ignores the event if it came from the scrollbar
#endregion

#region Version History: (CCM 8.1.0)
// V8-29742 : L.Ineson
//  Fixed drag drop
#endregion

#region Version History: (CCM 8.1.1)
// V8-30516 : M.Shelley
//  Added mouse double click behaviour to both the available columns and selected columns grids.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30927 : L.Ineson
//  Amended columns selection.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    /// Dialog to configure a <see cref="CustomColumnLayout" />.
    /// </summary>
    public sealed partial class ColumnLayoutEditorOrganiser
    {
       #region Constants

        /// <summary>
        ///     The key to the Dynamic Resource pointing to the RemoveFilterCommand in the view model.
        /// </summary>
        /// <remarks>This is needed so that the command is properly binded to a button inside a datatemplate.</remarks>
        public const String RemoveFilterCommandKey = "RemoveFilterCommand";

        public const String EditCalculatedColumnCommandKey = "EditCalculatedColumnCommand";

        #endregion

        #region Properties

        public static String HelpFileKey { get; set; }

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ColumnLayoutEditorViewModel), typeof(ColumnLayoutEditorOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel dependency property value changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ColumnLayoutEditorOrganiser senderControl = (ColumnLayoutEditorOrganiser)obj;

            if (e.OldValue != null)
            {
                ColumnLayoutEditorViewModel oldModel = e.OldValue as ColumnLayoutEditorViewModel;
                senderControl.Resources.Remove(RemoveFilterCommandKey);
                senderControl.Resources.Remove(EditCalculatedColumnCommandKey);
                oldModel.UnregisterWindowControl();

                oldModel.ColumnOrderChanged -= senderControl.ViewModel_ColumnOrderChanged;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

                oldModel.MoveColumnDownCommand.Executed -= senderControl.ViewModel_MoveColumnExecuted;
                oldModel.MoveColumnUpCommand.Executed -= senderControl.ViewModel_MoveColumnExecuted;  
            }

            if (e.NewValue != null)
            {
                ColumnLayoutEditorViewModel newModel = e.NewValue as ColumnLayoutEditorViewModel;

                senderControl.Resources.Add(RemoveFilterCommandKey, newModel.RemoveFilterCommand);
                senderControl.Resources.Add(EditCalculatedColumnCommandKey, newModel.EditCalculatedColumnCommand);
                newModel.RegisterWindowControl(senderControl);

                newModel.ColumnOrderChanged += senderControl.ViewModel_ColumnOrderChanged;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                newModel.MoveColumnDownCommand.Executed += senderControl.ViewModel_MoveColumnExecuted;
                newModel.MoveColumnUpCommand.Executed += senderControl.ViewModel_MoveColumnExecuted;  
            }

        }

        /// <summary>
        /// Gets the viewmodel context.
        /// </summary>
        public ColumnLayoutEditorViewModel ViewModel
        {
            get { return (ColumnLayoutEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ColumnLayoutEditorOrganiser(ColumnLayoutEditorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            Galleria.Framework.Controls.Wpf.Help.SetFilename(this, CCMClient.ViewState.HelpFilePath);
            if (!String.IsNullOrEmpty(HelpFileKey)) Galleria.Framework.Controls.Wpf.Help.SetKeyword(this, HelpFileKey);

            this.ViewModel = viewModel;

            this.AssignedColumnsGrid.DragScope = (FrameworkElement)this.Content;

            this.Loaded += ColumnLayoutEditorOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void ColumnLayoutEditorOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ColumnLayoutEditorOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ColumnLayoutEditorViewModel.SelectedFieldGroupProperty.Path)
            {
                if (!String.IsNullOrWhiteSpace(this.FieldGrid.PrefilterText))
                {
                    //force the grid to refilter.
                    this.FieldGrid.BeginFilter();
                }
            }
        }

        /// <summary>
        /// Called whenever the viewmodel column order changed event is fired.
        /// </summary>
        private void ViewModel_ColumnOrderChanged(object sender, EventArgs e)
        {
            if (this.AssignedColumnsGrid == null) return;

            //forcibly reapply the column sorts.
            try
            {
                SortDescription[] sorts = this.AssignedColumnsGrid.SortByDescriptions.ToArray();
                if (sorts.Any())
                {
                    this.AssignedColumnsGrid.SortByDescriptions.Clear();
                    foreach (var s in sorts) this.AssignedColumnsGrid.SortByDescriptions.Add(s);
                }
                this.AssignedColumnsGrid.Items.Refresh();
            }
            catch (InvalidOperationException) { }
        }

        /// <summary>
        /// Called when changing assigned column order to keep the row in view.
        /// </summary>
        private void ViewModel_MoveColumnExecuted(object sender, EventArgs e)
        {
            if (this.ViewModel.SelectedAssignedColumnViews.Any())
            {
                this.AssignedColumnsGrid.ScrollIntoView(this.ViewModel.SelectedAssignedColumnViews.First());
            }
        }

        /// <summary>
        /// Called whenever the user double clicks on the field grid
        /// </summary>
        private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem == null) return;
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever rows are dropped from the assigned columns grid.
        /// </summary>
        private void FieldGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid != AssignedColumnsGrid) return;
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called when the user double clicks on a row in the grid.
        /// </summary>
        private void AssignedColumnsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //remove the selected columns
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever rows are dropped from the field grid to this one.
        /// </summary>
        private void AssignedColumnsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid != FieldGrid) return;
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever the totals checkbox is checked.
        /// </summary>
        private void HasTotalsCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.TotalTypeColumn.Visibility = System.Windows.Visibility.Visible;
        }

        /// <summary>
        /// Called whenever the totals checkbox is unchecked.
        /// </summary>
        private void HasTotalsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.TotalTypeColumn.Visibility = System.Windows.Visibility.Collapsed;
        }

        #endregion

        #region window close

        /// <summary>
        /// Disposes of the viewmodel on close.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null) dis.Dispose();
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        

        
    }

}
