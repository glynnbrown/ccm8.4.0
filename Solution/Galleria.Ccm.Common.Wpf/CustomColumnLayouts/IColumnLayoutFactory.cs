﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27197 : L.Ineson
//  Created
// V8-27504 : A.Silva
//      Added comments.
// V8-27898 : L.Ineson
//  Updated methods to take field instead of property path.
// V8-28175 : A.Kuszyk
//  Added ColumnLayoutExtensions class to contain extension methods useful for the IColumnLayoutFactory methods.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29681 : A.Probyn
//  Added new RegisterPlanogram
//  Removed ColumnLayoutExtensions and the obsolete DecoratePerformanceFields
#endregion
#region Version History: (CCM 8.3.0)
// V8-32122 : L.Ineson
//  Removed get header group number.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    /// Interface defining the expected members of a column layout factory.
    /// </summary>
    public interface IColumnLayoutFactory
    {
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="defaultOnly">If true, only default fields will be returned</param>
        CustomColumnList NewCustomColumnList(Boolean defaultOnly);

        /// <summary>
        ///  Gets all the fields belonging to the layout type.
        /// </summary>
        IEnumerable<ObjectFieldInfo> GetModelObjectFields();

        /// <summary>
        ///  Gets the name of the header group matching the given field
        /// </summary>
        ///<param name="field">the field to match</param>
        /// <returns>The friendly name for the header group.</returns>
        String GetHeaderGroupName(ObjectFieldInfo field);

        /// <summary>
        /// Registers the planogram context for use in planogram context specific fields
        /// </summary>
        /// <param name="context"></param>
        void RegisterPlanogram(Planogram context);

        /// <summary>
        ///  Gets whether the property matching the given field is editable or not.
        /// </summary>
        ///<param name="field">the field to match</param>
        /// <returns>Whether the matching property is editable or not.</returns>;
        Boolean IsPropertyReadOnly(ObjectFieldInfo field, Type sourceRowType);

        /// <summary>
        /// Gets the layout type for this factory.
        /// </summary>
        CustomColumnLayoutType LayoutType { get; }


        Dictionary<String, ObjectFieldInfo> GetFieldLookupCache();
        
        Type RowSourceType { get; }

        IEnumerable<CustomColumn> GetFullCustomColumnList();

        /// <summary>
        /// Gets the deault header to use for a column created for the given field.
        /// </summary>
        String GetDefaultHeaderName(ObjectFieldInfo field);
    }


}