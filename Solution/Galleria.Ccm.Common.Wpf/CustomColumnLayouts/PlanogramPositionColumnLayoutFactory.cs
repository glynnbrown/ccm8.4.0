﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-32121 : L.Ineson
//  Created
#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Common.Wpf.CustomColumnLayouts
{
    /// <summary>
    ///     Column Layout factory for positions
    /// </summary>
    public sealed class PlanogramPositionColumnLayoutFactory : BaseColumnLayoutFactory
    {
        const String fixturePrefix = "Fixture.";
        const String assemblyPrefix = "Assembly.";
        const String componentsPrefix = "Component.";
        const String subcomponentsPrefix = "SubComponent.";
        const String productPrefix = "Product.";
        const String planogramPrefix = "Planogram.";

        #region Constructors

        public PlanogramPositionColumnLayoutFactory(Type rowSourceType)
            : this(rowSourceType, null)
        { }

        public PlanogramPositionColumnLayoutFactory(Type rowSourceType, Planogram planogramContext)
            :base(rowSourceType, CustomColumnLayoutType.PlanogramPosition, planogramContext)
            
        {
            //get the field infos
            IEnumerable<ObjectFieldInfo> fieldInfos = GetModelObjectFields();
            RegisterGroupNames(fieldInfos.Select(g => g.GroupName).Distinct());

            base.RegisterVisibleFields(
                new List<String>
            {
                productPrefix + PlanogramProduct.GtinProperty.Name,
                productPrefix + PlanogramProduct.NameProperty.Name,
                PlanogramPosition.TotalUnitsProperty.Name,
                PlanogramPosition.PositionSequenceNumberProperty.Name,
                componentsPrefix + PlanogramFixtureComponent.ComponentSequenceNumberProperty.Name,
                componentsPrefix + PlanogramComponent.NameProperty.Name,
                fixturePrefix + PlanogramFixtureItem.BaySequenceNumberProperty.Name,
                fixturePrefix + PlanogramFixture.NameProperty.Name,
                //Add any new properties that should be visible on load here.
            });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        public override IEnumerable<ObjectFieldInfo> GetModelObjectFields()
        {
            Planogram plan = base.GetPlanogramContext();
            PlanogramPerformance performance = (plan != null) ? plan.Performance : null;

            //ProductFields
            String groupName = PlanogramProduct.FriendlyName;
            foreach (var field in PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true, performance, true))
            {
                field.PropertyName = productPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Position fields
            groupName = PlanogramPosition.FriendlyName;
            foreach (var field in PlanogramPosition.EnumerateDisplayableFieldInfos(/*appliedMerch*/false))
            {
                field.GroupName = groupName;
                yield return field;
            }

            //Subcomponent fields
            groupName = PlanogramSubComponent.FriendlyName;
            foreach (var field in PlanogramSubComponent.EnumerateDisplayableFieldInfos())
            {
                //don't include the duplicated generic properties as these will either be the same
                // or blank.
                if (field.PropertyName == PlanogramSubComponent.NameProperty.Name
                    || field.PropertyName == PlanogramSubComponent.HeightProperty.Name
                    || field.PropertyName == PlanogramSubComponent.WidthProperty.Name
                    || field.PropertyName == PlanogramSubComponent.DepthProperty.Name
                    || field.PropertyName == PlanogramSubComponent.SlopeProperty.Name
                    || field.PropertyName == PlanogramSubComponent.RollProperty.Name
                    || field.PropertyName == PlanogramSubComponent.AngleProperty.Name
                    || field.PropertyName == PlanogramSubComponent.XProperty.Name
                    || field.PropertyName == PlanogramSubComponent.YProperty.Name
                    || field.PropertyName == PlanogramSubComponent.ZProperty.Name)
                    continue;


                field.PropertyName = subcomponentsPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Component fields
            groupName = PlanogramComponent.FriendlyName;
            foreach (var field in PlanogramComponent.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.PropertyName = componentsPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixtureComponent.EnumerateDisplayableFieldInfos())
            {
                field.PropertyName = componentsPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Assembly
            groupName = PlanogramAssembly.FriendlyName;
            foreach (var field in PlanogramAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.PropertyName = assemblyPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixtureAssembly.EnumerateDisplayableFieldInfos(/*incMeta*/true))
            {
                field.PropertyName = assemblyPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Fixture
            groupName = PlanogramFixture.FriendlyName;
            foreach (var field in PlanogramFixtureItem.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
            foreach (var field in PlanogramFixture.EnumerateDisplayableFieldInfos(/*includeMetadata*/true))
            {
                field.PropertyName = fixturePrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }

            //Planogram
            groupName = Planogram.FriendlyName;
            foreach (var field in Planogram.EnumerateDisplayableFieldInfos(true))
            {
                field.PropertyName = planogramPrefix + field.PropertyName;
                field.GroupName = groupName;
                yield return field;
            }
        }

        #endregion

    }
}
