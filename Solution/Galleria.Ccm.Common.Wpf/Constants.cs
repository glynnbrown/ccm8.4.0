﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28130 : N.Foster
//  Created
#endregion

#endregion

using System;

namespace Galleria.Ccm.Common.Wpf
{
    /// <summary>
    /// Contains constant values available to the entire application.
    /// </summary>
    public static class Constants
    {
        #region Constants
        private static Int32 _pollingPeriod = 5000; // the ui polling period in milliseconds
        #endregion

        #region Properties
        /// <summary>
        /// Returns the ui polling period
        /// </summary>
        public static Int32 PollingPeriod
        {
            get { return _pollingPeriod; }
        }
        #endregion
    }
}
