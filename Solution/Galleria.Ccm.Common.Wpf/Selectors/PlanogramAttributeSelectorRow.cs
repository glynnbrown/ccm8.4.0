﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 803
// V8-29643 : D.Pleasance
//	Created.
#endregion
#region Version History: CCM 810
// V8-30063 : D.Pleasance
//	Added Inventory settings
#endregion
#region Version History: CCM820
// V8-31128 : D.Pleasance
//  Added Info settings
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using System.ComponentModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Represents a row in the Planogram Attribute Selector window, encapsulating attribute properties.
    /// </summary>
    public class PlanogramAttributeSelectorRow : IEquatable<PlanogramAttributeSelectorRow>, INotifyPropertyChanged
    {
        #region Fields 

        private Object _propertyItems;
        private Object _propertyValue;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PropertyItemsProperty = WpfHelper.GetPropertyPath<PlanogramAttributeSelectorRow>(p => p.PropertyItems);
        public static readonly PropertyPath PropertyValueProperty = WpfHelper.GetPropertyPath<PlanogramAttributeSelectorRow>(p => p.PropertyValue);
                
        #endregion

        #region Properties

        /// <summary>
        /// The row Header, representing the friendly name of an attribute,
        /// </summary>
        public String Header { get; private set; }

        /// <summary>
        /// The row Header Group, representing the group that this attribute belongs to.
        /// </summary>
        public String HeaderGroup { get; private set; }

        /// <summary>
        /// The actual property name that this attribute refers to.
        /// </summary>
        public String PropertyName { get; private set; }

        /// <summary>
        /// The Source type that this attribute belongs to.
        /// </summary>
        public PlanogramAttributeSourceType Source { get; private set; }

        public Object PropertyItems
        {
            get { return _propertyItems; }
            private set { _propertyItems = value; }
        }

        public Object PropertyValue
        {
            get { return _propertyValue; }
            set
            {
                _propertyValue = value;
                OnPropertyChanged(PropertyValueProperty);
            }
        }

        public Type PropertyType { get; set; }
        #endregion

        #region Constructor
        
        /// <summary>
        /// Instantiates a new row from using the values in the given object field info.
        /// </summary>
        /// <param name="fieldInfo"></param>
        public PlanogramAttributeSelectorRow(ObjectFieldInfo fieldInfo)
        {
            Header = fieldInfo.FieldFriendlyName;
            HeaderGroup = fieldInfo.GroupName;
            PropertyName = fieldInfo.PropertyName;
            PropertyType = fieldInfo.PropertyType;

            if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_General))
            {
                Source = PlanogramAttributeSourceType.General;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataText))
            {
                Source = PlanogramAttributeSourceType.CustomDataText;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataValue))
            {
                Source = PlanogramAttributeSourceType.CustomDataValue;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataFlag))
            {
                Source = PlanogramAttributeSourceType.CustomDataFlag;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataDate))
            {
                Source = PlanogramAttributeSourceType.CustomDataDate;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataNote))
            {
                Source = PlanogramAttributeSourceType.CustomDataNote;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_Settings))
            {
                Source = PlanogramAttributeSourceType.Settings;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramInventory_PropertyGroup))
            {
                Source = PlanogramAttributeSourceType.Inventory;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_Info))
            {
                Source = PlanogramAttributeSourceType.Info;
            }
            else
            {
                Source = PlanogramAttributeSourceType.General;
            }

            if (fieldInfo.PropertyType == typeof(PlanogramCurrencyUnitOfMeasureType))
            {
                _propertyItems = PlanogramCurrencyUnitOfMeasureTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramLengthUnitOfMeasureType))
            {
                _propertyItems = PlanogramLengthUnitOfMeasureTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramAreaUnitOfMeasureType))
            {
                _propertyItems = PlanogramAreaUnitOfMeasureTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramVolumeUnitOfMeasureType))
            {
                _propertyItems = PlanogramVolumeUnitOfMeasureTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramWeightUnitOfMeasureType))
            {
                _propertyItems = PlanogramWeightUnitOfMeasureTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramProductPlacementXType))
            {
                _propertyItems = PlanogramProductPlacementXTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramProductPlacementYType))
            {
                _propertyItems = PlanogramProductPlacementYTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramProductPlacementZType))
            {
                _propertyItems = PlanogramProductPlacementZTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramType))
            {
                _propertyItems = PlanogramTypeHelper.FriendlyNames;
            }
            else if (fieldInfo.PropertyType == typeof(PlanogramStatusType))
            {
                _propertyItems = PlanogramStatusTypeHelper.FriendlyNames;
            }
        } 

        #endregion

        #region Methods

        /// <summary>
        /// IEquatable implementation, for object comparisons.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public Boolean Equals(PlanogramAttributeSelectorRow other)
        {
            return
                other.Header.Equals(this.Header) &&
                other.HeaderGroup.Equals(this.HeaderGroup) &&
                other.PropertyName.Equals(this.PropertyName) &&
                other.Source == this.Source;
        }

        internal void ApplyPropertyValue(Object propertyValue)
        {
            if (propertyValue == null) return;

            if (PropertyType == typeof(PlanogramCurrencyUnitOfMeasureType))
            {
                this.PropertyValue = (PlanogramCurrencyUnitOfMeasureType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramLengthUnitOfMeasureType))
            {
                this.PropertyValue = (PlanogramLengthUnitOfMeasureType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramAreaUnitOfMeasureType))
            {
                this.PropertyValue = (PlanogramAreaUnitOfMeasureType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramVolumeUnitOfMeasureType))
            {
                this.PropertyValue = (PlanogramVolumeUnitOfMeasureType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramWeightUnitOfMeasureType))
            {
                this.PropertyValue = (PlanogramWeightUnitOfMeasureType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramProductPlacementXType))
            {
                this.PropertyValue = (PlanogramProductPlacementXType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramProductPlacementYType))
            {
                this.PropertyValue = (PlanogramProductPlacementYType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramProductPlacementZType))
            {
                this.PropertyValue = (PlanogramProductPlacementZType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramType))
            {
                this.PropertyValue = (PlanogramType)propertyValue;
            }
            else if (PropertyType == typeof(PlanogramStatusType))
            {
                this.PropertyValue = (PlanogramStatusType)propertyValue;
            }
            else
            {
                _propertyValue = propertyValue;
            }
        }
        
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}