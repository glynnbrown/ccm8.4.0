﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-27150 : L.Ineson 
//  Created.
#endregion

#region Version History: (CCM 8.0.1)
// V8-28103 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History: (CCM 8.0.3)
// V8-29723 : D.Pleasance
//  Added ProductSelectorHelper along with constructor overload. Also added ability to exclude additional products.
#endregion

#region Version History : CCM830
// V8-31561 : A.Kuszyk
//  Added ability to manually order products.
// V8-32086 : N.Haywood
//  Added category code auto selection
// V8-32356 : N.Haywood
//  Added clipboard selection and changed criteria searching
// V8-32531 : N.Haywood
//  Made clipboard selection distinct
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll functionality; amended AddSelected and RemoveSelected commands.
// V8-32812 : A.Heathcote
//  Added AddManualBuddyCommand/field/property as well as a new constructor and the BuddyButtonVisable buddyButtonPressed
// V8-32964 : A.Heathcote
//  Solved an issue with the buddies multi-select - Now loads the category.
// V8-32972 : A.Silva
//  Amended SelectUsingClipboardCommand_Executed so that newly added products do not remove previously added ones.
// V8-14046 : M.Brumby
//  Refactored search update to work better when there is no search type.
// CCM--14012 : A.Heathcote
//  Split existing constructor for the Product Buddy Screens into two.
// CCM-18316 : A.Heathcote
//  Added the ability to set a maximum number of items to select but made it nullable so that it doesnt interfere with existing constructers.
// CCM-18490 : M.Pettit
//  Added overload to constructor, passing in ProductSelectorHelper and initial Category Code
// CCM-18489 : A.Heathcote
//  Added a friendly description to the AddManualBuddy Command to be used as a tooltip
// CCM-18458 : A.Heathcote
//  Added a perameter to the product buddy constructor, so that excluded (already selected) products can not be re-added.
// CCM-18495 : M.Pettit
//  Added new overload for constructor passing in aken gtins and initial category code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using System.Collections;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Csla;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Controls.Wpf;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for product selector window.
    /// </summary>
    public class ProductSelectorViewModel : ViewModelAttachedControlObject<ProductSelectorWindow>
    {
        private const String EscapedNewLine = "\r\n";
        private const ModalMessageResult ContinueWithPasting = ModalMessageResult.Button2;

        #region Static Fields
        private static ProductGroupInfo _lastCategorySearchCriteria;
        private static String _lastSearchCriteria;
        #endregion

        #region Fields

        private Boolean? _dialogResult;
        private Boolean _isMultiSelectEnabled = false;
        private ProductSelectorSearchMode _searchMode = ProductSelectorSearchMode.Category;
        private readonly ProductListViewModel _productListViewModel = new ProductListViewModel();
        private readonly BulkObservableCollection<Product> _searchResults = new BulkObservableCollection<Product>();
        private readonly ReadOnlyBulkObservableCollection<Product> _searchResultsRO;
        private Boolean _isSearching;
        private Boolean _isNewSearchQueued;
        private String _searchCriteria;
        private ProductGroupInfo _categorySearchCriteria;
        private readonly BulkObservableCollection<Product> _assignedProducts = new BulkObservableCollection<Product>();
        private List<String> _excludedProducts = new List<String>();
        private Product _selectedProduct;
        private Product _selectedAssignedProduct;
        private Boolean _buddyButtonPressed;
        private Boolean _buddyButtonVisable = false;
        private List<Product> _defaultProductList = new List<Product>(); //product list for no search
        private Int32? _maximumProductSelection;
       
        #endregion

        #region Binding Property Paths

        //Properties
        public static PropertyPath IsMultiSelectEnabledProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.IsMultiSelectEnabled);
        public static PropertyPath IsOrderableProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.IsOrderable);
        public static PropertyPath SearchModeProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SearchMode);
        public static PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SearchCriteria);
        public static PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.IsSearching);
        public static PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SearchResults);
        public static PropertyPath AssignedProductsProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.AssignedProducts);
        public static PropertyPath CategorySearchCriteriaProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.CategorySearchCriteria);
        public static PropertyPath SelectedProductProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SelectedProduct);
        public static PropertyPath SelectedAssignedProductProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SelectedAssignedProduct);
        public static PropertyPath BuddyButtonVisableProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.BuddyButtonVisable);

        //Commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath AssignSelectedCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.AssignSelectedCommand);
        public static readonly PropertyPath UnassignSelectedCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.UnassignSelectedCommand);
        public static PropertyPath SetCategoryCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SetCategoryCommand);
        public static PropertyPath MoveProductUpCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.MoveProductUpCommand);
        public static PropertyPath MoveProductDownCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.MoveProductDownCommand);
        public static PropertyPath SelectUsingClipboardCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.SelectUsingClipboardCommand);
        public static PropertyPath AddManualBuddyCommandProperty = WpfHelper.GetPropertyPath<ProductSelectorViewModel>(p => p.AddManualBuddyCommand);
        #endregion

        #region Properties

        public Boolean BuddyButtonVisable
        {
            get { return _buddyButtonVisable; }
            private set 
            {
                _buddyButtonVisable = value;
            }
        }
        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns true if multi select is enabled.
        /// </summary>
        public Boolean IsMultiSelectEnabled
        {
            get { return _isMultiSelectEnabled; }
        }

        /// <summary>
        /// Returns true if this window does not have
        /// a fixed item source.
        /// </summary>
        public ProductSelectorSearchMode SearchMode
        {
            get { return _searchMode; }
            set
            {
                _searchMode = value;
                OnPropertyChanged(SearchModeProperty);

                //research
                BeginOrQueueSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the criteria to search for products by.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);

                //research
                BeginOrQueueSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected category for the category search mode.
        /// </summary>
        public ProductGroupInfo CategorySearchCriteria
        {
            get { return _categorySearchCriteria; }
            set
            {
                _categorySearchCriteria = value;
                OnPropertyChanged(CategorySearchCriteriaProperty);

                //research
                BeginOrQueueSearch();
            }
        }

        /// <summary>
        /// Returns true if a search is underway
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<Product> SearchResults
        {
            get { return _searchResultsRO; }
        }

        /// <summary>
        /// Returns the collection of assigned products.
        /// </summary>
        public BulkObservableCollection<Product> AssignedProducts
        {
            get { return _assignedProducts; }
        }
        
        /// <summary>
        /// Gets/Sets the selected product for single selection.
        /// </summary>
        public Product SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(SelectedProductProperty);
            }
        }

        /// <summary>
        /// The selected product in the assigned products grid.
        /// </summary>
        public Product SelectedAssignedProduct 
        {
            get { return _selectedAssignedProduct; }
            set
            {
                _selectedAssignedProduct = value;
                OnPropertyChanged(SelectedAssignedProductProperty);
            }
        }

        /// <summary>
        /// Indicates whether or not the selected products can be ordered manually.
        /// </summary>
        public Boolean IsOrderable { get; private set; }

        public Boolean ManualBuddyButtonPressed
        {
            get { return _buddyButtonPressed;}
            set { _buddyButtonPressed = value;}
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(IEnumerable<Int32> assignedProductIds)
        {
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            try
            {
                _assignedProducts.AddRange(ProductList.FetchByProductIds(assignedProductIds));
            }
            catch (DataPortalException) { }

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts">The products already assigned - these cannot be selected</param>
        /// <param name="categoryCode">The initial search criteria for the products</param>
        public ProductSelectorViewModel(IEnumerable<Int32> assignedProductIds, String categoryCode)
        {
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            try
            {
                CategorySearchCriteria = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { categoryCode }).FirstOrDefault();
                _assignedProducts.AddRange(ProductList.FetchByProductIds(assignedProductIds));
            }
            catch (DataPortalException) { }

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(IEnumerable<Int32> assignedProductIds, Int32 productGroupId)
        {
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            try
            {
                CategorySearchCriteria = ProductGroupInfo.FetchById(productGroupId);
                _assignedProducts.AddRange(ProductList.FetchByProductIds(assignedProductIds));
            }
            catch (DataPortalException) { }

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(IEnumerable<String> assignedProductGtins, Boolean isOrderable = false)
        {
            IsOrderable = isOrderable;
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            try
            {
                _assignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(CCMClient.ViewState.EntityId, assignedProductGtins));
            }
            catch (DataPortalException) { }

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(ProductList availableProducts, IEnumerable<Int32> assignedProductIds)
        {
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _assignedProducts.AddRange(availableProducts.Where(p => assignedProductIds.Contains(p.Id)));

            _searchMode = ProductSelectorSearchMode.None;
            _productListViewModel.Model = availableProducts;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(ProductList availableProducts, IEnumerable<Product> assignedProducts)
        {
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _assignedProducts.AddRange(assignedProducts);

            _searchMode = ProductSelectorSearchMode.None;
            _productListViewModel.Model = availableProducts;
        }

        /// <summary>
        /// Constructor - used for single selection mode.
        /// </summary>
        public ProductSelectorViewModel()
        {
            _isMultiSelectEnabled = false;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor- used for single selection mode.
        /// </summary>
        public ProductSelectorViewModel(ProductList availableProducts)
        {
            _isMultiSelectEnabled = false;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _searchMode = ProductSelectorSearchMode.None;
            _productListViewModel.Model = availableProducts;
        }

        /// <summary>
        /// Constructor - used for single selection mode.
        /// </summary>
        public ProductSelectorViewModel(ProductSelectorHelper productSelectorHelper)
        {
            _isMultiSelectEnabled = productSelectorHelper.IsMultiSelectEnabled;
            _maximumProductSelection = productSelectorHelper.MaximumResultCount;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _assignedProducts.AddRange(productSelectorHelper.AssignedProducts);
            _excludedProducts.AddRange(productSelectorHelper.ExcludedProducts);

            LoadLastSetting();
        }

        /// <summary>
        /// Constructor - used for single selection mode where an initial category is set
        /// </summary>
        public ProductSelectorViewModel(ProductSelectorHelper productSelectorHelper, String categoryCode)
        {
            _isMultiSelectEnabled = productSelectorHelper.IsMultiSelectEnabled;
            _maximumProductSelection = productSelectorHelper.MaximumResultCount;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _assignedProducts.AddRange(productSelectorHelper.AssignedProducts);
            _excludedProducts.AddRange(productSelectorHelper.ExcludedProducts);
            try
            {
                CategorySearchCriteria = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { categoryCode }).FirstOrDefault();
            }
            catch (DataPortalException) { }
            LoadLastSetting();
        }

        /// <summary>
        /// Constructor - used for single selection mode where an initial category is set
        /// </summary>
        public ProductSelectorViewModel(ProductSelectorHelper productSelectorHelper, Int32 productGroupId)
        {
            _isMultiSelectEnabled = productSelectorHelper.IsMultiSelectEnabled;
            _maximumProductSelection = productSelectorHelper.MaximumResultCount;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            _assignedProducts.AddRange(productSelectorHelper.AssignedProducts);
            _excludedProducts.AddRange(productSelectorHelper.ExcludedProducts);
            try
            {
                CategorySearchCriteria = ProductGroupInfo.FetchById(productGroupId);
            }
            catch (DataPortalException) { }
            LoadLastSetting();
        }

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts"></param>
        public ProductSelectorViewModel(IEnumerable<String> assignedProductGtins, String categoryCode, Boolean isOrderable = false)
        {
            IsOrderable = isOrderable;
            _isMultiSelectEnabled = true;

            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;

            try
            {
                CategorySearchCriteria = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { categoryCode }).FirstOrDefault();
                _assignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(CCMClient.ViewState.EntityId, assignedProductGtins));
            }
            catch (DataPortalException) { }
            LoadLastSetting();
        }

        public ProductSelectorViewModel(String categoryCode)
        {
            _isMultiSelectEnabled = false;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;
            CategorySearchCriteria = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { categoryCode }).FirstOrDefault();
            LoadLastSetting();
        }

        /// <summary>
        /// Space Automation Product Buddy Screen ver.
        /// Multi Selection.
        /// </summary>
        /// <param name="manualButtonVisable"> Sets if the "Manual buddy button" is visable</param>
        /// <param name="productGroupId">This allows the selector screen to use the category instead of being passed the assortment</param>
        public ProductSelectorViewModel(Boolean manualButtonVisable, Int32 productGroupId, List<String> excludeProducts)
        {
            _buddyButtonVisable = manualButtonVisable;
            _isMultiSelectEnabled = true;
            IsOrderable = false;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);            
            SearchMode = ProductSelectorSearchMode.Category;
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;
            try
            {
                CategorySearchCriteria = ProductGroupInfo.FetchById(productGroupId); 
                                   
            }
            catch (DataPortalException) { }
            _excludedProducts = excludeProducts;
            //LoadLastSetting();            
        }

        /// <summary>
        /// Space Planning Product Buddy Screen ver.
        /// </summary>
        /// <param name="manualButtonVisable"> Sets if the "Manual buddy button" is visable</param>
        /// <param name="categoryCode"> This allows the selector screen to use the category instead of being passed the assortment</param>
        public ProductSelectorViewModel(Boolean manualButtonVisable, String categoryCode, List<Product> productList, List<String> excludeProducts)
        {
            //The Try/Catch allows the buddy screens in PSace Planning to be used offline using the product list that has been passed.           
            List<String> categoryList = new List<string>() {categoryCode };
            try
            {
                CategorySearchCriteria = ProductGroupInfoList.FetchByProductGroupCodes(categoryList).FirstOrDefault();
            }
            catch (System.Security.SecurityException) { _defaultProductList.AddRange(productList); }
            _excludedProducts = excludeProducts;
            _searchResultsRO = new ReadOnlyBulkObservableCollection<Product>(_searchResults);           
            _buddyButtonVisable = manualButtonVisable;
            _isMultiSelectEnabled = true;
            IsOrderable = false;          
            SearchMode = ProductSelectorSearchMode.Category;
            _productListViewModel.ModelChanged += ProductListViewModel_ModelChanged;            
            //LoadLastSetting();            
        }
        
        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            if (!this.IsMultiSelectEnabled && this.SelectedProduct == null)
            {
                return false;
            }
            if (this._maximumProductSelection != null && this.AssignedProducts.Count() > this._maximumProductSelection)
            {
                this.OKCommand.DisabledReason = Message.ProductSelector_ToManyAssigned;             
                return false;
            }
            return true;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }


        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AssignSelectedCommand

        private RelayCommand _assignSelectedCommand;

        /// <summary>
        /// AssignSelecteds the selection and closes the window.
        /// </summary>
        public RelayCommand AssignSelectedCommand
        {
            get
            {
                if (_assignSelectedCommand != null) return _assignSelectedCommand;

                _assignSelectedCommand =
                    new RelayCommand(AssignSelectedCommand_Executed, AssignSelectedCommand_CanExecute)
                    {
                        FriendlyName = Message.Generic_AddSelectedItems,
                        SmallIcon = ImageResources.Add_16
                    };
                ViewModelCommands.Add(_assignSelectedCommand);
                return _assignSelectedCommand;
            }
        }

        private static Boolean AssignSelectedCommand_CanExecute(Object args)
        {
            //  Check that the parameter is of the expected type.
            var infos = args as IEnumerable;
            if (infos == null) return false;

            //  Check that there are products selected.
            if (!infos.Cast<Product>().Any()) return false;
            
            return true;
        }

        private void AssignSelectedCommand_Executed(Object args)
        {
            IEnumerable<Product> items = ((IEnumerable)args).Cast<Product>().ToList();

            _assignedProducts.AddRange(items);
            _searchResults.RemoveRange(items);
        }

        #endregion

        #region UnassignSelectedCommand

        private RelayCommand _unassignSelectedCommand;

        /// <summary>
        /// UnassignSelecteds the selection and closes the window.
        /// </summary>
        public RelayCommand UnassignSelectedCommand
        {
            get
            {
                if (_unassignSelectedCommand != null) return _unassignSelectedCommand;

                _unassignSelectedCommand = 
                    new RelayCommand(UnassignSelectedCommand_Executed,UnassignSelectedCommand_CanExecute)
                    {
                        FriendlyName = Message.Generic_RemoveSelectedItems,
                        SmallIcon = ImageResources.Delete_16
                    };
                ViewModelCommands.Add(_unassignSelectedCommand);
                return _unassignSelectedCommand;
            }
        }

        private static Boolean UnassignSelectedCommand_CanExecute(Object args)
        {
            //  Check that the parameter is of the expected type.
            var infos = args as IEnumerable;
            if (infos == null) return false;

            //  Check that there are products selected.
            if (!infos.Cast<Product>().Any()) return false;

            return true;
        }

        private void UnassignSelectedCommand_Executed(Object args)
        {
            IEnumerable<Product> infos = ((IEnumerable)args).Cast<Product>().ToList();

            _assignedProducts.RemoveRange(infos);
            _searchResults.AddRange(infos);
        }

        #endregion

        #region SetCategoryCommand

        private RelayCommand _setCategoryCommand;

        /// <summary>
        /// Sets the category to search by
        /// </summary>
        public RelayCommand SetCategoryCommand
        {
            get
            {
                if (_setCategoryCommand == null)
                {
                    _setCategoryCommand =
                        new RelayCommand(
                            p => SetCategory_Executed(),
                            p => SetCategory_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    this.ViewModelCommands.Add(_setCategoryCommand);
                }
                return _setCategoryCommand;
            }
        }

        private Boolean SetCategory_CanExecute()
        {
            ////user must have product group fetch permission
            //if (!_productHierarchyPermissions.CanFetch)
            //{
            //    this.SetCategoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
            //    return false;
            //}

            return true;
        }

        private void SetCategory_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow merchandisingHierarchyWindow = new MerchGroupSelectionWindow();
                merchandisingHierarchyWindow.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;

                WindowHelper.ShowWindow(merchandisingHierarchyWindow, this.AttachedControl, true);

                if (merchandisingHierarchyWindow.DialogResult == true)
                {
                    this.CategorySearchCriteria = ProductGroupInfo.NewProductGroupInfo(merchandisingHierarchyWindow.SelectionResult[0]);
                }
            }
        }

        #endregion

        #region MoveProductUpCommand

        private RelayCommand _moveProductUpCommand;

        /// <summary>
        /// Moves the selected product up one.
        /// </summary>
        public RelayCommand MoveProductUpCommand
        {
            get
            {
                if (_moveProductUpCommand == null)
                {
                    _moveProductUpCommand = new RelayCommand(
                        p => MoveProductUpCommand_Executed(),
                        p => MoveProductUpCommand_CanExecute())
                    {
                        FriendlyName = Message.ProductSelectorViewModel_MoveProductUp, 
                        SmallIcon = ImageResources.AddSelectedItems
                    };
                    base.ViewModelCommands.Add(_moveProductUpCommand);
                }
                return _moveProductUpCommand;
            }
        }

        private Boolean MoveProductUpCommand_CanExecute()
        {
            if (!AssignedProducts.Any())
            {
                return false;
            }

            if (this.SelectedAssignedProduct == null)
            {
                MoveProductUpCommand.DisabledReason = Message.ProductSelectorViewModel_SelectAProductToMove; 
                return false;
            }

            if (SelectedAssignedProduct == AssignedProducts.First())
            {
                return false;
            }

            return true;
        }

        private void MoveProductUpCommand_Executed()
        {
            Product selectedAssignedProduct = SelectedAssignedProduct;
            Int32 currentIndex = this.AssignedProducts.IndexOf(this.SelectedAssignedProduct);
            Int32 newIndex = currentIndex - 1;
            if (currentIndex < 0 || newIndex < 0) return;
            AssignedProducts.Move(currentIndex, newIndex);
            SelectedAssignedProduct = selectedAssignedProduct;
        }

        #endregion

        #region MoveProductDownCommand

        private RelayCommand _moveProductDownCommand;

        /// <summary>
        /// Moves the selected product up one.
        /// </summary>
        public RelayCommand MoveProductDownCommand
        {
            get
            {
                if (_moveProductDownCommand == null)
                {
                    _moveProductDownCommand = new RelayCommand(
                        p => MoveProductDownCommand_Executed(),
                        p => MoveProductDownCommand_CanExecute())
                    {
                        FriendlyName = Message.ProductSelectorViewModel_MoveProductDown, 
                        SmallIcon = ImageResources.AddSelectedItems
                    };
                    base.ViewModelCommands.Add(_moveProductDownCommand);
                }
                return _moveProductDownCommand;
            }
        }

        private Boolean MoveProductDownCommand_CanExecute()
        {
            if (!AssignedProducts.Any())
            {
                return false;
            }

            if (this.SelectedAssignedProduct == null)
            {
                MoveProductDownCommand.DisabledReason = Message.ProductSelectorViewModel_SelectAProductToMove;
                return false;
            }

            if (SelectedAssignedProduct == AssignedProducts.Last())
            {
                return false;
            }

            return true;
        }

        private void MoveProductDownCommand_Executed()
        {
            Product selectedAssignedProduct = SelectedAssignedProduct;
            Int32 currentIndex = this.AssignedProducts.IndexOf(this.SelectedAssignedProduct);
            Int32 newIndex = currentIndex + 1;
            if (currentIndex < 0 || newIndex >= AssignedProducts.Count) return;
            AssignedProducts.Move(currentIndex, newIndex);
            SelectedAssignedProduct = selectedAssignedProduct;
        }

        #endregion

        #region AddManualBuddyButtonCommand
        private RelayCommand _addManualBuddyCommand;

        public RelayCommand AddManualBuddyCommand
        {
            get
            {
                if (_addManualBuddyCommand == null)
                {
                    _addManualBuddyCommand = new RelayCommand(
                        p => AddManualBuddyCommand_Executed(),
                        p => AddManualBuddyCommand_CanExecute())
                        {
                            FriendlyName = Message.ProductSelectorWindow_AddManualBuddies,
                            FriendlyDescription = Message.ProductSelector_ManualBuddyDescription,
                        };
                    base.ViewModelCommands.Add(_addManualBuddyCommand);
                }
                return _addManualBuddyCommand;
            }
        }
        private Boolean AddManualBuddyCommand_CanExecute()
        {
            if (!this.IsMultiSelectEnabled && this.SelectedProduct == null)
            {
                return false;
            }

            return true;
        }
        private void AddManualBuddyCommand_Executed()
        {
            ManualBuddyButtonPressed = true;
            this.DialogResult = true;
        }
        #endregion

        #region SelectUsingClipboardCommand

        private RelayCommand _selectUsingClipboardCommand;

        /// <summary>
        /// Moves the selected product up one.
        /// </summary>
        public RelayCommand SelectUsingClipboardCommand
        {
            get
            {
                if (_selectUsingClipboardCommand == null)
                {
                    _selectUsingClipboardCommand = new RelayCommand(
                        p => SelectUsingClipboardCommand_Executed())
                    {
                        FriendlyName = Message.ProductSelectorViewModel_SelectUsingClipboard,
                        SmallIcon = ImageResources.SelectUsingClipboard
                    };
                    base.ViewModelCommands.Add(_selectUsingClipboardCommand);
                }
                return _selectUsingClipboardCommand;
            }
        }

        private void SelectUsingClipboardCommand_Executed()
        {
            List<String> gtinsToAssign;
            if (!TryGetGtinsFromClipboard(out gtinsToAssign)) return;

            ICollection<String> assignedGtins = AssignedProducts.Select(product => product.Gtin).ToList();
            ICollection<String> newGtinsToAssign = gtinsToAssign.Except(assignedGtins).ToList();
            ProductList newProductsToAssign;
            if (!TryFetchProductsFromGtins(newGtinsToAssign, out newProductsToAssign)) return;
            AssignedProducts.AddRange(newProductsToAssign);

            BeginOrQueueSearch();

            List<String> failedToAssignGtins = newGtinsToAssign.Except(newProductsToAssign.Select(product => product.Gtin)).ToList();
            if (!failedToAssignGtins.Any()) return;

            if (PromptUserSomeGtinsFailedToBeAssignedFromClipboard(failedToAssignGtins.Count) != ContinueWithPasting) return;

            Clipboard.SetText(failedToAssignGtins.Aggregate(String.Empty,
                (gtinList, gtin) => $"{gtinList}{gtin}{EscapedNewLine}"));
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the product info list viewmodel model changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductListViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductList> e)
        {
            List<Product> searchResults = new List<Product>();

            if (!_isNewSearchQueued)
            {
                UpdateSearchResults(e.NewModel);                       
                this.IsSearching = false;
            }
            else
            {
                this.IsSearching = false;
                BeginOrQueueSearch();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Kicks of a new search or queues one 
        /// for when we are ready.
        /// </summary>
        private void BeginOrQueueSearch()
        {
            
            if (_searchResults.Count > 0) { _searchResults.Clear(); }

            switch (this.SearchMode)
            {
                case ProductSelectorSearchMode.None:
                    {
                        //just call a reload of the existing list.
                        UpdateSearchResults(_defaultProductList);
                    }
                    break;

                case ProductSelectorSearchMode.Criteria:
                    {
                        if (!String.IsNullOrEmpty(this.SearchCriteria) && this.SearchCriteria.Length > 2)
                        {
                            if (!IsSearching)
                            {
                                _isNewSearchQueued = false;
                                IsSearching = true;

                                _productListViewModel.FetchForCurrentEntityByMultipleSearchCriteriaAsync(this.SearchCriteria);
                            }
                            else
                            {
                                _isNewSearchQueued = true;
                            }
                        }
                    }
                    break;

                case ProductSelectorSearchMode.Category:
                    {
                        if (this._defaultProductList != null) UpdateSearchResults(_defaultProductList);
                        if (this.CategorySearchCriteria != null)
                        {
                            if (!IsSearching)
                            {
                                _isNewSearchQueued = false;
                                IsSearching = true;

                                _productListViewModel.FetchByCategoryIdAsync(this.CategorySearchCriteria.Id);
                            }
                            else
                            {
                                _isNewSearchQueued = true;
                            }
                        }

                    }
                    break;
            }

        }

        /// <summary>
        /// Updates the search results based on the supplied list.
        /// Products already assigned or excluded will not be applied
        /// to the final search results.
        /// </summary>
        /// <param name="productList"></param>
        private void UpdateSearchResults(IEnumerable<Product> productList)
        {
            List<Product> searchResults = new List<Product>();
            if (productList != null)
            {
                List<String> assignedGtins = _assignedProducts.Select(p => p.Gtin).ToList();

                foreach (Product product in productList)
                {
                    if (!assignedGtins.Contains(product.Gtin) && !_excludedProducts.Contains(product.Gtin))
                    {
                        searchResults.Add(product);
                    }
                }
            }

            if (_searchResults.Count > 0) { _searchResults.Clear(); }
            _searchResults.AddRange(searchResults);
        }

        private void SaveLastSetting()
        {
            if (this.SearchMode == ProductSelectorSearchMode.Category
                && this.CategorySearchCriteria != null)
            {
                _lastCategorySearchCriteria = this.CategorySearchCriteria;
            }
            else if (this.SearchMode == ProductSelectorSearchMode.Criteria
                && !String.IsNullOrEmpty(this.SearchCriteria))
            {
                _lastSearchCriteria = this.SearchCriteria;
            }
        }

        private void LoadLastSetting()
        {
            if (_categorySearchCriteria == null)
            {
                _categorySearchCriteria = _lastCategorySearchCriteria;
            }
            _searchCriteria = _lastSearchCriteria;

            if (this.SearchMode != ProductSelectorSearchMode.None)
            {
                BeginOrQueueSearch();
            }
        }

        /// <summary>
        ///     Prompts the user to eitehr
        /// </summary>
        /// <param name="failedToAssignCount"></param>
        /// <returns></returns>
        private static ModalMessageResult PromptUserSomeGtinsFailedToBeAssignedFromClipboard(Int32 failedToAssignCount)
        {
            var windowService = Ccm.Services.ServiceContainer.GetService<IWindowService>();
            return windowService.ShowMessage(
                MessageWindowType.Question,
                Message.ProductSelectorViewModel_ProductsNotFound_Header,
                String.Format(Message.ProductSelectorViewModel_ProductsNotFound_Description, failedToAssignCount),
                2,
                Message.Generic_Ok,
                Message.Generic_CopyToClipboard,
                null,
                ModalMessageButton.Button1,
                ModalMessageButton.Button1);
        }

        /// <summary>
        ///     Tries to get the gtins to assign from the clipboard's content.
        /// </summary>
        /// <param name="gtins">Will contain the gtins that were found in the clipboard's contents.</param>
        /// <returns><c>True</c> if gtins were found in the clipboard, <c>false</c> otherwise.</returns>
        private static Boolean TryGetGtinsFromClipboard(out List<String> gtins)
        {
            gtins = new List<String>();
            var clipboardText = Clipboard.GetData(DataFormats.Text) as String;
            if (clipboardText == null) return false;

            String[] gtinsInClipBoard = clipboardText.Split(new[] { EscapedNewLine }, StringSplitOptions.None);
            gtins.AddRange(gtinsInClipBoard.Where(s => !String.IsNullOrWhiteSpace(s)).Distinct());
            return gtins.Count != 0;
        }

        /// <summary>
        /// Try fetching a <see cref="ProductList"/> from the dal, given a collection of <paramref name="gtins"/>.
        /// </summary>
        /// <param name="gtins">Enumeration of GTINs to obtain products for.</param>
        /// <param name="products">The resulting products that match the provided <paramref name="gtins"/></param>
        /// <returns></returns>
        /// <remarks>If there is any exception during the fetch, the method will return an empty product list and false.</remarks>
        private static Boolean TryFetchProductsFromGtins(IEnumerable<String> gtins, out ProductList products)
        {
            try
            {
                products = ProductList.FetchByEntityIdProductGtins(CCMClient.ViewState.EntityId, gtins);
            }
            catch (Exception ex)
            {
                products = ProductList.NewProductList();
                Console.WriteLine(ex);
                CommonHelper.RecordException(ex);
                return false;
            }
            return true;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                //Save the last search settings so we can use them again
                // if the window is reopened during this session.
                SaveLastSetting();

                _productListViewModel.ModelChanged -= ProductListViewModel_ModelChanged;

                base.IsDisposed = true;
            }
        }

        #endregion

    }

    /// <summary>
    /// Denotes the different search modes for this selector.
    /// </summary>
    public enum ProductSelectorSearchMode
    {
        Criteria,
        Category,
        None
    }

    /// <summary>
    /// Helper class for providing selector attributes
    /// </summary>
    public class ProductSelectorHelper
    {
        public BulkObservableCollection<Product> AssignedProducts { get; set; }
        public List<String> ExcludedProducts { get; set; }
        public Boolean IsMultiSelectEnabled { get; set; }
        public Int32? MaximumResultCount { get; set; }
        public ProductSelectorHelper()
        {
            AssignedProducts = new BulkObservableCollection<Product>();
            ExcludedProducts = new List<String>();
            MaximumResultCount = new Int32?();
        }        
    }
}