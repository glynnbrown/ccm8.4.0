﻿<!--region: Header Information-->
<!--Copyright © Galleria RTS Ltd 2015.-->
<!--region: Version History : (8.3)--><!--
    V8-31763 : A.Silva  ~ Created.
    V8-31963 : A.Silva  ~ Added Display column.
    V8-32157 : A.Silva  ~ Amended move buttons (now just add/remove selected)
--><!--endregion: Version History : (8.3)-->
<!--endregion: Header Information-->

<UserControl x:Class="Galleria.Ccm.Common.Wpf.Selectors.FieldPickerControl"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:Galleria.Ccm.Common.Wpf.Selectors"
             xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
             xmlns:lg="clr-namespace:Galleria.Ccm.Common.Wpf.Resources.Language;assembly=Galleria.Ccm.Common.Wpf"
             xmlns:componentModel="clr-namespace:System.ComponentModel;assembly=WindowsBase"
             xmlns:vm="clr-namespace:Galleria.Framework.ViewModel;assembly=Galleria.Framework"
             x:Name="xUserControl">
    <Grid DataContext="{Binding ElementName=xUserControl, Path=ViewModel}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*" />
            <ColumnDefinition Width="Auto" />
        </Grid.ColumnDefinitions>

        <TextBlock Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2" Margin="5,2,5,2" HorizontalAlignment="Left" VerticalAlignment="Center"
                               FontSize="14" Text="{x:Static lg:Message.FieldPicker_Content_Title}" />

        <g:ContentSectionHeader Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="2" Margin="0,0,0,2" Padding="2" />

        <!--region: Field Picker-->
        <Grid Grid.Row="2" Grid.Column="0" Grid.ColumnSpan="2" >
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto" />
                <ColumnDefinition Width="*" />
            </Grid.ColumnDefinitions>

            <TextBlock Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2"
                       Margin="2,2,2,1" HorizontalAlignment="Left"
                       Text="{x:Static lg:Message.FieldPicker_Label_AvailableFields}" />

            <ListBox Grid.Row="1" Grid.RowSpan="2" Grid.Column="0"
                     HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                     BorderThickness="1" Width="140"
                     ItemsSource="{Binding Path={x:Static local:FieldPickerViewModel.AvailableFieldPickerGroupsProperty}}"
                     SelectedItem="{Binding Path={x:Static local:FieldPickerViewModel.SelectedFieldGroupProperty}}">
                <ListBox.ItemContainerStyle>
                    <Style TargetType="{x:Type ListBoxItem}" BasedOn="{StaticResource {x:Type ListBoxItem}}">
                        <Setter Property="Padding" Value="2,6,2,6" />
                    </Style>
                </ListBox.ItemContainerStyle>
            </ListBox>

            <g:ExtendedTextBox Grid.Row="1" Grid.Column="1" Margin="1,0,0,0"
                                 HorizontalAlignment="Stretch" VerticalAlignment="Top"
                                 IsSearchBox="True"
                                 PromptText="{x:Static lg:Message.FieldSelector_FieldSearchPrompt}"
                                 Text="{Binding ElementName=xFieldGrid, Path=PrefilterText, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                 x:Name="xFieldGridFilter" />

            <g:ExtendedDataGrid   Grid.Row="2" Grid.Column="1"
                                  BorderThickness="1" HeadersVisibility="None" AutoResizeColumns="True"
                                  CanUserDragRows="True" RowDropCaught="FieldGrid_OnRowDropCaught"
                                  AlternatingRowBackground="{x:Null}"
                                  PrefilterText="{Binding Path=Text, ElementName=xFieldGridFilter}"
                                  ScrollViewer.HorizontalScrollBarVisibility="Disabled"
                                  HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                                  ItemsSourceExtended="{Binding Path={x:Static local:FieldPickerViewModel.AvailableFieldPickerFieldsProperty}}"
                                  g:SelectedItemsBehaviour.SynchroniseSelectedItems="{Binding Path={x:Static local:FieldPickerViewModel.SelectedFieldPickerFieldsProperty}}"
                                  RowItemMouseDoubleClick="FieldGrid_OnRowItemMouseDoubleClick"
                                  x:Name="xFieldGrid"
                                  PreviewKeyDown="XFieldGrid_OnPreviewKeyDown">
                <g:ExtendedDataGrid.Columns>
                    <g:DataGridExtendedTextColumn Header="{x:Null}"
                                                    IsReadOnly="True"
                                                    Binding="{Binding Path=Header, Mode=OneWay}"
                                                    SortDirection="Ascending" />


                </g:ExtendedDataGrid.Columns>
            </g:ExtendedDataGrid>

        </Grid>
        <!--endregion: Field Picker-->

        <!--region: Move Selection Buttons-->
        <StackPanel Grid.Row="3" Grid.Column="0" Grid.ColumnSpan="2" Margin="0,2,0,2"
                                VerticalAlignment="Center" HorizontalAlignment="Center" Orientation="Horizontal">

            <!--region AddSelected button-->
            <Button Margin="2" Padding="0" Background="Transparent"
                                    Command="{Binding Path={x:Static local:FieldPickerViewModel.AddSelectedFieldsCommandProperty}}"
                    ToolTip="{x:Static lg:Message.Generic_AddSelectedItems}">
                <Image Height="16" Width="16"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, 
                                    Path=Command.SmallIcon}" />
            </Button>
            <!--endregion AddSelected button-->

            <!--region RemoveSelected button-->
            <Button Margin="10,2,2,2" Padding="0" Background="Transparent"
                                    Command="{Binding Path={x:Static local:FieldPickerViewModel.RemoveSelectedFieldsCommandProperty}}"
                    ToolTip="{x:Static lg:Message.Generic_RemoveSelectedItems}">
                <Image Height="16" Width="16"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, 
                                    Path=Command.SmallIcon}" />
            </Button>
            <!--endregion RemoveSelected button-->

        </StackPanel>
        <!--endregion: Move Selection Buttons-->

        <!--region: Comparison Fields-->
        <Grid Grid.Row="4" Grid.Column="0">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="Auto" />
            </Grid.ColumnDefinitions>

            <TextBlock Grid.Row="0" Grid.Column="0" Margin="2,2,2,1" HorizontalAlignment="Left"
                                   Text="{x:Static lg:Message.FieldPicker_Label_ComparisonFields}" />

            <g:ExtendedDataGrid Grid.Row="1" Grid.Column="0" 
                                            CanUserHideColumns="False" CanUserFreezeColumns="False"
                                            CanUserSortColumns="False" CanUserReorderColumns="False"
                                            CanUserDragRows="True" RowDropCaught="ComparisonFieldGrid_OnRowDropCaught"
                                            ScrollViewer.VerticalScrollBarVisibility="Visible"
                                            ItemsSourceExtended="{Binding Path={x:Static local:FieldPickerViewModel.FieldPickerRowsProperty}}"
                                            g:SelectedItemsBehaviour.SynchroniseSelectedItems="{Binding Path={x:Static local:FieldPickerViewModel.SelectedFieldPickerRowsProperty}}"
                                            RowItemMouseDoubleClick="ComparisonFieldGrid_OnRowItemMouseDoubleClick"
                                            x:Name="xComparisonFieldGrid"
                                PreviewKeyDown="XComparisonFieldGrid_OnPreviewKeyDown">
                <g:ExtendedDataGrid.Columns>

                    <g:DataGridExtendedTextColumn Header="{x:Static lg:Message.FieldPicker_Column_Field}" IsReadOnly="True"
                                                  Binding="{Binding Path=(local:FieldPickerRow.Header), Mode=OneWay}" />

                    <g:DataGridExtendedTextColumn Header="{x:Static lg:Message.FieldPicker_Column_DisplayName}"
                                                  Binding="{Binding Path=(local:FieldPickerRow.DisplayName), UpdateSourceTrigger=PropertyChanged}" />

                    <g:DataGridExtendedCheckBoxColumn Header="{x:Static lg:Message.FieldPicker_Column_Display}"
                                                      Binding="{Binding Path=(local:FieldPickerRow.Display), UpdateSourceTrigger=PropertyChanged}" />

                </g:ExtendedDataGrid.Columns>
                <g:ExtendedDataGrid.SortByDescriptions>
                    <componentModel:SortDescription PropertyName="Number"/>
                </g:ExtendedDataGrid.SortByDescriptions>
            </g:ExtendedDataGrid>

        </Grid>
        <!--endregion: Comparison Fields-->

        <!--region: Sort Selection Buttons-->
        <StackPanel Grid.Row="4" Grid.Column="1" Margin="2" VerticalAlignment="Center"
                                    HorizontalAlignment="Center" Orientation="Vertical">

            <Button Margin="0,2,0,2" Padding="2"
                                    Command="{Binding Path={x:Static local:FieldPickerViewModel.MoveFieldUpCommandProperty}}"
                    ToolTip="{x:Static lg:Message.ColumnLayoutEditor_MoveColumnUp_Description}">
                <Image Height="16" Width="16"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, Path=Command.(vm:RelayCommand.SmallIcon)}" />
            </Button>

            <Button Margin="0,2,0,2" Padding="2"
                                    Command="{Binding Path={x:Static local:FieldPickerViewModel.MoveFieldDownCommandProperty}}"
                    ToolTip="{x:Static lg:Message.ColumnLayoutEditor_MoveColumnDown_Description}">

                <Image Height="16" Width="16"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, Path=Command.(vm:RelayCommand.SmallIcon)}" />

            </Button>

        </StackPanel>
        <!--endregion: Sort Selection Buttons-->
    </Grid>
</UserControl>
