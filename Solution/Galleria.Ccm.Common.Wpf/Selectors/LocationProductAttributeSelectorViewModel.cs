﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 820
// V8-30733 : L.Luong
//	Created.
#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll functionality; amended AddSelected and RemoveSelected commands.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// A view model for the LocationProductAttributeSelector view.
    /// </summary>
    public class LocationProductAttributeSelectorViewModel : ViewModelObject
    {
        #region Fields

        Boolean? _dialogResult;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableAttributesProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.AvailableAttributes);
        public static readonly PropertyPath SelectedAttributesProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.SelectedAttributes);
        public static readonly PropertyPath AvailableAttributesSelectionProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.AvailableAttributesSelection);
        public static readonly PropertyPath AddSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.AddSelectedAttributesCommand);
        public static readonly PropertyPath RemoveSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.RemoveSelectedAttributesCommand);
        public static readonly PropertyPath SelectedAttributesSelectionProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.SelectedAttributesSelection);
        public static readonly PropertyPath OKCommandProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<LocationProductAttributeSelectorViewModel>(p => p.CancelCommand);
        #endregion

        #region Properties

        /// <summary>
        /// The attributes that are available for selection.
        /// </summary>
        public BulkObservableCollection<ObjectFieldInfo> AvailableAttributes { get; set; }

        /// <summary>
        /// The attributes that have been selected.
        /// </summary>
        public BulkObservableCollection<ObjectFieldInfo> SelectedAttributes { get; set; }

        /// <summary>
        /// The available attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<ObjectFieldInfo> AvailableAttributesSelection { get; set; }

        /// <summary>
        /// The selected attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<ObjectFieldInfo> SelectedAttributesSelection { get; set; }

        /// <summary>
        /// Indicates whether or not the user has clicked Ok or cancel.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (_dialogResult.HasValue)
                {
                    // Fire the dialog result set event. This should be subscribed to by the view
                    // so that it can respond appropriately.
                    if (DialogResultSet != null) DialogResultSet(this, EventArgs.Empty);
                }
            }
        }

        #endregion

        #region Constructor
        
        /// <summary>
        /// Instantiates a new view model.
        /// </summary>
        /// <param name="includeCustomFields">True indidicates that custom attributes should be included in the available attributes.</param>
        /// <param name="currentSelection">The current attributes selection, if any, that should be pre-selected. Can be null.</param>
        /// <param name="includeProperties">A collection of properties to display, arrange as a list of tuples containing the source type and property name.</param>
        /// <remarks>When using this view model, the view should subscribe to the DialogResultSet event.</remarks>
        public LocationProductAttributeSelectorViewModel(
            IEnumerable<String> currentSelection = null)
        {
            // Populate the available attributes with those from Planogram Product inlcuding custom and performance properties.
            AvailableAttributes = new BulkObservableCollection<ObjectFieldInfo>();
            AvailableAttributes.AddRange(LocationProductAttribute.
                EnumerateDisplayableFieldInfos());

            // Create empty collections for the selection properties.
            SelectedAttributes = new BulkObservableCollection<ObjectFieldInfo>();
            AvailableAttributesSelection = new BulkObservableCollection<ObjectFieldInfo>();
            SelectedAttributesSelection = new BulkObservableCollection<ObjectFieldInfo>();

            SelectedAttributes.AddRange(AvailableAttributes.Where(a => currentSelection.Contains(a.PropertyName)));
            AvailableAttributes.RemoveRange(SelectedAttributes);
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        DisabledReason = Message.ProductAttributeSelector_Ok_DisabledReason
                    };
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            // V8-28944
            // Ok should always be enabled.
            return true;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedAttributesCommand

        private RelayCommand _addSelectedAttributesCommand;

        /// <summary>
        ///     Adds any selected available Attributes to the current view Attribute list
        /// </summary>
        public RelayCommand AddSelectedAttributesCommand
        {
            get
            {
                if (_addSelectedAttributesCommand != null) return _addSelectedAttributesCommand;

                _addSelectedAttributesCommand = new RelayCommand(
                    p => AddSelectedAttributes_Executed(),
                    p => AddSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.ProductAttributeSelector_AddSelectedAttribute,
                    SmallIcon = ImageResources.Add_16,
                    DisabledReason = Message.ProductAttributeSelector_AddSelectedAttribute_DisabledReason
                };

                return _addSelectedAttributesCommand;
            }
        }

        private Boolean AddSelectedAttributes_CanExecute()
        {
            return AvailableAttributesSelection.Count > 0;
        }

        private void AddSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<ObjectFieldInfo> rowsToAdd = new List<ObjectFieldInfo>();
            foreach (ObjectFieldInfo row in AvailableAttributesSelection)
            {
                if (!SelectedAttributes.Contains(row)) rowsToAdd.Add(row);
            }

            try
            {
                AvailableAttributes.RemoveRange(rowsToAdd);
                SelectedAttributes.AddRange(rowsToAdd);
                if (AvailableAttributesSelection.Any())
                {
                    AvailableAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #region RemoveSelectedAttributesCommand

        private RelayCommand _removeSelectedAttributesCommand;

        /// <summary>
        ///     Remove any selected available Attributes from the current view Attribute list
        /// </summary>
        public RelayCommand RemoveSelectedAttributesCommand
        {
            get
            {
                if (_removeSelectedAttributesCommand != null) return _removeSelectedAttributesCommand;

                _removeSelectedAttributesCommand = new RelayCommand(
                    p => RemoveSelectedAttributes_Executed(),
                    p => RemoveSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.ProductAttributeSelector_RemoveSelectedAttributes,
                    SmallIcon = ImageResources.Delete_16,
                    DisabledReason = Message.ProductAttributeSelector_RemoveSelectedAttributes_DisabledReason
                };

                return _removeSelectedAttributesCommand;
            }
        }

        private Boolean RemoveSelectedAttributes_CanExecute()
        {
            return SelectedAttributesSelection.Count > 0;
        }

        private void RemoveSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<ObjectFieldInfo> rowsToAdd = new List<ObjectFieldInfo>();
            foreach (ObjectFieldInfo row in SelectedAttributesSelection)
            {
                if (!AvailableAttributes.Contains(row)) rowsToAdd.Add(row);
            }
            try
            {
                SelectedAttributes.RemoveRange(rowsToAdd);
                AvailableAttributes.AddRange(rowsToAdd);
                if (SelectedAttributesSelection.Any())
                {
                    SelectedAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // Nothing to dispose.
        }
        #endregion
    }
}