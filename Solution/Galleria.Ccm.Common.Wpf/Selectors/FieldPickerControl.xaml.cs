﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31763 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Aspose.Cells.Drawing;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for FieldPickerControl.xaml
    /// </summary>
    public sealed partial class FieldPickerControl
    {
        #region Properties

        #region View Model

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof (FieldPickerViewModel),
                                        typeof (FieldPickerControl),
                                        new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var control = (FieldPickerControl) obj;

            if (e.OldValue != null)
            {
                var oldModel = (FieldPickerViewModel) e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.FieldOrderChanged -= control.OnViewModelFieldOrderChanged;
            }

            if (e.NewValue == null) return;

            var newModel = (FieldPickerViewModel)e.NewValue;
            newModel.AttachedControl = control;
            newModel.FieldOrderChanged += control.OnViewModelFieldOrderChanged;
        }

        public FieldPickerViewModel ViewModel
        {
            get { return (FieldPickerViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public FieldPickerControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked whenever the View Model fires a FieldOrderChanged event.
        /// </summary>
        private void OnViewModelFieldOrderChanged(Object sender, EventArgs e)
        {
            if (xComparisonFieldGrid == null) return;

            //  Forcibly reapply the column sorting.
            try
            {
                SortDescription[] currentSorting = xComparisonFieldGrid.SortByDescriptions.ToArray();
                if (currentSorting.Any())
                {
                    xComparisonFieldGrid.SortByDescriptions.Clear();
                    foreach (SortDescription sortDescription in currentSorting)
                    {
                        xComparisonFieldGrid.SortByDescriptions.Add(sortDescription);
                    }
                }
                xComparisonFieldGrid.Items.Refresh();
            }
            catch (InvalidOperationException) { }
        }

        private void FieldGrid_OnRowDropCaught(Object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (!Equals(e.SourceGrid, xComparisonFieldGrid) ||
                ViewModel == null) return;

            ViewModel.RemoveSelectedFieldsCommand.Execute();
        }

        private void FieldGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem == null ||
                ViewModel == null) return;

            ViewModel.AddSelectedFieldsCommand.Execute();
        }

        private void ComparisonFieldGrid_OnRowDropCaught(Object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (!Equals(e.SourceGrid, xFieldGrid) ||
                ViewModel == null) return;

            ViewModel.AddSelectedFieldsCommand.Execute();
        }

        private void ComparisonFieldGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem == null ||
                ViewModel == null) return;

            ViewModel.RemoveSelectedFieldsCommand.Execute();
        }

        private void XFieldGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                if (senderControl?.SelectedItem == null ||
                ViewModel == null) return;

                ViewModel.AddSelectedFieldsCommand.Execute();
                Keyboard.Focus(xFieldGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xComparisonFieldGrid);
                e.Handled = true;
            }
        }

        private void XComparisonFieldGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                if (senderControl?.SelectedItem == null ||
                ViewModel == null) return;
                ViewModel.RemoveSelectedFieldsCommand.Execute();
                Keyboard.Focus(xComparisonFieldGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                var senderControl = sender as ExtendedDataGrid;
                //if check box, allow selection; otherwise give focus to other grid
                if (senderControl?.CurrentColumn is DataGridExtendedCheckBoxColumn) return;
                Keyboard.Focus(xFieldGrid);
                e.Handled = true;
            }
        }
        #endregion
    }
}