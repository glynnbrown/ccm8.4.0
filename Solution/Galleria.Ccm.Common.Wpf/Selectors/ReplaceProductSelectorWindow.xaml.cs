﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.3)
// V8-29723 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for ReplaceProductSelectorWindow.xaml
    /// </summary>
    public partial class ReplaceProductSelectorWindow
    {
        const String SelectReplacementProductCommandKey = "SelectReplacementProductCommand";
        const String RemoveReplaceProductCommandKey = "RemoveReplaceProductCommand";

        #region Fields

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReplaceProductSelectorViewModel), typeof(ReplaceProductSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReplaceProductSelectorWindow senderControl = (ReplaceProductSelectorWindow)obj;

            if (e.OldValue != null)
            {
                ReplaceProductSelectorViewModel oldModel = (ReplaceProductSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(SelectReplacementProductCommandKey);
                senderControl.Resources.Remove(RemoveReplaceProductCommandKey);
            }

            if (e.NewValue != null)
            {
                ReplaceProductSelectorViewModel newModel = (ReplaceProductSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(SelectReplacementProductCommandKey, newModel.SelectReplacementProductCommand);
                senderControl.Resources.Add(RemoveReplaceProductCommandKey, newModel.RemoveReplaceProductCommand);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public ReplaceProductSelectorViewModel ViewModel
        {
            get { return (ReplaceProductSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value);}
 
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public ReplaceProductSelectorWindow(ReplaceProductSelectorViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers


        #endregion

        #region Methods



        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}