﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 803
// V8-29643 : D.Pleasance
//	Created.
#endregion
#region Version History: CCM 810
// V8-30063 : D.Pleasance
//	Added Inventory settings
#endregion
#region Version History: CCM820
// V8-31128 : D.Pleasance
//  Added Info settings
#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll functionality; amended AddSelected and RemoveSelected commands.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// A view model for the PlanogramAttributeSelector view.
    /// </summary>
    public class PlanogramAttributeSelectorViewModel : ViewModelObject
    {
        #region Fields

        Boolean? _dialogResult;
        private ReadOnlyCollection<PlanogramAttributeSelectorGroup> _availablePlanogramAttributeSelectorGroups;
        private PlanogramAttributeSelectorGroup _selectedPlanogramAttributeSelectorGroup;

        private readonly BulkObservableCollection<PlanogramAttributeSelectorRow> _availableAttributes = new BulkObservableCollection<PlanogramAttributeSelectorRow>();
        private ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow> _availableAttributesRO;

        private readonly BulkObservableCollection<PlanogramAttributeSelectorRow> _selectedAttributes = new BulkObservableCollection<PlanogramAttributeSelectorRow>();
        private ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow> _selectedAttributesRO;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailablePlanogramAttributeSelectorGroupsProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.AvailablePlanogramAttributeSelectorGroups);
        public static readonly PropertyPath SelectedPlanogramAttributeSelectorGroupProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.SelectedPlanogramAttributeSelectorGroup);

        public static readonly PropertyPath AvailableAttributesProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.AvailableAttributes);
        public static readonly PropertyPath SelectedAttributesProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.SelectedAttributes);
        public static readonly PropertyPath AvailableAttributesSelectionProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.AvailableAttributesSelection);
        public static readonly PropertyPath AddSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.AddSelectedAttributesCommand);
        public static readonly PropertyPath RemoveSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.RemoveSelectedAttributesCommand);
        public static readonly PropertyPath SelectedAttributesSelectionProperty = 
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.SelectedAttributesSelection);
        public static readonly PropertyPath OkCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramAttributeSelectorViewModel>(p => p.CancelCommand);
        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of available field groups
        /// </summary>
        public ReadOnlyCollection<PlanogramAttributeSelectorGroup> AvailablePlanogramAttributeSelectorGroups
        {
            get { return _availablePlanogramAttributeSelectorGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected field group
        /// </summary>
        public PlanogramAttributeSelectorGroup SelectedPlanogramAttributeSelectorGroup
        {
            get { return _selectedPlanogramAttributeSelectorGroup; }
            set
            {
                _selectedPlanogramAttributeSelectorGroup = value;
                OnPropertyChanged(SelectedPlanogramAttributeSelectorGroupProperty);

                OnSelectedPlanogramAttributeSelectorGroupChanged(value);
            }
        }
        

        /// <summary>
        /// The attributes that are available for selection.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow> AvailableAttributes
        {
            get
            {
                if (_availableAttributesRO == null)
                {
                    _availableAttributesRO = new ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow>(_availableAttributes);
                }
                return _availableAttributesRO;
            }
        }


        /// <summary>
        /// The attributes that have been selected.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow> SelectedAttributes
        {
            get
            {
                if (_selectedAttributesRO == null)
                {
                    _selectedAttributesRO = new ReadOnlyBulkObservableCollection<PlanogramAttributeSelectorRow>(_selectedAttributes);
                }
                return _selectedAttributesRO;
            }
        }


        /// <summary>
        /// The available attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<PlanogramAttributeSelectorRow> AvailableAttributesSelection { get; set; }

        /// <summary>
        /// The selected attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<PlanogramAttributeSelectorRow> SelectedAttributesSelection { get; set; }

        /// <summary>
        /// Indicates whether or not the user has clicked Ok or cancel.
        /// </summary>
        public Boolean? DialogResult 
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (_dialogResult.HasValue)
                {
                    // Fire the dialog result set event. This should be subscribed to by the view
                    // so that it can respond appropriately.
                    if(DialogResultSet != null) DialogResultSet(this,EventArgs.Empty);
                }
            }
        }
        #endregion

        #region Constructor
        
        /// <summary>
        /// Instantiates a new view model.
        /// </summary>
        /// <param name="currentSelection">The current attributes selection, if any, that should be pre-selected. Can be null.</param>
        /// <remarks>When using this view model, the view should subscribe to the DialogResultSet event.</remarks>
        public PlanogramAttributeSelectorViewModel(
            IEnumerable<Tuple<PlanogramAttributeSourceType,String, Object>> currentSelection = null)
        {
            List<PlanogramAttributeSelectorGroup> groups = new List<PlanogramAttributeSelectorGroup>();

            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            IEnumerable<ObjectFieldInfo> planogramInventoryAttributes = PlanogramInventory.EnumerateAttributeFieldInfos();
            
            // Create groups
            PlanogramAttributeSelectorGroup generalGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_General, PlanogramAttributeSourceType.General, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_General)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup customDataTextGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_CustomDataText, PlanogramAttributeSourceType.CustomDataText, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataText)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup customDataValueGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_CustomDataValue, PlanogramAttributeSourceType.CustomDataValue, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataValue)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup customDataFlagGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_CustomDataFlag, PlanogramAttributeSourceType.CustomDataFlag, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataFlag)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup customDataDateGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_CustomDataDate, PlanogramAttributeSourceType.CustomDataDate, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataDate)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup customDataNoteGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_CustomDataNote, PlanogramAttributeSourceType.CustomDataNote, planogramAttributes.Where(p => p.GroupName.Equals(
                           Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataNote)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup settingsGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_Settings, PlanogramAttributeSourceType.Settings, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_Settings)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup infoGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_Info, PlanogramAttributeSourceType.Info, planogramAttributes.Where(p => p.GroupName.Equals(
                            Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_Info)).Select(f => new PlanogramAttributeSelectorRow(f)));

            PlanogramAttributeSelectorGroup inventoryGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_Inventory, PlanogramAttributeSourceType.Inventory, planogramInventoryAttributes.Select(f => new PlanogramAttributeSelectorRow(f)));

            List<PlanogramAttributeSelectorRow> attributes = new List<PlanogramAttributeSelectorRow>();
            attributes.AddRange(generalGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(customDataTextGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(customDataValueGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(customDataFlagGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(customDataDateGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(customDataNoteGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(settingsGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(infoGroup.PlanogramAttributeSelectorRows);
            attributes.AddRange(inventoryGroup.PlanogramAttributeSelectorRows);
            PlanogramAttributeSelectorGroup allGroup = new PlanogramAttributeSelectorGroup(Message.PlanogramAttributeSelector_All, null, attributes);

            groups.Add(allGroup);
            groups.Add(generalGroup);
            groups.Add(customDataTextGroup);
            groups.Add(customDataValueGroup);
            groups.Add(customDataFlagGroup);
            groups.Add(customDataDateGroup);
            groups.Add(customDataNoteGroup);
            groups.Add(settingsGroup);
            groups.Add(infoGroup);
            groups.Add(inventoryGroup);

            _availablePlanogramAttributeSelectorGroups = groups.AsReadOnly();
            SelectedPlanogramAttributeSelectorGroup = _availablePlanogramAttributeSelectorGroups[0];

            // Create empty collections for the selection properties.
            AvailableAttributesSelection = new BulkObservableCollection<PlanogramAttributeSelectorRow>();
            SelectedAttributesSelection = new BulkObservableCollection<PlanogramAttributeSelectorRow>();

            // Finally, for each item in the current selection, move these items from the available
            // to the selected list.
            if (currentSelection != null)
            {
                List<PlanogramAttributeSelectorRow> currentAttributeSelection = new List<PlanogramAttributeSelectorRow>();
                foreach (Tuple<PlanogramAttributeSourceType, String, Object> sourceAndProperty in currentSelection)
                {
                    PlanogramAttributeSelectorGroup planogramAttributeSelectorGroup = groups.Where(p => p.SourceType == sourceAndProperty.Item1).FirstOrDefault();

                    if (planogramAttributeSelectorGroup != null)
                    {
                        PlanogramAttributeSelectorRow planogramAttributeSelectorRow = planogramAttributeSelectorGroup.PlanogramAttributeSelectorRows.Where(a =>
                                a.Source == sourceAndProperty.Item1 && a.PropertyName.Equals(sourceAndProperty.Item2)).FirstOrDefault();

                        if (planogramAttributeSelectorRow != null)
                        {
                            planogramAttributeSelectorRow.ApplyPropertyValue(sourceAndProperty.Item3);
                            currentAttributeSelection.Add(planogramAttributeSelectorRow);
                        }
                    }
                }
                IEnumerable<PlanogramAttributeSelectorRow> distinctCurrentAttributeSelection =
                    currentAttributeSelection.Distinct().ToList();
                _selectedAttributes.AddRange(distinctCurrentAttributeSelection);
                _availableAttributes.RemoveRange(distinctCurrentAttributeSelection);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedPlanogramAttributeSelectorGroupChanged(PlanogramAttributeSelectorGroup newValue)
        {
            _availableAttributes.Clear();
            _availableAttributes.AddRange(newValue.PlanogramAttributeSelectorRows.Except(_selectedAttributes));
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OkCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        DisabledReason = Message.PlanogramAttributeSelector_Ok_DisabledReason
                    };
                }
                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            // V8-28944
            // Ok should always be enabled.
            return true;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedAttributesCommand

        private RelayCommand _addSelectedAttributesCommand;

        /// <summary>
        ///     Adds any selected available Attributes to the current view Attribute list
        /// </summary>
        public RelayCommand AddSelectedAttributesCommand
        {
            get
            {
                if (_addSelectedAttributesCommand != null) return _addSelectedAttributesCommand;

                _addSelectedAttributesCommand =
                    new RelayCommand(p => AddSelectedAttributes_Executed(), p => AddSelectedAttributes_CanExecute())
                    {
                        FriendlyDescription = Message.PlanogramAttributeSelector_AddSelectedAttribute,
                        SmallIcon = ImageResources.Add_16,
                        DisabledReason = Message.PlanogramAttributeSelector_AddSelectedAttribute_DisabledReason
                    };
                
                return _addSelectedAttributesCommand;
            }
        }

        private Boolean AddSelectedAttributes_CanExecute()
        {
            return AvailableAttributesSelection.Count > 0;
        }

        private void AddSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<PlanogramAttributeSelectorRow> rowsToAdd = AvailableAttributesSelection.Where(row => !SelectedAttributes.Contains(row)).ToList();

            try
            {
                _availableAttributes.RemoveRange(rowsToAdd);
                _selectedAttributes.AddRange(rowsToAdd);
                if (AvailableAttributesSelection.Any())
                {
                    AvailableAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #region RemoveSelectedAttributesCommand

        private RelayCommand _removeSelectedAttributesCommand;

        /// <summary>
        ///     Remove any selected available Attributes from the current view Attribute list
        /// </summary>
        public RelayCommand RemoveSelectedAttributesCommand
        {
            get
            {
                if (_removeSelectedAttributesCommand != null) return _removeSelectedAttributesCommand;

                _removeSelectedAttributesCommand =
                    new RelayCommand(p => RemoveSelectedAttributes_Executed(), p => RemoveSelectedAttributes_CanExecute())
                    {
                        FriendlyDescription = Message.PlanogramAttributeSelector_RemoveSelectedAttributes,
                        SmallIcon = ImageResources.Delete_16,
                        DisabledReason = Message.PlanogramAttributeSelector_RemoveSelectedAttributes_DisabledReason
                    };

                return _removeSelectedAttributesCommand;
            }
        }

        private Boolean RemoveSelectedAttributes_CanExecute()
        {
            return SelectedAttributesSelection.Count > 0;
        }

        private void RemoveSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<PlanogramAttributeSelectorRow> rowsToAdd = new List<PlanogramAttributeSelectorRow>();
            List<PlanogramAttributeSelectorRow> rowsToRemove = new List<PlanogramAttributeSelectorRow>();
            foreach(PlanogramAttributeSelectorRow row in SelectedAttributesSelection)
            {
                if (SelectedPlanogramAttributeSelectorGroup.SourceType == null || row.Source == SelectedPlanogramAttributeSelectorGroup.SourceType)
                {
                    rowsToAdd.Add(row);
                }

                if (!AvailableAttributes.Contains(row)) rowsToRemove.Add(row);
            }
            try
            {
                _selectedAttributes.RemoveRange(rowsToRemove);
                _availableAttributes.AddRange(rowsToAdd);
                if (SelectedAttributesSelection.Any())
                {
                    SelectedAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // Nothing to dispose.
        }
        #endregion
    }

    /// <summary>
    /// Represents a group of planogram attributes that may be selected.
    /// </summary>
    public class PlanogramAttributeSelectorGroup
    {
        #region Fields
        private String _name;
        private ReadOnlyCollection<PlanogramAttributeSelectorRow> _planogramAttributeSelectorRows;
        private PlanogramAttributeSourceType? _planogramAttributeSourceType;
        #endregion

        #region Properties

        public String Name
        {
            get { return _name; }
        }

        public ReadOnlyCollection<PlanogramAttributeSelectorRow> PlanogramAttributeSelectorRows
        {
            get { return _planogramAttributeSelectorRows; }
        }

        public PlanogramAttributeSourceType? SourceType
        {
            get { return _planogramAttributeSourceType; }
        }

        #endregion

        #region Constructor

        public PlanogramAttributeSelectorGroup(String name, PlanogramAttributeSourceType? planogramAttributeSourceType, IEnumerable<PlanogramAttributeSelectorRow> planogramAttributeSelectorRows)
        {
            _name = name;
            _planogramAttributeSourceType = planogramAttributeSourceType;
            _planogramAttributeSelectorRows = new ReadOnlyCollection<PlanogramAttributeSelectorRow>(planogramAttributeSelectorRows.ToList());
        }

        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }
}