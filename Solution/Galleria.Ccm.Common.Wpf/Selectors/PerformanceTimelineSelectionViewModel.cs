﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31830 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Csla;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.ComponentModel;
using System.Collections.Specialized;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for performance selector window.
    /// </summary>
    public sealed class PerformanceTimelineSelectionViewModel : ViewModelAttachedControlObject<PerformanceTimelineSelectionWindow>
    {
        #region Fields

        private Boolean? _dialogResult;

        private Boolean _isMultiSelectEnabled = false;
        private PerformanceSelection _currentItem;
        private GFSModel.PerformanceSource _performanceSource;

        private readonly GFSModel.TimelineGroupList _timelineGroupList;
        private String _warningText;
        private BulkObservableCollection<TimelineGroupView> _timelineGroups = new BulkObservableCollection<TimelineGroupView>();
        private ReadOnlyBulkObservableCollection<TimelineGroupView> _timelineGroupsRO;

        
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsMultiSelectEnabledProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.IsMultiSelectEnabled);
        public static readonly PropertyPath IsOrderableProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.IsOrderable);
        public static readonly PropertyPath CurrentItemProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.CurrentItem);
        public static readonly PropertyPath PerformanceSourceProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.PerformanceSource);
        public static readonly PropertyPath TimelineGroupsProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.TimelineGroups);
        public static readonly PropertyPath SelectedTimelineGroupsCountProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.SelectedTimelineGroupsCount);
        public static readonly PropertyPath WarningTextProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.WarningText);

        //Commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PerformanceTimelineSelectionViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties
        
        /// <summary>
        /// Returns the current performance selection.
        /// </summary>
        public PerformanceSelection CurrentItem
        {
            get { return _currentItem; }
            private set
            {
                PerformanceSelection oldValue = _currentItem;
                _currentItem = value;
                OnPropertyChanged(CurrentItemProperty);
            }
        }

        /// <summary>
        /// Returns the current performance selections performance source.
        /// </summary>
        public GFSModel.PerformanceSource PerformanceSource
        {
            get { return _performanceSource; }
        }

        /// <summary>
        /// Returns the collection of timeline groups.
        /// </summary>
        public ReadOnlyBulkObservableCollection<TimelineGroupView> TimelineGroups
        {
            get
            {
                if (_timelineGroupsRO == null)
                {
                    _timelineGroupsRO = new ReadOnlyBulkObservableCollection<TimelineGroupView>(_timelineGroups);
                }
                return _timelineGroupsRO;
            }
        }

        /// <summary>
        /// Returns a count of the number of selected timeline groups.
        /// </summary>
        public Int32 SelectedTimelineGroupsCount
        {
            get { return TimelineGroups.Count(t => t.IsSelected); }
        }

        /// <summary>
        /// Gets a warning to be shown when an existing performance selection is loaded.
        /// This may be that the performance source is 
        /// </summary>
        public String WarningText
        {
            get { return _warningText; }
            private set
            {
                _warningText = value;
                OnPropertyChanged(WarningTextProperty);
            }
        }


        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns true if multi select is enabled.
        /// </summary>
        public Boolean IsMultiSelectEnabled
        {
            get { return _isMultiSelectEnabled; }
        }
        
        /// <summary>
        /// Indicates whether or not the selected products can be ordered manually.
        /// </summary>
        public Boolean IsOrderable { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor- used for multi select
        /// </summary>
        /// <param name="assignedProducts"></param>
        public PerformanceTimelineSelectionViewModel(PerformanceSelection performanceSelection, 
            GFSModel.PerformanceSource performanceSource, 
            GFSModel.TimelineGroupList timelineGroups, 
            IEnumerable<Int64> excludedTimelineGroupCodes)
        {
            //Attach to bulk collection changed event
            _timelineGroups.BulkCollectionChanged += TimelineGroups_BulkCollectionChanged;

            //Load performance timeline groups
            _performanceSource = performanceSource;
            _timelineGroupList = timelineGroups;

            //Set current performance selection
            this.CurrentItem = performanceSelection;

            //Update timeline groups
            UpdateTimelineGroups(excludedTimelineGroupCodes);
        }
        
        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            return true;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }


        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Event Handlers
        
        /// <summary>
        /// Called whenever the timeline groups collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineGroups_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (TimelineGroupView groupView in e.ChangedItems)
                    {
                        groupView.PropertyChanged += TimelineGroupView_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (TimelineGroupView groupView in e.ChangedItems)
                    {
                        groupView.PropertyChanged -= TimelineGroupView_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (TimelineGroupView groupView in e.ChangedItems)
                        {
                            groupView.PropertyChanged -= TimelineGroupView_PropertyChanged;
                        }
                    }
                    foreach (TimelineGroupView groupView in _timelineGroups)
                    {
                        groupView.PropertyChanged += TimelineGroupView_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a property changes on a timeline group view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineGroupView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimelineGroupView.IsSelectedProperty.Path)
            {
                //notify that the selection count has changed.
                OnPropertyChanged(SelectedTimelineGroupsCountProperty);
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Updates the timeline groups collection.
        /// </summary>
        private void UpdateTimelineGroups(IEnumerable<Int64> excludedTimelineGroupCodes)
        {
            if (this.CurrentItem == null) return;
            if (_timelineGroups.Count > 0) _timelineGroups.Clear();

            PerformanceSelectionTimeType timeType = this.CurrentItem.TimeType;

            List<TimelineGroupView> groups = new List<TimelineGroupView>();

            foreach (var gfsGroup in _timelineGroupList)
            {
                groups.Add(new TimelineGroupView(gfsGroup));
            }

            if (groups.Count > 0)
            {
                timeType = groups[0].TimelineFrequency;
            }
            
            if (this.CurrentItem != null)
            {
                List<Int64> unavailableGroups = new List<Int64>();

                foreach (Int64 selectedCode in
                    this.CurrentItem.TimelineGroups.Select(t => t.Code))
                {
                    TimelineGroupView group = groups.FirstOrDefault(g => g.Code == selectedCode);
                    if (group == null)
                    {
                        unavailableGroups.Add(selectedCode);
                    }
                    else
                    {
                        //Default all others to is selected of true
                        group.IsSelected = true;
                    }
                }

                //Ensure inclusions are set
                foreach (Int64 includedCode in groups.Select(p => p.Code).Except(excludedTimelineGroupCodes))
                {
                    TimelineGroupView group = groups.FirstOrDefault(g => g.Code == includedCode);
                    if (group != null)
                    {
                        if (this.CurrentItem.SelectionType == PerformanceSelectionType.Dynamic)
                        {
                            //Deselect for dynamic inclusion
                            group.IsSelected = false;
                        }
                        else
                        {
                            //Select for fixed inclusion
                            group.IsSelected = true;
                        }
                    }
                }

                //Ensure timelines are excluded
                foreach (Int64 excludedCode in excludedTimelineGroupCodes)
                {
                    if (excludedCode > 0)
                    {
                        TimelineGroupView group = groups.FirstOrDefault(g => g.Code == excludedCode);
                        if (group != null)
                        {
                            if (this.CurrentItem.SelectionType == PerformanceSelectionType.Dynamic)
                            {
                                //Select for dynamic exclusion
                                group.IsSelected = true;
                            }
                            else
                            {
                                //Deselect for fixed exclusion
                                group.IsSelected = false;
                            }
                        }
                    }
                }

                if (unavailableGroups.Count > 0 && this.WarningText == null)
                {
                    this.WarningText = Message.PerformanceTimelineSelector_UnavailableTimelineGroups;
                }
            }
            _timelineGroups.AddRange(groups.OrderBy(g => g.Code));
            
            OnPropertyChanged(SelectedTimelineGroupsCountProperty);
        }


        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _timelineGroups.Clear();
                _timelineGroups.BulkCollectionChanged -= TimelineGroups_BulkCollectionChanged;
                base.IsDisposed = true;
            }
        }

        #endregion
    }


    public sealed class TimelineGroupView : ModelView<GFSModel.TimelineGroup>
    {
        #region Fields
        private Boolean _isSelected;
        private PerformanceSelectionTimeType _timelineFrequency;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsSelectedProperty = WpfHelper.GetPropertyPath<TimelineGroupView>(p => p.IsSelected);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether 
        /// this timeline group is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged(IsSelectedProperty);
            }
        }

        /// <summary>
        /// Returns the timeline code.
        /// </summary>
        public Int64 Code
        {
            get { return Model.Code; }
        }

        public String Name
        {
            get { return Model.Name; }
        }

        public PerformanceSelectionTimeType TimelineFrequency
        {
            get { return _timelineFrequency; }
        }


        #endregion

        #region Constructor

        public TimelineGroupView(GFSModel.TimelineGroup model)
            : base(model)
        {
            ProcessCode();
        }

        #endregion

        private void ProcessCode()
        {
            String codeString = Code.ToString();

            switch (codeString.Length)
            {
                case 4:
                    _timelineFrequency = PerformanceSelectionTimeType.Year;
                    break;
                case 6:
                    _timelineFrequency = PerformanceSelectionTimeType.Quarter;
                    break;
                case 8:
                    _timelineFrequency = PerformanceSelectionTimeType.Month;
                    break;
                case 10:
                    _timelineFrequency = PerformanceSelectionTimeType.Week;
                    break;
                case 12:
                    _timelineFrequency = PerformanceSelectionTimeType.Day;
                    break;
                default:
                    _timelineFrequency = PerformanceSelectionTimeType.Week;
                    break;
            }

        }

        public override void Dispose()
        {

        }
    }
}
