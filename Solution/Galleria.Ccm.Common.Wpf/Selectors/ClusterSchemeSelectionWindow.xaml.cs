﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
// V8-28987 : I.George 
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Globalization;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for ClusterSchemeSelectionWindow.xaml
    /// </summary>
    public sealed partial class ClusterSchemeSelectionWindow : ExtendedRibbonWindow
    {
        #region Property

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ClusterSchemeSelectionViewModel), typeof(ClusterSchemeSelectionWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ClusterSchemeSelectionWindow senderControl = (ClusterSchemeSelectionWindow)obj;

            if (e.OldValue != null)
            {
                ClusterSchemeSelectionViewModel oldModel = (ClusterSchemeSelectionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ClusterSchemeSelectionViewModel newModel = (ClusterSchemeSelectionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public ClusterSchemeSelectionViewModel ViewModel
        {
            get { return (ClusterSchemeSelectionViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion
  
        #region Constructor

        public ClusterSchemeSelectionWindow(ClusterSchemeSelectionViewModel viewModel)
        {
           this.ViewModel = viewModel;
          InitializeComponent();

          this.Loaded += new RoutedEventHandler(ClusterSchemeSelectionWindow_Loaded);

        }

        #endregion

        #region Events

        private void ClusterSchemeSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ClusterSchemeSelectionWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }


        private void xSelectClusterSchemeGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.OKCommand.Execute();
        }
        #endregion

        private void rootControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.ViewModel.CancelCommand_Executed();
            }
        }

        private void xSelectClusterSchemeGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && xSelectClusterSchemeGrid.SelectionMode == DataGridSelectionMode.Single)
            {
                e.Handled = true;
                this.ViewModel.OKCommand_Executed();
            }
            else
            {
                return;
            }
        }
    }
}
