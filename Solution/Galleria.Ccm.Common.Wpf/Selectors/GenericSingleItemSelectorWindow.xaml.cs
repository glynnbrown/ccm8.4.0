﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// V8-26736 : L.Ineson ~ Copied from SA.

#endregion

#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Re-factored to use view model.
#endregion

#endregion


using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Windows.Input;

namespace Galleria.Ccm.Common.Wpf.Selectors
{

    /// <summary>
    /// Interaction logic for QuickItemsSelectDialog.xaml
    /// </summary>
    public partial class GenericSingleItemSelectorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(GenericSingleItemSelectorViewModel), typeof(GenericSingleItemSelectorWindow));

        /// <summary>
        /// Gets/Sets the item source for the available items
        /// </summary>
        public GenericSingleItemSelectorViewModel ViewModel
        {
            get { return (GenericSingleItemSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        /// <summary>
        /// Gets/Sets the item source for the available items
        /// </summary>
        public IEnumerable ItemSource
        {
            get { return ViewModel.ItemSource; }
            set { ViewModel.ItemSource = value; }
        }

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public IEnumerable SelectedItems
        {
            get { return ViewModel.SelectedItems; }
        }

        /// <summary>
        /// Gets/Sets the columnset to use to display the items
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return ViewModel.ColumnSet; }
            set { ViewModel.ColumnSet = value; }
        }

        /// <summary>
        /// Gets/Sets the selection mode
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return ViewModel.SelectionMode; }
            set { ViewModel.SelectionMode = value; }
        }

        /// <summary>
        /// Gets/Sets the extend column
        /// </summary>
        public Int32 ExtendColumn
        {
            get { return ViewModel.ExtendColumn; }
            set { ViewModel.ExtendColumn = value; }
        }

        /// <summary>
        /// Gets/Sets the default column to group by
        /// </summary>
        public String DefaultGroupBy
        {
            get { return ViewModel.DefaultGroupBy; }
            set { ViewModel.DefaultGroupBy = value; }
        }

        /// <summary>
        /// Gets/Sets if double click should return when the selection mode is extended
        /// </summary>
        public Boolean AllowDoubleClickOnExtendedSelection
        {
            get { return ViewModel.AllowDoubleClickOnExtendedSelection; }
            set { ViewModel.AllowDoubleClickOnExtendedSelection = value; }
        }

        #endregion

        #region Constructor

        public GenericSingleItemSelectorWindow(GenericSingleItemSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
            this.Loaded += QuickItemsSelectDialog_Loaded;
        }

        public GenericSingleItemSelectorWindow()
        {
            InitializeComponent();
            ViewModel = new GenericSingleItemSelectorViewModel();
            this.Loaded += QuickItemsSelectDialog_Loaded;
        }

        #endregion

        #region Event Handlers

        private void QuickItemsSelectDialog_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= QuickItemsSelectDialog_Loaded;

            CreateDefaultGroupByDescription();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            CommitSelection();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = this.ViewModel.DialogResult = false;
        }

        private void itemsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.SelectionMode == DataGridSelectionMode.Single)
            {
                CommitSelection();
            }
            else
            {
                //Only commit if the window should return on double click when selection mode is extended
                if (this.AllowDoubleClickOnExtendedSelection)
                {
                    CommitSelection();
                }
            }
        }
        /// <summary>
        /// Commits the selection on Return Key being pressed for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void itemsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && this.SelectionMode == DataGridSelectionMode.Single
                && itemsGrid.SelectedItem != null)
            {
                CommitSelection();
                e.Handled = true;
            }
            else
            {
                return;
            }
        }

        #endregion

        #region Methods

        private void CommitSelection()
        {
            this.ViewModel.SelectedItems = this.itemsGrid.SelectedItems;
            this.DialogResult = this.ViewModel.DialogResult = true;
        }

        /// <summary>
        /// Creates a default group by on the items grid
        /// </summary>
        private void CreateDefaultGroupByDescription()
        {
            if (!String.IsNullOrEmpty(this.DefaultGroupBy))
            {
                itemsGrid.GroupByDescriptions.Add(new PropertyGroupDescription(this.DefaultGroupBy));
            }
        }

        #endregion

    }

    /// <summary>
    /// A controlling view model for the <see cref="GenericSingleItemSelectorWindow"/> that can be 
    /// injected into the view's constructor.
    /// </summary>
    public sealed class GenericSingleItemSelectorViewModel : ViewModelObject
    {
        #region Fields 

        private IEnumerable _itemSource;
        private IEnumerable _selectedItems;
        private DataGridColumnCollection _columnSet;
        private DataGridSelectionMode _selectionMode;
        private Int32 _extendColumn;
        private String _defaultGroupBy;
        private Boolean _allowDoubleClickOnExtendedSelection;
        private String _title;
        private Boolean? _dialogResult;

        #endregion

        #region Property Paths

        public static PropertyPath ItemSourceProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.ItemSource);
        public static PropertyPath SelectedItemsProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.SelectedItems);
        public static PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.ColumnSet);
        public static PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.SelectionMode);
        public static PropertyPath ExtendColumnProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.ExtendColumn);
        public static PropertyPath DefaultGroupByProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.DefaultGroupBy);
        public static PropertyPath AllowDoubleClickOnExtendedSelectionProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.AllowDoubleClickOnExtendedSelection);
        public static PropertyPath TitleProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.Title);
        public static PropertyPath DialogResultProperty = WpfHelper.GetPropertyPath<GenericSingleItemSelectorViewModel>(v => v.DialogResult);

        #endregion

        #region Properties

        /// <summary>
        /// The result of the view.
        /// </summary>
        public Boolean? DialogResult 
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                OnPropertyChanged(DialogResultProperty);
            }
        }

        /// <summary>
        /// The title that the view should display.
        /// </summary>
        public String Title 
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(TitleProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the item source for the available items
        /// </summary>
        public IEnumerable ItemSource
        {
            get { return _itemSource; }
            set 
            { 
                _itemSource = value;
                OnPropertyChanged(ItemSourceProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public IEnumerable SelectedItems
        {
            get { return _selectedItems; }
            set 
            { 
                _selectedItems = value;
                OnPropertyChanged(SelectedItemsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the columnset to use to display the items
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
            set 
            { 
                _columnSet = value;
                OnPropertyChanged(ColumnSetProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selection mode
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set 
            { 
                _selectionMode = value;
                OnPropertyChanged(SelectionModeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the extend column
        /// </summary>
        public Int32 ExtendColumn
        {
            get { return _extendColumn; }
            set 
            {
                _extendColumn = value;
                OnPropertyChanged(ExtendColumnProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the default column to group by
        /// </summary>
        public String DefaultGroupBy
        {
            get { return _defaultGroupBy; }
            set 
            { 
                _defaultGroupBy = value; 
                OnPropertyChanged(DefaultGroupByProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if double click should return when the selection mode is extended
        /// </summary>
        public Boolean AllowDoubleClickOnExtendedSelection
        {
            get { return _allowDoubleClickOnExtendedSelection; }
            set 
            { 
                _allowDoubleClickOnExtendedSelection = value;
                OnPropertyChanged(AllowDoubleClickOnExtendedSelectionProperty);
            }
        }

        #endregion

        #region Constructors

        public GenericSingleItemSelectorViewModel(String title = null)
        {
            if (String.IsNullOrEmpty(title))
            {
                Title = Message.GenericSingleItemSelector_Title;
            }
            else
            {
                Title = title;
            }
        }

        #endregion

        #region Methods
        protected override void Dispose(Boolean disposing)
        {
            base.Dispose();
        } 
        #endregion

    }
}
