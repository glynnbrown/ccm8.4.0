﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-26705 : A.Kuszyk
//  Moved to Common.Wpf from Workflow client.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public enum ProductSearchType
    {
        Criteria,
        ProductUniverse,
    }

    public static class ProductSearchTypeHelper
    {
        public static readonly Dictionary<ProductSearchType, String> FriendlyNames =
            new Dictionary<ProductSearchType, String>()
            {
                {ProductSearchType.Criteria, Message.Enum_ProductSearchType_Criteria},
                {ProductSearchType.ProductUniverse, Message.Enum_ProductSearchType_ProductUniverse}
            };
    }
}
