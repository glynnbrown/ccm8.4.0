﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for LocationSpaceElementAttributeSelectorWindow.xaml
    /// </summary>
    public partial class LocationSpaceElementAttributeSelectorWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(LocationSpaceElementAttributeSelectorViewModel), typeof(LocationSpaceElementAttributeSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// This view's view model.
        /// </summary>
        public LocationSpaceElementAttributeSelectorViewModel ViewModel
        {
            get { return (LocationSpaceElementAttributeSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Connects a new view model to the window.
        /// </summary>
        /// <param name="newViewModel">View model to attach to the window.</param>
        private void AttachViewModel(LocationSpaceElementAttributeSelectorViewModel newViewModel)
        {
            if (newViewModel == null) return;
            newViewModel.DialogResultSet += OnViewModelDialogResultSet;
        }

        /// <summary>
        ///     Disconnects an old view model from the window.
        /// </summary>
        /// <param name="oldViewModel">View model to dettach from the window.</param>
        private void DettachViewModel(LocationSpaceElementAttributeSelectorViewModel oldViewModel)
        {
            if (oldViewModel == null) return;
            oldViewModel.DialogResultSet -= OnViewModelDialogResultSet;
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = (LocationSpaceElementAttributeSelectorWindow)obj;

            sender.DettachViewModel(e.OldValue as LocationSpaceElementAttributeSelectorViewModel);
            sender.AttachViewModel(e.NewValue as LocationSpaceElementAttributeSelectorViewModel);
        }

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new product attribute selector view.
        /// </summary>
        public LocationSpaceElementAttributeSelectorWindow(LocationSpaceElementAttributeSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        } 
        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xAvailableAttributesGrid_OnMouseDoubleClick(Object sender, MouseEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xAvailableAttributesGrid)) return;
            if (!ViewModel.AvailableAttributesSelection.Any()) return;

            ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the available attributes grid
        /// </summary>
        private void xAvailableAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the selected attributes grid.
        /// </summary>
        private void xSelectedAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xSelectedAttributesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xSelectedAttributesGrid)) return;

            if (!ViewModel.SelectedAttributes.Any()) return;

            ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null || !ViewModel.DialogResult.HasValue) return;
            this.DialogResult = ViewModel.DialogResult;
            this.Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel != null)
            {
                ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
                ViewModel.Dispose();
            }
        }
        #endregion

        private void XAvailableAttributesGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel == null || !Equals(sender, this.xAvailableAttributesGrid)) return;
                if (!ViewModel.AvailableAttributesSelection.Any()) return;
                ViewModel.AddSelectedAttributesCommand.Execute();
                Keyboard.Focus(xAvailableAttributesGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xSelectedAttributesGrid);
                e.Handled = true;
            }
        }

        private void XSelectedAttributesGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel == null || !Equals(sender, this.xSelectedAttributesGrid)) return;
                if (!ViewModel.SelectedAttributes.Any()) return;
                ViewModel.RemoveSelectedAttributesCommand.Execute();
                Keyboard.Focus(xSelectedAttributesGrid);
                e.Handled = true;             
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xAvailableAttributesGrid);
                e.Handled = true;
            }
        }
    }
}