﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31560 : D.Pleasance
//  Initial version.
// CCM-18493 : L.Bailey
// Decrease Units and Facings by Product fail

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Csla;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for decrease product selector window.
    /// </summary>
    public class DecreaseProductSelectorViewModel : ViewModelAttachedControlObject<DecreaseProductSelectorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private Boolean _isUpdatingMultiInput;

        private BulkObservableCollection<DecreaseProductRow> _decreaseProductRows = new BulkObservableCollection<DecreaseProductRow>();
        private ReadOnlyBulkObservableCollection<DecreaseProductRow> _decreaseProductRowsRO;
        private DecreaseProductRow _selectedDecreaseProductRow;
        private ObservableCollection<DecreaseProductRow> _selectedDecreaseProductRows = new ObservableCollection<DecreaseProductRow>();


        #endregion

        #region Binding Property Paths

        //Commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<DecreaseProductSelectorViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<DecreaseProductSelectorViewModel>(p => p.CancelCommand);
        public static PropertyPath DecreaseProductRowsProperty = WpfHelper.GetPropertyPath<DecreaseProductSelectorViewModel>(p => p.DecreaseProductRows);
        public static PropertyPath SelectedDecreaseProductRowProperty = WpfHelper.GetPropertyPath<DecreaseProductSelectorViewModel>(p => p.SelectedDecreaseProductRow);
        public static readonly PropertyPath SelectedDecreaseProductRowsProperty = WpfHelper.GetPropertyPath<DecreaseProductSelectorViewModel>(p => p.SelectedDecreaseProductRows);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the collection of products to decrease
        /// </summary>
        public ReadOnlyBulkObservableCollection<DecreaseProductRow> DecreaseProductRows
        {
            get
            {
                if (_decreaseProductRowsRO == null)
                {
                    _decreaseProductRowsRO = new ReadOnlyBulkObservableCollection<DecreaseProductRow>(_decreaseProductRows);
                }
                return _decreaseProductRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected decrease product
        /// </summary>
        public DecreaseProductRow SelectedDecreaseProductRow
        {
            get { return _selectedDecreaseProductRow; }
            set
            {
                _selectedDecreaseProductRow = value;
                OnPropertyChanged(SelectedDecreaseProductRowProperty);
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<DecreaseProductRow> SelectedDecreaseProductRows
        {
            get { return _selectedDecreaseProductRows;}
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="decreaseProducts">The current decrease details (Decrease Gtin, decrease type, decrease value)</param>
        public DecreaseProductSelectorViewModel(List<Tuple<String, ProductDecreaseType, Single>> decreaseProducts)
        {

            List<String> decreaseProductDetails = new List<String>();

            decreaseProductDetails.AddRange(decreaseProducts.Select(p => p.Item1));
            
            if (decreaseProductDetails.Any())
            {
                // load decrease product selections
                Dictionary<String, Product> products = ProductList.FetchByEntityIdProductGtins(CCMClient.ViewState.EntityId, decreaseProductDetails.Distinct()).ToDictionary(p => p.Gtin);

                foreach (var item in decreaseProducts)
                {

                    DecreaseProductRow decreaseProductRow = new DecreaseProductRow(products[item.Item1], (ProductDecreaseType)item.Item2, item.Item3);
                    decreaseProductRow.PropertyChanged += Parameter_PropertyChanged;

                    // load into decrease product grid
                    _decreaseProductRows.Add(decreaseProductRow);

                }
            }
        }

        #endregion

        private void Parameter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!_isUpdatingMultiInput)
            { 
                if (e.PropertyName == "DecreaseTextInputType" || e.PropertyName == "DecreaseValue")
                {
                    _isUpdatingMultiInput = true; 
                    DecreaseProductRow decreaseProductRow = (DecreaseProductRow)sender;
                    foreach (DecreaseProductRow selectedDecreaseProductRow in _selectedDecreaseProductRows)
                    {

                        if (e.PropertyName == "DecreaseTextInputType")
                        {
                            selectedDecreaseProductRow.ProductDecreaseType = decreaseProductRow.ProductDecreaseType;
                        }
                        else
                        {
                            selectedDecreaseProductRow.DecreaseValue = decreaseProductRow.DecreaseValue;
                        }
                    }
                    _isUpdatingMultiInput = false;
                }
            }
        }

        #region Commands

        #region SelectDecreaseProductCommand

        private RelayCommand _selectDecreaseProductCommand;

        /// <summary>
        /// Select the product to decrease
        /// </summary>
        public RelayCommand SelectDecreaseProductCommand
        {
            get
            {
                if (_selectDecreaseProductCommand == null)
                {
                    _selectDecreaseProductCommand = new RelayCommand(
                        p => SelectDecreaseProduct_Executed(),
                        p => SelectDecreaseProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.DecreaseProductSelector_SelectDecreaseProduct
                    };
                    base.ViewModelCommands.Add(_selectDecreaseProductCommand);
                }
                return _selectDecreaseProductCommand;
            }
        }

        private Boolean SelectDecreaseProduct_CanExecute()
        {
            return true;
        }

        private void SelectDecreaseProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                List<String> currentDecreaseProducts = _decreaseProductRows.Select(p => p.ProductGtin).ToList();

                ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                productSelectorHelper.IsMultiSelectEnabled = true;
                productSelectorHelper.AssignedProducts.AddRange(_decreaseProductRows.Select(p => p.DecreaseProduct));

                var winModel = new ProductSelectorViewModel(productSelectorHelper);
                winModel.ShowDialog(this.AttachedControl);

                if (winModel.DialogResult == true)
                {
                    foreach (Product product in winModel.AssignedProducts)
                    {
                        if (!currentDecreaseProducts.Contains(product.Gtin))
                        {
                            DecreaseProductRow decreaseProductRow = new DecreaseProductRow(product, ProductDecreaseType.ReduceToCasePackMultiplier, (Single)1.25);
                            decreaseProductRow.PropertyChanged += Parameter_PropertyChanged;

                            _decreaseProductRows.Add(decreaseProductRow);
                        }
                    }
                }
            }
        }

        #endregion

        #region RemoveDecreaseProductCommand

        private RelayCommand _removeDecreaseProductCommand;

        /// <summary>
        /// Remove the selected row
        /// </summary>
        public RelayCommand RemoveDecreaseProductCommand
        {
            get
            {
                if (_removeDecreaseProductCommand == null)
                {
                    _removeDecreaseProductCommand = new RelayCommand(
                        p => RemoveDecreaseProduct_Executed(),
                        p => RemoveDecreaseProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.DecreaseProductSelector_RemoveDecreaseProduct,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeDecreaseProductCommand);
                }
                return _removeDecreaseProductCommand;
            }
        }

        private Boolean RemoveDecreaseProduct_CanExecute()
        {
            return (SelectedDecreaseProductRow != null);
        }

        private void RemoveDecreaseProduct_Executed()
        {
            _decreaseProductRows.Remove(SelectedDecreaseProductRow);
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            return true;
            //return this.DecreaseProductRows.All(p => p.IsValid);
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }


        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                foreach (DecreaseProductRow decreaseProductRow in _decreaseProductRows)
                {
                    decreaseProductRow.PropertyChanged -= Parameter_PropertyChanged;
                }

                _decreaseProductRows.Clear();
                _selectedDecreaseProductRow = null;
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Row view model for the product to be decreased
    /// </summary>
    public class DecreaseProductRow : ViewModelObject
    {
        #region Fields

        private Product _product;
        private ProductDecreaseType _productDecreaseType;
        private Single _decreaseValue;
        private Galleria.Framework.Controls.Wpf.InputType _decreaseTextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer;
        
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<DecreaseProductRow>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<DecreaseProductRow>(p => p.ProductName);

        #endregion

        #region Properties

        /// <summary>
        /// The decrease product
        /// </summary>
        public Product DecreaseProduct
        {
            get { return _product; }
        }

        /// <summary>
        /// Returns the decrease product gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _product.Gtin; }
        }

        /// <summary>
        /// Returns the decrease product name
        /// </summary>
        public String ProductName
        {
            get { return _product.Name; }
        }

        public ProductDecreaseType ProductDecreaseType
        {
            get { return _productDecreaseType; }
            set
            {
                _productDecreaseType = value;
                OnProductDecreaseTypeChanged();
                OnPropertyChanged("ProductDecreaseType");
            }
        }

        private void OnProductDecreaseTypeChanged()
        {
            switch (ProductDecreaseType)
            {
                case ProductDecreaseType.ReduceUnitsTo:
                case ProductDecreaseType.ReduceFacingsBy:
                case ProductDecreaseType.ReduceFacingsTo:
                    DecreaseTextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer;
                    DecreaseValue = Convert.ToInt32(_decreaseValue);
                    break;
                case ProductDecreaseType.ReduceToCasePackMultiplier:
                    DecreaseTextInputType = Galleria.Framework.Controls.Wpf.InputType.Decimal;
                    break;
                default:
                    DecreaseTextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer;
                    break;
            }

            


        }

        public Single DecreaseValue
        {
            get { return _decreaseValue; }
            set
            {
                _decreaseValue = value;
                OnPropertyChanged("DecreaseValue");
            }
        }

        public Galleria.Framework.Controls.Wpf.InputType DecreaseTextInputType
        {
            get { return _decreaseTextInputType; }
            set
            {
                _decreaseTextInputType = value;
                OnPropertyChanged("DecreaseTextInputType");
            }
        }

        #endregion

        #region Constructor
        
        public DecreaseProductRow(Product product, ProductDecreaseType productDecreaseType, Single decreaseValue)
        {
            _product = product;
            ProductDecreaseType = productDecreaseType;
            _decreaseValue = decreaseValue;
        }
        
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._product = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}