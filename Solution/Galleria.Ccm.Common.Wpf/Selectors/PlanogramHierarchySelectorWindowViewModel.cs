﻿using System;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// A view model for the PlanogramHierarchySelectorWindowViewModel view.
    /// </summary>
    public class PlanogramHierarchySelectorWindowViewModel : ViewModelObject
    {
        #region Fields

        Boolean? _dialogResult;
        private PlanogramGroup _selectedPlanogramGroup;

        #endregion

        #region Binding Property Paths
        
        public static readonly PropertyPath SelectedPlanogramGroupProperty =
            WpfHelper.GetPropertyPath<PlanogramHierarchySelectorWindowViewModel>(p => p.SelectedPlanogramGroup);
        public static readonly PropertyPath OkCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramHierarchySelectorWindowViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<PlanogramHierarchySelectorWindowViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties
        

        /// <summary>
        /// Indicates whether or not the user has clicked Ok or cancel.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (_dialogResult.HasValue)
                {
                    // Fire the dialog result set event. This should be subscribed to by the view
                    // so that it can respond appropriately.
                    if (DialogResultSet != null) DialogResultSet(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// The selected planogram group
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Instantiates a new view model.
        /// </summary>
        public PlanogramHierarchySelectorWindowViewModel()
        {           
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        #endregion

        #region Commands

        #region OkCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OkCommand_Executed(),
                        p => OkCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        DisabledReason = Message.PlanogramHierarchySelector_Ok_DisabledReason
                    };
                }
                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            return (SelectedPlanogramGroup != null);
        }

        private void OkCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // Nothing to dispose.
        }
        #endregion
    }
}