﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-28424 : M.Shelley
//  Modified the text box caret so that when the field selector is re-opened after fields have 
//  been added, the new fields are placed at the end of the text box (rather than in the middle of the last field).
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for FieldSelectorWindow.xaml
    /// </summary>
    public sealed partial class FieldSelectorWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(FieldSelectorViewModel), typeof(FieldSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public FieldSelectorViewModel ViewModel
        {
            get { return (FieldSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FieldSelectorWindow senderControl = (FieldSelectorWindow)obj;

            if (e.OldValue != null)
            {
                FieldSelectorViewModel oldModel = (FieldSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedGroupFields.BulkCollectionChanged -= senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                FieldSelectorViewModel newModel = (FieldSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedGroupFields.BulkCollectionChanged += senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }
        }



        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public FieldSelectorWindow(FieldSelectorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(FieldSelectorWindow_Loaded);
        }


        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldSelectorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= FieldSelectorWindow_Loaded;

            //initialize the caret to the end
            this.ViewModel.CaretIndex = this.ViewModel.Text.Length;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == FieldSelectorViewModel.CaretIndexProperty.Path)
            {
                if (this.xFieldTextBox != null)
                {
                    this.xFieldTextBox.CaretIndex = this.ViewModel.CaretIndex;
                }
            }
        }

        /// <summary>
        /// Called whenever the item type fields collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ItemTypeFieldsBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //Reapply the prefilter
            String val = this.xFieldGridFilter.Text;
            if (!String.IsNullOrEmpty(val))
            {
                this.xFieldGridFilter.Text = null;
                this.xFieldGridFilter.Text = val;
            }
        }


        /// <summary>
        /// Called whenever the user double clicks on the field grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem != null)
            {
                this.ViewModel.AddFieldToText((ObjectFieldInfo)e.RowItem);
            }
        }

        /// <summary>
        /// Called whenever the user double clicks on the operators list box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperatorsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem item = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            if (item != null)
            {
                this.ViewModel.AddOperator((ObjectFieldExpressionOperator)item.Content);
            }
        }

        private void xFieldTextBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFieldTextBox.CaretIndex;
            }
        }

        private void xFieldTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFieldTextBox.CaretIndex;
            }
        }

        /// <summary>
        /// The following key down events are to give focus to different elements on pressing the space bar
        /// to make the page more easily navigable and improve usablility for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFieldGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                //this is to cater for where the window is used to select fields without building a formula 
                if (xOperatorsListBox.Visibility == Visibility.Collapsed)
                {
                    this.ViewModel.OKCommand.Execute();
                    e.Handled = true;
                    return;
                }

                this.ViewModel.AddFieldToText((ObjectFieldInfo)xFieldGrid.SelectedItem);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xOperatorsListBox);
            }
        }

        private void xFieldGroups_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xFieldGrid);
                e.Handled = true;
            }

            else if (e.Key == Key.Return)
            {
                this.ViewModel.AddFieldToText((ObjectFieldInfo)xFieldGroups.SelectedItem);
                e.Handled = true;
            }
        }

        private void xOperatorsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(btnClearFieldText);
                e.Handled = true;
            }

            else if (e.Key == Key.Return)
            {
                ListBoxItem item = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                if (item != null)
                {
                    this.ViewModel.AddOperator((ObjectFieldExpressionOperator)item.Content);
                }
                e.Handled = true;
            }
        }

        private void BtnClearFieldText_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xFieldGroups);
                e.Handled = true;

            }

            else if (e.Key == Key.Return)
            {
                if (btnClearFieldText.Command.CanExecute(null)) btnClearFieldText.Command.Execute(null);
                e.Handled = true;
            }
        }

        #endregion

        #region window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion


    }

}
