﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-25732 : L.Ineson
//  Created
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
#endregion

#region Version History: (CCM 8.1.1)
// V8-28431 : M.Shelley
//  Added drag and drop support between the 2 data grids
//  Also added mouse double click handling to keep the same pattern as other similar controls
#endregion

#region Version History : CCM820
// V8-30948 : A.Kuszyk
//  Added allow double click selection overload to constructor.
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for ProductSelectorWindow.xaml
    /// </summary>
    public sealed partial class ProductSelectorWindow
    {
        #region Fields
        private ColumnLayoutManager _searchResultsColumnManager;
        private ColumnLayoutManager _assignedProductsColumnManager;
        private Boolean _isSynchingLayout;
        /// <summary>
        /// Indicates if double clicking on a row should add it to the assigned products or select it and close the window.
        /// </summary>
        private Boolean _allowDoubleClickSelection;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductSelectorViewModel), typeof(ProductSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductSelectorWindow senderControl = (ProductSelectorWindow)obj;

            if (e.OldValue != null)
            {
                ProductSelectorViewModel oldModel = (ProductSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductSelectorViewModel newModel = (ProductSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public ProductSelectorViewModel ViewModel
        {
            get { return (ProductSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value);}
 
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public ProductSelectorWindow(ProductSelectorViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            _allowDoubleClickSelection = true;

            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += ProductSelectorWindow_Loaded;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="allowDoubleClickSelection">Indicates if double clicking on a row should add it to the assigned products or select it and close the window.</param>
        public ProductSelectorWindow(ProductSelectorViewModel viewModel, Boolean allowDoubleClickSelection)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            _allowDoubleClickSelection = allowDoubleClickSelection;

            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += ProductSelectorWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called on initial load.
        /// </summary>
        private void ProductSelectorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductSelectorWindow_Loaded;
            
            //setup the column managers
            //Here we want the grid layouts to be kept in sync so we have 2 managers using the same factory.

            //nb - dont need to hook up to columnset changing here.

            ProductColumnLayoutFactory layoutFactory =  new ProductColumnLayoutFactory(typeof(Product));
            DisplayUnitOfMeasureCollection uom = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(CCMClient.ViewState.EntityId);

            _searchResultsColumnManager = new ColumnLayoutManager(layoutFactory, uom, /*screenKey*/CCMClient.ColumnScreenKeyProducts);
            _searchResultsColumnManager.CurrentLayoutChanged += ColumnManager_CurrentLayoutChanged;
            _searchResultsColumnManager.AttachDataGrid(this.SearchResultsGrid);

            _assignedProductsColumnManager = new ColumnLayoutManager(layoutFactory, uom, /*screenKey*/CCMClient.ColumnScreenKeyProducts);
            _assignedProductsColumnManager.CurrentLayoutChanged += ColumnManager_CurrentLayoutChanged;
            _assignedProductsColumnManager.AttachDataGrid(this.AssignedProductsGrid);


            //if multiselect is off then set the assigned grid row off
            if (!this.ViewModel.IsMultiSelectEnabled)
            {
                AssignedGridRow.Height = new GridLength(0);
            }

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called when the column layout has changed.
        /// </summary>
        private void ColumnManager_CurrentLayoutChanged(object sender, EventArgs e)
        {
            if (_isSynchingLayout) return;

            _isSynchingLayout = true;

            //Mirror to the other grid.
            if (sender == _searchResultsColumnManager)
            {
                _assignedProductsColumnManager.CurrentColumnLayout = _searchResultsColumnManager.CurrentColumnLayout;
            }
            else
            {
                _searchResultsColumnManager.CurrentColumnLayout = _assignedProductsColumnManager.CurrentColumnLayout;
            }

            _isSynchingLayout = false;
        }

        /// <summary>
        /// Called whenever a grid row is double clicked.
        /// </summary>
        private void SearchResultsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null && !this.ViewModel.IsMultiSelectEnabled)
            {
                this.ViewModel.OKCommand.Execute();
            }
        }

        /// <summary>
        /// Drop event handler for the SearchResultsGrid control
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The drag event arguments</param>
        private void SearchResultsGrid_Drop(object sender, DragEventArgs e)
        {
            if (ViewModel == null) return;

            // Get the data attached to the arguments
            var dragData = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);

            if (dragData == null) return;
            // Check the source is the AssignedProductsGrid
            if (dragData.SourceGrid != AssignedProductsGrid) return;
            if (dragData.Items == null || dragData.Items.Count == 0) return;

            ViewModel.UnassignSelectedCommand.Execute(dragData.Items);
        }

        /// <summary>
        /// The mouse double-click handler for the SearchResultsGrid control
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void SearchResultsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null) return;
            if (SearchResultsGrid.SelectedItems == null || SearchResultsGrid.SelectedItems.Count == 0) return;
            if (_allowDoubleClickSelection)
            {
                ViewModel.OKCommand.Execute();
            }
            //Commented out as not needed?
            //else
            //{
            //    ViewModel.AssignSelectedCommand.Execute(SearchResultsGrid.SelectedItems);   
            //}
        }

        /// <summary>
        /// Drop event handler for the AssignedProductsGrid control
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The drag event arguments</param>
        private void AssignedProductsGrid_Drop(object sender, DragEventArgs e)
        {
            if (ViewModel == null) return;

            // Get the data attached to the arguments
            var dragData = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);

            if (dragData == null) return;
            // Check the source is the SearchResultsGrid
            if (dragData.SourceGrid != SearchResultsGrid) return;
            if (dragData.Items == null || dragData.Items.Count == 0) return;

            ViewModel.AssignSelectedCommand.Execute(dragData.Items);
        }

        /// <summary>
        /// The mouse double-click handler for the AssignedProductsGrid control
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void AssignedProductsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null) return;
            if (AssignedProductsGrid.SelectedItems == null || AssignedProductsGrid.SelectedItems.Count == 0) return;
            ViewModel.UnassignSelectedCommand.Execute(AssignedProductsGrid.SelectedItems);
        }

        /// <summary>
        /// The following PreviewKeyDown events provide the same functionality as the mouse-click events to
        /// add and remove selected products and allow the user to move between the grids using Space bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchResultsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && SearchResultsGrid.SelectedItem != null)
            {
                if (this.ViewModel.IsMultiSelectEnabled)
                {
                    ViewModel.AssignSelectedCommand.Execute(SearchResultsGrid.SelectedItems);                    
                    //focus back to the grid as it was lost on selection
                    Keyboard.Focus(SearchResultsGrid);
                }
                else
                {
                    this.ViewModel.OKCommand.Execute();
                }
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(AssignedProductsGrid);
            }
            else
            {
                return;
            }
        }      
        private void AssignedProductsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && AssignedProductsGrid.SelectedItem != null)
            {
                ViewModel.UnassignSelectedCommand.Execute(AssignedProductsGrid.SelectedItems);
                e.Handled = true;
                //focus back to the grid as it was lost on selection
                Keyboard.Focus(AssignedProductsGrid);
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(SearchResultsGrid);
            }
            else
            {
                return;
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {

                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    _searchResultsColumnManager.CurrentLayoutChanged -= ColumnManager_CurrentLayoutChanged;
                    _assignedProductsColumnManager.CurrentLayoutChanged -= ColumnManager_CurrentLayoutChanged;

                    _searchResultsColumnManager.Dispose();
                    _assignedProductsColumnManager.Dispose(); 

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
