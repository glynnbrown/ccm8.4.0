﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramDeletionDialogWindow.xaml
    /// </summary>
    public partial class PlanogramDeletionDialogWindow
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanogramDeletionDialogViewModel), typeof (PlanogramDeletionDialogWindow),
            new PropertyMetadata(null, ViewModelOnPropertyChanged));

        public PlanogramDeletionDialogViewModel ViewModel
        {
            get { return (PlanogramDeletionDialogViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramDeletionDialogWindow(PlanogramDeletionDialogViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            ViewModel = viewModel;

            Loaded += PlanogramDeletionDialogWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        private static void ViewModelOnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PlanogramDeletionDialogWindow senderControl = sender as PlanogramDeletionDialogWindow;
            if (senderControl == null) return;

            PlanogramDeletionDialogViewModel oldModel = e.OldValue as PlanogramDeletionDialogViewModel;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
            }

            PlanogramDeletionDialogViewModel newModel = e.NewValue as PlanogramDeletionDialogViewModel;
            if (newModel != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        private void PlanogramDeletionDialogWindow_Loaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= PlanogramDeletionDialogWindow_Loaded;

            // Cancel the busy cursor.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion
    }
}