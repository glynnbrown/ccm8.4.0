﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32359 : A.Silva
//  Created
// V8-32808 : A.Silva
//  Amended SelectTemplate and GetInputType to use ProductAttributeUpdateValue.GetUnderlyingValue and avoid issues with nullable types.
// V8-32817 : A.Silva
//  Amended GetTextBoxVisualTree to add the appropriate converters to the control.
// V8-32886 : A.Silva
//  Amended GetDatePickerVisualTree to ensure the values are bound correctly.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using InputType = Galleria.Framework.Controls.Wpf.InputType;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    ///     Interaction logic for ProductAttributeUpdateSelectorWindow.xaml
    /// </summary>
    public partial class ProductAttributeUpdateSelectorWindow
    {
        #region Properties

        #region ViewModel Property

        /// <summary>
        ///     The view model that this screen uses to interact with the model.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ProductAttributeUpdateSelectorViewModel), typeof(ProductAttributeUpdateSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Get or set this instance's view model.
        /// </summary>
        public ProductAttributeUpdateSelectorViewModel ViewModel
        {
            get { return (ProductAttributeUpdateSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Invloked when this instance's view model property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = obj as ProductAttributeUpdateSelectorWindow;
            if (sender == null) return;

            var oldViewModel = e.OldValue as ProductAttributeUpdateSelectorViewModel;
            if (oldViewModel != null)
            {
                oldViewModel.DialogResultSet -= sender.OnViewModelDialogResultSet;
            }

            var newViewModel = e.NewValue as ProductAttributeUpdateSelectorViewModel;
            if (newViewModel != null)
            {
                newViewModel.DialogResultSet += sender.OnViewModelDialogResultSet;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="ProductAttributeUpdateSelectorWindow"/> that uses the given <paramref name="viewModel"/>.
        /// </summary>
        public ProductAttributeUpdateSelectorWindow(ProductAttributeUpdateSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        } 

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xAvailableAttributesGrid_OnMouseDoubleClick(Object sender, MouseEventArgs e)
        {
            if (ViewModel == null ||
                !Equals(sender, xAvailableAttributesGrid) ||
                !ViewModel.AvailableAttributesSelection.Any()) return;

            ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the available attributes grid
        /// </summary>
        private void xAvailableAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (ViewModel == null) return;

            ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the selected attributes grid.
        /// </summary>
        private void xSelectedAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (ViewModel == null) return;
            ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        private void xSelectedAttributesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null ||
                !Equals(sender, xSelectedAttributesGrid) ||
                !ViewModel.SelectedAttributes.Any()) return;

            ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null ||
                !ViewModel.DialogResult.HasValue) return;

            DialogResult = ViewModel.DialogResult;
            Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel == null) return;

            ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
            ViewModel.Dispose();
        }

        #endregion
    }

    public class ProductAttributeUpdateDataTemplateSelector : DataTemplateSelector
    {
        private static readonly List<String> ColorProperties = new List<String> {PlanogramProduct.FillColourProperty.Name};

        private static readonly Dictionary<String, Object> EnumProperties =
            new Dictionary<String, Object>
            {
                {PlanogramProduct.FillPatternTypeProperty.Name, PlanogramProductFillPatternTypeHelper.FriendlyNames},
                {PlanogramProduct.ShapeTypeProperty.Name, PlanogramProductShapeTypeHelper.FriendlyNames},
                {PlanogramProduct.MerchandisingStyleProperty.Name, PlanogramProductMerchandisingStyleHelper.FriendlyNames},
                {PlanogramProduct.OrientationTypeProperty.Name,PlanogramProductOrientationTypeHelper.FriendlyNames},
                {PlanogramProduct.StatusTypeProperty.Name, PlanogramProductStatusTypeHelper.FriendlyNames}
            };

        private DisplayUnitOfMeasureCollection Uoms { get; set; }

        public ProductAttributeUpdateDataTemplateSelector()
        {
            Uoms = DisplayUnitOfMeasureCollection.Empty;
        }

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
        /// </summary>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
        /// </returns>
        /// <param name="item">The data object for which to select the template.</param><param name="container">The data-bound object.</param>
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            var element = container as FrameworkElement;
            var value = item as ProductAttributeUpdateValue;
            if (element == null || value == null) return null;

            switch (value.GetUnderlyingTypeCode())
            {
                case TypeCode.Empty:
                    break;
                case TypeCode.Object:
                    break;
                case TypeCode.DBNull:
                    break;
                case TypeCode.Boolean:
                    return new DataTemplate(typeof(ProductAttributeUpdateValue)) { VisualTree = GetCheckBoxVisualTree(value) };
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    FrameworkElementFactory visualTree = ColorProperties.Contains(value.PropertyName) ? GetColorVisualTree(value)
                                                             : EnumProperties.ContainsKey(value.PropertyName) ? GetEnumVisualTree(value)
                                                                    : GetTextBoxVisualTree(value);
                    return new DataTemplate(typeof(ProductAttributeUpdateValue)) { VisualTree = visualTree };
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return new DataTemplate(typeof(ProductAttributeUpdateValue)) { VisualTree = GetTextBoxVisualTree(value) };
                case TypeCode.DateTime:
                    return new DataTemplate(typeof (ProductAttributeUpdateValue)) {VisualTree = GetDateTimeVisualTree(value)};
                default:
                    return new DataTemplate(typeof(ProductAttributeUpdateValue)) { VisualTree = GetTextBoxVisualTree(value) };
            }

            return null;
        }

        private FrameworkElementFactory GetTextBoxVisualTree(ProductAttributeUpdateValue value)
        {
            var visualTree = new FrameworkElementFactory(typeof(ExtendedTextBox));
            var valueBinding1 = new Binding("NewValue") {Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, ValidatesOnDataErrors = true, TargetNullValue = String.Empty};
            Uoms.ApplyConverter(valueBinding1, value.PropertyDisplayType, value.PropertyType);
            Binding valueBinding = valueBinding1;
            visualTree.SetBinding(TextBox.TextProperty, valueBinding);
            visualTree.SetValue(ExtendedTextBox.InputTypeProperty, GetInputType(value));
            visualTree.SetValue(UIElement.IsEnabledProperty, true);
            visualTree.SetValue(FrameworkElement.MinWidthProperty, 90.0D);
            return visualTree;
        }

        private FrameworkElementFactory GetCheckBoxVisualTree(ProductAttributeUpdateValue value)
        {
            var visualTree = new FrameworkElementFactory(typeof(CheckBox));
            var valueBinding = new Binding("NewValue") {Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, ValidatesOnDataErrors = true, TargetNullValue = String.Empty};
            Uoms.ApplyConverter(valueBinding, value.PropertyDisplayType, value.PropertyType);
            visualTree.SetBinding(ToggleButton.IsCheckedProperty, valueBinding);
            visualTree.SetValue(UIElement.IsEnabledProperty, true);
            visualTree.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center);
            visualTree.SetValue(FrameworkElement.MinWidthProperty, 90.0D);
            return visualTree;
        }

        private static InputType GetInputType(ProductAttributeUpdateValue value)
        {
            switch (value.GetUnderlyingTypeCode())
            {
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return InputType.Integer;
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return InputType.Decimal;
                default:
                    return InputType.None;
            }
        }

        private FrameworkElementFactory GetEnumVisualTree(ProductAttributeUpdateValue value)
        {
            var visualTree = new FrameworkElementFactory(typeof (ExtendedComboBox));
            visualTree.SetValue(ItemsControl.ItemsSourceProperty, EnumProperties[value.PropertyName]);
            var valueBinding = new Binding("NewValue") {Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, ValidatesOnDataErrors = true, TargetNullValue = String.Empty};
            Uoms.ApplyConverter(valueBinding, value.PropertyDisplayType, value.PropertyType);
            visualTree.SetBinding(Selector.SelectedValueProperty, valueBinding);
            visualTree.SetValue(Selector.SelectedValuePathProperty, "Key");
            visualTree.SetValue(ItemsControl.DisplayMemberPathProperty, "Value");
            visualTree.SetValue(UIElement.IsEnabledProperty, true);
            visualTree.SetValue(FrameworkElement.MinWidthProperty, 90.0D);
            visualTree.SetValue(ScrollViewer.VerticalScrollBarVisibilityProperty, ScrollBarVisibility.Auto);
            return visualTree;
        }

        private FrameworkElementFactory GetColorVisualTree(ProductAttributeUpdateValue value)
        {
            var visualTree = new FrameworkElementFactory(typeof (ColorPickerBox));
            var valueBinding = new Binding("NewValue") {Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, ValidatesOnDataErrors = true, TargetNullValue = String.Empty};
            Uoms.ApplyConverter(valueBinding, value.PropertyDisplayType, value.PropertyType);
            visualTree.SetBinding(ColorPickerBox.SelectedColorProperty, valueBinding);
            visualTree.SetValue(UIElement.IsEnabledProperty, true);
            visualTree.SetValue(FrameworkElement.MinWidthProperty, 90.0D);
            return visualTree;
        }

        private FrameworkElementFactory GetDateTimeVisualTree(ProductAttributeUpdateValue value)
        {
            var visualTree = new FrameworkElementFactory(typeof (DatePicker));
            var valueBinding1 = new Binding("NewValue") {Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, ValidatesOnDataErrors = true};
            Binding valueBinding = valueBinding1;
            visualTree.SetBinding(DatePicker.SelectedDateProperty, valueBinding);
            visualTree.SetValue(UIElement.IsEnabledProperty, true);
            visualTree.SetValue(FrameworkElement.MinWidthProperty, 90.0D);
            // V8-32886 : Do not apply converter as it messes up the binding.
            //Uoms.ApplyConverter(valueBinding, value.PropertyDisplayType, value.PropertyType);
            return visualTree;
        }
    }
}