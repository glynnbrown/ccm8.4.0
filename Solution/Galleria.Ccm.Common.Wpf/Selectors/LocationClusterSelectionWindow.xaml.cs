﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
// V8-28987 : I.George
//  Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for LocationClusterSelectionWindow.xaml
    /// </summary>
    public partial class LocationClusterSelectionWindow : ExtendedRibbonWindow
    {
        #region Property

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationClusterSelectorViewModel), typeof(LocationClusterSelectionWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationClusterSelectionWindow senderControl = (LocationClusterSelectionWindow)obj;

            if (e.OldValue != null)
            {
                LocationClusterSelectorViewModel oldModel = (LocationClusterSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationClusterSelectorViewModel newModel = (LocationClusterSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public LocationClusterSelectorViewModel ViewModel
        {
            get { return (LocationClusterSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationClusterSelectionWindow(LocationClusterSelectorViewModel viewModel)
        {
            this.ViewModel = viewModel;
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(LocationClusterSelectionWindow_Loaded);
        }
        #endregion

        #region Events

        private void LocationClusterSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationClusterSelectionWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        private void xClustersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.OKCommand.Execute();
        }
        #endregion
        
    }
}
