﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
// V8-28987 : I.George
//  Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows.Data;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public sealed class LocationClusterSelectorViewModel : ViewModelAttachedControlObject<LocationClusterSelectionWindow>
    {
        #region Fields

        private ClusterScheme _clusterScheme;
        private Cluster _selectedCluster;
        private readonly ClusterSchemeInfoListViewModel _clusterInfoList = new ClusterSchemeInfoListViewModel();
        private ObservableCollection<LocationInfo> _assignedLocations = new ObservableCollection<LocationInfo>();
        private readonly LocationInfoListViewModel _masterLocationInfoListView = new LocationInfoListViewModel();
        private ObservableCollection<Cluster> _clusterList = new ObservableCollection<Cluster>();
        private Boolean _canShowClusters;
        private Boolean? _dialogResult;
        #endregion

        #region Binding Property

        public static readonly PropertyPath SelectedClusterProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(p => p.SelectedCluster);
        public static readonly PropertyPath SelectedClusterSchemeProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(p => p.SelectedClusterScheme);
        public static readonly PropertyPath AssignedLocationsProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(v => v.AssignedLocations);
        public static readonly PropertyPath ClusterListProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(v => v.ClusterList);
        public static readonly PropertyPath CanShowClustersProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(v => v.CanShowClusters);

        //commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<LocationClusterSelectorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the current clusterScheme
        /// </summary>
        public ClusterScheme SelectedClusterScheme
        {
            get { return _clusterScheme; }
        }

        /// <summary>
        /// Gets/Sets the selected Cluster
        /// </summary>
        public Cluster SelectedCluster
        {
            get { return _selectedCluster; }
            set
            {
                _selectedCluster = value;
                OnSelectedClusterChanged(value);
                OnPropertyChanged(SelectedClusterProperty);
            }
        }

        /// <summary>
        /// Gets the list of locations assigned to the selected cluster
        /// </summary>
        public ObservableCollection<LocationInfo> AssignedLocations
        {
            get { return _assignedLocations; }
        }

        /// <summary>
        /// Gets the list of clusters in the selected cluster scheme
        /// </summary>
        public ObservableCollection<Cluster> ClusterList
        {
            get { return _clusterList; }
        }

        /// <summary>
        /// Gets/Sets if a ClusterScheme is already set
        /// </summary>
        public Boolean CanShowClusters
        {
            get { return _canShowClusters; }
            set
            {
                _canShowClusters = value;
                OnPropertyChanged(CanShowClustersProperty);
            }
        }

        #endregion

        #region Constructor

        public LocationClusterSelectorViewModel(ClusterScheme clusterScheme, Boolean isClusterSchemeSet)
        {
            //location infos
            _canShowClusters = isClusterSchemeSet; 
            if ( _canShowClusters == true)
            {
                _masterLocationInfoListView.FetchAllForEntity();
                _clusterScheme = clusterScheme;
                foreach (Cluster cluster in this.SelectedClusterScheme.Clusters)
                {
                    _clusterList.Add(cluster);
                }
            }
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _oKCommand;

        /// <summary>
        /// Command to set the selected cluster
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_oKCommand == null)
                {
                    _oKCommand = new RelayCommand(
                       p => OKCommand_Executed(),
                       p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_oKCommand);
                }
                return _oKCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            if (this.SelectedCluster == null)
            {
                OKCommand.DisabledReason = Message.LocationClusterSelection_NoClusterSelected;
                return false;
            }
            return true;
        }

        public void OKCommand_Executed()
        {
            this.DialogResult = true;
        }
        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;
        /// <summary>
        /// Command to cancel the opened window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                       p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.GenericCancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        public void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// fetch a location using the master list 
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        private LocationInfo GetLocations(String locationCode)
        {
            LocationInfo locationInfo = _masterLocationInfoListView.Model.FirstOrDefault(l => l.Code == locationCode);
            if (locationInfo != null)
            {
                return locationInfo;
            }
            return null;
        }

        #endregion

        #region Events

        /// <summary>
        /// Updates the clusterLocation based on the selected cluster
        /// </summary>
        /// <param name="value"></param>
        private void OnSelectedClusterChanged(Cluster value)
        {
            if (_assignedLocations.Count > 0) { _assignedLocations.Clear(); }
            //change the content of the assigned grid based on the selection
            foreach (ClusterLocation clusterLocation in value.Locations)
            {
                LocationInfo locInfo = GetLocations(clusterLocation.Code);
                if (locInfo != null)
                {
                    _assignedLocations.Add(locInfo);
                }
            }
        }

        #endregion

        #region Dispose

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }
        #endregion
    }
}
