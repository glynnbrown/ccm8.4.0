﻿<!--region:Header Information-->
<!--Copyright © Galleria RTS Ltd 2015-->

<!--region: Version History : CCM 801-->
<!--
    V8-27494 : A.Kuszyk
      ~ Created
-->
<!--endregion: Version History : CCM 801-->
    
<!--region: Version History : CCM 8.1.1-->
<!--
    V8-30516 : M.Shelley
        Added mouse double click behaviour to both the available columns and selected columns grids.
-->
<!--endregion: Version History : CCM 8.1.1-->
    
<!--region: Version History : CCM 8.30-->
<!--
    V8-32359  : J.Pickup
        Work, to add default columns. - On hold, PCR may be getting canned.
    V8-32359 : A.Silva
        And the change got canned. Removed the extra columns for default values when updating attributes.
    V8-32560 : A.Silva
        Removed extra AddAll and RemoveAll buttons. Amended Icons of AddSelected and RemoveSelected.
-->
<!--endregion: Version History : CCM 8.30-->
    
<!--endregion:Header Information-->

<g:ExtendedRibbonWindow x:Class="Galleria.Ccm.Common.Wpf.Selectors.ProductAttributeSelectorWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
        xmlns:lg="clr-namespace:Galleria.Ccm.Common.Wpf.Resources.Language"
        xmlns:local="clr-namespace:Galleria.Ccm.Common.Wpf.Selectors"
        xmlns:componentModel="clr-namespace:System.ComponentModel;assembly=WindowsBase"
        xmlns:fg="clr-namespace:Galleria.Framework.ViewModel;assembly=Galleria.Framework"
        x:Name="xRootControl"
        ExtendedWindowStyle="Ribbonless"
        Title="{x:Static lg:Message.ProductAttributeSelector_Title}" Height="570" Width="800">

    <Grid DataContext="{Binding ElementName=xRootControl, Path=ViewModel}"
          Background="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushWindowContentAreaBackground}}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width=".1*" />
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width=".1*" />
            <ColumnDefinition Width="Auto" />
        </Grid.ColumnDefinitions>

        <!--region Available Attributes-->
        <TextBlock Grid.Row="0" Grid.Column="0" Margin="5" HorizontalAlignment="Left"
                                   Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                                   Text="{x:Static lg:Message.ProductAttributeSelector_AvailableAttributes}" />
        
        <g:ExtendedDataGrid Grid.Row="1" Grid.Column="0" Margin="5"  CanUserFilter="True" 
                                            CanUserReorderColumns="False" CanUserDragRows="True"
                                            CanUserSelectRows="True" SelectionMode="Extended" CanUserSortColumns="True"
                                            ItemsSourceExtended="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.AvailableAttributesProperty}}"
                                            AutoResizeColumns="True"
                                            g:SelectedItemsBehaviour.SynchroniseSelectedItems="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.AvailableAttributesSelectionProperty}}"
                                            g:SelectedItemsBehaviour.SynchroniseInBackground="False"
                                            RowDropCaught="xAvailableAttributesGrid_RowDropCaught"
                                            MouseDoubleClick="xAvailableAttributesGrid_OnMouseDoubleClick"
                                            x:Name="xAvailableAttributesGrid">
            <g:ExtendedDataGrid.SortByDescriptions>
                <componentModel:SortDescription PropertyName="HeaderGroup" Direction="Ascending" />
                <componentModel:SortDescription PropertyName="Header" Direction="Ascending" />
            </g:ExtendedDataGrid.SortByDescriptions>
            <g:ExtendedDataGrid.Columns>

                <DataGridTextColumn IsReadOnly="True" Width="200">
                    <DataGridTextColumn.Binding>
                        <Binding Path="HeaderGroup" Mode="OneTime"
                                                 UpdateSourceTrigger="PropertyChanged" />
                    </DataGridTextColumn.Binding>
                </DataGridTextColumn>

                <DataGridTextColumn IsReadOnly="True" Width="200">
                    <DataGridTextColumn.Binding>
                        <Binding Path="Header" Mode="OneTime" UpdateSourceTrigger="PropertyChanged" />
                    </DataGridTextColumn.Binding>
                </DataGridTextColumn>

            </g:ExtendedDataGrid.Columns>
        </g:ExtendedDataGrid>
        <!--endregion-->

        <!--region Add/Remove buttons-->
        <StackPanel Grid.Row="1" Grid.Column="1" Margin="2" VerticalAlignment="Center"
                                    HorizontalAlignment="Center" Orientation="Vertical">

            <!--region AddSelected button-->
            <Button Margin="0,2,0,2" Padding="2"
                                    Command="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.AddSelectedAttributesCommandProperty}}">

                <Image Height="20" Width="20"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, Path=Command.(fg:RelayCommand.SmallIcon)}" />

            </Button>
            <!--endregion AddSelected button-->

            <!--region RemoveSelected button-->
            <Button Margin="0,2,0,2" Padding="2"
                                    Command="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.RemoveSelectedAttributesCommandProperty}}">

                <Image Height="20" Width="20"
                                       Source="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type Button}}, Path=Command.(fg:RelayCommand.SmallIcon)}" />

            </Button>
            <!--endregion RemoveSelected button-->

        </StackPanel>
        <!--endregion-->
        
        <!--region Selected Attributes-->

        <TextBlock Grid.Row="0" Grid.Column="2" Margin="5" HorizontalAlignment="Left"
                                   Style="{StaticResource {x:Static g:ResourceKeys.Text_StyMainHeader}}"
                                   Text="{x:Static lg:Message.ProductAttributeSelector_SelectedAttributes}" /> 

        <g:ExtendedDataGrid Grid.Row="1" Grid.Column="2" Margin="5"
                            CanUserDragRows="True"  CanUserFilter="True" 
                            CanUserReorderColumns="False" SelectionMode="Extended" 
                            ItemsSourceExtended="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.SelectedAttributesProperty}}"
                            AutoResizeColumns="True" 
                            RowDropCaught="xSelectedAttributesGrid_RowDropCaught"
                            MouseDoubleClick="xSelectedAttributesGrid_MouseDoubleClick"
                            g:SelectedItemsBehaviour.SynchroniseSelectedItems="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.SelectedAttributesSelectionProperty}}"
                            g:SelectedItemsBehaviour.SynchroniseInBackground="False"
                            x:Name="xSelectedAttributesGrid">
            
            
            <g:ExtendedDataGrid.SortByDescriptions>
                <componentModel:SortDescription PropertyName="HeaderGroup" Direction="Ascending" />
                <componentModel:SortDescription PropertyName="Header" Direction="Ascending" />
            </g:ExtendedDataGrid.SortByDescriptions>
            <g:ExtendedDataGrid.Columns>

                <DataGridTextColumn IsReadOnly="True" Width="200" Header="{x:Static lg:Message.ProductAttributeSelector_AvailableAttributes}">
                    <DataGridTextColumn.Binding>
                        <Binding Path="HeaderGroup" Mode="OneTime"
                                                 UpdateSourceTrigger="PropertyChanged" />
                    </DataGridTextColumn.Binding>
                </DataGridTextColumn>

                <DataGridTextColumn IsReadOnly="True" Width="200"  Header="{x:Static lg:Message.ProductAttributeSelector_Attributes}">
                    <DataGridTextColumn.Binding>
                        <Binding Path="Header" Mode="OneTime" UpdateSourceTrigger="PropertyChanged" />
                    </DataGridTextColumn.Binding>
                </DataGridTextColumn>

            </g:ExtendedDataGrid.Columns>
        </g:ExtendedDataGrid>

        <!--endregion-->

        <!--region Button Panel-->
        <g:ButtonPanel Grid.Row="3" Grid.ColumnSpan="99" Margin="0,5,0,0">
            <StackPanel Orientation="Horizontal" HorizontalAlignment="Right">

                <Button Command="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.OkCommandProperty}}"
                        Content="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"  />

                <Button Command="{Binding Path={x:Static local:ProductAttributeSelectorViewModel.CancelCommandProperty}}"
                        Content="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"  
                        IsCancel="True"/>

            </StackPanel>
        </g:ButtonPanel>
        <!--endregion-->
        
    </Grid>
</g:ExtendedRibbonWindow>