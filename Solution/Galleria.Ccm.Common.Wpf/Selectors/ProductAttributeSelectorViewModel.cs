﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 801
// V8-27494 : A.Kuszyk
//	Created.
// V8-28940 : A.Kuszyk
//  Added defensive code to Add and Remove commands.
// V8-28944 : A.Kuszyk
// Changed Ok command to always be enabled.
#endregion

#region Version History: CCM 802
// V8-29030 : J.Pickup
//	Constructor now enumerates through product performance when enumerating productfieldinfos. 
#endregion

#region Version History : CCM820
// V8-31246 : A.Kuszyk
//  Updated constructor to ignore assortment fields.
#endregion
#region Version History : CCM830
// V8-32359 : J.Pickup
//  Updated constructor to ignore should show default value etc.
// V8-32359 : A.Silva
//  Removed code regarding default values.
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll related code.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// A view model for the ProductAttributeSelector view.
    /// </summary>
    public class ProductAttributeSelectorViewModel : ViewModelObject
    {
        #region Fields

        Boolean? _dialogResult;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableAttributesProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.AvailableAttributes);
        public static readonly PropertyPath SelectedAttributesProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.SelectedAttributes);
        public static readonly PropertyPath AvailableAttributesSelectionProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.AvailableAttributesSelection);
        public static readonly PropertyPath AddSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.AddSelectedAttributesCommand);
        public static readonly PropertyPath RemoveSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.RemoveSelectedAttributesCommand);
        public static readonly PropertyPath SelectedAttributesSelectionProperty = 
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.SelectedAttributesSelection);
        public static readonly PropertyPath OkCommandProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<ProductAttributeSelectorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The attributes that are available for selection.
        /// </summary>
        public BulkObservableCollection<ProductAttributeSelectorRow> AvailableAttributes { get; set; }

        /// <summary>
        /// The attributes that have been selected.
        /// </summary>
        public BulkObservableCollection<ProductAttributeSelectorRow> SelectedAttributes { get; set; }

        /// <summary>
        /// The available attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<ProductAttributeSelectorRow> AvailableAttributesSelection { get; set; }

        /// <summary>
        /// The selected attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<ProductAttributeSelectorRow> SelectedAttributesSelection { get; set; }

        /// <summary>
        /// Indicates whether or not the user has clicked Ok or cancel.
        /// </summary>
        public Boolean? DialogResult 
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (_dialogResult.HasValue)
                {
                    // Fire the dialog result set event. This should be subscribed to by the view
                    // so that it can respond appropriately.
                    if(DialogResultSet != null) DialogResultSet(this,EventArgs.Empty);
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Instantiates a new view model.
        /// </summary>
        /// <param name="includeCustomFields">True indidicates that custom attributes should be included in the available attributes.</param>
        /// <param name="includePerformanceFields"></param>
        /// <param name="currentSelection">The current attributes selection, if any, that should be pre-selected. Can be null.</param>
        /// <param name="includeProperties">A collection of properties to display, arrange as a list of tuples containing the source type and property name.</param>
        /// <remarks>When using this view model, the view should subscribe to the DialogResultSet event.</remarks>
        public ProductAttributeSelectorViewModel(
            Boolean includeCustomFields,
            Boolean includePerformanceFields, 
            IEnumerable<Tuple<ProductAttributeSourceType,String>> currentSelection = null,
            ICollection<Tuple<ProductAttributeSourceType,String>> includeProperties = null)
        {
            // Populate the available attributes with those from Planogram Product inlcuding custom and performance properties.
            AvailableAttributes = new BulkObservableCollection<ProductAttributeSelectorRow>();
            AvailableAttributes.AddRange(PlanogramProduct.
                EnumerateDisplayableFieldInfos(/*incMetadata*/false, includeCustomFields, includePerformanceFields, includeAssortmentData: false).
                Select(ProductAttributeSelectorRow.CreateNewRow));
            
            // Create empty collections for the selection properties.
            SelectedAttributes = new BulkObservableCollection<ProductAttributeSelectorRow>();
            AvailableAttributesSelection = new BulkObservableCollection<ProductAttributeSelectorRow>();
            SelectedAttributesSelection = new BulkObservableCollection<ProductAttributeSelectorRow>();

            // If attributes have been specified for inclusion, then we remove all that don't match
            // (otherwise, all attributes are left in).
            if (includeProperties != null && includeProperties.Any())
            {
                List<ProductAttributeSelectorRow> includeAttributes = new List<ProductAttributeSelectorRow>();
                foreach (Tuple<ProductAttributeSourceType, String> sourceAndProperty in includeProperties)
                {
                    includeAttributes.AddRange(AvailableAttributes.Where(a =>
                            a.Source == sourceAndProperty.Item1 && a.PropertyName.Equals(sourceAndProperty.Item2)));
                }
                AvailableAttributes.RemoveRange(AvailableAttributes.Except(includeAttributes.Distinct()).ToList());
            }

            // Finally, for each item in the current selection, move these items from the available
            // to the selected list.
            if (currentSelection!=null)
            {
                List<ProductAttributeSelectorRow> currentAttributeSelection = new List<ProductAttributeSelectorRow>();
                foreach (Tuple<ProductAttributeSourceType, String> sourceAndProperty in currentSelection)
                {
                    currentAttributeSelection.AddRange(
                        AvailableAttributes.Where(a =>
                            a.Source == sourceAndProperty.Item1 && a.PropertyName.Equals(sourceAndProperty.Item2)));
                }
                IEnumerable<ProductAttributeSelectorRow> distinctCurrentAttributeSelection =
                    currentAttributeSelection.Distinct().ToList();
                SelectedAttributes.AddRange(distinctCurrentAttributeSelection);
                AvailableAttributes.RemoveRange(distinctCurrentAttributeSelection); 
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        #endregion

        #region Commands

        #region OkCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OkCommand_Executed(),
                        p => OkCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        DisabledReason = Message.ProductAttributeSelector_Ok_DisabledReason
                    };
                }
                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            // V8-28944
            // Ok should always be enabled.
            return true;
        }

        private void OkCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedAttributesCommand

        private RelayCommand _addSelectedAttributesCommand;

        /// <summary>
        ///     Adds any selected available Attributes to the current view Attribute list
        /// </summary>
        public RelayCommand AddSelectedAttributesCommand
        {
            get
            {
                if (_addSelectedAttributesCommand != null) return _addSelectedAttributesCommand;

                _addSelectedAttributesCommand = new RelayCommand(
                    p => AddSelectedAttributes_Executed(),
                    p => AddSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.ProductAttributeSelector_AddSelectedAttribute,
                    SmallIcon = ImageResources.Add_16,
                    DisabledReason = Message.ProductAttributeSelector_AddSelectedAttribute_DisabledReason 
                };

                return _addSelectedAttributesCommand;
            }
        }

        private Boolean AddSelectedAttributes_CanExecute()
        {
            return AvailableAttributesSelection.Count > 0;
        }

        private void AddSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<ProductAttributeSelectorRow> rowsToAdd = new List<ProductAttributeSelectorRow>();
            foreach (ProductAttributeSelectorRow row in AvailableAttributesSelection)
            {
                if (!SelectedAttributes.Contains(row)) rowsToAdd.Add(row);
            }

            try
            {
                AvailableAttributes.RemoveRange(rowsToAdd);
                SelectedAttributes.AddRange(rowsToAdd);
                if (AvailableAttributesSelection.Any())
                {
                    AvailableAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #region RemoveSelectedAttributesCommand

        private RelayCommand _removeSelectedAttributesCommand;

        /// <summary>
        ///     Remove any selected available Attributes from the current view Attribute list
        /// </summary>
        public RelayCommand RemoveSelectedAttributesCommand
        {
            get
            {
                if (_removeSelectedAttributesCommand != null) return _removeSelectedAttributesCommand;

                _removeSelectedAttributesCommand = new RelayCommand(
                    p => RemoveSelectedAttributes_Executed(),
                    p => RemoveSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.ProductAttributeSelector_RemoveSelectedAttributes, 
                    SmallIcon = ImageResources.Delete_16,
                    DisabledReason = Message.ProductAttributeSelector_RemoveSelectedAttributes_DisabledReason 
                };

                return _removeSelectedAttributesCommand;
            }
        }

        private Boolean RemoveSelectedAttributes_CanExecute()
        {
            return SelectedAttributesSelection.Count > 0;
        }

        private void RemoveSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<ProductAttributeSelectorRow> rowsToAdd = new List<ProductAttributeSelectorRow>();
            foreach(ProductAttributeSelectorRow row in SelectedAttributesSelection)
            {
                if(!AvailableAttributes.Contains(row)) rowsToAdd.Add(row);
            }
            try
            {
                SelectedAttributes.RemoveRange(rowsToAdd);
                AvailableAttributes.AddRange(rowsToAdd);
                if (SelectedAttributesSelection.Any())
                {
                    SelectedAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // Nothing to dispose.
        }
        #endregion
    }
}
