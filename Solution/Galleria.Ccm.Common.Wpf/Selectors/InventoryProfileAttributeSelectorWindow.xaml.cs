﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.20)
// GFS-30836 : J.Pickup
//  Created.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for InventoryProfileAttributeSelectorWindow.xaml
    /// </summary>
    public partial class InventoryProfileAttributeSelectorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(InventoryProfileAttributeSelectorViewModel), typeof(InventoryProfileAttributeSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public InventoryProfileAttributeSelectorViewModel ViewModel
        {
            get { return (InventoryProfileAttributeSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InventoryProfileAttributeSelectorWindow senderControl = (InventoryProfileAttributeSelectorWindow)obj;

            if (e.OldValue != null)
            {
                InventoryProfileAttributeSelectorViewModel oldModel = (InventoryProfileAttributeSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                InventoryProfileAttributeSelectorViewModel newModel = (InventoryProfileAttributeSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }



        #endregion

        #endregion

        #region Constructor

        public InventoryProfileAttributeSelectorWindow(InventoryProfileAttributeSelectorViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;

            ViewModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(ViewModel_PropertyChanged);
        }

        void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == InventoryProfileAttributeSelectorViewModel.DialogResultProperty.Path)
            { 
                this.DialogResult = ViewModel.DialogResult;
            }
        }

        #endregion

        #region Event Handlers

        private void xAvailableLocationsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //set category code and name
            this.ViewModel.DialogResult = true;
        }

        private void xAvailableInventoryProfilesGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && xAvailableInventoryProfilesGrid.SelectedItem != null)
            {
                //set category code and name
                this.ViewModel.DialogResult = true;
                e.Handled = true;
            }
            else
            {
                return;
            }
        }
        #endregion

        #region Methods

        #region Window Close

        public Boolean IsClosing
        {
            get;
            private set;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            ViewModel.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(ViewModel_PropertyChanged);

            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #endregion
    }
}

