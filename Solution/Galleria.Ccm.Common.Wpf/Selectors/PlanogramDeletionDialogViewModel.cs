﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0.0)

// V8-19032 : J.Mendes
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Message = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using Galleria.Ccm.Common.Wpf.Resources;
using System.Globalization;
using Galleria.Framework.Collections;
using System.ComponentModel;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    ///     ViewModel controller to select a deletion list of planograms that are linked to locations/stores.
    /// </summary>
    public sealed class PlanogramDeletionDialogViewModel : ViewModelAttachedControlObject<PlanogramDeletionDialogWindow>
    {
        #region Fields

        private string _WarningMessageLine1;
        private string _WarningMessageLine2;
        private string _gridRow2Height = "Auto";
        private Boolean _isSelectAll = true;
        private ReadOnlyBulkObservableCollection<GridDeletionRowItem> _availablePlanogramList;
        private BulkObservableCollection<GridDeletionRowItem> _planogramList = new BulkObservableCollection<GridDeletionRowItem>();
        private GridDeletionRowItem _selectedPlanogram;
        private List<GridDeletionRowItem> colGridRowItems;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath WarningMessageLine1Property = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.WarningMessageLine1);
        public static readonly PropertyPath WarningMessageLine2Property = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.WarningMessageLine2);
        public static readonly PropertyPath GridRow2HeightProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.GridRow2Height);
        public static readonly PropertyPath IsWarningMessageLine2VisibleProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.IsWarningMessageLine2Visible);
        public static readonly PropertyPath IsSelectAllProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.IsSelectAll);
        public static readonly PropertyPath AvailablePlanogramListProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.AvailablePlanogramList);
        public static readonly PropertyPath SelectedPlanogramProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.SelectedPlanogram);
        public static readonly PropertyPath NotAssignedPlanogramCountProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.NotAssignedPlanogramCount);
        public static readonly PropertyPath AssignedPlanogramCountProperty = WpfHelper.GetPropertyPath<PlanogramDeletionDialogViewModel>(p => p.AssignedPlanogramCount);
       
        // Commands.
        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SelectAllCommand"/>.
        /// </summary>
        public static readonly PropertyPath SelectAllCommandProperty = GetPropertyPath(o => o.SelectAllCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="DeleteCommand"/>.
        /// </summary>
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath(o => o.DeleteCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="CancelCommand"/>.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the warning message line 1.
        /// </summary>
        public string WarningMessageLine1
        {
            get {
                return _WarningMessageLine1;
            }
            set
            {
                _WarningMessageLine1 = value;
                OnPropertyChanged(WarningMessageLine1Property);
            }
        }

        /// <summary>
        /// Gets/Sets the warning message line 2.
        /// </summary>
        public string WarningMessageLine2
        {
            get {
                return _WarningMessageLine2;
            }
            set
            {
                _WarningMessageLine2 = value;
                OnPropertyChanged(WarningMessageLine2Property);
            }
        }

        /// <summary>
        /// Gets/Sets the Height for the row 2 of the layout grid.
        /// </summary>
        public string GridRow2Height
        {
            get {
                return _gridRow2Height;
            }
            set
            {
                _gridRow2Height = value;
                OnPropertyChanged(GridRow2HeightProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the is selected all option.
        /// </summary>
        public Boolean IsSelectAll
        {
            get {
                return _isSelectAll;
            }
            set
            {
                _isSelectAll = value;
                OnPropertyChanged(IsSelectAllProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available planograms
        /// </summary>
        public ReadOnlyBulkObservableCollection<GridDeletionRowItem> AvailablePlanogramList
        {
            get
            {
                _availablePlanogramList = new ReadOnlyBulkObservableCollection<GridDeletionRowItem>(_planogramList);
                return _availablePlanogramList;
            }
        }

        /// <summary>
        /// Gets Assigned Planogram Count
        /// </summary>
        public Int32 AssignedPlanogramCount
        {
            get
            {
                return _planogramList.Count();
            }
        }

        /// <summary>
        /// Gets Assigned Planogram Count
        /// </summary>
        public Int32 NotAssignedPlanogramCount
        {
            get
            {
                return _plansToDelete.Count() - AssignedPlanogramCount;
            }
        }
  
        /// <summary>
        /// Check if Is WarningMessageLine2 Visible
        /// </summary>
        public Boolean IsWarningMessageLine2Visible
        {
            get
            {
                Boolean blnIsVisible = Convert.ToBoolean(NotAssignedPlanogramCount > 0 && AssignedPlanogramCount > 0);

                if (blnIsVisible)
                {
                    GridRow2Height = "Auto";
                }
                else
                {
                    GridRow2Height = "0";
                }

                return blnIsVisible;
            }
        }


        /// <summary>
        /// Gets/Sets selected planogram from the grid.
        /// </summary>
        public GridDeletionRowItem SelectedPlanogram
        {
            get
            {
                return _selectedPlanogram;
            }
            set
            {
                _selectedPlanogram = value;
                OnPropertyChanged(SelectedPlanogramProperty);
            }
        }

        #region DialogResult

        private IEnumerable<PlanogramInfo> _plansToDelete;

        /// <summary>
        /// Gets/Sets the plans to delete collection.
        /// </summary>
        public IEnumerable<PlanogramInfo> PlansToDelete
        {
            get
            {
                return _plansToDelete;
            }
            set
            {
                _plansToDelete = value;
            }
        }

        private Boolean? _dialogResult = false;

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of this type.
        /// </summary>
        public PlanogramDeletionDialogViewModel(IEnumerable<PlanogramInfo> plansToDelete)
        {
            _plansToDelete = plansToDelete;
            BeginSelection();
        }

        private void BeginSelection()
        {

            colGridRowItems = new List<GridDeletionRowItem>();

            foreach (PlanogramInfo planogramInfo in _plansToDelete)
            {

                GridDeletionRowItem newItem = new GridDeletionRowItem(planogramInfo);

                //Add only planograms with more than one store to be in the Grid.
                if (newItem.NumberOfStores > 0)
                {
                    colGridRowItems.Add(newItem);
                }

            }

            //If there is at least 1 planogram assigned to a store/location then change default warning message with a dialog that includes information about all assigned planograms.
            if (colGridRowItems.Count() > 0)
            {

                _planogramList.AddRange(colGridRowItems);

                foreach (GridDeletionRowItem item in colGridRowItems)
                {
                    item.PropertyChanged += GridDeletionRowItem_PropertyChanged;
                }

                if (NotAssignedPlanogramCount == 1)
                {
                    WarningMessageLine1 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc2_CountEqualTo1, NotAssignedPlanogramCount);
                }
                else
                {
                    WarningMessageLine1 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc2, NotAssignedPlanogramCount);
                }

                if (AssignedPlanogramCount == 1)
                {
                    if (NotAssignedPlanogramCount > 0)
                    {
                        WarningMessageLine2 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc3_CountEqualTo1, AssignedPlanogramCount);
                    }
                    else
                    {
                        WarningMessageLine1 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc3_CountEqualTo1, AssignedPlanogramCount);
                    }
                }
                else
                {
                    if (NotAssignedPlanogramCount > 0)
                    {
                        WarningMessageLine2 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc3, AssignedPlanogramCount);
                    }
                    else
                    {
                        WarningMessageLine1 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc3, AssignedPlanogramCount);
                    }
                }

            }
            else
            {
                WarningMessageLine1 = String.Format(CultureInfo.CurrentCulture, Message.PlanogramRepository_DeletePlanograms_ConfirmDesc, _plansToDelete.Count());
            }

        }

        private void GridDeletionRowItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_planogramList.Where(i => i.IsSelected).Count() == 0 && this.IsSelectAll)
            {
                this.IsSelectAll = false;
            }

            if (_planogramList.Where(i => i.IsSelected).Count() == _planogramList.Count() && !this.IsSelectAll)
            {
                this.IsSelectAll = true; 
            }
        }

        #endregion

        #region Commands

        #region SelectAll

        /// <summary>
        ///     Reference to the current instance for <see cref="SelectAllCommand"/>.
        /// </summary>
        private RelayCommand _SelectAllCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SelectAllCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SelectAllCommand
        {
            get
            {
                if (_SelectAllCommand == null)
                {
                    _SelectAllCommand = new RelayCommand(o => SelectAll_Executed(),
                        o => SelectAll_CanExecute())
                    {
                        FriendlyName = Message.PlanogramRepository_DeletePlanograms_CheckBox_SelectAll
                    };
                    ViewModelCommands.Add(_SelectAllCommand);
                }
                return _SelectAllCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="SelectAllCommand"/>.
        /// </summary
        //[DebuggerStepThrough]
        private Boolean SelectAll_CanExecute()
        {
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SelectAllCommand"/> is executed.
        /// </summary>
        private void SelectAll_Executed()
        {
            base.ShowWaitCursor(true);

            foreach (GridDeletionRowItem item in _planogramList) {
                item.IsSelected = _isSelectAll;  
            }

            OnPropertyChanged(AvailablePlanogramListProperty);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Reference to the current instance for <see cref="DeleteCommand"/>.
        /// </summary>
        private RelayCommand _DeleteCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="DeleteCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_DeleteCommand == null)
                {
                    _DeleteCommand = new RelayCommand(o => Delete_Executed(),
                        o => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        DisabledReason = Message.PlanogramRepository_DeletePlanograms_DeleteCommand_FriendlyDescription
                    };
                    ViewModelCommands.Add(_DeleteCommand);
                }
                return _DeleteCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="DeleteCommand"/>.
        /// </summary
        //[DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            // User must have get permission.
            if (NotAssignedPlanogramCount == 0 && _planogramList.Where(i => i.IsSelected).Count() == 0)
            {
                this.DeleteCommand.DisabledReason = Message.PlanogramRepository_DeletePlanograms_DeleteCommand_DisabledReason;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="DeleteCommand"/> is executed.
        /// </summary>
        private void Delete_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                var modifiedPlans = _plansToDelete.ToList();

                //Change inital colletion and return true:
                foreach (GridDeletionRowItem item in _planogramList) {
                    if (!item.IsSelected) {
                        modifiedPlans.Remove(modifiedPlans.First(x => x.Id == item.Id));
                    }
                }

                _plansToDelete = modifiedPlans.ToList();
                DialogResult = true;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Cancel

        /// <summary>
        ///     Reference to the current instance for <see cref="CancelCommand"/>.
        /// </summary>
        private RelayCommand _cancelCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CancelCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(o => Cancel_Executed(), o => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="CancelCommand"/>.
        /// </summary>
        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CancelCommand"/> is executed.
        /// </summary>
        private void Cancel_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                // Close the window by returning false.
                DialogResult = false;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Methods

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramDeletionDialogViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<PlanogramDeletionDialogViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (base.IsDisposed)
            {
                return;
            }

            if (disposing)
            {
               
                // Release unmanaged resources.
                if (colGridRowItems != null && colGridRowItems.Count() > 0) {
                    foreach (GridDeletionRowItem item in colGridRowItems)
                    {
                        item.PropertyChanged -= GridDeletionRowItem_PropertyChanged;
                    }
                    colGridRowItems.Clear();
                    colGridRowItems = null;
                }

            }

            IsDisposed = true;
        }

        #endregion
    }

    public class GridDeletionRowItem : INotifyPropertyChanged
    {

        #region Fields

        int _id;
        String _planogramName;
        Int32 _numberOfStores = 0;
        Boolean _isSelected = true;

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of this type.
        /// </summary>
        public GridDeletionRowItem(PlanogramInfo planogramInfo)
        {
            _id = planogramInfo.Id;
            _planogramName = planogramInfo.Name;
            _numberOfStores = LocationPlanAssignmentList.FetchByPlanogramId(planogramInfo.Id).Count();
            _isSelected = true;
        }
    
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the Id.
        /// </summary>
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Planogram Name.
        /// </summary>
        public string PlanogramName
        {
            get
            {
                return _planogramName;
            }
            set
            {
                _planogramName = value;
            }
        }

        /// <summary>
        /// Gets/Sets the number of stores.
        /// </summary>
        public Int32 NumberOfStores
        {
            get
            {
                return _numberOfStores;
            }
            set
            {
                _numberOfStores = value;
            }
        }

        /// <summary>
        /// Gets/Sets the is selected value.
        /// </summary>
        public Boolean IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, e);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

}

