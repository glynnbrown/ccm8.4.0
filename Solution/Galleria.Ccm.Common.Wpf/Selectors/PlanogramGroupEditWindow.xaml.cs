﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramGroupEditWindow.xaml
    /// </summary>
    public partial class PlanogramGroupEditWindow
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanogramGroupEditViewModel), typeof (PlanogramGroupEditWindow),
            new PropertyMetadata(null, ViewModelOnPropertyChanged));

        public PlanogramGroupEditViewModel ViewModel
        {
            get { return (PlanogramGroupEditViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramGroupEditWindow(PlanogramGroupEditViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            ViewModel = viewModel;

            Loaded += PlanogramGroupEditorWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        private static void ViewModelOnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PlanogramGroupEditWindow senderControl = sender as PlanogramGroupEditWindow;
            if (senderControl == null) return;

            PlanogramGroupEditViewModel oldModel = e.OldValue as PlanogramGroupEditViewModel;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
            }

            PlanogramGroupEditViewModel newModel = e.NewValue as PlanogramGroupEditViewModel;
            if (newModel != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        private void PlanogramGroupEditorWindow_Loaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= PlanogramGroupEditorWindow_Loaded;

            // Cancel the busy cursor.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion
    }
}