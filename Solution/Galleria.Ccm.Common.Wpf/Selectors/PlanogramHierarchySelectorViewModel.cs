﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25724 : A.Silva ~ Created.
// V8-25871 : A.Kuszyk ~ Amended for drag and drop.
// V8-25664 : A.Kuszyk ~ Added My Categories functionality.
// V8-26284 : A.Kuszyk ~ Moved to Galleria.Ccm.Common.Wpf.
// V8-26534 : A.Kuszyk ~ Standardised warning messages.
// V8-26822 : L.Ineson ~ Changed how my categories groups are displayed.
// V8-27966 : A.Silva
//      Changed EditPlanogramGroup to use PlanogramGroupEditViewModel.
//      Amended EditPlanogramGroup to return the result of the edit (applied or canceled) and to refresh the groups if applied.
//      Added overload to optional get the newly added planogram group with AddNewPlanogramGroup.
//      Implemented planogram group move and copy (including contents).
//      Added PlanogramCopied event to notify whenever a new planogram is copied directly by this instance.
// V8-27964 : A.Silva
//      Added StartingPlanogramGroup Property. If none is set, or the value is not valid, it defaults to the root group of the hierarchy, then removed.
//      Amended to use StartingPlanogramGroup to find which planogram group to display first after loading a new hierarchy, then removed.
// V8-28083 : A.Probyn
//      Added defensive code null checks to stop exceptions when disposing.
// V8-27280 : A.Silva
//      Amended to use PlanogramHierarchyView.GetRecentWork instead of calculating the planogram groups directly.
// V8-28116 : A.Silva
//      Amended to recall planogram group selection by tab (Available/Favourite).
// V8-28292 : A.Silva
//      Amended UpdateAvailableGroups so that Recent Work Groups are reselected if necessary.
// V8-28337 : A.Silva
//      Added pasting of planograms along with planogram groups.
//      Added progress notification for paste operations.
//      Added warning to copy/move planogram group when duplicate names.
// V8-28330 : L.Ineson
//  On model change this now tries to reselect the planogram group.
// V8-28236 : A.Silva
//      When Planogram Hierarchy model changes, fetch if there is nothing fetched yet. 
//          This is to avoid not having groups when a selected group has been passed to the viewmodel, 
//          which would null the pre selection.
// V8-28318 : A.Silva
//      Amended SetPlanogramGroupSelection() so that it will select planogram views that are currently not 
//          displayed in the main treeview (so they are available for favorites too).
// V8-28445 : A.Silva
//      Amended SetPlanogramGroupSelection() so that it will select the first planogram from the correct tab 
//          (all/favourites) when there is no selection, instead of selecting the first one from the All tab.
// V8-28209 : A.Silva
//      Amended EditPlanogramGroup so that the planogram group view model will be updated along with its planogramn group.
// V8-28532 : A.Kuszyk
//      Updated paste planogram message to show move/copy as appropriate.
// V8-27881 : A.Kuszyk
//  Added CheckForOverwrite method.
// V8-28587 : A.Kuszyk
//  Added owner parameter to PlanogramHierarchySelectorViewModel.CheckForOverwrite.
#endregion

#region Version History: (CCM v8.01)
// V8-28716 : L.Ineson
//  Moved out code into PlanogramRepositoryUIHelper so 
// that it can be accessed without going through the ui control.
#endregion

#region Version History : CCM 802
// V8-27880 : A.Kuszyk
//  Added recent work check to SetPlanogramGroupSelection method.
#endregion

#region Version History : (CCM v8.2.0)
// V8-30047 : A.Probyn
//  Updated so favourites tab is now a tree view.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using ImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Diagnostics;
using System.Timers;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public enum PlanogramHierarchySelectorTab
    {
        All,
        Favourites
    }

    public sealed class PlanogramHierarchySelectorViewModel : ViewModelAttachedControlObject<PlanogramHierarchySelector>
    {
        #region Constants

        private const Int32 RecentWorkGroupsCount = 10;

        #endregion

        #region Fields

        private PlanogramHierarchySelectorTab _selectedTab = PlanogramHierarchySelectorTab.All;

        private readonly PlanogramGroupViewModel _recentGroup = PlanogramGroupViewModel.NewRecentWorkGroup();
        private Predicate<string> _isUniqueGroupPredicate;

        private readonly PlanogramHierarchyViewModel _masterPlanHierarchyView; //a view presenting the master planogramhierarchy


        private String _groupSearchFilter;
        private ReadOnlyCollection<PlanogramGroupViewModel> _availableGroups;
        private PlanogramGroupViewModel _selectedGroup;

        private Boolean _showRecentWorkGroup = true;

        private readonly UserPlanogramGroupListViewModel _userFavouritePlanGroupsView; // a view of favourite planogram groups for the current user.
        private Boolean _showFavouriteGroups = true;
        private readonly BulkObservableCollection<PlanogramGroupViewModel> _favouriteGroups = new BulkObservableCollection<PlanogramGroupViewModel>();
        private ReadOnlyBulkObservableCollection<PlanogramGroupViewModel> _favouriteGroupsRo;


        private PlanogramGroup _availableTabSelectedPlanogramGroup; //The selected Planogram Group in the Available tab.
        private PlanogramGroup _favouriteTabSelectedPlanogramGroup;// The selected Planogram Group in the Favourite tab.

        private Timer _groupSearchTimer;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProgressStatusProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.ProgressStatus);
        public static readonly PropertyPath SelectedItemProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.SelectedItem);
        public static readonly PropertyPath SelectedTabProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.SelectedTab);
        public static readonly PropertyPath GroupSearchFilterProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.GroupSearchFilter);
        public static readonly PropertyPath AvailableGroupsProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.AvailableGroups);
        public static readonly PropertyPath SelectedGroupProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.SelectedGroup);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.SelectedPlanogramGroup);

        public static readonly PropertyPath FavouriteGroupsProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.FavouriteGroups);
        public static readonly PropertyPath AddGroupToFavouritesCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.AddGroupToFavouritesCommand);
        public static readonly PropertyPath RemoveGroupFromFavouritesCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchySelectorViewModel>(p => p.RemoveGroupFromFavouritesCommand);

        #endregion

        #region Properties

        #region SelectedItem -TODO: GET RID.

        /// <summary>
        ///     Holds the value for the <see cref="SelectedItem"/> property.
        /// </summary>
        private PlanogramGroup _selectedItem;

        /// <summary>
        ///     Gets or sets the value of the <see cref="SelectedItem"/> property, notifying when the value changes.
        /// </summary>
        public PlanogramGroup SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(SelectedItemProperty);
            }
        }

        #endregion

        /// <summary>
        /// Gets/Sets the selected tab.
        /// </summary>
        public PlanogramHierarchySelectorTab SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                _selectedTab = value;
                OnPropertyChanged(SelectedTabProperty);

                OnSelectedTabChanged(value);
            }
        }

        /// <summary>
        /// Gets/Sets the planogram hierarchy viewmodel to use.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanHierarchyView; }
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when filtering the available groups list.
        /// </summary>
        public String GroupSearchFilter
        {
            get { return _groupSearchFilter; }
            set
            {
                _groupSearchFilter = value;
                OnPropertyChanged(GroupSearchFilterProperty);

                OnGroupSearchFilterChanged();
            }
        }

        /// <summary>
        /// Returns the collection of available workpackage groups
        /// </summary>
        public ReadOnlyCollection<PlanogramGroupViewModel> AvailableGroups
        {
            get { return _availableGroups; }
            set
            {
                _availableGroups = value;
                OnPropertyChanged(AvailableGroupsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected group
        /// </summary>
        public PlanogramGroupViewModel SelectedGroup
        {
            get { return _selectedGroup; }
            set
            {
                if (Equals(_selectedGroup, value)) return;

                _selectedGroup = value;
                OnPropertyChanged(SelectedGroupProperty);
                OnPropertyChanged(SelectedPlanogramGroupProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected planogram group.
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get
            {
                if (SelectedGroup == null) return null;
                return SelectedGroup.PlanogramGroup;
            }
            set
            {
                this.SetPlanogramGroupSelection(value);
            }
        }

        /// <summary>
        /// A predicate to check whether a Planogram Group exists, accepting the String Name.
        /// </summary>
        private Predicate<String> IsUniqueGroupPredicate
        {
            get
            {
                if (_isUniqueGroupPredicate == null)
                {
                    _isUniqueGroupPredicate = (name) =>
                    {
                        return !PlanogramHierarchyView.Model.EnumerateAllGroups().Any(group =>
                            group.Name == name &&
                            group.ParentHierarchy.Id == PlanogramHierarchyView.Model.Id &&
                            group.ParentGroup.Id == SelectedGroup.PlanogramGroup.Id);
                    };
                }
                return _isUniqueGroupPredicate;
            }
        }

        /// <summary>
        /// Gets/Sets whether favourite groups should be displayed.
        /// </summary>
        public Boolean ShowFavouriteGroups
        {
            get { return _showFavouriteGroups; }
            set
            {
                _showFavouriteGroups = value;
                OnGroupSearchFilterChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the usergroups viewmodel to use.
        /// </summary>
        public UserPlanogramGroupListViewModel UserGroupsView
        {
            get { return _userFavouritePlanGroupsView; }
        }

        /// <summary>
        /// Returns the collection of favourite groups for the current user.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramGroupViewModel> FavouriteGroups
        {
            get { return _favouriteGroupsRo ?? (_favouriteGroupsRo = new ReadOnlyBulkObservableCollection<PlanogramGroupViewModel>(_favouriteGroups)); }
        }

        /// <summary>
        /// Gets/Sets whether the recent work group should be displayed
        /// </summary>
        public Boolean ShowRecentWorkGroup
        {
            get { return _showRecentWorkGroup; }
            set
            {
                if (value != _showRecentWorkGroup)
                {
                    _showRecentWorkGroup = value;
                    OnGroupSearchFilterChanged();
                }
            }
        }

        #region ProgressStatus

        /// <summary>
        ///     Holds the value for the <see cref="ProgressStatus"/> property.
        /// </summary>
        private IProgressStatus _progressStatus;

        /// <summary>
        ///     Gets or sets the value of the <see cref="ProgressStatus"/> property, notifying when the value changes.
        /// </summary>
        public IProgressStatus ProgressStatus
        {
            get { return _progressStatus; }
            set
            {
                IProgressStatus oldValue = _progressStatus;
                _progressStatus = value;
                OnPropertyChanged(ProgressStatusProperty);

                OnProgressStatusChanged(oldValue, value);
            }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler GroupRefreshRequired;

        private void OnGroupRefreshRequired()
        {
            if (GroupRefreshRequired != null)
            {
                GroupRefreshRequired(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramHierarchySelectorViewModel(PlanogramHierarchyViewModel hierarchyView, UserPlanogramGroupListViewModel userGroupsView)
        {
            _masterPlanHierarchyView = hierarchyView;
            hierarchyView.ModelChanged += PlanogramHierarchy_ModelChanged;

            _userFavouritePlanGroupsView = userGroupsView;
            userGroupsView.ModelChanged += UserGroupsView_ModelChanged;

            OnPlanogramHierarchyModelChanged();
            UpdateFavouriteGroups();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the plan hierarchy model changes.
        /// </summary>
        private void PlanogramHierarchy_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramHierarchy> e)
        {
            OnPlanogramHierarchyModelChanged();
        }
        private void OnPlanogramHierarchyModelChanged()
        {
            //[V8-28474] drop these groups to stop them from memory leaking.
            _availableTabSelectedPlanogramGroup = null;
            _favouriteTabSelectedPlanogramGroup = null;

            PlanogramGroup selectedGroup = this.SelectedPlanogramGroup;

            // If we don't have a model yet, load one from the repository. This step is required, becuase, if we're
            // not polling, then we will never get a model.
            if (PlanogramHierarchyView != null && PlanogramHierarchyView.Model == null) PlanogramHierarchyView.FetchForCurrentEntity();

            UpdateAvailableGroups();
            UpdateFavouriteGroups();

            if (PlanogramHierarchyView != null && PlanogramHierarchyView.Model != null)
            {
                PlanogramGroup newSelectedGroup =
                    (selectedGroup != null) ? PlanogramHierarchyView.Model.EnumerateAllGroups().FirstOrDefault(g => g.Id == selectedGroup.Id) : PlanogramHierarchyView.Model.RootGroup;
                if (newSelectedGroup != null
                    && (this.SelectedPlanogramGroup == null || (this.SelectedPlanogramGroup != newSelectedGroup && this.SelectedPlanogramGroup.Id == newSelectedGroup.Id)))
                {
                    this.SelectedPlanogramGroup = selectedGroup;
                }
            }
        }

        /// <summary>
        /// Called whenever the user groups view model changes.
        /// </summary>
        private void UserGroupsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<UserPlanogramGroupList> e)
        {
            UpdateFavouriteGroups();
        }

        /// <summary>
        /// Called whenever the group search filter property value changes.
        /// </summary>
        private void OnGroupSearchFilterChanged()
        {
            if(_groupSearchTimer == null)
            {
                _groupSearchTimer = new Timer(250);
                _groupSearchTimer.AutoReset = false;
                _groupSearchTimer.Elapsed += (s, e) =>
                {
                    if (this.IsDisposed)
                    {
                        var timer = _groupSearchTimer;
                        _groupSearchTimer = null;
                        timer.Dispose();
                        return;
                    }

                    this.AttachedControl.Dispatcher.Invoke(
                        (Action)(() => UpdateAvailableGroups()));
                };
            }

            if (this.AttachedControl != null) _groupSearchTimer.Start();
            else UpdateAvailableGroups();
        }

        /// <summary>
        /// Called whenever the selected tab changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedTabChanged(PlanogramHierarchySelectorTab newValue)
        {
            if (newValue == PlanogramHierarchySelectorTab.Favourites)
            {
                _availableTabSelectedPlanogramGroup = SelectedPlanogramGroup;
                this.SelectedPlanogramGroup = _favouriteTabSelectedPlanogramGroup
                    ?? this.FavouriteGroups.OrderBy(f => f.Name).FirstOrDefault().PlanogramGroup;
            }
            else
            {
                _favouriteTabSelectedPlanogramGroup = SelectedPlanogramGroup;
                SelectedPlanogramGroup = _availableTabSelectedPlanogramGroup
                                         ??
                                         (this.PlanogramHierarchyView.Model != null
                                             ? this.PlanogramHierarchyView.Model.RootGroup
                                             : null);
            }
        }

        private void ProgressStatusOnPropertyChanged(Object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            //  Just notify the progress status has changed.
            OnPropertyChanged(ProgressStatusProperty);
        }

        #endregion

        #region Commands

        #region AddGroupToFavourites Command

        private RelayCommand _addGroupToFavouritesCommand;

        /// <summary>
        ///     The Add Group To Favourites command.
        /// </summary>
        public RelayCommand AddGroupToFavouritesCommand
        {
            get
            {
                if (_addGroupToFavouritesCommand != null) return _addGroupToFavouritesCommand;

                _addGroupToFavouritesCommand = new RelayCommand(
                    o => AddGroupToFavourites_Executed(), o => AddGroupToFavourites_CanExecute())
                {
                    FriendlyName = Message.PlanogramHierarchySelector_AddGroupToFavorites,
                    FriendlyDescription = Message.PlanogramHierarchySelector_AddGroupToFavorites_Description,
                    Icon = ImageResources.PlanogramHierarchySelector_AddToFavourites32,
                    SmallIcon = ImageResources.PlanogramHierarchySelector_AddToFavourites16,
                    DisabledReason = Message.PlanogramHierarchySelector_AddGroupToFavorites_DisabledReason
                };
                ViewModelCommands.Add(_addGroupToFavouritesCommand);
                return _addGroupToFavouritesCommand;
            }
        }

        private Boolean AddGroupToFavourites_CanExecute()
        {
            //must have a group selected.
            if (this.SelectedGroup == null
                || this.SelectedGroup.PlanogramGroup == null
                || this.SelectedGroup.PlanogramGroup.ParentGroup == null)
            {
                this.AddGroupToFavouritesCommand.DisabledReason =
                    Message.PlanHierarchySelector_AddGroupToFavorites_DisabledNoSelected;
                return false;
            }

            //group must not already be a favourites group.
            if (_userFavouritePlanGroupsView != null
                && _userFavouritePlanGroupsView.Model != null
                && PlanogramHierarchyView != null
                && PlanogramHierarchyView.Model != null)
            {
                Int32 selectedGroupId = this.SelectedPlanogramGroup.Id;
                if (_userFavouritePlanGroupsView.Model.Any(g => Equals(g.PlanogramGroupId, selectedGroupId)))
                {
                    this.AddGroupToFavouritesCommand.DisabledReason =
                    Message.PlanHierarchySelector_AddGroupToFavorites_AlreadyExists;
                    return false;
                }
            }

            return true;
        }

        private void AddGroupToFavourites_Executed()
        {
            _userFavouritePlanGroupsView.AddGroupToFavourites(this.SelectedGroup.PlanogramGroup);
        }

        #endregion

        #region RemoveGroupFromFavourites Command

        private RelayCommand _removeGroupFromFavouritesCommand;

        /// <summary>
        /// The Remove Group from Favourites command.
        /// </summary>
        public RelayCommand RemoveGroupFromFavouritesCommand
        {
            get
            {
                if (_removeGroupFromFavouritesCommand == null)
                {
                    _removeGroupFromFavouritesCommand = new RelayCommand(
                        o => RemoveGroupFromFavourites_Executed(),
                        o => RemoveGroupFromFavourites_CanExecute())
                    {
                        FriendlyName = Message.PlanogramHierarchySelector_RemoveGroupFromFavorites,
                        FriendlyDescription = Message.PlanogramHierarchySelector_RemoveGroupFromFavorites_Description,
                        Icon = ImageResources.PlanogramHierarchySelector_DeleteFromFavourites32,
                        SmallIcon = ImageResources.PlanogramHierarchySelector_DeleteFromFavourites16,
                        DisabledReason = Message.PlanogramHierarchySelector_RemoveGroupFromFavorites_DisabledReason
                    };
                    base.ViewModelCommands.Add(_removeGroupFromFavouritesCommand);
                }
                return _removeGroupFromFavouritesCommand;
            }
        }

        private Boolean RemoveGroupFromFavourites_CanExecute()
        {
            //must have a group selected
            if (this.SelectedGroup == null
                || this.SelectedGroup.PlanogramGroup == null
                || this.SelectedGroup.PlanogramGroup.ParentGroup == null)
            {
                this.RemoveGroupFromFavouritesCommand.DisabledReason =
                    Message.PlanHierarchySelector_RemoveGroupFromFavorites_DisabledNoSelected;
                return false;
            }

            //must be a favourite
            //group must not already be a favourites group.
            if (_userFavouritePlanGroupsView != null
                && _userFavouritePlanGroupsView.Model != null
                && PlanogramHierarchyView != null
                && PlanogramHierarchyView.Model != null
                && this.SelectedPlanogramGroup != null
                && this.SelectedPlanogramGroup != PlanogramHierarchyView.Model.RootGroup)
            {
                Int32 selectedGroupId = this.SelectedPlanogramGroup.Id;
                if (!_userFavouritePlanGroupsView.Model.Any(g => Equals(g.PlanogramGroupId, selectedGroupId)))
                {
                    this.RemoveGroupFromFavouritesCommand.DisabledReason =
                    Message.PlanHierarchySelector_RemoveGroupFromFavorites_DoesNotExist;
                    return false;
                }
            }


            return true;
        }

        private void RemoveGroupFromFavourites_Executed()
        {
            _userFavouritePlanGroupsView.RemoveGroupFromFavourites(this.SelectedGroup.PlanogramGroup);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Updates the list of available planogram group views from the master planogram group hierarchy view.
        /// </summary>
        private void UpdateAvailableGroups()
        {
            if (this.PlanogramHierarchyView == null) return;
            if (this.UserGroupsView == null) return;

            //// Make sure we have a master plan hierarchy view to pull available groups from.
            //if (PlanogramHierarchyView == null)
            //{
            //    AvailableGroups = new ReadOnlyCollection<PlanogramGroupViewModel>(new List<PlanogramGroupViewModel>());
            //    return;
            //}

            //if (PlanogramHierarchyView.Model == null)
            //{
            //    AvailableGroups = new ReadOnlyCollection<PlanogramGroupViewModel>(new List<PlanogramGroupViewModel>());
            //    PlanogramHierarchyView.FetchForCurrentEntityAsync();
            //    return;
            //}

            // Store previously selected planogram group.
            PlanogramGroup originalGroup = this.SelectedPlanogramGroup;
            Boolean isRecentWorkGroup = this.SelectedGroup != null && this.SelectedGroup.IsRecentWorkGroup;

            List<PlanogramGroupViewModel> groupViewModels = new List<PlanogramGroupViewModel>();
            if (this.PlanogramHierarchyView.Model != null)
            {
                // Either add the filtered groups, OR all recent and unfiltered groups.
                if (!String.IsNullOrEmpty(GroupSearchFilter))
                {
                    groupViewModels.AddRange(GetPlanogramGroupsViewModelsSearch(PlanogramHierarchyView, GroupSearchFilter));
                }
                else
                {
                    if (this.ShowRecentWorkGroup)
                    {
                        _recentGroup.ChildViews.Clear();
                        _recentGroup.ChildViews.AddRange(PlanogramHierarchyView.GetRecentWork(RecentWorkGroupsCount).ToList());
                        groupViewModels.Add(_recentGroup);
                    }

                    groupViewModels.Add(PlanogramHierarchyView.RootGroup);
                }
            }

            this.AvailableGroups = new ReadOnlyCollection<PlanogramGroupViewModel>(groupViewModels);

            if (SelectedTab == PlanogramHierarchySelectorTab.All)
            {
                // Try to restore the original selection.
                SetPlanogramGroupSelection(originalGroup, isRecentWorkGroup);
            }
        }

        /// <summary>
        /// Updates the favourite groups list
        /// </summary>
        private void UpdateFavouriteGroups()
        {
            // Store previously selected planogram group.
            PlanogramGroup originalGroup = SelectedPlanogramGroup;

            //clear out any existing.
            if (_favouriteGroups.Count > 0) _favouriteGroups.Clear();

            //load in the new.
            if (!ShowFavouriteGroups || PlanogramHierarchyView == null || PlanogramHierarchyView.Model == null || _userFavouritePlanGroupsView == null) return;
            if (_userFavouritePlanGroupsView.Model == null)
            {
                _userFavouritePlanGroupsView.FetchForCurrentUserAndEntity();
            }

            List<Int32> groupIds = _userFavouritePlanGroupsView.Model.Select(g => g.PlanogramGroupId).ToList();

            foreach (PlanogramGroup group in PlanogramHierarchyView.Model
                    .EnumerateAllGroups()
                    .Where(g => groupIds.Contains(g.Id))
                    .OrderBy(g => g.Name))
            {
                _favouriteGroups.Add(new PlanogramGroupViewModel(group, true));
            }
            OnPropertyChanged(FavouriteGroupsProperty);

            if (SelectedTab == PlanogramHierarchySelectorTab.Favourites)
            {
                // Try to restore the original selection.
                SelectedPlanogramGroup = originalGroup;
            }
        }

        public void MovePlanogramsToGroup(IEnumerable<PlanogramInfo> plans, PlanogramGroupViewModel targetGroup)
        {
            PlanogramRepositoryUIHelper.MovePlanogramsToGroup(plans, targetGroup.PlanogramGroup);
            OnGroupRefreshRequired();
        }

        /// <summary>
        ///     Sets the selection on the Planogram Group View Model whiches planogram group id matches the provided one.
        /// </summary>
        /// <param name="planogramGroup">Id that the selection needs to match.</param>
        /// <param name="isRecentWorkGroup"></param>
        private void SetPlanogramGroupSelection(PlanogramGroup planogramGroup, Boolean isRecentWorkGroup = false)
        {
            PlanogramGroupViewModel matchingGroup = null;

            //  Selected the correct available groups (recent work, favourites or all).
            IList<PlanogramGroupViewModel> groups = null;
            if (isRecentWorkGroup)
            {
                groups = AvailableGroups;
                if (groups != null) groups = groups.Where(g => g.IsRecentWorkGroup).ToList();
            }
            else if (PlanogramHierarchyView != null)
            {
                //  Filter the available groups depending on the selected tab, so we get the correct default if there is no selection.
                groups = SelectedTab == PlanogramHierarchySelectorTab.Favourites
                    ? PlanogramHierarchyView.EnumerateAllGroupViews().Where(o => _favouriteGroupsRo.Any(f => f.Id == o.Id)).ToList()
                    : PlanogramHierarchyView.EnumerateAllGroupViews().ToList();
            }

            //  If there are both available groups and a selection, try to select it.
            if (groups != null && planogramGroup != null)
            {
                List<PlanogramGroupViewModel> candidateGroups = groups.SelectMany(g => g.EnumerateAllChildGroups()).ToList();
                matchingGroup = candidateGroups.FirstOrDefault(p => p.IsRecentWorkGroup == isRecentWorkGroup && p.Id == planogramGroup.Id) ??
                                candidateGroups.FirstOrDefault();
            }

            //  Return the selection if there is any.
            if (matchingGroup != null) SelectedGroup = matchingGroup;
        }

        /// <summary>
        ///     Gets an ordered enumeration of Planogram Group View Models matching the provided Name Criteria.
        /// </summary>
        /// <param name="planogramHierarchy">The hierarchy to search matches in.</param>
        /// <param name="nameCriteria">The criteria for matching names.</param>
        /// <returns>Ordered enumeration with the Planogram Group View Models that match the Criteria.</returns>
        private static IEnumerable<PlanogramGroupViewModel> GetPlanogramGroupsViewModelsSearch(
            PlanogramHierarchyViewModel planogramHierarchy, String nameCriteria)
        {
            return planogramHierarchy
                .EnumerateAllGroupViews()
                .Where(planogramGroupViewModel =>
                    Regex.IsMatch(planogramGroupViewModel.Name, nameCriteria, RegexOptions.IgnoreCase))
                .OrderBy(planogramGroupViewModel => planogramGroupViewModel.Name);
        }

        #region PastePlanogramGroup

        //TODO: Tidy up and move code into PlanogramRepositoryUIHelper so that screen viewmodels that want this code don't
        // have to access it through the ui control - really really bad!

        /// <summary>
        ///     Moves or copies a <see cref="PlanogramGroupViewModel"/> into the currently <see cref="SelectedGroup"/>.
        /// </summary>
        /// <param name="data">The data to be pasted.</param>
        /// <param name="isMove">Whether the data should just be moved, or copied to the destination.</param>
        public void PasteGroup(PlanogramGroupViewModel data/*, Boolean isMove = true*/)
        {
            if (data == null || SelectedGroup == null) return;

            //nb - plan group move code is now in plan repositiory ui helper.
            // this will need to eventually be moved there too.

            //  Move or copy the planogram groups and planograms, update the data base immediately.
            //if (isMove)
            //{
            //    //  Just move all planogram groups to the new parent.
            //    if (MovePlanogramGroup(data, SelectedGroup)) 
            //        data.PlanogramGroup.UpdatePlanogramGroup();
            //}
            //else
            //{
            //  Copy all planogram groups as new versions.
            PlanogramGroupViewModel copy = CopyPlanogramGroup(data, SelectedGroup);
            if (copy == null) return;

            copy.PlanogramGroup.InsertPlanogramGroup();

            //  Start an async task to copy all planograms from the copied groups.
            ProgressStatus = new ProgressStatus { Name = "Copying Planograms Progress" };
            BackgroundWorker copyPlanogramsWorker = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };
            copyPlanogramsWorker.DoWork += CopyPlanogramsWorkerOnDoWork;
            copyPlanogramsWorker.RunWorkerCompleted += CopyPlanogramsWorkerOnRunWorkerCompleted;
            copyPlanogramsWorker.ProgressChanged += CopyPlanogramWorkerOnProgressChanged;
            copyPlanogramsWorker.RunWorkerAsync(new Tuple<PlanogramGroupViewModel, PlanogramGroupViewModel>(data, copy));
            //}
        }

        private PlanogramGroupViewModel CopyPlanogramGroup(PlanogramGroupViewModel source, PlanogramGroupViewModel target)
        {
            if (source == null || target == null || Equals(source, target)) return null;

            if (target.PlanogramGroup.ChildList.Any(p => p.Name == source.Name))
            {
                ContinueAfterWarning(Message.Generic_Ok, Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Title,
                    String.Format(Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Description, source.Name, target.Name),
                    Message.Generic_Warning, ImageResources.DialogWarning);
                return null;
            }

            if (!ContinueAfterWarning(Message.Generic_Continue, Message.Generic_Warning,
                String.Format(Message.PlanogramHierarchySelector_CopyGroup_WarningDescription,
                    source.Name, target.Name),
                Message.PlanogramHierarchySelector_CopyGroup_WarningTitle,
                ImageResources.DialogQuestion, Message.Generic_Cancel)) return null;

            PlanogramGroupViewModel copy = PlanogramHierarchyView.CopyPlanogramGroup(source, target);
            if (copy != null) OnGroupRefreshRequired();
            return copy;
        }

        private Boolean ContinueAfterWarning(
           String button1, String title, String description, String header, ImageSource messageIcon = null, String button2 = null)
        {
            var message = new ModalMessage();
            if (String.IsNullOrEmpty(button2))
            {
                message.ButtonCount = 1;
                message.Button1Content = button1;
            }
            else
            {
                message.ButtonCount = 2;
                message.Button1Content = button1;
                message.Button2Content = button2;
            }
            if (messageIcon != null) message.MessageIcon = messageIcon;
            message.Title = title;
            message.Description = description;
            message.Header = header;
            message.Owner = AttachedControl.GetParentWindow();
            message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            message.ShowDialog();
            return message.Result == ModalMessageResult.Button1;
        }

        private void OnProgressStatusChanged(IProgressStatus oldValue, IProgressStatus newValue)
        {
            if (oldValue != null) oldValue.PropertyChanged -= ProgressStatusOnPropertyChanged;
            if (newValue != null) newValue.PropertyChanged += ProgressStatusOnPropertyChanged;
        }

        /// <summary>
        ///     Invoked when the copy planogram async task has been completed.
        /// </summary>
        private void CopyPlanogramsWorkerOnRunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            using (BackgroundWorker copyPlanogramsWorker = sender as BackgroundWorker)
            {
                if (copyPlanogramsWorker == null) return;
                copyPlanogramsWorker.DoWork -= CopyPlanogramsWorkerOnDoWork;
                copyPlanogramsWorker.RunWorkerCompleted -= CopyPlanogramsWorkerOnRunWorkerCompleted;
                copyPlanogramsWorker.ProgressChanged -= CopyPlanogramWorkerOnProgressChanged;
            }
            ProgressStatus.Description = "Operation Complete";
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
            timer.Start();
            timer.Tick += (o, args) =>
            {
                timer.Stop();
                ProgressStatus = null;
                timer = null;
            };
        }

        /// <summary>
        ///     Invoked when the copy planogram async task has reported progress.
        /// </summary>
        private void CopyPlanogramWorkerOnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PlanogramGroup planogramGroup = e.UserState as PlanogramGroup;
            String currentGroup = String.Empty;
            if (planogramGroup != null)
            {
                currentGroup = planogramGroup.Name;
            }
            ProgressStatus.Current = e.ProgressPercentage;
            ProgressStatus.Max = 10000;
            ProgressStatus.Description = String.Format(" {0} ({1:p} complete)", currentGroup, (Double)e.ProgressPercentage / 10000);
        }

        /// <summary>
        ///     Copies asynchronously the planograms from the provided folders to an identic set of other folders.
        /// </summary>
        /// <remarks>Both folder structures need to be identical.</remarks>
        private static void CopyPlanogramsWorkerOnDoWork(Object sender, DoWorkEventArgs doWorkEventArgs)
        {
            //  Make sure the event came from the right type of source.
            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker == null) return;

            //  Make sure the context is valid (a tuple with two planogram group view models).
            Tuple<PlanogramGroupViewModel, PlanogramGroupViewModel> context =
                doWorkEventArgs.Argument as Tuple<PlanogramGroupViewModel, PlanogramGroupViewModel>;
            if (context == null || context.Item1 == null || context.Item2 == null)
            {
                Console.WriteLine(
                    @"No data was passed on for CopyPlanogramsWork. A PlanogramGroupViewModel was expected.");
                return;
            }

            //  Setup both planogram group collections and zip them together.
            PlanogramGroupViewModel data = context.Item1;
            PlanogramGroupViewModel copy = context.Item2;
            var planogramGroups =
                data.EnumerateAllChildGroups()
                    .Zip(copy.EnumerateAllChildGroups(), (d, c) => new { source = d, target = c })
                    .ToList();

            //  Process both collections in parallel.
            foreach (var current in planogramGroups)
            {
                //  Check whether the task has been cancelled.
                if (worker.CancellationPending) return;

                try
                {
                    PlanogramInfoList currentPlanogramInfos =
                        PlanogramInfoList.FetchByPlanogramGroupId(current.source.PlanogramGroup.Id);

                    Double total = currentPlanogramInfos.Count;
                    Double count = 0;
                    //  Make a copy of each planogram info's planogram, add it to the target group and report progress.
                    foreach (PlanogramInfo o in currentPlanogramInfos)
                    {
                        Package package =
                            Package.FetchById(o.Id, DomainPrincipal.CurrentUserId, PackageLockType.User)
                                .SaveAs(DomainPrincipal.CurrentUserId, current.target.PlanogramGroup.ParentHierarchy.EntityId);
                        current.target.PlanogramGroup.AssociatePlanograms(new[] { (Int32)package.Id });
                        package.Unlock();
                        Int32 percentProgress = (Int32)(10000 * ++count / total);
                        worker.ReportProgress(percentProgress, current.source.PlanogramGroup);
                    }
                }
                catch (Exception e)
                {
                    //  Cancel the task if there is any exception.
                    Console.WriteLine(e);
                    worker.CancelAsync();
                }
            }
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _masterPlanHierarchyView.ModelChanged -= PlanogramHierarchy_ModelChanged;
                _userFavouritePlanGroupsView.ModelChanged -= UserGroupsView_ModelChanged;

                //if (this.PlanogramHierarchyView != null)
                //{
                //    this.PlanogramHierarchyView.ModelChanged -= PlanogramHierarchy_ModelChanged;
                //}
                //if (this.UserGroupsView != null)
                //{
                //    this.UserGroupsView.ModelChanged -= UserGroupsView_ModelChanged;
                //}

                //dispose of the search timer.
                if(_groupSearchTimer != null)
                {
                    _groupSearchTimer.Stop();
                    _groupSearchTimer.Dispose();
                    _groupSearchTimer = null;
                }
                   

                base.IsDisposed = true;
            }

        }

        #endregion

        #region OLD

        ///// <summary>
        ///// Checks to see if the saving the plan with the given name in the given planogram group
        ///// will result in an overwrite, and prompts the user for confirmation if it will.
        ///// </summary>
        ///// <param name="planogramName">The name of the plan to be saved.</param>
        ///// <param name="planogramGroupId">The id of the group in which the plan is to be saved.</param>
        ///// <returns>
        ///// Null if there are no duplicates to overwrite.
        ///// True if the user has confirmed they wish to overwrite. 
        ///// False if the user does not wish to overwrite.
        ///// </returns>
        //public static Boolean? CheckForOverwrite(String planogramName, Int32 planogramGroupId, Window owner, ref Boolean hideFutureWarnings)
        //{
        //    PlanogramInfoList planogramsInLastGroup = PlanogramInfoList.FetchByPlanogramGroupId(planogramGroupId);
        //    List<PlanogramInfo> planogramsWithTheSameName = planogramsInLastGroup.
        //        Where(p => p.Name.Equals(planogramName, StringComparison.InvariantCultureIgnoreCase)).
        //        ToList();

        //    // If there are no other plans with the same name, return true to indicate
        //    // that a save can be performed.
        //    if (planogramsWithTheSameName.Count == 0) return null;

        //    // There are plans with the same name in the group, so check to see if the user
        //    // wants to overwrite these plans.
        //    Boolean overwriteDuplicates;
        //    if (hideFutureWarnings)
        //    {
        //        overwriteDuplicates = true;
        //    }
        //    else
        //    {
        //        var overwriteMessage = new Galleria.Framework.Controls.Wpf.ModalMessage()
        //        {
        //            ButtonCount = 2,
        //            Button1Content = Message.Generic_Overwrite,
        //            Button2Content = Message.Generic_Cancel,
        //            DefaultButton = ModalMessageButton.Button1,
        //            Title = Message.Generic_Warning,
        //            Header = Message.PlanogramHierarchySelector_PlanogramsAlreadyExist,
        //            Description = String.Format(Message.PlanogramHierarchySelector_PlanogramsAlreadyExist_Description, planogramsWithTheSameName.Count),
        //            IsHideFutureWarningsCheckVisible = true,
        //            Icon = ImageResources.Dialog_Warning,
        //            MessageIcon = ImageResources.Dialog_Warning,
        //            Owner = owner,
        //            WindowStartupLocation = WindowStartupLocation.CenterOwner
        //        };
        //        overwriteMessage.ShowDialog();
        //        overwriteDuplicates = overwriteMessage.Result == ModalMessageResult.Button1;
        //        hideFutureWarnings = overwriteMessage.IsHideFutureWarningsRequested;
        //    }

        //    // If the user doesn't want to overwrite, return false, because they have cancelled
        //    // the save operation.
        //    if (!overwriteDuplicates) return false;

        //    // If we get here, the user wants to overwrite duplicates. We're just going to
        //    // go ahead and save the plan, so we need to remove all the duplicates first. 
        //    // Before we do, we need to make sure that all the plans are unlocked.
        //    if (planogramsWithTheSameName.Any(p => p.IsLocked))
        //    {
        //        var plansLockedMessage = new Galleria.Framework.Controls.Wpf.ModalMessage()
        //        {
        //            ButtonCount = 1,
        //            Button1Content = Message.Generic_Ok,
        //            DefaultButton = ModalMessageButton.Button1,
        //            Title = Message.Generic_Warning,
        //            Header = Message.PlanogramHierarchySelector_PlanogramsAreLocked,
        //            Description = Message.PlanogramHierarchySelector_PlanogramsAreLocked_Description,
        //            Icon = ImageResources.Dialog_Warning,
        //            MessageIcon = ImageResources.Dialog_Warning,
        //            Owner = owner,
        //            WindowStartupLocation = WindowStartupLocation.CenterOwner
        //        };
        //        plansLockedMessage.ShowDialog();
        //        // Indicate that the save should not continue.
        //        return false;
        //    }
        //    // Now, we can delete all the duplicates.
        //    foreach (PlanogramInfo planInfoToDelete in planogramsWithTheSameName)
        //    {
        //        try
        //        {
        //            Package.DeletePackageById(planInfoToDelete.Id, DomainPrincipal.CurrentUserId, PackageLockType.User);
        //        }
        //        catch (Exception)
        //        {
        //            // If we reach this stage, we couldn't delete a plan to overwrite. This may have been because
        //            // of insufficient permissions or due to planogram locks. Either way, there's nothing we can do
        //            // about it, so continue with the save (there are no database constraints
        //            // around plans with duplicate names).
        //        }
        //    }

        //    // Indicate that its safe to continue with the save.
        //    return true;
        //}

        ///// <summary>
        ///// Called whenever the plan hierarchy viewmodel changes
        ///// </summary>
        //private void OnPlanogramHierarchyViewChanged(PlanogramHierarchyViewModel oldValue, PlanogramHierarchyViewModel newValue)
        //{
        //    if (oldValue != null)
        //    {
        //        oldValue.ModelChanged -= PlanogramHierarchy_ModelChanged;
        //    }

        //    if (newValue != null)
        //    {
        //        newValue.ModelChanged += PlanogramHierarchy_ModelChanged;
        //    }

        //    OnPlanogramHierarchyModelChanged();
        //}

        ///// <summary>
        ///// Called whenever the plan hierarchy viewmodel changes
        ///// </summary>
        //private void OnUserGroupsViewChanged(UserPlanogramGroupListViewModel oldValue, UserPlanogramGroupListViewModel newValue)
        //{
        //    if (oldValue != null)
        //    {
        //        oldValue.ModelChanged -= UserGroupsView_ModelChanged;
        //    }

        //    if (newValue != null)
        //    {
        //        newValue.ModelChanged += UserGroupsView_ModelChanged;
        //    }

        //    UpdateFavouriteGroups();
        //}

        ///// <summary>
        /////     Moves the Planograms with the given Ids to be children of the target group.
        ///// </summary>
        ///// <param name="planogramIds">The Ids of the Planograms to move.</param>
        ///// <param name="targetGroup">The group to move the Planograms to.</param>
        ///// <returns>True if operation was successful.</returns>
        //private Boolean MovePlanogramsToGroup(
        //    IEnumerable<Int32> planogramIds, PlanogramGroupViewModel targetGroup, Boolean isMove = true, Boolean supressWarning = false)
        //{
        //    if (planogramIds == null || targetGroup == null) return false;

        //    planogramIds = planogramIds.ToList();

        //    // Confirm action with user.
        //    if (!supressWarning)
        //    {
        //        String description =
        //            isMove ?
        //            Message.PlanogramHierarchySelector_MovePlanograms_WarningDescription :
        //            Message.PlanogramHierarchySelector_CopyPlanograms_WarningDescription;
        //        String title =
        //            isMove ?
        //            Message.PlanogramHierarchySelector_MovePlanograms_WarningTitle :
        //            Message.PlanogramHierarchySelector_CopyPlanograms_WarningTitle;
        //        Boolean continueAfterWarning = ContinueAfterWarning(
        //            Message.Generic_Continue,
        //            Message.Generic_Warning,
        //            String.Format(description, planogramIds.Count(), targetGroup.Name),
        //            title,
        //            ImageResources.DialogQuestion,
        //            Message.Generic_Cancel);
        //        if (!continueAfterWarning) return false;
        //    }

        //    try
        //    {
        //        targetGroup.PlanogramGroup.AssociatePlanograms(planogramIds);
        //    }
        //    catch (DataPortalException e)
        //    {
        //        CommonHelper.RecordException(e);
        //        return false;
        //    }

        //    OnGroupRefreshRequired();

        //    return true;
        //}

        ///// <summary>
        ///// Moves the given PlanogramGroup to be a child of a different PlanogramGroup.
        ///// </summary>
        ///// <param name="source">The Group to be moved.</param>
        ///// <param name="target">The Group to be the new parent.</param>
        ///// <returns>True if operation was successful.</returns>
        //public Boolean MovePlanogramGroup(PlanogramGroupViewModel source, PlanogramGroupViewModel target)
        //{
        //    if (source == null || target == null || Equals(source, target)) return false;

        //    if (target.PlanogramGroup.ChildList.Any(p => p.Name == source.Name))
        //    {
        //        ContinueAfterWarning(Message.Generic_Ok, Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Title,
        //            String.Format(Message.PlanogramHierarchySelector_MovePlanogram_DuplicateName_Description, source.Name, target.Name),
        //            Message.Generic_Warning, ImageResources.DialogWarning);
        //        return false;
        //    }

        //    if (!ContinueAfterWarning(Message.Generic_Continue, Message.Generic_Warning,
        //        String.Format(Message.PlanogramHierarchySelector_MoveGroup_WarningDescription,
        //            source.Name, target.Name),
        //        Message.PlanogramHierarchySelector_MoveGroup_WarningTitle,
        //        ImageResources.DialogQuestion, Message.Generic_Cancel)) return false;

        //    if (!PlanogramHierarchyView.MovePlanogramGroup(source, target)) return false;

        //    OnGroupRefreshRequired();
        //    return true;
        //}

        ///// <summary>
        /////     Moves or copies the <paramref name="data"/> into the currently selected <see cref="PlanogramGroupViewModel"/>
        ///// </summary>
        ///// <param name="data">The data to be pasted. It must be of a type that the ViewModel knows how to paste.</param>
        ///// <param name="isMove">Whether the data should just be moved, or copied to the destination.</param>
        ///// <exception cref="ArgumentNullException">Thrown when <paramref name="data"/> is null.</exception>
        ///// <exception cref="InvalidOperationException">Thrown when there is no target <see cref="SelectedGroup"/>.</exception>
        ///// <exception cref="NotImplementedException">Thrown when the paste operation for the <see cref="data"/>'s type is not implemented. </exception>
        //public void Paste(PlanogramGroupViewModel sourceGroup, Boolean isMove)
        //{
        //    if (sourceGroup == null || SelectedGroup == null) return;

        //    ////  Paste planograms if the data provided is a collection of planogram infos.
        //    //IEnumerable<PlanogramInfo> planogramInfos = data as IEnumerable<PlanogramInfo>;
        //    //if (planogramInfos != null)
        //    //{
        //    //    PastePlanograms(planogramInfos, isMove);
        //    //    return;
        //    //}

        //    //  Paste planogram groups if the data provided is a planogram group view model.
        //    //PlanogramGroupViewModel sourceGroup = data as PlanogramGroupViewModel;
        //    //if (sourceGroup != null)
        //    //{
        //    PasteGroup(sourceGroup, isMove);
        //    //}
        //}

        ///// <summary>
        /////     Moves or copies a collection of <see cref="Planogram"/> into the currently <see cref="SelectedGroup"/>.
        ///// </summary>
        ///// <param name="data">The data to be pasted.</param>
        ///// <param name="isMove">Whether the data should just be moved, or copied to the destination.</param>
        //private void PastePlanograms(IEnumerable<PlanogramInfo> data, Boolean isMove = true)
        //{
        //    // Check for overwrites first.
        //    Boolean dialogDisplayed = false;
        //    Boolean hideFutureWarnings = false;
        //    List<PlanogramInfo> plansToPasteOrMove = new List<PlanogramInfo>();
        //    foreach (PlanogramInfo planInfo in data)
        //    {
        //        Window owner = Window.GetWindow(this.AttachedControl);
        //        if (owner == null)
        //        {
        //            Debug.Fail("Parent could not be found and cast to a Window");
        //            continue;
        //        }
        //        Boolean? checkForOverwrite = CheckForOverwrite(
        //            planInfo.Name, SelectedGroup.Id, owner, ref hideFutureWarnings);
        //        if (checkForOverwrite.HasValue)
        //        {
        //            if (checkForOverwrite.Value)
        //            {
        //                plansToPasteOrMove.Add(planInfo);
        //                dialogDisplayed = true;
        //            }
        //        }
        //        else
        //        {
        //            plansToPasteOrMove.Add(planInfo);
        //            dialogDisplayed = dialogDisplayed || false;
        //        }
        //    }
        //    if (plansToPasteOrMove.Count == 0) return;

        //    // Either select the Ids or those of the copies and move them to the target group.
        //    IEnumerable<Int32> planogramIds;
        //    if (isMove)
        //    {
        //        planogramIds = plansToPasteOrMove.Select(info => info.Id);
        //    }
        //    else
        //    {
        //        planogramIds = plansToPasteOrMove.Select(
        //            info => Package.FetchById(info.Id, DomainPrincipal.CurrentUserId, PackageLockType.User))
        //            .Select(package =>
        //            {
        //                Package copy = package.SaveAs(DomainPrincipal.CurrentUserId);
        //                copy.Unlock();
        //                return (Int32)copy.Id;
        //            });
        //    }
        //    MovePlanogramsToGroup(planogramIds, SelectedGroup, isMove, dialogDisplayed);
        //}

        //public void ResetSelectedItems()
        //{
        //    //load in fav groups if we have not already done so.
        //    if (this.ShowFavouriteGroups &&
        //        _userFavouritePlanGroupsView.Model == null)
        //    {
        //        _userFavouritePlanGroupsView.FetchForCurrentUserAndEntity();
        //    }

        //    //set the selected item.
        //    if (this.FavouriteGroups.Count > 0)
        //    {
        //        this.SelectedTab = PlanogramHierarchySelectorTab.Favourites;
        //    }
        //    else
        //    {
        //        this.SelectedTab = PlanogramHierarchySelectorTab.All;
        //    }
        //}

        ///// <summary>
        /////     Displays a screen to edit the Planogram Group properties.
        ///// </summary>
        ///// <param name="planogramGroupView">The view containing the planogram group to be edited.</param>
        ///// <param name="showMerchGroupSelector">Whether to show or not the Merchandising Group selector (hidden by default).</param>
        ///// <returns>True if the re-name operation was successful.</returns>
        //public Boolean EditPlanogramGroup(PlanogramGroupViewModel planogramGroupView, Boolean showMerchGroupSelector = false)
        //{
        //    if (planogramGroupView == null) return false;
        //    var planogramGroup = planogramGroupView.PlanogramGroup;
        //    var editorViewModel = new PlanogramGroupEditViewModel(planogramGroup) { ShowMerchGroupSelector = showMerchGroupSelector };

        //    editorViewModel.ShowDialog();

        //    if (editorViewModel.DialogResult != true) return false;

        //    // Refresh just the edited viewmodel.
        //    planogramGroupView.Name = planogramGroup.Name;
        //    return true;
        //}

        ///// <summary>
        ///// Adds a new PlanogramGroup under the given parent Group.
        ///// </summary>
        ///// <param name="parentGroupView">The group under which the new group should be added.</param>
        ///// <returns>True if add operation was successful.</returns>
        //public Boolean AddNewPlanogramGroup(PlanogramGroupViewModel parentGroupView)
        //{
        //    PlanogramGroup newPlanogramGroup;
        //    return AddNewPlanogramGroup(parentGroupView, out newPlanogramGroup);
        //}

        ///// <summary>
        ///// Adds a new PlanogramGroup under the given parent Group.
        ///// </summary>
        ///// <param name="parentGroupView">The group under which the new group should be added.</param>
        ///// <param name="planogramGroup">The newly added planogram group.</param>
        ///// <returns>True if add operation was successful.</returns>
        //public Boolean AddNewPlanogramGroup(PlanogramGroupViewModel parentGroupView, out PlanogramGroup planogramGroup)
        //{
        //    planogramGroup = null;
        //    if (parentGroupView == null) return false;
        //    try
        //    {
        //        String newGroupName;
        //        if (!GetNewGroupName(out newGroupName)) return false;

        //        planogramGroup = PlanogramGroup.NewPlanogramGroup();
        //        planogramGroup.Name = newGroupName;
        //        parentGroupView.AddGroup(new PlanogramGroupViewModel(planogramGroup, false));
        //        parentGroupView.SortChildViews();
        //    }
        //    catch (DataPortalException e)
        //    {
        //        Console.WriteLine(e);
        //        Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(Framework.Model.ModelObject<PlanogramGroup>.FriendlyName, OperationType.Save);
        //        return false;
        //    }

        //    OnGroupRefreshRequired();

        //    return true;
        //}

        ///// <summary>
        ///// Prompts the user for a new group name.
        ///// </summary>
        ///// <param name="newGroupName">The string to store the new name in.</param>
        ///// <returns>False if dialog was cancelled.</returns>
        //private Boolean GetNewGroupName(out String newGroupName)
        //{
        //    return Framework.Controls.Wpf.Helpers.PromptUserForValue(
        //        Message.PlanogramHierarchySelector_NewGroupPrompt,
        //        Message.PlanogramHierarchySelector_NewGroupPrompt_Description,
        //        Message.PlanogramHierarchySelector_NewGroupPrompt_NotUniqueDescription,
        //        Message.Generic_Name,
        //        true,
        //        IsUniqueGroupPredicate,
        //        String.Empty,
        //        out newGroupName);
        //}


        ///// <summary>
        ///// Removes the give Planogram Group from the hierarchy, after warning the user of the consequences.
        ///// </summary>
        ///// <param name="planogramGroupView">The Planogram Group View to remove.</param>
        ///// <returns>True if the removal was successful.</returns>
        //public Boolean DeletePlanogramGroup(PlanogramGroupViewModel planogramGroupView)
        //{
        //    if (planogramGroupView == null) return false;

        //    if (planogramGroupView.PlanogramGroup.ParentGroup == null)
        //    {
        //        ContinueAfterWarning(
        //            Message.Generic_Ok,
        //            Message.Generic_Warning,
        //            Message.PlanogramHierarchySelector_DeleteGroup_WarningRootGroupDescription,
        //            String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, planogramGroupView.Name),
        //            ImageResources.DialogWarning);
        //        return false;
        //    }
        //    if (planogramGroupView.PlanogramGroup.ChildList.Any())
        //    {
        //        if (!ContinueAfterWarning(
        //            Message.Generic_Continue,
        //            Message.Generic_Warning,
        //            String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningDescriptionHasGroupChildren, planogramGroupView.Name),
        //            String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, planogramGroupView.Name),
        //            ImageResources.DialogWarning,
        //            Message.Generic_Cancel)) return false;
        //    }
        //    else
        //    {
        //        if (!ContinueAfterWarning(
        //            Message.Generic_Continue,
        //            Message.Generic_Warning,
        //            String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningDescription, planogramGroupView.Name),
        //            String.Format(Message.PlanogramHierarchySelector_DeleteGroup_WarningTitle, planogramGroupView.Name),
        //            ImageResources.DialogWarning,
        //            Message.Generic_Cancel)) return false;
        //    }

        //    try
        //    {
        //        var parentGroupView = PlanogramHierarchyView
        //            .RootGroup
        //            .EnumerateAllChildGroups()
        //            .Where(g => g.Id == planogramGroupView.PlanogramGroup.ParentGroup.Id)
        //            .FirstOrDefault();
        //        if (parentGroupView == null) return false;
        //        parentGroupView.RemoveGroup(planogramGroupView);
        //    }
        //    catch (DataPortalException e)
        //    {
        //        Console.WriteLine(e);
        //        return false;
        //    }

        //    OnGroupRefreshRequired();

        //    return true;
        //}

        #endregion
    }

    public interface IProgressStatus : INotifyPropertyChanged
    {
        /// <summary>
        ///     Name of this progress status instance.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        ///     Current value for this instance's progress.
        /// </summary>
        Int32 Current { get; set; }

        /// <summary>
        ///     Max value for this instance's progress.
        /// </summary>
        Int32 Max { get; set; }

        /// <summary>
        ///     Description for this instance's progress.
        /// </summary>
        String Description { get; set; }
    }

    public class ProgressStatus : IProgressStatus
    {
        #region Fields

        private String _name;
        private Int32 _current;
        private Int32 _max;
        private String _description;

        #endregion

        #region Binding Property Paths

        private static readonly PropertyPath NameProperty = GetPropertyPath(p => p.Name);
        private static readonly PropertyPath CurrentProperty = GetPropertyPath(p => p.Current);
        private static readonly PropertyPath MaxProperty = GetPropertyPath(p => p.Max);
        private static readonly PropertyPath DescriptionProperty = GetPropertyPath(p => p.Description);

        #endregion

        #region Properties

        /// <summary>
        ///     Name of this progress status instance.
        /// </summary>
        public String Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(NameProperty);
            }
        }

        /// <summary>
        ///     Current value for this instance's progress.
        /// </summary>
        public Int32 Current
        {
            get { return _current; }
            set
            {
                _current = value;
                OnPropertyChanged(CurrentProperty);
            }
        }

        /// <summary>
        ///     Max value for this instance's progress.
        /// </summary>
        public Int32 Max
        {
            get { return _max; }
            set
            {
                _max = value;
                OnPropertyChanged(MaxProperty);
            }
        }

        /// <summary>
        ///     Description for this instance's progress.
        /// </summary>
        public String Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged(DescriptionProperty);
            }
        }

        #endregion

        #region Methods

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Static Helper Mothds

        /// <summary>
        ///     Helper method to get the property path for the <see cref="ProgressStatus" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<ProgressStatus, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }
}
