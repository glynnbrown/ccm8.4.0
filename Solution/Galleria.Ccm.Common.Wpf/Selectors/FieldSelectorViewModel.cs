﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// CCM-25883 : N.Haywood
//  Changed behaviour so that additions are made in the order that you click them
// CCM-26009 : N.Haywood
//  Added {} to operators
// CCM-27821 : J.Pickup
//  Fixed the introdction of {} and improved the intelligence of the formula builder logic to add braces after first set created followed by user text.
#endregion
#region Version History: (CCM 8.3)
// V8-31721 : L.Ineson
//  Added functions.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for FieldSelectorWindow
    /// </summary>
    public sealed class FieldSelectorViewModel : ViewModelAttachedControlObject<FieldSelectorWindow>, IDataErrorInfo
    {
        #region Fields

        private Boolean? _dialogResult;

        private FieldSelectorInputType _inputType;
        private FieldSelectorResolveType _validationType;
        private ReadOnlyCollection<FieldSelectorGroup> _fieldGroups;
        private FieldSelectorGroup _allFieldsGroup;
        private FieldSelectorGroup _selectedFieldGroup;

        private readonly BulkObservableCollection<ObjectFieldInfo> _selectedGroupFields = new BulkObservableCollection<ObjectFieldInfo>();
        private ReadOnlyBulkObservableCollection<ObjectFieldInfo> _selectedGroupFieldsRO;
        private ObjectFieldInfo _selectedField;

        private ReadOnlyCollection<ObjectFieldExpressionOperator> _availableOperators;

        private String _fieldText;
        private String _fieldTextError;

        private String _text; //the text that the user is editing.
        private Int32 _caretIndex; //the position of the caret

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath InputTypeProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.InputType);
        public static readonly PropertyPath ValidationTypeTypeProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.ValidationType);
        public static readonly PropertyPath TextProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.Text);
        public static readonly PropertyPath FieldTextProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.FieldText);
        public static readonly PropertyPath AvailableFieldGroupsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath SelectedGroupFieldsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedGroupFields);
        public static readonly PropertyPath SelectedFieldProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedField);
        public static readonly PropertyPath AvailableOperatorsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableOperators);
        public static readonly PropertyPath CaretIndexProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.CaretIndex);

        //Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ClearCommandProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.ClearCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the dialog result 
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the input type of the selector.
        /// </summary>
        public FieldSelectorInputType InputType
        {
            get { return _inputType; }
        }

        /// <summary>
        /// Gets/Sets what type of field text this 
        /// selector is being used to produce
        /// </summary>
        public FieldSelectorResolveType ValidationType
        {
            get { return _validationType; }
        }

        /// <summary>
        /// Gets/Sets the text being edited by the user.
        /// </summary>
        public String Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnTextChanged(value);
                OnPropertyChanged(TextProperty);
            }
        }

        /// <summary>
        /// Gets the final text that will assigned.
        /// </summary>
        public String FieldText
        {
            get { return _fieldText; }
            private set
            {
                _fieldText = value;
                OnPropertyChanged(FieldTextProperty);
            }
        }


        /// <summary>
        /// Gets the list of available item types
        /// </summary>
        public ReadOnlyCollection<FieldSelectorGroup> AvailableFieldGroups
        {
            get { return _fieldGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected item type
        /// </summary>
        public FieldSelectorGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected item type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ObjectFieldInfo> SelectedGroupFields
        {
            get
            {
                if (_selectedGroupFieldsRO == null)
                {
                    _selectedGroupFieldsRO = new ReadOnlyBulkObservableCollection<ObjectFieldInfo>(_selectedGroupFields);
                }
                return _selectedGroupFieldsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected field
        /// </summary>
        public ObjectFieldInfo SelectedField
        {
            get { return _selectedField; }
            set
            {
                _selectedField = value;
                OnPropertyChanged(SelectedFieldProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available operators.
        /// </summary>
        public ReadOnlyCollection<ObjectFieldExpressionOperator> AvailableOperators
        {
            get { return _availableOperators; }
        }

        /// <summary>
        /// Gets/Sets the position of the caret on the text.
        /// </summary>
        public Int32 CaretIndex
        {
            get { return _caretIndex; }
            set
            {
                if (_caretIndex != value)
                {
                    _caretIndex = value;
                    OnPropertyChanged(CaretIndexProperty);
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public FieldSelectorViewModel(FieldSelectorInputType inputType, FieldSelectorResolveType validationType,
            String fieldText, IEnumerable<FieldSelectorGroup> availableFieldGroups)
        {
            _inputType = inputType;
            _validationType = validationType;

            _allFieldsGroup = new FieldSelectorGroup(Message.ColumnLayoutEditor_AllFields, availableFieldGroups.SelectMany(f => f.Fields).Distinct());

            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            groups.Add(_allFieldsGroup);
            groups.AddRange(availableFieldGroups);
            _fieldGroups = groups.AsReadOnly();

            this.SelectedFieldGroup = this.AvailableFieldGroups.First();

            _availableOperators = ObjectFieldExpression.GetSupportedOperators().AsReadOnly();

            //set the text value
            this.Text = ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(fieldText, _allFieldsGroup.Fields);

            //initialize the caret to the end
            this.CaretIndex =
                (!String.IsNullOrWhiteSpace(this.Text)) ?
                this.Text.Length - 1
                : 0;
        }

        #endregion

        #region Commands

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits the current formula.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (String.IsNullOrEmpty(this.FieldText)
                && this.SelectedField == null)
            {
                return false;
            }

            if (!String.IsNullOrEmpty(_fieldTextError))
            {
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            if (String.IsNullOrEmpty(this.FieldText))
            {
                //update the field text
                this.FieldText = this.SelectedField.FieldPlaceholder;
            }

            this.DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the change and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region Clear

        private RelayCommand _clearCommand;

        /// <summary>
        /// Clears out the current formula
        /// </summary>
        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed())
                    {
                        FriendlyName = Message.FieldSelector_ClearFormula,
                        SmallIcon = ImageResources.FieldSelector_ClearFormula
                    };
                    base.ViewModelCommands.Add(_clearCommand);
                }
                return _clearCommand;
            }
        }

        private void Clear_Executed()
        {
            this.Text = null;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the FieldText property value changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnTextChanged(String newValue)
        {
            //update the field text
            this.FieldText = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(this.Text, _allFieldsGroup.Fields);


            _fieldTextError = ObjectFieldInfo.ValidateFieldText(newValue, _allFieldsGroup.Fields,
                (ValidationType == FieldSelectorResolveType.SingleValue));
        }

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFieldGroupChanged(FieldSelectorGroup newValue)
        {
            _selectedGroupFields.Clear();

            _selectedGroupFields.AddRange(newValue.Fields);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the given field to the text.
        /// </summary>
        /// <param name="field"></param>
        public void AddFieldToText(ObjectFieldInfo field)
        {
            String addText = field.FieldFriendlyPlaceholder;
            Int32 caretIdx = Math.Max(0, this.CaretIndex);
            caretIdx = Math.Min(this.CaretIndex,
                (!String.IsNullOrEmpty(this.Text))? this.Text.Length : 0);

            StringBuilder sb = new StringBuilder(this.Text);

            

            Int32 initialSize = sb.Length;

            //check if the end of the text was a function or brace
            if (caretIdx > 0 && this.Text[caretIdx-1] == '(')
            {
                if (caretIdx >= sb.Length)
                {
                    sb.Append(addText);
                    sb.Append(")");
                }
                else
                {
                    sb.Insert(caretIdx, addText);
                    sb.Insert(caretIdx + addText.Length, ")");
                }
            }
            else
            {
                //When caret is precluding an operator by either 1 or 2 indexes then we increase caret to insert next field in the correct position. 
                //(Fields always succeed Operands).
                if (caretIdx > 0 && caretIdx < sb.Length && (AvailableOperators.Any(t => t.Symbol == sb[caretIdx].ToString())))
                {
                    caretIdx++;
                }
                else if (caretIdx > 0 && caretIdx + 1 < sb.Length && AvailableOperators.Any(t => t.Symbol == sb[caretIdx + 1].ToString()))
                {
                    caretIdx++;
                    caretIdx++;
                }

                if (caretIdx >= sb.Length)
                {
                    sb.Append(" ");
                    sb.Append(addText);
                }
                else
                {
                    sb.Insert(caretIdx, " ");
                    sb.Insert(caretIdx + 1, addText);
                }
            }

            

            this.Text = sb.ToString().Trim();

            Int32 finalSize = sb.Length;
            this.CaretIndex = caretIdx + (finalSize - initialSize);
        }

        /// <summary>
        /// Adds the given operator to the end of the field text
        /// </summary>
        /// <param name="fieldOperator"></param>
        public void AddOperator(ObjectFieldExpressionOperator fieldOperator)
        {
            Int32 origCaretIdx = Math.Max(0, this.CaretIndex);

            if (fieldOperator.IsFunction)
            {
                //add the function name plus an opening brace to the caret position
                String functionText = fieldOperator.Symbol + "(";
                Int32 insertStart = Math.Max(0, this.CaretIndex);

                if (String.IsNullOrEmpty(this.Text))
                {
                    this.Text = functionText;
                }
                else
                {
                    this.Text = (insertStart < this.Text.Length) ?
                        this.Text.Insert(insertStart, functionText)
                        : this.Text + " " + functionText;
                }

                this.CaretIndex = Math.Max(0, origCaretIdx) + functionText.Length;
                return;
            }



            String addText = fieldOperator.Symbol;
            Int32 caretIdx = Math.Max(0, this.CaretIndex);
            StringBuilder sb = new StringBuilder(this.Text);

            Boolean addBraces = (fieldOperator.Symbol != "(" && fieldOperator.Symbol != ")");

            //Add the start brace:
            if (addBraces)
            {
                String beforeCaret = (caretIdx > 0) ? this.Text.Substring(0, caretIdx - 1) : String.Empty;

                //get the index of the last sq bracket
                Int32 startBraceIdx = 0;
                if (beforeCaret.Contains(ObjectFieldInfo.FieldStart))
                {
                    startBraceIdx = Math.Max(0, beforeCaret.LastIndexOf(ObjectFieldInfo.FieldStart) - 1);
                }

                //If we are not in {} allready then lets go and add them else we dont need to.
                Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                //-1 for covering when not found (frst time only).
                if (closeCurlyIndex == -1)
                {
                    sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                    caretIdx += 2;
                }
                else
                {
                    //HERE THE must be some check about ptr being after that last } else add
                    if (closeCurlyIndex < openCurlyIndex || caretIdx > closeCurlyIndex)
                    {
                        sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                        caretIdx += 2;
                    }
                }

                //What is this doing and do we need it? probably wants moving into the conditions above
                if (startBraceIdx != 0)
                {
                    sb.Insert(startBraceIdx, " ");
                    caretIdx += 1;
                };
            }


            Int32 initialSize = sb.Length;

            if (caretIdx >= sb.Length)
            {
                sb.Append(" ");
                sb.Append(addText);

                //Add the end brace:
                if (addBraces)
                {
                    sb.Append(" " + ObjectFieldInfo.FormulaEnd);
                }

            }
            else
            {
                sb.Insert(caretIdx, " ");
                sb.Insert(caretIdx + 1, addText);
                sb.Insert(caretIdx + 1 + addText.Length, " ");

                //Add the end brace:
                if (addBraces)
                {
                    //Again, we only add the braces when we aren't already in a set {}.
                    Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                    Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                    if (closeCurlyIndex < openCurlyIndex)
                    {
                        sb.Insert(caretIdx + 1 + addText.Length + 1, " " + ObjectFieldInfo.FormulaEnd);
                    }
                }
            }


            this.Text = sb.ToString().Trim();

            Int32 finalSize = sb.Length;

            if (addBraces)
            {
                //if braces were added place the caret just within the closing one.
                this.CaretIndex = origCaretIdx + (finalSize - initialSize) - 1;
            }
            else
            {
                this.CaretIndex = origCaretIdx + (finalSize - initialSize);
            }
        }

        #endregion

        #region IDataErrorInfo Members

        private String _error;
        public String Error
        {
            get { return _error; }
            private set
            {
                _error = value;
            }
        }

        public String this[String columnName]
        {
            get
            {
                String error = null;

                if (columnName == TextProperty.Path)
                {
                    error = _fieldTextError;
                }


                return error;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.IsDisposed = true;
            }

        }

        #endregion

        #region Nested Classes

        ///// <summary>
        ///// Represents an operator item available in the field selector.
        ///// </summary>
        //public sealed class FieldSelectorOperator
        //{
        //    #region Properties

        //    public String Symbol { get; set; }
        //    public String Display { get; set; }
        //    public Boolean IsNumericOnlyOperator { get; set; }
        //    public Boolean IsFunction { get; set; }
        //    public String Description { get; set; }

        //    #endregion

        //    public FieldSelectorOperator(String symbol, String description)
        //    {
        //        this.Symbol = symbol;
        //        this.Description = description;
        //    }

        //    public override string ToString()
        //    {
        //        return this.Display;
        //    }

        //    /// <summary>
        //    /// Returns a list of all supported operators.
        //    /// </summary>
        //    /// <returns></returns>
        //    public static ReadOnlyCollection<FieldSelectorOperator> GetAllSupportedOperators()
        //    {
        //        return new ReadOnlyCollection<FieldSelectorOperator>(new FieldSelectorOperator[]
        //        {
        //           new FieldSelectorOperator("+", Message.FieldSelectorOperator_Add){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("-", Message.FieldSelectorOperator_Subtract){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("*", Message.FieldSelectorOperator_Multiply){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("/", Message.FieldSelectorOperator_Divide){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("<", Message.FieldSelectorOperator_LessThan){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("<=",Message.FieldSelectorOperator_LessThanOrEqualTo){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator(">", Message.FieldSelectorOperator_MoreThan){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator(">=",Message.FieldSelectorOperator_MoreThanOrEqualTo){IsNumericOnlyOperator =true},
        //           new FieldSelectorOperator("=", Message.FieldSelectorOperator_Equals),
        //           new FieldSelectorOperator("<>", Message.FieldSelectorOperator_NotEquals),
        //           new FieldSelectorOperator("(", Message.FieldSelectorOperator_OpenBracket),
        //           new FieldSelectorOperator(")", Message.FieldSelectorOperator_CloseBracket),
                   
        //           new FieldSelectorOperator("ROUNDUP", Message.FieldSelectorOperator_RoundUp){IsFunction = true, IsNumericOnlyOperator=true},
        //           new FieldSelectorOperator("ROUNDDOWN", Message.FieldSelectorOperator_RoundDown){IsFunction = true, IsNumericOnlyOperator=true},
        //           new FieldSelectorOperator("UPPERCASE", Message.FieldSelectorOperator_Uppercase){IsFunction = true},
        //           new FieldSelectorOperator("LOWERCASE", Message.FieldSelectorOperator_Lowercase){IsFunction = true},
        //           new FieldSelectorOperator("CONTAINS", Message.FieldSelectorOperator_Contains){IsFunction = true},
        //           new FieldSelectorOperator("NOTCONTAINS", Message.FieldSelectorOperator_NotContains){IsFunction = true},

        //           new FieldSelectorOperator("SUM", Message.FieldSelectorOperator_Sum){IsFunction = true, IsNumericOnlyOperator=true},
        //           new FieldSelectorOperator("AVG", Message.FieldSelectorOperator_Average){IsFunction = true, IsNumericOnlyOperator=true},
        //           new FieldSelectorOperator("MIN", Message.FieldSelectorOperator_Min){IsFunction = true},
        //           new FieldSelectorOperator("MAX", Message.FieldSelectorOperator_Max){IsFunction = true},
        //           new FieldSelectorOperator("COUNT", Message.FieldSelectorOperator_Count){IsFunction = true},
                     
        //           new FieldSelectorOperator("IF",  Message.FieldSelectorOperator_If){IsFunction = true},
        //           new FieldSelectorOperator("AND", Message.FieldSelectorOperator_And),
        //           new FieldSelectorOperator("OR", Message.FieldSelectorOperator_Or),

        //        });

        //    }

        //}

        #endregion
    }

    #region FieldSelectorGroup

    /// <summary>
    /// Represents a group of fields that may be selected.
    /// </summary>
    public class FieldSelectorGroup
    {
        #region Fields
        private String _name;
        private ReadOnlyCollection<ObjectFieldInfo> _fields;
        #endregion

        #region Properties

        public String Name
        {
            get { return _name; }
        }

        public ReadOnlyCollection<ObjectFieldInfo> Fields
        {
            get { return _fields; }
        }

        #endregion

        #region Constructor

        public FieldSelectorGroup(String name, IEnumerable<ObjectFieldInfo> fields)
        {
            _name = name;
            _fields = new ReadOnlyCollection<ObjectFieldInfo>(fields.ToList());
        }

        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }

    #endregion

    #region FieldSelectorResolveType
    /// <summary>
    /// Denotes the different error types this screen can show.
    /// </summary>
    public enum FieldSelectorResolveType
    {
        /// <summary>
        /// The fields must resolve to a single value
        /// This may be of any type.
        /// </summary>
        SingleValue,
        /// <summary>
        /// The fields will be resolved to text
        /// </summary>
        Text
    }
    #endregion

    #region FieldSelectorInputType
    /// <summary>
    /// Denotes the different types of input supported by this selector
    /// </summary>
    public enum FieldSelectorInputType
    {
        SingleField,
        MultiField,
        Formula
    }
    #endregion
}
