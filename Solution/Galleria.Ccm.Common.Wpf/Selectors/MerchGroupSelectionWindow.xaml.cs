﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27150 : L.Ineson 
//  Copied into common.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for MerchGroupSelectionWindow.xaml
    /// </summary>
    public sealed partial class MerchGroupSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields
        private ProductHierarchy _merchHierarchy;
        private IEnumerable<Int32> _excludeIds;
        private ProductGroupViewModel _rootUnitView;
        #endregion

        #region Properties

        #region SelectionModeProperty

        public static readonly DependencyProperty SelectionModeProperty =
           DependencyProperty.Register("SelectionMode", typeof(DataGridSelectionMode), typeof(MerchGroupSelectionWindow),
           new PropertyMetadata(DataGridSelectionMode.Extended));

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return (DataGridSelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        #endregion

        #region SelectionResult property

        private List<ProductGroup> _selectionResult;
        /// <summary>
        /// Returns a readonly collection of the group/groups selected.
        /// </summary>
        public List<ProductGroup> SelectionResult
        {
            get { return _selectionResult; }
            private set { _selectionResult = value; }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new merch group selection window.
        /// No null group shown, extended selection mode.
        /// </summary>
        public MerchGroupSelectionWindow() : this(null, null, DataGridSelectionMode.Extended) { }

        /// <summary>
        /// Creates a new merch group selection window.
        /// No null group shown, extended selection mode.
        /// </summary>
        /// <param name="merchHierarchy">The hierarchy to use</param>
        /// <param name="excludeIds">Exclusion Ids</param>
        public MerchGroupSelectionWindow(ProductHierarchy merchHierarchy, IEnumerable<Int32> excludeIds) :
            this(merchHierarchy, excludeIds, DataGridSelectionMode.Extended) { }


        /// <summary>
        /// Creates a new merch group selection window.
        /// </summary>
        /// <param name="showNullGroup">Toggle null group visibility</param>
        /// <param name="excludeIds">Ids to be excluded</param>
        /// <param name="selectionMode">The selection mode</param>
        public MerchGroupSelectionWindow(IEnumerable<Int32> excludeIds, DataGridSelectionMode selectionMode)
            : this(null, excludeIds, selectionMode) { }


        /// <summary>
        /// Creates a new merch group selection window.
        /// </summary>
        /// <param name="merchHierarchy">The hierarchy to use</param>
        /// <param name="showNullGroup">Toggle null group visibility</param>
        /// <param name="excludeIds">Ids to be excluded</param>
        /// <param name="selectionMode">The selection mode</param>
        public MerchGroupSelectionWindow(ProductHierarchy merchHierarchy,
            IEnumerable<Int32> excludeIds, DataGridSelectionMode selectionMode)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            _merchHierarchy = merchHierarchy;
            if (_merchHierarchy == null)
            {
                _merchHierarchy = ProductHierarchy.FetchByEntityId(CCMClient.ViewState.EntityId);
            }
            _excludeIds = excludeIds;

            this.SelectionMode = selectionMode;


            InitializeComponent();


            this.Loaded += new RoutedEventHandler(MerchGroupSelectionWindow_Loaded);

        }

        private void MerchGroupSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MerchGroupSelectionWindow_Loaded;

            FocusPrefilter();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            ReloadColumns();

            //get all groups in the hierarchy
            _rootUnitView = new ProductGroupViewModel(_merchHierarchy.RootGroup);

            List<ProductGroupViewModel> prodGroupViews = _rootUnitView.GetAllChildUnits().ToList();

            //remove any exclusions
            if (_excludeIds != null)
            {
                prodGroupViews.RemoveAll(g => _excludeIds.Contains(g.ProductGroup.Id));
            }


            flattenedGrid.ItemsSourceExtended = prodGroupViews;
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            OnSelectionComplete();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void flattenedGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.SelectionMode == DataGridSelectionMode.Single)
            {
                OnSelectionComplete();
            }
        }

        /// <summary>
        /// When enter is pressed return selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (flattenedGrid.SelectedItems.Count > 0)
                    {
                        e.Handled = true;
                        OnSelectionComplete();
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Carries out prefiltering when raised by the datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_PrefilterItem(object sender, ExtendedDataGridFilterEventArgs e)
        {
            String prefilter = flattenedGrid.PrefilterText;
            if (!String.IsNullOrEmpty(prefilter))
            {
                ProductGroupViewModel row = e.Item as ProductGroupViewModel;
                if (row != null)
                {
                    if (row.ProductGroup == null)
                    {
                        e.Accepted = false;
                    }
                    else
                    {
                        String productGroupDesc = row.ProductGroup.ToString().ToLowerInvariant();
                        if (!productGroupDesc.Contains(prefilter.ToLowerInvariant()))
                        {
                            e.Accepted = false;
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads level grid columns
        /// </summary>
        private void ReloadColumns()
        {
            if (flattenedGrid != null)
            {
                //clear out the existing columns
                flattenedGrid.Columns.Clear();

                //add in the new columns
                List<DataGridColumn> columnSet = GetProductGroupColumnSet(_merchHierarchy, /*includeRootCol*/true);
                if (columnSet.Count > 0)
                {
                    foreach (DataGridColumn col in columnSet)
                    {
                        flattenedGrid.Columns.Add(col);
                    }
                }
            }
        }

        private void OnSelectionComplete()
        {
            this.SelectionResult = flattenedGrid.SelectedItems.Cast<ProductGroupViewModel>().Select(s => s.ProductGroup).ToList();
            this.DialogResult = true;
        }

        /// <summary>
        /// Places focus into the prefilter search textbox
        /// </summary>
        private void FocusPrefilter()
        {
            if (this.flattenedGrid != null
                && this.flattenedGrid.Template != null)
            {
                Border prefilterBar = this.flattenedGrid.Template.FindName("PART_PrefilterBar", flattenedGrid) as Border;
                if (prefilterBar != null
                    && prefilterBar.Child != null)
                {
                    TextBox prefilterBox = prefilterBar.Child.FindVisualDescendent<TextBox>();
                    if (prefilterBox != null)
                    {
                        prefilterBox.Focus();
                    }
                }
            }
        }

        /// <summary>
        /// Returns the flattened readonly columnset for displaying product groups in a grid.
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <returns></returns>
        private static List<DataGridColumn> GetProductGroupColumnSet(ProductHierarchy hierarchy, Boolean includeRootColumn)
        {
            List<DataGridColumn> columnSet = new List<DataGridColumn>();

            if (hierarchy != null)
            {
                System.Windows.Style elementStyle = new System.Windows.Style(typeof(TextBlock));
                elementStyle.BasedOn = DataGridExtendedTextColumn.DefaultElementStyle;
                elementStyle.Setters.Add(new Setter(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis));


                foreach (ProductLevel level in hierarchy.EnumerateAllLevels())
                {
                    String searchKey = level.Name;

                    if (!level.IsRoot)
                    {
                        //Create the label col
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            ProductGroupViewModel.LevelPathValuesProperty.Path, searchKey, ProductGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn col = new DataGridExtendedTextColumn()
                        {
                            IsReadOnly = true,
                            Width = 150,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle
                        };


                        //bind the column header to the level name
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ProductLevel.NameProperty.Name) { Source = level });

                        //add the column to the set
                        columnSet.Add(col);

                    }
                    else if (includeRootColumn)
                    {
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            ProductGroupViewModel.LevelPathValuesProperty.Path, searchKey, ProductGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn rootCol = new DataGridExtendedTextColumn()
                        {
                            Header = null,
                            Width = 40,
                            IsReadOnly = true,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle
                        };
                        rootCol.SetValue(ExtendedDataGrid.CanUserFilterProperty, false);
                        columnSet.Add(rootCol);

                    }

                }
            }

            return columnSet;
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        #endregion

    }
}
