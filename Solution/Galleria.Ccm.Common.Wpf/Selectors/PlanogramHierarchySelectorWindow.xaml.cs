﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramHierarchySelectorWindow.xaml
    /// </summary>
    public partial class PlanogramHierarchySelectorWindow 
    {
        #region Properties

        #region ViewModel Property

        /// <summary>
        ///     The view model that this screen uses to interact with the model.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanogramHierarchySelectorWindowViewModel), typeof(PlanogramHierarchySelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Get or set this instance's view model.
        /// </summary>
        public PlanogramHierarchySelectorWindowViewModel ViewModel
        {
            get { return (PlanogramHierarchySelectorWindowViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Invloked when this instance's view model property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = obj as PlanogramHierarchySelectorWindow;
            if (sender == null) return;

            var oldViewModel = e.OldValue as PlanogramHierarchySelectorWindowViewModel;
            if (oldViewModel != null)
            {
                oldViewModel.DialogResultSet -= sender.OnViewModelDialogResultSet;
            }

            var newViewModel = e.NewValue as PlanogramHierarchySelectorWindowViewModel;
            if (newViewModel != null)
            {
                newViewModel.DialogResultSet += sender.OnViewModelDialogResultSet;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramHierarchySelectorWindow"/> that uses the given <paramref name="viewModel"/>.
        /// </summary>
        public PlanogramHierarchySelectorWindow(PlanogramHierarchySelectorWindowViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers
        
        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null ||
                !ViewModel.DialogResult.HasValue) return;

            DialogResult = ViewModel.DialogResult;
            Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel == null) return;

            ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
            ViewModel.Dispose();
        }

        private void xSelector_Loaded(object sender, RoutedEventArgs e)
        {

        }

        #endregion

    }
}
