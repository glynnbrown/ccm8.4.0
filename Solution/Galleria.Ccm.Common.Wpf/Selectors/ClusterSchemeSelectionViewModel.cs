﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.02)
// V8-28987 : I.George
//  Created 
#endregion

#region Version History: (CCM 8.10)
// V8-30169 : L.Luong
//  Added ClusterSchemeProductGroupRow so that CategoryName and CategoryCode can be shown
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using System.Windows;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public sealed class ClusterSchemeSelectionViewModel : ViewModelAttachedControlObject<ClusterSchemeSelectionWindow>
    {
        #region Fields
        private ObservableCollection<ClusterSchemeInfo> _clusterSchemeInfo = new ObservableCollection<ClusterSchemeInfo>();
        private ClusterSchemeInfoListViewModel _clusterSchemeInfoListViewModel = new ClusterSchemeInfoListViewModel();
        private readonly BulkObservableCollection<ClusterSchemeProductGroupRow> _clusterSchemeProductGroupRows = new BulkObservableCollection<ClusterSchemeProductGroupRow>();
        private ClusterSchemeProductGroupRow _selectedClusterSchemeProductGroupRow;
        private ClusterSchemeInfo _selectedClusterSchemeInfo;
        private Boolean? _dialogResult;
        #endregion

        #region Binding Property

        //properties
        public static readonly PropertyPath ClusterSchemeProductGroupRowsProperty = WpfHelper.GetPropertyPath<ClusterSchemeSelectionViewModel>(p => p.ClusterSchemeProductGroupRows);
        public static readonly PropertyPath SelectedClusterSchemeProductGroupRowProperty = WpfHelper.GetPropertyPath<ClusterSchemeSelectionViewModel>(p => p.SelectedClusterSchemeProductGroupRow);

        //commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ClusterSchemeSelectionViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ClusterSchemeSelectionViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the list of cluster scheme infos
        /// </summary>
        public ReadOnlyBulkObservableCollection<ClusterSchemeInfo> MasterClusterSchemeInfoList
        {
            get { return _clusterSchemeInfoListViewModel.BindableCollection; }
        }

        /// <summary>
        /// Gets/Sets the selected cluster scheme info
        /// </summary>
        public ClusterSchemeInfo SelectedClusterSchemeInfo
        {
            get { return _selectedClusterSchemeInfo; }
            set { _selectedClusterSchemeInfo = value; }
        }

        /// <summary>
        /// Returns the collection of ClusterSchemeProductGroupRows
        /// </summary>
        public IEnumerable<ClusterSchemeProductGroupRow> ClusterSchemeProductGroupRows
        {
            get { return _clusterSchemeProductGroupRows; }
        }

        /// <summary>
        /// Gets/Sets the selected cluster scheme info
        /// </summary>
        public ClusterSchemeProductGroupRow SelectedClusterSchemeProductGroupRow
        {
            get { return _selectedClusterSchemeProductGroupRow; }
            set
            {
                _selectedClusterSchemeProductGroupRow = value;

                _selectedClusterSchemeInfo = _selectedClusterSchemeProductGroupRow.ClusterSchemeInfo;
                OnPropertyChanged(SelectedClusterSchemeProductGroupRowProperty);
            }
        }

        #endregion

        #region Constructor

        public ClusterSchemeSelectionViewModel()
        {
            _clusterSchemeInfoListViewModel.FetchForCurrentEntity();
            UpdateClusterSchemeInfo();
        }
        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _oKCommand;

        /// <summary>
        /// Command to set clusterScheme
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_oKCommand == null)
                {
                    _oKCommand = new RelayCommand(
                       p => OKCommand_Executed(),
                       p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_oKCommand);
                }
                return _oKCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            if (this.SelectedClusterSchemeInfo == null)
            {
                OKCommand.DisabledReason = Message.LocationClusterSelection_NoClusterSchemeSelected;
                return false;
            }
            return true;
        }

        public void OKCommand_Executed()
        {
            this.DialogResult = true;
        }
        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;
        /// <summary>
        /// Command to cancel the opened window 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                       p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.GenericCancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        public void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods 

        /// <summary>
        /// Add the clusterSchemeInfo
        /// </summary>
        private void UpdateClusterSchemeInfo()
        {
            _clusterSchemeInfo.Clear();
            ProductGroupInfo productGroupInfo = null;
            IEnumerable<ClusterSchemeInfo> results = this.MasterClusterSchemeInfoList;

            // gets the list of product groups that are linked to the cluster scheme
            ProductGroupInfoList productGroupList = ProductGroupInfoList.FetchByProductGroupIds(
                results.Where(c => c.ProductGroupId != null)
                .Select(c => c.ProductGroupId.Value).Distinct().ToList());

            foreach (ClusterSchemeInfo clusterSchemeInfo in results)
            {
                _clusterSchemeInfo.Add(clusterSchemeInfo);
                if(clusterSchemeInfo.ProductGroupId != null && productGroupList.Any())
                {
                    productGroupInfo = productGroupList.Where(p => p.Id == clusterSchemeInfo.ProductGroupId).FirstOrDefault();
                }

                _clusterSchemeProductGroupRows.Add(new ClusterSchemeProductGroupRow(MasterClusterSchemeInfoList, productGroupInfo, clusterSchemeInfo));
            }
       }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Cluster Scheme showing product group name and code
    /// </summary>
    public class ClusterSchemeProductGroupRow : INotifyPropertyChanged
    {
        #region Fields

        private ClusterSchemeInfo _clusterSchemeInfo;
        private ProductGroupInfo _productGroupInfo;
        private IEnumerable<ClusterSchemeInfo> _availableClusterSchemeInfos;

        #endregion

        #region Property Paths

        public static readonly PropertyPath ClusterSchemeInfoProperty = WpfHelper.GetPropertyPath<ClusterSchemeProductGroupRow>(p => p.ClusterSchemeInfo);
        public static readonly PropertyPath ProductGroupInfoProperty = WpfHelper.GetPropertyPath<ClusterSchemeProductGroupRow>(p => p.ProductGroupInfo);
        public static readonly PropertyPath AvailableClusterSchemeInfosProperty = WpfHelper.GetPropertyPath<ClusterSchemeProductGroupRow>(p => p.AvailableClusterSchemeInfos);

        #endregion

        #region Properties

        /// <summary>
        /// Exposes the Cluster Scheme Info
        /// </summary>
        public ClusterSchemeInfo ClusterSchemeInfo
        {
            get { return _clusterSchemeInfo; }
        }

        /// <summary>
        /// Exposes the Product Group Info
        /// </summary>
        public ProductGroupInfo ProductGroupInfo
        {
            get { return _productGroupInfo; }
        }

        /// <summary>
        /// Exposes Availible ClusterSchemeInfos
        /// </summary>
        public IEnumerable<ClusterSchemeInfo> AvailableClusterSchemeInfos
        {
            get { return _availableClusterSchemeInfos; }
        }

        #endregion

        #region Constructor

        public ClusterSchemeProductGroupRow(IEnumerable<ClusterSchemeInfo> availableClusterSchemeInfos, ProductGroupInfo productGroupInfo, ClusterSchemeInfo clusterSchemeInfo)
        {
            _availableClusterSchemeInfos = availableClusterSchemeInfos;
            _clusterSchemeInfo = clusterSchemeInfo;
            _productGroupInfo = productGroupInfo;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
