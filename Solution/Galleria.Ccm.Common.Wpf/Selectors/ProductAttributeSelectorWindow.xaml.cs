﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 801
// V8-27494 : A.Kuszyk
//	Created.
#endregion

#region Version History: CCM 802
// V8-28940 : L.Ineson
//  Amended to use dg row drop caught handling.
#endregion

#region Version History: CCM 8.1.1
// V8-30516 : M.Shelley
//  Added mouse double click behaviour to both the available columns and selected columns grids.
#endregion

#region Version History: CCM 8.30
// V8-32359 : J.Pickup
//  Changes to visibility of default columns.
// V8-32359 : A.Silva
//  Removed previous changes regarding default columns.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for ProductAttributeSelectorWindow.xaml
    /// </summary>
    public partial class ProductAttributeSelectorWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ProductAttributeSelectorViewModel), typeof(ProductAttributeSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// This view's view model.
        /// </summary>
        public ProductAttributeSelectorViewModel ViewModel
        {
            get { return (ProductAttributeSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Connects a new view model to the window.
        /// </summary>
        /// <param name="newViewModel">View model to attach to the window.</param>
        private void AttachViewModel(ProductAttributeSelectorViewModel newViewModel)
        {
            if (newViewModel == null) return;
            newViewModel.DialogResultSet += OnViewModelDialogResultSet;
        }

        /// <summary>
        ///     Disconnects an old view model from the window.
        /// </summary>
        /// <param name="oldViewModel">View model to dettach from the window.</param>
        private void DettachViewModel(ProductAttributeSelectorViewModel oldViewModel)
        {
            if (oldViewModel == null) return;
            oldViewModel.DialogResultSet -= OnViewModelDialogResultSet;
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = (ProductAttributeSelectorWindow)obj;

            sender.DettachViewModel(e.OldValue as ProductAttributeSelectorViewModel);
            sender.AttachViewModel(e.NewValue as ProductAttributeSelectorViewModel);
        }

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new product attribute selector view.
        /// </summary>
        public ProductAttributeSelectorWindow(ProductAttributeSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        } 
        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xAvailableAttributesGrid_OnMouseDoubleClick(Object sender, MouseEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xAvailableAttributesGrid)) return;
            if (!ViewModel.AvailableAttributesSelection.Any()) return;

            ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the available attributes grid
        /// </summary>
        private void xAvailableAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the selected attributes grid.
        /// </summary>
        private void xSelectedAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xSelectedAttributesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xSelectedAttributesGrid)) return;

            if (!ViewModel.SelectedAttributes.Any()) return;

            ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null || !ViewModel.DialogResult.HasValue) return;
            this.DialogResult = ViewModel.DialogResult;
            this.Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel != null)
            {
                ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
                ViewModel.Dispose();
            }
        }
        #endregion
    }
}