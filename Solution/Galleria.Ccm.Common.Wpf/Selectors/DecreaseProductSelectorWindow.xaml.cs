﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31560 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Controls;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for DecreaseProductSelectorWindow.xaml
    /// </summary>
    public partial class DecreaseProductSelectorWindow
    {
        const String RemoveReplaceProductCommandKey = "RemoveDecreaseProductCommand";

        #region Fields

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(DecreaseProductSelectorViewModel), typeof(DecreaseProductSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            DecreaseProductSelectorWindow senderControl = (DecreaseProductSelectorWindow)obj;

            if (e.OldValue != null)
            {
                DecreaseProductSelectorViewModel oldModel = (DecreaseProductSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(RemoveReplaceProductCommandKey);
            }

            if (e.NewValue != null)
            {
                DecreaseProductSelectorViewModel newModel = (DecreaseProductSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(RemoveReplaceProductCommandKey, newModel.RemoveDecreaseProductCommand);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public DecreaseProductSelectorViewModel ViewModel
        {
            get { return (DecreaseProductSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public DecreaseProductSelectorWindow(DecreaseProductSelectorViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers


        #endregion

        #region Methods

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}