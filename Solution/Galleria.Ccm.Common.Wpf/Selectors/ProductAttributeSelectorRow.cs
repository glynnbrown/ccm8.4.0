﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 801
// V8-27494 : A.Kuszyk
//	Created.
#endregion
#region Version History: CCM 802
// V8-29030 : J.Pickup
//	Now handles the source type of performance.
#endregion
#region Version History: CCM 830
// V8-29030 : J.Pickup
//	Changes to allow row to represent a specific type for multiple types in a row (grid). (ON HOLD).
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Represents a row in the Product Attribute Selector window, encapsulating attribute properties.
    /// </summary>
    public class ProductAttributeSelectorRow : IEquatable<ProductAttributeSelectorRow>
    {

        #region Properties

        /// <summary>
        /// The row Header, representing the friendly name of an attribute,
        /// </summary>
        public String Header { get; private set; }

        /// <summary>
        /// The row Header Group, representing the group that this attribute belongs to.
        /// </summary>
        public String HeaderGroup { get; private set; }

        /// <summary>
        /// The actual property name that this attribute refers to.
        /// </summary>
        public String PropertyName { get; private set; }

        /// <summary>
        /// The Source type that this attribute belongs to.
        /// </summary>
        public ProductAttributeSourceType Source { get; private set; }

        /// <summary>
        /// The default value for this row.
        /// </summary>
        public Object DefaultValue { get; set; }

        /// <summary>
        /// The collection containing UOM details that should be applied to this row.
        /// </summary>
        public DisplayUnitOfMeasureCollection UOMCollectionForDefaultValue { get; set; }

        /// <summary>
        /// Whether or not to apply default values.
        /// </summary>
        public Boolean IsDefaultValueApplied { get; set; }

        #endregion

        #region Constants

        //Consts for type names
        const String cString = "String";

        const String cDatetime = "Nullable`1";

        const String cBoolean = "Boolean";

        const String cByte = "Byte";
        const String cSingle = "Single";
        const String cInt16 = "Int16";
        const String cInt32 = "Int32";
        const String cInt64 = "Int64";


        const String cPlanogramProductMerchandisingStyle = "PlanogramProductMerchandisingStyle";
        const String cFillColour = "FillColour";
        const String cFillPatternType = "FillPatternType";
        const String cShapeType = "ShapeType";
        const String cPlanogramProductOrientationType = "PlanogramProductOrientationType";
        const String cPlanogramProductStatusType = "PlanogramProductStatusType";
        const String cPlanogramProductShapeType = "PlanogramProductShapeType";
        const String cPlanogramProductFillPatternType = "PlanogramProductFillPatternType";

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new row from using the values in the given object field info.
        /// </summary>
        /// <param name="fieldInfo"></param>
        internal ProductAttributeSelectorRow(ObjectFieldInfo fieldInfo)
        {
            Header = fieldInfo.FieldFriendlyName;
            HeaderGroup = fieldInfo.GroupName;
            PropertyName = fieldInfo.PropertyName;

            // Determine the source time based on the Group Name of the field info.
            Source = ProductAttributeSourceTypeHelper.GetSourceType(fieldInfo);
        }

        #endregion

        #region Methods

        /// <summary>
        /// IEquatable implementation, for object comparisons.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        /// <remarks>Doesn't take into account Default Value and IsDefaultValue</remarks>
        public Boolean Equals(ProductAttributeSelectorRow other)
        {
            return
                other.Header.Equals(this.Header) &&
                other.HeaderGroup.Equals(this.HeaderGroup) &&
                other.PropertyName.Equals(this.PropertyName) &&
                other.Source == this.Source;
        }


        /// <summary>
        /// Summary creates a new row of the appropriate object type. 
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <returns></returns>
        /// <remarks>Object type is important for making decisons on data templating withing ui logic.</remarks>
        public static ProductAttributeSelectorRow CreateNewRow(ObjectFieldInfo fieldInfo)
        {
            String propertyFullName = fieldInfo.PropertyType.FullName;

            //Colour is a special case as it is an int but we need to handle with colour picker.
            if (fieldInfo.PropertyName.ToLowerInvariant().Contains("colour") || fieldInfo.PropertyName.ToLowerInvariant().Contains("color"))
            {
                return new ProductAttributeSelectorRowColour(fieldInfo);
            }

            //Handle based on the properties fully qualified name (should handle nullable types too). 
            if (propertyFullName.Contains(cString)) return new ProductAttributeSelectorRowString(fieldInfo);
            if (propertyFullName.Contains(cDatetime)) return new ProductAttributeSelectorRowDate(fieldInfo);
            if (propertyFullName.Contains(cBoolean)) return new ProductAttributeSelectorRowCheckBox(fieldInfo);
            if (propertyFullName.Contains(cSingle)) return new ProductAttributeSelectorRowDecimal(fieldInfo);
            if (propertyFullName.Contains(cInt16)) return new ProductAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cInt32)) return new ProductAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cInt64)) return new ProductAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cByte)) return new ProductAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cFillColour)) return new ProductAttributeSelectorRowColour(fieldInfo);
            if (propertyFullName.Contains(cPlanogramProductMerchandisingStyle)) return new ProductAttributeSelectorRowMerchStyle(fieldInfo);
            if (propertyFullName.Contains(cPlanogramProductStatusType)) return new ProductAttributeSelectorRowProductStatusType(fieldInfo);
            if (propertyFullName.Contains(cPlanogramProductOrientationType)) return new ProductAttributeSelectorRowProductOrientationType(fieldInfo);
            if (propertyFullName.Contains(cFillPatternType)) return new ProductAttributeSelectorRowFillPatternType(fieldInfo);
            if (propertyFullName.Contains(cPlanogramProductFillPatternType)) return new ProductAttributeSelectorRowFillPatternType(fieldInfo);
            if (propertyFullName.Contains(cShapeType)) return new ProductAttributeSelectorRowShapeType(fieldInfo);
            if (propertyFullName.Contains(cPlanogramProductShapeType)) return new ProductAttributeSelectorRowShapeType(fieldInfo);

            return new ProductAttributeSelectorRow(fieldInfo);
        }
    }
    #endregion

    #region Subclasses

    //These subclasses are important for their type, and help the xaml select approprtiate data templates.

    public class ProductAttributeSelectorRowString : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowString(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = String.Empty;
        }
        
    }

    public class ProductAttributeSelectorRowDate : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowDate(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = DateTime.Now;
        }
    }

    public class ProductAttributeSelectorRowCheckBox : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowCheckBox(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = false;
        }
    }

    public class ProductAttributeSelectorRowInteger : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowInteger(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class ProductAttributeSelectorRowDecimal : ProductAttributeSelectorRow
    {
        #region Properties
       
        #endregion

        #region Constructor

        internal ProductAttributeSelectorRowDecimal(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
        }

        #endregion
    }

    public class ProductAttributeSelectorRowColour : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowColour(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class ProductAttributeSelectorRowMerchStyle : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowMerchStyle(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class ProductAttributeSelectorRowShapeType : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowShapeType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class ProductAttributeSelectorRowProductStatusType : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowProductStatusType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class ProductAttributeSelectorRowProductShapeType : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowProductShapeType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }
    
    public class ProductAttributeSelectorRowFillPatternType : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowFillPatternType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
 
        #region ProductFillPatternBrushes

        public static Dictionary<PlanogramProductFillPatternType, Brush> ProductFillPatternBrushes
        {
            get
            {
                return new Dictionary<PlanogramProductFillPatternType, Brush>()
                    {
                        {PlanogramProductFillPatternType.Solid,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Solid, 5)},

                         {PlanogramProductFillPatternType.Border,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Border, 5)},

                         {PlanogramProductFillPatternType.Crosshatch,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Crosshatch, 5)},

                          {PlanogramProductFillPatternType.DiagonalDown,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalDown, 5)},

                            {PlanogramProductFillPatternType.DiagonalUp,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalUp, 5)},

                            {PlanogramProductFillPatternType.Dotted,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Dotted, 5)},

                            {PlanogramProductFillPatternType.Horizontal,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Horizontal, 5)},

                            {PlanogramProductFillPatternType.Vertical,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Vertical, 5)},
                    };
            }
        }

        #endregion
    }
    
    public class ProductAttributeSelectorRowProductOrientationType : ProductAttributeSelectorRow
    {
        internal ProductAttributeSelectorRowProductOrientationType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    #endregion
}



