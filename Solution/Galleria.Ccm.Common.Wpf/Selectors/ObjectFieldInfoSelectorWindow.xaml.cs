﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27196 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    ///     Interaction logic for ObjectFieldInfoSelectorWindow.xaml
    /// </summary>
    public partial class ObjectFieldInfoSelectorWindow
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (ObjectFieldInfoSelectorViewModel), typeof (ObjectFieldInfoSelectorWindow),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as ObjectFieldInfoSelectorWindow;
            if (senderControl == null) return;

            var oldModel = e.OldValue as ObjectFieldInfoSelectorViewModel;
            if (oldModel != null) oldModel.AttachedControl = null;

            var newModel = e.NewValue as ObjectFieldInfoSelectorViewModel;
            if (newModel != null) newModel.AttachedControl = senderControl;
        }

        public ObjectFieldInfoSelectorViewModel ViewModel
        {
            get { return (ObjectFieldInfoSelectorViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="ObjectFieldInfoSelectorWindow"/> using the provided <paramref name="viewModel"/>.
        /// </summary>
        /// <param name="viewModel">Instance of <see cref="ObjectFieldInfoSelectorViewModel"/> to control this view.</param>
        public ObjectFieldInfoSelectorWindow(ObjectFieldInfoSelectorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            ViewModel = viewModel;
            Loaded += ObjectFieldInfoSelectorWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Whenever the window has finished loading.
        /// </summary>
        private void ObjectFieldInfoSelectorWindow_Loaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= ObjectFieldInfoSelectorWindow_Loaded;
            Dispatcher.BeginInvoke((Action)(() => Mouse.OverrideCursor = null));
        }

        /// <summary>
        ///     Invoked whenever a row item is double clicked on the Selection Grid.
        /// </summary>
        private void xSelectionGrid_RowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            var fieldInfo = e.RowItem as ObjectFieldInfo;
            if (fieldInfo == null) return;

            ViewModel.OkCommand.Execute();
        }

        #endregion
    }
}