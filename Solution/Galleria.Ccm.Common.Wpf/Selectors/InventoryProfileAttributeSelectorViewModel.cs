﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.20)
// GFS-30836 : J.Pickup
//  Created.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended constructor to allow injection of inventory profiles.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using System.Collections.Generic;


namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public class InventoryProfileAttributeSelectorViewModel : ViewModelAttachedControlObject<InventoryProfileAttributeSelectorWindow>
    {
        #region Fields

        Boolean _dialogResult = false;
        private ObservableCollection<InventoryProfileInfo> _availableInventoryProfiles = new ObservableCollection<InventoryProfileInfo>();
        private InventoryProfileInfo _selectedInventoryProfile;
  
        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath AvailableInventoryProfilesProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.AvailableInventoryProfiles);
        public static readonly PropertyPath SelectedInventoryProfileProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.SelectedInventoryProfile);
        public static readonly PropertyPath AvailableInventoryProfilesCountDescriptionProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.AvailableInventoryProfilesCountDescription);
        public static readonly PropertyPath DialogResultProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.DialogResult);
        
        //Command Properties
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileAttributeSelectorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The result for the ok pressed.
        /// </summary>
        public Boolean DialogResult
        {
            get { return _dialogResult; }
            set 
            { 
                _dialogResult = value;
                OnPropertyChanged(DialogResultProperty);
            }
        }

        /// <summary>
        /// Gets/sets the list of locations
        /// </summary>
        public ObservableCollection<InventoryProfileInfo> AvailableInventoryProfiles
        {
            get 
            {
                return _availableInventoryProfiles; 
            }
        }

        /// <summary>
        /// Gets/sets the selected inventory profile
        /// </summary>
        public InventoryProfileInfo SelectedInventoryProfile
        {
            get
            {
                return _selectedInventoryProfile;
            }
            set
            {
                _selectedInventoryProfile = value;
            }
        }

        #endregion

        #region Helper Properties

        /// <summary>
        /// Gets/sets the list of inventory profiles
        /// </summary>
        public String AvailableInventoryProfilesCountDescription
        {
            get
            {
                return String.Format( Message.InventoryProfileSelector_Count, _availableInventoryProfiles.Count());
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InventoryProfileAttributeSelectorViewModel(IEnumerable<InventoryProfileInfo> inventoryProfileInfos)
        {
            _availableInventoryProfiles = new ObservableCollection<InventoryProfileInfo>(inventoryProfileInfos);
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(p => OKCommand_Executed(), p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            if (SelectedInventoryProfile == null)
            {
                OKCommand.DisabledReason = Message.InventoryProfileSelection_PleaseSelect;
                return false;
            }

            return true;
        }

        public void OKCommand_Executed()
        {
            //Close window
            _dialogResult = true;
            OnPropertyChanged(DialogResultProperty);
        }
        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            _dialogResult = false;
            OnPropertyChanged(DialogResultProperty);
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Do nothing...
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
