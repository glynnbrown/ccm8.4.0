﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 803
// V8-29643 : D.Pleasance
//	Created.
#endregion
#region Version History: CCM820
// V8-31128 : D.Pleasance
//  Added Info settings
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Controls.Primitives;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramAttributeSelectorWindow.xaml
    /// </summary>
    public partial class PlanogramAttributeSelectorWindow
    {
        #region Nested Types

        /// <summary>
        /// Event Args for dragging and dropping of attribute rows.
        /// </summary>
        private class PlanogramAttributeSelectorDropEventArgs
        {
            public PlanogramAttributeSelectorDropEventArgs() { }
        }

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanogramAttributeSelectorViewModel), typeof(PlanogramAttributeSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// This view's view model.
        /// </summary>
        public PlanogramAttributeSelectorViewModel ViewModel
        {
            get { return (PlanogramAttributeSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Connects a new view model to the window.
        /// </summary>
        /// <param name="newViewModel">View model to attach to the window.</param>
        private void AttachViewModel(PlanogramAttributeSelectorViewModel newViewModel)
        {
            if (newViewModel == null) return;
            newViewModel.DialogResultSet += OnViewModelDialogResultSet;
        }

        /// <summary>
        ///     Disconnects an old view model from the window.
        /// </summary>
        /// <param name="oldViewModel">View model to dettach from the window.</param>
        private void DettachViewModel(PlanogramAttributeSelectorViewModel oldViewModel)
        {
            if (oldViewModel == null) return;
            oldViewModel.DialogResultSet -= OnViewModelDialogResultSet;
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = (PlanogramAttributeSelectorWindow)obj;

            sender.DettachViewModel(e.OldValue as PlanogramAttributeSelectorViewModel);
            sender.AttachViewModel(e.NewValue as PlanogramAttributeSelectorViewModel);
        }

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new planogram attribute selector view.
        /// </summary>
        public PlanogramAttributeSelectorWindow(PlanogramAttributeSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        } 
        #endregion

        #region Event Handlers

        private void xAvailableAttributesGrid_OnMouseDoubleClick(Object sender, MouseEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xAvailableAttributesGrid)) return;
            if (!ViewModel.AvailableAttributesSelection.Any()) return;

            ViewModel.AddSelectedAttributesCommand.Execute();
        }
        
        /// <summary>
        /// Called whenever a row is dropped on the available attributes grid
        /// </summary>
        private void xAvailableAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the selected attributes grid.
        /// </summary>
        private void xSelectedAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null || !ViewModel.DialogResult.HasValue) return;
            this.DialogResult = ViewModel.DialogResult;
            this.Close();
        }


        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel != null)
            {
                ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
                ViewModel.Dispose();
            }
        }
        #endregion
    }

    public class PlanogramAttributeCellTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextBoxTemplate { get; set; }
        public DataTemplate TextBoxNumericTemplate { get; set; }
        public DataTemplate CheckBoxTemplate { get; set; }
        public DataTemplate ComboBoxTemplate { get; set; }
        public DataTemplate DateTemplate { get; set; }
        
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            PlanogramAttributeSelectorRow view = item as PlanogramAttributeSelectorRow;
            if (view != null)
            {
                if (view.PropertyType == typeof(String))
                {
                    return TextBoxTemplate;
                }
                if (view.PropertyType == typeof(Boolean))
                {
                    return CheckBoxTemplate;
                }
                if (view.PropertyType == typeof(Single))
                {
                    return TextBoxNumericTemplate;
                }
                if (view.PropertyType == typeof(DateTime) || view.PropertyType == typeof(DateTime?))
                {
                    return DateTemplate;
                }
                if (view.PropertyType == typeof(PlanogramLengthUnitOfMeasureType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramAreaUnitOfMeasureType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramVolumeUnitOfMeasureType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramWeightUnitOfMeasureType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramCurrencyUnitOfMeasureType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramProductPlacementXType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramProductPlacementYType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramProductPlacementZType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramType))
                {
                    return ComboBoxTemplate;
                }
                if (view.PropertyType == typeof(PlanogramStatusType))
                {
                    return ComboBoxTemplate;
                }
            }

            return TextBoxTemplate;
        }
    }
}