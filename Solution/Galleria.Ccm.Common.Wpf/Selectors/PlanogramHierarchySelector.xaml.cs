﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25724 : A.Silva 
//  Created.
// V8-25871 : A.Kuszyk 
//  Amended for drag and drop. Removed temporary classes.
// V8-26284 : A.Kuszyk
//  Moved to Galleria.Ccm.Common.Wpf.
// V8-26822 : L.Ineson ~ Changed how my categories groups are displayed.
// V8-26534 : A.Kuszyk
//  Ensured that move groups works correctly and the expansion states are preserved.
// V8-27964 : A.Silva
//      Added StartingPlanogramGroup depency property so that it can be set in xaml, then removed.
//      Amended to pass the StartingPlanogramGroup to the viewmodel, then removed.
// V8-27280 : A.Silva
//      Amended how the selector remembers which nodes are open when CreateTreeNode is called. 
//          Now each node should be identified uniquely avoiding several nodes expanding when one was expanded 
//          and they all shared the same underlying Planogram Group View Model.
//      Amended so that RecentWork groups are not added with their children.
// V8-28317 : A.Silva
//      Amended XTreeView_Drop to correctly move the planogram rows onto planogram groups.
// V8-28236 : A.Silva
//      Amended PlanogramHierarchySelector_Loaded so that the previous SelectedPlanogramGroup is kept after creating the viewmodel.
// V8-28209 : A.Silva
//      Amended CreateTreeNode so that no children nodes are created if the planogram group view has no ChildViews.
// V8-28423 : A.Kuszyk
//  Added overwrite check to XTreeView_Drop.
// V8-28587 : A.Kuszyk
//  Added owner parameter to PlanogramHierarchySelectorViewModel.CheckForOverwrite.
#endregion

#region Version History: (CCM v8.1.0)
// V8-29566 : M.Pettit
//  Added code so that if the heirarchy is on its first tree node gneration the All Categories folder s expanded, if not, the folder expansions are left as the user intended.
#endregion

#region Version History: CCM 820

// V8-30620 : A.Silva
//  Added ScrollIntoView as a helper method to bring a node into view from any PlanogramGroup instance.
// V8-30047 : A.Probyn
//  Updated so favourites tab is now a tree view.
#endregion

#region Version History: CCM830

// V8-32381 : A.Silva
//  Amended tree view context menu creation to ensure that common options are available both in the main and in the favorites tree views.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramHierarchySelector.xaml
    /// </summary>
    public sealed partial class PlanogramHierarchySelector : IDisposable
    {
        #region Fields
        private Boolean _isFirstRefresh = true;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramHierarchySelectorViewModel), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;

            if (e.OldValue != null)
            {
                PlanogramHierarchySelectorViewModel oldModel = (PlanogramHierarchySelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.GroupRefreshRequired -= senderControl.ViewModel_GroupRefreshRequired;

                //V8-27966 copying planograms, disabled for now.
                //oldModel.PlanogramCopied -= senderControl.ViewModel_PlanogramCopied;


                senderControl.AddToFavouritesCommand = null;
                senderControl.RemoveFromFavouritesCommand = null;
            }

            if (e.NewValue != null)
            {
                PlanogramHierarchySelectorViewModel newModel = (PlanogramHierarchySelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                //update values
                newModel.ShowRecentWorkGroup = senderControl.IsRecentWorkGroupVisible;
                newModel.ShowFavouriteGroups = senderControl.IsMyCategoriesGroupVisible;
                newModel.SelectedPlanogramGroup = senderControl.SelectedPlanogramGroup;


                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.GroupRefreshRequired += senderControl.ViewModel_GroupRefreshRequired;

                // V8-27966 copying planograms, disabled for now.
                //newModel.PlanogramCopied += senderControl.ViewModel_PlanogramCopied;

                senderControl.AddToFavouritesCommand = newModel.AddGroupToFavouritesCommand;
                senderControl.RemoveFromFavouritesCommand = newModel.RemoveGroupFromFavouritesCommand;
            }

            senderControl.GenerateTreeNodes();
            senderControl.GenerateFavouriteTreeNodes();
        }

        /// <summary>
        /// Viewmodel controller
        /// </summary>
        public PlanogramHierarchySelectorViewModel ViewModel
        {
            get { return (PlanogramHierarchySelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region PlanogramHierarchyView Property

        public static readonly DependencyProperty PlanogramHierarchyViewProperty =
            DependencyProperty.Register("PlanogramHierarchyView", typeof(PlanogramHierarchyViewModel), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null, OnPlanogramHierarchyViewPropertyChanged));

        private static void OnPlanogramHierarchyViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;
            senderControl.SetViewModel();
        }

        /// <summary>
        /// Viewmodel of the master planogram hierarchy
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return (PlanogramHierarchyViewModel)GetValue(PlanogramHierarchyViewProperty); }
            set { SetValue(PlanogramHierarchyViewProperty, value); }
        }

        #endregion

        #region UserGroupsView Property

        public static readonly DependencyProperty UserGroupsViewProperty =
            DependencyProperty.Register("UserGroupsView", typeof(UserPlanogramGroupListViewModel), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null, OnUserGroupsViewPropertyChanged));

        private static void OnUserGroupsViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;
            senderControl.SetViewModel();
        }

        /// <summary>
        /// Gets/Sets the master user groups viewmodel to use.
        /// </summary>
        public UserPlanogramGroupListViewModel UserGroupsView
        {
            get { return (UserPlanogramGroupListViewModel)GetValue(UserGroupsViewProperty); }
            set { SetValue(UserGroupsViewProperty, value); }
        }

        #endregion

        #region SelectedGroup Property

        public static readonly DependencyProperty SelectedGroupProperty =
            DependencyProperty.Register("SelectedGroup", typeof(PlanogramGroupViewModel), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null, OnSelectedGroupPropertyChanged));

        private static void OnSelectedGroupPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;

            PlanogramGroupViewModel newValue = e.NewValue as PlanogramGroupViewModel;
            if (senderControl.ViewModel != null &&
                !Equals(senderControl.ViewModel.SelectedGroup, newValue))
            {
                senderControl.ViewModel.SelectedGroup = newValue;
            }

        }

        /// <summary>
        /// Gets/Sets the selected group.
        /// </summary>
        public PlanogramGroupViewModel SelectedGroup
        {
            get { return (PlanogramGroupViewModel)GetValue(SelectedGroupProperty); }
            set { SetValue(SelectedGroupProperty, value); }
        }

        #endregion

        #region SelectedPlanogramGroup Property

        public static readonly DependencyProperty SelectedPlanogramGroupProperty =
            DependencyProperty.Register("SelectedPlanogramGroup", typeof(PlanogramGroup), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null, OnSelectedPlanGroupPropertyChanged));

        private static void OnSelectedPlanGroupPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;

            PlanogramGroup newValue = e.NewValue as PlanogramGroup;
            if (senderControl.ViewModel != null &&
                !Equals(senderControl.ViewModel.SelectedPlanogramGroup, newValue))
            {
                senderControl.ViewModel.SelectedPlanogramGroup = newValue;
            }
        }

        /// <summary>
        /// Gets/Sets the selected group.
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return (PlanogramGroup)GetValue(SelectedPlanogramGroupProperty); }
            set { SetValue(SelectedPlanogramGroupProperty, value); }
        }

        #endregion

        #region IsRecentWorkGroupVisible Property

        public static readonly DependencyProperty IsRecentWorkGroupVisibleProperty =
            DependencyProperty.Register("IsRecentWorkGroupVisible", typeof(Boolean), typeof(PlanogramHierarchySelector),
            new UIPropertyMetadata(true, OnIsRecentWorkGroupVisiblePropertyChanged));

        private static void OnIsRecentWorkGroupVisiblePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;

            if (senderControl.ViewModel != null)
            {
                senderControl.ViewModel.ShowRecentWorkGroup = (Boolean)e.NewValue;
            }
        }

        /// <summary>
        /// Gets/Sets whether the recent work group should be shown.
        /// </summary>
        public Boolean IsRecentWorkGroupVisible
        {
            get { return (Boolean)GetValue(IsRecentWorkGroupVisibleProperty); }
            set { SetValue(IsRecentWorkGroupVisibleProperty, value); }
        }

        #endregion

        #region IsMyCategoriesGroupVisible Property

        public static readonly DependencyProperty IsMyCategoriesGroupVisibleProperty =
            DependencyProperty.Register("IsMyCategoriesGroupVisible", typeof(Boolean), typeof(PlanogramHierarchySelector),
            new UIPropertyMetadata(true, OnIsMyCategoriesGroupVisiblePropertyChanged));

        private static void OnIsMyCategoriesGroupVisiblePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchySelector senderControl = (PlanogramHierarchySelector)obj;

            if (senderControl.ViewModel != null)
            {
                senderControl.ViewModel.ShowFavouriteGroups = (Boolean)e.NewValue;
            }
        }

        /// <summary>
        /// Gets/Sets whether the My Categories group should be shown.
        /// </summary>
        public Boolean IsMyCategoriesGroupVisible
        {
            get { return (Boolean)GetValue(IsMyCategoriesGroupVisibleProperty); }
            set { SetValue(IsMyCategoriesGroupVisibleProperty, value); }
        }

        #endregion

        #region AllowPlanogramGroupDrop Property

        public static readonly DependencyProperty AllowPlanogramGroupDropProperty =
            DependencyProperty.Register("AllowPlanogramGroupDrop", typeof(Boolean), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(false));

        /// <summary>
        /// Gets/Sets whether planogram groups may be dragged and dropped in this selector.
        /// </summary>
        public Boolean AllowPlanogramGroupDrop
        {
            get { return (Boolean)GetValue(AllowPlanogramGroupDropProperty); }
            set { SetValue(AllowPlanogramGroupDropProperty, value); }
        }

        #endregion

        #region AddToFavouriesCommandProperty

        public static readonly DependencyProperty AddToFavouriesCommandProperty =
            DependencyProperty.Register("AddToFavouritesCommand", typeof(RelayCommand), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null));

        public RelayCommand AddToFavouritesCommand
        {
            get { return (RelayCommand)GetValue(AddToFavouriesCommandProperty); }
            private set { SetValue(AddToFavouriesCommandProperty, value); }
        }

        #endregion

        #region RemoveFromFavouriesCommandProperty

        public static readonly DependencyProperty RemoveFromFavouriesCommandProperty =
            DependencyProperty.Register("RemoveFromFavouritesCommand", typeof(RelayCommand), typeof(PlanogramHierarchySelector),
            new PropertyMetadata(null));

        public RelayCommand RemoveFromFavouritesCommand
        {
            get { return (RelayCommand)GetValue(RemoveFromFavouriesCommandProperty); }
            private set { SetValue(RemoveFromFavouriesCommandProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler<PlanogramHierarchyContextEventArgs> NodeContextMenuShowing;

        private Boolean OnNodeContextMenuShowing(Fluent.ContextMenu cm)
        {
            if (NodeContextMenuShowing == null) return true;

            PlanogramHierarchyContextEventArgs args = new PlanogramHierarchyContextEventArgs { ContextMenu = cm };
            NodeContextMenuShowing(this, args);
            return !args.Cancel;
        }

        #region V8-27966 copying planograms, disabled for now.

        //public event EventHandler<PlanogramCopiedEventHandlerArgs> PlanogramCopied;

        //private void OnPlanogramCopied(PlanogramCopiedEventHandlerArgs args)
        //{
        //    if (PlanogramCopied != null)
        //    {
        //        PlanogramCopied(this, args);
        //    }
        //}

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramHierarchySelector()
        {
            InitializeComponent();

            //nb - viewmodel created in loaded event handler.
            Loaded += PlanogramHierarchySelector_Loaded;
        }

        #endregion

        #region Event Handlers

        #region Local Event Handlers

        /// <summary>
        /// Carries out actions when the selector is loaded.
        /// </summary>
        private void PlanogramHierarchySelector_Loaded(Object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramHierarchySelector_Loaded;

            //PlanogramGroup currentlySelected = SelectedPlanogramGroup;

            //load a hierarchy view if none if bound in.
            if (this.PlanogramHierarchyView == null)
            {
                var binding = BindingOperations.GetBinding(this, PlanogramHierarchyViewProperty);
                if (binding == null) this.PlanogramHierarchyView = new PlanogramHierarchyViewModel();
            }

            //load the user groups view if none is bound in.
            if (this.UserGroupsView == null)
            {
                var binding = BindingOperations.GetBinding(this, UserGroupsViewProperty);
                if (binding == null) this.UserGroupsView = new UserPlanogramGroupListViewModel();
            }

            //add handlers.
            this.xTreeView.AddHandler(Treeview.DropEvent, (DragEventHandler)XTreeView_Drop);
            this.xTreeView.AddHandler(TreeNode.MouseRightButtonDownEvent, (MouseButtonEventHandler)xTreeView_MouseRightButtonDown);
            this.xFavouriteTreeView.AddHandler(TreeNode.MouseRightButtonDownEvent, (MouseButtonEventHandler)xFavouriteTreeView_MouseRightButtonDown);

            //if (AllowPlanogramGroupDrop)
            //{
            //    //TODO: Add back in when group moving is supported.
            //    //xTreeView.AddHandler(TreeNode.PreviewMouseLeftButtonDownEvent, (MouseButtonEventHandler)PlanogramGroupDrop_MouseLeftButtonDown);
            //    //xTreeView.AddHandler(TreeNode.PreviewMouseMoveEvent, (MouseEventHandler)PlanogramGroupDrop_PreviewMouseMove);
            //    //xTreeView.AddHandler(TreeNode.DragOverEvent, (DragEventHandler)PlanogramGroupDrop_DragOver);
            //    //xTreeView.AddHandler(Treeview.DropEvent, (DragEventHandler)PlanogramGroupDrop_Drop);
            //    //xTreeView.AddHandler(TreeNode.DragLeaveEvent, (DragEventHandler)PlanogramGroupDrop_DragLeave);
            //}

        }

        #endregion

        #region Viewmodel handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramHierarchySelectorViewModel.AvailableGroupsProperty.Path)
            {
                GenerateTreeNodes();
            }
            else if (e.PropertyName == PlanogramHierarchySelectorViewModel.FavouriteGroupsProperty.Path)
            {
                GenerateFavouriteTreeNodes();
            }
            else if (e.PropertyName == PlanogramHierarchySelectorViewModel.SelectedGroupProperty.Path)
            {
                this.SelectedGroup = this.ViewModel.SelectedGroup;
            }
            else if (e.PropertyName == PlanogramHierarchySelectorViewModel.SelectedPlanogramGroupProperty.Path)
            {
                this.SelectedPlanogramGroup = this.ViewModel.SelectedPlanogramGroup;
            }
        }

        private void ViewModel_GroupRefreshRequired(Object sender, EventArgs e)
        {
            GenerateTreeNodes();
            GenerateFavouriteTreeNodes();
        }

        //V8-27966 copying planograms, disabled for now.
        //private void ViewModel_PlanogramCopied(object sender, PlanogramCopiedEventHandlerArgs args)
        //{
        //    OnPlanogramCopied(args);
        //}


        #endregion

        #region PlanogramGroup Drop

        //Have commented these out for now as they all relate to dragging to my cateogries groups
        // however the groups are now displayed on a separate tab.

        ///// <summary>
        ///// Called when the drag cursor leaves a tree view item on the side panel.
        ///// </summary>
        ///// <remarks>
        ///// Removes any override cursor that may have been applied to the cursor in the
        ///// previous drag over event.
        ///// </remarks>
        //private void PlanogramGroupDrop_DragLeave(Object sender, DragEventArgs e)
        //{
        //    Mouse.OverrideCursor = null;
        //}

        ///// <summary>
        ///// Called when the drag cursor moves over a tree view item in the side panel.
        ///// </summary>
        ///// <remarks>
        ///// Checks the item being dragged and the target over which it has been dragged and 
        ///// overrides the mouse cursor to No if an invalid combination.
        ///// </remarks>
        //private void PlanogramGroupDrop_DragOver(Object sender, DragEventArgs e)
        //{
        //    // Show the no cursor if dragging a My Categories or Recent Work item.
        //    if (!DragDropBehaviour.IsUnpackedType<PlanogramGroupDropEventArgs>(e.Data)) return;
        //    var draggedGroup = DragDropBehaviour.UnpackData<PlanogramGroupDropEventArgs>(e.Data).PlanogramGroupView;
        //    if (draggedGroup.IsMyCategoriesGroup || draggedGroup.IsRecentWorkGroup)
        //    {
        //        Mouse.OverrideCursor = Cursors.No;
        //        return;
        //    }

        //    // Show the No cursor if dragging over anything other than a My Categories group.
        //    var targetItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
        //    if (targetItem == null || !(targetItem.Item is PlanogramGroupViewModel)) return;
        //    var targetGroup = (PlanogramGroupViewModel)targetItem.Item;
        //    if (!targetGroup.IsMyCategoriesGroup)
        //    {
        //        Mouse.OverrideCursor = Cursors.No;
        //        return;
        //    }

        //    Mouse.OverrideCursor = null;
        //}

        ///// <summary>
        ///// Called when an item is dropped on a tree view item in the side panel.
        ///// </summary>
        //private void PlanogramGroupDrop_Drop(Object sender, DragEventArgs e)
        //{
        //    Mouse.OverrideCursor = null;

        //    // Check that target item is a PlanogramGroupSelectorView and that it is a My
        //    // Categories group.
        //    var targetItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
        //    if (targetItem == null || !(targetItem.Item is PlanogramGroupViewModel)) return;
        //    var targetGroup = (PlanogramGroupViewModel)targetItem.Item;
        //    if (!targetGroup.IsMyCategoriesGroup) return;

        //    // PlanogramGroup
        //    if (DragDropBehaviour.IsUnpackedType<PlanogramGroupDropEventArgs>(e.Data))
        //    {
        //        var draggedGroup = DragDropBehaviour.UnpackData<PlanogramGroupDropEventArgs>(e.Data).PlanogramGroupView;
        //        if (draggedGroup.IsRecentWorkGroup || draggedGroup.IsMyCategoriesGroup) return;
        //        e.Handled = ViewModel.AddPlanogramGroupToFavourites(draggedGroup);
        //    }
        //}

        ///// <summary>
        ///// Called when the left mouse button clicks on a Side Panel Tree View Item.
        ///// </summary>
        //private void PlanogramGroupDrop_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    _lastMousePosition = e.GetPosition(this);
        //}

        ///// <summary>
        ///// Called when the mouse starts to move over a Side Panel Tree View Item.
        ///// </summary>
        //private void PlanogramGroupDrop_PreviewMouseMove(object sender, MouseEventArgs e)
        //{
        //    if (e.LeftButton == MouseButtonState.Pressed && MouseIsMoving(e.GetPosition(this)))
        //    {
        //        // Ensure that a TreeNode has been clicked.
        //        var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
        //        if (item == null) return;

        //        // Proceed with drag and drop, if some Planograms have been selected.
        //        var selectedGroup = xTreeView.SelectedItem as PlanogramGroupViewModel;
        //        if (selectedGroup == null) return;
        //        var dragData = new PlanogramGroupDropEventArgs(selectedGroup);
        //        var dragDrop = new DragDropBehaviour(dragData);

        //        // Define drag scope of drag-drop operation.
        //        FrameworkElement dragScope = xTreeView as FrameworkElement;

        //        // Perform drag.
        //        dragDrop.DragArea = dragScope;
        //        if (selectedGroup.IsMyCategoriesGroup || selectedGroup.IsRecentWorkGroup)
        //        {
        //            Mouse.OverrideCursor = Cursors.No;
        //        }
        //        else
        //        {
        //            dragDrop.SetAdornerFromImageSource(Galleria.Ccm.Common.Wpf.Resources.ImageResources.TreeViewItem_ClosedFolder);
        //        }
        //        dragDrop.BeginDrag();
        //    }
        //}

        #endregion

        #region Treeview handlers

        /// <summary>
        /// Handles the event when an item is dropped on the Tree View.
        /// </summary>
        private void XTreeView_Drop(Object sender, DragEventArgs e)
        {
            //  Obtain the target Planogram Group. Return if none.
            PlanogramGroupViewModel targetGroup = GetTreeNodeItem<PlanogramGroupViewModel>(e.OriginalSource as DependencyObject);
            if (targetGroup == null) return;

            PlanogramGroupViewModel previouslySelectedGroup = ViewModel.SelectedGroup;

            //  Obtain the source Planogram Row(s). Return if none.
            if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                ExtendedDataGridRowDropEventArgs args = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (args.Items == null) return;

                List<PlanogramRepositoryRow> draggedPlans = args.Items.OfType<PlanogramRepositoryRow>().ToList();
                if (!draggedPlans.Any()) return;

                this.ViewModel.MovePlanogramsToGroup(draggedPlans.Select(o => o.Info), targetGroup);

                e.Handled = true;

                //// Check the plans to move for duplicates.
                //List<PlanogramInfo> plansToMove = new List<PlanogramInfo>();
                //Boolean dialogDisplayed = false;
                //Boolean hideFutureWarnings = false;
                //foreach (PlanogramInfo planInfo in draggedPlans.Select(p=>p.Info))
                //{
                //    Window owner = Window.GetWindow(this);
                //    if (owner == null)
                //    {
                //        Debug.Fail("Parent could not be found and cast to a Window");
                //        continue;
                //    }
                //    Boolean? checkForOverwrite = PlanogramHierarchySelectorViewModel.CheckForOverwrite(
                //        planInfo.Name, targetGroup.Id, owner, ref hideFutureWarnings);
                //    if (checkForOverwrite.HasValue)
                //    {
                //        if (checkForOverwrite.Value)
                //        {
                //            plansToMove.Add(planInfo);
                //            dialogDisplayed = true;
                //        }
                //    }
                //    else
                //    {
                //        plansToMove.Add(planInfo);
                //        dialogDisplayed = dialogDisplayed || false;
                //    }
                //}
                //if (plansToMove.Count == 0) return;

                //// Now, actually move the plans.
                //e.Handled = ViewModel.MovePlanogramsToGroup(
                //    draggedPlans.Select(o => o.Info.Id), targetGroup, supressWarning: dialogDisplayed);
            }

            // Ensure display is updated.
            ViewModel.SelectedGroup = null;
            ViewModel.SelectedGroup = previouslySelectedGroup;
        }

        /// <summary>
        /// Called whenever a tree view item is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xTreeView_MouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel == null) { return; }

            TreeNode item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
            if (item != null)
            {
                //if we double click on a search result then clear the search and select the group in situ.
                if (!String.IsNullOrEmpty(this.ViewModel.GroupSearchFilter))
                {
                    PlanogramGroup selectedGroup = this.SelectedPlanogramGroup;

                    this.ViewModel.GroupSearchFilter = null;

                    //reselect the group
                    this.SelectedPlanogramGroup = selectedGroup;
                }
            }
        }

        /// <summary>
        /// Called with the user right-clicks on a Tree View Item in the side panel's selector.
        /// </summary>
        private void xTreeView_MouseRightButtonDown(Object sender, MouseButtonEventArgs e)
        {
            InitializeContextMenu(((DependencyObject) e.OriginalSource).FindVisualAncestor<TreeNode>(), isMainTreeView: true);
        }

        /// <summary>
        /// Called whenever an item is right clicked in the favourites list box.
        /// </summary>
        private void xFavouriteTreeView_MouseRightButtonDown(Object sender, MouseButtonEventArgs e)
        {
            InitializeContextMenu(((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>(), isMainTreeView: false);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Sets the viewmodel property value.
        /// </summary>
        private void SetViewModel()
        {
            if (this.PlanogramHierarchyView == null || this.UserGroupsView == null)
            {
                if (this.ViewModel != null)
                {
                    IDisposable disView = this.ViewModel;
                    this.ViewModel = null;
                    disView.Dispose();
                }
            }
            else
            {
                //create the viewmodel.
                this.ViewModel = new PlanogramHierarchySelectorViewModel(this.PlanogramHierarchyView, this.UserGroupsView);

                //this.ViewModel.ResetSelectedItems();
                //this.ViewModel.SelectedPlanogramGroup = this.SelectedPlanogramGroup;
            }
        }

        /// <summary>
        /// Returns a list of all nodes in the tree
        /// </summary>
        /// <param name="treeNode"></param>
        /// <returns></returns>
        private IEnumerable<TreeNode> GetAllChildNodes(TreeNode treeNode)
        {
            var returnList = new List<TreeNode>();
            if (!treeNode.ChildNodes.Any()) return returnList;
            returnList.AddRange(treeNode.ChildNodes);
            foreach (var childNode in treeNode.ChildNodes)
            {
                returnList.AddRange(GetAllChildNodes(childNode));
            }
            return returnList;
        }

        /// <summary>
        /// Adds treenodes to the treeview.
        /// </summary>
        private void GenerateTreeNodes()
        {
            if (this.xTreeView == null) { return; }
            if (!this.xTreeView.IsLoaded) { this.xTreeView.ApplyTemplate(); }

            //take a note of previously expanded groups
            List<String> prevExpanded =
                this.xTreeView.Nodes.SelectMany(GetAllChildNodes)
                    .Union(xTreeView.Nodes)
                    .Where(n => n.IsExpanded)
                    .Select(GetFullNodePath)
                    .ToList();
            
            //clear down the old nodes.
            List<TreeNode> oldNodes = new List<TreeNode>(this.xTreeView.Nodes);
            this.xTreeView.Nodes.Clear();
            oldNodes.ForEach(n => n.Dispose());
            oldNodes = null;

            if (this.ViewModel == null) return;
            if (this.ViewModel.AvailableGroups == null) return;

            if (_isFirstRefresh)
            {
                _isFirstRefresh = false;
                if (prevExpanded.Count == 0)
                {
                    var allCatsGroup = this.ViewModel.AvailableGroups.FirstOrDefault(c => !c.IsRecentWorkGroup);
                    if (allCatsGroup != null)
                    {
                        prevExpanded.Add(String.Format("||{0}", allCatsGroup.Id));
                    }
                }
            }

            foreach (var group in this.ViewModel.AvailableGroups)
            {
                this.xTreeView.Nodes.Add(CreateTreeNode(group, prevExpanded));
            }
        }

        /// <summary>
        /// Adds treenodes to the favourite treeview.
        /// </summary>
        private void GenerateFavouriteTreeNodes()
        {
            if (this.xFavouriteTreeView == null) { return; }
            if (!this.xFavouriteTreeView.IsLoaded) { this.xFavouriteTreeView.ApplyTemplate(); }

            List<String> prevExpandedFavourites =
                this.xFavouriteTreeView.Nodes.SelectMany(GetAllChildNodes)
                    .Union(this.xFavouriteTreeView.Nodes)
                    .Where(n => n.IsExpanded)
                    .Select(GetFullNodePath)
                    .ToList();

            //clear down the old nodes.
            List<TreeNode> oldFavouriteNodes = new List<TreeNode>(this.xFavouriteTreeView.Nodes);
            this.xFavouriteTreeView.Nodes.Clear();
            oldFavouriteNodes.ForEach(n => n.Dispose());
            oldFavouriteNodes = null;

            if (this.ViewModel == null) return;
            if (this.ViewModel.FavouriteGroups == null) return;

            if (_isFirstRefresh)
            {
                _isFirstRefresh = false;

                if (prevExpandedFavourites.Count == 0)
                {
                    var allCatsGroup = this.ViewModel.FavouriteGroups.FirstOrDefault(c => !c.IsRecentWorkGroup);
                    if (allCatsGroup != null)
                    {
                        prevExpandedFavourites.Add(String.Format("||{0}", allCatsGroup.Id));
                    }
                }
            }

            foreach (var group in this.ViewModel.FavouriteGroups.OrderBy(p => p.Name))
            {
                this.xFavouriteTreeView.Nodes.Add(CreateTreeNode(group, prevExpandedFavourites));
            }
        }

        /// <summary>
        /// Adds the given command to the context menu, using its Friendly Name and Small Icon.
        /// </summary>
        private void AddCommandToMenu(Fluent.ContextMenu contextMenu, RelayCommand command)
        {
            contextMenu.Items.Add(new Fluent.MenuItem()
            {
                Command = command,
                Header = command.FriendlyName,
                Icon = command.SmallIcon
            });
        }

        /// <summary>
        ///     Initializes the context menu for the given <paramref name="node"/>.
        /// </summary>
        /// <param name="node">The tree view node that requested a context menu.</param>
        /// <param name="isMainTreeView">Whether the node belongs to the main tree view (<c>true</c>) or to the favorites one (<c>false</c>).</param>
        /// <remarks>This method will create some specific entries depending on the value of <paramref name="isMainTreeView"/>.</remarks>
        private void InitializeContextMenu(TreeNode node, Boolean isMainTreeView)
        {
            //  Check whether there really is a node to add the context menu to, and select it.
            if (node == null) return;
            node.IsSelected = true;

            if (ViewModel == null) return;

            var contextMenu = new Fluent.ContextMenu();

            //  Add the main tree view context menu specific entries.
            if (isMainTreeView)
            {
                if (ViewModel.AddGroupToFavouritesCommand.CanExecute())
                    AddCommandToMenu(contextMenu, ViewModel.AddGroupToFavouritesCommand);

                if (ViewModel.RemoveGroupFromFavouritesCommand.CanExecute())
                    AddCommandToMenu(contextMenu, ViewModel.RemoveGroupFromFavouritesCommand);
            }
            //  Otherwise, add the favorites tree view context menu specific entries.
            else
            {
                if (ViewModel.RemoveGroupFromFavouritesCommand.CanExecute())
                {
                    AddCommandToMenu(contextMenu, ViewModel.RemoveGroupFromFavouritesCommand);
                }
            }

            //  Add the context menu common entries.
            if (!OnNodeContextMenuShowing(contextMenu) ||
                contextMenu.Items.Count <= 0) return;

            //  Open the context menu.
            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        ///     Scrolls the node in the Hierarchy Selector matching the given <paramref name="planogramGroup" />.
        /// </summary>
        /// <param name="planogramGroup"><see cref="PlanogramGroup" /> instance to use when finding the match to scroll to.</param>
        /// <remarks>
        ///     The PlanogramProductId will be used to determine matches. If none is found in the treeview's nodes nothing
        ///     will be scrolled into view.
        ///     <para></para>
        ///     When a match is found and scrolled to, it is expanded to display any existing children.
        /// </remarks>
        public void ScrollSelectionIntoView(PlanogramGroup planogramGroup)
        {
            //  Check there is a Planogram Group to try matching.
            if (planogramGroup == null) return;

            //  Check there is a match in the Hierarchy View linked to the treeview.
            PlanogramGroupViewModel match = PlanogramHierarchyView
                .EnumerateAllGroupViews().FirstOrDefault(v => v.PlanogramGroup.ProductGroupId == planogramGroup.ProductGroupId);
            if (match == null) return;

            //  Check there is really a node to scroll to.
            TreeNode node = xTreeView.GetNodeFromItem(match);
            if (node == null) return;

            //  Check there is something that can actually do the scrolling.
            var scrollViewer = node.FindVisualAncestor<ScrollViewer>();
            if (scrollViewer == null) return;

            //  Scroll to and expand the node.
            node.BringIntoView();
            node.IsExpanded = true;
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Creates a <see cref="TreeNode"/> from a given <paramref name="item"/>. Makes sure any expanded group nodes stay that way.
        /// </summary>
        /// <param name="item">The <see cref="PlanogramGroupViewModel"/> to be added to the new node.</param>
        /// <param name="nodePathsToExpand">The collection of full Ids path for the nodes that need to be expanded.</param>
        /// <param name="parentPath">The current path for the parent of this node.</param>
        /// <returns>A new instance of <see cref="TreeNode"/> which item is the provided <paramref name="item"/>, expanded if its path was in the <paramref name="nodePathsToExpand"/> collection.</returns>
        private static TreeNode CreateTreeNode(PlanogramGroupViewModel item, ICollection<String> nodePathsToExpand, String parentPath = null)
        {
            //  Create a unique node path to identify the node in case the planogram group appears in different nodes.
            if (parentPath == null) parentPath = "|";
            var nodePath = String.Format("{0}|{1}", parentPath, item.Id);

            var node = new TreeNode(item, true);
            var childViews = item.ChildViews;
            if (childViews != null &&
                (parentPath == "|" || !item.IsRecentWorkGroup))
            {
                foreach (var child in childViews)
                {
                    node.ChildNodes.Add(CreateTreeNode(child, nodePathsToExpand, nodePath));
                }
            }

            //bind the is expanded to the node control.
            BindingOperations.SetBinding(node, TreeNode.IsExpandedProperty, new Binding("Item.IsExpanded")
            {
                RelativeSource = new RelativeSource(RelativeSourceMode.Self),
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

            item.IsExpanded = (node.HasChildren && nodePathsToExpand.Contains(nodePath));

            return node;
        }

        /// <summary>
        ///     Gets the full node path (made of the Ids of the viewmodels in each node).
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static String GetFullNodePath(TreeNode node)
        {
            if (node == null) return "|";

            PlanogramGroupViewModel planogramGroupViewModel = node.Item as PlanogramGroupViewModel;
            if (planogramGroupViewModel == null) return String.Empty;

            String parentPath = GetFullNodePath(node.ParentTreeNode);
            return String.Format("{0}|{1}", parentPath, planogramGroupViewModel.Id);
        }

        /// <summary>
        ///     Obtain the strongly typed item contained in a TreeNode found as visual ancestor of the given <paramref name="senderControl"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="senderControl"></param>
        /// <returns></returns>
        private static T GetTreeNodeItem<T>(DependencyObject senderControl) where T : class
        {
            if (senderControl == null) return null;
            TreeNode targetTreeNode = senderControl.FindVisualAncestor<TreeNode>();
            return targetTreeNode == null ? null : targetTreeNode.Item as T;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed = true;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                //unload the viewmodel.
                IDisposable disView = this.ViewModel;
                this.ViewModel = null;
                disView.Dispose();

                //remove treeview handlers
                //xTreeView.RemoveHandler(TreeNode.PreviewMouseLeftButtonDownEvent, (MouseButtonEventHandler)PlanogramGroupDrop_MouseLeftButtonDown);
                //xTreeView.RemoveHandler(TreeNode.PreviewMouseMoveEvent, (MouseEventHandler)PlanogramGroupDrop_PreviewMouseMove);
                //xTreeView.RemoveHandler(TreeNode.DragOverEvent, (DragEventHandler)PlanogramGroupDrop_DragOver);
                //xTreeView.RemoveHandler(Treeview.DropEvent, (DragEventHandler)PlanogramGroupDrop_Drop);
                //xTreeView.RemoveHandler(TreeNode.DragLeaveEvent, (DragEventHandler)PlanogramGroupDrop_DragLeave);
                xTreeView.RemoveHandler(TreeNode.MouseRightButtonDownEvent, (MouseButtonEventHandler)xTreeView_MouseRightButtonDown);

                _isDisposed = true;

                GC.SuppressFinalize(this);
            }
        }

        #endregion
    }

    public class PlanogramHierarchyContextEventArgs : EventArgs
    {
        public Fluent.ContextMenu ContextMenu { get; set; }
        public Boolean Cancel { get; set; }

    }
}