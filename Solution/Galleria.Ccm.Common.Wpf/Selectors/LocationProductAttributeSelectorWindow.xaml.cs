﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Controls.Primitives;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for LocationProductAttributeSelectorWindow.xaml
    /// </summary>
    public partial class LocationProductAttributeSelectorWindow
    {
        #region Nested Types

        /// <summary>
        /// Event Args for dragging and dropping of attribute rows.
        /// </summary>
        private class LocationProductAttributeSelectorDropEventArgs
        {
            public LocationProductAttributeSelectorDropEventArgs() { }
        }

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(LocationProductAttributeSelectorViewModel), typeof(LocationProductAttributeSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// This view's view model.
        /// </summary>
        public LocationProductAttributeSelectorViewModel ViewModel
        {
            get { return (LocationProductAttributeSelectorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        ///     Connects a new view model to the window.
        /// </summary>
        /// <param name="newViewModel">View model to attach to the window.</param>
        private void AttachViewModel(LocationProductAttributeSelectorViewModel newViewModel)
        {
            if (newViewModel == null) return;
            newViewModel.DialogResultSet += OnViewModelDialogResultSet;
        }

        /// <summary>
        ///     Disconnects an old view model from the window.
        /// </summary>
        /// <param name="oldViewModel">View model to dettach from the window.</param>
        private void DettachViewModel(LocationProductAttributeSelectorViewModel oldViewModel)
        {
            if (oldViewModel == null) return;
            oldViewModel.DialogResultSet -= OnViewModelDialogResultSet;
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = (LocationProductAttributeSelectorWindow)obj;

            sender.DettachViewModel(e.OldValue as LocationProductAttributeSelectorViewModel);
            sender.AttachViewModel(e.NewValue as LocationProductAttributeSelectorViewModel);
        }

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new product attribute selector view.
        /// </summary>
        public LocationProductAttributeSelectorWindow(LocationProductAttributeSelectorViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        } 
        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xAvailableAttributesGrid_OnMouseDoubleClick(Object sender, MouseEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xAvailableAttributesGrid)) return;
            if (!ViewModel.AvailableAttributesSelection.Any()) return;

            ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the available attributes grid
        /// </summary>
        private void xAvailableAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called whenever a row is dropped on the selected attributes grid.
        /// </summary>
        private void xSelectedAttributesGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Event handler to allow double click on the selected columns data grid to add the currently selected column items
        /// </summary>
        /// <param name="sender">The control that initiated the event</param>
        /// <param name="e">The mouse button event arguments</param>
        private void xSelectedAttributesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xSelectedAttributesGrid)) return;

            if (!ViewModel.SelectedAttributes.Any()) return;

            ViewModel.RemoveSelectedAttributesCommand.Execute();
        }

        /// <summary>
        /// Called when the ViewModel.DialogResultSet event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelDialogResultSet(Object sender, EventArgs e)
        {
            if (ViewModel == null || !ViewModel.DialogResult.HasValue) return;
            this.DialogResult = ViewModel.DialogResult;
            this.Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (ViewModel != null)
            {
                ViewModel.DialogResultSet -= OnViewModelDialogResultSet;
                ViewModel.Dispose();
            }
        }
        #endregion
    }
}
