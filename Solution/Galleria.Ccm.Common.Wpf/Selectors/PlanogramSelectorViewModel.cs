﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-26891 : L.Ineson
//	Created
// V8-27964 : A.Silva
//      Added overloaded constructor to allow setting an initial planogram group selection, then removed.
//      Added StartingPlanogramGroup Property, then removed.
// CCM-27599 : J.Pickup
//  Added _currentEntity
#endregion

#region Version History: (CCM 8.0.1)
// CCM-28819 : D.Pleasance
//	Amended so that all categories (selected planogram group) is a valid selection criteria, 
//  no longer passes null filter value which resulted in all plans returning.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30491
//  Added a change to UpdateAvailablePlanogramInfos to allow sub-group search for planograms
//  when the selected group is the root and a search filter term is entered in the top-right 
//  filter text
#endregion

#region Version History: (CCM 8.3.0)
// V8-31762 : M.Shelley
//  Add a check to the constructor to check if the repository is disconnected.
// V8-31832 : L.Ineson
//  Amended to use planograminfoview so that calculated column support can be enabled.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Resources;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for PlanogramSelectorWindow
    /// </summary>
    public sealed class PlanogramSelectorViewModel : ViewModelAttachedControlObject<PlanogramSelectorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private String _custColumnPath;

        private readonly PlanogramHierarchyViewModel _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();
        private PlanogramGroup _selectedPlanogramGroup;

        private readonly UserPlanogramGroupListViewModel _userFavouritePlanGroupsView = new UserPlanogramGroupListViewModel();
        
        private String _planogramSearchCriteria;
        private readonly PlanogramInfoListViewModel _planogramInfoListViewModel = new PlanogramInfoListViewModel();
        private readonly BulkObservableCollection<PlanogramInfoView> _availablePlans = new BulkObservableCollection<PlanogramInfoView>();
        private readonly ReadOnlyBulkObservableCollection<PlanogramInfoView> _availablePlansRO;

        private PlanogramInfo _selectedPlanogramInfo;

        private Int32 _currentEntityId;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.PlanogramHierarchyView);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.SelectedPlanogramGroup);
        public static readonly PropertyPath UserFavouriteGroupsViewProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.UserFavouriteGroupsView);
        public static readonly PropertyPath PlanogramSearchCriteriaProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.PlanogramSearchCriteria);
        public static readonly PropertyPath AvailablePlanogramInfosProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.AvailablePlanogramInfos);
        public static readonly PropertyPath SelectedPlanogramInfoProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.SelectedPlanogramInfo);

        //Commands
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RefreshPlanogramsCommandProperty = WpfHelper.GetPropertyPath<PlanogramSelectorViewModel>(p => p.RefreshPlanogramsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the path of the custom column
        /// layout directory.
        /// </summary>
        public String CustomColumnPath
        {
            get { return _custColumnPath; }
        }

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanogramHierarchyView; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram group.
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
                OnSelectedPlanogramGroupChanged();
            }
        }

        /// <summary>
        /// Returns the view of the current user favourite groups.
        /// </summary>
        public UserPlanogramGroupListViewModel UserFavouriteGroupsView
        {
            get { return _userFavouritePlanGroupsView; }
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when fetching planograms.
        /// </summary>
        public String PlanogramSearchCriteria
        {
            get { return _planogramSearchCriteria; }
            set
            {
                _planogramSearchCriteria = value;
                OnPropertyChanged(PlanogramSearchCriteriaProperty);

                OnPlanogramSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Returns the collection of available planograms
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramInfoView> AvailablePlanogramInfos
        {
            get { return _availablePlansRO; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram
        /// </summary>
        public PlanogramInfo SelectedPlanogramInfo
        {
            get { return _selectedPlanogramInfo; }
            set
            {
                _selectedPlanogramInfo = value;
                OnPropertyChanged(SelectedPlanogramInfoProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///   Creates a new instance of this type.
        /// </summary>
        public PlanogramSelectorViewModel(String custColumnPath, Int32 entityId)
            :this(custColumnPath, entityId, null)
        {
        }

        /// <summary>
        ///   Creates a new instance of this type.
        /// </summary>
        public PlanogramSelectorViewModel(String custColumnPath, Int32 entityId, Int32? groupIdToSelect)
        {
            _availablePlansRO = new ReadOnlyBulkObservableCollection<PlanogramInfoView>(_availablePlans);
            _planogramInfoListViewModel.ModelChanged += PlanogramInfoListViewModel_ModelChanged;

            _custColumnPath = custColumnPath;
            _currentEntityId = entityId;

            //load the hierarchy model.
            if (_masterPlanogramHierarchyView.Model == null) _masterPlanogramHierarchyView.FetchForCurrentEntity();

            //set the selected group.
            PlanogramGroup groupToSelect = null;
            if(groupIdToSelect.HasValue)
            {
                groupToSelect = _masterPlanogramHierarchyView.Model.EnumerateAllGroups().FirstOrDefault(g=> g.Id == groupIdToSelect.Value);
            }

            // Check for null - occurs when disconnected from a repository
            if (_masterPlanogramHierarchyView.Model != null)
            {
                if (groupToSelect == null) groupToSelect = _masterPlanogramHierarchyView.Model.RootGroup;
                this.SelectedPlanogramGroup = groupToSelect;
            }
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="custColumnPath"></param>
        /// <param name="entityId"></param>
        /// <param name="hierarchyView"></param>
        /// <param name="groupToSelect"></param>
        public PlanogramSelectorViewModel(String custColumnPath, Int32 entityId, PlanogramHierarchyViewModel hierarchyView, PlanogramGroup groupToSelect)
        {
            _availablePlansRO = new ReadOnlyBulkObservableCollection<PlanogramInfoView>(_availablePlans);
            _planogramInfoListViewModel.ModelChanged += PlanogramInfoListViewModel_ModelChanged;

            _custColumnPath = custColumnPath;
            _currentEntityId = entityId;

            //load the hierarchy model.
            _masterPlanogramHierarchyView = hierarchyView;
            if (_masterPlanogramHierarchyView.Model == null) _masterPlanogramHierarchyView.FetchForCurrentEntity();

            //set the selected group.
            this.SelectedPlanogramGroup = (groupToSelect != null) ? groupToSelect : _masterPlanogramHierarchyView.Model.RootGroup;
        }

        #endregion

        #region Commands

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs this window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p=> OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);

                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (this.SelectedPlanogramInfo == null)
            {
                return false;
            }
            return true;
        }

        private void OK_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels this window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                        {
                            FriendlyName = Message.Generic_Cancel
                        };
                    base.ViewModelCommands.Add(_cancelCommand);

                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region RefreshPlanogramsCommand

        private RelayCommand _refreshPlanogramsCommand;

        /// <summary>
        /// Refreshes the current available planograms
        /// </summary>
        public RelayCommand RefreshPlanogramsCommand
        {
            get
            {
                if (_refreshPlanogramsCommand == null)
                {
                    _refreshPlanogramsCommand = new RelayCommand(
                        p => RefreshPlanograms_Executed(),
                        p => RefreshPlanograms_CanExecute())
                    {
                        FriendlyName = Message.PlanogramSelector_Refresh,
                        SmallIcon = ImageResources.PlanogramSelector_Refresh16,
                        InputGestureKey = Key.F5,
                    };
                    base.ViewModelCommands.Add(_refreshPlanogramsCommand);

                }
                return _refreshPlanogramsCommand;
            }
        }

        private Boolean RefreshPlanograms_CanExecute()
        {
            if (this.SelectedPlanogramGroup == null)
            {
                return false;
            }

            return true;
        }

        private void RefreshPlanograms_Executed()
        {
            UpdateAvailablePlanogramInfos();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the planogram 
        /// </summary>
        private void OnPlanogramSearchCriteriaChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Called whenever the value of the SelectedPlanogramGroup changes
        /// </summary>
        private void OnSelectedPlanogramGroupChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Called whenever the planogram info list viewmodel changes.
        /// </summary>
        private void PlanogramInfoListViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            if (_availablePlans.Any()) _availablePlans.Clear();

            if (e.NewModel != null)
            {
                _availablePlans.AddRange(e.NewModel.Select(p => new PlanogramInfoView(p)));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the collection of available planograms.
        ///     If the root planogram group is selected, use the search filter and look recursively down 
        ///     the planogram group hierarchy for the specified search term
        ///     
        ///     If the root planogram group is selected and no search term is specified, only show plans from 
        ///     the root planogram group
        ///     
        ///     If the user has a sub group selected, do not search down the planogram group hierarchy even if 
        ///     there is a search term specified
        /// </summary>
        private void UpdateAvailablePlanogramInfos()
        {
            PlanogramGroup selectedGroup = this.SelectedPlanogramGroup;

            if (selectedGroup != null)
            {                
                //start an async search.
                // Check if the root planogram group is selected, in which case we need to allow searching down the planogram group hierarchy

                Int32? searchPlanGroupId = selectedGroup.Id;
                if (selectedGroup.ParentGroup == null && !String.IsNullOrEmpty(PlanogramSearchCriteria))
                {
                    searchPlanGroupId = null;
                }
                _planogramInfoListViewModel.FetchBySearchCriteriaAsync(this.PlanogramSearchCriteria, searchPlanGroupId, _currentEntityId);
            }
            else
            {
                _planogramInfoListViewModel.Model = null;
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                _planogramInfoListViewModel.ModelChanged -= PlanogramInfoListViewModel_ModelChanged;
                _planogramInfoListViewModel.Dispose();
                _userFavouritePlanGroupsView.Dispose();
            }

            IsDisposed = true;
        }

        #endregion
    }
}
