﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26891 : L.Ineson 
//  Created.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
#endregion
#region Version History: (CCM 8.2.0)
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion
#region Version History: (CCM 8.3.0)
//V8-31832 : L.Ineson
//  Enabled calculated columns
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PlanogramSelectorWindow.xaml
    /// </summary>
    public sealed partial class PlanogramSelectorWindow
    {
        #region Fields
        private ColumnLayoutManager _columnLayoutManager;
        const String _screenKey = "PlanRepository.Workflow";
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramSelectorViewModel), typeof(PlanogramSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanogramSelectorWindow)obj;

            // Detach previous viemodel if there was one.
            if (e.OldValue != null)
            {
                PlanogramSelectorViewModel oldModel = e.OldValue as PlanogramSelectorViewModel;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PlanogramSelectorViewModel newModel = e.NewValue as PlanogramSelectorViewModel;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;


            }
            senderControl.UpdateBreadcrumb();
        }

        public PlanogramSelectorViewModel ViewModel
        {
            get { return (PlanogramSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramSelectorWindow(PlanogramSelectorViewModel view)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = view;

            this.Loaded += PlanogramSelectorWindow_Loaded;
        }

        private void PlanogramSelectorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramSelectorWindow_Loaded;

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                new PlanogramInfoColumnLayoutFactory(typeof(PlanogramInfo)),
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(CCMClient.ViewState.EntityId),
                _screenKey, /*pathMask*/"Model.{0}");

            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.xPlanogramGrid);


            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Handles the change of selection for the current <see cref="PlanogramInfo" />.
        /// </summary>
        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramSelectorViewModel.SelectedPlanogramGroupProperty.Path)
            {
                UpdateBreadcrumb();
            }
        }

         /// <summary>
        /// Called whenever the column layout manager columns are about to change.
        /// </summary>
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //add hardcoded name column to the start
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Model.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = false,
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(0, nameGridCol);

        }


        #region XPlanogramGrid handlers

        /// <summary>
        /// Called whenever a planogram row is right clicked.
        /// </summary>
        private void xPlanogramGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel == null) { return; }

            //show the row context menu
            var contextMenu = new Fluent.ContextMenu();

            AddCommandToMenu(contextMenu, this.ViewModel.RefreshPlanogramsCommand);

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Called whenever a planogram is double clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.OKCommand.Execute();
        }

        /// <summary>
        /// Called when the enter key is pressed on the grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnKeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && xPlanogramGrid.SelectedItem != null)
            {
                this.ViewModel.OKCommand.Execute();
                e.Handled = true;
            }
            else
            {
                return;
            }
        }

        #endregion

        /// <summary>
        /// Called whenever a breadcrumb is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Breadcrumb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel == null) return;

            TextBlock breadcrumb = (TextBlock)sender;
            PlanogramGroup group = breadcrumb.Tag as PlanogramGroup;
            if (group != null)
            {
                this.ViewModel.SelectedPlanogramGroup = group;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the given command to the context menu, using its Friendly Name and Small Icon.
        /// </summary>
        private void AddCommandToMenu(Fluent.ContextMenu contextMenu, RelayCommand command)
        {
            contextMenu.Items.Add(new Fluent.MenuItem
            {
                Command = command,
                Header = command.FriendlyName,
                //Icon = command.SmallIcon
            });
        }


        /// <summary>
        /// Updates the breadcurb area
        /// </summary>
        private void UpdateBreadcrumb()
        {
            if (this.BreadcrumbStackPanel == null) return;

            //clear out any existing items.
            foreach (TextBlock tb in this.BreadcrumbStackPanel.Children.OfType<TextBlock>())
            {
                tb.MouseLeftButtonDown -= Breadcrumb_MouseLeftButtonDown;
            }
            this.BreadcrumbStackPanel.Children.Clear();

            //load in the path for the selected plan group
            if (this.ViewModel != null && this.ViewModel.SelectedPlanogramGroup != null)
            {
                //add in the icon
                var icon = new System.Windows.Controls.Image()
                {
                    Height = 15,
                    Width = 15,
                    Source = Galleria.Ccm.Common.Wpf.Resources.ImageResources.TreeViewItem_OpenFolder,
                    Margin = new Thickness(1, 1, 4, 1),
                    VerticalAlignment = System.Windows.VerticalAlignment.Center
                };
                this.BreadcrumbStackPanel.Children.Add(icon);

                Style itemStyle = (Style)this.Resources["PlanogramSelector_StyBreadcrumbItem"];
                Style sepStyle = (Style)this.Resources["PlanogramSelector_StyBreadcrumbSeparator"];

                //load in the breadcrumb parts
                foreach (var group in this.ViewModel.SelectedPlanogramGroup.FetchFullPath())
                {
                    TextBlock breadcrumb = new TextBlock();
                    breadcrumb.Style = itemStyle;
                    breadcrumb.Text = group.Name;
                    breadcrumb.Tag = group;
                    breadcrumb.MouseLeftButtonDown += Breadcrumb_MouseLeftButtonDown;
                    this.BreadcrumbStackPanel.Children.Add(breadcrumb);

                    Path separator = new Path();
                    separator.Style = sepStyle;
                    this.BreadcrumbStackPanel.Children.Add(separator);
                }
            }

        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {

                    IDisposable dis = this.ViewModel;

                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
