﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31763 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Amended SetObjectFieldInfos to account for the Display value of each field.
// V8-32652 : A.Silva
//  Refactored Add Remove Selected commands to use the common icons.
// V8-32926 : A.Silva
//  Amended AdjustNewFieldsOrder so that new fields are added in their existing order if there is any (the specific case is when reloading the settings from scratch).

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public sealed class FieldPickerViewModel : ViewModelAttachedControlObject<FieldPickerControl>
    {
        #region Fields

        /// <summary>
        ///     Read only collection of all the fields available.
        /// </summary>
        private readonly ReadOnlyCollection<FieldPickerField> _masterFieldPickerFields;

        #endregion

        #region Properties

        #region Available Field Picker Groups

        /// <summary>
        ///     The <see cref="AvailableFieldPickerGroups"/> for the comparison.
        /// </summary>
        private readonly ReadOnlyCollection<FieldPickerGroup> _availableFieldPickerGroups;

        /// <summary>
        ///     Gets the <see cref="AvailableFieldPickerGroups"/> for the comparison.
        /// </summary>
        public ReadOnlyCollection<FieldPickerGroup> AvailableFieldPickerGroups { get { return _availableFieldPickerGroups; } }

        /// <summary>
        ///     <see cref="PropertyPath"/> definition for the <see cref="AvailableFieldPickerGroups"/> property.
        /// </summary>
        public static readonly PropertyPath AvailableFieldPickerGroupsProperty = GetPropertyPath(o => o.AvailableFieldPickerGroups);

        #endregion

        #region Selected Field Picker Group

        /// <summary>
        ///     The <see cref="SelectedFieldPickerGroup"/> to filter fields with..
        /// </summary>
        private FieldPickerGroup _selectedFieldPickerGroup;

        /// <summary>
        ///     Gets the <see cref="SelectedFieldPickerGroup"/> to filter fields with.
        /// </summary>
        public FieldPickerGroup SelectedFieldPickerGroup
        {
            get { return _selectedFieldPickerGroup; }
            set
            {
                _selectedFieldPickerGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);
                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        ///     <see cref="PropertyPath"/> definition for the <see cref="SelectedFieldPickerGroup"/> property.
        /// </summary>
        public static readonly PropertyPath SelectedFieldGroupProperty = GetPropertyPath(o => o.SelectedFieldPickerGroup);

        #endregion

        #region Available Field Picker Fields

        private readonly BulkObservableCollection<FieldPickerField> _availableFieldPickerFields =
            new BulkObservableCollection<FieldPickerField>();

        private ReadOnlyBulkObservableCollection<FieldPickerField> _readOnlyAvailableFieldPickerFields;

        public ReadOnlyBulkObservableCollection<FieldPickerField> AvailableFieldPickerFields
        {
            get
            {
                return _readOnlyAvailableFieldPickerFields ??
                       (_readOnlyAvailableFieldPickerFields = new ReadOnlyBulkObservableCollection<FieldPickerField>(_availableFieldPickerFields));
            }
        }

        public static readonly PropertyPath AvailableFieldPickerFieldsProperty = GetPropertyPath(o => o.AvailableFieldPickerFields);

        #endregion

        #region Selected Field Picker Fields

        private readonly ObservableCollection<FieldPickerField> _selectedFieldPickerFields = new ObservableCollection<FieldPickerField>();

        public ObservableCollection<FieldPickerField> SelectedFieldPickerFields { get { return _selectedFieldPickerFields; } }

        public static readonly PropertyPath SelectedFieldPickerFieldsProperty = GetPropertyPath(o => o.SelectedFieldPickerFields);

        #endregion

        #region Field Picker Rows

        private readonly BulkObservableCollection<FieldPickerRow> _fieldPickerRows = new BulkObservableCollection<FieldPickerRow>();

        private ReadOnlyBulkObservableCollection<FieldPickerRow> _readOnlyFieldPickerRows;

        public ReadOnlyBulkObservableCollection<FieldPickerRow> FieldPickerRows
        {
            get
            {
                return _readOnlyFieldPickerRows ??
                       (_readOnlyFieldPickerRows =
                        new ReadOnlyBulkObservableCollection<FieldPickerRow>(_fieldPickerRows));
            }
        }

        public static readonly PropertyPath FieldPickerRowsProperty = GetPropertyPath(o => o.FieldPickerRows);

        #endregion

        #region Selected Field Picker Rows

        private readonly ObservableCollection<FieldPickerRow> _selectedFieldPickerRows = new ObservableCollection<FieldPickerRow>();

        public ObservableCollection<FieldPickerRow> SelectedFieldPickerRows
        {
            get { return _selectedFieldPickerRows; }
        }

        public static readonly PropertyPath SelectedFieldPickerRowsProperty = GetPropertyPath(o => o.SelectedFieldPickerRows);

        #endregion

        #endregion

        #region Events

        public event EventHandler FieldOrderChanged;

        private void OnFieldOrderChanged()
        {
            if (FieldOrderChanged != null)
            {
                FieldOrderChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        public FieldPickerViewModel(IEnumerable<ObjectFieldInfo> fieldInfos)
        {
            //  Wrap the Object Field Infos in Field Picker Fields.
            _masterFieldPickerFields = fieldInfos.Select(i => new FieldPickerField(i)).ToList().AsReadOnly();

            //  Get the available field groups.
            var fieldGroups = new List<FieldPickerGroup> { new FieldPickerGroup { GroupName = "All", IsAllGroup = true } };
            fieldGroups.AddRange(_masterFieldPickerFields.GroupBy(g => g.GroupName).Select(c => new FieldPickerGroup { GroupName = c.Key }));
            _availableFieldPickerGroups = fieldGroups.AsReadOnly();

            //  Select the first field group.
            SelectedFieldPickerGroup = AvailableFieldPickerGroups.First();
        }

        #endregion

        #region Commands

        #region Add Selected Fields

        private RelayCommand _addSelectedFieldsCommand;

        public static readonly PropertyPath AddSelectedFieldsCommandProperty = GetPropertyPath(o => o.AddSelectedFieldsCommand);

        public RelayCommand AddSelectedFieldsCommand
        {
            get
            {
                if (_addSelectedFieldsCommand != null) return _addSelectedFieldsCommand;

                _addSelectedFieldsCommand =
                    new RelayCommand(p => AddSelectedFields_Executed(), p => AddSelectedFields_CanExecute())
                    {
                        FriendlyDescription = Message.FieldPicker_AddSelectedFieldsCommand_Description,
                        SmallIcon = Resources.ImageResources.Add_16,
                        DisabledReason = Message.FieldPicker_AddSelectedFieldsCommand_Disabled_NoFieldsSelected
                    };

                RegisterCommand(_addSelectedFieldsCommand);
                return _addSelectedFieldsCommand;
            }
        }

        private Boolean AddSelectedFields_CanExecute()
        {
            return SelectedFieldPickerFields.Any();
        }

        private void AddSelectedFields_Executed()
        {
            List<FieldPickerField> fields = SelectedFieldPickerFields.ToList();
            SelectedFieldPickerFields.Clear();
            AddFields(fields);
        }

        #endregion

        #region Remove Selected Fields

        private RelayCommand _removeSelectedFieldsCommand;

        public static readonly PropertyPath RemoveSelectedFieldsCommandProperty = GetPropertyPath(o => o.RemoveSelectedFieldsCommand);

        public RelayCommand RemoveSelectedFieldsCommand
        {
            get
            {
                if (_removeSelectedFieldsCommand != null) return _removeSelectedFieldsCommand;

                _removeSelectedFieldsCommand =
                    new RelayCommand(p => RemoveSelectedFields_Executed(), p => RemoveSelectedFields_CanExecute())
                    {
                        FriendlyDescription = Message.FieldPicker_RemoveSelectedFieldsCommand_Description,
                        SmallIcon = Resources.ImageResources.Delete_16,
                        DisabledReason = Message.FieldPicker_AddSelectedFieldsCommand_Disabled_NoFieldsSelected
                    };
                RegisterCommand(_removeSelectedFieldsCommand);

                return _removeSelectedFieldsCommand;
            }
        }

        private Boolean RemoveSelectedFields_CanExecute()
        {
            return SelectedFieldPickerRows.Any();
        }

        private void RemoveSelectedFields_Executed()
        {
            RemoveFields(SelectedFieldPickerRows.ToList());
        }

        #endregion

        #region Move Field Up

        private RelayCommand _moveFieldUpCommand;

        public static readonly PropertyPath MoveFieldUpCommandProperty = GetPropertyPath(o => o.MoveFieldUpCommand);

        /// <summary>
        ///     Move column up in the column order
        /// </summary>
        public RelayCommand MoveFieldUpCommand
        {
            get
            {
                if (_moveFieldUpCommand != null) return _moveFieldUpCommand;

                _moveFieldUpCommand = new RelayCommand(
                    p => MoveFieldUp_Executed(),
                    p => MoveFieldUp_CanExecute())
                                      {
                                          FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnUp_Description,
                                          SmallIcon = Resources.ImageResources.ColumnLayoutEditor_MoveColumnUp,
                                          DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                                      };
                RegisterCommand(_moveFieldUpCommand);

                return _moveFieldUpCommand;
            }
        }

        private Boolean MoveFieldUp_CanExecute()
        {
            if (SelectedFieldPickerRows == null)
                return false;

            if (!SelectedFieldPickerRows.Any())
            {
                MoveFieldUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }
            if (SelectedFieldPickerRows.Count != 1)
            {
                MoveFieldUpCommand.DisabledReason =
                    Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }
            if (SelectedFieldPickerRows[0].Number == 1)
            {
                MoveFieldUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsTopAlready;
                return false;
            }

            return true;
        }

        private void MoveFieldUp_Executed()
        {
            ExchangeSelectionOrder(-1);
        }

        #endregion

        #region Move Field Down

        private RelayCommand _moveFieldDownCommand;

        public static readonly PropertyPath MoveFieldDownCommandProperty = GetPropertyPath(o => o.MoveFieldDownCommand);

        /// <summary>
        ///     Move column down in the column order
        /// </summary>
        public RelayCommand MoveFieldDownCommand
        {
            get
            {
                if (_moveFieldDownCommand != null) return _moveFieldDownCommand;

                _moveFieldDownCommand = new RelayCommand(
                    p => MoveFieldDown_Executed(),
                    p => MoveFieldDown_CanExecute())
                                        {
                                            FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnDown_Description,
                                            SmallIcon = Resources.ImageResources.ColumnLayoutEditor_MoveColumnDown,
                                            DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                                        };
                RegisterCommand(_moveFieldDownCommand);

                return _moveFieldDownCommand;
            }
        }

        private Boolean MoveFieldDown_CanExecute()
        {
            if (SelectedFieldPickerRows == null) return false;

            if (SelectedFieldPickerRows.Count == 0)
            {
                MoveFieldDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }

            //can only have one columns seleted.
            if (SelectedFieldPickerRows.Count != 1)
            {
                MoveFieldDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }

            //cannot move down if last already
            Int32 maxNo = FieldPickerRows.Max(c => c.Number);
            if (SelectedFieldPickerRows.Any(c => c.Number == maxNo))
            {
                MoveFieldDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsLastAlready;
                return false;
            }


            return true;
        }

        private void MoveFieldDown_Executed()
        {
            ExchangeSelectionOrder(1);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Resets the selection of <see cref="FieldPickerRows"/> for the given <paramref name="fields"/>.
        /// </summary>
        /// <param name="fields">The collection of <see cref="PlanogramComparisonField"/> that need their selection reset.</param>
        /// 
        public void SetObjectFieldInfos(List<IPlanogramComparisonSettingsField> fields)
        {
            //  Move all selected fields out of the selected fields collection and into the available one.
            RemoveFields(FieldPickerRows.ToList());

            //  Get the placeholders to select matches for.
            Dictionary<String, Boolean> filterFieldPlaceholders = fields.ToDictionary(f => f.FieldPlaceholder, f => f.Display);

            //  Get the master fields that match the fields.
            List<FieldPickerField> matchingFields =
                _masterFieldPickerFields.Where(field => filterFieldPlaceholders.ContainsKey(field.FieldInfo.FieldPlaceholder)).ToList();

            foreach (FieldPickerField fieldPickerField in matchingFields)
            {
                IPlanogramComparisonSettingsField settingsField = fields.FirstOrDefault(field => field.FieldPlaceholder == fieldPickerField.FieldPlaceholder);
                if (settingsField == null) continue;

                fieldPickerField.DisplayName = settingsField.DisplayName;
                fieldPickerField.Number = settingsField.Number;
            }

            //  Select all the fields so they are displayed as such to the user.
            AddFields(matchingFields, filterFieldPlaceholders.Where(pair => pair.Value).Select(pair => pair.Key).ToList());
        }

        /// <summary>
        ///     Finds the <see cref="CustomColumn" /> with the provided <paramref name="path" /> and selects it.
        /// </summary>
        /// <param name="path">The path of the <see cref="CustomColumn" /> to locate in the Selected Fields />.</param>
        private void SelectColumnByPath(String path)
        {
            FieldPickerRow match = FieldPickerRows.FirstOrDefault(p => p.FieldPlaceholder == path);
            if (match == null) return;

            SelectedFieldPickerRows.Clear();
            SelectedFieldPickerRows.Add(match);
        }

        /// <summary>
        ///     Adds the <see cref="FieldPickerField"/> in the given collection to the selected rows collection.
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="displayFields">Collection containing the <c>field placeholders</c> for those fields that should be displayed. If <c>null</c> all will be displayed.</param>
        /// <remarks>All fields added are removed from the available fields collection.</remarks>
        private void AddFields(List<FieldPickerField> fields, ICollection<String> displayFields = null)
        {
            //  Remove the fields from the available fields collection so that they do not show again for the user.
            _availableFieldPickerFields.RemoveRange(fields.Where(c => c.FieldInfo != null));

            //  Create the new selected field rows.
            List<FieldPickerRow> newFields = fields.Select(field => FieldPickerRow.CreateFromField(field, displayFields == null || displayFields.Contains(field.FieldInfo.FieldPlaceholder))).ToList();

            //  Add the new selected field rows to the selected collection AND adjust the order numbers.
            _fieldPickerRows.AddRange(newFields);
            AdjustNewFieldsOrder(newFields);
        }

        /// <summary>
        ///     Removes the <see cref="FieldPickerRow"/> in the given collection from the selected rows collection.
        /// </summary>
        /// <param name="fields"></param>
        /// <remarks>All fields removed are added to the available fields collection.</remarks>
        private void RemoveFields(List<FieldPickerRow> fields)
        {
            //  Remove the fields from the selected fields collection so they do not appear as selected.
            _fieldPickerRows.RemoveRange(fields);

            //  Notify that the selection has changed,
            //  this will readd the removed fields to the available collection.
            OnSelectedFieldGroupChanged(SelectedFieldPickerGroup);

            //  Adjust the order numbers to correct any gaps.
            AdjustNewFieldsOrder(new List<FieldPickerRow>());
        }

        #region Column Ordering

        /// <summary>
        ///     Moves the selected column (or group columns if a group is selected) the provided offser rows.
        /// </summary>
        /// <param name="offset">Number of positions to move the selection.</param>
        /// <remarks>
        ///     Negative offsets move the selection up, possitive offsets move the selection down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeSelectionOrder(Int32 offset)
        {
            FieldPickerRow source = SelectedFieldPickerRows.FirstOrDefault();
            if (source == null) return;

            ExchangeOrder(source, offset);

            SelectColumnByPath(source.FieldPlaceholder);

            OnFieldOrderChanged();
        }

        /// <summary>
        ///  Swaps column order within the group.
        /// </summary>
        /// <param name="source">The <see cref="CustomColumn" /> that will be moved.</param>
        /// <param name="offset">Offset to the desired position.</param>
        /// <remarks>
        ///     Negative offsets move the column up, possitive offsets move the column down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeOrder(FieldPickerRow source, Int32 offset)
        {
            if (source == null ||
                offset == 0) return; // Nothing to exchange.

            var targetOrder = (Int16) (source.Number + offset);
            FieldPickerRow target = FieldPickerRows.FirstOrDefault(o => o.Number == targetOrder);

            if (target == null) return;
            target.Number = source.Number;
            source.Number = targetOrder;
        }

        /// <summary>
        ///     Adjusts the order number of each <see cref="CustomColumn" /> from <paramref name="newItems" /> so that they
        ///     can be appended at the end of the <paramref name="newItems" />.
        /// </summary>
        /// <remarks>Each group header's <see cref="CustomColumn" /> collection has its own order within the group.</remarks>
        private void AdjustNewFieldsOrder(ICollection<FieldPickerRow> newItems)
        {
            if (newItems.Count == 0) return;

            IEnumerable<FieldPickerRow> orderedCols = FieldPickerRows.Except(newItems).OrderBy(c => c.Number).Union(newItems.OrderBy(row => row.Number));

            Int16 colNo = 1;
            foreach (FieldPickerRow col in orderedCols)
            {
                col.Number = colNo++;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        private void OnSelectedFieldGroupChanged(FieldPickerGroup fieldPickerGroup)
        {
            _availableFieldPickerFields.Clear();

            List<String> takenFields = _fieldPickerRows.Select(c => c.FieldPlaceholder).ToList();

            _availableFieldPickerFields.AddRange(
                _masterFieldPickerFields.Where(f => fieldPickerGroup.IsAllGroup || fieldPickerGroup.GroupName == f.GroupName)
                                        .Except(_masterFieldPickerFields.Where(f => takenFields.Contains(f.FieldPlaceholder))).OrderBy(f => f.FieldPlaceholder));
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="FieldPickerViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<FieldPickerViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        /// <summary>
        /// Registers the given command with this base.
        /// So it can be called using keyboard shortcuts.
        /// </summary>
        /// <param name="command"></param>
        private void RegisterCommand(IRelayCommand command)
        {
            Debug.Assert(!ViewModelCommands.Contains(command), "{0} Command has already been registered", command.FriendlyName);
            ViewModelCommands.Add(command);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            IsDisposed = true;
        }

        #endregion
    }

    public sealed class FieldPickerField
    {
        public ObjectFieldInfo FieldInfo { get; set; }

        public FieldPickerField(ObjectFieldInfo info)
        {
            FieldInfo = info;
        }

        public String Header { get { return FieldInfo.FieldFriendlyName; } }
        public String GroupName { get { return FieldInfo.GroupName; } }
        public String FieldPlaceholder { get { return FieldInfo.FieldPlaceholder; } }
        public Int32 Number { get; set; }
        public String DisplayName { get; set; }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return Header;
        }
    }

    public sealed class FieldPickerGroup
    {
        public String GroupName { get; set; }
        public Boolean IsAllGroup { get; set; }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return GroupName;
        }
    }

    /// <summary>
    ///     Wrapper class for a custom column used to configure Planogram Comparisons.
    /// </summary>
    public sealed class FieldPickerRow : INotifyPropertyChanged, IPlanogramComparisonSettingsField
    {
        #region Properties

        public FieldPickerField Field { get; private set; }

        /// <summary>
        ///     The <see cref="PlanogramItemType" /> that this instance's field is.
        /// </summary>
        public PlanogramItemType ItemType { get; set; }

        public String FieldPlaceholder { get { return Field.FieldPlaceholder; } set { } }

        public Int16 Number
        {
            get { return (Int16) Field.Number; }
            set
            {
                if (Field.Number == value) return;

                Field.Number = value;
                OnPropertyChanged("Number");
                OnPropertyChanged("ColumnNumberAndHeader");
            }
        }

        public String Header { get; private set; }

        public String DisplayName
        {
            get { return String.IsNullOrWhiteSpace(Field.DisplayName) ? Header : Field.DisplayName; }
            set
            {
                if (Field.DisplayName == value) return;

                Field.DisplayName = value;
                OnPropertyChanged("DisplayName");
                OnPropertyChanged("FullDisplayName");
            }
        }

        public Boolean Display { get; set; }

        public String ColumnNumberAndHeader { get { return String.Format("{0}.{1}", Number, Header); } }

        #endregion

        #region Constructor

        public FieldPickerRow() { }

        public static FieldPickerRow CreateFromField(FieldPickerField fieldPickerField, Boolean display)
        {
            return new FieldPickerRow { Field = fieldPickerField, Header = fieldPickerField.Header, Display = display, Number = (Int16) fieldPickerField.Number, DisplayName = fieldPickerField.DisplayName};
        }

        #endregion

        #region Overrides of Object

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return Header;
        }

        #endregion

        #region INotifyProperty Changed

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}