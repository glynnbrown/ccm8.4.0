﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.3)
// V8-29723 : D.Pleasance
//  Created.
#endregion
#region Version History : CCM820
// V8-30948 : A.Kuszyk
//  Added all double click selection overload to product selector constructor.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Csla;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.ComponentModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Viewmodel controller for replace product selector window.
    /// </summary>
    public class ReplaceProductSelectorViewModel : ViewModelAttachedControlObject<ReplaceProductSelectorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;

        private BulkObservableCollection<ReplaceProductRow> _replaceProductRows = new BulkObservableCollection<ReplaceProductRow>();
        private ReadOnlyBulkObservableCollection<ReplaceProductRow> _replaceProductRowsRO;
        private ReplaceProductRow _selectedReplaceProductRow;

        #endregion

        #region Binding Property Paths

        //Commands
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductSelectorViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductSelectorViewModel>(p => p.CancelCommand);
        public static PropertyPath ReplaceProductRowsProperty = WpfHelper.GetPropertyPath<ReplaceProductSelectorViewModel>(p => p.ReplaceProductRows);
        public static PropertyPath SelectedReplaceProductRowProperty = WpfHelper.GetPropertyPath<ReplaceProductSelectorViewModel>(p => p.SelectedReplaceProductRow);
                
        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the collection of products to replace
        /// </summary>
        public ReadOnlyBulkObservableCollection<ReplaceProductRow> ReplaceProductRows
        {
            get
            {
                if (_replaceProductRowsRO == null)
                {
                    _replaceProductRowsRO = new ReadOnlyBulkObservableCollection<ReplaceProductRow>(_replaceProductRows);
                }
                return _replaceProductRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected replace product
        /// </summary>
        public ReplaceProductRow SelectedReplaceProductRow
        {
            get { return _selectedReplaceProductRow; }
            set
            {
                _selectedReplaceProductRow = value;
                OnPropertyChanged(SelectedReplaceProductRowProperty);
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="replaceGtins">The current replace details (Replace Gtin, Replacement Gtin)</param>
        public ReplaceProductSelectorViewModel(List<Tuple<String, String>> replaceGtins)
        {
            List<String> replaceProductDetails = new List<String>();

            replaceProductDetails.AddRange(replaceGtins.Select(p=> p.Item1));
            replaceProductDetails.AddRange(replaceGtins.Select(p => p.Item2));

            if (replaceProductDetails.Any())
            {
                // load replace product selections
                Dictionary<String, Product> replaceProducts = ProductList.FetchByEntityIdProductGtins(CCMClient.ViewState.EntityId, replaceProductDetails.Distinct()).ToDictionary(p => p.Gtin);

                foreach (var item in replaceGtins)
                {
                    // load into replacement grid
                    _replaceProductRows.Add(new ReplaceProductRow(replaceProducts[item.Item1], replaceProducts[item.Item2]));
                }
            }
        }

        #endregion

        #region Commands

        #region SelectReplaceProductCommand

        private RelayCommand _selectReplaceProductCommand;

        /// <summary>
        /// Select the product to replace
        /// </summary>
        public RelayCommand SelectReplaceProductCommand
        {
            get
            {
                if (_selectReplaceProductCommand == null)
                {
                    _selectReplaceProductCommand = new RelayCommand(
                        p => SelectReplaceProduct_Executed(),
                        p => SelectReplaceProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.ReplaceProductSelector_SelectReplaceProduct
                    };
                    base.ViewModelCommands.Add(_selectReplaceProductCommand);
                }
                return _selectReplaceProductCommand;
            }
        }

        private Boolean SelectReplaceProduct_CanExecute()
        {
            return true;
        }

        private void SelectReplaceProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                List<String> currentReplaceProducts = _replaceProductRows.Select(p => p.ProductGtin).ToList();

                ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                productSelectorHelper.IsMultiSelectEnabled = true;
                productSelectorHelper.AssignedProducts.AddRange(_replaceProductRows.Select(p => p.ReplaceProduct));
                productSelectorHelper.ExcludedProducts.AddRange(_replaceProductRows.Select(p => p.ReplacementProductGtin));

                var winModel = new ProductSelectorViewModel(productSelectorHelper);
                winModel.ShowDialog(this.AttachedControl);

                if (winModel.DialogResult == true)
                {
                    foreach (Product product in winModel.AssignedProducts)
                    {
                        if (!currentReplaceProducts.Contains(product.Gtin))
                        {
                            _replaceProductRows.Add(new ReplaceProductRow(product));
                        }
                    }
                }
            }
        }

        #endregion

        #region SelectReplacementProductCommand

        private RelayCommand _selectReplacementProductCommand;

        /// <summary>
        /// Select the replacement product for the selected row
        /// </summary>
        public RelayCommand SelectReplacementProductCommand
        {
            get
            {
                if (_selectReplacementProductCommand == null)
                {
                    _selectReplacementProductCommand = new RelayCommand(
                        p => SelectReplacementProduct_Executed(),
                        p => SelectReplacementProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.ReplaceProductSelector_SelectReplacementProduct
                    };
                    base.ViewModelCommands.Add(_selectReplacementProductCommand);
                }
                return _selectReplacementProductCommand;
            }
        }

        private Boolean SelectReplacementProduct_CanExecute()
        {
            return (this.SelectedReplaceProductRow != null);
        }

        private void SelectReplacementProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                productSelectorHelper.ExcludedProducts.AddRange(_replaceProductRows.Select(p => p.ProductGtin));
                productSelectorHelper.ExcludedProducts.AddRange(_replaceProductRows.Select(p => p.ReplacementProductGtin));

                var winModel = new ProductSelectorViewModel(productSelectorHelper);
                CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(winModel, /*allowDoubleClickSelection*/false);

                if (winModel.DialogResult == true)
                {
                    this.SelectedReplaceProductRow.ReplacementProduct = winModel.SelectedProduct;
                }
            }
        }

        #endregion

        #region RemoveReplaceProductCommand

        private RelayCommand _removeReplaceProductCommand;

        /// <summary>
        /// Remove the selected row
        /// </summary>
        public RelayCommand RemoveReplaceProductCommand
        {
            get
            {
                if (_removeReplaceProductCommand == null)
                {
                    _removeReplaceProductCommand = new RelayCommand(
                        p => RemoveReplaceProduct_Executed(),
                        p => RemoveReplaceProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.ReplaceProductSelector_RemoveReplaceProduct,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeReplaceProductCommand);
                }
                return _removeReplaceProductCommand;
            }
        }

        private Boolean RemoveReplaceProduct_CanExecute()
        {
            return (SelectedReplaceProductRow != null);
        }

        private void RemoveReplaceProduct_Executed()
        {
            _replaceProductRows.Remove(SelectedReplaceProductRow);
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OKCommand_CanExecute()
        {
            return this.ReplaceProductRows.All(p => p.IsValid); ;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }


        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _replaceProductRows.Clear();
                _selectedReplaceProductRow = null;
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Row view model for the product to be replaced
    /// </summary>
    public class ReplaceProductRow : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private Product _product;
        private Product _replacementProduct = null;
        
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<ReplaceProductRow>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<ReplaceProductRow>(p => p.ProductName);
        public static readonly PropertyPath ReplacementProductGtinProperty = WpfHelper.GetPropertyPath<ReplaceProductRow>(p => p.ReplacementProductGtin);
        public static readonly PropertyPath ReplacementProductNameProperty = WpfHelper.GetPropertyPath<ReplaceProductRow>(p => p.ReplacementProductName);

        #endregion

        #region Properties

        /// <summary>
        /// The replace product
        /// </summary>
        public Product ReplaceProduct
        {
            get { return _product; }
        }

        /// <summary>
        /// Returns the replace product gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _product.Gtin; }
        }

        /// <summary>
        /// Returns the replace product name
        /// </summary>
        public String ProductName
        {
            get { return _product.Name; }
        }

        /// <summary>
        /// Returns the Replacement product
        /// </summary>
        public Product ReplacementProduct
        {
            get { return _replacementProduct; }
            set
            {
                _replacementProduct = value;
                OnPropertyChanged(ReplacementProductGtinProperty);
                OnPropertyChanged(ReplacementProductNameProperty);
            }
        }

        /// <summary>
        /// Returns the replacement product gtin
        /// </summary>
        public String ReplacementProductGtin
        {
            get { return _replacementProduct == null ? null : _replacementProduct.Gtin; }
        }

        /// <summary>
        /// Returns the replacement product name
        /// </summary>
        public String ReplacementProductName
        {
            get { return _replacementProduct == null ? null : _replacementProduct.Name; }
        }

        /// <summary>
        /// Returns whether the row is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                return _replacementProduct != null;
            }            
        }

        #endregion

        #region Constructor
        
        public ReplaceProductRow(Product product)
        {
            _product = product;
        }

        public ReplaceProductRow(Product product, Product replacementProduct)
        {
            _product = product;
            _replacementProduct = replacementProduct;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._product = null;
                    this._replacementProduct = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo


        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        string IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == ReplacementProductGtinProperty.Path
                    || columnName == ReplacementProductNameProperty.Path)
                {
                    if (_replacementProduct == null)
                        result = Message.ReplaceProductRow_ReplacementProductRequired;
                }
                
                return result;
            }
        }
        #endregion
    }
}