﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0.0)

// V8-27964 : A.Silva
//      Created
// V8-27966 : A.Silva
//      Added Dialog Result.
//      Added ShowMerchGroupSelector to allow hidding the product group selection.
// V8-28043 : J.Pickup
//      Added option to DeSelect Merchandising Group & localisation

#endregion

#region Version History: (CCM v8.0.1)
// V8-28936 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History: (CCM v8.0.2)
// V8-28980 : I.George
// Added a check for unique planogram group name in Apply Command
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Message = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using Galleria.Ccm.Common.Wpf.Resources;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    ///     ViewModel controller to edit Planogram Groups.
    /// </summary>
    public sealed class PlanogramGroupEditViewModel : ViewModelAttachedControlObject<PlanogramGroupEditWindow>
    {
        #region Fields
        
        private readonly PlanogramGroup _planogramGroup;
        private readonly ProductHierarchyViewModel _masterMerchandisingGroups;

        #endregion

        #region Binding Property Paths

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="GroupName"/>.
        /// </summary>
        public static readonly PropertyPath GroupNameProperty = GetPropertyPath(o => o.GroupName);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="MerchandisingGroupId"/>.
        /// </summary>
        public static readonly PropertyPath MerchandisingGroupIdProperty = GetPropertyPath(o => o.MerchandisingGroupId);

        // Commands.

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="ApplyCommand"/>.
        /// </summary>
        public static readonly PropertyPath ApplyCommandProperty = GetPropertyPath(o => o.ApplyCommand);

        #endregion

        #region Properties

        #region DialogResult

        private Boolean? _dialogResult;

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        #endregion

        #region GroupName

        /// <summary>
        ///     Gets or sets the value of the <see cref="GroupName"/> property, notifying when the value changes.
        /// </summary>
        public String GroupName
        {
            get
            {
                return _planogramGroup != null 
                    ? _planogramGroup.Name 
                    : null;
            }
            set
            {
                if (_planogramGroup == null) return;
                _planogramGroup.Name = value;
                OnPropertyChanged(GroupNameProperty);
            }
        }

        #endregion

        #region MerchandisingGroupId

        /// <summary>
        ///     Gets or sets the value of the <see cref="MerchandisingGroupId"/> property, notifying when the value changes.
        /// </summary>
        public Int32? MerchandisingGroupId
        {
            get
            {
                return _planogramGroup != null 
                    ? _planogramGroup.ProductGroupId 
                    : null;
            }
            set
            {
                if (_planogramGroup == null) return;

                _planogramGroup.ProductGroupId = value;
                OnPropertyChanged(MerchandisingGroupIdProperty);
            }
        }

        #endregion

        #region MerchandisingGroupNameLookUpById

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="MerchandisingGroupNameLookUpById"/>.
        /// </summary>
        public static readonly PropertyPath MerchandisingGroupNameLookUpByIdProperty = GetPropertyPath(o => o.MerchandisingGroupNameLookUpById);

        /// <summary>
        ///     Holds the value for the <see cref="MerchandisingGroupNameLookUpById"/> property.
        /// </summary>
        private Dictionary<Int32, String> _merchandisingGroupNameLookUpById;

        /// <summary>
        ///     Gets or sets the value of the <see cref="MerchandisingGroupNameLookUpById"/> property, notifying when the value changes.
        /// </summary>
        public Dictionary<Int32, String> MerchandisingGroupNameLookUpById
        {
            get { return _merchandisingGroupNameLookUpById; }
        }

        #endregion

        #region ShowMerchGroupSelector

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="ShowMerchGroupSelector"/>.
        /// </summary>
        public static readonly PropertyPath ShowProductGroupSyncProperty = GetPropertyPath(o => o.ShowMerchGroupSelector);

        /// <summary>
        ///     Holds the value for the <see cref="ShowMerchGroupSelector"/> property.
        /// </summary>
        private Boolean _showMerchGroupSelector;

        /// <summary>
        ///     Gets or sets whether to display the product group sync or not.
        /// </summary>
        public Boolean ShowMerchGroupSelector
        {
            get { return _showMerchGroupSelector; }
            set
            {
                _showMerchGroupSelector = value;
                OnPropertyChanged(ShowProductGroupSyncProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of this type.
        /// </summary>
        public PlanogramGroupEditViewModel(PlanogramGroup planogramGroup)
        {
            _planogramGroup = planogramGroup;

            //  Lookup Merchandising Group Names by Id
            _masterMerchandisingGroups = new ProductHierarchyViewModel();
            _masterMerchandisingGroups.FetchMerchandisingHierarchyForCurrentEntity();
            _merchandisingGroupNameLookUpById =
                _masterMerchandisingGroups.Model.EnumerateAllGroups().ToDictionary(g => g.Id, g => g.Name);

            _planogramGroup.BeginEdit();
        }

        #endregion

        #region Commands

        #region SelectMerchandisingGroup

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        public static readonly PropertyPath SelectMerchandisingGroupCommandProperty =
            GetPropertyPath(o => o.SelectMerchandisingGroupCommand);

        /// <summary>
        ///     Reference to the current instance for <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        private RelayCommand _selectMerchandisingGroupCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SelectMerchandisingGroupCommand
        {
            get
            {
                if (_selectMerchandisingGroupCommand == null)
                {
                    _selectMerchandisingGroupCommand = new RelayCommand(o => SelectMerchandisingGroup_Executed(),
                   o => SelectMerchandisingGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    ViewModelCommands.Add(_selectMerchandisingGroupCommand);
                }
                return _selectMerchandisingGroupCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        private Boolean SelectMerchandisingGroup_CanExecute()
        {
            // User must have get permission.
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof (ProductGroup)))
            {
                this.SelectMerchandisingGroupCommand.DisabledReason = Message.PlanogramGroupSelector_SelectMerchandisingGroup_DisabledReason_Permissions;
                return false;
            }

            if (_planogramGroup == null)
            {
                // There must be a planogram group selected.
                this.SelectMerchandisingGroupCommand.DisabledReason = Message.PlanogramGroupSelector_SelectMerchandisingGroup_DisabledReason_NotSelected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SelectMerchandisingGroupCommand"/> is executed.
        /// </summary>
        private void SelectMerchandisingGroup_Executed()
        {
            if (this.AttachedControl == null) return;

            // Get the user's Product Universe selection, if any.
            MerchGroupSelectionWindow dialog = new MerchGroupSelectionWindow();
            dialog.ShowDialog();
            if (dialog.SelectionResult == null) return;

            ProductGroup productGroup = dialog.SelectionResult.FirstOrDefault();
            if (productGroup == null || MerchandisingGroupId == productGroup.Id) return;

            MerchandisingGroupId = productGroup.Id;
        }

        #endregion

        #region DeSelectMerchandisingGroup

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="DeSelectMerchandisingGroupCommand"/>.
        /// </summary>
        public static readonly PropertyPath DeSelectMerchandisingGroupCommandProperty =
            GetPropertyPath(o => o.DeSelectMerchandisingGroupCommand);

        /// <summary>
        ///     Reference to the current instance for <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        private RelayCommand _deSelectMerchandisingGroupCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SelectMerchandisingGroupCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand DeSelectMerchandisingGroupCommand
        {
            get
            {
                if (_deSelectMerchandisingGroupCommand == null)
                {
                    _deSelectMerchandisingGroupCommand = new RelayCommand(o => DeSelectMerchandisingGroup_Executed(),
                     o => DeSelectMerchandisingGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramGroupSelector_DeSelectMerchandisingGroup_FriendlyName,
                        SmallIcon = ImageResources.Delete_16
                    };
                    ViewModelCommands.Add(_deSelectMerchandisingGroupCommand);
                }
                return _deSelectMerchandisingGroupCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="DeSelectMerchandisingGroupCommand"/>.
        /// </summary>
        private Boolean DeSelectMerchandisingGroup_CanExecute()
        {
            if (MerchandisingGroupId == null)
            {
                // There must be a Merchandising group defined.
                this.DeSelectMerchandisingGroupCommand.DisabledReason = Message.PlanogramGroupSelector_DeSelectMerchandisingGroup_NoMerchandisingSelected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="DeSelectMerchandisingGroupCommand"/> is executed.
        /// </summary>
        private void DeSelectMerchandisingGroup_Executed()
        {
            if (this.AttachedControl == null) return;

            MerchandisingGroupId = null;
        }

        #endregion

        #region Apply

        /// <summary>
        ///     Reference to the current instance for <see cref="ApplyCommand"/>.
        /// </summary>
        private RelayCommand _applyCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="ApplyCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(o => Apply_Executed(),
                        o => Apply_CanExecute())
                    {
                        FriendlyName =  Message.Generic_Apply
                    };
                    ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="ApplyCommand"/>.
        /// </summary
        //[DebuggerStepThrough]
        private Boolean Apply_CanExecute()
        {
            // Must be dirty.
            if (!this._planogramGroup.IsDirty)
            {
                this.ApplyCommand.DisabledReason = Message.Generic_ItemIsNotDirty;
                return false;
            }

            // Must be valid.
            if (!this._planogramGroup.IsValid)
            {
                this.ApplyCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            // checks that the planogram group name is unique
            if (GroupName != null && this._planogramGroup.ParentHierarchy.EnumerateAllGroups().Any(m => m.Id != _planogramGroup.Id && m.Name == GroupName))
            {
                this.ApplyCommand.DisabledReason = Message.ApplyCommand_DisabledResason;
                return false;
            }
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="ApplyCommand"/> is executed.
        /// </summary>
        private void Apply_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                // Close the window by returning true.
                _planogramGroup.ApplyEdit();
                DialogResult = true;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Cancel

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="CancelCommand"/>.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        /// <summary>
        ///     Reference to the current instance for <see cref="CancelCommand"/>.
        /// </summary>
        private RelayCommand _cancelCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CancelCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(o => Cancel_Executed(), o => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="CancelCommand"/>.
        /// </summary>
        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CancelCommand"/> is executed.
        /// </summary>
        private void Cancel_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                // Close the window by returning false.
                _planogramGroup.CancelEdit();
                DialogResult = false;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Methods

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramGroupEditViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<PlanogramGroupEditViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (base.IsDisposed)
            {
                return;
            }

            if (disposing)
            {
                // Release unmanaged resources.
            }

            IsDisposed = true;
        }

        #endregion
    }
}