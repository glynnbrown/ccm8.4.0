﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27196 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    public sealed class ObjectFieldInfoSelectorViewModel : ViewModelAttachedControlObject<ObjectFieldInfoSelectorWindow>
    {
        #region Binding Property Paths

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="AvailableObjectFieldInfos" />.
        /// </summary>
        public static readonly PropertyPath AvailableObjectFieldInfosProperty =
            GetPropertyPath(o => o.AvailableObjectFieldInfos);

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="AvailableOwners" />.
        /// </summary>
        public static readonly PropertyPath AvailableOwnersProperty = GetPropertyPath(o => o.AvailableOwners);

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="OkCommand" />.
        /// </summary>
        public static readonly PropertyPath OkCommandProperty = GetPropertyPath(o => o.OkCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="SelectedObjectFieldInfo" />.
        /// </summary>
        public static readonly PropertyPath SelectedObjectFieldInfoProperty =
            GetPropertyPath(o => o.SelectedObjectFieldInfo);

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="SelectedOwner" />.
        /// </summary>
        public static readonly PropertyPath SelectedOwnerProperty = GetPropertyPath(o => o.SelectedOwner);

        #endregion

        #region Properties

        #region AvailableObjectFieldInfos

        /// <summary>
        ///     Holds the value for the <see cref="AvailableObjectFieldInfos" /> property.
        /// </summary>
        private readonly ObservableCollection<ObjectFieldInfo> _availableObjectFieldInfos =
            new ObservableCollection<ObjectFieldInfo>();

        /// <summary>
        ///     Gets or sets the value of the <see cref="AvailableObjectFieldInfos" /> property, notifying when the value changes.
        /// </summary>
        public ReadOnlyObservableCollection<ObjectFieldInfo> AvailableObjectFieldInfos
        {
            get { return new ReadOnlyObservableCollection<ObjectFieldInfo>(_availableObjectFieldInfos); }
        }

        #endregion

        #region SelectedObjectFieldInfo

        /// <summary>
        ///     Holds the value for the <see cref="SelectedObjectFieldInfo" /> property.
        /// </summary>
        private ObjectFieldInfo _selectedObjectFieldInfo;

        /// <summary>
        ///     Gets or sets the value of the <see cref="SelectedObjectFieldInfo" /> property, notifying when the value changes.
        /// </summary>
        public ObjectFieldInfo SelectedObjectFieldInfo
        {
            get { return _selectedObjectFieldInfo; }
            set
            {
                _selectedObjectFieldInfo = value;
                OnPropertyChanged(SelectedObjectFieldInfoProperty);
            }
        }

        #endregion

        #region AvailableOwners

        /// <summary>
        ///     Holds the value for the <see cref="AvailableOwners" /> property.
        /// </summary>
        private readonly ReadOnlyCollection<OwnerRowItem> _availableOwners;

        /// <summary>
        ///     Gets or sets the value of the <see cref="AvailableOwners" /> property, notifying when the value changes.
        /// </summary>
        public ReadOnlyCollection<OwnerRowItem> AvailableOwners
        {
            get { return _availableOwners; }
        }

        #endregion

        #region SelectedOwner

        /// <summary>
        ///     Holds the value for the <see cref="SelectedOwner" /> property.
        /// </summary>
        private OwnerRowItem _selectedOwner;

        /// <summary>
        ///     Gets or sets the value of the <see cref="SelectedOwner" /> property, notifying when the value changes.
        /// </summary>
        public OwnerRowItem SelectedOwner
        {
            get { return _selectedOwner; }
            set
            {
                _selectedOwner = value;
                OnSelectedOwnerChanged();
                OnPropertyChanged(SelectedOwnerProperty);
            }
        }

        #endregion

        /// <summary>
        ///     Gets or sets the result for this dialog.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        #endregion

        #region Constructor

        public ObjectFieldInfoSelectorViewModel()
        {
            _availableOwners = new ReadOnlyCollection<OwnerRowItem>(InitializeAvailableOwners());
        }

        #endregion

        #region Commands

        #region Ok

        /// <summary>
        ///     Reference to the current instance for <see cref="OkCommand" />.
        /// </summary>
        private RelayCommand _okCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="OkCommand" />.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => Ok_Executed(), o => Ok_CanExecute())
                {
                    FriendlyName = "Ok"
                };
                ViewModelCommands.Add(_okCommand);
                return _okCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the UI needs to assess the availability of the <see cref="OkCommand" />.
        /// </summary>
        private Boolean Ok_CanExecute()
        {
            return SelectedObjectFieldInfo != null;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="OkCommand" /> is executed.
        /// </summary>
        private void Ok_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region Cancel

        /// <summary>
        ///     Static <see cref="PropertyPath" /> for the <see cref="CancelCommand" />.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        /// <summary>
        ///     Reference to the current instance for <see cref="CancelCommand" />.
        /// </summary>
        private RelayCommand _cancelCommand;

        private bool? _dialogResult;

        /// <summary>
        ///     Gets the reference to the <see cref="CancelCommand" />.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => Cancel_Executed(), o => Cancel_CanExecute())
                {
                    FriendlyName = "Cancel"
                };
                ViewModelCommands.Add(_cancelCommand);
                return _cancelCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the UI needs to assess the availability of the <see cref="CancelCommand" />.
        /// </summary>
        private Boolean Cancel_CanExecute()
        {
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="CancelCommand" /> is executed.
        /// </summary>
        private void Cancel_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        private void OnSelectedOwnerChanged()
        {
            _availableObjectFieldInfos.Clear();
            foreach (var fieldInfo in SelectedOwner.ObjectFieldInfos)
            {
                _availableObjectFieldInfos.Add(fieldInfo);
            }
            OnPropertyChanged(AvailableObjectFieldInfosProperty);
        }

        #endregion

        #region Static Helpers

        /// <summary>
        ///     Helper method to get the property path for the <see cref="ObjectFieldInfoSelectorViewModel" /> property indicated
        ///     by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(
            Expression<Func<ObjectFieldInfoSelectorViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        private static List<OwnerRowItem> InitializeAvailableOwners()
        {
            return new List<OwnerRowItem>
            {
                new OwnerRowItem(null, "All"),
                new OwnerRowItem(typeof(Planogram), Planogram.FriendlyName),
                new OwnerRowItem(typeof(PlanogramProduct), PlanogramProduct.FriendlyName),
                new OwnerRowItem(typeof(PlanogramPosition), PlanogramPosition.FriendlyName),
                new OwnerRowItem(typeof(PlanogramSubComponent), PlanogramSubComponent.FriendlyName),
                new OwnerRowItem(typeof(PlanogramComponent), PlanogramComponent.FriendlyName),
                new OwnerRowItem(typeof(PlanogramFixture), PlanogramFixture.FriendlyName)
            };
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                // Dispose resources here.
            }

            IsDisposed = true;
        }

        #endregion

        #region Nested Class: OwnerRowItem

        /// <summary>
        ///     Wrapper around the <see cref="Type" /> of Owner for an <see cref="ObjectFieldInfo" /> instance.
        /// </summary>
        public class OwnerRowItem
        {
            private readonly String _caption;
            private readonly Type _source;
            private ReadOnlyCollection<ObjectFieldInfo> _objectFieldInfos;

            #region Constructor

            /// <summary>
            ///     Initializes a new instance of <see cref="OwnerRowItem" /> from the given <paramref name="source" />
            ///     <see cref="ObjectFieldInfo" />.
            /// </summary>
            public OwnerRowItem(Type source, String caption)
            {
                _source = source;
                _caption = caption;
            }

            public IEnumerable<ObjectFieldInfo> ObjectFieldInfos
            {
                get { return _objectFieldInfos ?? (_objectFieldInfos = GetObjectFieldInfosFor(_source)); }
            }

            #endregion

            #region Static Helper Methods

            private static ReadOnlyCollection<ObjectFieldInfo> GetObjectFieldInfosFor(Type source)
            {
                return new ReadOnlyCollection<ObjectFieldInfo>(CommonHelper.EnumerateObjectFieldInfos(source).ToList());
            }

            #endregion

            /// <summary>
            ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
            /// </summary>
            /// <returns>
            ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
            /// </returns>
            public override string ToString()
            {
                return _caption;
            }
        }

        #endregion
    }
}