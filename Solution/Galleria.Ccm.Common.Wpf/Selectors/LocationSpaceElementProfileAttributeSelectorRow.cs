﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Represents a row in the Shelf Attribute Selector window, encapsulating attribute properties.
    /// </summary>
    public class LocationSpaceElementAttributeSelectorRow : IEquatable<LocationSpaceElementAttributeSelectorRow>
    {

        #region Properties

        /// <summary>
        /// The row Header, representing the friendly name of an attribute,
        /// </summary>
        public String Header { get; private set; }

        /// <summary>
        /// The row Header Group, representing the group that this attribute belongs to.
        /// </summary>
        public String HeaderGroup { get; private set; }

        /// <summary>
        /// The actual property name that this attribute refers to.
        /// </summary>
        public String PropertyName { get; private set; }

        /// <summary>
        /// The default value for this row.
        /// </summary>
        public Object DefaultValue { get; set; }

        /// <summary>
        /// The collection containing UOM details that should be applied to this row.
        /// </summary>
        public DisplayUnitOfMeasureCollection UOMCollectionForDefaultValue { get; set; }

        /// <summary>
        /// Whether or not to apply default values.
        /// </summary>
        public Boolean IsDefaultValueApplied { get; set; }

        #endregion

        #region Constants

        //Consts for type names
        const String cString = "String";

        const String cDatetime = "Nullable`1";

        const String cBoolean = "Boolean";

        const String cByte = "Byte";
        const String cSingle = "Single";
        const String cInt16 = "Int16";
        const String cInt32 = "Int32";
        const String cInt64 = "Int64";


        const String cLocationSpaceElementMerchandisingStyle = "LocationSpaceElementMerchandisingStyle";
        const String cFillColour = "FillColour";
        const String cFillPatternType = "FillPatternType";
        const String cShapeType = "ShapeType";
        const String cLocationSpaceElementOrientationType = "LocationSpaceElementOrientationType";
        const String cLocationSpaceElementStatusType = "LocationSpaceElementStatusType";
        const String cLocationSpaceElementShapeType = "LocationSpaceElementShapeType";
        const String cLocationSpaceElementFillPatternType = "LocationSpaceElementFillPatternType";

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new row from using the values in the given object field info.
        /// </summary>
        /// <param name="fieldInfo"></param>
        internal LocationSpaceElementAttributeSelectorRow(ObjectFieldInfo fieldInfo)
        {
            Header = fieldInfo.FieldFriendlyName;
            HeaderGroup = fieldInfo.GroupName;
            PropertyName = fieldInfo.PropertyName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// IEquatable implementation, for object comparisons.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        /// <remarks>Doesn't take into account Default Value and IsDefaultValue</remarks>
        public Boolean Equals(LocationSpaceElementAttributeSelectorRow other)
        {
            return
                other.Header.Equals(this.Header) &&
                other.HeaderGroup.Equals(this.HeaderGroup) &&
                other.PropertyName.Equals(this.PropertyName); 
        }


        /// <summary>
        /// Summary creates a new row of the appropriate object type. 
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <returns></returns>
        /// <remarks>Object type is important for making decisons on data templating withing ui logic.</remarks>
        public static LocationSpaceElementAttributeSelectorRow CreateNewRow(ObjectFieldInfo fieldInfo)
        {
            String propertyFullName = fieldInfo.PropertyType.FullName;

            //Colour is a special case as it is an int but we need to handle with colour picker.
            if (fieldInfo.PropertyName.ToLowerInvariant().Contains("colour") || fieldInfo.PropertyName.ToLowerInvariant().Contains("color"))
            {
                return new LocationSpaceElementAttributeSelectorRowColour(fieldInfo);
            }

            //Handle based on the properties fully qualified name (should handle nullable types too). 
            if (propertyFullName.Contains(cString)) return new LocationSpaceElementAttributeSelectorRowString(fieldInfo);
            if (propertyFullName.Contains(cDatetime)) return new LocationSpaceElementAttributeSelectorRowDate(fieldInfo);
            if (propertyFullName.Contains(cBoolean)) return new LocationSpaceElementAttributeSelectorRowCheckBox(fieldInfo);
            if (propertyFullName.Contains(cSingle)) return new LocationSpaceElementAttributeSelectorRowDecimal(fieldInfo);
            if (propertyFullName.Contains(cInt16)) return new LocationSpaceElementAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cInt32)) return new LocationSpaceElementAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cInt64)) return new LocationSpaceElementAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cByte)) return new LocationSpaceElementAttributeSelectorRowInteger(fieldInfo);
            if (propertyFullName.Contains(cFillColour)) return new LocationSpaceElementAttributeSelectorRowColour(fieldInfo);
            if (propertyFullName.Contains(cLocationSpaceElementMerchandisingStyle)) return new LocationSpaceElementAttributeSelectorRowMerchStyle(fieldInfo);
            if (propertyFullName.Contains(cLocationSpaceElementStatusType)) return new LocationSpaceElementAttributeSelectorRowShelfStatusType(fieldInfo);
            if (propertyFullName.Contains(cLocationSpaceElementOrientationType)) return new LocationSpaceElementAttributeSelectorRowShelfOrientationType(fieldInfo);
            if (propertyFullName.Contains(cFillPatternType)) return new LocationSpaceElementAttributeSelectorRowFillPatternType(fieldInfo);
            if (propertyFullName.Contains(cLocationSpaceElementFillPatternType)) return new LocationSpaceElementAttributeSelectorRowFillPatternType(fieldInfo);
            if (propertyFullName.Contains(cShapeType)) return new LocationSpaceElementAttributeSelectorRowShapeType(fieldInfo);
            if (propertyFullName.Contains(cLocationSpaceElementShapeType)) return new LocationSpaceElementAttributeSelectorRowShapeType(fieldInfo);

            return new LocationSpaceElementAttributeSelectorRow(fieldInfo);
        }
    }
    #endregion

    #region Subclasses

    //These subclasses are important for their type, and help the xaml select approprtiate data templates.

    public class LocationSpaceElementAttributeSelectorRowString : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowString(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = String.Empty;
        }
        
    }

    public class LocationSpaceElementAttributeSelectorRowDate : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowDate(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = DateTime.Now;
        }
    }

    public class LocationSpaceElementAttributeSelectorRowCheckBox : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowCheckBox(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
            this.DefaultValue = false;
        }
    }

    public class LocationSpaceElementAttributeSelectorRowInteger : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowInteger(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class LocationSpaceElementAttributeSelectorRowDecimal : LocationSpaceElementAttributeSelectorRow
    {
        #region Properties
       
        #endregion

        #region Constructor

        internal LocationSpaceElementAttributeSelectorRowDecimal(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {
        }

        #endregion
    }

    public class LocationSpaceElementAttributeSelectorRowColour : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowColour(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class LocationSpaceElementAttributeSelectorRowMerchStyle : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowMerchStyle(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class LocationSpaceElementAttributeSelectorRowShapeType : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowShapeType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class LocationSpaceElementAttributeSelectorRowShelfStatusType : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowShelfStatusType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    public class LocationSpaceElementAttributeSelectorRowShelfShapeType : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowShelfShapeType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }
    
    public class LocationSpaceElementAttributeSelectorRowFillPatternType : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowFillPatternType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }

        #region LocationSpaceElementFillPatternBrushes

        public static Dictionary<LocationSpaceElementFillPatternType, Brush> LocationSpaceElementFillPatternBrushes
        {
            get
            {
                return new Dictionary<LocationSpaceElementFillPatternType, Brush>()
                    {
                        {LocationSpaceElementFillPatternType.Solid,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Solid, 5)},

                         {LocationSpaceElementFillPatternType.Border,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Border, 5)},

                         {LocationSpaceElementFillPatternType.Crosshatch,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Crosshatch, 5)},

                          {LocationSpaceElementFillPatternType.DiagonalDown,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalDown, 5)},

                            {LocationSpaceElementFillPatternType.DiagonalUp,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalUp, 5)},

                            {LocationSpaceElementFillPatternType.Dotted,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Dotted, 5)},

                            {LocationSpaceElementFillPatternType.Horizontal,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Horizontal, 5)},

                            {LocationSpaceElementFillPatternType.Vertical,
                            WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Vertical, 5)},
                    };
            }
        }
        #endregion
    }
    
    public class LocationSpaceElementAttributeSelectorRowShelfOrientationType : LocationSpaceElementAttributeSelectorRow
    {
        internal LocationSpaceElementAttributeSelectorRowShelfOrientationType(ObjectFieldInfo fieldInfo)
            : base(fieldInfo)
        {

        }
    }

    #endregion
}



