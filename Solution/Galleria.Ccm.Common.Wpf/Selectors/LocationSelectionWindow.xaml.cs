﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#region Version History : CCM 802
// V8-28987 : I.George
//  Moved screen to common.Wpf and added a constructor that takes a selectionmode  
// V8-29078 : A.Kuszyk
//  Added constructor that accepts a view model as an argument.
#endregion

#region Version History : (CCM 8.3.0)
// V8-32245 : N.Haywood
//  Added up/down selection
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for LocationSelectionWindow.xaml
    /// </summary>
    public partial class LocationSelectionWindow : ExtendedRibbonWindow
    {       
        #region Fields
        //added to cater for Key Down event when LocationSelectionWindow has no SelectedLocationGrid
        private Boolean? _hasSelectedLocationGrid;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationSelectionViewModel), typeof(LocationSelectionWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationSelectionViewModel ViewModel
        {
            get { return (LocationSelectionViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSelectionWindow senderControl = (LocationSelectionWindow)obj;

            if (e.OldValue != null)
            {
                LocationSelectionViewModel oldModel = (LocationSelectionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationSelectionViewModel newModel = (LocationSelectionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }



        #endregion

        public Boolean? HasSelectedLocationGrid
        {
            get { return _hasSelectedLocationGrid; }
            set { _hasSelectedLocationGrid = value; }
        }

        #endregion

        #region Constructor

        public LocationSelectionWindow(LocationSelectionViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;

            this.Loaded += LocationSelectionWindow_Loaded;
            HasSelectedLocationGrid = false;
        }

        public LocationSelectionWindow(ObservableCollection<LocationInfo> availableLocations, ObservableCollection<LocationInfo> takenLocations, LocationHierarchy locationHierarchy)
        {
            InitializeComponent();


            //Create view model
            this.ViewModel = new LocationSelectionViewModel(availableLocations, takenLocations, locationHierarchy, DataGridSelectionMode.Extended);

            this.Loaded -= LocationSelectionWindow_Loaded;
        }

        public LocationSelectionWindow(ObservableCollection<LocationInfo> availableLocations, ObservableCollection<LocationInfo> takenLocations, LocationHierarchy locationHierarchy, DataGridSelectionMode selectionMode)
        {
            InitializeComponent();

            
            //Create view model
            this.ViewModel = new LocationSelectionViewModel(availableLocations, takenLocations, locationHierarchy, selectionMode);

            this.Loaded += LocationSelectionWindow_Loaded;
        }

        private void LocationSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSelectionWindow_Loaded;

            if (this.ViewModel.SelectionMode == DataGridSelectionMode.Single)
            {
                this.xAvailableLocationsGrid.SelectionMode = DataGridSelectionMode.Single;
                this.xAvailableLocationsGrid.CanUserDragRows = false;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Performs a RemoveLocationsCommand for rows dropped on the available locations grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xSelectedLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveLocationsCommand.Execute();
                }

            }
             
        }

        /// <summary>
        /// Performs an AddLocationsCommand for rows dropped selected locations grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xAvailableLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddLocationsCommand.Execute();
                }
            }
        }

        ///// <summary>
        ///// When a location is selected
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void xAvailableLocationsGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (this.ViewModel.SelectionMode == DataGridSelectionMode.Single)
        //    {
        //        this.xAvailableLocationsGrid.SelectionMode = DataGridSelectionMode.Single;
        //        this.xAvailableLocationsGrid.CanUserDragRows = false;
        //    }
        //}

        private void xAvailableLocationsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel.SelectionMode == DataGridSelectionMode.Single)
            {
                //set category code and name
                this.ViewModel.DialogResult = true;
            }
        }

        private void TakeButtonsFromClipboard_Click(object sender, RoutedEventArgs e)
        {
            String locationsString = Clipboard.GetData(DataFormats.Text) as String;

            if (locationsString != null)
            {
                String[] data = locationsString.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                this.ViewModel.SelectedAvailableLocations.Clear();
                LocationInfoRowViewModel loc;
                foreach (String location in data)
                {
                    String[] subData = location.Split(new string[] { "\t" }, StringSplitOptions.None);
                    //find the location
                    loc = this.ViewModel.AvailableLocations.FirstOrDefault(l => l.Location.Code.Equals(subData[0]));

                    if (loc != null && !this.ViewModel.SelectedAvailableLocations.Contains(loc))
                    {
                        this.ViewModel.SelectedAvailableLocations.Add(loc);
                    }
                }

                this.ViewModel.AddLocationsCommand.Execute();
            }
        }

        /// <summary>
        /// Imitates Mouse Double Click event on Enter key pressed for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && this.ViewModel.SelectionMode == DataGridSelectionMode.Single)
            {
                e.Handled = true;
                //set category code and name
                this.ViewModel.DialogResult = true;
            }
            
            else
            {
                return;
            }
        }

        /// <summary>
        /// This closes the window when escape key is pressed to mimic functionality of
        /// MerchGroupSelectionWindow and improved usage for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rootControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void XAvailableLocationsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (HasSelectedLocationGrid == false)
                {
                    this.ViewModel.OKCommand.Execute();
                    e.Handled = true;
                    return;
                }
                this.ViewModel.AddLocationsCommand.Execute();
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xSelectedLocationsGrid);
            }
        }

        private void XSelectedLocationsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ViewModel.RemoveLocationsCommand.Execute();
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xAvailableLocationsGrid);
            }
        }
        #endregion

        #region Methods

        #region Window Close

        public Boolean IsClosing
        {
            get;
            private set;
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //Close window
            this.DialogResult = false;
        }

        // Now uses a command
        //private void OkButton_Click(object sender, RoutedEventArgs e)
        //{
        //    //Close window
        //    this.DialogResult = true;
        //}

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #endregion          
    }
}

