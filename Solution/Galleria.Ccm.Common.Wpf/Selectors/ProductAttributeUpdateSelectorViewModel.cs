﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32560 : A.Silva
//  Created.
// V8-32967 : A.Silva
//  Amended Constructor to correctly update new values for Enum types.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    ///     A view model representing a selection of Product Attributes and new values to update them.
    /// </summary>
    public class ProductAttributeUpdateSelectorViewModel : ViewModelObject
    {
        #region Properties

        #region Available Attributes

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="AvailableAttributes"/> property.
        /// </summary>
        public static readonly PropertyPath AvailableAttributesProperty = GetPropertyPath(p => p.AvailableAttributes);

        /// <summary>
        ///     The collection of <see cref="ProductAttributeUpdateValue"/> items available for selection.
        /// </summary>
        private readonly BulkObservableCollection<ProductAttributeUpdateValue> _availableAttributes =
            new BulkObservableCollection<ProductAttributeUpdateValue>();

        /// <summary>
        ///     Get the collection of <see cref="ProductAttributeUpdateValue"/> items available for selection.
        /// </summary>
        public BulkObservableCollection<ProductAttributeUpdateValue> AvailableAttributes { get { return _availableAttributes; } }

        #endregion

        #region Selected Attributes

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="SelectedAttributes"/> property.
        /// </summary>
        public static readonly PropertyPath SelectedAttributesProperty = GetPropertyPath(p => p.SelectedAttributes);

        /// <summary>
        ///     The collection of <see cref="ProductAttributeUpdateValue"/> items selected to be updated.
        /// </summary>
        private readonly BulkObservableCollection<ProductAttributeUpdateValue> _selectedAttributes =
            new BulkObservableCollection<ProductAttributeUpdateValue>();

        /// <summary>
        ///     Get the collection of <see cref="ProductAttributeUpdateValue"/> items selected to be updated.
        /// </summary>
        public BulkObservableCollection<ProductAttributeUpdateValue> SelectedAttributes { get { return _selectedAttributes; } }

        #endregion

        #region Available Attributes Selection

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="AvailableAttributesSelection"/> property.
        /// </summary>
        public static readonly PropertyPath AvailableAttributesSelectionProperty = GetPropertyPath(p => p.AvailableAttributesSelection);

        /// <summary>
        ///     The collection of available <see cref="ProductAttributeUpdateValue"/> items currently selected by the user on the UI.
        /// </summary>
        private readonly BulkObservableCollection<ProductAttributeUpdateValue> _availableAttributesSelection =
            new BulkObservableCollection<ProductAttributeUpdateValue>();

        /// <summary>
        ///     Get the collection of available <see cref="ProductAttributeUpdateValue"/> items currently selected by the user on the UI.
        /// </summary>
        public BulkObservableCollection<ProductAttributeUpdateValue> AvailableAttributesSelection { get { return _availableAttributesSelection; } }

        #endregion

        #region Selected Attributes Selection

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="SelectedAttributesSelection"/> property.
        /// </summary>
        public static readonly PropertyPath SelectedAttributesSelectionProperty = GetPropertyPath(p => p.SelectedAttributesSelection);

        /// <summary>
        ///     The collection of selected <see cref="ProductAttributeUpdateValue"/> items currently selected by the user on the UI.
        /// </summary>
        private readonly BulkObservableCollection<ProductAttributeUpdateValue> _selectedAttributesSelection =
            new BulkObservableCollection<ProductAttributeUpdateValue>();

        /// <summary>
        ///     Get the collection of selected <see cref="ProductAttributeUpdateValue"/> items currently selected by the user on the UI.
        /// </summary>
        public BulkObservableCollection<ProductAttributeUpdateValue> SelectedAttributesSelection { get { return _selectedAttributesSelection; } }

        #endregion

        #region Dialog Result

        /// <summary>
        ///     Whether the user wants the dialog to be accepted or canceled.
        /// </summary>
        private Boolean? _dialogResult;

        /// <summary>
        ///     Get or set whether the user wants the dialog accepted or canceled.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                if (!_dialogResult.HasValue) return;

                // Fire the dialog result set event. This should be subscribed to by the view
                // so that it can respond appropriately.
                if (DialogResultSet != null) DialogResultSet(this, EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="ProductAttributeUpdateSelectorViewModel"/>.
        /// </summary>
        public ProductAttributeUpdateSelectorViewModel()
        {
            AvailableAttributes.AddRange(PlanogramProduct.GetAvailableProductAttributeUpdateValues());
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="ProductAttributeUpdateSelectorViewModel"/> and preselects the given <paramref name="presetValues"/>.
        /// </summary>
        public ProductAttributeUpdateSelectorViewModel(List<Tuple<String, Object>> presetValues)
            : this()
        {
            if (presetValues == null) return;
            Dictionary<String, Object> valuePerPropertyName = presetValues.ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2);
            List<ProductAttributeUpdateValue> values = AvailableAttributes.Where(value => valuePerPropertyName.ContainsKey(value.PropertyName)).ToList();
            foreach (ProductAttributeUpdateValue value in values)
            {
                value.NewValue = valuePerPropertyName[value.PropertyName];
                if (value.PropertyType.IsEnum)
                    value.NewValue = Enum.Parse(value.PropertyType, value.NewValue.ToString());
            }
            AvailableAttributesSelection.AddRange(values);
            AddSelectedAttributesCommand.Execute();
        }

        #endregion

        #region Events

        /// <summary>
        ///     Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        #endregion

        #region Commands

        #region OkCommand

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="OkCommand"/> property.
        /// </summary>
        public static readonly PropertyPath OkCommandProperty = GetPropertyPath(p => p.OkCommand);

        /// <summary>
        ///     The command to accept the dialog and close it.
        /// </summary>
        private RelayCommand _okCommand;

        /// <summary>
        ///     Gets the command to accept the dialog and close it.
        /// </summary>
        /// <remarks>[Lazy] This command is created when required for the first time.</remarks>
        public RelayCommand OkCommand
        {
            get
            {
                return _okCommand ?? (_okCommand =
                                      new RelayCommand(p => OkCommand_Executed(), p => OkCommand_CanExecute())
                                      {
                                          FriendlyName = Message.Generic_Ok,
                                          DisabledReason = Message.ProductAttributeSelector_Ok_DisabledReason
                                      });
            }
        }

        /// <summary>
        ///     Invoked whenever checking if the <see cref="OkCommand"/> can be executed.
        /// </summary>
        /// <returns></returns>
        private static Boolean OkCommand_CanExecute()
        {
            // V8-28944
            // Ok should always be enabled.
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="OkCommand"/> is executed.
        /// </summary>
        private void OkCommand_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region CancelCommand

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="CancelCommand"/> property.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(p => p.CancelCommand);

        /// <summary>
        ///     The command to cancel the dialog and close it.
        /// </summary>
        private RelayCommand _cancelCommand;

        /// <summary>
        ///     Get the command to cancel the dialog and close it.
        /// </summary>
        /// <remarks>[Lazy] This command is created when required for the first time.</remarks>
        public RelayCommand CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand =
                                          new RelayCommand(p => CancelCommand_Executed())
                                          {
                                              FriendlyName = Message.Generic_Cancel
                                          });
            }
        }

        /// <summary>
        ///     Invoked whenever the <see cref="CancelCommand"/> is executed.
        /// </summary>
        private void CancelCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #region AddSelectedAttributesCommand

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="AddSelectedAttributesCommand"/> property.
        /// </summary>
        public static readonly PropertyPath AddSelectedAttributesCommandProperty = GetPropertyPath(p => p.AddSelectedAttributesCommand);

        /// <summary>
        ///     The command to move the items in <see cref="AvailableAttributesSelection"/> to <see cref="SelectedAttributes"/>.
        /// </summary>
        private RelayCommand _addSelectedAttributesCommand;

        /// <summary>
        ///     Get the command to move the items in <see cref="AvailableAttributesSelection"/> to <see cref="SelectedAttributes"/>.
        /// </summary>
        /// <remarks>[Lazy] This command is created when required for the first time.</remarks>
        public RelayCommand AddSelectedAttributesCommand
        {
            get
            {
                return _addSelectedAttributesCommand ?? (_addSelectedAttributesCommand =
                                                         new RelayCommand(p => AddSelectedAttributes_Executed(),
                                                                          p => AddSelectedAttributes_CanExecute())
                                                         {
                                                             FriendlyDescription = Message.ProductAttributeSelector_AddSelectedAttribute,
                                                             SmallIcon = ImageResources.Add_16,
                                                             DisabledReason = Message.ProductAttributeSelector_AddSelectedAttribute_DisabledReason
                                                         });
            }
        }

        /// <summary>
        ///     Invoked whenever checking if the <see cref="AddSelectedAttributesCommand"/> can be executed.
        /// </summary>
        /// <returns></returns>
        private Boolean AddSelectedAttributes_CanExecute()
        {
            return AvailableAttributesSelection.Count > 0;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="AddSelectedAttributesCommand"/> is executed.
        /// </summary>
        private void AddSelectedAttributes_Executed()
        {
            List<ProductAttributeUpdateValue> itemsToMove = AvailableAttributesSelection.Where(item => !SelectedAttributes.Contains(item)).ToList();
            AvailableAttributes.RemoveRange(itemsToMove);
            SelectedAttributes.AddRange(itemsToMove);
            AvailableAttributesSelection.Clear();
        }

        #endregion

        #region RemoveSelectedAttributesCommand

        /// <summary>
        ///     The <see cref="PropertyPath"/> containing binding information for the <see cref="RemoveSelectedAttributesCommand"/> property.
        /// </summary>
        public static readonly PropertyPath RemoveSelectedAttributesCommandProperty = GetPropertyPath(p => p.RemoveSelectedAttributesCommand);

        /// <summary>
        ///     The command to move the items in <see cref="SelectedAttributesSelection"/> to <see cref="AvailableAttributes"/>.
        /// </summary>
        private RelayCommand _removeSelectedAttributesCommand;

        /// <summary>
        ///     Get the command to move the items in <see cref="SelectedAttributesSelection"/> to <see cref="AvailableAttributes"/>.
        /// </summary>
        public RelayCommand RemoveSelectedAttributesCommand
        {
            get
            {
                return _removeSelectedAttributesCommand ?? (_removeSelectedAttributesCommand =
                                                            new RelayCommand(p => RemoveSelectedAttributes_Executed(),
                                                                             p => RemoveSelectedAttributes_CanExecute())
                                                            {
                                                                FriendlyDescription = Message.ProductAttributeSelector_RemoveSelectedAttributes,
                                                                SmallIcon = ImageResources.Delete_16,
                                                                DisabledReason =
                                                                    Message.ProductAttributeSelector_RemoveSelectedAttributes_DisabledReason
                                                            });
            }
        }

        /// <summary>
        ///     Invoked whenever checking if the <see cref="RemoveSelectedAttributesCommand"/> can be executed.
        /// </summary>
        /// <returns></returns>
        private Boolean RemoveSelectedAttributes_CanExecute()
        {
            return SelectedAttributesSelection.Count > 0;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="RemoveSelectedAttributesCommand"/> is executed.
        /// </summary>
        private void RemoveSelectedAttributes_Executed()
        {
            List<ProductAttributeUpdateValue> itemsToMove = SelectedAttributesSelection.Where(item => !AvailableAttributes.Contains(item)).ToList();
            SelectedAttributes.RemoveRange(itemsToMove);
            AvailableAttributes.AddRange(itemsToMove);
            if (SelectedAttributesSelection.Any()) SelectedAttributesSelection.Clear();
        }

        #endregion

        #endregion

        #region Methods

        protected override void Dispose(Boolean disposing)
        {
            // Nothing to dispose.
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="ProductAttributeUpdateSelectorViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<ProductAttributeUpdateSelectorViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }
}