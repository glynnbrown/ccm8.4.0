﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Resources;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// A view model for the LocationSpaceElementAttributeSelector view.
    /// </summary>
    public class LocationSpaceElementAttributeSelectorViewModel : ViewModelObject
    {
        #region Fields

        Boolean? _dialogResult;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableAttributesProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.AvailableAttributes);
        public static readonly PropertyPath SelectedAttributesProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.SelectedAttributes);
        public static readonly PropertyPath AvailableAttributesSelectionProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.AvailableAttributesSelection);
        public static readonly PropertyPath AddSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.AddSelectedAttributesCommand);
        public static readonly PropertyPath RemoveSelectedAttributesCommandProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.RemoveSelectedAttributesCommand);
        public static readonly PropertyPath SelectedAttributesSelectionProperty = 
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.SelectedAttributesSelection);
        public static readonly PropertyPath OkCommandProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<LocationSpaceElementAttributeSelectorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The attributes that are available for selection.
        /// </summary>
        public BulkObservableCollection<LocationSpaceElementAttributeSelectorRow> AvailableAttributes { get; set; }

        /// <summary>
        /// The attributes that have been selected.
        /// </summary>
        public BulkObservableCollection<LocationSpaceElementAttributeSelectorRow> SelectedAttributes { get; set; }

        /// <summary>
        /// The available attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<LocationSpaceElementAttributeSelectorRow> AvailableAttributesSelection { get; set; }

        /// <summary>
        /// The selected attributes that have been selected in the UI.
        /// </summary>
        public BulkObservableCollection<LocationSpaceElementAttributeSelectorRow> SelectedAttributesSelection { get; set; }

        /// <summary>
        /// Indicates whether or not the user has clicked Ok or cancel.
        /// </summary>
        public Boolean? DialogResult 
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (_dialogResult.HasValue)
                {
                    // Fire the dialog result set event. This should be subscribed to by the view
                    // so that it can respond appropriately.
                    if(DialogResultSet != null) DialogResultSet(this,EventArgs.Empty);
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Instantiates a new view model.
        /// </summary>
        /// <param name="currentSelection">The current attributes selection, if any, that should be pre-selected. Can be null.</param>
        /// <param name="includeProperties">A collection of properties to display, arrange as a list of tuples containing the source type and property name.</param>
        /// <remarks>When using this view model, the view should subscribe to the DialogResultSet event.</remarks>
        public LocationSpaceElementAttributeSelectorViewModel(IEnumerable<Tuple<String, Object, Object>> currentSelection = null, ICollection<Tuple<String, Object>> includeProperties = null)
        {
            // Populate the available attributes with those from Planogram Product inlcuding custom and performance properties.
            AvailableAttributes = new BulkObservableCollection<LocationSpaceElementAttributeSelectorRow>();
            AvailableAttributes.AddRange(LocationSpaceElement.EnumerateDisplayableFieldInfos(true).Select(LocationSpaceElementAttributeSelectorRow.CreateNewRow));
            
            // Create empty collections for the selection properties.
            SelectedAttributes = new BulkObservableCollection<LocationSpaceElementAttributeSelectorRow>();
            AvailableAttributesSelection = new BulkObservableCollection<LocationSpaceElementAttributeSelectorRow>();
            SelectedAttributesSelection = new BulkObservableCollection<LocationSpaceElementAttributeSelectorRow>();

            // If attributes have been specified for inclusion, then we remove all that don't match
            // (otherwise, all attributes are left in).
            if (includeProperties != null && includeProperties.Any())
            {
                List<LocationSpaceElementAttributeSelectorRow> includeAttributes = new List<LocationSpaceElementAttributeSelectorRow>();
                foreach (Tuple<String, Object> sourceAndProperty in includeProperties)
                {
                    includeAttributes.AddRange(AvailableAttributes.Where(a => a.PropertyName.Equals(sourceAndProperty.Item2)));
                }
                AvailableAttributes.RemoveRange(AvailableAttributes.Except(includeAttributes.Distinct()).ToList());
            }

            // Finally, for each item in the current selection, move these items from the available
            // to the selected list.
            if (currentSelection != null)
            {
                List<LocationSpaceElementAttributeSelectorRow> currentAttributeSelection = new List<LocationSpaceElementAttributeSelectorRow>();
                foreach (Tuple<String, Object, Object> sourceAndProperty in currentSelection)
                {
                    currentAttributeSelection.AddRange(AvailableAttributes.Where(a => a.PropertyName.Equals(sourceAndProperty.Item1)));
                }
                IEnumerable<LocationSpaceElementAttributeSelectorRow> distinctCurrentAttributeSelection = currentAttributeSelection.Distinct().ToList();
                SelectedAttributes.AddRange(distinctCurrentAttributeSelection);
                AvailableAttributes.RemoveRange(distinctCurrentAttributeSelection);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the view model's Dialog Result is set, indicating that the user has clicked the ok or cancel buttons.
        /// </summary>
        public event EventHandler DialogResultSet;

        #endregion

        #region Commands

        #region OkCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs the selection and closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OkCommand_Executed(),
                        p => OkCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        DisabledReason = Message.LocationSpaceElementAttributeSelector_Ok_DisabledReason
                    };
                }
                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            // V8-28944
            // Ok should always be enabled.
            return true;
        }

        private void OkCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the selection and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedAttributesCommand

        private RelayCommand _addSelectedAttributesCommand;

        /// <summary>
        ///     Adds any selected available Attributes to the current view Attribute list
        /// </summary>
        public RelayCommand AddSelectedAttributesCommand
        {
            get
            {
                if (_addSelectedAttributesCommand != null) return _addSelectedAttributesCommand;

                _addSelectedAttributesCommand = new RelayCommand(
                    p => AddSelectedAttributes_Executed(),
                    p => AddSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.LocationSpaceElementAttributeSelector_AddSelectedAttribute,
                    SmallIcon = ImageResources.Add_16,
                    DisabledReason = Message.LocationSpaceElementAttributeSelector_AddSelectedAttribute_DisabledReason 
                };

                return _addSelectedAttributesCommand;
            }
        }

        private Boolean AddSelectedAttributes_CanExecute()
        {
            return AvailableAttributesSelection.Count > 0;
        }

        private void AddSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<LocationSpaceElementAttributeSelectorRow> rowsToAdd = new List<LocationSpaceElementAttributeSelectorRow>();
            foreach (LocationSpaceElementAttributeSelectorRow row in AvailableAttributesSelection)
            {
                if (!SelectedAttributes.Contains(row)) rowsToAdd.Add(row);
            }

            try
            {
                AvailableAttributes.RemoveRange(rowsToAdd);
                SelectedAttributes.AddRange(rowsToAdd);
                if (AvailableAttributesSelection.Any())
                {
                    AvailableAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #region RemoveSelectedAttributesCommand

        private RelayCommand _removeSelectedAttributesCommand;

        /// <summary>
        ///     Remove any selected available Attributes from the current view Attribute list
        /// </summary>
        public RelayCommand RemoveSelectedAttributesCommand
        {
            get
            {
                if (_removeSelectedAttributesCommand != null) return _removeSelectedAttributesCommand;

                _removeSelectedAttributesCommand = new RelayCommand(
                    p => RemoveSelectedAttributes_Executed(),
                    p => RemoveSelectedAttributes_CanExecute())
                {
                    FriendlyDescription = Message.LocationSpaceElementAttributeSelector_RemoveSelectedAttributes, 
                    SmallIcon = ImageResources.Delete_16,
                    DisabledReason = Message.LocationSpaceElementAttributeSelector_RemoveSelectedAttributes_DisabledReason 
                };

                return _removeSelectedAttributesCommand;
            }
        }

        private Boolean RemoveSelectedAttributes_CanExecute()
        {
            return SelectedAttributesSelection.Count > 0;
        }

        private void RemoveSelectedAttributes_Executed()
        {
            // Build collection of attributes to add.
            List<LocationSpaceElementAttributeSelectorRow> rowsToAdd = new List<LocationSpaceElementAttributeSelectorRow>();
            foreach(LocationSpaceElementAttributeSelectorRow row in SelectedAttributesSelection)
            {
                if(!AvailableAttributes.Contains(row)) rowsToAdd.Add(row);
            }
            try
            {
                SelectedAttributes.RemoveRange(rowsToAdd);
                AvailableAttributes.AddRange(rowsToAdd);
                if (SelectedAttributesSelection.Any())
                {
                    SelectedAttributesSelection.Clear();
                }
            }
            catch (InvalidOperationException)
            {
                // This sometimes seems to be thrown from the data grid.
                // If it does, ignore it and carry on.
            }
        }

        #endregion

        #endregion

        #region Methods
        protected override void Dispose(bool disposing)
        {
            // Nothing to dispose.
        }
        #endregion
    }
}
