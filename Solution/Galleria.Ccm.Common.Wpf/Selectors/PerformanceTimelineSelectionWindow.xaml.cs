﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31830 : A.Probyn ~ Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Common.Wpf.Selectors
{
    /// <summary>
    /// Interaction logic for PerformanceTimelineSelectionWindoww.xaml
    /// </summary>
    public sealed partial class PerformanceTimelineSelectionWindow
    {
        #region Properties
        
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PerformanceTimelineSelectionViewModel), typeof(PerformanceTimelineSelectionWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public PerformanceTimelineSelectionViewModel ViewModel
        {
            get { return (PerformanceTimelineSelectionViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceTimelineSelectionWindow senderControl = (PerformanceTimelineSelectionWindow)obj;

            if (e.OldValue != null)
            {
                PerformanceTimelineSelectionViewModel oldModel = (PerformanceTimelineSelectionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PerformanceTimelineSelectionViewModel newModel = (PerformanceTimelineSelectionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public PerformanceTimelineSelectionWindow(PerformanceTimelineSelectionViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            
            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(PerformanceTimelineSelectionWindow_Loaded);
        }
        #endregion

        #region Events

        private void PerformanceTimelineSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PerformanceTimelineSelectionWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion


    }

}
