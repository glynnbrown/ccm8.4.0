﻿/*
* This query displays the status
* of all planograms in the solution
*/
SELECT
	CASE [PlanogramProcessingStatus_AutomationStatus]
		WHEN 0 THEN 'Automation Pending'
		WHEN 1 THEN 'Automation Queued'
		WHEN 2 THEN 'Automation Processing'
		WHEN 3 THEN 'Automation Complete'
		WHEN 4 THEN 'Automation Failed'
	END AS [Planogram Status],
	COUNT(*) AS [Planogram Count]
FROM
	[Content].[PlanogramProcessingStatus] WITH (NOLOCK)
GROUP BY
	[PlanogramProcessingStatus_AutomationStatus]
UNION ALL
SELECT
	CASE WHEN [Planogram_DateMetadataCalculated] IS NULL THEN 'Metadata Pending' ELSE 'Metadata Up-to-date' END AS [Planogram Status],
	COUNT(*) AS [Planogram Count]
FROM
	[Content].[Planogram] WITH (NOLOCK)
GROUP BY
	CASE WHEN [Planogram_DateMetadataCalculated] IS NULL THEN 'Metadata Pending' ELSE 'Metadata Up-to-date' END
UNION ALL
SELECT
	CASE WHEN [Planogram_DateValidationDataCalculated] IS NULL THEN 'Validation Pending' ELSE 'Validation Up-to-date' END AS [Planogram Status],
	COUNT(*) AS [Planogram Count]
FROM
	[Content].[Planogram] WITH (NOLOCK)
GROUP BY
	CASE WHEN [Planogram_DateValidationDataCalculated] IS NULL THEN 'Validation Pending' ELSE 'Validation Up-to-date' END
ORDER BY
	[Planogram Status] ASC