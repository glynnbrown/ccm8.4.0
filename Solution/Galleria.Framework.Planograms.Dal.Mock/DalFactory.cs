﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Adapted from ISO's Mock DAL.
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;

using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Dal;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock
{
    public class DalFactory : MockDalFactoryBase
    {
        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new dal context
        /// </summary>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext()
        {
            return new DalContext(_dalCache);
        }
        #endregion
    }
}
