﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Adapted from ISO's Mock DAL.
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock
{
    public class DalContext : MockDalContextBase
    {

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalCache">The dal cache</param>
        internal DalContext(MockDalCache dalCache) : base(dalCache) {}
        #endregion

        protected override IEnumerable<MockDalContextBase.AssemblyAndNamespace> DalImplementationLocations
        {
            get
            {
                yield return new MockDalContextBase.AssemblyAndNamespace(GetType().Assembly, "Galleria.Framework.Planograms.Dal.Mock.Implementation");
            }
        }
    }
}
