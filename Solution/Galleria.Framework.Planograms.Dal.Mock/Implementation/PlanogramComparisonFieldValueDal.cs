﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="IPlanogramComparisonFieldValueDal"/>.
    /// </summary>
    /// <remarks>Instantiated by the DAL factory when required.</remarks>
    public class PlanogramComparisonFieldValueDal : MockDalBase<PlanogramComparisonFieldValueDto>, IPlanogramComparisonFieldValueDal
    {
        #region IPlanogramComparisonFieldValueDal Support

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Field Value</c> model object that belongs to a <c>Planogram Comparison Item</c> with the given<paramref name="id"/>.
        /// </summary>
        /// <param name="id"><see cref="Object"/> containing the id of the parent <c>Planogram Comparison Item</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonFieldDto"/>.</returns>
        public IEnumerable<PlanogramComparisonFieldValueDto> FetchByPlanogramComparisonItemId(Object id)
        {
            return FetchByProperties(new[] { "PlanogramComparisonItemId" }, new[] { id });
        }

        #endregion
    }
}
