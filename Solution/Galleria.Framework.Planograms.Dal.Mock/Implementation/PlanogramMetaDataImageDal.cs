﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27058 : A.Probyn
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;


namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramMetadataImageDal : MockDalBase<PlanogramMetadataImageDto>, IPlanogramMetadataImageDal
    {
        #region IPlanogramMetadataImageDal Members

        public IEnumerable<PlanogramMetadataImageDto> FetchByPlanogramId(Object planogramId)
        {
            return FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }

        #endregion
    }
}
