﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//	Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramSequenceGroupSubGroupDal : MockDalBase<PlanogramSequenceGroupSubGroupDto>, IPlanogramSequenceGroupSubGroupDal
    {
        public IEnumerable<PlanogramSequenceGroupSubGroupDto> FetchByPlanogramSequenceGroupId(object planogramSequenceGroupId)
        {
            return this.FetchByProperties(new[] { "PlanogramSequenceGroupId" }, new[] { planogramSequenceGroupId });
        }
    }
}
