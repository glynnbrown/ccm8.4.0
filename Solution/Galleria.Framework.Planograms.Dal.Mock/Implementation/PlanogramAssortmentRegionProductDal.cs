﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramAssortmentRegionProductDal : MockDalBase<PlanogramAssortmentRegionProductDto>, IPlanogramAssortmentRegionProductDal 
    {
        public IEnumerable<PlanogramAssortmentRegionProductDto> FetchByPlanogramAssortmentRegionId(object id)
        {
            return base.FetchByProperties(new String[] { "PlanogramAssortmentRegionId" }, new Object[] { id });
        }
    }
}
