﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="PlanogramComparisonDal"/>.
    /// </summary>
    /// <remarks>Instantiated by the DAL factory when required.</remarks>
    public class PlanogramComparisonDal : MockDalBase<PlanogramComparisonDto>, IPlanogramComparisonDal
    {
        #region Implementation of IPlanogramComparisonDal

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison</c> model object that belongs to a <c>Planogram</c> with the given<paramref name="id"/>.
        /// </summary>
        /// <param name="id"><see cref="Object"/> containing the id of the parent <c>Planogram</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonDto"/>.</returns>
        public PlanogramComparisonDto FetchByPlanogramId(Object id)
        {
            PlanogramComparisonDto dto = FetchByProperties(new[] {"PlanogramId"}, new[] {id}).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion
    }
}
