﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramPerformanceDal : MockDalBase<PlanogramPerformanceDto>, IPlanogramPerformanceDal
    {
        public PlanogramPerformanceDto FetchByPlanogramId(object id)
        {
            PlanogramPerformanceDto dto = base.FetchByProperties(new String[] { "PlanogramId" }, new Object[] { id }).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }
    }
}
