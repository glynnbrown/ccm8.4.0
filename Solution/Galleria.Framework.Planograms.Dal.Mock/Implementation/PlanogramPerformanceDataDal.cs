﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramPerformanceDataDal : MockDalBase<PlanogramPerformanceDataDto>, IPlanogramPerformanceDataDal
    {
        public IEnumerable<PlanogramPerformanceDataDto> FetchByPlanogramPerformanceId(object id)
        {
            return base.FetchByProperties(new String[] { "PlanogramPerformanceId" }, new Object[] { id });
        }
    }
}
