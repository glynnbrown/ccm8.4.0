﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public sealed class PlanogramRenumberingStrategyDal : MockDalBase<PlanogramRenumberingStrategyDto>, IPlanogramRenumberingStrategyDal
    {
        public PlanogramRenumberingStrategyDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramRenumberingStrategyDto dto = FetchByProperties(new[] { "PlanogramId" }, new[] { planogramId }).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }
    }
}