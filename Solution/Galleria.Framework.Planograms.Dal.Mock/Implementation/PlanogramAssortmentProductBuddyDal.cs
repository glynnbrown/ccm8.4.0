﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramAssortmentProductBuddyDal : MockDalBase<PlanogramAssortmentProductBuddyDto>, IPlanogramAssortmentProductBuddyDal
    {
        public IEnumerable<PlanogramAssortmentProductBuddyDto> FetchByPlanogramAssortmentId(object id)
        {
            return base.FetchByProperties(new String[] { "PlanogramAssortmentId" }, new Object[] { id });
        }
    }
}
