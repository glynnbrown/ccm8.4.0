﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27647 : L.Luong 
//   Created 

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramInventoryDal : MockDalBase<PlanogramInventoryDto>, IPlanogramInventoryDal
    {
        #region IPlanogramInventoryDal Members

        public PlanogramInventoryDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramInventoryDto dto = base.FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId }).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion
    }
}
