﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramSequenceGroupDal : MockDalBase<PlanogramSequenceGroupDto>, IPlanogramSequenceGroupDal
    {
        public IEnumerable<PlanogramSequenceGroupDto> FetchByPlanogramSequenceId(Object planogramSequenceId)
        {
            return this.FetchByProperties(new[] {"PlanogramSequenceId"}, new[] {planogramSequenceId});
        }
    }
}
