﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramPerformanceMetricDal : MockDalBase<PlanogramPerformanceMetricDto>, IPlanogramPerformanceMetricDal
    {
        public IEnumerable<PlanogramPerformanceMetricDto> FetchByPlanogramPerformanceId(object id)
        {
            return base.FetchByProperties(new String[] { "PlanogramPerformanceId" },new Object[] { id });
        }
    }
}
