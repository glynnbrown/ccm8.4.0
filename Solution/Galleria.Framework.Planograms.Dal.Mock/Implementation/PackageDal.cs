﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//  Initial version.
// V8-25745 : K.Pickup
//  Changed to use framework Mock DAL base classes.
// V8-25949 : N.Foster
//  Changed LockById to return success or failure
// V8-27919 : L.Ineson
//  Added FetchById with fetchArgument parameter
#endregion
#region Version History: (CCM 8.01)
// V8-28838 : L.Ineson
//  Added plan locking helpers.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PackageDal : MockDalBase<PackageDto>, IPackageDal
    {
        #region Plan Locking Helper

        [Serializable]
        private sealed class PlanogramLockDto
        {
            public Int32 Id { get; set; }
            public Int32 UserId { get; set; }
            public Byte LockType { get; set; }

            /// <summary>
            /// Returns a hash code for this object
            /// </summary>
            /// <returns>The object hash code</returns>
            public override Int32 GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            /// <summary>
            /// Check to see if two dtos are the same
            /// </summary>
            /// <param name="obj">The object to compare against</param>
            /// <returns>true if objects are equal</returns>
            public override Boolean Equals(Object obj)
            {
                PlanogramLockDto other = obj as PlanogramLockDto;
                if (other != null)
                {
                    if (other.Id != this.Id) { return false; }
                    if (other.UserId != this.UserId) { return false; }
                    if (other.LockType != this.LockType) { return false; }
                }
                else
                {
                    return false;
                }
                return true;
            }
        }

        private PlanogramLockDto FetchLockByPlanogramId(Object planId)
        {
            foreach (PlanogramLockDto dto in base.DalCache.GetDtos<PlanogramLockDto>())
            {
                if(Convert.ToInt32(planId) == dto.Id)
                {
                    return Clone(dto);
                }
            }
            return null;
        }

        private void InsertLock(Object planId, Object userId, Byte lockType)
        {
            base.DalCache.InsertDto(
                new PlanogramLockDto()
                {
                    Id = (Int32)planId,
                    UserId = (Int32)userId,
                    LockType = lockType
                });
        }

        private void DeleteLockByPlanogramId(Object planId)
        {
            PlanogramLockDto dto = FetchLockByPlanogramId(planId);
            if (dto != null)
            {
                base.DalCache.RemoveDto(dto);
            }
        }

        public Boolean IsPlanogramLocked(Object planId, out Int32? lockUserId, out Byte? lockType)
        {
            PlanogramLockDto dto = FetchLockByPlanogramId(planId);
            if (dto != null)
            {
                lockUserId = (Int32)dto.UserId;
                lockType = (Byte)dto.LockType;
                return true;
            }
            else
            {
                lockUserId = null;
                lockType = null;
                return false;
            }
        }

        #endregion

        #region IPackageDal Members

        /// <summary>
        /// Called when fetching a package with additional arguments
        /// </summary>
        public PackageDto FetchById(object id, object fetchArgument)
        {
            return base.FetchById(id);
        }

        /// <summary>
        /// Called when locking a package
        /// </summary>
        public Byte LockById(Object id, Object userId, Byte lockType, Boolean readOnly)
        {
            //if readonly dont bother just return success.
            if(readOnly) return 1;

            //check if the plan is already locked.
            PlanogramLockDto existingLock = FetchLockByPlanogramId(id);
            if (existingLock != null)
            {
                if (Object.Equals(existingLock.UserId, userId))
                {
                    return 1; //success - its already locked.
                }
                return 0; //fail.
            }

            //add the lock
            InsertLock(id, userId, lockType);
            return 1; // Success
        }

        /// <summary>
        /// Called when unlocking a package
        /// </summary>
        public Byte UnlockById(Object id, Object userId, Byte lockType, Boolean readOnly)
        {
            DeleteLockByPlanogramId(id);
            return 1; // Success
        }

        public byte UpdatePlanogramAttributes(IEnumerable<PlanogramAttributesDto> planAttributesDtoList)
        {
            throw new NotImplementedException();
        }

        public IList<Object> FetchIdsExceptFor(IEnumerable<string> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchByUcrsDateLastModified(IEnumerable<String> ucrs, DateTime dateLastModified)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();
        }

        public IList<Object> FetchDeletedByUcrs(IEnumerable<String> ucrs)
        {
            //  This method is not implemented for this DAL, however the interface requires it. 
            //  Returning an empty list for now is always going to represent no non matching UCRs.
            return new List<Object>();

        }
        #endregion
    }
}
