﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramAssortmentRegionDal : MockDalBase<PlanogramAssortmentRegionDto>, IPlanogramAssortmentRegionDal 
    {
        public IEnumerable<PlanogramAssortmentRegionDto> FetchByPlanogramAssortmentId(object id)
        {
            return base.FetchByProperties(new String[] { "PlanogramAssortmentId" }, new Object[] { id });
        }
    }
}
