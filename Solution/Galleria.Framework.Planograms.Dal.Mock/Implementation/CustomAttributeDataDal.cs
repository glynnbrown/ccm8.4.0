﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26147 : L.Ineson
//      Initial version.
//V8-26332 : L.Ineson
//  Added Upsert
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added method to fetch batch of custom attributes
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class CustomAttributeDataDal : MockDalBase<CustomAttributeDataDto>, ICustomAttributeDataDal
    {
        #region Fetch

        public CustomAttributeDataDto FetchByParentTypeParentId(byte parentType, object parentId)
        {
            CustomAttributeDataDto dto = FetchByProperties(new String[] { "ParentType", "ParentId" }, new Object[] { parentType, parentId }).FirstOrDefault();

            if (dto == null)
            {
                throw new DtoDoesNotExistException();
            }

            return dto;
        }

        public IEnumerable<CustomAttributeDataDto> FetchByParentTypeParentIds(Byte parentType, IEnumerable<Object> parentIds)
        {
            HashSet<Object> parentLookup = new HashSet<Object>();
            foreach (Object parentId in parentIds) parentLookup.Add(parentId);

            List<CustomAttributeDataDto> dtos = new List<CustomAttributeDataDto>();
            foreach (CustomAttributeDataDto dto in this.DalCache.GetDtos<CustomAttributeDataDto>())
            {
                if ((dto.ParentType == parentType) && (parentLookup.Contains(dto.ParentId)))
                {
                    dtos.Add(Clone(dto));
                }
            }
            return dtos;
        }

        #endregion

        #region Upsert

        public void Upsert(System.Collections.Generic.IEnumerable<CustomAttributeDataDto> dtoList, CustomAttributeDataIsSetDto isSetDto)
        {
            base.Upsert(dtoList, isSetDto, false);
        }

        #endregion
    }
}
