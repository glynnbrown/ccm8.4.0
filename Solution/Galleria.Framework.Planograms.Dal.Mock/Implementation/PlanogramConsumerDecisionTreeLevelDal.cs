﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramConsumerDecisionTreeLevelDal : MockDalBase<PlanogramConsumerDecisionTreeLevelDto>, IPlanogramConsumerDecisionTreeLevelDal
    {
        #region IPlanogramConsumerDecisionTreeLevelDal Members

        public IEnumerable<PlanogramConsumerDecisionTreeLevelDto> FetchByPlanogramConsumerDecisionTreeId(Object planogramConsumerDecisionTreeId)
        {
            return base.FetchByProperties(new String[] { "PlanogramConsumerDecisionTreeId" }, new Object[] { planogramConsumerDecisionTreeId });
        }

        #endregion
    }
}
