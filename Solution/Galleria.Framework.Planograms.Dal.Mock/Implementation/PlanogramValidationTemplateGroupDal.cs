﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    internal class PlanogramValidationTemplateGroupDal : MockDalBase<PlanogramValidationTemplateGroupDto>, IPlanogramValidationTemplateGroupDal
    {
        #region IPlanogramValidationTemplateGroupDal Members

        public IEnumerable<PlanogramValidationTemplateGroupDto> FetchByPlanogramValidationTemplateId(Object planogramValidationTemplateId)
        {
            return base.FetchByProperties(new String[] { "PlanogramValidationTemplateId" }, new Object[] { planogramValidationTemplateId });
        }

        #endregion
    }
}
