﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;


namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramSubComponentDal : MockDalBase<PlanogramSubComponentDto>, IPlanogramSubComponentDal
    {
        #region IPlanogramSubComponentDal Members

        public IEnumerable<PlanogramSubComponentDto> FetchByPlanogramComponentId(Object planogramComponentId)
        {
            return FetchByProperties(new String[] { "PlanogramComponentId" }, new Object[] { planogramComponentId });
        }

        #endregion
    }
}
