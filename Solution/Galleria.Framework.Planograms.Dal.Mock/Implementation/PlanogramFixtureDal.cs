﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24290 : K.Pickup
//      Initial version.
// V8-25745 : K.Pickup
//      Changed to use framework Mock DAL base classes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;


namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramFixtureDal : MockDalBase<PlanogramFixtureDto>, IPlanogramFixtureDal
    {
        #region IPlanogramFixtureDal Members

        public IEnumerable<PlanogramFixtureDto> FetchByPlanogramId(Object planogramId)
        {
            return FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }

        #endregion
    }
}
