﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramEventLogDal : MockDalBase<PlanogramEventLogDto>, IPlanogramEventLogDal
    {
        #region IPlanogramEventLogDal Members

        public IEnumerable<PlanogramEventLogDto> FetchByPlanogramId(Object planogramId)
        {
            return base.FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId });
        }

        #endregion
    }
}
