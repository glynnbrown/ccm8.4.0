﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramConsumerDecisionTreeNodeProductDal : MockDalBase<PlanogramConsumerDecisionTreeNodeProductDto>, IPlanogramConsumerDecisionTreeNodeProductDal
    {
        #region IPlanogramConsumerDecisionTreeNodeProductDal Members

        public IEnumerable<PlanogramConsumerDecisionTreeNodeProductDto> FetchByPlanogramConsumerDecisionTreeNodeId(Object planogramConsumerDecisionTreeNodeId)
        {
            return base.FetchByProperties(new String[] { "PlanogramConsumerDecisionTreeNodeId" }, new Object[] { planogramConsumerDecisionTreeNodeId });
        }

        #endregion
    }
}
