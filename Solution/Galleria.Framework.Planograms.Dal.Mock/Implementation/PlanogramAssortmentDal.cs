﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramAssortmentDal : MockDalBase<PlanogramAssortmentDto>, IPlanogramAssortmentDal
    {
        public PlanogramAssortmentDto FetchByPlanogramId(object id)
        {
            PlanogramAssortmentDto dto = base.FetchByProperties(new String[] { "PlanogramId" }, new Object[] { id }).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }
    }
}
