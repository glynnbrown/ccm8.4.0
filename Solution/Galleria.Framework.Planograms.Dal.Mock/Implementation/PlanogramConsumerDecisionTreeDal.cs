﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramConsumerDecisionTreeDal : MockDalBase<PlanogramConsumerDecisionTreeDto>, IPlanogramConsumerDecisionTreeDal
    {
        #region IPlanogramConsumerDecisionTreeDal Members

        public PlanogramConsumerDecisionTreeDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramConsumerDecisionTreeDto dto = base.FetchByProperties(new String[] { "PlanogramId" }, new Object[] { planogramId }).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }

        #endregion
    }
}
