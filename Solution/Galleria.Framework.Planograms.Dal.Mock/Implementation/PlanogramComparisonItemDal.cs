﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    /// <summary>
    ///     Mock implementation of <see cref="PlanogramComparisonItemDal"/>.
    /// </summary>
    /// <remarks>Instantiated by the DAL factory when required.</remarks>
    public class PlanogramComparisonItemDal : MockDalBase<PlanogramComparisonItemDto>, IPlanogramComparisonItemDal
    {
        #region IPlanogramComparisonItemDal Support

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Field</c> model object that belongs to a <c>Planogram Comparison Result</c> with the given<paramref name="id"/>.
        /// </summary>
        /// <param name="id"><see cref="Object"/> containing the id of the parent <c>Planogram Comparison Result</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonItemDto"/>.</returns>
        public IEnumerable<PlanogramComparisonItemDto> FetchByPlanogramComparisonResultId(Object id)
        {
            return FetchByProperties(new[] { "PlanogramComparisonResultId" }, new[] { id });
        }

        #endregion
    }
}