﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.UnitTesting.MockDal;

namespace Galleria.Framework.Planograms.Dal.Mock.Implementation
{
    public class PlanogramSequenceDal : MockDalBase<PlanogramSequenceDto>, IPlanogramSequenceDal
    {
        public PlanogramSequenceDto FetchByPlanogramId(Object planogramId)
        {
            PlanogramSequenceDto dto = this.FetchByProperties(new[] {"PlanogramId"}, new[] {planogramId}).FirstOrDefault();
            if (dto == null) throw new DtoDoesNotExistException();
            return dto;
        }
    }
}
