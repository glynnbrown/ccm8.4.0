﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Adapted from ISO's Mock DAL.
// V8-24658 : K.Pickup
//      Ongoing work to bring in line with CCM v8 conventions.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Galleria.Framework.Planograms.Dal;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.Dal.Mock
{
    /// <summary>
    /// Abstract class from which all dal objects inherit
    /// </summary>
    public abstract class DalBase<T> : IDal
        where T : new()
    {
        #region Fields
        protected DalContext _dalContext; // the current dal context
        #endregion

        #region Properties
        /// <summary>
        /// The current dal context
        /// </summary>
        public IDalContext DalContext
        {
            get { return _dalContext; }
            set { _dalContext = (DalContext)value; }
        }

        /// <summary>
        /// The current dal cache
        /// </summary>
        internal DalCache DalCache
        {
            get { return _dalContext.DalCache; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a clone of an Object
        /// </summary>
        /// <typeparam name="T">The type being cloned</typeparam>
        /// <param name="obj">the instance to clone</param>
        /// <returns>a clone of the provided instance</returns>
        protected static P Clone<P>(P obj)
        {
            if (obj == null)
            {
                return default(P);
            }
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                stream.Seek(0, SeekOrigin.Begin);
                return (P)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Called when this instance is being disposed of
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Called when this instance if being disposed of
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(Boolean disposing)
        {
            if (disposing)
            {
                // nothing to do
            }
        }
        #endregion

        #region Common Dal Methods

        #region Fetch Methods

        public IEnumerable<T> FetchAll()
        {
            return FetchAll(false);
        }

        public IEnumerable<T> FetchAll(Boolean includeDeleted = false)
        {
            return FetchByProperties<T>(new String[0], new Object[0], includeDeleted);
        }

        public IEnumerable<T> FetchAll<STORAGET>()
        {
            return FetchByProperties<STORAGET>(new String[0], new Object[0]);
        }

        public T FetchById(Object id)
        {
            return FetchSingleDtoByProperties<T>(new String[] { "Id" }, new Object[] { id });
        }

        private T GetInfoDtoFromStoreDto<STORAGET>(STORAGET dto)
        {
            T returnValue = new T();
            Type infoType = typeof(T);
            Type storageType = typeof(STORAGET);
            foreach (PropertyInfo infoProperty in infoType.GetProperties().Where(p => p.CanWrite))
            {
                PropertyInfo storageProperty = storageType.GetProperty(infoProperty.Name);
                if (storageProperty != null)
                {
                    infoProperty.SetValue(returnValue, storageProperty.GetValue(dto, null), null);
                }
            }
            return returnValue;
        }

        public IEnumerable<T> FetchByProperties(String[] propertyNames, Object[] values, Boolean includeDeleted = false)
        {
            return FetchByProperties<T>(propertyNames, values, includeDeleted);
        }

        // Probably needs obsoleting
        public IEnumerable<T> FetchDeletedByProperties(String[] propertyNames, Object[] values)
        {
            Type type = typeof(T);
            PropertyInfo dateDeletedProperty = type.GetProperty("DateDeleted");

            PropertyInfo[] properties = new PropertyInfo[propertyNames.Length];
            for (Int32 i = 0; i < propertyNames.Length; i++)
            {
                properties[i] = type.GetProperty(propertyNames[i]);
            }
            foreach (T dto in _dalContext.DalCache.GetDtos<T>())
            {
                Boolean match = true;
                for (Int32 i = 0; i < properties.Length; i++)
                {
                    if (properties[i] != null)
                    {
                        if (!AreEqual(properties[i].GetValue(dto, null), values[i]))
                        {
                            match = false;
                            break;
                        }
                    }
                    else
                    {
                        foreach (Object parentDto in _dalContext.DalCache.GetParentDtos(dto))
                        {
                            PropertyInfo property = parentDto.GetType().GetProperty(propertyNames[i]);
                            if (property != null)
                            {
                                if (!AreEqual(property.GetValue(parentDto, null), values[i]))
                                {
                                    match = false;
                                    break;
                                }
                            }
                        }
                        if (!match)
                        {
                            break;
                        }
                    }
                }
                if (match)
                {
                    if (dateDeletedProperty.GetValue(dto, null) != null)
                    {
                        yield return Clone(dto);
                    }
                }
            }
        }

        public Boolean AreEqual(Object obj1, Object obj2)
        {
            Boolean returnValue = true;
            if ((obj1 == null) && (obj2 != null))
            {
                returnValue = false;
            }
            else if ((obj2 == null) && (obj1 != null))
            {
                returnValue = false;
            }
            else if (obj1 is String && obj2 is String)
            {
                //string comparison should be case insensitive as in database.
                if (StringComparer.OrdinalIgnoreCase.Compare(obj1.ToString(), obj2.ToString()) != 0)
                {
                    returnValue = false;
                }
            }
            else if ((obj1 != null) && (obj2 != null) && (!obj1.Equals(obj2)))
            {
                returnValue = false;
            }
            return returnValue;
        }

        public T FetchSingleDtoByProperties(String[] propertyNames, Object[] values)
        {
            return FetchSingleDtoByProperties<T>(propertyNames, values);
        }

        public T FetchSingleDtoByProperties<STORAGET>(String[] propertyNames, Object[] values)
        {
            T returnValue = FetchByProperties<STORAGET>(propertyNames, values).FirstOrDefault();
            if (returnValue == null)
            {
                throw new DtoDoesNotExistException();
            }
            return returnValue;
        }

        // Should probably be obsoleted.
        public T FetchSingleDeletedDtoByProperties(String[] propertyNames, Object[] values)
        {
            Type type = typeof(T);
            PropertyInfo dateDeletedProperty = type.GetProperty("DateDeleted");

            PropertyInfo[] properties = new PropertyInfo[propertyNames.Length];
            for (Int32 i = 0; i < propertyNames.Length; i++)
            {
                properties[i] = type.GetProperty(propertyNames[i]);
            }
            foreach (T dto in _dalContext.DalCache.GetDtos<T>())
            {
                Boolean match = true;
                for (Int32 i = 0; i < properties.Length; i++)
                {
                    if (!AreEqual(properties[i].GetValue(dto, null), values[i]))
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    if (dateDeletedProperty.GetValue(dto, null) != null)
                    {
                        return Clone(dto);
                    }
                }
            }
            throw new DtoDoesNotExistException();
        }

        public IEnumerable<T> FetchByProperties<STORAGET>(
            String[] propertyNames,
            Object[] values,
            Boolean includeDeleted = false)
        {
            Type infoType = typeof(T);
            Type storageType = typeof(STORAGET);
            PropertyInfo dateDeletedProperty = storageType.GetProperty("DateDeleted");

            PropertyInfo[] properties = new PropertyInfo[propertyNames.Length];
            for (Int32 i = 0; i < propertyNames.Length; i++)
            {
                properties[i] = storageType.GetProperty(propertyNames[i]);
            }
            foreach (STORAGET dto in _dalContext.DalCache.GetDtos<STORAGET>())
            {
                Boolean match = true;
                for (Int32 i = 0; i < properties.Length; i++)
                {
                    if (properties[i] != null)
                    {
                        if ((!(values[i] is String)) && (values[i] is IEnumerable))
                        {
                            Boolean propertyMatch = false;
                            IEnumerable valueCollection = (IEnumerable)values[i];
                            foreach (Object value in valueCollection)
                            {
                                if (AreEqual(value, properties[i].GetValue(dto, null)))
                                {
                                    propertyMatch = true;
                                    break;
                                }
                            }
                            if (!propertyMatch)
                            {
                                match = false;
                                break;
                            }
                        }
                        else
                        {
                            if (!AreEqual(properties[i].GetValue(dto, null), values[i]))
                            {
                                match = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (Object parentDto in _dalContext.DalCache.GetParentDtos(dto))
                        {
                            PropertyInfo property = parentDto.GetType().GetProperty(propertyNames[i]);
                            if (property != null)
                            {
                                if (!AreEqual(property.GetValue(parentDto, null), values[i]))
                                {
                                    match = false;
                                    break;
                                }
                            }
                        }
                        if (!match)
                        {
                            break;
                        }
                    }
                }
                if (match)
                {
                    if (includeDeleted ||
                        dateDeletedProperty == null ||
                        dateDeletedProperty.GetValue(dto, null) == null)
                    {
                        yield return GetInfoDtoFromStoreDto<STORAGET>(dto);
                    }
                }
            }
        }

        public IEnumerable<T> SearchByProperties(String[] allPropertyNames, Object[] allValues)
        {
            return SearchByProperties<T>(allPropertyNames, allValues);
        }

        public IEnumerable<T> SearchByProperties<STORAGET>(
            String[] allPropertyNames,
            Object[] allValues)
        {
            Type infoType = typeof(T);
            Type storageType = typeof(STORAGET);
            PropertyInfo dateDeletedProperty = storageType.GetProperty("DateDeleted");

            PropertyInfo[] allProperties = new PropertyInfo[allPropertyNames.Length];
            for (Int32 i = 0; i < allPropertyNames.Length; i++)
            {
                allProperties[i] = storageType.GetProperty(allPropertyNames[i]);
            }
            foreach (STORAGET dto in _dalContext.DalCache.GetDtos<STORAGET>())
            {
                Boolean match = true;
                for (Int32 i = 0; i < allProperties.Length; i++)
                {
                    if (allValues[i] != null)
                    {
                        if (allValues[i] is String)
                        {
                            if (!((String)allProperties[i].GetValue(dto, null)).Contains((String)allValues[i]))
                            {
                                match = false;
                                break;
                            }
                        }
                        else
                        {
                            if (!AreEqual(allProperties[i].GetValue(dto, null), allValues[i]))
                            {
                                match = false;
                                break;
                            }
                        }
                    }
                }
                if (match)
                {
                    if (dateDeletedProperty == null
                        || dateDeletedProperty.GetValue(dto, null) == null)
                    {
                        yield return GetInfoDtoFromStoreDto<STORAGET>(dto);
                    }
                }
            }
        }

        #endregion

        #region Insert Methods

        public void Insert(T dto)
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty("Id");
            if (property != null)
            {
                property.SetValue(dto, _dalContext.DalCache.GetNextIdentity<T>(), null);
            }
            //property = type.GetProperty("RowVersion");
            //if (property != null)
            //{
            //    property.SetValue(dto, new RowVersion((Int64)1), null);
            //}
            //property = type.GetProperty("IsCreated");
            //if (property != null)
            //{
            //    property.SetValue(dto, true, null);
            //}
            //property = type.GetProperty("DateCreated");
            //if (property != null)
            //{
            //    property.SetValue(dto, DateTime.UtcNow, null);
            //}
            //property = type.GetProperty("DateLastModified");
            //if (property != null)
            //{
            //    property.SetValue(dto, DateTime.UtcNow, null);
            //}

            _dalContext.DalCache.InsertDto<T>(Clone(dto)/*, ChildDtoTypes*/);
        }

        public void Insert(IEnumerable<T> dtoList)
        {
            foreach (T dto in dtoList)
            {
                Insert(dto);
            }
        }

        #endregion

        #region Update Methods

        public virtual void Update(T dto)
        {
            Type type = typeof(T);
            T existingDto = default(T);
            PropertyInfo property = type.GetProperty("Id");
            if (property != null)
            {
                Int16 id = Convert.ToInt16(property.GetValue(dto, null));
                existingDto = FetchById(id);
            }
            else
            {
                property = type.GetProperty("DtoKey");
                existingDto = DalCache.GetDtos<T>().FirstOrDefault(d => property.GetValue(d, null).Equals(
                    property.GetValue(dto, null)));
            }


            if (existingDto != null)
            {
                //property = type.GetProperty("RowVersion");
                //if (property != null)
                //{
                //    RowVersion rowVersion = (RowVersion)property.GetValue(dto, null);
                //    if (rowVersion != (RowVersion)property.GetValue(existingDto, null))
                //    {
                //        throw new ConcurrencyException();
                //    }
                //    property.SetValue(dto, new RowVersion(rowVersion + 1), null);
                //}

                //property = type.GetProperty("DateLastModified");
                //if (property != null)
                //{
                //    property.SetValue(dto, DateTime.UtcNow, null);
                //}

                property = type.GetProperty("DtoKey");

                _dalContext.DalCache.RemoveDto<T>(existingDto);
                _dalContext.DalCache.InsertDto<T>(Clone(dto)/*, ChildDtoTypes*/);
            }
        }

        public virtual void UpdateForUpsert(T dto)
        {
            Type type = typeof(T);
            T existingDto = default(T);
            PropertyInfo property = type.GetProperty("DtoKey");
            if (property == null)
            {
                property = type.GetProperty("Id");
            }
            existingDto = DalCache.GetDtos<T>().FirstOrDefault(d => property.GetValue(d, null).Equals(
                property.GetValue(dto, null)));
            if (existingDto != null)
            {
                //set row version to existing dto's row version
                property = type.GetProperty("RowVersion");
                if (property != null)
                {
                    RowVersion rowVersion = (RowVersion)property.GetValue(existingDto, null);
                    property.SetValue(dto, new RowVersion(rowVersion + 1), null);
                }
                //set id to existing dto's id
                property = type.GetProperty("Id");
                if (property != null)
                {
                    try
                    {
                        Int16 id = (Int16)property.GetValue(existingDto, null);
                        property.SetValue(dto, id, null);
                    }
                    catch
                    {
                        Int32 id = (Int32)property.GetValue(existingDto, null);
                        property.SetValue(dto, id, null);
                    }
                }
                property = type.GetProperty("DtoKey");
                //DeleteByDtoKey(typeof(T), property.GetValue(existingDto, null), /*new List<String>(),*/ false, false);
                _dalContext.DalCache.RemoveDto<T>(existingDto);
                _dalContext.DalCache.InsertDto<T>(Clone(dto)/*, ChildDtoTypes*/);
            }
        }

        public virtual void Update<S>(T dto, S isSetDto)
        {
            Type type = typeof(T);
            T existingDto = default(T);
            PropertyInfo property = type.GetProperty("DtoKey");
            if (property == null)
            {
                property = type.GetProperty("Id");
            }
            existingDto = DalCache.GetDtos<T>().FirstOrDefault(d => property.GetValue(d, null).Equals(
                property.GetValue(dto, null)));
            if (existingDto != null)
            {
                //set row version to existing dto's row version
                property = type.GetProperty("RowVersion");
                if (property != null)
                {
                    RowVersion rowVersion = (RowVersion)property.GetValue(existingDto, null);
                    property.SetValue(dto, new RowVersion(rowVersion + 1), null);
                }
                //set id to existing dto's id
                property = type.GetProperty("Id");
                if (property != null)
                {
                    try
                    {
                        Int16 id = (Int16)property.GetValue(existingDto, null);
                        property.SetValue(dto, id, null);
                    }
                    catch
                    {
                        Int32 id = (Int32)property.GetValue(existingDto, null);
                        property.SetValue(dto, id, null);
                    }
                }

                property = type.GetProperty("DtoKey");

                //update the dto with the not set properties of the existingdto so that they
                // do not get overwritten
                foreach (PropertyInfo isSetProperty in typeof(S).GetProperties())
                {
                    Boolean isSet = (Boolean)isSetProperty.GetValue(isSetDto, null);

                    //if the isSetProperty is false then copy from the existing dto.
                    if (!isSet)
                    {
                        String dtoPropertyName = isSetProperty.Name;
                        dtoPropertyName = dtoPropertyName.Substring(2, dtoPropertyName.Length - 5);
                        PropertyInfo dtoProperty = typeof(T).GetProperty(dtoPropertyName);

                        Object existingValue = dtoProperty.GetValue(existingDto, null);
                        dtoProperty.SetValue(dto, existingValue, null);
                    }
                }


                //DeleteByDtoKey(typeof(T), property.GetValue(existingDto, null), /*new List<String>(),*/ false, false);
                _dalContext.DalCache.RemoveDto<T>(existingDto);
                _dalContext.DalCache.InsertDto<T>(Clone(dto)/*, ChildDtoTypes*/);
            }
        }

        public void Upsert(IEnumerable<T> dtoList)
        {
            Upsert(dtoList, true);
        }

        public void Upsert(IEnumerable<T> dtoList, Boolean updateDeleted)
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty("Id");
            PropertyInfo dateDeletedProperty = type.GetProperty("DateDeleted");

            T existingDto = default(T);
            foreach (T dto in dtoList)
            {
                property = type.GetProperty("DtoKey");
                existingDto = DalCache.GetDtos<T>().FirstOrDefault(d => property.GetValue(d, null).Equals(
                    property.GetValue(dto, null)));
                if (existingDto != null)
                {
                    if (updateDeleted //we are updating deleted items
                        || (dateDeletedProperty == null) // or this has no datedeletedproperty
                        || (dateDeletedProperty.GetValue(existingDto, null) == null)) // or datedeleted is null
                    {
                        UpdateForUpsert(dto);
                    }
                }
                else
                {
                    Insert(dto);
                }
            }
        }

        public void Upsert<S>(IEnumerable<T> dtoList, S isSetDto)
        {
            Upsert<S>(dtoList, isSetDto, true);
        }

        public void Upsert<S>(IEnumerable<T> dtoList, S isSetDto, Boolean updateDeleted)
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty("Id");
            PropertyInfo dateDeletedProperty = type.GetProperty("DateDeleted");

            T existingDto = default(T);
            foreach (T dto in dtoList)
            {
                property = type.GetProperty("DtoKey");
                existingDto = DalCache.GetDtos<T>().FirstOrDefault(d => property.GetValue(d, null).Equals(
                    property.GetValue(dto, null)));
                if (existingDto != null)
                {
                    if (updateDeleted //we are updating deleted items
                        || (dateDeletedProperty == null) // or this has no datedeletedproperty
                        || (dateDeletedProperty.GetValue(existingDto, null) == null)) // or datedeleted is null
                    {
                        Update(dto, isSetDto);
                    }
                }
                else
                {
                    Insert(dto);
                }
            }
        }

        #endregion

        #region Delete Methods

        private void DeleteByDtoKey(
            Type dtoType,
            Object dtoKey,
            Boolean enforceConstraints,
            Boolean repointChildIds)
        {
            //get the Object to be deleted
            Object mainDeleteObject = null;
            PropertyInfo dtoKeyProperty = dtoType.GetProperty("DtoKey");
            PropertyInfo dateDeletedProperty = dtoType.GetProperty("DateDeleted");

            foreach (Object dto in _dalContext.DalCache.GetDtos(dtoType))
            {
                if (Object.Equals(dtoKeyProperty.GetValue(dto, null), dtoKey))
                {
                    mainDeleteObject = dto;
                    break;
                }
            }

            if (mainDeleteObject != null)
            {
                //get the Object id
                PropertyInfo idProperty = dtoType.GetProperty("Id");
                if (idProperty != null)
                {
                    Object val = idProperty.GetValue(mainDeleteObject, null);
                    Int16 mainDeleteObjectId = Convert.ToInt16(val);

                    if (enforceConstraints)
                    {
                        // Delete child objects
                        foreach (Type childDtoType in dtoType.Assembly.GetTypes().Where(
                            t => t.Name.EndsWith("Dto") && t != dtoType)) // TODO: Handle self-references
                        {
                            foreach (PropertyInfo property in childDtoType.GetProperties().Where(p => p.CanWrite))
                            {
                                ForeignKeyAttribute parentType = GetParentType(property);
                                if ((parentType != null) && (parentType.ParentDtoType == dtoType))
                                {
                                    List<Object> dtos = new List<Object>(DalCache.GetDtos(childDtoType));
                                    foreach (Object dto in dtos)
                                    {
                                        Object foreignKeyValue = property.GetValue(dto, null);
                                        if ((foreignKeyValue != null) && (Convert.ToInt16(foreignKeyValue).Equals(mainDeleteObjectId)))
                                        {
                                            switch (parentType.DeleteBehavior)
                                            {
                                                case DeleteBehavior.Cascade:
                                                    PropertyInfo childDtoKeyProperty = childDtoType.GetProperty("DtoKey");
                                                    DeleteByDtoKey(childDtoType, childDtoKeyProperty.GetValue(dto, null), true, false);
                                                    break;
                                                case DeleteBehavior.LeaveButNullOffParentReference:
                                                    property.SetValue(dto, null, null);
                                                    break;
                                                case DeleteBehavior.Fail:
                                                    throw new Exception("constraint");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Update children of the same type
                    if (repointChildIds)
                    {
                        PropertyInfo parentIdProperty = dtoType.GetProperty("ParentId");
                        if (parentIdProperty != null)
                        {
                            foreach (Object dto in _dalContext.DalCache.GetDtos(dtoType))
                            {
                                Object parentId = parentIdProperty.GetValue(dto, null);
                                if ((parentId != null) && Convert.ToInt16(parentId) == mainDeleteObjectId)
                                {
                                    parentIdProperty.SetValue(dto, null, null);
                                }
                            }
                        }
                    }
                }

                // Delete Object
                if (dateDeletedProperty != null)
                {
                    //if object has datedeleted property then simply mark.
                    dateDeletedProperty.SetValue(mainDeleteObject, DateTime.UtcNow, null);
                }
                else
                {
                    _dalContext.DalCache.RemoveDto(dtoType, mainDeleteObject);
                }
            }
        }

        private static ForeignKeyAttribute GetParentType(PropertyInfo property)
        {
            ForeignKeyAttribute returnValue = null;
            foreach (Attribute attribute in property.GetCustomAttributes(true))
            {
                returnValue = attribute as ForeignKeyAttribute;
                if (returnValue != null)
                {
                    break;
                }
            }
            return returnValue;
        }

        private void DeleteByDtoKey(
            Type dtoType,
            Object dtoKey,
            List<String> childDtoTypesToDelete,
            Boolean enforceConstraints,
            Boolean repointChildIds)
        {
            //get the Object to be deleted
            Object mainDeleteObject = null;
            PropertyInfo dtoKeyProperty = dtoType.GetProperty("DtoKey");
            PropertyInfo dateDeletedProperty = dtoType.GetProperty("DateDeleted");

            foreach (Object dto in _dalContext.DalCache.GetDtos(dtoType))
            {
                if (Object.Equals(dtoKeyProperty.GetValue(dto, null), dtoKey))
                {
                    mainDeleteObject = dto;
                    break;
                }
            }

            if (mainDeleteObject != null)
            {
                //get the Object id
                PropertyInfo idProperty = dtoType.GetProperty("Id");
                if (idProperty != null)
                {
                    Object val = idProperty.GetValue(mainDeleteObject, null);
                    Int16 mainDeleteObjectId = Convert.ToInt16(val);

                    if (enforceConstraints)
                    {
                        // Delete child objects
                        foreach (Type childDtoType in dtoType.Assembly.GetTypes().Where(
                            t => t.Name.EndsWith("Dto") && t != dtoType)) // TODO: Handle self-references
                        {
                            foreach (PropertyInfo property in childDtoType.GetProperties().Where(p => p.CanWrite))
                            {
                                ForeignKeyAttribute parentType = GetParentType(property);
                                if ((parentType != null) && (parentType.ParentDtoType == dtoType))
                                {
                                    List<Object> dtos = new List<Object>(DalCache.GetDtos(childDtoType));
                                    foreach (Object dto in dtos)
                                    {
                                        Object foreignKeyValue = property.GetValue(dto, null);
                                        if ((foreignKeyValue != null) && (Convert.ToInt16(foreignKeyValue).Equals(mainDeleteObjectId)))
                                        {
                                            switch (parentType.DeleteBehavior)
                                            {
                                                case DeleteBehavior.Cascade:
                                                    PropertyInfo childDtoKeyProperty = childDtoType.GetProperty("DtoKey");
                                                    DeleteByDtoKey(childDtoType, childDtoKeyProperty.GetValue(dto, null), true, false);
                                                    break;
                                                case DeleteBehavior.LeaveButNullOffParentReference:
                                                    property.SetValue(dto, null, null);
                                                    break;
                                                case DeleteBehavior.Fail:
                                                    throw new Exception("constraint");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Update children of the same type
                    if (repointChildIds)
                    {
                        PropertyInfo parentIdProperty = dtoType.GetProperty("ParentId");
                        if (parentIdProperty != null)
                        {
                            foreach (Object dto in _dalContext.DalCache.GetDtos(dtoType))
                            {
                                Object parentId = parentIdProperty.GetValue(dto, null);
                                if ((parentId != null) && Convert.ToInt16(parentId) == mainDeleteObjectId)
                                {
                                    parentIdProperty.SetValue(dto, null, null);
                                }
                            }
                        }
                    }
                }

                // Delete Object
                if (dateDeletedProperty != null)
                {
                    //if object has datedeleted property then simply mark.
                    dateDeletedProperty.SetValue(mainDeleteObject, DateTime.UtcNow, null);
                }
                else
                {
                    _dalContext.DalCache.RemoveDto(dtoType, mainDeleteObject);
                }
            }
        }

        private void DeleteById(Type dtoType, Object id, Boolean enforceConstraints, Boolean repointChildIds)
        {
            //get the Object to be deleted
            Object deleteObject = null;

            //get its id value
            PropertyInfo property = dtoType.GetProperty("Id");
            foreach (Object dto in _dalContext.DalCache.GetDtos(dtoType))
            {
                if ((Int16)property.GetValue(dto, null) == (Int16)id)
                {
                    deleteObject = dto;
                    break;
                }
            }

            if (deleteObject != null)
            {
                //get its dtokey
                PropertyInfo dtoKeyproperty = dtoType.GetProperty("DtoKey");
                Object dtoKey = dtoKeyproperty.GetValue(deleteObject, null);

                DeleteByDtoKey(dtoType, dtoKey, /*childDtoTypesToDelete,*/ enforceConstraints, repointChildIds);
            }
        }

        public virtual void DeleteById(Object id)
        {
            DeleteById(typeof(T), Convert.ToInt16(id), true, true);
        }

        public void DeleteByProperties(String[] propertyNames, Object[] values)
        {
            Type type = typeof(T);
            PropertyInfo idProperty = type.GetProperty("Id");
            PropertyInfo[] properties = new PropertyInfo[propertyNames.Length];
            for (Int32 i = 0; i < propertyNames.Length; i++)
            {
                properties[i] = type.GetProperty(propertyNames[i]);
            }

            //Collection to hold dtos for deletion
            List<Int16> idsForRemoval = new List<Int16>();

            foreach (T dto in _dalContext.DalCache.GetDtos<T>())
            {
                Boolean match = true;
                for (Int32 i = 0; i < properties.Length; i++)
                {
                    if ((!(values[i] is String)) && (values[i] is IEnumerable))
                    {
                        Boolean propertyMatch = false;
                        IEnumerable valueCollection = (IEnumerable)values[i];
                        foreach (Object value in valueCollection)
                        {
                            if (AreEqual(value, properties[i].GetValue(dto, null)))
                            {
                                propertyMatch = true;
                                break;
                            }
                        }
                        if (!propertyMatch)
                        {
                            match = false;
                            break;
                        }
                    }
                    else
                    {
                        if (!AreEqual(properties[i].GetValue(dto, null), values[i]))
                        {
                            match = false;
                            break;
                        }
                    }
                }
                if (match)
                {
                    idsForRemoval.Add(Convert.ToInt16(idProperty.GetValue(dto, null)));
                }
            }

            //Remove from cache
            foreach (Int16 id in idsForRemoval)
            {
                DeleteById(id/*, childDtoTypesToDelete*/);
            }
        }

        #endregion

        private T Clone(T dto)
        {
            Type type = typeof(T);
            ConstructorInfo constructor = type.GetConstructor(System.Type.EmptyTypes);
            T returnValue = (T)constructor.Invoke(null);
            foreach (PropertyInfo property in type.GetProperties().Where(p => p.CanWrite))
            {
                property.SetValue(returnValue, property.GetValue(dto, null), null);
            }
            return returnValue;
        }


        #endregion
    }
}
