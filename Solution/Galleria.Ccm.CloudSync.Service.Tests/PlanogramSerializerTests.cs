﻿using System;
using System.Collections.ObjectModel;
using FluentAssertions;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System.Linq;

namespace Galleria.Ccm.CloudSync.Service.Tests
{
    [TestFixture]
    public class PlanogramSerializerTests
    {
        [Test]
        public void SerializePacakgeToJson()
        {
            // Arrange
            String expected =
                "{\"Id\":\"1\",\"DateCreated\":\"0001-01-01T00:00:00\",\"DateLastModified\":\"0001-01-01T00:00:00\",\"DateMetadataCalculated\":null,\"DateDeleted\":null,\"DateValidationDataCalculated\":null,\"EntityId\":null,\"Name\":\"New Package\",\"PackageType\":0,\"UserName\":\"\",\"IsReadOnly\":false,\"MetaPlanogramCount\":null,\"Planograms\":[]}";
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";
            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            String packageJson = sut.ToJson(source);

            // Assert
            packageJson.Should().Be(expected, because: "package models are serialized to their corresponding json format");
        }

        [Test]
        public void SerializeEachPlanogramInsidePackage()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";
            source.Planograms.Add(Planogram.NewPlanogram());
            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogram data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachFixtureInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());


            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("fixture data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachFixtureAssemblyInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            PlanogramFixtureAssembly fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();

            source.Planograms.Add(planogram);
            planogram.Fixtures.Add(fixture);
            fixture.Assemblies.Add(fixtureAssembly);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("FixtureAssembly data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramFixtureComponentInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            PlanogramFixtureComponent planogramFixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();

            source.Planograms.Add(planogram);
            planogram.Fixtures.Add(fixture);
            fixture.Components.Add(planogramFixtureComponent);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("FixtureAssembly data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramFixtureItemInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixture planogramFixture = PlanogramFixture.NewPlanogramFixture();
            PlanogramFixtureItem planogramFixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();

            source.Planograms.Add(planogram);
            planogram.Fixtures.Add(planogramFixture);

            planogramFixture.Id = 1;
            planogramFixture.Width = 2;
            planogramFixture.Height = 4;
            planogramFixtureItem.PlanogramFixtureId = 1;

            planogram.FixtureItems.Add(planogramFixtureItem);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramFixtureItem data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachFixtureItemInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixture planogramFixture = PlanogramFixture.NewPlanogramFixture();
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            source.Planograms.Add(planogram);
            planogram.Fixtures.Add(planogramFixture);

            planogramFixture.Id = 1;
            planogramFixture.Width = 2;
            planogramFixture.Height = 4;
            fixtureItem.PlanogramFixtureId = 1;
           
            planogram.FixtureItems.Add(fixtureItem);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("FixtureItem data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssemblyInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Assemblies.Add(PlanogramAssembly.NewPlanogramAssembly());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssembly data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssemblyComponentInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssembly assembly = PlanogramAssembly.NewPlanogramAssembly();
            PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();

            source.Planograms.Add(planogram);
            planogram.Assemblies.Add(assembly);
            assembly.Components.Add(assemblyComponent);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssemblyComponent data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachComponentInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("component data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachSubComponentInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent();
            PlanogramSubComponent subComponent = PlanogramSubComponent.NewPlanogramSubComponent();

            source.Planograms.Add(planogram);
            planogram.Components.Add(component);
            component.SubComponents.Add(subComponent);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramSubComponent data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramCustomAttributeDataInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            planogram.CustomAttributes.Text1 = "Nunit Testing Attrubute 1";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("CustomAttributeData data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachProductInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Products.Add(PlanogramProduct.NewPlanogramProduct());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("blocking data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPositionInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Positions.Add(PlanogramPosition.NewPlanogramPosition());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("position data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachAnnotationInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Annotations.Add(PlanogramAnnotation.NewPlanogramAnnotation());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("annotation data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramImageInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Images.Add(PlanogramImage.NewPlanogramImage());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramImage data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramPerformanceInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            planogram.Performance.Description = "PerformanceDescription";


            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramPerformance can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramPerformanceMetricInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramPerformanceMetric planogramPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();

            source.Planograms.Add(planogram);
            planogram.Performance.Description = "PerformanceDescription";
            planogram.Performance.Metrics.Add(planogramPerformanceMetric);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramPerformanceMetric can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramPerformanceDataInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramPerformanceData planogramPerformancData = PlanogramPerformanceData.NewPlanogramPerformanceData();

            source.Planograms.Add(planogram);
            planogram.Performance.Description = "PerformanceDescription";
            planogram.Performance.PerformanceData.Add(planogramPerformancData);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramPerformanceData can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortment data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentRegionInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentRegion assortmentRegion = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.Regions.Add(assortmentRegion);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentRegion data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentRegionLocationInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentRegion assortmentRegion = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
            PlanogramAssortmentRegionLocation assortmentRegionLocation = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.Regions.Add(assortmentRegion);
            assortmentRegion.Locations.Add(assortmentRegionLocation);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentRegionLocation data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentRegionProductInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentRegion assortmentRegion = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
            PlanogramAssortmentRegionProduct assortmentRegionProduct = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.Regions.Add(assortmentRegion);
            assortmentRegion.Products.Add(assortmentRegionProduct);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentRegionProduct data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentInventoryRuleInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentInventoryRule assortmentInventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.InventoryRules.Add(assortmentInventoryRule);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentInventoryRule data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentProductBuddyInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();

            assortment.ProductBuddies.Add(PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy());

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentProductBuddy data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentLocationBuddyInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();

            assortment.LocationBuddies.Add(PlanogramAssortmentLocationBuddy.NewPlanogramAssortmentLocationBuddy());

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentLocationBuddy data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramValidationTemplateInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);

            planogram.ValidationTemplate.Name = "Test Name"; 

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentLocationBuddy data can be serialized to json from the csla model");

        }

        [Test]
        public void SerializeEachPlanogramValdationTemplateGroupInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramValidationTemplateGroup planogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
         
            source.Planograms.Add(planogram);

            planogram.ValidationTemplate.Name = "Test Name";

            planogram.ValidationTemplate.Groups.Add(planogramValidationTemplateGroup);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentLocationBuddy data can be serialized to json from the csla model");

        }
        [Test]
        public void SerializeEachPlanogramValidationTemplateGroupMetricInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramValidationTemplateGroup planogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            PlanogramValidationTemplateGroupMetric planogramValidationTemplateGroupMetric = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();

            source.Planograms.Add(planogram);
            planogram.ValidationTemplate.Groups.Add(planogramValidationTemplateGroup);
            planogram.ValidationTemplate.Groups.First().Metrics.Add(planogramValidationTemplateGroupMetric);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentLocationBuddy data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramConsumerDecisionTreeInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            planogram.ConsumerDecisionTree.Name = "Test Name";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            
            serializingToJson.ShouldNotThrow("PlanogramAssortmentConsumerDecisionTree data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramConsumerDecisionTreeNodeInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            ReadOnlyCollection<PlanogramConsumerDecisionTreeNode> cdtNodes = planogram.ConsumerDecisionTree.FetchAllNodes();
            Assert.IsTrue(cdtNodes.Any(), "CDT Nodes must not be null for the test!");

            cdtNodes.First().Name = "TestValue";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert

            serializingToJson.ShouldNotThrow("PlanogramAssortmentConsumerDecisionTreeNode data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramConsumerDecisionTreeNodeProductInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            ReadOnlyCollection<PlanogramConsumerDecisionTreeNode> cdtNodes = planogram.ConsumerDecisionTree.FetchAllNodes();
            Assert.IsTrue(cdtNodes.Any(), "CDT Nodes must not be null for the test!");
            
            cdtNodes.First().Products.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(1));

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert

            serializingToJson.ShouldNotThrow("PlanogramAssortmentConsumerDecisionTreeNodeProduct data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramConsumerDecisionTreeLevelInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();

            source.Planograms.Add(planogram);
            planogram.ConsumerDecisionTree.RootLevel.Name = "TestValue";
            

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert

            serializingToJson.ShouldNotThrow("PlanogramAssortmentConsumerDecisionTreeNode data can be serialized to json from the csla model");
        }
        
        [Test]
        public void SerializeEachPlanogramAssortmentLocalProductInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentLocalProduct assortmentLocalProduct = PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.LocalProducts.Add(assortmentLocalProduct);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentLocalProduct data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramAssortmentProductInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();

            source.Planograms.Add(planogram);
            planogram.Assortment.AddAssortment(assortment);
            planogram.Assortment.Products.Add(assortmentProduct);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramAssortmentProduct data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachEventLogInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("event log data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachBlockingInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("blocking data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachlanogramBlockingGroupDividerInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            source.Planograms.Add(planogram);

            planogram.Blocking.Add(blocking);
            blocking.Dividers.Add(PlanogramBlockingDivider.NewPlanogramBlockingDivider());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("blocking data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramBlockingLocationInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            source.Planograms.Add(planogram);

            planogram.Blocking.Add(blocking);
            blocking.Locations.Add(PlanogramBlockingLocation.NewPlanogramBlockingLocation());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("blocking data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramPlanogramRenumberingStrategyInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.RenumberingStrategy.Name = "TestValue";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogramSequence data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramInventoryInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.Inventory.MinDeliveries = 1;

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogramSequence data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramSequenceInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            
            // Sequence is readonly and created when loaded. An Item needs adding in order to test serialization.
            source.Planograms.Add(planogram);
            planogram.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogramSequence data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramSequenceGroupInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            
            source.Planograms.Add(planogram);
            planogram.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup());

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogramSequence data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramSequenceGroupProductInsidePlanogram()
        {
            // Arrange
            
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramSequenceGroup planogramSequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            PlanogramSequenceGroupProduct planogramSequenceGroupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();

            source.Planograms.Add(planogram);
            planogram.Sequence.Groups.Add(planogramSequenceGroup);
            planogramSequenceGroup.Products.Add(planogramSequenceGroupProduct);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("planogramSequence data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramComparisonInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            planogram.Comparison.Description = "TestDescription";

            source.Planograms.Add(planogram);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramComparison data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramComparisonResultInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComparisonResult planogramComparisonResultItem = PlanogramComparisonResult.NewPlanogramComparisonResult();

            planogram.Comparison.Results.Add(planogramComparisonResultItem);
            planogram.Comparison.Results.First().PlanogramName = "TestName";
            
            source.Planograms.Add(planogram);

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramComparisonResult data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramComparisonItemInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComparisonResult planogramComparisonResultItem = PlanogramComparisonResult.NewPlanogramComparisonResult();
            PlanogramComparisonItem planogramComparisonResultComparedIem = PlanogramComparisonItem.NewPlanogramComparisonItem();

            source.Planograms.Add(planogram);
            planogram.Comparison.Results.Add(planogramComparisonResultItem);
            planogram.Comparison.Results.First().ComparedItems.Add(planogramComparisonResultComparedIem);

            PlanogramComparisonItem comparedItem = planogram.Comparison.Results.First().ComparedItems.First();
            comparedItem.Status = PlanogramComparisonItemStatusType.New;

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramComparisonFieldValue data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramComparisonFieldValueInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComparisonResult planogramResult = PlanogramComparisonResult.NewPlanogramComparisonResult();
            PlanogramComparisonItem planogramComparedItem = PlanogramComparisonItem.NewPlanogramComparisonItem();
            PlanogramComparisonFieldValue planogramComparedItemFieldValue = PlanogramComparisonFieldValue.NewPlanogramComparisonFieldValue();
            source.Planograms.Add(planogram);

            planogram.Comparison.Results.Add(planogramResult);
            planogram.Comparison.Results.First().ComparedItems.Add(planogramComparedItem);
            planogram.Comparison.Results.First().ComparedItems.First().FieldValues.Add(planogramComparedItemFieldValue);

            PlanogramComparisonFieldValue fieldValue = planogram.Comparison.Results.First().ComparedItems.First().FieldValues.First();
            fieldValue.Value = "Test Value";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramComparisonFieldValue data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramComparisonFieldInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComparisonField planogramFieldItem = PlanogramComparisonField.NewPlanogramComparisonField();

            source.Planograms.Add(planogram);
            planogram.Comparison.ComparisonFields.Add(planogramFieldItem);

            planogram.Comparison.ComparisonFields.First().DisplayName = "TestDescription";

            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramComparisonField data can be serialized to json from the csla model");
        }

        [Test]
        public void SerializeEachPlanogramMetadataImageInsidePlanogram()
        {
            // Arrange
            Package source = Package.NewPackage(1, PackageLockType.User);
            source.Id = "1";

            Planogram planogram = Planogram.NewPlanogram();
            source.Planograms.Add(planogram);

            planogram.PlanogramMetadataImages.Add(PlanogramMetadataImage.NewPlanogramMetadataImage());


            IPlanogramSerializer sut = new PlanogramSerializer();

            // Act
            Action serializingToJson = () => sut.ToJson(source);

            // Assert
            serializingToJson.ShouldNotThrow("PlanogramMetadataImage data can be serialized to json from the csla model");
        }
    }

}