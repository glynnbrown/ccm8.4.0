﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Galleria.Ccm.CloudSync.Service.Clients;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Providers;
using Galleria.Framework.Planograms.Model;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;
using System.IO;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.CloudSync.Service.Tests.Providers
{
    [TestFixture]
    internal sealed class RemoteSyncProviderTests : TestBase
    {
        private static readonly List<PlanInfo> PlanInfos = new List<PlanInfo>
                                                           {
                                                               new PlanInfo {Ucr = "1", Name = "Not updated Plan", DateUpdated = DateTime.UtcNow.AddDays(-7)},
                                                               new PlanInfo {Ucr = "2", Name = "Updated Plan", DateUpdated = DateTime.UtcNow.AddDays(-2)},
                                                               new PlanInfo {Ucr = "3", Name = "Deleted Plan", DateUpdated = DateTime.UtcNow.AddDays(-7)}
                                                           };

        private static Boolean IsCorrectMessage(HttpRequestMessage message, String content, HttpMethod httpMethod, String requestUri)
        {
            return message.Method == httpMethod &&
                   message.RequestUri.OriginalString == requestUri &&
                   message.Content.ReadAsStringAsync().Result == content;
        }

        private static Boolean IsCorrectMessage(HttpRequestMessage message, Stream content, HttpMethod httpMethod, String requestUri)
        {
            if (message.Method != httpMethod) return false;

            if (message.RequestUri.OriginalString != requestUri) return false;

            Stream result = message.Content.ReadAsStreamAsync().Result;
            if (result.Length != content.Length) return false;

            var buffer = new byte[result.Length];
            result.Read(buffer, 0, buffer.Length);
            var contentBuffer = new byte[content.Length];
            content.Read(contentBuffer, 0, contentBuffer.Length);
            return buffer.Zip(contentBuffer, (a,b) => a == b).All(same => same);
        }

        [Test]
        public void ReturnsListPlanInfosFromWebService()
        {
            var webClient =
                Mock.Of<IWebApiClient>(
                    client =>
                    client.GetAsync("api/planograms/infos/") ==
                    TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
                                      {
                                          Content = new StringContent(JsonConvert.SerializeObject(PlanInfos))
                                      }));
            var sut = new RemoteSyncProvider(webClient);

            IList<IPlanInfo> infos = sut.FetchPlanInfos();

            infos.Should().HaveCount(3, "3 plan infos were sent by the service");
        }

        [Test]
        public void SendsPackagesToWebServiceAsAdd()
        {
            Package newPackage = Package.NewPackage(1, PackageLockType.System);
            String content = new PlanogramSerializer().ToJson(newPackage);
            var testWebClient = new Mock<IWebApiClient>();
            testWebClient.Setup(
                client =>
                client.SendAsync(It.Is<HttpRequestMessage>(message => IsCorrectMessage(message, content, HttpMethod.Put, "api/planograms/add"))))
                                     .Returns(() => TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK) {Content = new StringContent(String.Empty)}))
                                     .Verifiable($"{nameof(IRemoteSyncProvider.AddPackage)} should call the webservice correctly.");
            IRemoteSyncProvider sut = new RemoteSyncProvider(testWebClient.Object);

            sut.AddPackage(newPackage, new Dictionary<Guid, Guid>());

            testWebClient.Verify();
        }

        [Test]
        public void SendsPackagesToWebServiceAsUpdate()
        {
            Package newPackage = Package.NewPackage(1, PackageLockType.System);
            String content = new PlanogramSerializer().ToJson(newPackage);
            var testWebClient = new Mock<IWebApiClient>();
            testWebClient.Setup(
                client =>
                client.SendAsync(It.Is<HttpRequestMessage>(message => IsCorrectMessage(message, content, HttpMethod.Post, "api/planograms/update"))))
                     .Returns(TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK) {Content = new StringContent("")}))
                     .Verifiable($"{nameof(IRemoteSyncProvider.AddPackage)} should call the webservice correctly.");
            var sut = new RemoteSyncProvider(testWebClient.Object);

            Boolean result = sut.UpdatePackages(new List<Package> {newPackage}, new Dictionary<Guid, Guid>());

            testWebClient.Verify();
            result.Should().BeTrue("packages should have been sent to the webservice for update");
        }

        [Test]
        public void SendsUcrsToWebServiceAsDelete()
        {
            Planogram planogram = Package.NewPackage(1, PackageLockType.System).Planograms.AddNew();
            String content = planogram.UniqueContentReference.ToString();
            var testWebClient = new Mock<IWebApiClient>();
            testWebClient.Setup(
                client =>
                client.SendAsync(It.Is<HttpRequestMessage>(message => IsCorrectMessage(message, content, HttpMethod.Delete, "api/planograms/delete"))))
                         .Returns(TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK) {Content = new StringContent("")}))
                         .Verifiable($"{nameof(IRemoteSyncProvider.AddPackage)} should call the webservice correctly.");
            var sut = new RemoteSyncProvider(testWebClient.Object);

            Boolean result = sut.DeleteUcrs(new List<String> { content });

            testWebClient.Verify();
            result.Should().BeTrue("package UCRs should have been sent to the webservice for deletion");
        }

        [Test]
        public void ThrowsExceptionWhenNoWebClientIsProvided()
        {
            Action sut = () =>
                         {
                             var test = new RemoteSyncProvider(null);
                         };

            sut.ShouldThrow<ArgumentNullException>("it is not possible to instantiate a new remote sync provider without a web api client");
        }


        [Test]
        public void SendsFileAsStreamContentToWebService()
        {
            var stream = Mock.Of<Stream>();
            ILogFile logFile = new LogFile(stream);
            Stream content = logFile.stream;
            var testWebApiClient = new Mock<IWebApiClient>();
            testWebApiClient.Setup(client => client.SendAsync(It.Is<HttpRequestMessage>(message => IsCorrectMessage(message, content, HttpMethod.Put, "api/logfiles/add"))))
                .Returns(TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(String.Empty) }))
                .Verifiable($"{nameof(IRemoteSyncProvider.AddLogFiles)} should call the webservice correctly.");
            IList<ILogFile> logFiles = new List<ILogFile> { logFile };
            var sut = new RemoteSyncProvider(testWebApiClient.Object);

            Boolean result = sut.AddLogFiles(logFiles);

            testWebApiClient.Verify();
        }

        [Test]
        public void SendsPlanogramHierarchyToWebService()
        {
            PlanogramHierarchy hierarchy = PlanogramHierarchy.NewPlanogramHierarchy(1);
            PlanogramGroup group = PlanogramGroup.NewPlanogramGroup();
            hierarchy.RootGroup.ChildList.Add(group);
            var content = new PlanogramHierarchySerializer().ToJson(hierarchy);
            var testWebClient = new Mock<IWebApiClient>();
            testWebClient.Setup(
                client =>
                client.SendAsync(It.Is<HttpRequestMessage>(message => IsCorrectMessage(message, content, HttpMethod.Put, "api/planogramhierarchy/update"))))
                         .Returns(TaskEx.FromResult(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("") }))
                         .Verifiable($"{nameof(IRemoteSyncProvider.UpdatePlanogramHierarchy)} should call the webservice correctly.");
            var sut = new RemoteSyncProvider(testWebClient.Object);

            Boolean result = sut.UpdatePlanogramHierarchy(hierarchy);

            testWebClient.Verify();
            result.Should().BeTrue("Planogram Hierarchy Data should have been communicated to the web service");

        }
    }
}