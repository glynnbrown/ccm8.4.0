﻿using FluentAssertions;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Ccm.CloudSync.Service.Providers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using Topshelf.Logging;
using static System.DateTime;

namespace Galleria.Ccm.CloudSync.Service.Tests.Providers
{
    [TestFixture]
    internal sealed class LocalSyncProviderTests
    {
        [Test]
        public void ReturnAllPackagesGivenNoPlanInfos()
        {
            //Given
            ILocalSyncProvider sut = new LocalSyncProvider(Mock.Of<ISyncSettings>(), new JsonServiceStateRepository(""), Mock.Of<LogWriter>());
            String testUcr = Guid.NewGuid().ToString();
            var newPlanInfos = new List<IPlanInfo> { new PlanInfo { Ucr = testUcr } };
            IEnumerable<String> newUcrs = newPlanInfos.Select(i => i.Ucr);

            var testDalPackage = new Mock<IPackageDal>();
            var testDalPlanogram = new Mock<IPlanogramDal>();
            Mother.SetUpDalContext(nameof(LocalSyncProviderTests))
                  .ThatGetsDal(testDalPackage.ThatCanBeLocked()
                                             .WhereFetchByIdReturns(PackageDto)
                                             .ThatFetchesIdsExceptFor(newPlanInfos))
                  .ThatGetsDal(testDalPlanogram.WhereFetchByPackageIdReturns(id => PlanogramDtos(id, testUcr)));

            //When
            IList<Package> packages = sut.GetPackagesDistinct(newPlanInfos);

            //Then
            IEnumerable<String> returnedUcrs = packages.Select(p => p.Planograms.Single().UniqueContentReference.ToString());
            returnedUcrs.Should().BeEquivalentTo(newUcrs, "all ucrs in the plan info list were new");
        }
        
        [Test]
        public void ReturnPackagesThatAreUpdatedGivenPlanInfos()
        {
            // Given
            ILocalSyncProvider sut = new LocalSyncProvider(Mock.Of<ISyncSettings>(), new JsonServiceStateRepository(""), Mock.Of<LogWriter>());
            String testUcr = Guid.NewGuid().ToString();
            var earlyDateTime = UtcNow;
            var updatedPlanInfos = new List<IPlanInfo> { new PlanInfo { Ucr = testUcr, DateUpdated = earlyDateTime } };
            IEnumerable<String> newUcrs = updatedPlanInfos.Select(i => i.Ucr);

            var testDalPackage = new Mock<IPackageDal>();
            var testDalPlanogram = new Mock<IPlanogramDal>();
            Mother.SetUpDalContext(nameof(LocalSyncProviderTests))
                  .ThatGetsDal(testDalPackage.ThatCanBeLocked()
                                             .WhereFetchByIdReturns(PackageDto)
                                             .WhereFetchPackagesModifiedAfterReturns(updatedPlanInfos))
                  .ThatGetsDal(testDalPlanogram.WhereFetchByPackageIdReturns(id => PlanogramDtos(id, testUcr)));


            // When
            IList<Package> packages = sut.GetUpdatedPackages(updatedPlanInfos);

            // Then
            IEnumerable<String> returnedUcrs = packages.Select(p => p.Planograms.Single().UniqueContentReference.ToString());
            returnedUcrs.Should().BeEquivalentTo(newUcrs, "All ucrs in the planinfo list were modified late");
            
        }

        [Test]
        public void ReturnAllPackagesThatHaveBeenDeleted()
        {
            //Given
            ILocalSyncProvider sut = new LocalSyncProvider(Mock.Of<ISyncSettings>(), new JsonServiceStateRepository(""), Mock.Of<LogWriter>());
            String testUcr = Guid.NewGuid().ToString();
            var newPlanInfos = new List<IPlanInfo> { new PlanInfo { Ucr = testUcr } };
            IEnumerable<String> newUcrs = newPlanInfos.Select(i => i.Ucr);

            var testDalPackage = new Mock<IPackageDal>();
            var testDalPlanogram = new Mock<IPlanogramDal>();
            Mother.SetUpDalContext(nameof(LocalSyncProviderTests))
                  .ThatGetsDal(testDalPackage.ThatCanBeLocked()
                                             .WhereFetchByIdReturns(PackageDto)
                                             .ThatFetchesIdsExceptFor(newPlanInfos))
                  .ThatGetsDal(testDalPlanogram.WhereFetchByPackageIdReturns(id => PlanogramDtos(id, testUcr)));

            //When
            IList<IPlanInfo> planInfos = sut.GetDeletedUcrs(newPlanInfos);

            //Then
            IEnumerable<String> returnedUcrs = planInfos.Select(p => p.Ucr.ToString());
            returnedUcrs.Should().NotBeEquivalentTo(newUcrs, "all ucrs in the plan info list were deleted so no UCRs should have been returned");
        }

        private static PackageDto PackageDto(Object id, Object fetchArgument)
        {
            return new PackageDto { Name = "test" };
        }

        private static IEnumerable<PlanogramDto> PlanogramDtos(Object id, String testUcr)
        {
            return new List<PlanogramDto> { new PlanogramDto { Name = "test", UniqueContentReference = testUcr } };
        }
    }
}
