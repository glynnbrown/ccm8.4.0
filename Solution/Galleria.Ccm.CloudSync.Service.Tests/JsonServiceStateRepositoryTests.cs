﻿using System;
using System.IO;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Galleria.Ccm.CloudSync.Service.Tests
{
    [TestFixture]
    public class JsonServiceStateRepositoryTests
    {
        private const String JsonRepositoryPath = "c:/temp/galleria/cloudsync/state.json";

        [Test]
        public void LastFullSyncDatePersistsWhenFileMissing()
        {
            DateTime expected = DateTime.UtcNow;
            if (File.Exists(JsonRepositoryPath)) { File.Delete(JsonRepositoryPath); }
            IServiceStateRepository sut = new JsonServiceStateRepository(JsonRepositoryPath);

            sut.LastFullSyncDate = expected;

            File.Exists(JsonRepositoryPath).Should().BeTrue("the persistance file should have been created");
            JsonConvert.DeserializeObject<JsonServiceState>(File.ReadAllText(JsonRepositoryPath)).LastFullSyncDate.Should().Be(expected);
        }

        [Test]
        public void LastFullSyncDateGetsPersistedData()
        {
            DateTime expected = DateTime.UtcNow;
            if (File.Exists(JsonRepositoryPath)) { File.Delete(JsonRepositoryPath); }
            var serviceState = new JsonServiceState { LastFullSyncDate = expected };
            File.WriteAllText(JsonRepositoryPath, JsonConvert.SerializeObject(serviceState));
            IServiceStateRepository sut = new JsonServiceStateRepository(JsonRepositoryPath);

            DateTime date = sut.LastFullSyncDate;

            date.Should().Be(expected);
        }

        [Test]
        public void LastPublishingSyncDatePersistsWhenFileMissing()
        {
            DateTime expected = DateTime.UtcNow;
            if (File.Exists(JsonRepositoryPath)) { File.Delete(JsonRepositoryPath); }
            IServiceStateRepository sut = new JsonServiceStateRepository(JsonRepositoryPath);

            sut.LastPublishingSyncDate = expected;

            File.Exists(JsonRepositoryPath).Should().BeTrue("the persistance file should have been created");
            JsonConvert.DeserializeObject<JsonServiceState>(File.ReadAllText(JsonRepositoryPath)).LastPublishingSyncDate.Should().Be(expected);
        }

        [Test]
        public void LastPublishingSyncDateGetsPersistedData()
        {
            DateTime expected = DateTime.UtcNow;
            if (File.Exists(JsonRepositoryPath)) { File.Delete(JsonRepositoryPath); }
            var serviceState = new JsonServiceState { LastPublishingSyncDate = expected };
            File.WriteAllText(JsonRepositoryPath, JsonConvert.SerializeObject(serviceState));
            IServiceStateRepository sut = new JsonServiceStateRepository(JsonRepositoryPath);

            DateTime date = sut.LastPublishingSyncDate;

            date.Should().Be(expected);
        }

        // Persists LastPublishingSyncDate.
        // Gets persisted LastFullSyncDate.
        // Gets persisted LastPublishSyncDate.
    }
}