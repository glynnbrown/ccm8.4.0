﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Galleria.Ccm.CloudSync.Service.Clients;

namespace Galleria.Ccm.CloudSync.Service.Tests.Clients
{
    [TestFixture]
    internal sealed class TokenShould
    {
        [Test]
        public void ReturnInValidWhenHasNoToken()
        {
            IToken sut = new Token();

            sut.IsValid().Should().BeFalse("the token is not valid");
        }

        [Test]
        public void AcceptValidToken()
        {
            IToken sut = new Token();

            sut.Accept(Guid.NewGuid().ToString()).Should().BeTrue("the provided token json was valid");
        }

        [Test]
        public void NotAcceptInvalidToken()
        {
            IToken sut = new Token();

            sut.Accept("Invalid Token").Should().BeFalse("the provided token json was invalid");
        }

        [Test]
        public void ReturnValidWhenHasToken()
        {
            IToken sut = new Token();
            sut.Accept(Guid.NewGuid().ToString());

            sut.IsValid().Should().BeTrue("the token is valid");
        }

        [Test]
        public void ReturnJsonToken()
        {
            IToken sut = new Token();
            String json = Guid.NewGuid().ToString();
            sut.Accept(json);

            sut.ToJson().Should().Be(json, "the guid becomes a string when tojson");
        }

        [Test]
        public void ReturnEmptyStringWhenTokenIsInvalid()
        {
            IToken sut = new Token();

            sut.ToJson().Should().Be(String.Empty, "invalid tokens are serialized to json as empty strings");
        }
    }
}