﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.CloudSync.Service.Tests
{
    public abstract class TestBase
    {
        #region Fields

        private Galleria.Ccm.Dal.Mock.DalFactory _dalFactory;
        private String _dalName;

        #endregion

        [SetUp]
        public virtual void Setup()
        {
            //Register the main mock dal
            _dalName = Guid.NewGuid().ToString();
            _dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(_dalName, _dalFactory);
            _dalFactory.CreateDatabase();

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            //Authenticate the test user.
            DomainPrincipal.Authenticate();
        }
    }
}
