﻿using System;
using FluentAssertions;
using Galleria.Ccm.CloudSync.Service.Clients;
using Galleria.Ccm.CloudSync.Service.Models;
using Moq;
using NUnit.Framework;
using Topshelf.Logging;

namespace Galleria.Ccm.CloudSync.Service.Tests
{
    [TestFixture]
    internal sealed class Net4WebApiClientTests
    {
        [Test]
        public void ConstructorThrowsArgumentNullExceptionWhenEndpointIsNull()
        {
            Action constructor = () => new Net4WebApiClient(null, null);

            constructor.ShouldThrow<ArgumentNullException>($"a {nameof(Net4WebApiClient)} cannot be constructed with a null endpoint");
        }

        [Test]
        public void ConstructorThrowsArgumentNullExceptionWhenEndpointIsEmpty()
        {
            Action constructor = () => new Net4WebApiClient("", null);

            constructor.ShouldThrow<ArgumentNullException>($"a {nameof(Net4WebApiClient)} cannot be constructed with an empty endpoint");
        }


        [Test]
        public void ConstructorThrowsArgumentNullExceptionWhenEndpointIsWhitespace()
        {
            Action constructor = () => new Net4WebApiClient("   ", null);

            constructor.ShouldThrow<ArgumentNullException>($"a {nameof(Net4WebApiClient)} cannot be constructed with a whitespace endpoint");
        }
    }
}