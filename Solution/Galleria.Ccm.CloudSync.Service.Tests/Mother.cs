using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.CloudSync.Service.Models;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using Moq;

namespace Galleria.Ccm.CloudSync.Service.Tests
{
    internal static class Mother
    {
        public static Mock<IDalContext> SetUpDalContext(String dalName)
        {
            var dalFactory = new Mock<IDalFactory>();
            var dalContext = new Mock<IDalContext>();
            dalFactory.Setup(i => i.CreateContext()).Returns(dalContext.Object);
            DalContainer.Reset();
            DalContainer.RegisterFactory(dalName, dalFactory.Object);
            return dalContext;
        }

        public static Mock<IDalContext> ThatGetsDal<T>(this Mock<IDalContext> source, IMock<T> testDalPackage) where T : class, IDal
        {
            source.Setup(i => i.GetDal<T>()).Returns(testDalPackage.Object);
            return source;
        }

        public static Mock<IPackageDal> ThatFetchesIdsExceptFor(this Mock<IPackageDal> source, IEnumerable<IPlanInfo> newPlanInfos)
        {
            source.Setup(i => i.FetchIdsExceptFor(It.IsAny<IEnumerable<String>>()))
                  .Returns(newPlanInfos.Select(info => (Object) Guid.Parse(info.Ucr)).ToList());
            return source;
        }

        public static Mock<IPackageDal> ThatCanBeLocked(this Mock<IPackageDal> source)
        {
            source.Setup(i => i.LockById(It.IsAny<Object>(), It.IsAny<Object>(), It.IsAny<Byte>(), It.IsAny<Boolean>()))
                  .Returns((Byte) PackageLockResult.Success);
            return source;
        }

        public static Mock<IPackageDal> WhereFetchByIdReturns(this Mock<IPackageDal> source, Func<Object, Object, PackageDto> dtoCreator)
        {
            source.Setup(i => i.FetchById(It.IsAny<Object>(), It.IsAny<Object>())).Returns(dtoCreator);
            return source;
        }

        public static Mock<IPlanogramDal> WhereFetchByPackageIdReturns(this Mock<IPlanogramDal> source, Func<Object, IEnumerable<PlanogramDto>> planogramDtosByPackageId)
        {
            source.Setup(i => i.FetchByPackageId(It.IsAny<Object>()))
                  .Returns(planogramDtosByPackageId);
            return source;
        }

        public static Mock<IPackageDal> WhereFetchPackagesModifiedAfterReturns(this Mock<IPackageDal> source, IEnumerable<IPlanInfo> newPlanInfos)
        {
            source.Setup(i => i.FetchByUcrsDateLastModified(It.IsAny<IEnumerable<String>>(), It.IsAny<DateTime>()))
                  .Returns(newPlanInfos.Select(info => (Object)Guid.Parse(info.Ucr)).ToList());
            return source;
        }

    }
}