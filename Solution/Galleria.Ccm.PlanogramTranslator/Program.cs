﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// Previous history missing

// V8-27058 : A.Probyn
//  Updated references to updated meta data properties on PlanogramFixture
#endregion
#region Version history: CCM830
// V8-31531 : A.Heathcote   
//  Removed IsTrayProduct
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using GFSDAL = Galleria.Foundation.Dal.Interfaces;
using GFSDTO = Galleria.Foundation.Dal.DataTransferObjects;

namespace Galleria.Ccm.PlanogramTranslator
{
    public static class Program
    {
        const Int32 maxPlanCount = 15000;
        const Int32 maxProductCount = 200000;

        const String GFSDalName = "GFSMssql";
        public static String GFSServer { get; set; }
        public static String GFSDB { get; set; }
        public static Int32 GFSEntityId = 1;

        const String CCMDalName = "CCMMssql";
        public static String CCMServer { get; set; }
        public static String CCMDB { get; set; }
        public static Int32 CCMEntityId { get; set; }

        public static void Main(String[] args)
        {
            //read config
            GFSServer = ConfigurationManager.AppSettings["GFSServer"];
            GFSDB = ConfigurationManager.AppSettings["GFSDB"];
            GFSEntityId = Int32.Parse(ConfigurationManager.AppSettings["GFSEntityId"]);
            CCMServer = ConfigurationManager.AppSettings["CCMServer"];
            CCMDB = ConfigurationManager.AppSettings["CCMDB"];
            CCMEntityId = Int32.Parse(ConfigurationManager.AppSettings["CCMEntityId"]);


            //GFS
            Console.WriteLine("Registering GFS DAL - {0} {1} (EntityId: {2})", GFSServer, GFSDB, GFSEntityId);
            DalFactoryConfigElement dalDatabaseFactoryConfig = new DalFactoryConfigElement(GFSDalName);
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", GFSServer));
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", GFSDB));
            IDalFactory dalFactory = new Galleria.Foundation.Dal.Mssql.DalFactory(dalDatabaseFactoryConfig);
            DalContainer.RegisterFactory(GFSDalName, dalFactory);


            //CCM
            Console.WriteLine("Registering CCM DAL - {0} {1} (EntityId: {2})", CCMServer, CCMDB, CCMEntityId);
            dalDatabaseFactoryConfig = new DalFactoryConfigElement(CCMDalName);
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", CCMServer));
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", CCMDB));
            dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalDatabaseFactoryConfig);
            DalContainer.RegisterFactory(CCMDalName, dalFactory);


            DalContainer.DalName = CCMDalName;
            DomainPrincipal.Authenticate();

            PlanogramHelper.RegisterPlanogramImageRenderer<PlanogramImageRenderer>();

            //output functions
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Please select an option:");
                Console.WriteLine("1 - Exit");
                Console.WriteLine("2 - Copy Planograms");
                //Console.WriteLine("3 - Copy Products");
                //Console.WriteLine("4 - Copy Universes");
                Console.WriteLine("");

                String val = Console.ReadLine();
                Console.WriteLine("");

                switch (val)
                {
                    case "1": return;

                    case "2":
                        CopyPlans();
                        break;

                    //case "3":
                    //    CopyProducts();
                    //    break;

                    //case "4":
                    //    CopyProductUniverses();
                    //    break;
                }

            }
        }


        #region Copy Plans

        private static void CopyPlans()
        {

            Entity ccmEntity = Entity.FetchById(CCMEntityId);

            //clear the existing plan hierarchy and import from GFS
            Int32 hierarchyId = 1;
            Int32 rootId = 1;

            using (IDalFactory ccmDalFactory = DalContainer.GetDalFactory(CCMDalName))
            {
                using (IDalFactory gfsDalFactory = DalContainer.GetDalFactory(GFSDalName))
                {

                    Console.WriteLine("Clearing existing data");

                    PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(CCMEntityId);

                    foreach (var group in hierarchy.EnumerateAllGroups())
                    {
                        PlanogramInfoList planInfos = PlanogramInfoList.FetchByPlanogramGroupId(group.Id);
                        foreach (var info in planInfos)
                        {
                            try
                            {
                                Package pk = Package.FetchById(info.Id, (Int32)Galleria.Ccm.Security.DomainPrincipal.CurrentUserId, PackageLockType.User);
                                pk.Delete();
                                pk.Save();
                            }
                            catch (Exception) { }
                        }
                    }
                    hierarchy = null;




                    Console.WriteLine("Fetching plan groups");

                    List<GFSDTO.ProductGroupDto> gfsGroups = new List<GFSDTO.ProductGroupDto>();
                    using (IDalContext dalContext = gfsDalFactory.CreateContext())
                    {
                        GFSDTO.EntityDto gfsEntity = null;
                        using (GFSDAL.IEntityDal dal = dalContext.GetDal<GFSDAL.IEntityDal>())
                        {
                            gfsEntity = dal.FetchById(GFSEntityId);
                        }

                        using (GFSDAL.IProductGroupDal dal = dalContext.GetDal<GFSDAL.IProductGroupDal>())
                        {
                            gfsGroups = dal.FetchByProductHierarchyId(gfsEntity.MerchandisingHierarchyId).ToList();
                        }
                    }

                    var planGroups = new List<PlanogramGroupDto>();
                    using (IDalContext dalContext = ccmDalFactory.CreateContext())
                    {
                        using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                        {
                            hierarchyId = dal.FetchByEntityId(CCMEntityId).Id;
                        }

                        using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                        {
                            rootId = dal.FetchByPlanogramHierarchyId(hierarchyId).First(p => p.ParentGroupId == null).Id;

                            InsertChildGroups(dal, gfsGroups.First(g => g.ParentId == null), gfsGroups, rootId, hierarchyId);

                            planGroups = dal.FetchByPlanogramHierarchyId(hierarchyId).ToList();
                        }
                    }



                    Console.WriteLine("Fetching plans");


                    //fetch plan infos
                    List<GFSDTO.PlanogramInfoDto> gfsPlanInfos = new List<GFSDTO.PlanogramInfoDto>();
                    using (IDalContext dalContext = gfsDalFactory.CreateContext())
                    {
                        using (GFSDAL.IPlanogramInfoDal dal = dalContext.GetDal<GFSDAL.IPlanogramInfoDal>())
                        {
                            gfsPlanInfos = dal.FetchByEntityId(GFSEntityId).Where(p => p.IsLatestVersion)
                                .Take(maxPlanCount).ToList();
                        }
                    }

                    Int32 userId = (Int32)DomainPrincipal.CurrentUserId;

                    Int32 planCount = gfsPlanInfos.Count;
                    Int32 planNo = 1;
                    foreach (GFSDTO.PlanogramInfoDto gfsPlanInfo in gfsPlanInfos)
                    {
                        Console.WriteLine("Loading plan {0} of {1} - {2}", planNo, planCount, gfsPlanInfo.Name);

                        Int32 planId = 0;
                        String groupName = null;
                        try
                        {
                            GFSPlanogramConverter converter = new GFSPlanogramConverter(ccmEntity);
                            Planogram ccmPlan = converter.LoadCCMPlan(gfsDalFactory, gfsPlanInfo);
                            groupName = converter.PlanogramGroupName;

                            //save the ccm plan.
                            Package pk = Package.NewPackage(userId, PackageLockType.User);
                            pk.Name = ccmPlan.Name;
                            pk.Planograms.Add(ccmPlan);
                            pk = pk.SaveAs(DomainPrincipal.CurrentUserId, ccmEntity.Id);

                            planId = (Int32)pk.Planograms[0].Id;

                            pk.Dispose();
                            pk = null;

                            converter = null;
                            ccmPlan = null;


                            //refetch the plan and update its meta data to save the service the bother of having to do it again later
                            //pk = Package.FetchById(pk.Id, (Int32)Galleria.Ccm.Security.DomainPrincipal.CurrentUserId, PackageLockType.User);
                            //pk.CalculateMetadata();
                            //pk.CalculateValidationData();
                            //pk.Save();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("--- FAILED TO CREATE PLAN");
                        }

                        //associate the plan with a group
                        if (!String.IsNullOrEmpty(groupName) && planId != 0)
                        {
                            try
                            {
                                PlanogramGroupDto planGroup = planGroups.FirstOrDefault(p => p.Name == groupName);
                                if (planGroup == null)
                                {
                                    Console.WriteLine("--- No plan group found");
                                    planGroup = planGroups.FirstOrDefault(g => g.ParentGroupId == null);
                                }
                                PlanogramGroup.AssociatePlanograms(planGroup.Id, new List<Int32> { planId });

                                planGroup = null;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("--- FAILED TO ASSOCIATE PLAN");
                            }
                        }

                        planNo++;
                    }

                }
            }
        }

        private static void InsertChildGroups(IPlanogramGroupDal dal, GFSDTO.ProductGroupDto gfsGroup, List<GFSDTO.ProductGroupDto> allGFSGroups,
            Int32 ccmParentGroupId, Int32 ccmParentHierarchyId)
        {
            foreach (GFSDTO.ProductGroupDto gfsChild in allGFSGroups)
            {
                if (gfsChild.ParentId == gfsGroup.Id)
                {
                    PlanogramGroupDto planGroupDto = new PlanogramGroupDto();
                    planGroupDto.Name = gfsChild.Code + ' ' + gfsChild.Name;
                    planGroupDto.ParentGroupId = ccmParentGroupId;
                    planGroupDto.PlanogramHierarchyId = ccmParentHierarchyId;
                    planGroupDto.DateCreated = gfsChild.DateCreated;
                    planGroupDto.DateLastModified = gfsChild.DateLastModified;

                    try
                    {
                        dal.Insert(planGroupDto);
                    }
                    catch (Exception ex)
                    {
                        Console.Out.WriteLine(ex);
                    }

                    InsertChildGroups(dal, gfsChild, allGFSGroups, planGroupDto.Id, ccmParentHierarchyId);
                }
            }
        }
        #endregion

       

        #region OLD

        //public static void Main(String[] args)
        //{








        //    //using (System.Security.Cryptography.AesManaged aes = new System.Security.Cryptography.AesManaged())
        //    //{
        //    //    Console.WriteLine(aes.KeySize);
        //    //    aes.GenerateKey();
        //    //    Console.Write("{ ");
        //    //    for (Int32 i = 0; i < aes.Key.Length; i++)
        //    //    {
        //    //        Console.Write(aes.Key[i]);
        //    //        if (i < aes.Key.Length - 1)
        //    //        {
        //    //            Console.Write(", ");
        //    //        }
        //    //    }
        //    //    Console.Write(" };");
        //    //}
        //    //Console.WriteLine(typeof(Byte[]).Name);
        //    Galleria.Ccm.Security.DomainPrincipal.Authenticate();
        //    String fileName = args[0];
        //    //Package.LockPackageByFileName(fileName);
        //    Package package = Package.FetchByFileName(fileName);
        //    foreach (PlanogramProduct product in package.Planograms[0].Products)
        //    {
        //        PlanogramImage image1 = PlanogramImage.NewPlanogramImage();
        //        image1.FileName = "Test.jpg";
        //        image1.Description = "Test";
        //        image1.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        PlanogramImage image2 = PlanogramImage.NewPlanogramImage();
        //        image2.FileName = "Test.jpg";
        //        image2.Description = "Test";
        //        image2.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        PlanogramImage image3 = PlanogramImage.NewPlanogramImage();
        //        image3.FileName = "Test.jpg";
        //        image3.Description = "Test";
        //        image3.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        PlanogramImage image4 = PlanogramImage.NewPlanogramImage();
        //        image4.FileName = "Test.jpg";
        //        image4.Description = "Test";
        //        image4.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        PlanogramImage image5 = PlanogramImage.NewPlanogramImage();
        //        image5.FileName = "Test.jpg";
        //        image5.Description = "Test";
        //        image5.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        PlanogramImage image6 = PlanogramImage.NewPlanogramImage();
        //        image6.FileName = "Test.jpg";
        //        image6.Description = "Test";
        //        image6.ImageData = File.ReadAllBytes("Jack5.jpg");
        //        package.Planograms[0].Images.Add(image1);
        //        package.Planograms[0].Images.Add(image2);
        //        package.Planograms[0].Images.Add(image3);
        //        package.Planograms[0].Images.Add(image4);
        //        package.Planograms[0].Images.Add(image5);
        //        package.Planograms[0].Images.Add(image6);
        //        product.PlanogramImageIdFront = image1.Id;
        //        product.PlanogramImageIdBack = image2.Id;
        //        product.PlanogramImageIdTop = image3.Id;
        //        product.PlanogramImageIdBottom = image4.Id;
        //        product.PlanogramImageIdLeft = image5.Id;
        //        product.PlanogramImageIdRight = image6.Id;
        //    }
        //    Console.WriteLine(package.Planograms[0].Images.Count);
        //    package.Save();
        //    //Package.UnlockPackageByFileName(fileName);
        //    //String fileName = args[0] + ".pog";
        //    //FM.Package.LockPackageByFileName(fileName);
        //    //FM.Package package = FM.Package.NewPackage(fileName);
        //    //package.Name = Path.GetDirectoryName(args[0]);
        //    //foreach (String planDirectory in Directory.GetDirectories(args[0]))
        //    //{
        //    //    CM.Planogram ccmPlanogram = CM.Planogram.FetchByFolderPath(planDirectory + "\\");
        //    //    FM.Planogram frameworkPlanogram = FM.Planogram.NewPlanogram();
        //    //    package.Planograms.Add(frameworkPlanogram);
        //    //    CM.PlanogramImageList ccmImages = CM.PlanogramImageList.FetchByPlanogramFolderPath(planDirectory + "\\");
        //    //    CopyContents(ccmPlanogram, ccmImages, frameworkPlanogram);

        //    //}
        //    //package.Save();
        //    //package.Planograms[0].FixtureItems.Add(package.Planograms[0].FixtureItems[0].Copy());
        //    //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        //    //stopwatch.Start();
        //    //package.Save();
        //    //stopwatch.Stop();
        //    //Console.WriteLine(stopwatch.ElapsedMilliseconds);
        //    //FM.Package.UnlockPackageByFileName(fileName);
        //    //FM.Package.LockPackageByFileName(fileName);
        //    //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        //    //stopwatch.Start();
        //    //FM.Package package = FM.Package.FetchByFileName(fileName);
        //    //FM.PlanogramImageList foo = package.Planograms[0].ImagesAsync;
        //    //foreach (FM.PlanogramImage image in package.Planograms[0].ImagesAsync)
        //    //{
        //    //    Console.WriteLine(image.ImageData.Length);
        //    //}
        //    //foreach (FM.PlanogramPosition position in package.Planograms[0].Positions)
        //    //{
        //    //    if ((Int32?)position.PlanogramFixtureComponentId == 0)
        //    //    {
        //    //        System.Diagnostics.Debugger.Break();
        //    //    }
        //    //}
        //    //Int32 count = 0;
        //    //foreach (FM.PlanogramImage image in package.Planograms[0].Images)
        //    //{
        //    //    // Just checking how long it takes to load the images.
        //    //    count++;
        //    //}
        //    //stopwatch.Stop();
        //    ////Directory.CreateDirectory(Path.Combine(args[0], "ImagesOut"));
        //    ////foreach (FM.PlanogramImage image in package.Planograms[0].Images)
        //    ////{
        //    ////    String piccy = Path.Combine(Path.Combine(args[0], "ImagesOut"), image.Id.ToString() + ".jpg");
        //    ////    File.WriteAllBytes(piccy, image.ImageData);
        //    ////}
        //    //Console.WriteLine(String.Format("Image Load: {0}", stopwatch.ElapsedMilliseconds));
        //    //M.Package.UnlockPackageByFileName(fileName);
        //    Console.ReadKey();
        //}

        //private static void CopyContents(CM.Planogram from, CM.PlanogramImageList fromImages, FM.Planogram to)
        //{

        //    Dictionary<String, String> propertyNameMappings = new Dictionary<String, String>();
        //    propertyNameMappings["Gtin"] = "GTIN";
        //    propertyNameMappings["RetailerReferenceCode"] = "RetailerReference";
        //    CopyPropertyValues(from, to, propertyNameMappings);
        //    foreach (CM.PlanogramImage fromImage in fromImages)
        //    {
        //        FM.PlanogramImage toImage = FM.PlanogramImage.NewPlanogramImage();
        //        CopyPropertyValues(fromImage, toImage, propertyNameMappings);
        //        to.Images.Add(toImage);
        //    }
        //    foreach (CM.PlanogramComponent fromComponent in from.Components)
        //    {
        //        FM.PlanogramComponent toComponent = FM.PlanogramComponent.NewPlanogramComponent();
        //        CopyPropertyValues(fromComponent, toComponent, propertyNameMappings);
        //        to.Components.Add(toComponent);
        //        foreach (CM.PlanogramSubComponent fromSubComponent in fromComponent.SubComponents)
        //        {
        //            FM.PlanogramSubComponent toSubComponent = FM.PlanogramSubComponent.NewPlanogramSubComponent();
        //            CopyPropertyValues(fromSubComponent, toSubComponent, propertyNameMappings);
        //            toComponent.SubComponents.Add(toSubComponent);
        //        }
        //    }
        //    foreach (CM.PlanogramAssembly fromAssembly in from.Assemblies)
        //    {
        //        FM.PlanogramAssembly toAssembly = FM.PlanogramAssembly.NewPlanogramAssembly();
        //        CopyPropertyValues(fromAssembly, toAssembly, propertyNameMappings);
        //        to.Assemblies.Add(toAssembly);
        //        foreach (CM.PlanogramAssemblyComponent fromAssemblyComponent in fromAssembly.Components)
        //        {
        //            FM.PlanogramAssemblyComponent toAssemblyComponent = FM.PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
        //            CopyPropertyValues(fromAssemblyComponent, toAssemblyComponent, propertyNameMappings);
        //            toAssembly.Components.Add(toAssemblyComponent);
        //        }
        //    }
        //    foreach (CM.PlanogramFixture fromFixture in from.Fixtures)
        //    {
        //        FM.PlanogramFixture toFixture = FM.PlanogramFixture.NewPlanogramFixture();
        //        CopyPropertyValues(fromFixture, toFixture, propertyNameMappings);
        //        to.Fixtures.Add(toFixture);
        //        foreach (CM.PlanogramFixtureAssembly fromFixtureAssembly in fromFixture.Assemblies)
        //        {
        //            FM.PlanogramFixtureAssembly toFixtureAssembly = FM.PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
        //            CopyPropertyValues(fromFixtureAssembly, toFixtureAssembly, propertyNameMappings);
        //            toFixture.Assemblies.Add(toFixtureAssembly);
        //        }
        //        foreach (CM.PlanogramFixtureComponent fromFixtureComponent in fromFixture.Components)
        //        {
        //            FM.PlanogramFixtureComponent toFixtureComponent = FM.PlanogramFixtureComponent.NewPlanogramFixtureComponent();
        //            CopyPropertyValues(fromFixtureComponent, toFixtureComponent, propertyNameMappings);
        //            toFixture.Components.Add(toFixtureComponent);
        //        }
        //    }
        //    foreach (CM.PlanogramFixtureItem fromFixtureItem in from.FixtureItems)
        //    {
        //        FM.PlanogramFixtureItem toFixtureItem = FM.PlanogramFixtureItem.NewPlanogramFixtureItem();
        //        CopyPropertyValues(fromFixtureItem, toFixtureItem, propertyNameMappings);
        //        to.FixtureItems.Add(toFixtureItem);
        //    }
        //    foreach (CM.PlanogramProduct fromProduct in from.Products)
        //    {
        //        FM.PlanogramProduct toProduct = FM.PlanogramProduct.NewPlanogramProduct();
        //        CopyPropertyValues(fromProduct, toProduct, propertyNameMappings);
        //        to.Products.Add(toProduct);
        //    }
        //    foreach (CM.PlanogramPosition fromPosition in from.Positions)
        //    {
        //        FM.PlanogramPosition toPosition = FM.PlanogramPosition.NewPlanogramPosition();
        //        CopyPropertyValues(fromPosition, toPosition, propertyNameMappings);
        //        to.Positions.Add(toPosition);
        //    }
        //    foreach (CM.PlanogramAnnotation fromAnnotation in from.Annotations)
        //    {
        //        FM.PlanogramAnnotation toAnnotation = FM.PlanogramAnnotation.NewPlanogramAnnotation();
        //        CopyPropertyValues(fromAnnotation, toAnnotation, propertyNameMappings);
        //        to.Annotations.Add(toAnnotation);
        //    }
        //}

        //private static void CopyPropertyValues(
        //    Object from, 
        //    Object to, 
        //    Dictionary<String, String> propertyNameMappings)
        //{
        //    Type fromType = from.GetType();
        //    Type toType = to.GetType();
        //    PropertyInfo fromIdProperty = fromType.GetProperty("Id");
        //    if (fromIdProperty == null)
        //    {
        //        fromIdProperty = fromType.GetProperty(fromType.Name + "Id");
        //    }
        //    PropertyInfo toIdProperty = toType.GetProperty("Id");
        //    toIdProperty.SetValue(to, fromIdProperty.GetValue(from, null), null);
        //    foreach (PropertyInfo toProperty in toType.GetProperties().Where(p => p.CanWrite && p.Name != "Id"))
        //    {
        //        PropertyInfo fromProperty = fromType.GetProperty(toProperty.Name);
        //        if (fromProperty == null)
        //        {
        //            fromProperty = fromType.GetProperty("Linked" + toProperty.Name);
        //        }
        //        if ((fromProperty == null) && (propertyNameMappings.ContainsKey(toProperty.Name)))
        //        {
        //            fromProperty = fromType.GetProperty(propertyNameMappings[toProperty.Name]);
        //        }
        //        if (fromProperty != null)
        //        {
        //            if (fromProperty.PropertyType == toProperty.PropertyType)
        //            {
        //                toProperty.SetValue(
        //                    to,
        //                    fromProperty.GetValue(from, null),
        //                    null);
        //            }
        //            else
        //            {
        //                toProperty.SetValue(
        //                    to,
        //                    ChangeType(
        //                        fromProperty.GetValue(from, null),
        //                        toProperty.PropertyType),
        //                    null);
        //            }
        //        }
        //        else
        //        {
        //            if (toProperty.Name != "IsProductOverlapAllowed")
        //            {
        //                Console.WriteLine(String.Format("{0}.{1}", toType.Name, toProperty.Name));
        //            }
        //        }
        //    }
        //}

        //private static Object ChangeType(Object value, Type type)
        //{
        //    if (value == null)
        //    {
        //        return value;
        //    }
        //    if (type.IsEnum)
        //    {
        //        Int32 intValue = Convert.ToInt32(value);
        //        return Enum.ToObject(type, intValue);
        //    }
        //    else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        //    {
        //        return Activator.CreateInstance(type, Convert.ChangeType(value, type.GetGenericArguments()[0]));
        //    }
        //    else
        //    {
        //        return Convert.ChangeType(value, type);
        //    }
        //}

        //#region Copy Products

        //private static void CopyProducts()
        //{
        //    List<ProductDto> ccmProducts = new List<ProductDto>();

        //    using (IDalFactory ccmDalFactory = DalContainer.GetDalFactory(CCMDalName))
        //    {

        //        using (IDalFactory gfsDalFactory = DalContainer.GetDalFactory(GFSDalName))
        //        {
        //            Console.WriteLine("Fetching products from entity {0}", GFSEntityId);

        //            List<GFSDTO.ProductInfoDto> gfsProducts;
        //            using (IDalContext dalContext = gfsDalFactory.CreateContext())
        //            {
        //                using (GFSDAL.IProductInfoDal dal = dalContext.GetDal<GFSDAL.IProductInfoDal>())
        //                {
        //                    gfsProducts = dal.FetchByEntityId(GFSEntityId).Take(maxProductCount).ToList();
        //                }
        //            }

        //            Console.WriteLine("Found {0} Products", gfsProducts.Count);
        //            if (gfsProducts.Count == 0) { return; }

        //            Int32 prodNo = 1;
        //            foreach (GFSDTO.ProductInfoDto dto in gfsProducts)
        //            {
        //                //fetch the product
        //                GFSDTO.ProductDto gfsProduct;
        //                GFSDTO.ProductAttributeDataDto gfsProductAttribute;

        //                using (IDalContext dalContext = gfsDalFactory.CreateContext())
        //                {
        //                    using (GFSDAL.IProductDal dal = dalContext.GetDal<GFSDAL.IProductDal>())
        //                    {
        //                        gfsProduct = dal.FetchById(dto.Id);
        //                    }
        //                    using (GFSDAL.IProductAttributeDataDal dal = dalContext.GetDal<GFSDAL.IProductAttributeDataDal>())
        //                    {
        //                        gfsProductAttribute = dal.FetchByProductId(dto.Id);
        //                    }
        //                }


        //                //insert
        //                ProductDto ccmProduct = new ProductDto();
        //                ccmProduct.EntityId = CCMEntityId;
        //                ccmProduct.Gtin = gfsProduct.GTIN;
        //                ccmProduct.AlternateDepth = gfsProduct.AlternateDepth;
        //                ccmProduct.AlternateHeight = gfsProduct.AlternateHeight;
        //                ccmProduct.AlternateWidth = gfsProduct.AlternateWidth;
        //                ccmProduct.CanBreakTrayBack = gfsProduct.CanBreakTrayBack;
        //                ccmProduct.CanBreakTrayDown = gfsProduct.CanBreakTrayDown;
        //                ccmProduct.CanBreakTrayTop = gfsProduct.CanBreakTrayTop;
        //                ccmProduct.CanBreakTrayUp = gfsProduct.CanBreakTrayUp;
        //                ccmProduct.CanMerchandiseOnBar = gfsProduct.CanMerchandiseOnBar;
        //                ccmProduct.CanMerchandiseInChest = gfsProduct.CanMerchandiseInChest;
        //                ccmProduct.CanMerchandiseOnPeg = gfsProduct.CanMerchandiseOnPeg;
        //                ccmProduct.CanMerchandiseOnShelf = gfsProduct.CanMerchandiseOnShelf;
        //                ccmProduct.CasePackUnits = gfsProduct.CasePackUnits;
        //                ccmProduct.CaseHigh = gfsProduct.CaseHigh;
        //                ccmProduct.CaseWide = gfsProduct.CaseWide;
        //                ccmProduct.CaseDeep = gfsProduct.CaseDeep;
        //                ccmProduct.CaseHeight = gfsProduct.CaseHeight;
        //                ccmProduct.CaseWidth = gfsProduct.CaseWidth;
        //                ccmProduct.CaseDepth = gfsProduct.CaseDepth;
        //                ccmProduct.Depth = gfsProduct.Depth;
        //                ccmProduct.DisplayDepth = gfsProduct.DisplayDepth;
        //                ccmProduct.DisplayHeight = gfsProduct.DisplayHeight;
        //                ccmProduct.DisplayWidth = gfsProduct.DisplayWidth;
        //                ccmProduct.FingerSpaceAbove = gfsProduct.FingerSpaceAbove;
        //                ccmProduct.FingerSpaceToTheSide = gfsProduct.FingerSpaceToTheSide;
        //                ccmProduct.ForceBottomCap = gfsProduct.ForceBottomCap;
        //                ccmProduct.ForceMiddleCap = gfsProduct.ForceMiddleCap;
        //                ccmProduct.FrontOverhang = gfsProduct.FrontOverhang;
        //                ccmProduct.Height = gfsProduct.Height;
        //                ccmProduct.IsActive = gfsProduct.IsActive;
        //                ccmProduct.IsFrontOnly = gfsProduct.IsFrontOnly;
        //                ccmProduct.IsPlaceHolderProduct = gfsProduct.IsPlaceHolderProduct;
        //                //ccmProduct.IsRestOfMarket = false;
        //                ccmProduct.IsTrayProduct = gfsProduct.IsTrayProduct;
        //                ccmProduct.MaxDeep = gfsProduct.MaxDeep;
        //                ccmProduct.MaxNestingDeep = gfsProduct.MaxNestingDeep;
        //                ccmProduct.MaxNestingHigh = gfsProduct.MaxNestingHigh;
        //                ccmProduct.MaxRightCap = gfsProduct.MaxRightCap;
        //                ccmProduct.MaxStack = gfsProduct.MaxStack;
        //                ccmProduct.MaxTopCap = gfsProduct.MaxTopCap;
        //                ccmProduct.MerchandisingStyle = gfsProduct.MerchandisingStyle;
        //                ccmProduct.MinDeep = gfsProduct.MinDeep;
        //                ccmProduct.Name = gfsProduct.Name;
        //                ccmProduct.NestingDepth = gfsProduct.NestingDepth;
        //                ccmProduct.NestingHeight = gfsProduct.NestingHeight;
        //                ccmProduct.NestingWidth = gfsProduct.NestingWidth;
        //                ccmProduct.NumberOfPegHoles = gfsProduct.NumberOfPegHoles;
        //                ccmProduct.OrientationType = gfsProduct.OrientationType;
        //                ccmProduct.PegDepth = gfsProduct.PegDepth;
        //                ccmProduct.PegProngOffset = gfsProduct.PegProngOffset;
        //                ccmProduct.PegX = gfsProduct.PegX;
        //                ccmProduct.PegX2 = gfsProduct.PegX2;
        //                ccmProduct.PegX3 = gfsProduct.PegX3;
        //                ccmProduct.PegY = gfsProduct.PegY;
        //                ccmProduct.PegY2 = gfsProduct.PegY2;
        //                ccmProduct.PegY3 = gfsProduct.PegY3;
        //                ccmProduct.PointOfPurchaseDepth = gfsProduct.PointOfPurchaseDepth;
        //                ccmProduct.PointOfPurchaseHeight = gfsProduct.PointOfPurchaseHeight;
        //                ccmProduct.PointOfPurchaseWidth = gfsProduct.PointOfPurchaseWidth;
        //                ccmProduct.SqueezeDepth = gfsProduct.SqueezeDepth;
        //                ccmProduct.SqueezeHeight = gfsProduct.SqueezeHeight;
        //                ccmProduct.SqueezeWidth = gfsProduct.SqueezeWidth;
        //                ccmProduct.StatusType = gfsProduct.StatusType;
        //                ccmProduct.TrayDeep = gfsProduct.TrayDeep;
        //                ccmProduct.TrayDepth = gfsProduct.TrayDepth;
        //                ccmProduct.TrayHigh = gfsProduct.TrayHigh;
        //                ccmProduct.TrayHeight = gfsProduct.TrayHeight;
        //                ccmProduct.TrayPackUnits = gfsProduct.TrayPackUnits;
        //                ccmProduct.TrayThickDepth = gfsProduct.TrayThickDepth;
        //                ccmProduct.TrayThickHeight = gfsProduct.TrayThickHeight;
        //                ccmProduct.TrayThickWidth = gfsProduct.TrayThickWidth;
        //                ccmProduct.TrayWide = gfsProduct.TrayWide;
        //                ccmProduct.TrayWidth = gfsProduct.TrayWidth;
        //                ccmProduct.Width = gfsProduct.Width;
        //                ccmProduct.Barcode = gfsProductAttribute.BarCode;
        //                ccmProduct.Brand = gfsProductAttribute.Brand;
        //                ccmProduct.CaseCost = gfsProductAttribute.CaseCost;
        //                ccmProduct.Colour = gfsProductAttribute.Colour;
        //                ccmProduct.ConsumerInformation = gfsProductAttribute.ConsumerInformation;
        //                ccmProduct.CorporateCode = gfsProductAttribute.CorporateCode;
        //                ccmProduct.CostPrice = gfsProductAttribute.CostPrice;
        //                ccmProduct.CountryOfOrigin = gfsProductAttribute.CountryOfOrigin;
        //                ccmProduct.CountryOfProcessing = gfsProductAttribute.CountryOfProcessing;
        //                ccmProduct.CustomerStatus = gfsProductAttribute.CustomerStatus;
        //                ccmProduct.DateDiscontinued = gfsProductAttribute.DateDiscontinued;
        //                ccmProduct.DateEffective = gfsProductAttribute.DateEffective;
        //                ccmProduct.DateIntroduced = gfsProductAttribute.DateIntroduced;
        //                //ccmProduct.DeliveryFrequencyDays =gfsProductAttribute.DeliveryFrequencyDays;
        //                ccmProduct.DeliveryMethod = gfsProductAttribute.DeliveryMethod;
        //                ccmProduct.Flavour = gfsProductAttribute.Flavour;
        //                ccmProduct.GarmentType = gfsProductAttribute.GarmentType;
        //                ccmProduct.Health = gfsProductAttribute.Health;
        //                //ccmProduct.IsNew =gfsProductAttribute.IsNewProduct;
        //                ccmProduct.IsPrivateLabel = gfsProductAttribute.IsPrivateLabel;
        //                ccmProduct.Manufacturer = gfsProductAttribute.Manufacturer;
        //                ccmProduct.ManufacturerCode = gfsProductAttribute.ManufacturerCode;
        //                ccmProduct.ManufacturerRecommendedRetailPrice = gfsProductAttribute.ManufacturerRecommendedRetailPrice;
        //                ccmProduct.Model = gfsProductAttribute.Model;
        //                ccmProduct.PackagingShape = gfsProductAttribute.PackagingShape;
        //                ccmProduct.PackagingType = gfsProductAttribute.PackagingType;
        //                ccmProduct.Pattern = gfsProductAttribute.Pattern;
        //                ccmProduct.PointOfPurchaseDescription = gfsProductAttribute.PointOfPurchaseDescription;
        //                ccmProduct.RecommendedRetailPrice = gfsProductAttribute.RecommendedRetailPrice;
        //                ccmProduct.SellPackCount = gfsProductAttribute.SellPackCount;
        //                ccmProduct.SellPackDescription = gfsProductAttribute.SellPackDescription;
        //                ccmProduct.SellPrice = gfsProductAttribute.SellPrice;
        //                ccmProduct.ShelfLife = gfsProductAttribute.ShelfLife;
        //                ccmProduct.ShortDescription = gfsProductAttribute.ShortDescription;
        //                ccmProduct.Size = gfsProductAttribute.Size;
        //                ccmProduct.StyleNumber = gfsProductAttribute.StyleNumber;
        //                ccmProduct.Subcategory = gfsProductAttribute.SubCategory;
        //                ccmProduct.TaxRate = gfsProductAttribute.TaxRate;
        //                ccmProduct.Texture = gfsProductAttribute.Texture;
        //                ccmProduct.UnitOfMeasure = gfsProductAttribute.UnitOfMeasure;
        //                ccmProduct.Vendor = gfsProductAttribute.Vendor;
        //                ccmProduct.VendorCode = gfsProductAttribute.VendorCode;

        //                ccmProducts.Add(ccmProduct);


        //                if (ccmProducts.Count > 500)
        //                {
        //                    Console.WriteLine("Inserting {0}", prodNo);

        //                    using (IDalContext dalContext = ccmDalFactory.CreateContext())
        //                    {
        //                        using (IProductDal dal = dalContext.GetDal<IProductDal>())
        //                        {
        //                            dal.Upsert(ccmProducts, new ProductIsSetDto(true));
        //                        }
        //                    }
        //                    ccmProducts.Clear();
        //                }

        //                prodNo++;
        //            }

        //            //insert the rest
        //            using (IDalContext dalContext = ccmDalFactory.CreateContext())
        //            {
        //                Console.WriteLine("Inserting final");
        //                using (IProductDal dal = dalContext.GetDal<IProductDal>())
        //                {
        //                    dal.Upsert(ccmProducts, new ProductIsSetDto(true));
        //                }
        //                ccmProducts.Clear();
        //            }

        //        }



        //    }

        //}

        //#endregion

        //#region Copy Product Universes

        //private static void CopyProductUniverses()
        //{
        //    using (IDalFactory ccmDalFactory = DalContainer.GetDalFactory(CCMDalName))
        //    {
        //        Dictionary<String, ProductGroupDto> groupLookup = new Dictionary<string, ProductGroupDto>();
        //        using (IDalContext ccmDalContext = ccmDalFactory.CreateContext())
        //        {
        //            using (IProductGroupDal dal = ccmDalContext.GetDal<IProductGroupDal>())
        //            {
        //                groupLookup = dal.FetchByProductHierarchyId(1).ToDictionary(p => p.Code);
        //            }
        //        }

        //        using (IDalFactory gfsDalFactory = DalContainer.GetDalFactory(GFSDalName))
        //        {
        //            List<GFSDTO.ProductUniverseInfoDto> gfsUniverses;
        //            using (IDalContext gfsDalContext = gfsDalFactory.CreateContext())
        //            {
        //                using (GFSDAL.IProductUniverseInfoDal dal = gfsDalContext.GetDal<GFSDAL.IProductUniverseInfoDal>())
        //                {
        //                    gfsUniverses = dal.FetchByEntityId(GFSEntityId).Where(p => p.IsLatestVersion).ToList();
        //                }

        //                foreach (var gfsUniverseInfo in gfsUniverses)
        //                {
        //                    GFSDTO.ProductUniverseDto gfsUniverseDto;
        //                    IEnumerable<GFSDTO.ProductUniverseProductDto> gfsUniverseProductDtos;
        //                    IEnumerable<GFSDTO.ProductInfoDto> gfsUniverseProductInfosDtos;
        //                    GFSDTO.ProductGroupDto gfsGroup;

        //                    using (GFSDAL.IProductUniverseDal dal = gfsDalContext.GetDal<GFSDAL.IProductUniverseDal>())
        //                    {
        //                        gfsUniverseDto = dal.FetchById(gfsUniverseInfo.Id);
        //                    }
        //                    using (GFSDAL.IProductUniverseProductDal dal = gfsDalContext.GetDal<GFSDAL.IProductUniverseProductDal>())
        //                    {
        //                        gfsUniverseProductDtos = dal.FetchByProductUniverseId(gfsUniverseInfo.Id);
        //                    }
        //                    using (GFSDAL.IProductInfoDal dal = gfsDalContext.GetDal<GFSDAL.IProductInfoDal>())
        //                    {
        //                        gfsUniverseProductInfosDtos = dal.FetchByProductUniverseId(gfsUniverseInfo.Id);
        //                    }
        //                    using (GFSDAL.IProductGroupDal dal = gfsDalContext.GetDal<GFSDAL.IProductGroupDal>())
        //                    {
        //                        gfsGroup = dal.FetchById(gfsUniverseInfo.ProductGroupId.Value);
        //                    }




        //                    using (IDalContext ccmDalContext = ccmDalFactory.CreateContext())
        //                    {
        //                        ccmDalContext.Begin();

        //                        IEnumerable<ProductInfoDto> ccmProducts;
        //                        using (IProductInfoDal dal = ccmDalContext.GetDal<IProductInfoDal>())
        //                        {
        //                            ccmProducts = dal.FetchByEntityIdProductGtins(1, gfsUniverseProductInfosDtos.Select(g => g.GTIN));
        //                        }


        //                        ProductUniverseDto ccmUniverse = new ProductUniverseDto()
        //                        {
        //                            EntityId = CCMEntityId,
        //                            ProductGroupId = groupLookup[gfsGroup.Code].Id,
        //                            Name = gfsUniverseDto.Name,
        //                            DateCreated = DateTime.Now,
        //                            DateLastModified = DateTime.Now,
        //                            IsMaster = false,
        //                            UniqueContentReference = gfsUniverseDto.UniqueContentReference,
        //                        };
        //                        using (IProductUniverseDal dal = ccmDalContext.GetDal<IProductUniverseDal>())
        //                        {
        //                            dal.Insert(ccmUniverse);
        //                        }

        //                        using (IProductUniverseProductDal dal = ccmDalContext.GetDal<IProductUniverseProductDal>())
        //                        {
        //                            using (IProductDal prodDal = ccmDalContext.GetDal<IProductDal>())
        //                            {
        //                                foreach (var gfsProduct in gfsUniverseProductDtos)
        //                                {
        //                                    ProductInfoDto ccmProd = ccmProducts.FirstOrDefault(c => c.Gtin == gfsProduct.GTIN);
        //                                    if (ccmProd == null)
        //                                    {
        //                                        ProductDto proddto = new ProductDto()
        //                                        {
        //                                            Name = gfsProduct.Name,
        //                                            Gtin = gfsProduct.GTIN,
        //                                            EntityId = CCMEntityId
        //                                        };
        //                                        prodDal.Insert(proddto);
        //                                        ccmProd = new ProductInfoDto() { Id = proddto.Id };
        //                                    }


        //                                    ProductUniverseProductDto productDto = new ProductUniverseProductDto()
        //                                    {
        //                                        Gtin = gfsProduct.GTIN,
        //                                        Name = gfsProduct.Name,
        //                                        ProductUniverseId = ccmUniverse.Id,
        //                                        ProductId = ccmProd.Id
        //                                    };
        //                                    dal.Insert(productDto);
        //                                }
        //                            }
        //                        }


        //                        ccmDalContext.Commit();
        //                    }
        //                }

        //            }

        //        }
        //    }
        //}

        //#endregion

        #endregion
    }

    /// <summary>
    /// TEMPORY
    /// Converts a folder of GFS Planogram csv files to 
    /// the new CCM model
    /// </summary>
    internal sealed class GFSPlanogramConverter
    {
        #region Fields

       private Planogram _plan;
        private Entity _entity;
        private GfsToCcmPlanogramConverter _converter;

        #endregion

        #region Properties

        public Planogram Plan
        {
            get { return _plan; }
        }

        public String PlanogramGroupName { get; set; }

        #endregion

        #region Constructor

        public GFSPlanogramConverter(Entity entity)
        {
            _entity = entity;
            _converter = new GfsToCcmPlanogramConverter(_entity);
        }

        #endregion

        #region Methods

        public Planogram LoadCCMPlan(IDalFactory gfsDalFactory, GFSDTO.PlanogramInfoDto infoDto)
        {
            Import(gfsDalFactory, infoDto);
            _plan = _converter.Convert();
            
            return _plan;
        }

        #region Import from GFS

        private void Import(IDalFactory gfsDalFactory, GFSDTO.PlanogramInfoDto infoDto)
        {
            Dictionary<Object, String> componentNewIdToColour = new Dictionary<Object, String>();

            using (IDalContext dalContext = gfsDalFactory.CreateContext())
            {
                _converter.IsMetric = GetIsMetric(dalContext, infoDto.EntityId);

                ImportPlanogram(dalContext, infoDto.Id);
                ImportProducts(dalContext, infoDto.Id);
                ImportFixtures(dalContext, infoDto.Id);
                ImportFixtureItems(dalContext, infoDto.Id);
                ImportAssemblies(dalContext, infoDto.Id);
                ImportFixtureAssemblies(dalContext, _converter.GfsFixtures.Select(f => f.Id).ToList());
                ImportComponents(dalContext, infoDto.Id);
                ImportAssemblyComponents(dalContext, _converter.GfsAssemblies.Select(f => f.Id).ToList());
                ImportSubComponents(dalContext, _converter.GfsComponents.Select(c => c.Id).ToList());
                ImportPositions(dalContext, infoDto.Id);
                ImportAnnotations(dalContext, infoDto.Id);


                if (_converter.GfsPlanogram.ProductGroupId.HasValue)
                {
                    GetMerchGroup(dalContext, _converter.GfsPlanogram.ProductGroupId.Value);
                    this.PlanogramGroupName = _converter.GfsProductGroup.Code + ' ' + _converter.GfsProductGroup.Name;
                }
            }
        }

        private static Boolean GetIsMetric(IDalContext dalContext, Int32 entityId)
        {
            GFSDTO.SystemSettingsDto dto = new GFSDTO.SystemSettingsDto();
            using (GFSDAL.ISystemSettingsDal dal = dalContext.GetDal<GFSDAL.ISystemSettingsDal>())
            {
                dto = dal.FetchByEntityId(entityId).FirstOrDefault(s => s.Key == "DataSourceUnitOfMeasure");
            }

            if (dto != null && dto.Value == "Inches") return false;
            else return true;
        }

        private void ImportPlanogram(IDalContext dalContext, Int32 planId)
        {
            GFSDTO.PlanogramDto dto = new GFSDTO.PlanogramDto();
            using (GFSDAL.IPlanogramDal dal = dalContext.GetDal<GFSDAL.IPlanogramDal>())
            {
                dto = dal.FetchById(planId);
            }


            _converter.GfsPlanogram =
                new GfsToCcmPlanogramConverter.GFSPlanogramDto()
                {
                    Id = dto.Id,
                    Depth = dto.Depth,
                    Description = dto.Description,
                    EntityId = dto.EntityId,
                    Height = dto.Height,
                    IsLatestVersion = dto.IsLatestVersion,
                    Name = dto.Name,
                    UniqueContentReference = dto.UniqueContentReference,
                    Width = dto.Width,
                    ProductGroupId = dto.ProductGroupId
                };
        }

        private void GetMerchGroup(IDalContext dalContext, Int32 id)
        {
            GFSDTO.ProductGroupInfoDto dto = new GFSDTO.ProductGroupInfoDto();
            using (GFSDAL.IProductGroupInfoDal dal = dalContext.GetDal<GFSDAL.IProductGroupInfoDal>())
            {
                dto = dal.FetchByProductGroupIds(new List<Int32> { id }).FirstOrDefault();
            }

            _converter.GfsProductGroup = new GfsToCcmPlanogramConverter.GFSProductGroupDto()
            {
                Code = dto.Code,
                Name = dto.Name
            };
        }

        private void ImportProducts(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramProductDto> productDtos = new List<GFSDTO.PlanogramProductDto>();

            using (GFSDAL.IPlanogramProductDal dal = dalContext.GetDal<GFSDAL.IPlanogramProductDal>())
            {
                productDtos = dal.FetchByPlanogramId(planId);
            }


            foreach (var dto in productDtos)
            {
                _converter.GfsProducts.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramProductDto()
                    {
                        Id =dto.Id,
                        PlanogramContentId = dto.PlanogramContentId,
                        ProductId = dto.ProductId,
                        GTIN = dto.GTIN,
                        Name = dto.Name,
                        Brand = dto.Brand,
                        Height = dto.Height,
                        Width = dto.Width,
                        Depth = dto.Depth,
                        DisplayHeight = dto.DisplayHeight,
                        DisplayWidth = dto.DisplayWidth,
                        DisplayDepth = dto.DisplayDepth,
                        AlternateHeight = dto.AlternateHeight,
                        AlternateWidth = dto.AlternateWidth,
                        AlternateDepth = dto.AlternateDepth,
                        PointOfPurchaseHeight = dto.PointOfPurchaseHeight,
                        PointOfPurchaseWidth = dto.PointOfPurchaseWidth,
                        PointOfPurchaseDepth = dto.PointOfPurchaseDepth,
                        NumberOfPegHoles = dto.NumberOfPegHoles,
                        PegX = dto.PegX,
                        PegX2 = dto.PegX2,
                        PegX3 = dto.PegX3,
                        PegY = dto.PegY,
                        PegY2 = dto.PegY2,
                        PegY3 = dto.PegY3,
                        PegProngOffset = dto.PegProngOffset,
                        PegDepth = dto.PegDepth,
                        SqueezeHeight = Math.Min(1, dto.SqueezeHeight),
                        SqueezeWidth = Math.Min(1, dto.SqueezeWidth),
                        SqueezeDepth = Math.Min(1, dto.SqueezeDepth),
                        NestingHeight = dto.NestingHeight,
                        NestingWidth = dto.NestingWidth,
                        NestingDepth = dto.NestingDepth,
                        CasePackUnits = dto.CasePackUnits,
                        CaseHigh = dto.CaseHigh,
                        CaseWide = dto.CaseWide,
                        CaseDeep = dto.CaseDeep,
                        CaseHeight = dto.CaseHeight,
                        CaseWidth = dto.CaseWidth,
                        CaseDepth = dto.CaseDepth,
                        MaxStack = dto.MaxStack,
                        MaxTopCap = dto.MaxTopCap,
                        MaxRightCap = dto.MaxRightCap,
                        MinDeep = dto.MinDeep,
                        MaxDeep = dto.MaxDeep,
                        TrayPackUnits = dto.TrayPackUnits,
                        TrayHigh = dto.TrayHigh,
                        TrayWide = dto.TrayWide,
                        TrayDeep = dto.TrayDeep,
                        TrayHeight = dto.TrayHeight,
                        TrayWidth = dto.TrayWidth,
                        TrayDepth = dto.TrayDepth,
                        TrayThickHeight = dto.TrayThickHeight,
                        TrayThickWidth = dto.TrayThickWidth,
                        TrayThickDepth = dto.TrayThickDepth,
                        FrontOverhang = dto.FrontOverhang,
                        FingerSpaceAbove = dto.FingerSpaceAbove,
                        FingerSpaceToTheSide = dto.FingerSpaceToTheSide,
                        StatusType = dto.StatusType,
                        OrientationType = dto.OrientationType,
                        MerchandisingStyle = dto.MerchandisingStyle,
                        IsFrontOnly = dto.IsFrontOnly,
                      
                        IsPlaceHolderProduct = dto.IsPlaceHolderProduct,
                        IsActive = dto.IsActive,
                        CanBreakTrayUp = dto.CanBreakTrayUp,
                        CanBreakTrayDown = dto.CanBreakTrayDown,
                        CanBreakTrayBack = dto.CanBreakTrayBack,
                        CanBreakTrayTop = dto.CanBreakTrayTop,
                        ForceMiddleCap = dto.ForceMiddleCap,
                        ForceBottomCap = dto.ForceBottomCap,
                        CanMerchandiseInChest = dto.CanMerchandiseInChest,
                        CanMerchandiseOnBar = dto.CanMerchandiseOnBar,
                        CanMerchandiseOnPeg = dto.CanMerchandiseOnPeg,
                        CanMerchandiseOnShelf = dto.CanMerchandiseOnShelf,
                    });

            }

        }

        private void ImportFixtures(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramFixtureDto> dtoList = new List<GFSDTO.PlanogramFixtureDto>();

            using (GFSDAL.IPlanogramFixtureDal dal = dalContext.GetDal<GFSDAL.IPlanogramFixtureDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId);
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsFixtures.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramFixtureDto()
                    {
                        Depth = dto.Depth,
                        Height = dto.Height,
                        Id = dto.Id,
                        Name = dto.Name,
                        PlanogramContentId = dto.PlanogramContentId,
                        Width = dto.Width
                    });
            }
        }

        private void ImportFixtureItems(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramFixtureItemDto> dtoList = new List<GFSDTO.PlanogramFixtureItemDto>();

            using (GFSDAL.IPlanogramFixtureItemDal dal = dalContext.GetDal<GFSDAL.IPlanogramFixtureItemDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId).ToList();
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsFixtureItems.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramFixtureItemDto()
                    {
                        Angle = dto.Angle,
                        Id =dto.Id,
                        IsBay = dto.IsBay,
                        PlanogramContentId =dto.PlanogramContentId,
                        PlanogramFixtureId =dto.PlanogramFixtureId,
                        Roll =dto.Roll,
                        Slope = dto.Slope,
                        X =dto.X,
                        Y = dto.Y,
                        Z = dto.Z,
                    });
            }
        }

        private void ImportAssemblies(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramAssemblyDto> dtoList = new List<GFSDTO.PlanogramAssemblyDto>();

            using (GFSDAL.IPlanogramAssemblyDal dal = dalContext.GetDal<GFSDAL.IPlanogramAssemblyDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId).ToList();
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsAssemblies.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramAssemblyDto()
                    {
                        Depth = dto.Depth,
                        Height = dto.Height,
                        Id = dto.Id,
                        Name = dto.Name,
                        PlanogramContentId = dto.PlanogramContentId,
                        TotalComponentCost = dto.TotalComponentCost,
                        Width = dto.Width,
                    });
            }

        }

        private void ImportFixtureAssemblies(IDalContext dalContext, List<Int32> fixtureIds)
        {
            List<GFSDTO.PlanogramFixtureAssemblyDto> dtoList = new List<GFSDTO.PlanogramFixtureAssemblyDto>();

            using (GFSDAL.IPlanogramFixtureAssemblyDal dal = dalContext.GetDal<GFSDAL.IPlanogramFixtureAssemblyDal>())
            {
                foreach (Int32 fixtureId in fixtureIds)
                {
                    dtoList.AddRange(dal.FetchByFixtureId(fixtureId));
                }
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsFixtureAssemblies.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramFixtureAssemblyDto()
                    {
                        Angle = dto.Angle,
                        Id = dto.Id,
                        PlanogramAssemblyId = dto.PlanogramAssemblyId,
                        PlanogramFixtureId = dto.PlanogramFixtureId,
                        Roll = dto.Roll,
                        Slope = dto.Slope,
                        X = dto.X,
                        Y = dto.Y,
                        Z = dto.Z,
                    });
            }
        }

        private void ImportComponents(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramComponentDto> dtoList = new List<GFSDTO.PlanogramComponentDto>();

            using (GFSDAL.IPlanogramComponentDal dal = dalContext.GetDal<GFSDAL.IPlanogramComponentDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId).ToList();
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsComponents.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramComponentDto()
                    {
                        BarCode=dto.BarCode,
                        CanAttachShelfEdgeLabel=dto.CanAttachShelfEdgeLabel,
                        SupplierCostPrice=dto.SupplierCostPrice,
                        SupplierDiscount=dto.SupplierDiscount,
                        SupplierLeadTime=dto.SupplierLeadTime,
                        SupplierName=dto.SupplierName,
                        SupplierPartNumber=dto.SupplierPartNumber,
                        NumberOfShelfEdgeLabels=dto.NumberOfShelfEdgeLabels,
                        Capacity=dto.Capacity,
                        Colour=dto.Colour,
                        PlanogramContentId=dto.PlanogramContentId,
                        Depth=dto.Depth,
                        Diameter=dto.Diameter,
                        Height=dto.Height,
                        Id=dto.Id,
                        IsDisplayOnly=dto.IsDisplayOnly,
                        IsMoveable=dto.IsMoveable,
                        Manufacturer=dto.Manufacturer,
                        ManufacturerPartName=dto.ManufacturerPartName,
                        ManufacturerPartNumber=dto.ManufacturerPartNumber,
                        MerchandiseType=dto.MerchandiseType,
                        MinPurchaseQty=dto.MinPurchaseQty,
                        Name=dto.Name,
                        RetailerReference=dto.RetailerReference,
                        Volume=dto.Volume,
                        Weight=dto.Weight,
                        WeightLimit=dto.WeightLimit,
                        Width = dto.Width,
                    });
            }
        }

        private void ImportAssemblyComponents(IDalContext dalContext, List<Int32> assemblyIds)
        {
            List<GFSDTO.PlanogramAssemblyComponentDto> dtoList = new List<GFSDTO.PlanogramAssemblyComponentDto>();

            using (GFSDAL.IPlanogramAssemblyComponentDal dal = dalContext.GetDal<GFSDAL.IPlanogramAssemblyComponentDal>())
            {
                foreach (Int32 parentId in assemblyIds)
                {
                    dtoList.AddRange(dal.FetchByPlanogramAssemblyId(parentId));
                }
            }
            foreach (var dto in dtoList)
            {
                _converter.GfsAssemblyComponents.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramAssemblyComponentDto()
                    {
                        Angle = dto.Angle,
                        Id = dto.Id,
                        PlanogramAssemblyId = dto.PlanogramAssemblyId,
                        PlanogramComponentId = dto.PlanogramComponentId,
                        Roll = dto.Roll,
                        Slope = dto.Slope,
                        X = dto.X,
                        Y = dto.Y,
                        Z = dto.Z
                    });
            }
        }

        private void ImportSubComponents(IDalContext dalContext, List<Int32> componentIds)
        {
            List<GFSDTO.PlanogramSubComponentDto> dtoList = new List<GFSDTO.PlanogramSubComponentDto>();

            using (GFSDAL.IPlanogramSubComponentDal dal = dalContext.GetDal<GFSDAL.IPlanogramSubComponentDal>())
            {
                foreach (Int32 parentId in componentIds)
                {
                    dtoList.AddRange(dal.FetchByPlanogramComponentId(parentId));
                }
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsSubComponents.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramSubComponentDto()
                    {
                        Id = dto.Id,
                        Name = dto.Name,
                        Height = dto.Height,
                        Width = dto.Width,
                        Depth = dto.Depth,
                        X = dto.X,
                        Y = dto.Y,
                        Z = dto.Z,
                        Slope = dto.Slope,
                        Angle = dto.Angle,
                        Roll = dto.Roll,
                        ShapeType = dto.ShapeType,
                        MerchandisableHeight = dto.MerchandisableHeight,
                        IsVisible = dto.IsVisible,
                        HasCollisionDetection = dto.HasCollisionDetection,
                        FillPatternTypeFront = dto.FillPatternTypeFront,
                        FillPatternTypeBack = dto.FillPatternTypeBack,
                        FillPatternTypeTop = dto.FillPatternTypeTop,
                        FillPatternTypeBottom = dto.FillPatternTypeBottom,
                        FillPatternTypeLeft = dto.FillPatternTypeLeft,
                        FillPatternTypeRight = dto.FillPatternTypeRight,
                        LineColour = dto.LineColour,
                        TransparencyPercentFront = dto.TransparencyPercentFront,
                        TransparencyPercentBack = dto.TransparencyPercentBack,
                        TransparencyPercentTop = dto.TransparencyPercentTop,
                        TransparencyPercentBottom = dto.TransparencyPercentBottom,
                        TransparencyPercentLeft = dto.TransparencyPercentLeft,
                        TransparencyPercentRight = dto.TransparencyPercentRight,
                        FaceThicknessTop = dto.FaceThicknessTop,
                        RiserHeight = dto.RiserHeight,
                        IsRiserPlacedOnRight = dto.IsRiserPlacedOnRight,
                        IsRiserPlacedOnLeft = dto.IsRiserPlacedOnLeft,
                        IsRiserPlacedOnBack = dto.IsRiserPlacedOnBack,
                        IsRiserPlacedOnFront = dto.IsRiserPlacedOnFront,
                        RiserFillPatternType = dto.RiserFillPatternType,
                        NotchStartX = dto.NotchStartX,
                        NotchSpacingX = dto.NotchSpacingX,
                        NotchHeight = dto.NotchHeight,
                        IsNotchPlacedOnRight = dto.IsNotchPlacedOnRight,
                        IsNotchPlacedOnLeft = dto.IsNotchPlacedOnLeft,
                        IsNotchPlacedOnBack = dto.IsNotchPlacedOnBack,
                        IsNotchPlacedOnFront = dto.IsNotchPlacedOnFront,
                        NotchStyleType = dto.NotchStyleType,
                        DividerObstructionHeight = dto.DividerObstructionHeight,
                        DividerObstructionWidth = dto.DividerObstructionWidth,
                        DividerObstructionStartX = dto.DividerObstructionStartX,
                        DividerObstructionSpacingX = dto.DividerObstructionSpacingX,
                        DividerObstructionStartY = dto.DividerObstructionStartY,
                        DividerObstructionSpacingY = dto.DividerObstructionSpacingY,
                        MerchConstraintRow1StartX = dto.MerchConstraintRow1StartX,
                        MerchConstraintRow1SpacingX = dto.MerchConstraintRow1SpacingX,
                        MerchConstraintRow1StartY = dto.MerchConstraintRow1StartY,
                        MerchConstraintRow1SpacingY = dto.MerchConstraintRow1SpacingY,
                        MerchConstraintRow2StartX = dto.MerchConstraintRow2StartX,
                        MerchConstraintRow2SpacingX = dto.MerchConstraintRow2SpacingX,
                        MerchConstraintRow2StartY = dto.MerchConstraintRow2StartY,
                        MerchConstraintRow2SpacingY = dto.MerchConstraintRow2SpacingY,
                        ComponentId=dto.ComponentId,
                        SubComponentType=dto.SubComponentType,
                        FaceThickness=dto.FaceThickness,
                        FillColourBack=dto.FillColourBack,
                        FillColourBottom=dto.FillColourBottom,
                        FillColourFront=dto.FillColourFront,
                        FillColourLeft=dto.FillColourLeft,
                        FillColourRight=dto.FillColourRight,
                        FillColourTop=dto.FillColourTop,
                        ImageIdBack=dto.ImageIdBack,
                        ImageIdBottom=dto.ImageIdBottom,
                        ImageIdFront=dto.ImageIdFront,
                        ImageIdLeft=dto.ImageIdLeft,
                        ImageIdRight=dto.ImageIdRight,
                        ImageIdTop=dto.ImageIdTop,
                        IsMerchandisable = dto.IsMerchandisable,
                    });
            }

         }

        private void ImportPositions(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramPositionDto> dtoList = new List<GFSDTO.PlanogramPositionDto>();

            using (GFSDAL.IPlanogramPositionDal dal = dalContext.GetDal<GFSDAL.IPlanogramPositionDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId).ToList();
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsPositions.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramPositionDto()
                    {
                        Angle = dto.Angle,
                        TotalUnits = dto.TotalUnits,
                        Slope = dto.Slope,
                        BaySequenceNumber = dto.BaySequenceNumber,
                        ElementSequenceNumber = dto.ElementSequenceNumber,
                        PlanogramSubComponentId = dto.PlanogramSubComponentId,
                        BlockingColour = dto.BlockingColour,
                        FrontDeep = dto.FrontDeep,
                        FrontHigh = dto.FrontHigh,
                        FrontRightDeep = dto.FrontRightDeep,
                        FrontRightHigh = dto.FrontRightHigh,
                        FrontRightWide = dto.FrontRightWide,
                        FrontTopCapDeep = dto.FrontTopCapDeep,
                        FrontTopCapHigh = dto.FrontTopCapHigh,
                        FrontTopCapRightDeep = dto.FrontTopCapRightDeep,
                        FrontTopCapRightHigh = dto.FrontTopCapRightHigh,
                        FrontTopCapRightWide = dto.FrontTopCapRightWide,
                        FrontTopCapWide = dto.FrontTopCapWide,
                        Id = dto.Id,
                        FrontWide = dto.FrontWide,
                        PlanogramAssemblyComponentId = dto.PlanogramAssemblyComponentId,
                        PlanogramContentId = dto.PlanogramContentId,
                        PlanogramFixtureAssemblyId = dto.PlanogramFixtureAssemblyId,
                        PlanogramFixtureItemId = dto.PlanogramFixtureItemId,
                        PlanogramProductId = dto.PlanogramProductId,
                        PositionSequenceNumber = dto.PositionSequenceNumber,
                        Roll = dto.Roll,
                        TrayBreakBackDeep = dto.TrayBreakBackDeep,
                        TrayBreakBackHigh = dto.TrayBreakBackHigh,
                        TrayBreakBackWide = dto.TrayBreakBackWide,
                        TrayBreakDownDeep = dto.TrayBreakDownDeep,
                        TrayBreakDownHigh = dto.TrayBreakDownHigh,
                        TrayBreakDownWide = dto.TrayBreakDownWide,
                        TrayBreakTopHigh = dto.TrayBreakTopHigh,
                        TrayBreakTopDeep = dto.TrayBreakTopDeep,
                        TrayBreakTopRightDeep = dto.TrayBreakTopRightDeep,
                        TrayBreakTopRightHigh = dto.TrayBreakTopRightHigh,
                        TrayBreakTopRightWide = dto.TrayBreakTopRightWide,
                        TrayBreakTopWide = dto.TrayBreakTopWide,
                        TrayBreakUpDeep = dto.TrayBreakUpDeep,
                        TrayBreakUpHigh = dto.TrayBreakUpHigh,
                        TrayBreakUpWide = dto.TrayBreakUpWide,
                        TraysDeep = dto.TraysDeep,
                        TraysHigh = dto.TraysHigh,
                        TraysRightDeep = dto.TraysRightDeep,
                        TraysRightHigh = dto.TraysRightHigh,
                        TraysRightWide = dto.TraysRightWide,
                        TraysWide = dto.TraysWide,
                        UnitsDeep = dto.UnitsDeep,
                        UnitsHigh = dto.UnitsHigh,
                        UnitsWide = dto.UnitsWide,
                        X = dto.X ,
                        Y = dto.Y,
                        Z = dto.Z,
                    });


            }
        }

        private void ImportAnnotations(IDalContext dalContext, Int32 planId)
        {
            List<GFSDTO.PlanogramAnnotationDto> dtoList = new List<GFSDTO.PlanogramAnnotationDto>();

            using (GFSDAL.IPlanogramAnnotationDal dal = dalContext.GetDal<GFSDAL.IPlanogramAnnotationDal>())
            {
                dtoList = dal.FetchByPlanogramId(planId).ToList();
            }

            foreach (var dto in dtoList)
            {
                _converter.GfsAnnotations.Add(
                    new GfsToCcmPlanogramConverter.GFSPlanogramAnnotationDto()
                    {
                        AnnotationType = dto.AnnotationType,
                        Text = dto.Text,
                        PlanogramSubComponentId = dto.PlanogramSubComponentId,
                        Depth = dto.Depth,
                        Height = dto.Height,
                        Id = dto.Id,
                        PlanogramAssemblyComponentId = dto.PlanogramAssemblyComponentId,
                        PlanogramContentId = dto.PlanogramContentId,
                        PlanogramFixtureAssemblyId = dto.PlanogramFixtureAssemblyId,
                        PlanogramFixtureItemId = dto.PlanogramFixtureItemId,
                        PlanogramPositionId = dto.PlanogramPositionId,
                        Width = dto.Width,
                        X = dto.X,
                        Y = dto.Y,
                        Z = dto.Z,
                    });
            }

        }

        #endregion

        #endregion
    }
}
