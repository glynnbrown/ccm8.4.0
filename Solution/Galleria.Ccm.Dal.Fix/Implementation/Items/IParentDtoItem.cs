﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Fix.Implementation.Items
{
    public interface IParentDtoItem<DTO> : GalleriaBinaryFile.IItem
    {
        Int32 Id { get; set; }

        DTO CopyDto();
    }
}
