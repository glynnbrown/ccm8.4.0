﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Fix.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Fix.Implementation.Items
{
    public class FixtureAssemblyDtoItem : FixtureAssemblyDto, IChildDtoItem<FixtureAssemblyDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureAssemblyDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureAssemblyDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyId));
            FixturePackageId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureAssemblyFixturePackageId));
            Name = (String)item.GetItemPropertyValue(FieldNames.FixtureAssemblyName);
            Height = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyHeight));
            Width = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyWidth));
            Depth = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyDepth));
            NumberOfComponents = GalleriaBinaryFileItemHelper.GetInt16(item.GetItemPropertyValue(FieldNames.FixtureAssemblyNumberOfComponents));
            TotalComponentCost = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureAssemblyTotalComponentCost));
        }

        /// <summary>
        /// Creates a new FixtureAssemblyDtoItem and populates its properties with values taken from a 
        /// FixtureAssemblyDto.
        /// </summary>
        public FixtureAssemblyDtoItem(FixtureAssemblyDto dto)
        {
            Id = dto.Id;
            FixturePackageId = dto.FixturePackageId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            NumberOfComponents = dto.NumberOfComponents;
            TotalComponentCost = dto.TotalComponentCost;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureAssemblyDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureAssemblyDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureAssemblyDto>.ParentId
        {
            get { return (Int32)FixturePackageId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureAssemblyDto CopyDto()
        {
            return new FixtureAssemblyDto()
            {
                Id=this.Id,
                FixturePackageId = this.FixturePackageId,
                Name =this.Name ,
                Height =this.Height ,
                Width =this.Width ,
                Depth =this.Depth ,
                NumberOfComponents =this.NumberOfComponents ,
                TotalComponentCost =this.TotalComponentCost
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureAssemblyId:
                    return Id;
                case FieldNames.FixtureAssemblyFixturePackageId:
                    return FixturePackageId;
                case FieldNames.FixtureAssemblyName:
                    return Name;
                case FieldNames.FixtureAssemblyHeight:
                    return Height;
                case FieldNames.FixtureAssemblyWidth:
                    return Width;
                case FieldNames.FixtureAssemblyDepth:
                    return Depth;
                case FieldNames.FixtureAssemblyNumberOfComponents:
                    return NumberOfComponents;
                case FieldNames.FixtureAssemblyTotalComponentCost:
                    return TotalComponentCost;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
