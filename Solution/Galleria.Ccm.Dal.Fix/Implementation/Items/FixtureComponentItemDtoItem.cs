﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Fix.Schema;
using Galleria.Framework.Dal.Gbf;
using Galleria.Framework.Helpers;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.Fix.Implementation.Items
{
    public class FixtureComponentItemDtoItem : FixtureComponentItemDto, IChildDtoItem<FixtureComponentItemDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixtureComponentItemDtoItem and populates its properties with value taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixtureComponentItemDtoItem(GalleriaBinaryFile.IItem item)
        {
            Id = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentItemId));
            FixtureId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentItemFixtureId));
            FixtureComponentId = GalleriaBinaryFileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixtureComponentItemFixtureComponentId));
            X = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemX));
            Y = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemY));
            Z = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemZ));
            Slope = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemSlope));
            Angle = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemAngle));
            Roll = GalleriaBinaryFileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixtureComponentItemRoll));
        }

        /// <summary>
        /// Creates a new FixtureComponentItemDtoItem and populates its properties with values taken from a 
        /// FixtureComponentItemDto.
        /// </summary>
        public FixtureComponentItemDtoItem(FixtureComponentItemDto dto)
        {
            Id = dto.Id;
            FixtureId = dto.FixtureId;
            FixtureComponentId = dto.FixtureComponentId;
            X = dto.X;
            Y = dto.Y;
            Z = dto.Z;
            Slope = dto.Slope;
            Angle = dto.Angle;
            Roll = dto.Roll;
        }

        #endregion

        #region Public Properties

        Object IChildDtoItem<FixtureComponentItemDto>.Id
        {
            get { return Id; }
            set { Id = (Int32)value; }
        }

        Boolean IChildDtoItem<FixtureComponentItemDto>.AutoIncrementId
        {
            get { return true; }
        }

        Object IChildDtoItem<FixtureComponentItemDto>.ParentId
        {
            get { return (Int32)FixtureId; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixtureComponentItemDto CopyDto()
        {
            return new FixtureComponentItemDto()
            {
                Id = this.Id,
                FixtureId = this.FixtureId,
                FixtureComponentId = this.FixtureComponentId,
                X = this.X,
                Y = this.Y,
                Z = this.Z,
                Slope = this.Slope,
                Angle = this.Angle,
                Roll = this.Roll
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixtureComponentItemId:
                    return Id;
                case FieldNames.FixtureComponentItemFixtureId:
                    return FixtureId;
                case FieldNames.FixtureComponentItemFixtureComponentId:
                    return FixtureComponentId;
                case FieldNames.FixtureComponentItemX:
                    return X;
                case FieldNames.FixtureComponentItemY:
                    return Y;
                case FieldNames.FixtureComponentItemZ:
                    return Z;
                case FieldNames.FixtureComponentItemSlope:
                    return Slope;
                case FieldNames.FixtureComponentItemAngle:
                    return Angle;
                case FieldNames.FixtureComponentItemRoll:
                    return Roll;
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }
}
