﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Fix.Implementation.Items
{
    public static class FileItemHelper
    {
        public static Boolean GetBoolean(Object value)
        {
            Boolean returnValue = new Boolean();
            if (value != null)
            {
                returnValue = (Boolean)value;
            }
            return returnValue;
        }

        public static Byte GetByte(Object value)
        {
            Byte returnValue = new Byte();
            if (value != null)
            {
                returnValue = (Byte)value;
            }
            return returnValue;
        }

        public static Char GetChar(Object value)
        {
            Char returnValue = new Char();
            if (value != null)
            {
                returnValue = (Char)value;
            }
            return returnValue;
        }

        public static DateTime GetDateTime(Object value)
        {
            DateTime returnValue = new DateTime();
            if (value != null)
            {
                returnValue = (DateTime)value;
            }
            return returnValue;
        }

        public static Decimal GetDecimal(Object value)
        {
            Decimal returnValue = new Decimal();
            if (value != null)
            {
                returnValue = (Decimal)value;
            }
            return returnValue;
        }

        public static Double GetDouble(Object value)
        {
            Double returnValue = new Double();
            if (value != null)
            {
                returnValue = (Double)value;
            }
            return returnValue;
        }

        public static Int32 GetInt32(Object value)
        {
            Int32 returnValue = new Int32();
            if (value != null)
            {
                returnValue = (Int32)value;
            }
            return returnValue;
        }

        public static Int64 GetInt64(Object value)
        {
            Int64 returnValue = new Int64();
            if (value != null)
            {
                returnValue = (Int64)value;
            }
            return returnValue;
        }

        public static SByte GetSByte(Object value)
        {
            SByte returnValue = new SByte();
            if (value != null)
            {
                returnValue = (SByte)value;
            }
            return returnValue;
        }

        public static Int16 GetInt16(Object value)
        {
            Int16 returnValue = new Int16();
            if (value != null)
            {
                returnValue = (Int16)value;
            }
            return returnValue;
        }

        public static Single GetSingle(Object value)
        {
            Single returnValue = new Single();
            if (value != null)
            {
                returnValue = (Single)value;
            }
            return returnValue;
        }

        public static UInt32 GetUInt32(Object value)
        {
            UInt32 returnValue = new UInt32();
            if (value != null)
            {
                returnValue = (UInt32)value;
            }
            return returnValue;
        }

        public static UInt64 GetUInt64(Object value)
        {
            UInt64 returnValue = new UInt64();
            if (value != null)
            {
                returnValue = (UInt64)value;
            }
            return returnValue;
        }

        public static UInt16 GetUInt16(Object value)
        {
            UInt16 returnValue = new UInt16();
            if (value != null)
            {
                returnValue = (UInt16)value;
            }
            return returnValue;
        }
    }
}
