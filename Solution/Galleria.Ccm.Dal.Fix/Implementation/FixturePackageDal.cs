﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Fix.Resources;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    /// <summary>
    /// Implementation of IFixturePackageDal
    /// </summary>
    /// <remarks>
    /// Note: The id parameter to various methods in this class is ignored, as a Galleria Fixture File may only
    /// contain one package anyway
    /// </remarks>
    public class FixturePackageDal : DalBase<DalCache>, IFixturePackageDal
    {

        /// <summary>
        /// Returns the item with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FixturePackageDto FetchById(object id)
        {
            FixturePackageDto returnValue = DalCache.FixturePackage.Fetch();
            if (returnValue == null)
            {
                throw new DtoDoesNotExistException();
            }
            return returnValue;
        }

        /// <summary>
        /// Inserts an item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixturePackageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            if (DalCache.FixturePackage.Fetch() != null)
            {
                throw new Exception("TODO");
            }
            DalCache.FixturePackage.InsertOrUpdate(dto);
            dto.Id = 1;
        }

        /// <summary>
        /// Finds and updates an item by ID, if it exists.
        /// </summary>
        public void Update(FixturePackageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            if (DalCache.FixturePackage.Fetch() != null)
            {
                DalCache.FixturePackage.InsertOrUpdate(dto);
                dto.Id = 1;
            }
        }

        /// <summary>
        /// Deletes the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(object id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_Transaction);
            }
            DalCache.FixturePackage.Delete();
        }

        /// <summary>
        /// Locks the package.
        /// </summary>
        public void LockById(Object id)
        {
            DalContext.LockDalCache(false);
        }

        /// <summary>
        /// Unlocks the package.
        /// </summary>
        public void UnlockById(Object id)
        {
            DalContext.UnlockDalCache();
        }
    }
}
