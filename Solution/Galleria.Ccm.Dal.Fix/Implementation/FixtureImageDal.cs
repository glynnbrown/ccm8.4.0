﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Fix.Resources;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    /// <summary>
    /// Implementation of IFixtureImageDal
    /// </summary>
    public class FixtureImageDal : DalBase<DalCache>, IFixtureImageDal
    {
        #region IFixtureImageDal Members

        /// <summary>
        /// Fetches all fixture images with a given fixture package ID.
        /// </summary>
        public IEnumerable<FixtureImageDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.FixtureImages.FetchByParentId((Int32)fixturePackageId);
        }

        /// <summary>
        /// Inserts a fixture image, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureImages.Insert(dto);
        }

        /// <summary>
        /// Finds and updates a fixture image by ID, if the fixture exists.
        /// </summary>
        public void Update(FixtureImageDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureImages.Update(dto);
        }

        /// <summary>
        /// Finds and deletes a fixture image by ID, if the fixture exists.
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureImages.Delete(id);
        }

        #endregion
    }
}
