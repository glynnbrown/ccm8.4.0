﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Fix.Resources;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    public class FixtureComponentItemDal : DalBase<DalCache>, IFixtureComponentItemDal
    {
        #region IFixtureComponentItemDal Members

        /// <summary>
        /// Fetches all fixture fixture components with a given fixture component ID.
        /// </summary>
        public IEnumerable<FixtureComponentItemDto> FetchByFixtureId(Int32 fixtureId)
        {
            return DalCache.FixtureComponentItems.FetchByParentId(fixtureId);
        }

        /// <summary>
        /// Inserts a fixture fixture component, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureComponentItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureComponentItems.Insert(dto);
        }

        /// <summary>
        /// Finds and updates a fixture fixture component by ID, if the fixture exists.
        /// </summary>
        public void Update(FixtureComponentItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureComponentItems.Update(dto);
        }

        /// <summary>
        /// Finds and deletes a fixture fixture component by ID, if the fixture exists.
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureComponentItems.Delete(id);
        }

        #endregion
    }
}
