﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Fix.Resources;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    public class FixtureSubComponentDal : DalBase<DalCache>, IFixtureSubComponentDal
    {
        #region IFixtureSubComponentDal Members

        /// <summary>
        /// Fetches all fixture sub components with a given fixture component ID.
        /// </summary>
        public IEnumerable<FixtureSubComponentDto> FetchByFixtureComponentId(Int32 fixtureComponentId)
        {
            return DalCache.FixtureSubComponents.FetchByParentId(fixtureComponentId);
        }

        /// <summary>
        /// Inserts a fixture sub component, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureSubComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureSubComponents.Insert(dto);
        }

        /// <summary>
        /// Finds and updates a fixture sub component by ID, if the fixture exists.
        /// </summary>
        public void Update(FixtureSubComponentDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureSubComponents.Update(dto);
        }

        /// <summary>
        /// Finds and deletes a fixture sub component by ID, if the fixture exists.
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureSubComponents.Delete(id);
        }

        #endregion

        
    }
}
