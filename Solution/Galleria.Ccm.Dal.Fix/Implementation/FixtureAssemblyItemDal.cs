﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;
using Galleria.Ccm.Dal.Fix.Resources;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    /// <summary>
    /// Implementation of IFixtureAssemblyItemDal
    /// </summary>
    public class FixtureAssemblyItemDal : DalBase<DalCache>, IFixtureAssemblyItemDal
    {

        /// <summary>
        /// Fetches all items with the given FixtureId
        /// </summary>
        /// <param name="fixturePackageId"></param>
        /// <returns></returns>
        public IEnumerable<FixtureAssemblyItemDto> FetchByFixtureId(Int32 fixtureId)
        {
            return DalCache.FixtureAssemblyItems.FetchByParentId(fixtureId);
        }

        /// <summary>
        /// Inserts an item, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureAssemblyItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.FixtureAssemblyItems.Insert(dto);
        }

        /// <summary>
        /// Finds and updates an item by ID, if it exists.
        /// </summary>
        public void Update(FixtureAssemblyItemDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAssemblyItems.Update(dto);
        }

        /// <summary>
        /// Deletes the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.FixtureAssemblyItems.Delete(id);
        }
    }
}
