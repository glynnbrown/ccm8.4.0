﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Fix.Resources;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Gbf;

namespace Galleria.Ccm.Dal.Fix.Implementation
{
    public class FixtureDal : DalBase<DalCache>, IFixtureDal
    {
        #region IFixtureDal Members

        /// <summary>
        /// Fetches all fixture fixtures with a given fixture ID.
        /// </summary>
        public IEnumerable<FixtureDto> FetchByFixturePackageId(Object fixturePackageId)
        {
            return DalCache.Fixtures.FetchByParentId((Int32)fixturePackageId);
        }

        /// <summary>
        /// Inserts a fixture fixture, assigning it the next available integer ID.
        /// </summary>
        public void Insert(FixtureDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            dto.Id = (Int32)DalCache.Fixtures.Insert(dto);
        }

        /// <summary>
        /// Finds and updates a fixture fixture by ID, if the fixture exists.
        /// </summary>
        public void Update(FixtureDto dto)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.Fixtures.Update(dto);
        }

        /// <summary>
        /// Finds and deletes a fixture fixture by ID, if the fixture exists.
        /// </summary>
        public void DeleteById(Int32 id)
        {
            if (!DalContext.TransactionInProgress)
            {
                throw new InvalidOperationException(Language.Exception_NoTransactionForDataChange);
            }
            DalCache.Fixtures.Delete(id);
        }

        #endregion
    }
}
