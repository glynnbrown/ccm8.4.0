﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM CCM800)
//// CCM-24979 : L.Hodson
////		Created
//#endregion
//#endregion

//using Galleria.Framework.IO;

//namespace Galleria.Ccm.Dal.Fix.Scripts
//{
//    public interface IScript
//    {
//        /// <summary>
//        /// Upgrades a file of the previous schema version to the this one.
//        /// </summary>
//        void Upgrade(GalleriaBinaryFile file);

//        /// <summary>
//        /// Downgrads a file of this schema version to the previous one.
//        /// </summary>
//        void Downgrade(GalleriaBinaryFile file);
//    }
//}
