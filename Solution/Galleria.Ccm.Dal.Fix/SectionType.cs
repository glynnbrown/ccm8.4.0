﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Fix
{
    /// <summary>
    /// The SectionType enum represents the type of a section within a Galleria Fixture File.
    /// </summary>
    internal enum SectionType : ushort
    {
        SchemaVersion = 1,
        FixturePackage = 2,
        FixtureAssemblies = 3,
        FixtureAssemblyComponents = 4,
        FixtureComponents = 5,
        Fixtures = 6,
        FixtureAssemblyItems = 7,
        FixtureComponentItems = 8,
        FixtureItems = 9,
        FixtureImages = 10,
        FixtureSubComponents = 11,
    }
}
