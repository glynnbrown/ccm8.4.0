﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;

using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Fix
{
    public class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Constants

        protected const String _noTransactionError =
            "The FIX DAL does not support the changing of data outside of a transaction.";

        #endregion

        #region Properties
        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
        }

        /// <summary>
        /// The DalCache for this dal
        /// </summary>
        internal DalCache DalCache
        {
            get { return DalContext.DalCache; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }
        #endregion
    }
}
