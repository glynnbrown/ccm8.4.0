﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Dal.Fix
{
    public class DalContext : DalContextBase
    {
        #region Fields
        private DalFactory _dalFactory; // the DalFactory that created this DalContext.
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalFactoryConfig">The dal factory configuration element</param>
        public DalContext(DalFactory dalFactory, DalFactoryConfigElement dalFactoryConfig)
        {
            _dalFactory = dalFactory;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The dal cache, taken from the dal factory.  If LockDalCache has never been called then an exception will be 
        /// thrown.
        /// </summary>
        internal DalCache DalCache
        {
            get
            {
                if (_dalFactory.LockedDalCache == null)
                {
                    throw new Exception("TODO");
                }
                return _dalFactory.LockedDalCache;
            }
        }

        /// <summary>
        /// Whether a transaction is currently in progress.
        /// </summary>
        internal Boolean TransactionInProgress { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Begins a transaction within this context
        /// </summary>
        public override void Begin()
        {
            lock (_dalFactory.TransactionLock)
            {
                if (_dalFactory.TransactionInProgress)
                {
                    throw new InvalidOperationException("Only one transaction may be open on a POG file at a time.");
                }
                _dalFactory.TransactionInProgress = TransactionInProgress = true;
            }
        }

        /// <summary>
        /// Commits a transaction within this context
        /// </summary>
        public override void Commit()
        {
            lock (_dalFactory.TransactionLock)
            {
                if (!TransactionInProgress)
                {
                    throw new InvalidOperationException("A transaction must be in progress before it can be committed.");
                }
                DalCache.Commit();
                _dalFactory.TransactionInProgress = TransactionInProgress = false;
            }
        }

        /// <summary>
        /// Creates a new locked cache if required
        /// </summary>
        public void LockDalCache()
        {
            if (_dalFactory.LockedDalCache == null)
            {
                _dalFactory.LockedDalCache = new DalCache(_dalFactory);
            }
        }

        /// <summary>
        /// Unlocks and disposes of the current cache.
        /// </summary>
        public void UnlockDalCache()
        {
            if (_dalFactory.LockedDalCache != null)
            {
                _dalFactory.LockedDalCache.Dispose();
            }
            _dalFactory.LockedDalCache = null;
        }

        /// <summary>
        /// Called when disposing of this context
        /// </summary>
        public override void Dispose()
        {
            if (TransactionInProgress && (_dalFactory.LockedDalCache != null))
            {
                _dalFactory.LockedDalCache.Rollback();
            }
        }
        #endregion
    }
}
