﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32359 : A.Silva
//  Created.
// V8-32818 : A.Silva
//  Amended to return with information "nothing for task to do" when there are no attributes selected to update.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductAttributesUsingList.v1
{
    /// <summary>
    ///     Engine Task to Update Product Attributes using a list of attributes and values to update.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Nested types

        /// <summary>
        ///     The enumeration of parameters that this task can have.
        /// </summary>
        private enum Parameter
        {
            ProductCodes = 0,
            ProductAttributeUpdateValues = 1
        }

        #endregion

        #region Fields

        /// <summary>
        ///     The task's major version.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        /// <summary>
        ///     The task's minor version number.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        #endregion

        #region Properties

        /// <summary>
        ///     Get the task name.
        /// </summary>
        public override String Name { get { return Message.UpdateProductAttributesUsingList_Name; } }

        /// <summary>
        ///     Get the task description.
        /// </summary>
        public override String Description
        {
            get { return Message.UpdateProductAttributesUsingList_Description; }
        }

        /// <summary>
        ///     Get the task grouping category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_UpdatePlanogram; } }

        /// <summary>
        ///     Get the task minor version number
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     Get the task major version number
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        #endregion

        /// <summary>
        ///     Called when parameters are required to be registered.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            RegisterParameter(CreateProductCodesParameter());
            RegisterParameter(CreateProductAttributeUpdateValuesParameter());
        }

        /// <summary>
        ///     Called when this task is to be executed.
        /// </summary>
        /// <param name="context">The current task context.</param>
        protected override void OnExecute(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!GetProductsToUpdate(taskContext)) return;
                if (!UpdateProductAttributes(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
                LogCompletion(taskContext);
                // Task complete.
            }
        }

        /// <summary>
        ///     Get the task parameters and ensure they are valid before executing the task.
        /// </summary>
        /// <param name="context">The current task context.</param>
        /// <returns><c>True</c> if the task was completed succesfully, <c>false</c> otherwise.</returns>
        /// <remarks>This task will fail if <c>all</c> the property names provided to be updated fail to map to existing properties OR if none is provided.</remarks>
        private Boolean GetAndValidateParameters(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                TaskParameter taskParameter = Parameters[Parameter.ProductCodes];
                // Get and validate the Product Codes. No values indicates that all products should be updated.
                if (taskParameter.Value != null)
                    context.ProductGtins.AddRange(taskParameter.Values.Select(item => Convert.ToString(item.Value1)));

                // Get and validate the Product Attribute Update Values. There needs be at least one new value or else there is nothing to update.
                if (Parameters[Parameter.ProductAttributeUpdateValues].Value != null)
                {
                    context.ProductAttributeUpdateValues.AddRange(
                        Parameters[Parameter.ProductAttributeUpdateValues].Values.Select(value => TaskParameterAsProductAttributeValue(value, context))
                                     .Where(value => value != null));
                }

                if (!context.ProductAttributeUpdateValues.Any())
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductAttributesSupplied);
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        ///     Get the matching <see cref="ProductAttributeUpdateValue"/> and set, from the <paramref name="taskParameter"/>, the value that will be updated.
        /// </summary>
        /// <param name="taskParameter">The parameter to match with an update value.</param>
        /// <param name="context">The current task context.</param>
        /// <returns>The matching <see cref="ProductAttributeUpdateValue"/> or <c>null</c> if none was found.</returns>
        /// <remarks>The match returned will have its <see cref="ProductAttributeUpdateValue.NewValue"/> property uptdated as the <paramref name="taskParameter"/> indicates.</remarks>
        private static ProductAttributeUpdateValue TaskParameterAsProductAttributeValue(ITaskParameterValue taskParameter, TaskContext context)
        {
            ProductAttributeUpdateValue updateValue = context.AttributeUpdateValues.FirstOrDefault(value => value.PropertyName == taskParameter.Value1 as String);
            if (updateValue == null) return null;

            updateValue.NewValue = taskParameter.Value2;
            return updateValue;
        }

        /// <summary>
        ///     Get the specific products that need to be updated.
        /// </summary>
        /// <param name="context">The current task context.</param>
        /// <returns><c>True</c> if the task was completed succesfully, <c>false</c> otherwise.</returns>
        /// <remarks>This task is always completed succesfully.<para />
        /// If no explicit products were selected, all will be updated.
        /// </remarks>
        private static Boolean GetProductsToUpdate(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                //  No products to update will update all products in the planogram.
                if (!context.ProductGtins.Any()) return true;

                //  Get the target products.
                context.TargetProducts.AddRange(context.Planogram.Products.Where(product => context.ProductGtins.Contains(product.Gtin)));

                return true;
            }
        }

        /// <summary>
        ///     Update the product attributes in the planogram
        /// </summary>
        /// <param name="context">The current task context.</param>
        /// <returns><c>True</c> if the task was completed succesfully, <c>false</c> otherwise.</returns>
        /// <remarks>This task is always completed succesfully.</remarks>
        private static Boolean UpdateProductAttributes(TaskContext context)
        {
            context.UpdatedProductCount = context.Planogram.UpdateProductAttributes(context.ProductAttributeUpdateValues, context.ProductGtins);

            return true;
        }

        /// <summary>
        ///     Remerchandise the planogram in case there were changes to sizes and similar.
        /// </summary>
        /// <param name="context">The current task context.</param>
        /// <returns><c>True</c> if the task was completed succesfully, <c>false</c> otherwise.</returns>
        /// <remarks>This task is always completed succesfully.</remarks>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                //  Remerchandise only if there were changes.
                if (context.UpdatedProductCount != 0)
                    context.RemerchandisePlanogram();

                return true;
            }
        }

        /// <summary>
        ///     Log completion information for the task.
        /// </summary>
        /// <param name="context">The current task context.</param>
        private static void LogCompletion(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                context.LogInformation(EventLogEvent.PlanogramProductsUpdated, context.UpdatedProductCount);
            }
        }

        #region Static Helper Methods

        /// <summary>
        ///     Create a new instance of <see cref="TaskParameter"/> for the <see cref="Parameter.ProductCodes"/> parameter.
        /// </summary>
        /// <returns></returns>
        private static TaskParameter CreateProductCodesParameter()
        {
            return new TaskParameter(
                Parameter.ProductCodes,
                Message.UpdateProductAttributesUsingList_ProductCodes,
                Message.UpdateProductAttributesUsingList_ProductCodes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductCodeMultiple,
                null,
                true,
                true);
        }

        /// <summary>
        ///     Create a new instance of <see cref="TaskParameter"/> for the <see cref="Parameter.ProductAttributeUpdateValues"/> parameter.
        /// </summary>
        /// <returns></returns>
        private static TaskParameter CreateProductAttributeUpdateValuesParameter()
        {
            return new TaskParameter(
                Parameter.ProductAttributeUpdateValues, 
                Message.UpdateProductAttributesUsingList_ProductAttributeUpdateValues,
                Message.UpdateProductAttributesUsingList_ProductAttributeUpdateValues_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductAttributeUpdateValueMultiple,
                null,
                true,
                true);
        }

        #endregion
    }
}
