﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32359 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductAttributesUsingList.v1
{
    /// <summary>
    ///     <see cref="ITaskContext"/> implementation for the <c>Update Product Attributes Using List</c> <see cref="Task"/>.
    /// </summary>
    internal class TaskContext : Ccm.Engine.TaskContext
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) {}

        #endregion

        #region Properties

        #region AttributeUpdateValues

        private IEnumerable<ProductAttributeUpdateValue> _attributeUpdateValues;

        internal IEnumerable<ProductAttributeUpdateValue> AttributeUpdateValues
        {
            get { return _attributeUpdateValues ?? (_attributeUpdateValues = PlanogramProduct.GetAvailableProductAttributeUpdateValues()); }
        }

        #endregion

        #region Product Gtins

        /// <summary>
        ///     The list of product Gtins to update with the new values.
        /// </summary>
        private List<String> _productGtins;

        /// <summary>
        ///     Get the list of product Gtins to update with the new values.
        /// </summary>
        /// <remarks>[Lazy] This collection is initialized right when requested for the first time.</remarks>
        public List<String> ProductGtins { get { return _productGtins ?? (_productGtins = new List<String>()); } }

        #endregion

        #region Product Attribute Update Values

        /// <summary>
        ///     The list of product attribute update values to be used when updating products.
        /// </summary>
        private List<ProductAttributeUpdateValue> _productAttributeUpdateValues;

        /// <summary>
        ///     Get the list of product attribute update values to be used when updating products.
        /// </summary>
        /// <remarks>[Lazy] This collection is initialized right when requested for the first time.</remarks>
        internal List<ProductAttributeUpdateValue> ProductAttributeUpdateValues
        {
            get { return _productAttributeUpdateValues ?? (_productAttributeUpdateValues = new List<ProductAttributeUpdateValue>()); }
        }

        #endregion

        #region Target Products

        /// <summary>
        ///     The list of products to update.
        /// </summary>
        private List<IPlanogramProductInfo> _targetProducts;

        /// <summary>
        ///     Get the list of products to update.
        /// </summary>
        /// <remarks>[Lazy] This collection is initialized right when requested for the first time.</remarks>
        internal List<IPlanogramProductInfo> TargetProducts { get { return _targetProducts ?? (_targetProducts = new List<IPlanogramProductInfo>()); } }

        #endregion

        #region Products Updated

        /// <summary>
        ///     Get or set the count of products updated in the task.
        /// </summary>
        internal Int32 UpdatedProductCount { get; set; }

        #endregion

        #endregion
    }
}