﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24779 : D.Pleasance
//  Created
// V8-24779 : N.Foster
//  Removed common path from source path of planograms
// V8-28143 : A.Kuszyk
//  Added basic logging.
// V8-28329 : A.Probyn
//  Extended to create PlanogramHierarchy groups when a Move by category code task is setup.
// V8-28557 : D.Pleasance
//  Added validation to check that the merch group exists when creating plan hierarchy by category code
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Code Review
#endregion
#region Version History: CCM830
// CCM-18508 : M.Brumby
//  Handled empty source path values
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.MovePlanogram.v1
{
    /// <summary>
    /// Moves the Planogram to a Planogram Group based on its source path or category code.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            MoveType = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.MovePlanogram_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.MovePlanogram_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_RepositoryActions; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Do not capture debug for this task
        /// </summary>
        public override bool CaptureDebug
        {
            get { return false; }
        }

        /// <summary>
        /// Indicates that this task has a pre-step
        /// </summary>
        public override Boolean HasPreStep
        {
            get { return true; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // move type
            this.RegisterParameter(new TaskParameter(
                Parameter.MoveType,
                Message.MovePlanogram_Parameter_MoveType_Name,
                Message.MovePlanogram_Parameter_MoveType_Description,
                Message.ParameterCategory_Default,
                typeof(MoveType),
                MoveType.MoveBySourcePath,
                false,
                false));
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called once prior to executing the task for each planogram
        /// </summary>
        public override void OnPreExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateWorkpackageParameters(taskContext)) return;
                if (!this.GetSourcePaths(taskContext)) return;
                if (!this.GetRootPath(taskContext)) return;
                if (!this.CreatePlanogramHierarchy(taskContext)) return;
            }
        }

        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateParameters(taskContext)) return;
                if (!this.GetPlanogramGroup(taskContext)) return;
                if (!this.MovePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for the workpackage
        /// </summary>
        private Boolean ValidateWorkpackageParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                foreach (WorkpackagePlanogram planogram in context.Workpackage.Planograms)
                {
                    #region Move Type

                    // validate the move type
                    WorkflowTaskParameter moveTypeTaskParameter = context.WorkflowTask.Parameters.FirstOrDefault(p => p.ParameterDetails.Id == (Int32)Parameter.MoveType);
                    WorkpackagePlanogramParameter moveTypeWorkflowParameter = planogram.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == moveTypeTaskParameter.Id);
                    if ((moveTypeWorkflowParameter == null) ||
                        (moveTypeWorkflowParameter.Value == null))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MovePlanogram_Parameter_MoveType_Name, Message.Error_NoValue);
                        success = false;
                    }
                    else
                    {
                        try
                        {
                            context.WorkpackageMoveTypes.Add(planogram.DestinationPlanogramId, (MoveType)moveTypeWorkflowParameter.Value);
                        }
                        catch
                        {
                            context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MovePlanogram_Parameter_MoveType_Name, moveTypeWorkflowParameter.Value);
                            success = false;
                        }
                    }

                    #endregion
                }

                return success;
            }
        }

        /// <summary>
        /// Determines the source path for all planograms in the workpackage
        /// </summary>
        private Boolean GetSourcePaths(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of those planograms that have
                // a move type of sourcepath, as we are only
                // interested in those planograms
                List<Object> planogramIds = new List<Object>();
                foreach (var item in context.WorkpackageMoveTypes)
                {
                    if (item.Value == MoveType.MoveBySourcePath)
                        planogramIds.Add(item.Key);
                }

                // if no items have a sourcepath move type
                // then there is nothing for us to do
                if (planogramIds.Count == 0) return true;

                // build a list of all the source paths for these planograms
                foreach (Object planogramId in planogramIds)
                {
                    using (Package package = Package.FetchById(planogramId, DomainPrincipal.CurrentUserId, PackageLockType.System))
                    {
                        foreach (Planogram planogram in package.Planograms)
                        {
                            if ((!String.IsNullOrEmpty(planogram.SourcePath)) && (!context.WorkpackageSourcePaths.Contains(planogram.SourcePath)))
                                context.WorkpackageSourcePaths.Add(planogram.SourcePath);
                        }
                    }
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Determines the root path for all planograms
        /// within the workpackage
        /// </summary>
        private Boolean GetRootPath(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we have no source paths
                // then there is nothing for us to do
                if (context.WorkpackageSourcePaths.Count == 0) return true;

                // determine the root path amongst all the source paths
                String rootPath = context.WorkpackageSourcePaths[0];
                foreach (String sourcePath in context.WorkpackageSourcePaths.Select(p => Path.GetDirectoryName(p)))
                {
                    StringBuilder sb = new StringBuilder();
                    String[] rootPaths = rootPath.Split(Path.DirectorySeparatorChar);
                    String[] sourcePaths = sourcePath.Split(Path.DirectorySeparatorChar);

                    int pathLengh = Math.Min(rootPaths.Length, sourcePaths.Length);
                    for (Int32 i = 0; i < pathLengh; i++)
                    {
                        if (rootPaths[i] == sourcePaths[i])
                        {
                            sb.Append(rootPaths[i]);
                            sb.Append(Path.DirectorySeparatorChar);
                        }
                        else
                            break;
                    }
                    rootPath = sb.ToString();
                }

                // set the root path in the context
                context.WorkpackageRootPath = rootPath;

                // return success
                return true;
            }
        }

        /// <summary>
        /// Creates the planogram hierarchy
        /// </summary>
        private Boolean CreatePlanogramHierarchy(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if no root path has been specified
                // then there is nothing for us to do
                if (String.IsNullOrEmpty(context.WorkpackageRootPath)) return true;

                // fetch the planogram hierarchy
                PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(context.EntityId);

                // build the hierarchy based on the source path
                foreach (String sourcePath in context.WorkpackageSourcePaths)
                {
                 
                    // remove the root path from the source path
                     String  folderPath = Path.GetDirectoryName(sourcePath.Replace(context.WorkpackageRootPath, String.Empty));
                   

                    // split into sub folders
                    String[] folderNames = folderPath.Split(Path.DirectorySeparatorChar);

                    // create a folder in the planogram hierarchy if required
                    PlanogramGroup currentGroup = planogramHierarchy.RootGroup;
                    for (Int32 i = 0; i < folderNames.Length; i++)
                    {
                        String folderName = folderNames[i];
                        if (!String.IsNullOrEmpty(folderName))
                        {
                            PlanogramGroup childGroup = currentGroup.ChildList.FirstOrDefault(group => group.Name == folderName);
                            if (childGroup == null)
                            {
                                childGroup = PlanogramGroup.NewPlanogramGroup();
                                childGroup.Name = folderName;
                                currentGroup.ChildList.Add(childGroup);
                            }
                            currentGroup = childGroup;
                        }
                    }
                }

                // now attempt to save the planogram hierarchy
                try
                {
                    planogramHierarchy = planogramHierarchy.Save();
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is ConcurrencyException)
                    {
                        // requeue the task and try again
                        throw new TaskRetryException();
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region Move Type

                // validate the product placement type
                if ((!this.Parameters.Contains((Int32)Parameter.MoveType)) ||
                    (this.Parameters[Parameter.MoveType].Value == null) ||
                    (this.Parameters[Parameter.MoveType].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MovePlanogram_Parameter_MoveType_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.PlanogramMoveType = (MoveType)this.Parameters[Parameter.MoveType].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MovePlanogram_Parameter_MoveType_Name, Parameters[Parameter.MoveType].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Gets the destination planogram group
        /// </summary>
        private Boolean GetPlanogramGroup(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (context.PlanogramMoveType == MoveType.MoveBySourcePath)
                    return this.GetPlanogramGroupBySourcePath(context);
                else
                    return this.GetPlanogramGroupByCategoryCode(context);
            }
        }

        /// <summary>
        /// Gets the destination planogram group based on the planogram source path
        /// </summary>
        private Boolean GetPlanogramGroupBySourcePath(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the planogram hierarchy
                PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(context.EntityId);


                //start with the root group
                context.PlanogramGroup = planogramHierarchy.RootGroup;

                //Return if there is no source path
                if (String.IsNullOrWhiteSpace(context.Planogram.SourcePath)) return true;

                // split the source planogram path
                String[] folders = Path.GetDirectoryName(context.Planogram.SourcePath).Split(Path.DirectorySeparatorChar);
                
                // locate the correct planogram group                
                for (Int32 i = 0; i < folders.Length; i++)
                {
                    String folder = folders[i];
                    PlanogramGroup childGroup = context.PlanogramGroup.ChildList.FirstOrDefault(group => group.Name == folder);
                    if (childGroup != null) context.PlanogramGroup = childGroup;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Gets the destination planogram group based on the category code
        /// </summary>
        private Boolean GetPlanogramGroupByCategoryCode(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a lookup of product group codes and their ids
                Dictionary<Int32, String> productGroupLookup = new Dictionary<Int32, String>();
                ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(context.EntityId);
                foreach (ProductGroup productGroup in productHierarchy.RootGroup.EnumerateAllChildGroups())
                {
                    productGroupLookup.Add(productGroup.Id, productGroup.Code);
                }

                // enumerate the child groups of the hierarchy
                PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(context.EntityId);
                foreach (PlanogramGroup planogramGroup in planogramHierarchy.RootGroup.EnumerateAllChildGroups())
                {
                    if ((planogramGroup.ProductGroupId != null) && (context.Planogram.CategoryCode == productGroupLookup[(Int32)planogramGroup.ProductGroupId]))
                    {
                        context.PlanogramGroup = planogramGroup;
                        break;
                    }
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Moves the planogram to the destination planogram group
        /// </summary>
        private Boolean MovePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if no planogram group could be located
                // then do not move the planogram
                if (context.PlanogramGroup == null) return true;

                // create planogram association
                PlanogramGroup.AssociatePlanograms(context.PlanogramGroup.Id, new List<Int32>() { (Int32)context.Planogram.Id });

                // log the move
                context.LogInformation(
                    PlanogramEventLogAffectedType.Planogram,
                    context.Planogram.Id,
                    EventLogEvent.PlanogramMoved,
                    context.PlanogramGroup.Name);

                // return success
                return true;
            }
        }

        #endregion
    }
}