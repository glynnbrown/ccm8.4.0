﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-24779 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MovePlanogram.v1
{
    /// <summary>
    /// Denotes the actions that
    /// can be performed when moving a planogram.
    /// </summary>
    public enum MoveType
    {
        MoveBySourcePath,
        MoveByCategoryCode
    }

    /// <summary>
    /// Contains helpers for the MoveType enum.
    /// </summary>
    public static class MoveTypeHelper
    {
        public static readonly Dictionary<MoveType, String> FriendlyNames =
            new Dictionary<MoveType, String>()
            {
                {MoveType.MoveBySourcePath, Message.MovePlanogram_Parameter_MoveType_MoveBySourcePath},
                {MoveType.MoveByCategoryCode, Message.MovePlanogram_Parameter_MoveType_MoveByCategoryCode },
            };
    }
}