﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29597 : N.Foster
//  Code Review
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.MovePlanogram.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private Dictionary<Object, MoveType> _workpackageMoveTypes = new Dictionary<Object, MoveType>();
        private List<String> _workpackageSourcePaths = new List<String>();
        private String _workpackageRootPath;
        private MoveType _planogramMoveType;
        private PlanogramGroup _planogramGroup;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns the move type for each planogram within the workpackage
        /// indexed by the planogram id
        /// </summary>
        public Dictionary<Object, MoveType> WorkpackageMoveTypes
        {
            get { return _workpackageMoveTypes; }
        }

        /// <summary>
        /// Returns all source paths in the workpackage
        /// </summary>
        public List<String> WorkpackageSourcePaths
        {
            get { return _workpackageSourcePaths; }
        }

        /// <summary>
        /// Gets or sets the workpackage root path
        /// </summary>
        public String WorkpackageRootPath
        {
            get { return _workpackageRootPath; }
            set { _workpackageRootPath = value; }
        }

        /// <summary>
        /// Gets or sets the planogram move type
        /// </summary>
        public MoveType PlanogramMoveType
        {
            get { return _planogramMoveType; }
            set { _planogramMoveType = value; }
        }

        /// <summary>
        /// Gets or sets the destination planogram group
        /// </summary>
        public PlanogramGroup PlanogramGroup
        {
            get { return _planogramGroup; }
            set { _planogramGroup = value; }
        }
        #endregion
    }
}