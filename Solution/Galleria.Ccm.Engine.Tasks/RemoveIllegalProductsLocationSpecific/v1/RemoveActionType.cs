﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30736 : L.Luong
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RemoveIllegalProductsLocationSpecific.v1
{
    /// <summary>
    /// Provides types of action to take when remove a Product from an Location Specific planogram.
    /// </summary>
    public enum RemoveActionType
    {
        RemovePositions = 0,
        RemovePositionsAndProducts = 1,
        RemovePositionsAndAddToCarPark = 2
    }

    // Helper for RemoveActionType enum.
    public static class RemoveActionTypeHelper
    {
        public static Dictionary<RemoveActionType, String> FriendlyNames =
            new Dictionary<RemoveActionType, String>()
            {
                { RemoveActionType.RemovePositionsAndAddToCarPark, Message.RemoveIllegalProductsLocationSpecific_Parameter_RemoveActionType_RemovePositionsAndAddToCarPark },
                { RemoveActionType.RemovePositions, Message.RemoveIllegalProductsLocationSpecific_Parameter_RemoveActionType_RemovePositions },
                { RemoveActionType.RemovePositionsAndProducts, Message.RemoveIllegalProductsLocationSpecific_Parameter_RemoveActionType_RemovePositionsAndProducts }
            };
    }
}
