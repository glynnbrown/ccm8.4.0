﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30427 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.RemoveUnplacedProductsFromProductList.v1
{
    internal class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields

        List<String> _placedGtins = new List<String>();
        List<PlanogramProduct> _unplacedProducts = new List<PlanogramProduct>();
        PlanogramProductList _planogramProducts;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties

        public List<String> PlacedGtins
        {
            get { return _placedGtins; }
            set { _placedGtins = value; }
        }

        public List<PlanogramProduct> UnplacedProducts
        {
            get { return _unplacedProducts; }
            set { _unplacedProducts = value; }
        }

        public PlanogramProductList PlanogramProducts
        {
            get { return _planogramProducts; }
            set { _planogramProducts = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
           //Dispose of anything necessary...
        }

        #endregion
    }
}

