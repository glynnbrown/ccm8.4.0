﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30427 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RemoveUnplacedProductsFromProductList.v1
{
    public class Task : TaskBase
    {
        #region Version

        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter Registration

        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // no parameters to register
        }

        #endregion

        #region Properties

        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RemoveUnplacedProductsFromProductList_Task_Name; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.RemoveUnplacedProductsFromProductList_Task_Description; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Execution

        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidatePlanogram(taskContext)) return;
                if (!this.CheckForUnplacedProductsRequiringRemoval(taskContext)) return;
                if (!this.RemoveUnplacedProductsFromProductList(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks the planogram actually contains any products in it's product list.
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>true if the plan contains at least one product in product list</returns>
        private Boolean ValidatePlanogram(TaskContext taskContext)
        {
            if ((taskContext.Planogram.Products == null) || (!taskContext.Planogram.Products.Any()))
            {
                taskContext.LogWarning(EventLogEvent.PlanogramNoProductsPresent);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether the list of products in the plan contains any unplaced and works out what they are
        /// </summary>
        /// <param name="taskContext">the current task context</param>
        /// <returns>true when at least one unplaced product was found</returns>
        private Boolean CheckForUnplacedProductsRequiringRemoval(TaskContext taskContext)
        {
            taskContext.PlacedGtins = taskContext.Planogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            taskContext.PlanogramProducts = taskContext.Planogram.Products;

            foreach (PlanogramProduct productInPlanogram in taskContext.PlanogramProducts)
            {
                if (taskContext.PlacedGtins.Contains(productInPlanogram.Gtin)) continue;

                taskContext.UnplacedProducts.Add(productInPlanogram);
            }

            if (!taskContext.UnplacedProducts.Any())
            {
                taskContext.LogInformation(EventLogEvent.PlanogramProductsContainedNoUnplacedProducts);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Removes the unplaced products from the product list in the planogram.
        /// </summary>
        /// <param name="taskContext">Current Task Context</param>
        /// <returns>true if successful</returns>
        private Boolean RemoveUnplacedProductsFromProductList(TaskContext taskContext)
        {
            taskContext.PlanogramProducts.RemoveList(taskContext.UnplacedProducts);

            taskContext.LogInformation(EventLogEvent.PlanogramUnplacedProductsRemoved, taskContext.UnplacedProducts.Count());

            return true;
        }

        #endregion
    }
}
