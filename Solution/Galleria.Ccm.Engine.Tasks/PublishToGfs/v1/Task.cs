﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27783 : M.Brumby
//  Created.
// V8-28143 : A.Kuszyk
//  Added basic logging.
// V8-28528 : A.Kuszyk
//  Re-factored OnPostExecute to process plans in batches keyed by project name.
// V8-28546 : L.Ineson
//  If the plan could not be published an error is now logged.
// V8-28572 : A.Kuszyk
//  Added try-catch to GFS publish service call, and logged exceptions relating to products not matching in GFS.
#endregion

#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion

#region Version History: CCM801
// V8-29075 : N.Haywood
//  Made each publish create a new version of the planogram
#endregion

#region Version History: CCM810
// V8-29806 : M.Brumby
//  Refactored code while fixing project name issue
// V8-29987 : M.Brumby
//  Added more defensive coding around what plans to try and assign to the project at the
//  end of the process as well as adding more logging.
// V8-30064 : M.Brumby
//  Brand new planograms will no longer fail to validate when they should pass
// V8-30113 : M.Brumby
//  Prevent publish if assigned to multiple product groups
// V8-30287 : M.Brumby
//  Change to trim category code, forgot to check for null!
#endregion

#region Version History: CCM811
// V8-30417 : M.Brumby
//  Added location parameter which will be used to limit assignments to the location.
//  This may get updated to multiple locations in the future.
#endregion


#region Version History: CCM 8.2.0
// V8-30508 : M.Shelley
//  Added a PublishStatus field to the LocationPlanAssignment table that needs to be set to the 
//  appropriate status when a planogram is published and the GFS context links are set.
// V8-31233 : A.Probyn
//  Updated to publish more properties from the PlanogramProduct and PlanogramPosition structure to GFS.
#endregion

#region Version History: CCM830

// V8-32282 : A.Silva
//  Amended ValidatePublish and SetPublishedPlanograms for an issue when there might be event logs without a TaskSource (it would be null) throwing a null exception.
// V8-32300 : A.Probyn
//  Added more defensive code around dictionary references to ward off duplicate key exceptions.
// V8-32633 : N.Haywood
//  Moved CleanUpOrphanedParts to the planogram model to clean up orphaned parts before publish
// CCM-18393 : M.Brumby
//  PCR01601 - Improvements to CCM publishing
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Processes.Publishing;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Content;
using Galleria.Ccm.Services.Planogram;
using Galleria.Ccm.Services.Project;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Planogram = Galleria.Framework.Planograms.Model.Planogram;

namespace Galleria.Ccm.Engine.Tasks.PublishToGfs.v1
{
    /// <summary>
    /// Publishes a Planogram to GFS.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            GfsProjectName = 0,
            LocationCode = 1,
            CategoryCode = 2,
            PublishType = 3
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.PublishPlanogramToGFS_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.PublishPlanogramToGFS_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// If the task has a post step
        /// </summary>
        public override Boolean HasPostStep
        {
            get { return true; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // gfs project name
            this.RegisterParameter(new TaskParameter(
                Parameter.GfsProjectName,
                Message.PublishPlanogramToGFS_Parameter_GFSProject_Name,
                Message.PublishPlanogramToGFS_Parameter_GFSProject_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.GfsProjectName,
                null,
                false,
                false));

            // Location Code
            this.RegisterParameter(new TaskParameter(
                Parameter.LocationCode,
                Message.PublishPlanogramToGFS_LocationCode_Name,
                Message.PublishPlanogramToGFS_LocationCode_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.LocationCodeSingle,
                null,
                true,
                false));

            // Category Code
            this.RegisterParameter(new TaskParameter(
                Parameter.CategoryCode,
                Message.PublishPlanogramToGFS_CategoryCode_Name,
                Message.PublishPlanogramToGFS_CategoryCode_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.CategoryCodeSingle,
                null,
                true,
                false));

            // Publish type
            this.RegisterParameter(new TaskParameter(
                Parameter.PublishType,
                Message.PublishPlanogramToGFS_PublishType_Name,
                Message.PublishPlanogramToGFS_PublishType_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(PublishType),
                defaultValue: PublishType.Changed,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateParameters(taskContext)) return;
                if (!this.SetPlanPublishContext(taskContext)) return;
                if (!this.PublishPlanogram(taskContext)) return;
            }
        }

        /// <summary>
        /// Called after this task has been executed for all planograms
        /// </summary>
        public override void OnPostExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.SetAssignmentContext(taskContext)) return;
                if (!this.AssignPlanograms(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validate the parameters for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region GfsProjectName

                //  Validate that the GFS Project name parameter exists.
                if ((!this.Parameters.Contains((Int32)Parameter.GfsProjectName)) ||
                     (this.Parameters[Parameter.GfsProjectName].Value == null) ||
                     (this.Parameters[Parameter.GfsProjectName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.PublishPlanogramToGFS_Parameter_GFSProject_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.GfsProjectName = Convert.ToString(this.Parameters[Parameter.GfsProjectName].Value.Value1);
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.PublishPlanogramToGFS_Parameter_GFSProject_Name, Parameters[Parameter.GfsProjectName].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Setup the task context values for publishing the planogram
        /// </summary>
        private Boolean SetPlanPublishContext(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // setup the task context
                Entity ccmEntity = Entity.FetchById(context.EntityId);
                context.GfsEntity = GFSModel.Entity.FetchByCcmEntity(ccmEntity);
                context.User = User.FetchById(context.Workpackage.UserId);
                context.AssignmentList = LocationPlanAssignmentList.FetchByPlanogramId(Convert.ToInt32(context.Planogram.Id));

                //Use the assignment product group code first if it exists otherwise we can't sync the results
                //back from GFS once the plans go live/communicated
                if (context.AssignmentList.Count > 0)
                {
                    Int32 productGroupId = 0;
                    //if the content is assigned to more than one product group then it is invalid
                    if (context.AssignmentList.GroupBy(a => a.ProductGroupId).Count() > 1)
                    {
                        // we cannot publish so fail the plan
                        context.LogError(EventLogEvent.PlanogramLocationAssignmentsInvalid, String.Format("{0}{0}{1}", Environment.NewLine, Message.PublishToGfs_InvalidAssignments_MultipleProductGroups));
                        return false;
                    }
                    productGroupId = context.AssignmentList.FirstOrDefault().ProductGroupId;
                    ProductGroupInfo groupInfo = ProductGroupInfo.FetchById(productGroupId);
                    context.ProductGroupCode = groupInfo.Code;
                }
                else if (context.Planogram.CategoryCode != null && !String.IsNullOrEmpty(context.Planogram.CategoryCode.Trim()))
                {
                    //Use the plans category code if there are no assignments.
                    context.ProductGroupCode = context.Planogram.CategoryCode.Trim();
                }
                else
                {
                    // we cannot publish so fail the plan
                    context.LogError(EventLogEvent.PlanogramCouldNotPublishUnassigned);
                    return false;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Publish the planogram to gfs
        /// </summary>
        private Boolean PublishPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean validated = false;
                using (FoundationServiceClient proxy = new FoundationServiceClient(context.GfsEntity.EndpointRoot))
                {
                    // publish the planogram to GFS
                    context.Planogram.CleanUpOrphanedParts();
                    Galleria.Ccm.Services.Planogram.Planogram dc = null;
                    try
                    {
                        dc = GfsPublish.PublishPlanToGFS(proxy, CcmToGfsPlanogramConverter.ConvertToPlanogramDc(context.Planogram), context.GfsEntity.Name, context.ProductGroupCode);
                    }
                    catch (FaultException ex)
                    {
                        if (Regex.IsMatch(ex.Message, @"product code \'(\w+)\'"))
                        {
                            String productCode = Regex.Match(ex.Message, @"product code \'(\w+)\'").Groups[1].Value;
                            context.LogError(EventLogEvent.PlanogramCouldNotPublishProductsDontMatch, productCode);

                            return false;
                        }
                        throw ex;
                    }
                    finally
                    {
                        validated = ValidatePublish(context, dc);
                    }


                    // if for whatever reason the Unique content reference that has come back from GFS is different
                    // to the one we have (unlikely, unless we have a blank guid) then we must set the CCM plan
                    // to have the same UCR as GFS.
                    if (validated && !context.Planogram.UniqueContentReference.Equals(dc.UniqueContentReference))
                    {
                        context.Planogram.UniqueContentReference = dc.UniqueContentReference;
                    }
                }

                if (validated)
                {
                    // log the success.
                    context.LogInformation(EventLogEvent.PlanogramPublishedToGfs);
                }


                // return success
                return true;
            }
        }

        /// <summary>
        /// Checks to see if the plan has made it to GFS. If it has not then
        /// a message will be logged stating that it will not be linked to
        /// a project.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dc"></param>
        private static Boolean ValidatePublish(TaskContext context, Content dc)
        {
            using (new CodePerformanceMetric()) 
            {
                Boolean success;
                using (var proxy = new FoundationServiceClient(context.GfsEntity.EndpointRoot))
                {
                    if (dc == null)
                    {
                        success = false;
                    }
                    else
                    {
                        //If the plan has errored before this then it will not be linked to the project.
                        if (context.Planogram.EventLogs.Any(log => IsErrorFromThisTask(context, log)))
                            success = false;
                        else
                        {
                            //Take the unique content reference from the datacontract as the planogram will
                            //not have been updated with it yet.
                            ExistsContent response =
                                proxy.ContentServiceClient.ExistsContentByUniqueContentReference(
                                    new ExistsContentByUniqueContentReferenceRequest(dc.UniqueContentReference)).ExistsContent;

                            //if there is no source plan id then we can't get the assignment list later
                            //so it will be excluded from the plan lists
                            success = (response.Exists && !response.IsDeleted && context.WorkpackagePlanogram.SourcePlanogramId.HasValue);
                        }
                    }
                }

                if (!success)
                {
                    context.LogError(EventLogEvent.PublishErrorUnableToLink, context.Planogram.Name, context.GfsProjectName);
                }

                return success;
            }
        }

        /// <summary>
        /// Setup the task context values for assigning planograms to the project
        /// </summary>
        private Boolean SetAssignmentContext(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // setup the task context
                Entity ccmEntity = Entity.FetchById(context.EntityId);
                context.GfsEntity = GFSModel.Entity.FetchByCcmEntity(ccmEntity);

                // return success
                return SetPublishedPlanograms(context);
            }
        }

        /// <summary>
        /// Creates lists of planograms available and unavailable in gfs.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>False if there are no plangorams in GFS</returns>
        private static Boolean SetPublishedPlanograms(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                using (var proxy = new FoundationServiceClient(context.GfsEntity.EndpointRoot))
                {
                    // For every planogram in this tasks workpackage we need to check to see if it is assigned
                    // any locations.
                    foreach (WorkpackagePlanogram plan in context.Workpackage.Planograms)
                    {
                        //Get the current planogram so that we can find out its UCR (this could be changed to be faster!)
                        Package package = Package.FetchById(plan.DestinationPlanogramId);
                        Planogram planogram = package.Planograms.First(p => p.Id.Equals(plan.DestinationPlanogramId));

                        //Errored plans will not have their UCR updated. So we need to check to see if the current plan
                        //failed earlier so that we can prevent an older version in GFS from linking to the project.
                        if (planogram.EventLogs.Any(e => IsErrorFromThisTask(context, e)))
                        {
                            if (!context.PlanogramsNotInGFS.ContainsKey(planogram))
                            {
                                context.PlanogramsNotInGFS.Add(planogram, plan);
                            }
                        }
                        else
                        {
                            ExistsContent response =
                                proxy.ContentServiceClient.ExistsContentByUniqueContentReference(
                                    new ExistsContentByUniqueContentReferenceRequest(planogram.UniqueContentReference)).ExistsContent;

                            //if there is no source plan id then we can't get the assignment list later
                            //so it should be excluded from the plan lists
                            if (response.Exists &&
                                !response.IsDeleted &&
                                plan.SourcePlanogramId.HasValue)
                            {
                                if (!context.PlanogramsInGFS.ContainsKey(planogram))
                                {
                                    context.PlanogramsInGFS.Add(planogram, plan);
                                }
                            }
                            else
                            {
                                if (!context.PlanogramsNotInGFS.ContainsKey(planogram))
                                {
                                    context.PlanogramsNotInGFS.Add(planogram, plan);
                                }
                            }
                        }
                    }
                }

                if (context.PlanogramsInGFS.Count == 0)
                {
                    LoggingHelper.Logger.Write(
                        EventLogEntryType.Error,
                        "Engine",
                        context.Workpackage.EntityId,
                        EventLogEvent.PublishAllAssignmentsFailed,
                        context.Workpackage.Name,
                        context.Workpackage.Id);

                    // The publish status is determined by the field "LocationPlanAssignment_PlanPublishStatus"
                    // and not the work flow automation status so always return true
                    //return false;     
                }

                return true;
            }
        }

        /// <summary>
        /// Setup the task context values for assigning planograms to the project
        /// </summary>
        private Boolean AssignPlanograms(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean logged = false;
                try
                {
                    // A dictionary of project contents keyed by project name, so that we can batch update them 
                    // with the GFS web services.
                    var contentsByProjectName = new Dictionary<String, List<ProjectContent>>();

                    // A list of assignments which we will modify and will need to save.
                    var assignmentLists = new List<LocationPlanAssignmentList>();
                                     
                    // Get the workflow task parameter
                    WorkflowTaskParameter projectTaskParameter = context.WorkflowTask.Parameters.First(p => p.ParameterDetails.Id == (Int32)Parameter.GfsProjectName);
                    WorkflowTaskParameter locationTaskParameter = context.WorkflowTask.Parameters.First(p => p.ParameterDetails.Id == (Int32)Parameter.LocationCode);
                    WorkflowTaskParameter categoryTaskParameter = context.WorkflowTask.Parameters.First(p => p.ParameterDetails.Id == (Int32)Parameter.CategoryCode);
                    WorkflowTaskParameter publishTypeTaskParameter = context.WorkflowTask.Parameters.First(p => p.ParameterDetails.Id == (Int32)Parameter.PublishType);
                    using (var proxy = new FoundationServiceClient(context.GfsEntity.EndpointRoot))
                    {

                        // For every planogram in this tasks workpackage we need to check to see if it is assigned
                        // any locations.
                        foreach (var record in context.PlanogramsInGFS)
                        {
                            WorkpackagePlanogram plan = record.Value;
                            
                            // Determine the location code filter for this plan
                            var locationParameter = plan.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == locationTaskParameter.Id);
                            String locationCodeFilter = String.Empty;
                            if (locationParameter != null && locationParameter.Values.Count > 0)
                            {
                                locationCodeFilter = Convert.ToString(locationParameter.Values.First().Value2);
                            }
                            // Determine the category code filter for this plan
                            var categoyParameter = plan.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == categoryTaskParameter.Id);
                            String categoyCodeFilter = String.Empty;
                            if (categoyParameter != null && categoyParameter.Values.Count > 0)
                            {
                                categoyCodeFilter = Convert.ToString(categoyParameter.Values.First().Value2);
                            }
                            // Determine the location code filter for this plan
                            var publishTypeParameter = plan.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == publishTypeTaskParameter.Id);
                            PublishType? publishType = null;
                            if (publishTypeParameter != null && publishTypeParameter.Values.Count > 0)
                            {
                                publishType = (PublishType)Convert.ToInt32(publishTypeParameter.Values.First().Value1);
                            }
                            
                            // Determine the project name for this plan, so that we can insert its content
                            // against the right key in the dictionary.
                            var parameter = plan.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == projectTaskParameter.Id);
                            if (parameter?.Value == null) continue;
                            String projectName = Convert.ToString(parameter.Value);

                            if (String.IsNullOrEmpty(projectName)) continue;
                            if (!plan.SourcePlanogramId.HasValue) continue;

                            //Fetch all the assigment list for the current planogram
                            Int32 planId = plan.SourcePlanogramId.Value;
                            LocationPlanAssignmentList assignmentList = context.CompileAssignmentPublishList(planId, locationCodeFilter, categoyCodeFilter, publishType);
                            Dictionary<Int32, PlanogramInfo> assignedPlans = PlanogramInfoList.FetchByIds(assignmentList.Select(a => a.PlanogramId).Distinct()).ToDictionary(p => p.Id);

                            // Go through the assignment list to create project content location records that can be inserted.
                            foreach (var assignmentGroup in assignmentList.GroupBy(a => a.PlanogramId))
                            {
                                Guid uniqueContentReference;
                                if (!TryGetUniqueContentReference(assignmentGroup, record, assignedPlans, out uniqueContentReference)) continue;
                                
                                List<ProjectContent> projectContents = GetProjectContentsOrDefault(contentsByProjectName, projectName);
                                ProjectContent content = GetProjectContentOrDefault(projectContents, uniqueContentReference);

                                foreach (LocationPlanAssignment assignment in assignmentGroup)
                                {
                                    assignment.PublishedByUserId = User.FetchById(context.Workpackage.UserId).Id;
                                    assignment.SetPublishedDate(DateTime.UtcNow);
                                    assignment.PublishType = PlanPublishType.PublishGFS;
                                    assignment.OutputDestination = projectName;
                                    assignment.PublishStatus = PlanAssignmentPublishStatusType.Successful;

                                    LocationInfo location;
                                    String locationCode =
                                        context.LocationInfoIdLookup.TryGetValue(assignment.LocationId, out location)
                                            ? location.Code
                                            : Location.FetchById(assignment.LocationId).Code;

                                    if (content.Locations.Any(cl => cl.LocationCode == locationCode)) continue;
                                    content.Locations.Add(new ProjectContentLocation
                                    {
                                        LocationCode = locationCode,                                       
                                    });
                                }
                            }
                            
                            // Add the assignment that we have changed to the list of assignments to save.
                            assignmentLists.Add(assignmentList);
                        }


                        // For each unique project name, publish the content that is relevant.
                        foreach (String projectName in contentsByProjectName.Keys)
                        {
                            if (String.IsNullOrEmpty(projectName)) continue;
                            GfsPublish.SaveToProject(proxy, contentsByProjectName[projectName], context.GfsEntity.Name, projectName);
                        }

                        // Write a failed record in the LocationPlanAssignment table for each assignment for a failed plan publish
                        foreach (var failPlan in context.PlanogramsNotInGFS)
                        {
                            WorkpackagePlanogram tmpPlan = failPlan.Value;

                            //Fetch all the assigment list for the current planogram
                            LocationPlanAssignmentList failedAssignmentList = LocationPlanAssignmentList.FetchByPlanogramId(tmpPlan.SourcePlanogramId.Value);

                            // Determine the location code filter for this plan
                            var locationParameter = tmpPlan.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == locationTaskParameter.Id);
                            String locationCodeFilter = String.Empty;
                            if (locationParameter != null && locationParameter.Values.Count > 0)
                            {
                                locationCodeFilter = Convert.ToString(locationParameter.Values.First().Value2);
                            }

                            foreach (LocationPlanAssignment assignment in failedAssignmentList)
                            {
                                LocationInfo location = null;
                                String locationCode;
                                if (!context.LocationInfoIdLookup.TryGetValue(assignment.LocationId, out location))
                                {
                                    locationCode = Location.FetchById(assignment.LocationId).Code;
                                }
                                else
                                {
                                    locationCode = location.Code;
                                }

                                //if we have a filter then skip anything that doesn't match
                                if (String.IsNullOrEmpty(locationCodeFilter) || locationCodeFilter.Equals(locationCode))
                                {
                                    assignment.PublishStatus = PlanAssignmentPublishStatusType.Failed;
                                }

                                // Add the assignment that we have changed to the list of assignments to save.
                                assignmentLists.Add(failedAssignmentList);
							}
                        }
                    }

                    // Save the changes to the assignments list.
                    assignmentLists.ForEach(l => l.Save());

                    if (context.PlanogramsNotInGFS.Count == 0)
                    {
                        LoggingHelper.Logger.Write(
                             EventLogEntryType.Information,
                             "Engine",
                             context.Workpackage.EntityId,
                             EventLogEvent.PublishPlanogramAssignmentResults,
                             context.Workpackage.Name,
                             context.Workpackage.Id,
                             context.PlanogramsInGFS.Count,
                             context.Workpackage.Planograms.Count);
                        logged = true;
                    }
                    else
                    {
                        LoggingHelper.Logger.Write(EventLogEntryType.Error,
                            "Engine",
                            context.Workpackage.EntityId,
                             EventLogEvent.PublishPlanogramAssignmentResults,
                             context.Workpackage.Name,
                             context.Workpackage.Id,
                             context.PlanogramsInGFS.Count,
                             context.Workpackage.Planograms.Count);
                        logged = true;
                    }
                }
                finally
                {
                    if (!logged)
                    {
                        LoggingHelper.Logger.Write(
                         EventLogEntryType.Error,
                         "Engine",
                         context.Workpackage.EntityId,
                         EventLogEvent.PublishAllAssignmentsFailed,
                         context.Workpackage.Name,
                         context.Workpackage.Id);
                    }
                }
                // return success
                return true;
            }
        }

        private static ProjectContent GetProjectContentOrDefault(List<ProjectContent> projectContents, Guid uniqueContentReference)
        {
            ProjectContent projectContent = projectContents.FirstOrDefault(c => c.UniqueContentReference == uniqueContentReference);
            if (projectContent != null) return projectContent;

            projectContent = new ProjectContent
            {
                UniqueContentReference = uniqueContentReference,
                Locations = new List<ProjectContentLocation>()
            };
            projectContents.Add(projectContent);

            return projectContent;
        }

        private static List<ProjectContent> GetProjectContentsOrDefault(Dictionary<String, List<ProjectContent>> contentsByProjectName, String projectName)
        {
            List<ProjectContent> projectContents;
            if (!contentsByProjectName.TryGetValue(projectName, out projectContents))
            {
                projectContents = new List<ProjectContent>();
                contentsByProjectName[projectName] = projectContents;
            }
            return projectContents;
        }

        private static Boolean TryGetUniqueContentReference(IGrouping<Int32, LocationPlanAssignment> assignmentGroup,
                                                         KeyValuePair<Planogram, WorkpackagePlanogram> record,
                                                         Dictionary<Int32, PlanogramInfo> assignedPlans,
                                                         out Guid uniqueContentReference)
        {
            //Use the current planogram UCR if it exists as it might have changed and not been
            //saved to the database yet
            if (Equals(assignmentGroup.Key, record.Key.Id))
            {
                uniqueContentReference = record.Key.UniqueContentReference;
            }
            else
            {
                PlanogramInfo planInfo;
                if (assignedPlans.TryGetValue(assignmentGroup.Key, out planInfo))
                {
                    //Hopefully the plan will have been published before
                    //ExistsContent response =
                    //    proxy.ContentServiceClient.ExistsContentByUniqueContentReference(
                    //        new ExistsContentByUniqueContentReferenceRequest(planInfo.UniqueContentReference)).ExistsContent;

                    ////check to see if the plan is still in gfs.
                    //if (!response.Exists || response.IsDeleted) continue;

                    uniqueContentReference = planInfo.UniqueContentReference;
                }
                else
                {
                    //if we can't find the plan continue.
                    uniqueContentReference = Guid.Empty;
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
