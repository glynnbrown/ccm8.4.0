﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29806 : M.Brumby
//  Created
// V8-29987 : M.Brumby
//  Added planograms to assign
#endregion
#region Version History: CCM811
// V8-30417 : M.Brumby
//  Added location parameter which will be used to limit assignments to the location.
//  This may get updated to multiple locations in the future.
#endregion
#region Version History: CCM830
// CCM-18393 : M.Brumby
//  PCR01601 - Improvements to CCM publishing
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.PublishToGfs.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private String _gfsProjectName; // holds the project name the plans will be published to
        private GFSModel.Entity _gfsEntity; //the gfs entity to publish to
        private User _user; //the user publishing the plan
        private LocationPlanAssignmentList _assignmentList; //links between locations and plans
        private String _productGroupCode; //the product group for this plan
        private Dictionary<Planogram, WorkpackagePlanogram> _planogramsInGFS = new Dictionary<Planogram, WorkpackagePlanogram>();
        private Dictionary<Planogram, WorkpackagePlanogram> _planogramsNotInGFS = new Dictionary<Planogram, WorkpackagePlanogram>();
        private Dictionary<String, LocationInfo> _locationInfoCodeLookup = null;
        private Dictionary<Int16, LocationInfo> _locationInfoIdLookup = null;
        private Dictionary<Int16, IEnumerable<LocationPlanAssignment>> _locationAssignmentCache = new Dictionary<short, IEnumerable<LocationPlanAssignment>>();
        private Dictionary<Int32, IEnumerable<LocationPlanAssignment>> _categoryAssignmentCache = new Dictionary<Int32, IEnumerable<LocationPlanAssignment>>();

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the GFS Project name
        /// </summary>
        public String GfsProjectName
        {
            get { return _gfsProjectName; }
            set { _gfsProjectName = value; }
        }

        /// <summary>
        /// Gets or sets the GFS entity
        /// </summary>
        public GFSModel.Entity GfsEntity
        {
            get { return _gfsEntity; }
            set { _gfsEntity = value; }
        }

        /// <summary>
        /// Gets or sets the Product Group Code
        /// </summary>
        public String ProductGroupCode
        {
            get { return _productGroupCode; }
            set { _productGroupCode = value; }
        }
        /// <summary>
        /// Gets or sets the User
        /// </summary>
        public User User
        {
            get { return _user; }
            set { _user = value; }
        }

        /// <summary>
        /// Gets or sets the list of links between locations and plans
        /// </summary>
        public LocationPlanAssignmentList AssignmentList
        {
            get { return _assignmentList; }
            set { _assignmentList = value; }
        }

        /// <summary>
        /// For use in OnPostExecute contains a list of all
        /// workpackage planograms that exist in GFS.
        /// </summary>
        public Dictionary<Planogram, WorkpackagePlanogram> PlanogramsInGFS
        {
            get
            {
                return _planogramsInGFS;
            }
        }

        /// <summary>
        /// For use in OnPostExecute contains a list of all
        /// workpackage planograms that are not in GFS.
        /// </summary>
        public Dictionary<Planogram, WorkpackagePlanogram> PlanogramsNotInGFS
        {
            get
            {
                return _planogramsNotInGFS;
            }
        }

        public Dictionary<String, LocationInfo> LocationInfoCodeLookup
        {
            get
            {
                PopulateLocationLookups();
                return _locationInfoCodeLookup;
            }
        }
        public Dictionary<Int16, LocationInfo> LocationInfoIdLookup
        {
            get
            {
                PopulateLocationLookups();
                return _locationInfoIdLookup;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the location info lookups if they are not already populated.
        /// </summary>
        private void PopulateLocationLookups()
        {
            LocationInfoList locations = null;
            if (_locationInfoCodeLookup == null)
            {
                if (locations == null) locations = LocationInfoList.FetchByEntityId(EntityId);

                _locationInfoCodeLookup = locations.ToDictionary(l => l.Code);
            }

            if (_locationInfoIdLookup == null)
            {
                if (locations == null) locations = LocationInfoList.FetchByEntityId(EntityId);

                _locationInfoIdLookup = locations.ToDictionary(l => l.Id);
            }
        }

        /// <summary>
        /// Creates a list of plan assignments to be published
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="locationCode"></param>
        /// <param name="productGroupCode"></param>
        /// <param name="publishType"></param>
        /// <returns></returns>
        public LocationPlanAssignmentList CompileAssignmentPublishList(Int32 planId, String locationCode, String productGroupCode, PublishType? publishType)
        {
            LocationPlanAssignmentList output = LocationPlanAssignmentList.NewLocationPlanAssignmentList();
            PublishType type = publishType ?? PublishType.Changed;

            Int32? categoryId = GetProductGroupId(productGroupCode);

            switch (type)
            {
                case PublishType.All:
                    foreach (LocationPlanAssignment assignment in LocationPlanAssignmentList.FetchByPlanogramId(planId))
                    {
                        //If we have a cagtegory id and it doesn't match then skip the assignment
                        if (categoryId.HasValue && !Equals(assignment.ProductGroupId, categoryId.Value)) continue;

                        String assignmentLocationCode = GetLocationCode(assignment.LocationId);
                        //If we have a location code and it doesn't match then skip the assignment
                        if (!String.IsNullOrEmpty(locationCode) && !locationCode.Equals(assignmentLocationCode)) continue;

                        output.Add(assignment);
                    }
                    LocationInfo locationInfo;
                    if (!String.IsNullOrWhiteSpace(locationCode) && this.LocationInfoCodeLookup.TryGetValue(locationCode, out locationInfo))
                    {
                        IEnumerable<LocationPlanAssignment> locationAssignements;
                        if(!_locationAssignmentCache.TryGetValue(locationInfo.Id, out locationAssignements))
                        {
                            locationAssignements = LocationPlanAssignmentList.FetchByEntityIdLocationId(this.EntityId, locationInfo.Id).Where(a => a.DatePublished.HasValue).ToList();
                            _locationAssignmentCache[locationInfo.Id] = locationAssignements;
                        }

                        //get all assignments by location that have been published
                        foreach (LocationPlanAssignment assignment in locationAssignements)
                        {
                            if (!output.Any(o => Equals(assignment.Id, o.Id)))
                            {
                                output.Add(assignment);
                            }
                        }
                    }
                    if (!String.IsNullOrWhiteSpace(productGroupCode) && categoryId.HasValue)
                    {
                        IEnumerable<LocationPlanAssignment> categoryAssignements;
                        if (!_categoryAssignmentCache.TryGetValue(categoryId.Value, out categoryAssignements))
                        {
                            categoryAssignements = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(this.EntityId, categoryId.Value).Where(a => a.DatePublished.HasValue).ToList();
                            _categoryAssignmentCache[categoryId.Value] = categoryAssignements;
                        }

                        //get all assignments by category that have been published
                        foreach (LocationPlanAssignment assignment in categoryAssignements)
                        {
                            if (!output.Any(o => Equals(assignment.Id, o.Id)))
                            {
                                output.Add(assignment);
                            }
                        }
                    }
                    break;
                case PublishType.Changed:

                    foreach (LocationPlanAssignment assignment in LocationPlanAssignmentList.FetchByPlanogramId(planId))
                    {
                        //If we have a cagtegory id and it doesn't match then skip the assignment
                        if (categoryId.HasValue && !Equals(assignment.ProductGroupId, categoryId.Value)) continue;

                        String assignmentLocationCode = GetLocationCode(assignment.LocationId);
                        //If we have a location code and it doesn't match then skip the assignment
                        if (!String.IsNullOrEmpty(locationCode) && !locationCode.Equals(assignmentLocationCode)) continue;

                        output.Add(assignment);
                    }
                    break;
            }

            return output;
        }

        /// <summary>
        /// Helper to lookup a location code from Id
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        private String GetLocationCode(Int16 locationId)
        {
            LocationInfo assignmentLocation = null;            
            if (!this.LocationInfoIdLookup.TryGetValue(locationId, out assignmentLocation))
            {
                return Location.FetchById(locationId).Code;
            }
            else
            {
                return assignmentLocation.Code;
            }
        }

        /// <summary>
        /// Helper to lookup a product group id from code
        /// </summary>
        /// <param name="productGroupCode"></param>
        /// <returns></returns>
        private Int32? GetProductGroupId(String productGroupCode)
        {
            if (!String.IsNullOrWhiteSpace(productGroupCode))
            {
                ProductGroupInfoList groups = ProductGroupInfoList.FetchByProductGroupCodes(new String[] { productGroupCode });
                if (groups.Any())
                {
                    return groups.First().Id;
                }
            }

            return null;
        }
        #endregion
    }
}
