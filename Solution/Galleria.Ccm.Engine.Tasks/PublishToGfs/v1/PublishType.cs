﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: CCM830

// CCM-18393 : M.Brumby
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.PublishToGfs.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// add to carpark shelf param
    /// </summary>
    public enum PublishType
    {
        All,
        Changed
    }

    /// <summary>
    /// Contains helpers for the AddToCarparkType enum.
    /// </summary>
    public static class PublishTypeHelper
    {
        public static readonly Dictionary<PublishType, String> FriendlyNames =
            new Dictionary<PublishType, String>()
            {
                {PublishType.All, Message.PublishPlanogramToGFS_PublishType_All },
                {PublishType.Changed, Message.PublishPlanogramToGFS_PublishType_Changed },
            };
    }
}
