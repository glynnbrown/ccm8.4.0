﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1
{
    public enum ProcessingMethodType
    {
        SingleIncrementPerPass = 0,
        MaximumIncreasePerPass = 1
    }

    public static class ProcessingMethodTypeHelper
    {
        public static Dictionary<ProcessingMethodType, String> FriendlyNames =
            new Dictionary<ProcessingMethodType, String>()
            {
                { ProcessingMethodType.SingleIncrementPerPass, Message.ProcessingMethodType_SingleIncreasePerProduct },
                { ProcessingMethodType.MaximumIncreasePerPass, Message.ProcessingMethodType_MaximumIncreasePerProduct }
            };
    }
}
