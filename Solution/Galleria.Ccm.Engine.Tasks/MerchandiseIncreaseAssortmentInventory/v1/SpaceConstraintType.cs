﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
// V8-32612 : D.Pleasance
//  Added MinimumBlockSpace
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1
{
    public enum SpaceConstraintType
    {
        BlockSpace = 0,
        BeyondBlockSpace = 1,
        MinimumBlockSpace = 2
    }

    public static class SpaceConstraintTypeHelper
    {
        public static Dictionary<SpaceConstraintType, String> FriendlyNames = new Dictionary<SpaceConstraintType, String>()
        {
            { SpaceConstraintType.BeyondBlockSpace, Message.MerchandiseIncreaseInventory_SpaceConstraintType_BeyondBlockSpace}, 
            { SpaceConstraintType.BlockSpace, Message.MerchandiseIncreaseInventory_SpaceConstraintType_WithinBlockSpace},
            { SpaceConstraintType.MinimumBlockSpace, Message.MerchandiseIncreaseInventory_SpaceConstraintType_MinimumBlockSpace},
        };
    }
}
