﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
// V8-32372 : D.Pleasance
//  Amended so that task logs error if any of (Assortment, Blocking, Sequence) is missing from the plan.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32358 : D.Pleasance
//  Amended LogInventoryChanges() to log inventory changes as information.
// V8-32612 : D.Pleasance
//  Amended PlanogramMerchandisingBlock constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32671 : D.Pleasance
//  Added call to update merchandising block sequence numbers, UpdateMerchandisingBlockPositionPlacementsWithActualSequenceGroupNumber()
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// V8-32800 : D.Pleasance
//  Changes applied to increase inventory units fairly between positions that are targeted to increase. 
//  To achieve this we keep a running total of the target units whilst checking this against the actual placed units.
// V8-32825 : A.Silva
//  Added call to RestoreSequenceNumbers in the context so that any altered sequence numbers as part of the process are restored.
// V8-32787 : A.Silva
//  Added an AssortmentRuleEnforcer field to allow easy enabling of rule enforcing.
// V8-32982 : A.Kuszyk
//  Re-factored common code into PlanogramMerchandiser helper class and introduced performance enhancements.
// CCM-18563 : A.Silva
//  Changed the way BuildDictionaries gets the Merchandising Groups so that now all outside the blocking space are ignored.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1
{
    /// <summary>
    /// Increases the inventory of products on a plan, whilst maintaining block presentation.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version

        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter
        private enum Parameter
        {
            TopPercentageOfProducts = 0,
            ProcessingMethodOne = 1,
            ProcessingMethodTwo = 2,
            SpaceConstraint = 3,
            CarParkComponentName = 4
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.MerchandiseProductInventoryUsingAssortment_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.MerchandiseProductInventoryUsingAssortment_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // top percentage of products
            this.RegisterParameter(new TaskParameter(
                Parameter.TopPercentageOfProducts,
                Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name,
                Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.25,
                false,
                false));

            // processing method one
            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingMethodOne,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingMethodType),
                ProcessingMethodType.MaximumIncreasePerPass,
                false,
                false));

            // processing method two
            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingMethodTwo,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingMethodType),
                ProcessingMethodType.SingleIncrementPerPass,
                false,
                false));

            // space constraint
            this.RegisterParameter(new TaskParameter(
                Parameter.SpaceConstraint,
                Message.MerchandiseProductInventoryUsingAssortment_SpaceConstraint_Name,
                Message.MerchandiseProductInventoryUsingAssortment_SpaceConstraint_Description,
                Message.ParameterCategory_Default,
                typeof(SpaceConstraintType),
                SpaceConstraintType.BlockSpace,
                false,
                false));

            // car park
            this.RegisterParameter(new TaskParameter(
                Parameter.CarParkComponentName,
                Message.MerchandiseProductInventoryUsingAssortment_Parameter_CarParkComponentName_Name,
                Message.MerchandiseProductInventoryUsingAssortment_Parameter_CarParkComponentName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.String,
                Message.InserProduct_Parameter_CarParkComponentName_Default,
                true,
                true,
                null,
                null,
                null,
                () => IsCarParkNameValid()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!PopulateLookups(taskContext)) return;
                if (!IncreaseBlockInventory(taskContext)) return;
                if (!IncreaseInventory(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
                ReMerchandisePlanogram(taskContext);
            }
        }

        /// <summary>
        /// Performs inventory increases on all the applicable products in all of the blocks in one go
        /// to try to fill up as much space as possible, before reverting to individual product increases.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean IncreaseBlockInventory(TaskContext taskContext)
        {
            PlanogramMerchandiser<TaskContext> planogramMerchandiser = new PlanogramMerchandiser<TaskContext>(taskContext);
            Boolean unitsWereIncreased = true;

            foreach (ProcessingMethodType processingMethod in taskContext.EnumerateProcessingMethods())
            {
                while (unitsWereIncreased)
                {
                    taskContext.CurrentProcessingMethod = taskContext.ProcessingMethodOne;
                    taskContext.CurrentAssortmentProducts = GetAssortmentProductsToIncrease(taskContext.CurrentProcessingMethod, taskContext);
                    Dictionary<Int32, Dictionary<String, Int32>> blockUnitsBySequenceColour = GetBlockUnitsBySequenceColour(taskContext);
                    if (blockUnitsBySequenceColour == null) break;

                    unitsWereIncreased = planogramMerchandiser.MerchandiseInventoryChange(
                        taskContext.Blocking.EnumerateGroupsInOptimisationOrder(), blockUnitsBySequenceColour);
                } 
            }

            return true;
        }

        /// <summary>
        /// Gets a dictionary of all the inventory increases that would be required, given the current
        /// processing methods.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Dictionary<Int32, Dictionary<String, Int32>> GetBlockUnitsBySequenceColour(TaskContext taskContext)
        {
            Dictionary<String, PlanogramAssortmentProduct> assortmentProductsByGtin = 
                taskContext.CurrentAssortmentProducts.ToDictionary(p => p.Gtin);
            Dictionary<Int32, Dictionary<String, Int32>> blockUnitsBySequenceColour =
                new Dictionary<Int32, Dictionary<String, Int32>>();
            Boolean unitsWereIncreased = false;
            
            foreach(KeyValuePair<Int32,Dictionary<String,Int32>> dictionaryBySequenceColour in taskContext.TargetUnitsBySequenceColour)
            {
                Dictionary<String,Int32> blockUnits = new Dictionary<String,Int32>();
                blockUnitsBySequenceColour.Add(dictionaryBySequenceColour.Key, blockUnits);

                foreach(KeyValuePair<String,Int32> unitByGtin in dictionaryBySequenceColour.Value)
                {
                    Int32 units;
                    PlanogramAssortmentProduct assortmentProduct;
                    if(assortmentProductsByGtin.TryGetValue(unitByGtin.Key, out assortmentProduct))
                    {
                        if(taskContext.CurrentProcessingMethod == ProcessingMethodType.SingleIncrementPerPass)
                        {
                            if(unitByGtin.Value < assortmentProduct.Units && assortmentProduct.Units != 0)
                            {
                                units = unitByGtin.Value + 1;
                                unitsWereIncreased = true;
                            }
                            else
                            {
                                units = unitByGtin.Value;
                            }
                        }
                        else
                        {
                            if (unitByGtin.Value < assortmentProduct.Units)
                            {
                                units = assortmentProduct.Units;
                                unitsWereIncreased = true;
                            }
                            else
                            {
                                units = unitByGtin.Value;
                            }
                        }
                    }
                    else
                    {
                        units = unitByGtin.Value;
                    }

                    blockUnits.Add(unitByGtin.Key, units);
                }
            }

            return unitsWereIncreased ? blockUnitsBySequenceColour : null;
        }

        /// <summary>
        /// Performs the increase of inventory
        /// </summary>
        private static Boolean IncreaseInventory(TaskContext context)
        {
            foreach (ProcessingMethodType processingMethod in context.EnumerateProcessingMethods())
            {
                context.CurrentProcessingMethod = processingMethod;
                context.CurrentAssortmentProducts = GetAssortmentProductsToIncrease(context.CurrentProcessingMethod, context);

                while (context.CurrentAssortmentProducts.Any())
                {
                    List<PlanogramAssortmentProduct> assortmentProductsToRemove = new List<PlanogramAssortmentProduct>();

                    foreach (PlanogramAssortmentProduct assortmentProduct in context.CurrentAssortmentProducts)
                    {
                        context.CurrentAssortmentProduct = assortmentProduct;
                        context.CurrentProductPositions = GetCurrentProductPositions(context);

                        if (!context.CurrentProductPositions.Any())
                        {
                            context.ProductsWithNoPositions++;
                            assortmentProductsToRemove.Add(assortmentProduct);
                            continue;
                        }

                        switch (context.CurrentProcessingMethod)
                        {
                            case ProcessingMethodType.SingleIncrementPerPass:
                                if (TryIncreaseInventory(context))
                                    assortmentProductsToRemove.Add(assortmentProduct);
                                break;

                            case ProcessingMethodType.MaximumIncreasePerPass:
                                while (!TryIncreaseInventory(context))
                                {
                                    // Just keep increasing inventory while possible.
                                }
                                assortmentProductsToRemove.Add(assortmentProduct);
                                break;

                            default:
                                System.Diagnostics.Debug.Fail("Unrecognised processing method");
                                break;
                        }
                    }

                    foreach (PlanogramAssortmentProduct assortmentProduct in assortmentProductsToRemove)
                    {
                        context.CurrentAssortmentProducts.Remove(assortmentProduct);
                    }
                }

                LogInventoryChanges(context);
            }

            return true;
        }

        private static List<PlanogramAssortmentProduct> GetAssortmentProductsToIncrease(
            ProcessingMethodType processingMethod, TaskContext context)
        {
            var productCeiling = (Int32)Math.Ceiling(context.TopPercentageOfProducts * context.TotalNumberOfProducts);
            context.CurrentAssortmentProducts = processingMethod == context.ProcessingMethodOne
                                                    ? context.AssortmentProducts.Take(productCeiling).ToList()
                                                    : context.AssortmentProducts.Skip(productCeiling).ToList();
            return context.CurrentAssortmentProducts;
        }

        /// <summary>
        /// Gets all the position placements for the current assortment product.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static PlanogramPositionPlacementList GetCurrentProductPositions(TaskContext context)
        {
            return PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(context.MerchandisingGroups.SelectMany(m => m.PositionPlacements).Where(p => p.Product.Gtin.Equals(context.CurrentAssortmentProduct.Gtin)));
        }

        /// <summary>
        /// Attempts to increase the units for the given position and registers the result
        /// in the given logging context.
        /// </summary>
        /// <param name="context">The logging context in which to register the increase.</param>
        /// <returns>
        /// True if the target units are achieved.
        /// False if the target units have not been achieved and further increases should be attempted.
        /// </returns>
        private static Boolean TryIncreaseInventory(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                context.CurrentProductUnits = context.CurrentProductPositions.GetPositionDetails().TotalUnitCount;
                if (!RecordStartingUnits(context)) return false;

                // Try to increase the inventory by one and remove the product if
                // no more increases are required.
                Boolean? unitsAchieved = IncreaseUnits(context);
                if (unitsAchieved == null)
                {
                    context.ProductsPartiallyAchievedUnits++;
                    context.FinalUnits[context.CurrentAssortmentProduct.Gtin] = context.CurrentProductUnits;
                    return true;
                }

                // If unitsAchieved is false, continue with iteration.
                if (!unitsAchieved.Value) return false;

                // If we reach this stage, the product must have achieved its target units.
                context.ProductsAchievedTargets++;
                context.FinalUnits[context.CurrentAssortmentProduct.Gtin] = context.CurrentProductUnits;
                return true;
            }
        }

        /// <summary>
        /// Records the starting units of the current assortment product in the task context.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static Boolean RecordStartingUnits(TaskContext context)
        {
            //  If there are no starting units for this product,
            //  register the current units, this must be the first time
            //  that increase inventory is attempted on this product.
            if (!context.StartingUnits.ContainsKey(context.CurrentAssortmentProduct.Gtin))
            {
                context.StartingUnits.Add(context.CurrentAssortmentProduct.Gtin, context.CurrentProductUnits);

                //  If the product units are already on target log it
                //  and return false (cannot increase inventory more).
                if (context.CurrentAssortmentProduct.Units <= context.CurrentProductUnits)
                {
                    context.ProductsAlreadyAtTarget++;
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Increases the inventory of the given assortment product by one unit.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>
        /// True if the target units were achieved by the increase.
        /// False if the target units have not yet been achieved.
        /// Null if there is no space for further increases and the target units have not been achieved.
        /// </returns>
        private static Boolean? IncreaseUnits(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                PlanogramMerchandiser<TaskContext> planogramMerchandiser = new PlanogramMerchandiser<TaskContext>(context);

                if (context.CurrentProductUnits < context.CurrentAssortmentProduct.Units)
                {
                    //  Order the placements in ascending unit count (try first to increase the ones with less units).
                    IEnumerable<PlanogramPositionPlacement> placementsOrderedByUnitCount = context.CurrentProductPositions
                        .OrderBy(p => p.GetPositionDetails().TotalUnitCount)
                        .ThenBy(p => p.Id);

                    //  Return the first, or null, position placement that successfully increases units.
                    PlanogramPositionPlacement positionAffected = placementsOrderedByUnitCount
                        .FirstOrDefault(p => planogramMerchandiser.MerchandiseInventoryChange(p));

                    // determine if we managed to increase
                    // the inventory for a position
                    if (positionAffected == null)
                    {
                        // if we could not increase the inventory for
                        // a position, then we are finished with this
                        // assortment product so flag it for removal
                        return null;
                    }

                    // get the current units again
                    context.CurrentProductPositions = GetCurrentProductPositions(context);
                    context.CurrentProductUnits = context.CurrentProductPositions.GetPositionDetails().TotalUnitCount;

                    // if we are now equal to or above our target
                    // units then remove this assortment product
                    return context.CurrentProductUnits >= context.CurrentAssortmentProduct.Units;
                }
                else
                {
                    // this case should never actually happen, but
                    // just in case ensure the assortment product
                    // is added to the list of assortment products
                    // to be removed
                    return true;
                }
            }
        }

        private static void LogInventoryChanges(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                StringBuilder inventoryChanges = new StringBuilder();
                Int32 totalInventoryChanges = 0;

                // Log the changes
                foreach (PlanogramAssortmentProduct assortmentProduct in context.AssortmentProducts)
                {
                    String currentGtin = assortmentProduct.Gtin;

                    //  Get the starting units,
                    Int32 startingUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.StartingUnits.TryGetValue(currentGtin, out startingUnits)) continue;

                    //  Get the final units,
                    Int32 finalUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.FinalUnits.TryGetValue(currentGtin, out finalUnits)) continue;

                    //  The increase had no effect.
                    if (startingUnits == finalUnits) continue;

                    inventoryChanges.AppendFormat(Message.TaskLogging_PlanogramProductInventoryIncreased, currentGtin, assortmentProduct.Name, startingUnits, finalUnits);

                    totalInventoryChanges++;
                }

                //  Log the change.
                context.LogInformation(EventLogEvent.PlanogramProductInventoryIncreased, totalInventoryChanges, inventoryChanges);
            }
        }

        /// <summary>
        /// Validate the parameter values for this task.
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                var success = true;

                #region TopPercentageOfProducts

                //  Validate that the top percentage of products parameter exists
                if (!Parameters.Contains((Int32) Parameter.TopPercentageOfProducts) || Parameters[Parameter.TopPercentageOfProducts].Value?.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.TopPercentageOfProducts = Convert.ToSingle(Parameters[Parameter.TopPercentageOfProducts].Value.Value1);
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name, Parameters[Parameter.TopPercentageOfProducts].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProcessingMethodOne

                //  Validate that the processing method one parameter exists
                if (!Parameters.Contains((Int32) Parameter.ProcessingMethodOne) || Parameters[Parameter.ProcessingMethodOne].Value?.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.ProcessingMethodOne = (ProcessingMethodType) Parameters[Parameter.ProcessingMethodOne].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name, Parameters[Parameter.ProcessingMethodOne].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProcessingMethodTwo

                //  Validate that the processing method one parameter exists
                if (!Parameters.Contains((Int32) Parameter.ProcessingMethodTwo) || Parameters[Parameter.ProcessingMethodTwo].Value?.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.ProcessingMethodTwo = (ProcessingMethodType) Parameters[Parameter.ProcessingMethodTwo].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name, Parameters[Parameter.ProcessingMethodTwo].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Space Constraint

                if (!Parameters.Contains((Int32) Parameter.SpaceConstraint) || Parameters[Parameter.SpaceConstraint].Value?.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.SpaceConstraint = (SpaceConstraintType) Parameters[Parameter.SpaceConstraint].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Name, Parameters[Parameter.SpaceConstraint].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32) Parameter.CarParkComponentName)) || Parameters[Parameter.CarParkComponentName].Value?.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseProductInventoryUsingAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) ||
                        (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseProductInventoryUsingAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Populates the task context with the assortment products and other data.
        /// </summary>
        private static Boolean PopulateLookups(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                #region Assortment

                // verify we have an assortment
                if (context.Planogram.Assortment?.Products == null || ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                {
                    context.LogError(EventLogEvent.PlanogramNoAssortmentPresent);
                    return false;
                }
                
                // get the assortment products ordered by rank and then GTIN
                // NF - we are only interested in ranged products within the assortment
                context.AssortmentProducts = context.Planogram.Assortment.Products.Where(p => p.IsRanged).OrderBy(p => p.Rank).ThenBy(p => p.Gtin).ToList();
                context.TotalNumberOfProducts = context.AssortmentProducts.Count;

                #endregion

                #region Merchandising Groups

                context.MerchandisingGroups = context.Planogram.GetMerchandisingGroupsInBlockingArea();

                #endregion

                #region Blocking

                Single width,
                       height,
                       offset;
                context.Planogram.GetBlockingAreaSize(out height, out width, out offset);
                context.BlockingWidth = width;
                context.BlockingHeight = height;
                context.BlockingHeightOffset = offset;

                context.Blocking = context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ?? context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ?? context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial) ?? context.Planogram.Blocking.FirstOrDefault();
                if (context.Blocking == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent, Message.TaskLogging_BlockingStrategy, Message.TaskLogging_NoBlockingStrategyWasPresent);
                    return false;
                }

                #endregion

                #region Validate Sequence Data

                //  If there is no sequence the task cannot proceed.
                if (context.Planogram.Sequence == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent, Message.TaskLogging_Sequence, Message.TaskLogging_NoSequenceInformationWasPresent);
                    return false;
                }
                
                //  OR If there are no sequence groups the task cannot proceed.
                if (context.Planogram.Sequence.Groups.Count == 0 ||
                    context.Planogram.Sequence.Groups.All(g => g.Products.Count == 0))
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent, Message.TaskLogging_SequenceProducts, Message.TaskLogging_NoSequenceProductsWerePresent);
                    return false;
                }

                #endregion

                #region Product Stacks

                context.ProductStacksBySequenceColour = context.Blocking.GetAndUpdateProductStacksByColour(context.MerchandisingGroups);

                #endregion

                #region Target Units

                context.TargetUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();

                IEnumerable<PlanogramPosition> allSequencedPositions =
                    context.MerchandisingGroups
                           .SelectMany(merchGroup => merchGroup.PositionPlacements, (merchGroup, placement) => placement.Position)
                           .Where(position => position.SequenceColour.HasValue);
                foreach (PlanogramPosition position in allSequencedPositions)
                {
                    Dictionary<String, Int32> targetUnits;
                    Int32 sequenceColour = position.SequenceColour ?? 0;
                    if (!context.TargetUnitsBySequenceColour.TryGetValue(sequenceColour, out targetUnits))
                    {
                        targetUnits = new Dictionary<String, Int32>();
                        context.TargetUnitsBySequenceColour.Add(sequenceColour, targetUnits);
                    }

                    String productGtin = position.GetPlanogramProduct().Gtin;
                    if (targetUnits.ContainsKey(productGtin)) continue;
                    targetUnits.Add(productGtin, position.TotalUnits);
                }

                #endregion

                return true;
            }
        }

        /// <summary>
        /// Logs final details about the task execution.
        /// </summary>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                // Log that the assortment was correctly increased.
                context.LogInformation(EventLogEvent.PlanogramAssortmentProductsUnitsIncreased, context.TotalNumberOfProducts, context.ProductsAlreadyAtTarget, context.ProductsAchievedTargets, context.ProductsPartiallyAchievedUnits, context.ProductsWithNoPositions);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Ensure that the planogram is remerchandised
        /// </summary>
        private static void ReMerchandisePlanogram(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                context.RestoreSequenceNumbers();
                context.RemerchandisePlanogram();
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters?[Parameter.CarParkComponentName] == null || !this.Parameters.Contains((Int32) Parameter.CarParkComponentName) || this.Parameters[Parameter.CarParkComponentName].Value?.Value1 == null)
                return Message.NullComponentName;
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString().Length > PlanogramComponent.MaximumComponentNameLength)
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            else
                return null;
        }

        #endregion
    }
}
