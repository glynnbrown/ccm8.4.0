﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
// V8-32612 : D.Pleasance
//  Added GetSpaceConstraintType
// V8-32982 : A.Kuszyk
//  Implemented IPlanogramMerchandiserContext.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1
{
    public class TaskContext : Ccm.Engine.TaskContext, IPlanogramMerchandiserContext
    {
        #region Fields
        private PlanogramAssortmentProduct _currentAssortmentProduct;
        private PlanogramPositionPlacementList _currentProductPositions;
        private PlanogramBlocking _blocking;
        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            StartingUnits = new Dictionary<String, Int32>();
            FinalUnits = new Dictionary<String, Int32>();
            RanksByProductGtin = new Dictionary<String, Int32>();
            PositionsToIgnore = new List<String>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The width of the blocking space in planogram units.
        /// </summary>
        public Single BlockingWidth { get; set; }

        /// <summary>
        /// The height of the blocking space in planogram units.
        /// </summary>
        public Single BlockingHeight { get; set; }

        /// <summary>
        /// The height offset of the blocking space in planogram units.
        /// </summary>
        public Single BlockingHeightOffset { get; set; }

        /// <summary>
        /// A dictionary of product stacks, keyed by their sequence group colour.
        /// </summary>
        public Dictionary<Int32, Stack<PlanogramProduct>> ProductStacksBySequenceColour { get; set; }

        /// <summary>
        /// A dictionary of target unit dictionaries (units keyed by gtin), keyed by the sequence group colour they belong to.
        /// </summary>
        public Dictionary<Int32, Dictionary<String, Int32>> TargetUnitsBySequenceColour { get; set; }

        /// <summary>
        /// Indicates whether or not the task should increase into or beyond block space.
        /// </summary>
        public SpaceConstraintType SpaceConstraint { get; set; }

        /// <summary>
        ///     Gets or sets the top percentage of products.
        /// </summary>
        public Single TopPercentageOfProducts { get; set; }

        /// <summary>
        ///     Gets or sets the primary <see cref="ProcessingMethodType"/>.
        /// </summary>
        public ProcessingMethodType ProcessingMethodOne { get; set; }

        /// <summary>
        ///     Gets or sets the secondary <see cref="ProcessingMethodType"/>.
        /// </summary>
        public ProcessingMethodType ProcessingMethodTwo { get; set; }

        public ProcessingMethodType CurrentProcessingMethod { get; set; }

        /// <summary>
        ///     Gets or sets the stack of <see cref="PlanogramAssortmentProduct"/> instances to process.
        /// </summary>
        public List<PlanogramAssortmentProduct> AssortmentProducts { get; set; }

        /// <summary>
        /// The <see cref="Planogram"/>'s merchandising groups.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        public List<PlanogramAssortmentProduct> CurrentAssortmentProducts { get; set; }

        public Int32 ProductsWithNoPositions { get; set; }
        public Dictionary<String, Int32> StartingUnits { get; private set; }
        public Dictionary<String, Int32> FinalUnits { get; private set; }

        public PlanogramAssortmentProduct CurrentAssortmentProduct { get; set; }

        public Int32 ProductsAlreadyAtTarget { get; set; }
        public Int32 ProductsPartiallyAchievedUnits { get; set; }
        public Int32 ProductsAchievedTargets { get; set; }

        public PlanogramPositionPlacementList CurrentProductPositions { get; set; }

        public Int32 TotalNumberOfProducts { get; set; }
        public Int32 CurrentProductUnits { get; set; }

        public PlanogramBlocking Blocking 
        { 
            get { return _blocking;  }
            set 
            { 
                _blocking = value;
                BlockingGroupsByColour = _blocking.Groups.ToDictionary(b => b.Colour);
            }
        }

        public Dictionary<Int32, PlanogramBlockingGroup> BlockingGroupsByColour{ get; private set; }

        public List<String> PositionsToIgnore
        {
            get;
            set;
        }

        public Dictionary<String, Int32> RanksByProductGtin
        {
            get;
            set;
        }

        /// <summary>
        ///     Gets or sets the name for the car park component.
        /// </summary>
        public String CarParkComponentName { get; set; }
        PlanogramMerchandisingSpaceConstraintType IPlanogramMerchandiserContext.SpaceConstraint
        {
            get { return GetSpaceConstraintType(SpaceConstraint); }
        }

        public Dictionary<Object, PlanogramPerformanceData> PerformanceDataByProductId
        {
            get
            {
                return new Dictionary<Object, PlanogramPerformanceData>();
            }
        }

        PlanogramMerchandisingInventoryChangeType IPlanogramMerchandiserContext.InventoryChangeType
        {
            get { return PlanogramMerchandisingInventoryChangeType.ByUnits; }
        }

        #endregion

        #region Methods
        public PlanogramMerchandisingSpaceConstraintType GetSpaceConstraintType(SpaceConstraintType spaceConstraintType)
        {
            switch (spaceConstraintType)
            {
                case SpaceConstraintType.BlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
                case SpaceConstraintType.BeyondBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace;
                case SpaceConstraintType.MinimumBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation;
            }

            return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
        }

        protected override void OnDipose()
        {
            base.OnDipose();
            if (MerchandisingGroups != null) MerchandisingGroups.Dispose();
        }

        public IEnumerable<ProcessingMethodType> EnumerateProcessingMethods()
        {
            yield return ProcessingMethodOne;
            yield return ProcessingMethodTwo;
            yield break;
        }

        public void LogStartingUnits(String gtin, Int32 units)
        {

        }

        public void LogStartingUnits(PlanogramPositionPlacement position)
        {
            
        }

        public void LogFinalUnits(PlanogramPositionPlacement position)
        {
            
        }

        public void LogFinalUnits(String gtin, Int32 units)
        {
        }
        #endregion
    }
}
