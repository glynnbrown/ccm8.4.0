﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25839 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductPerformanceFromGfs.v1
{
    /// <summary>
    /// Update the planogram products performance information 
    /// from the GFS solution directly.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdateProductPerformanceFromGfs_Name; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.UpdateProductPerformanceFromGfs_Category; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Indicates that this task has a pre step
        /// </summary>
        public override Boolean HasPreStep
        {
            get { return true; }
        }

        /// <summary>
        /// Indicates that this task has a post step
        /// </summary>
        public override Boolean HasPostStep
        {
            get { return true; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            //TODO
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(TaskContext context)
        {
            // TODO
        }
        #endregion
    }
}
