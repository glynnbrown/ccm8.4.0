﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26950 : A.Kuszyk
//  Created.
// V8-27241 : A.Kuszyk
//  Changed fetch method to EntityIdName.
// V8-27485 : A.Kuszyk
//  Changed OnExecute to use GetBlockingByName code.
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-27692 : A.Kuszyk
//  Add try-catch for GetBlockingException to ensure that errors are caught and logged.
// V8-28060 : A.Kuszyk
//  Added detailed logging.

#endregion

#region Version History: CCM801

// V8-28612 : A.Kuszyk
//  Defended against null parameter values.

#endregion

#region Version History: CCM802

// V8-28989 : A.Kuszyk
//  Combined with GetAndRecalculateBlocking task.

#endregion

#region Version History: CCM803

// V8-29425 : N.Foster
//  Hide this task as the ability to create blocking strategies has also been hidden

#endregion

#region Version History: CCM810

// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
// V8-30141 : A.Silva
//  Refactored use of GetAvailableSpaceOnAxis.

#endregion

#region Version History: CCM830
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.GetBlockingByName.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 1; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter
        {
            BlockingName = 0,
            MetricProfileName = 1
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetBlockingByName_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetBlockingByName_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.GetBlockingByName_Category; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Indicates if this task is visible or not
        /// </summary>
        public override bool IsVisible
        {
            get { return false; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.BlockingName,
                Message.GetBlockingByName_BlockingName,
                Message.GetBlockingByName_BlockingName_Description,
                Message.GetBlockingByName_Category,
                TaskParameterType.BlockingNameSingle,
                null,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.MetricProfileName,
                Message.GetAndReCalculateBlockingByName_MetricProfileName,
                Message.GetAndReCalculateBlockingByName_MetricProfileName_Description,
                Message.GetBlockingByName_Category,
                TaskParameterType.MetricProfileNameSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get parameter values
                String blockingName;
                String metricProfileName;
                if (!GetAndValidateParameters(context, out blockingName, out metricProfileName)) return;

                // Get parameter objects
                Blocking blockingSource;
                IPlanogramMetricProfile metricProfile;
                if (!GetParameterObjects(context, blockingName, metricProfileName, out blockingSource, out metricProfile)) return;

                // Clear the existing blocking and then add the new blocking as initial (unchanged).
                context.Planogram.Blocking.Clear();
                PlanogramBlocking initialBlocking =
                    PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial, blockingSource);
                context.Planogram.Blocking.Add(initialBlocking);
                context.LogInformation(
                    EventLogEvent.PlanogramBlockingAdded, blockingName, PlanogramBlockingTypeHelper.FriendlyNames[initialBlocking.Type]);

                // Performance adjusted
                PlanogramBlocking adjustedBlocking = null;
                if (metricProfile != null)
                {
                    adjustedBlocking =
                        PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied, initialBlocking);
                    context.Planogram.Blocking.Add(adjustedBlocking);
                    try
                    {
                        adjustedBlocking.ApplyPerformanceData(metricProfile);
                        context.LogInformation(
                            EventLogEvent.PlanogramBlockingAdded, blockingName, PlanogramBlockingTypeHelper.FriendlyNames[adjustedBlocking.Type]);
                    }
                    catch (PlanogramBlockingException)
                    {
                        context.LogWarning(EventLogEvent.PlanogramBlockingMissingProducts);
                        context.Planogram.Blocking.Remove(adjustedBlocking);
                        adjustedBlocking = null;
                    }
                }

                // Now, add the adjusted or initial blocking as final.
                PlanogramBlocking finalBlocking =
                    PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, adjustedBlocking ?? initialBlocking);
                context.Planogram.Blocking.Add(finalBlocking);

                // Having added the blocking as final, try to apply it to the fixtures.
                try
                {
                    finalBlocking.ApplyToFixtures();
                    context.LogInformation(EventLogEvent.PlanogramBlockingAdded, blockingName, PlanogramBlockingTypeHelper.FriendlyNames[finalBlocking.Type]);
                }
                catch (PlanogramBlockingException ex)
                {
                    // If we were unsuccessful, remove the blocking and log the error.
                    context.Planogram.Blocking.Remove(finalBlocking);
                    context.LogError(EventLogEvent.PlanogramBlockingApplyToFixturesFailed, ex.Message);
                }
            }
        }

        private Boolean GetParameterObjects(ITaskContext context, String blockingName, String metricProfileName, out Blocking blockingSource, out IPlanogramMetricProfile metricProfile)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                blockingSource = null;
                metricProfile = null;

                try
                {
                    blockingSource = Blocking.FetchByEntityIdName(context.EntityId, blockingName);
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw;

                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetBlockingByName_BlockingName, blockingName);
                    return false;
                }

                // Only try to get a metric profile if we have a name at this stage.
                if (!String.IsNullOrEmpty(metricProfileName))
                {
                    try
                    {
                        MetricList globalMetricList = MetricList.FetchByEntityId(context.EntityId);

                        metricProfile = MetricProfile.FetchByEntityIdName(context.EntityId, metricProfileName)?.ToPlanogramMetricProfile(context.Planogram.Performance, globalMetricList);

                        if (metricProfile  == null)
                        {
                            context.LogError(EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile);
                            return false;
                        } 
                    }
                    catch (DataPortalException ex)
                    {
                        if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw;

                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetAndReCalculateBlockingByName_MetricProfileName, metricProfileName);
                        return false;
                    }
                }

                return true;
            }
        }

        private Boolean GetAndValidateParameters(ITaskContext context, out String blockingName, out String metricProfileName)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                blockingName = String.Empty;
                metricProfileName = String.Empty;

                // Blocking name.
                if (this.Parameters[Parameter.BlockingName].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetBlockingByName_BlockingName, Message.Error_NoValue);
                    return false;
                }
                blockingName = Convert.ToString(this.Parameters[Parameter.BlockingName].Value.Value1);
                if (String.IsNullOrEmpty(blockingName))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetBlockingByName_BlockingName, Parameters[Parameter.BlockingName].Value.Value1);
                    return false;
                }

                // Metric profile name. It's ok for this parameter to be null, because if it is, we just won't re-calculate
                // the blocking.
                if (this.Parameters[Parameter.MetricProfileName].Value != null)
                {
                    metricProfileName = Convert.ToString(Parameters[Parameter.MetricProfileName].Value.Value1);
                    // If the string is null or empty at this stage, that's ok.
                }

                return true;
            }
        }
        #endregion
    }
}
