﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800
// V8-26773 : N.Foster
//  Created
// V8-27035 : L.Ineson
//  Added aggregation types.
// V8-27472 : L.Ineson
//  Amended PerformanceSelection.FetchByName to take entity id
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-27918 : J.Pickup
//  GetLocationCodes now returns only a collection of codes for the planogram(s) we are interested in.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History : CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History : CCM803
// V8-29491 : D.Pleasance
//  Added performance weighting parameters. Cluster, store lookups are now also obtained from planogram properties (LocationCode, ClusterName)
#endregion
#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Refactored task to use TaskContext
// V8-30086 : D.Pleasance
//  Added PlanogramPerformanceDataNoPerformanceSelection for information logging when performance selection not provided.
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
// V8-30215 : M.Brumby
//  Ensure the aggregation type used for each metric is saved to the metric.
#endregion 
#region Version History : CCM811
// V8-30434 : M.Pettit
//  The message associated with planograms having no assigned clusters has been clarified
// V8-30523 : J.Pickup
//  The message associated with planograms having no assigned location code has been clarified to bring into line with assigned clusters.
#endregion
#region Version History : CCM820
// V8-30727 : D.Pleasance
//  Added PlanogramAssignmentPerformanceWeighting.
// V8-31358 : M.Brumby
//  Ensure we use the correct number of tumeline groups when calculating days of performance
// V8-30990 : D.Pleasance
//  Amended default parameter, EnterprisePerformanceWeighting - 5%, ClusterPerformanceWeighting - 20%, StorePerformanceWeighting 25%, PlanogramAssignmentPerformanceWeighting 50%
#endregion
#region Version History : CCM830
// V8-31550 : A.Probyn
//  Extended to factor in any assortment location and product buddies.
// V8-31830 : A.Probyn
//  Extended to factor in new timeline selection parameter.
// V8-32205 : A.Probyn
//  Updated to keep track of budddies applied and log.
// V8-32260 : A.Probyn
//  Moved point of product buddies being applied to affter any workpackage performance weightings have been factored in.
// V8-32133 : A.Probyn
//  Refactored to use more common code, and fixed a missing use case when the timeline exclusions specified in the task
//  may not be the same as those originally set on the a performance selection.
// V8-32730 : D.Pleasance
//  Added MergePerformanceBehaviourType. 
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
// CCM-14043 : D.Pleasance
//  Changes applied to UpdateProductPerformance() so that if option MergeWhenNoLocationPerformance then additional performance data 
//  is weighted if the product didn’t have performance for the location.
// V8-18448 : J.Pickup
//  Change to ensure performance in the plan is updated prior to the normalisation of the metric profile. (plan performance is swapped out by GFS). 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Performance;
using Galleria.Framework.Dal;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1
{
    /// <summary>
    /// Updates the performance data held by a planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Constants
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        private static Int32 _serverBusyMaxRetryCount = 5; // maximum retry count when the performance service responds that it is too busy
        private static Int32 _serverBusyWaitTime = 15000; // the number of milliseconds to wait before retrying a performance operation
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PerformanceSelectionName = 0,
            EnterprisePerformanceWeighting = 1,
            ClusterPerformanceWeighting = 2,
            StorePerformanceWeighting = 3,
            PlanogramAssignmentPerformanceWeighting = 4,
            MetricProfileName = 5,
            PerformanceTimelinesSelection = 6,
            MergePerformanceBehaviour = 7
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdateProductPerformance_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.UpdateProductPerformance_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Indicates that this task has a pre step
        /// </summary>
        public override bool HasPreStep
        {
            get { return true; }
        }

        /// <summary>
        /// Indicates that this task has a post step
        /// </summary>
        public override bool HasPostStep
        {
            get { return true; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Performance Selection Name
            this.RegisterParameter(new TaskParameter(
                Parameter.PerformanceSelectionName,
                Message.UpdateProductPerformance_PerformanceSelectionName_Name,
                Message.UpdateProductPerformance_PerformanceSelectionName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.PerformanceSelectionNameSingle,
                null,
                false,
                false));

            // Performance Week Selection (selection type, number of weeks, timeline code exclusions)
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.PerformanceTimelinesSelection,
                    name: Message.UpdateProductPerformance_PerformanceWeekSelection_Name,
                    description: Message.UpdateProductPerformance_PerformanceWeekSelection_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.PerformanceTimelinesSelection,
                    defaultValue: null,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false,
                    isRequired: null,
                    isNullAllowed: () => { return false; },
                    isValid: null,
                    parentId: (Int32)Parameter.PerformanceSelectionName));

            // Metric Profile Name
            this.RegisterParameter(new TaskParameter(
                Parameter.MetricProfileName,
                Message.UpdateProductPerformance_MetricProfile_Name,
                Message.UpdateProductPerformance_MetricProfile_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.MetricProfileNameSingle,
                null,
                false,
                false,
                null,
                isRequired: null,
                isNullAllowed: () => { return true; },
                isValid: null));

            // Enterprise weighting
            this.RegisterParameter(new TaskParameter(
                Parameter.EnterprisePerformanceWeighting,
                Message.UpdateProductPerformance_EnterprisePerformanceWeighting_Name,
                Message.UpdateProductPerformance_EnterprisePerformanceWeighting_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.05,
                false,
                false));

            // Cluster weighting
            this.RegisterParameter(new TaskParameter(
                Parameter.ClusterPerformanceWeighting,
                Message.UpdateProductPerformance_ClusterPerformanceWeighting_Name,
                Message.UpdateProductPerformance_ClusterPerformanceWeighting_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.2,
                false,
                false));

            // Store weighting
            this.RegisterParameter(new TaskParameter(
                Parameter.StorePerformanceWeighting,
                Message.UpdateProductPerformance_StorePerformanceWeighting_Name,
                Message.UpdateProductPerformance_StorePerformanceWeighting_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.25,
                false,
                false));

            // Planogram Assignment weighting
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramAssignmentPerformanceWeighting,
                Message.UpdateProductPerformance_PlanogramAssignmentPerformanceWeighting_Name,
                Message.UpdateProductPerformance_PlanogramAssignmentPerformanceWeighting_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.5,
                false,
                false));

            // Merge Performance Behaviour
            this.RegisterParameter(new TaskParameter(
                Parameter.MergePerformanceBehaviour,
                Message.UpdateProductPerformance_MergePerformanceBehaviour_Name,
                Message.UpdateProductPerformance_MergePerformanceBehaviour_Description,
                Message.ParameterCategory_Default,
                typeof(MergePerformanceBehaviourType),
                MergePerformanceBehaviourType.AlwaysMergePerformance,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called before the task is executed
        /// </summary>
        public override void OnPreExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!ValidatePerformanceSelectionTimelines(context)) return;
                GetPerformanceData(context);
            }
        }
        
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!GetAndValidateParameterObjects(taskContext)) return;
                UpdatePlanogramPerformanceMetrics(taskContext);
                ResolveMetricProfileAgainstPlanogramPerformance(taskContext);
                GetLookupDictionaries(taskContext);
                if (!UpdateProductPerformance(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }
        
        /// <summary>
        /// Applies normalisation to the metric profile using task context values
        /// </summary>
        /// <param name="taskContext"></param>
        /// <remarks>Is seperated from the fetch of metric profile as the performance data in GFS updates the performance in the planogram</remarks>
        private void ResolveMetricProfileAgainstPlanogramPerformance(TaskContext taskContext)
        {
            //If the task context doesn't start off as null:
            if (taskContext.MetricProfile != null)
            {
                MetricList globalMetricList = MetricList.FetchByEntityId(taskContext.EntityId);
                taskContext.MetricProfile = taskContext.MasterMetricProfile.ToPlanogramMetricProfile(taskContext.Planogram.Performance, globalMetricList);

                if (taskContext.MetricProfile == null) taskContext.LogError(EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile);
            }
        }

        /// <summary>
        /// Called after the task is executed
        /// </summary>
        public override void OnPostExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                DeleteWorkpackagePerformanceData(context.Workpackage);
            }
        }

        #endregion

        #region Methods

        private static Boolean ValidatePerformanceSelectionTimelines(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!ArePerformanceSelectionTimelinesConsistent(context))
                { 
                    //Throw exception as timelines must be the same for each performance selection
                    throw new ArgumentException("Performance selection timelines must match");
                }

                return true;
            }
        }

        /// <summary>
        /// Check the distinct performance selection timelines across all of the workpackage planogram parameters.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Boolean ArePerformanceSelectionTimelinesConsistent(ITaskContext context)
        {
            //Ensure all of the timeline selections, per performance selection are the same. It is not 
            //valid to have varying timelines selected for the same performance selection.

            // build a list of performance selection names
            // that are within the workpackage
            Dictionary<String, List<Int64>> performanceSelectionTimelines = new Dictionary<String, List<Int64>>();

            // locate the performance selection name
            // parameter within the workflow task
            WorkflowTaskParameter taskParameter = context.WorkflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PerformanceSelectionName);
            WorkflowTaskParameter timelinesTaskParameter = context.WorkflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PerformanceTimelinesSelection);

            // now iterate through all the planograms
            // within the workpackage and build a list
            // of all the unique performance selections
            // timeline codes
            WorkpackagePlanogramList workpackagePlanograms = context.Workpackage.Planograms;
            foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
            {
                foreach (WorkpackagePlanogramParameter planogramParameter in workpackagePlanogram.Parameters)
                {
                    if (planogramParameter.WorkflowTaskParameterId == taskParameter.Id)
                    {
                        //Get timelines for this
                        WorkpackagePlanogramParameter timelineParameter = workpackagePlanogram.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == timelinesTaskParameter.Id);
                        if (timelineParameter != null)
                        {
                            List<Int64> timelineCodes = timelineParameter.Values.Select(p => Convert.ToInt64(p.Value1)).Distinct().ToList();

                            // add the performance selection name to the collection
                            String performanceSelectionName = Convert.ToString(planogramParameter.Value);
                            if (!performanceSelectionTimelines.ContainsKey(performanceSelectionName))
                            {
                                performanceSelectionTimelines.Add(performanceSelectionName, timelineCodes);
                            }
                            else
                            {
                                //Check timelines match, if not, fail
                                if (!CompareTimelineCodeList(performanceSelectionTimelines[performanceSelectionName], timelineCodes)) return false;
                            }
                        }
                        else { /* no parameter setup, old workpackage which we can do nothign with */ }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Compare two lsits of timeline codes to see if they match
        /// </summary>
        public static Boolean CompareTimelineCodeList(List<Int64> timelineCodes1, List<Int64> timelineCodes2)
        {
            if (timelineCodes1.Count != timelineCodes2.Count)
                return false;

            for (int i = 0; i < timelineCodes1.Count; i++)
            {
                if (timelineCodes1[i] != timelineCodes2[i])
                    return false;
            }

            return true;
        }

        private static Boolean GetPerformanceData(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the entity details
                Entity entity = Entity.FetchById(context.EntityId);

                // if there is no gfs settings, then there is nothing to do
                if ((String.IsNullOrEmpty(entity.SystemSettings.FoundationServicesEndpoint)) || (entity.GFSId == null))
                    return true;

                // fetch the gfs entity details
                GFSModel.Entity gfsEntity = GFSModel.Entity.FetchByCcmEntity(entity);

                // delete all the existing performance data
                // that has already been retrieved for this workpackage
                DeleteWorkpackagePerformanceData(context.Workpackage);

                // define a list that will hold all the performance
                // data that is a result of this step
                List<WorkpackagePerformanceDataDto> performanceData = new List<WorkpackagePerformanceDataDto>();

                // enumerate through all the performance selections
                // within the workpackage and their associated
                // planograms
                var gfsPerformanceSources = GFSModel.PerformanceSourceList.FetchAll(gfsEntity);
                var performanceSelections = GetPerformanceSelectionNames(context.Workpackage, context.WorkflowTask);
                var performanceSelectionTimelineExclusions = GetPerformanceSelectionTimelineExclusions(context.Workpackage, context.WorkflowTask);
                foreach (var performanceSelectionItem in performanceSelections)
                {
                    // get the performance selection details
                    String performanceSelectionName = performanceSelectionItem.Key;
                    List<WorkpackagePlanogram> workpackagePlanograms = performanceSelectionItem.Value;

                    // retrieve the performance selection
                    // based upon its name
                    PerformanceSelection performanceSelection = GetPerformanceSelection(context.EntityId, performanceSelectionName);
                    if (performanceSelection != null)
                    {
                        PerformanceDataSelectionHelper performanceDataSelectionHelper = new PerformanceDataSelectionHelper();
                        Single enterprisePerformanceWeighting = 0;
                        Single clusterPerformanceWeighting = 0;
                        Single storePerformanceWeighting = 0;
                        Single planogramAssignmentPerformanceWeighting = 0;

                        // locate the performance source that
                        // we are actually interested in
                        GFSModel.PerformanceSource performanceSource = gfsPerformanceSources.FirstOrDefault(item => item.Id == performanceSelection.GFSPerformanceSourceId);

                        // using the performance selection
                        // build a list of gfs timelines
                        // that we are interested in
                        IEnumerable<Int64> availableTimelineCodes = GetPerformanceSelectionAvailableTimelines(gfsEntity, performanceSource);


                        IEnumerable<Int64> timelineCodeExclusions = performanceSelectionTimelineExclusions.ContainsKey(performanceSelectionName) ? performanceSelectionTimelineExclusions[performanceSelectionName] : null;

                        IEnumerable<Int64> timelineCodes = GetTimelineCodes(gfsEntity, performanceSelection, performanceSource, availableTimelineCodes, timelineCodeExclusions);
                        performanceDataSelectionHelper.TimelineCodes.AddRange(timelineCodes);
                        if (performanceDataSelectionHelper.TimelineCodes.Any())
                        {
                            GetLookupCodes(workpackagePlanograms, performanceDataSelectionHelper, context.WorkflowTask, ref enterprisePerformanceWeighting, ref clusterPerformanceWeighting, ref storePerformanceWeighting, ref planogramAssignmentPerformanceWeighting);

                            if (clusterPerformanceWeighting > 0 && performanceDataSelectionHelper.ClusterSchemes.Any())
                            {
                                ClusterSchemeInfoList clusterSchemeInfoList = ClusterSchemeInfoList.FetchByEntityId(context.EntityId);

                                foreach (String clusterSchemeName in performanceDataSelectionHelper.ClusterSchemes.Keys)
                                {
                                    ClusterSchemeInfo clusterSchemeInfo = clusterSchemeInfoList.Where(p => p.Name == clusterSchemeName).FirstOrDefault();

                                    if (clusterSchemeInfo != null)
                                    {
                                        ClusterScheme clusterScheme = ClusterScheme.FetchById(clusterSchemeInfo.Id);

                                        foreach (String clusterName in performanceDataSelectionHelper.ClusterSchemes[clusterSchemeName])
                                        {
                                            Cluster cluster = clusterScheme.Clusters.Where(p => p.Name == clusterName).FirstOrDefault();

                                            if (cluster != null)
                                            {
                                                //fetch cluster data
                                                performanceDataSelectionHelper.WorkpackagePerformanceDataType = WorkpackagePerformanceDataType.Cluster;
                                                performanceDataSelectionHelper.DataId = cluster.Id;

                                                GetGfsPerformanceData(gfsEntity,
                                                            context.EntityId,
                                                            context.Workpackage,
                                                            performanceSelection,
                                                            performanceData,
                                                            performanceDataSelectionHelper,
                                                            cluster.Locations.Select(p => p.Code).ToList());

                                            }
                                        }
                                    }
                                }
                            }

                            //Store location info list for reuse
                            LocationInfoList locationInfoList = null;

                            if (storePerformanceWeighting > 0 && performanceDataSelectionHelper.LocationCodes.Any())
                            {
                                //fetch store data
                                performanceDataSelectionHelper.WorkpackagePerformanceDataType = WorkpackagePerformanceDataType.Store;

                                GetGfsPerformanceData(gfsEntity,
                                            context.EntityId,
                                            context.Workpackage,
                                            performanceSelection,
                                            performanceData,
                                            performanceDataSelectionHelper,
                                            performanceDataSelectionHelper.LocationCodes.ToList());
                            }

                            if (enterprisePerformanceWeighting > 0)
                            {
                                //fetch enterprise data
                                performanceDataSelectionHelper.WorkpackagePerformanceDataType = WorkpackagePerformanceDataType.Enterprise;
                                performanceDataSelectionHelper.DataId = 0;
                                //Fetch if not yet populated
                                if (locationInfoList == null)
                                {
                                    locationInfoList = LocationInfoList.FetchByEntityId(context.EntityId);
                                }

                                GetGfsPerformanceData(gfsEntity,
                                            context.EntityId,
                                            context.Workpackage,
                                            performanceSelection,
                                            performanceData,
                                            performanceDataSelectionHelper,
                                            locationInfoList.Select(p => p.Code).ToList());
                            }

                            if (planogramAssignmentPerformanceWeighting > 0)
                            {
                                //Fetch if not yet populated
                                if (locationInfoList == null)
                                {
                                    locationInfoList = LocationInfoList.FetchByEntityId(context.EntityId);
                                }
                                Dictionary<Int16, String> locIdToCodeLookup;
                                locIdToCodeLookup = locationInfoList.ToDictionary(p => p.Id, p => p.Code);

                                foreach (Int32 planAssignment in performanceDataSelectionHelper.PlanogramAssignments)
                                {
                                    LocationPlanAssignmentList locationPlanAssignmentList = LocationPlanAssignmentList.FetchByPlanogramId(planAssignment);
                                    performanceDataSelectionHelper.LocationCodes.Clear();

                                    foreach (LocationPlanAssignment locationPlanAssignment in locationPlanAssignmentList)
                                    {
                                        if (!performanceDataSelectionHelper.LocationCodes.Contains(locIdToCodeLookup[locationPlanAssignment.LocationId]))
                                        {
                                            performanceDataSelectionHelper.LocationCodes.Add(locIdToCodeLookup[locationPlanAssignment.LocationId]);
                                        }
                                    }

                                    if (performanceDataSelectionHelper.LocationCodes.Any())
                                    {
                                        //fetch plan assignment data
                                        performanceDataSelectionHelper.WorkpackagePerformanceDataType = WorkpackagePerformanceDataType.PlanogramAssignment;
                                        performanceDataSelectionHelper.DataId = planAssignment;

                                        GetGfsPerformanceData(gfsEntity,
                                                    context.EntityId,
                                                    context.Workpackage,
                                                    performanceSelection,
                                                    performanceData,
                                                    performanceDataSelectionHelper,
                                                    performanceDataSelectionHelper.LocationCodes.ToList());
                                    }
                                }
                            }
                        }
                    }
                }

                // save the performance data
                SaveGfsPerformanceData(context.Workpackage, performanceData);

                // return success
                return true;
            }
        }

        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // parameters
                if (this.Parameters[Parameter.EnterprisePerformanceWeighting].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_EnterprisePerformanceWeighting_Name, Message.Error_NoValue);
                    return false;
                }

                taskContext.EnterprisePerformanceWeighting = Convert.ToSingle(this.Parameters[Parameter.EnterprisePerformanceWeighting].Value.Value1);

                if (this.Parameters[Parameter.ClusterPerformanceWeighting].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_ClusterPerformanceWeighting_Name, Message.Error_NoValue);
                    return false;
                }

                taskContext.ClusterPerformanceWeighting = Convert.ToSingle(this.Parameters[Parameter.ClusterPerformanceWeighting].Value.Value1);

                if (this.Parameters[Parameter.StorePerformanceWeighting].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_StorePerformanceWeighting_Name, Message.Error_NoValue);
                    return false;
                }

                taskContext.StorePerformanceWeighting = Convert.ToSingle(this.Parameters[Parameter.StorePerformanceWeighting].Value.Value1);

                if (this.Parameters[Parameter.PlanogramAssignmentPerformanceWeighting].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_PlanogramAssignmentPerformanceWeighting_Name, Message.Error_NoValue);
                    return false;
                }

                taskContext.PlanogramAssignmentPerformanceWeighting = Convert.ToSingle(this.Parameters[Parameter.PlanogramAssignmentPerformanceWeighting].Value.Value1);

                if (this.Parameters[Parameter.PerformanceSelectionName].Value == null)
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection);
                    return false;
                }

                taskContext.PerformanceSelectionName = Convert.ToString(this.Parameters[Parameter.PerformanceSelectionName].Value.Value1);

                // parameter validation
                if (String.IsNullOrEmpty(taskContext.PerformanceSelectionName))
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection);
                    return false;
                }

                //metric profile name
                if (this.Parameters[Parameter.MetricProfileName].Value != null)
                {
                    taskContext.MetricProfileName = Convert.ToString(this.Parameters[Parameter.MetricProfileName].Value.Value1);
                }

                #region Merge Performance Behaviour

                //  Validate that the Dropped Product Behaviour parameter exists.
                if (Parameters[Parameter.MergePerformanceBehaviour].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.UpdateProductPerformance_MergePerformanceBehaviour_Name,
                                     Message.Error_NoValue);
                    return false;
                }
                else
                {
                    try
                    {
                        taskContext.MergePerformanceBehaviour =
                            (MergePerformanceBehaviourType)Parameters[Parameter.MergePerformanceBehaviour].Value.Value1;
                    }
                    catch
                    {
                        taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.UpdateProductPerformance_MergePerformanceBehaviour_Name,
                                         Parameters[Parameter.MergePerformanceBehaviour].Value.Value1);
                        return false;
                    }
                }

                #endregion

                return true;
            }
        }

        private static Boolean GetAndValidateParameterObjects(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the performance selection that this planogram is using
                try
                {
                    taskContext.PerformanceSelection = PerformanceSelection.FetchByEntityIdName(taskContext.EntityId, taskContext.PerformanceSelectionName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_PerformanceSelectionName_Name, taskContext.PerformanceSelectionName);
                        return false;
                    }
                    throw ex;
                }

                if (!String.IsNullOrEmpty(taskContext.MetricProfileName))
                {
                    // fetch the metric profile selection that this planogram is using
                    try
                    {
                        taskContext.MasterMetricProfile = MetricProfile.FetchByEntityIdName(taskContext.EntityId, taskContext.MetricProfileName);
                    }
                    catch (DataPortalException ex)
                    {
                        if (ex.GetBaseException() is DtoDoesNotExistException)
                        {
                            taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.UpdateProductPerformance_MetricProfile_Name, taskContext.MetricProfileName);
                            return false;
                        }
                        throw ex;
                    }
                }


                // fetch the full entity details
                Entity entity = Entity.FetchById(taskContext.EntityId);

                // fetch the gfs entity that we are interested in
                taskContext.GfsEntity = GFSModel.Entity.FetchByCcmEntity(entity);

                // get the list of metrics for the gfs entity
                taskContext.GfsMetrics = GFSModel.MetricList.FetchAll(taskContext.GfsEntity);

                // get the performance source associated to the performance selection
                taskContext.GfsPerformanceSource = taskContext.PerformanceSelection.GetPerformanceSource(taskContext.GfsEntity);
                taskContext.SourceDaysPerPeriodType = EnumHelper.Parse<GFSModel.PerformanceSourceDaysPerPeriodType>(taskContext.GfsPerformanceSource.DaysPerPeriod, GFSModel.PerformanceSourceDaysPerPeriodType.Unknown);

                taskContext.CalculationResolver = new PerformanceCalculationResolver(taskContext.GfsPerformanceSource?.PerformanceSourceMetrics, taskContext.GfsMetrics);

                return true;
            }
        }

        /// <summary>
        /// Create lookups for the rest of the process
        /// </summary>
        private static void GetLookupDictionaries(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a dictionary of all planogram products
                // indexed by the product GTIN            
                foreach (PlanogramProduct planogramProduct in context.Planogram.Products)
                {
                    context.PlanogramProductsByGtin.Add(planogramProduct.Gtin, planogramProduct);
                }

                //build a dictionary of all locations indexed by the code
                LocationInfoList locationInfos = LocationInfoList.FetchByEntityId(context.EntityId);
                foreach (LocationInfo locationInfo in locationInfos)
                {
                    context.LocationIdsByCode.Add(locationInfo.Code, locationInfo.Id);
                }

                //build a dictionary of all location buddies indexed by the code
                foreach (PlanogramAssortmentLocationBuddy locationBuddy in context.Planogram.Assortment.LocationBuddies)
                {
                    context.LocationBuddiesByCode.Add(locationBuddy.LocationCode, locationBuddy);
                }

                //build a dictionary of all product buddies indexed by the code
                foreach (PlanogramAssortmentProductBuddy productBuddy in context.Planogram.Assortment.ProductBuddies)
                {
                    context.ProductBuddiesByGtin.Add(productBuddy.ProductGtin, productBuddy);
                }
            }
        }

        private static Boolean LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //Log that the updated performance data had buddies applied if necessary (at least 1)
                //Detailed logging will be implemented in a later version.
                if (taskContext.IsAppliedProductBuddyLoggingRequired)
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataProductBuddyApplied);
                }
                if (taskContext.IsAppliedLocationBuddyLoggingRequired)
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataLocationBuddyApplied);
                }

                // Log the number of products we have processed.
                taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataUpdated,
                                taskContext.ProcessedProductCount, taskContext.Planogram.Products.Count,
                                (taskContext.EnterprisePerformanceWeighting * 100), (taskContext.ClusterPerformanceWeighting * 100), (taskContext.StorePerformanceWeighting * 100), (taskContext.PlanogramAssignmentPerformanceWeighting * 100));

                // return success
                return true;
            }
        }

        private static Boolean UpdateProductPerformance(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                List<WorkpackagePerformanceDataDto> performanceData = new List<WorkpackagePerformanceDataDto>();

                if (taskContext.GfsPerformanceSource != null)
                {
                    LocationInfo locationInfo = null;
                    Cluster cluster = null;
                    IEnumerable<String> productsNotToBeMerged = null;

                    if (taskContext.ClusterPerformanceWeighting > 0 || taskContext.StorePerformanceWeighting > 0)
                    {
                        // get location lookup
                        if (taskContext.Planogram.LocationCode != null && taskContext.Planogram.LocationCode.Length > 0)
                        {
                            LocationInfoList locationInfoList = LocationInfoList.FetchByEntityIdLocationCodes(taskContext.EntityId, new List<String>() { taskContext.Planogram.LocationCode });
                            locationInfo = locationInfoList.FirstOrDefault();
                        }

                        // get cluster lookup
                        if (taskContext.Planogram.ClusterSchemeName != null && taskContext.Planogram.ClusterSchemeName.Length > 0 &&
                            taskContext.Planogram.ClusterName != null && taskContext.Planogram.ClusterName.Length > 0)
                        {
                            ClusterSchemeInfoList clusterSchemeInfoList = ClusterSchemeInfoList.FetchByEntityId(taskContext.EntityId);
                            if (clusterSchemeInfoList != null)
                            {
                                ClusterSchemeInfo clusterSchemeInfo = clusterSchemeInfoList.Where(p => p.Name == taskContext.Planogram.ClusterSchemeName).FirstOrDefault();

                                if (clusterSchemeInfo != null)
                                {
                                    ClusterScheme clusterScheme = ClusterScheme.FetchById(clusterSchemeInfo.Id);
                                    cluster = clusterScheme.Clusters.Where(p => p.Name == taskContext.Planogram.ClusterName).FirstOrDefault();
                                }
                            }
                        }
                    }

                    if (taskContext.StorePerformanceWeighting > 0)
                    {
                        if (locationInfo != null)
                        {
                            // fetch the store performance data for this planogram and weight accordingly
                            GetPlanogramPerformanceData(
                                taskContext,
                                performanceData,
                                taskContext.Workpackage,
                                taskContext.PerformanceSelection,
                                WorkpackagePerformanceDataType.Store,
                                locationInfo.Id,
                                taskContext.MergePerformanceBehaviour == MergePerformanceBehaviourType.AlwaysMergePerformance ? taskContext.StorePerformanceWeighting : 1F);

                            if (taskContext.MergePerformanceBehaviour == MergePerformanceBehaviourType.MergeWhenNoLocationPerformance && performanceData.Any())
                            {
                                productsNotToBeMerged = performanceData.Select(p => p.ProductGTIN);                                
                            }
                        }
                        else
                        {
                            // unable to weight store performance data, store lookup reference failed.
                            String returnableDetails = Message.UpdateProductPerformance_NoLocationCodeAssigned;
                            if (!String.IsNullOrEmpty(taskContext.Planogram.LocationCode)) returnableDetails = taskContext.Planogram.LocationCode;

                            taskContext.LogWarning(EventLogEvent.PlanogramPerformanceDataInvalidLocationReference, returnableDetails);
                        }
                    }

                    if (taskContext.EnterprisePerformanceWeighting > 0)
                    {
                        // fetch the enterprise performance data for this planogram and weight accordingly
                        GetPlanogramPerformanceData(
                            taskContext,
                            performanceData,
                            taskContext.Workpackage,
                            taskContext.PerformanceSelection,
                            WorkpackagePerformanceDataType.Enterprise,
                            0,
                            taskContext.EnterprisePerformanceWeighting,
                            productsNotToBeMerged);
                    }

                    if (taskContext.ClusterPerformanceWeighting > 0)
                    {
                        if (cluster != null)
                        {
                            // fetch the cluster performance data for this planogram and weight accordingly
                            GetPlanogramPerformanceData(
                                taskContext,
                                performanceData,
                                taskContext.Workpackage,
                                taskContext.PerformanceSelection,
                                WorkpackagePerformanceDataType.Cluster,
                                cluster.Id,
                                taskContext.ClusterPerformanceWeighting,
                                productsNotToBeMerged);
                        }
                        else
                        {
                            // unable to weight cluster performance data, cluster lookup reference failed.
                            String clusterName = taskContext.Planogram.ClusterName != null ? taskContext.Planogram.ClusterName : Message.UpdateProductPerformance_InvalidClusterReference_NoClusterAssigned;
                            taskContext.LogWarning(EventLogEvent.PlanogramPerformanceDataInvalidClusterReference, clusterName);
                        }
                    }

                    if (taskContext.PlanogramAssignmentPerformanceWeighting > 0)
                    {
                        LocationPlanAssignmentList locationPlanAssignmentList = LocationPlanAssignmentList.FetchByPlanogramId((Int32)taskContext.WorkpackagePlanogram.SourcePlanogramId);

                        if (locationPlanAssignmentList.Any())
                        {
                            // fetch the planogram assignment performance data for this planogram and weight accordingly
                            GetPlanogramPerformanceData(
                                taskContext,
                                performanceData,
                                taskContext.Workpackage,
                                taskContext.PerformanceSelection,
                                WorkpackagePerformanceDataType.PlanogramAssignment,
                                (Int32)taskContext.WorkpackagePlanogram.SourcePlanogramId,
                                taskContext.PlanogramAssignmentPerformanceWeighting,
                                productsNotToBeMerged);
                        }
                        else
                        {
                            taskContext.LogWarning(EventLogEvent.PlanogramPerformanceDataNoPlanogramAssignmentReference);
                        }
                    }
                }

                // If we weren't able to retrieve any performance data at this stage, log an error and return.
                if (!performanceData.Any())
                {
                    taskContext.LogError(EventLogEvent.PlanogramPerformanceDataCouldNotBeFetched);
                    return false;
                }

                //ensure we use the correct number of tumeline groups when calculating days of performance
                Int32 groupCount = 0;
                switch (taskContext.PerformanceSelection.SelectionType)
                {
                    case PerformanceSelectionType.Dynamic:
                        groupCount = taskContext.PerformanceSelection.DynamicValue;
                        break;
                    case PerformanceSelectionType.Fixed:
                        groupCount = taskContext.PerformanceSelection.TimelineGroups.Count;
                        break;
                    default:
                        groupCount = taskContext.PerformanceSelection.TimelineGroups.Count;
                        break;
                }

                taskContext.Planogram.Inventory.DaysOfPerformance = ((Int16)taskContext.SourceDaysPerPeriodType * groupCount);

                // now save the performance data to the planogram
                SavePlanogramPerformanceData(taskContext, performanceData);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Deletes all existing performance data
        /// for the provided workpackage
        /// </summary>
        private static Boolean DeleteWorkpackagePerformanceData(Workpackage workpackage)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // delete the workpackage performance data
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackagePerformanceDal dal = dalContext.GetDal<IWorkpackagePerformanceDal>())
                    {
                        dal.DeleteByWorkpackageId(workpackage.Id);
                    }
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Returns all performance selections that are
        /// being used within a workpackage and the planograms
        /// that are using those performance selections
        /// </summary>
        private static Dictionary<String, List<WorkpackagePlanogram>> GetPerformanceSelectionNames(Workpackage workpackage, WorkflowTask workflowTask)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of performance selection names
                // that are within the workpackage
                Dictionary<String, List<WorkpackagePlanogram>> performanceSelections = new Dictionary<String, List<WorkpackagePlanogram>>();

                // locate the performance selection name
                // parameter within the workflow task
                WorkflowTaskParameter taskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PerformanceSelectionName);

                // now iterate through all the planograms
                // within the workpackage and build a list
                // of all the unique performance selections
                WorkpackagePlanogramList workpackagePlanograms = workpackage.Planograms;
                foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
                {
                    foreach (WorkpackagePlanogramParameter planogramParameter in workpackagePlanogram.Parameters)
                    {
                        if (planogramParameter.WorkflowTaskParameterId == taskParameter.Id)
                        {
                            // add the performance selection name to the collection
                            String performanceSelectionName = Convert.ToString(planogramParameter.Value);
                            if (!performanceSelections.ContainsKey(performanceSelectionName))
                            {
                                performanceSelections.Add(performanceSelectionName, new List<WorkpackagePlanogram>());
                            }

                            // add the planogram to the performance selection name
                            performanceSelections[performanceSelectionName].Add(workpackagePlanogram);
                        }
                    }
                }

                // return the performance selection names
                return performanceSelections;
            }
        }

        private static Dictionary<String, IEnumerable<Int64>> GetPerformanceSelectionTimelineExclusions(Workpackage workpackage, WorkflowTask workflowTask)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of performance selection names
                // that are within the workpackage
                Dictionary<String, IEnumerable<Int64>> performanceSelectionTimelines = new Dictionary<String, IEnumerable<Int64>>();

                // locate the performance selection name
                // parameter within the workflow task
                WorkflowTaskParameter taskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PerformanceSelectionName);
                WorkflowTaskParameter timelineCodeTaskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PerformanceTimelinesSelection);


                // now iterate through all the planograms
                // within the workpackage and build a list
                // of all the unique performance selections
                WorkpackagePlanogramList workpackagePlanograms = workpackage.Planograms;
                foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
                {
                    foreach (WorkpackagePlanogramParameter planogramParameter in workpackagePlanogram.Parameters)
                    {

                        if (planogramParameter.WorkflowTaskParameterId == taskParameter.Id)
                        {
                            // add the performance selection name to the collection
                            String performanceSelectionName = Convert.ToString(planogramParameter.Value);
                            if (!performanceSelectionTimelines.ContainsKey(performanceSelectionName))
                            {
                                //Get timelines too
                                WorkpackagePlanogramParameter timelineParamter = workpackagePlanogram.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection);
                                if (timelineParamter != null)
                                {
                                    performanceSelectionTimelines.Add(performanceSelectionName, timelineParamter.Values.Select(p => Convert.ToInt64(p.Value1)));
                                }
                            }
                        }
                    }
                }

                // return the performance selection names
                return performanceSelectionTimelines;
            }
        }

        private static IEnumerable<Int64> GetPerformanceSelectionAvailableTimelines(GFSModel.Entity gfsEntity, GFSModel.PerformanceSource performanceSource)
        {
            GFSModel.TimelineGroupList timelineGroupList;

            //If found
            if (performanceSource != null)
            {
                //Fetch the timeline group
                DateTime startDate = new DateTime();
                DateTime endDate = DateTime.UtcNow;
                timelineGroupList = GFSModel.TimelineGroupList.FetchByDateRange(gfsEntity, performanceSource, startDate, endDate);
                //Return available timelines
                return timelineGroupList.Select(p => p.Code);
            }

            //Return empty list
            return new List<Int64>();
        }

        /// <summary>
        /// Retrieves the specified performance selection
        /// based on the provided name
        /// </summary>
        private static PerformanceSelection GetPerformanceSelection(Int32 entityId, String name)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    return PerformanceSelection.FetchByEntityIdName(entityId, name);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        return null;
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Returns the timlines that are part
        /// of the specified performance selection
        /// </summary>
        private static List<Int64> GetTimelineCodes(GFSModel.Entity entity, 
            PerformanceSelection performanceSelection, 
            GFSModel.PerformanceSource performanceSource,
            IEnumerable<Int64> availableTimelineCodes,
            IEnumerable<Int64> timelineCodeExclusions)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //Get timeline codes from performance selection
                //Can't rely on the timeline parameter as these may be out of date, so we need to query
                //GFS again.
                IEnumerable<Int64> timelineCodes = performanceSelection.GetTimelineCodes(entity, performanceSource);

                if (timelineCodeExclusions != null)
                {
                    switch (performanceSelection.SelectionType)
                    {
                        case PerformanceSelectionType.Dynamic:

                            //Find any potential timeline codes, OUTSIDE of the dynamic time range, but also included from parameter.
                            IEnumerable<Int64> includedTimelinesOutsideDynamicRange = availableTimelineCodes.Except(timelineCodeExclusions).Except(timelineCodes);

                            //If we are running using a simple performance source, this does not contain timeline information
                            //and therefore cannot have exclusions
                            if (performanceSource.Type == GFSModel.PerformanceSourceType.Simple)
                            {
                                return timelineCodes.Union(includedTimelinesOutsideDynamicRange).ToList();
                            }
                            else
                            {
                                //Return timeline codes, EXCEPT timeline code exclusions from parameter.
                                return timelineCodes.Except(timelineCodeExclusions).Union(includedTimelinesOutsideDynamicRange).ToList();
                            }

                        case PerformanceSelectionType.Fixed:

                            //Get available timeline codes plus any from the performance selection that has been setup
                            IEnumerable<Int64> totalAvailableTimelines = availableTimelineCodes.Union(timelineCodes).Distinct();

                            //If we are running using a simple performance source, this does not contain timeline information
                            //and therefore cannot have exclusions
                            if (performanceSource.Type == GFSModel.PerformanceSourceType.Simple)
                            {
                                return totalAvailableTimelines.ToList();
                            }
                            else
                            {
                                //Return timeline codes, EXCEPT timeline code exclusions from parameter
                                return totalAvailableTimelines.Except(timelineCodeExclusions).ToList();
                            }

                        default:
                            throw new InvalidOperationException();
                    }
                }
                else
                {
                    //Fall back on the performance selection setup
                    return timelineCodes.ToList();
                }
            }
        }

        /// <summary>
        /// Returns the aggregation types from the given performance selection.
        /// </summary>
        private static List<PerformanceSourceMetricAggregationType> GetAggregationTypes(
            PerformanceSelection performanceSelection, GFSModel.PerformanceSource performanceSource)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                return performanceSelection.GetAggregationTypes(performanceSource).ToList();
            }
        }

        /// <summary>
        /// Returns all product codes that are
        /// covered by the list of planograms
        /// </summary>
        private static void GetLookupCodes(List<WorkpackagePlanogram> workpackagePlanograms, PerformanceDataSelectionHelper performanceDataSelectionHelper, WorkflowTask workflowTask,
                                ref Single enterprisePerformanceWeighting, ref Single clusterPerformanceWeighting, ref Single storePerformanceWeighting, ref Single planogramAssignmentPerformanceWeighting)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                WorkflowTaskParameter enterpriseTaskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.EnterprisePerformanceWeighting);
                WorkflowTaskParameter clusterTaskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.ClusterPerformanceWeighting);
                WorkflowTaskParameter storeTaskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.StorePerformanceWeighting);
                WorkflowTaskParameter planogramAssignmentTaskParameter = workflowTask.Parameters.FirstOrDefault(i => i.ParameterDetails.Id == (Int32)Parameter.PlanogramAssignmentPerformanceWeighting);

                // enumerate through each planogram in turn
                foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
                {
                    Single workpackageClusterPerformanceWeighting = 0;
                    Single workpackageStorePerformanceWeighting = 0;
                    Single workpackagePlanogramAssignmentPerformanceWeighting = 0;

                    foreach (WorkpackagePlanogramParameter planogramParameter in workpackagePlanogram.Parameters)
                    {
                        if (planogramParameter.WorkflowTaskParameterId == enterpriseTaskParameter.Id)
                        {
                            if (planogramParameter.Value != null)
                            {
                                Single workpackagePerformanceWeighting = Convert.ToSingle(planogramParameter.Value);
                                if (workpackagePerformanceWeighting > enterprisePerformanceWeighting)
                                {
                                    enterprisePerformanceWeighting = workpackagePerformanceWeighting;
                                }
                            }
                        }
                        if (planogramParameter.WorkflowTaskParameterId == clusterTaskParameter.Id)
                        {
                            if (planogramParameter.Value != null)
                            {
                                workpackageClusterPerformanceWeighting = Convert.ToSingle(planogramParameter.Value);
                                if (workpackageClusterPerformanceWeighting > clusterPerformanceWeighting)
                                {
                                    clusterPerformanceWeighting = workpackageClusterPerformanceWeighting;
                                }
                            }
                        }
                        if (planogramParameter.WorkflowTaskParameterId == storeTaskParameter.Id)
                        {
                            if (planogramParameter.Value != null)
                            {
                                workpackageStorePerformanceWeighting = Convert.ToSingle(planogramParameter.Value);
                                if (workpackageStorePerformanceWeighting > storePerformanceWeighting)
                                {
                                    storePerformanceWeighting = workpackageStorePerformanceWeighting;
                                }
                            }
                        }
                        if (planogramParameter.WorkflowTaskParameterId == planogramAssignmentTaskParameter.Id)
                        {
                            if (planogramParameter.Value != null)
                            {
                                workpackagePlanogramAssignmentPerformanceWeighting = Convert.ToSingle(planogramParameter.Value);
                                if (workpackagePlanogramAssignmentPerformanceWeighting > planogramAssignmentPerformanceWeighting)
                                {
                                    planogramAssignmentPerformanceWeighting = workpackagePlanogramAssignmentPerformanceWeighting;
                                }
                            }
                        }
                    }

                    // fetch the package read-only
                    using (Package package = Package.FetchById(workpackagePlanogram.DestinationPlanogramId))
                    {
                        // enumerate the products within the package planogram
                        foreach (PlanogramProduct product in package.Planograms[0].Products)
                        {
                            if (!performanceDataSelectionHelper.ProductCodes.Contains(product.Gtin))
                                performanceDataSelectionHelper.ProductCodes.Add(product.Gtin);
                        }

                        //V831550 : Check for any product buddies
                        if (package.Planograms[0].Assortment != null &&
                            package.Planograms[0].Assortment.ProductBuddies.Any())
                        {
                            foreach (PlanogramAssortmentProductBuddy productBuddy in package.Planograms[0].Assortment.ProductBuddies)
                            {
                                if (!String.IsNullOrEmpty(productBuddy.S1ProductGtin) && !performanceDataSelectionHelper.ProductCodes.Contains(productBuddy.S1ProductGtin))
                                    performanceDataSelectionHelper.ProductCodes.Add(productBuddy.S1ProductGtin);
                                if (!String.IsNullOrEmpty(productBuddy.S2ProductGtin) && !performanceDataSelectionHelper.ProductCodes.Contains(productBuddy.S2ProductGtin))
                                    performanceDataSelectionHelper.ProductCodes.Add(productBuddy.S2ProductGtin);
                                if (!String.IsNullOrEmpty(productBuddy.S3ProductGtin) && !performanceDataSelectionHelper.ProductCodes.Contains(productBuddy.S3ProductGtin))
                                    performanceDataSelectionHelper.ProductCodes.Add(productBuddy.S3ProductGtin);
                                if (!String.IsNullOrEmpty(productBuddy.S4ProductGtin) && !performanceDataSelectionHelper.ProductCodes.Contains(productBuddy.S4ProductGtin))
                                    performanceDataSelectionHelper.ProductCodes.Add(productBuddy.S4ProductGtin);
                                if (!String.IsNullOrEmpty(productBuddy.S5ProductGtin) && !performanceDataSelectionHelper.ProductCodes.Contains(productBuddy.S5ProductGtin))
                                    performanceDataSelectionHelper.ProductCodes.Add(productBuddy.S5ProductGtin);
                            }
                        }

                        if (workpackageStorePerformanceWeighting > 0)
                        {
                            // get location code lookups
                            if (package.Planograms[0].LocationCode != null && package.Planograms[0].LocationCode.Length > 0)
                            {
                                if (!performanceDataSelectionHelper.LocationCodes.Contains(package.Planograms[0].LocationCode))
                                    performanceDataSelectionHelper.LocationCodes.Add(package.Planograms[0].LocationCode);
                            }

                            //V831550 : Check for any location buddies
                            if (package.Planograms[0].Assortment != null &&
                                package.Planograms[0].Assortment.LocationBuddies.Any())
                            {
                                foreach (PlanogramAssortmentLocationBuddy locationBuddy in package.Planograms[0].Assortment.LocationBuddies)
                                {
                                    if (!String.IsNullOrEmpty(locationBuddy.S1LocationCode) && !performanceDataSelectionHelper.LocationCodes.Contains(locationBuddy.S1LocationCode))
                                        performanceDataSelectionHelper.LocationCodes.Add(locationBuddy.S1LocationCode);
                                    if (!String.IsNullOrEmpty(locationBuddy.S2LocationCode) && !performanceDataSelectionHelper.LocationCodes.Contains(locationBuddy.S2LocationCode))
                                        performanceDataSelectionHelper.LocationCodes.Add(locationBuddy.S2LocationCode);
                                    if (!String.IsNullOrEmpty(locationBuddy.S3LocationCode) && !performanceDataSelectionHelper.LocationCodes.Contains(locationBuddy.S3LocationCode))
                                        performanceDataSelectionHelper.LocationCodes.Add(locationBuddy.S3LocationCode);
                                    if (!String.IsNullOrEmpty(locationBuddy.S4LocationCode) && !performanceDataSelectionHelper.LocationCodes.Contains(locationBuddy.S4LocationCode))
                                        performanceDataSelectionHelper.LocationCodes.Add(locationBuddy.S4LocationCode);
                                    if (!String.IsNullOrEmpty(locationBuddy.S5LocationCode) && !performanceDataSelectionHelper.LocationCodes.Contains(locationBuddy.S5LocationCode))
                                        performanceDataSelectionHelper.LocationCodes.Add(locationBuddy.S5LocationCode);
                                }
                            }
                        }

                        if (workpackageClusterPerformanceWeighting > 0)
                        {
                            // get location code lookups
                            if (package.Planograms[0].ClusterSchemeName != null && package.Planograms[0].ClusterSchemeName.Length > 0 &&
                                package.Planograms[0].ClusterName != null && package.Planograms[0].ClusterName.Length > 0)
                            {
                                List<String> clusterCodes = null;

                                if (!performanceDataSelectionHelper.ClusterSchemes.TryGetValue(package.Planograms[0].ClusterSchemeName, out clusterCodes))
                                {
                                    clusterCodes = new List<String>();
                                    performanceDataSelectionHelper.ClusterSchemes.Add(package.Planograms[0].ClusterSchemeName, clusterCodes);
                                }

                                if (!clusterCodes.Contains(package.Planograms[0].ClusterName))
                                {
                                    clusterCodes.Add(package.Planograms[0].ClusterName);
                                }
                            }
                        }

                        if (workpackagePlanogramAssignmentPerformanceWeighting > 0)
                        {
                            if (!performanceDataSelectionHelper.PlanogramAssignments.Contains((Int32)workpackagePlanogram.SourcePlanogramId))
                                performanceDataSelectionHelper.PlanogramAssignments.Add((Int32)workpackagePlanogram.SourcePlanogramId);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns all the performance data from gfs
        /// for the specified performance selection,
        /// timelines, products, and codes
        /// </summary>
        /// <remarks>
        /// Note that this method goes direct to the gfs web services
        /// rather than through the GFS model. As performance data can
        /// be huge, we fetch the data directly rather than decanting 
        /// this information into a new model, which effectively doubles
        /// the amount of required memory
        /// </remarks>
        private static void GetGfsPerformanceData(
            GFSModel.Entity entity,
            Int32 entityId,
            Workpackage workpackage,
            PerformanceSelection performanceSelection,
            List<WorkpackagePerformanceDataDto> performanceData,
            PerformanceDataSelectionHelper performanceDataSelectionHelper,
            List<String> locationCodes)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the gfs performance source
                GFSModel.PerformanceSource gfsPerformanceSource = performanceSelection.GetPerformanceSource(entity);

                //get the aggregation types
                List<PerformanceSourceMetricAggregationType> aggregationTypes = GetAggregationTypes(performanceSelection, gfsPerformanceSource);

                // now fetch the performance data
                using (FoundationServiceClient proxy = new FoundationServiceClient(entity.EndpointRoot))
                {
                    performanceDataSelectionHelper.LocationBatchSize = locationCodes.Count;

                    GetGfsPerformanceData(
                        proxy,
                        entity.Name,
                        entityId,
                        workpackage.Id,
                        performanceSelection.Id,
                        gfsPerformanceSource.Name,
                        performanceData,
                        aggregationTypes,
                        locationCodes,
                        performanceDataSelectionHelper);
                }
            }
        }

        /// <summary>
        /// This method recusively requests performance data from
        /// gfs until the request batch size is below the limit that
        /// is actually enforced by gfs
        /// </summary>
        private static void GetGfsPerformanceData(
            FoundationServiceClient proxy,
            String entityName,
            Int32 entityId,
            Int32 workpackageId,
            Int32 performanceSelectionId,
            String performanceSourceName,
            List<WorkpackagePerformanceDataDto> performanceData,
            List<PerformanceSourceMetricAggregationType> aggregationTypes,
            List<String> remainingLocationCodes,
            PerformanceDataSelectionHelper performanceDataSelectionHelper)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // set the retry counts
                Int32 serverBusyRetryCount = 0;

                // recurse until all locations have been requested
                while (remainingLocationCodes.Count > 0)
                {
                    // build a list of locations based
                    // on the current batch size
                    List<String> locationCodes = new List<String>();
                    Int32 batchSize = Math.Min(performanceDataSelectionHelper.LocationBatchSize, remainingLocationCodes.Count);
                    for (Int32 i = 0; i < batchSize; i++)
                    {
                        locationCodes.Add(remainingLocationCodes[i]);
                    }

                    LocationInfoList locationInfoList = null;
                    Dictionary<String, Int16> locGroupCodeToIdLookup = new Dictionary<String, Int16>();
                    if (performanceDataSelectionHelper.WorkpackagePerformanceDataType == WorkpackagePerformanceDataType.Store)
                    {
                        locationInfoList = LocationInfoList.FetchByEntityId(entityId);
                        locGroupCodeToIdLookup = locationInfoList.ToDictionary(g => g.Code, g => g.Id, StringComparer.OrdinalIgnoreCase);
                    }

                    // we now have all the details we need to
                    // perform a request for performance data
                    // from GFS
                    try
                    {
                        Boolean aggregateByLocation = performanceDataSelectionHelper.WorkpackagePerformanceDataType == WorkpackagePerformanceDataType.Store ? true : false;

                        // make the call to GFS for the performance data
                        GetPerformanceDataByProductGTINRequest request = new GetPerformanceDataByProductGTINRequest(
                                entityName,
                                performanceSourceName,
                                true,
                                aggregateByLocation,
                                false,
                                false,
                                performanceDataSelectionHelper.ProductCodes,
                                locationCodes,
                                performanceDataSelectionHelper.TimelineCodes,
                                aggregationTypes);

                        var response = proxy.PerformanceServiceClient.GetPerformanceDataByProductGTIN(request);

                        // if we recieved some performance data
                        if (response.PerformanceData.Count > 0)
                        {
                            // add the response data to the list of performance data
                            foreach (var item in response.PerformanceData)
                            {
                                performanceData.Add(new WorkpackagePerformanceDataDto()
                                {
                                    WorkpackageId = workpackageId,
                                    PerformanceSelectionId = performanceSelectionId,
                                    ProductGTIN = item.ProductGTIN,
                                    DataId = performanceDataSelectionHelper.WorkpackagePerformanceDataType == WorkpackagePerformanceDataType.Store ? locGroupCodeToIdLookup[item.LocationCode] : performanceDataSelectionHelper.DataId,
                                    DataType = (Byte)performanceDataSelectionHelper.WorkpackagePerformanceDataType,
                                    P1 = item.P1,
                                    P2 = item.P2,
                                    P3 = item.P3,
                                    P4 = item.P4,
                                    P5 = item.P5,
                                    P6 = item.P6,
                                    P7 = item.P7,
                                    P8 = item.P8,
                                    P9 = item.P9,
                                    P10 = item.P10,
                                    P11 = item.P11,
                                    P12 = item.P12,
                                    P13 = item.P13,
                                    P14 = item.P14,
                                    P15 = item.P15,
                                    P16 = item.P16,
                                    P17 = item.P17,
                                    P18 = item.P18,
                                    P19 = item.P19,
                                    P20 = item.P20
                                });
                            }
                        }

                        // since we have successfully retrieved a batch
                        // of performance data, we remove the locations
                        // from the remaining location list
                        foreach (String locationCode in locationCodes)
                        {
                            remainingLocationCodes.Remove(locationCode);
                        }
                    }
                    catch (FaultException<PerformanceServiceRowCountFault>)
                    {
                        // the number of request performance items is too
                        // great, therefore we halve the location dimension
                        performanceDataSelectionHelper.LocationBatchSize = Math.Max(performanceDataSelectionHelper.LocationBatchSize / 2, 1);

                        // and try again
                        GetGfsPerformanceData(
                            proxy,
                            entityName,
                            entityId,
                            workpackageId,
                            performanceSelectionId,
                            performanceSourceName,
                            performanceData,
                            aggregationTypes,
                            remainingLocationCodes,
                            performanceDataSelectionHelper);
                    }
                    catch (FaultException<PerformanceServiceServerTooBusyFault>)
                    {
                        // the server is too busy at the moment
                        // so try again unless we have hit our
                        // max retry count
                        if (serverBusyRetryCount >= _serverBusyMaxRetryCount) throw;

                        // increase the retry count
                        serverBusyRetryCount++;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Saves the performance data to the data store
        /// </summary>
        private static void SaveGfsPerformanceData(
            Workpackage workpackage,
            List<WorkpackagePerformanceDataDto> performanceData)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // ensure we have something to do
                if (performanceData.Count == 0) return;

                // upsert the performance data
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // insert the performance dto
                    using (IWorkpackagePerformanceDal dal = dalContext.GetDal<IWorkpackagePerformanceDal>())
                    {
                        dal.Insert(new WorkpackagePerformanceDto()
                        {
                            WorkpackageId = workpackage.Id
                        });
                    }

                    // now upload the performance data
                    using (IWorkpackagePerformanceDataDal dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                    {
                        dal.BulkInsert(workpackage.Id, performanceData);
                    }
                }
            }
        }
        
        /// <summary>
        /// Returns all performance data for the specified data selection
        /// </summary>
        /// <param name="performanceData">The current performance data list</param>
        /// <param name="workpackage">The workpackage</param>
        /// <param name="performanceSelection">The performance selection</param>
        /// <param name="workpackagePerformanceDataType">The type of performance data to fetch</param>
        /// <param name="dataId">The relating performance data id</param>
        /// <param name="weighting">The weighting value to apply</param>
        /// <param name="productsNotToBeMerged">optional selection of gtins that are not to be merged</param>
        private static void GetPlanogramPerformanceData(TaskContext taskContext, List<WorkpackagePerformanceDataDto> performanceData, Workpackage workpackage,
            PerformanceSelection performanceSelection,
            WorkpackagePerformanceDataType workpackagePerformanceDataType,
            Int32 dataId, Single weighting,
            IEnumerable<String> productsNotToBeMerged = null)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Dictionary<String, WorkpackagePerformanceDataDto> performanceDataLookup = performanceData.ToDictionary(p => p.ProductGTIN);

                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackagePerformanceDataDal dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                    {
                        // Get performance data
                        List<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(workpackage.Id,
                                                                                    performanceSelection.Id,
                                                                                    (Byte)workpackagePerformanceDataType,
                                                                                    dataId).ToList();

                        //Apply any possible location buddies at location level only
                        //Needs to be applied here before checking for existing data, incase location selected has 0 performance data.
                        if (workpackagePerformanceDataType == WorkpackagePerformanceDataType.Store)
                        {
                            ApplyLocationBuddies(taskContext, dal, performanceSelection, workpackagePerformanceDataType, workpackage, workpackagePerformanceDataDtos);
                        }

                        if (workpackagePerformanceDataDtos.Any())
                        {
                            //Create workpackage performance data dto product lookup
                            Dictionary<String, WorkpackagePerformanceDataDto> productWorkpackagePerformanceDataLookup = workpackagePerformanceDataDtos.ToDictionary(p => p.ProductGTIN);

                            //Check for any products that are buddied, included in the planogram, but have no existing performance data
                            IEnumerable<String> planProductsWithBuddies = taskContext.PlanogramProductsByGtin.Keys.Intersect(taskContext.ProductBuddiesByGtin.Keys);
                            IEnumerable<String> buddiedPlanProductsWithNoPeformanceData = planProductsWithBuddies.Except(productWorkpackagePerformanceDataLookup.Keys);
                            foreach (String productGtin in buddiedPlanProductsWithNoPeformanceData)
                            {
                                //Add dummy performance data dto for creation
                                WorkpackagePerformanceDataDto newBuddiedPlanProductsWithNoPeformanceData = new WorkpackagePerformanceDataDto(dataId,
                                    (Byte)workpackagePerformanceDataType,
                                    performanceSelection.Id,
                                    productGtin,
                                    workpackage.Id);
                                //Add blank dto to collection so buddies can be applied
                                workpackagePerformanceDataDtos.Add(newBuddiedPlanProductsWithNoPeformanceData);
                            }

                            //Product requring buddies
                            List<String> productRequiringBuddying = new List<String>();

                            foreach (WorkpackagePerformanceDataDto workpackagePerformanceDataDto in workpackagePerformanceDataDtos)
                            {
                                if (productsNotToBeMerged == null || 
                                    (productsNotToBeMerged != null && !productsNotToBeMerged.Contains(workpackagePerformanceDataDto.ProductGTIN)))
                                {
                                //does a product buddy need to be applied
                                if (taskContext.ProductBuddiesByGtin.ContainsKey(workpackagePerformanceDataDto.ProductGTIN))
                                {
                                    productRequiringBuddying.Add(workpackagePerformanceDataDto.ProductGTIN);
                                }

                                WorkpackagePerformanceDataDto currentWorkpackagePerformanceDataDto = null;
                                if (!performanceDataLookup.TryGetValue(workpackagePerformanceDataDto.ProductGTIN, out currentWorkpackagePerformanceDataDto))
                                {
                                    workpackagePerformanceDataDto.ApplyWeighting(weighting);
                                    performanceData.Add(workpackagePerformanceDataDto);
                                    performanceDataLookup.Add(workpackagePerformanceDataDto.ProductGTIN, workpackagePerformanceDataDto);
                                }
                                else
                                {
                                    currentWorkpackagePerformanceDataDto.ApplyPerformance(workpackagePerformanceDataDto, weighting);
                                }

                                    if (productsNotToBeMerged != null)
                                    {
                                        taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataProductMerged, workpackagePerformanceDataDto.ProductGTIN, WorkpackagePerformanceDataTypeHelper.FriendlyNames[workpackagePerformanceDataType]);
                                    }
                                }
                            }

                            //V8-32260 : if any buddies require applying AFTER performance weighting for data type applied
                            if (productRequiringBuddying.Any())
                            {
                                foreach (String productGTIN in productRequiringBuddying)
                                {
                                    WorkpackagePerformanceDataDto currentWorkpackagePerformanceDataDto = null;
                                    if (performanceDataLookup.TryGetValue(productGTIN, out currentWorkpackagePerformanceDataDto))
                                    {
                                        //Check for product buddies across all weighting types
                                        ApplyProductBuddies(taskContext, currentWorkpackagePerformanceDataDto, performanceDataLookup);
                                    }
                                }
                            }
                        }
                        else
                        {
                            taskContext.LogWarning(EventLogEvent.PlanogramNoPerformanceSelectionData, WorkpackagePerformanceDataTypeHelper.FriendlyNames[workpackagePerformanceDataType]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Apply any location buddys setup to the workpackage performance data
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="dal"></param>
        /// <param name="performanceSelection"></param>
        /// <param name="workpackagePerformanceDataType"></param>
        /// <param name="workpackagePerformanceDataDtos"></param>
        public static void ApplyLocationBuddies(TaskContext taskContext,
            IWorkpackagePerformanceDataDal dal,
            PerformanceSelection performanceSelection,
            WorkpackagePerformanceDataType workpackagePerformanceDataType,
            Workpackage workpackage,
            List<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtos)
        {
            //Find buddy
            PlanogramAssortmentLocationBuddy locationBuddy = null;
            if(taskContext.LocationBuddiesByCode.TryGetValue(taskContext.Planogram.LocationCode, out locationBuddy))
            {
                //Set location buddy logging flag
                if (!taskContext.IsAppliedLocationBuddyLoggingRequired)
                {
                    taskContext.IsAppliedLocationBuddyLoggingRequired = true;
                }

                Int32 sourceLocationsFound = locationBuddy.GetNumberOfSourceLocations();

                //Create copy of performance data that we can buddy values to
                Dictionary<String, WorkpackagePerformanceDataDto> existingWorkpackagePerformanceDataDtoLookup
                    = workpackagePerformanceDataDtos.ToDictionary(p => p.ProductGTIN);
                Dictionary<String, WorkpackagePerformanceDataDto> newBuddiedWorkpackagePerformanceDataDtoLookup
                    = new Dictionary<String, WorkpackagePerformanceDataDto>();
                for (int i = 0; i < sourceLocationsFound; i++)
                {
                    //Store buddy values
                    String sourceLocationCode;
                    Single? sourcePercentage;
                    Boolean buddyApplied = GetLocationBuddySourceValues(i, locationBuddy, out sourceLocationCode, out sourcePercentage);
                    //If no buddy for this index, break
                    if (!buddyApplied) break;
                    else
                    {
                        Int16 locationId;
                        if (taskContext.LocationIdsByCode.TryGetValue(sourceLocationCode, out locationId))
                        {
                            // Get performance data for buddy location
                            IEnumerable<WorkpackagePerformanceDataDto> sourceWorkpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(
                                workpackage.Id,
                                                                                        performanceSelection.Id,
                                                                                        (Byte)workpackagePerformanceDataType,
                                                                                        locationId);
                            //Enumerate through applying buddy values to data
                            foreach (WorkpackagePerformanceDataDto sourcePerformanceDataDto in sourceWorkpackagePerformanceDataDtos)
                            { 
                                //Check for existing performance data for this product
                                WorkpackagePerformanceDataDto existingPerformanceDataDto = null;
                                if (existingWorkpackagePerformanceDataDtoLookup.TryGetValue(sourcePerformanceDataDto.ProductGTIN, out existingPerformanceDataDto))
                                {
                                    //Remove existing performance data
                                    workpackagePerformanceDataDtos.Remove(existingPerformanceDataDto);
                                }

                                //Has new performance data already been created
                                WorkpackagePerformanceDataDto newPerformanceDataDto = null;
                                if (newBuddiedWorkpackagePerformanceDataDtoLookup.TryGetValue(sourcePerformanceDataDto.ProductGTIN, out newPerformanceDataDto))
                                {
                                    //Apply values from buddy to already created performance data
                                    ApplySourceLocationBuddyValues(newPerformanceDataDto, sourcePerformanceDataDto, locationBuddy.TreatmentType, sourcePercentage, sourceLocationsFound);
                                }
                                else
                                {
                                    //Copy values from buddy performance data
                                    WorkpackagePerformanceDataDto newBuddiedPerformanceData = new WorkpackagePerformanceDataDto(sourcePerformanceDataDto.DataId,
                                        sourcePerformanceDataDto.DataType,
                                        sourcePerformanceDataDto.PerformanceSelectionId,
                                        sourcePerformanceDataDto.ProductGTIN,
                                        sourcePerformanceDataDto.WorkpackageId);

                                    //Apply values from buddy
                                    ApplySourceLocationBuddyValues(newBuddiedPerformanceData, sourcePerformanceDataDto, locationBuddy.TreatmentType, sourcePercentage, sourceLocationsFound);

                                    //Add to the lookup
                                    newBuddiedWorkpackagePerformanceDataDtoLookup.Add(newBuddiedPerformanceData.ProductGTIN, newBuddiedPerformanceData);
                                }                                                    
                            }
                        }
                    }
                }
                //Add all new buddied performance data back into the data collection
                workpackagePerformanceDataDtos.AddRange(newBuddiedWorkpackagePerformanceDataDtoLookup.Values);
            }
        }

        /// <summary>
        /// Apply any product buddys setup to the workpackage performance data
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="dal"></param>
        /// <param name="performanceSelection"></param>
        /// <param name="workpackagePerformanceDataType"></param>
        /// <param name="workpackagePerformanceDataDtos"></param>
        public static void ApplyProductBuddies(TaskContext taskContext,
            WorkpackagePerformanceDataDto workpackagePerformanceDataDto,
            Dictionary<String, WorkpackagePerformanceDataDto> existingProductWorkpackagePerformanceDataLookup)
        {
            //Try find buddy
            PlanogramAssortmentProductBuddy productBuddy = null;
            if (taskContext.ProductBuddiesByGtin.TryGetValue(workpackagePerformanceDataDto.ProductGTIN, out productBuddy))
            {
                //Set product buddy logging flag
                if (!taskContext.IsAppliedProductBuddyLoggingRequired)
                {
                    taskContext.IsAppliedProductBuddyLoggingRequired = true;
                }

                if ((productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                    productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg) &&
                    taskContext.MetricProfile == null)
                {
                    //Log product buddy cannot be applied
                    taskContext.LogInformation(EventLogEvent.PlanogramPerformanceDataNoMetricProfile, new Object[] { PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[productBuddy.TreatmentType], workpackagePerformanceDataDto.ProductGTIN });
                }
                else
                {
                    //Take a copy of the current performace just in case we want
                    // to buddy this product to itself.
                    WorkpackagePerformanceDataDto origPerformanceDataDto = workpackagePerformanceDataDto.Clone();


                    //Clear any existing values, as this is being buddied.
                    workpackagePerformanceDataDto.ResetPerformanceValues();

                    //If manual source product selection
                    if (productBuddy.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                    {
                        Int32 sourceProductsFound = productBuddy.GetNumberOfSourceProducts();

                        if (sourceProductsFound > 0)
                        {
                            //Enumerate through source products in buddy
                            for (int i = 0; i < sourceProductsFound; i++)
                            {
                                //Store buddy values
                                String sourceProductGtin;
                                Single? sourcePercentage;
                                Boolean buddyApplied = GetProductBuddySourceValues(i, productBuddy, out sourceProductGtin, out sourcePercentage);
                                //If no buddy for this index, break
                                if (!buddyApplied) break;
                                else
                                {
                                    //Lookup source product performance 
                                    WorkpackagePerformanceDataDto sourcePerformanceDataDto = null;
                                    if (String.Compare(sourceProductGtin, workpackagePerformanceDataDto.ProductGTIN, /*ignoreCase*/true) == 0)
                                    {
                                        //we are buddying the product to itself so use the cloned dto.
                                        sourcePerformanceDataDto = origPerformanceDataDto;
                                    }

                                    if (sourcePerformanceDataDto != null 
                                        || existingProductWorkpackagePerformanceDataLookup.TryGetValue(sourceProductGtin, out sourcePerformanceDataDto))
                                    {
                                        //Apply values from buddy to already created performance data
                                        ApplySourceProductBuddyValues(workpackagePerformanceDataDto, sourcePerformanceDataDto, productBuddy.TreatmentType, sourcePercentage, sourceProductsFound);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //Build up buddied products performance
                        List<WorkpackagePerformanceDataDto> buddiedPerformanceData = new List<WorkpackagePerformanceDataDto>();

                        // We need to buddy this product to all products that share a particular attribute value 
                        // with it.
                        PlanogramProduct targetProduct = null;
                        if (taskContext.PlanogramProductsByGtin.TryGetValue(productBuddy.ProductGtin, out targetProduct))
                        {
                            Object targetProductValue = PlanogramAssortmentProductBuddyProductAttributeTypeHelper.GetValue(
                                targetProduct,
                                productBuddy.ProductAttributeType);
                            foreach (PlanogramProduct product in
                                taskContext.Planogram.Products.Where(
                                p => Object.Equals(targetProductValue,
                                PlanogramAssortmentProductBuddyProductAttributeTypeHelper.GetValue(p, productBuddy.ProductAttributeType))))
                            {
                                if (!product.Gtin.Equals(productBuddy.ProductGtin))
                                {
                                    //Lookup performance
                                    WorkpackagePerformanceDataDto sourcePerformanceDataDto = null;
                                    if (existingProductWorkpackagePerformanceDataLookup.TryGetValue(product.Gtin, out sourcePerformanceDataDto))
                                    {
                                        //Calculate combined metric value
                                        sourcePerformanceDataDto.CalculateCombinedFinancialMetricValue(taskContext.MetricProfile);
                                        buddiedPerformanceData.Add(sourcePerformanceDataDto);
                                    }
                                }
                            }
                        }

                        if (buddiedPerformanceData.Any())
                        {
                            // Now resolve the buddies into a single performance value using the treatment type.
                            if ((productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.Avg) ||
                                (productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.Sum))
                            {
                                // Average or sum: start by adding up the performance for all buddy products.
                                foreach (WorkpackagePerformanceDataDto value in buddiedPerformanceData)
                                {
                                    //Apply values from buddy to already created performance data
                                    ApplySourceProductBuddyValues(workpackagePerformanceDataDto, value, productBuddy.TreatmentType, 1, buddiedPerformanceData.Count);
                                }
                            }
                            else if ((productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg) ||
                                    (productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg))
                            {
                                IEnumerable<WorkpackagePerformanceDataDto> orderedBuddiedPerformanceData = null;
                                // Now sort the buddy performance data from "best" to "worst", based on the financial profile.
                                if (productBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                                {
                                    //Order by ascending for top %
                                    orderedBuddiedPerformanceData = buddiedPerformanceData.OrderByDescending(p => p.CombinedFinancialMetricValue);
                                }
                                else
                                {
                                    //Order by descending for bottom %
                                    orderedBuddiedPerformanceData = buddiedPerformanceData.OrderBy(p => p.CombinedFinancialMetricValue);
                                }

                                //Get number of products based on treatment type percentage
                                Int32 count = Math.Max(1, (Int32)Math.Round((Double)buddiedPerformanceData.Count() * productBuddy.TreatmentTypePercentage));
                                //Take top number required, correct ordering has been applied previously
                                foreach (WorkpackagePerformanceDataDto value in orderedBuddiedPerformanceData.Take(count))
                                {
                                    ApplySourceProductBuddyValues(workpackagePerformanceDataDto, value, PlanogramAssortmentProductBuddyTreatmentType.Avg, 1, count);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Apply location buddy values based on the treatment type
        /// </summary>
        /// <param name="existingValues"></param>
        /// <param name="newValues"></param>
        /// <param name="treatmentType"></param>
        /// <param name="sourcePercentage"></param>
        /// <param name="numSourceLocations"></param>
        private static void ApplySourceLocationBuddyValues(WorkpackagePerformanceDataDto existingValues,
            WorkpackagePerformanceDataDto newValues,
            PlanogramAssortmentLocationBuddyTreatmentType treatmentType,
            Single? sourcePercentage,
            Int32 numSourceLocations)
        {
            if (treatmentType == PlanogramAssortmentLocationBuddyTreatmentType.Sum)
            {
                //Apply performance as sum
                existingValues.ApplyPerformance(newValues, sourcePercentage.HasValue ? sourcePercentage.Value : 0);
            }
            else
            {
                //Divide all values by num of buddies first
                existingValues.ApplyAvgBuddyPerformance(newValues, sourcePercentage.HasValue ? sourcePercentage.Value : 0, numSourceLocations);
            }
        }

        /// <summary>
        /// Apply product buddy values based on the treatment type
        /// </summary>
        /// <param name="existingValues"></param>
        /// <param name="newValues"></param>
        /// <param name="treatmentType"></param>
        /// <param name="sourcePercentage"></param>
        /// <param name="numSourceLocations"></param>
        private static void ApplySourceProductBuddyValues(WorkpackagePerformanceDataDto existingValue,
            WorkpackagePerformanceDataDto sourceValue,
            PlanogramAssortmentProductBuddyTreatmentType treatmentType,
            Single? sourcePercentage,
            Int32 numSourceProducts)
        {
            if (treatmentType == PlanogramAssortmentProductBuddyTreatmentType.Sum)
            {
                //Apply performance as sum
                existingValue.ApplyPerformance(sourceValue, sourcePercentage.HasValue ? sourcePercentage.Value : 0);
            }
            else if (treatmentType == PlanogramAssortmentProductBuddyTreatmentType.Avg)
            {
                //Divide all values by num of buddies first
                existingValue.ApplyAvgBuddyPerformance(sourceValue, sourcePercentage.HasValue ? sourcePercentage.Value : 0, numSourceProducts);
            }
            else if (treatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
            {
                //Should never reach this as top avg doens't apply to manual source product selection
                throw new NotImplementedException();
            }
            else if (treatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg)
            {
                //Should never reach this as bottom avg doens't apply to manual source product selection
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Helper method to extract a location buddy's specific source values
        /// </summary>
        /// <param name="sourceIndex"></param>
        /// <param name="locationBuddy"></param>
        /// <param name="sourceLocationCode"></param>
        /// <param name="sourcePercentage"></param>
        /// <returns></returns>
        private static Boolean GetLocationBuddySourceValues(Int32 sourceIndex, 
            PlanogramAssortmentLocationBuddy locationBuddy,
            out String sourceLocationCode,
            out Single? sourcePercentage)
        {
            switch (sourceIndex)
            { 
                case (0):
                    sourceLocationCode = locationBuddy.S1LocationCode;
                    sourcePercentage = locationBuddy.S1Percentage;
                    break;
                case (1):
                    sourceLocationCode = locationBuddy.S2LocationCode;
                    sourcePercentage = locationBuddy.S2Percentage;
                    break;
                case (2):
                    sourceLocationCode = locationBuddy.S3LocationCode;
                    sourcePercentage = locationBuddy.S3Percentage;
                    break;
                case (3):
                    sourceLocationCode = locationBuddy.S4LocationCode;
                    sourcePercentage = locationBuddy.S4Percentage;
                    break;
                case (4):
                    sourceLocationCode = locationBuddy.S5LocationCode;
                    sourcePercentage = locationBuddy.S5Percentage;
                    break;
                default:
                    sourceLocationCode = null;
                    sourcePercentage = null;
                    return false;
            }
            
            if (!String.IsNullOrEmpty(sourceLocationCode))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Helper method to extract a product buddy's specific source values
        /// </summary>
        /// <param name="sourceIndex"></param>
        /// <param name="productBuddy"></param>
        /// <param name="sourceLocationCode"></param>
        /// <param name="sourcePercentage"></param>
        /// <returns></returns>
        private static Boolean GetProductBuddySourceValues(Int32 sourceIndex,
            PlanogramAssortmentProductBuddy productBuddy,
            out String sourceProductGtin,
            out Single? sourcePercentage)
        {
            switch (sourceIndex)
            {
                case (0):
                    sourceProductGtin = productBuddy.S1ProductGtin;
                    sourcePercentage = productBuddy.S1Percentage;
                    break;
                case (1):
                    sourceProductGtin = productBuddy.S2ProductGtin;
                    sourcePercentage = productBuddy.S2Percentage;
                    break;
                case (2):
                    sourceProductGtin = productBuddy.S3ProductGtin;
                    sourcePercentage = productBuddy.S3Percentage;
                    break;
                case (3):
                    sourceProductGtin = productBuddy.S4ProductGtin;
                    sourcePercentage = productBuddy.S4Percentage;
                    break;
                case (4):
                    sourceProductGtin = productBuddy.S5ProductGtin;
                    sourcePercentage = productBuddy.S5Percentage;
                    break;
                default:
                    sourceProductGtin = null;
                    sourcePercentage = null;
                    return false;
            }

            if (!String.IsNullOrEmpty(sourceProductGtin))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Saves performance data to the planogram
        /// </summary>
        private static void SavePlanogramPerformanceData(
            TaskContext taskContext,
            IEnumerable<WorkpackagePerformanceDataDto> performanceData)
        {
            using (CodePerformanceMetric codeMetric = new CodePerformanceMetric())
            {
                Planogram planogram = taskContext.Planogram;

                // copy the performance source details into the planogram
                planogram.Performance.Name = taskContext.GfsPerformanceSource.Name;
                planogram.Performance.Description = taskContext.GfsPerformanceSource.Description;

                #region Metrics

                // Metrics should already have been updated at the start of the task as is required before normalising.

                #endregion

                #region Performance Data

                // build a lookup of planogram gtin to
                // planogram product id
                Dictionary<Object, String> productLookup = new Dictionary<Object, String>();
                foreach (PlanogramProduct product in planogram.Products)
                {
                    productLookup.Add(product.Id, product.Gtin);
                }

                // build a lookup dictionary of planogram performance
                // indexed by the product id
                Dictionary<Object, PlanogramPerformanceData> planogramPerformanceData = new Dictionary<Object, PlanogramPerformanceData>();
                foreach (var item in planogram.Performance.PerformanceData)
                {
                    planogramPerformanceData.Add(item.PlanogramProductId, item);
                }

                // ensure we have one performance item
                // for every product in the planogram
                foreach (PlanogramProduct product in planogram.Products)
                {
                    if (!planogramPerformanceData.ContainsKey(product.Id))
                    {
                        PlanogramPerformanceData item = PlanogramPerformanceData.NewPlanogramPerformanceData(product);
                        planogram.Performance.PerformanceData.Add(item);
                        planogramPerformanceData.Add(item.PlanogramProductId, item); // dont forget to add to the lookup
                    }
                }

                Dictionary<String, WorkpackagePerformanceDataDto> performanceDataLookup = performanceData.ToDictionary(p => p.ProductGTIN);

                
                foreach (var item in planogram.Performance.PerformanceData)
                {
                    WorkpackagePerformanceDataDto workpackagePerformanceDataDto = null;

                    if (performanceDataLookup.TryGetValue(productLookup[item.PlanogramProductId], out workpackagePerformanceDataDto))
                    {
                        // populate all the metric values
                        item.P1 = workpackagePerformanceDataDto.P1;
                        item.P2 = workpackagePerformanceDataDto.P2;
                        item.P3 = workpackagePerformanceDataDto.P3;
                        item.P4 = workpackagePerformanceDataDto.P4;
                        item.P5 = workpackagePerformanceDataDto.P5;
                        item.P6 = workpackagePerformanceDataDto.P6;
                        item.P7 = workpackagePerformanceDataDto.P7;
                        item.P8 = workpackagePerformanceDataDto.P8;
                        item.P9 = workpackagePerformanceDataDto.P9;
                        item.P10 = workpackagePerformanceDataDto.P10;
                        item.P11 = workpackagePerformanceDataDto.P11;
                        item.P12 = workpackagePerformanceDataDto.P12;
                        item.P13 = workpackagePerformanceDataDto.P13;
                        item.P14 = workpackagePerformanceDataDto.P14;
                        item.P15 = workpackagePerformanceDataDto.P15;
                        item.P16 = workpackagePerformanceDataDto.P16;
                        item.P17 = workpackagePerformanceDataDto.P17;
                        item.P18 = workpackagePerformanceDataDto.P18;
                        item.P19 = workpackagePerformanceDataDto.P19;
                        item.P20 = workpackagePerformanceDataDto.P20;

                        //calculate CP1-CP10 values
                        taskContext.CalculationResolver.CalculateValues(item);

                        taskContext.ProcessedProductCount++;
                    }
                    else
                    {
                        // reset performance values
                        item.ClearAllValues();
                    }
                }

                #endregion
            }
        }

        private void UpdatePlanogramPerformanceMetrics(TaskContext taskContext)
        {
            Planogram planogram = taskContext.Planogram;


            // build a dictionary of all the performance source
            // metrics, correctly indexed by their metric id
            Dictionary<Byte, GFSModel.PerformanceSourceMetric> performanceSourceMetrics = new Dictionary<Byte, GFSModel.PerformanceSourceMetric>();
            foreach (var performanceSourceMetric in taskContext.GfsPerformanceSource.PerformanceSourceMetrics)
            {
                Byte metricId = (Byte)performanceSourceMetric.ColumnNumber;
                performanceSourceMetrics.Add(
                    metricId,
                    performanceSourceMetric);
            }

            // build a dictionary of metrics,
            // indexed by the metric name
            Dictionary<String, GFSModel.Metric> masterMetrics = new Dictionary<string, GFSModel.Metric>();
            foreach (GFSModel.Metric metric in taskContext.GfsMetrics)
            {
                masterMetrics.Add(metric.Name, metric);
            }

            // ensure the number of metrics within the
            // planogram matches that within the performance source
            while (planogram.Performance.Metrics.Count != performanceSourceMetrics.Count)
            {
                if (planogram.Performance.Metrics.Count > performanceSourceMetrics.Count)
                {
                    planogram.Performance.Metrics.RemoveAt(0);
                }
                else
                {
                    planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric());
                }
            }

            Dictionary<String, PerformanceSourceMetricAggregationType> aggregationTypes = taskContext.PerformanceSelection.GetAggregationTypes(taskContext.GfsPerformanceSource).ToDictionary(d => d.PerformanceSourceMetricName);

            // now populate the planogram performance metrics
            Int32 itemIndex = 0;
            foreach (var item in performanceSourceMetrics.OrderBy(i => i.Key))
            {
                // get all the metrics
                PlanogramPerformanceMetric performanceMetric = planogram.Performance.Metrics[itemIndex];
                GFSModel.PerformanceSourceMetric performanceSourceMetric = item.Value;
                GFSModel.Metric masterMetric = masterMetrics[performanceSourceMetric.MetricName];

                //Only get the aggregation types for non-calculated metrics. Calculated metrics are not aggregatable
                PerformanceSourceMetricAggregationType aggregationType = null;
                if (!masterMetric.IsCalculated)
                {
                    aggregationType = aggregationTypes[performanceSourceMetric.MetricName];
                }

                // copy the metric details
                performanceMetric.CopyValues(masterMetric);
                if (aggregationType != null)
                {
                    performanceMetric.AggregationType = GetAggregationType(aggregationType);
                }

                performanceMetric.MetricId = item.Key;

                itemIndex++;
            }

            planogram.Performance.CheckBusinessRules(PlanogramPerformance.MetricsProperty);
        }

        /// <summary>
        /// Gets the equivalent PlanogramPerformanceMetricAggregationType from the PerformanceSourceMetricAggregationType
        /// </summary>
        /// <param name="aggregation"></param>
        /// <returns></returns>
        private static AggregationType GetAggregationType(PerformanceSourceMetricAggregationType aggregation)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                AggregationType gfsType = AggregationType.Sum; //default to sum
                Enum.TryParse<AggregationType>(aggregation.AggregationType, out gfsType);
                
                return gfsType;
            }
        }
        
        #endregion
    }

    /// <summary>
    /// Helper class for holding performance data selection details
    /// </summary>
    public class PerformanceDataSelectionHelper
    {
        #region Fields
        private List<Int64> _timelineCodes = new List<Int64>();
        private List<String> _productCodes = new List<String>();
        private List<String> _locationCodes = new List<String>();
        private List<Int32> _planogramAssignments = new List<Int32>();
        private Dictionary<String, List<String>> _clusterSchemes = new Dictionary<String, List<String>>();
        #endregion

        #region Properties

        public List<Int64> TimelineCodes
        {
            get { return _timelineCodes; }
        }
        public List<String> ProductCodes
        {
            get { return _productCodes; }
        }
        public List<String> LocationCodes
        {
            get { return _locationCodes; }
        }
        public List<Int32> PlanogramAssignments
        {
            get { return _planogramAssignments; }
        }
        public Dictionary<String, List<String>> ClusterSchemes
        {
            get { return _clusterSchemes; }
        }

        public WorkpackagePerformanceDataType WorkpackagePerformanceDataType { get; set; }
        public Int32 DataId { get; set; }
        public Int32 LocationBatchSize { get; set; }
        
        #endregion

        public PerformanceDataSelectionHelper() { }
    }
}
