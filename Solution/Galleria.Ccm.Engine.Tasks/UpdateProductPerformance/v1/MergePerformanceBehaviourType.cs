﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32730 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Merge Performance Behaviour parameter.
    /// </summary>
    public enum MergePerformanceBehaviourType
    {
        AlwaysMergePerformance,
        MergeWhenNoLocationPerformance
    }

    /// <summary>
    ///     Helpers for <see cref="MergePerformanceBehaviourType"/>.
    /// </summary>
    public static class MergePerformanceBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="MergePerformanceBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<MergePerformanceBehaviourType, String> FriendlyNames = new Dictionary
            <MergePerformanceBehaviourType, String>
        {
            {MergePerformanceBehaviourType.AlwaysMergePerformance, Message.MergePerformanceBehaviourType_AlwaysMergePerformance},
            {MergePerformanceBehaviourType.MergeWhenNoLocationPerformance, Message.MergePerformanceBehaviourType_MergeWhenNoLocationPerformance} 
        };
    }
}