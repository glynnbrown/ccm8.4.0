﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
#endregion
#region Version History : CCM820
// V8-30727 : D.Pleasance
//  Added PlanogramAssignmentPerformanceWeighting.
#endregion
#region Version History : CCM830
// V8-32205 : A.Probyn
//  Added new NumberOfProductBuddiesApplied
//  Added new NumberOfLocationBuddiesApplied
// V8-32730 : D.Pleasance
//  Added MergePerformanceBehaviourType.
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields
        private Single _enterprisePerformanceWeighting;
        private Single _clusterPerformanceWeighting;
        private Single _storePerformanceWeighting;
        private Single _planogramAssignmentPerformanceWeighting;
        private String _performanceSelectionName;
        private String _metricProfileName;
        private PerformanceSelection _performanceSelection;
        private MetricProfile _masterMetricProfile;
        private IPlanogramMetricProfile _metricProfile;
        private Boolean _isAppliedProductBuddyLoggingRequired = false;
        private Boolean _isAppliedLocationBuddyLoggingRequired = false;

        private GFSModel.Entity _gfsEntity;
        private GFSModel.MetricList _gfsMetrics;
        private GFSModel.PerformanceSource _gfsPerformanceSource;
        private GFSModel.PerformanceSourceDaysPerPeriodType _sourceDaysPerPeriodType = GFSModel.PerformanceSourceDaysPerPeriodType.Unknown;
        private Int32 _processedProductCount = 0;
        private Dictionary<String, PlanogramProduct> _planogramProductsByGtin = new Dictionary<String, PlanogramProduct>();
        private Dictionary<String, Int16> _locationIdsByCode = new Dictionary<String, Int16>();
        private Dictionary<String, PlanogramAssortmentProductBuddy> _productBuddiesByGtin = new Dictionary<String, PlanogramAssortmentProductBuddy>();
        private Dictionary<String, PlanogramAssortmentLocationBuddy> _locationBuddiesByCode = new Dictionary<String, PlanogramAssortmentLocationBuddy>();

        #endregion

        #region Constructor

        public TaskContext(ITaskContext context) : base(context) { }

        #endregion

        #region Properties

        /// <summary>
        /// The enterprise performance data weighting
        /// </summary>
        public Single EnterprisePerformanceWeighting
        {
            get { return _enterprisePerformanceWeighting; }
            set { _enterprisePerformanceWeighting = value; }
        }

        /// <summary>
        /// The cluster performance data weighting
        /// </summary>
        public Single ClusterPerformanceWeighting
        {
            get { return _clusterPerformanceWeighting; }
            set { _clusterPerformanceWeighting = value; }
        }

        /// <summary>
        /// The store performance data weighting
        /// </summary>
        public Single StorePerformanceWeighting
        {
            get { return _storePerformanceWeighting; }
            set { _storePerformanceWeighting = value; }
        }

        /// <summary>
        /// The planogram assignment performance data weighting
        /// </summary>
        public Single PlanogramAssignmentPerformanceWeighting
        {
            get { return _planogramAssignmentPerformanceWeighting; }
            set { _planogramAssignmentPerformanceWeighting = value; }
        }

        /// <summary>
        /// The performance selection name
        /// </summary>
        public String PerformanceSelectionName
        {
            get { return _performanceSelectionName; }
            set { _performanceSelectionName = value; }
        }

        /// <summary>
        /// The performance selection
        /// </summary>
        public PerformanceSelection PerformanceSelection
        {
            get { return _performanceSelection; }
            set { _performanceSelection = value; }
        }
        
        /// <summary>
        /// The metric profile name
        /// </summary>
        public String MetricProfileName
        {
            get { return _metricProfileName; }
            set { _metricProfileName = value; }
        }

        /// <summary>
        /// The master metric profile
        /// </summary>
        public MetricProfile MasterMetricProfile
        {
            get { return _masterMetricProfile; }
            set { _masterMetricProfile = value; }
        }


        /// <summary>
        /// The metric profile
        /// </summary>
        public IPlanogramMetricProfile MetricProfile
        {
            get { return _metricProfile; }
            set { _metricProfile = value; }
        }

        /// <summary>
        /// The GFS Entity
        /// </summary>
        public GFSModel.Entity GfsEntity
        {
            get { return _gfsEntity; }
            set { _gfsEntity = value; }
        }

        /// <summary>
        /// The GFS Metrics
        /// </summary>
        public GFSModel.MetricList GfsMetrics
        {
            get { return _gfsMetrics; }
            set { _gfsMetrics = value; }
        }

        /// <summary>
        /// The GFS performance source
        /// </summary>
        public GFSModel.PerformanceSource GfsPerformanceSource
        {
            get { return _gfsPerformanceSource; }
            set { _gfsPerformanceSource = value; }
        }

        /// <summary>
        /// The performance source period type
        /// </summary>
        public GFSModel.PerformanceSourceDaysPerPeriodType SourceDaysPerPeriodType
        {
            get { return _sourceDaysPerPeriodType; }
            set { _sourceDaysPerPeriodType = value; }
        }

        /// <summary>
        /// The processed product count (Number of products performance updated)
        /// </summary>
        public Int32 ProcessedProductCount
        {
            get { return _processedProductCount; }
            set { _processedProductCount = value; }
        }

        // dictionary of all products products
        public Dictionary<String, PlanogramProduct> PlanogramProductsByGtin
        {
            get { return _planogramProductsByGtin; }
        }

        // dictionary of all available locations
        public Dictionary<String, Int16> LocationIdsByCode
        {
            get { return _locationIdsByCode; }
        }

        // dictionary of all available product buddies
        public Dictionary<String, PlanogramAssortmentProductBuddy> ProductBuddiesByGtin
        {
            get { return _productBuddiesByGtin; }
        }

        // dictionary of all available location buddies
        public Dictionary<String, PlanogramAssortmentLocationBuddy> LocationBuddiesByCode
        {
            get { return _locationBuddiesByCode; }
        }

        public Boolean IsAppliedProductBuddyLoggingRequired 
        {
            get { return _isAppliedProductBuddyLoggingRequired; }
            set { _isAppliedProductBuddyLoggingRequired = value; }
        }

        public Boolean IsAppliedLocationBuddyLoggingRequired
        {
            get { return _isAppliedLocationBuddyLoggingRequired; }
            set { _isAppliedLocationBuddyLoggingRequired = value; }
        }

        public MergePerformanceBehaviourType MergePerformanceBehaviour { get; set; }

        public PerformanceCalculationResolver CalculationResolver { get; set; }
        #endregion
    }
}