#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29198 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.InsertProductFromAssortment.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Unplaced Products parameter.
    /// </summary>
    public enum UnplacedProductsType
    {
        AddToCarparkShelf,
        DoNotAdd
    }

    /// <summary>
    ///     Helpers for <see cref="UnplacedProductsType"/>.
    /// </summary>
    public static class UnplacedProductsTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="UnplacedProductsType"/>.
        /// </summary>
        public static readonly Dictionary<UnplacedProductsType, String> FriendlyNames = new Dictionary
            <UnplacedProductsType, String>
        {
            {UnplacedProductsType.AddToCarparkShelf, Message.UnplacedProductsType_AddToCarparkShelf},
            {UnplacedProductsType.DoNotAdd, Message.UnplacedProductsType_DoNotAdd}
        };
    }
}