#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29198 : A.Silva
//      Created
// V8-29424 : A.Silva
//      Refactored the name of the enum.
// V8-29419 : A.Silva
//      Added AlwaysAdd member to DroppedProductBehaviour.

#endregion

#region Version History CCM803

// V8-29554 : A.Silva
//  Removed obsolete methods.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.InsertProductFromAssortment.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Unplaced Products parameter.
    /// </summary>
    public enum DroppedProductBehaviourType
    {
        AddToCarParkComponent = 0,
        DoNotAdd = 1,
        AlwaysAdd = 2
    }

    /// <summary>
    ///     Helpers for <see cref="DroppedProductBehaviourType"/>.
    /// </summary>
    public static class DroppedProductBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="DroppedProductBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<DroppedProductBehaviourType, String> FriendlyNames = new Dictionary
            <DroppedProductBehaviourType, String>
        {
            {DroppedProductBehaviourType.AddToCarParkComponent, Message.DroppedProductBehaviourType_AddToCarParkComponent},
            {DroppedProductBehaviourType.DoNotAdd, Message.DroppedProductBehaviourType_DoNotAdd},
            {DroppedProductBehaviourType.AlwaysAdd, Message.DroppedProductBehaviourType_AlwaysAdd}
        };
    }
}