﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30760 : D.Pleasance
//      Created.
#endregion
#region Version History: CCM830
// V8-32726 : D.Pleasance
//  Added IsOptionalAndNotRangedInRankOrder
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.InsertProductFromAssortment.v1
{
    /// <summary>
    ///     Possible values for the PlacementOrderType parameter
    /// </summary>
    public enum PlacementOrderType
    {
        RangedInRankOrder = 0,
        NotRangedInRankOrder = 1,
        InAssortmentRankOrder = 2,
        IsOptionalAndNotRangedInRankOrder = 3
    }

    /// <summary>
    /// Contains helpers for the PlacementOrderType enum.
    /// </summary>
    public static class PlacementOrderTypeHelper
    {
        public static readonly Dictionary<PlacementOrderType, String> FriendlyNames =
            new Dictionary<PlacementOrderType, String>
            {
                {PlacementOrderType.RangedInRankOrder, Message.PlacementOrderType_RangedInRankOrder },
                {PlacementOrderType.NotRangedInRankOrder, Message.PlacementOrderType_NotRangedInRankOrder },
                {PlacementOrderType.InAssortmentRankOrder, Message.PlacementOrderType_InAssortmentRankOrder },
                {PlacementOrderType.IsOptionalAndNotRangedInRankOrder, Message.PlacementOrderType_IsOptionalAndNotRangedInRankOrder }
            };
    }
}