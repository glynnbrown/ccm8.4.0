﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Threading;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.Debug.v1
{
    /// <summary>
    /// A simple debug task
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.Debug_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.Debug_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_Debug; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Indicates if this task has a pre step
        /// </summary>
        public override bool HasPreStep
        {
            get { return false; }
        }

        /// <summary>
        /// Indicates if this task has a post step
        /// </summary>
        public override bool HasPostStep
        {
            get { return false; }
        }

#if !DEBUG
        /// <summary>
        /// Indicates if this task is visible
        /// </summary>
        public override bool IsVisible
        {
            get
            {
                return false;
            }
        }
#endif
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // no parameters to register
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called on a pre-execution of this task
        /// </summary>
        public override void OnPreExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Thread.Sleep(30000);
            }
        }

        /// <summary>
        /// Called on a post execution of this task
        /// </summary>
        public override void OnPostExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Thread.Sleep(30000);
            }
        }

        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Thread.Sleep(30000);
                context.LogInformation(PlanogramEventLogAffectedType.Planogram, context.Planogram.Id, EventLogEvent.NothingForTaskToDo, "This message is for debugging purposes");
            }
        }
        #endregion
    }
}
