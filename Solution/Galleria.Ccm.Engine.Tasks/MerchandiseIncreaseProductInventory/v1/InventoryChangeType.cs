﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016
#endregion

using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Planograms.Merchandising;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1
{
    /// <summary>
    /// Denotes the different ways that the task can increase the inventory of products.
    /// </summary>
    public enum InventoryChangeType
    {
        /// <summary>
        /// Indicates that the task should increase inventory one unit at a time.
        /// </summary>
        ByUnits = 0,

        /// <summary>
        /// Indicates that the task should increase inventory one facing at a time.
        /// </summary>
        ByFacings  = 1,
    }

    public static class InventoryChangeTypeHelper
    {
        public static Dictionary<InventoryChangeType, String> FriendlyNames = new Dictionary<InventoryChangeType, String>()
        {
            { InventoryChangeType.ByFacings, Message.InventoryChangeType_ByFacings },
            { InventoryChangeType.ByUnits, Message.InventoryChangeType_ByUnits},
        };

        public static PlanogramMerchandisingInventoryChangeType GetPlanogramMerchandisingInventoryChange(
            this InventoryChangeType inventoryChange)
        {
            switch (inventoryChange)
            {
                case InventoryChangeType.ByFacings:
                    return PlanogramMerchandisingInventoryChangeType.ByFacings;
                case InventoryChangeType.ByUnits:
                    return PlanogramMerchandisingInventoryChangeType.ByUnits;
                default:
                    System.Diagnostics.Debug.Fail($"Unknown inventory change: {inventoryChange}");
                    return PlanogramMerchandisingInventoryChangeType.ByUnits;
            }
        }
    }
}
