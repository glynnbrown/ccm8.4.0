﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : D.Pleasance
//  Created.
// V8-32612 : D.Pleasance
//  Added MinimumBlockSpace
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1
{
    /// <summary>
    /// 
    /// </summary>
    public enum SpaceConstraintType
    {
        BlockSpace = 0,
        BeyondBlockSpace = 1,
        MinimumBlockSpace = 2
    }

    public static class SpaceConstraintTypeHelper
    {
        public static Dictionary<SpaceConstraintType, String> FriendlyNames = new Dictionary<SpaceConstraintType, String>()
        {
            { SpaceConstraintType.BlockSpace, Message.MerchandiseIncreaseProductInventory_SpaceConstraintType_BlockSpace }, 
            { SpaceConstraintType.BeyondBlockSpace, Message.MerchandiseIncreaseProductInventory_SpaceConstraintType_BeyondBlockSpace },
            { SpaceConstraintType.MinimumBlockSpace, Message.MerchandiseIncreaseProductInventory_SpaceConstraintType_MinimumBlockSpace },
        };
    }
}