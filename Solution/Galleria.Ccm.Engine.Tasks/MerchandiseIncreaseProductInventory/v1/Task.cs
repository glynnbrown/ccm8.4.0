﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : D.Pleasance
//  Created.
// V8-32315 : D.Pleasance
//  Amended so that the percentage of ranked products doesn’t change product selection after each iteration. 
//  The product selection should be the original product GTINs that the process started with. These are the only products that should be increased.
// V8-32372 : D.Pleasance
//  Amended so that task logs error if any of (Blocking, Sequence) is missing from the plan.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32358 : D.Pleasance
//  Added further logging for starting \ final inventory units.
// V8-32612 : D.Pleasance
//  Amended PlanogramMerchandisingBlock constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32671 : D.Pleasance
//  Added call to update merchandising block sequence numbers, UpdateMerchandisingBlockPositionPlacementsWithActualSequenceGroupNumber()
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// V8-32617 : D.Pleasance
//  Amended ProductSelectionType.ProductsBelowAverageDos, so that all positions are increased to the initial achieved dos average before 
//  recaulating achieved dos average to iterate through next selection of positions.
// V8-32742 : D.Pleasance
//  Added IncreaseBlockInventory()
//  Amended IncreaseProductInventory() so that consecuetive block positions can be increased at the same time.
// V8-32800 : D.Pleasance
//  Changes applied to increase inventory units fairly between positions that are targeted to increase. 
//  To achieve this we keep a running total of the target units whilst checking this against the actual placed units.
// V8-32825 : A.Silva
//  Added call to RestoreSequenceNumbers in the context so that any altered sequence numbers as part of the process are restored.
// V8-32787 : A.Silva
//  Added an AssortmentRuleEnforcer field to allow easy enabling of rule enforcing.
// V8-32982 : A.Kuszyk
//  Re-factored common code into PlanogramMerchandiser helper class and introduced performance enhancements.
// CCM-18563 : A.Silva
//  Changed the way BuildDictionaries gets the Merchandising Groups so that now all outside the blocking space are ignored.
// CCM-18487 : A.Kuszyk
//  Added call to ClearPositionsToIgnore to ensure that products that have a chance to be re-added do so.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1
{
    /// <summary>
    /// Increases the units of products in the plan, in accordance with the selection and ordering parameter values,
    /// until the plan is full whilst maintaining block presentation.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Fields

        private PlanogramMerchandiser<TaskContext> _planogramMerchandiser;

        #endregion  

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter
        private enum Parameter
        {
            /// <summary>
            /// The constraint for increasing product inventory, Into Block Space \ Beyond block space
            /// </summary>
            SpaceConstraint = 0,
            /// <summary>
            /// The order in which products should be processed.
            /// </summary>
            ProcessingOrder = 1,
            /// <summary>
            /// The method to be used when selecting products.
            /// </summary>
            ProductSelection = 2,
            /// <summary>
            /// The percentage of ranked products to use in conjunction with the TopRankedProducts selection option.
            /// </summary>
            PercentageOfRankedProducts = 3,
            /// <summary>
            /// The car park component to exclude from inventory increase
            /// </summary>
            CarParkComponentName = 4,
            /// <summary>
            /// Indicates whether facings or units should be changed when making inventory increases.
            /// </summary>
            InventoryChange = 5,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.MerchandiseIncreaseProductInventory_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.MerchandiseIncreaseProductInventory_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.SpaceConstraint,
                Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Name,
                Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Description,
                Message.ParameterCategory_Default,
                typeof(SpaceConstraintType),
                SpaceConstraintType.BlockSpace,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingOrder,
                Message.MerchandiseIncreaseProductInventory_ProcessingOrder_Name,
                Message.MerchandiseIncreaseProductInventory_ProcessingOrder_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingOrderType),
                ProcessingOrderType.ByRank,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.ProductSelection,
                Message.MerchandiseIncreaseProductInventory_ProductSelection_Name, 
                Message.MerchandiseIncreaseProductInventory_ProductSelection_Description,
                Message.ParameterCategory_Default,
                typeof(ProductSelectionType),
                ProductSelectionType.AllProducts,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.PercentageOfRankedProducts,
                Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProducts_Name, 
                Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProducts_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                1,
                false,
                false,
                (Int32)Parameter.ProductSelection,
                IsPercentageOfRankedProductsRequired,
                null,
                IsPercentageOfRankedProductsValid));

            // car park
            this.RegisterParameter(new TaskParameter(
                Parameter.CarParkComponentName,
                Message.MerchandiseIncreaseProductInventory_Parameter_CarParkComponentName_Name,
                Message.MerchandiseIncreaseProductInventory_Parameter_CarParkComponentName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.String,
                Message.InserProduct_Parameter_CarParkComponentName_Default,
                true,
                true,
                null,
                null,
                null,
                IsCarParkNameValid));

            this.RegisterParameter(new TaskParameter(
                Parameter.InventoryChange,
                Message.IncreaseProductInventory_InventoryChange_Name,
                Message.IncreaseProductInventory_InventoryChange_Description,
                Message.ParameterCategory_Default,
                typeof(InventoryChangeType),
                InventoryChangeType.ByUnits,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                _planogramMerchandiser = new PlanogramMerchandiser<TaskContext>(taskContext);
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!BuildDictionaries(taskContext)) return;
                if (!IncreaseBlockInventory(taskContext)) return;
                if (!IncreaseProductInventory(taskContext)) return;
                if (!LogCompletionAndInventoryChanges(taskContext)) return;
                ReMerchandisePlanogram(taskContext);
            }
        }

        #endregion

        #region Methods

        private static Boolean ValidatePlanogram(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                #region Validate Blocking Strategy

                //  Get the last Blocking strategy in the planogram.
                context.Blocking =
                    context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                    context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ??
                    context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial) ??
                    context.Planogram.Blocking.FirstOrDefault();
                if (context.Blocking == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent, Message.TaskLogging_BlockingStrategy, Message.TaskLogging_NoBlockingStrategyWasPresent);
                    return false;
                }

                #endregion

                #region Validate Sequence Data

                //  If there is no sequence the task cannot proceed.
                if (context.Planogram.Sequence == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_Sequence,
                                     Message.TaskLogging_NoSequenceInformationWasPresent);
                    return false;
                }
                //  OR If there are no sequence groups the task cannot proceed.
                else if (context.Planogram.Sequence.Groups.Count == 0 ||
                         context.Planogram.Sequence.Groups.All(g => g.Products.Count == 0))
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_SequenceProducts,
                                     Message.TaskLogging_NoSequenceProductsWerePresent);
                    return false;
                }

                #endregion

                if(context.ProcessingOrder == ProcessingOrderType.ByLowestDos)
                {
                    IEnumerable<PlanogramPerformanceData> performanceData = context.Planogram.Positions
                        .Select(p => p.GetPlanogramPerformanceData()).Where(p => p != null);
                    if (performanceData.All(p => p.AchievedDos.HasValue && p.AchievedDos.Value.EqualTo(0)))
                    {
                        context.LogWarning(EventLogEvent.NothingForTaskToDo, Message.IncreaseProductInventory_ByLowestDos_NothingToDo);
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Initializes the task context.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean BuildDictionaries(TaskContext taskContext)
        {
            using (new CodePerformanceMetric()) {
                taskContext.RanksByProductGtin = taskContext.Planogram.Assortment.Products
                                                            .ToDictionary(ap => ap.Gtin, ap => (Int32)ap.Rank);
                taskContext.PerformanceDataByProductId = taskContext.Planogram.Performance.PerformanceData
                                                                    .Where(p =>
                                                                           {
                                                                               var prod = p.GetPlanogramProduct();
                                                                               return prod != null && prod.GetPlanogramPositions().Any();
                                                                           })
                                                                    .ToDictionary(p => p.PlanogramProductId, p => p);

                #region Blocking

                Single width, height, offset;
                taskContext.Planogram.GetBlockingAreaSize(out height, out width, out offset);
                taskContext.BlockingWidth = width;
                taskContext.BlockingHeight = height;
                taskContext.BlockingHeightOffset = offset;

                #endregion

                #region Merchandising Groups

                taskContext.MerchandisingGroups = taskContext.Planogram.GetMerchandisingGroupsInBlockingArea();

                #endregion

                #region Product Stacks

                taskContext.ProductStacksBySequenceColour = taskContext.Blocking.GetAndUpdateProductStacksByColour(taskContext.MerchandisingGroups);

                #endregion

                #region Target Units

                taskContext.TargetUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();
                taskContext.BlockUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();

                foreach (PlanogramMerchandisingGroup merchandisingGroup in taskContext.MerchandisingGroups)
                {
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        PlanogramPosition position = positionPlacement.Position;

                        if (!position.SequenceColour.HasValue) continue;

                        Dictionary<String, Int32> targetUnits;
                        Dictionary<String, Int32> blockUnits;
                        if (!taskContext.TargetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                        {
                            targetUnits = new Dictionary<String, Int32>();
                            taskContext.TargetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                        }

                        if (!taskContext.BlockUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out blockUnits))
                        {
                            blockUnits = new Dictionary<String, Int32>();
                            taskContext.BlockUnitsBySequenceColour.Add(position.SequenceColour.Value, blockUnits);
                        }

                        String productGtin = position.GetPlanogramProduct().Gtin;
                        if (targetUnits.ContainsKey(productGtin)) continue;
                        targetUnits.Add(productGtin, position.TotalUnits);
                        blockUnits.Add(productGtin, position.TotalUnits);
                    }
                }

                #endregion

                taskContext.BlockingGroupsByColour = taskContext.Blocking.Groups.ToDictionary(p => p.Colour, p => p);
                taskContext.SequenceGroupsByColour = taskContext.Planogram.Sequence.Groups.ToDictionary(p => p.Colour, p => p);
                
                return true;
            }
        }

        /// <summary>
        /// Validates the task parameters and stores them in <paramref name="taskContext"/>.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean ValidateParameters(TaskContext taskContext)
        {
            using (new CodePerformanceMetric())
            {
                Boolean success = true;

                #region ProcessingOrder
                if (Parameters.Count <= (Int32)Parameter.ProcessingOrder || Parameters[Parameter.ProcessingOrder].Value?.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.MerchandiseIncreaseProductInventory_ProcessingOrder_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProcessingOrder = (ProcessingOrderType)Parameters[Parameter.ProcessingOrder].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseIncreaseProductInventory_ProcessingOrder_Name,
                            Parameters[Parameter.ProcessingOrder].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region ProductSelection
                if (Parameters.Count <= (Int32)Parameter.ProductSelection || Parameters[Parameter.ProductSelection].Value?.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.MerchandiseIncreaseProductInventory_ProductSelection_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProductSelection = (ProductSelectionType)Parameters[Parameter.ProductSelection].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseIncreaseProductInventory_ProductSelection_Name,
                            Parameters[Parameter.ProductSelection].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region PercentageOfRankedProducts
                if (Parameters.Count <= (Int32)Parameter.PercentageOfRankedProducts ||
                    Parameters[Parameter.PercentageOfRankedProducts].Value == null) // This value can be null
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProducts_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.PercentageOfRankedProducts = Convert.ToSingle(Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                    }
                    catch (FormatException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProducts_Name,
                            Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region Space Constraint
                if (Parameters.Count <= (Int32)Parameter.SpaceConstraint || Parameters[Parameter.SpaceConstraint].Value?.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.SpaceConstraint = (SpaceConstraintType)Parameters[Parameter.SpaceConstraint].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseIncreaseProductInventory_SpaceConstraint_Name,
                            Parameters[Parameter.SpaceConstraint].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.CarParkComponentName)) || Parameters[Parameter.CarParkComponentName].Value?.Value1 == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseIncreaseProductInventory_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    taskContext.CarParkComponentName = Convert.ToString(Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(taskContext.CarParkComponentName)) || (String.IsNullOrWhiteSpace(taskContext.CarParkComponentName)))
                    {
                        taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseIncreaseProductInventory_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region InventoryChange
                if (Parameters.Count <= (Int32)Parameter.InventoryChange ||
                    Parameters[Parameter.InventoryChange].Value == null ||
                    Parameters[Parameter.InventoryChange].Value.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.IncreaseProductInventory_InventoryChange_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.InventoryChange = (InventoryChangeType)Parameters[Parameter.InventoryChange].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.IncreaseProductInventory_InventoryChange_Name,
                            Parameters[Parameter.InventoryChange].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                return success;
            }
        }

        /// <summary>
        /// Enumerates through the product selection in order, increasing units until no further increases are possible within inventory blocks
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean IncreaseBlockInventory(TaskContext taskContext)
        {
            using (new CodePerformanceMetric())
            {
                if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos) CalculateAverageAchievedDos(taskContext);
                taskContext.UnitsWereIncreased = true;

                while (taskContext.UnitsWereIncreased)
                {
                    List<PlanogramPositionPlacement> positionsToIncrease = _planogramMerchandiser
                        .EnumerateSelectedPositionPlacementsInOrder(
                            taskContext.ProductSelection.GetPlanogramMerchandiserProductSelection(),
                            taskContext.ProcessingOrder.GetPlanogramMerchandiserProcessingOrder(),
                            taskContext.AverageAchievedDos,
                            taskContext.PercentageOfRankedProducts)
                        .ToList();
                    if (!positionsToIncrease.Any()) break;
                    SetBlockUnitsToIncrease(taskContext, positionsToIncrease);

                    taskContext.UnitsWereIncreased = _planogramMerchandiser.MerchandiseInventoryChange(
                        taskContext.Blocking.EnumerateGroupsInOptimisationOrder(), taskContext.BlockUnitsBySequenceColour);

                    if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos && 
                        taskContext.UnitsWereIncreased)
                    {
                        ValidateProductsBelowAverageDosIncrease(taskContext);
                    }
                }

                return true;
            }
        }

        private static void SetBlockUnitsToIncrease(TaskContext taskContext, List<PlanogramPositionPlacement> positionsToIncrease)
        {
            foreach (PlanogramBlockingGroup block in taskContext.Blocking.Groups)
            {
                List<PlanogramPositionPlacement> blockPositionsToIncrease = positionsToIncrease
                    .Where(p =>
                        p.Position.SequenceColour != null &&
                        p.Position.SequenceColour.Value == block.Colour)
                    .ToList();
                if (!blockPositionsToIncrease.Any())
                {
                    continue;
                }

                taskContext.CurrentGtinsIncreasedByBlockInventory = new List<String>();

                // increment until all positions within the block have been increased past current placed unit value
                while (blockPositionsToIncrease.Count != taskContext.CurrentGtinsIncreasedByBlockInventory.Count)
                {
                    foreach (PlanogramPositionPlacement position in blockPositionsToIncrease)
                    {
                        // Remember the top ranked products, these should not change after each round of inventory increase
                        if (taskContext.ProductSelection == ProductSelectionType.TopRankedProducts)
                        {
                            if (!taskContext.TopRakedProductPercentageGtins.Contains(position.Product.Gtin))
                            {
                                taskContext.TopRakedProductPercentageGtins.Add(position.Product.Gtin);
                            }
                        }

                        IncreaseUnitsForBlockInventory(taskContext, position);
                    }
                }
            }
        }

        /// <summary>
        /// Enumerates through the product selection in order, increasing units until no further increases are possible.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean IncreaseProductInventory(TaskContext taskContext)
        {
            using (new CodePerformanceMetric())
            {
                taskContext.UnitsWereIncreased = true;
                List<Int32> consecutiveBlocksToIgnore = new List<Int32>();

                if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos && !taskContext.RecalculateAverageAchievedDos)
                {
                    CalculateAverageAchievedDos(taskContext);
                }

                // Loop whilst we're still able to increase inventory, or when we're processing lowest dos positions.
                // We'll always break out of this loop when there are no longer positions to be increased.
                while (taskContext.UnitsWereIncreased || taskContext.ProcessingOrder == ProcessingOrderType.ByLowestDos)
                {
                    // Reset units increased flag. This will only be set to true if we were able to increase the units
                    // of a position. If not, then the loop will terminate when no further increases were possible.
                    taskContext.UnitsWereIncreased = false;

                    // Clean the positions to ignore list of positions that may now have space to be placed.
                    // Note : as of CCM-18873, this line has been commented out, because it appears to cause an infinite
                    // loop. The line has been left in for completeness, in order to illustrate that this method exists
                    // and should, at some point, be re-introduced into the task. It was initially written to resolve
                    // CCM-18487, but has ultimately been left out due to time constraints prior to the 831 release.
                    //_planogramMerchandiser.CleanPositionsToIgnore();

                    List<PlanogramPositionPlacement> positionPlacementsToIncrease = _planogramMerchandiser
                        .EnumerateSelectedPositionPlacementsInOrder(
                            taskContext.ProductSelection.GetPlanogramMerchandiserProductSelection(),
                            taskContext.ProcessingOrder.GetPlanogramMerchandiserProcessingOrder(),
                            taskContext.AverageAchievedDos,
                            taskContext.PercentageOfRankedProducts)
                        .ToList();

                    // Ensure that if there are no products to increase we still break out of the loop.
                    // This handles the fact that that the while always loops with we're increasing the
                    // lowest dos positions.
                    if (!positionPlacementsToIncrease.Any()) break;

                    List<PlanogramPositionPlacement> positionPlacementsGroupsToIncrease = new List<PlanogramPositionPlacement>();
                    Int32 positionPlacementsToIncreaseCount = 0;

                    // Iterate through the selected products in order and try increasing units.
                    foreach (PlanogramPositionPlacement position in positionPlacementsToIncrease)
                    {
                        // Remember the top ranked products, these should not change after each round of inventory increase
                        if (taskContext.ProductSelection == ProductSelectionType.TopRankedProducts)
                        {
                            if (!taskContext.TopRakedProductPercentageGtins.Contains(position.Product.Gtin))
                            {
                                taskContext.TopRakedProductPercentageGtins.Add(position.Product.Gtin);
                            }
                        }

                        positionPlacementsGroupsToIncrease.Add(positionPlacementsToIncrease[positionPlacementsToIncreaseCount]);

                        // If the position increase is the last one or is of a consecuetive block to ignore or isnt part of a consecuetive group (same block)
                        if (positionPlacementsToIncrease.Count == positionPlacementsToIncreaseCount + 1 ||
                            consecutiveBlocksToIgnore.Contains(positionPlacementsToIncrease[positionPlacementsToIncreaseCount].Position.SequenceColour.Value) ||
                            positionPlacementsToIncrease[positionPlacementsToIncreaseCount].Position.SequenceColour != positionPlacementsToIncrease[positionPlacementsToIncreaseCount + 1].Position.SequenceColour)
                        {
                            if (positionPlacementsGroupsToIncrease.Count > 1)
                            {
                                Boolean increasedMultiple = _planogramMerchandiser.MerchandiseInventoryChange(positionPlacementsGroupsToIncrease);
                                if (!increasedMultiple)
                                {
                                    foreach (PlanogramPositionPlacement placement in positionPlacementsGroupsToIncrease)
                                    {
                                        taskContext.UnitsWereIncreased = 
                                            _planogramMerchandiser.MerchandiseInventoryChange(placement) || 
                                            taskContext.UnitsWereIncreased;
                                    }

                                    Int32? sequenceColour = positionPlacementsGroupsToIncrease.First().Position.SequenceColour;
                                    if (sequenceColour != null)
                                    {
                                        Int32 groupToIncreaseSequenceColor = sequenceColour.Value;
                                        if (!consecutiveBlocksToIgnore.Contains(groupToIncreaseSequenceColor))
                                        {
                                            consecutiveBlocksToIgnore.Add(groupToIncreaseSequenceColor);
                                        }
                                    }
                                }
                                else
                                {
                                    taskContext.UnitsWereIncreased = true;
                                }
                            }
                            else
                            {
                                taskContext.UnitsWereIncreased = 
                                    _planogramMerchandiser.MerchandiseInventoryChange(positionPlacementsGroupsToIncrease) || 
                                    taskContext.UnitsWereIncreased;
                            }

                            positionPlacementsGroupsToIncrease.Clear();
                        }

                        positionPlacementsToIncreaseCount++;

                        UpdateTargetUnits(taskContext, position);
                    }

                    if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos)
                    {
                        ValidateProductsBelowAverageDosIncrease(taskContext);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// If we're increasing inventory by facings, then we want to bring the target units
        /// dictionary up-to-date with the number of units we now have.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="position"></param>
        private static void UpdateTargetUnits(TaskContext taskContext, PlanogramPositionPlacement position)
        {
            if (taskContext.InventoryChange != InventoryChangeType.ByFacings) return;

            if (!position.Position.SequenceColour.HasValue) return;

            Dictionary<String, Int32> targetUnits;
            if (!taskContext.TargetUnitsBySequenceColour.TryGetValue(position.Position.SequenceColour.Value, out targetUnits)) return;

            if (position.Position.TotalUnits > targetUnits[position.Product.Gtin])
            {
                targetUnits[position.Product.Gtin] = position.Position.TotalUnits;
            }
        }

        /// <summary>
        /// validates ProductSelectionType.ProductsBelowAverageDos increase iteration being successful. 
        /// Success - some products were increased \ have been increased previously
        /// Failure - No products were increased and Average Achieved Dos does not require recalculating
        /// </summary>
        /// <param name="taskContext"></param>
        private static void ValidateProductsBelowAverageDosIncrease(TaskContext taskContext)
        {
            if (taskContext.ProcessingOrder == ProcessingOrderType.ByLowestDos)
            {
                CalculateAverageAchievedDos(taskContext);
                return;
            }

            if (taskContext.UnitsWereIncreased)
            {
                // we can recalculate DOS when all positions have achieved the initial average
                taskContext.RecalculateAverageAchievedDos = true;
            }
            else
            {
                if (taskContext.RecalculateAverageAchievedDos)
                {
                    // some products were increased to average but now nothing has been increased with the current selections that were below DOS, 
                    // need to recalculate to see if anymore positions that now meet the new plan average can be faced up.
                    CalculateAverageAchievedDos(taskContext);
                    taskContext.UnitsWereIncreased = true;
                }
                taskContext.RecalculateAverageAchievedDos = false;
            }
        }

        /// <summary>
        /// Calculates the current average achieved Dos
        /// </summary>
        /// <param name="taskContext"></param>
        private static void CalculateAverageAchievedDos(TaskContext taskContext)
        {
            if (!taskContext.PerformanceDataByProductId.Values.Any(p => p.AchievedDos.HasValue))
            {
                return;
            }

            taskContext.AverageAchievedDos = taskContext.PerformanceDataByProductId.Values
                .Where(p => p.AchievedDos.HasValue)
                .Average(p => p.AchievedDos.Value);
        }

        /// <summary>
        /// Increases the units for the <paramref name="placement"/>, returning true if successful and false if not.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="placement"></param>
        /// <returns></returns>
        private static void IncreaseUnitsForBlockInventory(TaskContext context, PlanogramPositionPlacement placement)
        {
            using (new CodePerformanceMetric()) 
            {
                //  If there are no starting units for this product,
                //  register the current units, this must be the first time
                //  that increase inventory is attempted on this product.
                context.LogStartingUnits(placement);
                
                // Try to find the target units dictionary for this block.
                Dictionary<String, Int32> blockUnits;
                if (placement.Position.SequenceColour == null ||
                    !context.BlockUnitsBySequenceColour.TryGetValue(placement.Position.SequenceColour.Value, out blockUnits))
                    return;

                switch (context.InventoryChange)
                {
                    case InventoryChangeType.ByUnits:
                        blockUnits[placement.Product.Gtin]++;
                        break;
                    case InventoryChangeType.ByFacings:
                // Only increment position within the block if it hasn't increased past its currently placed unit value
                if (blockUnits[placement.Product.Gtin] <= placement.Position.TotalUnits)
                {
                    blockUnits[placement.Product.Gtin]++;
                        }
                        break;
                    default:
                        blockUnits[placement.Product.Gtin]++;
                        break;
                }

                if (blockUnits[placement.Product.Gtin] <= placement.Position.TotalUnits) return;

                if (!context.CurrentGtinsIncreasedByBlockInventory.Contains(placement.Product.Gtin))
                {
                    context.CurrentGtinsIncreasedByBlockInventory.Add(placement.Product.Gtin);                        
                }
            }
        }

        private static Boolean LogCompletionAndInventoryChanges(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                Dictionary<String, String> productNameLookUpByGtin = context.Planogram.Products.ToDictionary(p => p.Gtin, p => p.Name);
                StringBuilder inventoryChanges = new StringBuilder();

                // Log the changes
                foreach (String productGtin in context.StartingUnits.Keys)
                {
                    //  Get the starting units,
                    Int32 startingUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.StartingUnits.TryGetValue(productGtin, out startingUnits)) continue;

                    //  Get the final units,
                    Int32 finalUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.FinalUnits.TryGetValue(productGtin, out finalUnits)) continue;

                    //  The increase had no effect.
                    if (startingUnits == finalUnits) continue;

                    inventoryChanges.AppendFormat(Message.TaskLogging_PlanogramProductInventoryIncreased,
                                                  productGtin, productNameLookUpByGtin[productGtin], startingUnits, finalUnits);
                }

                //  Log the change.
                context.LogInformation(
                    EventLogEvent.PlanogramProductInventoryIncreased,
                    context.ProductGtinsIncreased.Count,
                    inventoryChanges);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Ensure that the planogram is remerchandised
        /// </summary>
        private static void ReMerchandisePlanogram(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                context.RestoreSequenceNumbers();
    
                // remerchandise the planogram
                context.RemerchandisePlanogram();
            }
        }

        private String IsPercentageOfRankedProductsRequired()
        {
            using (new CodePerformanceMetric())
            {
                if (Parameters.Count <= (Int32)Parameter.ProductSelection || this.Parameters[Parameter.ProductSelection].Value?.Value1 == null)
                {
                    return Message.MerchandiseIncreaseProductInventory_ProductSelectionParameterDidNotExist;
                }

                ProductSelectionType productSelection;
                try
                {
                    productSelection = (ProductSelectionType)this.Parameters[Parameter.ProductSelection].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    return Message.MerchandiseIncreaseProductInventory_ProcessingOrderParameterDidNotExist;
                }

                if (productSelection == ProductSelectionType.TopRankedProducts)
                {
                    return null;
                }

                return Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProductsNotRequired;
            }
        }

        private String IsPercentageOfRankedProductsValid()
        {
            using (new CodePerformanceMetric())
            {
                if (!String.IsNullOrEmpty(IsPercentageOfRankedProductsRequired())) return null;

                if (Parameters.Count <= (Int32)Parameter.PercentageOfRankedProducts || this.Parameters[Parameter.PercentageOfRankedProducts].Value?.Value1 == null)
                {
                    return Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProductsParameterDidNotExist;
                }

                Single percentageOfRankedProducts;
                try
                {
                    percentageOfRankedProducts = Convert.ToSingle(this.Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                }
                catch (FormatException)
                {
                    return Message.MerchandiseIncreaseProductInventory_ValueWasNotAValidInteger;
                }

                if (percentageOfRankedProducts.GreaterThan(0))
                {
                    return null;
                }

                return Message.MerchandiseIncreaseProductInventory_PercentageOfRankedProductsInvalid;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters?[Parameter.CarParkComponentName] == null || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName) || this.Parameters[Parameter.CarParkComponentName].Value?.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}