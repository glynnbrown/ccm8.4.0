﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : D.Pleasance
//  Created.
// V8-32315 : D.Pleasance
//  Added TopRakedProductPercentageGtins
// V8-32358 : D.Pleasance
//  Added StartingUnits \ FinalUnits
// V8-32612 : D.Pleasance
//  Added GetSpaceConstraintType
// V8-32617 : D.Pleasance
//  Added AverageAchievedDos \ RecalculateAverageAchievedDos.
// V8-32742 : D.Pleasance
//  Added block inventory increase
// V8-32800 : D.Pleasance
//  Added CurrentGtinsIncreasedByBlockInventory \ BlockingGroupsByColour \ SequenceGroupsByColour
// V8-32982 : A.Kuszyk
//  Implemented IPlanogramMerchandiserContext.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1
{
    public class TaskContext : Ccm.Engine.TaskContext, IPlanogramMerchandiserContext
    {
        #region Constructors

        /// <summary>
        ///     Creates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            ProductGtinsIncreased = new List<String>();
            PositionsToIgnore = new List<String>();
            TopRakedProductPercentageGtins = new List<String>();
            StartingUnits = new Dictionary<String, Int32>();
            FinalUnits = new Dictionary<String, Int32>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// A dictionary of the Planogram Assortment ranks, keyed by Product Gtin.
        /// </summary>
        public Dictionary<String, Int32> RanksByProductGtin { get; set; }

        /// <summary>
        /// A dictionary of target unit dictionaries (units keyed by gtin), keyed by the sequence group colour they belong to.
        /// </summary>
        public Dictionary<Int32, Dictionary<String, Int32>> TargetUnitsBySequenceColour { get; set; }

        /// <summary>
        /// A dictionary of plogram blocking group unit dictionaries (units keyed by gtin), keyed by the sequence group colour they belong to.
        /// </summary>
        public Dictionary<Int32, Dictionary<String, Int32>> BlockUnitsBySequenceColour { get; set; }

        /// <summary>
        ///     Gets or sets the Blocking Strategy to use for the task.
        /// </summary>
        public PlanogramBlocking Blocking { get; set; }

        /// <summary>
        /// A dictionary of the Planogram's Performance Data, keyed by Product Id.
        /// </summary>
        public Dictionary<Object, PlanogramPerformanceData> PerformanceDataByProductId { get; set; }

        /// <summary>
        /// The order to select products in.
        /// </summary>
        public ProcessingOrderType ProcessingOrder { get; set; }

        /// <summary>
        /// The method to use when selecting products.
        /// </summary>
        public ProductSelectionType ProductSelection { get; set; }

        /// <summary>
        /// The method to use when determining space constraint for product increase.
        /// </summary>
        public SpaceConstraintType SpaceConstraint { get; set; }

        /// <summary>
        /// The percentage of ranked products to select, if specified by the user.
        /// </summary>
        public Single? PercentageOfRankedProducts { get; set; }

        /// <summary>
        /// The product gtins that had their units increased by the task.
        /// </summary>
        public List<String> ProductGtinsIncreased{ get; set; }

        /// <summary>
        /// Indicates if an increase to position units was possible.
        /// </summary>
        public Boolean UnitsWereIncreased { get; set; }

        /// <summary>
        /// The position placements for which no more increases are possible and that should
        /// be ignored in future increases.
        /// </summary>
        public List<String> PositionsToIgnore{ get; set; }

        /// <summary>
        /// The top ranked product gtins to reprocess
        /// </summary>
        public List<String> TopRakedProductPercentageGtins { get; set; }

        /// <summary>
        /// The <see cref="Planogram"/>'s merchandising groups.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }
                
        /// <summary>
        /// A dictionary of product stacks, keyed by their sequence group colour.
        /// </summary>
        public Dictionary<Int32, Stack<PlanogramProduct>> ProductStacksBySequenceColour { get; set; }

        /// <summary>
        /// The width of the blocking space in planogram units.
        /// </summary>
        public Single BlockingWidth { get; set; }

        /// <summary>
        /// The height of the blocking space in planogram units.
        /// </summary>
        public Single BlockingHeight { get; set; }

        /// <summary>
        /// The height offset of the blocking space in planogram units.
        /// </summary>
        public Single BlockingHeightOffset { get; set; }

        /// <summary>
        ///     Gets or sets the name for the car park component.
        /// </summary>
        public String CarParkComponentName { get; set; }

        public Dictionary<String, Int32> StartingUnits { get; private set; }
        public Dictionary<String, Int32> FinalUnits { get; private set; }

        /// <summary>
        /// gets \ sets the current achieved dos average
        /// </summary>
        public Single AverageAchievedDos { get; set; }
        
        /// <summary>
        /// Determines if average achieved dos needs to be recalculated after a successful increase
        /// </summary>
        public Boolean RecalculateAverageAchievedDos { get; set; }

        /// <summary>
        /// list of gtins that have increased from current unit allocation
        /// </summary>
        public List<String> CurrentGtinsIncreasedByBlockInventory { get; set; }

        /// <summary>
        /// Lookup of blocking groups by colour
        /// </summary>
        public Dictionary<Int32, PlanogramBlockingGroup> BlockingGroupsByColour { get; set; }
        
        /// <summary>
        /// Lookup of sequence groups by colour
        /// </summary>
        public Dictionary<Int32, PlanogramSequenceGroup> SequenceGroupsByColour { get; set; }

        /// <summary>
        /// Indicates whether the task should increase inventory by facings or by units.
        /// </summary>
        public InventoryChangeType InventoryChange { get; set; }

        PlanogramMerchandisingInventoryChangeType IPlanogramMerchandiserContext.InventoryChangeType
        {
            get { return InventoryChange.GetPlanogramMerchandisingInventoryChange(); }
        }

        #endregion

        #region Methods

        public PlanogramMerchandisingSpaceConstraintType GetSpaceConstraintType(SpaceConstraintType spaceConstraintType)
        {
            switch (spaceConstraintType)
            {
                case SpaceConstraintType.BlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;                    
                case SpaceConstraintType.BeyondBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace;
                case SpaceConstraintType.MinimumBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation;
            }

            return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
        }

        public void LogStartingUnits(PlanogramPositionPlacement placement)
        {
            LogStartingUnits(placement.Product.Gtin, placement.Position.TotalUnits);
        }

        public void LogStartingUnits(String gtin, Int32 units)
        {
            if (!StartingUnits.ContainsKey(gtin))
            {
                StartingUnits.Add(gtin, units);
            }

            if (FinalUnits.ContainsKey(gtin))
            {
                FinalUnits[gtin] = units;
            }
            else
            {
                FinalUnits.Add(gtin, units);
            }
        }

        public void LogFinalUnits(PlanogramPositionPlacement placement)
        {
            LogFinalUnits(placement.Product.Gtin, placement.Position.TotalUnits);
        }

        public void LogFinalUnits(String gtin, Int32 units)
        {
            FinalUnits[gtin] = units;

            if (!ProductGtinsIncreased.Contains(gtin))
            {
                ProductGtinsIncreased.Add(gtin);
            }
        }

        /// <summary>
        /// Disposes of this instance, disposing of the internal merchandising groups.
        /// </summary>
        protected override void OnDipose()
        {
            base.OnDipose();
            if (MerchandisingGroups != null) MerchandisingGroups.Dispose();
        }

        #endregion


        PlanogramMerchandisingSpaceConstraintType IPlanogramMerchandiserContext.SpaceConstraint
        {
            get { return GetSpaceConstraintType(SpaceConstraint); }
        }
    }
}