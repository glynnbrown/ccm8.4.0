﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
// V8-29597 : N.Foster
//  Code Review
#endregion
#endregion

using System;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationAttribute.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields
        private String _locationName;
        private String _locationCode;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// The location name
        /// </summary>
        public String LocationName
        {
            get { return _locationName; }
            set { _locationName = value; }
        }

        /// <summary>
        /// The Location code
        /// </summary>
        public String LocationCode
        {
            get { return _locationCode; }
            set { _locationCode = value; }
        }
        
        #endregion
    }
}