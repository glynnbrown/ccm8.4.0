﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29609 : D.Pleasance
//  Created.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationAttribute.v1
{
    /// <summary>
    /// Updates Location Attributes in the planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            LocationCode = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramLocationAttribute_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramLocationAttribute_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Location Code
            this.RegisterParameter(new TaskParameter(
                Parameter.LocationCode,
                Message.UpdatePlanogramLocationAttribute_LocationCode_Name,
                Message.UpdatePlanogramLocationAttribute_LocationCode_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.LocationCodeSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!DoesLocationExist(taskContext)) return;
                if (!UpdateLocationAttributes(taskContext)) return;
            }
        }
                
        #endregion

        #region Methods
        /// <summary>
        /// gets task parameters and validates
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if successful</returns>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (Parameters[Parameter.LocationCode].Value == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramLocationAttribute_LocationCode_Name,
                        Message.Error_NoValue);
                    return false;
                }

                taskContext.LocationName = Convert.ToString(Parameters[Parameter.LocationCode].Value.Value1);
                taskContext.LocationCode = Convert.ToString(Parameters[Parameter.LocationCode].Value.Value2);
                if (String.IsNullOrEmpty(taskContext.LocationCode))
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramLocationAttribute_LocationCode_Name,
                        String.Format("{0} {1}", taskContext.LocationCode, taskContext.LocationName));
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Validates that the provided location code is valid
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>If the location code is valid</returns>
        private static Boolean DoesLocationExist(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                LocationInfo locationInfo = null;
                try
                {
                    locationInfo = LocationInfoList
                        .FetchByEntityIdLocationCodes(taskContext.EntityId, new[] { taskContext.LocationCode })
                        .FirstOrDefault();
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw ex;
                }

                if (locationInfo == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramLocationAttribute_LocationCode_Name,
                        String.Format("{0} {1}", taskContext.LocationCode, taskContext.LocationName));
                    return false;
                }

                taskContext.LocationName = locationInfo.Name;
                return true;
            }
        }

        /// <summary>
        /// Update the planograms location attributes
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        private static Boolean UpdateLocationAttributes(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                String oldLocationCode = taskContext.Planogram.LocationCode;
                String oldLocationName = taskContext.Planogram.LocationName;

                taskContext.Planogram.LocationCode = taskContext.LocationCode;
                taskContext.Planogram.LocationName = taskContext.LocationName;

                taskContext.LogInformation(
                    EventLogEvent.PlanogramLocationUpdated, oldLocationCode, oldLocationName, taskContext.LocationCode, taskContext.LocationName);

                // return success
                return true;
            }
        }

        #endregion
    }
}