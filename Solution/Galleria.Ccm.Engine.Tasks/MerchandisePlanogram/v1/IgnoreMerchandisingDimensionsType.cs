﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30907 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1
{
    public enum IgnoreMerchandisingDimensionsType
    {
        /// <summary>
        /// Indicates that merchandising dimensions should be ignored when placing products.
        /// </summary>
        /// <remarks>
        /// Initilialized to 1, because Yes corresponds to the previous boolean value of true.
        /// </remarks>
        Yes = 1,
        /// <summary>
        /// Indicates that merchandising dimensions should be observed when placing products.
        /// </summary>
        /// <remarks>
        /// Initilialized to 0, because No corresponds to the previous boolean value of false.
        /// </remarks>
        No = 0,
    }

    public static class PlaceUnPlacedOnCarParkComponentTypeHelper
    {
        public static Dictionary<IgnoreMerchandisingDimensionsType, String> FriendlyNames =
            new Dictionary<IgnoreMerchandisingDimensionsType, String>()
            {
                { IgnoreMerchandisingDimensionsType.Yes, Message.Generic_Yes },
                { IgnoreMerchandisingDimensionsType.No, Message.Generic_No },
            };
    }
}
