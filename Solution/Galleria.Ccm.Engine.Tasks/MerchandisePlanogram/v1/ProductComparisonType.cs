﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820

// V8-30705 : A.Probyn
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1
{
    public enum ProductComparisonType
    {
        Rank,
        AssortmentRules

    }

    /// <summary>
    ///     Helpers to use the <see cref="ProductComparisonType"/> enumeration.
    /// </summary>
    public static class ProductComparisonTypeHelper
    {
        public static readonly Dictionary<ProductComparisonType, String> FriendlyNames =
            new Dictionary<ProductComparisonType, String>
            {
                {ProductComparisonType.Rank, Message.ProductComparisonType_Rank },
                {ProductComparisonType.AssortmentRules, Message.ProductComparisonType_AssortmentRules },
                
            };
    }
}