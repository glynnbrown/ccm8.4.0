﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810

// V8-29768 : A.Silva
//  Created.

#endregion
#region Version History: CCM830

// V8-31805 : D.Pleasance
//  Removed enum InventoryUnits.

#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1
{
    /// <summary>
    ///     Possible values for the Placement Units parameter in the Merchandise Planogram task.
    /// </summary>
    public enum PlacementUnitsType
    {
        SingleUnit = 0,
        AssortmentUnits = 1
    }

    /// <summary>
    ///     Helpers to use the <see cref="PlacementUnitsType"/> enumeration.
    /// </summary>
    public static class PlacementUnitsTypeHelper
    {
        public static readonly Dictionary<PlacementUnitsType, String> FriendlyNames =
            new Dictionary<PlacementUnitsType, String>
            {
                {PlacementUnitsType.SingleUnit, Message.PlacementUnitsType_SingleUnit },
                {PlacementUnitsType.AssortmentUnits, Message.PlacementUnitsType_AssortmentUnits }
            };
    }
}