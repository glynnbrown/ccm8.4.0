﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM802
// V8-29137 : A.Kuszyk
//  Created.
#endregion
#region Version History : CCM803
// V8-29694 : A.Silva
//  Refactored to use TaskContext.
// V8-29709 : A.Silva
//  Amended LayOutBlock so that it will keep trying until no better performing product is left unplaced.
//  Added drop worst and re add least worst
// V8-29420 : A.Silva
//  Amended GetProductsFromAssortment so that products come from an existing assortment.
// V8-29722 : A.Kuszyk
//  Amended TryProductPlacement to skip a product if it doesn't fit in an axis other than the placement axis.
#endregion
#region Version History : CCM810
// V8-29764 : A.Kuszyk
//  Removed MerchandisingStrategy parameter.
// V8-29709 : A.Silva
//  Amended ReAddLeastWorstPerformingProduct so that products re added that still do not get placed are ignored for the subsequent passes.
//  Amended ReAddLeastWorstPerformingProduct so that worse products are not re added if a better performing product cannot be inserted and the setting is Only Best Products.
// V8-29805 : A.Silva
//  Amended LayoutProduct and LayoutPlacement so that problematic first placements are skipped.
// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.
// V8-29768: A.Silva
//  Refactored UpdateSequenceData call as it is now in the base TaskContext.
//  Refactored PopulateLookups so that each bit is isolated.
// V8-29905 : A.Kuszyk
//  Fixed some bugs related to the IgnoreMerchandisingDimensions flag.
// V8-29772 : A.Silva
//  Added ReduceBlockSpace and refactored the layout placement code a bit.
// V8-29523 : A.Silva
//  Added logging.
// V8-30052 : A.Silva
//  Added First on Block to the task behavior.
// V8-30120 : A.Silva
//  Small amendment to account for overfilled when dealing with Hang From Bottom Components.
// V8-30103 : N.Foster
//  Autofill performance enhancements
#endregion
#region Version History : CCM820
// V8-30705 : A.Probyn
//  Updated RemoveWorstPerformingProduct to use new comparison type of AssortmentRules
// V8-30760 : D.Pleasance
//  Added PlacementOrder task parameter.
// V8-30907 : A.Kuszyk
//  Changed Ignore Merchandising Dimensions from boolean to Yes/No enum.
// V8-30990 : D.Pleasance
//  Amended default task parameter, IgnoreMerchandisingDimensionsType.Yes, ProductComparisonType.AssortmentRules
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
// V8-31243 : A.Probyn
//  ~ Extended RemoveWorstPerformingProduct further so that the current product being placed is also considered, and if its a lower rank,
//  and within the same set of assortment rules (to keep the priorities applied), this gets dropped instead.
//  ~ Extended RemoveWorstPerformingProduct to keep inline with the Rank comparison type, so that if a product is dropped, and there
//  are other ranked products, in the same assortment rule group, these also get dropped (logging as they go)
//  ~ Some general refactoring. 
// V8-31615 : A.Kuszyk
//  Added a call to PlanogramPositionPlacement.RecalculateUnits() following inventory changes.
#endregion
#region Version History : CCM830
// V8-31666 : A.Kuszyk
//  Corrected round error in GetAvailableFacingSpaceAsPercentage.
// V8-31677 : A.Kuszyk
//  Corrected issue in ReduceBlockSpace relating to an unexpected increase in the block's range.
// V8-31784 : A.Kuszyk
//  Re-factored layout code into PlanogramMerchandisingBlock.
// V8-31686 : A.Probyn
//  Updated to populate and utilise DroppedDuetoAssortmentRuleGtin in ReAddLeastWorstPerformingProduct
// V8-31805 : D.Pleasance
//  Removed enum PlacementUnitsType, InventoryUnits.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32504 : A.Kuszyk
//  Re-factored to utilise sequence sub groups.
// V8-32871 : A.Silva
//  Amended task so that missing products are not removed after adding them from the assortment. 
//  If they were, the assortment would have them removed too, and this task should not modify the products list.
// V8-32787 : A.Silva
//  Amended code to rank products so that Assortment Rule Priority is taken into account. Refactored to not use TaskContext.
// V8-33026 : A.Kuszyk
//  Ensured that merchandising group positions are cleared at the same time as planogram positions
// V8-33027 : D.Pleasance
//  Removed OnDispose(), merchandising group apply edits are committed at the end of the task process as part of ReMerchandisePlanogram()
// CCM-18507 : A.Silva
//  Log warning for skipped placements (if there are any). Log warning for skipped high rank products due to First on Block rule.
// CCM-18528 : A.Silva
//  Ignore Merchandising Dimensions parameter now defaults to No for Merchandise Planogram.
// CCM-18786 : M.Pettit
//  GenerateProductStack now allows products to be fit-tested using their position orientation rather than forcing a test based on Front0 orientation
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Fields

        /// <summary>
        ///     Gets or sets the Blocking Strategy to use for the task.
        /// </summary>
        private PlanogramBlocking _blocking;

        /// <summary>
        ///     Gets or sets the height of the current <see cref="Blocking" />.
        /// </summary>
        private Single _blockingHeight;

        /// <summary>
        ///     Gets or sets the height offset of the current <see cref="Blocking" />.
        /// </summary>
        private Single _blockingHeightOffset;

        /// <summary>
        ///     Gets or sets the width of the current <see cref="Blocking" />.
        /// </summary>
        private Single _blockingWidth;

        /// <summary>
        /// The products that have been skipped due to sequence sub-groups for the current block being merchandised.
        /// </summary>
        private readonly Stack<PlanogramProduct> _currentBlockSkippedSubGroupProducts = new Stack<PlanogramProduct>();

        /// <summary>
        ///     Gets or sets the current <see cref="PlanogramMerchandisingBlock"/> being processed.
        /// </summary>
        private PlanogramMerchandisingBlock _currentMerchandisingBlock;

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramProduct"/> currently being processed by the task.
        /// </summary>
        private PlanogramProduct _currentProduct;

        private Boolean _ignoreMerchandisingDimensions;

        private String _lastReAddedGtin;

        /// <summary>
        ///     The <see cref="PlanogramMerchandisingBlocks" /> for the current Planogram.
        /// </summary>
        private PlanogramMerchandisingBlocks _merchandisingBlocks;

        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        private PlanogramMerchandisingGroupList _merchandisingGroups;

        private PlacementUnitsType _placementUnits;

        /// <summary>
        ///     Gets or sets the list of products available to process.
        /// </summary>
        /// <remarks>This may include products that are not currently considered for placement.</remarks>
        private List<PlanogramProduct> _products;

        private ProductComparisonType _productComparisonType;

        /// <summary>
        ///     Gets or sets the order in which to place products.
        /// </summary>
        private PlacementOrderType _productPlacementOrder;

        /// <summary>
        ///     Dictionary lookup containing the rank for each GTIN.
        /// </summary>
        private readonly Dictionary<String, Int32> _rankByGtin = new Dictionary<String, Int32>();

        /// <summary>
        ///     Gets or sets a list of the subcomponents starting with Manual Merchandising Strategy on the X axis.
        /// </summary>
        private List<PlanogramSubComponent> _subComponentsWithManualX;

        /// <summary>
        ///     Gets or sets a list of the subcomponents starting with Manual Merchandising Strategy on the Y axis.
        /// </summary>
        private List<PlanogramSubComponent> _subComponentsWithManualY;

        /// <summary>
        ///     Gets or sets a list of the subcomponents starting with Manual Merchandising Strategy on the Z axis.
        /// </summary>
        private List<PlanogramSubComponent> _subComponentsWithManualZ;

        private readonly Dictionary<String, PlanogramAssortmentProduct> _assortmentProductsByGtin = new Dictionary<String, PlanogramAssortmentProduct>();

        /// <summary>
        ///     Gets the list of <see cref="PlanogramProduct" /> that will not fit on ANY component in its
        ///     <see cref="PlanogramMerchandisingBlock" />.
        /// </summary>
        /// <remarks>
        ///     Any <see cref="PlanogramProduct" /> in this list will be ignored when getting the
        ///     <see cref="_currentBlockProductStack" />.
        /// </remarks>
        private readonly HashSet<String> _tooLargeProductGtins = new HashSet<String>();

        /// <summary>
        ///     Gets the set of GTIN that were removed due to low performance.
        /// </summary>
        /// <remarks>
        ///     Any <see cref="PlanogramProduct" /> in this set will be ignored when getting the
        ///     <see cref="_currentBlockProductStack" />.
        /// </remarks>
        private readonly HashSet<String> _lowPerformanceGtin = new HashSet<String>();

        private readonly List<PlanogramProduct> _missingProducts = new List<PlanogramProduct>();

        /// <summary>
        ///     Gets or sets the collection of previously placed GTINs.
        /// </summary>
        /// <remarks>
        ///     Some steps of the task cannot get less products placed than the ones existing on this list.
        /// </remarks>
        private ICollection<String> _previouslyPlacedGtins;

        private readonly HashSet<String> _skippedTwice = new HashSet<String>();
        private readonly Dictionary<String, Int32> _targetUnitsByGtin = new Dictionary<String, Int32>();

        private readonly List<PlanogramAssortmentRuleType> _assortmentRuleTypesPriorityOrder =
            new List<PlanogramAssortmentRuleType>
            {
                PlanogramAssortmentRuleType.LocalLine,
                PlanogramAssortmentRuleType.Distribution,
                PlanogramAssortmentRuleType.Product,
                PlanogramAssortmentRuleType.Family,
                PlanogramAssortmentRuleType.Inheritance,
                PlanogramAssortmentRuleType.Normal,
                PlanogramAssortmentRuleType.Optional
            };

        #endregion

        #region Debug switches

        /// <summary>
        ///     Show debug info about removal of worst performing products.
        /// </summary>
        private const Boolean IsDebugRemoveWorstPerformingProduct = true;

        /// <summary>
        ///     Show debug info about re additions of least worst performing product.
        /// </summary>
        private const Boolean IsDebugReAddLeastWorstPerformingProduct = true;

        #endregion

        #region Version

        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number

        /// <summary>
        ///     Stack of <see cref="PlanogramProduct" /> that belong to the current
        ///     <see cref="PlanogramMerchandisingBlock" />.
        /// </summary>
        private readonly Stack<PlanogramProduct> _currentBlockProductStack = new Stack<PlanogramProduct>();

        private ITaskContext _taskContext;

        #endregion

        #region Parameter

        /// <summary>
        ///     Enumeration of parameter IDs for this task.
        /// </summary>
        private enum Parameter
        {
            PlacementUnits = 0,
            IgnoreMerchandisingDimensions = 1,
            ProductComparisonType = 2,
            PlacementOrder = 3
        }

        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.MerchandisePlanogram_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.MerchandisePlanogram_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration

        /// <summary>
        ///     Called when parameters are being registered for the task.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            RegisterParameter(NewPlacementUnitsParameter());
            RegisterParameter(NewIgnoreMerchandisingDimensionsParameter());
            RegisterParameter(NewProductComparisonTypeParameter());
            RegisterParameter(NewPlacementOrderParameter());
        }

        /// <summary>
        ///     Instantiates a new <see cref="TaskParameter"/> instance for the Product Units parameter.
        /// </summary>
        /// <returns>New instance of <see cref="TaskParameter"/>.</returns>
        private static TaskParameter NewPlacementUnitsParameter()
        {
            return new TaskParameter(
                id: Parameter.PlacementUnits,
                name: Message.MerchandisePlanogram_PlacementUnitsType_Name,
                description: Message.MerchandisePlanogram_PlacementUnitsType_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(PlacementUnitsType),
                defaultValue: PlacementUnitsType.AssortmentUnits,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false);
        }

        /// <summary>
        ///     Instantiates a new <see cref="TaskParameter"/> instance for the Ignore Merchandising Dimensions parameter.
        /// </summary>
        /// <returns>New instance of <see cref="TaskParameter"/>.</returns>
        private static TaskParameter NewIgnoreMerchandisingDimensionsParameter()
        {
            return new TaskParameter(
                id: Parameter.IgnoreMerchandisingDimensions,
                name: Message.MerchandisePlanogram_IgnoreMerchandisingDimensions_Name,
                description: Message.MerchandisePlanogram_IgnoreMerchandisingDimensions_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(IgnoreMerchandisingDimensionsType),
                defaultValue: IgnoreMerchandisingDimensionsType.No,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false);
        }

        /// <summary>
        ///     Instantiates a new <see cref="TaskParameter"/> instance for the Ignore Merchandising Dimensions parameter.
        /// </summary>
        /// <returns>New instance of <see cref="TaskParameter"/>.</returns>
        private static TaskParameter NewProductComparisonTypeParameter()
        {
            return new TaskParameter(
                id: Parameter.ProductComparisonType,
                name: Message.MerchandisePlanogram_ProductComparisonType_Name,
                description: Message.MerchandisePlanogram_ProductComparisonType_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(ProductComparisonType),
                defaultValue: ProductComparisonType.AssortmentRules,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false);
        }

        /// <summary>
        ///     Instantiates a new <see cref="TaskParameter"/> instance for the placement order parameter.
        /// </summary>
        /// <returns>New instance of <see cref="TaskParameter"/>.</returns>
        private static TaskParameter NewPlacementOrderParameter()
        {
            return new TaskParameter(
                id: Parameter.PlacementOrder,
                name: Message.MerchandisePlanogram_PlacementOrder_Name,
                description: Message.MerchandisePlanogram_PlacementOrder_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(PlacementOrderType),
                defaultValue: PlacementOrderType.RangedInRankOrder,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false);
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            if (context == null) return;
            if (_taskContext != null)
                System.Diagnostics.Debug.Fail(
                    "Tasks should be executed only once per instance and this instance was executed already. The Task may have previous values hanging around.");

            using (new CodePerformanceMetric()) 
            {
                _taskContext = context;
                if (!ValidateParameters()) return;
                if (!ValidatePlanogram()) return;
                if (!PopulateLookups()) return;
                if (!SetManualMerchandisingStrategiesToAutomatedPreference()) return;
                if (!CreateMerchandisingBlocks()) return;
                if (!RemoveAllPositions()) return;
                if (!LayOutPlanogram()) return;
                LogUnplacedMissingProducts();
                if (!ResetManualMerchandisingStrategies()) return;
                ReMerchandisePlanogram();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Validate the parameters for this instance.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext" /> to store the validated parameters in.</param>
        /// <returns><c>True</c> if all the parameters validate correctly; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     <list type="bullet">
        ///         <listheader>
        ///             <term>Events Logged</term>
        ///         </listheader>
        ///         <item>
        ///             <term>Error: Invalid Task Parameter, <c>Placement Units</c></term>
        ///             <description>The parameter is missing or has an invalid value.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Invalid Task Parameter, <c>Ignore Merchandising Dimensions</c></term>
        ///             <description>The parameter is missing or has an invalid value.</description>
        ///         </item>
        ///     </list>
        /// </remarks>
        private Boolean ValidateParameters()
        {
            using (new CodePerformanceMetric()) 
            {
                var success = true;

                #region PlacementUnits

                // Validate that the Product Units paramter exists.
                if (Parameters[Parameter.PlacementUnits].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                           Message.MerchandisePlanogram_PlacementUnitsType_Name,
                                                           Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _placementUnits = (PlacementUnitsType)Parameters[Parameter.PlacementUnits].Value.Value1;
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandisePlanogram_PlacementUnitsType_Name, Parameters[Parameter.PlacementUnits].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region IgnoreMerchandisingDimensions

                // Validate that the Ignore Merchandising Dimensions exists.
                if (Parameters[Parameter.IgnoreMerchandisingDimensions].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                           Message.MerchandisePlanogram_IgnoreMerchandisingDimensions_Name,
                                                           Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        IgnoreMerchandisingDimensionsType ignore = 
                            (IgnoreMerchandisingDimensionsType)Convert.ToByte(Parameters[Parameter.IgnoreMerchandisingDimensions].Value.Value1);
                        _ignoreMerchandisingDimensions = ignore == IgnoreMerchandisingDimensionsType.Yes;
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                               Message.MerchandisePlanogram_IgnoreMerchandisingDimensions_Name,
                                                               Parameters[Parameter.IgnoreMerchandisingDimensions].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region ProductComparisonType

                // Validate that the Product Comparison Type exists.
                if (Parameters[Parameter.ProductComparisonType].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                           Message.MerchandisePlanogram_ProductComparisonType_Name,
                                                           Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _productComparisonType = (ProductComparisonType)Parameters[Parameter.ProductComparisonType].Value.Value1;
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                               Message.MerchandisePlanogram_ProductComparisonType_Name,
                                                               Parameters[Parameter.ProductComparisonType].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Placement Order

                //  Validate that the placement Order parameter exists.
                if (Parameters[Parameter.PlacementOrder].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                           Message.MerchandisePlanogram_PlacementOrder_Name,
                                                           Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _productPlacementOrder = (PlacementOrderType)Parameters[Parameter.PlacementOrder].Value.Value1;
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                                               Message.MerchandisePlanogram_PlacementOrder_Name,
                                                               Parameters[Parameter.PlacementOrder].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        ///     Validates the contents of the task's <c>Planogram</c> before running the task.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to store the task state.</param>
        /// <returns><c>True</c> if the <c>Planogram</c> has everything required by the task; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     <list type="bullet">
        ///         <listheader>
        ///             <term>Events Logged</term>
        ///         </listheader>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, Missing Assortment.</term>
        ///             <description>The planogram has no Assortment.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, Missing Assortment Products.</term>
        ///             <description>The planogram has no Assortment Products.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, No Ranged Assortment Products.</term>
        ///             <description>The planogram has no Ranged Assortment products to process.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, Missing Blocking Strategy.</term>
        ///             <description>The planogram has no Blocking Strategy.</description>
        ///         </item>
        ///     </list>
        /// </remarks>
        private Boolean ValidatePlanogram()
        {
            using (new CodePerformanceMetric()) 
            {
                Planogram planogram = _taskContext.Planogram;

                #region Validate Assortment

                //  Verify there is an assortment present.
                if (_taskContext.Planogram.Assortment == null)
                {
                    _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                                           Message.TaskLogging_Assortment,
                                                           Message.TaskLogging_NoAssortmentWasPresent);
                    return false;
                }

                //  Verify the assortment contains products.
                if (_taskContext.Planogram.Assortment.Products == null ||
                    !_taskContext.Planogram.Assortment.Products.Any())
                {
                    _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                                           Message.TaskLogging_AssortmentProducts,
                                                           Message.TaskLogging_NoAssortmentProductsListWasPresent);
                    return false;
                }

                //  Verify at least one assortment product is ranged.
                if (_productPlacementOrder == PlacementOrderType.RangedInRankOrder && !_taskContext.Planogram.Assortment.Products.Any(p => p.IsRanged))
                {
                    _taskContext.LogInformation(EventLogEvent.NothingForTaskToDo,
                                                                 Message.TaskLogging_NoProductsWerePresentInThePlanogramAssortment);
                    return false;
                }

                #endregion

                #region Validate Blocking Strategy

                //  Get the last Blocking strategy in the planogram.
                PlanogramBlockingList blockingStrategies = planogram.Blocking;
                _blocking = blockingStrategies.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                            blockingStrategies.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ??
                            blockingStrategies.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial);
                if (_blocking != null)
                {
                    Single width;
                    Single height;
                    Single offset;
                    planogram.GetBlockingAreaSize(out height, out width, out offset);
                    _blockingWidth = width;
                    _blockingHeight = height;
                    _blockingHeightOffset = offset;
                }
                else
                {
                    _taskContext.LogWarning(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoBlockingStrategyWasPresent);
                    return false;
                }

                #endregion

                return true;
            }
        }

        #region Populate Lookups

        /// <summary>
        ///     Populate the <see cref="TaskContext"/> lookups for different data.
        /// </summary>
        /// <param name="context"></param>
        private Boolean PopulateLookups()
        {
            var assortmentProducts = new List<PlanogramAssortmentProduct>();

            switch (_productPlacementOrder)
            {
                case PlacementOrderType.RangedInRankOrder:
                    //  Get the assortment products GTINs that are ranged, ordered by rank and then GTIN.
                    assortmentProducts =
                        _taskContext.Planogram.Assortment.Products.Where(p => p.IsRanged && p.IsAvailableToPlace(_taskContext.Planogram.LocationCode)).OrderBy(p => p.Rank).ThenBy(p => p.Gtin).ToList();
                    break;
                case PlacementOrderType.NotRangedInRankOrder:
                    //  Get the assortment products GTINs that are NOT ranged, ordered by rank and then GTIN.
                    assortmentProducts =
                        _taskContext.Planogram.Assortment.Products.Where(p => !p.IsRanged && p.IsAvailableToPlace(_taskContext.Planogram.LocationCode)).OrderBy(p => p.Rank).ThenBy(p => p.Gtin).ToList();
                    break;
                case PlacementOrderType.InAssortmentRankOrder:
                    //  Get the assortment products GTINs ordered by rank and then GTIN.
                    assortmentProducts =
                        _taskContext.Planogram.Assortment.Products.Where(p => p.IsAvailableToPlace(_taskContext.Planogram.LocationCode)).OrderBy(p => p.Rank).ThenBy(p => p.Gtin).ToList();
                    break;
                default:
                    break;
            }
            
            //  Log the number of ranged products that will be processed.
            _taskContext.LogInformation(EventLogEvent.PlanogramProductsToInsert, assortmentProducts.Count);

            LookupProductsToInsert(assortmentProducts);
            LookupRankByGtin(assortmentProducts);
            LookupTargetUnitsByGtin(assortmentProducts);

            return true;
        }

        /// <summary>
        ///     Lookup each ranked assortment product by its GTIN.
        /// </summary>
        /// <param name="assortmentProducts"></param>
        private void LookupRankByGtin(IEnumerable<PlanogramAssortmentProduct> assortmentProducts)
        {
            IEnumerable<String> rankedAssortmentProductGtins =
                _productComparisonType == ProductComparisonType.AssortmentRules
                    ? assortmentProducts.OrderBy(product => _taskContext.Planogram.Assortment.GetAssortmentRulePriorityRank(product, _assortmentRuleTypesPriorityOrder))
                                        .ThenBy(product => product.Rank)
                                        .Select(product => product.Gtin)
                    : assortmentProducts.OrderBy(product => product.Rank)
                                        .Select(product => product.Gtin);
            var rankCount = 0;
            foreach (String gtin in rankedAssortmentProductGtins)
            {
                _rankByGtin.Add(gtin, ++rankCount);
            }
        }

        /// <summary>
        ///     Lookup missing and to insert products.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="assortmentProducts"></param>
        /// <remarks>
        ///     Any missing product from the planogram will be added by default 
        ///     to the Planogram's Product List in case it is placed.
        /// </remarks>
        private void LookupProductsToInsert(IEnumerable<PlanogramAssortmentProduct> assortmentProducts)
        {
            ICollection<String> gtinsToInsert = assortmentProducts.Select(p => p.Gtin).ToList();

            //  Get the products missing from the planogram.
            IEnumerable<PlanogramProduct> existingProducts = _taskContext.Planogram.Products.Where(p => gtinsToInsert.Any(gtin => p.Gtin == gtin)).ToList();
            IEnumerable<String> missingGtins = gtinsToInsert.Except(existingProducts.Select(p => p.Gtin)).ToList();
            if (missingGtins.Any())
            {
                _missingProducts.AddRange(ProductList.FetchByEntityIdProductGtins(_taskContext.EntityId, missingGtins).Select(PlanogramProduct.NewPlanogramProduct));
                existingProducts = existingProducts.Union(_missingProducts);

                //  Add any missing products into the planogram products.
                _taskContext.Planogram.Products.AddRange(_missingProducts);
            }

            //  Get the products that should be merchandised.
            _products = gtinsToInsert.Select(gtin => existingProducts.First(p => p.Gtin == gtin)).ToList();
        }

        /// <summary>
        ///     Lookup the target units to place by GTIN.
        /// </summary>
        /// <param name="assortmentProducts"></param>
        private void LookupTargetUnitsByGtin(IEnumerable<PlanogramAssortmentProduct> assortmentProducts)
        {
            // Lookup each GTIN target units.
            switch (_placementUnits)
            {
                case PlacementUnitsType.SingleUnit:
                    break;
                case PlacementUnitsType.AssortmentUnits:
                    foreach (PlanogramAssortmentProduct product in assortmentProducts)
                    {
                        _targetUnitsByGtin.Add(product.Gtin, product.Units);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion

        /// <summary>
        ///     Removes all pre-existing positions,
        ///     logging how many there were originally.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to log original positions information in.</param>
        private Boolean RemoveAllPositions()
        {
            if (_taskContext == null) return false;

            using (new CodePerformanceMetric())
            {
                _taskContext.Planogram.Positions.Clear();

                foreach (PlanogramMerchandisingGroup merchandisingGroup in _merchandisingGroups)
                {
                    merchandisingGroup.PositionPlacements.Clear();
                }

                return true;
            }
        }

        /// <summary>
        ///     Sets any manual merchandising strategies per axis on the existing subcomponents to a strategy that the task will be able to use.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to store the original Merchandising Strategy per axis in.</param>
        /// <remarks>The subcomponents changed are stored in the context, so that they can eventually be restored to Manual.</remarks>
        private Boolean SetManualMerchandisingStrategiesToAutomatedPreference()
        {
            using (new CodePerformanceMetric())
            {
                //  Find and store the subcomponents having an initial manual merchandising strategy on any axis.
                IEnumerable<PlanogramSubComponent> subComponents = _taskContext.Planogram.Components.SelectMany(c => c.SubComponents).ToList();
                _subComponentsWithManualX = subComponents.Where(s => s.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Manual).ToList();
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualX)
                {
                    subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
                }

                _subComponentsWithManualY = subComponents.Where(s => s.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Manual).ToList();
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualY)
                {
                    subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
                }

                _subComponentsWithManualZ = subComponents.Where(s => s.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Manual).ToList();
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualZ)
                {
                    subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;
                }

                return true;
            }
        }

        private Boolean CreateMerchandisingBlocks()
        {
            using (new CodePerformanceMetric())
            {
                //  Create the merchandising group in the context... it will be disposed when the task is disposed itself.
                _merchandisingGroups = _taskContext.Planogram.GetMerchandisingGroups();

                // First, get the merchandising blocks and their block placements.
                _merchandisingBlocks = new PlanogramMerchandisingBlocks(_blocking, _taskContext.Planogram.Sequence, _merchandisingGroups, _blockingWidth, _blockingHeight, _blockingHeightOffset, assortmentRuleEnforcer: AssortmentRuleEnforcer);

                return true;
            }
        }

        #region Lay Out Planogram

        /// <summary>
        ///     Lay out each <see cref="PlanogramMerchandisingBlock"/> in the Planogram trying to place 
        ///     its corresponding products while there is space for them, in sequence order.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to layout products from.</param>
        /// <remarks>
        ///     After each <see cref="PlanogramMerchandisingBlock"/>'s placements have been laid out,
        ///     if there are still better performing products left unplaced, the block will be relaid out
        ///     leaving out the worst performing product.</remarks>
        private Boolean LayOutPlanogram()
        {
            using (new CodePerformanceMetric())
            {
                foreach (PlanogramMerchandisingBlock merchandisingBlock in _merchandisingBlocks)
                {
                    _currentMerchandisingBlock = merchandisingBlock;
                    GenerateProductStack(_currentBlockProductStack);
                    _previouslyPlacedGtins = new List<String>();

                    IPlanogramMerchandisingLayoutContext<ITaskContext> layoutContext = GetLayoutContext();
                    merchandisingBlock.Layout(layoutContext);
                    ICollection<PlanogramMerchandisingBlockPlacement> skippedPlacements = layoutContext.SkippedBlockPlacement;
                    if (skippedPlacements.Any()) LogComponentsWithoutRepresentationForBlock(merchandisingBlock.BlockingGroup.Name, skippedPlacements);
                    ICollection<String> skippedFirstOnBlock = layoutContext.SkippedFirstOnBlock;
                    if (!skippedFirstOnBlock.Any()) continue;

                    //  Log any skipped products for the block.
                    Dictionary<String, String> productNameByGtin = _taskContext.Planogram.Products.ToLookupDictionary(product => product.Gtin, product => product.Name);
                    foreach (String gtin in skippedFirstOnBlock)
                    {
                        String productName;
                        productNameByGtin.TryGetValue(gtin, out productName);
                        _taskContext.LogWarning(EventLogEvent.PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize, gtin, productName);
                    }
                }

                ApplyModifiedMerchandisingGroups();
                return true;
            }
        }

        private void LogComponentsWithoutRepresentationForBlock(String blockName, IEnumerable<PlanogramMerchandisingBlockPlacement> skippedPlacements)
        {
            IEnumerable<String> skippedComponentNames = 
                skippedPlacements.Select(placement => placement.MerchandisingGroup.SubComponentPlacements.First().Component.Name);
            foreach (String componentName in skippedComponentNames)
            {
                _taskContext.LogWarning(EventLogEvent.PlanogramComponentFailedToGetRepresentationForBlock, blockName, componentName);
            }
        }

        private void ApplyModifiedMerchandisingGroups()
        {
            IEnumerable<PlanogramMerchandisingGroup> modifiedMerchandisingGroups = _merchandisingGroups.Where(merchandisingGroup => merchandisingGroup.EditLevel > 0);
            foreach (PlanogramMerchandisingGroup merchandisingGroup in modifiedMerchandisingGroups)
            {
                merchandisingGroup.ApplyEdit();
            }
        }

        private IPlanogramMerchandisingLayoutContext<ITaskContext> GetLayoutContext()
        {
            var postLayoutMethods = new List<Func<ITaskContext, Boolean>>
                                    {
                                        RemoveWorstPerformingProduct, ReAddLeastWorstPerformingProduct,
                                    };

            IPlanogramMerchandisingLayoutContext<ITaskContext> layoutContext = new PlanogramMerchandisingLayoutContext<ITaskContext>(_currentBlockProductStack.Reverse().ToDictionary(p => p.Gtin, p =>
                                                                                                                                                                                                   {
                                                                                                                                                                                                       Int32 units;
                                                                                                                                                                                                       return _targetUnitsByGtin.TryGetValue(p.Gtin, out units) ? units : 1;
                                                                                                                                                                                                   }), _currentBlockProductStack, GenerateProductStack, _taskContext.Planogram, true, _taskContext, postLayoutMethods, _ignoreMerchandisingDimensions, _previouslyPlacedGtins, _rankByGtin, _currentBlockSkippedSubGroupProducts);
            return layoutContext;
        }

        private Boolean RemoveWorstPerformingProduct(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                //  Nothing more to do if there are no left over products.
                if (!(_currentBlockProductStack.Any() || _currentBlockSkippedSubGroupProducts.Any())) return false;

                System.Diagnostics.Debug.WriteLineIf(IsDebugRemoveWorstPerformingProduct, "--");
                System.Diagnostics.Debug.WriteLineIf(IsDebugRemoveWorstPerformingProduct, "Debugging RemoveWorstPerformingProduct: Remove worst performing product...");

                //  Get the rank of the product that could not fit.
                _currentProduct = _currentBlockProductStack.Any() ? _currentBlockProductStack.Pop() : _currentBlockSkippedSubGroupProducts.Pop();
                System.Diagnostics.Debug.WriteLineIf(IsDebugRemoveWorstPerformingProduct, String.Format("Could not fit: {0}({1})", _rankByGtin[_currentProduct.Gtin], _currentProduct.Gtin));

                IEnumerable<String> droppedGtins = RemoveProductsUsingRank();

                System.Diagnostics.Debug.WriteLineIf(IsDebugRemoveWorstPerformingProduct, String.Format("Dropped products: {0}", String.Join(",", droppedGtins.Select(gtin => String.Format("{0}({1})", _rankByGtin[gtin], gtin)))));
                return true;
            }
        }

        private IEnumerable<String> RemoveProductsUsingRank()
        {
            //  Get the rank of the worst placed product if there are any.
            PlanogramPositionPlacement positionPlacement = _currentMerchandisingBlock.BlockPlacements.SelectMany(p => p.PositionPlacements).OrderBy(p => _rankByGtin[p.Product.Gtin]).LastOrDefault();

            Int32 worstPlacedRank = GetWorstPlacedRank(positionPlacement);

            //  Drop any product with this rank or worse.
            Int32 worstRank = Math.Max(_rankByGtin[_currentProduct.Gtin], worstPlacedRank);

            var droppedGtins = new List<String>();
            foreach (String gtin in _currentMerchandisingBlock.SequenceGroup.Products.Select(p => p.Gtin))
            {
                Int32 rank;
                if (!_rankByGtin.TryGetValue(gtin, out rank) ||
                    rank < worstRank ||
                    _lowPerformanceGtin.Contains(gtin)) continue;

                _lowPerformanceGtin.Add(gtin);
                droppedGtins.Add(gtin);

                PlanogramAssortmentProduct product;
                _assortmentProductsByGtin.TryGetValue(gtin, out product);
                Boolean isDroppedForAssortmentRulesRank = product != null && product.Rank < rank;
                if (isDroppedForAssortmentRulesRank)
                {
                    LogDroppedAssortmentRuleProduct(gtin, _taskContext.Planogram.Assortment.GetAssortmentRuleType(product, _assortmentRuleTypesPriorityOrder));
                }
            }

            return droppedGtins;
        }

        private Int32 GetWorstPlacedRank(PlanogramPositionPlacement positionPlacement)
        {
            Int32 worstPlacedRank;
            if (positionPlacement != null)
            {
                String worstPerformingPlacedProductGtin = positionPlacement.Product.Gtin;
                worstPlacedRank = _rankByGtin[worstPerformingPlacedProductGtin];
            }
            else
            {
                IEnumerable<PlanogramSequenceGroupProduct> availableSequenceProducts = _currentMerchandisingBlock.SequenceGroup.Products.Where(p => !_lowPerformanceGtin.Contains(p.Gtin)).ToList();
                if (availableSequenceProducts.Any())
                {
                    worstPlacedRank = availableSequenceProducts.Max(p =>
                                                                    {
                                                                        Int32 rank;
                                                                        return !_rankByGtin.TryGetValue(p.Gtin, out rank) ? 0 : rank;
                                                                    });
                }
                else
                    worstPlacedRank = 0;
            }
            return worstPlacedRank;
        }

        private void LogDroppedAssortmentRuleProduct(String gtinToDrop, PlanogramAssortmentRuleType ruleType)
        {
            //Log warning to planogram events
            switch (ruleType)
            {
                case PlanogramAssortmentRuleType.Optional:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToAssortmentOptionalProductRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.Normal:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToAssortmentNormalProductRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.Inheritance:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToAssortmentInheritanceProductRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.Family:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToFamilyRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.Product:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToProductRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.Distribution:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToAssortmentDistributionProductRule, gtinToDrop);
                    break;
                case PlanogramAssortmentRuleType.LocalLine:
                    _taskContext.LogInformation(EventLogEvent.PlanogramProductDelistedDueToLocalProductRule, gtinToDrop);
                    break;
                default:
                    break;
            }
        }

        private Boolean ReAddLeastWorstPerformingProduct(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                //  Nothing more to do if there are left over products.
                if (_currentBlockProductStack.Count > 0) return false;

                System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "--");
                System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "Debugging ReAddLeastWorstPerformingProduct: Re add least worst performing product...");

                //  If the previous placement has lost some of its Gtins
                //  Add the next product to the SkippedTwice list and move on to the next.
                ICollection<String> previouslyPlacedGtins = _currentMerchandisingBlock.BlockPlacements.SelectMany(p => p.PositionPlacements).Select(pp => pp.Product.Gtin).ToList();
                if (!String.IsNullOrEmpty(_lastReAddedGtin) && !previouslyPlacedGtins.Contains(_lastReAddedGtin) ||
                    (!CollectionsAreEqual(_previouslyPlacedGtins, previouslyPlacedGtins)))
                {
                    //  the previous product could not be placed again, 
                    //  or removed a better performing product when re added
                    //  so skip it again and do not consider it more.
                    _skippedTwice.Add(_lastReAddedGtin);
                    _lowPerformanceGtin.Add(_lastReAddedGtin);

                    //  Stop re adding if we drop a product and we cannot skip assortment ranks for place products (exclusively best performing products).
                    System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "Last best product could not be added and worse products are not allowed.");
                    System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "--");

                    return false; // No products to re add so nothing to do.
                }

                //  The last re added product got placed without removing previously placed ones
                //  so keep that list and continue re adding.
                _previouslyPlacedGtins = previouslyPlacedGtins;

                //  Get all the gtins that should appear in the current block
                //  except the too large or the skipped twice.
                ICollection<String> currentBlockGtins = _currentMerchandisingBlock.SequenceGroup.Products.Select(p => p.Gtin).Except(_tooLargeProductGtins).Except(_skippedTwice).ToList();

                //  Get the gtins that should appear but were dropped due to poor performance.
                ICollection<String> lowPerformanceGtin = _lowPerformanceGtin.Where(gtin => currentBlockGtins.Contains(gtin)).ToList();
                if (lowPerformanceGtin.Count == 0)
                {
                    System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "No more products to re add.");
                    System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, "--");

                    return false; // No products to re add so nothing to do.
                }

                //  Re add the best leftout product and try again.
                _lastReAddedGtin = lowPerformanceGtin.OrderBy(gtin => _rankByGtin[gtin]).First();
                _lowPerformanceGtin.Remove(_lastReAddedGtin);

                System.Diagnostics.Debug.WriteLineIf(IsDebugReAddLeastWorstPerformingProduct, String.Format("Try re adding: {0}({1})", _rankByGtin[_lastReAddedGtin], _lastReAddedGtin));

                //  Return success.
                return true;
            }
        }

        private static Boolean CollectionsAreEqual(ICollection<String> previouslyPlacedGtins, ICollection<String> placedGtins)
        {
            using (new CodePerformanceMetric())
            {
                return placedGtins.Count == previouslyPlacedGtins.Count && placedGtins.OrderBy(gtin => gtin).Zip(previouslyPlacedGtins.OrderBy(gtin => gtin), String.Equals).All(b => b);
            }
        }

        #endregion

        /// <summary>
        ///     Log total number of unplaced products at the end of the task execution.
        /// </summary>
        /// <remarks>
        ///     <list type="bullet">
        ///         <listheader>
        ///             <term>Information logged</term>
        ///             <description>Calling this method will result in the following items being logged.</description>
        ///         </listheader>
        ///         <item>
        ///             <term>Products not placed.</term>
        ///             <description>Total count of products that were ranged but could not be merchandised.</description>
        ///         </item>
        ///         <item>
        ///             <term>Products not added.</term>
        ///             <description>
        ///                 Total count of products that were ranged but were not merchandised, nor added to the
        ///                 <c>Planogram's Product List</c>.
        ///             </description>
        ///         </item>
        ///     </list>
        /// </remarks>
        private void LogUnplacedMissingProducts()
        {
            ICollection<String> placedGtins =
                _taskContext.Planogram.Positions.Select(position => position.GetPlanogramProduct().Gtin).Distinct().ToList();
            ICollection<PlanogramProduct> unplacedProducts =
                _products.Where(product => placedGtins.All(gtin => product.Gtin != gtin)).ToList();
            ICollection<PlanogramProduct> unplacedMissingProducts =
                _missingProducts.Where(product => placedGtins.All(gtin => product.Gtin != gtin)).ToList();

            //  Log how many ranged products were not Merchandised into Planogram, if nay.
            if (unplacedProducts.Count > 0)
                _taskContext.LogInformation(EventLogEvent.PlanogramPositionsNotInserted, unplacedProducts.Count);

            //  Log how many products were not added to the Planogram's Product List, if nay.
            if (unplacedMissingProducts.Count > 0)
                _taskContext.LogInformation(EventLogEvent.PlanogramProductsNotPlaced, unplacedMissingProducts.Count);
        }

        /// <summary>
        ///     Resets any manual merchandising strategies per axis on the existing subcomponents that were changed to an automation preference.
        /// </summary>
        private Boolean ResetManualMerchandisingStrategies()
        {
            using (new CodePerformanceMetric())
            {
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualX)
                {
                    subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                }
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualY)
                {
                    subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
                }
                foreach (PlanogramSubComponent subComponent in _subComponentsWithManualZ)
                {
                    subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
                }

                return true;
            }
        }

        /// <summary>
        /// Re-merchandises the planogram, ensuring
        /// that autofill is applied to the final plan
        /// </summary>
        private void ReMerchandisePlanogram()
        {
            using (new CodePerformanceMetric())
            {
                if (_merchandisingGroups != null)
                {
                    IEnumerable<PlanogramMerchandisingGroup> merchGroupsWithPendingChanges = _merchandisingGroups.Where(merchandisingGroup => merchandisingGroup.EditLevel > 0);
                    foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroupsWithPendingChanges)
                    {
                        merchandisingGroup.ApplyEdit();
                    }
                    _merchandisingGroups.Dispose();
                    _merchandisingGroups = null;
                }

                // remerchandise the planogram
                var context = _taskContext as TaskContext;
                if (context != null) context.RemerchandisePlanogram();
            }
        }

        #endregion

        /// <summary>
        /// Generates the stack of products that need to be placed.
        /// </summary>
        /// <param name="productStack">The stack to clear and re-stack.</param>
        private void GenerateProductStack(Stack<PlanogramProduct> productStack)
        {
            //  Clear any previous product stack.
            productStack.Clear();

            //  Get the products from the starting list 
            //  that appear the sequence for the current block
            //  and in sequence order.
            IEnumerable<PlanogramMerchandisingBlockPlacement> blockPlacements = _currentMerchandisingBlock.BlockPlacements.ToList();
            List<PlanogramProduct> productsToInsertInReversedSequenceOrder = _currentMerchandisingBlock.SequenceGroup.Products.Where(sp => !_lowPerformanceGtin.Contains(sp.Gtin) && !_tooLargeProductGtins.Contains(sp.Gtin)).OrderByDescending(sp => sp.SequenceNumber).Select(sp => _products.FirstOrDefault(p => p.Gtin == sp.Gtin)).Where(p => p != null).ToList();

            //  The list of products is sorted in reverse so that
            //  the stack pops them in the right order.
            foreach (PlanogramProduct product in productsToInsertInReversedSequenceOrder)
            {
                //find the orientation of the position related to the product if any. if none, just use the default
                PlanogramMerchandisingPositionPresentationState state = null;
                PlanogramPositionOrientationType orientation = PlanogramPositionOrientationType.Front0;
                if (_currentMerchandisingBlock.PositionsPresentationStates.TryGetValue(product.Gtin, out state))
                {
                    orientation = state.OrientationType;
                }

                //  Add the product to the stack only if it fits inside at least one placement.
                //  Otherwise, mark it as unfittable.
                if (_ignoreMerchandisingDimensions || 
                    blockPlacements
                        .Any(bp => product.ToPositionOrientedSize(orientation)
                        .FitsInside(bp.MerchandisingGroup
                            .GetMerchandisingSpace(false)
                            .SpaceReducedByObstructions.ToSize())))
                    productStack.Push(product);
                else
                    _tooLargeProductGtins.Add(product.Gtin);
            }
        }       
    }

    internal static class FrameworkExtension
    {
        public static WidthHeightDepthValue ToSize(this PlanogramProduct source)
        {
            return source != null
                       ? new WidthHeightDepthValue(
                           source.Width * (source.SqueezeWidth.EqualTo(0) ? 1f : source.SqueezeWidth),
                           source.Height * (source.SqueezeHeight.EqualTo(0) ? 1f : source.SqueezeHeight),
                           source.Depth * (source.SqueezeDepth.EqualTo(0) ? 1f : source.SqueezeDepth))
                       : default(WidthHeightDepthValue);
        }

        /// <summary>
        /// Returns the size dimensions of a product unit orienteted to a specified orientation
        /// </summary>
        /// <param name="source">The source product to examine</param>
        /// <param name="orientation">The preferred orientation</param>
        /// <returns>The WidthHeightDepthValue object representing the size</returns>
        public static WidthHeightDepthValue ToPositionOrientedSize(this PlanogramProduct source, PlanogramPositionOrientationType orientation)
        {
            return ProductOrientationHelper.GetOrientatedSize(orientation, source.ToSize());
        }

        public static Boolean FitsInside(this WidthHeightDepthValue source, WidthHeightDepthValue other)
        {
            return !source.Width.GreaterThan(other.Width) && !source.Height.GreaterThan(other.Height) && !source.Depth.GreaterThan(other.Depth);
        }

        internal static WidthHeightDepthValue ToSize(this RectValue rectValue)
        {
            return new WidthHeightDepthValue(rectValue.Width, rectValue.Height, rectValue.Depth);
        }
    }
}