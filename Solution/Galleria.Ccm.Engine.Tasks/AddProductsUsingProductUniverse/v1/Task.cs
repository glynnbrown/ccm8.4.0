﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30730 : A.Kuszyk
//  Created.
// V8-30840 : A.Kuszyk
//  Updated to use ProductUniverse.FetchByEntityIdName.
// V8-30905 : A.Kuszyk
//  Added LogProductsInPlanNotInProductUniverse.
// V8-30907 : A.Kuszyk
//  Added place unplaced products parameter.
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
// V8-30990 : D.Pleasance
//  Amended default parameter, AddToCarParkComponent (hidden-true, readonly-true), CarParkComponentName (hidden-false, readonly-false)
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1
{
    /// <summary>
    /// Add products to a plan using the contents of a product universe as a source.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version

        private readonly Byte _majorVersion = 1; // holds the tasks major version number
        private readonly Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter

        private enum Parameter
        {
            ProductUniverse = 0,
            AddToCarParkComponent = 1,
            CarParkComponentName = 2,
            PlaceUnPlacedOnCarParkComponent = 3,
            AddCarParkTextBox = 4,
        }

        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name { get { return Message.AddProductsUsingProductUniverse_Name; } } 

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description { get { return Message.AddProductsUsingProductUniverse_Description; } }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category { get { return Message.TaskCategory_MerchandisePlanogram; } }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // product codes
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.ProductUniverse,
                    name: Message.AddProductsUsingProductUniverse_ProductUniverse_Name,
                    description: Message.AddProductsUsingProductUniverse_ProductUniverse_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.ProductUniverseNameSingle,
                    defaultValue: null,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false));

            // add to car park component?
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.AddToCarParkComponent,
                    name: Message.AddProductsUsingProductUniverse_AddToCarParkComponent_Name, 
                    description: Message.AddProductsUsingProductUniverse_AddToCarParkComponent_Description,
                    category: Message.ParameterCategory_Default,
                    enumType: typeof(AddToCarParkType),
                    defaultValue: AddToCarParkType.Yes,
                    isHiddenByDefault: true,
                    isReadOnlyByDefault: true));

            // car park component name
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.CarParkComponentName,
                    name: Message.AddProductsUsingProductUniverse_CarParkComponentName_Name, 
                    description: Message.AddProductsUsingProductUniverse_CarParkComponentName_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.String,
                    defaultValue: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Default,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false,
                    parentId: null,
                    isRequired: null,
                    isNullAllowed: null,
                    isValid: () => IsCarParkNameValid()));

            RegisterParameter(
                new TaskParameter(
                    Parameter.PlaceUnPlacedOnCarParkComponent,
                    Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                    Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Description,
                    Message.ParameterCategory_Default,
                    typeof(PlaceUnPlacedOnCarParkComponentType),
                    PlaceUnPlacedOnCarParkComponentType.Yes,
                    false,
                    false,
                    isRequired:() => IsPlaceUnPlacedOnCarParkComponentRequired()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.AddProductsUsingProductUniverse_Parameter_AddCarParkTextBox_Name,
                Message.AddProductsUsingProductUniverse_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetProductUniverse(taskContext)) return;
                if (!LogProductsInPlanNotInProductUniverse(taskContext)) return;
                if (!AddProductsToPlanogram(taskContext)) return;
                if (!AddPositionsToCarParkComponent(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates whether the place unplaced products on car park component parameter is required.
        /// </summary>
        /// <returns></returns>
        private String IsPlaceUnPlacedOnCarParkComponentRequired()
        {
            // Get Add to car park component parameter value.
            if (Parameters.Count <= (Int32)Parameter.AddToCarParkComponent ||
                Parameters[(Int32)Parameter.AddToCarParkComponent] == null ||
                Parameters[(Int32)Parameter.AddToCarParkComponent].Value == null ||
                Parameters[(Int32)Parameter.AddToCarParkComponent].Value.Value1 == null)
            {
                return Message.AddProducts_AddToCarParkParameterCannotBeFound;
            }

            AddToCarParkType addToCarPark;
            try
            {
                addToCarPark = (AddToCarParkType)Parameters[(Int32)Parameter.AddToCarParkComponent].Value.Value1;
            }
            catch (InvalidCastException)
            {
                return Message.AddProducts_AddToCarParkParameterCannotBeFound;
            }

            if (addToCarPark == AddToCarParkType.Yes)
            {
                return null; // This parameter is required
            }
            else
            {
                return Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_NotRequired;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Logs any products that already exist in the plan that aren't in the product universe being added.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean LogProductsInPlanNotInProductUniverse(TaskContext taskContext)
        {
            if(taskContext.ProductUniverse == null) return false;

            // Get the products already in the plan that aren't in the product universe.
            IEnumerable<String> productUniverseProductGtins = taskContext.ProductUniverse.Products.Select(p => p.Gtin).ToList();
            IEnumerable<PlanogramProduct> placedProductsInPlanNotInUniverse = taskContext.Planogram.Products
                .Where(p => p.GetPlanogramPositions().Any() && !productUniverseProductGtins.Contains(p.Gtin))
                .ToList();
            IEnumerable<PlanogramProduct> unplacedProductsInPlanNotInUniverse = taskContext.Planogram.Products
                .Where(p => !p.GetPlanogramPositions().Any() && !productUniverseProductGtins.Contains(p.Gtin))
                .ToList();

            // Log the fact that there were products not in the universe.
            if(placedProductsInPlanNotInUniverse.Any())
            {
                taskContext.LogWarning(
                    EventLogEvent.ProductsInPlanNotInUniverse, 
                    placedProductsInPlanNotInUniverse.Count(), 
                    Message.AddProductsUsingProductUniverse_Placed,
                    taskContext.ProductUniverse.Name);
                
                foreach (PlanogramProduct product in placedProductsInPlanNotInUniverse)
                {
                    taskContext.LogDebug(
                        PlanogramEventLogAffectedType.Products,
                        product.Id,
                        EventLogEvent.ProductInPlanNotInUniverse,
                        Message.AddProductsUsingProductUniverse_Placed,
                        product.Gtin,
                        product.Name,
                        taskContext.ProductUniverse.Name);
                }
            }

            if(unplacedProductsInPlanNotInUniverse.Any())
            {
                taskContext.LogWarning(
                    EventLogEvent.ProductsInPlanNotInUniverse,
                    unplacedProductsInPlanNotInUniverse.Count(),
                    Message.AddProductsUsingProductUniverse_Unplaced,
                    taskContext.ProductUniverse.Name);

                foreach (PlanogramProduct product in unplacedProductsInPlanNotInUniverse)
                {
                    taskContext.LogDebug(
                        PlanogramEventLogAffectedType.Products,
                        product.Id,
                        EventLogEvent.ProductInPlanNotInUniverse,
                        Message.AddProductsUsingProductUniverse_Unplaced,
                        product.Gtin,
                        product.Name,
                        taskContext.ProductUniverse.Name);
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region Product Universe
                if ((!Parameters.Contains((Int32)Parameter.ProductUniverse)) ||
                    (Parameters[Parameter.ProductUniverse].Values == null))
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProductsUsingProductUniverse_ProductUniverse_Name,
                        Message.Error_NoValue);
                    return false;
                }
                try
                {
                    context.ProductUniverseName = (String)Parameters[Parameter.ProductUniverse].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProductsUsingProductUniverse_ProductUniverse_Name,
                        Message.Error_NoValue);
                    return false;
                }

                #endregion

                #region Add to Car Park Component

                // validate the add to car park shelf
                if ((!Parameters.Contains((Int32)Parameter.AddToCarParkComponent)) ||
                    (Parameters[Parameter.AddToCarParkComponent].Value == null) ||
                    (Parameters[Parameter.AddToCarParkComponent].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddProductsUsingProductUniverse_AddToCarParkComponent_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.AddToCarParkComponent = (AddToCarParkType)Parameters[Parameter.AddToCarParkComponent].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddProductsUsingProductUniverse_AddToCarParkComponent_Name,
                                         Parameters[Parameter.AddToCarParkComponent].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddProductsUsingProductUniverse_CarParkComponentName_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) ||
                        (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddProductsUsingProductUniverse_CarParkComponentName_Name,
                                         Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region Place Unplaced on Car Park Component
                if (Parameters.Count <= (Int32)Parameter.PlaceUnPlacedOnCarParkComponent ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent] == null ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value == null ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1 == null)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        PlaceUnPlacedOnCarParkComponentType placeUnplaced =
                            (PlaceUnPlacedOnCarParkComponentType)Convert.ToByte(Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1);
                        context.PlaceUnplacedProductsOnCarParkComponent = placeUnplaced == PlaceUnPlacedOnCarParkComponentType.Yes;
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                            Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductsUsingProductUniverse_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.AddProductsUsingProductUniverse_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Gets the Product Universe based on its name parameter and stores it in the task context.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean GetProductUniverse(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (String.IsNullOrEmpty(taskContext.ProductUniverseName)) return false;

                try
                {
                    taskContext.ProductUniverse = ProductUniverse.FetchByEntityIdName(taskContext.EntityId, taskContext.ProductUniverseName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.AddProductsUsingProductUniverse_ProductUniverse_Name,
                            taskContext.ProductUniverseName);
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Adds all the products from the product universe to the planogram if they don't already exist.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean AddProductsToPlanogram(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (taskContext.ProductUniverse == null) return false;

                // Find the gtins of the products in the universe that aren't already in the plan.
                IEnumerable<String> productsAlreadyInPlan = taskContext.Planogram.Products.Select(p => p.Gtin).ToList();
                IEnumerable<String> gtinsToAdd = taskContext.ProductUniverse.Products
                    .Where(p => !productsAlreadyInPlan.Contains(p.Gtin))
                    .Select(p => p.Gtin);

                //  Log number of products that already are present and will not be added.
                taskContext.LogInformation(
                    EventLogEvent.PlanogramProductsExistInPlanogram,
                    productsAlreadyInPlan.Count());

                // Fetch the products to add and add them to the plan.
                Int32 productCount = 0;
                foreach (Product productToAdd in ProductList.FetchByEntityIdProductGtins(taskContext.EntityId, gtinsToAdd))
                {
                    PlanogramProduct newProduct = PlanogramProduct.NewPlanogramProduct(productToAdd);
                    taskContext.Planogram.Products.Add(newProduct);
                    taskContext.ProductsAdded.Add(newProduct);
                    productCount++;
                }
                
                //  Log number of products that were added.
                if (productCount > 0)
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramProductsAdded, productCount);
                }

                return true;
            }
        }

        /// <summary>
        /// Adds positions to this car park component
        /// for all assortment products, if required
        /// </summary>
        private Boolean AddPositionsToCarParkComponent(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are not adding to a car park shelf
                // then there is nothing for use to do
                if (taskContext.AddToCarParkComponent == AddToCarParkType.No) return false;

                // Get the products that we've added that we need to place on the car park shelf (those that don't have positions)
                IEnumerable<String> positionGtins = taskContext.Planogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
                IEnumerable<PlanogramProduct> newProductsToAddToCarPark =
                    taskContext.ProductsAdded.Where(p => !positionGtins.Contains(p.Gtin));

                // If we're adding unplaced products already in the plan, then we also need to get those unplaced products
                // that need to be added.
                IEnumerable<PlanogramProduct> productsToAddToCarPark;
                if (taskContext.PlaceUnplacedProductsOnCarParkComponent)
                {
                    IEnumerable<String> productUniverseGtins = taskContext.ProductUniverse.Products.Select(p => p.Gtin).ToList();
                    IEnumerable<PlanogramProduct> unplacedProductsToAdd = 
                        taskContext.Planogram.Products
                        .Where(p => productUniverseGtins.Contains(p.Gtin) && !p.GetPlanogramPositions().Any());
                    productsToAddToCarPark = newProductsToAddToCarPark.Union(unplacedProductsToAdd).Distinct();
                }
                else
                {
                    productsToAddToCarPark = newProductsToAddToCarPark;
                }

                if (!productsToAddToCarPark.Any()) return false;

                // Get the car park component
                taskContext.MerchandisingGroups = taskContext.Planogram.GetMerchandisingGroups();
                taskContext.CarParkMerchandisingGroup = taskContext.GetCarParkMerchandisingGroup(taskContext.CarParkComponentName, taskContext.MerchandisingGroups, taskContext.AddCarParkTextBox);

                Int32 positionsCount = 0;
                foreach (PlanogramProduct newProduct in productsToAddToCarPark)
                {
                    // insert the product into the merchandising group
                    // which will create us a position placement
                    taskContext.CarParkMerchandisingGroup.InsertPositionPlacement(newProduct);
                    positionsCount++;
                }

                // finally, process the merchandising group
                taskContext.CarParkMerchandisingGroup.Process();
                taskContext.CarParkMerchandisingGroup.ApplyEdit();

                //  Log number of products placed on the Car Park Shelf.
                if (positionsCount > 0)
                {
                    taskContext.LogInformation(EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf, positionsCount);
                }

                return true;
            }
        }

        #endregion
    }
}
