﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30730 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// add to carpark shelf param
    /// </summary>
    public enum AddToCarParkType
    {
        Yes,
        No
    }

    /// <summary>
    /// Contains helpers for the AddToCarparkType enum.
    /// </summary>
    public static class AddToCarParkTypeHelper
    {
        public static readonly Dictionary<AddToCarParkType, String> FriendlyNames =
            new Dictionary<AddToCarParkType, String>()
            {
                {AddToCarParkType.Yes, Message.Generic_Yes },
                {AddToCarParkType.No, Message.Generic_No },
            };
    }
}
