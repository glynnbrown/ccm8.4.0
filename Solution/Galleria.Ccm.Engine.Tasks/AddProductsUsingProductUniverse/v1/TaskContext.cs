﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30730 : A.Kuszyk
//  Created.
// V8-30840 : A.Kuszyk
//  Removed ProductUniverseId property.
// V8-30907 : A.Kuszyk
//  Added Place unplaced products property.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Properties

        /// <summary>
        /// The name of the Product Universe to load.
        /// </summary>
        public String ProductUniverseName { get; set; }

        /// <summary>
        /// Indicates whether or not positions should be added to the car park component.
        /// </summary>
        public AddToCarParkType AddToCarParkComponent { get; set; }

        /// <summary>
        /// Indiciates whether or not products already on the plan that aren't placed should be added to the car park component.
        /// </summary>
        public Boolean PlaceUnplacedProductsOnCarParkComponent { get; set; }

        /// <summary>
        /// The name of the car park component.
        /// </summary>
        public String CarParkComponentName { get; set; }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox { get; set; }

        /// <summary>
        /// The product universe to add products from.
        /// </summary>
        public ProductUniverse ProductUniverse { get; set; }

        /// <summary>
        /// The products that have been added to the plan.
        /// </summary>
        public List<PlanogramProduct> ProductsAdded { get; private set; }

        /// <summary>
        /// The merchandising groups for the plan.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// The merchandising group for the car park component.
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup  { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            ProductsAdded = new List<PlanogramProduct>();
        }
        #endregion

        #region Methods
        protected override void OnDipose()
        {
            base.OnDipose();
            CarParkMerchandisingGroup = null;
            if (MerchandisingGroups != null) MerchandisingGroups.Dispose();
            MerchandisingGroups = null;
        } 
        #endregion
    }
}
