﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
// V8-31284 : D.Pleasance
//  Removed MainAnchorPosition, SecondaryAnchorPosition - Added AnchorPositions, to hold possible list of anchor positions for the sequence groups
#endregion
#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Task changes to become block aware.
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32612 : D.Pleasance
//  Added GetSpaceConstraintType
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1
{
    /// <summary>
    ///     Provides the context to run <see cref="Task"/> in.
    /// </summary>
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields
        private List<PlanogramProduct> _planogramProductsToDrop;
        private Dictionary<PlanogramBlockingGroup, Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>>> _planogramBlockingGroupSequencedSubComponentsByIdLookup
            = new Dictionary<PlanogramBlockingGroup, Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>>>();
        #endregion

        #region Constructor

        /// <summary>
        ///     Instantiates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) { }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the behaviour when coming across dropped products.
        /// </summary>
        public DroppedProductBehaviourType DroppedProductBehaviour { get; set; }

        /// <summary>
        ///     Gets or sets the name for the car park component.
        /// </summary>
        public String CarParkComponentName { get; set; }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox { get; set; }

        /// <summary>
        /// Indicates whether or not the task should increase into or beyond block space.
        /// </summary>
        public SpaceConstraintType SpaceConstraint { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramMerchandisingGroup"/> that contains the car park component.
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup { get; set; }

        /// <summary>
        ///     Gets the collection of <see cref="PlanogramProduct"/> that will be dropped.
        /// </summary>
        public List<PlanogramProduct> PlanogramProductsToDrop
        {
            get { return _planogramProductsToDrop ?? (_planogramProductsToDrop = new List<PlanogramProduct>()); }
        }

        /// <summary>
        ///     Gets or sets the list of GTINs passed as a parameter, ordered by GTIN.
        /// </summary>
        public List<String> GtinsInOrder { get; set; }

        /// <summary>
        ///     Gets or sets the lookup dictionary of <see cref="PlanogramSequenceGroup"/> instances by GTIN.
        /// </summary>
        public Dictionary<String, List<PlanogramSequenceGroup>> SequenceGroupByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramMerchandisingGroup"/> instances present in the Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// A dictionary of product stacks, keyed by their sequence group colour.
        /// </summary>
        public Dictionary<Int32, Stack<PlanogramProduct>> ProductStacksBySequenceColour { get; set; }

        /// <summary>
        /// A dictionary of target unit dictionaries (units keyed by gtin), keyed by the sequence group colour they belong to.
        /// </summary>
        public Dictionary<Int32, Dictionary<String, Int32>> TargetUnitsBySequenceColour { get; set; }

        /// <summary>
        ///     Gets or sets the lookup of all positions in the planogram by GTIN.
        /// </summary>
        public Dictionary<String, List<PlanogramPositionPlacement>> PlanogramPositionsByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> instances that 
        ///     are missing from the Planogram's Product List.
        /// </summary>
        public List<PlanogramProduct> MissingPlanogramProducts { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> instances that 
        ///     are to be inserted into the Planogram.
        /// </summary>
        public List<PlanogramProduct> PlanogramProductsToInsert { get; set; }

        /// <summary>
        ///     Gets or sets the lookup of target units to place by GTIN.
        /// </summary>
        public Dictionary<String, Int16> TargetUnitsByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramProduct"/> that is being processed currently.
        /// </summary>
        public PlanogramProduct CurrentProduct { get; set; }

        /// <summary>
        ///     Gets or sets the current target units for the current product.
        /// </summary>
        public Int32 CurrentTargetUnits { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramSequenceGroup"/> instance that is currently being processed.
        /// </summary>
        public PlanogramSequenceGroup CurrentSequenceGroup { get; set; }

        /// <summary>
        ///     Gets or sets the available anchor positions available for the current product to be inserted.
        /// </summary>
        public List<PlanogramPositionPlacementHelper> AnchorPositions { get; set; }

        /// <summary>
        ///     Gets or sets the current anchor <see cref="PlanogramPositionPlacement"/> for the current product to be inserted.
        /// </summary>
        public PlanogramPositionPlacement CurrentAnchor { get; set; }

        /// <summary>
        ///     Gets or sets the current product inserted count.
        /// </summary>
        public Int32 InsertCount { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramPositionPlacement"/> that is being processed currently.
        /// </summary>
        public PlanogramPositionPlacement CurrentPosition { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramSubComponentPlacement"/> that is being processed currently.
        /// </summary>
        public PlanogramSubComponentPlacement CurrentSubComponentPlacement { get; set; }

        /// <summary>
        ///     Gets or sets the current axis of insertion.
        /// </summary>
        public AxisType CurrentAxis { get; set; }

        /// <summary>
        ///     Gets or sets if the sequence reference data within the plan can be used to obtain sequence groups.
        /// </summary>
        public Boolean IsPositionSequenceDataAvailable { get; set; }

        /// <summary>
        ///     Gets or sets the plans <see cref="PlanogramBlocking"/> lookup. 
        /// </summary>
        public PlanogramBlocking PlanogramBlocking { get; set; }

        /// <summary>
        ///     Gets or sets the current planogram blocking width.
        /// </summary>
        public Single PlanogramBlockingWidth { get; set; }

        /// <summary>
        ///     Gets or sets the current planogram blocking height.
        /// </summary>
        public Single PlanogramBlockingHeight { get; set; }

        /// <summary>
        ///     Gets or sets the current planogram blocking height offset.
        /// </summary>
        public Single PlanogramBlockingHeightOffset { get; set; }

        /// <summary>
        ///     Gets or sets the merchandising groups, design view positions.
        /// </summary>
        public Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> MerchandisingGroupsAndDvps { get; set; }

        /// <summary>
        ///     Gets the dictionary of blocking groups and there sequenced subcomponents
        /// </summary>
        public Dictionary<PlanogramBlockingGroup, Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>>> PlanogramBlockingGroupSequencedSubComponentsByIdLookup
        {
            get { return _planogramBlockingGroupSequencedSubComponentsByIdLookup; }
        }

        #endregion

        #region Methods

        public PlanogramMerchandisingSpaceConstraintType GetSpaceConstraintType(SpaceConstraintType spaceConstraintType)
        {
            switch (spaceConstraintType)
            {
                case SpaceConstraintType.BlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
                case SpaceConstraintType.BeyondBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace;
                case SpaceConstraintType.MinimumBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation;
            }

            return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
        }

        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }

        #endregion
    }

    public class PlanogramPositionPlacementHelper
    {
        public PlanogramPositionPlacement PlanogramPositionPlacement { get; set; }
        public PlanogramSequenceGroup PlanogramSequenceGroup { get; set; }

        public PlanogramPositionPlacementHelper(PlanogramPositionPlacement planogramPositionPlacement, PlanogramSequenceGroup planogramSequenceGroup)
        {
            PlanogramPositionPlacement = planogramPositionPlacement;
            PlanogramSequenceGroup = planogramSequenceGroup;
        }
    }
}