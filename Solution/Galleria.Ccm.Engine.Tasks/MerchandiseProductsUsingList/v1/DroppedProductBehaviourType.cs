﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Unplaced Products parameter.
    /// </summary>
    public enum DroppedProductBehaviourType
    {
        AddToCarparkShelf,
        DoNotAdd,
        AlwaysAdd
    }

    /// <summary>
    ///     Helpers for <see cref="DroppedProductBehaviourType"/>.
    /// </summary>
    public static class DroppedProductBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="DroppedProductBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<DroppedProductBehaviourType, string> FriendlyNames = new Dictionary<DroppedProductBehaviourType, string>
        {
            {DroppedProductBehaviourType.AddToCarparkShelf, Message.DroppedProductBehaviourType_AddToCarParkComponent},
            {DroppedProductBehaviourType.DoNotAdd, Message.DroppedProductBehaviourType_DoNotAdd},
            {DroppedProductBehaviourType.AlwaysAdd, Message.DroppedProductBehaviourType_AlwaysAdd}
        };

    }
}