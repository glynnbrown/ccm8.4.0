﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
// V8-31284 : D.Pleasance
//  Changes applied to gather all possible anchor positions in a list which is ultimetly sorted to find the most appropriate anchor. 
//  These changes have been made due to anchor positions possibly being multisited on the plan.
// V8-31409 : D.Pleasance
//  Car park items also have sequence number \ sequence colour applied if possible.
#endregion
#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Task changes to become block aware.
// V8-32371 : D.Pleasance
//  Added product.Gtin detail to event log message EventLogEvent.PlanogramSequenceMissingProduct
// V8-32372 : D.Pleasance
//  Amended so that task logs error if any of (Blocking, Sequence) is missing from the plan.
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32612 : D.Pleasance
//  Amended PlanogramMerchandisingProductLayout constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32688 : A.Kuszyk
//  Corrected issue with task failing to re-merchandise at the end of its execution.
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// V8-32825 : A.Silva
//  Added call to RestoreSequenceNumbers in the context so that any altered sequence numbers as part of the process are restored.
// V8-32787 : A.Silva
//  Amended TryIncreaseUnits to enforce Assortment Rules.
// CCM-18435 : A.Silva
//  Refactored by pulling up HasOverlap to PlanogramMerchandisingGroup.HasCollisionOverlapping.
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1
{
    /// <summary>
    /// Inserts one or more new products into a planogram, placing them in a suitable location based on the planogram's Blocking and Sequence Strategies.
    /// </summary>
    [Serializable]
    public sealed class Task : TaskBase
    {
        #region Version

        /// <summary>
        ///     This instance's task major version number.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        /// <summary>
        ///     This instance's task minor version number.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        #endregion

        #region Parameter IDs

        /// <summary>
        ///     Enumeration of parameter IDs for this task.
        /// </summary>
        private enum Parameter
        {
            ProductCode = 0,
            DroppedProductBehaviour = 1,
            CarParkComponentName = 2,
            SpaceConstraint = 3,
            AddCarParkTextBox = 4,
        }

        #endregion

        #region Properties

        /// <summary>
        ///     The task name.
        /// </summary>
        public override String Name { get { return Message.MerchandiseProductsUsingList_Name; } }

        /// <summary>
        ///     The task description.
        /// </summary>
        public override String Description { get { return Message.MerchandiseProductsUsingList_Description; } }

        /// <summary>
        ///     The task category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_MerchandisePlanogram; } }

        /// <summary>
        ///     The task minor version number.
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     The task major version number.
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }
        
        #endregion

        #region Parameter Registration

        /// <summary>
        ///     Called when parameters are being registered for the task.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            //ProductCode:
            RegisterParameter(new TaskParameter(
                id: Parameter.ProductCode,
                name: Message.InsertProduct_ProductCode_Name,
                description: Message.InsertProduct_ProductCode_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.ProductCodeMultiple,
                defaultValue: null,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));

            //DroppedProductBehaviour:
            RegisterParameter(new TaskParameter(
                id: Parameter.DroppedProductBehaviour,
                name: Message.InsertProduct_DroppedProductBehaviour_Name,
                description: Message.InsertProduct_DroppedProductBehaviour_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(DroppedProductBehaviourType),
                defaultValue: DroppedProductBehaviourType.AddToCarparkShelf,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));

            //CarParkComponentName:
            RegisterParameter(new TaskParameter(
                id: Parameter.CarParkComponentName,
                name: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name,
                description: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.String,
                defaultValue: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Default,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true,
                parentId: null,
                isRequired: null,
                isNullAllowed: null,
                isValid: () => IsCarParkNameValid()));

            //SpaceConstraint:
            RegisterParameter(new TaskParameter(
                id: Parameter.SpaceConstraint,
                name: Message.MerchandiseProductsUsingList_SpaceConstraint_Name,
                description: Message.MerchandiseProductsUsingList_SpaceConstraint_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(SpaceConstraintType),
                defaultValue: SpaceConstraintType.BlockSpace,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.MerchandiseProductsUsingList_Parameter_AddCarParkTextBox_Name,
                Message.MerchandiseProductsUsingList_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }


        #endregion

        #region Execution
        /// <summary>
        ///     Executes the <see cref="Task"/> using the provided <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The <see cref="ITaskContext"/> for this <see cref="Task"/>.</param>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!PopulateLookups(taskContext)) return;
                if (!InsertProducts(taskContext)) return;
                if (!GetCarParkComponent(taskContext)) return;
                if (!AddPositionsToCarParkComponent(taskContext)) return;
                if (!RemoveProductsFromPlanogram(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Validate the parameters for this instance.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to store the validated parameters in.</param>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region DroppedProductBehaviour

                //  Validate that the Dropped Product Behaviour parameter exists.
                if (Parameters[Parameter.DroppedProductBehaviour].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProduct_DroppedProductBehaviour_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.DroppedProductBehaviour =
                            (DroppedProductBehaviourType)Parameters[Parameter.DroppedProductBehaviour].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProduct_DroppedProductBehaviour_Name,
                                         Parameters[Parameter.DroppedProductBehaviour].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProductCode

                //  Validate that the Product Code parameter exists.
                if (Parameters[Parameter.ProductCode].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProduct_ProductCode_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        //  Get the GTINs in order.
                        context.GtinsInOrder = Parameters[Parameter.ProductCode].Values.Select(o => Convert.ToString(o.Value1)).OrderBy(gtin => gtin).ToList();

                        //  Nothing to do if the List of Product Codes is empty.
                        if (context.GtinsInOrder.Count == 0)
                        {
                            context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductCodesSupplied);
                            success = false;
                        }
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProduct_ProductCode_Name,
                                         Parameters[Parameter.ProductCode].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region Space Constraint
                if (!Parameters.Contains((Int32)Parameter.SpaceConstraint) ||
                    Parameters[Parameter.SpaceConstraint].Value == null ||
                    Parameters[Parameter.SpaceConstraint].Value.Value1 == null)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.MerchandiseProductsUsingList_SpaceConstraint_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.SpaceConstraint = (SpaceConstraintType)Parameters[Parameter.SpaceConstraint].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseProductsUsingList_SpaceConstraint_Name,
                            Parameters[Parameter.SpaceConstraint].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.MerchandiseProductsUsingList_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.MerchandiseProductsUsingList_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        ///     Validate the planogram details required by the task.
        /// </summary>
        private static Boolean ValidatePlanogram(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region Blocking

                if (context.Planogram.Blocking == null || !context.Planogram.Blocking.Any())
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent, Message.TaskLogging_BlockingStrategy, Message.TaskLogging_NoBlockingStrategyWasPresent);
                    success = false;
                }

                #endregion

                #region Validate Sequence Data

                //  If there is no sequence the task cannot proceed.
                if (context.Planogram.Sequence == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_Sequence,
                                     Message.TaskLogging_NoSequenceInformationWasPresent);
                    success = false;
                }
                //  OR If there are no sequence groups the task cannot proceed.
                else if (context.Planogram.Sequence.Groups.Count == 0 ||
                         context.Planogram.Sequence.Groups.All(g => g.Products.Count == 0))
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_SequenceProducts,
                                     Message.TaskLogging_NoSequenceProductsWerePresent);
                    success = false;
                }

                #endregion

                #region Validate Initial Products and Positions

                //  If there are no products in the planogram the task has nothing to do.
                if (context.Planogram.Products.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductsInPlanogram);
                    success = false;
                }
                //  OR If there are no positions in the planogram the task has nothing to do.
                else if (context.Planogram.Positions.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoPositionsInPlanogram);
                    success = false;
                }

                //  Log any required inserts that are already in the Planogram.
                #endregion

                //  return success.
                return success;
            }
        }

        /// <summary>
        ///     Populate the lookup lists to be used in the task.
        /// </summary>
        private static Boolean PopulateLookups(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region GTINs, in order (obtained from the task parameter)

                //  Log the number of products we are set to insert initially.
                context.LogInformation(EventLogEvent.PlanogramProductsToInsert, context.GtinsInOrder.Count);

                #endregion

                #region Sequence Groups by GTIN

                //  Sequence groups by GTIN.
                context.SequenceGroupByGtin = new Dictionary<String, List<PlanogramSequenceGroup>>();
                foreach (String gtin in context.GtinsInOrder)
                {
                    if (!context.SequenceGroupByGtin.ContainsKey(gtin))
                    {
                        context.SequenceGroupByGtin.Add(gtin, context.Planogram.Sequence.GetSequenceGroups(gtin).ToList());
                    }
                }

                #endregion

                #region Merchandising Groups

                //  Get the existing positions by Gtin.
                context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();

                #endregion

                #region Planogram Blocking
                if (context.Planogram.Blocking.Any())
                {
                    // final, performance, initial
                    context.PlanogramBlocking =
                        context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                        context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ??
                        context.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial) ??
                        context.Planogram.Blocking.FirstOrDefault();

                    Single planogramBlockingWidth;
                    Single planogramBlockingHeight;
                    Single planogramBlockingHeightOffset;
                    context.Planogram.GetBlockingAreaSize(out planogramBlockingHeight, out planogramBlockingWidth, out planogramBlockingHeightOffset);

                    context.PlanogramBlockingWidth = planogramBlockingWidth;
                    context.PlanogramBlockingHeight = planogramBlockingHeight;
                    context.PlanogramBlockingHeightOffset = planogramBlockingHeightOffset;

                    context.MerchandisingGroupsAndDvps =
                                        context.MerchandisingGroups.ToLookupDictionary(m => m, m => new DesignViewPosition(m, planogramBlockingHeightOffset));
                }
                #endregion

                #region Product Stacks

                if (context.PlanogramBlocking != null)
                {
                    context.ProductStacksBySequenceColour = context.PlanogramBlocking.GetAndUpdateProductStacksByColour(context.MerchandisingGroups);
                }

                #endregion

                #region Existing Planogram Positions by GTIN

                //  Lookup all the existing Positions by GTIN.
                context.PlanogramPositionsByGtin = context
                    .MerchandisingGroups
                    .SelectMany(g => g.PositionPlacements)
                    .ToLookup(p => p.Product.Gtin)
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

                #endregion

                //  Get the initial products in the Planogram.
                IEnumerable<PlanogramProduct> existingProducts = context
                    .Planogram.Products
                    .Where(p => context.GtinsInOrder.Any(gtin => p.Gtin == gtin))
                    .ToList();

                #region Missing Planogram Products

                context.MissingPlanogramProducts = ProductList
                    .FetchByEntityIdProductGtins(context.EntityId,
                                                 context.GtinsInOrder
                                                        .Except(existingProducts.Select(p => p.Gtin)))
                    .Select(PlanogramProduct.NewPlanogramProduct)
                    .ToList();

                //  Add any missing products into the planogram products.
                context.Planogram.Products.AddRange(context.MissingPlanogramProducts);

                //  Update the existing products to include the ones that just got added.
                existingProducts = existingProducts.Union(context.MissingPlanogramProducts);

                #endregion

                #region Planogram Products to Insert

                //  Get the products that should be inserted.
                context.PlanogramProductsToInsert = context
                    .GtinsInOrder
                    .Select(gtin => existingProducts.FirstOrDefault(p => p.Gtin == gtin))
                    .Where(p => p != null)
                    .ToList();

                //  Remove any already placed products from the ToInsert list.
                ICollection<PlanogramProduct> alreadyPlacedProducts = context.PlanogramProductsToInsert.Where(p => context.PlanogramPositionsByGtin.ContainsKey(p.Gtin)).ToList();
                foreach (PlanogramProduct placedProduct in alreadyPlacedProducts)
                {
                    context.PlanogramProductsToInsert.Remove(placedProduct);
                }

                // Log how many products were already placed.
                if (alreadyPlacedProducts.Count > 0)
                {
                    context.LogInformation(EventLogEvent.PlanogramProductsExistInPlanogram, alreadyPlacedProducts.Count);
                }

                //  Check that the task has products to insert.
                if (context.PlanogramProductsToInsert.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_AllProductsAlreadyPlaced);
                    return false;
                }

                #endregion

                #region Target Units by Gtin

                //  NB Always place one single facing.
                context.TargetUnitsByGtin = context
                    .GtinsInOrder
                    .ToDictionary(gtin => gtin, gtin => (Int16)1);

                #endregion

                #region Target Units by sequence colour
                
                //Pre-Populate sequence colours so that we don't loose any if they have no positions
                context.TargetUnitsBySequenceColour = context.Planogram.Sequence.Groups.ToDictionary(g => g.Colour, k => new Dictionary<String, Int32>());
                foreach (PlanogramPosition position in context.Planogram.Positions)
                {
                    if (!position.SequenceColour.HasValue) continue;

                    Dictionary<String, Int32> targetUnits;
                    if (!context.TargetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                    {
                        targetUnits = new Dictionary<String, Int32>();
                        context.TargetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                    }

                    String productGtin = position.GetPlanogramProduct().Gtin;
                    if (targetUnits.ContainsKey(productGtin)) continue;
                    targetUnits.Add(productGtin, position.TotalUnits);
                }

                #endregion

                // Validate if we can make the assumption that the position sequence data is available on the plan.
                context.IsPositionSequenceDataAvailable = context.Planogram.Positions.Where(p => p.SequenceColour != null).Any();

                //  Return success.
                return true;
            }
        }

        #region Insert Products

        private static Boolean InsertProducts(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Find the best placement, if any, for each product and insert it.
                //  For each product we need to find the placed previous and next products.
                //  Then decide on what side of which one the product needs to be inserted.
                //  And finally insert it.
                foreach (PlanogramProduct product in context.PlanogramProductsToInsert)
                {
                    context.CurrentProduct = product;

                    //  Get the target units for the product per assorment.
                    context.CurrentTargetUnits = context.TargetUnitsByGtin[product.Gtin];

                    List<PlanogramSequenceGroup> sequenceGroups = context.SequenceGroupByGtin[product.Gtin];

                    //  Drop the Product if it is not in any Sequence.
                    if (!sequenceGroups.Any())
                    {
                        //  Warn the user via logging that the product
                        //  was missing from the sequence before moving
                        //  on to the next.
                        context.LogWarning(EventLogEvent.PlanogramSequenceMissingProduct, product.Gtin, product.Name);
                        context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                        continue;
                    }

                    // if no blocking group representation then we target all positions on the plan, otherwise we can attempt to filter a sub set of positions
                    Boolean hasBlockingGroupRepresentation = false;
                    if (context.PlanogramBlocking != null)
                    {
                        foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
                        {
                            hasBlockingGroupRepresentation = context.PlanogramBlocking.Groups.Where(p => p.Colour == sequenceGroup.Colour).Any();
                            if (hasBlockingGroupRepresentation) break;
                        }
                    }

                    Boolean productPlaced = false;

                    if (context.PlanogramBlocking != null)
                    {
                        // we have blocking but dont have blocking representation on the plan for the new product
                        if (!hasBlockingGroupRepresentation)
                        {
                            context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                            continue;
                        }

                        productPlaced = TryRelayoutBlockWithNewProduct(context);
                        if (!productPlaced && context.DroppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd)
                        {
                            productPlaced = TryAddProductUsingSequence(context, product, sequenceGroups);
                        }
                    }
                    else
                    {
                        productPlaced = TryAddProductUsingSequence(context, product, sequenceGroups);
                    }

                    if (!productPlaced)
                    {
                        context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                        continue;
                    }
                    else
                    {
                        if (sequenceGroups.Count > 1)
                        {
                            // log the sequence group used to place the multisited item.
                            context.LogInformation(EventLogEvent.PlanogramSequenceMultiSitedProduct, product.Gtin, context.CurrentSequenceGroup.Name);
                        }
                    }

                    context.InsertCount++;
                }

                //  Log the information about new products inserted.
                if (context.InsertCount > 0)
                {
                    context.LogInformation(EventLogEvent.PlanogramPositionsInserted,
                                           context.InsertCount,
                                           Message.TaskLogging_ThePlanogram);
                }

                return true;
            }
        }

        /// <summary>
        /// Adds given product using available sequence groups.
        /// </summary>
        /// <param name="context">The current task context</param>
        /// <param name="product">The product to add</param>
        /// <param name="sequenceGroups">The list of available sequence groups</param>
        /// <returns>true if the product was placed</returns>
        private static Boolean TryAddProductUsingSequence(TaskContext context, PlanogramProduct product, List<PlanogramSequenceGroup> sequenceGroups)
        {
            Boolean productPlaced = false;
            context.AnchorPositions = new List<PlanogramPositionPlacementHelper>();

            foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
            {
                Dictionary<String, List<PlanogramPositionPlacement>> productPlacements = context
                    .MerchandisingGroups
                    .SelectMany(g => g.PositionPlacements)
                    .ToLookup(p => p.Product.Gtin)
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

                if (context.IsPositionSequenceDataAvailable)
                {
                    // find the positions on the plan that belong to this sequence group
                    productPlacements = GetProductPositionPlacementsForSequenceGroup(productPlacements, sequenceGroup.Colour);
                }

                context.AnchorPositions.AddRange(
                    sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, productPlacements, true).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
                context.AnchorPositions.AddRange(
                    sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, productPlacements).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
            }

            //  Drop the Product if there is no 
            //  anchor position for it in the Planogram.
            if (context.AnchorPositions.Any())
            {
                SortAnchors(context);

                foreach (PlanogramPositionPlacementHelper currentAnchor in context.AnchorPositions)
                {
                    context.CurrentSequenceGroup = currentAnchor.PlanogramSequenceGroup;
                    context.CurrentAnchor = currentAnchor.PlanogramPositionPlacement;

                    if (TryInsertProduct(context))
                    {
                        productPlaced = true;
                        break;
                    }
                }
            }
            return productPlaced;
        }

        private static bool TryRelayoutBlockWithNewProduct(TaskContext context)
        {
            PlanogramMerchandisingProductLayout productLayout = new PlanogramMerchandisingProductLayout(
                                                        context.Planogram,
                                                        context.MerchandisingGroups,
                                                        context.SequenceGroupByGtin[context.CurrentProduct.Gtin],
                                                        context.PlanogramBlocking,
                                                        context.PlanogramBlockingWidth,
                                                        context.PlanogramBlockingHeight,
                                                        context.PlanogramBlockingHeightOffset,
                                                        context.CurrentProduct,
                                                        context.ProductStacksBySequenceColour,
                                                        context.TargetUnitsBySequenceColour,
                                                        context.CurrentTargetUnits,
                                                        context.GetSpaceConstraintType(context.SpaceConstraint));

            Boolean success = productLayout.TryRelayoutBlockWithNewProduct();

            if (success)
            {
                context.CurrentSequenceGroup = productLayout.ProductSequenceGroup;
            }

            return success;
        }

        /// <summary>
        /// Returns a list of product positions that form the sequence group. 
        /// If there are no sequence group positions on the plan, all product placements are considered.
        /// </summary>
        /// <param name="productPositionPlacements">Dictionary of product and position placements on the plan</param>
        /// <param name="sequenceColour">The sequence group colour</param>
        /// <returns></returns>
        private static Dictionary<string, List<PlanogramPositionPlacement>> GetProductPositionPlacementsForSequenceGroup(Dictionary<string, List<PlanogramPositionPlacement>> productPositionPlacements, Int32 sequenceColour)
        {
            Dictionary<String, List<PlanogramPositionPlacement>> productPlacements = new Dictionary<string, List<PlanogramPositionPlacement>>();

            foreach (String gtin in productPositionPlacements.Keys)
            {
                List<PlanogramPositionPlacement> positionPlacements = productPositionPlacements[gtin].Where(p => p.Position.SequenceColour == sequenceColour).ToList();
                if (positionPlacements.Any())
                {
                    productPlacements.Add(gtin, positionPlacements);
                }
            }

            if (!productPlacements.Any())
            {
                // there are no representation for the sequence group, provide list of all positions on the plan.
                productPlacements = productPositionPlacements;
            }
            return productPlacements;
        }

        /// <summary>
        ///     Sorts the anchors by most available free space in its merchandising group, 
        ///     then by number of positions within the anchor sequence group, then by sequence group name
        /// </summary>
        /// <param name="context">The current task context</param>
        private static void SortAnchors(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.AnchorPositions = context.AnchorPositions
                    .OrderByDescending(pos => pos.PlanogramPositionPlacement.GetAvailableSpaceOnAxis(pos.PlanogramPositionPlacement.SubComponentPlacement.GetFacingAxis()))
                    .ThenBy(pos => pos.PlanogramSequenceGroup.Products.Count)
                    .ThenBy(pos => pos.PlanogramSequenceGroup.Name)
                    .ToList();
            }
        }

        private static Boolean TryInsertProduct(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.CurrentAnchor.MerchandisingGroup.BeginEdit();
                if (!TryProductPlacement(context))
                {
                    context.CurrentAnchor.MerchandisingGroup.CancelEdit();
                    return false;
                }

                if (context.PlanogramPositionsByGtin.ContainsKey(context.CurrentProduct.Gtin))
                {
                    context.PlanogramPositionsByGtin[context.CurrentProduct.Gtin].Add(context.CurrentPosition);
                }
                else
                {
                    context.PlanogramPositionsByGtin
                           .Add(context.CurrentProduct.Gtin, new List<PlanogramPositionPlacement> { context.CurrentPosition });
                }

                if (!TryIncreaseUnits(context))
                {
                    context.PlanogramPositionsByGtin.Remove(context.CurrentProduct.Gtin);
                    context.CurrentAnchor.MerchandisingGroup.CancelEdit();
                    return false;
                }
                else
                {
                    context.CurrentAnchor.MerchandisingGroup.Process();
                    context.CurrentAnchor.MerchandisingGroup.ApplyEdit();
                }
                return true;
            }
        }

        private static Boolean TryProductPlacement(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                PlanogramMerchandisingGroup merchandisingGroup = context.CurrentAnchor.MerchandisingGroup;
                merchandisingGroup.BeginEdit();

                //  Get the axis of insertion.
                context.CurrentSubComponentPlacement = context.CurrentAnchor.SubComponentPlacement;
                context.CurrentAxis = context.CurrentSubComponentPlacement.GetFacingAxis();

                PlanogramPosition newPlacement = PlanogramPosition.NewPlanogramPosition(
                    0,
                    context.CurrentProduct,
                    context.CurrentSubComponentPlacement,
                    context.CurrentSequenceGroup.Products.First(p => p.Gtin == context.CurrentProduct.Gtin).SequenceNumber,
                    context.CurrentSequenceGroup.Colour);

                context.CurrentPosition = merchandisingGroup.InsertPositionPlacement(
                    newPlacement,
                    context.CurrentProduct,
                    context.CurrentAnchor,
                    GetAnchorDirection(context));
                return true;
            }
        }

        private static PlanogramPositionAnchorDirection GetAnchorDirection(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get sequence direction based upon sequence position
                PlanogramSequenceGroupProductList products = context.CurrentSequenceGroup.Products;
                Int32 newSequenceNumber = products.First(p => p.Gtin == context.CurrentProduct.Gtin).SequenceNumber;
                Int32 anchorSequenceNumber =
                    products.First(p => p.Gtin == context.CurrentAnchor.Product.Gtin).SequenceNumber;
                Boolean isAfter = newSequenceNumber > anchorSequenceNumber;

                // Get sequence direction based upon current plans blocking
                if (context.PlanogramBlocking != null)
                {
                    PlanogramBlockingGroup planogramBlockingGroup = context.CurrentSequenceGroup.GetBlockingGroup(context.PlanogramBlocking);

                    if (planogramBlockingGroup != null)
                    {
                        Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById = null;

                        if (!context.PlanogramBlockingGroupSequencedSubComponentsByIdLookup.ContainsKey(planogramBlockingGroup))
                        {
                            ILookup<PlanogramBlockingLocation, PlanogramMerchandisingGroup> intersectingMerchGroupsByLocation =
                                        planogramBlockingGroup.GetIntersectingMerchGroupsByLocation(
                                        context.MerchandisingGroupsAndDvps, PlanogramMerchandisingBlock.OverlapThreshold, context.PlanogramBlockingWidth, context.PlanogramBlockingHeight);

                            if (intersectingMerchGroupsByLocation.Any())
                            {
                                sequencedSubComponentsById = planogramBlockingGroup
                                                .EnumerateSequencableItemsInSequence(
                                                    intersectingMerchGroupsByLocation
                                                    .SelectMany(g => g)
                                                    .Distinct()
                                                    .SelectMany(m => m.SubComponentPlacements))
                                                .ToDictionary(s => Convert.ToInt32(s.Item.SubComponent.Id), s => s);

                                context.PlanogramBlockingGroupSequencedSubComponentsByIdLookup.Add(planogramBlockingGroup, sequencedSubComponentsById);
                            }
                        }
                        else
                        {
                            sequencedSubComponentsById = context.PlanogramBlockingGroupSequencedSubComponentsByIdLookup[planogramBlockingGroup];
                        }

                        if (sequencedSubComponentsById != null && sequencedSubComponentsById.ContainsKey(Convert.ToInt32(context.CurrentSubComponentPlacement.SubComponent.Id)))
                        {
                            IEnumerable<PlanogramSequencedPositionPlacement> sequencedPositions = planogramBlockingGroup
                                            .EnumerateSubComponentPositionsInSequence(
                                                sequencedSubComponentsById[Convert.ToInt32(context.CurrentSubComponentPlacement.SubComponent.Id)],
                                                context.CurrentSubComponentPlacement.GetPlanogramPositions().Cast<IPlanogramSubComponentSequencedItem>());

                            var sequencePositionForAnchorProduct = sequencedPositions.FirstOrDefault(p => p.Position.Id.Equals(context.CurrentAnchor.Position.Id));

                            if (sequencePositionForAnchorProduct != null)
                            {
                                isAfter =
                                    planogramBlockingGroup.GetPlacementType(context.CurrentAxis).Invert(sequencePositionForAnchorProduct.GetIsReversed(context.CurrentAxis, planogramBlockingGroup)).IsPositive() ?
                                    isAfter :
                                    !isAfter;
                            }
                        }
                    }
                }

                switch (context.CurrentAxis)
                {
                    case AxisType.X:
                        return isAfter ? PlanogramPositionAnchorDirection.ToRight : PlanogramPositionAnchorDirection.ToLeft;
                    case AxisType.Y:
                        return isAfter ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below;
                    case AxisType.Z:
                        return isAfter ? PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;
                    default:
                        return PlanogramPositionAnchorDirection.ToRight;
                }
            }
        }

        private static Boolean TryIncreaseUnits(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                //  Get a new merchandising group with the new inserted position.
                PlanogramMerchandisingGroup merchandisingGroup = context.CurrentPosition.MerchandisingGroup;
                PlanogramPositionPlacement newPlacement = context.CurrentPosition;
                PlanogramPositionPlacement anchor = context.CurrentAnchor;

                //  Check if the Merchandising Strategy for the current axis is manual.
                Boolean isManualMerchandising = IsManualMerchandising(context.CurrentAxis, merchandisingGroup);

                //  Account for Manual Merchandising cases.
                List<PointValue> startingCoordinates = null;
                if (isManualMerchandising)
                {
                    //  Save pre existing positions.
                    startingCoordinates = merchandisingGroup
                        .PositionPlacements
                        .Where(p => !p.Id.Equals(newPlacement.Id))
                        .OrderBy(p => p.Id)
                        .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                        .ToList();

                    CorrectCoordinatesForManualStrategy(context, newPlacement, anchor);
                }

                //  Set the target units.
                newPlacement.SetUnits(context.CurrentTargetUnits, PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo, null);

                //  Validate the placement, decreasing units if necessary and allowed.
                do
                {
                    newPlacement.RecalculateUnits();
                    merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                    //  Return success if the placement is valid.
                    if (ValidatePlacement(context,
                                          isManualMerchandising,
                                          merchandisingGroup,
                                          newPlacement,
                                          startingCoordinates)) return true;

                    //  If units must be exact, return failure.
                    //  NB At the time, units always must be exact, no decreasing allowed.
                    //if (context.ProductPlacement != PlacementType.CloseToAssortmentFacings) return false;
                    return false;

                } while (newPlacement.DecreaseUnits());

                return false;
            }
        }

        private static Boolean IsManualMerchandising(AxisType currentAxis,
                                                     PlanogramMerchandisingGroup merchandisingGroup)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean isManualMerchandising = currentAxis == AxisType.X &&
                                                merchandisingGroup.StrategyX ==
                                                PlanogramSubComponentXMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Y &&
                                                merchandisingGroup.StrategyY ==
                                                PlanogramSubComponentYMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Z &&
                                                merchandisingGroup.StrategyZ ==
                                                PlanogramSubComponentZMerchStrategyType.Manual;
                return isManualMerchandising;
            }
        }

        private static void CorrectCoordinatesForManualStrategy(TaskContext context,
                                                                PlanogramPositionPlacement newPlacement,
                                                                PlanogramPositionPlacement anchor)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                switch (GetAnchorDirection(context))
                {
                    case PlanogramPositionAnchorDirection.InFront:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z + anchor.GetReservedSpace().Depth;
                        break;
                    case PlanogramPositionAnchorDirection.Behind:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z - newPlacement.GetReservedSpace().Depth;
                        break;
                    case PlanogramPositionAnchorDirection.Above:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y + anchor.GetReservedSpace().Height;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.Below:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y - newPlacement.GetReservedSpace().Height;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.ToLeft:
                        newPlacement.Position.X = anchor.Position.X - newPlacement.GetReservedSpace().Width;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.ToRight:
                        newPlacement.Position.X = anchor.Position.X + anchor.GetReservedSpace().Width;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static Boolean ValidatePlacement(TaskContext context,
                                                 Boolean isManualMerchandising,
                                                 PlanogramMerchandisingGroup merchandisingGroup,
                                                 PlanogramPositionPlacement newPlacement,
                                                 IEnumerable<PointValue> startingCoordinates)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Validate the placement.
                if (context.DroppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd) return true;

                //  Assume the placement is valid.
                var isValidPlacement = true;

                //  Account for manual merchandising strategy.
                if (isManualMerchandising)
                {
                    //  Check the pre existing positions did not need to move.
                    List<PointValue> finalCoordinates = merchandisingGroup
                        .PositionPlacements
                        .Where(p => !p.Id.Equals(newPlacement.Id))
                        .OrderBy(p => p.Id)
                        .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                        .ToList();
                    isValidPlacement = startingCoordinates
                        .Zip(finalCoordinates,
                             (starting, final) =>
                             starting.X.EqualTo(final.X) &&
                             starting.Y.EqualTo(final.Y) &&
                             starting.Z.EqualTo(final.Z)).All(b => b);
                }

                //  Validate the merchandising group can fit the positions.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.MerchandisingGroup.IsOverfilled();

                //  Validate the placement is insisde the merchandisable space.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.IsOutsideMerchandisingSpace();

                //  Validate the merchandising group has not internal overlaps.
                isValidPlacement = isValidPlacement &&
                                   !merchandisingGroup.HasCollisionOverlapping();

                //  Return the valid state.
                return isValidPlacement;
            }
        }

        #endregion

        /// <summary>
        ///     Retrieve the car park component if there needs to be one.
        /// </summary>
        /// <remarks>
        ///     If there needs to be a car park component and none exists
        ///     with the provided name a new one will be created.
        /// </remarks>
        private static Boolean GetCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If the task does not need the car park shelf
                //  then there is no need to get it.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.AddToCarparkShelf) return true;

                //  If there are no products to add
                //  then there is no need to get it.
                if (context.PlanogramProductsToDrop.Count == 0) return true;

                //  Get the car park component.
                context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

                //  Return success.
                return true;
            }
        }

        /// <summary>
        ///     Add positions for all products to be dropped on the car park component.
        /// </summary>
        private static Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If dropped positions are not added to the car park
                //  then there is nothing to do.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.AddToCarparkShelf) return true;

                //  If there are no dropped products
                //  then there is nothing to do.
                if (context.PlanogramProductsToDrop.Count == 0) return true;

                //  If there is no car park component
                //  then there is nothing to do.
                if (context.CarParkMerchandisingGroup == null) return true;

                //  Create a new position in the car park merchandising group
                //  for each product in the list of products to drop.
                foreach (PlanogramProduct product in context.PlanogramProductsToDrop)
                {
                    List<PlanogramSequenceGroup> sequenceGroups = context.SequenceGroupByGtin[product.Gtin];

                    if (sequenceGroups.Any())
                    {
                        PlanogramSequenceGroupProduct sequenceProduct = sequenceGroups.First().Products.FirstOrDefault(p => p.Gtin == product.Gtin);
                        if (sequenceProduct != null)
                        {
                            context.CarParkMerchandisingGroup.InsertPositionPlacement(product, sequenceProduct.SequenceNumber, sequenceGroups.First().Colour);
                        }
                    }
                    else
                    {
                        context.CarParkMerchandisingGroup.InsertPositionPlacement(product);
                    }
                }

                //  Apply the changes to the car park component.
                context.CarParkMerchandisingGroup.Process();
                context.CarParkMerchandisingGroup.ApplyEdit();

                //  Log the information about the products that got placed on the car park component.
                context.LogInformation(EventLogEvent.PlanogramPositionsInserted,
                                       context.PlanogramProductsToDrop.Count,
                                       Message.TaskLogging_TheCarparkShelf);

                //  Return success.
                return true;
            }
        }

        /// <summary>
        ///     Remove dropped products from the Planogram's Product List.
        /// </summary>
        private static Boolean RemoveProductsFromPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If we are not removing products at all
                //  then there is nothing to do.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.DoNotAdd) return true;

                //  If there are no products to drop that need removing
                //  then there is nothing to do.
                List<PlanogramProduct> planogramProductsToRemove = context.PlanogramProductsToDrop.Intersect(context.MissingPlanogramProducts).ToList();
                if (planogramProductsToRemove.Count == 0) return true;

                //  Remove dropped products from the planogram.
                context.Planogram.Products.RemoveList(planogramProductsToRemove);

                //  Log the information about products that could not be inserted.
                context.LogInformation(EventLogEvent.PlanogramPositionsNotInserted, context.PlanogramProductsToDrop.Count);

                //  Return success.
                return true;
            }
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (new CodePerformanceMetric())
            {
                context.RestoreSequenceNumbers();
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}