﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1
{
    /// <summary>
    ///     Possible values for the ProductPlacement parameter
    /// </summary>
    public enum PlacementType
    {
        SingleUnit = 0,
        AssortmentUnits = 1,
        CloseToAssortmentUnits = 2
    }

    /// <summary>
    /// Contains helpers for the PlacementType enum.
    /// </summary>
    public static class PlacementTypeHelper
    {
        public static readonly Dictionary<PlacementType, String> FriendlyNames =
            new Dictionary<PlacementType, String>
            {
                {PlacementType.SingleUnit, Message.PlacementType_SingleUnit },
                {PlacementType.AssortmentUnits, Message.PlacementType_AssortmentUnits },
                {PlacementType.CloseToAssortmentUnits, Message.PlacementType_CloseToAssortmentUnits }
            };
    }
}