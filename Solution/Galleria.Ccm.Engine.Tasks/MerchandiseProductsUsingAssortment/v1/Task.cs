﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
// V8-31284 : D.Pleasance
//  Changes applied to gather all possible anchor positions in a list which is ultimetly sorted to find the most appropriate anchor. 
//  These changes have been made due to anchor positions possibly being multisited on the plan.
// V8-31409 : D.Pleasance
//  Car park items also have sequence number \ sequence colour applied if possible.
#endregion
#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Task changes to become block aware.
// V8-32371 : D.Pleasance
//  Added product.Gtin detail to event log message EventLogEvent.PlanogramSequenceMissingProduct
// V8-32372 : D.Pleasance
//  Amended so that task logs error if any of (Assortment, Blocking, Sequence) is missing from the plan.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32612 : D.Pleasance
//  Amended PlanogramMerchandisingProductLayout constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
// V8-32688 : A.Kuszyk
//  Corrected issue with task failing to re-merchandise at the end of its execution.
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// V8-32726 : D.Pleasance
//  Added PlacementOrderType option - IsOptionalAndNotRangedInRankOrder
// V8-31682 : A.Silva
//  Amended PopulateLookups so that products that are in sequence groups other than those in the planogram blocking groups are never placed.
// V8-32825 : A.Silva
//  Added call to RestoreSequenceNumbers in the context so that any altered sequence numbers as part of the process are restored.
// V8-32880 : A.Kuszyk
//  Removed remove product behaviour, because this removes products from the assortment.
// V8-32787 : A.Silva
//  Refactored and amended to take into account Assortment Rules Priority when Merchandising Products.
// V8-32932 : A.Kuszyk
//  Added defensive check to sequence group/blocking group matching.
// V8-32787 : A.Silva
//  Amended TryIncreaseUnits to enforce Assortment Rules.
// CCM-18435 : A.Silva
//  Refactored by pulling up HasOverlap to PlanogramMerchandisingGroup.HasCollisionOverlapping.
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1
{
    /// <summary>
    ///     Inserts one or more new products into a planogram, placing them in a suitable location based on the planogram's Blocking and Sequence Strategies.
    /// </summary>
    [Serializable]
    public sealed class Task : TaskBase
    {
        #region Nested Class : PlanogramPositionPlacementHelper

        private class PlanogramPositionPlacementHelper
        {
            public PlanogramPositionPlacement PlanogramPositionPlacement { get; set; }
            public PlanogramSequenceGroup PlanogramSequenceGroup { get; set; }

            public PlanogramPositionPlacementHelper(PlanogramPositionPlacement planogramPositionPlacement,
                                                    PlanogramSequenceGroup planogramSequenceGroup)
            {
                PlanogramPositionPlacement = planogramPositionPlacement;
                PlanogramSequenceGroup = planogramSequenceGroup;
            }
        }

        #endregion

        #region Version

        /// <summary>
        ///     This instance's task major version number.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        /// <summary>
        ///     This instance's task minor version number.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        #endregion

        #region Parameter IDs

        /// <summary>
        ///     Enumeration of parameter IDs for this task.
        /// </summary>
        private enum Parameter
        {
            InsertAction = 0,
            ProductPlacement = 1,
            CarParkComponentName = 2,
            PlacementOrder = 3,
            SpaceConstraint = 4,
            AddCarParkTextBox = 5
        }

        #endregion

        #region Fields

        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();

        /// <summary>
        ///     The instance of <see cref="ITaskContext"/> used for this <see cref="Task"/> execution.
        /// </summary>
        private ITaskContext _taskContext;

        /// <summary>
        ///     Behaviour when coming across dropped products.
        /// </summary>
        private DroppedProductBehaviourType _droppedProductBehaviour;

        /// <summary>
        ///     How to place products when inserting them.
        /// </summary>
        private PlacementType _productPlacement;

        /// <summary>
        ///     The order in which to place products.
        /// </summary>
        private PlacementOrderType _productPlacementOrder;

        /// <summary>
        ///     Whether or not the task should increase into or beyond block space.
        /// </summary>
        private SpaceConstraintType _spaceConstraint;

        /// <summary>
        ///     The name for the car park component.
        /// </summary>
        private String _carParkComponentName;

        /// <summary>
        ///     Whether the carpark shelf should have a linked textbox.
        /// </summary>
        private Boolean _addCarParkTextBox;

        /// <summary>
        ///     The <see cref="PlanogramMerchandisingGroup"/> that contains the car park component.
        /// </summary>
        private PlanogramMerchandisingGroup _carParkMerchandisingGroup;

        /// <summary>
        ///     The <see cref="List{T}"/> of <see cref="PlanogramProduct"/> that will be dropped.
        /// </summary>
        private readonly List<PlanogramProduct> _planogramProductsToDrop = new List<PlanogramProduct>();

        /// <summary>
        ///     The <see cref="List{T}"/> of ranged GTINs in the Assortment, ordered by rank.
        /// </summary>
        private List<String> _sortedGtinsForProductToInsert;

        /// <summary>
        ///     Lookup <see cref="Dictionary{TKey,TValue}"/> of <see cref="PlanogramSequenceGroup"/> instances by GTIN.
        /// </summary>
        private Dictionary<String, List<PlanogramSequenceGroup>> _sequenceGroupByGtin;

        /// <summary>
        ///     The <see cref="PlanogramMerchandisingGroupList"/> with the <see cref="PlanogramMerchandisingGroup"/> instances present in the Planogram.
        /// </summary>
        private PlanogramMerchandisingGroupList _merchandisingGroups;

        /// <summary>
        ///     The <see cref="Dictionary{TKey,TValue}"/> of <see cref="Stack{T}"/> of <see cref="PlanogramProduct"/> by their Sequence Color.
        /// </summary>
        private Dictionary<Int32, Stack<PlanogramProduct>> _productStacksBySequenceColour;

        /// <summary>
        ///     The lookup <see cref="Dictionary{TKey,TValue}"/> of all <see cref="PlanogramPositionPlacement"/> instances for a given GTIN in the Planogram.
        /// </summary>
        private Dictionary<String, List<PlanogramPositionPlacement>> _planogramPositionsByGtin;

        /// <summary>
        ///     The <see cref="List{T}"/> of <see cref="PlanogramProduct"/> instances that where in the Planogram's Assortment but not in its Product List.
        /// </summary>
        private List<PlanogramProduct> _missingPlanogramProducts;

        /// <summary>
        ///     The <see cref="List{T}"/> of <see cref="PlanogramProduct"/> instances that need to be inserted.
        /// </summary>
        private List<PlanogramProduct> _planogramProductsToInsert;

        /// <summary>
        ///     The lookup <see cref="Dictionary{TKey,TValue}"/> of target units to place by GTIN.
        /// </summary>
        private Dictionary<String, Int16> _targetUnitsByGtin;

        /// <summary>
        ///     The <see cref="PlanogramProduct"/> that is being processed currently.
        /// </summary>
        private PlanogramProduct _currentProduct;

        /// <summary>
        ///     The current target units for the current product.
        /// </summary>
        private Int32 _currentTargetUnits;

        /// <summary>
        ///     The <see cref="PlanogramSequenceGroup"/> instance that is currently being processed.
        /// </summary>
        private PlanogramSequenceGroup _currentSequenceGroup;

        /// <summary>
        ///     The available anchor positions available for the <see cref="_currentProduct"/> to be inserted.
        /// </summary>
        private List<PlanogramPositionPlacementHelper> _anchorPositions;

        /// <summary>
        ///     The current anchor <see cref="PlanogramPositionPlacement"/> for the <see cref="_currentProduct"/> to be inserted.
        /// </summary>
        private PlanogramPositionPlacement _currentAnchor;

        /// <summary>
        ///     The <see cref="_currentProduct"/> inserted count.
        /// </summary>
        private Int32 _insertCount;

        /// <summary>
        ///     The <see cref="PlanogramPositionPlacement"/> that is being processed currently.
        /// </summary>
        private PlanogramPositionPlacement _currentPosition;

        /// <summary>
        ///     The <see cref="PlanogramSubComponentPlacement"/> that is being processed currently.
        /// </summary>
        private PlanogramSubComponentPlacement _currentSubComponentPlacement;

        /// <summary>
        ///     The current axis of insertion.
        /// </summary>
        private AxisType _currentAxis;

        /// <summary>
        ///     The planogram's latest <see cref="PlanogramBlocking"/>. 
        /// </summary>
        private PlanogramBlocking _planogramBlocking;

        /// <summary>
        ///     The current planogram blocking width.
        /// </summary>
        private Single _planogramBlockingWidth;

        /// <summary>
        ///     The current planogram blocking height.
        /// </summary>
        private Single _planogramBlockingHeight;

        /// <summary>
        ///     The current planogram blocking height offset.
        /// </summary>
        private Single _planogramBlockingHeightOffset;

        /// <summary>
        ///     The lookup <see cref="Dictionary{TKey,TValue}"/> of <see cref="Dictionary{TKey,TValue}"/> of target units by Gtin and then by Sequence Group Color.
        /// </summary>
        private Dictionary<Int32, Dictionary<String, Int32>> _targetUnitsBySequenceColour;

        /// <summary>
        ///     The lookup <see cref="Dictionary{TKey,TValue}"/> of <see cref="DesignViewPosition"/> by <see cref="PlanogramMerchandisingGroup"/>.
        /// </summary>
        private Dictionary<PlanogramMerchandisingGroup, DesignViewPosition> _designViewPositionByMerchandisingGroup;

        /// <summary>
        ///     The lookup <see cref="Dictionary{TKey,TValue}"/> of <see cref="Dictionary{TKey,TValue}"/> of <see cref="PlanogramSequencedItem{T}"/> for <see cref="PlanogramSubComponentPlacement"/> by their Id and then by their <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        private readonly Dictionary<PlanogramBlockingGroup, Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>>> _planogramBlockingGroupSequencedSubComponentsByIdLookup = 
            new Dictionary<PlanogramBlockingGroup, Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>>>();

        /// <summary>
        ///     Whether the sequence reference data within the plan can be used to obtain sequence groups.
        /// </summary>
        private Boolean _isPositionSequenceDataAvailable;

        private readonly List<PlanogramAssortmentRuleType> _assortmentRuleTypesPriorityOrder =
            new List<PlanogramAssortmentRuleType>
            {
                PlanogramAssortmentRuleType.LocalLine,
                PlanogramAssortmentRuleType.Distribution,
                PlanogramAssortmentRuleType.Product,
                PlanogramAssortmentRuleType.Family,
                PlanogramAssortmentRuleType.Inheritance,
                PlanogramAssortmentRuleType.Normal,
                PlanogramAssortmentRuleType.Optional
            };

        #endregion

        #region Properties

        /// <summary>
        ///     The task name.
        /// </summary>
        public override String Name { get { return Message.MerchandiseProductsUsingAssortment_Name; } }

        /// <summary>
        ///     The task description.
        /// </summary>
        public override String Description { get { return Message.MerchandiseProductsUsingAssortment_Description; } }

        /// <summary>
        ///     The task category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_MerchandisePlanogram; } }

        /// <summary>
        ///     The task minor version number.
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     The task major version number.
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        #endregion

        #region Parameter Registration

        /// <summary>
        ///     Called when parameters are being registered for the task.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            //ProductPlacement:
            RegisterParameter(new TaskParameter(Parameter.ProductPlacement, Message.InsertProductFromAssortment_ProductPlacement, Message.InsertProductFromAssortment_ProductPlacement_Description, Message.ParameterCategory_Default, typeof(PlacementType), PlacementType.SingleUnit, false, false));

            //DroppedProductBehaviour
            RegisterParameter(new TaskParameter(Parameter.InsertAction, Message.InsertProduct_DroppedProductBehaviour_Name, Message.InsertProduct_DroppedProductBehaviour_Description, Message.ParameterCategory_Default, typeof(DroppedProductBehaviourType), DroppedProductBehaviourType.AddToCarParkComponent, false, false));

            //CarParkComponentName
            RegisterParameter(new TaskParameter(Parameter.CarParkComponentName, Message.InserProduct_Parameter_CarParkComponentName_Name, Message.InserProduct_Parameter_CarParkComponentName_Description, Message.ParameterCategory_Default, TaskParameterType.String, Message.InserProduct_Parameter_CarParkComponentName_Default, true, true, null, null, null, IsCarParkNameValid));

            //PlacementOrder
            RegisterParameter(new TaskParameter(Parameter.PlacementOrder, Message.InsertProductFromAssortment_PlacementOrder, Message.InsertProductFromAssortment_PlacementOrder_Description, Message.ParameterCategory_Default, typeof(PlacementOrderType), PlacementOrderType.RangedInRankOrder, false, false));

            //SpaceConstraint
            RegisterParameter(new TaskParameter(Parameter.SpaceConstraint, Message.MerchandiseProductsUsingAssortment_SpaceConstraint_Name, Message.MerchandiseProductsUsingAssortment_SpaceConstraint_Description, Message.ParameterCategory_Default, typeof(SpaceConstraintType), SpaceConstraintType.BlockSpace, false, false));

            //Add car park textbox
            RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.MerchandiseProductsUsingAssortment_Parameter_AddCarParkTextBox_Name,
                Message.MerchandiseProductsUsingAssortment_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default, typeof(AddCarParkTextBoxType), AddCarParkTextBoxType.No, true, true));
        }

        #endregion

        #region Execution

        /// <summary>
        ///     Executes the <see cref="Task"/> using the provided <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The <see cref="ITaskContext"/> for this <see cref="Task"/>.</param>
        protected override void OnExecute(ITaskContext context)
        {
            if (context == null) return;
            if (_taskContext != null)
                System.Diagnostics.Debug.Fail(
                    "Tasks should be executed only once per instance and this instance was executed already. The Task may have previous values hanging around.");
            
            _taskContext = context;
            if (!ValidateParameters()) return;
            if (!ValidatePlanogram()) return;
            if (!PopulateLookups()) return;
            if (!InsertProducts()) return;
            if (!GetCarParkComponent()) return;
            if (!AddPositionsToCarParkComponent()) return;
            if (!RemoveProductsFromPlanogram()) return;
            ReMerchandisePlanogram();
        }

        #endregion

        #region Methods

        #region Validate Parameters

        /// <summary>
        ///     Validate the parameters for this instance.
        /// </summary>
        private Boolean ValidateParameters()
        {
            return TryGetParameterValueForInsertAction() &&
                   TryGetParameterValueForProductPlacement() &&
                   TryGetParameterValueForCarParkComponentName() &&
                   TryGetParameterValueForPlacementOrder() &&
                   TryGetParameterValueForSpaceContraint() &&
                   TryGetParameterValueForAddCarParkTextBox();
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.AddCarParkTextBox"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForAddCarParkTextBox()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.AddCarParkTextBox, out parameter) || parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.MerchandiseProductsUsingAssortment_Parameter_AddCarParkTextBox_Name,
                                      Message.Error_NoValue);
                return false;
            }

            //  Make sure the parameter value is of the expected type.
            Object parameterValue = parameter.Value.Value1;
            AddCarParkTextBoxType addTextBox;
            try
            {
                addTextBox = (AddCarParkTextBoxType)Convert.ToByte(parameterValue);
            }
            catch (InvalidCastException)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.MerchandiseProductsUsingAssortment_Parameter_AddCarParkTextBox_Name,
                                      parameterValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            _addCarParkTextBox = addTextBox == AddCarParkTextBoxType.Yes;
            return true;
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.SpaceConstraint"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForSpaceContraint()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.SpaceConstraint, out parameter) ||
                parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.MerchandiseProductsUsingAssortment_SpaceConstraint_Name,
                                      Message.Error_NoValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            Object parameterValue = parameter.Value.Value1;
            if (Enum.TryParse(Convert.ToString(parameterValue), out _spaceConstraint)) return true;

            //  The parameter had an invalid value.
            _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                  Message.MerchandiseProductsUsingAssortment_SpaceConstraint_Name,
                                  parameterValue);
            return false;
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.PlacementOrder"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForPlacementOrder()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.PlacementOrder, out parameter) || parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.InsertProductFromAssortment_PlacementOrder,
                                      Message.Error_NoValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            Object parameterValue = parameter.Value.Value1;
            if (Enum.TryParse(Convert.ToString(parameterValue), out _productPlacementOrder)) return true;

            //  The parameter had an invalid value.
            _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                  Message.InsertProductFromAssortment_PlacementOrder,
                                  parameterValue);
            return false;
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.CarParkComponentName"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForCarParkComponentName()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.CarParkComponentName, out parameter) || parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.InserProduct_Parameter_CarParkComponentName_Name,
                                      Message.Error_NoValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            Object parameterValue = parameter.Value.Value1;
            _carParkComponentName = Convert.ToString(parameterValue);
            if (!String.IsNullOrWhiteSpace(_carParkComponentName)) return true;

            //  The parameter had an invalid value.
            _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                  Message.InserProduct_Parameter_CarParkComponentName_Name,
                                  Message.Error_NoValue);
            return false;
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.ProductPlacement"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForProductPlacement()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.ProductPlacement, out parameter) || parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.InsertProductFromAssortment_ProductPlacement,
                                      Message.Error_NoValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            Object parameterValue = parameter.Value.Value1;
            if (Enum.TryParse(Convert.ToString(parameterValue), out _productPlacement)) return true;

            //  The parameter had an invalid value.
            _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                  Message.InsertProductFromAssortment_ProductPlacement,
                                  parameterValue);
            return false;
        }

        /// <summary>
        ///     Try to get the <see cref="Parameter.InsertAction"/> parameter value.
        /// </summary>
        /// <returns><c>True</c> if the parameter was loaded correctly, <c>false</c> otherwise.</returns>
        private Boolean TryGetParameterValueForInsertAction()
        {
            //  Make sure there is a parameter value.
            TaskParameter parameter;
            if (!Parameters.TryGetParameter(Parameter.InsertAction, out parameter) || parameter.Value == null)
            {
                _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                      Message.InsertProduct_DroppedProductBehaviour_Name,
                                      Message.Error_NoValue);
                return false;
            }

            //  Get the parameter value and store it for later use.
            Object parameterValue = parameter.Value.Value1;
            if (Enum.TryParse(Convert.ToString(parameterValue), out _droppedProductBehaviour)) return true;

            //  The parameter had an invalid value.
            _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                  Message.InsertProduct_DroppedProductBehaviour_Name,
                                  parameterValue);
            return false;
        }

        #endregion

        /// <summary>
        ///     Validate the planogram details required by the task.
        /// </summary>
        private Boolean ValidatePlanogram()
        {
            var success = true;

            #region Validate Assorment Data

            // verify we have an assortment
            if ((_taskContext.Planogram.Assortment == null) ||
                (_taskContext.Planogram.Assortment.Products == null) ||
                ((_taskContext.Planogram.Assortment.Products != null) && (_taskContext.Planogram.Assortment.Products.Count == 0)))
            {
                _taskContext.LogError(EventLogEvent.PlanogramNoAssortmentPresent);
                success = false;
            }

            #endregion

            #region Blocking

            if (_taskContext.Planogram.Blocking == null ||
                !_taskContext.Planogram.Blocking.Any())
            {
                _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                      Message.TaskLogging_BlockingStrategy,
                                      Message.TaskLogging_NoBlockingStrategyWasPresent);
                success = false;
            }

            #endregion

            #region Validate Sequence Data

            //  If there is no sequence the task cannot proceed.
            if (_taskContext.Planogram.Sequence == null)
            {
                _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                      Message.TaskLogging_Sequence,
                                      Message.TaskLogging_NoSequenceInformationWasPresent);
                success = false;
            }
            //  OR If there are no sequence groups the task cannot proceed.
            else if (_taskContext.Planogram.Sequence.Groups.Count == 0 ||
                     _taskContext.Planogram.Sequence.Groups.All(g => g.Products.Count == 0))
            {
                _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                      Message.TaskLogging_SequenceProducts,
                                      Message.TaskLogging_NoSequenceProductsWerePresent);
                success = false;
            }

            #endregion

            #region Validate Initial Products and Positions

            //  If there are no products in the planogram the task has nothing to do.
            if (_taskContext.Planogram.Products.Count == 0)
            {
                _taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductsInPlanogram);
                success = false;
            }
            //  OR If there are no positions in the planogram the task has nothing to do.
            else if (_taskContext.Planogram.Positions.Count == 0)
            {
                _taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoPositionsInPlanogram);
                success = false;
            }

            #endregion

            //  return success.
            return success;
        }

        #region Populate Lookups

        /// <summary>
        ///     Populate the lookup lists to be used in the task.
        /// </summary>
        private Boolean PopulateLookups()
        {
            SortGtinToInsert();
            LookupSequenceGroupByGtinToInsert();
            //  Get the existing positions by Gtin.
            _merchandisingGroups = _taskContext.Planogram.GetMerchandisingGroups();

            #region Planogram Blocking

            if (_taskContext.Planogram.Blocking.Any())
            {
                // final, performance, initial
                _planogramBlocking = _taskContext.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                                     _taskContext.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.PerformanceApplied) ??
                                     _taskContext.Planogram.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial) ??
                                     _taskContext.Planogram.Blocking.FirstOrDefault();

                Single planogramBlockingWidth;
                Single planogramBlockingHeight;
                Single planogramBlockingHeightOffset;
                _taskContext.Planogram.GetBlockingAreaSize(out planogramBlockingHeight, out planogramBlockingWidth, out planogramBlockingHeightOffset);

                _planogramBlockingWidth = planogramBlockingWidth;
                _planogramBlockingHeight = planogramBlockingHeight;
                _planogramBlockingHeightOffset = planogramBlockingHeightOffset;

                _designViewPositionByMerchandisingGroup = _merchandisingGroups.ToLookupDictionary(m => m,
                                                                                                  m =>
                                                                                                  new DesignViewPosition(m,
                                                                                                                         planogramBlockingHeightOffset));
            }

            #endregion

            #region Product Stacks

            if (_planogramBlocking != null)
            {
                _productStacksBySequenceColour = _planogramBlocking.GetAndUpdateProductStacksByColour(_merchandisingGroups);
            }

            #endregion

            #region Existing Planogram Positions by GTIN

            //  Lookup all the existing Positions by GTIN.
            _planogramPositionsByGtin = _merchandisingGroups
                .SelectMany(g => g.PositionPlacements)
                .ToLookup(p => p.Product.Gtin)
                .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

            #endregion

            //  Get the initial products in the Planogram.
            IEnumerable<PlanogramProduct> existingProducts = _taskContext
                .Planogram.Products
                .Where(p => _sortedGtinsForProductToInsert.Any(gtin => p.Gtin == gtin))
                .ToList();

            #region Missing Planogram Products

            IEnumerable<String> missingProducts = _sortedGtinsForProductToInsert.Except(existingProducts.Select(p => p.Gtin)).ToList();

            _missingPlanogramProducts = ProductList
                .FetchByEntityIdProductGtins(_taskContext.EntityId, missingProducts)
                .Select(PlanogramProduct.NewPlanogramProduct)
                .ToList();

            if (missingProducts.Any())
            {
                IEnumerable<String> missingDatabaseProducts = missingProducts.Except(_missingPlanogramProducts.Select(p => p.Gtin)).ToList();

                //  Log any missing products if there are any.
                if (missingDatabaseProducts.Any())
                    _taskContext.LogWarning(EventLogEvent.PlanogramProductDoesNotExist, String.Join(", ", missingDatabaseProducts));
            }

            //  Add any missing products into the planogram products.
            _taskContext.Planogram.Products.AddRange(_missingPlanogramProducts);

            //  Update the existing products to include the ones that just got added.
            existingProducts = existingProducts.Union(_missingPlanogramProducts);

            #endregion

            #region Planogram Products to Insert

            //  Get the products that should be inserted.
            _planogramProductsToInsert = _sortedGtinsForProductToInsert
                .Select(gtin => existingProducts.FirstOrDefault(p => p.Gtin == gtin))
                .Where(p => p != null)
                .ToList();

            //  Remove any already placed products from the ToInsert list.
            _planogramProductsToInsert.RemoveAll(p => _planogramPositionsByGtin.ContainsKey(p.Gtin));

            #endregion

            #region Target Units by Gtin

            //  Set the target units by GTIN.
            if (_productPlacement == PlacementType.SingleUnit)
            {
                _targetUnitsByGtin = _sortedGtinsForProductToInsert
                    .ToDictionary(gtin => gtin, gtin => (Int16) 1);
            }
            else
            {
                PlanogramAssortmentProductList products = _taskContext.Planogram.Assortment.Products;
                _targetUnitsByGtin = _sortedGtinsForProductToInsert
                    .ToDictionary(gtin => gtin,
                                  gtin => products
                                              .First(p => p.Gtin == gtin)
                                              .Units);
            }

            #endregion

            #region Target Units by sequence colour
            
            //Pre-Populate sequence colours so that we don't loose any if they have no positions
            _targetUnitsBySequenceColour = _taskContext.Planogram.Sequence.Groups.ToDictionary(g => g.Colour, k => new Dictionary<String, Int32>());
            foreach (PlanogramPosition position in _taskContext.Planogram.Positions)
            {
                if (!position.SequenceColour.HasValue) continue;

                Dictionary<String, Int32> targetUnits;
                if (!_targetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                {
                    targetUnits = new Dictionary<String, Int32>();
                    _targetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                }

                String productGtin = position.GetPlanogramProduct().Gtin;
                if (targetUnits.ContainsKey(productGtin)) continue;
                targetUnits.Add(productGtin, position.TotalUnits);
            }

            #endregion

            // Validate if we can make the assumption that the position sequence data is available on the plan.
            _isPositionSequenceDataAvailable = _taskContext.Planogram.Positions.Any(p => p.SequenceColour != null);

            //  Return success.
            return true;
        }

        /// <summary>
        ///     Sort every product Gtin that needs to be inserted.
        /// </summary>
        private void SortGtinToInsert()
        {
            _sortedGtinsForProductToInsert =
                FilterProductsByPlacementOrderType(_taskContext)
                    .OrderBy(p => _taskContext.Planogram.Assortment.GetAssortmentRulePriorityRank(p, _assortmentRuleTypesPriorityOrder))
                    .ThenBy(p => p.Rank)
                    .ThenBy(p => p.Gtin)
                    .Select(p => p.Gtin)
                    .ToList();

            //  Log the number of products we are set to insert initially.
            _taskContext.LogInformation(EventLogEvent.PlanogramProductsToInsert, _sortedGtinsForProductToInsert.Count);
        }

        /// <summary>
        ///     Lookup the sequence groups of all Gtin to be inserted.
        /// </summary>
        private void LookupSequenceGroupByGtinToInsert()
        {
            _sequenceGroupByGtin = new Dictionary<String, List<PlanogramSequenceGroup>>();
            foreach (String gtin in _sortedGtinsForProductToInsert.Where(gtin => !_sequenceGroupByGtin.ContainsKey(gtin)))
            {
                _sequenceGroupByGtin.Add(gtin, _taskContext.Planogram.Sequence.GetSequenceGroups(gtin).ToList());
            }
        }

        #endregion

        /// <summary>
        ///     Filter the assortment products in accordance with the value of the Product Placement Order parameter.
        /// </summary>
        /// <param name="context">The current context for this task's execution.</param>
        /// <returns>An enumeration of <see cref="PlanogramAssortmentProduct"/> items that meet the criteria determined by the Product Placement Order parameter.</returns>
        private IEnumerable<PlanogramAssortmentProduct> FilterProductsByPlacementOrderType(ITaskContext context)
        {
            PlanogramAssortmentProductList allAssortmentProducts = context.Planogram.Assortment.Products;
            IEnumerable<PlanogramSequenceGroup> sequenceGroupsInBlocking = context.Planogram.Blocking
                .SelectMany(blocking => blocking.Groups.Select(group => group.GetPlanogramSequenceGroup()))
                .Where(g => g != null);
            IEnumerable<PlanogramAssortmentProduct> allSequencedAssortmentProducts =
                allAssortmentProducts.Where(product =>
                                            sequenceGroupsInBlocking.Any(sequenceGroup =>
                                                                         sequenceGroup.Products.Any(groupProduct =>
                                                                                                    String.Equals(groupProduct.Gtin, product.Gtin))));
            switch (_productPlacementOrder)
            {
                case PlacementOrderType.RangedInRankOrder:
                    IEnumerable<PlanogramAssortmentProduct> rangedAssortmentProducts = allSequencedAssortmentProducts.Where(p => p.IsRanged);
                    return rangedAssortmentProducts;

                case PlacementOrderType.NotRangedInRankOrder:
                    IEnumerable<PlanogramAssortmentProduct> nonRangedAssortmentProducts = allSequencedAssortmentProducts.Where(p => !p.IsRanged);
                    return nonRangedAssortmentProducts;

                case PlacementOrderType.IsOptionalAndNotRangedInRankOrder:
                    IEnumerable<PlanogramAssortmentProduct> nonRangedOptionalAssortmentProducts =
                        allSequencedAssortmentProducts.Where(p => !p.IsRanged && p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Optional);
                    return nonRangedOptionalAssortmentProducts;

                case PlacementOrderType.InAssortmentRankOrder:
                default:
                    return allSequencedAssortmentProducts;
            }
        }

        #region Insert Products

        private Boolean InsertProducts()
        {
            //  Find the best placement, if any, for each product and insert it.
            //  For each product we need to find the placed previous and next products.
            //  Then decide on what side of which one the product needs to be inserted.
            //  And finally insert it.
            foreach (PlanogramProduct product in _planogramProductsToInsert)
            {
                _currentProduct = product;

                //  Get the target units for the product per assorment.
                _currentTargetUnits = _targetUnitsByGtin[product.Gtin];

                List<PlanogramSequenceGroup> sequenceGroups = _sequenceGroupByGtin[product.Gtin];

                //  Drop the Product if it is not in any Sequence.
                if (!sequenceGroups.Any())
                {
                    //  Warn the user via logging that the product
                    //  was missing from the sequence before moving
                    //  on to the next.
                    _taskContext.LogWarning(EventLogEvent.PlanogramSequenceMissingProduct, product.Gtin, product.Name);
                    _planogramProductsToDrop.Add(_currentProduct);
                    continue;
                }

                // if no blocking group representation then we target all positions on the plan, otherwise we can attempt to filter a sub set of positions
                Boolean hasBlockingGroupRepresentation = false;
                if (_planogramBlocking != null)
                {
                    foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
                    {
                        hasBlockingGroupRepresentation = _planogramBlocking.Groups.Any(p => p.Colour == sequenceGroup.Colour);
                        if (hasBlockingGroupRepresentation) break;
                    }
                }

                Boolean productPlaced;

                if (_planogramBlocking != null)
                {
                    // we have blocking but dont have blocking representation on the plan for the new product
                    if (!hasBlockingGroupRepresentation)
                    {
                        _planogramProductsToDrop.Add(_currentProduct);
                        continue;
                    }

                    productPlaced = TryRelayoutBlockWithNewProduct(_taskContext);
                    if (!productPlaced &&
                        _droppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd)
                    {
                        productPlaced = TryAddProductUsingSequence(product, sequenceGroups);
                    }
                }
                else
                {
                    productPlaced = TryAddProductUsingSequence(product, sequenceGroups);
                }

                if (!productPlaced)
                {
                    _planogramProductsToDrop.Add(_currentProduct);
                    continue;
                }

                if (sequenceGroups.Count > 1)
                {
                    // log the sequence group used to place the multisited item.
                    _taskContext.LogInformation(EventLogEvent.PlanogramSequenceMultiSitedProduct, product.Gtin, _currentSequenceGroup.Name);
                }

                _insertCount++;
            }

            //  Log the information about new products inserted.
            if (_insertCount > 0)
            {
                _taskContext.LogInformation(EventLogEvent.PlanogramPositionsInserted,
                                                             _insertCount,
                                                             Message.TaskLogging_ThePlanogram);
            }

            return true;
        }

        /// <summary>
        /// Adds given product using available sequence groups.
        /// </summary>
        /// <param name="product">The product to add</param>
        /// <param name="sequenceGroups">The list of available sequence groups</param>
        /// <returns>true if the product was placed</returns>
        private Boolean TryAddProductUsingSequence(IPlanogramProductInfo product, IEnumerable<PlanogramSequenceGroup> sequenceGroups)
        {
            var productPlaced = false;
            _anchorPositions = new List<PlanogramPositionPlacementHelper>();

            foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
            {
                Dictionary<String, List<PlanogramPositionPlacement>> productPlacements = _merchandisingGroups
                    .SelectMany(g => g.PositionPlacements)
                    .ToLookup(p => p.Product.Gtin)
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

                if (_isPositionSequenceDataAvailable)
                {
                    // find the positions on the plan that belong to this sequence group
                    productPlacements = GetProductPositionPlacementsForSequenceGroup(productPlacements, sequenceGroup.Colour);
                }

                _anchorPositions.AddRange(
                    sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, productPlacements, true).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
                _anchorPositions.AddRange(
                    sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, productPlacements).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
            }

            //  Drop the Product if there is no 
            //  anchor position for it in the Planogram.
            if (_anchorPositions.Any())
            {
                SortAnchors();

                foreach (PlanogramPositionPlacementHelper currentAnchor in _anchorPositions)
                {
                    _currentSequenceGroup = currentAnchor.PlanogramSequenceGroup;
                    _currentAnchor = currentAnchor.PlanogramPositionPlacement;

                    if (!TryInsertProduct()) continue;

                    productPlaced = true;
                    break;
                }
            }
            return productPlaced;
        }

        private Boolean TryRelayoutBlockWithNewProduct(ITaskContext context)
        {
            var productLayout =
                new PlanogramMerchandisingProductLayout(
                    context.Planogram,
                    _merchandisingGroups,
                    _sequenceGroupByGtin[_currentProduct.Gtin],
                    _planogramBlocking,
                    _planogramBlockingWidth,
                    _planogramBlockingHeight,
                    _planogramBlockingHeightOffset,
                    _currentProduct,
                    _productStacksBySequenceColour,
                    _targetUnitsBySequenceColour,
                    _currentTargetUnits,
                    GetSpaceConstraintType(_spaceConstraint),
                    AssortmentRuleEnforcer);

            Boolean success = productLayout.TryRelayoutBlockWithNewProduct();

            if (success)
            {
                _currentSequenceGroup = productLayout.ProductSequenceGroup;
            }

            return success;
        }

        /// <summary>
        /// Returns a list of product positions that form the sequence group. 
        /// If there are no sequence group positions on the plan, all product placements are considered.
        /// </summary>
        /// <param name="productPositionPlacements">Dictionary of product and position placements on the plan</param>
        /// <param name="sequenceColour">The sequence group colour</param>
        /// <returns></returns>
        private static Dictionary<String, List<PlanogramPositionPlacement>> GetProductPositionPlacementsForSequenceGroup(Dictionary<String, List<PlanogramPositionPlacement>> productPositionPlacements, Int32 sequenceColour)
        {
            var productPlacements = new Dictionary<String, List<PlanogramPositionPlacement>>();

            foreach (String gtin in productPositionPlacements.Keys)
            {
                List<PlanogramPositionPlacement> positionPlacements = productPositionPlacements[gtin].Where(p => p.Position.SequenceColour == sequenceColour).ToList();
                if (positionPlacements.Any())
                {
                    productPlacements.Add(gtin, positionPlacements);
                }
            }

            if (!productPlacements.Any())
            {
                // there are no representation for the sequence group, provide list of all positions on the plan.
                productPlacements = productPositionPlacements;
            }
            return productPlacements;
        }

        /// <summary>
        ///     Sorts the anchors by most available free space in its merchandising group, 
        ///     then by number of positions within the anchor sequence group, then by sequence group name
        /// </summary>
        private void SortAnchors()
        {
            _anchorPositions = _anchorPositions
                .OrderByDescending(
                    pos =>
                    pos.PlanogramPositionPlacement.GetAvailableSpaceOnAxis(pos.PlanogramPositionPlacement.SubComponentPlacement.GetFacingAxis()))
                .ThenBy(pos => pos.PlanogramSequenceGroup.Products.Count)
                .ThenBy(pos => pos.PlanogramSequenceGroup.Name)
                .ToList();
        }

        private Boolean TryInsertProduct()
        {
            _currentAnchor.MerchandisingGroup.BeginEdit();
            if (!TryProductPlacement())
            {
                _currentAnchor.MerchandisingGroup.CancelEdit();
                return false;
            }

            if (_planogramPositionsByGtin.ContainsKey(_currentProduct.Gtin))
            {
                _planogramPositionsByGtin[_currentProduct.Gtin].Add(_currentPosition);
            }
            else
            {
                _planogramPositionsByGtin
                    .Add(_currentProduct.Gtin, new List<PlanogramPositionPlacement> {_currentPosition});
            }

            if (!TryIncreaseUnits())
            {
                _planogramPositionsByGtin.Remove(_currentProduct.Gtin);
                _currentAnchor.MerchandisingGroup.CancelEdit();
                return false;
            }

            _currentAnchor.MerchandisingGroup.Process();
            _currentAnchor.MerchandisingGroup.ApplyEdit();
            return true;
        }

        private Boolean TryProductPlacement()
        {
            PlanogramMerchandisingGroup merchandisingGroup = _currentAnchor.MerchandisingGroup;
            merchandisingGroup.BeginEdit();

            //  Get the axis of insertion.
            _currentSubComponentPlacement = _currentAnchor.SubComponentPlacement;
            _currentAxis = _currentSubComponentPlacement.GetFacingAxis();

            PlanogramPosition newPlacement = PlanogramPosition.NewPlanogramPosition(
                0,
                _currentProduct,
                _currentSubComponentPlacement,
                _currentSequenceGroup.Products.First(p => p.Gtin == _currentProduct.Gtin).SequenceNumber,
                _currentSequenceGroup.Colour);

            _currentPosition = merchandisingGroup.InsertPositionPlacement(
                newPlacement,
                _currentProduct,
                _currentAnchor,
                GetAnchorDirection());
            return true;
        }

        private PlanogramPositionAnchorDirection GetAnchorDirection()
        {
            // Get sequence direction based upon sequence position
            PlanogramSequenceGroupProductList products = _currentSequenceGroup.Products;
            Int32 newSequenceNumber = products.First(p => p.Gtin == _currentProduct.Gtin).SequenceNumber;
            Int32 anchorSequenceNumber =
                products.First(p => p.Gtin == _currentAnchor.Product.Gtin).SequenceNumber;
            Boolean isAfter = newSequenceNumber > anchorSequenceNumber;

            // Get sequence direction based upon current plans blocking
            if (_planogramBlocking != null)
            {
                PlanogramBlockingGroup planogramBlockingGroup = _currentSequenceGroup.GetBlockingGroup(_planogramBlocking);

                if (planogramBlockingGroup != null)
                {
                    Dictionary<Int32, PlanogramSequencedItem<PlanogramSubComponentPlacement>> sequencedSubComponentsById = null;

                    if (!_planogramBlockingGroupSequencedSubComponentsByIdLookup.ContainsKey(planogramBlockingGroup))
                    {
                        ILookup<PlanogramBlockingLocation, PlanogramMerchandisingGroup> intersectingMerchGroupsByLocation =
                            planogramBlockingGroup.GetIntersectingMerchGroupsByLocation(
                                _designViewPositionByMerchandisingGroup,
                                PlanogramMerchandisingBlock.OverlapThreshold,
                                _planogramBlockingWidth,
                                _planogramBlockingHeight);

                        if (intersectingMerchGroupsByLocation.Any())
                        {
                            sequencedSubComponentsById = planogramBlockingGroup
                                .EnumerateSequencableItemsInSequence(
                                    intersectingMerchGroupsByLocation
                                        .SelectMany(g => g)
                                        .Distinct()
                                        .SelectMany(m => m.SubComponentPlacements))
                                .ToDictionary(s => Convert.ToInt32(s.Item.SubComponent.Id), s => s);

                            _planogramBlockingGroupSequencedSubComponentsByIdLookup.Add(planogramBlockingGroup, sequencedSubComponentsById);
                        }
                    }
                    else
                    {
                        sequencedSubComponentsById = _planogramBlockingGroupSequencedSubComponentsByIdLookup[planogramBlockingGroup];
                    }

                    if (sequencedSubComponentsById != null &&
                        sequencedSubComponentsById.ContainsKey(Convert.ToInt32(_currentSubComponentPlacement.SubComponent.Id)))
                    {
                        IEnumerable<PlanogramSequencedPositionPlacement> sequencedPositions = planogramBlockingGroup
                            .EnumerateSubComponentPositionsInSequence(
                                sequencedSubComponentsById[Convert.ToInt32(_currentSubComponentPlacement.SubComponent.Id)],
                                _currentSubComponentPlacement.GetPlanogramPositions());

                        PlanogramSequencedPositionPlacement sequencePositionForAnchorProduct = sequencedPositions.FirstOrDefault(p => p.Position.Id.Equals(_currentAnchor.Position.Id));

                        if (sequencePositionForAnchorProduct != null)
                        {
                            isAfter =
                                planogramBlockingGroup.GetPlacementType(_currentAxis)
                                                      .Invert(sequencePositionForAnchorProduct.GetIsReversed(_currentAxis, planogramBlockingGroup))
                                                      .IsPositive()
                                    ? isAfter
                                    : !isAfter;
                        }
                    }
                }
            }

            switch (_currentAxis)
            {
                case AxisType.X:
                    return isAfter ? PlanogramPositionAnchorDirection.ToRight : PlanogramPositionAnchorDirection.ToLeft;
                case AxisType.Y:
                    return isAfter ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below;
                case AxisType.Z:
                    return isAfter ? PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;
                default:
                    return PlanogramPositionAnchorDirection.ToRight;
            }
        }

        private Boolean TryIncreaseUnits()
        {
            //  Get a new merchandising group with the new inserted position.
            PlanogramMerchandisingGroup merchandisingGroup = _currentPosition.MerchandisingGroup;
            PlanogramPositionPlacement newPlacement = _currentPosition;
            PlanogramPositionPlacement anchor = _currentAnchor;

            //  Check if the Merchandising Strategy for the current axis is manual.
            Boolean isManualMerchandising = IsManualMerchandising(_currentAxis, merchandisingGroup);

            //  Account for Manual Merchandising cases.
            List<PointValue> startingCoordinates = null;
            if (isManualMerchandising)
            {
                //  Save pre existing positions.
                startingCoordinates = merchandisingGroup
                    .PositionPlacements
                    .Where(p => !p.Id.Equals(newPlacement.Id))
                    .OrderBy(p => p.Id)
                    .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                    .ToList();

                CorrectCoordinatesForManualStrategy(newPlacement, anchor);
            }

            //  Set the target units.
            newPlacement.SetUnits(_currentTargetUnits, PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo, AssortmentRuleEnforcer);

            //  Validate the placement, decreasing units if necessary and allowed.
            do
            {
                newPlacement.RecalculateUnits();
                merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                //  Return success if the placement is valid.
                if (ValidatePlacement(isManualMerchandising,
                                      merchandisingGroup,
                                      newPlacement,
                                      startingCoordinates)) return true;

                //  If units must be exact, return failure.
                if (_productPlacement != PlacementType.CloseToAssortmentUnits) return false;
            } while (newPlacement.DecreaseUnits(AssortmentRuleEnforcer));

            return false;
        }

        private static Boolean IsManualMerchandising(AxisType currentAxis,
                                                     PlanogramMerchandisingGroup merchandisingGroup)
        {
            using (new CodePerformanceMetric()) 
            {
                Boolean isManualMerchandising = currentAxis == AxisType.X &&
                                                merchandisingGroup.StrategyX ==
                                                PlanogramSubComponentXMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Y &&
                                                merchandisingGroup.StrategyY ==
                                                PlanogramSubComponentYMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Z &&
                                                merchandisingGroup.StrategyZ ==
                                                PlanogramSubComponentZMerchStrategyType.Manual;
                return isManualMerchandising;
            }
        }

        private void CorrectCoordinatesForManualStrategy(PlanogramPositionPlacement newPlacement,
                                                                PlanogramPositionPlacement anchor)
        {
            switch (GetAnchorDirection())
            {
                case PlanogramPositionAnchorDirection.InFront:
                    newPlacement.Position.X = anchor.Position.X;
                    newPlacement.Position.Y = anchor.Position.Y;
                    newPlacement.Position.Z = anchor.Position.Z + anchor.GetReservedSpace().Depth;
                    break;
                case PlanogramPositionAnchorDirection.Behind:
                    newPlacement.Position.X = anchor.Position.X;
                    newPlacement.Position.Y = anchor.Position.Y;
                    newPlacement.Position.Z = anchor.Position.Z - newPlacement.GetReservedSpace().Depth;
                    break;
                case PlanogramPositionAnchorDirection.Above:
                    newPlacement.Position.X = anchor.Position.X;
                    newPlacement.Position.Y = anchor.Position.Y + anchor.GetReservedSpace().Height;
                    newPlacement.Position.Z = anchor.Position.Z;
                    break;
                case PlanogramPositionAnchorDirection.Below:
                    newPlacement.Position.X = anchor.Position.X;
                    newPlacement.Position.Y = anchor.Position.Y - newPlacement.GetReservedSpace().Height;
                    newPlacement.Position.Z = anchor.Position.Z;
                    break;
                case PlanogramPositionAnchorDirection.ToLeft:
                    newPlacement.Position.X = anchor.Position.X - newPlacement.GetReservedSpace().Width;
                    newPlacement.Position.Y = anchor.Position.Y;
                    newPlacement.Position.Z = anchor.Position.Z;
                    break;
                case PlanogramPositionAnchorDirection.ToRight:
                    newPlacement.Position.X = anchor.Position.X + anchor.GetReservedSpace().Width;
                    newPlacement.Position.Y = anchor.Position.Y;
                    newPlacement.Position.Z = anchor.Position.Z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Boolean ValidatePlacement(Boolean isManualMerchandising,
                                                 PlanogramMerchandisingGroup merchandisingGroup,
                                                 PlanogramPositionPlacement newPlacement,
                                                 IEnumerable<PointValue> startingCoordinates)
        {
            //  Validate the placement.
            if (_droppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd) return true;

            //  Assume the placement is valid.
            var isValidPlacement = true;

            //  Account for manual merchandising strategy.
            if (isManualMerchandising)
            {
                //  Check the pre existing positions did not need to move.
                List<PointValue> finalCoordinates = merchandisingGroup
                    .PositionPlacements
                    .Where(p => !p.Id.Equals(newPlacement.Id))
                    .OrderBy(p => p.Id)
                    .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                    .ToList();
                isValidPlacement = startingCoordinates
                    .Zip(finalCoordinates,
                         (starting, final) =>
                         starting.X.EqualTo(final.X) &&
                         starting.Y.EqualTo(final.Y) &&
                         starting.Z.EqualTo(final.Z)).All(b => b);
            }

            //  Validate the merchandising group can fit the positions.
            isValidPlacement = isValidPlacement &&
                               !newPlacement.MerchandisingGroup.IsOverfilled();

            //  Validate the placement is insisde the merchandisable space.
            isValidPlacement = isValidPlacement &&
                               !newPlacement.IsOutsideMerchandisingSpace();

            //  Validate the merchandising group has not internal overlaps.
            isValidPlacement = isValidPlacement &&
                               !merchandisingGroup.HasCollisionOverlapping();

            //  Return the valid state.
            return isValidPlacement;
        }

        private static PlanogramMerchandisingSpaceConstraintType GetSpaceConstraintType(SpaceConstraintType spaceConstraintType)
        {
            switch (spaceConstraintType)
            {
                case SpaceConstraintType.BlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
                case SpaceConstraintType.BeyondBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace;
                case SpaceConstraintType.MinimumBlockSpace:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation;
                default:
                    return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
            }
        }

        #endregion

        /// <summary>
        ///     Retrieve the car park component if there needs to be one.
        /// </summary>
        /// <remarks>
        ///     If there needs to be a car park component and none exists
        ///     with the provided name a new one will be created.
        /// </remarks>
        private Boolean GetCarParkComponent()
        {
            //  If the task does not need the car park shelf
            //  then there is no need to get it.
            if (_droppedProductBehaviour != DroppedProductBehaviourType.AddToCarParkComponent) return true;

            //  If there are no products to add
            //  then there is no need to get it.
            if (_planogramProductsToDrop.Count == 0) return true;

            //  Get the car park component.
            _carParkMerchandisingGroup = ((TaskContext) _taskContext).GetCarParkMerchandisingGroup(_carParkComponentName,
                                                                                                   _merchandisingGroups,
                                                                                                   _addCarParkTextBox);

            //  Return success.
            return true;
        }

        /// <summary>
        ///     Add positions for all products to be dropped on the car park component.
        /// </summary>
        private Boolean AddPositionsToCarParkComponent()
        {
            //  If dropped positions are not added to the car park
            //  then there is nothing to do.
            if (_droppedProductBehaviour != DroppedProductBehaviourType.AddToCarParkComponent) return true;

            //  If there are no dropped products
            //  then there is nothing to do.
            if (_planogramProductsToDrop.Count == 0) return true;

            //  If there is no car park component
            //  then there is nothing to do.
            if (_carParkMerchandisingGroup == null) return true;

            //  Create a new position in the car park merchandising group
            //  for each product in the list of products to drop.
            foreach (PlanogramProduct product in _planogramProductsToDrop)
            {
                List<PlanogramSequenceGroup> sequenceGroups = _sequenceGroupByGtin[product.Gtin];

                if (sequenceGroups.Any())
                {
                    PlanogramSequenceGroupProduct sequenceProduct = sequenceGroups.First().Products.FirstOrDefault(p => p.Gtin == product.Gtin);
                    if (sequenceProduct != null)
                        _carParkMerchandisingGroup.InsertPositionPlacement(product, sequenceProduct.SequenceNumber, sequenceGroups.First().Colour);
                }
                else
                    _carParkMerchandisingGroup.InsertPositionPlacement(product);
            }

            //  Apply the changes to the car park component.
            _carParkMerchandisingGroup.Process();
            _carParkMerchandisingGroup.ApplyEdit();

            //  Log the information about the products that got placed on the car park component.
            _taskContext.LogInformation(EventLogEvent.PlanogramPositionsInserted, _planogramProductsToDrop.Count, Message.TaskLogging_TheCarparkShelf);

            //  Return success.
            return true;
        }

        /// <summary>
        ///     Remove dropped products from the Planogram's Product List.
        /// </summary>
        private Boolean RemoveProductsFromPlanogram()
        {
            //  If we are not removing products at all
            //  then there is nothing to do.
            if (_droppedProductBehaviour != DroppedProductBehaviourType.DoNotAdd) return true;

            //  If there are no products to drop that need removing
            //  then there is nothing to do.
            List<PlanogramProduct> planogramProductsToRemove = _planogramProductsToDrop.Intersect(_missingPlanogramProducts).ToList();
            if (planogramProductsToRemove.Count == 0) return true;

            // V8-32880 :   This task should not be removing products from the assortment.
            //              Removing them from the product list also removes them from 
            //              the assortment.
            //  Remove dropped products from the planogram.
            //context.Planogram.Products.RemoveList(planogramProductsToRemove);

            //  Log the information about products that could not be inserted.
            _taskContext.LogInformation(EventLogEvent.PlanogramPositionsNotInserted, _planogramProductsToDrop.Count);

            //  Return success.
            return true;
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private void ReMerchandisePlanogram()
        {
            ((TaskContext) _taskContext).RestoreSequenceNumbers();
            ((TaskContext) _taskContext).RemerchandisePlanogram();
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (Parameters == null ||
                Parameters[Parameter.CarParkComponentName] == null ||
                !Parameters.Contains((Int32) Parameter.CarParkComponentName) ||
                Parameters[Parameter.CarParkComponentName].Value == null ||
                Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
                return Message.NullComponentName;
            return Parameters[Parameter.CarParkComponentName].Value.Value1.ToString().Length > PlanogramComponent.MaximumComponentNameLength ? Message.IsCarParkNameValid_NotValid_ErrorMessage : null;
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDispose()
        {
            if (_merchandisingGroups != null) _merchandisingGroups.Dispose();

            base.OnDispose();
        }

        #endregion
    }
}