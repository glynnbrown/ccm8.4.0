﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29105 : A.Silva
//  Created
// V8-29424 : A.Silva
//  Refactored to extract common functionality.
// V8-29423 : A.Silva
//  Amended OnExecute to pass PlacementType.SingleFacing as default.
//  Amended DroppedProductBehaviour.
// V8-29516 : A.Silva
//  Amended OnExecute to use the current product instead of trying to retrieve it from an invalid position.
//  Amended OnExecute to remove any products from the Product List if they had been added previously but not placed.

#endregion

#region Version History: CCM803

// V8-29554 : A.Silva
//  Refactored to bring in line with the other tasks.
// V8-29551 : A.Silva
//  Added some metric reporting.
// V8-29768: A.Silva
//  Refactored UpdateSequenceData call as it is now in the base TaskContext.

#endregion

#region Version History: CCM810

// V8-29902 : A.Silva
//  Added CarParkComponentName and changed how the car park shelf is used in the task.
// V8-29868 : A.Silva
//  Added logging of already placed products that will not be inserted and warn the user if all the products to isnert are already placed.
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
// V8-30144 : D.Pleasance
//  Amended so that MerchandisingGroups are obtained on the task context and disposed when the task is completed. This is to ensure that positions 
//  are not removed when previously looking up the car park shelf. Previously the car park shelf was found, then dispose was called immediately 
//  after which resulting in existing car park placement positions being removed.
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30141 : A.Silva
//  Refactored use of GetAvailableSpaceOnAxis.

#endregion

#region Version History: CCM820
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
//  Amended so that positions can be multisited, and positions are placed for each sequence group allocation.
//  GetAnchorDirection also amended to calculate sequence direction based upon blocking of the plan. Simple example of the change is to imagine the plans 
//  blocking being left to right and a product is listed to come after in the sequence but is sitting on a block that is right to left in the block sequence. Direction inverted.
// V8-31284 : D.Pleasance
// Re-factored optimisation logic into new Merchandise Products Using List task.
// V8-31409 : D.Pleasance
//  Car park items also have sequence number \ sequence colour applied if possible.
#endregion 

#region Version History : CCM830
// V8-31561 : A.Kuszyk
//  Ensured order of products is taken from parameter, not arbitrarily by Gtin.
// V8-32012 : A.Kuszyk
//  Amended logging messages when no products are supplied.
// V8-32371 : D.Pleasance
//  Added product.Gtin detail to event log message EventLogEvent.PlanogramSequenceMissingProduct
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// V8-32787 : A.Silva
//  Amended TryIncreaseUnits to enforce Assortment Rules.
//  Added an AssortmentRuleEnforcer field to allow easy enabling of rule enforcing.
// CCM-18435 : A.Silva
//  Refactored by pulling up HasOverlap to PlanogramMerchandisingGroup.HasCollisionOverlapping.
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.

#endregion

#region Version History : CCM832
// CCM-18923 : G.Richards
//  Products that were not added to the planogram were still been added to the planograms performance data. This caused a conflict with a foreign key constraint because the products didn’t exist with the planograms product data. The task now ensures that any products that were not successfully added are also removed from the planogram performance product list.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.InsertProduct.v1
{
    /// <summary>
    /// Inserts one or more new products into a planogram, placing them in a suitable location based on the planogram's Sequence Strategies.
    /// </summary>
    [Serializable]
    public sealed class Task : TaskBase
    {
        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();

        #region Version

        /// <summary>
        ///     This instance's task major version number.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        /// <summary>
        ///     This instance's task minor version number.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        #endregion

        #region Parameter IDs

        /// <summary>
        ///     Enumeration of parameter IDs for this task.
        /// </summary>
        private enum Parameter
        {
            ProductCode = 0,
            DroppedProductBehaviour = 1,
            CarParkComponentName = 2,
            AddCarParkTextBox = 3,
        }

        #endregion

        #region Properties

        /// <summary>
        ///     The task name.
        /// </summary>
        public override String Name { get { return Message.InsertProduct_Name; } }

        /// <summary>
        ///     The task description.
        /// </summary>
        public override String Description { get { return Message.InsertProduct_Description; } }

        /// <summary>
        ///     The task category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_MerchandisePlanogram; } }

        /// <summary>
        ///     The task minor version number.
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     The task major version number.
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        #endregion

        #region Parameter Registration

        /// <summary>
        ///     Called when parameters are being registered for the task.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            //Product code:
            RegisterParameter(new TaskParameter(
                id: Parameter.ProductCode,
                name: Message.InsertProduct_ProductCode_Name,
                description: Message.InsertProduct_ProductCode_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.ProductCodeMultiple,
                defaultValue: null,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));

            //NewDroppedProductBehaviour:
            RegisterParameter(new TaskParameter(
                id: Parameter.DroppedProductBehaviour,
                name: Message.InsertProduct_DroppedProductBehaviour_Name,
                description: Message.InsertProduct_DroppedProductBehaviour_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(DroppedProductBehaviourType),
                defaultValue: DroppedProductBehaviourType.AddToCarparkShelf,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));

            //CarParkComponentName
            RegisterParameter(new TaskParameter(
                id: Parameter.CarParkComponentName,
                name: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name,
                description: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.String,
                defaultValue: Message.InserProductFromAssortment_Parameter_CarParkComponentName_Default,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true,
                parentId: null,
                isRequired: null,
                isNullAllowed: null,
                isValid: () => IsCarParkNameValid()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.InsertProduct_Parameter_AddCarParkTextBox_Name,
                Message.InsertProduct_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }


        #endregion

        #region Execution
        /// <summary>
        ///     Executes the <see cref="Task"/> using the provided <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The <see cref="ITaskContext"/> for this <see cref="Task"/>.</param>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!PopulateLookups(taskContext)) return;
                if (!InsertProducts(taskContext)) return;
                if (!GetCarParkComponent(taskContext)) return;
                if (!AddPositionsToCarParkComponent(taskContext)) return;
                if (!RemoveProductsFromPlanogram(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Validate the parameters for this instance.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to store the validated parameters in.</param>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region DroppedProductBehaviour

                //  Validate that the Dropped Product Behaviour parameter exists.
                if (Parameters[Parameter.DroppedProductBehaviour].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProduct_DroppedProductBehaviour_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.DroppedProductBehaviour =
                            (DroppedProductBehaviourType)Parameters[Parameter.DroppedProductBehaviour].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProduct_DroppedProductBehaviour_Name,
                                         Parameters[Parameter.DroppedProductBehaviour].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProductCode

                //  Validate that the Product Code parameter exists.
                if (Parameters[Parameter.ProductCode].Values == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProduct_ProductCode_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        //  Get the GTINs in order.
                        //  We don't order the gtins here, because the user can specify the order of the products in the UI.
                        context.GtinsInOrder = Parameters[Parameter.ProductCode].Values.Select(o => Convert.ToString(o.Value1)).ToList();
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProduct_ProductCode_Name,
                                         Parameters[Parameter.ProductCode].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.InserProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.InsertProduct_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.InsertProduct_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        ///     Validate the planogram details required by the task.
        /// </summary>
        private static Boolean ValidatePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region Gtins to add

                if (!context.GtinsInOrder.Any())
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductCodesSupplied);
                    return false;
                }

                #endregion

                #region Validate Sequence Data

                //  If there is no sequence the task cannot proceed.
                if (context.Planogram.Sequence == null)
                {
                    context.LogInformation(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_Sequence,
                                     Message.TaskLogging_NoSequenceInformationWasPresent);
                    success = false;
                }
                //  OR If there are no sequence groups the task cannot proceed.
                else if (context.Planogram.Sequence.Groups.Count == 0 ||
                         context.Planogram.Sequence.Groups.All(g => g.Products.Count == 0))
                {
                    context.LogInformation(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_SequenceProducts,
                                     Message.TaskLogging_NoSequenceProductsWerePresent);
                    success = false;
                }

                #endregion

                #region Validate Initial Products and Positions

                //  If there are no products in the planogram the task has nothing to do.
                if (context.Planogram.Products.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductsInPlanogram);
                    success = false;
                }
                //  OR If there are no positions in the planogram the task has nothing to do.
                else if (context.Planogram.Positions.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoPositionsInPlanogram);
                    success = false;
                }

                //  Log any required inserts that are already in the Planogram.
                #endregion

                //  return success.
                return success;
            }
        }

        /// <summary>
        ///     Populate the lookup lists to be used in the task.
        /// </summary>
        private static Boolean PopulateLookups(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region GTINs, in order (obtained from the task parameter)

                //  Log the number of products we are set to insert initially.
                context.LogInformation(EventLogEvent.PlanogramProductsToInsert, context.GtinsInOrder.Count);

                #endregion

                #region Sequence Groups by GTIN
                
                //  Sequence groups by GTIN.
                context.SequenceGroupByGtin = new Dictionary<String, List<PlanogramSequenceGroup>>();
                foreach (String gtin in context.GtinsInOrder)
                {
                    if (!context.SequenceGroupByGtin.ContainsKey(gtin))
                    {
                        context.SequenceGroupByGtin.Add(gtin, context.Planogram.Sequence.GetSequenceGroups(gtin).ToList());
                    }
                }

                #endregion

                #region Merchandising Groups

                //  Get the existing positions by Gtin.
                context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();

                #endregion

                #region Existing Planogram Positions by GTIN

                //  Lookup all the existing Positions by GTIN.
                context.PlanogramPositionsByGtin = context
                    .MerchandisingGroups
                    .SelectMany(g => g.PositionPlacements)
                    .ToLookup(p => p.Product.Gtin)
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

                #endregion

                //  Get the initial products in the Planogram.
                IEnumerable<PlanogramProduct> existingProducts = context
                    .Planogram.Products
                    .Where(p => context.GtinsInOrder.Any(gtin => p.Gtin == gtin))
                    .ToList();

                #region Missing Planogram Products

                context.MissingPlanogramProducts = ProductList
                    .FetchByEntityIdProductGtins(context.EntityId,
                                                 context.GtinsInOrder
                                                        .Except(existingProducts.Select(p => p.Gtin)))
                    .Select(PlanogramProduct.NewPlanogramProduct)
                    .ToList();

                //  Add any missing products into the planogram products.
                context.Planogram.Products.AddRange(context.MissingPlanogramProducts);

                //  Update the existing products to include the ones that just got added.
                existingProducts = existingProducts.Union(context.MissingPlanogramProducts);

                #endregion

                #region Planogram Products to Insert

                //  Get the products that should be inserted.
                context.PlanogramProductsToInsert = context
                    .GtinsInOrder
                    .Select(gtin => existingProducts.FirstOrDefault(p => p.Gtin == gtin))
                    .Where(p => p != null)
                    .ToList();

                //  Remove any already placed products from the ToInsert list.
                ICollection<PlanogramProduct> alreadyPlacedProducts = context.PlanogramProductsToInsert.Where(p => context.PlanogramPositionsByGtin.ContainsKey(p.Gtin)).ToList();
                foreach (PlanogramProduct placedProduct in alreadyPlacedProducts)
                {
                    context.PlanogramProductsToInsert.Remove(placedProduct);
                }

                // Log how many products were already placed.
                if (alreadyPlacedProducts.Count > 0)
                {
                    context.LogInformation(EventLogEvent.PlanogramProductsExistInPlanogram, alreadyPlacedProducts.Count);
                }

                //  Check that the task has products to insert.
                if (context.PlanogramProductsToInsert.Count == 0)
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_AllProductsAlreadyPlaced);
                    return false;
                }

                #endregion

                #region Target Units by Gtin

                //  NB Always place one single facing.
                //  Set the target units by GTIN.
                //if (context.ProductPlacement == PlacementType.SingleFacing)
                //{
                context.TargetUnitsByGtin = context
                    .GtinsInOrder
                    .ToDictionary(gtin => gtin, gtin => (Int16)1);
                //}
                //else
                //{
                //    PlanogramAssortmentProductList products = context.Planogram.Assortment.Products;
                //    context.TargetUnitsByGtin = context
                //        .GtinsInOrder
                //        .ToDictionary(gtin => gtin,
                //                      gtin => products
                //                                  .First(p => p.GTIN == gtin)
                //                                  .Units);
                //}

                #endregion
                                
                //  Return success.
                return true;
            }
        }

        #region Insert Products

        private static Boolean InsertProducts(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Find the best placement, if any, for each product and insert it.
                //  For each product we need to find the placed previous and next products.
                //  Then decide on what side of which one the product needs to be inserted.
                //  And finally insert it.
                foreach (PlanogramProduct product in context.PlanogramProductsToInsert)
                {
                    context.CurrentProduct = product;

                    //  Get the target units for the product per assorment.
                    context.CurrentTargetUnits = context.TargetUnitsByGtin[product.Gtin];

                    List<PlanogramSequenceGroup> sequenceGroups = context.SequenceGroupByGtin[product.Gtin];

                    //  Drop the Product if it is not in any Sequence.
                    if (!sequenceGroups.Any())
                    {
                        //  Warn the user via logging that the product
                        //  was missing from the sequence before moving
                        //  on to the next.
                        context.LogWarning(EventLogEvent.PlanogramSequenceMissingProduct, product.Gtin, product.Name);
                        context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                        continue;
                    }

                    context.AnchorPositions = new List<PlanogramPositionPlacementHelper>();

                    foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
                    {
                        context.AnchorPositions.AddRange(
                            sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, context.PlanogramPositionsByGtin, true).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
                        context.AnchorPositions.AddRange(
                            sequenceGroup.GetAdjacentPositionsBySequence(product.Gtin, context.PlanogramPositionsByGtin).ToList().Select(p => new PlanogramPositionPlacementHelper(p, sequenceGroup)));
                    }

                    //  Drop the Product if there is no 
                    //  anchor position for it in the Planogram.
                    if (!context.AnchorPositions.Any())
                    {
                        context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                        continue;
                    }

                    SortAnchors(context);

                    Boolean productPlaced = false;                    
                    foreach (PlanogramPositionPlacementHelper currentAnchor in context.AnchorPositions)
                    {
                        context.CurrentSequenceGroup = currentAnchor.PlanogramSequenceGroup;
                        context.CurrentAnchor = currentAnchor.PlanogramPositionPlacement;
                        if (TryInsertProduct(context))
                        {
                            if (sequenceGroups.Count > 1)
                            {
                                // log the sequence group used to place the multisited item.
                                context.LogInformation(EventLogEvent.PlanogramSequenceMultiSitedProduct, product.Gtin, context.CurrentSequenceGroup.Name);
                            }

                            productPlaced = true;
                            break;
                        }
                    }

                    if (!productPlaced)
                    {
                        context.PlanogramProductsToDrop.Add(context.CurrentProduct);
                        continue;
                    }

                    context.InsertCount++;
                }
                
                //  Log the information about new products inserted.
                if (context.InsertCount > 0)
                {
                    context.LogInformation(EventLogEvent.PlanogramPositionsInserted,
                                           context.InsertCount,
                                           Message.TaskLogging_ThePlanogram);
                }

                return true;
            }
        }

        /// <summary>
        ///     Sorts the anchors by number of positions within the anchor sequence group, then by sequence group name,
        ///     then by most available free space in its merchandising group.
        /// </summary>
        /// <param name="context">The current task context</param>
        private static void SortAnchors(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.AnchorPositions = context.AnchorPositions
                    .OrderBy(pos => pos.PlanogramSequenceGroup.Products.Count)
                    .ThenByDescending(pos => pos.PlanogramPositionPlacement.GetAvailableSpaceOnAxis(pos.PlanogramPositionPlacement.SubComponentPlacement.GetFacingAxis()))
                    .ThenBy(pos => pos.PlanogramSequenceGroup.Name)
                    .ToList();
            }
        }

        private static Boolean TryInsertProduct(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.CurrentAnchor.MerchandisingGroup.BeginEdit();
                if (!TryProductPlacement(context))
                {
                    context.CurrentAnchor.MerchandisingGroup.CancelEdit();
                    return false;
                }

                if (context.PlanogramPositionsByGtin.ContainsKey(context.CurrentProduct.Gtin))
                {
                    context.PlanogramPositionsByGtin[context.CurrentProduct.Gtin].Add(context.CurrentPosition);
                }
                else
                {
                    context.PlanogramPositionsByGtin
                           .Add(context.CurrentProduct.Gtin, new List<PlanogramPositionPlacement> { context.CurrentPosition });
                }

                if (!TryIncreaseUnits(context))
                {
                    context.PlanogramPositionsByGtin.Remove(context.CurrentProduct.Gtin);
                    context.CurrentAnchor.MerchandisingGroup.CancelEdit();
                    return false;
                }
                else
                {
                    context.CurrentAnchor.MerchandisingGroup.Process();
                    context.CurrentAnchor.MerchandisingGroup.ApplyEdit();
                }
                return true;
            }
        }

        private static Boolean TryProductPlacement(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                PlanogramMerchandisingGroup merchandisingGroup = context.CurrentAnchor.MerchandisingGroup;
                merchandisingGroup.BeginEdit();

                //  Get the axis of insertion.
                context.CurrentSubComponentPlacement = context.CurrentAnchor.SubComponentPlacement;
                context.CurrentAxis = context.CurrentSubComponentPlacement.GetFacingAxis();

                PlanogramPosition newPlacement = PlanogramPosition.NewPlanogramPosition(
                    0, context.CurrentProduct, 
                    context.CurrentSubComponentPlacement,
                    context.CurrentSequenceGroup.Products.First(p => p.Gtin == context.CurrentProduct.Gtin).SequenceNumber,
                    context.CurrentSequenceGroup.Colour);

                context.CurrentPosition = merchandisingGroup.InsertPositionPlacement(
                    newPlacement,
                    context.CurrentProduct,
                    context.CurrentAnchor,
                    GetAnchorDirection(context));
                return true;
            }
        }

        private static PlanogramPositionAnchorDirection GetAnchorDirection(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get sequence direction based upon sequence position
                PlanogramSequenceGroupProductList products = context.CurrentSequenceGroup.Products;
                Int32 newSequenceNumber = products.First(p => p.Gtin == context.CurrentProduct.Gtin).SequenceNumber;
                Int32 anchorSequenceNumber =
                    products.First(p => p.Gtin == context.CurrentAnchor.Product.Gtin).SequenceNumber;
                Boolean isAfter = newSequenceNumber > anchorSequenceNumber;
                
                switch (context.CurrentAxis)
                {
                    case AxisType.X:
                        return isAfter ? PlanogramPositionAnchorDirection.ToRight : PlanogramPositionAnchorDirection.ToLeft;
                    case AxisType.Y:
                        return isAfter ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below;
                    case AxisType.Z:
                        return isAfter ? PlanogramPositionAnchorDirection.InFront : PlanogramPositionAnchorDirection.Behind;
                    default:
                        return PlanogramPositionAnchorDirection.ToRight;
                }
            }
        }

        private static Boolean TryIncreaseUnits(TaskContext context)
        {
            using (new CodePerformanceMetric()) 
            {
                //  Get a new merchandising group with the new inserted position.
                PlanogramMerchandisingGroup merchandisingGroup = context.CurrentPosition.MerchandisingGroup;
                PlanogramPositionPlacement newPlacement = context.CurrentPosition;
                PlanogramPositionPlacement anchor = context.CurrentAnchor;

                //  Check if the Merchandising Strategy for the current axis is manual.
                Boolean isManualMerchandising = IsManualMerchandising(context.CurrentAxis, merchandisingGroup);

                //  Account for Manual Merchandising cases.
                List<PointValue> startingCoordinates = null;
                if (isManualMerchandising)
                {
                    //  Save pre existing positions.
                    startingCoordinates = merchandisingGroup
                        .PositionPlacements
                        .Where(p => !p.Id.Equals(newPlacement.Id))
                        .OrderBy(p => p.Id)
                        .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                        .ToList();

                    CorrectCoordinatesForManualStrategy(context, newPlacement, anchor);
                }

                //  Set the target units.
                newPlacement.SetUnits(context.CurrentTargetUnits, PlanogramPositionInventoryTargetType.GreaterThanOrEqualTo, AssortmentRuleEnforcer);

                //  Validate the placement, decreasing units if necessary and allowed.
                do
                {
                    newPlacement.RecalculateUnits();
                    merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                    //  Return success if the placement is valid.
                    if (ValidatePlacement(context,
                                          isManualMerchandising,
                                          merchandisingGroup,
                                          newPlacement,
                                          startingCoordinates)) return true;

                    //  If units must be exact, return failure.
                    //  NB At the time, units always must be exact, no decreasing allowed.
                    //if (context.ProductPlacement != PlacementType.CloseToAssortmentFacings) return false;
                    return false;

                } while (newPlacement.DecreaseUnits(AssortmentRuleEnforcer));

                return false;
            }
        }

        private static Boolean IsManualMerchandising(AxisType currentAxis,
                                                     PlanogramMerchandisingGroup merchandisingGroup)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean isManualMerchandising = currentAxis == AxisType.X &&
                                                merchandisingGroup.StrategyX ==
                                                PlanogramSubComponentXMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Y &&
                                                merchandisingGroup.StrategyY ==
                                                PlanogramSubComponentYMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Z &&
                                                merchandisingGroup.StrategyZ ==
                                                PlanogramSubComponentZMerchStrategyType.Manual;
                return isManualMerchandising;
            }
        }

        private static void CorrectCoordinatesForManualStrategy(TaskContext context,
                                                                PlanogramPositionPlacement newPlacement,
                                                                PlanogramPositionPlacement anchor)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                switch (GetAnchorDirection(context))
                {
                    case PlanogramPositionAnchorDirection.InFront:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z + anchor.GetReservedSpace().Depth;
                        break;
                    case PlanogramPositionAnchorDirection.Behind:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z - newPlacement.GetReservedSpace().Depth;
                        break;
                    case PlanogramPositionAnchorDirection.Above:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y + anchor.GetReservedSpace().Height;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.Below:
                        newPlacement.Position.X = anchor.Position.X;
                        newPlacement.Position.Y = anchor.Position.Y - newPlacement.GetReservedSpace().Height;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.ToLeft:
                        newPlacement.Position.X = anchor.Position.X - newPlacement.GetReservedSpace().Width;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    case PlanogramPositionAnchorDirection.ToRight:
                        newPlacement.Position.X = anchor.Position.X + anchor.GetReservedSpace().Width;
                        newPlacement.Position.Y = anchor.Position.Y;
                        newPlacement.Position.Z = anchor.Position.Z;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static Boolean ValidatePlacement(TaskContext context,
                                                 Boolean isManualMerchandising,
                                                 PlanogramMerchandisingGroup merchandisingGroup,
                                                 PlanogramPositionPlacement newPlacement,
                                                 IEnumerable<PointValue> startingCoordinates)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Validate the placement.
                if (context.DroppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd) return true;

                //  Assume the placement is valid.
                var isValidPlacement = true;

                //  Account for manual merchandising strategy.
                if (isManualMerchandising)
                {
                    //  Check the pre existing positions did not need to move.
                    List<PointValue> finalCoordinates = merchandisingGroup
                        .PositionPlacements
                        .Where(p => !p.Id.Equals(newPlacement.Id))
                        .OrderBy(p => p.Id)
                        .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                        .ToList();
                    isValidPlacement = startingCoordinates
                        .Zip(finalCoordinates,
                             (starting, final) =>
                             starting.X.EqualTo(final.X) &&
                             starting.Y.EqualTo(final.Y) &&
                             starting.Z.EqualTo(final.Z)).All(b => b);
                }

                //  Validate the merchandising group can fit the positions.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.MerchandisingGroup.IsOverfilled();

                //  Validate the placement is insisde the merchandisable space.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.IsOutsideMerchandisingSpace();

                //  Validate the merchandising group has not internal overlaps.
                isValidPlacement = isValidPlacement &&
                                   !merchandisingGroup.HasCollisionOverlapping();

                //  Return the valid state.
                return isValidPlacement;
            }
        }

        #endregion

        /// <summary>
        ///     Retrieve the car park component if there needs to be one.
        /// </summary>
        /// <remarks>
        ///     If there needs to be a car park component and none exists
        ///     with the provided name a new one will be created.
        /// </remarks>
        private static Boolean GetCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If the task does not need the car park shelf
                //  then there is no need to get it.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.AddToCarparkShelf) return true;

                //  If there are no products to add
                //  then there is no need to get it.
                if (context.PlanogramProductsToDrop.Count == 0) return true;

                //  Get the car park component.
                context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

                //  Return success.
                return true;
            }
        }

        /// <summary>
        ///     Add positions for all products to be dropped on the car park component.
        /// </summary>
        private static Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If dropped positions are not added to the car park
                //  then there is nothing to do.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.AddToCarparkShelf) return true;

                //  If there are no dropped products
                //  then there is nothing to do.
                if (context.PlanogramProductsToDrop.Count == 0) return true;

                //  If there is no car park component
                //  then there is nothing to do.
                if (context.CarParkMerchandisingGroup == null) return true;

                //  Create a new position in the car park merchandising group
                //  for each product in the list of products to drop.
                foreach (PlanogramProduct product in context.PlanogramProductsToDrop)
                {
                    List<PlanogramSequenceGroup> sequenceGroups = context.SequenceGroupByGtin[product.Gtin];

                    if (sequenceGroups.Any())
                    {
                        PlanogramSequenceGroupProduct sequenceProduct = sequenceGroups.First().Products.FirstOrDefault(p => p.Gtin == product.Gtin);
                        if (sequenceProduct != null)
                        {
                            context.CarParkMerchandisingGroup.InsertPositionPlacement(product, sequenceProduct.SequenceNumber, sequenceGroups.First().Colour);
                        }
                    }
                    else
                    {
                        context.CarParkMerchandisingGroup.InsertPositionPlacement(product);
                    }
                }

                //  Apply the changes to the car park component.
                context.CarParkMerchandisingGroup.Process();
                context.CarParkMerchandisingGroup.ApplyEdit();

                //  Log the information about the products that got placed on the car park component.
                context.LogInformation(EventLogEvent.PlanogramPositionsInserted,
                                       context.PlanogramProductsToDrop.Count,
                                       Message.TaskLogging_TheCarparkShelf);

                //  Return success.
                return true;
            }
        }

        /// <summary>
        ///     Remove dropped products and the related performance data from the planogram's product to drop list.
        /// </summary>
        private static Boolean RemoveProductsFromPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  If we are not removing products at all
                //  then there is nothing to do.
                if (context.DroppedProductBehaviour != DroppedProductBehaviourType.DoNotAdd) return true;

                //  If there are no products to drop that need removing
                //  then there is nothing to do.
                List<PlanogramProduct> planogramProductsToRemove = context.PlanogramProductsToDrop.Intersect(context.MissingPlanogramProducts).ToList();
                if (planogramProductsToRemove.Count == 0) return true;

                //  Remove dropped products from the planogram.
                context.Planogram.Products.RemoveList(planogramProductsToRemove);

                //  Select the related performance data for the products to be removed
                List<PlanogramPerformanceData> planogramPerformanceProductsToRemove = context.Planogram.Performance.PerformanceData.Where(w => planogramProductsToRemove.Select(p => p.Id).Contains(w.PlanogramProductId)).ToList();

                //  Remove dropped products from the performance data.
                context.Planogram.Performance.PerformanceData.RemoveList(planogramPerformanceProductsToRemove);

                //  Log the information about products that could not be inserted.
                context.LogInformation(EventLogEvent.PlanogramPositionsNotInserted, context.PlanogramProductsToDrop.Count);

                //  Return success.
                return true;
            }
        }
        
        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // check that some positions have either been inserted.
                if (context.InsertCount == 0) return true;

                // remerchandise the whole planogram.
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}