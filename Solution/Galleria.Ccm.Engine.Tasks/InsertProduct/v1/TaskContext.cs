﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803

// V8-29554 : A.Silva
//  Created

#endregion

#region Version History: CCM810

// V8-29902 : A.Silva
//  Added CarParkComponentName.

#endregion

#region Version History: CCM820
// V8-30760 : D.Pleasance
//  Added ProductPlacementOrder.
// V8-31176 : D.Pleasance
//  Added PlanogramBlocking, PlanogramBlockingWidth, PlanogramBlockingHeight, MerchandisingGroupsAndDvps, PlanogramBlockingGroupSequencedSubComponentsByIdLookup,
//        IsPositionSequenceDataAvailable
// V8-31284 : D.Pleasance
//  Re-factored optimisation logic into new Merchandise Products Using Assortment task.
#endregion

#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.InsertProduct.v1
{
    /// <summary>
    ///     Provides the context to run <see cref="Task"/> in.
    /// </summary>
    public class TaskContext : Ccm.Engine.TaskContext
    {
        private List<PlanogramProduct> _planogramProductsToDrop;

        #region Constructor

        /// <summary>
        ///     Instantiates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) {}

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the behaviour when coming across dropped products.
        /// </summary>
        public DroppedProductBehaviourType DroppedProductBehaviour { get; set; }

        /// <summary>
        ///     Gets or sets the name for the car park component.
        /// </summary>
        public String CarParkComponentName { get; set; }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramMerchandisingGroup"/> that contains the car park component.
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup { get; set; }

        /// <summary>
        ///     Gets the collection of <see cref="PlanogramProduct"/> that will be dropped.
        /// </summary>
        public List<PlanogramProduct> PlanogramProductsToDrop
        {
            get { return _planogramProductsToDrop ?? (_planogramProductsToDrop = new List<PlanogramProduct>()); }
        }

        /// <summary>
        ///     Gets or sets the list of GTINs passed as a parameter, ordered by the order they were provided in.
        /// </summary>
        public List<String> GtinsInOrder { get; set; }

        /// <summary>
        ///     Gets or sets the lookup dictionary of <see cref="PlanogramSequenceGroup"/> instances by GTIN.
        /// </summary>
        public Dictionary<String, List<PlanogramSequenceGroup>> SequenceGroupByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramMerchandisingGroup"/> instances present in the Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        ///     Gets or sets the lookup of all positions in the planogram by GTIN.
        /// </summary>
        public Dictionary<String, List<PlanogramPositionPlacement>> PlanogramPositionsByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> instances that 
        ///     are missing from the Planogram's Product List.
        /// </summary>
        public List<PlanogramProduct> MissingPlanogramProducts { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> instances that 
        ///     are to be inserted into the Planogram.
        /// </summary>
        public List<PlanogramProduct> PlanogramProductsToInsert { get; set; }

        /// <summary>
        ///     Gets or sets the lookup of target units to place by GTIN.
        /// </summary>
        public Dictionary<String, Int16> TargetUnitsByGtin { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramProduct"/> that is being processed currently.
        /// </summary>
        public PlanogramProduct CurrentProduct { get; set; }

        /// <summary>
        ///     Gets or sets the current target units for the current product.
        /// </summary>
        public Int32 CurrentTargetUnits { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramSequenceGroup"/> instance that is currently being processed.
        /// </summary>
        public PlanogramSequenceGroup CurrentSequenceGroup { get; set; }

        /// <summary>
        ///     Gets or sets the available anchor positions available for the current product to be inserted.
        /// </summary>
        public List<PlanogramPositionPlacementHelper> AnchorPositions { get; set; }
        
        /// <summary>
        ///     Gets or sets the current anchor <see cref="PlanogramPositionPlacement"/> for the current product to be inserted.
        /// </summary>
        public PlanogramPositionPlacement CurrentAnchor { get; set; }

        /// <summary>
        ///     Gets or sets the current product inserted count.
        /// </summary>
        public Int32 InsertCount { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramPositionPlacement"/> that is being processed currently.
        /// </summary>
        public PlanogramPositionPlacement CurrentPosition { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="PlanogramSubComponentPlacement"/> that is being processed currently.
        /// </summary>
        public PlanogramSubComponentPlacement CurrentSubComponentPlacement { get; set; }

        /// <summary>
        ///     Gets or sets the current axis of insertion.
        /// </summary>
        public AxisType CurrentAxis { get; set; }
        
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }

        #endregion
    }

    public class PlanogramPositionPlacementHelper
    {
        public PlanogramPositionPlacement PlanogramPositionPlacement { get; set; }
        public PlanogramSequenceGroup PlanogramSequenceGroup { get; set; }

        public PlanogramPositionPlacementHelper(PlanogramPositionPlacement planogramPositionPlacement, PlanogramSequenceGroup planogramSequenceGroup)
        {
            PlanogramPositionPlacement = planogramPositionPlacement;
            PlanogramSequenceGroup = planogramSequenceGroup;
        }
    }
}