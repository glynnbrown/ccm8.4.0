﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32519 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.InsertProduct.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// add to AddCarParkTextBox param
    /// </summary>
    public enum AddCarParkTextBoxType
    {
        Yes,
        No
    }

    /// <summary>
    /// Contains helpers for the AddCarParkTextBoxType enum.
    /// </summary>
    public static class AddCarParkTextBoxTypeHelper
    {
        public static readonly Dictionary<AddCarParkTextBoxType, String> FriendlyNames =
            new Dictionary<AddCarParkTextBoxType, String>()
            {
                {AddCarParkTextBoxType.Yes, Message.Generic_Yes },
                {AddCarParkTextBoxType.No, Message.Generic_No },
            };
    }
}
