using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.ProductBuddyAssignment.V1
{
    /// <summary>
    ///     Possible values for the Placement Units parameter in the Merchandise Planogram task.
    /// </summary>
    public enum BuddyCreationType
    {
        ClearAllBuddiesFromAssortment,
        MergeBuddiesNewHavePriority,
        MergeBuddiesExistingHavePriority
    }

    /// <summary>
    ///     Helpers to use the <see cref="BuddyCreationType"/> enumeration.
    /// </summary>
    public static class BuddyCreationTypeHelper
    {
        public static readonly Dictionary<BuddyCreationType, String> FriendlyNames =
            new Dictionary<BuddyCreationType, String>
            {
                {BuddyCreationType.ClearAllBuddiesFromAssortment, Message.BudyCreationMode_RemoveAll },
                {BuddyCreationType.MergeBuddiesNewHavePriority, Message.BudyCreationMode_AddBefore },
                {BuddyCreationType.MergeBuddiesExistingHavePriority, Message.BudyCreationMode_AddAfter }
            };
    }
}