using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.ProductBuddyAssignment.V1
{
    public class Task : TaskBase
    {
        /// <summary>
        ///     Enumeration of parameter IDs for this task.
        /// </summary>
        internal enum Parameter
        {
            BuddyProductGTIN = 0,
            BuddyPercentage = 1,
            BuddyCreationType = 2
        }

        private ITaskContext _taskContext;
        private String _masterDataGTINAttributeFieldID;
        private String _masterDataBuddyPercentageAttributeFieldID;
        private BuddyCreationType _masterDataBuddyCreation;

        public override String Name => Message.AddProductBuddies_Name;

        public override String Description => Message.AddProductBuddies_Description;

        public override String Category => Message.TaskCategory_MerchandisePlanogram;
        public override Byte MinorVersion => 0;
        public override Byte MajorVersion => 1;

        // Remove the next line to allow the user to select it.
        public override Boolean IsVisible => false;

        protected override void OnRegisterParameters()
        {
            var parametersToRegister = new List<TaskParameter>() {
                CreateProductBuddyGTINParameter(),
                CreateProductBuddyPercentParameters(),
                CreateProductBuddyCreationModeParameter()
            };

            foreach (TaskParameter param in parametersToRegister)
                RegisterParameter(param);
        }

        

        private TaskParameter CreateProductBuddyGTINParameter()
        {
            return (new TaskParameter(
                id: Parameter.BuddyProductGTIN,
                name: Message.AddProductBuddies_ProductGtinCustomAtrributeField_Name,
                description: Message.AddProductBuddies_ProductGtinCustomAtrributeField_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.ProductAttributeMultiple,
                defaultValue: "",
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));
        }

        private TaskParameter CreateProductBuddyPercentParameters()
        {
            return (new TaskParameter(
                id: Parameter.BuddyPercentage,
                name: Message.AddProductBuddies_PercentageAssignmentCustomAtrributeField_Name,
                description: Message.AddProductBuddies_PercentageAssignmentCustomAtrributeField_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.ProductAttributeMultiple,
                defaultValue: null,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));
        }

        private TaskParameter CreateProductBuddyCreationModeParameter()
        {
            return (new TaskParameter(
                id: Parameter.BuddyCreationType,
                name: Message.AddProductBuddies_BuddyCreationMode_Name,
                description: Message.AddProductBuddies_BuddyCreationMode_Description,
                category: Message.ParameterCategory_Default,
                enumType: typeof(BuddyCreationType),
                defaultValue: BuddyCreationType.ClearAllBuddiesFromAssortment,
                isHiddenByDefault: false,
                isReadOnlyByDefault: false));
        }
        protected override void OnExecute(ITaskContext context)
        {
            if (context == null) return;

            CheckSingleExecution();

            using (new CodePerformanceMetric())
            {
                _taskContext = context;
                if (!ValidateParameters()) return;
                if (!CheckThatTheAssortIsValidAndContainsProducts()) return;
                AssignBuddyProducts();
            }
        }

       

        /// <summary>
        ///     Validate the parameters for this instance.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext" /> to store the validated parameters in.</param>
        /// <returns><c>True</c> if all the parameters validate correctly; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     <list type="bullet">
        ///         <listheader>
        ///             <term>Events Logged</term>
        ///         </listheader>
        ///         <item>
        ///             <term>Error: Invalid Task Parameter, <c>Placement Units</c></term>
        ///             <description>The parameter is missing or has an invalid value.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Invalid Task Parameter, <c>Ignore Merchandising Dimensions</c></term>
        ///             <description>The parameter is missing or has an invalid value.</description>
        ///         </item>
        ///     </list>
        /// </remarks>
        private Boolean ValidateParameters()
        {
            using (new CodePerformanceMetric())
            {
                var success = true;

                // Validate that the Product Units paramter exists.
                if (Parameters[Parameter.BuddyProductGTIN].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProductBuddyTask_ProductGTIN_Parameter,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _masterDataGTINAttributeFieldID = (String)Parameters[Parameter.BuddyProductGTIN].Value.Value2;
                        _masterDataGTINAttributeFieldID = _masterDataGTINAttributeFieldID.Split('.').Last();
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductBuddyTask_ProductGTIN_Parameter,
                            Parameters[Parameter.BuddyProductGTIN].Value.Value2);
                        success = false;
                    }
                }

                if (Parameters[Parameter.BuddyPercentage].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProductBuddyTask_AQllocationPercentage_Parameter,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _masterDataBuddyPercentageAttributeFieldID = (String)Parameters[Parameter.BuddyPercentage].Value.Value2;
                        _masterDataBuddyPercentageAttributeFieldID =
                            _masterDataBuddyPercentageAttributeFieldID.Split('.').Last();
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductBuddyTask_AQllocationPercentage_Parameter,
                            Parameters[Parameter.BuddyPercentage].Value.Value2);
                        success = false;
                    }
                }

                BuddyCreationType value = BuddyCreationType.ClearAllBuddiesFromAssortment;
                if (Parameters[Parameter.BuddyCreationType].Value == null)
                {
                    _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProductBuddyTask_BuddyCreationMode_Parameter,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        _masterDataBuddyCreation = (BuddyCreationType)Parameters[Parameter.BuddyCreationType].Value.Value1;
                    }
                    catch
                    {
                        _taskContext.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductBuddyTask_BuddyCreationMode_Parameter,
                            Parameters[Parameter.BuddyCreationType].Value.Value1);
                        success = false;
                    }
                }

                return success;
            }
        }

       
        private Boolean CheckThatTheAssortIsValidAndContainsProducts()
        {
            using (new CodePerformanceMetric())
            {
                #region Validate Assortment

                //  Verify there is an assortment present.
                if (_taskContext.Planogram.Assortment == null)
                {
                    _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                                           Message.TaskLogging_Assortment,
                                                           Message.TaskLogging_NoAssortmentWasPresent);
                    return false;
                }

                //  Verify the assortment contains products.
                if (_taskContext.Planogram.Assortment.Products == null ||
                    !_taskContext.Planogram.Assortment.Products.Any())
                {
                    _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                                           Message.TaskLogging_AssortmentProducts,
                                                           Message.TaskLogging_NoAssortmentProductsListWasPresent);
                    return false;
                }

               #endregion
                
                return true;
            }
        }

        private void AssignBuddyProducts()
        {
            // Extract all the GTINS from the planogram assortment.
            var gtinsToProcess = _taskContext.Planogram.Assortment.Products.Select(p => p.Gtin).ToList();
            
            // Get the product record for each GTIN from the master data
            ProductList productsToProcess = ProductList.FetchByEntityIdProductGtins(_taskContext.EntityId,
                gtinsToProcess);

            // Check that the relevant atrribute field has a valid GTIN (as specified in the parameters).
            foreach (Product product in productsToProcess)
            {
                var gtinOfProductToBuddy = product.CustomAttributes.GetAttributeValueByName<String>(_masterDataGTINAttributeFieldID);
                if (String.IsNullOrEmpty(gtinOfProductToBuddy) && _taskContext.Planogram.Assortment.Products.All(p => p.Gtin != gtinOfProductToBuddy)) continue;

                Double percentage = ExtractPercentageFromCustomAttrbiutes(product);
                AddProductBuddy(product, gtinOfProductToBuddy, (Single?)percentage, _masterDataBuddyCreation);
            }
        }

        private Double ExtractPercentageFromCustomAttrbiutes(Product product)
        {
            var percentageValue =
                product.CustomAttributes.GetAttributeValueByName<string>(_masterDataBuddyPercentageAttributeFieldID);
            Double percentage = 0F;
            Double.TryParse(Regex.Match(percentageValue, @"^-?\d+(?:\.\d+)?").Value, out percentage);
            percentage = percentage <= 0F ? 1 : percentage*0.01;
            return percentage;
        }

        private void AddProductBuddy(Product product, String gtinOfProductToBuddy, float? percentage, BuddyCreationType mode)
        {
            var buddy = PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy(product.Gtin);
            buddy.ProductGtin = product.Gtin;
            buddy.S1Percentage = percentage;
            buddy.S1ProductGtin = gtinOfProductToBuddy;
            buddy.SourceType = PlanogramAssortmentProductBuddySourceType.Manual;

            PlanogramAssortmentProductBuddyList buddies = _taskContext.Planogram.Assortment.ProductBuddies;
            switch (mode)
            {
                case BuddyCreationType.ClearAllBuddiesFromAssortment:
                    buddies.RemoveAllBuddies();
                    buddies.Add(buddy);
                    break;

                case BuddyCreationType.MergeBuddiesNewHavePriority:
                    buddies.RemoveBuddiesBySourceGtin(product.Gtin);
                    buddies.Add(buddy);
                    break;

                case BuddyCreationType.MergeBuddiesExistingHavePriority:
                    if (!buddies.HasBuddiesForSourceGtin(product.Gtin))
                        buddies.Add(buddy);
                    break;

                default:
                    break;
            }
        }

        [Conditional("DEBUG")]
        private void CheckSingleExecution()
        {
            if (_taskContext != null)
                System.Diagnostics.Debug.Fail(
                    "Tasks should be executed only once per instance and this instance was executed already. The Task may have previous values hanging around.");
        }
    }
}