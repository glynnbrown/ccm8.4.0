﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.RemoveLegalProductsLocationSpecific.v1
{
    internal class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private RemoveActionType _removeAction; // holds the remove action type
        private String _carParkComponentName; // holds the car park component name
        private Boolean _addCarParkTextBox;
        private LocationInfo _planogramLocation;
        private Dictionary<Object, PlanogramProduct> _planogramProductIdLookup = new Dictionary<Object, PlanogramProduct>(); // holds a list of planogram products indexed by id
        private Dictionary<Object, PlanogramProduct> _planogramProductsToRemove = new Dictionary<Object, PlanogramProduct>(); // holds a list of planogram products to remove
        private List<PlanogramPosition> _planogramPositionsToRemove = new List<PlanogramPosition>(); // holds a list of planogram positions to remove
        private PlanogramMerchandisingGroup _carParkMerchandisingGroup; // holds a reference to the car park merchandising group
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the remove action
        /// </summary>
        public RemoveActionType RemoveAction
        {
            get { return _removeAction; }
            set { _removeAction = value; }
        }

        /// <summary>
        /// Gets or sets the car park component name
        /// </summary>
        public String CarParkComponentName
        {
            get { return _carParkComponentName; }
            set { _carParkComponentName = value; }
        }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox
        {
            get { return _addCarParkTextBox; }
            set { _addCarParkTextBox = value; }
        }

        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// Returns a list of planogram products indexed by id
        /// </summary>
        public Dictionary<Object, PlanogramProduct> PlanogramProductIdLookup
        {
            get { return _planogramProductIdLookup; }
        }

        /// <summary>
        /// Returns a list of planogram products to remove
        /// </summary>
        public Dictionary<Object, PlanogramProduct> PlanogramProductsToRemove
        {
            get { return _planogramProductsToRemove; }
        }

        /// <summary>
        /// Returns a list of planogram positions to remove from the plan
        /// </summary>
        public List<PlanogramPosition> PlanogramPositionsToRemove
        {
            get { return _planogramPositionsToRemove; }
        }

        /// <summary>
        /// Gets or sets the car park merchandising group
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup
        {
            get { return _carParkMerchandisingGroup; }
            set { _carParkMerchandisingGroup = value; }
        }

        public LocationInfo PlanogramLocation
        {
            get { return _planogramLocation; }
            set { _planogramLocation = value; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }

        #endregion
    }
}
