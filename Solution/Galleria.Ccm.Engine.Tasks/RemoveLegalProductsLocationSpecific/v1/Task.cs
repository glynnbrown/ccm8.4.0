﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)

// V8-31835 : N.Haywood
//  Created
// V8-32246 : A.Silva
//  Amended warnings for moved to car park shelf positions (it takes the place of the removed ones if they are moving to the car park shelf).
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.RemoveLegalProductsLocationSpecific.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameters
        private enum Parameter : int
        {
            RemoveAction = 0,
            CarParkComponentName = 1,
            AddCarParkTextBox = 2
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RemoveLegalProductsLocationSpecific_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.RemoveLegalProductsLocationSpecific_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // remove action
            this.RegisterParameter(new TaskParameter(
                Parameter.RemoveAction,
                Message.RemoveLegalProductsLocationSpecific_Parameter_RemoveAction_Name,
                Message.RemoveLegalProductsLocationSpecific_Parameter_RemoveAction_Description,
                Message.ParameterCategory_Default,
                typeof(RemoveActionType),
                RemoveActionType.RemovePositionsAndProducts,
                false,
                false));

            // car park component name
            this.RegisterParameter(new TaskParameter(
                id: Parameter.CarParkComponentName,
                name: Message.RemoveLegalProductsLocationSpecific_Parameter_CarParkComponentName_Name,
                description:Message.RemoveLegalProductsLocationSpecific_Parameter_CarParkComponentName_Description,
                category: Message.ParameterCategory_Default,
                parameterType: TaskParameterType.String,
                defaultValue: Message.RemoveLegalProductsLocationSpecific_Parameter_CarParkComponentName_Default,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true,
                parentId: null,
                isRequired: null,
                isNullAllowed: null,
                isValid: () => IsCarParkNameValid()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.RemoveLegalProductsLocationSpecific_Parameter_AddCarParkTextBox_Name,
                Message.RemoveLegalProductsLocationSpecific_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateParameters(taskContext)) return;
                if (!this.ValidatePlanogram(taskContext)) return;
                if (!this.PopulateLookups(taskContext)) return;
                if (!this.RemovePositionsFromPlanogram(taskContext)) return;
                if (!this.GetCarParkComponent(taskContext)) return;
                if (!this.AddPositionsToCarParkComponent(taskContext)) return;
                if (!this.RemoveProductsFromPlanogram(taskContext)) return;
                if (!this.ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            Boolean success = true;

            #region Remove Action

            // validate the remove action type
            if ((!this.Parameters.Contains((Int32)Parameter.RemoveAction)) ||
                (this.Parameters[Parameter.RemoveAction].Value == null) ||
                (this.Parameters[Parameter.RemoveAction].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveLegalProductsLocationSpecific_Parameter_RemoveAction_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                try
                {
                    context.RemoveAction = (RemoveActionType)this.Parameters[Parameter.RemoveAction].Value.Value1;
                }
                catch
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveLegalProductsLocationSpecific_Parameter_RemoveAction_Name, Parameters[Parameter.RemoveAction].Value.Value1);
                    success = false;
                }
            }

            #endregion

            #region Car Park Component Name

            // validate that the car park shelf name parameter exists
            if ((!this.Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                (this.Parameters[Parameter.CarParkComponentName].Value == null) ||
                (this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveLegalProductsLocationSpecific_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                // get the car park shelf name
                context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveLegalProductsLocationSpecific_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
            }

            #endregion

            #region AddCar Park Text Box

            //validate that the car park text box parameter exists
            if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveLegalProductsLocationSpecific_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                try
                {
                    AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                    context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                }
                catch (InvalidCastException)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.RemoveLegalProductsLocationSpecific_Parameter_AddCarParkTextBox_Name,
                        Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                    success = false;
                }
            }

            #endregion

            return success;
        }

        /// <summary>
        /// Validates the planogram details
        /// </summary>
        private Boolean ValidatePlanogram(TaskContext context)
        {
            // verify we have an location
            if ((context.Planogram.LocationCode == null) ||
                (context.Planogram.LocationName == null))
            {
                context.LogWarning(EventLogEvent.PlanogramNoValidLocation);
                return false;
            }
            else
            {
                List<String> locationCode = new List<String>();
                locationCode.Add(context.Planogram.LocationCode);

                LocationInfoList locationList = LocationInfoList.FetchByEntityIdLocationCodes(context.EntityId, locationCode);
                if (locationList.Any())
                {
                    context.PlanogramLocation = locationList.FirstOrDefault();
                }
                else
                {
                    context.LogWarning(EventLogEvent.PlanogramNoValidLocation);
                    return false;
                }
            }

            // return succes
            return true;
        }

        /// <summary>
        /// Populates lookup lists for the rest of the process
        /// </summary>
        private Boolean PopulateLookups(TaskContext context)
        {
            // build a lookup of all products within
            // the planogram indexed by product id
            foreach (PlanogramProduct product in context.Planogram.Products)
            {
                if (!context.PlanogramProductIdLookup.ContainsKey(product.Id))
                    context.PlanogramProductIdLookup.Add(product.Id, product);
            }

            // gets list of products Legal for that location
            LocationProductLegalList LegalProductList = LocationProductLegalList.FetchByEntityId(context.EntityId);
            List<Int32> productIds = new List<Int32>();
            productIds = LegalProductList.Where(p => p.LocationId == context.PlanogramLocation.Id).Select(p => p.ProductId).ToList();
            ProductList products = ProductList.FetchByProductIds(productIds);

            // now build a list of all planogram products
            // that exist within the Legal products
            foreach (PlanogramProduct product in context.Planogram.Products.OrderBy(p => p.Gtin))
            {
                if ((!products.Select(p => p.Gtin).Contains(product.Gtin)) && (!context.PlanogramProductsToRemove.ContainsKey(product.Id)))
                    context.PlanogramProductsToRemove.Add(product.Id, product);
            }

            // now build a list of all planogram positions
            // that exist within the Legal products
            foreach (PlanogramPosition position in context.Planogram.Positions.OrderBy(p => context.PlanogramProductIdLookup[p.PlanogramProductId].Gtin))
            {
                if ((context.PlanogramProductsToRemove.ContainsKey(position.PlanogramProductId)) && (!context.PlanogramPositionsToRemove.Contains(position)))
                    context.PlanogramPositionsToRemove.Add(position);
            }

            // return success
            return true;
        }

        /// <summary>
        /// Removes unranged positions from the planogram
        /// </summary>
        private Boolean RemovePositionsFromPlanogram(TaskContext context)
        {
            // check that we have some positions to remove
            if (context.PlanogramPositionsToRemove.Count == 0) return true;

            // remove all positions from the planogram
            foreach (PlanogramPosition position in context.PlanogramPositionsToRemove)
            {
                context.Planogram.Positions.Remove(position);
            }

            //  Check whether the removal is such or just moving them to the car park shelf.
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark)
            {
                // Log totals for products removed.
                context.LogInformation(EventLogEvent.PlanogramProductPositionsRemoved, context.PlanogramPositionsToRemove.Count);
            }

            // return success
            return true;
        }

        /// <summary>
        /// Retrieves the car park component if it exists
        /// else creates the car park component
        /// </summary>
        private Boolean GetCarParkComponent(TaskContext context)
        {
            // if we are not moving positions to the car park shelf
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

            // if there are no positions to remove, then there
            // is nothing for use to do either
            if (context.PlanogramPositionsToRemove.Count == 0) return true;

            // get the merchandising groups
            context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();

            // get the car park component
            context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

            // return success
            return true;
        }

        /// <summary>
        /// Moves positions from the planogram to the car park component
        /// </summary>
        private Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            // if we are not moving positions to the car park component
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

            // check that we have some positions to add
            if (context.PlanogramPositionsToRemove.Count == 0) return true;

            // if we have no car park component, then do nothing
            if (context.CarParkMerchandisingGroup == null)
            {
                // Still Log totals for products removed.
                context.LogInformation(
                    EventLogEvent.PlanogramProductPositionsRemoved,
                    context.PlanogramPositionsToRemove.Count);
                return true;
            }

            // create a new position in the car park merchandising group
            // for each position in the list of positions to remove
            foreach (PlanogramPosition oldPosition in context.PlanogramPositionsToRemove)
            {
                // get the planogram product
                PlanogramProduct product = context.PlanogramProductIdLookup[oldPosition.PlanogramProductId];

                // create a new position on the merchandising group
                context.CarParkMerchandisingGroup.InsertPositionPlacement(product);
            }

            // apply changes to the car park component
            context.CarParkMerchandisingGroup.Process();
            context.CarParkMerchandisingGroup.ApplyEdit();

            // Log totals for products moved to the car park shelf.
            context.LogInformation(
                EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf,
                context.PlanogramPositionsToRemove.Count);

            // return success
            return true;
        }

        /// <summary>
        /// Removes products from the planogram
        /// </summary>
        private Boolean RemoveProductsFromPlanogram(TaskContext context)
        {
            // if we are moving positions to the car park shelf
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndProducts) return true;

            // check that we have some products to remove
            if (context.PlanogramProductsToRemove.Count == 0) return true;

            // remove products from the planogram
            foreach (PlanogramProduct product in context.PlanogramProductsToRemove.Values)
            {
                context.Planogram.Products.Remove(product);
            }

            // return success
            return true;
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private Boolean ReMerchandisePlanogram(TaskContext context)
        {
            // check that some positions have either been moved or removed
            if (context.PlanogramPositionsToRemove.Count == 0) return true;

            // remerchandise the whole planogram
            context.RemerchandisePlanogram();

            // return success
            return true;
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
