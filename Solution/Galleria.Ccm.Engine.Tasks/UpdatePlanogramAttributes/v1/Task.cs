﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802
// V8-29078: A.Kuszyk
//  Created.
#endregion
#region Version History: CCM803
// V8-29643 : D.Pleasance
//  Amended task so that list of planogram attributes are provided along with update value.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-30063 : D.Pleasance
//  Added PlanogramInventory settings
#endregion
#region Version History: CCM820
// V8-30193 : L.Ineson
//  Updated to support renumbering strategy property.
// V8-31128 : D.Pleasance
//  Added Planogram Info settings
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Nested Classes
        /// <summary>
        /// A simple container for the attribute source, name and value.
        /// </summary>
        internal class PlanogramAttributeSelection
        {
            /// <summary>
            /// The source for this attribute data (General, CustomDataText, CustomDataValue, CustomDataFlag, CustomDataDate, Settings).
            /// </summary>
            public PlanogramAttributeSourceType Source { get; private set; }

            /// <summary>
            /// The name of the property on the source that this item refers to.
            /// </summary>
            public String AttributeName { get; private set; }

            /// <summary>
            /// The value to apply to the property.
            /// </summary>
            public Object PropertyValue { get; private set; }

            /// <summary>
            /// Instantiates a new object with the given values.
            /// </summary>
            /// <param name="source"></param>
            /// <param name="attributeName"></param>
            public PlanogramAttributeSelection(PlanogramAttributeSourceType source, String attributeName, Object propertyValue)
            {
                Source = source;
                AttributeName = attributeName;
                PropertyValue = propertyValue;
            }
        }

        #endregion

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PlanogramAttributes = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramAttributes_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramAttributes_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramAttributes,
                Message.UpdatePlanogramAttributes_PlanogramAttributes,
                Message.UpdatePlanogramAttributes_PlanogramAttributes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.PlanogramAttributeMultiple,
                null,
                true,
                true));
        }
        #endregion
        
        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                taskContext.AttributesUpdatedCount = UpdatePlanogramAttributes(taskContext.Planogram, taskContext.PlanogramAttributeSelections, context.EntityId);
                if (!ReMerchandisePlanogram(taskContext)) return;
                LogCompletion(taskContext);
            }
        }
                
        #endregion

        #region Methods

        /// <summary>
        /// Gets parameter values, valdidates them and assigns them to the the given arguments.
        /// </summary>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get and validate planogram attributes.
                if (Parameters[Parameter.PlanogramAttributes].Value != null)
                {
                    try
                    {
                        taskContext.PlanogramAttributeSelections.AddRange(this.Parameters[Parameter.PlanogramAttributes].Values.
                            Select(item =>
                                new PlanogramAttributeSelection((PlanogramAttributeSourceType)item.Value1, item.Value2.ToString(), item.Value3)
                                ).
                            ToList());
                    }
                    catch
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdatePlanogramAttributes_PlanogramAttributes,
                            Message.Error_NoValue);
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Updates the planogram with the supplied planogram attribute selection detail, list of attributes along with values to update.
        /// </summary>
        /// <param name="planogram">The planogram</param>
        /// <param name="planogramAttributeSelections">The list of planogram attribute selections</param>
        /// /// <param name="entityId">Current planogram entity</param>
        internal static Int32 UpdatePlanogramAttributes(
            Planogram planogram,
            IEnumerable<PlanogramAttributeSelection> planogramAttributeSelections, 
            Int32 entityId)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Int32 attributesUpdated = 0;

 
                var attributeSelections = planogramAttributeSelections as IList<PlanogramAttributeSelection> ?? planogramAttributeSelections.ToList();
                if (!attributeSelections.Any()) return attributesUpdated;

                foreach (PlanogramAttributeSelection attributeSelection in attributeSelections)
                {
                    
                    if (attributeSelection.PropertyValue == null) continue;

                    PropertyInfo property = null;

                    switch (attributeSelection.Source)
                    {
                        case PlanogramAttributeSourceType.CustomDataText:
                        case PlanogramAttributeSourceType.CustomDataValue:
                        case PlanogramAttributeSourceType.CustomDataFlag:
                        case PlanogramAttributeSourceType.CustomDataDate:
                            case PlanogramAttributeSourceType.CustomDataNote:
                        {
                            String[] attribute = attributeSelection.AttributeName.Split('.');
                            property = typeof(ICustomAttributeData).GetProperty(attribute.Last());
                            if (property == null) continue;

                            if (property.PropertyType == typeof(Single) || property.PropertyType == typeof(Single?))
                            {
                                property.SetValue(planogram.CustomAttributes, Convert.ToSingle(attributeSelection.PropertyValue), null);
                            }
                            else
                            {
                                property.SetValue(planogram.CustomAttributes, attributeSelection.PropertyValue, null);
                            }
                        }
                            break;

                        case PlanogramAttributeSourceType.Settings:
                        {
                            if (attributeSelection.AttributeName.StartsWith(Planogram.RenumberingStrategyProperty.Name + "."))
                            {
                                String[] attribute = attributeSelection.AttributeName.Split('.');
                                property = typeof(PlanogramRenumberingStrategy).GetProperty(attribute.Last());
                                if (property == null) continue;
                                if (attributeSelection.PropertyValue.GetType() != typeof(Boolean)) continue;
                                property.SetValue(planogram.RenumberingStrategy, attributeSelection.PropertyValue, null);
                            }
                            else
                            {
                                property = typeof(Planogram).GetProperty(attributeSelection.AttributeName);
                                if (property == null) continue;
                                property.SetValue(planogram, attributeSelection.PropertyValue, null);
                            }
                        }
                            break;

                        case PlanogramAttributeSourceType.General:
                        case PlanogramAttributeSourceType.Info:
                        {
                            property = typeof(Planogram).GetProperty(attributeSelection.AttributeName);
                            if (property == null) continue;
                            property.SetValue(planogram, attributeSelection.PropertyValue, null);
                                    
                            if (property.Name == Planogram.CategoryCodeProperty.Name)
                            {
                                if (attributeSelections.Any(a => a.AttributeName == Planogram.CategoryNameProperty.Name)) break;
                                Boolean categoryNameUpdated = UpdateCategoryNameForCatgeoryCode(attributeSelection.PropertyValue.ToString(), planogram, entityId);
                                if (categoryNameUpdated) attributesUpdated++;
                            }
                        }
                            break;

                        case PlanogramAttributeSourceType.Inventory:
                        {
                            property = typeof(PlanogramInventory).GetProperty(attributeSelection.AttributeName);
                            if (property == null) continue;

                            if (PlanogramInventory.MinShelfLifeProperty.Name == attributeSelection.AttributeName)
                            {
                                Single minShelfLife = Convert.ToSingle(attributeSelection.PropertyValue);
                                property.SetValue(planogram.Inventory, (minShelfLife / 100), null);
                            }
                            else
                            {
                                if (property.PropertyType == typeof(Single) || property.PropertyType == typeof(Single?))
                                {
                                    property.SetValue(planogram.Inventory, Convert.ToSingle(attributeSelection.PropertyValue), null);
                                }
                                else
                                {
                                    property.SetValue(planogram.Inventory, attributeSelection.PropertyValue, null);
                                }
                            }
                        }
                            break;
                        default:
                            break;
                    }

                    attributesUpdated++;
                }
                return attributesUpdated;
            }
        }



        /// <summary>
        /// Tries to find an appropriate category name for the category code being set. 
        /// </summary>
        /// <param name="categoryCode"></param>
        /// <param name="planogram"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        private static Boolean UpdateCategoryNameForCatgeoryCode(String categoryCode, Planogram planogram, Int32 entityId)
        {
            var merchHierarchy = ProductHierarchy.FetchByEntityId(entityId);
            var productGroups = merchHierarchy.EnumerateAllGroups();
            var productgroup = productGroups.FirstOrDefault(pg => pg.Code == categoryCode);
            var categoryName = productgroup?.Name;

            if (String.IsNullOrEmpty(categoryName)) return false;

            var property = typeof(Planogram).GetProperty(Planogram.CategoryNameProperty.Name);
            if (property == null) return false;

            property.SetValue(planogram, categoryName, null);
            return true;
        }


        /// <summary>
        /// Remerchandises the planogram if required
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if no products were changed, then plan does not need to be remerchandised
                if (context.AttributesUpdatedCount == 0) return true;

                // remerchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        /// <summary>
        /// Log planogram attributes updated
        /// </summary>
        /// <param name="taskContext"></param>
        private void LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                taskContext.LogInformation(EventLogEvent.PlanogramAttributesUpdated, taskContext.AttributesUpdatedCount);
            }
        }

        #endregion
    }
}