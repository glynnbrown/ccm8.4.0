﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields

        private List<Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1.Task.PlanogramAttributeSelection> _planogramAttributeSelections;
        private Int32 _attributesUpdatedCount;

        #endregion

        #region Constructor

        public TaskContext(ITaskContext context)
            : base(context)
        {
            _planogramAttributeSelections = new List<Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1.Task.PlanogramAttributeSelection>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The planogram attribute selections
        /// </summary>
        internal List<Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1.Task.PlanogramAttributeSelection> PlanogramAttributeSelections
        {
            get { return _planogramAttributeSelections; }
        }

        /// <summary>
        /// The number of attributes that have been updated
        /// </summary>
        public Int32 AttributesUpdatedCount
        {
            get { return _attributesUpdatedCount; }
            set { _attributesUpdatedCount = value; }
        }

        #endregion
    }
}