﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1
{
    /// <summary>
    /// Denotes the actions that
    /// can be performed when removing a product.
    /// </summary>
    public enum RemoveActionType
    {
        RemovePositions,
        RemovePositionsAndProducts,
        RemovePositionsAndAddToCarPark
    }

    /// <summary>
    /// Contains helpers for the RemoveType enum.
    /// </summary>
    public static class RemoveActionTypeHelper
    {
        public static readonly Dictionary<RemoveActionType, String> FriendlyNames =
            new Dictionary<RemoveActionType, String>()
            {
                {RemoveActionType.RemovePositions, Message.RemoveProductManual_Parameter_RemoveActionType_RemovePositions },
                {RemoveActionType.RemovePositionsAndProducts, Message.RemoveProductManual_Parameter_RemoveActionType_RemovePositionsAndProducts },
                {RemoveActionType.RemovePositionsAndAddToCarPark, Message.RemoveProductManual_Parameter_RemoveActionType_RemovePositionsAndAddToCarPark },
            };
    }
}
