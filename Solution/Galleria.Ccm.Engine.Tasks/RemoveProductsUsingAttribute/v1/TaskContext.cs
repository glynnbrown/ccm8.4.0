﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk
//  Created.
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1
{
    internal class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private RemoveActionType _removeAction; // holds the remove action type
        private String _carParkComponentName; // holds the car park component name
        private PlanogramMerchandisingGroup _carParkMerchandisingGroup; // holds a reference to the car park merchandising group
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            PlanogramProductsToRemove = new List<PlanogramProduct>();
            PlanogramPositionsToRemove = new List<PlanogramPosition>();
            PlanogramProductIdLookup = new Dictionary<Object, PlanogramProduct>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the remove action
        /// </summary>
        public RemoveActionType RemoveAction
        {
            get { return _removeAction; }
            set { _removeAction = value; }
        }

        /// <summary>
        /// The product attribute to filter products by.
        /// </summary>
        public ObjectFieldInfo CriteriaAttribute { get; set; }

        /// <summary>
        /// The operator to use when comparing the <see cref="CriteriaAttribute"/>s to the <see cref="CriteriaFilter"/>.
        /// </summary>
        public TaskParameterFilterOperandType CriteriaOperator { get; set; }

        /// <summary>
        /// The filter to apply to the <see cref="CriteriaAttribute"/>s when selecting products.
        /// </summary>
        public Object CriteriaFilter { get; set; }

        /// <summary>
        /// The products to remove from the plan.
        /// </summary>
        public List<PlanogramProduct> PlanogramProductsToRemove { get; set; }

        /// <summary>
        /// The positions to remove from the plan.
        /// </summary>
        public List<PlanogramPosition> PlanogramPositionsToRemove { get; set; }

        /// <summary>
        /// The products in the planogram keyed by Id.
        /// </summary>
        public Dictionary<Object, PlanogramProduct> PlanogramProductIdLookup { get; set; }

        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// Gets or sets the car park component name
        /// </summary>
        public String CarParkComponentName
        {
            get { return _carParkComponentName; }
            set { _carParkComponentName = value; }
        }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox { get; set; }

        /// <summary>
        /// Gets or sets the car park merchandising group
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup
        {
            get { return _carParkMerchandisingGroup; }
            set { _carParkMerchandisingGroup = value; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null) this.MerchandisingGroups.Dispose();
        }

        #endregion
    }
}
