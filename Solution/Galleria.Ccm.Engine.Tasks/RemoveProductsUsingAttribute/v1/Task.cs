﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk
//  Created.
// V8-31977 : A.Kuszyk
//  Ensured that Custom Attribute, Assortment and Performance fields are handled correctly.
// V8-32093 : A.Kuszyk
//  Modified log message when no matching products can be found.
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Interfaces;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1
{
    /// <summary>
    /// Removes products from a Planogram based on criteria for a product attribute.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Fields

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramProduct"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> _productPropertiesByName = null;

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="CustomAttributeData"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> _customAttributeDataPropertiesByName = null;

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramAssortmentProduct"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> _assortmentProductPropertiesByName = null;

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramPerformanceData"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> _performanceDataPropertiesByName = null;

        #endregion

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameters
        private enum Parameter : int
        {
            ProductAttributeCriteria = 0,
            RemoveAction = 1,
            CarParkComponentName = 2,
            AddCarParkTextBox = 3,
        }
        #endregion

        #region Properties

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramProduct"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> ProductPropertiesByName
        {
            get
            {
                if (_productPropertiesByName == null)
                {
                    _productPropertiesByName = new Dictionary<String,PropertyInfo>();
                    foreach(PropertyInfo propertyInfo in typeof(PlanogramProduct).GetProperties())
                    {
                        if (_productPropertiesByName.ContainsKey(propertyInfo.Name)) continue;
                        _productPropertiesByName.Add(propertyInfo.Name, propertyInfo);
                    }
                }
                return _productPropertiesByName;
            }
        }

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="CustomAttributeData"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> CustomAttributeDataPropertiesByName
        {
            get
            {
                if (_customAttributeDataPropertiesByName == null)
                {
                    _customAttributeDataPropertiesByName = new Dictionary<String, PropertyInfo>();
                    foreach (PropertyInfo propertyInfo in typeof(Galleria.Framework.Planograms.Model.CustomAttributeData).GetProperties())
                    {
                        if (_customAttributeDataPropertiesByName.ContainsKey(propertyInfo.Name)) continue;
                        _customAttributeDataPropertiesByName.Add(propertyInfo.Name, propertyInfo);
                    }
                }
                return _customAttributeDataPropertiesByName;
            }
        }

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramAssortmentProduct"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> AssortmentProductPropertiesByName
        {
            get
            {
                if (_assortmentProductPropertiesByName == null)
                {
                    _assortmentProductPropertiesByName = new Dictionary<String, PropertyInfo>();
                    foreach (PropertyInfo propertyInfo in typeof(PlanogramAssortmentProduct).GetProperties())
                    {
                        if (_assortmentProductPropertiesByName.ContainsKey(propertyInfo.Name)) continue;
                        _assortmentProductPropertiesByName.Add(propertyInfo.Name, propertyInfo);
                    }
                }
                return _assortmentProductPropertiesByName;
            }
        }

        /// <summary>
        /// A dictionary of the <see cref="PropertyInfo"/>s available on <see cref="PlanogramPerformanceData"/>,
        /// keyed by their name.
        /// </summary>
        private Dictionary<String, PropertyInfo> PerformanceDataPropertiesByName
        {
            get
            {
                if (_performanceDataPropertiesByName == null)
                {
                    _performanceDataPropertiesByName = new Dictionary<String, PropertyInfo>();
                    foreach (PropertyInfo propertyInfo in typeof(PlanogramPerformanceData).GetProperties())
                    {
                        if (_performanceDataPropertiesByName.ContainsKey(propertyInfo.Name)) continue;
                        _performanceDataPropertiesByName.Add(propertyInfo.Name, propertyInfo);
                    }
                }
                return _performanceDataPropertiesByName;
            }
        }

        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RemoveProductsUsingAttribute_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.RemoveProductsUsingAttribute_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // product code
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductAttributeCriteria,
                Message.RemoveProductsUsingAttribute_ProductAttributeCriteria_Name, 
                Message.RemoveProductsUsingAttribute_ProductAttributeCriteria_Description, 
                Message.ParameterCategory_Default,
                TaskParameterType.ProductFilterSingle,
                null,
                false,
                false));

            // remove action
            this.RegisterParameter(new TaskParameter(
                Parameter.RemoveAction,
                Message.RemoveProductManual_Parameter_RemoveAction_Name,
                Message.RemoveProductManual_Parameter_RemoveAction_Description,
                Message.ParameterCategory_Default,
                typeof(RemoveActionType),
                RemoveActionType.RemovePositions,
                false,
                false));

            // car park component name
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.CarParkComponentName,
                    name: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name,
                    description: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.String,
                    defaultValue: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Default,
                    isHiddenByDefault: true,
                    isReadOnlyByDefault: true,
                    parentId: null,
                    isRequired: null,
                    isNullAllowed: null,
                    isValid: () => IsCarParkNameValid()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.RemoveProductsUsingAttribute_Parameter_AddCarParkTextBox_Name,
                Message.RemoveProductsUsingAttribute_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }
        #endregion

        #region Methods
        
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateParameters(taskContext)) return;
                if (!this.PopulateLookups(taskContext)) return;
                if (!this.RemovePositionsFromPlanogram(taskContext)) return;
                if (!this.GetCarParkComponent(taskContext)) return;
                if (!this.AddPositionsToCarParkComponent(taskContext)) return;
                if (!this.RemoveProductsFromPlanogram(taskContext)) return;
                if (!this.ReMerchandisePlanogram(taskContext)) return;
                LogCompletion(taskContext);
            }
        }

        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            Boolean success = true;

            #region Product Attribute Criteria

            // validate the product filter
            if ((!this.Parameters.Contains((Int32)Parameter.ProductAttributeCriteria)) ||
                (this.Parameters[Parameter.ProductAttributeCriteria].Value == null))
            {
                context.LogError(
                    EventLogEvent.InvalidTaskParameterValue, 
                    Message.RemoveProductsUsingAttribute_ProductAttributeCriteria_Name,
                    Message.Error_NoValue); 
                return false;
            }
            else
            {
                try
                {
                    ITaskParameterValue productAttributeCriteriaParameterValue = this.Parameters[Parameter.ProductAttributeCriteria].Value;

                    ObjectFieldInfo objectFieldInfo = PlanogramProduct
                        .EnumerateDisplayableFieldInfos(includeMetadata: true, includeCustom: true, includePerformanceData: true)
                        .FirstOrDefault(o => o.FieldPlaceholder.Equals(productAttributeCriteriaParameterValue.Value1));
                    if (objectFieldInfo == null)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue, 
                            Message.RemoveProductsUsingAttribute_ProductAttributeCriteria_Name,
                            productAttributeCriteriaParameterValue.Value1); 
                        return false;
                    }

                    context.CriteriaAttribute = objectFieldInfo;
                    context.CriteriaOperator = (TaskParameterFilterOperandType)productAttributeCriteriaParameterValue.Value2;
                    context.CriteriaFilter = productAttributeCriteriaParameterValue.Value3;
                }
                catch
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue, 
                        Message.RemoveProductsUsingAttribute_ProductAttributeCriteria_Name,
                        Message.Error_NoValue); 
                    return false;
                }
            }

            #endregion

            #region Remove Action

            // validate the remove action type
            if ((!this.Parameters.Contains((Int32)Parameter.RemoveAction)) ||
                (this.Parameters[Parameter.RemoveAction].Value == null) ||
                (this.Parameters[Parameter.RemoveAction].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductManual_Parameter_RemoveAction_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                try
                {
                    context.RemoveAction = (RemoveActionType)this.Parameters[Parameter.RemoveAction].Value.Value1;
                }
                catch
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductManual_Parameter_RemoveAction_Name, Parameters[Parameter.RemoveAction].Value.Value1);
                    success = false;
                }
            }

            #endregion

            #region Car Park Component Name

            // validate that the car park shelf name parameter exists
            if ((!this.Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                (this.Parameters[Parameter.CarParkComponentName].Value == null) ||
                (this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductManual_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                // get the car park shelf name
                context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductManual_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
            }

            #endregion

            #region AddCar Park Text Box

            //validate that the car park text box parameter exists
            if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
            {
                context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsUsingAttribute_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                success = false;
            }
            else
            {
                try
                {
                    AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                    context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                }
                catch (InvalidCastException)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.RemoveProductsUsingAttribute_Parameter_AddCarParkTextBox_Name,
                        Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                    success = false;
                }
            }

            #endregion

            return success;
        }

        /// <summary>
        /// Compares the given <paramref name="product"/> against the criteria in <paramref name="context"/> and evaluates
        /// if the <paramref name="product"/> is a match.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="product">The product to compare.</param>
        /// <returns>True of the <paramref name="product"/> meets the criteria.</returns>
        private Boolean IsProductMatchForCriteria(TaskContext context, PlanogramProduct product)
        {
            try
            {
                PropertyInfo criteriaProperty;
                Object propertyObject;
                if (context.CriteriaAttribute.PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                {
                    String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name);
                    if(!Regex.IsMatch(context.CriteriaAttribute.PropertyName,propertyPattern)) return false;
                    String propertyName = Regex.Match(context.CriteriaAttribute.PropertyName,propertyPattern).Groups[1].Value;
                    if (!CustomAttributeDataPropertiesByName.TryGetValue(propertyName, out criteriaProperty)) return false;
                    propertyObject = product.CustomAttributes;
                }
                else if(context.CriteriaAttribute.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
                {
                    propertyObject = product.GetPlanogramAssortmentProduct();
                    if (propertyObject == null) return false;
                    String propertyPattern = String.Format(@"{0}(\w+)", PlanogramProduct.AssortmentFieldPrefix);
                    if (!Regex.IsMatch(context.CriteriaAttribute.PropertyName, propertyPattern)) return false;
                    String propertyName = Regex.Match(context.CriteriaAttribute.PropertyName, propertyPattern).Groups[1].Value;
                    if (!AssortmentProductPropertiesByName.TryGetValue(propertyName, out criteriaProperty)) return false;
                }
                else if (context.CriteriaAttribute.PropertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
                {
                    propertyObject = product.GetPlanogramPerformanceData();
                    if (propertyObject == null) return false;
                    String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name);
                    if (!Regex.IsMatch(context.CriteriaAttribute.PropertyName, propertyPattern)) return false;
                    String propertyName = Regex.Match(context.CriteriaAttribute.PropertyName, propertyPattern).Groups[1].Value;
                    if (!PerformanceDataPropertiesByName.TryGetValue(propertyName, out criteriaProperty)) return false;
                }
                else
                {
                    if (!ProductPropertiesByName.TryGetValue(context.CriteriaAttribute.PropertyName, out criteriaProperty)) return false;
                    propertyObject = product;
                }
                Object criteriaPropertyValue = criteriaProperty.GetValue(propertyObject, null);
                return context.CriteriaOperator.IsMatch(criteriaPropertyValue, context.CriteriaFilter);
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        /// <summary>
        /// Populates lookup lists for the rest of the process
        /// </summary>
        private Boolean PopulateLookups(TaskContext context)
        {
            // build a lookup of all products within
            // the planogram indexed by product id
            foreach (PlanogramProduct product in context.Planogram.Products)
            {
                if (!context.PlanogramProductIdLookup.ContainsKey(product.Id))
                {
                    context.PlanogramProductIdLookup.Add(product.Id, product);
                }

                if (IsProductMatchForCriteria(context, product))
                {
                    context.PlanogramProductsToRemove.Add(product);
                }
            }

            foreach (PlanogramPosition position in context.Planogram.Positions)
            {
                if(context.PlanogramProductsToRemove.Contains(position.GetPlanogramProduct()))
                {
                    context.PlanogramPositionsToRemove.Add(position);
                }
            }

            return true;
        }

        /// <summary>
        ///     Log final details about the task execution.
        /// </summary>
        /// <param name="context">The <see cref="RemoveProductManual.v1.TaskContext"/> contaning what needs to be logged.</param>
        private void LogCompletion(TaskContext context)
        {
            if (!context.PlanogramPositionsToRemove.Any())
            {
                context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.RemoveProductsUsingAttribute_NoMatchingProducts);
            }
            else
            {
                if (context.RemoveAction != RemoveActionType.RemovePositionsAndProducts)
                {
                    context.LogInformation(EventLogEvent.PlanogramPositionsRemoved, context.PlanogramPositionsToRemove.Count);
                }
                else
                {
                    context.LogInformation(EventLogEvent.PlanogramProductsAndPositionsRemoved, context.PlanogramPositionsToRemove.Count);
                }

                if (context.RemoveAction == RemoveActionType.RemovePositionsAndAddToCarPark)
                {
                    context.LogInformation(EventLogEvent.PlanogramPositionsRemovedCarPark, context.PlanogramPositionsToRemove.Count);
                }
            }
        }

        /// <summary>
        /// Moves positions from the planogram to the car park component
        /// </summary>
        private Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            // if we are not moving positions to the car park component
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

            // check that we have some positions to add
            if (!context.PlanogramPositionsToRemove.Any()) return true;

            // if we have no car park component, then do nothing
            if (context.CarParkMerchandisingGroup == null) return true;

            // create a new position in the car park merchandising group
            // for each position in the list of positions to remove
            foreach (PlanogramPosition oldPosition in context.PlanogramPositionsToRemove)
            {
                // get the planogram product
                PlanogramProduct product = context.PlanogramProductIdLookup[oldPosition.PlanogramProductId];

                // create a new position in the merchandising group
                context.CarParkMerchandisingGroup.InsertPositionPlacement(product);
            }

            // apply changes to the car park component
            context.CarParkMerchandisingGroup.Process();
            context.CarParkMerchandisingGroup.ApplyEdit();

            // return success
            return true;
        }

        /// <summary>
        /// Removes products from the planogram
        /// </summary>
        private Boolean RemoveProductsFromPlanogram(TaskContext context)
        {
            // if we are moving positions to the car park shelf
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndProducts) return true;

            // check that we have some products to remove
            if (!context.PlanogramProductsToRemove.Any()) return true;

            // remove products from the planogram
            context.Planogram.Products.RemoveList(context.PlanogramProductsToRemove);

            // return success
            return true;
        }

        /// <summary>
        /// Removes positions from the planogram
        /// </summary>
        private Boolean RemovePositionsFromPlanogram(TaskContext context)
        {
            // check that we have some positions to remove
            if (!context.PlanogramPositionsToRemove.Any()) return true;

            // remove all positions from the planogram
            context.Planogram.Positions.RemoveList(context.PlanogramPositionsToRemove);

            // return success
            return true;
        }

        /// <summary>
        /// Retrieves the car park component if it exists
        /// else creates the car park component
        /// </summary>
        private Boolean GetCarParkComponent(TaskContext context)
        {
            // if we are not moving positions to the car park shelf
            // then there is nothing for us to do
            if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

            // if there are no positions to remove, then there
            // is nothing for use to do either
            if (!context.PlanogramPositionsToRemove.Any()) return true;

            // get the merchandising groups
            context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();

            // get the car park component
            context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

            // return success
            return true;
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private Boolean ReMerchandisePlanogram(TaskContext context)
        {
            // check that some positions have either been moved or removed
            if (!context.PlanogramPositionsToRemove.Any()) return true;

            // remerchandise the whole planogram
            context.RemerchandisePlanogram();

            // return success
            return true;
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    } 
}
