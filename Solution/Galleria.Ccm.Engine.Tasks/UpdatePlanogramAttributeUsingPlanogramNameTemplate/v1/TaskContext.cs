﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31831 : A.Probyn
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributeUsingPlanogramNameTemplate.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields

        #endregion

        #region Constructor

        public TaskContext(ITaskContext context)
            : base(context)
        {
            
        }

        #endregion

        #region Properties

        /// <summary>
        /// The name of the Planogram Name Template to load.
        /// </summary>
        public String PlanogramNameTemplateName { get; set; }

        /// <summary>
        /// The planogram name template to update from.
        /// </summary>
        public PlanogramNameTemplate PlanogramNameTemplate { get; set; }

        #endregion
    }
}