﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31831 : A.Probyn
//  Created.
// V8-32218 : A.Probyn
//  Corrected Csla namespace being referenced. Should be Csla not Csla.Server. The wrong namespace dataportalexception 
//  was being caught.
// V8-32300 : A.Probyn
//  Updated to ensure if planogram name is being set, that the value is unique per planogram group.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributeUsingPlanogramNameTemplate.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PlanogramNameTemplate = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramNameTemplate,
                Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_PlanogramNameTemplate,
                Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_PlanogramNameTemplate_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.PlanogramNameTemplateNameSingle,
                null,
                false,
                false,
                parentId: null,
                isRequired: () => { return String.Empty; },
                isNullAllowed: () => { return true; },
                isValid: null));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!GetPlanogramNameTemplate(taskContext)) return;
                if (!UpdatePlanogramAttribute(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets parameter values, valdidates them and assigns them to the the given arguments.
        /// </summary>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region Planogram Name Template
                if ((!Parameters.Contains((Int32)Parameter.PlanogramNameTemplate)) ||
                    (Parameters[Parameter.PlanogramNameTemplate].Values == null) ||
                    (Parameters[Parameter.PlanogramNameTemplate].Value == null))
                {
                    //  If there is no planogram name template selection the task has nothing to do.
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_NoTemplateSelected);
                    return false;
                }
                try
                {
                    taskContext.PlanogramNameTemplateName = (String)Parameters[Parameter.PlanogramNameTemplate].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_PlanogramNameTemplate_Name, 
                        Message.Error_NoValue);
                    return false;
                }

                #endregion

                return true;
            }
        }
        
        /// <summary>
        /// Gets the Planogram Template based on its name parameter and stores it in the task context.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean GetPlanogramNameTemplate(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (String.IsNullOrEmpty(taskContext.PlanogramNameTemplateName)) return false;

                try
                {
                    taskContext.PlanogramNameTemplate = PlanogramNameTemplate.FetchByEntityIdName(taskContext.EntityId, taskContext.PlanogramNameTemplateName);
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw;

                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramAttributeUsingPlanogramNameTemplate_PlanogramNameTemplate_Name,
                        taskContext.PlanogramNameTemplateName);
                    return false;
                    
                }

                return true;
            }
        }
        
        /// <summary>
        /// Updates the planogram with the supplied planogram attribute selection detail, list of attributes along with values to update.
        /// </summary>
        /// <param name="planogram">The planogram</param>
        /// <param name="planogramAttributeSelections">The list of planogram attribute selections</param>
        private Boolean UpdatePlanogramAttribute(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (taskContext.PlanogramNameTemplate == null) return false;

                // update the planogram attribute from template
                String templateFormula = taskContext.PlanogramNameTemplate.BuilderFormula;

                // new planogram attribute field
                String newPlanogramAttributeField = null;

                //Get source planogram for this context
                Planogram sourcePlanogram = this.GetSourcePlanogram(taskContext);

                if(sourcePlanogram != null)
                {
                    try
                    {
                        List<ObjectFieldInfo> availableFormulaFields = new List<ObjectFieldInfo>();
                        availableFormulaFields.AddRange(Planogram.EnumerateDisplayableFieldInfos(false));
                        availableFormulaFields.AddRange(Location.EnumerateDisplayableFieldInfos());
                        availableFormulaFields.AddRange(LocationSpaceProductGroup.EnumerateDisplayableFieldInfos());

                        //Create object field expression
                        ObjectFieldExpression expression = new ObjectFieldExpression(templateFormula);
                        
                        //Extract fields
                        IEnumerable<ObjectFieldInfo> formulaFields = ObjectFieldInfo.ExtractFieldsFromText(templateFormula, availableFormulaFields);
                        
                        //Get linked data (if required by formula fields)
                        Location linkedLocation = null;
                        LocationSpaceProductGroupInfo linkedLocationSpaceData = null;
                        if(formulaFields.Any(p => p.OwnerType.Equals(typeof(LocationSpaceProductGroup)) || p.OwnerType.Equals(typeof(Location))))
                        {
                            if(linkedLocation == null && !String.IsNullOrEmpty(sourcePlanogram.LocationCode))
                            {
                                LocationInfoList infoList = LocationInfoList.FetchByEntityIdLocationCodes(taskContext.EntityId, new List<String>() { sourcePlanogram.LocationCode});
                                if (infoList.Count > 0)
                                {
                                    linkedLocation = Location.FetchById(infoList.First().Id);
                                }
                            }
                            if(formulaFields.Any(p => p.OwnerType.Equals(typeof(LocationSpaceProductGroup))))
                            {
                                if (linkedLocationSpaceData == null 
                                    && !String.IsNullOrEmpty(sourcePlanogram.CategoryCode)
                                    && linkedLocation != null)
                                {
                                    ProductGroupInfoList categoryInfoList = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { sourcePlanogram.CategoryCode });
                                    if(categoryInfoList.Count > 0)
                                    {
                                        linkedLocationSpaceData = LocationSpaceProductGroupInfoList.FetchByProductGroupId(categoryInfoList.First().Id).FirstOrDefault(p => p.LocationId.Equals(linkedLocation.Id));
                                    }
                                }
                            }
                        }
                        
                        //Enumerate through fields getting value from linked object
                        foreach (ObjectFieldInfo fieldInfo in formulaFields)
                        { 
                            Object value = null;
                            if(fieldInfo.OwnerType.Equals(typeof(Planogram)))
                            {
                                value = fieldInfo.GetValue(sourcePlanogram);
                            }
                            else if (fieldInfo.OwnerType.Equals(typeof(Location)))
                            {
                                value = fieldInfo.GetValue(linkedLocation);
                            }
                            else if (fieldInfo.OwnerType.Equals(typeof(LocationSpaceProductGroup)))
                            {
                                value = fieldInfo.GetValue(linkedLocationSpaceData);                                
                            }
                            expression.AddParameter(fieldInfo.FieldPlaceholder, value);
                        }

                        //evaluate the formula against the source planogram
                        Object returnValue = expression.Evaluate();

                        if (returnValue != null)
                        {
                            newPlanogramAttributeField = returnValue.ToString();
                        }
                    }
                    catch (Exception)
                    { 
                        //Log the failure to resolve the formulae
                        taskContext.LogInformation(EventLogEvent.PlanogramNameTemplateFormulaCouldNotBeResolved, taskContext.PlanogramNameTemplateName);
                    }

                    if (!String.IsNullOrEmpty(newPlanogramAttributeField)
                        && !String.IsNullOrWhiteSpace(newPlanogramAttributeField))
                    {
                        //If name is being updated we need to ensure it's unique
                        if (taskContext.PlanogramNameTemplate.PlanogramAttribute == PlanogramNameTemplatePlanogramAttributeType.PlanogramName)
                        {
                            PlanogramInfoList planInfo = PlanogramInfoList.FetchByIds(new List<Int32>() { taskContext.WorkpackagePlanogram.DestinationPlanogramId });

                            if (planInfo.Any())
                            {
                                //Fetch sibling plans
                                PlanogramInfoList siblingPlanogramGroupPlans = PlanogramInfoList.FetchByPlanogramGroupId(planInfo.First().PlanogramGroupId.Value);

                                //Ensure name is unique
                                newPlanogramAttributeField = PlanogramNameTemplate.ValidatePlanogramNameUnique(newPlanogramAttributeField, siblingPlanogramGroupPlans.Where(p => !p.Id.Equals(taskContext.Planogram.Id)).Select(p => p.Name).ToList());
                            }
                        }

                        #region Set Planogram Attribute Value
                        //Set value in planogram based on 
                        switch (taskContext.PlanogramNameTemplate.PlanogramAttribute)
                        {
                            case PlanogramNameTemplatePlanogramAttributeType.PlanogramName:
                                taskContext.Planogram.Name = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text1:
                                taskContext.Planogram.CustomAttributes.Text1 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text2:
                                taskContext.Planogram.CustomAttributes.Text2 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text3:
                                taskContext.Planogram.CustomAttributes.Text3 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text4:
                                taskContext.Planogram.CustomAttributes.Text4 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text5:
                                taskContext.Planogram.CustomAttributes.Text5 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text6:
                                taskContext.Planogram.CustomAttributes.Text6 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text7:
                                taskContext.Planogram.CustomAttributes.Text7 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text8:
                                taskContext.Planogram.CustomAttributes.Text8 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text9:
                                taskContext.Planogram.CustomAttributes.Text9 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text10:
                                taskContext.Planogram.CustomAttributes.Text10 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text11:
                                taskContext.Planogram.CustomAttributes.Text11 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text12:
                                taskContext.Planogram.CustomAttributes.Text12 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text13:
                                taskContext.Planogram.CustomAttributes.Text13 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text14:
                                taskContext.Planogram.CustomAttributes.Text14 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text15:
                                taskContext.Planogram.CustomAttributes.Text15 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text16:
                                taskContext.Planogram.CustomAttributes.Text16 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text17:
                                taskContext.Planogram.CustomAttributes.Text17 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text18:
                                taskContext.Planogram.CustomAttributes.Text18 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text19:
                                taskContext.Planogram.CustomAttributes.Text19 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text20:
                                taskContext.Planogram.CustomAttributes.Text20 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text21:
                                taskContext.Planogram.CustomAttributes.Text21 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text22:
                                taskContext.Planogram.CustomAttributes.Text22 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text23:
                                taskContext.Planogram.CustomAttributes.Text23 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text24:
                                taskContext.Planogram.CustomAttributes.Text24 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text25:
                                taskContext.Planogram.CustomAttributes.Text25 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text26:
                                taskContext.Planogram.CustomAttributes.Text26 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text27:
                                taskContext.Planogram.CustomAttributes.Text27 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text28:
                                taskContext.Planogram.CustomAttributes.Text28 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text29:
                                taskContext.Planogram.CustomAttributes.Text29 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text30:
                                taskContext.Planogram.CustomAttributes.Text30 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text31:
                                taskContext.Planogram.CustomAttributes.Text31 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text32:
                                taskContext.Planogram.CustomAttributes.Text32 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text33:
                                taskContext.Planogram.CustomAttributes.Text33 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text34:
                                taskContext.Planogram.CustomAttributes.Text34 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text35:
                                taskContext.Planogram.CustomAttributes.Text35 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text36:
                                taskContext.Planogram.CustomAttributes.Text36 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text37:
                                taskContext.Planogram.CustomAttributes.Text37 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text38:
                                taskContext.Planogram.CustomAttributes.Text38 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text39:
                                taskContext.Planogram.CustomAttributes.Text39 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text40:
                                taskContext.Planogram.CustomAttributes.Text40 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text41:
                                taskContext.Planogram.CustomAttributes.Text41 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text42:
                                taskContext.Planogram.CustomAttributes.Text42 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text43:
                                taskContext.Planogram.CustomAttributes.Text43 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text44:
                                taskContext.Planogram.CustomAttributes.Text44 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text45:
                                taskContext.Planogram.CustomAttributes.Text45 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text46:
                                taskContext.Planogram.CustomAttributes.Text46 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text47:
                                taskContext.Planogram.CustomAttributes.Text47 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text48:
                                taskContext.Planogram.CustomAttributes.Text48 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text49:
                                taskContext.Planogram.CustomAttributes.Text49 = newPlanogramAttributeField;
                                break;
                            case PlanogramNameTemplatePlanogramAttributeType.Text50:
                                taskContext.Planogram.CustomAttributes.Text50 = newPlanogramAttributeField;
                                break;
                        }
                        #endregion

                        // log the success
                        taskContext.LogInformation(EventLogEvent.PlanogramNameTemplateApplied, PlanogramNameTemplatePlanogramAttributeTypeHelper.FriendlyNames[taskContext.PlanogramNameTemplate.PlanogramAttribute], newPlanogramAttributeField, taskContext.PlanogramNameTemplateName);
                    }
                    else
                    {
                        //Log the failure to resolve the formulae
                        taskContext.LogInformation(EventLogEvent.PlanogramNameTemplateFormulaCouldNotBeResolved, taskContext.PlanogramNameTemplateName);                    
                    }
                }

                // return success
                return true;                    
            }
        }
        
        /// <summary>
        /// Returns the source planogram to base the new planogram attribute from
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Planogram GetSourcePlanogram(TaskContext taskContext)
        {
            //try find thge source planogram to get its original name as the workpackage planogram name
            //may already have been altered, if not revert to created planogram
            if (taskContext.WorkpackagePlanogram.SourcePlanogramId.HasValue)
            {
                // attempt to fetch the source planogram by its name
                try
                {
                    return Package.FetchById(taskContext.WorkpackagePlanogram.SourcePlanogramId).Planograms[0];
                }
                catch (DataPortalException ex)
                {
                    //Revert back to new planogram created in workpackage
                    return taskContext.Planogram;

                    //No need to log as there is a fallback
                }
            }
            else
            {
                //Revert back to new planogram created in workpackage
                return taskContext.Planogram;
            }
        }
        
        #endregion

    }    
}