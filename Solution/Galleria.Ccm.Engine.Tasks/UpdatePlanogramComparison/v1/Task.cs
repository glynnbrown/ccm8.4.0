﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-31909 : A.Silva
//  Created
// V8-31944 : A.Silva
//  Added PlanogramComparisonSettings parameter.
// V8-32120 : A.Silva
//  Amended ValidateParameters and LogCompletion so that the task now logs information if the Template Name was null or empty.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramComparison.v1
{
    /// <summary>
    ///     Performs a Planogram Comparison with the source planogram to indicate any changes.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version

        /// <summary>
        ///     The task's minor version number.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        /// <summary>
        ///     The task's major version number.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        #endregion

        #region Parameter

        private enum Parameter
        {
            PlanogramComparisonTemplateName = 0
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Get the task's name
        /// </summary>
        public override String Name { get { return Message.UpdatePlanogramComparison_Name; } }

        /// <summary>
        ///     Get the task's description
        /// </summary>
        public override String Description { get { return Message.UpdatePlanogramComparison_Description; } }

        /// <summary>
        ///     Get the task's grouping category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_PlanogramMaintenance; } }

        /// <summary>
        ///     Get the task's minor version number
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     Get the task's major version number
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        /// <summary>
        /// Indicates if this task is visible
        /// </summary>
        /// <remarks>NB: Overriden to hide the task for now, as the features do not seem complete enough.</remarks>
        public override Boolean IsVisible { get { return true; } }

        #endregion

        #region Methods

        #region Parameter Registration

        /// <summary>
        ///     Invoked when the task is registering parameters.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            RegisterParameter(PlanogramComparisonSettingsNameParameter);
        }

        private static TaskParameter PlanogramComparisonSettingsNameParameter
        {
            get
            {
                return new TaskParameter(
                    Parameter.PlanogramComparisonTemplateName,
                    Message.UpdatePlanogramComparison_Parameter_PlanogramComparisonTemplateName_Name,
                    Message.UpdatePlanogramComparison_Parameter_PlanogramComparisonTemplateName_Description,
                    Message.ParameterCategory_Default,
                    TaskParameterType.PlanogramComparisonTemplateNameSingle,
                    null,
                    false,
                    false);
            }
        }

        #endregion

        /// <summary>
        ///     Invoked when the task is being executed.
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetPlanogramComparisonTemplate(taskContext)) return;
                if (!UpdateComparisonFields(taskContext)) return;
                if (!CompareToSourcePlanogram(taskContext)) return;
                LogCompletion(taskContext);
            }
        }

        /// <summary>
        ///     Validate and store the task parameters.
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            var success = true;

            #region PlanogramComparisonTemplateName

            //  Check whether the parameter exists, has been assigned or contains a string value.
            //  NB: Null values for this parameter are fine.
            if (Parameters.Contains((Int32) Parameter.PlanogramComparisonTemplateName) &&
                Parameters[Parameter.PlanogramComparisonTemplateName].Value != null)
            {
                context.PlanogramComparisonTemplateName = Convert.ToString(Parameters[Parameter.PlanogramComparisonTemplateName].Value.Value1);
            }

            #endregion

            return success;
        }

        /// <summary>
        ///     If there was a template name provided, fetch the matching template from the repository.
        /// </summary>
        private static Boolean GetPlanogramComparisonTemplate(TaskContext context)
        {
            var success = true;

            //  Check whether there is a template to get.
            if (!String.IsNullOrWhiteSpace(context.PlanogramComparisonTemplateName))
            {
                try
                {
                    //  Fetch the specified template by name.
                    context.PlanogramComparisonTemplate = PlanogramComparisonTemplate.FetchByEntityIdName(context.EntityId, context.PlanogramComparisonTemplateName);
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw;

                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.UpdatePlanogramComparison_Parameter_PlanogramComparisonTemplateName_Name,
                                     context.PlanogramComparisonTemplateName);
                    success = false;
                }
            }

            return success;
        }

        /// <summary>
        ///     Update the comparison fields from the template, if there is one. Otherwise, clear the collection to force default values.
        /// </summary>
        private static Boolean UpdateComparisonFields(TaskContext context)
        {
            PlanogramComparisonFieldList fields = context.Planogram.Comparison.ComparisonFields;

            //  Clear existing fields so we get at least default values.
            //  NB: When running the comparison, if the Comparison Fields are empty, default values will be used.
            //      See Galleria.Framework.Planograms.Helpers.PlanogramComparerHelper.CheckComparisonFields()
            fields.Clear();

            //  Add template fields if a template was provided.
            if (context.PlanogramComparisonTemplate != null) fields.AddRange(context.PlanogramComparisonTemplate.ComparisonFields);

            //  Return success.
            return true;
        }

        /// <summary>
        ///     Run the comparison and store the results in the Planogram. Only the Source Planogram will be compared.
        /// </summary>
        private static Boolean CompareToSourcePlanogram(ITaskContext context)
        {
            PlanogramComparison comparison = context.Planogram.Comparison;
            IEnumerable<Planogram> targetPlanograms = new List<Planogram> { Package.FetchById(context.WorkpackagePlanogram.SourcePlanogramId).Planograms.First() };
            comparison.UpdateResults(targetPlanograms);
            comparison.Results.First().PlanogramName = Message.UpdatePlanogramComparison_DefaultSourcePlanName;

            return true;
        }

        /// <summary>
        ///     Perform all logging after the task completes.
        /// </summary>
        /// <param name="context">Context containing data needed to generate the task's log.</param>
        private static void LogCompletion(TaskContext context)
        {
            String targetPlanograms = String.Join(",", context.Planogram.Comparison.Results.Select(result => result.PlanogramName));
            String templateName = context.PlanogramComparisonTemplateName;
            if (String.IsNullOrWhiteSpace(templateName)) templateName = "Default";
            context.LogInformation(EventLogEvent.PlanogramComparisonTemplateApplied, targetPlanograms, templateName);
        }

        #endregion
    }
}