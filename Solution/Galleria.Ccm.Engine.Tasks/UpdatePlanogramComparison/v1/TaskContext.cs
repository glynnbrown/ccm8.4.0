﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-31909 : A.Silva
//  Created

#endregion

#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramComparison.v1
{
    public sealed class TaskContext : Ccm.Engine.TaskContext
    {
        #region Properties

        public String PlanogramComparisonTemplateName { get; set; }
        public PlanogramComparisonTemplate PlanogramComparisonTemplate { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) {}

        #endregion
    }
}