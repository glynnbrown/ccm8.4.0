﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32520 : N.Haywood
//  Created.
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsComponentNameValid() method.
// V8-32639 : A.Heathcote
//  Changed the Category to UpdatePlanogram from MerchandisePlanogram.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.AddComponent.v1
{
    /// <summary>
    /// Add a component to a plan if a component with the same name does not already exist
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version

        private readonly Byte _majorVersion = 1; // holds the tasks major version number
        private readonly Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter

        private enum Parameter
        {
            ComponentName = 0,
            AddLinkedTextbox = 1,
        }

        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name { get { return Message.AddComponent_Name; } }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description { get { return Message.AddComponent_Description; } }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category { get { return Message.TaskCategory_UpdatePlanogram; } }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        #endregion

        #region Parameter Registration

        protected override void OnRegisterParameters()
        {
            // product codes
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.AddLinkedTextbox,
                    name: Message.AddComponent_AddLinkedTextbox_Name,
                    description: Message.AddComponent_AddLinkedTextbox_Description,
                    category: Message.ParameterCategory_Default,
                    enumType: typeof(AddLinkedTextboxType),
                    defaultValue: AddLinkedTextboxType.Yes,
                    isHiddenByDefault: true,
                    isReadOnlyByDefault: true));

            RegisterParameter(
                new TaskParameter(
                    id: Parameter.ComponentName,
                    name: Message.AddComponent_ComponentName_Name,
                    description: Message.AddComponent_ComponentName_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.String,
                    defaultValue: Message.AddComponent_ComponentName_DefaultValue,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false,
                    parentId: null,
                    isRequired: null,
                    isNullAllowed: null,
                    isValid: ()=> IsComponentNameValid()));

        }



        #endregion

        #region Execution

        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!AddComponent(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// This method checks to make sure that the Component Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsComponentNameValid()
        {
            if(this.Parameters == null || this.Parameters[Parameter.ComponentName] == null 
                || !this.Parameters.Contains((Int32)Parameter.ComponentName) 
                || this.Parameters[Parameter.ComponentName].Value == null 
                || this.Parameters[Parameter.ComponentName].Value.Value1 == null)
            {
                 return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.ComponentName].Value.Value1.ToString().Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsComponentNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }           
        }

        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region AddLinkedTextbox

                // validate the add to car park shelf
                if ((!Parameters.Contains((Int32)Parameter.AddLinkedTextbox)) ||
                    (Parameters[Parameter.AddLinkedTextbox].Value == null) ||
                    (Parameters[Parameter.AddLinkedTextbox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddComponent_AddLinkedTextbox_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        AddLinkedTextboxType addTextBox = (AddLinkedTextboxType)Convert.ToByte(Parameters[(Int32)Parameter.AddLinkedTextbox].Value.Value1);
                        context.AddLinkedTextbox = (addTextBox == AddLinkedTextboxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddComponent_AddLinkedTextbox_Name,
                                         Parameters[Parameter.AddLinkedTextbox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.ComponentName)) ||
                    (Parameters[Parameter.ComponentName].Value == null) ||
                    (Parameters[Parameter.ComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddComponent_ComponentName_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.ComponentName = Convert.ToString(Parameters[Parameter.ComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.ComponentName)) ||
                        (String.IsNullOrWhiteSpace(context.ComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddComponent_ComponentName_Name,
                                         Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }
        

        private Boolean AddComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in context.Planogram.GetMerchandisingGroups())
                {
                    // enumerate throught the sub components
                    // of the merchandising group
                    foreach (PlanogramSubComponentPlacement subComponentPlacement in merchandisingGroup.SubComponentPlacements)
                    {
                        if (subComponentPlacement.SubComponent.Name == context.ComponentName)
                        {
                            return false;
                        }
                    }
                }
                context.CreateCarparkShelf(context.ComponentName, context.AddLinkedTextbox);

            }
            return true;
        }

        
        #endregion

    }
}
