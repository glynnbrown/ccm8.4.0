﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32520 : N.Haywood
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Engine.Tasks.AddComponent.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Properties

        /// <summary>
        /// The name of the component to add
        /// </summary>
        public String ComponentName { get; set; }

        /// <summary>
        /// Indicates whether or not we should add a linked text box to the component
        /// </summary>
        public Boolean AddLinkedTextbox { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Methods
        protected override void OnDipose()
        {
            base.OnDipose();
        }
        #endregion
    }
}
