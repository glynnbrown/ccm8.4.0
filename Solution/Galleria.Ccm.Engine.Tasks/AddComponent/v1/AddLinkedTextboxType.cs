﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32520 : N.Haywood
//  Created.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.AddComponent.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// add to carpark shelf param
    /// </summary>
    public enum AddLinkedTextboxType
    {
        Yes,
        No
    }

    /// <summary>
    /// Contains helpers for the AddLinkedTextboxType enum.
    /// </summary>
    public static class AddLinkedTextboxTypeHelper
    {
        public static readonly Dictionary<AddLinkedTextboxType, String> FriendlyNames =
            new Dictionary<AddLinkedTextboxType, String>()
            {
                {AddLinkedTextboxType.Yes, Message.Generic_Yes },
                {AddLinkedTextboxType.No, Message.Generic_No },
            };
    }
}
