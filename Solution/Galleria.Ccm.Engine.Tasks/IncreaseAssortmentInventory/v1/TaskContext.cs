﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803

// V8-29280 : A.Silva
//  Created

#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32800 : D.Pleasance
//  Added TargetUnitsByGtin
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        private PlanogramMerchandisingGroupList _merchandisingGroups;
        private PlanogramAssortmentProduct _currentAssortmentProduct;
        private PlanogramPositionPlacementList _currentProductPositions;

        #region Constructors

        /// <summary>
        ///     Creates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context)
        {
            StartingUnits = new Dictionary<String, Int32>();
            FinalUnits = new Dictionary<String, Int32>();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the top percentage of products.
        /// </summary>
        public Single TopPercentageOfProducts { private get; set; }

        /// <summary>
        ///     Gets or sets the primary <see cref="ProcessingMethodType"/>.
        /// </summary>
        public ProcessingMethodType ProcessingMethodOne { private get; set; }

        /// <summary>
        ///     Gets or sets the secondary <see cref="ProcessingMethodType"/>.
        /// </summary>
        public ProcessingMethodType ProcessingMethodTwo { private get; set; }

        public ProcessingMethodType CurrentProcessingMethod { get; private set; }

        /// <summary>
        ///     Gets or sets the stack of <see cref="PlanogramAssortmentProduct"/> instances to process.
        /// </summary>
        public List<PlanogramAssortmentProduct> AssortmentProducts { get; set; }

        public PlanogramMerchandisingGroupList MerchandisingGroups 
        {
            get
            {
                if (_merchandisingGroups != null) return _merchandisingGroups;

                _merchandisingGroups = Planogram.GetMerchandisingGroups();
                PlacementsByGtin = _merchandisingGroups.LookUpPlacementsByGtin();
                return _merchandisingGroups;
            }
        }

        public Dictionary<String, PlanogramPositionPlacementList> PlacementsByGtin { get; private set; }

        public TaskContext UseProcessingMethodOne
        {
            get
            {
                CurrentProcessingMethod = ProcessingMethodOne;
                CurrentAssortmentProducts =
                    AssortmentProducts.Take((Int32) Math.Ceiling(TopPercentageOfProducts*TotalNumberOfProducts)).ToList();
                return this;
            }
        }

        public List<PlanogramAssortmentProduct> CurrentAssortmentProducts { get; set; }

        public TaskContext UseProcessingMethodTwo
        {
            get
            {
                CurrentProcessingMethod = ProcessingMethodTwo;
                CurrentAssortmentProducts =
                   AssortmentProducts.Skip((Int32)Math.Ceiling(TopPercentageOfProducts * TotalNumberOfProducts)).ToList();
                return this;
            }
        }

        public Int32 ProductsWithNoPositions { get; set; }
        public Dictionary<String, Int32> StartingUnits { get; private set; }
        public Dictionary<String, Int32> FinalUnits { get; private set; }

        public PlanogramAssortmentProduct CurrentAssortmentProduct
        {
            get { return _currentAssortmentProduct; }
            set
            {
                _currentAssortmentProduct = value;
                _currentProductPositions = null;
                if (value != null) PlacementsByGtin.TryGetValue(value.Gtin, out _currentProductPositions);
            }
        }

        public Int32 ProductsAlreadyAtTarget { get; set; }
        public Int32 ProductsPartiallyAchievedUnits { get; set; }
        public Int32 ProductsAchievedTargets { get; set; }

        public PlanogramPositionPlacementList CurrentProductPositions
        {
            get
            {
                return _currentProductPositions ??
                       (_currentProductPositions = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList());
            }
        }

        public Int32 TotalNumberOfProducts { get; set; }
        public Int32 CurrentProductUnits { get; set; }

        /// <summary>
        /// A dictionary of target units by gtin
        /// </summary>
        public Dictionary<String, Int32> TargetUnitsByGtin { get; set; }

        #endregion
    }
}
