﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27852 : N.Foster
//  Created
// V8-27946 : A.Kuszyk
//  Updated CalculateAvailableSpace methods to account for divider space.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
// V8-28279 : A.Kuszyk
//  Amended to use Single percentage parameter.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM803

// V8-29502 : A.Silva
//      Refactored CalculateAvailableSpaceStrategy to use CalculateAvailableSpaceStrategy.
// V8-29280 : A.Silva
//      Refactored the task to bring it in line with other tasks,

#endregion
#region Version History: CCM810
// V8-29802 : M.Brumby
//  IncreaseUnits now passes in the merch group so that the correct merch group gets saved preventing much looping.
// V8-29597 : N.Foster
//  Code Review
// V8-29959 : D.Pleasance
//  Fixed object reference not set exception, context.CurrentAssortmentProduct was not being set when using ProcessingMethodType.MaximumIncreasePerPass
//  Also fixed TryIncreaseUnits method so that increase occurs on the correct merchandising group, previously was recreating the merchandising group 
//  each time so when total unit count was checked after each iteration it appeared that there was no change!
// V8-29801 : M.Brumby
//  Single increase is now actually limited by the assortment units.
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
// V8-30103 : N.Foster
//  Autofill performance enhancements
#endregion
#region Version History: CCM811
// V8-30387 : M.Brumby
//  Fail if the assortment has 0 products. (In Line with other tasks)
#endregion
#region Version History: CCM820
// V8-30710 : M.Brumby
//  Make missing assortment messaging consistent across tasks.
// V8-30761 : A.Kuszyk
//  Ensured that units are always decreased after an unsuccessful increase.
// V8-30990 : D.Pleasance
//  Amended default task parameter, top percentage of products 25%, ProcessingMethodOne - ProcessingMethodType.MaximumIncreasePerPass
// V8-31615 : A.Kuszyk
//  Added a call to PlanogramPositionPlacement.RecalculateUnits() following inventory changes.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32358 : D.Pleasance
//  Amended LogInventoryChanges() to log inventory changes as information.
// V8-32800 : D.Pleasance
//  Changes applied to increase inventory units fairly between positions that are targeted to increase. 
//  To achieve this we keep a running total of the target units whilst checking this against the actual placed units.
// V8-32787 : A.Silva
//  Amended TryIncreaseUnits so that assortment rules are enforced.
//  Added an AssortmentRuleEnforcer field to allow easy enabling of rule enforcing.
// CCM-18435 : A.Silva
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using System.Text;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1
{
    /// <summary>
    /// A task that increases product inventory
    /// to match the inventory specified within the assortment
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();

        #region Version

        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number

        #endregion

        #region Parameter
        private enum Parameter
        {
            TopPercentageOfProducts = 0,
            ProcessingMethodOne = 1,
            ProcessingMethodTwo = 2
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.IncreaseAssortmentInventory_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.IncreaseAssortmentInventory_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // top percentage of products
            this.RegisterParameter(new TaskParameter(
                Parameter.TopPercentageOfProducts,
                Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name,
                Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                0.25,
                false,
                false));

            // processing method one
            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingMethodOne,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingMethodType),
                ProcessingMethodType.MaximumIncreasePerPass,
                false,
                false));

            // processing method two
            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingMethodTwo,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name,
                Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingMethodType),
                ProcessingMethodType.SingleIncrementPerPass,
                false,
                false));
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetAssortmentProducts(taskContext)) return;
                if (!BuildDictionaries(taskContext)) return;
                if (!IncreaseInventory(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validate the parameter values for this task.
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region TopPercentageOfProducts

                //  Validate that the top percentage of products parameter exists
                if (!Parameters.Contains((Int32)Parameter.TopPercentageOfProducts) ||
                    Parameters[Parameter.TopPercentageOfProducts].Value == null ||
                    Parameters[Parameter.TopPercentageOfProducts].Value.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.TopPercentageOfProducts = Convert.ToSingle(Parameters[Parameter.TopPercentageOfProducts].Value.Value1);
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_TopPercentageOfProducts_Name, Parameters[Parameter.TopPercentageOfProducts].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProcessingMethodOne

                //  Validate that the processing method one parameter exists
                if (!Parameters.Contains((Int32)Parameter.ProcessingMethodOne) ||
                    Parameters[Parameter.ProcessingMethodOne].Value == null ||
                    Parameters[Parameter.ProcessingMethodOne].Value.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.ProcessingMethodOne = (ProcessingMethodType)Parameters[Parameter.ProcessingMethodOne].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod1_Name, Parameters[Parameter.ProcessingMethodOne].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region ProcessingMethodTwo

                //  Validate that the processing method one parameter exists
                if (!Parameters.Contains((Int32)Parameter.ProcessingMethodTwo) ||
                    Parameters[Parameter.ProcessingMethodTwo].Value == null ||
                    Parameters[Parameter.ProcessingMethodTwo].Value.Value1 == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.ProcessingMethodTwo = (ProcessingMethodType)Parameters[Parameter.ProcessingMethodTwo].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.IncreaseAssortmentInventory_Parameter_ProcessingMethod2_Name, Parameters[Parameter.ProcessingMethodTwo].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Populates the task context with the assortment products
        /// </summary>
        private static Boolean GetAssortmentProducts(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                // verify we have an assortment
                if ((context.Planogram.Assortment == null) ||
                    (context.Planogram.Assortment.Products == null) ||
                    ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                {
                    context.LogWarning(EventLogEvent.PlanogramNoAssortmentPresent);
                    success = false;
                }
                else
                {
                    // get the assortment products ordered by rank and then GTIN
                    // NF - we are only interested in ranged products within the assortment
                    context.AssortmentProducts = context.Planogram.Assortment.Products.Where(p => p.IsRanged).OrderBy(p => p.Rank).ThenBy(p => p.Gtin).ToList();
                    context.TotalNumberOfProducts = context.AssortmentProducts.Count;
                }

                return success;
            }
        }

        /// <summary>
        /// Populates the task context dictionaries 
        /// </summary>
        private static Boolean BuildDictionaries(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.TargetUnitsByGtin = new Dictionary<String, Int32>();

                foreach (PlanogramMerchandisingGroup merchandisingGroup in context.MerchandisingGroups)
                {
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        String productGtin = positionPlacement.Product.Gtin;
                        if (context.TargetUnitsByGtin.ContainsKey(productGtin)) continue;
                        context.TargetUnitsByGtin.Add(productGtin, positionPlacement.Position.TotalUnits);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Performs the increase of inventory
        /// </summary>
        private Boolean IncreaseInventory(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                using (context.MerchandisingGroups)
                {
                    IterateThroughProductsAndIncreaseInventory(context.UseProcessingMethodOne);
                    IterateThroughProductsAndIncreaseInventory(context.UseProcessingMethodTwo);
                }

                return success;
            }
        }

        /// <summary>
        /// Logs final details about the task execution.
        /// </summary>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Log that the assortment was correctly increased.
                context.LogInformation(
                    EventLogEvent.PlanogramAssortmentProductsUnitsIncreased,
                    context.TotalNumberOfProducts,
                    context.ProductsAlreadyAtTarget,
                    context.ProductsAchievedTargets,
                    context.ProductsPartiallyAchievedUnits,
                    context.ProductsWithNoPositions);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Iterates through the given assortment products, increasing units in accordance
        /// with the processing method provided.
        /// </summary>
        /// <param name="context">The task context.</param>
        private static void IterateThroughProductsAndIncreaseInventory(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (context.CurrentProcessingMethod == ProcessingMethodType.SingleIncrementPerPass)
                {
                    // we now enumerate through this list several
                    // times, each time increasing the inventory of
                    // a product once, so that we spread out the increasing
                    // of inventory across all products in the assortment
                    // if possible, but with higher ranking products taking
                    // preference
                    while (context.CurrentAssortmentProducts.Any())
                    {
                        // build a list of products to remove
                        // from this list of assortment products
                        // after each pass through
                        var assortmentProductsToRemove = new List<PlanogramAssortmentProduct>();

                        // now enumerate through our assortment products
                        foreach (PlanogramAssortmentProduct assortmentProduct in context.CurrentAssortmentProducts)
                        {
                            //  Set the current assortment product being processed.
                            context.CurrentAssortmentProduct = assortmentProduct;

                            //  If there are no current positions to process...
                            if (!context.CurrentProductPositions.Any())
                            {
                                // Log the products without positions.
                                context.ProductsWithNoPositions++;

                                //  Mark the assortment product for removal so it is no longer processed.
                                assortmentProductsToRemove.Add(assortmentProduct);

                                continue;
                            }

                            if (TryIncreaseInventory(context))
                            {
                                assortmentProductsToRemove.Add(assortmentProduct);
                            }
                        }

                        // now remove all assortment products that
                        // can no longer be increased
                        foreach (PlanogramAssortmentProduct assortmentProduct in assortmentProductsToRemove)
                        {
                            context.CurrentAssortmentProducts.Remove(assortmentProduct);
                        }
                    }
                }
                else if (context.CurrentProcessingMethod == ProcessingMethodType.MaximumIncreasePerPass)
                {
                    // Now, instead of increasing each product once in turn, we iterate
                    // over the products and increase each on until we run out of space.
                    foreach (PlanogramAssortmentProduct assortmentProduct in context.CurrentAssortmentProducts)
                    {
                        // first check to see if the assortment product
                        // exists on the planogram
                        PlanogramPositionPlacementList positions;
                        if (!context.PlacementsByGtin.TryGetValue(assortmentProduct.Gtin, out positions))
                        {
                            context.ProductsWithNoPositions++; // Log products with no positions.
                            continue;
                        }

                        //  Set the current assortment product being processed.
                        context.CurrentAssortmentProduct = assortmentProduct;

                        // Keep increasing inventory until there is no space left.
                        Boolean canIncreaseInventory;
                        do
                        {
                            canIncreaseInventory = !TryIncreaseInventory(context);
                        } while (canIncreaseInventory);
                    }
                }

                LogInventoryChanges(context);
            }
        }

        private static void LogInventoryChanges(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                StringBuilder inventoryChanges = new StringBuilder();
                Int32 totalInventoryChanges = 0;

                // Log the changes
                foreach (PlanogramAssortmentProduct assortmentProduct in context.AssortmentProducts)
                {
                    String currentGtin = assortmentProduct.Gtin;

                    //  Get the starting units,
                    Int32 startingUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.StartingUnits.TryGetValue(currentGtin, out startingUnits)) continue;

                    //  Get the final units,
                    Int32 finalUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.FinalUnits.TryGetValue(currentGtin, out finalUnits)) continue;

                    //  The increase had no effect.
                    if (startingUnits == finalUnits) continue;

                    inventoryChanges.AppendFormat(Message.TaskLogging_PlanogramProductInventoryIncreased,
                                        currentGtin, assortmentProduct.Name, startingUnits, finalUnits);

                    totalInventoryChanges++;
                }

                //  Log the change.
                context.LogInformation(
                    EventLogEvent.PlanogramProductInventoryIncreased,
                    totalInventoryChanges,
                    inventoryChanges);
            }
        }

        /// <summary>
        /// Attempts to increase the units for the given position and registers the result
        /// in the given logging context.
        /// </summary>
        /// <param name="context">The logging context in which to register the increase.</param>
        /// <returns>
        /// True if the target units are achieved.
        /// False if the target units have not been achieved and further increases should be attempted.
        /// Null if the target units have not been achieved and there is no space for further increases.
        /// </returns>
        private static Boolean TryIncreaseInventory(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                PlanogramAssortmentProduct assortmentProduct = context.CurrentAssortmentProduct;
                String currentGtin = assortmentProduct.Gtin;

                // get the current units for this assortment product
                context.CurrentProductUnits = context.CurrentProductPositions.GetPositionDetails().TotalUnitCount;

                //  If there are no starting units for this product,
                //  register the current units, this must be the first time
                //  that increase inventory is attempted on this product.
                if (!context.StartingUnits.ContainsKey(currentGtin))
                {
                    context.StartingUnits.Add(currentGtin, context.CurrentProductUnits);

                    //  If the product units are already on target log it
                    //  and return false (cannot increase inventory more).
                    if (context.CurrentAssortmentProduct.Units <= context.CurrentProductUnits)
                    {
                        context.ProductsAlreadyAtTarget++;
                        return false;
                    }
                }

                // Try to increase the inventory by one and remove the product if
                // no more increases are required.
                Boolean? unitsAchieved = IncreaseUnits(context);
                if (unitsAchieved == null)
                {
                    context.ProductsPartiallyAchievedUnits++;
                    context.FinalUnits[currentGtin] = context.CurrentProductUnits;
                    return true;
                }

                // If unitsAchieved is false, continue with iteration.
                if (!unitsAchieved.Value) return false;

                context.ProductsAchievedTargets++;
                context.FinalUnits[currentGtin] = context.CurrentProductUnits;
                return true;
            }
        }

        /// <summary>
        /// Increases the inventory of the given assortment product by one unit.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>
        /// True if the target units were achieved by the increase.
        /// False if the target units have not yet been achieved.
        /// Null if there is no space for further increases and the target units have not been achieved.
        /// </returns>
        private static Boolean? IncreaseUnits(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the assortment product target units
                Int32 targetUnits = context.CurrentAssortmentProduct.Units;

                if (context.CurrentProductUnits < targetUnits)
                {
                    // attempt to increase the inventory for 
                    // a position that exists within our position list
                    // ensure we have something to do

                    // so heres where things get hard!
                    // we need to increase the inventory
                    // of one of our positions
                    // without causing an overlap with any of
                    // our other positions - unless our
                    // sub component allows overlaps of course!

                    // enumerate through our positions based on the
                    // smallest total units first

                    //  Order the placements in ascending unit count (try first to increase the ones with less units).
                    var placementsOrderedByUnitCount = context.CurrentProductPositions
                        .OrderBy(p => p.GetPositionDetails().TotalUnitCount)
                        .ThenBy(p => p.Id);

                    //  Return the first, or null, position placement that successfully increases units.
                    PlanogramPositionPlacement positionAffected =
                        placementsOrderedByUnitCount.FirstOrDefault(p => TryIncreaseUnits(context, p));

                    // determine if we managed to increase
                    // the inventory for a position
                    if (positionAffected == null)
                    {
                        // if we could not increase the inventory for
                        // a position, then we are finished with this
                        // assortment product so flag it for removal
                        return null;
                    }

                    // get the current units again
                    context.CurrentProductUnits = context.CurrentProductPositions.GetPositionDetails().TotalUnitCount;

                    // if we are now equal to or above our target
                    // units then remove this assortment product
                    if (context.CurrentProductUnits >= targetUnits)
                    {
                        return true;
                    }
                }
                else
                {
                    // this case should never actually happen, but
                    // just in case ensure the assortment product
                    // is added to the list of assortment products
                    // to be removed
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        ///     Attempts to increase units for the specified position <paramref name="placement"/>.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="placement">The <see cref="PlanogramPositionPlacement"/> that will try to increase units.</param>
        /// <returns>True if the increase was valid, False if not.</returns>
        /// <remarks>
        ///     After increasing units, the containing <see cref="PlanogramMerchandisingGroup"/> is processed 
        ///     and checked for overfill or overlaps. Only increases that do not overfill 
        ///     or overlap will be considered valid; any other will be cancelled.
        /// </remarks>
        private static Boolean TryIncreaseUnits(TaskContext context, PlanogramPositionPlacement placement)
        {
            using (new CodePerformanceMetric()) 
            {
                //  Instantiate a new Merchandising Group to manipulate the position.
                IEnumerable<PlanogramSubComponentPlacement> subComponentPlacements = placement.SubComponentPlacement.GetCombinedWithList();
                PlanogramMerchandisingGroup merchandisingGroup = placement.MerchandisingGroup;

                context.TargetUnitsByGtin[placement.Product.Gtin]++;

                if (context.TargetUnitsByGtin[placement.Product.Gtin] <= placement.Position.TotalUnits)
                {
                    return true;
                }

                //  Check whether the placement can increase more units.
                if (!placement.IncreaseUnits(AssortmentRuleEnforcer)) return false;
                placement.RecalculateUnits();

                merchandisingGroup.Process();

                //  Check wether the change invalidated the planogram's positions.
                if (merchandisingGroup.IsOverfilled() || merchandisingGroup.HasOverlapping())
                {
                    placement.DecreaseUnits(AssortmentRuleEnforcer);
                    placement.RecalculateUnits();
                    return false;
                }

                //  Accept the changes.
                merchandisingGroup.ApplyEdit();

                // return success
                return true;
            }
        }

        /// <summary>
        /// Ensure that the planogram is remerchandised
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // remerchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        #endregion
    }
}