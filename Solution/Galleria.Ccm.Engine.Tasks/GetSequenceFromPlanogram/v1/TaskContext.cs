﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29597 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.GetSequenceFromPlanogram.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private String _sourcePlanogramName;
        private Int32 _sourcePlanogramId;
        private Planogram _sourcePlanogram;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the source planogram name
        /// </summary>
        public String SourcePlanogramName
        {
            get { return _sourcePlanogramName; }
            set { _sourcePlanogramName = value; }
        }

        /// <summary>
        /// Gets or sets the source planogram id
        /// </summary>
        public Int32 SourcePlanogramId
        {
            get { return _sourcePlanogramId; }
            set { _sourcePlanogramId = value; }
        }

        /// <summary>
        /// Gets or sets the renumbering strategy
        /// </summary>
        public Planogram SourcePlanogram
        {
            get { return _sourcePlanogram; }
            set { _sourcePlanogram = value; }
        }
        #endregion
    }
}
