﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802
// V8-29072 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.GetSequenceFromPlanogram.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PlanogramName = 0,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetSequenceFromPlanogram_Task_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetSequenceFromPlanogram_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // planogram name
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramName,
                Message.GetSequenceFromPlanogram_Parameter_PlanogramName_Name,
                Message.GetSequenceFromPlanogram_Parameter_PlanogramName_Description,
                Message.GetBlockingByName_Category,
                TaskParameterType.PlanogramSequenceNameSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when a task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetSourcePlanogram(taskContext)) return;
                if (!LoadSequence(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameters for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region Planogram Name

                // validate that the planogram name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.PlanogramName)) ||
                    (this.Parameters[Parameter.PlanogramName].Value == null) ||
                    (this.Parameters[Parameter.PlanogramName].Value.Value1 == null) ||
                    (this.Parameters[Parameter.PlanogramName].Value.Value2 == null))
                {
                    context.LogError(EventLogEvent.NothingForTaskToDo, Message.GetSequenceFromPlanogram_SourcePlanogramDidNotContainSequence);
                    success = false;
                }
                else
                {
                    // get the assortment name
                    context.SourcePlanogramName = Convert.ToString(this.Parameters[Parameter.PlanogramName].Value.Value1);
                    context.SourcePlanogramId = Convert.ToInt32(this.Parameters[Parameter.PlanogramName].Value.Value2);
                    if ((String.IsNullOrEmpty(context.SourcePlanogramName)) || (String.IsNullOrWhiteSpace(context.SourcePlanogramName)))
                    {
                        context.LogError(EventLogEvent.NothingForTaskToDo, Message.GetSequenceFromPlanogram_SourcePlanogramDidNotContainSequence);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Loads the source planogram from the database
        /// </summary>
        private static Boolean GetSourcePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to fetch the source planogram by its name
                try
                {
                    context.SourcePlanogram = Package.FetchById(context.SourcePlanogramId).Planograms[0];
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetSequenceFromPlanogram_Parameter_PlanogramName_Name, context.SourcePlanogramName);
                        return false;
                    }
                    throw ex;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Loads the sequence into the planogram
        /// </summary>
        private static Boolean LoadSequence(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // validate that the source planogram has a sequence
                if (context.SourcePlanogram.Sequence == null)
                {
                    context.LogError(EventLogEvent.NothingForTaskToDo, Message.GetSequenceFromPlanogram_SourcePlanogramDidNotContainSequence);
                    return true;
                }

                // copy the sequence into the destination planogram
                context.Planogram.Sequence.LoadFrom(context.SourcePlanogram.Sequence);

                // log success
                context.LogInformation(EventLogEvent.PlanogramSequenceReplaced, context.SourcePlanogramName);

                // return success
                return true;
            }
        }

        #endregion
    }
}
