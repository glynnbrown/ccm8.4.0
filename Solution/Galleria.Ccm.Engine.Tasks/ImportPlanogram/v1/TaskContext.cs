﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29597 : N.Foster
//  Created
#endregion

#region Version History: CCM820
// V8-30857 : L.Luong
//  Added Create Assortment
// V8-31005 : A.Probyn
//  Removed code to CreateAssortment and moved it to its own task.
#endregion
#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.ImportPlanogram.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private String _sourceFile; // holds the source file name
        private String _planogramImportTemplateName; // holds the planogram import template name
        private PlanogramImportTemplate _planogramImportTemplate; // holds the planogram import template
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the source file information
        /// </summary>
        public String SourceFile
        {
            get { return _sourceFile; }
            set { _sourceFile = value; }
        }

        /// <summary>
        /// Gets or sets the planogram import template name
        /// </summary>
        public String PlanogramImportTemplateName
        {
            get { return _planogramImportTemplateName; }
            set { _planogramImportTemplateName = value; }
        }

        /// <summary>
        /// Gets or sets the planogram import template
        /// </summary>
        public PlanogramImportTemplate PlanogramImportTemplate
        {
            get { return _planogramImportTemplate; }
            set { _planogramImportTemplate = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            //Dispose of anything necessary...
            this.SourceFile = null;
            this.PlanogramImportTemplateName = null;
            this.PlanogramImportTemplate = null;
        }

        #endregion
    }
}
