﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-24779 : D.Pleasance
//  Created
// V8-28558 : D.Pleasance
//  Amended so that CCM re-lays out the plan
#endregion
#region Version History: CCM801
// V8-28622 : D.Pleasance
//  Amended to pass through metric mappings
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM803
// V8-29483 : D.Pleasance
//  Amended to allow null planogram import template, not required for pog files
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Code review
// V8-29992 : D.Pleasance
//  Added PlanogramInvalidFileAccess to provide more description if the source file cannot be accessed.
// V8-29533 : D.Pleasance
//  Added logic to capture PlanogramDuplicateProductGtin exceptions.
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
#endregion
#region Version History: CCM820
// V8-30857 : L.Luong
//  Added CreateAssortment parameter
// V8-31005 : A.Probyn
//  Removed code to CreateAssortment and moved it to its own task.
// V8-31092 : M.Brumby
//  Added parent Id for Filter templates by file type
// V8-31023 : A.Silva
//  Changed LoadPlanogram to account for ProSpace multi plan files.
// V8-31069 : M.Brumby
//  Apollo multi bay import
// V8-31073 : A.Silva
//  GetPlanogramImportTemplate now obtains the default version for import file types from the type helper.
// V8-31067 : A.Probyn
//  Added extra defensive code so that the task logs when the source file is locked. 
//  Also logs when the source file is loaded, but no planograms can be loaded into the package (index exception was being thrown).
// V8-31152 : M.Brumby
//  External file import uses a context now
// V8-31175 : A.Silva
//  Amended the creation of the package ID when calling LoadPlanogram so that it includes the specific planogram name for Space Planning files.

#endregion
#region Version History : CCM830
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
#endregion
#endregion

using System;
using System.Linq;
using System.IO;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.External;
using System.Text;

namespace Galleria.Ccm.Engine.Tasks.ImportPlanogram.v1
{
    /// <summary>
    /// Imports a source file as a Planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        private const String ProSpaceFileExtension = ".PSA";

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            SourceFile = 0,
            PlanogramImportTemplateName = 1,
            CreateAssortment = 2
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.ImportPlanogram_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.ImportPlanogram_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_RepositoryActions; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // source file
            this.RegisterParameter(new TaskParameter(
                Parameter.SourceFile,
                Message.ImportPlanogram_Parameter_SourceFile_Name,
                Message.ImportPlanogram_Parameter_SourceFile_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.FileNameAndPath,
                null,
                false,
                false));

            // planogram import template name
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramImportTemplateName,
                Message.ImportPlanogram_Parameter_PlanogramImportTemplateName_Name,
                Message.ImportPlanogram_Parameter_PlanogramImportTemplateName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.PlanogramImportTemplateNameSingle,
                null,
                false,
                false,
                (Int32)Parameter.SourceFile));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetPlanogramImportTemplate(taskContext)) return;
                if (!LoadPlanogram(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region Source File

                // validate that the car park shelf name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.SourceFile)) ||
                    (this.Parameters[Parameter.SourceFile].Value == null) ||
                    (this.Parameters[Parameter.SourceFile].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ImportPlanogram_Parameter_SourceFile_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.SourceFile = Convert.ToString(this.Parameters[Parameter.SourceFile].Value.Value1);
                    if ((String.IsNullOrEmpty(context.SourceFile)) || (String.IsNullOrWhiteSpace(context.SourceFile)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ImportPlanogram_Parameter_SourceFile_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region Planogram Import Template Name

                // validate that the planogram import template name exists
                if ((this.Parameters.Contains((Int32)Parameter.PlanogramImportTemplateName)) &&
                    (this.Parameters[Parameter.PlanogramImportTemplateName].Value != null) &&
                    (this.Parameters[Parameter.PlanogramImportTemplateName].Value.Value1 != null))
                {
                    // get the planogram import template name
                    context.PlanogramImportTemplateName = Convert.ToString(this.Parameters[Parameter.PlanogramImportTemplateName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.PlanogramImportTemplateName)) || (String.IsNullOrWhiteSpace(context.PlanogramImportTemplateName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ImportPlanogram_Parameter_PlanogramImportTemplateName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Attempts to load the planogram import template
        /// </summary>
        private static Boolean GetPlanogramImportTemplate(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to fetch the planogram import template
                if (context.PlanogramImportTemplateName != null)
                {
                    try
                    {
                        context.PlanogramImportTemplate = PlanogramImportTemplate.FetchByEntityIdName(context.EntityId, context.PlanogramImportTemplateName);
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetBaseException() is DtoDoesNotExistException)
                        {
                            context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ImportPlanogram_Parameter_SourceFile_Name, context.PlanogramImportTemplateName);
                            return false;
                        }
                    }
                }

                // if no import template has been specified
                // then we may need to create a default one
                // based on the file extension type
                if (context.PlanogramImportTemplate == null)
                {
                    switch (System.IO.Path.GetExtension(context.SourceFile).ToUpperInvariant())
                    {
                        case ".PLN":
                            context.PlanogramImportTemplate = PlanogramImportTemplate.NewPlanogramImportTemplate(context.EntityId, PlanogramImportFileType.SpacemanV9);
                            break;
                        case ".XMZ":
                            context.PlanogramImportTemplate = PlanogramImportTemplate.NewPlanogramImportTemplate(context.EntityId, PlanogramImportFileType.Apollo);
                            break;
                        case ProSpaceFileExtension:
                            context.PlanogramImportTemplate = PlanogramImportTemplate.NewPlanogramImportTemplate(context.EntityId, PlanogramImportFileType.ProSpace);
                            break;
                    }
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Loads the planogram from file
        /// </summary>
        private static Boolean LoadPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify that the source file exists
                if (!System.IO.File.Exists(context.SourceFile))
                {
                    context.LogError(EventLogEvent.PlanogramInvalidFileAccess, context.SourceFile);
                    return false;
                }

                // attempt to load the planogram from the file
                Package package = null;
                //  Add the specific Planogram Name to the package ID.
                String packageId = String.Format("{0}::{1}", context.SourceFile, context.WorkpackagePlanogram.Name);

                String SourcePath = context.SourceFile;

                try
                {
                    if (context.PlanogramImportTemplate == null)
                    {
                        package = Package.FetchByFileName(packageId);
                       
                    }
                    else
                    {
                        var importContext = new ImportContext(
                            Entity.FetchById(context.EntityId).SystemSettings,
                            context.PlanogramImportTemplate.GetPlanogramFieldMappings().Select(m => m.GetDataTransferObject()),
                            context.PlanogramImportTemplate.GetPlanogramMetricMappings().Select(m => m.GetDataTransferObject()));

                        package = Package.FetchByFileName(packageId, importContext);
                        RestructurePlanogramSettings rSettings = context.PlanogramImportTemplate.GetRestructurePlanogramSettings();
                        foreach (Planogram plan in package.Planograms)
                        {
                            if (rSettings.Enabled)
                            {
                                rSettings.Restructure(plan);
                            }
                            else if (plan.MetaBayCount > 1 && context.PlanogramImportTemplate.FileType == PlanogramImportFileType.Apollo)
                            {
                                PlanogramRestructuringHelper.RestructureBaysByCount(plan, plan.MetaBayCount.Value);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception rootEx = ex.GetBaseException();

                    if (rootEx is PackageDuplicateProductGtinException)
                    {
                        Package.TryUnlockPackageById(PackageType.FileSystem, context.SourceFile, DomainPrincipal.CurrentUserId, PackageLockType.User);
                        context.LogError(EventLogEvent.PlanogramDuplicateProductGtin, context.SourceFile, rootEx.Message);
                    }
                    else if (rootEx is PackageLockException)
                    {
                        Package.TryUnlockPackageById(PackageType.FileSystem, context.SourceFile, DomainPrincipal.CurrentUserId, PackageLockType.User);
                        context.LogError(EventLogEvent.SourceFileLocked, context.SourceFile);
                    }
                    else
                    {
                        Package.TryUnlockPackageById(PackageType.FileSystem, context.SourceFile, DomainPrincipal.CurrentUserId, PackageLockType.User);
                        throw ex;
                    }
                }

                if (package != null)
                {
                    // ensure the package is fully loaded
                    package.LoadChildren();

                    //V831067 Ensure planograms could be loaded
                    if (package.Planograms.Count > 0)
                    {
                        // set the context planogram to the new planogram
                        context.Planogram = package.Planograms[0];

                        // set the source path
                        context.Planogram.SourcePath = SourcePath;
                    }
                    else
                    {
                        //Log error that no planogram could be loaded from the file
                        context.LogWarning(EventLogEvent.PlanogramCouldNotBeLoadedFromSourceFile, context.SourceFile);
                        return false;
                    }
                }   

                // update the planogram username
                context.Planogram.UserName = context.WorkpackagePlanogram.UserName;

                // ensure the planogram has a name specicified
                if (String.IsNullOrEmpty(context.Planogram.Name))
                {
                    context.Planogram.Name = context.WorkpackagePlanogram.Name;
                }

                // return success
                return true;                
            }
        }

        /// <summary>
        /// Re-merchandises the planogram
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // re-merchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        #endregion
    }
}