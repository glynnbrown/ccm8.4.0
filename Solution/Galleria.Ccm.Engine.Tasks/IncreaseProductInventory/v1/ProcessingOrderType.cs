﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30761 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1
{
    /// <summary>
    /// Denotes the order in which products will be processed when increasing units.
    /// </summary>
    public enum ProcessingOrderType
    {
        /// <summary>
        /// Indicates that products will be processed in ascending rank order.
        /// </summary>
        ByRank = 0,
        /// <summary>
        /// Indicates that products will be processed in ascending achieved days of supply order.
        /// </summary>
        ByAchievedDos = 1,
        /// <summary>
        /// Indicates that only the products with the lowest DOS will be processed.
        /// </summary>
        ByLowestDos = 2,
    }

    public static class ProcessingOrderTypeHelper
    {
        public static Dictionary<ProcessingOrderType, String> FriendlyNames = new Dictionary<ProcessingOrderType, String>()
        {
            { ProcessingOrderType.ByRank, Message.IncreaseProductInventory_ProcessingOrderType_ByRank }, 
            { ProcessingOrderType.ByAchievedDos, Message.IncreaseProductInventory_ProcessingOrderType_ByAchievedDaysOfSupply },
            { ProcessingOrderType.ByLowestDos, Message.IncreaseProductInventory_ProcessingOrderType_ByLowestDos },
        };

        public static PlanogramMerchandiserProcessingOrderType GetPlanogramMerchandiserProcessingOrder(
                this ProcessingOrderType processingOrder)
        {
            switch (processingOrder)
            {
                case ProcessingOrderType.ByRank:
                    return PlanogramMerchandiserProcessingOrderType.ByRank;
                case ProcessingOrderType.ByAchievedDos:
                    return PlanogramMerchandiserProcessingOrderType.ByAchievedDos;
                case ProcessingOrderType.ByLowestDos:
                    return PlanogramMerchandiserProcessingOrderType.ByLowestDos;
                default:
                    System.Diagnostics.Debug.Fail($"Unrecoginised processing order: {processingOrder}");
                    return PlanogramMerchandiserProcessingOrderType.ByRank;
            }
        }
    }
}
