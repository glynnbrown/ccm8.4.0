﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30761 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-31806 : D.Pleasance
//  Amended NumberOfRankedProducts to be % of ranked products.
// V8-32315 : D.Pleasance
//  Added TopRakedProductPercentageGtins
// V8-32358 : D.Pleasance
//  Added StartingUnits \ FinalUnits
// V8-32617 : D.Pleasance
//  Added AverageAchievedDos \ RecalculateAverageAchievedDos.
// V8-32800 : D.Pleasance
//  Added TargetUnitsByGtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1
{
    public class TaskContext : Ccm.Engine.TaskContext, IPlanogramMerchandiserContext
    {
        #region Fields

        /// <summary>
        /// The Planogram's merchandising groups.
        /// </summary>
        private PlanogramMerchandisingGroupList _merchandisingGroups;

        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            ProductGtinsIncreased = new List<String>();
            PositionsToIgnore = new List<String>();
            _merchandisingGroups = base.Planogram.GetMerchandisingGroups();
            TopRakedProductPercentageGtins = new List<String>();
            StartingUnits = new Dictionary<String, Int32>();
            FinalUnits = new Dictionary<String, Int32>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// A dictionary of the Planogram Assortment ranks, keyed by Product Gtin.
        /// </summary>
        public Dictionary<String, Int32> RanksByProductGtin { get; set; }

        /// <summary>
        /// A dictionary of the Planogram's Performance Data, keyed by Product Id.
        /// </summary>
        public Dictionary<Object, PlanogramPerformanceData> PerformanceDataByProductId { get; set; }

        /// <summary>
        /// The order to select products in.
        /// </summary>
        public ProcessingOrderType ProcessingOrder { get; set; }

        /// <summary>
        /// The method to use when selecting products.
        /// </summary>
        public ProductSelectionType ProductSelection { get; set; }

        /// <summary>
        /// The percentage of ranked products to select, if specified by the user.
        /// </summary>
        public Single? PercentageOfRankedProducts { get; set; }

        /// <summary>
        /// The product gtins that had their units increased by the task.
        /// </summary>
        public List<String> ProductGtinsIncreased{ get; set; }

        /// <summary>
        /// Indicates if an increase to position units was possible.
        /// </summary>
        public Boolean UnitsWereIncreased { get; set; }

        /// <summary>
        /// The position placements for which no more increases are possible and that should
        /// be ignored in future increases.
        /// </summary>
        public List<String> PositionsToIgnore{ get; set; }

        /// <summary>
        /// The top ranked product gtins to reprocess
        /// </summary>
        public List<String> TopRakedProductPercentageGtins { get; set; }

        /// <summary>
        /// The Planogram's merchandising groups.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups
        {
            get { return _merchandisingGroups; }
        }

        public Dictionary<String, Int32> StartingUnits { get; private set; }
        public Dictionary<String, Int32> FinalUnits { get; private set; }

        /// <summary>
        /// gets \ sets the current achieved dos average
        /// </summary>
        public Single AverageAchievedDos { get; set; }

        /// <summary>
        /// Determines if average achieved dos needs to be recalculated after a successful increase
        /// </summary>
        public Boolean RecalculateAverageAchievedDos { get; set; }

        /// <summary>
        /// A dictionary of target units by gtin
        /// </summary>
        public Dictionary<String, Int32> TargetUnitsByGtin { get; set; }

        /// <summary>
        /// Indicates whether the task should increase inventory by facings or by units.
        /// </summary>
        public InventoryChangeType InventoryChange { get; set; }

        #region IPlanogramMerchandiserContext implementation
        PlanogramBlocking IPlanogramMerchandiserContext.Blocking
        {
            get
            {
                return null;
            }
        }

        Dictionary<Int32, Stack<PlanogramProduct>> IPlanogramMerchandiserContext.ProductStacksBySequenceColour
        {
            get
            {
                return new Dictionary<int, Stack<PlanogramProduct>>();
            }
        }

        Dictionary<Int32, Dictionary<String, Int32>> IPlanogramMerchandiserContext.TargetUnitsBySequenceColour
        {
            get
            {
                return new Dictionary<int, Dictionary<string, int>>();
            }
        }

        Single IPlanogramMerchandiserContext.BlockingWidth
        {
            get
            {
                return 0;
            }
        }

        Single IPlanogramMerchandiserContext.BlockingHeight
        {
            get
            {
                return 0;
            }
        }

        Single IPlanogramMerchandiserContext.BlockingHeightOffset
        {
            get
            {
                return 0;
            }
        }

        PlanogramMerchandisingSpaceConstraintType IPlanogramMerchandiserContext.SpaceConstraint
        {
            get
            {
                return PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace;
            }
        }

        Dictionary<Int32, PlanogramBlockingGroup> IPlanogramMerchandiserContext.BlockingGroupsByColour
        {
            get
            {
                return new Dictionary<int, PlanogramBlockingGroup>();
            }
        }

        PlanogramMerchandisingInventoryChangeType IPlanogramMerchandiserContext.InventoryChangeType
        {
            get { return InventoryChange.GetPlanogramMerchandisingInventoryChange(); }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Disposes of this instance, disposing of the internal merchandising groups.
        /// </summary>
        protected override void OnDipose()
        {
            base.OnDipose();
            if (_merchandisingGroups != null) _merchandisingGroups.Dispose();
        }

        void IPlanogramMerchandiserContext.LogStartingUnits(PlanogramPositionPlacement position)
        {
        }

        void IPlanogramMerchandiserContext.LogFinalUnits(PlanogramPositionPlacement position)
        {
        }

        void IPlanogramMerchandiserContext.LogFinalUnits(String gtin, Int32 units)
        {
        }

        #endregion

    }
}
