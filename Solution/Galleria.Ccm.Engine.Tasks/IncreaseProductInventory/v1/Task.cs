﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30761 : A.Kuszyk
//  Created.
// V8-30990 : D.Pleasance
//  Amended default task parameter, NumberOfRankedProducts 100
// V8-31270 : D.Pleasance
//  Amended EnumerateProductPositionsWithMinUnits to exclude positions to ignore
// V8-31316 : A.Kuszyk
//  Updated average dos calculation to only take into account placed products.
// V8-31615 : A.Kuszyk
//  Added a call to PlanogramPositionPlacement.RecalculateUnits() following inventory changes.
#endregion
#region Version History: CCM830
// V8-31806 : D.Pleasance
//  Amended NumberOfRankedProducts to be % of ranked products.
// V8-32315 : D.Pleasance
//  Amended so that the percentage of ranked products doesn’t change product selection after each iteration. 
//  The product selection should be the original product GTINs that the process started with. These are the only products that should be increased.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32358 : D.Pleasance
//  Added further logging for starting \ final inventory units.
// V8-32617 : D.Pleasance
//  Amended ProductSelectionType.ProductsBelowAverageDos, so that all positions are increased to the initial achieved dos average before 
//  recaulating achieved dos average to iterate through next selection of positions.
// V8-32800 : D.Pleasance
//  Changes applied to increase inventory units fairly between positions that are targeted to increase. 
//  To achieve this we keep a running total of the target units whilst checking this against the actual placed units.
// V8-32787 : A.Silva
//  Amended TryIncreaseUnitsForPosition so that assortment rules are enforced.
//  Added an AssortmentRuleEnforcer field to allow easy enabling of rule enforcing.
// CCM-18435 : A.Silva
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1
{
    /// <summary>
    /// Increases the units of products in the plan, in accordance with the selection and ordering parameter values,
    /// until the plan is full and no space is left in increase units into.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Fields
        private static readonly AssortmentRuleEnforcer AssortmentRuleEnforcer = new AssortmentRuleEnforcer();
        private PlanogramMerchandiser<TaskContext> _planogramMerchandiser;
        #endregion

        #region Version

        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 1; // holds the tasks minor version number

        #endregion

        #region Parameter
        private enum Parameter
        {
            /// <summary>
            /// The order in which products should be processed.
            /// </summary>
            ProcessingOrder = 0,
            /// <summary>
            /// The method to be used when selecting products.
            /// </summary>
            ProductSelection = 1,
            /// <summary>
            /// The percentage of ranked products to use in conjunction with the TopRankedProducts selection option.
            /// </summary>
            PercentageOfRankedProducts = 2,
            /// <summary>
            /// Indicates whether facings or units should be changed when making inventory increases.
            /// </summary>
            InventoryChange = 3,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.IncreaseProductInventory_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.IncreaseProductInventory_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.ProcessingOrder,
                Message.IncreaseProductInventory_ProcessingOrder_Name, 
                Message.IncreaseProductInventory_ProcessingOrder_Description,
                Message.ParameterCategory_Default,
                typeof(ProcessingOrderType),
                ProcessingOrderType.ByRank,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.ProductSelection,
                Message.IncreaseProductInventory_ProductSelection_Name, 
                Message.IncreaseProductInventory_ProductSelection_Description,
                Message.ParameterCategory_Default,
                typeof(ProductSelectionType),
                ProductSelectionType.AllProducts,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                Parameter.PercentageOfRankedProducts,
                Message.IncreaseProductInventory_PercentageOfRankedProducts_Name, 
                Message.IncreaseProductInventory_PercentageOfRankedProducts_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Percentage,
                1,
                false,
                false,
                (Int32)Parameter.ProductSelection,
                () => IsPercentageOfRankedProductsRequired(),
                null,
                () => IsPercentageOfRankedProductsValid()));

            this.RegisterParameter(new TaskParameter(
                Parameter.InventoryChange,
                Message.IncreaseProductInventory_InventoryChange_Name,
                Message.IncreaseProductInventory_InventoryChange_Description,
                Message.ParameterCategory_Default,
                typeof(InventoryChangeType),
                InventoryChangeType.ByUnits,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                _planogramMerchandiser = new PlanogramMerchandiser<TaskContext>(taskContext);
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!BuildDictionaries(taskContext)) return;
                if (!IncreaseInventory(taskContext)) return;
                if (!LogCompletionAndInventoryChanges(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Validates that the task can proceed with execution given the plan's state.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean ValidatePlanogram(TaskContext context)
        {
            if (context.ProcessingOrder == ProcessingOrderType.ByLowestDos)
            {
                IEnumerable<PlanogramPerformanceData> performanceData = context.Planogram.Positions
                    .Select(p => p.GetPlanogramPerformanceData()).Where(p => p != null);
                if (performanceData.All(p => p.AchievedDos.HasValue && p.AchievedDos.Value.EqualTo(0)))
                {
                    context.LogWarning(EventLogEvent.NothingForTaskToDo, Message.IncreaseProductInventory_ByLowestDos_NothingToDo);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Initializes the task context.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean BuildDictionaries(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                taskContext.RanksByProductGtin = taskContext.Planogram.Assortment.Products
                    .ToDictionary(ap => ap.Gtin, ap => (Int32)ap.Rank);
                taskContext.PerformanceDataByProductId = taskContext.Planogram.Performance.PerformanceData
	                .Where(p =>
	                {
	                    var prod = p.GetPlanogramProduct();
	                    return prod == null ? false : prod.GetPlanogramPositions().Any();
	                })
	                .ToDictionary(p => p.PlanogramProductId, p => p);

                #region Target Units

                taskContext.TargetUnitsByGtin = new Dictionary<String, Int32>();

                foreach (PlanogramMerchandisingGroup merchandisingGroup in taskContext.MerchandisingGroups)
                {
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        String productGtin = positionPlacement.Product.Gtin;
                        if (taskContext.TargetUnitsByGtin.ContainsKey(productGtin)) continue;
                        taskContext.TargetUnitsByGtin.Add(productGtin, positionPlacement.Position.TotalUnits);
                    }
                }

                #endregion

                return true;
            }
        }

        /// <summary>
        /// Validates the task parameters and stores them in <paramref name="taskContext"/>.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean ValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region ProcessingOrder
                if (Parameters.Count <= (Int32)Parameter.ProcessingOrder ||
                        Parameters[Parameter.ProcessingOrder].Value == null ||
                        Parameters[Parameter.ProcessingOrder].Value.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.IncreaseProductInventory_ProcessingOrder_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProcessingOrder = (ProcessingOrderType)Parameters[Parameter.ProcessingOrder].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.IncreaseProductInventory_ProcessingOrder_Name,
                            Parameters[Parameter.ProcessingOrder].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region ProductSelection
                if (Parameters.Count <= (Int32)Parameter.ProductSelection ||
                    Parameters[Parameter.ProductSelection].Value == null ||
                    Parameters[Parameter.ProductSelection].Value.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.IncreaseProductInventory_ProductSelection_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProductSelection = (ProductSelectionType)Parameters[Parameter.ProductSelection].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.IncreaseProductInventory_ProductSelection_Name,
                            Parameters[Parameter.ProductSelection].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region NumberOfRankedProducts
                if (Parameters.Count <= (Int32)Parameter.PercentageOfRankedProducts ||
                    Parameters[Parameter.PercentageOfRankedProducts].Value == null) // This value can be null
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.IncreaseProductInventory_PercentageOfRankedProducts_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.PercentageOfRankedProducts = Convert.ToSingle(Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                    }
                    catch (FormatException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.IncreaseProductInventory_PercentageOfRankedProducts_Name,
                            Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region InventoryChange
                if (Parameters.Count <= (Int32)Parameter.InventoryChange ||
                    Parameters[Parameter.InventoryChange].Value == null ||
                    Parameters[Parameter.InventoryChange].Value.Value1 == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.IncreaseProductInventory_InventoryChange_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.InventoryChange = (InventoryChangeType)Parameters[Parameter.InventoryChange].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.IncreaseProductInventory_InventoryChange_Name,
                            Parameters[Parameter.InventoryChange].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                return success;
            }
        }

        /// <summary>
        /// Enumerates through the product selection in order, increasing units until no further increases are possible.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean IncreaseInventory(TaskContext taskContext)
        {
            using (new CodePerformanceMetric())
            {
                taskContext.UnitsWereIncreased = true;

                if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos)
                {
                    CalculateAverageAchievedDos(taskContext);
                }

                //exclude products with a null or 0 for DOS
                if (taskContext.ProcessingOrder == ProcessingOrderType.ByLowestDos || taskContext.ProcessingOrder == ProcessingOrderType.ByAchievedDos)
                {
                    IEnumerable<PlanogramPerformanceData> performanceData = taskContext.Planogram.Positions
                        .Select(p => p.GetPlanogramPerformanceData()).Where(p => p != null);

                    foreach (var item in performanceData)
                    {
                        if (item.AchievedDos == null || item.AchievedDos.Value.EqualTo(0))
                        {
                            taskContext.PositionsToIgnore.Add(item.GetPlanogramProduct().Gtin);
                        }
                    }
                }

                // Loop whilst we're still able to increase inventory, or when we're processing lowest dos positions.
                // We'll always break out of this loop when there are no longer positions to be increased.
                while (taskContext.UnitsWereIncreased || taskContext.ProcessingOrder == ProcessingOrderType.ByLowestDos)
                {
                    // Reset units increased flag. This will only be set to true if we were able to increase the units
                    // of a position. If not, then the loop will terminate when no further increases were possible.
                    taskContext.UnitsWereIncreased = false;

                    // Iterate through the selected products in order and try increasing units.
                    IEnumerable<PlanogramPositionPlacement> positionsInOrder = _planogramMerchandiser
                        .EnumerateSelectedPositionPlacementsInOrder(
                            taskContext.ProductSelection.GetPlanogramMerchandiserProductSelection(),
                            taskContext.ProcessingOrder.GetPlanogramMerchandiserProcessingOrder(),
                            taskContext.AverageAchievedDos,
                            taskContext.PercentageOfRankedProducts);

                    // Ensure that if there are no products to increase we still break out of the loop.
                    // This handles the fact that that the while always loops with we're increasing the
                    // lowest dos positions.
                    if (!positionsInOrder.Any()) break;

                    foreach (PlanogramPositionPlacement position in positionsInOrder)
                    {
                        // Remember the top ranked products, these should not change after each round of inventory increase
                        if (taskContext.ProductSelection == ProductSelectionType.TopRankedProducts) 
                        {
                            if (!taskContext.TopRakedProductPercentageGtins.Contains(position.Product.Gtin))
                            {
                                taskContext.TopRakedProductPercentageGtins.Add(position.Product.Gtin);
                            }
                        }

                        taskContext.UnitsWereIncreased = TryIncreaseUnitsForPosition(taskContext, position) || taskContext.UnitsWereIncreased;
                    }

                    if (taskContext.ProductSelection == ProductSelectionType.ProductsBelowAverageDos)
                    {
                        ValidateProductsBelowAverageDosIncrease(taskContext);
                    }
                }
                
                return true;
            }
        }

        /// <summary>
        /// validates ProductSelectionType.ProductsBelowAverageDos increase iteration being successful. 
        /// Success - some products were increased \ have been increased previously
        /// Failure - No products were increased and Average Achieved Dos does not require recalculating
        /// </summary>
        /// <param name="taskContext"></param>
        private static void ValidateProductsBelowAverageDosIncrease(TaskContext taskContext)
        {
            if(taskContext.ProcessingOrder == ProcessingOrderType.ByLowestDos)
            {
                CalculateAverageAchievedDos(taskContext);
                return;
            }

            if (taskContext.UnitsWereIncreased)
            {
                // we can recalculate DOS when all positions have achieved the initial average
                taskContext.RecalculateAverageAchievedDos = true;
            }
            else
            {
                if (taskContext.RecalculateAverageAchievedDos)
                {
                    // some products were increased to average but now nothing has been increased with the current selections that were below DOS, 
                    // need to recalculate to see if anymore positions that now meet the new plan average can be faced up.
                    CalculateAverageAchievedDos(taskContext);
                    taskContext.UnitsWereIncreased = true;
                }
                taskContext.RecalculateAverageAchievedDos = false;
            }
        }

        /// <summary>
        /// Calculates the current average achieved Dos
        /// </summary>
        /// <param name="taskContext"></param>
        private static void CalculateAverageAchievedDos(TaskContext taskContext)
        {
            if (!taskContext.PerformanceDataByProductId.Values.Any(p => p.AchievedDos.HasValue))
            {
                return;
            }

            taskContext.AverageAchievedDos = taskContext.PerformanceDataByProductId.Values
                .Where(p => p.AchievedDos.HasValue)
                .Average(p => p.AchievedDos.Value);
        }

        /// <summary>
        /// Increases the units for the <paramref name="position"/>, returning true if successful and false if not.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        private static Boolean TryIncreaseUnitsForPosition(TaskContext taskContext, PlanogramPositionPlacement position)
        {
            using (new CodePerformanceMetric()) 
            {
                //  If there are no starting units for this product,
                //  register the current units, this must be the first time
                //  that increase inventory is attempted on this product.
                RecordStartingUnits(taskContext, position);

                taskContext.TargetUnitsByGtin[position.Product.Gtin]++;
                if (taskContext.TargetUnitsByGtin[position.Product.Gtin] <= position.Position.TotalUnits)
                {
                    return true;
                }
                
                // Try to increase position units.
                if (PerformInventoryIncrease(taskContext, position))
                {
                    position.RecalculateUnits();

                    // If successful, process and then check for overfills and overlaps.
                    position.MerchandisingGroup.Process();
                    if (position.MerchandisingGroup.IsOverfilled() ||
                        position.MerchandisingGroup.HasOverlapping())
                    {
                        // If there was an overlap, decrease the units and ignore this position in future.
                        PerformInventoryDecrease(taskContext, position);
                        position.RecalculateUnits();

                        if (!taskContext.PositionsToIgnore.Contains(position.Product.Gtin))
                        {
                            taskContext.PositionsToIgnore.Add(position.Product.Gtin);
                        }
                        return false;
                    }
                    else
                    {
                        // If the increase was successful and there were no overlaps, apply the edit to the merch group.
                        position.MerchandisingGroup.ApplyEdit();
                        taskContext.FinalUnits[position.Product.Gtin] = position.Position.TotalUnits;
                        if (!taskContext.ProductGtinsIncreased.Contains(position.Product.Gtin))
                        {
                            taskContext.ProductGtinsIncreased.Add(position.Product.Gtin);
                        }

                        // If we're increasing inventory by facings, then we want to bring the target units
                        // dictionary up-to-date with the number of units we now have.
                        if(taskContext.InventoryChange == InventoryChangeType.ByFacings)
                        {
                            taskContext.TargetUnitsByGtin[position.Product.Gtin] = position.Position.TotalUnits;
                        }

                        return true;
                    }
                }
                else
                {
                    // If no units could be increase, ignore this position in the future.
                    if (!taskContext.PositionsToIgnore.Contains(position.Product.Gtin))
                    {
                        taskContext.PositionsToIgnore.Add(position.Product.Gtin);
                    }
                    return false;
                }
            }
        }

        private static Boolean PerformInventoryIncrease(TaskContext context, PlanogramPositionPlacement position)
        {
            if(context.InventoryChange == InventoryChangeType.ByUnits)
            {
                return position.IncreaseUnits(AssortmentRuleEnforcer);
            }
            else
            {
                return position.IncreaseFacings(AssortmentRuleEnforcer);
            }
        }

        private static Boolean PerformInventoryDecrease(TaskContext context, PlanogramPositionPlacement position)
        {
            if (context.InventoryChange == InventoryChangeType.ByUnits)
            {
                return position.DecreaseUnits(AssortmentRuleEnforcer);
            }
            else
            {
                return position.DecreaseFacings(AssortmentRuleEnforcer);
            }
        }

        /// <summary>
        /// Records the starting units of the current assortment product in the task context.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static void RecordStartingUnits(TaskContext context, PlanogramPositionPlacement placement)
        {
            if (!context.StartingUnits.ContainsKey(placement.Product.Gtin))
            {
                context.StartingUnits.Add(placement.Product.Gtin, placement.Position.TotalUnits);
                context.FinalUnits.Add(placement.Product.Gtin, placement.Position.TotalUnits);
            }
        }

        private static Boolean LogCompletionAndInventoryChanges(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Dictionary<String, String> productNameLookUpByGtin = context.Planogram.Products.ToDictionary(p => p.Gtin, p => p.Name);
                StringBuilder inventoryChanges = new StringBuilder();

                // Log the changes
                foreach (String productGtin in context.StartingUnits.Keys)
                {
                    //  Get the starting units,
                    Int32 startingUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.StartingUnits.TryGetValue(productGtin, out startingUnits)) continue;

                    //  Get the final units,
                    Int32 finalUnits;
                    //  If there are none, the product's inventory has not changed.
                    if (!context.FinalUnits.TryGetValue(productGtin, out finalUnits)) continue;

                    //  The increase had no effect.
                    if (startingUnits == finalUnits) continue;

                    inventoryChanges.AppendFormat(Message.TaskLogging_PlanogramProductInventoryIncreased,
                                        productGtin, productNameLookUpByGtin[productGtin], startingUnits, finalUnits);

                }

                //  Log the change.
                context.LogInformation(
                    EventLogEvent.PlanogramProductInventoryIncreased,
                    context.ProductGtinsIncreased.Count,
                    inventoryChanges);

                // return success
                return true;
            }
        }
        
        /// <summary>
        /// Ensure that the planogram is remerchandised
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // remerchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        private String IsPercentageOfRankedProductsRequired()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (Parameters.Count <= (Int32)Parameter.ProductSelection ||
                    this.Parameters[Parameter.ProductSelection].Value == null ||
                    this.Parameters[Parameter.ProductSelection].Value.Value1 == null)
                {
                    return Message.IncreaseProductInventory_ProductSelectionParameterDidNotExist;
                }

                ProductSelectionType productSelection;
                try
                {
                    productSelection = (ProductSelectionType)this.Parameters[Parameter.ProductSelection].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    return Message.IncreaseProductInventory_ProcessingOrderParameterDidNotExist;
                }

                if (productSelection == ProductSelectionType.TopRankedProducts)
                {
                    return null;
                }

                return Message.IncreaseProductInventory_PercentageOfRankedProductsNotRequired;
            }
        }

        private String IsPercentageOfRankedProductsValid()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!String.IsNullOrEmpty(IsPercentageOfRankedProductsRequired())) return null;

                if (Parameters.Count <= (Int32)Parameter.PercentageOfRankedProducts ||
                    this.Parameters[Parameter.PercentageOfRankedProducts].Value == null ||
                    this.Parameters[Parameter.PercentageOfRankedProducts].Value.Value1 == null)
                {
                    return Message.IncreaseProductInventory_PercentageOfRankedProductsParameterDidNotExist;
                }

                Single percentageOfRankedProducts;
                try
                {
                    percentageOfRankedProducts = Convert.ToSingle(this.Parameters[Parameter.PercentageOfRankedProducts].Value.Value1);
                }
                catch (FormatException)
                {
                    return Message.IncreaseProductInventory_ValueWasNotAValidInteger;
                }

                if (percentageOfRankedProducts.GreaterThan(0))
                {
                    return null;
                }

                return Message.IncreaseProductInventory_PercentageOfRankedProductsInvalid;
            }
        }

        #endregion
    }
}
