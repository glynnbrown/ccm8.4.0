﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30761 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1
{
    /// <summary>
    /// Denotes that products that will be selected of an increase of units.
    /// </summary>
    public enum ProductSelectionType
    {
        /// <summary>
        /// Indicates that products will be increased.
        /// </summary>
        AllProducts = 0,
        /// <summary>
        /// Indicates that a top number of products will be increased. This number is specified by the user.
        /// </summary>
        TopRankedProducts = 1,
        /// <summary>
        /// Indicates that product below or equal to the average days of supply value will be increased.
        /// </summary>
        ProductsBelowAverageDos = 2,
    }

    public static class ProductSelectionTypeHelper
    {
        public static Dictionary<ProductSelectionType, String> FriendlyNames = new Dictionary<ProductSelectionType, String>()
        {
            { ProductSelectionType.AllProducts, Message.IncreaseProductInventory_ProductSelectionType_AllProducts }, 
            { ProductSelectionType.TopRankedProducts, Message.IncreaseProductInventory_ProductSelectionType_TopRankedProducts},
            { ProductSelectionType.ProductsBelowAverageDos, Message.IncreaseProductInventory_ProductSelectionType_ProductsBelowAverageDaysOfSupply},
        };

        public static PlanogramMerchandiserProductSelectionType GetPlanogramMerchandiserProductSelection(
                this ProductSelectionType productSelection)
        {
            switch (productSelection)
            {
                case ProductSelectionType.AllProducts:
                    return PlanogramMerchandiserProductSelectionType.AllProducts;
                case ProductSelectionType.TopRankedProducts:
                    return PlanogramMerchandiserProductSelectionType.TopRankedProducts;
                case ProductSelectionType.ProductsBelowAverageDos:
                    return PlanogramMerchandiserProductSelectionType.ProductsBelowAverageDos;
                default:
                    System.Diagnostics.Debug.Fail($"Unrecognised product selection: {productSelection}");
                    return PlanogramMerchandiserProductSelectionType.AllProducts;
            }
        }
    }
}
