﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31005 : L.Luong
//  Created (Moved From Import Planogram Task to a new task)
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Engine.Tasks.CreateAssortment.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private CreateAssortmentType _createAssortment;
        private Int32 _productAdded;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets if the user wants to create an assortment
        /// </summary>
        public CreateAssortmentType CreateAssortment
        {
            get { return _createAssortment; }
            set { _createAssortment = value; }
        }

        /// <summary>
        /// Gets or sets the products added in the assortment
        /// </summary>
        public Int32 ProductAdded
        {
            get { return _productAdded; }
            set { _productAdded = value; }
        }
        #endregion
    }
}
