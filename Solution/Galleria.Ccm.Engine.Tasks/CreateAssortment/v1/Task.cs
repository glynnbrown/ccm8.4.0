﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31005 : L.Luong
//  Created (Moved From Import Planogram Task to a new task)
// V8-31005 : A.Probyn
//  Completed the task, refactored and added more logging.
// V8-31241 : D.Pleasance
//  Amended logging to show number of products added, number ranged, number not ranged.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.CreateAssortment.v1
{
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            CreateAssortment = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.CreateAssortment_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.CreateAssortment_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // create assortment?
            RegisterParameter(
                new TaskParameter(
                    Parameter.CreateAssortment,
                    Message.ImportPlanogram_Parameter_CreateAssortment_Name,
                    Message.ImportPlanogram_Parameter_CreateAssortment_Description,
                    Message.ParameterCategory_Default,
                    typeof(CreateAssortmentType),
                    CreateAssortmentType.NeverCreateAssortment,
                    false,
                    false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!AddAssortment(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region Create Assortment

                // validate that the CreateAssortmentType is valid
                if ((this.Parameters.Contains((Int32)Parameter.CreateAssortment)) &&
                    (this.Parameters[Parameter.CreateAssortment].Value != null) &&
                    (this.Parameters[Parameter.CreateAssortment].Value.Value1 != null))
                {
                    try
                    {
                        context.CreateAssortment = (CreateAssortmentType)this.Parameters[Parameter.CreateAssortment].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ImportPlanogram_Parameter_CreateAssortment_Name, Parameters[Parameter.CreateAssortment].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Add Assortment to the Planogram
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static Boolean AddAssortment(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (context.CreateAssortment != CreateAssortmentType.NeverCreateAssortment)
                {
                    if (context.CreateAssortment == CreateAssortmentType.AlwaysCreateAssortment
                        || (context.CreateAssortment == CreateAssortmentType.OnlyCreateIfAssortmentDoesNotExist
                        && (context.Planogram.Assortment == null || !context.Planogram.Assortment.Products.Any())))
                    {
                        //Create new assortment
                        PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
                        IEnumerable<PlanogramPosition> positions;
                        Int32 productsAdded = 0;
                        List<String> newProductGtins = new List<String>();
                        if (context.Planogram.Products.Count != 0)
                        {

                            foreach (PlanogramProduct product in context.Planogram.Products)
                            {
                                productsAdded++;
                                PlanogramAssortmentProduct assortmentProduct = assortment.Products.Add(product);
                                positions = product.GetPlanogramPositions();

                                if (positions.Any())
                                {
                                    //keep track of facings
                                    Byte facings = 0;

                                    // some imported plans dont have units calculated so calculate them here
                                    foreach (PlanogramPosition position in positions)
                                    {
                                        //Get placement once 
                                        PlanogramSubComponentPlacement placement = position.GetPlanogramSubComponentPlacement();
                                        
                                        //Calculate units/facings
                                        position.RecalculateUnits(product, placement);
                                        facings += (Byte)position.GetFacings(placement);
                                    }

                                    assortmentProduct.IsRanged = true;
                                    assortmentProduct.Facings = facings;
                                    assortmentProduct.Units = (Int16)positions.Select(a => a.TotalUnits).Sum();

                                    //Keep track of new product gtins
                                    if (!newProductGtins.Contains(product.Gtin))
                                    {
                                        newProductGtins.Add(product.Gtin);
                                    }
                                }
                            }
                        }

                        //Take copy of assortmen products before it gets replaced
                        List<String> existingAssortmentProductGtins = context.Planogram.Assortment.Products.Select(p => p.Gtin).Distinct().ToList();

                        //Get number of existing products updating
                        Int32 numberExistingProductsUpdated = existingAssortmentProductGtins.Intersect<String>(newProductGtins).Count();

                        //Replace existing assortment
                        context.ProductAdded = productsAdded;
                        context.Planogram.Assortment.ReplaceAssortment(assortment);

                        //If any existing products were updated, log how many
                        if (numberExistingProductsUpdated > 0)
                        {
                            context.LogInformation(
                                EventLogEvent.CreateAssortmentUpdatedProducts, numberExistingProductsUpdated);
                        }

                        return true;
                    }
                    else if (context.CreateAssortment == CreateAssortmentType.OnlyCreateIfAssortmentDoesNotExist
                        && (context.Planogram.Assortment != null || context.Planogram.Assortment.Products.Any()))
                    { 
                        //Log that an assortment has not been created because an assortment already existed and the paramter was set
                        //to not replace this.
                        context.LogInformation(
                                EventLogEvent.NothingForTaskToDo,
                                Message.CreateAssortment_OnlyCreateIfAssortmentDoesNotExistSelectedAndAssortmentExisted);
                        return false;
                    }
                }
                else
                {
                    context.LogInformation(
                        EventLogEvent.NothingForTaskToDo,
                        Message.CreateAssortment_NeverCreateAssortmentSelected);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Logs the completion of the task, successfully created the assortment.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.LogInformation(
                    EventLogEvent.CreateAssortmentSuccessful, 
                    new Object[] { 
                        context.Planogram, 
                        context.ProductAdded, 
                        context.Planogram.Assortment.Products.Where(p => p.IsRanged).Count(), 
                        context.Planogram.Assortment.Products.Where(p => !p.IsRanged).Count() });

                return true;
            }
        }

        #endregion
    }
}