﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31005 : L.Luong
//  Created (Moved From Import Planogram Task)
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.CreateAssortment.v1
{
    /// <summary>
    /// Gets the options to create the assortment or not
    /// </summary>
    public enum CreateAssortmentType
    {
        AlwaysCreateAssortment,
        OnlyCreateIfAssortmentDoesNotExist,
        NeverCreateAssortment
    }

    /// <summary>
    ///     Helpers for <see cref="CreateAssortmentType"/>.
    /// </summary>
    public static class CreateAssortmentTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="CreateAssortmentType"/>.
        /// </summary>
        public static readonly Dictionary<CreateAssortmentType, String> FriendlyNames = new Dictionary<CreateAssortmentType, String>
        {
            {CreateAssortmentType.AlwaysCreateAssortment, Message.CreateAssortmentType_AlwaysCreateAssortment},
            {CreateAssortmentType.OnlyCreateIfAssortmentDoesNotExist, Message.CreateAssortmentType_OnlyCreateIfAssortmentDoesNotExist},
            {CreateAssortmentType.NeverCreateAssortment, Message.CreateAssortmentType_NeverCreateAssortment}
        };

    }
}