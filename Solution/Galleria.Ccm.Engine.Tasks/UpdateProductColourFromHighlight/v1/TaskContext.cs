﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30728 : A.Kuszyk
// Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1
{
    /// <summary>
    /// Task context object used to pass parameters
    /// throughout the execution of the task
    /// </summary>
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// The product filter to apply to the plan before applying the highlight.
        /// </summary>
        public ProductFilterType ProductFilter { get; set; }

        /// <summary>
        /// The gtins to filter the products by, if filtering by gtin.
        /// </summary>
        public IEnumerable<String> ProductFilterGtins { get; set; }

        /// <summary>
        /// The filtered enumeration of products to apply the highlight to.
        /// </summary>
        public IEnumerable<PlanogramProduct> FilteredProducts { get; set; }

        /// <summary>
        /// The name of the highlight to apply.
        /// </summary>
        public String HighlightName { get; set; }

        /// <summary>
        /// The highlight to apply to the <see cref="FilteredProducts"/>.
        /// </summary>
        public Highlight Highlight { get; set; }

        /// <summary>
        /// The number of products updated by the task, used for logging.
        /// </summary>
        public Int32 ProductsUpdated { get; set; }

        #endregion
    }
}
