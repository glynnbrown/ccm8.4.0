﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30728 : A.Kuszyk
// Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1
{
    public enum ProductFilterType
    {
        /// <summary>
        /// Apply the highlight to all products.
        /// </summary>
        All = 0,
        /// <summary>
        /// Apply the highlight to those products whose Gtin has been specified.
        /// </summary>
        Gtin = 1,
        /// <summary>
        /// Apply the highlight to only the products ranged in the plan's assortment.
        /// </summary>
        RangedInAssortment = 2,
        /// <summary>
        /// Apply the highlight to only the products not ranged in the plan's assortment.
        /// </summary>
        NotRangedInAssortment = 3
    }

    public static class ProductFilterTypeHelper
    {
        public static Dictionary<ProductFilterType, String> FriendlyNames = new Dictionary<ProductFilterType, String>()
        {
            { ProductFilterType.All, Message.UpdateProductColourFromHighlight_ProductFilter_All },
            { ProductFilterType.Gtin, Message.UpdateProductColourFromHighlight_ProductFilter_GTIN },
            { ProductFilterType.RangedInAssortment, Message.UpdateProductColourFromHighlight_ProductFilter_RangedInAssortment },
            { ProductFilterType.NotRangedInAssortment, Message.UpdateProductColourFromHighlight_ProductFilter_NotRangedInAssortment },
        };
    }
}
