﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30728 : A.Kuszyk
// Created.
// V8-30794 : A.Kuszyk
//  Re-factored update product code into Planogram.
// V8-31088 : A.Probyn
//  Updated so that highlight can be left as null when setting up the workflow task parameters and logs appropriately.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1
{
    /// <summary>
    /// Updates the colours of the Products in a Planogram using a highlight and a product filter.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            /// <summary>
            /// The name of the highlight to apply to the products.
            /// </summary>
            Highlight = 0,
            /// <summary>
            /// The filter to apply to the products.
            /// </summary>
            ProductFilter = 1,
            /// <summary>
            /// The gtins of the products to apply the highlight to, if filtering by gtin.
            /// </summary>
            ProductCode = 2,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdateProductColorFromHighlight_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.UpdateProductColorFromHighlight_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.Highlight,
                Message.UpdateProductColorFromHighlight_HighlightName, 
                Message.UpdateProductColorFromHighlight_HighlightDescription, 
                Message.ParameterCategory_Default,
                TaskParameterType.HighlightNameSingle,
                null,
                false,
                false,
                null,
                null,
                () => IsNullHighlightAllowed(),
                null));

            this.RegisterParameter(new TaskParameter(
                Parameter.ProductFilter,
                Message.UpdateProductColorFromHighlight_ProductFilter, 
                Message.UpdateProductColorFromHighlight_ProductFilter_Description, 
                Message.ParameterCategory_Default,
                typeof(ProductFilterType),
                ProductFilterType.All,
                false,
                false));

            this.RegisterParameter(new TaskParameter(
                    Parameter.ProductCode,
                    Message.UpdateProductColorFromHighlight_ProductGTINs_Name, 
                    Message.UpdateProductColorFromHighlight_ProductGtins_Description, 
                    Message.ParameterCategory_Default,
                    TaskParameterType.ProductCodeMultiple,
                    null,
                    false,
                    false,
                    () => IsProductCodeRequired())); // This parameter depends on the product filter.

        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetHighlight(taskContext)) return;
                if (!FilterProducts(taskContext)) return;
                if (!ApplyHighlightColourAndColourGroupValue(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Identifies whether or not a product code parameter value is required and returns the reason if not.
        /// </summary>
        /// <returns></returns>
        private String IsProductCodeRequired()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!this.Parameters.Contains((Int32)Parameter.ProductFilter)) return null;
                if (this.Parameters[(Int32)Parameter.ProductFilter] == null ||
                    this.Parameters[(Int32)Parameter.ProductFilter].Value == null ||
                    this.Parameters[(Int32)Parameter.ProductFilter].Value.Value1 == null) return null;

                ProductFilterType productFilter;
                try
                {
                    productFilter = (ProductFilterType)this.Parameters[(Int32)Parameter.ProductFilter].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    return null;
                }

                if (productFilter == ProductFilterType.Gtin) return null;
                return Message.UpdateProductColorFromHighlight_ProductCodeNotRequired;
            }
        }
        
        /// <summary>
        /// Identifies whether or not a highlight is allowed or not, and returns a reason why if not
        /// </summary>
        /// <returns></returns>
        private Boolean IsNullHighlightAllowed()
        {
            return true;
        }

        /// <summary>
        /// Validates the task parameters, storing their values in the <paramref name="taskContext"/> if they are valid
        /// or logging an error if they are not.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns>True if the parameter values are validated successfully.</returns>
        private Boolean ValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                // Highlight name
                if (!this.Parameters.Contains((Int32)Parameter.Highlight) ||
                    (this.Parameters[Parameter.Highlight].Value == null) ||
                    (this.Parameters[Parameter.Highlight].Value.Value1 == null))
                {
                    //  If there is no highlight selection the task has nothing to do.
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.UpdateProductColorFromHighlight_NoHighlightSelected);
                    success = false;
                }
                else
                {
                    taskContext.HighlightName = Convert.ToString(this.Parameters[Parameter.Highlight].Value.Value1);
                    if ((String.IsNullOrEmpty(taskContext.HighlightName)) || (String.IsNullOrWhiteSpace(taskContext.HighlightName)))
                    {
                        //  If there is no highlight selection the task has nothing to do.
                        taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.UpdateProductColorFromHighlight_NoHighlightSelected);
                        success = false;
                    }
                }

                // Filter type
                if (!this.Parameters.Contains((Int32)Parameter.ProductFilter) ||
                    (this.Parameters[Parameter.ProductFilter].Value == null) ||
                    (this.Parameters[Parameter.ProductFilter].Value.Value1 == null))
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdateProductColorFromHighlight_ProductFilter,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProductFilter = (ProductFilterType)this.Parameters[Parameter.ProductFilter].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdateProductColorFromHighlight_ProductFilter,
                            this.Parameters[Parameter.ProductFilter].Value.Value1);
                        success = false;
                    }
                }

                // Product gtins
                if ((!Parameters.Contains((Int32)Parameter.ProductCode)) ||
                    (Parameters[Parameter.ProductCode].Values == null))
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdateProductColorFromHighlight_ProductGTINs_Name,
                        Message.Error_NoValue);
                    return false;
                }
                try
                {
                    taskContext.ProductFilterGtins = Parameters[Parameter.ProductCode].Values
                        .Select(item => Convert.ToString(item.Value1)).ToList();
                }
                catch
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdateProductColorFromHighlight_ProductGTINs_Name,
                        Message.Error_NoValue);
                    return false;
                }

                return success;
            }
        }

        /// <summary>
        /// Loads the highlight from the database and stores it in the <paramref name="taskContext"/>.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns>True if the highlight was loaded successfully.</returns>
        private Boolean GetHighlight(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    taskContext.Highlight = Highlight.FetchByEntityIdName(taskContext.EntityId, taskContext.HighlightName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdateProductColorFromHighlight_HighlightName,
                            taskContext.HighlightName);
                        return false;
                    }
                    throw;
                }

                return true;
            }
        }

        /// <summary>
        /// Filters the planogram's products based on the product filter parameter value and stores the filtered
        /// list in the <paramref name=" taskContext"/>.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean FilterProducts(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (taskContext.Planogram == null) return false;

                switch (taskContext.ProductFilter)
                {
                    case ProductFilterType.All:
                        taskContext.FilteredProducts = taskContext.Planogram.Products;
                        break;

                    case ProductFilterType.Gtin:
                        if (taskContext.ProductFilterGtins.Any())
                        {
                            taskContext.FilteredProducts = taskContext.Planogram.Products
                                .Where(p => taskContext.ProductFilterGtins.Contains(p.Gtin))
                                .ToList();
                        }
                        else
                        {
                            // A gtin filter was specified, but no gtins were provided. Log a warning.
                            taskContext.LogWarning(
                                EventLogEvent.InvalidProductFilter,
                                Message.UpdateProductColourFromHighlight_InvalidGtinFilter);
                            taskContext.FilteredProducts = new List<PlanogramProduct>();
                        }
                        break;

                    case ProductFilterType.RangedInAssortment:
                        taskContext.FilteredProducts = FilterRangedProducts(taskContext, ranged: true);
                        break;

                    case ProductFilterType.NotRangedInAssortment:
                        taskContext.FilteredProducts = FilterRangedProducts(taskContext, ranged: false);
                        break;

                    default:
                        return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Filters products in the plan based on whether or not they are ranged or un-ranged in the assortment.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="ranged">True will pull out all ranged products; false all unranged products.</param>
        /// <returns></returns>
        private IEnumerable<PlanogramProduct> FilterRangedProducts(TaskContext taskContext, Boolean ranged)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                List<PlanogramProduct> filteredProducts = new List<PlanogramProduct>();

                if (taskContext.Planogram.Assortment != null && taskContext.Planogram.Assortment.Products.Any())
                {
                    foreach (PlanogramAssortmentProduct assortmentProduct in
                        taskContext.Planogram.Assortment.Products.Where(p => p.IsRanged == ranged))
                    {
                        PlanogramProduct planogramProduct = taskContext.Planogram.Products
                            .FirstOrDefault(p => p.Gtin.Equals(assortmentProduct.Gtin));
                        if (planogramProduct != null)
                        {
                            filteredProducts.Add(planogramProduct);
                        }
                    }
                }
                else
                {
                    // No assortment - log warning.
                    taskContext.LogWarning(
                        EventLogEvent.InvalidProductFilter,
                        Message.UpdateProductColourFromHighlight_InvalidAssortmentFilter);
                }

                return filteredProducts;
            }
        }

        /// <summary>
        /// Applies the highlight to the plan and sets the colour and label against each of the products in the highlight
        /// that are also in the filtered products list.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean ApplyHighlightColourAndColourGroupValue(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!taskContext.FilteredProducts.Any() || taskContext.Highlight == null) return false;

                IEnumerable<PlanogramProduct> productsAffected =
                    taskContext.Planogram.UpdateProductColoursFromHighlight(taskContext.Highlight, taskContext.FilteredProducts);

                taskContext.ProductsUpdated = productsAffected.Count();

                return true;
            }
        }

        /// <summary>
        /// Logs the completion of the task with the number of products updated and the name of the highlight used.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <returns></returns>
        private Boolean LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                taskContext.LogInformation(
                    EventLogEvent.ProductColoursUpdatedFromHighlight,
                    taskContext.ProductsUpdated,
                    taskContext.Highlight.Name);

                return true;
            }
        }

        #endregion
    }
}
