﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
// V8-29597 : N.Foster
//  Code Review
#endregion
#endregion

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramRenumbering.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion
    }
}