﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27474 : A.Silva
//  Created.
// V8-27562 : A.Silva
//  Localised texts.
// V8-27765/V8-28060 : A.Kuszyk
//  Added basic logging.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramRenumbering.v1
{
    /// <summary>
    /// Updates The sequence numbers for bays, components and positions in a planogram.
    /// </summary>
    [Serializable]
    public class Task :TaskBase
    {
        #region Version
        private Byte _majorVersion = 1;
        private Byte _minorVersion = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Returns the task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramRenumbering_Name; }
        }

        /// <summary>
        /// Returns the task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramRenumbering_Description; }
        }

        /// <summary>
        /// Returns the task grouping category.
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// Returns the minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Returns the major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        #endregion

        #region Parameter registration
        /// <summary>
        /// Called when parameters are required to be registered
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // This task does not require parameters so far.
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is to be executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!UpdatePlanogramRenumbering(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }
        
        #endregion

        #region Methods

        private static Boolean UpdatePlanogramRenumbering(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // update the planogram renumbering strategy
                taskContext.Planogram.RenumberingStrategy.RenumberBays();

                // return success
                return true;
            }
        }

        /// <summary>
        /// Logs completion of the task
        /// </summary>
        private static Boolean LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // log completion of the task
                taskContext.LogInformation(EventLogEvent.PlanogramReNumberingStrategyUpdated);

                // return success
                return true;
            }
        }

        #endregion
    }
}
