﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820
// V8-30733 : L.Luong
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields
        private List<String> _productGtins = new List<String>();
        private List<Task.LocationProductAttributeSelection> _locationProductAttributeSelections = new List<Task.LocationProductAttributeSelection>();
        private Dictionary<String, LocationProductAttribute> _masterProductsByGtin = new Dictionary<String, LocationProductAttribute>();
        private IEnumerable<PlanogramProduct> _planProducts;
        private Int32 _productsUpdated;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// The location name
        /// </summary>
        public List<String> ProductGtins
        {
            get { return _productGtins; }
        }

        /// <summary>
        /// The Location code
        /// </summary>
        internal List<Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1.Task.LocationProductAttributeSelection> LocationProductAttributeSelections
        {
            get { return _locationProductAttributeSelections; }
        }

        public IEnumerable<PlanogramProduct> PlanProducts
        {
            get { return _planProducts; }
            set { _planProducts = value; }
        }

        public Dictionary<String, LocationProductAttribute> MasterProductsByGtin
        {
            get { return _masterProductsByGtin; }
            set { _masterProductsByGtin = value; }
        }

        public Int32 ProductsUpdated
        {
            get { return _productsUpdated; }
            set { _productsUpdated = value; }
        }

        #endregion
    }
}
