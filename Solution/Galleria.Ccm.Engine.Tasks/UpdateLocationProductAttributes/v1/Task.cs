﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30733 : L.Luong
//  Created.
// V8-30904 : A.Probyn
//  Corrected the task so that null/blank values are applied the correct way around.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Nested Types
        /// <summary>
        /// A simple container for the attribute source and name values.
        /// </summary>
        internal class LocationProductAttributeSelection
        {
            /// <summary>
            /// The name of the property on the source that this item refers to.
            /// </summary>
            public String AttributeName { get; private set; }

            /// <summary>
            /// Instantiates a new object with the given values.
            /// </summary>
            /// <param name="source"></param>
            /// <param name="attributeName"></param>
            public LocationProductAttributeSelection(String attributeName)
            {
                AttributeName = attributeName;
            }
        }

        #endregion

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ProductCodes = 0,
            LocationProductAttributes = 1
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdateLocationProductAttributes_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdateLocationProductAttributes_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Product Codes
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductCodes,
                Message.UpdateLocationProductAttributes_ProductCodes,
                Message.UpdateLocationProductAttributes_ProductCodes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductCodeMultiple,
                null,
                true,
                true));

            // Location Product Attributes
            this.RegisterParameter(new TaskParameter(
                Parameter.LocationProductAttributes,
                Message.UpdateLocationProductAttributes_LocationProductAttributes,
                Message.UpdateLocationProductAttributes_LocationProductAttributes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.LocationProductAttributeMultiple,
                null,
                true,
                true));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (var taskContext = new TaskContext(context))
            {
                // Get and validate parameters                
                if (!GetAndValidateParameters(taskContext)) return;

                // Gets the products to update on the planogram
                if (!GetProductsToUpdate(taskContext)) return;

                // Update the product attributes and log the number updated.
                taskContext.ProductsUpdated =
                    UpdateProducts(taskContext.PlanProducts, taskContext.LocationProductAttributeSelections, taskContext.MasterProductsByGtin);

                // remerchandise the planogram to take into account any product size changes
                if (!ReMerchandisePlanogram(taskContext)) return;

                // Log updated products
                LogCompletion(taskContext);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets parameter values, valdidates them and assigns them to the the given arguments.
        /// </summary>
        /// <param name="context">The current task context</param>
        /// <returns></returns>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            // Get and validate product codes.
            // Ordinarily, a null value would be an issue, but in this case we don't mind, because
            // we'll just process all products in the plan later on. So, we can just leave
            // productCodes assigned to a new list.
            if (Parameters[Parameter.ProductCodes].Value != null)
            {
                try
                {
                    taskContext.ProductGtins.AddRange(this.Parameters[Parameter.ProductCodes].Values.
                        Select(item => Convert.ToString(item.Value1)).ToList());
                }
                catch
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdateLocationProductAttributes_ProductCodes,
                        Message.Error_NoValue);
                    return false;
                }
            }

            // Get and validate location product attributes.
            // Similarly to product codes, a null parameter value here isn't an issue, 
            // because we'll just process all of the product attributes. Just leave
            // locationProductAttributes assigned to a new list.
            if (Parameters[Parameter.LocationProductAttributes].Value != null)
            {
                try
                {
                    taskContext.LocationProductAttributeSelections.AddRange(this.Parameters[Parameter.LocationProductAttributes].Values.
                        Select(item =>
                            new LocationProductAttributeSelection(item.Value1.ToString())
                            ).
                        ToList());
                }
                catch
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdateLocationProductAttributes_ProductCodes,
                        Message.Error_NoValue);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets and validates that there are products to update on the planogram
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if products to update</returns>
        private Boolean GetProductsToUpdate(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                List<Tuple<Int16, Int32>> locationProducts = new List<Tuple<Int16, Int32>>();
                Boolean locationValid = true;

                // If we don't have any product gtins, we can just use all plan products.
                if (taskContext.ProductGtins.Any())
                {
                    taskContext.PlanProducts = taskContext.Planogram.Products.Where(p => taskContext.ProductGtins.Contains(p.Gtin));
                }
                // Get the planogram products from their gtins.
                else
                {
                    taskContext.PlanProducts = taskContext.Planogram.Products;
                }

                IEnumerable<String> gtins = taskContext.ProductGtins.Any() ? taskContext.ProductGtins : taskContext.PlanProducts.Select(p => p.Gtin);
                ProductList productList = ProductList.FetchByEntityIdProductGtins(taskContext.EntityId, gtins);

                if (taskContext.Planogram.LocationCode != null && taskContext.Planogram.LocationName != null)
                {
                    List<String> locationCode = new List<String>();
                    locationCode.Add(taskContext.Planogram.LocationCode);

                    LocationInfoList locationList = LocationInfoList.FetchByEntityIdLocationCodes(taskContext.EntityId, locationCode);

                    if (locationList.Any())
                    {
                        foreach (Product product in productList)
                        {
                            locationProducts.Add(new Tuple<Int16, Int32>(locationList.FirstOrDefault().Id, product.Id));
                        }


                        // Get master data products and key them by Gtin.
                        taskContext.MasterProductsByGtin = LocationProductAttributeList.FetchByLocationIdProductIdCombinations(taskContext.EntityId, locationProducts).
                            ToDictionary(p => productList.FirstOrDefault(l => l.Id == p.ProductId).Gtin, p => p);
                    }
                    else
                    {
                        locationValid = false;
                    }
                }
                else
                {
                    locationValid = false;
                }

                if (!locationValid)
                {
                    taskContext.LogWarning(EventLogEvent.PlanogramNoValidLocation,
                        Message.UpdateLocationProductAttributes_PlanogramHasNoValidLocation);
                    return false;
                }

                // Check that there is still something for us to do.
                if (!taskContext.MasterProductsByGtin.Any())
                {
                    taskContext.LogInformation(
                        EventLogEvent.NothingForTaskToDo,
                        Message.UpdateLocationProductAttributes_NoMatchingLocationProducts);
                    return false;
                }

                // Check that there is still something for us to do.
                if (!taskContext.PlanProducts.Any())
                {
                    taskContext.LogInformation(
                        EventLogEvent.NothingForTaskToDo,
                        Message.UpdateLocationProductAttributes_NoMatchingPlanogramProducts);
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Updates the attributes for the given products, for the given attributes, from the given masterdata products.
        /// </summary>
        /// <param name="planProducts">The planogram products to update.</param>
        /// <param name="locationProductAttributeSelections">The attributes to update.</param>
        /// <param name="masterProductsByGtin">The masterdata products to use as a source, keyed by gtin.</param>
        /// <param name="context">The task context.</param>
        internal static Int32 UpdateProducts(
            IEnumerable<PlanogramProduct> planProducts,
            IEnumerable<LocationProductAttributeSelection> locationProductAttributeSelections,
            Dictionary<String, LocationProductAttribute> masterProductsByGtin)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Update each product in turn, and for each product update each attribute in turn.
                Int32 productsUpdated = 0;
                foreach (PlanogramProduct planProduct in planProducts)
                {
                    LocationProductAttribute product;
                    if (!masterProductsByGtin.TryGetValue(planProduct.Gtin, out product)) continue;
                    productsUpdated++;

                    // If we have some product attributes selected, just update those attributes. = override all values selected (all or specific
                    // field selection, even if these values are null/empty
                    if (locationProductAttributeSelections.Any())
                    {
                        foreach (LocationProductAttributeSelection attributeSelection in locationProductAttributeSelections)
                        {
                            planProduct.UpdateLocationProductAttributes(product, true, attributeSelection.AttributeName);
                        }
                    }
                    else //0 fields selected = dont override with any null/empty values, but override any set for all fields
                    {
                        List<String> allProperties = LocationProductAttribute.EnumerateDisplayableFieldInfos().Select(p => p.PropertyName).ToList();
                        foreach (String property in allProperties)
                        {
                            planProduct.UpdateLocationProductAttributes(product, false, property);
                        }
                    }
                }
                return productsUpdated;
            }
        }
        /// <summary>
        /// Log planogram products updated.
        /// </summary>
        /// <param name="taskContext"></param>
        private void LogCompletion(TaskContext taskContext)
        {
            taskContext.LogInformation(EventLogEvent.PlanogramProductsUpdated, taskContext.ProductsUpdated);
        }

        /// <summary>
        /// Remerchandises the planogram if required
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            // if nothing has been updated, then no need to remerchandise the planogram
            if (context.ProductsUpdated == 0) return true;

            // remerchandise the planogram
            context.RemerchandisePlanogram();

            // return success
            return true;
        }

        #endregion
    }
}
