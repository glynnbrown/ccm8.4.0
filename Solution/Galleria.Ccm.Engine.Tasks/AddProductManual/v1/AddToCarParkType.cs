﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.AddProductManual.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// add to carpark shelf param
    /// </summary>
    public enum AddToCarParkType
    {
        Yes,
        No
    }

    /// <summary>
    /// Contains helpers for the AddToCarparkType enum.
    /// </summary>
    public static class AddToCarParkTypeHelper
    {
        public static readonly Dictionary<AddToCarParkType, String> FriendlyNames =
            new Dictionary<AddToCarParkType, String>()
            {
                {AddToCarParkType.Yes, Message.Generic_Yes },
                {AddToCarParkType.No, Message.Generic_No },
            };
    }
}
