﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
// V8-29597 : N.Foster
//  Code Review
#endregion

#region Version History : CCM811
// V8-30429  : J.Pickup
//  Introduced ClusterName & ClusterId
#endregion

#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramClusterAttribute.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields

        private String _clusterSchemeName;
        private Int32 _clusterSchemeId = 0;
        private String _clusterName;
        private Cluster _cluster;

        #endregion

        #region Constructor

        public TaskContext(ITaskContext context) : base(context) { }

        #endregion

        #region Properties

        /// <summary>
        /// The cluster scheme name
        /// </summary>
        public String ClusterSchemeName
        {
            get { return _clusterSchemeName; }
            set { _clusterSchemeName = value; }
        }

        /// <summary>
        /// The cluster scheme Id
        /// </summary>
        public Int32 ClusterSchemeId
        {
            get { return _clusterSchemeId; }
            set { _clusterSchemeId = value; }
        }

        /// <summary>
        /// The cluster name
        /// </summary>
        public String ClusterName
        {
            get { return _clusterName; }
            set { _clusterName = value; }
        }

        /// <summary>
        /// The Cluster
        /// </summary>
        public Cluster Cluster
        {
            get { return _cluster; }
            set { _cluster = value; }
        }
        
        #endregion
    }
}