﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29609 : D.Pleasance
//  Created.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Cluster name is now a parameter. Will now use provided name where provided else continues with a location lookup. 
//  Location lookup is now a warning not an error.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramClusterAttribute.v1
{
    /// <summary>
    /// Updates Cluster Attributes in the planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ClusterSchemeName = 0,
            ClusterName = 1
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramClusterAttribute_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramClusterAttribute_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Cluster Scheme Name
            this.RegisterParameter(new TaskParameter(
                Parameter.ClusterSchemeName,
                Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_Name,
                Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ClusterSchemeNameSingle,
                null,
                false,
                false));

            // Cluster Name
            this.RegisterParameter(new TaskParameter(
                Parameter.ClusterName,
                Message.UpdatePlanogramClusterAttribute_ClusterName_Name,
                Message.UpdatePlanogramClusterAttribute_ClusterName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ClusterNameSingle,
                null,
                false,
                false,
                (Int32)Parameter.ClusterSchemeName));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!DoesClusterSchemeExist(taskContext)) return;
                if (taskContext.ClusterName != null && taskContext.ClusterName != String.Empty)
                {
                    if (!DoesClusterExistWithinClusterScheme(taskContext)) return; 
                }
                if (!UpdateClusterAttributes(taskContext)) return;
            }
        }
                
        #endregion

        #region Methods

        /// <summary>
        /// Gets task parameters and validates
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if successful</returns>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (Parameters[Parameter.ClusterSchemeName].Value == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_Name,
                        Message.Error_NoValue);
                    return false;
                }

                taskContext.ClusterSchemeName = Convert.ToString(Parameters[Parameter.ClusterSchemeName].Value.Value1);
                if (String.IsNullOrEmpty(taskContext.ClusterSchemeName))
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_Name,
                        String.Format("{0}", taskContext.ClusterSchemeName));
                    return false;
                }

                if (Parameters[Parameter.ClusterName].Value != null)
                {
                    taskContext.ClusterName = Convert.ToString(Parameters[Parameter.ClusterName].Value.Value1);
                }

                if ((taskContext.Planogram.LocationCode == null || taskContext.Planogram.LocationCode.Length == 0)
                    && (taskContext.ClusterName == null || taskContext.ClusterName == String.Empty))
                {
                    taskContext.LogWarning(EventLogEvent.PlanogramLocationReferenceInvalid, Message.Error_NoValue);
                }
                else if (taskContext.ClusterName == null || taskContext.ClusterName == String.Empty)
                {
                    LocationInfo locationInfo = null;
                    try
                    {
                        locationInfo = LocationInfoList
                            .FetchByEntityIdLocationCodes(taskContext.EntityId, new[] { taskContext.Planogram.LocationCode })
                            .FirstOrDefault();
                    }
                    catch (DataPortalException ex)
                    {
                        if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw ex;
                    }

                    if (locationInfo == null)
                    {
                        taskContext.LogError(
                        EventLogEvent.PlanogramLocationReferenceInvalid, taskContext.Planogram.LocationCode);
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Validates that the provided cluster scheme is valid
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if the cluster scheme is valid</returns>
        private static Boolean DoesClusterSchemeExist(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                ClusterSchemeInfo clusterSchemeInfo = null;
                ClusterSchemeInfoList clusterSchemeInfoList = ClusterSchemeInfoList.FetchByEntityId(taskContext.EntityId);

                clusterSchemeInfo = clusterSchemeInfoList.Where(p => p.Name == taskContext.ClusterSchemeName).FirstOrDefault();

                if (clusterSchemeInfo == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_Name,
                        String.Format("{0}", taskContext.ClusterSchemeName));
                    return false;
                }

                taskContext.ClusterSchemeId = clusterSchemeInfo.Id;
                return true;
            }
        }

        /// <summary>
        /// Validates that the provided cluster exists within the given cluster scheme
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if the cluster is exists in the cluster scheme</returns>
        private static Boolean DoesClusterExistWithinClusterScheme(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                ClusterList clusterList = ClusterList.FetchByClusterSchemeId(taskContext.ClusterSchemeId);

                List<Cluster> matchingClusters = clusterList.Where(cs => cs.Name == taskContext.ClusterName).ToList();

                if (matchingClusters != null && matchingClusters.Any())
                {
                    taskContext.Cluster = matchingClusters.First();
                }

                if (taskContext.Cluster == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramClusterAttribute_ClusterName_Name,
                        String.Format("{0}", taskContext.ClusterName));
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Update the planograms cluster attributes
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <param name="locationCode">The planogram location code</param>
        private static Boolean UpdateClusterAttributes(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Planogram planogram = taskContext.Planogram;

                String returnableOldClusterSchemeName = planogram.ClusterSchemeName == String.Empty ? Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_NoValue : planogram.ClusterSchemeName;
                String returnableOldClusterName = planogram.ClusterName == String.Empty ? Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_NoValue : planogram.ClusterName;
                String returnableNewClusterSchemeName = taskContext.ClusterSchemeName == String.Empty ? Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_NoValue : taskContext.ClusterSchemeName;
                String returnableNewClusterName = taskContext.ClusterName == String.Empty ? Message.UpdatePlanogramClusterAttribute_ClusterSchemeName_NoValue : taskContext.ClusterName;

                ClusterScheme clusterScheme = ClusterScheme.FetchById(taskContext.ClusterSchemeId);

                // If have a cluster already specified.
                if (taskContext.Cluster != null)
                {
                    planogram.ClusterSchemeName = taskContext.ClusterSchemeName;

                    if (taskContext.ClusterName != null && taskContext.ClusterName != String.Empty)
                    {
                        planogram.ClusterName = taskContext.ClusterName;
                        taskContext.LogInformation(
                                EventLogEvent.PlanogramClusterUpdated, returnableOldClusterSchemeName, returnableOldClusterName, returnableNewClusterSchemeName, returnableNewClusterName);
                    }
                    else
                    {
                        returnableNewClusterName = returnableOldClusterName;
                        taskContext.LogInformation(
                                EventLogEvent.PlanogramClusterUpdated, returnableOldClusterSchemeName, returnableOldClusterName, returnableNewClusterSchemeName, returnableNewClusterName);
                    }
                }
                else
                {
                    // We try and use location code to set the cluster 
                    planogram.ClusterSchemeName = taskContext.ClusterSchemeName;

                    Boolean foundAMatchingCluster = false;

                    foreach (Cluster cluster in clusterScheme.Clusters)
                    {
                        if (cluster.Locations.Where(p => p.Code == planogram.LocationCode).Any())
                        {
                            foundAMatchingCluster = true;
                            planogram.ClusterName = cluster.Name;
                            break;
                        }
                    }

                    if (foundAMatchingCluster)
                    {
                        returnableNewClusterName = planogram.ClusterName;
                        taskContext.LogInformation(
                                EventLogEvent.PlanogramClusterUpdated, returnableOldClusterSchemeName, returnableOldClusterName, returnableNewClusterSchemeName, returnableNewClusterName);
                    }
                    else
                    {
                        returnableNewClusterName = returnableOldClusterName;
                        taskContext.LogInformation(
                                EventLogEvent.PlanogramClusterUpdated, returnableOldClusterSchemeName, returnableOldClusterName, returnableNewClusterSchemeName, returnableNewClusterName);
                    }
                }

                // return success
                return true;
            }
        }

        #endregion
    }
}