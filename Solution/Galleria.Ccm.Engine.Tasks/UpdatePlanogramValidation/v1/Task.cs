﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25839 : L.Ineson
//  Created
// V8-27062 : A.Silva ~ Implemented OnExecute() and OnRegisterParameters().
// V8-27765/28060 : A.Kuszyk
//  Added basic logging.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-29980 : M.Brumby
//  Made use of a new CalculateValidationData method to call if we are wanting to update meta data in
//  the same way that CalculateMetadata does. The OnCalculateValidationData nolonger tries to create
//  metadata.
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1
{
    /// <summary>
    /// Runs the planogram validation system which will provide important 
    /// information about the planogram to enable the user to determine if it meets the users requirements
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramValidation_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.UpdatePlanogramValidation_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // This task does not require parameters so far.
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!UpdatePlanogramValidation(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        private static Boolean UpdatePlanogramValidation(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // pass in metadata variables incase there is no current metadata.
                taskContext.Planogram.Parent.CalculateValidationData(/*processMetaImages*/true,
                    PlanogramMetadataHelper.NewPlanogramMetadataHelper(taskContext.EntityId, taskContext.Planogram.Parent));

                // return success
                return true;
            }
        }

        private static Boolean LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // log task completion
                taskContext.LogInformation(
                        EventLogEvent.PlanogramValidationUpdated,
                        GetValidationTemplateResult(taskContext.Planogram.ValidationTemplate));

                // return success
                return true;
            }
        }

        private static String GetValidationTemplateResult(PlanogramValidationTemplate planogramValidationTemplate)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                StringBuilder resultBuilder = new StringBuilder();
                Int32 index = 1;
                Int32 groupCount = planogramValidationTemplate.Groups.Count;
                foreach (var group in planogramValidationTemplate.Groups)
                {
                    String format = "{0} - {1}, ";
                    if (index == groupCount)
                    {
                        format = "{0} - {1}";
                    }

                    resultBuilder.AppendFormat(format, group.Name, PlanogramValidationTemplateResultTypeHelper.FriendlyNames[group.ResultType]);
                    index++;
                }
                return resultBuilder.ToString();
            }
        }

        #endregion
    }
}
