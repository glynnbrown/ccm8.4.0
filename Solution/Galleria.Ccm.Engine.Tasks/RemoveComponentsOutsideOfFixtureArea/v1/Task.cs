﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32522 : A.Silva
//  Created
//  Amended RemoveComponents to not depend on metadata being present, all necessary data is fetched directly by the task now.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.RemoveComponentsOutsideOfFixtureArea.v1
{
    /// <summary>
    ///     Task to remove any components flagged as IsOutsideOfFixtureArea.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        private const Boolean Success = true;

        #region Fields

        /// <summary>
        ///     The minor version number for this implementation.
        /// </summary>
        private readonly Byte _minorVersion = 0;

        /// <summary>
        ///     The major version number for this implementation.
        /// </summary>
        private readonly Byte _majorVersion = 1;

        #endregion

        #region Properties

        /// <summary>
        ///     Get the task name.
        /// </summary>
        public override String Name { get { return Message.RemoveComponentsOutsideOfFixtureArea_Name; } }

        /// <summary>
        ///     Get the task description.
        /// </summary>
        public override String Description { get { return Message.RemoveComponentsOutsideOfFixtureArea_Description; } }

        /// <summary>
        ///     Get the task grouping category.
        /// </summary>
        public override String Category { get { return Message.TaskCategory_UpdatePlanogram; } }

        /// <summary>
        ///     Get the minor version number.
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        /// <summary>
        ///     Get the major version number.
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when parameters are required to be registered.
        /// </summary>
        protected override void OnRegisterParameters()
        {
            //  This tasks has not parameters at the moment.
        }

        /// <summary>
        ///     Called when this task is to be executed.
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters()) return;
                if (!RemoveComponents(taskContext)) return;
                LogCompletion(taskContext);
            }
        }

        /// <summary>
        ///     Validate this instance's task parameters, ensuring they are correct.
        /// </summary>
        /// <returns><c>True</c> if all the parameters are valid, <c>false</c> otherwise.</returns>
        private static Boolean ValidateParameters()
        {
            //  This task has no parameters at the moment, so nothing to validate.
            return Success;
        }

        /// <summary>
        ///     Remove any component flagged IsOuotsideOfFixtureArea.
        /// </summary>
        /// <param name="context">The current context to use for this task.</param>
        /// <returns><c>True</c> if all the parameters are valid, <c>false</c> otherwise.</returns>
        private static Boolean RemoveComponents(TaskContext context)
        {
            foreach (PlanogramFixture fixture in context.Planogram.Fixtures)
            {
                context.RemovedComponentsCount += RemoveFixtureComponentsWhere(fixture, IsOutsideOfFixtureAreaOrWithoutFixture);
                context.RemovedComponentsCount += RemoveFixtureAssembliesWhere(fixture, assembly => AllAssemblyCompoments(assembly, IsOutsideOfFixtureAreaOrWithoutFixture));
            }

            return Success;
        }

        /// <summary>
        ///     Remove any of the fixture's <see cref="PlanogramFixtureComponent"/> that match the <paramref name="predicate"/>.
        /// </summary>
        /// <param name="fixture">The fixture to remove matching fixture components from.</param>
        /// <param name="predicate">Condition that a <see cref="PlanogramFixtureComponent"/> must meet to be removed.</param>
        /// <returns>The number of items removed.</returns>
        /// <remarks>This static method should be in <see cref="PlanogramFixture"/> as an instance method.</remarks>
        private static Int32 RemoveFixtureComponentsWhere(PlanogramFixture fixture, Func<PlanogramFixtureComponent, Boolean> predicate)
        {
            PlanogramFixtureComponentList components = fixture.Components;
            List<PlanogramFixtureComponent> matches = components.Where(predicate).ToList();
            components.RemoveList(matches);
            return matches.Count;
        }

        /// <summary>
        ///     Remove any of the fixture's <see cref="PlanogramAssemblyComponent"/> that match the <paramref name="predicate"/>.
        /// </summary>
        /// <param name="fixture">The fixture to remove matching assembly components from.</param>
        /// <param name="predicate">Condition that a <see cref="PlanogramFixtureAssembly"/> must meet to be removed.</param>
        /// <returns>The number of items removed.</returns>
        /// <remarks>This static method should be in <see cref="PlanogramFixture"/> as an instance method.</remarks>
        private static Int32 RemoveFixtureAssembliesWhere(PlanogramFixture fixture, Func<PlanogramFixtureAssembly, Boolean> predicate)
        {
            PlanogramFixtureAssemblyList assemblies = fixture.Assemblies;
            List<PlanogramFixtureAssembly> matches = assemblies.Where(predicate).ToList();
            assemblies.RemoveList(matches);
            return matches.Count;
        }

        /// <summary>
        ///     Check whether all the <see cref="PlanogramFixtureAssembly"/>'s <see cref="PlanogramAssemblyComponent"/> match the <paramref name="predicate"/>.
        /// </summary>
        /// <param name="assembly">The fixture to remove matching assembly components from.</param>
        /// <param name="predicate">Condition that all <see cref="PlanogramAssemblyComponent"/> must meet for the method to return true.</param>
        /// <returns><c>True</c> if all the components for the assembly meet the criteria.</returns>
        /// <remarks>This static method should be in <see cref="PlanogramFixtureAssembly"/> as an instance method.</remarks>
        private static Boolean AllAssemblyCompoments(PlanogramFixtureAssembly assembly, Func<PlanogramAssemblyComponent, Boolean> predicate)
        {
            return assembly.GetPlanogramAssembly().Components.All(predicate);
        }

        /// <summary>
        ///     Predicate to match <see cref="IPlanogramFixtureComponent"/> instances that are either entirely outside their parent fixture OR without a parent fixture.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        private static Boolean IsOutsideOfFixtureAreaOrWithoutFixture(IPlanogramFixtureComponent component)
        {
            return component.IsEntirelyOutsideParentFixture() != false;
        }

        /// <summary>
        ///     Log the completion log for this instance's execution.
        /// </summary>
        /// <param name="context">The current context to use for this task.</param>
        private static void LogCompletion(TaskContext context)
        {
            context.LogInformation(EventLogEvent.PlanogramComponentsRemoved, context.RemovedComponentsCount);
        }

        #endregion
    }
}