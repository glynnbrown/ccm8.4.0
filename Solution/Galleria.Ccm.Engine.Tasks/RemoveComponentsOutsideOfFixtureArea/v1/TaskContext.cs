﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32522 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Linq;

namespace Galleria.Ccm.Engine.Tasks.RemoveComponentsOutsideOfFixtureArea.v1
{
    /// <summary>
    ///     The <see cref="Ccm.Engine.TaskContext"/> sub class to serve as a context for the Remove Components Outside Of Fixture Area <see cref="Task"/>.
    /// </summary>
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) {}

        #endregion

        #region Properties

        /// <summary>
        ///     The count of removed components when the task is complete.
        /// </summary>
        public Int32 RemovedComponentsCount { get; set; }

        #endregion
    }
}