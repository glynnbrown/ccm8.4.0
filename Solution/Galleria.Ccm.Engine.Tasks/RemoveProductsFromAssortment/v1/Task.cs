﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-27688 : A.Kuszyk
//  Created.
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-28060 : A.Kuszyk
//  Added more detailed logging.
// V8-28190 : A.Kuszyk
//  Updated to also remove products that are in the assortment, but that are set to unranged.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM810
// V8-29373 : N.Foster
//  Car park shelf changes
// V8-29821 : M.Brumby
//  Log number of product positions removed
// V8-30144 : D.Pleasance
//  Amended so that MerchandisingGroups are obtained on the task context and disposed when the task is completed. This is to ensure that positions 
//  are not removed when previously looking up the car park shelf. Previously the car park shelf was found, then dispose was called immediately 
//  after which resulting in existing car park placement positions being removed.
#endregion
#region Version History: CCM820
// V8-30710 : M.Brumby
//  Make missing assortment messaging consistent across tasks.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1
{
    /// <summary>
    /// Remove Products from the plan that don't appear in the assortment
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            RemoveAction = 0,
            CarParkComponentName = 1,
            AddCarParkTextBox = 2,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RemoveProductsFromAssortment_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.RemoveProductsFromAssortment_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // remove action
            this.RegisterParameter(new TaskParameter(
                Parameter.RemoveAction,
                Message.RemoveProductsFromAssortment_Parameter_RemoveAction_Name,
                Message.RemoveProductsFromAssortment_Parameter_RemoveAction_Description,
                Message.ParameterCategory_Default,
                typeof(RemoveActionType),
                RemoveActionType.RemovePositions,
                false,
                false));

            // car park component name
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.CarParkComponentName,
                    name: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name,
                    description: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.String,
                    defaultValue: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Default,
                    isHiddenByDefault: true,
                    isReadOnlyByDefault: true,
                    parentId: null,
                    isRequired: null,
                    isNullAllowed: null,
                    isValid: () => IsCarParkNameValid()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.RemoveProductsFromAssortment_Parameter_AddCarParkTextBox_Name,
                Message.RemoveProductsFromAssortment_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!this.ValidateParameters(taskContext)) return;
                if (!this.ValidatePlanogram(taskContext)) return;
                if (!this.PopulateLookups(taskContext)) return;
                if (!this.RemovePositionsFromPlanogram(taskContext)) return;
                if (!this.GetCarParkComponent(taskContext)) return;
                if (!this.AddPositionsToCarParkComponent(taskContext)) return;
                if (!this.RemoveProductsFromPlanogram(taskContext)) return;
                if (!this.ReMerchandisePlanogram(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region Remove Action

                // validate the remove action type
                if ((!this.Parameters.Contains((Int32)Parameter.RemoveAction)) ||
                    (this.Parameters[Parameter.RemoveAction].Value == null) ||
                    (this.Parameters[Parameter.RemoveAction].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsFromAssortment_Parameter_RemoveAction_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.RemoveAction = (RemoveActionType)this.Parameters[Parameter.RemoveAction].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsFromAssortment_Parameter_RemoveAction_Name, Parameters[Parameter.RemoveAction].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (this.Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RemoveProductsFromAssortment_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.RemoveProductsFromAssortment_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Validates the planogram details
        /// </summary>
        private Boolean ValidatePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify we have an assortment
                if ((context.Planogram.Assortment == null) ||
                    (context.Planogram.Assortment.Products == null) ||
                    ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                {
                    context.LogWarning(EventLogEvent.PlanogramNoAssortmentPresent);
                    return false;
                }

                // return succes
                return true;
            }
        }

        /// <summary>
        /// Populates lookup lists for the rest of the process
        /// </summary>
        private Boolean PopulateLookups(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a lookup of all products within
                // the planogram indexed by product id
                foreach (PlanogramProduct product in context.Planogram.Products)
                {
                    if (!context.PlanogramProductIdLookup.ContainsKey(product.Id))
                        context.PlanogramProductIdLookup.Add(product.Id, product);
                }


                // build a list of all assortment product codes
                List<String> assortmentProductCodes = new List<String>();
                foreach (PlanogramAssortmentProduct product in context.Planogram.Assortment.Products.Where(p => p.IsRanged))
                {
                    if (!assortmentProductCodes.Contains(product.Gtin))
                        assortmentProductCodes.Add(product.Gtin);
                }

                // now build a list of all planogram products
                // that do not exist within the assortment
                foreach (PlanogramProduct product in context.Planogram.Products.OrderBy(p => p.Gtin))
                {
                    if ((!assortmentProductCodes.Contains(product.Gtin)) && (!context.PlanogramProductsToRemove.ContainsKey(product.Id)))
                        context.PlanogramProductsToRemove.Add(product.Id, product);
                }

                // now build a list of all planogram positions
                // that do not exist within the assortment
                foreach (PlanogramPosition position in context.Planogram.Positions.OrderBy(p => context.PlanogramProductIdLookup[p.PlanogramProductId].Gtin))
                {
                    if ((context.PlanogramProductsToRemove.ContainsKey(position.PlanogramProductId)) && (!context.PlanogramPositionsToRemove.Contains(position)))
                        context.PlanogramPositionsToRemove.Add(position);
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Removes unranged positions from the planogram
        /// </summary>
        private Boolean RemovePositionsFromPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // check that we have some positions to remove
                if (context.PlanogramPositionsToRemove.Count == 0) return true;

                // remove all positions from the planogram
                foreach (PlanogramPosition position in context.PlanogramPositionsToRemove)
                {
                    context.Planogram.Positions.Remove(position);
                }

                // Log totals for products removed.
                context.LogInformation(
                    EventLogEvent.PlanogramProductPositionsRemoved, context.PlanogramPositionsToRemove.Count);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Retrieves the car park component if it exists
        /// else creates the car park component
        /// </summary>
        private Boolean GetCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are not moving positions to the car park shelf
                // then there is nothing for us to do
                if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

                // if there are no positions to remove, then there
                // is nothing for use to do either
                if (context.PlanogramPositionsToRemove.Count == 0) return true;

                // get the merchandising groups
                context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();

                // get the car park component
                context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Moves positions from the planogram to the car park component
        /// </summary>
        private Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are not moving positions to the car park component
                // then there is nothing for us to do
                if (context.RemoveAction != RemoveActionType.RemovePositionsAndAddToCarPark) return true;

                // check that we have some positions to add
                if (context.PlanogramPositionsToRemove.Count == 0) return true;

                // if we have no car park component, then do nothing
                if (context.CarParkMerchandisingGroup == null) return true;

                // create a new position in the car park merchandising group
                // for each position in the list of positions to remove
                foreach (PlanogramPosition oldPosition in context.PlanogramPositionsToRemove)
                {
                    // get the planogram product
                    PlanogramProduct product = context.PlanogramProductIdLookup[oldPosition.PlanogramProductId];

                    // create a new position on the merchandising group
                    context.CarParkMerchandisingGroup.InsertPositionPlacement(product);
                }

                // apply changes to the car park component
                context.CarParkMerchandisingGroup.Process();
                context.CarParkMerchandisingGroup.ApplyEdit();

                // return success
                return true;
            }
        }

        /// <summary>
        /// Removes products from the planogram
        /// </summary>
        private Boolean RemoveProductsFromPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are moving positions to the car park shelf
                // then there is nothing for us to do
                if (context.RemoveAction != RemoveActionType.RemovePositionsAndProducts) return true;

                // check that we have some products to remove
                if (context.PlanogramProductsToRemove.Count == 0) return true;

                // remove products from the planogram
                context.Planogram.Products.RemoveList(context.PlanogramProductsToRemove.Values);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // check that some positions have either been moved or removed
                if (context.PlanogramPositionsToRemove.Count == 0) return true;

                // remerchandise the whole planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion
    } 
}
