﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27688 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1
{
    /// <summary>
    /// Provides types of action to take when remove a Product from an Assortment.
    /// </summary>
    public enum RemoveActionType
    {
        RemovePositions,
        RemovePositionsAndProducts,
        RemovePositionsAndAddToCarPark
    }

    // Helper for RemoveActionType enum.
    public static class RemoveActionTypeHelper
    {
        public static Dictionary<RemoveActionType, String> FriendlyNames =
            new Dictionary<RemoveActionType, String>()
            {
                { RemoveActionType.RemovePositionsAndAddToCarPark, Message.RemoveProductsFromAssortment_Parameter_RemoveActionType_RemovePositionsAndAddToCarPark },
                { RemoveActionType.RemovePositions, Message.RemoveProductsFromAssortment_Parameter_RemoveActionType_RemovePositions },
                { RemoveActionType.RemovePositionsAndProducts, Message.RemoveProductsFromAssortment_Parameter_RemoveActionType_RemovePositionsAndProducts }
            };
    }
}
