﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-30051 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// V8-30847 : D.Pleasance
//  Added car park properties
// V8-30920 : A.Kuszyk
//  Modified task behaviour to place shelves in subsequent bays once the first bay is full.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private SequenceType _sequenceType;
        private SequencePlacementPrimaryType _sequencePlacementPrimaryType;
        private SequencePlacementSecondaryType _sequencePlacementSecondaryType;
        private SequencePlacementTertiaryType _sequencePlacementTertiaryType;
        private UnplacedProductBehaviourType _unplacedProductBehaviourType;
        private Dictionary<Int32, String> _blockingGroups = new Dictionary<Int32, String>();
        private Dictionary<String, PlanogramProduct> _planogramProducts = new Dictionary<String, PlanogramProduct>();
        private List<PlanogramProduct> _unplacedPlanogramProducts = new List<PlanogramProduct>();        
        private Dictionary<String, PlanogramSequenceGroupProduct> _sequenceProducts = new Dictionary<String, PlanogramSequenceGroupProduct>();
        private List<PlanogramSubComponentPlacement> _subComponentPlacements = new List<PlanogramSubComponentPlacement>();
        private List<PlanogramSubComponentPlacement> _carParkSubComponentPlacements = new List<PlanogramSubComponentPlacement>();        
        private String _carParkComponentName;
        private List<PlanogramProduct> _carParkProducts = new List<PlanogramProduct>();
        private Boolean _addCarParkTextBox;
        #endregion

        #region Contants

        /// <summary>
        /// Used to multiply the maximum height of placed products when spacing shelves out vertically.
        /// </summary>
        public const Single HeightMultiplier = 1.5f;

        /// <summary>
        /// Used to multiply the total width of all the products that might appear on a shelf when
        /// calculating the maximum width of shelves.
        /// </summary>
        public const Single WidthMultiplier = 1.2f;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            CarParkComponentWidthsByComponent = new Dictionary<PlanogramComponent, Single>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// The Settings to use when constructing shelves and fixtures.
        /// </summary>
        public IPlanogramSettings Settings { get; set; }

        /// <summary>
        /// The current fixture in use by the task.
        /// </summary>
        public PlanogramFixture CurrentFixture { get; set; }

        /// <summary>
        /// The current fixture item in use by the task.
        /// </summary>
        public PlanogramFixtureItem CurrentFixtureItem { get; set; }

        /// <summary>
        /// The maximum height of all the products placed, used to determine the vertical 
        /// spacing between shelves.
        /// </summary>
        public Single MaxProductHeight { get; set; }

        /// <summary>
        /// The maximum width of all the components placed, determined by the maximum
        /// width of all the products placed on a single component.
        /// </summary>
        public Single MaxComponentWidth { get; set; }

        /// <summary>
        /// The sequence group currently in use by the task.
        /// </summary>
        public PlanogramSequenceGroup CurrentSequenceGroup { get; set; }

        /// <summary>
        /// Gets or sets the sequence type
        /// </summary>
        public SequenceType SequenceType
        {
            get { return _sequenceType; }
            set { _sequenceType = value; }
        }

        /// <summary>
        /// Gets or sets the sequence placement primary type
        /// </summary>
        public SequencePlacementPrimaryType SequencePlacementPrimaryType
        {
            get { return _sequencePlacementPrimaryType; }
            set { _sequencePlacementPrimaryType = value; }
        }

        /// <summary>
        /// Gets or sets the sequence placement secondary type
        /// </summary>
        public SequencePlacementSecondaryType SequencePlacementSecondaryType
        {
            get { return _sequencePlacementSecondaryType; }
            set { _sequencePlacementSecondaryType  = value; }
        }

        /// <summary>
        /// Gets or sets the sequence placement tertiary type
        /// </summary>
        public SequencePlacementTertiaryType SequencePlacementTertiaryType
        {
            get { return _sequencePlacementTertiaryType; }
            set { _sequencePlacementTertiaryType = value; }
        }

        /// <summary>
        /// Gets or sets the sequence type
        /// </summary>
        public UnplacedProductBehaviourType UnplacedProductBehaviourType
        {
            get { return _unplacedProductBehaviourType; }
            set { _unplacedProductBehaviourType = value; }
        }

        /// <summary>
        /// Holds block information indexed by colour
        /// </summary>
        public Dictionary<Int32, String> BlockingGroups
        {
            get { return _blockingGroups; }
        }

        /// <summary>
        /// Holds planogram products indexed by their GTIN
        /// </summary>
        public Dictionary<String, PlanogramProduct> PlanogramProducts
        {
            get { return _planogramProducts; }
        }

        /// <summary>
        /// Holds unplaced planogram products
        /// </summary>
        public List<PlanogramProduct> UnplacedPlanogramProducts
        {
            get { return _unplacedPlanogramProducts; }
        }

        /// <summary>
        /// Holds sequence products indexed by their GTIN
        /// </summary>
        public Dictionary<String, PlanogramSequenceGroupProduct> SequenceProducts
        {
            get { return _sequenceProducts; }
        }

        /// <summary>
        /// Holds all sub component placements created by the task
        /// </summary>
        public List<PlanogramSubComponentPlacement> SubComponentPlacements
        {
            get { return _subComponentPlacements; }
        }

        /// <summary>
        /// Holds all Car Park sub component placements created by the task
        /// </summary>
        public List<PlanogramSubComponentPlacement> CarParkSubComponentPlacements
        {
            get { return _carParkSubComponentPlacements; }
        }

        /// <summary>
        /// Gets or sets the car park component name
        /// </summary>
        public String CarParkComponentName
        {
            get { return _carParkComponentName; }
            set { _carParkComponentName = value; }
        }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox
        {
            get { return _addCarParkTextBox; }
            set { _addCarParkTextBox = value; }
        }

        /// <summary>
        /// Holds list of car park product references
        /// </summary>
        public List<PlanogramProduct> CarParkProducts
        {
            get { return _carParkProducts; }
        }

        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// The total width of all the fixtures in the plan.
        /// </summary>
        public Single TotalFixtureWidth { get; set; }

        /// <summary>
        /// A dictionary of all the widths of the car park components, keyed by <see cref="PlanogramComponent"/>, 
        /// as calculated by summing the width of all the positions on each component.
        /// </summary>
        public Dictionary<PlanogramComponent, Single> CarParkComponentWidthsByComponent { get; private set; }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }
        
        /// <summary>
        /// Adds the given <paramref name="subComponentPlacement"/> to either <see cref="CarParkSubComponentPlacements"/>
        /// or <see cref="SubComponentPlacements"/> depending on <paramref name="isCarPark"/>.
        /// </summary>
        /// <param name="subComponentPlacement"></param>
        /// <param name="isCarPark"></param>
        public void AddSubComponentPlacement(PlanogramSubComponentPlacement subComponentPlacement, Boolean isCarPark)
        {
            if (isCarPark)
            {
                CarParkSubComponentPlacements.Add(subComponentPlacement);
            }
            else
            {
                SubComponentPlacements.Add(subComponentPlacement);
            }
        }

        /// <summary>
        /// Adds a fixture to the plan, optionally adding a backboard component if <paramref name="addBackboard"/> is true.
        /// </summary>
        /// <param name="addBackboard"></param>
        public void AddFixture(Boolean addBackboard)
        {
            CurrentFixture = Planogram.Fixtures.Add(Settings);
            CurrentFixtureItem = Planogram.FixtureItems.Add(CurrentFixture);
            CurrentFixtureItem.X = Planogram.Fixtures.Count * Settings.FixtureWidth;

            if (!addBackboard) return;

            PlanogramFixtureComponent backboardFixtureComponent = CurrentFixture.Components.Add(PlanogramComponentType.Backboard, Settings);
            PlanogramComponent backboardComponent = backboardFixtureComponent.GetPlanogramComponent();
            PlanogramSubComponent backboardSubComponent = backboardComponent.SubComponents[0];

            // size the backboard
            backboardComponent.Width = CurrentFixture.Width;
            backboardComponent.Height = CurrentFixture.Height;
            backboardSubComponent.Width = CurrentFixture.Width;
            backboardSubComponent.Height = CurrentFixture.Height;

            // position the backboard
            backboardFixtureComponent.X = 0;
            backboardFixtureComponent.Y = 0;
            backboardFixtureComponent.Z = -backboardComponent.Depth;
        }

        #endregion
    }
}
