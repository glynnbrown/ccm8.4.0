﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30729 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Unplaced Products parameter.
    /// </summary>
    public enum UnplacedProductBehaviourType
    {
        AddToCarParkComponent = 0,
        DoNotAdd = 1
    }

    /// <summary>
    ///     Helpers for <see cref="UnplacedProductBehaviourType"/>.
    /// </summary>
    public static class UnplacedProductBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="UnplacedProductBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<UnplacedProductBehaviourType, String> FriendlyNames = new Dictionary
            <UnplacedProductBehaviourType, String>
        {
            {UnplacedProductBehaviourType.AddToCarParkComponent, Message.UnplacedProductBehaviourType_AddToCarParkComponent},
            {UnplacedProductBehaviourType.DoNotAdd, Message.UnplacedProductBehaviourType_DoNotAdd}
        };
    }
}