﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM801
// V8-30051 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1
{
    /// <summary>
    /// Denotes the possible values for the 
    /// sequence type
    /// </summary>
    public enum SequenceType
    {
        ProductColor = 0,
        BlockingGroupings = 1,
    }

    /// <summary>
    /// Contains helpers for the SequenceType enum.
    /// </summary>
    public static class SequenceTypeHelper
    {
        public static readonly Dictionary<SequenceType, String> FriendlyNames =
            new Dictionary<SequenceType, String>()
            {
                {SequenceType.ProductColor, Message.CreateSequenceTemplate_SequenceType_ProductColor },
                {SequenceType.BlockingGroupings, Message.CreateSequenceTemplate_SequenceType_BlockingGroups }, 
            };
    }
}