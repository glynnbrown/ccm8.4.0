﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-30729 : A.Kuszyk
//  Added friendly names.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1
{
    public enum SequencePlacementPrimaryType
    {
        LeftToRight = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.LeftToRight, // 0
        RightToLeft = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.RightToLeft, // 1
        BottomToTop = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.BottomToTop, // 2
        TopToBottom = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.TopToBottom, // 3
        BackToFront = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.BackToFront, // 4
        FrontToBack = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.FrontToBack, // 5
    }

    public static class SequencePlacementPrimaryTypeHelper
    {
        public static Dictionary<SequencePlacementPrimaryType, String> FriendlyNames = new Dictionary<SequencePlacementPrimaryType, String>()
        {
            {SequencePlacementPrimaryType.LeftToRight, Message.CreateSequenceTemplate_SequencePlacementType_LeftToRight},
            {SequencePlacementPrimaryType.RightToLeft, Message.CreateSequenceTemplate_SequencePlacementType_RightToLeft},
            {SequencePlacementPrimaryType.BottomToTop, Message.CreateSequenceTemplate_SequencePlacementType_BottomToTop},
            {SequencePlacementPrimaryType.TopToBottom, Message.CreateSequenceTemplate_SequencePlacementType_TopToBottom},
            {SequencePlacementPrimaryType.FrontToBack, Message.CreateSequenceTemplate_SequencePlacementType_FrontToBack},
            {SequencePlacementPrimaryType.BackToFront, Message.CreateSequenceTemplate_SequencePlacementType_BackToFront},
        };
    }
}