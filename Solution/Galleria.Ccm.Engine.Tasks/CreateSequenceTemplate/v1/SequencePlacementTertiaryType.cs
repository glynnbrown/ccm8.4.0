﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-30729 : A.Kuszyk
//  Added friendly names.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1
{
    public enum SequencePlacementTertiaryType
    {
        LeftToRight = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.LeftToRight, // 0
        RightToLeft = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.RightToLeft, // 1
        BottomToTop = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.BottomToTop, // 2
        TopToBottom = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.TopToBottom, // 3
        BackToFront = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.BackToFront, // 4
        FrontToBack = Galleria.Framework.Planograms.Model.PlanogramBlockingGroupPlacementType.FrontToBack, // 5
    }

    public static class SequencePlacementTertiaryTypeHelper
    {
        public static Dictionary<SequencePlacementTertiaryType, String> FriendlyNames = new Dictionary<SequencePlacementTertiaryType, String>()
        {
            {SequencePlacementTertiaryType.LeftToRight, Message.CreateSequenceTemplate_SequencePlacementType_LeftToRight},
            {SequencePlacementTertiaryType.RightToLeft, Message.CreateSequenceTemplate_SequencePlacementType_RightToLeft},
            {SequencePlacementTertiaryType.BottomToTop, Message.CreateSequenceTemplate_SequencePlacementType_BottomToTop},
            {SequencePlacementTertiaryType.TopToBottom, Message.CreateSequenceTemplate_SequencePlacementType_TopToBottom},
            {SequencePlacementTertiaryType.FrontToBack, Message.CreateSequenceTemplate_SequencePlacementType_FrontToBack},
            {SequencePlacementTertiaryType.BackToFront, Message.CreateSequenceTemplate_SequencePlacementType_BackToFront},
        };
    }
}