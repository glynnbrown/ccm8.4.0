﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25839 : L.Ineson
//  Created
// V8-26950 : A.Kuszyk
// Added Name parameter.
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Code Review
// V8-29911 : D.Pleasance
//  Amended GetConsumerDecisionTree to provide Consumer Decision Tree Name in evelog log if the item no longer exists.
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.GetConsumerDecisionTreeByName.v1
{
    /// <summary>
    /// Load a Consumer Decision Tree into the planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ConsumerDecisionTreeName = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetConsumerDecisionTreeByName_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.GetConsumerDecisionTreeByName_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // consumer decision tree name
            this.RegisterParameter(new TaskParameter(
                Parameter.ConsumerDecisionTreeName,
                Message.GetConsumerDecisionTreeByName_Parameter_ConsumerDecisionTreeName_Name,
                Message.GetConsumerDecisionTreeByName_Parameter_ConsumerDecisionTreeName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ConsumerDecisionTreeNameSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetConsumerDecisionTree(taskContext)) return;
                if (!LoadConsumerDecisionTree(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region ConsumerDecisionTreeName

                // validate that the consumer decision tree name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.ConsumerDecisionTreeName)) ||
                    (this.Parameters[Parameter.ConsumerDecisionTreeName].Value == null) ||
                    (this.Parameters[Parameter.ConsumerDecisionTreeName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetConsumerDecisionTreeByName_Parameter_ConsumerDecisionTreeName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the consumer decision tree name
                    context.ConsumerDecisionTreeName = Convert.ToString(this.Parameters[Parameter.ConsumerDecisionTreeName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.ConsumerDecisionTreeName)) || (String.IsNullOrWhiteSpace(context.ConsumerDecisionTreeName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetConsumerDecisionTreeByName_Parameter_ConsumerDecisionTreeName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Loads the consumer decision tree from the database
        /// </summary>
        private static Boolean GetConsumerDecisionTree(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to fetch the consumer decision tree by its name
                try
                {
                    context.ConsumerDecisionTree = ConsumerDecisionTree.FetchByEntityIdName(context.EntityId, context.ConsumerDecisionTreeName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetConsumerDecisionTreeByName_Parameter_ConsumerDecisionTreeName_Name, context.ConsumerDecisionTreeName);
                        return false;
                    }
                    throw ex;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Loads the consumer decision tree into the planogram
        /// </summary>
        private static Boolean LoadConsumerDecisionTree(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // replace the consumer decision tree within the planogram
                context.Planogram.ConsumerDecisionTree.ReplaceConsumerDecisionTree(context.ConsumerDecisionTree);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Logs that the task has completed successfully
        /// </summary>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // log that the cdt was successfully applied
                context.LogInformation(
                    PlanogramEventLogAffectedType.Planogram,
                    context.Planogram.Id,
                    EventLogEvent.PlanogramConsumerDecisionTreeAdded,
                    context.ConsumerDecisionTreeName);

                // build a lookup of product ids and codes within the planogram
                Dictionary<Object, PlanogramProduct> planogramProductIdLookup = new Dictionary<Object, PlanogramProduct>();
                foreach (PlanogramProduct product in context.Planogram.Products)
                    planogramProductIdLookup.Add(product.Id, product);

                // build a lookup of all placed products within the planogram
                List<PlanogramProduct> planogramPositionProducts = new List<PlanogramProduct>();
                foreach (PlanogramPosition position in context.Planogram.Positions)
                    planogramPositionProducts.Add(planogramProductIdLookup[position.PlanogramProductId]);

                // build a list of products within the cdt
                List<PlanogramProduct> cdtProducts = new List<PlanogramProduct>();
                foreach (Object productId in context.Planogram.ConsumerDecisionTree.GetAssignedPlanogramProducts())
                    cdtProducts.Add(planogramProductIdLookup[productId]);

                // build a list of placed and unplaced products within planogram
                // that are not contained within the cdt
                List<PlanogramProduct> planogramPlacedProductCodes = new List<PlanogramProduct>();
                List<PlanogramProduct> planogramUnplacedProductCodes = new List<PlanogramProduct>();
                foreach (PlanogramProduct product in context.Planogram.Products)
                {
                    if (!cdtProducts.Contains(product))
                    {
                        if (planogramPositionProducts.Contains(product))
                        {
                            planogramPlacedProductCodes.Add(product);
                        }
                        else
                        {
                            planogramUnplacedProductCodes.Add(product);
                        }
                    }
                }

                // log the number of placed products in the plan that are not in the cdt
                if (planogramPlacedProductCodes.Count > 0)
                {
                    // log the total as information
                    context.LogInformation(
                        PlanogramEventLogAffectedType.ConsumerDecisionTree,
                        context.Planogram.ConsumerDecisionTree.Id,
                        EventLogEvent.PlacedPlanogramProductsNotInCdt,
                        planogramPlacedProductCodes.Count);

                    // log each product that was placed
                    foreach (PlanogramProduct product in planogramPlacedProductCodes)
                        context.LogDebug(
                            PlanogramEventLogAffectedType.ConsumerDecisionTree,
                            context.Planogram.ConsumerDecisionTree.Id,
                            EventLogEvent.PlacedPlanogramProductNotInCdt,
                            product.Name,
                            product.Gtin);
                }

                // log the number of unplaced products in the plan that are not in the cdt
                if (planogramUnplacedProductCodes.Count > 0)
                {
                    // log the total as information
                    context.LogInformation(
                        PlanogramEventLogAffectedType.ConsumerDecisionTree,
                        context.Planogram.ConsumerDecisionTree.Id,
                        EventLogEvent.UnPlacedPlanogramProductsNotInCdt,
                        planogramUnplacedProductCodes.Count);

                    // log each product that was not placed
                    foreach (PlanogramProduct product in planogramUnplacedProductCodes)
                        context.LogDebug(
                            PlanogramEventLogAffectedType.ConsumerDecisionTree,
                            context.Planogram.ConsumerDecisionTree.Id,
                            EventLogEvent.UnPlacedPlanogramProductNotInCdt,
                            product.Name,
                            product.Gtin);
                }

                // return success
                return true;
            }
        }

        #endregion
    }
}
