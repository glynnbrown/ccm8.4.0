﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29520 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.GetConsumerDecisionTreeByName.v1
{
    /// <summary>
    /// Task context object used to pass parameters
    /// throughout the execution of the task
    /// </summary>
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        public String _consumerDecisionTreeName; // holds the assortment name
        public ConsumerDecisionTree _consumerDecisionTree; // holds the loaded assortment
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the assortment name
        /// </summary>
        public String ConsumerDecisionTreeName
        {
            get { return _consumerDecisionTreeName; }
            set { _consumerDecisionTreeName = value; }
        }

        /// <summary>
        /// Gets or sets the assortment
        /// </summary>
        public ConsumerDecisionTree ConsumerDecisionTree
        {
            get { return _consumerDecisionTree; }
            set { _consumerDecisionTree = value; }
        }
        #endregion
    }
}
