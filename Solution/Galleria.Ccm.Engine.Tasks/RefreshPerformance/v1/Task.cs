﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Performance;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.RefreshPerformance.v1
{
    /// <summary>
    /// Refreshes the performance data held by a planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Constants
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        private static Int32 _serverBusyMaxRetryCount = 5; // maximum retry count when the performance service responds that it is too busy
        private static Int32 _serverBusyWaitTime = 15000; // the number of milliseconds to wait before retrying a performance operation
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PerformanceSelectionName = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RefreshPerformance_Name; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.RefreshPerformance_Category; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// Indicates that this task has a pre step
        /// </summary>
        public override bool HasPreStep
        {
            get { return true; }
        }

        /// <summary>
        /// Indicates that this task has a post step
        /// </summary>
        public override bool HasPostStep
        {
            get { return true; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Performance Selection Name
            this.RegisterParameter(new TaskParameter(
                Parameter.PerformanceSelectionName,
                Message.RefreshPerformance_PerformanceSelectionName_Name,
                Message.RefreshPerformance_PerformanceSelectionName_Description,
                Message.RefreshPerformance_PerformanceSelectionName_Category,
                TaskParameterType.PerformanceSelectionName,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called before the task is executed
        /// </summary>
        public override void OnPreExecute(TaskContext context)
        {
            // initialize the connection to gfs
            InitializeGfsConnection(context.EntityId);

            // delete all the existing performance data
            // that has already been retrieved for this workpackage
            DeleteWorkpackagePerformanceData(context.Workpackage);

            // define a list that will hold all the performance
            // data that is a result of this step
            List<WorkpackagePerformanceDataDto> performanceData = new List<WorkpackagePerformanceDataDto>();

            // enumerate through all the performance selections
            // within the workpackage and their associated
            // planograms
            var performanceSelections = GetPerformanceSelectionNames(context.Workpackage, context.WorkflowTask);
            foreach (var performanceSelectionItem in performanceSelections)
            {
                // get the performance selection details
                String performanceSelectionName = performanceSelectionItem.Key;
                List<WorkpackagePlanogram> workpackagePlanograms = performanceSelectionItem.Value;

                // retrieve the performance selection
                // based upon its name
                PerformanceSelection performanceSelection = GetPerformanceSelection(performanceSelectionName);
                if (performanceSelection != null)
                {
                    // using the performance selection
                    // build a list of gfs timelines
                    // that we are interested in
                    List<Int64> timelineCodes = GetTimelineCodes(performanceSelection);
                    if (timelineCodes.Count > 0)
                    {
                        // get the product codes that the
                        // planograms in this workpackage
                        // and performance select cover
                        List<String> productCodes = GetProductCodes(workpackagePlanograms);
                        if (productCodes.Count > 0)
                        {
                            // get the location codes that the
                            // planograms in this workpackage
                            // and performance selection cover
                            List<String> locationCodes = GetLocationCodes(context.Workpackage.EntityId);
                            if (locationCodes.Count > 0)
                            {
                                // now fetch the performance data
                                GetPerformanceData(
                                    context.EntityId,
                                    context.Workpackage,
                                    performanceSelection,
                                    performanceData,
                                    timelineCodes,
                                    productCodes,
                                    locationCodes);
                            }
                        }
                    }
                }
            }

            // save the performance data
            SavePerformanceData(context.Workpackage, performanceData);
        }

        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(TaskContext context)
        {
            // parameters
            String performanceSelectionName = Convert.ToString(this.Parameters[Parameter.PerformanceSelectionName].Value);

            // parameter validation
            if (String.IsNullOrEmpty(performanceSelectionName)) return;
        }

        /// <summary>
        /// Called after the task is executed
        /// </summary>
        public override void OnPostExecute(TaskContext context)
        {
            DeleteWorkpackagePerformanceData(context.Workpackage);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Initializes the connection details so that
        /// we can call gfs web services
        /// </summary>
        private static void InitializeGfsConnection(Int32 entityId)
        {
            Entity entity = Entity.FetchById(entityId);
            String connectionPath = entity.SystemSettings.FoundationServicesEndpoint;
            ApplicationContext.GlobalContext[Constants.FoundationServicesEndpoint] =
                String.Format("{0}/Services", connectionPath.TrimEnd('/'));
        }

        /// <summary>
        /// Deletes all existing performance data
        /// for the provided workpackage
        /// </summary>
        private static void DeleteWorkpackagePerformanceData(Workpackage workpackage)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackagePerformanceDal dal = dalContext.GetDal<IWorkpackagePerformanceDal>())
                {
                    dal.DeleteByWorkpackageId(workpackage.Id);
                }
            }
        }

        /// <summary>
        /// Returns a dictionary of location codes to ids
        /// </summary>
        private static Dictionary<String, Int16> GetLocationLookup(Int32 entityId)
        {
            Dictionary<String, Int16> locationLookup = new Dictionary<String, Int16>();
            LocationInfoList locations = LocationInfoList.FetchByEntityIdIncludingDeleted(entityId);
            foreach (LocationInfo location in locations)
            {
                locationLookup.Add(location.Code, location.Id);
            }
            return locationLookup;
        }

        /// <summary>
        /// Returns a dictionary of product codes to ids
        /// </summary>
        private static Dictionary<String, Int32> GetProductLookup(Int32 entityId)
        {
            Dictionary<String, Int32> productLookup = new Dictionary<String, Int32>();
            ProductInfoList products = ProductInfoList.FetchByEntityIdIncludingDeleted(entityId);
            foreach (ProductInfo product in products)
            {
                productLookup.Add(product.Gtin, product.Id);
            }
            return productLookup;
        }

        /// <summary>
        /// Returns all performance selections that are
        /// being used within a workpackage and the planograms
        /// that are using those performance selections
        /// </summary>
        private static Dictionary<String, List<WorkpackagePlanogram>> GetPerformanceSelectionNames(Workpackage workpackage, WorkflowTask workflowTask)
        {
            // build a list of performance selection names
            // that are within the workpackage
            Dictionary<String, List<WorkpackagePlanogram>> performanceSelections = new Dictionary<String, List<WorkpackagePlanogram>>();

            // locate the performance selection name
            // parameter within the workflow task
            WorkflowTaskParameter taskParameter = workflowTask.Parameters.FirstOrDefault(i => i.Details.Id == (Int32)Parameter.PerformanceSelectionName);

            // now iterate through all the planograms
            // within the workpackage and build a list
            // of all the unique performance selections
            WorkpackagePlanogramList workpackagePlanograms = workpackage.Planograms;
            foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
            {
                foreach (WorkpackagePlanogramParameter planogramParameter in workpackagePlanogram.Parameters)
                {
                    if (planogramParameter.WorkflowTaskParameterId == taskParameter.Id)
                    {
                        // add the performance selection name to the collection
                        String performanceSelectionName = Convert.ToString(planogramParameter.Value);
                        if (!performanceSelections.ContainsKey(performanceSelectionName))
                        {
                            performanceSelections.Add(performanceSelectionName, new List<WorkpackagePlanogram>());
                        }

                        // add the planogram to the performance selection name
                        performanceSelections[performanceSelectionName].Add(workpackagePlanogram);
                    }
                }
            }

            // return the performance selection names
            return performanceSelections;
        }

        /// <summary>
        /// Retrieves the specified performance selection
        /// based on the provided name
        /// </summary>
        private static PerformanceSelection GetPerformanceSelection(String name)
        {
            try
            {
                return PerformanceSelection.FetchByName(name);
            }
            catch (DataPortalException ex)
            {
                Exception baseException = ex.GetBaseException();
                if (baseException is DtoDoesNotExistException)
                {
                    return null;
                }
                throw;
            }
        }

        /// <summary>
        /// Returns the timlines that are part
        /// of the specified performance selection
        /// </summary>
        private static List<Int64> GetTimelineCodes(PerformanceSelection performanceSelection)
        {
            return performanceSelection.GetTimelineCodes().ToList();
        }

        /// <summary>
        /// Returns the stores codes that are
        /// covered by the list of planograms
        /// </summary>
        /// <remarks>
        /// NF - TODO - At present, CCM does not hold which planograms
        /// are allocated to each store, so for now, we fetch all store codes
        /// </remarks>
        private static List<String> GetLocationCodes(Int32 entityId)
        {
            return LocationInfoList.FetchByEntityId(entityId).Select(l => l.Code).ToList();
        }

        /// <summary>
        /// Returns all product codes that are
        /// covered by the list of planograms
        /// </summary>
        private static List<String> GetProductCodes(List<WorkpackagePlanogram> workpackagePlanograms)
        {
            // build a list of all product codes
            List<String> productCodes = new List<String>();

            // enumerate through each planogram in turn
            foreach (WorkpackagePlanogram workpackagePlanogram in workpackagePlanograms)
            {
                // attempt to lock the package
                // NF - TODO - What if the package cannot be locked?
                Package.LockPackageById(workpackagePlanogram.PlanogramId);

                // get the source package
                Package package = Package.FetchById(workpackagePlanogram.PlanogramId);

                // enumerate the products within the package planogram
                foreach (PlanogramProduct product in package.Planograms[0].Products)
                {
                    if (!productCodes.Contains(product.Gtin))
                        productCodes.Add(product.Gtin);
                }

                // unlock the package
                Package.UnlockPackageById(workpackagePlanogram.PlanogramId);
            }

            // return all the product codes
            return productCodes;
        }

        /// <summary>
        /// Returns all the performance data from gfs
        /// for the specified performance selection,
        /// timelines, products, and codes
        /// </summary>
        /// <remarks>
        /// Note that this method goes direct to the gfs web services
        /// rather than through the GFS model. As performance data can
        /// be huge, we fetch the data directly rather than decanting 
        /// this information into a new model, which effectively doubles
        /// the amount of required memory
        /// </remarks>
        private static void GetPerformanceData(
            Int32 entityId,
            Workpackage workpackage,
            PerformanceSelection performanceSelection,
            List<WorkpackagePerformanceDataDto> performanceData,
            List<Int64> timelineCodes,
            List<String> productCodes,
            List<String> locationCodes)
        {
            // get the gfs entity
            GFSModel.Entity gfsEntity = GFSModel.Entity.FetchByCcmEntityId(entityId);

            // get the gfs performance source
            GFSModel.PerformanceSource gfsPerformanceSource = performanceSelection.GetPerformanceSource(gfsEntity);

            // now fetch the performance data
            using (FoundationServiceClient proxy = new FoundationServiceClient())
            {
                GetPerformanceData(
                    proxy,
                    entityId,
                    gfsEntity.Name,
                    workpackage.Id,
                    performanceSelection.Id,
                    gfsPerformanceSource.Name,
                    performanceData,
                    timelineCodes,
                    productCodes,
                    locationCodes.ToList(), // we need to copy the list
                    locationCodes.Count);
            }
        }

        /// <summary>
        /// This method recusively requests performance data from
        /// gfs until the request batch size is below the limit that
        /// is actually enforced by gfs
        /// </summary>
        private static void GetPerformanceData(
            FoundationServiceClient proxy,
            Int32 entityId,
            String entityName,
            Int32 workpackageId,
            Int32 performanceSelectionId,
            String performanceSourceName,
            List<WorkpackagePerformanceDataDto> performanceData,
            List<Int64> timelineCodes,
            List<String> productCodes,
            List<String> remainingLocationCodes,
            Int32 locationBatchSize)
        {
            // set the retry counts
            Int32 serverBusyRetryCount = 0;

            // recurse until all locations have been requested
            while (remainingLocationCodes.Count > 0)
            {
                // build a list of locations based
                // on the current batch size
                List<String> locationCodes = new List<String>();
                Int32 batchSize = Math.Min(locationBatchSize, remainingLocationCodes.Count);
                for (Int32 i = 0; i < batchSize; i++)
                {
                    locationCodes.Add(remainingLocationCodes[i]);
                }

                // we now have all the details we need to
                // perform a request for performance data
                // from GFS
                try
                {
                    // make the call to GFS for the performance data
                    var response = proxy.PerformanceServiceClient.GetPerformanceDataByProductGTIN(
                        new GetPerformanceDataByProductGTINRequest(
                            entityName,
                            performanceSourceName,
                            true,
                            true,
                            false,
                            false,
                            productCodes,
                            locationCodes,
                            timelineCodes));

                    // if we recieved some performance data
                    if (response.PerformanceData.Count > 0)
                    {
                        // build some lookups for locations and products
                        Dictionary<String, Int16> locationLookup = GetLocationLookup(entityId);
                        Dictionary<String, Int32> productLookup = GetProductLookup(entityId);

                        // add the response data to the list of performance data
                        foreach (var item in response.PerformanceData)
                        {
                            Int16 locationId;
                            if (locationLookup.TryGetValue(item.LocationCode, out locationId))
                            {
                                Int32 productId;
                                if (productLookup.TryGetValue(item.ProductGTIN, out productId))
                                {
                                    performanceData.Add(new WorkpackagePerformanceDataDto()
                                    {
                                        WorkpackageId = workpackageId,
                                        PerformanceSelectionId = performanceSelectionId,
                                        LocationId = locationId,
                                        ProductId = productId,
                                        P1 = item.P1,
                                        P2 = item.P2,
                                        P3 = item.P3,
                                        P4 = item.P4,
                                        P5 = item.P5,
                                        P6 = item.P6,
                                        P7 = item.P7,
                                        P8 = item.P8,
                                        P9 = item.P9,
                                        P10 = item.P10,
                                        P11 = item.P11,
                                        P12 = item.P12,
                                        P13 = item.P13,
                                        P14 = item.P14,
                                        P15 = item.P15,
                                        P16 = item.P16,
                                        P17 = item.P17,
                                        P18 = item.P18,
                                        P19 = item.P19,
                                        P20 = item.P20
                                    });
                                }
                            }
                        }
                    }

                    // since we have successfully retrieved a batch
                    // of performance data, we remove the locations
                    // from the remaining location list
                    foreach (String locationCode in locationCodes)
                    {
                        remainingLocationCodes.Remove(locationCode);
                    }
                }
                catch (FaultException<PerformanceServiceRowCountFault>)
                {
                    // the number of request performance items is too
                    // great, therefore we halve the location dimension
                    locationBatchSize = Math.Max(locationBatchSize / 2, 1);

                    // and try again
                    GetPerformanceData(
                        proxy,
                        entityId,
                        entityName,
                        workpackageId,
                        performanceSelectionId,
                        performanceSourceName,
                        performanceData,
                        timelineCodes,
                        productCodes,
                        remainingLocationCodes,
                        locationBatchSize);
                }
                catch (FaultException<PerformanceServiceServerTooBusyFault>)
                {
                    // the server is too busy at the moment
                    // so try again unless we have hit our
                    // max retry count
                    if (serverBusyRetryCount >= _serverBusyMaxRetryCount) throw;

                    // increase the retry count
                    serverBusyRetryCount++;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Saves the performance data to the data store
        /// </summary>
        private static void SavePerformanceData(
            Workpackage workpackage,
            List<WorkpackagePerformanceDataDto> performanceData)
        {
            // ensure we have something to do
            if (performanceData.Count == 0) return;

            // upsert the performance data
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // insert the performance dto
                using (IWorkpackagePerformanceDal dal = dalContext.GetDal<IWorkpackagePerformanceDal>())
                {
                    dal.Insert(new WorkpackagePerformanceDto()
                    {
                        WorkpackageId = workpackage.Id
                    });
                }

                // now upload the performance data
                using (IWorkpackagePerformanceDataDal dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                {
                    dal.BulkInsert(workpackage.Id, performanceData);
                }
            }
        }

        #endregion
    }
}
