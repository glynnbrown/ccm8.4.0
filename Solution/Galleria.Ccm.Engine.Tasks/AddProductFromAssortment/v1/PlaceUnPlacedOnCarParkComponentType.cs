﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30907 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1
{
    public enum PlaceUnPlacedOnCarParkComponentType
    {
        /// <summary>
        /// Indicates that unplaced products should be placed on the car park component.
        /// </summary>
        Yes = 1,
        /// <summary>
        /// Indicates that unplaced products shouldn't be placed on the car park component.
        /// </summary>
        No = 0,
    }

    public static class PlaceUnPlacedOnCarParkComponentTypeHelper
    {
        public static Dictionary<PlaceUnPlacedOnCarParkComponentType, String> FriendlyNames =
            new Dictionary<PlaceUnPlacedOnCarParkComponentType, String>()
            {
                { PlaceUnPlacedOnCarParkComponentType.Yes, Message.Generic_Yes },
                { PlaceUnPlacedOnCarParkComponentType.No, Message.Generic_No },
            };
    }
}
