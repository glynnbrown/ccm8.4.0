﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-25839 : L.Ineson
//  Created
// V8-27687 : A.Kuszyk
//  Implemented OnExecute and changed parameters.
// V8-27765 : A.Kuszyk
//  Added Logging methods.
// V8-27830 : A.Kuszyk
//  Ensured that facings are always at least one, even when the assortment facing is zero.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM804
// V8-29373 : N.Foster
//  Car park shelf changes
#endregion
#region Version History: CCM810
// V8-29921 : A.Silva
//  Added logging for already present products that will not be added, products added and products placed on the car park shelf.
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
// V8-30144 : D.Pleasance
//  Amended so that MerchandisingGroups are obtained on the task context and disposed when the task is completed. This is to ensure that positions 
//  are not removed when previously looking up the car park shelf. Previously the car park shelf was found, then dispose was called immediately 
//  after which resulting in existing car park placement positions being removed.
#endregion
#region Version History: CCM820
// V8-30710 : M.Brumby
//  Make missing assortment messaging consistent across tasks.
// V8-30907 : A.Kuszyk
//  Added Place unplaced products parameter.
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
// V8-30907 : D.Pleasance
//  Amended PlaceUnPlacedOnCarParkComponentType default to No. Also amended so that if this setting is No and you are to add to a carpark 
//  then products that are to be added and are missing a product position will be added to the car park component.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1
{
    /// <summary>
    /// Add Products from the plan assortment
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version

        private readonly Byte _majorVersion = 1; // holds the tasks major version number
        // Minor version incremented to 1 for addition of place unplaced products parameter.
        private readonly Byte _minorVersion = 1; // holds the tasks minor version number

        #endregion

        #region Parameter

        private enum Parameter
        {
            ProductPlacement = 0,
            AddToCarParkComponent = 1,
            CarParkComponentName = 2,
            PlaceUnPlacedOnCarParkComponent = 3,
            AddCarParkTextBox = 4,
        }

        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name { get { return Message.AddProductFromAssortment_Task_Name; } }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description { get { return Message.AddProductFromAssortment_Task_Description; } }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category { get { return Message.TaskCategory_MerchandisePlanogram; } }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion { get { return _majorVersion; } }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion { get { return _minorVersion; } }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // placement type
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.ProductPlacement,
                    name: Message.AddProductFromAssortment_Parameter_ProductPlacement_Name,
                    description: Message.AddProductFromAssortment_Parameter_ProductPlacement_Description,
                    category: Message.ParameterCategory_Default,
                    enumType: typeof (PlacementType),
                    defaultValue: PlacementType.AssortmentFacings,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false));

            // add to car park component?
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.AddToCarParkComponent,
                    name: Message.AddProductFromAssortment_Parameter_AddToCarParkComponent_Name,
                    description: Message.AddProductFromAssortment_Parameter_AddToCarParkComponent_Description,
                    category: Message.ParameterCategory_Default,
                    enumType: typeof (AddToCarParkType),
                    defaultValue: AddToCarParkType.Yes,
                    isHiddenByDefault: false,
                    isReadOnlyByDefault: false));

            // car park component name
            RegisterParameter(
                new TaskParameter(
                    id: Parameter.CarParkComponentName,
                    name: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name,
                    description: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Description,
                    category: Message.ParameterCategory_Default,
                    parameterType: TaskParameterType.String,
                    defaultValue: Message.AddProductFromAssortment_Parameter_CarParkComponentName_Default,
                    isHiddenByDefault: true,
                    isReadOnlyByDefault: true,
                    parentId: null,
                    isRequired: null,
                    isNullAllowed: null,
                    isValid: () => IsCarParkNameValid()));

            //PlaceUnPlacedOnCarParkComponent
            RegisterParameter(
                new TaskParameter(
                    Parameter.PlaceUnPlacedOnCarParkComponent,
                    Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                    Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Description,
                    Message.ParameterCategory_Default,
                    typeof(PlaceUnPlacedOnCarParkComponentType),
                    PlaceUnPlacedOnCarParkComponentType.No,
                    false,
                    false,
                    isRequired: () => IsPlaceUnPlacedOnCarParkComponentRequired()));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.AddProducts_Parameter_AddCarParkTextBox_Name,
                Message.AddProducts_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }

        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!PopulateLookups(taskContext)) return;
                if (!AddProductsToPlanogram(taskContext)) return;
                if (!AddPositionsToCarParkComponent(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates whether the place unplaced products on car park component parameter is required.
        /// </summary>
        /// <returns></returns>
        private String IsPlaceUnPlacedOnCarParkComponentRequired()
        {
            // Get Add to car park component parameter value.
            if (Parameters.Count <= (Int32)Parameter.AddToCarParkComponent ||
                Parameters[(Int32)Parameter.AddToCarParkComponent] == null ||
                Parameters[(Int32)Parameter.AddToCarParkComponent].Value == null ||
                Parameters[(Int32)Parameter.AddToCarParkComponent].Value.Value1 == null)
            {
                return Message.AddProducts_AddToCarParkParameterCannotBeFound;
            }

            AddToCarParkType addToCarPark;
            try
            {
                addToCarPark = (AddToCarParkType)Parameters[(Int32)Parameter.AddToCarParkComponent].Value.Value1;
            }
            catch (InvalidCastException)
            {
                return Message.AddProducts_AddToCarParkParameterCannotBeFound;
            }

            if (addToCarPark == AddToCarParkType.Yes)
            {
                return null; // This parameter is required
            }
            else
            {
                return Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_NotRequired;
            }
        }

        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region Product Placement Type

                // validate the product placement type
                if ((!Parameters.Contains((Int32)Parameter.ProductPlacement)) ||
                    (Parameters[Parameter.ProductPlacement].Value == null) ||
                    (Parameters[Parameter.ProductPlacement].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddProductFromAssortment_Parameter_ProductPlacement_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.ProductPlacement = (PlacementType)Parameters[Parameter.ProductPlacement].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddProductFromAssortment_Parameter_ProductPlacement_Name,
                                         Parameters[Parameter.ProductPlacement].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Add to Car Park Component

                // validate the add to car park shelf
                if ((!Parameters.Contains((Int32)Parameter.AddToCarParkComponent)) ||
                    (Parameters[Parameter.AddToCarParkComponent].Value == null) ||
                    (Parameters[Parameter.AddToCarParkComponent].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddProductFromAssortment_Parameter_AddToCarParkComponent_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        context.AddToCarParkComponent = (AddToCarParkType)Parameters[Parameter.AddToCarParkComponent].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddProductFromAssortment_Parameter_AddToCarParkComponent_Name,
                                         Parameters[Parameter.AddToCarParkComponent].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) ||
                        (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name,
                                         Message.Error_NoValue);
                        success = false;
                    }
                }


                #endregion

                #region Place Unplaced on Car Park Component
                if (Parameters.Count <= (Int32)Parameter.PlaceUnPlacedOnCarParkComponent ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent] == null ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value == null ||
                    Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1 == null)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        PlaceUnPlacedOnCarParkComponentType placeUnplaced =
                            (PlaceUnPlacedOnCarParkComponentType)Convert.ToByte(Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1);
                        context.PlaceUnplacedProductsOnCarParkComponent = placeUnplaced == PlaceUnPlacedOnCarParkComponentType.Yes;
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.AddProducts_PlaceUnplacedProductsOnCarParkComponent_Name,
                            Parameters[(Int32)Parameter.PlaceUnPlacedOnCarParkComponent].Value.Value1);
                        success = false;
                    }
                }
                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProducts_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.AddProducts_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if(this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null 
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName) 
                || this.Parameters[Parameter.CarParkComponentName].Value == null 
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                 return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Validates the planogram details
        /// </summary>
        private Boolean ValidatePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify we have an assortment
                if ((context.Planogram.Assortment == null) ||
                    (context.Planogram.Assortment.Products == null) ||
                    ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                {
                    context.LogWarning(EventLogEvent.PlanogramNoAssortmentPresent);
                    return false;
                }

                // return succes
                return true;
            }
        }

        /// <summary>
        /// Populates lookup lists for the rest of the process
        /// </summary>
        private Boolean PopulateLookups(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of all planogram products indexed by id and code
                foreach (PlanogramProduct product in context.Planogram.Products)
                {
                    if (!context.PlanogramProductIdLookup.ContainsKey(product.Id))
                        context.PlanogramProductIdLookup.Add(product.Id, product);
                    if (!context.PlanogramProductCodeLookup.ContainsKey(product.Gtin))
                        context.PlanogramProductCodeLookup.Add(product.Gtin, product);
                }

                // build a list of all planogram positions indexed by product id and code
                foreach (PlanogramPosition position in context.Planogram.Positions)
                {
                    if (!context.PlanogramPositionProductIdLookup.ContainsKey(position.PlanogramProductId))
                        context.PlanogramPositionProductIdLookup.Add(position.PlanogramProductId, position);
                    if (!context.PlanogramPositionProductCodeLookup.ContainsKey(context.PlanogramProductIdLookup[position.PlanogramProductId].Gtin))
                        context.PlanogramPositionProductCodeLookup.Add(context.PlanogramProductIdLookup[position.PlanogramProductId].Gtin, position);
                }

                // build a list of all assortment products that need to be
                // added to the planogram and have a position created
                var assortmentProducts = context.Planogram.Assortment.Products.Where(p => p.IsRanged).OrderBy(p => p.Rank).ThenBy(p => p.Gtin);
                foreach (PlanogramAssortmentProduct product in assortmentProducts)
                {
                    if ((!context.PlanogramProductCodeLookup.ContainsKey(product.Gtin)) &&
                        (!context.AssortmentProductsToAdd.ContainsKey(product.Gtin)))
                    {
                        context.AssortmentProductsToAdd.Add(product.Gtin, product);
                    }

                    if ((!context.PlanogramPositionProductCodeLookup.ContainsKey(product.Gtin)) &&
                        (!context.AssortmentPositionsToAdd.ContainsKey(product.Gtin)))
                    {
                        context.AssortmentPositionsToAdd.Add(product.Gtin, product);
                    }
                }

                //  Log number of products that already are present and will not be added.
                context.LogInformation(EventLogEvent.PlanogramProductsExistInPlanogram, assortmentProducts.Count(product => context.PlanogramProductCodeLookup.ContainsKey(product.Gtin)));

                // return success
                return true;
            }
        }

        /// <summary>
        /// Adds products from the assortment to the planogram
        /// product list if they do not already exist
        /// </summary>
        private Boolean AddProductsToPlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if all products already exist in the planogram
                // then there is nothing to do
                Int32 productCount = context.AssortmentProductsToAdd.Count;
                if (productCount == 0) return true;

                // add all ranged assortment products to the planogram
                // that do not already exist within the planogram
                foreach (Product product in ProductList.FetchByEntityIdProductGtins(context.EntityId, context.AssortmentProductsToAdd.Keys))
                {
                    // create a new planogram product
                    PlanogramProduct planogramProduct = PlanogramProduct.NewPlanogramProduct(product);

                    // add the planogram product to the planogram
                    context.Planogram.Products.Add(planogramProduct);

                    // add the planogram product to the lookup lists
                    context.PlanogramProductIdLookup.Add(planogramProduct.Id, planogramProduct);
                    context.PlanogramProductCodeLookup.Add(planogramProduct.Gtin, planogramProduct);
                }
                
                //  Log number of products that were added.
                context.LogInformation(EventLogEvent.PlanogramProductsAdded, productCount);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Adds positions to this car park component
        /// for all assortment products, if required
        /// </summary>
        private Boolean AddPositionsToCarParkComponent(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are not adding to a car park shelf
                // then there is nothing for use to do
                if (context.AddToCarParkComponent == AddToCarParkType.No) return true;

                // Get the products to add. If we are placing unplaced products, get the products that should have 
                // have been added that don't have positions as well as all the positions we previously identified as 
                // needing positions.
                IEnumerable<PlanogramProduct> productsToAdd;
                if(context.PlaceUnplacedProductsOnCarParkComponent)
                {
                    IEnumerable<String> assortmentProductGtins = context.Planogram.Assortment.Products.Select(ap => ap.Gtin).ToList();
                    productsToAdd = context.Planogram.Products
                        .Where(p => assortmentProductGtins.Contains(p.Gtin) && !p.GetPlanogramPositions().Any())
                        .Union(context.AssortmentPositionsToAdd.Keys.Select(gtin => context.PlanogramProductCodeLookup[gtin]))
                        .Distinct()
                        .ToList();
                }
                else
                {
                    // If we're not placing unplaced products, just use the products we previously identified.
                    productsToAdd = context.AssortmentPositionsToAdd.Keys
                        .Select(gtin => context.PlanogramProductCodeLookup[gtin])
                        .ToList();
                }

                // If there are no products to add, we can just return.
                Int32 positionsCount = productsToAdd.Count();
                if (positionsCount == 0) return true;

                // If we need to add positions, add the car park shelf.
                context.MerchandisingGroups = context.Planogram.GetMerchandisingGroups();
                context.CarParkMerchandisingGroup = context.GetCarParkMerchandisingGroup(context.CarParkComponentName, context.MerchandisingGroups, context.AddCarParkTextBox);

                // create a new positions in the car park merchandising group
                // for each assortment product that is ranged and does not already
                // have a position on the planogram
                foreach (PlanogramProduct product in productsToAdd)
                {
                    // insert the product into the merchandising group
                    // which will create us a position placement
                    PlanogramPosition position = context.CarParkMerchandisingGroup.InsertPositionPlacement(product).Position;
                    
                    // determine the number of facings wide
                    // to place the assortment product
                    Byte facingsWide = 1;
                    PlanogramAssortmentProduct assortmentProduct = context.Planogram.Assortment.Products
                        .FirstOrDefault(p => p.Gtin.Equals(product.Gtin));
                    if (assortmentProduct != null &&
                        (context.ProductPlacement == PlacementType.AssortmentFacings) &&
                        (assortmentProduct.Facings > 1))
                    {
                        facingsWide = assortmentProduct.Facings;
                    }

                    // set the position details
                    position.FacingsWide = facingsWide;
                }

                // finally, process the merchandising group
                context.CarParkMerchandisingGroup.Process();
                context.CarParkMerchandisingGroup.ApplyEdit();

                //  Log number of products placed on the Car Park Shelf.
                context.LogInformation(EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf, positionsCount);

                // return success
                return true;
            }
        }

        
        #endregion
    }
}