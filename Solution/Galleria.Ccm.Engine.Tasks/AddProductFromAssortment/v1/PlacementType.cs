﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27687 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1
{
    /// <summary>
    /// Denotes the possible values for the Placement Type parameter.
    /// </summary>
    public enum PlacementType
    {
        SingleFacing,
        AssortmentFacings
    }

    /// <summary>
    /// Contains helpers for the PlacementType enum.
    /// </summary>
    public static class PlacementTypeHelper
    {
        public static readonly Dictionary<PlacementType, String> FriendlyNames =
            new Dictionary<PlacementType, String>()
            {
                {PlacementType.SingleFacing, Message.PlacementType_SingleFacing },
                {PlacementType.AssortmentFacings, Message.PlacementType_AssortmentFacings }
            };
    }
}
