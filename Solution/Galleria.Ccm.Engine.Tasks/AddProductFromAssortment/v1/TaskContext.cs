﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM804
// V8-29373 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-30144 : D.Pleasance
//  Added MerchandisingGroups
#endregion
#region Version History : CCM820
// V8-30907 : A.Kuszyk
//  Added Place unplaced products property.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private PlacementType _productPlacement; // holds the placement type
        private AddToCarParkType _addToCarParkComponent; // holds the add to car park type
        private String _carParkComponentName; // holds the car park component name
        private PlanogramMerchandisingGroup _carParkMerchandisingGroup; // holds the car park merchandising group
        private Dictionary<Object, PlanogramProduct> _planogramProductIdLookup = new Dictionary<Object, PlanogramProduct>();
        private Dictionary<String, PlanogramProduct> _planogramProductCodeLookup = new Dictionary<String, PlanogramProduct>();
        private Dictionary<Object, PlanogramPosition> _planogramPositionProductIdLookup = new Dictionary<Object, PlanogramPosition>();
        private Dictionary<String, PlanogramPosition> _planogramPositionProductCodeLookup = new Dictionary<String, PlanogramPosition>();
        private Dictionary<String, PlanogramAssortmentProduct> _assortmentProductsToAdd = new Dictionary<String, PlanogramAssortmentProduct>();
        private Dictionary<String, PlanogramAssortmentProduct> _assortmentPositionsToAdd = new Dictionary<String, PlanogramAssortmentProduct>();
        private Boolean _addCarParkTextBox;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the product placement type
        /// </summary>
        public PlacementType ProductPlacement
        {
            get { return _productPlacement; }
            set { _productPlacement = value; }
        }

        /// <summary>
        /// Gets or sets the add to car park component
        /// </summary>
        public AddToCarParkType AddToCarParkComponent
        {
            get { return _addToCarParkComponent; }
            set { _addToCarParkComponent = value; }
        }

        /// <summary>
        /// Gets or sets the car park component name
        /// </summary>
        public String CarParkComponentName
        {
            get { return _carParkComponentName; }
            set { _carParkComponentName = value; }
        }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox
        {
            get { return _addCarParkTextBox; }
            set { _addCarParkTextBox = value; }
        }


        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        /// Gets or sets the car park merchandising group
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup
        {
            get { return _carParkMerchandisingGroup; }
            set { _carParkMerchandisingGroup = value; }
        }

        /// <summary>
        /// Returns a dictionary of all planogram products indexed by id
        /// </summary>
        public Dictionary<Object, PlanogramProduct> PlanogramProductIdLookup
        {
            get { return _planogramProductIdLookup; }
        }

        /// <summary>
        /// Returns a dictionary of all planogram products index by code
        /// </summary>
        public Dictionary<String, PlanogramProduct> PlanogramProductCodeLookup
        {
            get { return _planogramProductCodeLookup; }
        }

        /// <summary>
        /// Returns a dictionary of all planogram positions indexed by product id
        /// </summary>
        public Dictionary<Object, PlanogramPosition> PlanogramPositionProductIdLookup
        {
            get { return _planogramPositionProductIdLookup; }
        }

        /// <summary>
        /// Returns a dictionary of all planogram positions indexed by product code
        /// </summary>
        public Dictionary<String, PlanogramPosition> PlanogramPositionProductCodeLookup
        {
            get { return _planogramPositionProductCodeLookup; }
        }

        /// <summary>
        /// Returns a dictionary of all assortment products
        /// that require adding to the planogram
        /// indexed by product code
        /// </summary>
        public Dictionary<String, PlanogramAssortmentProduct> AssortmentProductsToAdd
        {
            get { return _assortmentProductsToAdd; }
        }

        /// <summary>
        /// Returns a dictionary of all assortment products
        /// that require a position adding to the planogram
        /// indexed by product code
        /// </summary>
        public Dictionary<String, PlanogramAssortmentProduct> AssortmentPositionsToAdd
        {
            get { return _assortmentPositionsToAdd; }
        }

        /// <summary>
        /// Indiciates whether or not products already on the plan that aren't placed should be added to the car park component.
        /// </summary>
        public Boolean PlaceUnplacedProductsOnCarParkComponent { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }

        #endregion
    }
}
