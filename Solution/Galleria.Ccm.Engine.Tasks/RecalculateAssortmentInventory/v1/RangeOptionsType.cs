﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-29809 : N.Foster
//  Created.
#endregion

#region Version History : CCM 803
// V8-29137 : A.Kuszyk
//  Changed FriendlyName to FriendlyNames.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1
{
    public enum RangeOptionsType : int
    {
        /// <summary>
        /// Indicates that only the products in the assortment will be re-calculated.
        /// </summary>
        AssortmentProductsOnly = 0,
        /// <summary>
        /// Indicates that all the products in the planogram will be added to the assortment (if they don't already
        /// exist) and then all the products in the assortment will be re-calculated.
        /// </summary>
        AssortmentAndPlanogramProducts = 1
    }

    public static class RangeOptionsTypeHelper
    {
        public static readonly Dictionary<RangeOptionsType, String> FriendlyNames =
            new Dictionary<RangeOptionsType, String>()
            {
                { RangeOptionsType.AssortmentProductsOnly, Message.RangeOptionsType_AssortmentProductsOnly},
                { RangeOptionsType.AssortmentAndPlanogramProducts, Message.RangeOptionsType_AssortmentAndPlanogramProducts}
            };
    }
}
