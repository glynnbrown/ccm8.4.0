﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27949 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1
{
    public enum CalculationOptionsType : int
    {
        RankAndUnits = 0,
        RankOnly = 1,
        UnitsOnly = 2
    }

    public static class CalculationOptionsTypeHelper
    {
        public static readonly Dictionary<CalculationOptionsType, String> FriendlyNames =
            new Dictionary<CalculationOptionsType, String>()
            {
                {CalculationOptionsType.RankAndUnits, Message.CalculationOptionsType_RankAndUnits },
                {CalculationOptionsType.RankOnly, Message.CalculationOptionsType_RankOnly },
                {CalculationOptionsType.UnitsOnly, Message.CalculationOptionsType_UnitsOnly }
            };
    }
}
