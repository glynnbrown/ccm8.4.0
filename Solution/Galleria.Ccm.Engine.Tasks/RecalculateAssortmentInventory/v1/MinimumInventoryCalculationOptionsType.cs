﻿using Galleria.Ccm.Engine.Tasks.Resources.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1
{
    public enum MinimumInventoryCalculationOptionsType : int
    {
        SingleUnit = 0,
        CasePackMinimum = 1
    }

    public static class MinimumInventoryCalculationOptionsTypeHelper
    {
        public static readonly Dictionary<MinimumInventoryCalculationOptionsType, String> FriendlyNames =
            new Dictionary<MinimumInventoryCalculationOptionsType, String>()
            {
                {MinimumInventoryCalculationOptionsType.SingleUnit, Message.MinimumInventoryCalculationOptionsType_SingleUnit },
                {MinimumInventoryCalculationOptionsType.CasePackMinimum, Message.MinimumInventoryCalculationOptionsType_CasePackMinimum }
            };
    }
}
