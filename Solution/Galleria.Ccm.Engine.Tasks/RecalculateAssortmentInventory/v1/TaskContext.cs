﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM810
// V8-29827 : D.Pleasance
//  Created
#endregion
#region Version History : CCM830
// V8-31551 : A.Probyn
//  Added new AssortmentInventoryRulesByGtin
// V8-32205 : A.Probyn
//  Added new NumberOfInventoryRulesApplied
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Constructor

        public TaskContext(ITaskContext context)
            : base(context)
        {
            AssortmentProductsByGtin = new Dictionary<String, PlanogramAssortmentProduct>();
            ProductsByGtin = new Dictionary<String, PlanogramProduct>();
            PerformanceDataByProductId = new Dictionary<Object, PlanogramPerformanceData>();
            AssortmentInventoryRulesByGtin = new Dictionary<String, PlanogramAssortmentInventoryRule>();
            NumberOfInventoryRulesApplied = 0;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The name of the metric profile to be applied.
        /// </summary>
        public String MetricProfileName { get; set; }

        /// <summary>
        /// Gets the resolved metric profile to be applied to the planogram
        /// </summary>
        public IPlanogramMetricProfile MetricProfile { get; set; }

        public String InventoryProfileName { get; set; }
        public InventoryProfile InventoryProfile { get; set; }
        public CalculationOptionsType CalculationOption { get; set; }

        public MinimumInventoryCalculationOptionsType MinimumInventoryCalculation { get; set; }

        public RangeOptionsType RangeOption { get; set; }
        public Int32 NumberOfInventoryRulesApplied { get; set; }

        // dictionary of all assortment products
        public Dictionary<String, PlanogramAssortmentProduct> AssortmentProductsByGtin { get; set; }
        
        // dictionary of all products on
        // the planogram that match products in the assortment
        public Dictionary<String, PlanogramProduct> ProductsByGtin { get; set; }

        // dictionary of all performance data
        // for products on the planogram
        public Dictionary<Object, PlanogramPerformanceData> PerformanceDataByProductId { get; set; }

        // dictionary of all assortment inventory rules
        public Dictionary<String, PlanogramAssortmentInventoryRule> AssortmentInventoryRulesByGtin { get; set; }

        #endregion

    }


}
