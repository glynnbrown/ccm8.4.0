﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800
// V8-25839 : L.Ineson
//  Created
// V8-27949 : A.Kuszyk
//  Added Parameters and OnExectute method.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History : CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History : CCM802
// V8-28989 : A.Kuszyk
//  Amended to use common PerformanceDataTotals class.
// V8-29089 : N.Foster
//  Added Range Options parameter
//  Removed IsVisible parameter
// V8-29175 : N.Foster
//  Ensure inventory values are calcualted before we calculated the assortment
//  If calculated max shelf life value is zero, then ignore shelf life
#endregion
#region Version History : CCM810
// V8-29827 : D.Pleasance
//  Refactored to use TaskContext and also added ApplyInventoryProfileToPlanogram()
// V8-29768 : A.Silva
//  Refactored CalculateUnitsAndLogOnError so that the actual unit calculation is in the InventoryProfile.
// V8-29597 : N.Foster
//  Code Review
#endregion
#region Version History : CCM811
// V8-30486 : M.Pettit
//  Metric Profile and Inventory Profile have CalculationOptions set as a parent parameter
//  Updated execution so if CalculationOption = UnitsOnly then Metric Profile is not required or used
//  Updated execution so if CalculationOption = RankOnly then Inventory Profile is not required or used
// V8-30584 : A.Kuszyk
//  Updated logging so that if Assortment or Planogram products are missing then a warning is logged.
#endregion
#region Version History : CCM820
// V8-30710 : M.Brumby
//  Make missing assortment messaging consistent across tasks.
// V8-30771 : A.Kuszyk
//  Re-factored IsNullAllowed logic from ui.
// V8-30780 :  J.Pickup
//  InventoryMetricType now set in applyInventoryToPlanogram.
// V8-30705 : A.Probyn
//  Updated CalculateUnitsAndLogOnError to factor in assortment rules relating to units.
#endregion
#region Version History : CCM830
// V8-31551 : A.Probyn
//  Updated CalculateUnitsAndLogOnError to factor in any inventory rule overrides.
// V8-32205 : A.Probyn
//  Updated to keep track of inventory rules applied and log.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
// CCM-18645 : A.Silva
//  Amended ApplyInventoryRuleValues so that the inventoy rules are matched against the performance based units and not the original assortment ones.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1
{
    /// <summary>
    /// Recalculates an assortment units and rank values.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            MetricProfileName = 0,
            InventoryProfileName = 1,
            CalculationOptions = 2,
            RangeOptions = 3,
            MinimumInventoryCalculation = 4
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.RecalculateAssortmentInventory_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.RecalculateAssortmentInventory_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Metric Profile Name
            this.RegisterParameter(new TaskParameter(
                Parameter.MetricProfileName,
                Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Name,
                Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.MetricProfileNameSingle,
                null,
                false,
                false,
                (Int32)Parameter.CalculationOptions,
                isRequired: null,
                isNullAllowed: () => IsMetricProfileNullAllowed(),
                isValid: null));

            // Inventory Profile Name
            this.RegisterParameter(new TaskParameter(
                Parameter.InventoryProfileName,
                Message.RecalculateAssortmentInventory_Parameter_InventoryProfileName_Name,
                Message.RecalculateAssortmentInventory_Parameter_InventoryProfileName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.InventoryProfileNameSingle,
                null,
                false,
                false,
                (Int32)Parameter.CalculationOptions,
                isRequired: null,
                isNullAllowed: () => IsInventoryProfileNullAllowed(),
                isValid: null));

            // Calculation Options
            this.RegisterParameter(new TaskParameter(
                Parameter.CalculationOptions,
                Message.RecalculateAssortmentInventory_Parameter_CalculationOptions_Name,
                Message.RecalculateAssortmentInventory_Parameter__CalculationOptions_Description,
                Message.ParameterCategory_Default,
                typeof(CalculationOptionsType),
                CalculationOptionsType.RankAndUnits,
                false,
                false));

            // Range Options
            this.RegisterParameter(new TaskParameter(
                Parameter.RangeOptions,
                Message.RecalculateAssortmentInventory_Parameter_RangeOptions_Name,
                Message.RecalculateAssortmentInventory_Parameter_RangeOptions_Description,
                Message.ParameterCategory_Default,
                typeof(RangeOptionsType),
                RangeOptionsType.AssortmentProductsOnly,
                false,
                false));

            // Minimum Inventory Calculation
            this.RegisterParameter(new TaskParameter(
                Parameter.MinimumInventoryCalculation,
                Message.RecalculateAssortmentInventory_Parameter_MinimumIventoryCalculation_Name,
                Message.RecalculateAssortmentInventory_Parameter_MinimumInventoryCalculation_Description,
                Message.ParameterCategory_Default,
                typeof(MinimumInventoryCalculationOptionsType),
                MinimumInventoryCalculationOptionsType.SingleUnit,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!GetAndValidateParameterObjects(taskContext)) return;
                if (!CheckThereIsSomethingToDo(taskContext)) return;
                ApplyInventoryProfileToPlanogram(taskContext);
                UpdateAllInventoryValues(taskContext);
                GetLookupDictionaries(taskContext);
                CalculateProductRanks(taskContext);
                LogCompletion(taskContext);
            }
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves an validates the parameter objects
        /// </summary>
        private static Boolean GetAndValidateParameterObjects(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region Metric Profile

                // Metric profile
                try
                {
                    if (context.CalculationOption != CalculationOptionsType.UnitsOnly)
                    {
                        MetricProfile metricProfile = MetricProfile.FetchByEntityIdName(context.EntityId, context.MetricProfileName);
                        MetricList globalMetricList = MetricList.FetchByEntityId(context.EntityId);

                        //now validate that the metrics match up to what is in the planogram.
                        IPlanogramMetricProfile resolvedMetricProfile = metricProfile.ToPlanogramMetricProfile(context.Planogram.Performance, globalMetricList);
                        
                        if (resolvedMetricProfile == null)
                        {
                            context.LogError(EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile);
                            return false;
                        }

                        //if we have no metrics at all then fail as an invalid parameter.
                        if (!resolvedMetricProfile.EnumerateAppliedMetricsAndRatios().Any())
                        {
                            context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Name, context.MetricProfileName);
                            return false;
                        }


                        //add the list to the context.
                        context.MetricProfile = resolvedMetricProfile;
                    }
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Name, context.MetricProfileName);
                        return false;
                    }
                    throw ex;
                }

                #endregion

                #region Inventory Profile

                // Inventory profile
                try
                {
                    if (context.CalculationOption != CalculationOptionsType.RankOnly)
                    {
                        context.InventoryProfile = InventoryProfile.FetchByEntityIdName(context.EntityId, context.InventoryProfileName);
                    }
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_InventoryProfileName_Name, context.InventoryProfileName);
                        return false;
                    }
                    throw ex;
                }

                #endregion

                // return success
                return true;
            }
        }

        /// <summary>
        /// Retrieves and returns the configured parameter
        /// values for this task
        /// </summary>
        private Boolean GetAndValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // set defaults
                context.MetricProfileName = String.Empty;
                context.InventoryProfileName = String.Empty;
                context.CalculationOption = CalculationOptionsType.RankAndUnits;
                context.RangeOption = RangeOptionsType.AssortmentProductsOnly;
                context.MinimumInventoryCalculation = MinimumInventoryCalculationOptionsType.SingleUnit;

                #region Metric Profile Name

                // metric profile name
                //Note: parameter is allowed to be null if calculation option does not include Rank data
                if ((CalculationOptionsType)this.Parameters[Parameter.CalculationOptions].Value.Value1 != CalculationOptionsType.UnitsOnly)
                {
                    if (this.Parameters[Parameter.MetricProfileName].Value == null)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Name, Message.Error_NoValue);
                        return false;
                    }
                    context.MetricProfileName = Convert.ToString(this.Parameters[Parameter.MetricProfileName].Value.Value1);
                    if (String.IsNullOrEmpty(context.MetricProfileName))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MetricProfileName_Name, context.MetricProfileName);
                        return false;
                    }
                }

                #endregion

                #region Inventory Profile Name

                // inventory profile name
                // Note: parameter is allowed to be null if calculation option does not include Units data
                if ((CalculationOptionsType)this.Parameters[Parameter.CalculationOptions].Value.Value1 != CalculationOptionsType.RankOnly)
                {
                    if (this.Parameters[Parameter.InventoryProfileName].Value == null)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_InventoryProfileName_Name, Message.Error_NoValue);
                        return false;
                    }
                    context.InventoryProfileName = Convert.ToString(this.Parameters[Parameter.InventoryProfileName].Value.Value1);
                    if (String.IsNullOrEmpty(context.InventoryProfileName))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_InventoryProfileName_Name, context.InventoryProfileName);
                        return false;
                    }
                }

                #endregion

                #region Calculation Options

                // calculation options.
                if (this.Parameters[Parameter.CalculationOptions].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_CalculationOptions_Name, Message.Error_NoValue);
                    return false;
                }
                try
                {
                    context.CalculationOption = (CalculationOptionsType)this.Parameters[Parameter.CalculationOptions].Value.Value1;
                }
                catch
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_CalculationOptions_Name, this.Parameters[Parameter.CalculationOptions].Value.Value1);
                    return false;
                }

                #endregion

                #region Range Options

                // range options.
                if (this.Parameters[Parameter.RangeOptions].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_RangeOptions_Name, Message.Error_NoValue);
                    return false;
                }
                try
                {
                    context.RangeOption = (RangeOptionsType)this.Parameters[Parameter.RangeOptions].Value.Value1;
                }
                catch
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_RangeOptions_Name, this.Parameters[Parameter.RangeOptions].Value.Value1);
                    return false;
                }

                #endregion

                #region Minimum Iventory Calculation

                // Minimum Inventory calculation.
                if (this.Parameters[Parameter.MinimumInventoryCalculation].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MinimumIventoryCalculation_Name, Message.Error_NoValue);
                    return false;
                }
                try
                {
                    context.MinimumInventoryCalculation = (MinimumInventoryCalculationOptionsType)this.Parameters[Parameter.MinimumInventoryCalculation].Value.Value1;
                }
                catch
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.RecalculateAssortmentInventory_Parameter_MinimumIventoryCalculation_Name, this.Parameters[Parameter.MinimumInventoryCalculation].Value.Value1);
                    return false;
                }

                #endregion

                // return success
                return true;
            }
        }

        /// <summary>
        /// Verifies that the Task has an action to perform
        /// </summary>
        private Boolean CheckThereIsSomethingToDo(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we are only using assortment products for the
                // recalculation, then ensure we have some
                // assortment products to calculate            
                if ((context.RangeOption == RangeOptionsType.AssortmentProductsOnly) || (context.RangeOption == RangeOptionsType.AssortmentAndPlanogramProducts))
                {
                    // verify we have an assortment
                    if ((context.Planogram.Assortment == null) ||
                        (context.Planogram.Assortment.Products == null) ||
                        ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                    {
                        context.LogWarning(EventLogEvent.PlanogramNoAssortmentPresent);
                        return false;
                    }
                }

                // if we got this far then return success
                return true;
            }
        }

        /// <summary>
        /// Applies the assigned inventory profile detail to the current planogram
        /// </summary>
        /// <param name="taskContext"></param>
        private void ApplyInventoryProfileToPlanogram(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //only update settings if calculation option includes is Rank and Units or Units Only. 
                //If it is the InventoryProfile will have been set
                if (taskContext.InventoryProfile != null)
                {
                    taskContext.Planogram.Inventory.MinCasePacks = taskContext.InventoryProfile.CasePack;
                    taskContext.Planogram.Inventory.MinShelfLife = taskContext.InventoryProfile.ShelfLife;
                    taskContext.Planogram.Inventory.MinDos = taskContext.InventoryProfile.DaysOfSupply;
                    taskContext.Planogram.Inventory.MinDeliveries = taskContext.InventoryProfile.ReplenishmentDays;
                    taskContext.Planogram.Inventory.InventoryMetricType = taskContext.InventoryProfile.InventoryProfileType;
                }
            }
        }

        /// <summary>
        /// Update all inventory values
        /// </summary>
        private static void UpdateAllInventoryValues(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //only update settings if calculation option includes is Rank and Units or Units Only. 
                //If it is the InventoryProfile will have been set
                if (context.InventoryProfile != null)
                {
                    // ensure the planogram inventory levels are up-to-date
                    context.Planogram.Performance.UpdateAllInventoryValues();
                }
            }
        }

        /// <summary>
        /// Create lookups for the rest of the process
        /// </summary>
        private static void GetLookupDictionaries(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a dictionary of all assortment products
                // indexed by the product GTIN            
                foreach (PlanogramAssortmentProduct assortmentProduct in context.Planogram.Assortment.Products)
                {
                    context.AssortmentProductsByGtin.Add(assortmentProduct.Gtin, assortmentProduct);
                }

                // now build a dictionary of all products on
                // the planogram that match products in the assortment
                foreach (PlanogramProduct product in context.Planogram.Products)
                {
                    if (context.AssortmentProductsByGtin.ContainsKey(product.Gtin))
                    {
                        // the product exists within the assortment
                        context.ProductsByGtin.Add(product.Gtin, product);
                    }
                    else
                    {
                        // the product does not exist within the assortment
                        // but if our range option is to include planogram
                        // products, then we need to add it to the assortment
                        if (context.RangeOption == RangeOptionsType.AssortmentAndPlanogramProducts)
                        {
                            // create a new assortment product within the assortment
                            PlanogramAssortmentProduct assortmentProduct = context.Planogram.Assortment.Products.Add(product);

                            // add to the assortment product lookup
                            context.AssortmentProductsByGtin.Add(assortmentProduct.Gtin, assortmentProduct);

                            // add to the product lookup
                            context.ProductsByGtin.Add(product.Gtin, product);
                        }
                    }
                }

                // build a dictionary of all performance data
                // for products on the planogram
                foreach (PlanogramPerformanceData performanceData in context.Planogram.Performance.PerformanceData)
                {
                    context.PerformanceDataByProductId.Add(performanceData.PlanogramProductId, performanceData);
                }

                // build a dictionary of all the inventory rules for products in the assortment
                foreach (PlanogramAssortmentInventoryRule assortmentInventoryRule in context.Planogram.Assortment.InventoryRules)
                {
                    context.AssortmentInventoryRulesByGtin.Add(assortmentInventoryRule.ProductGtin, assortmentInventoryRule);
                }
            }
        }

        /// <summary>
        /// Calculate product ranks
        /// </summary>
        private void CalculateProductRanks(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Calculate normalised ranks for each product.
                Dictionary<PlanogramAssortmentProduct, Int16> ranksByProduct = null;
                var unRankedProducts = new List<PlanogramAssortmentProduct>();
                if (context.CalculationOption == CalculationOptionsType.RankAndUnits || context.CalculationOption == CalculationOptionsType.RankOnly)
                {
                    ranksByProduct = CalculateRanksByProduct(
                        context, context.MetricProfile, context.ProductsByGtin, context.AssortmentProductsByGtin, context.PerformanceDataByProductId);
                }

                // For each product in the assortment, assign the rank and calculate units.
                Int16 assortmentProductsCount = Convert.ToInt16(context.Planogram.Assortment.Products.Count());
                foreach (var assortmentProduct in context.Planogram.Assortment.Products)
                {
                    // Store original rank and units values so we can log the change later.
                    Int16 oldRank = assortmentProduct.Rank;
                    Int16 oldUnits = assortmentProduct.Units;

                    if (context.CalculationOption == CalculationOptionsType.RankAndUnits || context.CalculationOption == CalculationOptionsType.RankOnly)
                    {
                        // If a rank has been calculated, assign it.
                        Int16 rank;
                        if (ranksByProduct.TryGetValue(assortmentProduct, out rank))
                        {
                            assortmentProduct.Rank = rank;
                        }
                        else
                        {
                            // If no rank has been calculated, this is because no performance data was found.
                            // This will have been logged at the time, so we don't need to log it again. We
                            // just need to keep track of these un-ranked products, so that we can re-rank 
                            // them at the end.
                            unRankedProducts.Add(assortmentProduct);
                        }
                    }
                    if (context.CalculationOption != CalculationOptionsType.RankOnly)
                    {
                        if (context.CalculationOption == CalculationOptionsType.RankAndUnits ||
                            context.CalculationOption == CalculationOptionsType.UnitsOnly ||
                            context.MinimumInventoryCalculation == MinimumInventoryCalculationOptionsType.SingleUnit)
                        {
                            // Try to calculate units and assign to product.
                            Int16? units = CalculateUnitsAndLogOnError(
                                assortmentProduct, context, context.ProductsByGtin, context.PerformanceDataByProductId,
                                context.AssortmentInventoryRulesByGtin, context.InventoryProfile);
                            if (units.HasValue) assortmentProduct.Units = units.Value;
                        }

                        if (context.MinimumInventoryCalculation ==
                            MinimumInventoryCalculationOptionsType.CasePackMinimum)
                        {
                            // Try to calculate units and assign to product.
                            Int16? units = CalculateUnitsAndLogOnError(
                                assortmentProduct, context, context.ProductsByGtin, context.PerformanceDataByProductId,
                                context.AssortmentInventoryRulesByGtin, context.InventoryProfile,
                                context.MinimumInventoryCalculation);
                            if (units.HasValue) assortmentProduct.Units = units.Value;
                        }
                    }
                    // If no change has taken place, continue because there's nothing to log.
                    if (assortmentProduct.Rank == oldRank && assortmentProduct.Units == oldUnits) continue;

                    // Log the change in units and rank if necessary.
                    StringBuilder changeBuilder = new StringBuilder();
                    if (assortmentProduct.Rank != oldRank)
                    {
                        changeBuilder.AppendFormat(Message.RecalculateAssortmentInventory_TheRankChangedFrom0To1, oldRank, assortmentProduct.Rank);
                    }
                    if (assortmentProduct.Units != oldUnits)
                    {
                        if (changeBuilder.Length != 0) changeBuilder.Append(Message.Generic_And);
                        changeBuilder.AppendFormat(Message.RecalculateAssortmentInventory_TheUnitsChangedFrom0To1, oldUnits, assortmentProduct.Units);
                    }
                    context.LogDebug(
                        PlanogramEventLogAffectedType.AssortmentProduct,
                        assortmentProduct.Id,
                        EventLogEvent.PlanogramAssortmentProductChanged,
                        assortmentProduct.Name,
                        assortmentProduct.Gtin,
                        changeBuilder.ToString());
                }

                // Re-rank un-ranked products if necessary.
                if (context.CalculationOption == CalculationOptionsType.RankAndUnits ||
                    context.CalculationOption == CalculationOptionsType.RankOnly &&
                    unRankedProducts.Count > 0)
                {
                    // If we reach this stage, we have ranked most products, but some did not have performance data.
                    // We rank these products by their existing rank, at the bottom of the list.
                    Int16 lowestRank = context.Planogram.Assortment.Products.Max(p => p.Rank);
                    foreach (var assortmentProduct in unRankedProducts.OrderBy(p => p.Rank).ThenBy(p => p.Gtin))
                    {
                        lowestRank++;
                        assortmentProduct.Rank = lowestRank;
                        context.LogWarning(
                            EventLogEvent.PlanogramAssortmentProductCouldNotBeRanked, assortmentProduct.Name, assortmentProduct.Gtin, lowestRank);
                    }
                }
            }
        }

        /// <summary>
        /// Logs final details about the task execution.
        /// </summary>
        private static void LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (context.NumberOfInventoryRulesApplied > 0)
                { 
                    //Log that the recalculated assortment inventory had inventory rules (at least 1)
                    //Detailed logging will be implemented in a later version.
                    context.LogInformation(EventLogEvent.PlanogramRecalculateAssortmentInventoryRuleApplied);
                }

                //  Log that the recalculated assortment inventory was correctly updated.
                context.LogInformation(
                    EventLogEvent.PlanogramRecalculateAssortmentInventoryUpdated);
            }
        }

        /// <summary>
        /// Calculates the PSI values for the given assortment products and ranks 
        /// them accordingly.
        /// </summary>
        /// <param name="context">The task context</param>
        /// <param name="metricProfile">The resolved metric profile metrics to use when calculating PSIs.</param>
        /// <param name="productsByGtin">A dictionary keyed by GTIN containing the planogram products for the products in the plan's assortment.</param>
        /// <param name="assortmentProductsByGtin">The products in the plan's assortment, keyed by GTIN.</param>
        /// <param name="performanceDataByProductId">The plan's performance data items, keyed by product Id.</param>
        /// <returns>The ranks calculated ranks, keyed by assortment product.</returns>
        internal static Dictionary<PlanogramAssortmentProduct, Int16> CalculateRanksByProduct(
            ITaskContext context,
            IPlanogramMetricProfile metricProfile,
            Dictionary<String, PlanogramProduct> productsByGtin, 
            Dictionary<String, PlanogramAssortmentProduct> assortmentProductsByGtin,
            Dictionary<Object, PlanogramPerformanceData> performanceDataByProductId)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Calculate performance data totals.
                PlanogramPerformanceDataTotals performanceDataTotals = new PlanogramPerformanceDataTotals(context.Planogram.Performance.PerformanceData);

                // Calculate normalised PSI values.
                Dictionary<PlanogramProduct, Double> normalisedProductPsis =
                    GetNormalisedPsis(metricProfile, productsByGtin.Values, performanceDataTotals, performanceDataByProductId);

                // Order products by highest PSI first, then Gtin.
                var productsRankedByPsi = normalisedProductPsis.
                    Select(kvp => kvp.Key).
                    OrderByDescending(p => normalisedProductPsis[p]).
                    ThenBy(p => p.Gtin);

                // Rank products according to PSI.
                var ranksByAssortmentProduct = new Dictionary<PlanogramAssortmentProduct, Int16>();
                Int16 rank = 1;
                foreach (var product in productsRankedByPsi)
                {
                    ranksByAssortmentProduct.Add(assortmentProductsByGtin[product.Gtin], rank);
                    rank++;
                }

                return ranksByAssortmentProduct;
            }
        }


        /// <summary>
        /// Calculates the units value for the given assortment product and logs any warnings
        /// or errors that interupt the calulation.
        /// </summary>
        /// <param name="assortmentProduct">The assortment product for which the units should be calculated.</param>
        /// <param name="context">The task context</param>
        /// <param name="productsByGtin">A dictionary keyed by GTIN containing the planogram products for the products in the plan's assortment.</param>
        /// <param name="performanceDataByProductId">The plan's performance data items, keyed by product Id.</param>
        /// <param name="inventoryRulesByGtin"></param>
        /// <param name="inventoryProfile">The Inventory Profile to use when calculating units.</param>
        /// <returns>The units value, or null if the calculation could not be performed.</returns>
        internal static Int16? CalculateUnitsAndLogOnError(
            PlanogramAssortmentProduct assortmentProduct,
            TaskContext context,
            Dictionary<String, PlanogramProduct> productsByGtin,
            Dictionary<Object, PlanogramPerformanceData> performanceDataByProductId,
            Dictionary<String, PlanogramAssortmentInventoryRule> inventoryRulesByGtin,
            InventoryProfile inventoryProfile,
            MinimumInventoryCalculationOptionsType calculationOptionType)
        {
            using (new CodePerformanceMetric())
            {
                // Get the matching Product and Performance Data for this assortment product and return
                // null if we're unable to do so.
                PlanogramProduct product;
                if (!productsByGtin.TryGetValue(assortmentProduct.Gtin, out product))
                {
                    context.LogWarning(
                        EventLogEvent.InsufficientPlanogramContent,
                        Message.TaskLogging_PlanogramProduct,
                        String.Format(Message.TaskLogging_NoMatchingPlanogramProduct, assortmentProduct.Gtin));
                    return null;
                }

                if (!assortmentProduct.IsAvailableToPlace(context.Planogram.LocationCode)) return null;

                //If assortment product is available to place
                Int16? returnValue = null;

                PlanogramPerformanceData performanceData;
                if (!performanceDataByProductId.TryGetValue(product.Id, out performanceData))
                {
                    context.LogWarning(
                        EventLogEvent.InsufficientPlanogramContent,
                        Message.TaskLogging_PlanogramPerformanceData,
                        String.Format(Message.TaskLogging_NoMatchingPerformanceData, assortmentProduct.Gtin));
                    //return null; dont return null as assortment rules could set value further on
                }

                //If no assortment force units rule exists, calculate using performance data
                if (!(assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Force
                      && assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Units))
                {
                    //If we do have performance data
                    if (performanceData != null)
                    {
                        //  Update Inventory values if we do not have sufficient data.
                        if (!performanceData.UnitsSoldPerDay.HasValue) performanceData.UpdateInventoryValues();

                        //  Log a warning if calculating units without sufficient data.
                        if (!(performanceData.UnitsSoldPerDay.HasValue && performanceData.UnitsSoldPerDay.Value.GreaterThan(0)))
                        {
                            // Log this as a warning, because we can still continue to calculate a units value.
                            context.LogInformation(
                                EventLogEvent.PlanogramPerformanceDataMissingOrEmpty, assortmentProduct.Name, assortmentProduct.Gtin);
                        }

                        //Calculate value
                        returnValue = inventoryProfile.CalculateMaxUnits(product, performanceData);
                    }

                    //If the product has no performance and the calculation option is Case Pack Minimum use settings against selected inventory profile
                    if ((performanceData.P1 == null || performanceData.P1 == 0.0F) &&
                        (performanceData.P2 == null || performanceData.P2 == 0.0F) &&
                        (performanceData.P3 == null || performanceData.P3 == 0.0F) &&
                        (performanceData.P4 == null || performanceData.P4 == 0.0F) &&
                        (performanceData.P5 == null || performanceData.P5 == 0.0F) &&
                        (performanceData.P6 == null || performanceData.P6 == 0.0F) &&
                        (performanceData.P7 == null || performanceData.P7 == 0.0F) &&
                        (performanceData.P8 == null || performanceData.P8 == 0.0F) &&
                        (performanceData.P9 == null || performanceData.P9 == 0.0F) &&
                        (performanceData.P10 == null || performanceData.P10 == 0.0F) && 
                        (calculationOptionType == MinimumInventoryCalculationOptionsType.CasePackMinimum))
                    {
                        returnValue = Convert.ToInt16(product.CasePackUnits * inventoryProfile.CasePack);
                    }

                    //Ensure assortment rules are adhered to
                    returnValue = assortmentProduct.EnforceNonForceAssortmentRuleUnits(returnValue);
                }
                else
                {
                    //Return assortment rules force units value
                    returnValue = assortmentProduct.RuleValue;
                }

                //does any inventory rules exist
                PlanogramAssortmentInventoryRule assortmentInventoryRule;
                if (!inventoryRulesByGtin.TryGetValue(assortmentProduct.Gtin, out assortmentInventoryRule)) return returnValue;

                //Increase counter for logging
                context.NumberOfInventoryRulesApplied++;

                //apply rule override
                return ApplyInventoryRuleValues(
                    returnValue ?? assortmentProduct.Units,
                    assortmentProduct,
                    performanceData,
                    product,
                    assortmentInventoryRule);
            }
        }
        
        /// <summary>
        /// Calculates the units value for the given assortment product and logs any warnings
        /// or errors that interupt the calulation.
        /// </summary>
        /// <param name="assortmentProduct">The assortment product for which the units should be calculated.</param>
        /// <param name="context">The task context</param>
        /// <param name="productsByGtin">A dictionary keyed by GTIN containing the planogram products for the products in the plan's assortment.</param>
        /// <param name="performanceDataByProductId">The plan's performance data items, keyed by product Id.</param>
        /// <param name="inventoryRulesByGtin"></param>
        /// <param name="inventoryProfile">The Inventory Profile to use when calculating units.</param>
        /// <returns>The units value, or null if the calculation could not be performed.</returns>
        internal static Int16? CalculateUnitsAndLogOnError(
            PlanogramAssortmentProduct assortmentProduct, 
            TaskContext context, 
            Dictionary<String, PlanogramProduct> productsByGtin,
            Dictionary<Object, PlanogramPerformanceData> performanceDataByProductId,
            Dictionary<String, PlanogramAssortmentInventoryRule> inventoryRulesByGtin,
            InventoryProfile inventoryProfile)
        {
            using (new CodePerformanceMetric())
            {
                // Get the matching Product and Performance Data for this assortment product and return
                // null if we're unable to do so.
                PlanogramProduct product;
                if (!productsByGtin.TryGetValue(assortmentProduct.Gtin, out product))
                {
                    context.LogWarning(
                        EventLogEvent.InsufficientPlanogramContent,
                        Message.TaskLogging_PlanogramProduct,
                        String.Format(Message.TaskLogging_NoMatchingPlanogramProduct, assortmentProduct.Gtin));
                    return null;
                }

                if (!assortmentProduct.IsAvailableToPlace(context.Planogram.LocationCode)) return null;

                //If assortment product is available to place
                Int16? returnValue = null;

                PlanogramPerformanceData performanceData;
                if (!performanceDataByProductId.TryGetValue(product.Id, out performanceData))
                {
                    context.LogWarning(
                        EventLogEvent.InsufficientPlanogramContent,
                        Message.TaskLogging_PlanogramPerformanceData,
                        String.Format(Message.TaskLogging_NoMatchingPerformanceData, assortmentProduct.Gtin));
                    //return null; dont return null as assortment rules could set value further on
                }

                //If no assortment force units rule exists, calculate using performance data
                if (!(assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.Force
                      && assortmentProduct.RuleValueType == PlanogramAssortmentProductRuleValueType.Units))
                {
                    //If we do have performance data
                    if (performanceData != null)
                    {
                        //  Update Inventory values if we do not have sufficient data.
                        if (!performanceData.UnitsSoldPerDay.HasValue) performanceData.UpdateInventoryValues();

                        //  Log a warning if calculating units without sufficient data.
                        if (!(performanceData.UnitsSoldPerDay.HasValue && performanceData.UnitsSoldPerDay.Value.GreaterThan(0)))
                        {
                            // Log this as a warning, because we can still continue to calculate a units value.
                            context.LogInformation(
                                EventLogEvent.PlanogramPerformanceDataMissingOrEmpty, assortmentProduct.Name, assortmentProduct.Gtin);
                        }

                        //Calculate value
                        returnValue = inventoryProfile.CalculateMaxUnits(product, performanceData);
                    }

                    //Ensure assortment rules are adhered to
                    returnValue = assortmentProduct.EnforceNonForceAssortmentRuleUnits(returnValue);
                }
                else
                {
                    //Return assortment rules force units value
                    returnValue = assortmentProduct.RuleValue;
                }

                //does any inventory rules exist
                PlanogramAssortmentInventoryRule assortmentInventoryRule;
                if (!inventoryRulesByGtin.TryGetValue(assortmentProduct.Gtin, out assortmentInventoryRule)) return returnValue;

                //Increase counter for logging
                context.NumberOfInventoryRulesApplied++;

                //apply rule override
                return ApplyInventoryRuleValues(
                    returnValue ?? assortmentProduct.Units,
                    assortmentProduct,
                    performanceData,
                    product,
                    assortmentInventoryRule);
            }
        }

        /// <summary>
        /// Applies the inventory rule values and returns a units value
        /// </summary>
        /// <param name="assortmentUnits">The number of units recommended in the assortment so far, based on performance.</param>
        /// <param name="product"></param>
        /// <param name="productPerformanceData"></param>
        /// <param name="planProduct"></param>
        /// <param name="planogramAssortmentInventoryRule"></param>
        /// <returns></returns>
        private static Int16 ApplyInventoryRuleValues(
            Int16 assortmentUnits,
            IPlanogramAssortmentProduct product,
            IPlanogramPerformanceData productPerformanceData,
            IPlanogramProduct planProduct,
            IPlanogramAssortmentInventoryRule planogramAssortmentInventoryRule)
        {
            Single minWasteHurdleUnits = planogramAssortmentInventoryRule.WasteHurdleUnits;
            Int32 minFacings = planogramAssortmentInventoryRule.MinFacings;
            Int32 minUnits = planogramAssortmentInventoryRule.MinUnits;
            Single minReplenishmentDays = planogramAssortmentInventoryRule.ReplenishmentDays;
            Single maxShelfLife = planogramAssortmentInventoryRule.ShelfLife;
            Single minCasePack = planogramAssortmentInventoryRule.CasePack;
            Single minDaysOfSupply = planogramAssortmentInventoryRule.DaysOfSupply;

            //Calculate required values 
            Single unitsSoldPerDay = productPerformanceData.UnitsSoldPerDay ?? 0;
            Single deliveryFrequencyDays = planProduct.DeliveryFrequencyDays ?? 0;
            var minimumDaysOfSupplyUnits = (Int32) Math.Ceiling(minDaysOfSupply*unitsSoldPerDay);
            var minimumCasePackUnits = (Int32) Math.Ceiling(minCasePack*planProduct.CasePackUnits);
            Int32? maximumShelfLifeUnits = null;
            if ((planProduct.ShelfLife > 0) &&
                (maxShelfLife > 0) &&
                (unitsSoldPerDay > 0))
            {
                maximumShelfLifeUnits = Math.Max(
                    1,
                    (Int32) Math.Floor(maxShelfLife*planProduct.ShelfLife*unitsSoldPerDay));
            }
            var minimumReplenishmentDaysUnits = (Int32) Math.Ceiling(
                minReplenishmentDays*deliveryFrequencyDays*unitsSoldPerDay);

            //Default to current assortment value
            Int16 newUnits = assortmentUnits;

            //Apply setup inventory rules 
            newUnits = Convert.ToInt16(Math.Max(newUnits, minimumDaysOfSupplyUnits));
            newUnits = Convert.ToInt16(Math.Max(newUnits, minimumCasePackUnits));
            newUnits = Convert.ToInt16(Math.Max(newUnits, minimumReplenishmentDaysUnits));
            newUnits = Convert.ToInt16(Math.Max(newUnits, minUnits));
            newUnits = Convert.ToInt16(Math.Max(newUnits, minWasteHurdleUnits));

            if (maximumShelfLifeUnits.HasValue &&
                newUnits > maximumShelfLifeUnits.Value)
            {
                newUnits = Convert.ToInt16(maximumShelfLifeUnits.Value);
            }

            //Apply minimum facings
            if (minFacings > 0)
            {
                product.Facings = Convert.ToByte(Math.Max(minFacings, product.Facings));
            }

            Single minWasteHurdleCasePack = planogramAssortmentInventoryRule.WasteHurdleCasePack;
            Boolean hasMinWasteHurdleCasePack = minWasteHurdleCasePack > 0;
            Boolean hasMinWasteHurdleUnits = minWasteHurdleUnits > 0;
            if (!hasMinWasteHurdleCasePack &&
                !hasMinWasteHurdleUnits) return newUnits;

            //Apply minimum waste case pack/units
            // calculate the waste hurdle units and case packs per week
            Double calculatedUnits = Math.Round(unitsSoldPerDay*7, 4, MidpointRounding.AwayFromZero);
            Double calculatedCasePacks = Math.Round(calculatedUnits/planProduct.CasePackUnits, 4);

            if (hasMinWasteHurdleUnits && calculatedUnits < minWasteHurdleUnits ||
                hasMinWasteHurdleCasePack && calculatedCasePacks < minWasteHurdleCasePack)
            {
                //Force 0 units as minimums weren't met
                newUnits = 0;
            }

            return newUnits;
        }


        /// <summary>
        /// Calculates normalised Psis for the given products.
        /// </summary>
        /// <param name="metricProfile">The resolved metric profile metrics to use for the calculations.</param>
        /// <param name="products">The products to calculate the Psis for.</param>
        /// <param name="performanceDataTotals">Pre-calculated performance data totals.</param>
        /// <returns>A dictionary of products and their Psi values.</returns>
        internal static Dictionary<PlanogramProduct, Double> GetNormalisedPsis(
            IPlanogramMetricProfile metricProfile,
            IEnumerable<PlanogramProduct> products, 
            PlanogramPerformanceDataTotals performanceDataTotals,
            Dictionary<Object, PlanogramPerformanceData> performanceDataByProductId)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var unnormalisedPsis = products.
                    ToDictionary(p => p, p => GetUnNormalisedPsi(p, metricProfile, performanceDataTotals, performanceDataByProductId));
                var totalPsiValue = unnormalisedPsis.Values.Select(v => v == null ? 0 : (Double)v).Sum();
                var normalisedProductPsis = new Dictionary<PlanogramProduct, Double>();
                foreach (var kvp in unnormalisedPsis)
                {
                    if (kvp.Value == null) continue;
                    normalisedProductPsis.Add(kvp.Key, ((Double)kvp.Value) / totalPsiValue);
                }
                return normalisedProductPsis;
            }
        }

        /// <summary>
        /// Calculates the un-normalised Psi value for the given product.
        /// </summary>
        /// <param name="product">The Planogram Product for which the Psi should be calculated.</param>
        /// <param name="metricProfile">The resolved profile of the metrics to use in the calculation.</param>
        /// <param name="performanceDataTotals">The pre-calculated totals of the performance data.</param>
        /// <returns>The product's un-normalised Psi, or null if no corresponding performance data is available.</returns>
        /// <exception cref="ArgumentNullException">Thrown if product is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if metricProfile is null.</exception>
        /// <exception cref="ArgumentNullException">Thrown if performanceDataTotals is null.</exception>
        internal static Double? GetUnNormalisedPsi(
            PlanogramProduct product,
            IPlanogramMetricProfile metricProfile,
            PlanogramPerformanceDataTotals performanceDataTotals,
            Dictionary<Object,PlanogramPerformanceData> performanceDataByProductId)
        {
            using (CodePerformanceMetric codeMetric = new CodePerformanceMetric())
            {
                if (product == null) throw new ArgumentNullException("product cannot be null");
                if (metricProfile == null) return null;//throw new ArgumentNullException("metricProfile cannot be null");
                if (performanceDataTotals == null) throw new ArgumentNullException("performanceDataTotals cannot be null");

                PlanogramPerformanceData performanceData;
                if (!performanceDataByProductId.TryGetValue(product.Id, out performanceData)) return null;

                Double unnormalisedPsi = 0;
                
                //cycle through applying ratios
                foreach(var ratioEntry in metricProfile.EnumerateAppliedMetricsAndRatios())
                {
                    PlanogramPerformanceMetric metric = ratioEntry.Item1;
                    if (metric == null) continue;

                    Single metricRatio = ratioEntry.Item2;
                    Single? pValue = performanceData.GetPerformanceData(metric.MetricId);
                    Single? pTotal = performanceDataTotals.GetTotalByMetricNumber(metric.MetricId);

                    if (pValue != null && pTotal != null && pTotal > 0)
                    {
                        if (metric.Direction == MetricDirectionType.MaximiseIncrease)
                        {
                            unnormalisedPsi += metricRatio * (pValue.Value / pTotal.Value);
                        }
                        else
                        {
                            unnormalisedPsi += metricRatio * (1 - (pValue.Value / pTotal.Value));
                        }
                    }
                }

                return unnormalisedPsi;
            }
        }

        private Boolean IsMetricProfileNullAllowed()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                CalculationOptionsType? calculationOptions = GetCalculationOptionsParameterValue();

                if (!calculationOptions.HasValue) return false;

                if (calculationOptions.Value == CalculationOptionsType.UnitsOnly) return true;

                return false;
            }
        }
        
        private Boolean IsInventoryProfileNullAllowed()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                CalculationOptionsType? calculationOptions = GetCalculationOptionsParameterValue();

                if (!calculationOptions.HasValue) return false;

                if (calculationOptions.Value == CalculationOptionsType.RankOnly) return true;

                return false;
            }
        }

        private CalculationOptionsType? GetCalculationOptionsParameterValue()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (!this.Parameters.Contains((Int32)Parameter.CalculationOptions)) return null;
                if (this.Parameters[(Int32)Parameter.CalculationOptions] == null ||
                    this.Parameters[(Int32)Parameter.CalculationOptions].Value == null ||
                    this.Parameters[(Int32)Parameter.CalculationOptions].Value.Value1 == null) return null;

                CalculationOptionsType calculationOptionsValue;
                try
                {
                    calculationOptionsValue = (CalculationOptionsType)this.Parameters[(Int32)Parameter.CalculationOptions].Value.Value1;
                }
                catch (InvalidCastException)
                {
                    return null;
                }

                return calculationOptionsValue;
            }
        }

        #endregion
    }
}
