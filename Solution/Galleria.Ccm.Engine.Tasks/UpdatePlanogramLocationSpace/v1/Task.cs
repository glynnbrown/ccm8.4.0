﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31562 : D.Pleasance
//  Created.
// CCM-18492 : D.Pleasance
//  Amended to update NotchSpacingY \ NotchStartY and changes applied to update all component widths if they are of the size of the original bay. 
#endregion

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1
{
    /// <summary>
    /// Updates Planogram fixture profile to location space profile
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            IsUpdatingBayProfile,
            IsUpdatingShelfProfile,
            ProfileMismatchBehaviour,
            ShelfProfileAttributes
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramLocationSpace_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdatePlanogramLocationSpace_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Bay profile
            this.RegisterParameter(new TaskParameter(
                Parameter.IsUpdatingBayProfile,
                Message.UpdatePlanogramLocationSpace_BayProfile_Name,
                Message.UpdatePlanogramLocationSpace_BayProfile_Description, 
                Message.ParameterCategory_Default,
                TaskParameterType.Boolean,
                true,
                false,
                false));

            // Shelf profile
            this.RegisterParameter(new TaskParameter(
                Parameter.IsUpdatingShelfProfile,
                Message.UpdatePlanogramLocationSpace_ShelfProfile_Name,
                Message.UpdatePlanogramLocationSpace_ShelfProfile_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.Boolean,
                true,
                false,
                false));

            // Shelf profile ignore attributes
            this.RegisterParameter(new TaskParameter(
                Parameter.ShelfProfileAttributes,
                Message.UpdatePlanogramLocationSpace_ShelfProfileAttributes_Name,
                Message.UpdatePlanogramLocationSpace_ShelfProfileAttributes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.LocationSpaceElementAttributeUpdateValueMultiple,
                null,
                false,
                false));

            // Profile mis match behaviour
            this.RegisterParameter(new TaskParameter(
                Parameter.ProfileMismatchBehaviour,
                Message.UpdatePlanogramLocationSpace_ProfileMismatchBehaviour_Name,
                Message.UpdatePlanogramLocationSpace_ProfileMismatchBehaviour_Description,
                Message.ParameterCategory_Default,
                typeof(FixtureProfileMismatchBehaviourType),
                FixtureProfileMismatchBehaviourType.Fail,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!UpdateFixtureProfile(taskContext)) return;
            }
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Validates task parameters
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if successful</returns>
        private Boolean ValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region IsUpdatingBayProfile

                //  Validate that the IsUpdatingBayProfile parameter exists.
                if (Parameters[Parameter.IsUpdatingBayProfile].Value == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramLocationSpace_BayProfile_Name,
                        Message.Error_NoValue);
                    success = false;
                }
                
                taskContext.IsUpdatingBayProfile = Convert.ToBoolean(Parameters[Parameter.IsUpdatingBayProfile].Value.Value1);

                #endregion

                #region IsUpdatingShelfProfile

                //  Validate that the IsUpdatingShelfProfile parameter exists.
                if (Parameters[Parameter.IsUpdatingShelfProfile].Value == null)
                {
                    taskContext.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.UpdatePlanogramLocationSpace_ShelfProfile_Name,
                        Message.Error_NoValue);
                    success = false;
                }

                taskContext.IsUpdatingShelfProfile = Convert.ToBoolean(Parameters[Parameter.IsUpdatingShelfProfile].Value.Value1);

                #endregion

                #region ShelfProfileAttributes

                taskContext.ShelfProfileAttributes = new List<string>();

                // Get and validate planogram attributes.
                if (Parameters[Parameter.ShelfProfileAttributes].Value != null)
                {
                    try
                    {
                        foreach (ITaskParameterValue parameterValue in this.Parameters[Parameter.ShelfProfileAttributes].Values)
                        {
                            taskContext.ShelfProfileAttributes.Add(parameterValue.Value1.ToString());
                        }
                    }
                    catch
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdatePlanogramLocationSpace_ShelfProfileAttributes_Name,
                            Message.Error_NoValue);
                        return false;
                    }
                }

                #endregion

                #region ProfileMismatchBehaviour

                //  Validate that the Profile Mismatch Behaviour parameter exists.
                if (Parameters[Parameter.ProfileMismatchBehaviour].Value == null)
                {
                    taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.UpdatePlanogramLocationSpace_ProfileMismatchBehaviour_Name,
                                     Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    try
                    {
                        taskContext.ProfileMismatchBehaviour =
                            (FixtureProfileMismatchBehaviourType)Parameters[Parameter.ProfileMismatchBehaviour].Value.Value1;
                    }
                    catch
                    {
                        taskContext.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.UpdatePlanogramLocationSpace_ProfileMismatchBehaviour_Name,
                                         Parameters[Parameter.ProfileMismatchBehaviour].Value.Value1);
                        success = false;
                    }
                }

                #endregion
               
                return success;
            }
        }

        /// <summary>
        ///     Validate the planogram details required by the task.
        /// </summary>
        private static Boolean ValidatePlanogram(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                // Validate that there is something to update
                if (!taskContext.IsUpdatingBayProfile && !taskContext.IsUpdatingShelfProfile)
                {
                    taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoFixtureProfileUpdateParametersApplied);
                    return false;
                }
                
                #region Validate Obtaining Location Space data

                // verify that the planogram is linked to a location, required to obtain location space data
                if (String.IsNullOrEmpty(taskContext.Planogram.LocationCode))
                {
                    taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoPlanogramLocationCode);
                    return false;
                }

                // validate that the location exists
                LocationInfoList locationInfoList = LocationInfoList.FetchByEntityIdLocationCodes(taskContext.EntityId, new List<String>() { taskContext.Planogram.LocationCode });

                if (!locationInfoList.Any())
                {
                    taskContext.LogError(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_InvalidPlanogramLocationCode);
                    return false;
                }

                // verify that the planogram is linked to a category, required to obtain location space data
                if (String.IsNullOrEmpty(taskContext.Planogram.CategoryCode))
                {
                    taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoPlanogramCategoryCode);
                    return false;
                }

                // validate that the category exists
                ProductGroupInfoList productGroupInfoList = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { taskContext.Planogram.CategoryCode });

                if (!productGroupInfoList.Any() || productGroupInfoList.Where(p => p.DateDeleted != null).Any())
                {
                    taskContext.LogError(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_InvalidPlanogramCategoryCode);
                    return false;
                }

                LocationSpaceInfoList locationSpaceInfoList = LocationSpaceInfoList.FetchByEntityId(taskContext.EntityId);
                LocationSpaceInfo locationSpaceInfo = locationSpaceInfoList.FirstOrDefault(p => p.LocationCode == taskContext.Planogram.LocationCode);

                if (locationSpaceInfo != null)
                {
                    LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(locationSpaceInfo.Id);
                    taskContext.LocationSpaceProductGroup = locationSpace.LocationSpaceProductGroups.FirstOrDefault(p => p.ProductGroupId == productGroupInfoList.First().Id);

                    if (taskContext.LocationSpaceProductGroup == null)
                    {
                        taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                        Message.TaskLogging_NoLocationSpaceCategoryData);
                        return false;
                    }
                    else
                    {
                        // Validate that there are location space bay data that can be used to perform the update
                        if (!taskContext.LocationSpaceProductGroup.Bays.Any())
                        {
                            taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                        Message.TaskLogging_NoLocationSpaceBayData);
                            return false;
                        }
                    }                    
                }
                else
                {
                    taskContext.LogWarning(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoLocationSpaceData);
                    return false;
                }

                #endregion

                #region Validate Updating Location Space data

                if (taskContext.ProfileMismatchBehaviour == FixtureProfileMismatchBehaviourType.Fail)
                {
                    if (taskContext.IsUpdatingBayProfile)
                    {
                        Int32 bayCount = taskContext.LocationSpaceProductGroup.Bays.Any() ? taskContext.LocationSpaceProductGroup.Bays.Count : (Int32)taskContext.LocationSpaceProductGroup.BayCount;

                        if (taskContext.Planogram.FixtureItems.Count != bayCount)
                        {
                            taskContext.LogError(EventLogEvent.PlanogramFixtureBayMismatchBetweenLocationSpaceData);
                            success = false;
                        }
                    }

                    if (success && taskContext.IsUpdatingShelfProfile)
                    {
                        Stack<LocationSpaceBay> locationSpaceBays = new Stack<LocationSpaceBay>();

                        foreach (LocationSpaceBay locationSpaceBay in taskContext.LocationSpaceProductGroup.Bays.OrderByDescending(p=> p.Order))
                        {
                            locationSpaceBays.Push(locationSpaceBay);
                        }

                        foreach (PlanogramFixtureItem fixtureItem in taskContext.Planogram.FixtureItems.OrderBy(p=> p.X))
                        {
                            LocationSpaceBay locationSpaceBay = locationSpaceBays.Pop();

                            if (locationSpaceBay != null)
                            {
                                if (locationSpaceBay.Elements.Any())
                                {
                                    if (locationSpaceBay.Elements.Count != fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).Count())
                                    {
                                        taskContext.LogError(EventLogEvent.PlanogramFixtureBayElementMismatchBetweenLocationSpaceData, fixtureItem.GetPlanogramFixture().Name);
                                        success = false;
                                    }
                                }
                                else
                                {
                                    taskContext.LogError(EventLogEvent.LocationSpaceBayElementDataMissing);
                                    success = false;
                                }
                            }                            
                        }
                    }
                }

                #endregion

                //  return success.
                return success;
            }
        }

        /// <summary>
        /// Updates the Planogram fixture profile to match Location Space data
        /// </summary>
        /// <param name="taskContext">The task context</param>
        /// <returns></returns>
        private Boolean UpdateFixtureProfile(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                taskContext.Settings = (IPlanogramSettings)Entity.FetchById(taskContext.EntityId).SystemSettings;

                Stack<PlanogramFixtureItem> fixtureItems = new Stack<PlanogramFixtureItem>();
                foreach (PlanogramFixtureItem fixtureItem in taskContext.Planogram.FixtureItems.OrderByDescending(p=> p.X))
                {
                    fixtureItems.Push(fixtureItem);
                }

                Single xPosition = 0;
                Int32? backBoardColour = null;
                Single? backBoardDepth = null;
                Int32? baseColour = null;
                PlanogramFixtureItem previousFixtureItem = null;

                foreach (LocationSpaceBay locationSpaceBay in taskContext.LocationSpaceProductGroup.Bays.OrderBy(p=> p.Order))
                {
                    PlanogramFixtureItem fixtureItem = fixtureItems.Any() ? fixtureItems.Pop() : null;
                    taskContext.PreviousBayWidth = 0;

                    if (fixtureItem != null) 
                    {
                        taskContext.CurrentFixture = fixtureItem.GetPlanogramFixture();
                    }

                    if (taskContext.IsUpdatingBayProfile)
                    {
                        if (fixtureItem != null)
                        {
                            IEnumerable<PlanogramSubComponentPlacement> subComponentPlacements = fixtureItem.GetPlanogramSubComponentPlacements();
                            PlanogramSubComponentPlacement backboard = subComponentPlacements.FirstOrDefault(p => p.Component.ComponentType == PlanogramComponentType.Backboard);
                            if (backboard != null && backboard.SubComponent != null && backBoardColour == null)
                            {
                                backBoardColour = backboard.SubComponent.FillColourFront;
                                backBoardDepth = backboard.SubComponent.Depth;
                            }
                            
                            PlanogramSubComponentPlacement fixtureBase = subComponentPlacements.FirstOrDefault(p => p.Component.ComponentType == PlanogramComponentType.Base);
                            if (fixtureBase != null && fixtureBase.SubComponent != null && baseColour == null)
                            {
                                baseColour = fixtureBase.SubComponent.FillColourFront;
                            }

                            taskContext.CurrentFixtureComponentBackboard = backboard == null ? null : backboard.FixtureComponent;
                            taskContext.CurrentFixtureComponentBase = fixtureBase == null ? null : fixtureBase.FixtureComponent;

                            UpdateFixture(taskContext, fixtureItem, locationSpaceBay, xPosition);
                        }
                        else
                        {
                            fixtureItem = AddFixtureItem(taskContext, locationSpaceBay, xPosition, backBoardColour, baseColour, backBoardDepth);
                        }
                    }

                    if (taskContext.IsUpdatingShelfProfile && fixtureItem != null)
                    {
                        var hasLocationSpaceBayElements = locationSpaceBay.Elements.Any();
                        if (hasLocationSpaceBayElements)
                        {
                            UpdateFixtureBayShelfProfileFromLocationSpaceElements(taskContext, locationSpaceBay, fixtureItem, previousFixtureItem, xPosition);
                        }
                        // we have a previous bay and current bay doesnt have shelf profile then we create based upon previous.
                        else if (previousFixtureItem != null 
                                && !fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType != PlanogramComponentType.Base 
                                                                                            && p.Component.ComponentType != PlanogramComponentType.Backboard).Any())
                        {
                            CreateFixtureBayShelfProfileFromPreviousBay(taskContext, fixtureItem, previousFixtureItem);
                        }
                        // update shelf widths to match fixture bay changes
                        UpdateFixtureBayComponentWidths(taskContext, fixtureItem, !hasLocationSpaceBayElements);
                    }
                   
                    previousFixtureItem = fixtureItem;
                    xPosition += locationSpaceBay.Width;
                }

                // remove any remaining fixture items on the stack
                while (fixtureItems.Any())
                {
                    PlanogramFixtureItem fixtureItem = fixtureItems.Pop();
                    List<PlanogramSubComponentPlacement> subComponentPlacements = fixtureItem.GetPlanogramSubComponentPlacements().ToList();

                    foreach (PlanogramSubComponentPlacement subComponentPlacement in subComponentPlacements)
                    {
                        taskContext.Planogram.Positions.RemoveList(subComponentPlacement.GetPlanogramPositions());
                        fixtureItem.GetPlanogramFixture().Components.Remove(subComponentPlacement.FixtureComponent);
                        taskContext.Planogram.Components.Remove(subComponentPlacement.Component);
                    }

                    taskContext.Planogram.FixtureItems.Remove(fixtureItem);
                    taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayRemovedFromLocationSpace, fixtureItem.GetPlanogramFixture().Name);
                }
                
                //  Return success.
                return true;
            }
        }

        private void UpdateFixtureBayComponentWidths(TaskContext taskContext, PlanogramFixtureItem fixtureItem, Boolean includeShelf = true)
        {
            foreach (PlanogramSubComponentPlacement planogramSubComponentPlacement in
                                fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType != PlanogramComponentType.Base &&
                                                                                            p.Component.ComponentType != PlanogramComponentType.Backboard))
            {
                if (taskContext.PreviousBayWidth > 0)
                {
                    PlanogramFixture planogramFixture = fixtureItem.GetPlanogramFixture();

                    if (!includeShelf && planogramSubComponentPlacement.Component.ComponentType == PlanogramComponentType.Shelf) { continue; }

                    if (planogramSubComponentPlacement.Component.Width == taskContext.PreviousBayWidth)
                    {
                        planogramSubComponentPlacement.Component.Width = planogramFixture.Width;
                        planogramSubComponentPlacement.SubComponent.Width = planogramFixture.Width;
                    }
                }
            }
        }

        /// <summary>
        /// Creates the element profile for the fixture item from the previous bay
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="fixtureItem"></param>
        /// <param name="previousFixtureItem"></param>
        private void CreateFixtureBayShelfProfileFromPreviousBay(TaskContext taskContext, PlanogramFixtureItem fixtureItem, PlanogramFixtureItem previousFixtureItem)
        {
            PlanogramFixture previousPlanogramFixture = previousFixtureItem.GetPlanogramFixture();
            PlanogramFixture planogramFixture = fixtureItem.GetPlanogramFixture();

            taskContext.PreviousBayWidth = previousPlanogramFixture.Width;
            
            var merchandisableComponents = previousPlanogramFixture.Components.Where(p=> p.IsMerchandisibleComponent());
            foreach (PlanogramFixtureComponent planogramFixtureComponent in merchandisableComponents.OrderBy(p => p.Y))
            {
                var planogramComponent = planogramFixtureComponent.GetPlanogramComponent();
                var componentCopy = planogramComponent.Copy();
                componentCopy.SubComponents.Clear();
                taskContext.Planogram.Components.Add(componentCopy);

                var fixtureComponentCopy = planogramFixtureComponent.Copy();
                fixtureComponentCopy.PlanogramComponentId = componentCopy.Id;

                fixtureComponentCopy.X = planogramFixtureComponent.X;
                fixtureComponentCopy.Y = planogramFixtureComponent.Y;
                fixtureComponentCopy.Z = planogramFixtureComponent.Z;

                planogramFixture.Components.Add(fixtureComponentCopy);

                foreach (var planogramSubComponent in planogramComponent.SubComponents)
                {
                    PlanogramSubComponent subComponentModel = planogramSubComponent.Copy();
                    componentCopy.SubComponents.Add(subComponentModel);
                }

                if(planogramComponent.ComponentType == PlanogramComponentType.Shelf)
                {
                    componentCopy.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
                    planogramComponent.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
                }
                
                taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayElementCreatedFromLocationSpace, componentCopy.Name, previousPlanogramFixture.Name);
            }
        }
        
        /// <summary>
        /// Updates the planogram fixture to match the location space bay. Creates \ removes shelf components as required.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="locationSpaceBay"></param>
        /// <param name="fixtureItem"></param>
        /// <param name="xPosition"></param>
        private void UpdateFixtureBayShelfProfileFromLocationSpaceElements(TaskContext taskContext, LocationSpaceBay locationSpaceBay, PlanogramFixtureItem fixtureItem, PlanogramFixtureItem previousFixtureItem, Single xPosition)
        {
            Stack<PlanogramSubComponentPlacement> planogramSubComponentPlacements = new Stack<PlanogramSubComponentPlacement>();
            foreach (PlanogramSubComponentPlacement planogramSubComponentPlacement in fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).OrderByDescending(p => p.FixtureComponent.Y))
            {
                planogramSubComponentPlacements.Push(planogramSubComponentPlacement);
            }

            Single yPosition = locationSpaceBay.BaseHeight;
            Int32? shelfColour = null;
                        
            foreach (LocationSpaceElement locationSpaceElement in locationSpaceBay.Elements.OrderBy(p => p.Order))
            {
                PlanogramSubComponentPlacement planogramSubComponentPlacement = planogramSubComponentPlacements.Any() ? planogramSubComponentPlacements.Pop() : null;

                if (planogramSubComponentPlacement == null)
                {
                    if (shelfColour == null) { shelfColour = taskContext.Settings.ShelfFillColour; }
                    planogramSubComponentPlacement = AddShelf(taskContext, fixtureItem, fixtureItem.GetPlanogramFixture(), locationSpaceElement, (Int32)shelfColour);
                }
                else
                {
                    if (shelfColour == null) { shelfColour = planogramSubComponentPlacement.SubComponent.FillColourFront; }
                }

                UpdateShelf(taskContext, previousFixtureItem, locationSpaceElement, planogramSubComponentPlacement.FixtureComponent,
                                planogramSubComponentPlacement.Component, planogramSubComponentPlacement.SubComponent, xPosition, yPosition);

                // just stack the shelves on top of each other if the data for y position isnt deemed valid (zero)
                yPosition += (locationSpaceElement.Height + locationSpaceElement.MerchandisableHeight); 
            }

            // remove any remaining fixture components on the stack
            while (planogramSubComponentPlacements.Any())
            {
                PlanogramSubComponentPlacement planogramSubComponentPlacement = planogramSubComponentPlacements.Pop();
                taskContext.Planogram.Positions.RemoveList(planogramSubComponentPlacement.GetPlanogramPositions());
                fixtureItem.GetPlanogramFixture().Components.Remove(planogramSubComponentPlacement.FixtureComponent);
                taskContext.Planogram.Components.Remove(planogramSubComponentPlacement.Component);

                taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayElementRemovedFromLocationSpace, planogramSubComponentPlacement.Component.Name, fixtureItem.GetPlanogramFixture().Name);
            }
        }

        /// <summary>
        /// Creates shelf component from location space element data
        /// </summary>
        /// <param name="fixtureItem"></param>
        /// <param name="planogramFixture"></param>
        /// <param name="locationSpaceElement"></param>
        /// <returns>a new sub component placement</returns>
        private PlanogramSubComponentPlacement AddShelf(TaskContext taskContext, PlanogramFixtureItem fixtureItem, PlanogramFixture planogramFixture, LocationSpaceElement locationSpaceElement, Int32 shelfColour)
        {
            PlanogramFixtureComponent fixtureComponent = planogramFixture.Components.Add(PlanogramComponentType.Shelf,
                            (locationSpaceElement.Width > 0 && locationSpaceElement.Width <= planogramFixture.Width) ? locationSpaceElement.Width : planogramFixture.Width,
                            locationSpaceElement.Height > 0 ? locationSpaceElement.Height : taskContext.Settings.ShelfHeight, 
                            locationSpaceElement.Depth > 0 ? locationSpaceElement.Depth : taskContext.Settings.ShelfDepth, 
                            shelfColour);
            PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
            component.Name = String.Format(Message.UpdatePlanogramLocationSpace_DefaultShelfName, locationSpaceElement.Order);
            component.SubComponents[0].Name = component.Name;

            taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayElementCreatedFromLocationSpace, component.Name, planogramFixture.Name);
            return PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents[0], fixtureItem, fixtureComponent);
        }

        /// <summary>
        /// Updates a shelf component to location space element
        /// </summary>
        /// <param name="locationSpaceElement"></param>
        /// <param name="shelfFixtureComponent"></param>
        /// <param name="shelfComponent"></param>
        /// <param name="shelfSubComponent"></param>
        /// <param name="xPosition"></param>
        /// <param name="yPosition"></param>
        private void UpdateShelf(TaskContext taskContext, PlanogramFixtureItem previousFixtureItem, LocationSpaceElement locationSpaceElement,PlanogramFixtureComponent shelfFixtureComponent, PlanogramComponent shelfComponent,
                                    PlanogramSubComponent shelfSubComponent, Single xPosition, Single yPosition)
        {
            String updatedProperties = String.Empty;
            String updatedValues = String.Empty;

            // size the shelf

            //Width
            if (!taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.WidthProperty.Name) && locationSpaceElement.Width > 0 && (locationSpaceElement.Width != shelfComponent.Width))
            {
                shelfComponent.Width = locationSpaceElement.Width;
                shelfSubComponent.Width = locationSpaceElement.Width;
                updatedProperties = LocationSpaceElement.WidthProperty.FriendlyName + ",";
                updatedValues = locationSpaceElement.Width.ToString() + ",";
            }

            //If we are ignoring the shelf width then set up the shelf with to be the same as the bay width
            if (taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.WidthProperty.Name))
            {
                shelfComponent.Width = locationSpaceElement.Parent.Width;
                shelfSubComponent.Width = locationSpaceElement.Parent.Width;
                updatedProperties = LocationSpaceElement.WidthProperty.FriendlyName + ",";
                updatedValues = locationSpaceElement.Parent.Width.ToString() + ",";
            }

            //Height
            if (!taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.HeightProperty.Name) && locationSpaceElement.Height > 0 && (locationSpaceElement.Height != shelfComponent.Height))
            {
                shelfComponent.Height = locationSpaceElement.Height;
                shelfSubComponent.Height = locationSpaceElement.Height;
                updatedProperties += LocationSpaceElement.HeightProperty.FriendlyName + ",";
                updatedValues += locationSpaceElement.Height.ToString() + ",";
            }

            //Depth
            if (!taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.DepthProperty.Name) && locationSpaceElement.Depth > 0 && (locationSpaceElement.Depth != shelfComponent.Depth))
            {
                shelfComponent.Depth = locationSpaceElement.Depth;
                shelfSubComponent.Depth = locationSpaceElement.Depth;
                updatedProperties += LocationSpaceElement.DepthProperty.FriendlyName + ",";
                updatedValues += locationSpaceElement.Depth.ToString() + ",";
            }

            //MerchandisableHeight
            if (!taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.MerchandisableHeightProperty.Name) && locationSpaceElement.MerchandisableHeight > 0 && (locationSpaceElement.MerchandisableHeight != shelfSubComponent.MerchandisableHeight))
            {
                shelfSubComponent.MerchandisableHeight = locationSpaceElement.MerchandisableHeight;
                updatedProperties += LocationSpaceElement.MerchandisableHeightProperty.FriendlyName + ",";
                updatedValues += locationSpaceElement.MerchandisableHeight.ToString() + ",";
            }

            // position the shelf
            Single yPos = locationSpaceElement.Y == 0 ? yPosition : locationSpaceElement.Y;
            if (previousFixtureItem != null)
            {
                if (yPos != shelfFixtureComponent.Y) // Y position has changed validate if it is placed next to a shelf that can now combine
                {
                    PlanogramSubComponentPlacement previousShelfToCombine = previousFixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf && p.FixtureComponent.Y == yPos).FirstOrDefault();
                    if (previousShelfToCombine != null && previousShelfToCombine.SubComponent != null)
                    {
                        previousShelfToCombine.SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                        shelfSubComponent.CombineType = PlanogramSubComponentCombineType.Both;
                    }
                }
            }

            //Y
            if (!taskContext.ShelfProfileAttributes.Contains(LocationSpaceElement.YProperty.Name))
            {
                if (locationSpaceElement.Y > 0 && locationSpaceElement.Y != shelfFixtureComponent.Y)
                {
                    shelfFixtureComponent.Y = locationSpaceElement.Y == 0 ? yPosition : locationSpaceElement.Y;
                    updatedProperties += LocationSpaceElement.YProperty.FriendlyName + ",";
                    updatedValues += locationSpaceElement.Y.ToString() + ",";
                }
                else
                {
                    shelfFixtureComponent.Y = locationSpaceElement.Y == 0 ? yPosition : locationSpaceElement.Y;
                }
            }

            if (!String.IsNullOrEmpty(updatedProperties))
            {
                updatedProperties = updatedProperties.Substring(0, updatedProperties.Length - 1);
                updatedValues = updatedValues.Substring(0, updatedValues.Length - 1);
                taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayElementUpdatedFromLocationSpace, shelfComponent.Name, taskContext.CurrentFixture.Name, updatedProperties, updatedValues);
            }
        }

        /// <summary>
        /// Creates fixture item from location space bay data, if no location space bay data (height width depth) then values are obtained from the 
        /// previous bay else falls back to the system settings for a new bay. 
        /// Note that base is only created if there is data to drive it.
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="locationSpaceBay"></param>
        /// <param name="xPosition"></param>
        /// <returns>a new fixture item</returns>
        public PlanogramFixtureItem AddFixtureItem(TaskContext taskContext, LocationSpaceBay locationSpaceBay, Single xPosition, Int32? backBoardColour, Int32? baseColour, Single? backBoardDepth)
        {
            PlanogramFixture planogramFixture = taskContext.Planogram.Fixtures.Add();
            planogramFixture.Width = (locationSpaceBay.Width > 0) ? locationSpaceBay.Width : // take value from location space if available
                                    (taskContext.CurrentFixture != null && taskContext.CurrentFixture.Width > 0) ? taskContext.CurrentFixture.Width : // if not from previous fixture
                                    planogramFixture.Width = taskContext.Settings.FixtureWidth; // else settings

            planogramFixture.Height = (locationSpaceBay.Height > 0) ? locationSpaceBay.Height : // take value from location space if available
                                    (taskContext.CurrentFixture != null && taskContext.CurrentFixture.Height > 0) ? taskContext.CurrentFixture.Height : // if not from previous fixture
                                    planogramFixture.Height = taskContext.Settings.FixtureHeight; // else settings

            planogramFixture.Depth = (locationSpaceBay.Depth > 0) ? locationSpaceBay.Depth : // take value from location space if available
                                    (taskContext.CurrentFixture != null && taskContext.CurrentFixture.Depth > 0) ? taskContext.CurrentFixture.Depth : // if not from previous fixture
                                    planogramFixture.Depth = taskContext.Settings.FixtureDepth; // else settings

            planogramFixture.Name = String.IsNullOrEmpty(locationSpaceBay.FixtureName) ? String.Format(Message.UpdatePlanogramLocationSpace_DefaultBayName, locationSpaceBay.Order) : locationSpaceBay.FixtureName;
            taskContext.CurrentFixture = planogramFixture;

            PlanogramFixtureItem planogramFixtureItem = taskContext.Planogram.FixtureItems.Add(taskContext.CurrentFixture);
            planogramFixtureItem.X = xPosition;

            taskContext.CurrentFixtureComponentBackboard = taskContext.CurrentFixture.Components.Add(PlanogramComponentType.Backboard,
                                                    planogramFixture.Width,
                                                    planogramFixture.Height,
                                                    backBoardDepth == null ? taskContext.Settings.BackboardDepth : (Single)backBoardDepth, 
                                                    backBoardColour == null ? -2894893 : (Int32)backBoardColour);

            PlanogramComponent backboardComponent = taskContext.CurrentFixtureComponentBackboard.GetPlanogramComponent();
            PlanogramSubComponent backboardSubComponent = backboardComponent.SubComponents[0];

            backboardSubComponent.NotchSpacingY = locationSpaceBay.NotchPitch == null ? backboardSubComponent.NotchSpacingY : (Single)locationSpaceBay.NotchPitch;
            backboardSubComponent.NotchStartY = locationSpaceBay.NotchStartY;

            taskContext.CurrentFixtureComponentBase = null; // Base only created if there is location space bay data to drive it.
            if (locationSpaceBay.Height > 0 && locationSpaceBay.BaseWidth > 0 && locationSpaceBay.BaseDepth > 0)
            {
                taskContext.CurrentFixtureComponentBase = taskContext.CurrentFixture.Components.Add(PlanogramComponentType.Base, locationSpaceBay.BaseWidth, locationSpaceBay.BaseHeight, locationSpaceBay.BaseDepth, baseColour == null ? -5658199 : (Int32)baseColour);
            }

            UpdateFixture(taskContext, planogramFixtureItem, locationSpaceBay, xPosition);

            taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayCreatedFromLocationSpace, taskContext.CurrentFixture.Name);
            return planogramFixtureItem;
        }

        /// <summary>
        /// Updates the fixture to location space bay data
        /// </summary>
        /// <param name="planogramFixtureItem"></param>
        /// <param name="planogramFixture"></param>
        /// <param name="locationSpaceBay"></param>
        /// <param name="backboardFixtureComponent"></param>
        /// <param name="baseFixtureComponent"></param>
        /// <param name="xPosition"></param>
        private void UpdateFixture(TaskContext taskContext, PlanogramFixtureItem planogramFixtureItem, LocationSpaceBay locationSpaceBay, float xPosition)
        {
            String updatedProperties = String.Empty;
            String updatedValues = String.Empty;

            if (locationSpaceBay.Height > 0 && locationSpaceBay.Height != taskContext.CurrentFixture.Height)
            {
                taskContext.CurrentFixture.Height = locationSpaceBay.Height;
                updatedProperties = LocationSpaceBay.HeightProperty.FriendlyName + ",";
                updatedValues = locationSpaceBay.Height.ToString() + ",";
            }

            if (locationSpaceBay.Width > 0 && locationSpaceBay.Width != taskContext.CurrentFixture.Width)
            {
                taskContext.PreviousBayWidth = taskContext.CurrentFixture.Width;
                taskContext.CurrentFixture.Width = locationSpaceBay.Width;
                updatedProperties += LocationSpaceBay.WidthProperty.FriendlyName + ",";
                updatedValues += locationSpaceBay.Width.ToString() + ",";
            }

            taskContext.CurrentFixture.Depth = locationSpaceBay.Depth > 0 ? locationSpaceBay.Depth : taskContext.CurrentFixture.Depth;
            if (locationSpaceBay.Depth > 0 && locationSpaceBay.Depth != taskContext.CurrentFixture.Depth)
            {
                taskContext.CurrentFixture.Depth = locationSpaceBay.Depth;
                updatedProperties += LocationSpaceBay.DepthProperty.FriendlyName + ",";
                updatedValues += locationSpaceBay.Depth.ToString() + ",";
            }
            
            planogramFixtureItem.X = xPosition;

            if (taskContext.CurrentFixtureComponentBackboard != null)
            {
                PlanogramComponent backboardComponent = taskContext.CurrentFixtureComponentBackboard.GetPlanogramComponent();
                PlanogramSubComponent backboardSubComponent = backboardComponent.SubComponents[0];

                // size the backboard
                backboardComponent.Width = taskContext.CurrentFixture.Width;
                backboardComponent.Height = taskContext.CurrentFixture.Height;
                backboardSubComponent.Width = taskContext.CurrentFixture.Width;
                backboardSubComponent.Height = taskContext.CurrentFixture.Height;

                if ((locationSpaceBay.NotchPitch != null && locationSpaceBay.NotchPitch > 0) && locationSpaceBay.NotchPitch != backboardSubComponent.NotchSpacingY)
                {
                    backboardSubComponent.NotchSpacingY = (Single)locationSpaceBay.NotchPitch;
                    updatedProperties += LocationSpaceBay.NotchPitchProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.NotchPitch.ToString() + ",";
                }

                if ((locationSpaceBay.NotchStartY > 0) && locationSpaceBay.NotchStartY != backboardSubComponent.NotchStartY)
                {
                    backboardSubComponent.NotchStartY = locationSpaceBay.NotchStartY;
                    updatedProperties += LocationSpaceBay.NotchStartYProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.NotchStartY.ToString() + ",";
                }
                
                // position the backboard
                taskContext.CurrentFixtureComponentBackboard.X = 0;
                taskContext.CurrentFixtureComponentBackboard.Y = 0;
                taskContext.CurrentFixtureComponentBackboard.Z = -backboardComponent.Depth;
            }

            if (taskContext.CurrentFixtureComponentBase != null)
            {
                PlanogramComponent baseComponent = taskContext.CurrentFixtureComponentBase.GetPlanogramComponent();
                PlanogramSubComponent baseSubComponent = baseComponent.SubComponents[0];

                // size the base
                if (locationSpaceBay.BaseWidth > 0 && baseComponent.Width != locationSpaceBay.BaseWidth)
                {
                    baseComponent.Width = locationSpaceBay.BaseWidth;
                    baseSubComponent.Width = locationSpaceBay.BaseWidth;
                    updatedProperties += LocationSpaceBay.BaseWidthProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.BaseWidth.ToString() + ",";
                }
                //If BaseWidth is equal to 0 and the bay width greater than 0 then update the basewidth to mach the bay width instead of keep the original basewidth value.
                else if (locationSpaceBay.BaseWidth == 0 && locationSpaceBay.Width > 0) {  
                    baseComponent.Width = locationSpaceBay.Width;
                    baseSubComponent.Width = locationSpaceBay.Width;
                    updatedProperties += LocationSpaceBay.BaseWidthProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.Width.ToString() + ",";
                }
                if (locationSpaceBay.BaseHeight > 0 && baseComponent.Height != locationSpaceBay.BaseHeight)
                {
                    baseComponent.Height = locationSpaceBay.BaseHeight;
                    baseSubComponent.Height = locationSpaceBay.BaseHeight;
                    updatedProperties += LocationSpaceBay.BaseHeightProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.BaseHeight.ToString() + ",";
                }
                if (locationSpaceBay.BaseDepth > 0 && baseComponent.Depth != locationSpaceBay.BaseDepth)
                {
                    baseComponent.Depth = locationSpaceBay.BaseDepth;
                    baseSubComponent.Depth = locationSpaceBay.BaseDepth;
                    updatedProperties += LocationSpaceBay.BaseDepthProperty.FriendlyName + ",";
                    updatedValues += locationSpaceBay.BaseDepth.ToString() + ",";
                }
            }

            if (!String.IsNullOrEmpty(updatedProperties))
            {
                updatedProperties = updatedProperties.Substring(0, updatedProperties.Length - 1);
                updatedValues = updatedValues.Substring(0, updatedValues.Length - 1);
                taskContext.LogInformation(EventLogEvent.PlanogramFixtureBayUpdatedFromLocationSpace, taskContext.CurrentFixture.Name, updatedProperties, updatedValues);
            }
        }

        #endregion
    }
}