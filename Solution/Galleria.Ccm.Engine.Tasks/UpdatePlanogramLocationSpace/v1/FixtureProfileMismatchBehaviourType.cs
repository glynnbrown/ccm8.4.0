﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31562 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     fixture profile mismatch parameter.
    /// </summary>
    public enum FixtureProfileMismatchBehaviourType
    {
        Fail,
        EnforceLocationSpace
    }

    /// <summary>
    ///     Helpers for <see cref="FixtureProfileMismatchBehaviourType"/>.
    /// </summary>
    public static class FixtureProfileMismatchBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="FixtureProfileMismatchBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<FixtureProfileMismatchBehaviourType, String> FriendlyNames = new Dictionary
            <FixtureProfileMismatchBehaviourType, String>
        {
            {FixtureProfileMismatchBehaviourType.Fail, Message.FixtureProfileMismatchBehaviourType_Fail},
            {FixtureProfileMismatchBehaviourType.EnforceLocationSpace, Message.FixtureProfileMismatchBehaviourType_EnforceLocationSpace} 
        };
    }
}