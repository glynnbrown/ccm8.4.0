﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31562 : D.Pleasance
//  Created.
#endregion

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion

        #region Properties

        public Boolean IsUpdatingBayProfile { get; set; }
        public Boolean IsUpdatingShelfProfile { get; set; }
        public List<String> ShelfProfileAttributes { get; set; }
        public FixtureProfileMismatchBehaviourType ProfileMismatchBehaviour { get; set; }
        public LocationSpaceProductGroup LocationSpaceProductGroup { get; set; }

        public PlanogramFixture CurrentFixture { get; set; }
        public PlanogramFixtureComponent CurrentFixtureComponentBackboard { get; set; }
        public PlanogramFixtureComponent CurrentFixtureComponentBase { get; set; }

        /// <summary>
        /// The Settings to use when constructing shelves and fixtures.
        /// </summary>
        public IPlanogramSettings Settings { get; set; }

        public Single PreviousBayWidth { get; set; }

        #endregion
    }
}
