﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29520 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1
{
    /// <summary>
    /// Task context object used to pass parameters
    /// throughout the execution of the task
    /// </summary>
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        public String _assortmentName; // holds the assortment name
        public Assortment _assortment; // holds the loaded assortment
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the assortment name
        /// </summary>
        public String AssortmentName
        {
            get { return _assortmentName; }
            set { _assortmentName = value; }
        }

        /// <summary>
        /// Gets or sets the assortment
        /// </summary>
        public Assortment Assortment
        {
            get { return _assortment; }
            set { _assortment = value; }
        }
        #endregion
    }
}
