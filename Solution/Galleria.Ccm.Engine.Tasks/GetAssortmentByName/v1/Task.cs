﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26746 : N.Foster
//  Created
// V8-27241 : A.Kuszyk
//  Changed fetch method to EntityIdName.
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-28060 : A.Kuszyk
//  Updated level of detail in logging.
// V8-28608 : A.Kuszyk
//  Added validation for a null parameter value.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM803
// V8-29520 : N.Foster
//  Ensure assortment products are added to planogram product list
// V8-29741 : A.Silva
//  Amended PopulatePlanogramProductList to not add existing product GTINs.

#endregion
#region Version History: CCM810
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
#endregion
#region Version History: CCM820
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1
{
    /// <summary>
    /// Gets an assortment from master data and copies
    /// the assortment details into the planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            AssortmentName = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetAssortmentByName_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetAssortmentByName_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // assortment Name
            this.RegisterParameter(new TaskParameter(
                Parameter.AssortmentName,
                Message.GetAssortmentByName_Parameter_AssortmentName_Name,
                Message.GetAssortmentByName_Parameter_AssortmentName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.AssortmentNameSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetAssortment(taskContext)) return;
                if (!LoadAssortment(taskContext)) return;
                if (!PopulatePlanogramProductList(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameter values for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region AssortmentName

                // validate that the assortment name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AssortmentName)) ||
                    (this.Parameters[Parameter.AssortmentName].Value == null) ||
                    (this.Parameters[Parameter.AssortmentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetAssortmentByName_Parameter_AssortmentName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the assortment name
                    context.AssortmentName = Convert.ToString(this.Parameters[Parameter.AssortmentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.AssortmentName)) || (String.IsNullOrWhiteSpace(context.AssortmentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetAssortmentByName_Parameter_AssortmentName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Loads the assortment from the datatabase
        /// </summary>
        private static Boolean GetAssortment(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to fetch the assortment by its name
                try
                {
                    context.Assortment = Assortment.FetchByEntityIdName(context.EntityId, context.AssortmentName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetAssortmentByName_Parameter_AssortmentName_Name, context.AssortmentName);
                        return false;
                    }
                    throw ex;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Loads the assortment into the planogram
        /// </summary>
        private static Boolean LoadAssortment(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // replace the assortment within the planogram
                context.Planogram.Assortment.ReplaceAssortment(context.Assortment);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Populates the planogram product list with assortment products
        /// </summary>
        private static Boolean PopulatePlanogramProductList(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of all product codes in the planogram
                List<String> planogramProductCodes = new List<String>();
                foreach (PlanogramProduct product in context.Planogram.Products)
                {
                    planogramProductCodes.Add(product.Gtin);
                }

                // build a list of all product codes within the assortment
                List<String> assortmentProductCodes = new List<String>();
                foreach (PlanogramAssortmentProduct product in context.Planogram.Assortment.Products)
                {
                    if ((!planogramProductCodes.Contains(product.Gtin)) && (!assortmentProductCodes.Contains(product.Gtin)))
                        assortmentProductCodes.Add(product.Gtin);
                }
                foreach (PlanogramAssortmentLocalProduct product in context.Planogram.Assortment.LocalProducts)
                {
                    if ((!planogramProductCodes.Contains(product.ProductGtin)) && (!assortmentProductCodes.Contains(product.ProductGtin)))
                        assortmentProductCodes.Add(product.ProductGtin);
                }

                // we now have a list of all products that need adding to the planogram
                // add these products to the planogram product list
                List<PlanogramProduct> planogramProducts = new List<PlanogramProduct>();
                foreach (Product product in ProductList.FetchByEntityIdProductGtins(context.EntityId, assortmentProductCodes))
                {
                    planogramProducts.Add(PlanogramProduct.NewPlanogramProduct(product));
                }
                context.Planogram.Products.AddList(planogramProducts);

                // ensure these products have performance data within the planogram
                context.Planogram.Performance.PerformanceData.AddList(planogramProducts);
                
                // return success
                return true;
            }
        }

        /// <summary>
        /// Logs that the task has completed successfully
        /// </summary>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // log that the assortment was successfully applied
                context.LogInformation(EventLogEvent.PlanogramAssortmentReplaced, context.AssortmentName);

                // return success
                return true;
            }
        }

        #endregion
    }
}
