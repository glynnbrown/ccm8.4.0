using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Processes.ProductAttributeValidation;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.ProductAttrbiuteValidation.V1
{
    public class Task : TaskBase
    {
        public Task()
        {
           
        }

        private IAttributeValidationEngine _processor;
        public IAttributeValidationEngine Processor {
            get {
                return _processor;
            }
            set { _processor = value; }
        }

        private ITaskContext _taskContext;

        public override String Name => "Refresh Product Attribute Validation";
        public override String Description => "Runs the product attribute validation system which will provide important information about the products in the planogram to enable the user to determine if it meets the users requirements.";
        public override String Category => Message.TaskCategory_PlanogramMaintenance;
        public override Byte MinorVersion => 0;
        public override Byte MajorVersion => 1;

        protected override void OnRegisterParameters()
        {
            
        }

        protected override void OnExecute(ITaskContext context)
        {
            CheckSingleExecution();

            _taskContext = context;
            Entity currentEntity = Entity.FetchById(_taskContext.EntityId);
            if (currentEntity == null) throw new InvalidOperationException($"No entity can be found for {_taskContext.EntityId}");

            if (context == null) return;
            if (_processor == null) _processor = new AttributeValidationEngine();

            using (new CodePerformanceMetric())
            {
                if (!CheckThatTheAssortIsValidAndContainsProducts()) return;
                ValidateProductAttributes(currentEntity);
            }
        }


        private void ValidateProductAttributes(Entity currentEntity)
        {
            _processor.ValidateProductAttributes(_taskContext.Planogram, currentEntity);
        }

        [Conditional("DEBUG")]
        private void CheckSingleExecution()
        {
            if (_taskContext != null)
                System.Diagnostics.Debug.Fail(
                    "Tasks should be executed only once per instance and this instance was executed already. The Task may have previous values hanging around.");
        }

        private Boolean CheckThatTheAssortIsValidAndContainsProducts()
        {
            using (new CodePerformanceMetric())
            {
                #region Validate Assortment

                //  Verify the assortment contains products.
                if (_taskContext.Planogram.Products == null ||
                    !_taskContext.Planogram.Products.Any())
                {
                    _taskContext.LogError(EventLogEvent.InsufficientPlanogramContent,
                                                           Message.TaskLogging_PlanogramProduct,
                                                           Message.TaskLogging_NoProductsInPlanogram);
                    return false;
                }

                #endregion

                return true;
            }
        }
    }
}