﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29723 : D.Pleasance
//  Created
#endregion
#region Version History : CCM820
// V8-30948 : A.Kuszyk
//  Added Product Not Found Action parameter.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1
{
    /// <summary>
    ///     Provides the context to run <see cref="Task"/> in.
    /// </summary>
    public class TaskContext : Ccm.Engine.TaskContext
    {
        private List<ReplacementProductSelection> _replacementProductDetails = new List<ReplacementProductSelection>();
        private String _carParkComponentName; // holds the car park component name
        private PlanogramMerchandisingGroup _carParkMerchandisingGroup; // holds the car park merchandising group
        private Boolean _addCarParkTextBox;

        #region Constructor

        /// <summary>
        ///     Instantiates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) { }

        #endregion

        #region Properties

        /// <summary>
        /// The action to take with the replacement product if the product being replaced isn't placed on the plan.
        /// </summary>
        public ProductNotFoundActionType ProductNotFoundAction { get; set; }

        /// <summary>
        ///     Gets or sets whether to cancel any pending edits on disposal of this instance.
        /// </summary>
        public Boolean CancelEditOnExit { get; set; }

        /// <summary>
        ///     Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get; set; }

        /// <summary>
        ///     Gets or sets the behaviour when coming across dropped products.
        /// </summary>
        public DroppedProductBehaviourType DroppedProductBehaviour { get; set; }

        /// <summary>
        ///     Gets or sets how to place products when inserting them.
        /// </summary>
        public PlacementType ProductPlacement { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> that the task should try to replace.
        /// </summary>
        public List<PlanogramProduct> ReplacementProducts { get; set; }

        // get the positions to replace
        public IEnumerable<PlanogramPositionPlacement> ReplacePositions { get; set; }

        /// <summary>
        ///     Gets or sets the list of <see cref="PlanogramProduct"/> that were not initially in the Planogram.
        /// </summary>
        public List<PlanogramProduct> MissingProducts { get; set; }

        /// <summary>
        ///     Gets or sets the list of replace <see cref="Product"/> that were not on the Planogram.
        /// </summary>
        public ProductList MissingReplaceProducts { get; set; }

        /// <summary>
        ///     Gets or sets the current replacement product
        /// </summary>
        public PlanogramProduct ReplacementProduct { get; set; }

        /// <summary>
        ///     Gets or sets the replacement product detail (replacement product gtin, replacement product gtin).
        /// </summary>
        internal List<ReplacementProductSelection> ReplacementProductDetails
        {
            get
            {
                return _replacementProductDetails;
            }
        }

        /// <summary>
        ///     Gets sets the number of products that have been replaced
        /// </summary>
        public Int32 ReplaceCount { get; set; }

        /// <summary>
        ///     Gets sets the current replace product Gtin
        /// </summary>
        public String CurrentReplaceProductGtin { get; set; }

        /// <summary>
        ///     Gets sets the current replacement product Gtin
        /// </summary>
        public String CurrentReplacementProductGtin { get; set; }
        
        /// <summary>
        ///     Gets sets the number of products inserted to the carpark shelf
        /// </summary>
        public Int32 CarParkShelfInsertCount { get; set; }

        /// <summary>
        /// Gets or sets the car park component name
        /// </summary>
        public String CarParkComponentName
        {
            get { return _carParkComponentName; }
            set { _carParkComponentName = value; }
        }

        /// <summary>
        /// Gets/Sets whether the carpark shelf should have a linked textbox.
        /// </summary>
        public Boolean AddCarParkTextBox
        {
            get { return _addCarParkTextBox; }
            set { _addCarParkTextBox = value; }
        }

        /// <summary>
        /// Gets or sets the car park merchandising group
        /// </summary>
        public PlanogramMerchandisingGroup CarParkMerchandisingGroup
        {
            get { return _carParkMerchandisingGroup; }
            set { _carParkMerchandisingGroup = value; }
        }

        #endregion

        #region Methods
        /// <summary>
        ///     Called when this instance is being disposed
        /// </summary>
        protected override void OnDipose()
        {
            if (MerchandisingGroups != null)
            {
                if (!CancelEditOnExit)
                {
                    foreach (PlanogramMerchandisingGroup merchandisingGroup in MerchandisingGroups)
                    {
                        merchandisingGroup.Process();
                        merchandisingGroup.ApplyEdit();
                    }
                }
                MerchandisingGroups.Dispose();
                MerchandisingGroups = null;
            }

            base.OnDipose();
        }

        #endregion
    }

    /// <summary>
    /// A simple container for the attribute source and name values.
    /// </summary>
    internal class ReplacementProductSelection
    {
        public String ReplaceProductGtin { get; private set; }        
        public String ReplacementProductGtin { get; private set; }

        public ReplacementProductSelection(String replaceProductGtin, String replacementProductGtin)
        {
            ReplaceProductGtin = replaceProductGtin;
            ReplacementProductGtin = replacementProductGtin;
        }
    }
}
