﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29723 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1
{
    /// <summary>
    ///     Enumerates the possible values for the
    ///     Unplaced Products parameter.
    /// </summary>
    public enum DroppedProductBehaviourType
    {
        AddToCarparkShelf,
        DoNotAdd,
        AlwaysAdd
    }

    /// <summary>
    ///     Helpers for <see cref="DroppedProductBehaviourType"/>.
    /// </summary>
    public static class DroppedProductBehaviourTypeHelper
    {
        /// <summary>
        ///     Maps the friendly names of each value in <see cref="DroppedProductBehaviourType"/>.
        /// </summary>
        public static readonly Dictionary<DroppedProductBehaviourType, String> FriendlyNames = new Dictionary
            <DroppedProductBehaviourType, String>
        {
            {DroppedProductBehaviourType.AddToCarparkShelf, Message.DroppedProductBehaviourType_AddToCarParkComponent},
            {DroppedProductBehaviourType.DoNotAdd, Message.DroppedProductBehaviourType_DoNotAdd},
            {DroppedProductBehaviourType.AlwaysAdd, Message.DroppedProductBehaviourType_AlwaysAdd}
        };
    }
}