﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29723 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1
{
    /// <summary>
    /// Denotes the possible values for the Placement Type parameter.
    /// </summary>
    public enum PlacementType
    {
        SingleFacing,
        FillAvailableSpace
    }

    /// <summary>
    /// Contains helpers for the PlacementType enum.
    /// </summary>
    public static class PlacementTypeHelper
    {
        public static readonly Dictionary<PlacementType, String> FriendlyNames =
            new Dictionary<PlacementType, String>()
            {
                {PlacementType.SingleFacing, Message.PlacementType_SingleFacing },
                {PlacementType.FillAvailableSpace, "Fill Available Space" }
            };
    }
}