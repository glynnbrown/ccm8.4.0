﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM803
// V8-29723 : D.Pleasance
//  Created
#endregion
#region Version History : CCM810
// V8-29768: A.Silva
//  Refactored UpdateSequenceData call as it is now in the base TaskContext.
// V8-29902 : A.Silva
//  Refactored how the task increases units.
// V8-30144 : D.Pleasance
//  Amended so that MerchandisingGroups are obtained on the task context and disposed when the task is completed. This is to ensure that positions 
//  are not removed when previously looking up the car park shelf. Previously the car park shelf was found, then dispose was called immediately 
//  after which resulting in existing car park placement positions being removed.
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30254 : M.Brumby
//  EventLogEvent.ReplacementProductCouldNotBeAdded changed to log as warning
#endregion
#region Version History : CCM811
// V8-30378 : M.Brumby
//  ReplacementProductAlreadyOnPlanogram, ReplacementProductDoesNotExist, ReplaceProductDoesNotExist changed to warnings
//  No longer fail validation if there are no products to replace (as it will fail entire workflows for no good reason)
//  Log results even if there are no changes.
#endregion
#region Version History : CCM820
// V8-30948 : A.Kuszyk
//  Added Product Not Found Action parameter.
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added new parameter AddCarParkTextBox
// V8-32578 : A.Heathcote
//  Added validation to the "RegisterParameter" CarParkComponentName
//  Added the IsCarParkNameValid() method.
// CCM-18435 : A.Silva
//  Refactored by pulling up HasOverlap to PlanogramMerchandisingGroup.HasCollisionOverlapping.
//  Removed the check for IsProductOverlapAllowed. Automation should not overlap as that is a manual decision.
// CCM-13932 : D.Pleasance
//  Added logic to satisfy PlacementType.FillAvailableSpace when option DroppedProductBehaviourType.AlwaysAdd is applied.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1
{
    /// <summary>
    /// Replaces products on the planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        /// <summary>
        /// V8-29723 : 0 - Created.
        /// V8-30948 : 1 - Product Not Found Action parameter added.
        /// </summary>
        private Byte _minorVersion = 1; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ProductCodeAndReplaceProductCode = 0,
            ProductPlacement = 1,
            DroppedProductBehaviour = 2,
            CarParkComponentName = 3,
            ProductNotFoundAction = 4,
            AddCarParkTextBox = 5,
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.ReplaceProductManual_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.ReplaceProductManual_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Product Code and replace product code
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductCodeAndReplaceProductCode,
                Message.ReplaceProductManual_ProductCodeAndReplaceProductCode_Name,
                Message.ReplaceProductManual_ProductCodeAndReplaceProductCode_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductCodeAndReplaceProductCodeMultiple,
                null,
                false,
                false));

            // Product placement
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductPlacement,
                Message.ReplaceProductManual_ProductPlacement,
                Message.ReplaceProductManual_ProductPlacement_Description,
                Message.ParameterCategory_Default,
                typeof(PlacementType),
                PlacementType.FillAvailableSpace,
                false,
                false));

            // Dropped product behaviour
            this.RegisterParameter(new TaskParameter(
                Parameter.DroppedProductBehaviour,
                Message.ReplaceProductManual_DroppedProductBehaviour_Name,
                Message.ReplaceProductManual_DroppedProductBehaviour_Description,
                Message.ParameterCategory_Default,
                typeof(DroppedProductBehaviourType),
                DroppedProductBehaviourType.DoNotAdd,
                false,
                false));

            // car park component name
            this.RegisterParameter(new TaskParameter(
                Parameter.CarParkComponentName,
                Message.ReplaceProductManual_CarParkComponentName_Name,
                Message.ReplaceProductManual_CarParkComponentName_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.String,
                Message.ReplaceProductManual_CarParkComponentName_Default,
                true,
                true,
                null,
                null,
                null,
                () => IsCarParkNameValid()));

            this.RegisterParameter(new TaskParameter(
                Parameter.ProductNotFoundAction,
                Message.ReplaceProductManual_ProductNotFoundAction_Name, 
                Message.ReplaceProductManual_ProductNotFoundAction_Description,
                Message.ParameterCategory_Default,
                typeof(ProductNotFoundActionType),
                ProductNotFoundActionType.AddReplacementToProductList,
                false,
                false));

            //Add car park textbox
            this.RegisterParameter(new TaskParameter(
                Parameter.AddCarParkTextBox,
                Message.ReplaceProductManual_Parameter_AddCarParkTextBox_Name,
                Message.ReplaceProductManual_Parameter_AddCarParkTextBox_Description,
                Message.ParameterCategory_Default,
                enumType: typeof(AddCarParkTextBoxType),
                defaultValue: AddCarParkTextBoxType.No,
                isHiddenByDefault: true,
                isReadOnlyByDefault: true));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {                
                if (!ValidateParameters(taskContext)) return;
                if (!GetReplaceProductDetails(taskContext)) return;
                if (!GetMerchandisingGroups(taskContext)) return;
                if (!ReplaceProducts(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }            
        }
                
        /// <summary>
        /// Validate task parameters
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region ProductCodeAndReplaceProductCode

                //  Validate that the Product Code and Replace Product code parameter exists.
                if (this.Parameters[Parameter.ProductCodeAndReplaceProductCode].Value != null)
                {
                    try
                    {
                        context.ReplacementProductDetails.AddRange(this.Parameters[Parameter.ProductCodeAndReplaceProductCode].Values.
                                                                    Select(item => new ReplacementProductSelection(item.Value1.ToString(), item.Value2.ToString())).ToList());
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                        Message.ReplaceProductManual_ProductCodeAndReplaceProductCode_Name,
                                        Message.Error_InvalidParameterValue);
                        return false;
                    }
                }

                #endregion

                #region DroppedProductBehaviour

                //  Validate that the Dropped Product Behaviour parameter exists.
                if (Parameters[Parameter.DroppedProductBehaviour].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProduct_DroppedProductBehaviour_Name,
                                     Message.Error_NoValue);
                    return false;
                }
                else
                {
                    try
                    {
                        context.DroppedProductBehaviour =
                            (DroppedProductBehaviourType)Parameters[Parameter.DroppedProductBehaviour].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProduct_DroppedProductBehaviour_Name,
                                         Parameters[Parameter.DroppedProductBehaviour].Value.Value1);

                        return false;
                    }
                }

                #endregion

                #region ProductPlacement

                //  Validate that the Product Placement parameter exists.
                if (Parameters[Parameter.ProductPlacement].Value == null)
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.InsertProductFromAssortment_ProductPlacement,
                                     Message.Error_NoValue);
                    return false;
                }
                else
                {
                    try
                    {
                        context.ProductPlacement =
                            (PlacementType)Parameters[Parameter.ProductPlacement].Value.Value1;
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                         Message.InsertProductFromAssortment_ProductPlacement,
                                         Parameters[Parameter.ProductPlacement].Value.Value1);
                        return false;
                    }
                }

                #endregion

                #region Car Park Component Name

                // validate that the car park shelf name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.CarParkComponentName)) ||
                    (this.Parameters[Parameter.CarParkComponentName].Value == null) ||
                    (this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                    return false;
                }
                else
                {
                    // get the car park shelf name
                    context.CarParkComponentName = Convert.ToString(this.Parameters[Parameter.CarParkComponentName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.CarParkComponentName)) || (String.IsNullOrWhiteSpace(context.CarParkComponentName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.AddProductFromAssortment_Parameter_CarParkComponentName_Name, Message.Error_NoValue);
                        return false;
                    }
                }

                #endregion

                #region Product Not Found Action

                if (!this.Parameters.Contains((Int32)Parameter.ProductNotFoundAction) ||
                    this.Parameters[Parameter.ProductNotFoundAction].Value == null ||
                    this.Parameters[Parameter.ProductNotFoundAction].Value.Value1 == null)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.ReplaceProductManual_ProductNotFoundAction_Name,  
                        Message.Error_NoValue);
                    return false;
                }
                else
                {
                    try
                    {
                        context.ProductNotFoundAction = (ProductNotFoundActionType)Parameters[Parameter.ProductNotFoundAction].Value.Value1;
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.ReplaceProductManual_ProductNotFoundAction_Name,  
                            Parameters[Parameter.ProductNotFoundAction].Value.Value1);
                        return false;
                    }
                }

                #endregion

                #region AddCar Park Text Box

                //validate that the car park text box parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.AddCarParkTextBox)) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value == null) ||
                    (this.Parameters[Parameter.AddCarParkTextBox].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.ReplaceProductManual_Parameter_AddCarParkTextBox_Name, Message.Error_NoValue);
                    return false;
                }
                else
                {
                    try
                    {
                        AddCarParkTextBoxType addTextBox = (AddCarParkTextBoxType)Convert.ToByte(Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        context.AddCarParkTextBox = (addTextBox == AddCarParkTextBoxType.Yes);
                    }
                    catch (InvalidCastException)
                    {
                        context.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.ReplaceProductManual_Parameter_AddCarParkTextBox_Name,
                            Parameters[(Int32)Parameter.AddCarParkTextBox].Value.Value1);
                        return false;
                    }
                }

                #endregion

                return true;
            }
        }
        
        /// <summary>
        /// gets the list of replacement products to add and also list of replace products that are missing from the current plan.
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private Boolean GetReplaceProductDetails(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                if (!taskContext.Planogram.Products.Any())
                {
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoProductsInPlanogram); // No Products were present in the Planogram.
                    success = false;
                }
                else if (!taskContext.Planogram.Positions.Any())
                {
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoPositionsInPlanogram); // No Positions exist in the Planogram.);
                    success = false;
                }
                else
                {
                    //  Get the replacement products that are missing from the planogram.
                    IEnumerable<PlanogramProduct> existingProducts = taskContext.Planogram.Products
                        .Where(existingProduct => 
                            taskContext.ReplacementProductDetails
                            .Any(replacement => existingProduct.Gtin == replacement.ReplacementProductGtin))
                        .ToList();
                    taskContext.MissingProducts = ProductList.FetchByEntityIdProductGtins(
                        taskContext.EntityId,
                        taskContext.ReplacementProductDetails
                            .Select(replacement => replacement.ReplacementProductGtin)
                            .Except(existingProducts.Select(p => p.Gtin)))
                        .Select(PlanogramProduct.NewPlanogramProduct)
                        .ToList();
                    existingProducts = existingProducts.Union(taskContext.MissingProducts);

                    //  Get the products that should be replaced.
                    taskContext.ReplacementProducts = taskContext.ReplacementProductDetails
                        .Select(replacement => existingProducts.First(p => p.Gtin == replacement.ReplacementProductGtin))
                        .ToList();

                    // get list of replace products that dont exist on the plan
                    existingProducts = taskContext.Planogram.Products
                        .Where(p => taskContext.ReplacementProductDetails.Any(gtin => p.Gtin == gtin.ReplaceProductGtin))
                        .ToList();
                    taskContext.MissingReplaceProducts = ProductList.FetchByEntityIdProductGtins(
                        taskContext.EntityId,
                        taskContext.ReplacementProductDetails
                            .Select(p => p.ReplaceProductGtin)
                            .Except(existingProducts.Select(p => p.Gtin)));
                }

                return success;
            }
        }

        /// <summary>
        /// Gets the merchandising groups for the planogram.
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static Boolean GetMerchandisingGroups(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get merchandising groups
                taskContext.MerchandisingGroups = taskContext.Planogram.GetMerchandisingGroups();

                return true;
            }
        }

        /// <summary>
        /// Main method to iterate through products to be replaced.
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static Boolean ReplaceProducts(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                foreach (var item in taskContext.ReplacementProductDetails)
                {
                    taskContext.CurrentReplaceProductGtin = item.ReplaceProductGtin;
                    taskContext.CurrentReplacementProductGtin = item.ReplacementProductGtin;
                    if (!ValidateReplacePositionAndGetReplacements(taskContext)) continue;
                    ReplaceProduct(taskContext);
                }

                return true;
            }
        }
        
        /// <summary>
        /// Validates the current task context detail (ReplaceProductDoesNotExist, ReplacementProductDoesNotExist, ReplacementProductAlreadyOnPlanogram)
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static bool ValidateReplacePositionAndGetReplacements(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                IEnumerable<PlanogramPositionPlacement> allPositionPlacements =
                    taskContext.MerchandisingGroups.SelectMany(g => g.PositionPlacements).ToList();

                // get the positions to replace
                taskContext.ReplacePositions = allPositionPlacements
                    .Where(p => p.Product.Gtin == taskContext.CurrentReplaceProductGtin)
                    .ToList();

                taskContext.ReplacementProduct = taskContext.ReplacementProducts
                    .FirstOrDefault(p => p.Gtin == taskContext.CurrentReplacementProductGtin);

                // If there are positions on the plan for the product being replaced.
                if (taskContext.ReplacePositions.Any())
                {
                    // get the positions of the potential replacement product, if it exists on the plan already do nothing
                    IEnumerable<PlanogramPositionPlacement> replacementProductPositions = allPositionPlacements
                        .Where(p => p.Product.Gtin == taskContext.CurrentReplacementProductGtin)
                        .ToList();

                    if (taskContext.ReplacementProduct != null)
                    {
                        if (replacementProductPositions.Any())
                        {
                            // replacement already exists on the plan, dont add another position. Score 2
                            taskContext.LogWarning(EventLogEvent.ReplacementProductAlreadyOnPlanogram, taskContext.ReplacementProduct.Gtin, taskContext.ReplacementProduct.Name);

                            return false;
                        }
                    }
                    else
                    {
                        // replacement product not found, Score 4
                        taskContext.LogWarning(EventLogEvent.ReplacementProductDoesNotExist, taskContext.CurrentReplacementProductGtin);

                        return false;
                    }
                }
                else
                {
                    if (taskContext.ProductNotFoundAction == ProductNotFoundActionType.AddReplacementToProductList)
                    {
                        String productName = String.Empty;
                        Product replaceProduct = taskContext.MissingReplaceProducts.Where(p => p.Gtin == taskContext.CurrentReplaceProductGtin).FirstOrDefault();

                        if (replaceProduct == null)
                        {
                            PlanogramProduct existingProduct = taskContext.Planogram.Products.Where(p => p.Gtin == taskContext.CurrentReplaceProductGtin).FirstOrDefault();
                            if (existingProduct != null) { productName = existingProduct.Name; }
                        }
                        else
                        {
                            productName = replaceProduct.Name;
                        }

                        // the replace product doesnt exist on plan, Score 4
                        taskContext.LogWarning(EventLogEvent.ReplaceProductDoesNotExist, taskContext.CurrentReplaceProductGtin, productName);

                        return false;
                    }
                }

                return true;
            }
        }
        
        /// <summary>
        /// Main replace method, 
        /// Add replacement to product list
        /// Remove replace position
        /// Insert replacement position
        /// Remove replace merch group position
        /// Set placement units (1 facing \ fill available)
        /// Validate placement, log if it doesnt fit and add to car park if dropped behaviour 
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static Boolean ReplaceProduct(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean addedReplacementProduct = false;
                //  Add Replacement Product if it isnt already part of the product list.
                if (taskContext.MissingProducts.Any(p => p.Id.Equals(taskContext.ReplacementProduct.Id)))
                {
                    taskContext.Planogram.Products.Add(taskContext.ReplacementProduct);
                    addedReplacementProduct = true;
                }

                // If there aren't any positions to replace and we're supposed to add to the car park shelf in this
                // situation, add to the car park shelf instead.
                if (!taskContext.ReplacePositions.Any() && 
                    taskContext.ProductNotFoundAction == ProductNotFoundActionType.AddReplacementToCarParkComponent)
                {
                    if (taskContext.CarParkMerchandisingGroup == null)
                    {
                        taskContext.CarParkMerchandisingGroup =
                            taskContext.GetCarParkMerchandisingGroup(taskContext.CarParkComponentName, taskContext.MerchandisingGroups, taskContext.AddCarParkTextBox);
                    }

                    taskContext.CarParkMerchandisingGroup.InsertPositionPlacement(taskContext.ReplacementProduct);
                    taskContext.CarParkMerchandisingGroup.Process();
                    taskContext.CarParkMerchandisingGroup.ApplyEdit();

                    taskContext.CarParkShelfInsertCount++;
                    return true;
                }

                // Replace all positions
                foreach (PlanogramPositionPlacement placement in taskContext.ReplacePositions)
                {
                    // remove planogram position, always removed.
                    taskContext.Planogram.Positions.Remove(taskContext.Planogram.Positions.FindById(placement.Position.Id));

                    // Get merchandising group and begin edit
                    PlanogramMerchandisingGroup merchandisingGroup = placement.MerchandisingGroup;
                    merchandisingGroup.BeginEdit();

                    // create replacemet position
                    PlanogramPosition replacementPosition = PlanogramPosition.NewPlanogramPosition(0, taskContext.ReplacementProduct);
                    replacementPosition.SetMinimumUnits(taskContext.ReplacementProduct);

                    AxisType axisType = placement.SubComponentPlacement.GetFacingAxis();
                    Boolean isManualMerchandising = IsManualMerchandising(axisType, merchandisingGroup);

                    //  Account for Manual Merchandising cases, get a picture of the merchandising group before replacement
                    List<PointValue> startingCoordinates = null;
                    if (isManualMerchandising)
                    {
                        //  Save pre existing positions.
                        startingCoordinates = merchandisingGroup
                            .PositionPlacements
                            .Where(p => !p.Id.Equals(placement.Id))
                            .OrderBy(p => p.Id)
                            .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                            .ToList();
                    }

                    // replace position, removes replace product, adds replacement
                    PlanogramPositionPlacement replacement = merchandisingGroup.ReplacePositionPlacement(replacementPosition,
                                                                                    taskContext.ReplacementProduct,
                                                                                    placement);

                    if (replacement == null) // this should never happen but if it does break out and continue
                    {
                        merchandisingGroup.CancelEdit();
                        continue;
                    }

                    // Process the merch group
                    merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                    var isValidPlacement = true;
                    if (taskContext.DroppedProductBehaviour != DroppedProductBehaviourType.AlwaysAdd)
                    {
                        do
                        {
                            isValidPlacement = ValidatePlacement(taskContext,
                                                                 isManualMerchandising,
                                                                 merchandisingGroup,
                                                                 replacement,
                                                                 startingCoordinates);

                            //  If the placement is not valid, do not try to keep increasing units... move on.
                            if (!isValidPlacement) break;

                            //  If the task must place a single placing.. do not try to keep increasing units... move on.
                            if (taskContext.ProductPlacement == PlacementType.SingleFacing) break;

                            //  Try the next increase of units...
                            replacement.IncreaseUnits();
                            replacement.RecalculateUnits();
                            merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);
                        } while (true);
                    }
                    else 
                    {
                        // We always want to add but we may still want to fill available space
                        if (taskContext.ProductPlacement != PlacementType.SingleFacing)
                        {
                            do
                            {
                                replacement.IncreaseUnits();
                                replacement.RecalculateUnits();
                                merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);
                                
                                if (!ValidatePlacement(taskContext,
                                                                     isManualMerchandising,
                                                                     merchandisingGroup,
                                                                     replacement,
                                                                     startingCoordinates, 
                                                                     false))
                                {
                                    replacement.DecreaseUnits();
                                    replacement.RecalculateUnits();
                                    merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);
                                    break;
                                }
                                
                            } while (true);
                        }
                    }

                    if (!isValidPlacement)
                    {
                        replacement.DecreaseUnits();
                        replacement.RecalculateUnits();
                        merchandisingGroup.Process(PlanogramSubComponentSqueezeType.FullSqueeze);

                        if (!ValidatePlacement(taskContext,
                                               isManualMerchandising,
                                               merchandisingGroup,
                                               replacement,
                                               startingCoordinates))
                        {
                            // replacement product could not be added, Score 2
                            taskContext.LogWarning(EventLogEvent.ReplacementProductCouldNotBeAdded, taskContext.ReplacementProduct.Gtin, taskContext.ReplacementProduct.Name);

                            // remove replacement 
                            merchandisingGroup.RemovePositionPlacement(replacement);

                            // check if the unplaced product is to be added to a car park shelf
                            if (taskContext.DroppedProductBehaviour == DroppedProductBehaviourType.AddToCarparkShelf)
                            {
                                AddPositionToCarParkComponent(taskContext);
                                continue;
                            }

                            // remove unplaced replacement product from product list
                            if (addedReplacementProduct) { taskContext.Planogram.Products.Remove(taskContext.ReplacementProduct); }
                            continue;
                        }
                    }

                    //  If the task got this far everything was dandy.
                    taskContext.ReplaceCount++;
                }

                return true;
            }
        }

        private static void AddPositionToCarParkComponent(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get the car park shelf
                if (taskContext.CarParkMerchandisingGroup == null)
                {
                    taskContext.CarParkMerchandisingGroup = taskContext.GetCarParkMerchandisingGroup(taskContext.CarParkComponentName, taskContext.MerchandisingGroups, taskContext.AddCarParkTextBox);
                }

                Int16 sequence = 0;
                if (taskContext.CarParkMerchandisingGroup.PositionPlacements.Count > 0)
                    sequence = taskContext.CarParkMerchandisingGroup.PositionPlacements.Max(p => p.Position.Sequence);

                // create a new position
                PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(sequence, taskContext.ReplacementProduct);
                position.SetMinimumUnits(
                    taskContext.ReplacementProduct);

                // insert the position into the planogram
                taskContext.Planogram.Positions.Add(position);

                // insert into the merchandising group
                taskContext.CarParkMerchandisingGroup.InsertPositionPlacement(position, taskContext.ReplacementProduct);
                taskContext.CarParkShelfInsertCount++;
            }
        }
        
        /// <summary>
        ///     Log final details about the task execution.
        /// </summary>
        /// <param name="context">The <see cref="ReplaceProductManual.v1.TaskContext"/> contaning what needs to be logged.</param>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.LogInformation(
                    EventLogEvent.PlanogramPositionsReplaced,
                    (context.ReplaceCount + context.CarParkShelfInsertCount),
                    context.ReplaceCount);

                if (context.CarParkMerchandisingGroup != null && context.CarParkShelfInsertCount > 0)
                {
                    context.LogInformation(
                        EventLogEvent.PlanogramPositionsReplacedCarPark,
                        context.CarParkShelfInsertCount,
                        Message.TaskLogging_TheCarparkShelf);
                }

                // return success
                return true;
            }
        }

        private static Boolean IsManualMerchandising(AxisType currentAxis,
                                                     PlanogramMerchandisingGroup merchandisingGroup)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean isManualMerchandising = currentAxis == AxisType.X &&
                                                merchandisingGroup.StrategyX ==
                                                PlanogramSubComponentXMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Y &&
                                                merchandisingGroup.StrategyY ==
                                                PlanogramSubComponentYMerchStrategyType.Manual ||
                                                currentAxis == AxisType.Z &&
                                                merchandisingGroup.StrategyZ ==
                                                PlanogramSubComponentZMerchStrategyType.Manual;
                return isManualMerchandising;
            }
        }

        private static Boolean ValidatePlacement(TaskContext context,
                                                 Boolean isManualMerchandising,
                                                 PlanogramMerchandisingGroup merchandisingGroup,
                                                 PlanogramPositionPlacement newPlacement,
                                                 IEnumerable<PointValue> startingCoordinates,
                                                 Boolean checkForAlwaysAdd = true)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Validate the placement.
                if (checkForAlwaysAdd)
                {
                    if (context.DroppedProductBehaviour == DroppedProductBehaviourType.AlwaysAdd) return true;
                }

                //  Assume the placement is valid.
                var isValidPlacement = true;

                //  Account for manual merchandising strategy.
                if (isManualMerchandising)
                {
                    //  Check the pre existing positions did not need to move.
                    List<PointValue> finalCoordinates = merchandisingGroup
                        .PositionPlacements
                        .Where(p => !p.Id.Equals(newPlacement.Id))
                        .OrderBy(p => p.Id)
                        .Select(p => p.Position.GetPlanogramRelativeCoordinates(p.SubComponentPlacement))
                        .ToList();
                    isValidPlacement = startingCoordinates
                        .Zip(finalCoordinates,
                             (starting, final) =>
                             starting.X.EqualTo(final.X) &&
                             starting.Y.EqualTo(final.Y) &&
                             starting.Z.EqualTo(final.Z)).All(b => b);
                }

                //  Validate the merchandising group can fit the placement.
                //  we only check IsPlacementOverfilled rather than IsOverfilled here as other
                // placements in the group could then cause this one to not be increased.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.MerchandisingGroup.IsPlacementOverfilled(newPlacement);
                                    

                //  Validate the placement is insisde the merchandisable space.
                isValidPlacement = isValidPlacement &&
                                   !newPlacement.IsOutsideMerchandisingSpace();

                //  Validate the merchandising group has not internal overlaps.
                isValidPlacement = isValidPlacement &&
                                   !merchandisingGroup.HasCollisionOverlapping();

                //  Return the valid state.
                return isValidPlacement;
            }
        }

        /// <summary>
        /// Re-merchandises the planogram
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // remerchandise the whole planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method checks to make sure that the CarPark Name is less
        /// than 50 characters in legnth.
        /// </summary>
        /// <returns>Error Message or Null</returns>
        private String IsCarParkNameValid()
        {
            if (this.Parameters == null || this.Parameters[Parameter.CarParkComponentName] == null
                || !this.Parameters.Contains((Int32)Parameter.CarParkComponentName)
                || this.Parameters[Parameter.CarParkComponentName].Value == null
                || this.Parameters[Parameter.CarParkComponentName].Value.Value1 == null)
            {
                return Message.NullComponentName;
            }
            if (this.Parameters[Parameter.CarParkComponentName].Value.Value1.ToString()
                .Length > PlanogramComponent.MaximumComponentNameLength)
            {
                return Message.IsCarParkNameValid_NotValid_ErrorMessage;
            }
            else
            {
                return null;
            }
        }

        #endregion 
    }
}