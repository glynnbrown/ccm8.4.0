﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820
// V8-30948 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;

namespace Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1
{
    /// <summary>
    /// Denotes the possible actions that can be taken when the product to be replaced
    /// is not positioned on the plan.
    /// </summary>
    public enum ProductNotFoundActionType
    {
        /// <summary>
        /// Do not add the replacement to the plan.
        /// </summary>
        AddReplacementToProductList = 0,
        /// <summary>
        /// Add the replacement product to the car park component.
        /// </summary>
        AddReplacementToCarParkComponent = 1,
    }

    public static class ProductNotFoundActionTypeHelper
    {
        public static Dictionary<ProductNotFoundActionType, String> FriendlyNames =
            new Dictionary<ProductNotFoundActionType, String>()
            {
                { ProductNotFoundActionType.AddReplacementToProductList, Message.ReplaceProductManual_ProductNotFoundActionType_AddReplacementToProductList }, 
                { ProductNotFoundActionType.AddReplacementToCarParkComponent, Message.ReplaceProductManual_ProductNotFoundActionType_AddReplacementToCarParkComponent },
            };
    }
}
