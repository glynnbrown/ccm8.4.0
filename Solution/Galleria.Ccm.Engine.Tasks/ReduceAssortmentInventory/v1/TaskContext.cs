﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
// V8-29597 : N.Foster
//  Code Review
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.ReduceAssortmentInventory.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields

        private List<PlanogramMerchandisingGroup> _remerchandisingGroups;

        #endregion

        #region Properties
        /// <summary>
        /// Gets the merchandising groups for the current Planogram.
        /// </summary>
        public PlanogramMerchandisingGroupList MerchandisingGroups { get ; set; }

        // we also build a list of merchandising groups that require
        // to be re-processed after we have made our changes
        public List<PlanogramMerchandisingGroup> RemerchandisingGroups
        {
            get { return _remerchandisingGroups; }
        }

        public Int32 ProductsAtTarget { get; set; }
        public Int32 ProductsThatFullyAchievedTarget { get; set; }
        public Int32 ProductsThatPartiallyAchievedTarget { get; set; }
        public Int32 ProductsThatCouldNotBeDecreased { get; set; }
        public Int32 ProductsThatHadNoPositions { get; set; }
        public Int32 TotalProductCount { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
            _remerchandisingGroups = new List<PlanogramMerchandisingGroup>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDipose()
        {
            if (this.MerchandisingGroups != null)
                this.MerchandisingGroups.Dispose();
        }

        #endregion
    }
}
