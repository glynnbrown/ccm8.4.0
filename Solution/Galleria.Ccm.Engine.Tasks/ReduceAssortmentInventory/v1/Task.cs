﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29801 : M.Brumby
//  Prevented infinate loop due to decrease unit changes.
// V8-29597 : N.Foster
//  Code Review
// V8-29737 : D.Pleasance
//  Reworked ReduceAssortmentInventory so that logging is calculated against the last valid position unit value.
//  Editing is against the merchandising group.
#endregion
#region Version History: CCM811
// V8-30388 : M.Brumby
//  Fail if the assortment has 0 products. (In Line with other tasks)
#endregion
#region Version History: CCM820
// V8-30710 : M.Brumby
//  Make missing assortment messaging consistent across tasks.
// V8-30873 : L.Ineson
//  Removed IsCarPark flag
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.ReduceAssortmentInventory.v1
{
    /// <summary>
    /// A task that reduces product inventory
    /// to match the inventory specified within the assortment
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.ReduceAssortmentInventory_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.ReduceAssortmentInventory_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // no parameters to register
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!CheckThereIsSomethingToDo(taskContext)) return;
                if (!SetContext(taskContext)) return;
                ReduceAssortmentInventory(taskContext);
                LogCompletion(taskContext);
                ReMerchandisePlanogram(taskContext);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Verify that there is something to do
        /// </summary>
        private Boolean CheckThereIsSomethingToDo(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify we have an assortment
                if ((context.Planogram.Assortment == null) ||
                    (context.Planogram.Assortment.Products == null) ||
                    ((context.Planogram.Assortment.Products != null) && (context.Planogram.Assortment.Products.Count == 0)))
                {
                    context.LogWarning(EventLogEvent.PlanogramNoAssortmentPresent);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Initialises the current task context properties
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static Boolean SetContext(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get merchandising groups
                taskContext.MerchandisingGroups = taskContext.Planogram.GetMerchandisingGroups();
                return true;
            }
        }

        /// <summary>
        /// Performs the reduction of product inventory to meet assortment
        /// </summary>
        private static void ReduceAssortmentInventory(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Seeing as we are reducing inventory on this planogram
                // we dont actually care what order we process groups or positions
                // as we can always free up space.
                // We have to consider how we deal with multi-sited products
                // when we reduce inventory to match the assortment,
                // so build a lookup dictionary of all positions
                // on the plan indexed by their GTIN
                Dictionary<String, PlanogramPositionPlacementList> positionPlacements = new Dictionary<String, PlanogramPositionPlacementList>();
                foreach (PlanogramMerchandisingGroup merchandisingGroup in taskContext.MerchandisingGroups)
                {
                    foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                    {
                        String gtin = positionPlacement.Product.Gtin;
                        if (!positionPlacements.ContainsKey(gtin)) positionPlacements.Add(gtin, PlanogramPositionPlacementList.NewPlanogramPositionPlacementList());
                        positionPlacements[gtin].Add(positionPlacement);
                    }
                }

                // Prepare to capture the number of products already at their target units, those that achieved
                // their target units and those that partially acheieved their target.
                taskContext.ProductsAtTarget = 0;
                taskContext.ProductsThatFullyAchievedTarget = 0;
                taskContext.ProductsThatPartiallyAchievedTarget = 0;
                taskContext.ProductsThatCouldNotBeDecreased = 0;
                taskContext.ProductsThatHadNoPositions = 0;
                taskContext.TotalProductCount = taskContext.Planogram.Assortment.Products.Count;

                // we can now run through our assortment products
                // and attempt to match the positions inventory
                foreach (PlanogramAssortmentProduct assortmentProduct in taskContext.Planogram.Assortment.Products)
                {
                    // get the assortment products gtin and target units
                    String gtin = assortmentProduct.Gtin;
                    Int32 targetUnits = assortmentProduct.Units;

                    // determine if we have any positions for
                    // this assortment product
                    PlanogramPositionPlacementList positions;
                    if (positionPlacements.TryGetValue(gtin, out positions))
                    {
                        Int32 currentUnits = positions.GetPositionDetails().TotalUnitCount;
                        Int32 originalUnits = currentUnits;
                        Int32 validUnits = currentUnits;
                        if (currentUnits == targetUnits)
                        {
                            taskContext.ProductsAtTarget++; // position already at target, move onto next
                            continue;
                        }

                        while (currentUnits > targetUnits)
                        {
                            // attempt to decrease units for one
                            // of the positions within the list
                            PlanogramPositionPlacement positionAffected =
                                DecreaseUnits(positions);

                            // if we could not decrease units then
                            // exit this loop
                            if (positionAffected == null) break;

                            // get the current units again
                            currentUnits = positions.GetPositionDetails().TotalUnitCount;

                            // if we are equal to or above the target
                            // units, then commit the last edit
                            if (currentUnits >= targetUnits)
                            {
                                positionAffected.MerchandisingGroup.ApplyEdit();
                                validUnits = currentUnits; // valid unit change

                                // a change was made to this position, so we record
                                // the merchandising group so that it can be
                                // reprocessed at the end of this task
                                if (!taskContext.RemerchandisingGroups.Contains(positionAffected.MerchandisingGroup))
                                    taskContext.RemerchandisingGroups.Add(positionAffected.MerchandisingGroup);
                            }
                            else
                            {
                                positionAffected.MerchandisingGroup.CancelEdit();
                            }
                        }

                        // Log the change for this product.
                        if (originalUnits != validUnits)
                        {
                            taskContext.LogDebug(
                                PlanogramEventLogAffectedType.AssortmentProduct,
                                assortmentProduct.Id,
                                EventLogEvent.PlanogramAssortmentProductPositionsChanged,
                                assortmentProduct.Name,
                                assortmentProduct.Gtin,
                                originalUnits,
                                validUnits);
                        }

                        if (validUnits == targetUnits)
                        {
                            taskContext.ProductsThatFullyAchievedTarget++;
                        }
                        else
                        {
                            if (validUnits == originalUnits)
                            {
                                taskContext.ProductsThatCouldNotBeDecreased++;
                            }
                            else
                            {
                                taskContext.ProductsThatPartiallyAchievedTarget++;
                            }
                        }
                    }
                    else
                    {
                        taskContext.ProductsThatHadNoPositions++;
                    }
                }
            }
        }

        /// <summary>
        /// Log final details about the task execution.
        /// </summary>
        private void LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Log totals for products affected.
                taskContext.LogInformation(
                    EventLogEvent.PlanogramAssortmentProductsUnitsDecreased,
                    taskContext.TotalProductCount,
                    taskContext.ProductsAtTarget,
                    taskContext.ProductsThatFullyAchievedTarget,
                    taskContext.ProductsThatPartiallyAchievedTarget,
                    taskContext.ProductsThatHadNoPositions + taskContext.ProductsThatCouldNotBeDecreased);
            }
        }

        /// <summary>
        /// Re-merchandises the whole planogram to take
        /// into account the changes that have been made
        /// </summary>
        private void ReMerchandisePlanogram(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // remerchandise the effected groups
                foreach (PlanogramMerchandisingGroup merchandisingGroup in taskContext.RemerchandisingGroups)
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                }
            }
        }

        /// <summary>
        /// Decreases the units of a position within
        /// a list of position placements
        /// </summary>
        private static PlanogramPositionPlacement DecreaseUnits(PlanogramPositionPlacementList positionPlacements)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // ensure we have something to do
                if (positionPlacements.Count == 0) return null;

                // enumerate through our positions based
                // on the largest total units first
                foreach (PlanogramPositionPlacement placement in positionPlacements.OrderByDescending(p => p.GetPositionDetails().TotalUnitCount).ThenBy(p => p.Id))
                {
                    placement.MerchandisingGroup.BeginEdit();

                    // attempt to decrease units for this position
                    if (DecreaseUnits(placement)) return placement;
                }

                // if we got this far then return null
                return null;
            }
        }

        /// <summary>
        /// Attempts to decrease units for the specified position placement
        /// </summary>
        private static Boolean DecreaseUnits(PlanogramPositionPlacement positionPlacement)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the facing axis for the position
                AxisType facingAxis = positionPlacement.SubComponentPlacement.GetFacingAxis();

                // calculate the current space occupied by the placement
                Single currentSpace = 0;
                switch (facingAxis)
                {
                    case AxisType.X:
                        currentSpace = positionPlacement.GetPositionDetails().TotalSpace.Width;
                        break;
                    case AxisType.Y:
                        currentSpace = positionPlacement.GetPositionDetails().TotalSpace.Height;
                        break;
                    case AxisType.Z:
                        currentSpace = positionPlacement.GetPositionDetails().TotalSpace.Depth;
                        break;
                }

                if (!positionPlacement.Position.DecreaseUnits(
                    positionPlacement.Product,
                    positionPlacement.SubComponentPlacement,
                    positionPlacement.MerchandisingGroup))
                    return false;
                // now perform the decrease in inventory
                //if (!positionPlacement.DecreaseUnits(positionPlacement.MerchandisingGroup))
                //    return false;

                // calculate the new space occupied by the placement
                Single newSpace = 0;
                switch (facingAxis)
                {
                    case AxisType.X:
                        newSpace = positionPlacement.GetPositionDetails().TotalSpace.Width;
                        break;
                    case AxisType.Y:
                        newSpace = positionPlacement.GetPositionDetails().TotalSpace.Height;
                        break;
                    case AxisType.Z:
                        newSpace = positionPlacement.GetPositionDetails().TotalSpace.Depth;
                        break;
                }

                // now shift the positions coordinates in order
                // to the keep the position centralised to its
                // exist coordinates in the facing axis
                Single coordinateChange = (currentSpace - newSpace) / 2;
                switch (facingAxis)
                {
                    case AxisType.X:
                        positionPlacement.Position.X += coordinateChange;
                        break;
                    case AxisType.Y:
                        positionPlacement.Position.Y += coordinateChange;
                        break;
                    case AxisType.Z:
                        positionPlacement.Position.Z += coordinateChange;
                        break;
                }

                // and return success
                return true;
            }
        }

        #endregion
    }
}
