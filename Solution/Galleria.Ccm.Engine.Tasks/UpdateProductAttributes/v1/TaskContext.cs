﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29865 : D.Pleasance
//  Created
// V8-29597 : N.Foster
//  Code Review
#endregion
#region Version History : CCM820
// V8-30988 : D.Pleasance
//  Added AssortmentProducts
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1
{
    public class TaskContext : Ccm.Engine.TaskContext
    {
        #region Fields
        private List<String> _productGtins = new List<String>();
        private List<Task.ProductAttributeSelection> _productAttributeSelections = new List<Task.ProductAttributeSelection>();
        private Dictionary<String, Product> _masterProductsByGtin = new Dictionary<String, Product>();
        private IEnumerable<PlanogramProduct> _planProducts;
        private IEnumerable<PlanogramAssortmentProduct> _assortmentProducts;
        private Int32 _productsUpdated;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// The location name
        /// </summary>
        public List<String> ProductGtins
        {
            get { return _productGtins; }
        }

        /// <summary>
        /// The Location code
        /// </summary>
        internal List<Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task.ProductAttributeSelection> ProductAttributeSelections
        {
            get { return _productAttributeSelections; }
        }

        public IEnumerable<PlanogramProduct> PlanProducts
        {
            get { return _planProducts; }
            set { _planProducts = value; }
        }

        public IEnumerable<PlanogramAssortmentProduct> AssortmentProducts
        {
            get { return _assortmentProducts; }
            set { _assortmentProducts = value; }
        }

        public Dictionary<String, Product> MasterProductsByGtin
        {
            get { return _masterProductsByGtin; }
            set { _masterProductsByGtin = value; }
        }

        public Int32 ProductsUpdated
        {
            get { return _productsUpdated; }
            set { _productsUpdated = value; }
        }

        #endregion
    }
}