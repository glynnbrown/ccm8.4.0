﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-27494 : A.Kuszyk
//  Created.
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM802
// V8-29060 : A.Kuszyk
//  Ensured that plan is re-laid out to take account of any size changes that might have occured.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
// V8-30216 : A.Kuszyk
//  Added some performance metrics.
#endregion
#region Version History: CCM820
// V8-30988 : D.Pleasance
//  Amended to also update assortment product Name
#endregion
#region Version History : CCM830
// V8-31612 : A.Kuszyk
//  Re-factored UpdateProducts to improve performance.
// V8-31648 : A.Kuszyk
//  Corrected issue with custom attributes field prefix.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// CCM-18453 : A.Silva
//  Amended GetProductsToUpdate so that duplicate Gtins will be ignored.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using System.Reflection;
using System.Text.RegularExpressions;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Nested Types
        /// <summary>
        /// A simple container for the attribute source and name values.
        /// </summary>
        internal class ProductAttributeSelection
        {
            /// <summary>
            /// The source for this attribute data (Product, Custom Attribute, etc.).
            /// </summary>
            public ProductAttributeSourceType Source { get; private set; }

            /// <summary>
            /// The name of the property on the source that this item refers to.
            /// </summary>
            public String AttributeName { get; private set; }

            /// <summary>
            /// Instantiates a new object with the given values.
            /// </summary>
            /// <param name="source"></param>
            /// <param name="attributeName"></param>
            public ProductAttributeSelection(ProductAttributeSourceType source, String attributeName)
            {
                Source = source;
                AttributeName = attributeName;
            }
        }

        #endregion

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ProductCodes = 0,
            ProductAttributes = 1
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdateProductAttributes_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override String Description
        {
            get { return Message.UpdateProductAttributes_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Product Codes
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductCodes,
                Message.UpdateProductAttributes_ProductCodes,
                Message.UpdateProductAttributes_ProductCodes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductCodeMultiple,
                null,
                true,
                true));

            // Product Attributes
            this.RegisterParameter(new TaskParameter(
                Parameter.ProductAttributes,
                Message.UpdateProductAttributes_ProductAttributes,
                Message.UpdateProductAttributes_ProductAttributes_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.ProductAttributeMultiple,
                null,
                true,
                true));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                // Get and validate parameters                
                if (!GetAndValidateParameters(taskContext)) return;

                // Gets the products to update on the planogram
                if (!GetProductsToUpdate(taskContext)) return;

                // Update the product attributes and log the number updated.
                taskContext.ProductsUpdated =
                    UpdateProducts(taskContext.PlanProducts, taskContext.AssortmentProducts, taskContext.ProductAttributeSelections, taskContext.MasterProductsByGtin);

                // remerchandise the planogram to take into account any product size changes
                if (!ReMerchandisePlanogram(taskContext)) return;

                // Log updated products
                LogCompletion(taskContext); 
            }
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Gets parameter values, valdidates them and assigns them to the the given arguments.
        /// </summary>
        /// <param name="context">The current task context</param>
        /// <returns></returns>
        private Boolean GetAndValidateParameters(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Get and validate product codes.
                // Ordinarily, a null value would be an issue, but in this case we don't mind, because
                // we'll just process all products in the plan later on. So, we can just leave
                // productCodes assigned to a new list.
                if (Parameters[Parameter.ProductCodes].Value != null)
                {
                    try
                    {
                        taskContext.ProductGtins.AddRange(this.Parameters[Parameter.ProductCodes].Values.
                            Select(item => Convert.ToString(item.Value1)).ToList());
                    }
                    catch
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdateProductAttributes_ProductCodes,
                            Message.Error_NoValue);
                        return false;
                    }
                }

                // Get and validate product attributes.
                // Similarly to product codes, a null parameter value here isn't an issue, 
                // because we'll just process all of the product attributes. Just leave
                // productAttributes assigned to a new list.
                if (Parameters[Parameter.ProductAttributes].Value != null)
                {
                    try
                    {
                        taskContext.ProductAttributeSelections.AddRange(this.Parameters[Parameter.ProductAttributes].Values.
                            Select(item =>
                                new ProductAttributeSelection((ProductAttributeSourceType)item.Value1, item.Value2.ToString())
                                ).
                            ToList());
                    }
                    catch
                    {
                        taskContext.LogError(
                            EventLogEvent.InvalidTaskParameterValue,
                            Message.UpdateProductAttributes_ProductCodes,
                            Message.Error_NoValue);
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Gets and validates that there are products to update on the planogram
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns>True if products to update</returns>
        private static Boolean GetProductsToUpdate(TaskContext taskContext)
        {
            using (new CodePerformanceMetric())
            {
                Boolean hasUserProductGtins = taskContext.ProductGtins.Any();

                if (hasUserProductGtins)
                {
                    //  The user provided the product Gtins to update, use just those.
                    taskContext.PlanProducts = taskContext.Planogram.Products.Where(p => taskContext.ProductGtins.Contains(p.Gtin)).ToList();
                    taskContext.AssortmentProducts = taskContext.Planogram.Assortment.Products.Where(p => taskContext.ProductGtins.Contains(p.Gtin));
                }
                else
                {
                    //  Use all the products in the planogram and assortment.
                    taskContext.PlanProducts = taskContext.Planogram.Products;
                    taskContext.AssortmentProducts = taskContext.Planogram.Assortment.Products;
                }

                Boolean hasMatchingPlanogramProducts = taskContext.PlanProducts.Any();
                if (!hasMatchingPlanogramProducts)
                {
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.UpdateProductAttributes_NoMatchingPlanogramProducts);
                    return false;
                }

                // Get master data products and key them by Gtin.
                IEnumerable<String> productGtins = hasUserProductGtins ? taskContext.ProductGtins : taskContext.PlanProducts.Select(p => p.Gtin);
                taskContext.MasterProductsByGtin = ProductList.FetchByEntityIdProductGtins(taskContext.EntityId, productGtins).
                                                               ToLookupDictionary(p => p.Gtin, p => p);

                Boolean hasMatchingMasterDataProducts = taskContext.MasterProductsByGtin.Any();
                if (!hasMatchingMasterDataProducts)
                {
                    taskContext.LogInformation(EventLogEvent.NothingForTaskToDo, Message.UpdateProductAttributes_NoMatchingMasterDataProducts);
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Updates the attributes for the given products, for the given attributes, from the given masterdata products.
        /// </summary>
        /// <param name="planProducts">The planogram products to update.</param>
        /// <param name="productAttributeSelections">The attributes to update.</param>
        /// <param name="masterProductsByGtin">The masterdata products to use as a source, keyed by gtin.</param>
        /// <param name="context">The task context.</param>
        internal static Int32 UpdateProducts(
            IEnumerable<PlanogramProduct> planProducts,
            IEnumerable<PlanogramAssortmentProduct> assortmentProducts,
            IEnumerable<ProductAttributeSelection> productAttributeSelections,
            Dictionary<String, Product> masterProductsByGtin)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // Update each product in turn, and for each product update each attribute in turn.
                Int32 productsUpdated = 0;
                
                foreach (PlanogramProduct planProduct in planProducts)
                {
                    Product product;
                    if (!masterProductsByGtin.TryGetValue(planProduct.Gtin, out product)) continue;
                    productsUpdated++;

                    // If we have some product attributes selected, just update those attributes.
                    if (productAttributeSelections.Any())
                    {
                        Regex regexCustomAttribute = new Regex(String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name));
                        String[] customAttributeNames = productAttributeSelections
                            .Where(a =>
                                a.Source == ProductAttributeSourceType.ProductCustomAttributes &&
                                regexCustomAttribute.IsMatch(a.AttributeName))
                            .Select(a => regexCustomAttribute.Match(a.AttributeName).Groups[1].ToString())
                            .ToArray();
                        if (customAttributeNames.Any())
                        {
                            planProduct.CustomAttributes.UpdateFrom(product.CustomAttributes, customAttributeNames);
                        }

                        IEnumerable<ProductAttributeSelection> productAttributeSelection = productAttributeSelections
                            .Where(a => a.Source == ProductAttributeSourceType.ProductAttributes);
                        if (productAttributeSelection.Any())
                        {
                            planProduct.UpdateFrom(product, productAttributeSelection.Select(a => a.AttributeName).ToArray());
                        }
                    }
                    // If not, update all attributes.
                    else
                    {
                        planProduct.UpdateFrom(product);
                    }
                }

                if (!productAttributeSelections.Any() 
                    || productAttributeSelections.Where(p => p.Source == ProductAttributeSourceType.ProductAttributes && p.AttributeName == PlanogramProduct.NameProperty.Name).Any())
                {
                    PropertyInfo property = null;
                    foreach (PlanogramAssortmentProduct assortmentProduct in assortmentProducts)
                    {
                        Product product;
                        if (!masterProductsByGtin.TryGetValue(assortmentProduct.Gtin, out product)) continue;

                        property = typeof(PlanogramAssortmentProduct).GetProperty(PlanogramAssortmentProduct.NameProperty.Name);
                        if (property == null) continue;
                        property.SetValue(assortmentProduct, product.Name, null);
                    }
                }

                return productsUpdated;
            }
        }

        /// <summary>
        /// Log planogram products updated.
        /// </summary>
        /// <param name="taskContext"></param>
        private void LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                taskContext.LogInformation(EventLogEvent.PlanogramProductsUpdated, taskContext.ProductsUpdated);
            }
        }

        /// <summary>
        /// Remerchandises the planogram if required
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if nothing has been updated, then no need to remerchandise the planogram
                if (context.ProductsUpdated == 0) return true;

                // remerchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        #endregion
    }
}
