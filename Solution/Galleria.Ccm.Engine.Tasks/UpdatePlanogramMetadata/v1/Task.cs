﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25839 : L.Ineson
//  Created
// V8-27765/V8-28060 : A.Kuszyk
//  Added basic logging.
#endregion
#region Version History: CCM810
// V8-29865 : D.Pleasance
//  Refactored task.
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.UpdatePlanogramMetadata.v1
{
    /// <summary>
    /// Updates all the planogram metadata including planogram thumbnail, various statistics etc.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.UpdatePlanogramMetadata_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.UpdatePlanogramMetadata_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // No parameters required for this task
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!UpdatePlanogramMetadata(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Update the planogram metadata
        /// </summary>
        private static Boolean UpdatePlanogramMetadata(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // update the metadata
                taskContext.Planogram.Parent.CalculateMetadata(/*processMetaImages*/true,
                        PlanogramMetadataHelper.NewPlanogramMetadataHelper(taskContext.EntityId, taskContext.Planogram.Parent));

                // return success
                return true;
            }
        }

        /// <summary>
        /// Log completion
        /// </summary>
        private static Boolean LogCompletion(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // log information
                taskContext.LogInformation(EventLogEvent.PlanogramMetadataUpdated);

                // return success
                return true;
            }
        }

        #endregion
    }
}
