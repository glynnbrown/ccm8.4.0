﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810
// V8-29597 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private String _validationTemplateName;
        private ValidationTemplate _validationTemplate;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the renumbering strategy name
        /// </summary>
        public String ValidationTemplateName
        {
            get { return _validationTemplateName; }
            set { _validationTemplateName = value; }
        }

        /// <summary>
        /// Gets or sets the renumbering strategy
        /// </summary>
        public ValidationTemplate ValidationTemplate
        {
            get { return _validationTemplate; }
            set { _validationTemplate = value; }
        }
        #endregion
    }
}
