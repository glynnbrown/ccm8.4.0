﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27269 : A.Kuszyk
//  Created.
// V8-27765 : A.Kuszyk
//  Added basic logging.

#endregion

#region Version History: CCM801

// V8-28612 : A.Kuszyk
//  Defended against null parameter values.

#endregion

#region Version History: CCM810

// V8-29597 : N.Foster
//  Code Review
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required

#endregion

#region Version History: CCM811

// V8-30255 : A.Silva
//  Changed event log when the user does not set a value for the ValidationTemplateName parameter so that it is just a warning, not an error.

#endregion

#region Version History: CCM820

// V8-30255 : A.Silva
//  Amended error when validation template name was missing to be a warning instead.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            ValidationTemplateName  = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetValidationTemplate_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetValidationTemplate_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // validation template name
            RegisterParameter(
                new TaskParameter(
                    Parameter.ValidationTemplateName,
                    Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name,
                    Message.GetValidationTemplate_Parameter_ValidationTemplateName_Description,
                    Message.TaskCategory_PlanogramMaintenance,
                    TaskParameterType.ValidationTemplateSingle,
                    null,
                    false,
                    false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetValidationTemplate(taskContext)) return;
                if (!LoadValidationTemplate(taskContext)) return;
            }
        }
        #endregion

        #region Methods

        /// <summary>
        ///     Validates the parameters for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region ValidationTemplateName

                // validate that the ValidationTemplateName parameter exists
                if (!Parameters.Contains((Int32)Parameter.ValidationTemplateName) ||
                    Parameters[Parameter.ValidationTemplateName].Value == null)
                {
                    context.LogWarning(EventLogEvent.InvalidTaskParameterValue,
                                       Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name,
                                       Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the Validation Template name
                    context.ValidationTemplateName = Convert.ToString(Parameters[Parameter.ValidationTemplateName].Value.Value1);
                    if (String.IsNullOrWhiteSpace(context.ValidationTemplateName))
                    {
                        context.LogWarning(EventLogEvent.InvalidTaskParameterValue,
                                           Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name,
                                           Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        ///     Loads the validation template from the database
        /// </summary>
        private static Boolean GetValidationTemplate(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                // attempt to fetch the assortment by its name
                try
                {
                    context.ValidationTemplate = ValidationTemplate.FetchByEntityIdName(context.EntityId, context.ValidationTemplateName);
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw;

                    context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                     Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name,
                                     context.ValidationTemplateName);
                    success = false;
                }

                return success;
            }
        }

        /// <summary>
        ///     Loads the validation template into the planogram
        /// </summary>
        private static Boolean LoadValidationTemplate(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // load the validation template into the planogram
                context.Planogram.ValidationTemplate.LoadFrom(context.ValidationTemplate);

                // log the success
                context.LogInformation(EventLogEvent.PlanogramValidationTemplateAdded, context.ValidationTemplateName);

                // return success
                return true;
            }
        }

        #endregion
    }
}
