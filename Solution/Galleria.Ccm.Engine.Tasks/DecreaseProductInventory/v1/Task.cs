﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31560 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Framework.Logging;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.Engine.Tasks.DecreaseProductInventory.v1
{
    /// <summary>
    /// Decrease each specified product individually either a specific unit value to reduce to or a number of facings to reduce by.
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter
        {
            DecreaseProducts = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.DecreaseProductInventory_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.DecreaseProductInventory_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_MerchandisePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }

        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // Decrease product details
            this.RegisterParameter(new TaskParameter(
                Parameter.DecreaseProducts,
                Message.DecreaseProductInventory_DecreaseProduct_Name,
                Message.DecreaseProductInventory_DecreaseProduct_Description,
                Message.ParameterCategory_Default,
                TaskParameterType.DecreaseProductMultiple,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when executing this task
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (var taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!PopulateLookups(taskContext)) return;
                if (!ReduceProductInventory(taskContext)) return;
                if (!ReMerchandisePlanogram(taskContext)) return;
                if (!LogCompletion(taskContext)) return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validate task parameters
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                #region Decrease Products

                //  Validate that the DecreaseProducts parameter exists.
                if (this.Parameters[Parameter.DecreaseProducts].Value != null)
                {
                    try
                    {
                        context.DecreaseProductDetails.AddRange(this.Parameters[Parameter.DecreaseProducts].Values.
                                                                    Select(item => new DecreaseProductSelection(item.Value1.ToString(), (ProductDecreaseType)item.Value2, Convert.ToSingle(item.Value3))).ToList());
                    }
                    catch
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue,
                                        Message.DecreaseProductInventory_DecreaseProduct_Name,
                                        Message.Error_InvalidParameterValue);
                        return false;
                    }
                }

                #endregion
                
                return true;
            }
        }

        /// <summary>
        ///     Validate the planogram details required by the task.
        /// </summary>
        private static Boolean ValidatePlanogram(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                var success = true;

                #region Validate Products and Positions

                //  If there are no products in the planogram the task has nothing to do.
                if (!context.Planogram.Products.Any())
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoProductsInPlanogram);
                    success = false;
                }
                //  OR If there are no positions in the planogram the task has nothing to do.
                else if (!context.Planogram.Positions.Any())
                {
                    context.LogInformation(EventLogEvent.NothingForTaskToDo, Message.TaskLogging_NoPositionsInPlanogram);
                    success = false;
                }

                #endregion

                //  return success.
                return success;
            }
        }

        /// <summary>
        /// Gets the list of positions to decrease
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private Boolean PopulateLookups(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Get the decrease positions from the planogram.
                foreach (DecreaseProductSelection decreaseProductSelection in taskContext.DecreaseProductDetails)
                {
                    List<PlanogramPosition> positions = taskContext.Planogram.Positions.Where(p => p.GetPlanogramProduct().Gtin == decreaseProductSelection.DecreaseProductGtin).ToList();

                    if (positions.Any())
                    {
                        if (!taskContext.DecreaseProducts.ContainsKey(decreaseProductSelection.DecreaseProductGtin))
                        {
                            taskContext.DecreaseProducts.Add(decreaseProductSelection.DecreaseProductGtin, positions);
                        }
                    }
                    else
                    {
                        // no positions on the plan to decrease...
                        taskContext.LogWarning(EventLogEvent.PlanogramProductInventoryDecreasedNoProductPositions, decreaseProductSelection.DecreaseProductGtin);
                    }
                }

                taskContext.DecreaseProductSelectionByGtin = taskContext.DecreaseProductDetails.ToDictionary(p => p.DecreaseProductGtin, p => p);

                return true;
            }
        }

        /// <summary>
        /// Main method to iterate through products to be decreased.
        /// </summary>
        /// <param name="taskContext">The current task context</param>
        /// <returns></returns>
        private static Boolean ReduceProductInventory(TaskContext taskContext)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                foreach (KeyValuePair<String,List<PlanogramPosition>> decreaseProduct in taskContext.DecreaseProducts)
                {
                    DecreaseProductSelection decreaseProductSelection = taskContext.DecreaseProductSelectionByGtin[decreaseProduct.Key];
                    List<PlanogramPosition> planogramPositions = decreaseProduct.Value;
                    
                    Boolean validProductReduction = ValidateProductReduction(taskContext, decreaseProduct.Key, decreaseProductSelection, planogramPositions);

                    if (validProductReduction)
                    {
                        ReduceInventory(taskContext, decreaseProduct.Key, decreaseProductSelection, planogramPositions);
                    }
                }

                return true;
            }
        }
        
        /// <summary>
        /// Validates if the product can be reduced
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="productGtin"></param>
        /// <param name="decreaseProductSelection"></param>
        /// <param name="planogramPositions"></param>
        /// <returns></returns>
        private static Boolean ValidateProductReduction(TaskContext taskContext, String productGtin, DecreaseProductSelection decreaseProductSelection, List<PlanogramPosition> planogramPositions)
        {
            Boolean validProductReduction = true;
            Single totalPlanUnits = 0;
            Int32 totalPlanFacings = 0;

            // validate that recuction on plan can occur
            switch (decreaseProductSelection.ProductDecreaseType)
            {
                case ProductDecreaseType.ReduceUnitsTo:

                    totalPlanUnits = GetTotalUnitsForAllPositions(planogramPositions);

                    if (totalPlanUnits <= decreaseProductSelection.ProductDecreaseValue)
                    {
                        // total plan positions already at the desired unit value, log message and continue
                        taskContext.LogInformation(EventLogEvent.PlanogramPositionUnableToDecreaseInventory, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseUnits, Convert.ToInt32(decreaseProductSelection.ProductDecreaseValue)));
                        validProductReduction = false;
                    }

                    decreaseProductSelection.TargetUnits = decreaseProductSelection.ProductDecreaseValue;
                    break;

                case ProductDecreaseType.ReduceToCasePackMultiplier:

                    PlanogramProduct product = planogramPositions.First().GetPlanogramProduct();
                    if (product == null) validProductReduction = false;

                    totalPlanUnits = GetTotalUnitsForAllPositions(planogramPositions);

                    if (totalPlanUnits <= decreaseProductSelection.ProductDecreaseValue * product.CasePackUnits)
                    {
                        // total plan positions already at the desired unit value, log message and continue
                        taskContext.LogInformation(EventLogEvent.PlanogramPositionUnableToDecreaseInventory, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseUnits, Math.Round(decreaseProductSelection.ProductDecreaseValue * product.CasePackUnits, 2)));
                        validProductReduction = false;
                    }

                    decreaseProductSelection.TargetUnits = decreaseProductSelection.ProductDecreaseValue * product.CasePackUnits;
                    break;

                case ProductDecreaseType.ReduceFacingsTo:

                    totalPlanFacings = GetTotalFacingsForAllPositions(planogramPositions);

                    if (totalPlanFacings <= decreaseProductSelection.ProductDecreaseValue)
                    {
                        // total plan positions already at the desired facing value, log message and continue
                        taskContext.LogInformation(EventLogEvent.PlanogramPositionUnableToDecreaseInventory, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseFacings, Convert.ToInt32(decreaseProductSelection.ProductDecreaseValue)));
                        validProductReduction = false;
                    }

                    decreaseProductSelection.TargetFacings = decreaseProductSelection.ProductDecreaseValue;
                    break;

                case ProductDecreaseType.ReduceFacingsBy:

                    totalPlanFacings = GetTotalFacingsForAllPositions(planogramPositions);
                    decreaseProductSelection.TargetFacings = totalPlanFacings - decreaseProductSelection.ProductDecreaseValue;
                    break;
            }
            return validProductReduction;
        }

        /// <summary>
        /// Reduces inventory for the given product and positions
        /// </summary>
        /// <param name="taskContext"></param>
        /// <param name="productGtin"></param>
        /// <param name="decreaseProductSelection"></param>
        /// <param name="planogramPositions"></param>
        private static void ReduceInventory(TaskContext taskContext, String productGtin, DecreaseProductSelection decreaseProductSelection, List<PlanogramPosition> planogramPositions)
        {
            Single totalPlanUnits = 0;
            Int32 totalPlanFacings = 0;
            Single initialTotalUnits = 0;
            Int32 initialPlanFacings = 0;

            switch (decreaseProductSelection.ProductDecreaseType)
            {
                case ProductDecreaseType.ReduceUnitsTo:
                case ProductDecreaseType.ReduceToCasePackMultiplier:

                    initialTotalUnits = GetTotalUnitsForAllPositions(planogramPositions);

                    while (true)
                    {
                        totalPlanUnits = GetTotalUnitsForAllPositions(planogramPositions);
                        List<PlanogramPosition> cannotReducePosition = new List<PlanogramPosition>();

                        foreach (PlanogramPosition decreasePosition in planogramPositions)
                        {
                            Single positionTotalUnit = decreasePosition.TotalUnits;

                            if (totalPlanUnits > decreaseProductSelection.TargetUnits)
                            {
                                if (!decreasePosition.DecreaseUnits(decreasePosition.GetPlanogramProduct(), decreasePosition.GetPlanogramSubComponentPlacement(), null))
                                {
                                    // unable to decrease further...
                                    taskContext.LogInformation(EventLogEvent.PlanogramPositionUnableToFurtherDecreaseInventory, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseUnits, decreaseProductSelection.TargetUnits));
                                    cannotReducePosition.Add(decreasePosition);
                                }
                                else
                                {
                                    if (!taskContext.ProductGtinsDecreased.Contains(productGtin)) { taskContext.ProductGtinsDecreased.Add(productGtin); }
                                    decreasePosition.RecalculateUnits(decreasePosition.GetPlanogramProduct(), decreasePosition.GetPlanogramSubComponentPlacement());

                                    totalPlanUnits -= positionTotalUnit - decreasePosition.TotalUnits;
                                }
                            }
                        }

                        if (cannotReducePosition.Any())
                        {
                            foreach (PlanogramPosition decreasePosition in cannotReducePosition)
                            {
                                planogramPositions.Remove(decreasePosition);
                            }
                        }

                        // can reduce until there are no more products that can be reduced or we have reached our target
                        if (!planogramPositions.Any() || totalPlanUnits <= decreaseProductSelection.TargetUnits) break;
                    }

                    break;

                case ProductDecreaseType.ReduceFacingsTo:
                case ProductDecreaseType.ReduceFacingsBy:

                    initialPlanFacings = GetTotalFacingsForAllPositions(planogramPositions);

                    while (true)
                    {
                        totalPlanFacings = GetTotalFacingsForAllPositions(planogramPositions);
                        List<PlanogramPosition> cannotReducePosition = new List<PlanogramPosition>();

                        foreach (PlanogramPosition decreasePosition in planogramPositions)
                        {
                            PlanogramSubComponentPlacement decreaseSubComponentPlacement = decreasePosition.GetPlanogramSubComponentPlacement();

                            Int32 positionFacingWide = decreasePosition.GetFacings(decreaseSubComponentPlacement);

                            if (totalPlanFacings > decreaseProductSelection.TargetFacings)
                            {
                                if (!decreasePosition.DecreaseFacings(decreasePosition.GetPlanogramProduct(), decreaseSubComponentPlacement, null))
                                {
                                    // unable to decrease further...
                                    taskContext.LogInformation(EventLogEvent.PlanogramPositionUnableToFurtherDecreaseInventory, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseFacings, decreaseProductSelection.ProductDecreaseType == ProductDecreaseType.ReduceFacingsBy ? decreaseProductSelection.ProductDecreaseValue : decreaseProductSelection.TargetFacings));
                                    cannotReducePosition.Add(decreasePosition);
                                }
                                else
                                {
                                    if (!taskContext.ProductGtinsDecreased.Contains(productGtin)) { taskContext.ProductGtinsDecreased.Add(productGtin); }
                                    decreasePosition.RecalculateUnits(decreasePosition.GetPlanogramProduct(), decreaseSubComponentPlacement);

                                    totalPlanFacings -= positionFacingWide - decreasePosition.GetFacings(decreaseSubComponentPlacement);
                                }
                            }
                        }

                        if (cannotReducePosition.Any())
                        {
                            foreach (PlanogramPosition decreasePosition in cannotReducePosition)
                            {
                                planogramPositions.Remove(decreasePosition);
                            }
                        }

                        // can reduce until there are no more products that can be reduced or we have reached our target
                        if (!planogramPositions.Any() || totalPlanFacings <= decreaseProductSelection.TargetFacings) break;
                    }

                    break;
            }

            if (initialTotalUnits > 0 && ((initialTotalUnits - totalPlanUnits) > 0)) { taskContext.LogInformation(EventLogEvent.PlanogramProductInventoryDecreasedBy, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseUnits, (initialTotalUnits - totalPlanUnits))); }
            if (initialPlanFacings > 0 && ((initialPlanFacings - totalPlanFacings) > 0)) { taskContext.LogInformation(EventLogEvent.PlanogramProductInventoryDecreasedBy, productGtin, String.Format(Message.DecreaseProductInventory_DecreaseFacings, (initialPlanFacings - totalPlanFacings))); }            
        }

        private static Int32 GetTotalFacingsForAllPositions(List<PlanogramPosition> planogramPositions)
        {
            Int32 totalPlanFacings = 0;

            foreach (PlanogramPosition position in planogramPositions)
            {
                totalPlanFacings += position.GetFacings(position.GetPlanogramSubComponentPlacement());
            }
            return totalPlanFacings;
        }

        private static Single GetTotalUnitsForAllPositions(List<PlanogramPosition> planogramPositions)
        {
            Single totalPlanUnits = 0;

            foreach (PlanogramPosition position in planogramPositions)
            {
                totalPlanUnits += position.TotalUnits;
            }
            return totalPlanUnits;
        }

        /// <summary>
        /// Re-merchandises the planogram, ensuring
        /// that autofill is applied to the final plan
        /// </summary>
        private static Boolean ReMerchandisePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // remerchandise the planogram
                context.RemerchandisePlanogram();

                // return success
                return true;
            }
        }

        /// <summary>
        ///     Log final details about the task execution.
        /// </summary>
        /// <param name="context">The <see cref="DecreaseProductInventory.v1.TaskContext"/> contaning what needs to be logged.</param>
        private static Boolean LogCompletion(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                context.LogInformation(EventLogEvent.PlanogramProductInventoryDecreased, context.ProductGtinsDecreased.Count);

                // return success
                return true;
            }
        }

        #endregion
    }
}