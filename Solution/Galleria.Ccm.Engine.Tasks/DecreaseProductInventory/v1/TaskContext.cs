﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31560 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.DecreaseProductInventory.v1
{
    /// <summary>
    ///     Provides the context to run <see cref="Task"/> in.
    /// </summary>
    public class TaskContext : Ccm.Engine.TaskContext
    {
        private List<DecreaseProductSelection> _decreaseProductDetails;
        private Dictionary<String, List<PlanogramPosition>> _decreaseProducts;
        private Dictionary<String, DecreaseProductSelection> _decreaseProductSelectionByGtin;
        private List<String> _productGtinsDecreased;

        #region Constructor

        /// <summary>
        ///     Instantiates a new instance of <see cref="TaskContext"/>.
        /// </summary>
        public TaskContext(ITaskContext context) : base(context) { }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the decrease product detail (decrease product gtin, decrease type, decrease value).
        /// </summary>
        internal List<DecreaseProductSelection> DecreaseProductDetails
        {
            get { return _decreaseProductDetails ?? (_decreaseProductDetails = new List<DecreaseProductSelection>()); }
        }

        public Dictionary<String, List<PlanogramPosition>> DecreaseProducts
        {
            get { return _decreaseProducts ?? (_decreaseProducts = new Dictionary<String, List<PlanogramPosition>>()); }
        }

        internal Dictionary<String, DecreaseProductSelection> DecreaseProductSelectionByGtin
        {
            get { return _decreaseProductSelectionByGtin ?? (_decreaseProductSelectionByGtin = new Dictionary<String, DecreaseProductSelection>()); }
            set { _decreaseProductSelectionByGtin = value; }
        }

        /// <summary>
        /// The product gtins that had their inventory decreased by the task.
        /// </summary>
        public List<String> ProductGtinsDecreased 
        {
            get { return _productGtinsDecreased ?? (_productGtinsDecreased = new List<String>()); }
        }

        #endregion
    }

    /// <summary>
    /// A simple container for the decrease product detail
    /// </summary>
    internal class DecreaseProductSelection
    {
        public String DecreaseProductGtin { get; private set; }
        public ProductDecreaseType ProductDecreaseType { get; private set; }
        public Single ProductDecreaseValue { get; private set; }
        public Single TargetUnits { get; set; }
        public Single TargetFacings { get; set; }

        public DecreaseProductSelection(String decreaseProductGtin, ProductDecreaseType productDecreaseType, Single productDecreaseValue)
        {
            DecreaseProductGtin = decreaseProductGtin;
            ProductDecreaseType = productDecreaseType;
            ProductDecreaseValue = productDecreaseValue;
        }
    }
}