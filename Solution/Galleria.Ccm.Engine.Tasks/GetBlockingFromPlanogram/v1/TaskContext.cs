﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810
// V8-29597 : D.Pleasance
//  Created
#endregion

#region Version History: CCM830
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1
{
    /// <summary>
    /// Task context object used to pass parameters
    /// throughout the execution of the task
    /// </summary>
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Constructors
        
        private Int32 _planogramId = 0;
        private String _planogramName = String.Empty;
        private String _metricProfileName = String.Empty;

        private Planogram _planogramSource = null;
        private IPlanogramMetricProfile _metricProfile = null;
        private PlanogramBlocking _blockingSource = null;
        
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the planogram Id
        /// </summary>
        public Int32 PlanogramId
        {
            get { return _planogramId; }
            set { _planogramId = value; }
        }

        /// <summary>
        /// Gets or sets the planogram name
        /// </summary>
        public String PlanogramName
        {
            get { return _planogramName; }
            set { _planogramName = value; }
        }

        /// <summary>
        /// Gets or sets the metric profile name
        /// </summary>
        public String MetricProfileName
        {
            get { return _metricProfileName; }
            set { _metricProfileName = value; }
        }

        /// <summary>
        /// Gets or sets the PlanogramSource
        /// </summary>
        public Planogram PlanogramSource
        {
            get { return _planogramSource; }
            set { _planogramSource = value; }
        }

        /// <summary>
        /// Gets or sets the Metric Profile
        /// </summary>
        public IPlanogramMetricProfile MetricProfile
        {
            get { return _metricProfile; }
            set { _metricProfile = value; }
        }

        /// <summary>
        /// Gets or sets the Blocking Source
        /// </summary>
        public PlanogramBlocking BlockingSource
        {
            get { return _blockingSource; }
            set { _blockingSource = value; }
        }



        #endregion
    }
}
