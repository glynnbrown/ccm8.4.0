﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28991 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM810
// V8-29597 : D.Pleasance
//  Refactored task to use TaskContext
// V8-29057 : D.Pleasance
//  Amended to capture PlanogramBlockingResultType errors
// V8-29562 : D.Pleasance
//  Added PlanogramBlockingAddedPerformance
// V8-30123 : N.Foster
// Ensure planogram is remerchandised when only required
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Added isNullAllowed to Metric Profile parameter.
// V8-31174 : A.Kuszyk
//  Made Planogram parameter nullable and logged information if a null value is provided.
// V8-31378 : A.Silva
//  Amended GetBlockingFromPlanogram to record a warning if a Metric Profile was provided, but the performance data was insufficient to calculate the blocking.
// V8-31495 : D.Pleasance
//  Added validation to the task to ensure - The planogram has an assortment, the planogram assortment has products, the planogram assortment has ranged products.
#endregion

#region Version History : CCM830
// V8-32449 : A.Kuszyk
//  Added performance ratio to call to ApplyPerformanceData.
// V8-32539 : A.Kuszyk
//  Amended logging to only log missing assortment information if a metric profile is supplied.
// V8-13996 : J.Pickup
//  When fetching the metric profile obejcts, their order is now normalised against the order of metrics within the planogram performance. 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1
{
    [Serializable]
    public class Task : TaskBase
    {
        /// <summary>
        /// The ratio to use when weighting performance data against inventory data for
        /// space re-calculations. This may become a task parameter in the future.
        /// </summary>
        private const Single _performanceWeightingRatio = 0.5f;

        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 1; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            PlanogramName = 0,
            MetricProfileName = 1
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetBlockingFromPlanogram_Name; } 
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetBlockingByName_Description; } 
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_UpdatePlanogram; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            this.RegisterParameter(new TaskParameter(
                Parameter.PlanogramName,
                Message.GetBlockingFromPlanogram_PlanogramName_Name, 
                Message.GetBlockingFromPlanogram_PlanogramName_Description,
                Message.GetBlockingByName_Category,
                TaskParameterType.PlanogramBlockingNameSingle,
                null,
                false,
                false,
                parentId: null,
                isRequired: null,
                isNullAllowed: () => { return true; },
                isValid: null));

            this.RegisterParameter(new TaskParameter(
                Parameter.MetricProfileName,
                Message.GetAndReCalculateBlockingByName_MetricProfileName,
                Message.GetAndReCalculateBlockingByName_MetricProfileName_Description,
                Message.GetBlockingByName_Category,
                TaskParameterType.MetricProfileNameSingle,
                null,
                false,
                false,
                parentId: null,
                isRequired: null,
                isNullAllowed: () => { return true; },
                isValid: null));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!GetAndValidateParameters(taskContext)) return;
                if (!GetParameterObjects(taskContext)) return;
                if (!ValidatePlanogram(taskContext)) return;
                if (!GetBlockingFromPlanogram(taskContext)) return;
            }
        }
        
        #endregion

        #region Methods

        private Boolean GetAndValidateParameters(
            TaskContext context)
        {
            // Planogram Id.
            if (this.Parameters[Parameter.PlanogramName].Value == null)
            {
                // A null value is allowed here.
                context.LogInformation(
                    EventLogEvent.NothingForTaskToDo, 
                    Message.GetBlockingFromPlanogram_NoPlanogramProvided);
                return false;
            }
            context.PlanogramName = Convert.ToString(this.Parameters[Parameter.PlanogramName].Value.Value1);
            context.PlanogramId = Convert.ToInt32(this.Parameters[Parameter.PlanogramName].Value.Value2);

            // Metric profile name. It's ok for this parameter to be null, because if it is, we just won't re-calculate
            // the blocking.
            if (this.Parameters[Parameter.MetricProfileName].Value != null)
            {
                context.MetricProfileName = Convert.ToString(Parameters[Parameter.MetricProfileName].Value.Value1);
                // If the string is null or empty at this stage, that's ok.
            }

            return true;
        }

        private static Boolean GetParameterObjects(TaskContext context)
        {
            try
            {
                context.PlanogramSource = Package.FetchById(context.PlanogramId).Planograms.FirstOrDefault();
            }
            catch (DataPortalException ex)
            {
                if (ex.GetBaseException() is DtoDoesNotExistException)
                {
                    context.LogError(
                        EventLogEvent.InvalidTaskParameterValue,
                        Message.GetBlockingFromPlanogram_PlanogramName_Name,
                        context.PlanogramName);
                    return false;
                }
                throw ex;
            }
            if (context.PlanogramSource == null)
            {
                context.LogError(
                    EventLogEvent.InvalidTaskParameterValue,
                    Message.GetBlockingFromPlanogram_PlanogramName_Name,
                    context.PlanogramName);
                return false;
            }
            context.BlockingSource = context.PlanogramSource.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final);
            if (context.BlockingSource == null)
            {
                context.LogError(EventLogEvent.NothingForTaskToDo, Message.GetBlockingFromPlanogram_NoFinalBlockingsInSource);
                return false;
            }

            // Only try to get a metric profile if we have a name at this stage.
            if (!String.IsNullOrEmpty(context.MetricProfileName))
            {
                try
                {
                    MetricList globalMetricList = MetricList.FetchByEntityId(context.EntityId);

                    context.MetricProfile = MetricProfile.FetchByEntityIdName(context.EntityId, context.MetricProfileName)?.ToPlanogramMetricProfile(context.Planogram.Performance, globalMetricList);

                    if (context.MetricProfile == null)
                    {
                        context.LogError(EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile);
                        return false;
                    }
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetAndReCalculateBlockingByName_MetricProfileName, context.MetricProfileName);
                        return false;
                    }
                    throw ex;
                }
            }

            return true;
        }

        /// <summary>
        ///     Validates the contents of the task's <c>Planogram</c> before running the task.
        /// </summary>
        /// <param name="context">The <see cref="TaskContext"/> to store the task state.</param>
        /// <returns><c>True</c> if the <c>Planogram</c> has everything required by the task; <c>False</c> otherwise.</returns>
        /// <remarks>
        ///     <list type="bullet">
        ///         <listheader>
        ///             <term>Events Logged</term>
        ///         </listheader>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, Missing Assortment.</term>
        ///             <description>The planogram has no Assortment.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, Missing Assortment Products.</term>
        ///             <description>The planogram has no Assortment Products.</description>
        ///         </item>
        ///         <item>
        ///             <term>Error: Insufficient Planogram Content, No Ranged Assortment Products.</term>
        ///             <description>The planogram has no Ranged Assortment products to process.</description>
        ///         </item>        
        ///     </list>
        /// </remarks>
        private static Boolean ValidatePlanogram(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Planogram planogram = context.Planogram;

                #region Validate Assortment

                //  Verify there is an assortment present.
                if (context.Planogram.Assortment == null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_Assortment,
                                     Message.TaskLogging_NoAssortmentWasPresent);
                    return false;
                }

                //  Verify the assortment contains products.
                if ((context.Planogram.Assortment.Products == null ||
                    !context.Planogram.Assortment.Products.Any()) &&
                    context.MetricProfile != null)
                {
                    context.LogError(EventLogEvent.InsufficientPlanogramContent,
                                     Message.TaskLogging_AssortmentProducts,
                                     Message.TaskLogging_NoAssortmentProductsListWasPresent);
                    return false;
                }

                //  Verify at least one assortment product is ranged.
                if (!context.Planogram.Assortment.Products.Any(p => p.IsRanged) && context.MetricProfile != null)
                {
                    context.LogError(EventLogEvent.NothingForTaskToDo,
                                           Message.TaskLogging_NoProductsWereRangedInThePlanogramAssortment);
                    return false;
                }

                #endregion
                
                return true;
            }
        }

        private static Boolean GetBlockingFromPlanogram(TaskContext context)
        {
            // Clear the existing blocking and then add the new blocking as initial (unchanged).
            context.Planogram.Blocking.Clear();
            PlanogramBlocking initialBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial, context.BlockingSource);
            context.Planogram.Blocking.Add(initialBlocking);
            context.LogInformation(
                EventLogEvent.PlanogramBlockingAdded, context.PlanogramSource.Name, PlanogramBlockingTypeHelper.FriendlyNames[initialBlocking.Type]);

            // Performance adjusted
            PlanogramBlocking adjustedBlocking = null;
            if (context.MetricProfile != null)
            {
                //  Check that there is performance data to recalculate.
                if (IsEmptyOrMissing(context.MetricProfile) ||
                    context.Planogram.Performance.PerformanceData.All(IsEmptyOrMissingPerformanceData))
                {
                    context.LogWarning(EventLogEvent.PlanogramInsufficientPerformanceDataToRecalculateBlocking);
                }
                else
                {
                    adjustedBlocking =
                        PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied, initialBlocking);
                    context.Planogram.Blocking.Add(adjustedBlocking);
                    try
                    {
                        adjustedBlocking.ApplyPerformanceData(context.MetricProfile, _performanceWeightingRatio);
                        context.LogInformation(
                            EventLogEvent.PlanogramBlockingAddedPerformance,
                            context.PlanogramSource.Name,
                            PlanogramBlockingTypeHelper.FriendlyNames[adjustedBlocking.Type],
                            context.MetricProfile.Name);
                    }
                    catch (PlanogramBlockingException)
                    {
                        context.LogWarning(EventLogEvent.PlanogramBlockingMissingProducts);
                        context.Planogram.Blocking.Remove(adjustedBlocking);
                        adjustedBlocking = null;
                    }
                }
            }

            // Now, add the adjusted or initial blocking as final.
            PlanogramBlocking finalBlocking =
                PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, adjustedBlocking ?? initialBlocking);
            context.Planogram.Blocking.Add(finalBlocking);

            // Having added the blocking as final, try to apply it to the fixtures.
            try
            {
                // Apply blocking to fixtures, returning any block issues
                List<PlanogramBlockingResultType> planogramBlockingResults = finalBlocking.ApplyToFixtures();

                if (planogramBlockingResults != null && planogramBlockingResults.Any())
                {
                    Int32 insufficientComponentSpaceResult = planogramBlockingResults.Where(p => p == PlanogramBlockingResultType.InsufficientComponentSpace).Count();
                    context.LogError(EventLogEvent.PlanogramBlockingInsufficientComponentSpace, insufficientComponentSpaceResult);
                    return false;
                }

                context.LogInformation(EventLogEvent.PlanogramBlockingAdded, context.PlanogramSource.Name, PlanogramBlockingTypeHelper.FriendlyNames[finalBlocking.Type]);
            }
            catch (PlanogramBlockingException ex)
            {
                // If we were unsuccessful, remove the blocking and log the error.
                context.Planogram.Blocking.Remove(finalBlocking);
                context.LogError(EventLogEvent.PlanogramBlockingApplyToFixturesFailed, ex.Message);
                return false;
            }

            // return success
            return true;
        }

        private static Boolean IsEmptyOrMissingPerformanceData(IPlanogramPerformanceData arg)
        {
            return arg == null || EnumeratePerformanceMetricValues(arg).All(value => value.EqualTo(0));
        }

        private static IEnumerable<Single> EnumeratePerformanceMetricValues(IPlanogramPerformanceData planogramPerformanceData)
        {
            yield return planogramPerformanceData.P1 ?? 0;
            yield return planogramPerformanceData.P2 ?? 0;
            yield return planogramPerformanceData.P3 ?? 0;
            yield return planogramPerformanceData.P4 ?? 0;
            yield return planogramPerformanceData.P5 ?? 0;
            yield return planogramPerformanceData.P6 ?? 0;
            yield return planogramPerformanceData.P7 ?? 0;
            yield return planogramPerformanceData.P8 ?? 0;
            yield return planogramPerformanceData.P9 ?? 0;
            yield return planogramPerformanceData.P10 ?? 0;
            yield return planogramPerformanceData.P11 ?? 0;
            yield return planogramPerformanceData.P12 ?? 0;
            yield return planogramPerformanceData.P13 ?? 0;
            yield return planogramPerformanceData.P14 ?? 0;
            yield return planogramPerformanceData.P15 ?? 0;
            yield return planogramPerformanceData.P16 ?? 0;
            yield return planogramPerformanceData.P17 ?? 0;
            yield return planogramPerformanceData.P18 ?? 0;
            yield return planogramPerformanceData.P19 ?? 0;
            yield return planogramPerformanceData.P20 ?? 0;
        }

        private static Boolean IsEmptyOrMissing(IPlanogramMetricProfile metricProfile)
        {
            return metricProfile == null || EnumerateRatios(metricProfile).All(ratio => ratio.EqualTo(0));
        }

        private static IEnumerable<Single> EnumerateRatios(IPlanogramMetricProfile metricProfile)
        {
            yield return metricProfile.Metric1Ratio ?? 0;
            yield return metricProfile.Metric2Ratio ?? 0;
            yield return metricProfile.Metric3Ratio ?? 0;
            yield return metricProfile.Metric4Ratio ?? 0;
            yield return metricProfile.Metric5Ratio ?? 0;
            yield return metricProfile.Metric6Ratio ?? 0;
            yield return metricProfile.Metric7Ratio ?? 0;
            yield return metricProfile.Metric8Ratio ?? 0;
            yield return metricProfile.Metric9Ratio ?? 0;
            yield return metricProfile.Metric10Ratio ?? 0;
            yield return metricProfile.Metric11Ratio ?? 0;
            yield return metricProfile.Metric12Ratio ?? 0;
            yield return metricProfile.Metric13Ratio ?? 0;
            yield return metricProfile.Metric14Ratio ?? 0;
            yield return metricProfile.Metric15Ratio ?? 0;
            yield return metricProfile.Metric16Ratio ?? 0;
            yield return metricProfile.Metric17Ratio ?? 0;
            yield return metricProfile.Metric18Ratio ?? 0;
            yield return metricProfile.Metric19Ratio ?? 0;
            yield return metricProfile.Metric20Ratio ?? 0;
        }

        #endregion
    }
}