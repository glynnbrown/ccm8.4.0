﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29520 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Tasks.GetRenumberingStrategyByName.v1
{
    public class TaskContext : Galleria.Ccm.Engine.TaskContext
    {
        #region Fields
        private String _renumberingStrategyName;
        private RenumberingStrategy _renumberingStrategy;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
            : base(context)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the renumbering strategy name
        /// </summary>
        public String RenumberingStrategyName
        {
            get { return _renumberingStrategyName; }
            set { _renumberingStrategyName = value; }
        }

        /// <summary>
        /// Gets or sets the renumbering strategy
        /// </summary>
        public RenumberingStrategy RenumberingStrategy
        {
            get { return _renumberingStrategy; }
            set { _renumberingStrategy = value; }
        }
        #endregion
    }
}
