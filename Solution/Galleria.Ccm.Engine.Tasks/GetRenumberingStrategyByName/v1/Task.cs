﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27562 : A.Silva
//  Created.
// V8-27765 : A.Kuszyk
//  Added basic logging.
// V8-28060 : A.Kuszyk
//  Added detailed logging.
#endregion
#region Version History: CCM801
// V8-28612 : A.Kuszyk
//  Defended against null parameter values.
#endregion
#region Version History: CCM803
// V8-29597 : N.Foster
//  Code Review
#endregion
#region Version History: CCM810
// V8-29696 : M.Brumby
//  Marked the stategy as dirty so it will save
// V8-30123 : N.Foster
//  Ensure planogram is remerchandised when only required
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Tasks.GetRenumberingStrategyByName.v1
{
    /// <summary>
    /// Task that loads a renumbering strategy
    /// into a planogram
    /// </summary>
    [Serializable]
    public class Task : TaskBase
    {
        #region Version
        private Byte _majorVersion = 1; // holds the tasks major version number
        private Byte _minorVersion = 0; // holds the tasks minor version number
        #endregion

        #region Parameter
        private enum Parameter : int
        {
            RenumberingStrategyName  = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task name
        /// </summary>
        public override String Name
        {
            get { return Message.GetRenumberingStrategy_Task_Name; }
        }

        /// <summary>
        /// The task description
        /// </summary>
        public override string Description
        {
            get { return Message.GetRenumberingStrategy_Task_Description; }
        }

        /// <summary>
        /// The task category
        /// </summary>
        public override String Category
        {
            get { return Message.TaskCategory_PlanogramMaintenance; }
        }

        /// <summary>
        /// The tasks major version number
        /// </summary>
        public override Byte MajorVersion
        {
            get { return _majorVersion; }
        }

        /// <summary>
        /// The tasks minor version number
        /// </summary>
        public override Byte MinorVersion
        {
            get { return _minorVersion; }
        }
        #endregion

        #region Parameter Registration
        /// <summary>
        /// Called when registering the tasks parameters
        /// </summary>
        protected override void OnRegisterParameters()
        {
            // renumbering strategy name
            RegisterParameter(new TaskParameter(
                Parameter.RenumberingStrategyName,
                Message.GetRenumberingStrategy_Parameter_RenumberingStrategyName_Name,
                Message.GetRenumberingStrategy_Parameter_RenumberingStrategyName_Description,
                Message.TaskCategory_PlanogramMaintenance,
                TaskParameterType.RenumberingStrategyNameSingle,
                null,
                false,
                false));
        }
        #endregion

        #region Execution
        /// <summary>
        /// Called when this task is executed
        /// </summary>
        protected override void OnExecute(ITaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (TaskContext taskContext = new TaskContext(context))
            {
                if (!ValidateParameters(taskContext)) return;
                if (!GetRenumberingStrategy(taskContext)) return;
                if (!LoadRenumberingStrategy(taskContext)) return;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Validates the parameters for this task
        /// </summary>
        private Boolean ValidateParameters(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                Boolean success = true;

                #region RenumberingStrategyName

                // validate that the renumbering strategy name parameter exists
                if ((!this.Parameters.Contains((Int32)Parameter.RenumberingStrategyName)) ||
                    (this.Parameters[Parameter.RenumberingStrategyName].Value == null) ||
                    (this.Parameters[Parameter.RenumberingStrategyName].Value.Value1 == null))
                {
                    context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetRenumberingStrategy_Parameter_RenumberingStrategyName_Name, Message.Error_NoValue);
                    success = false;
                }
                else
                {
                    // get the assortment name
                    context.RenumberingStrategyName = Convert.ToString(this.Parameters[Parameter.RenumberingStrategyName].Value.Value1);
                    if ((String.IsNullOrEmpty(context.RenumberingStrategyName)) || (String.IsNullOrWhiteSpace(context.RenumberingStrategyName)))
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetRenumberingStrategy_Parameter_RenumberingStrategyName_Name, Message.Error_NoValue);
                        success = false;
                    }
                }

                #endregion

                return success;
            }
        }

        /// <summary>
        /// Loads the renumbering strategy from the database
        /// </summary>
        private static Boolean GetRenumberingStrategy(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to fetch the assortment by its name
                try
                {
                    context.RenumberingStrategy = RenumberingStrategy.FetchByEntityIdName(context.EntityId, context.RenumberingStrategyName);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is DtoDoesNotExistException)
                    {
                        context.LogError(EventLogEvent.InvalidTaskParameterValue, Message.GetRenumberingStrategy_Parameter_RenumberingStrategyName_Name, context.RenumberingStrategyName);
                        return false;
                    }
                    throw ex;
                }

                // return success
                return true;
            }
        }

        /// <summary>
        /// Loads the renumbering strategy into the planogram
        /// </summary>
        private static Boolean LoadRenumberingStrategy(TaskContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // load the renumbering strategy into the planogram
                context.Planogram.RenumberingStrategy.LoadFrom(context.RenumberingStrategy);

                //Mark as dirty so it will save later.
                context.Planogram.RenumberingStrategy.MarkGraphAsDirty();

                // log the success
                context.LogInformation(EventLogEvent.PlanogramReNumberingStrategyAdded, context.RenumberingStrategyName);

                // return success
                return true;
            }
        }

        #endregion
    }
}
