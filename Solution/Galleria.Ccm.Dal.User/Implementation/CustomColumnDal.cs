﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Modified Update and Delete methods, to use the new identifiers.
// V8-26671 : A.Silva ~ Added Serializable attribute.

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added ids to child objects.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     User DAL implementation of <see cref="ICustomColumnDal"/>.
    /// </summary>
    /// <remarks>Instantiated by reflection.</remarks>
    [Serializable]
    public sealed class CustomColumnDal : DalBase, ICustomColumnDal
    {
        #region Helpers

        private CustomColumnLayoutCache Cache
        {
            get { return this.DalCache.GetCache<CustomColumnLayoutCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetches the collection of <see cref="CustomColumnDto" /> whose parent column layout's Id is like
        ///     <paramref name="columnLayoutId" />.
        /// </summary>
        /// <param name="columnLayoutId">Unique identifier for the column layout to retrive all custom columns from.</param>
        /// <returns>A collection of <see cref="CustomColumnDto" /> which match the <paramref name="columnLayoutId" /> provided.</returns>
        public IEnumerable<CustomColumnDto> FetchByColumnLayoutId(Object columnLayoutId)
        {
            List<CustomColumnDto> dtoList = new List<CustomColumnDto>();

            CustomColumnLayoutItem parent = Cache.FindCustomColumnLayoutById(Convert.ToString(columnLayoutId));
            if (parent != null)
            {
                foreach (CustomColumnItem dalObject in parent.Columns)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts a new custom column in the DAL with the data provided in the <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">Data to insert as a custom column into the DAL.</param>
        public void Insert(CustomColumnDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates a custom column in the DAL with the data provided in the <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">Data to update the custom column with.</param>
        public void Update(CustomColumnDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            CustomColumnItem dalObject = Cache.FindCustomColumnById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

    }
}