﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public class HighlightInfoDal : DalBase, IHighlightInfoDal
    {
        #region Fetch

        public IEnumerable<HighlightInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            List<HighlightInfoDto> dtoList = new List<HighlightInfoDto>();

            foreach (Object id in ids)
            {
                String fileName = id as String;
                if (File.Exists(fileName))
                {
                    HighlightInfoItem item = HighlightInfoItem.FetchById(fileName);
                    if (item != null)
                    {
                        dtoList.Add(item.GetDataTransferObject());
                    }

                }
            }

            return dtoList;
        }

        public IEnumerable<HighlightInfoDto> FetchByEntityId(Int32 entityId)
        {
            //Is not currently implemented in user dal as files have no concept of entity - not required.
            throw new NotImplementedException();
        }

        public IEnumerable<HighlightInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            //Is not currently implemented in user dal as files have no concept of entity - not required.
            throw new NotImplementedException();
        }

        #endregion
    }
}
