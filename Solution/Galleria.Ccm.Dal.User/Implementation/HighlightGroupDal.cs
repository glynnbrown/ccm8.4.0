﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion


using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class HighlightGroupDal : DalBase, IHighlightGroupDal
    {
        #region Helpers

        private HighlightDalCache Cache
        {
            get { return this.DalCache.GetCache<HighlightDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<HighlightGroupDto> FetchByHighlightId(object highlightId)
        {
            List<HighlightGroupDto> dtoList = new List<HighlightGroupDto>();

            HighlightItem highlight = Cache.FindHighlightById(Convert.ToString(highlightId));
            if (highlight != null)
            {
                foreach (HighlightGroupItem dalObject in highlight.Groups)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(HighlightGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(HighlightGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            HighlightGroupItem dalObject = Cache.FindHighlightGroupById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}
