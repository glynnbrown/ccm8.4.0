﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class PrintTemplateComponentDetailDal : DalBase, IPrintTemplateComponentDetailDal
    {
        #region Helpers

        private PrintTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PrintTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PrintTemplateComponentDetailDto FetchById(int id)
        {
            PrintTemplateComponentDetailItem dalObject = Cache.FindPrintTemplateComponentDetailById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();
            else return dalObject.GetDataTransferObject();
        }

        public IEnumerable<PrintTemplateComponentDetailDto> FetchByPrintTemplateComponentId(Int32 printTemplateComponentId)
        {
            List<PrintTemplateComponentDetailDto> dtoList = new List<PrintTemplateComponentDetailDto>();

            PrintTemplateComponentItem parentItem = Cache.FindPrintTemplateComponentById(printTemplateComponentId);
            if (parentItem != null)
            {
                foreach (PrintTemplateComponentDetailItem dalObject in parentItem.ComponentDetails)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PrintTemplateComponentDetailDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PrintTemplateComponentDetailDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PrintTemplateComponentDetailItem dalObject = Cache.FindPrintTemplateComponentDetailById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }


        public void DeleteByPrintTemplateComponentId(int printTemplateComponentId)
        {
            PrintTemplateComponentItem parentItem = Cache.FindPrintTemplateComponentById(printTemplateComponentId);
            if (parentItem != null)
            {
                foreach (PrintTemplateComponentDetailItem dalObject in parentItem.ComponentDetails.ToArray())
                {
                    parentItem.ComponentDetails.Remove(dalObject);
                }
            }
        }

        #endregion


        
    }
}
