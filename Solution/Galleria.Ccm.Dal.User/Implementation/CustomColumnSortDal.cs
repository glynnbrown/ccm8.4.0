﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Modified Update and Delete methods, to use the new identifiers.

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added ids to child objects.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     Implementation of <see cref="ICustomColumnSortDal"/> called by reflection when needed.
    /// </summary>
    public sealed class CustomColumnSortDal : DalBase, ICustomColumnSortDal
    {
        #region Helpers

        private CustomColumnLayoutCache Cache
        {
            get { return this.DalCache.GetCache<CustomColumnLayoutCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetches an collection of <see cref="CustomColumnSortDto" /> instances which have the given
        ///     <paramref name="customColumnLayoutId" /> value.
        /// </summary>
        /// <param name="customColumnLayoutId">
        ///     Unique identifier of the CustomColumnLayout that is parent to the fetched
        ///     CustomColumnSort collection.
        /// </param>
        /// <returns>
        ///     A collection of <see cref="CustomColumnSortDto" /> which belong to the given
        ///     <paramref name="customColumnLayoutId" />.
        /// </returns>
        public IEnumerable<CustomColumnSortDto> FetchByCustomColumnLayoutId(Object customColumnLayoutId)
        {
            List<CustomColumnSortDto> dtoList = new List<CustomColumnSortDto>();

            CustomColumnLayoutItem parent = Cache.FindCustomColumnLayoutById(Convert.ToString(customColumnLayoutId));
            if (parent != null)
            {
                foreach (CustomColumnSortItem dalObject in parent.ColumnSorts)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }


        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto" /> into the DAL.
        /// </summary>
        /// <param name="dto"><see cref="CustomColumnSortDto" /> instance containing the data to insert into the DAL.</param>
        public void Insert(CustomColumnSortDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates an existing DAL item with the data contained in the given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto"><see cref="CustomColumnSortDto" /> instance containing the data to update an item in the DAL with.</param>
        public void Update(CustomColumnSortDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            CustomColumnSortItem dalObject = Cache.FindCustomColumnSortById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

    }
}