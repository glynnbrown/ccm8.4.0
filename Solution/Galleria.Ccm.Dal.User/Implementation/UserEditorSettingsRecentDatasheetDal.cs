﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History CCM 830
// V8-31699 : A.Heathcote
//     Created   
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class UserEditorSettingsRecentDatasheetDal : DalBase, IUserEditorSettingsRecentDatasheetDal
    {
        #region Fetch
        public IEnumerable<UserEditorSettingsRecentDatasheetDto> FetchAll()
        {
            List<UserEditorSettingsRecentDatasheetDto> dtoList = new List<UserEditorSettingsRecentDatasheetDto>();
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentDatasheetItem item in cache.RecentDatasheet)
            {
                dtoList.Add(item.GetDataTransferObject());
            }
            return dtoList;
        }

        public UserEditorSettingsRecentDatasheetDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingsRecentDatasheetItem item in cache.RecentDatasheet)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }
            throw new DtoDoesNotExistException();
        }
        #endregion

        #region Insert
        public void Insert(UserEditorSettingsRecentDatasheetDto dto)
        {

            this.DalCache.GetCache<UserEditorSettingDalCache>().Insert(dto);
        }
        #endregion

        #region Update
        public void Update(UserEditorSettingsRecentDatasheetDto dto)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentDatasheetItem item in cache.RecentDatasheet)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }
        #endregion

        #region Delete
        public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentDatasheetItem item in cache.RecentDatasheet)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
        #endregion
            }
        }
    }
}
