﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History CCM 830
// V8-31699 : A.Heathcote
//     Created   
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class UserEditorSettingsRecentHighlightDal : DalBase, IUserEditorSettingsRecentHighlightDal
    {
        #region Fetch
        public IEnumerable<UserEditorSettingsRecentHighlightDto> FetchAll()
        {
            List<UserEditorSettingsRecentHighlightDto> dtoList = new List<UserEditorSettingsRecentHighlightDto>();
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentHighlightItem item in cache.RecentHighlight)
            {
                dtoList.Add(item.GetDataTransferObject());
            }
            return dtoList;
        }

        public UserEditorSettingsRecentHighlightDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingsRecentHighlightItem item in cache.RecentHighlight)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }
            throw new DtoDoesNotExistException();
        }
        #endregion

        #region Insert
        public void Insert(UserEditorSettingsRecentHighlightDto dto)
        {
            
            this.DalCache.GetCache<UserEditorSettingDalCache>().Insert(dto);
        }
        #endregion

        #region Update
        public void Update(UserEditorSettingsRecentHighlightDto dto)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentHighlightItem item in cache.RecentHighlight)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }
        #endregion

        #region Delete
        public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentHighlightItem item in cache.RecentHighlight)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }
        #endregion
    }
}
