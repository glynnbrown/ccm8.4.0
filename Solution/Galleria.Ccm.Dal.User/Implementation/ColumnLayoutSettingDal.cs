﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IColumnLayoutSettingDal
    /// </summary>
    [Serializable]
    public sealed class ColumnLayoutSettingDal : DalBase, IColumnLayoutSettingDal
    {
        public IEnumerable<ColumnLayoutSettingDto> FetchAll()
        {
            var cache = DalCache.GetCache<UserSystemSettingDalCache>();

            return cache.Item.ColumnLayoutSettings.Select(item => item.GetDataTransferObject()).ToList();
        }

        public void Insert(ColumnLayoutSettingDto dto)
        {
            var cache = DalCache.GetCache<UserSystemSettingDalCache>();

            dto.Id = ColumnLayoutSettingItem.GetNextId();

            cache.Item.ColumnLayoutSettings.Add(new ColumnLayoutSettingItem(dto));
        }

        public void Update(ColumnLayoutSettingDto dto)
        {
            var cache = DalCache.GetCache<UserSystemSettingDalCache>();

            var item = cache.Item.ColumnLayoutSettings.FirstOrDefault(c => c.Id == dto.Id);
            if (item != null)
            {
                item.UpdateFromDto(dto);
            }
            else
            {
                throw new DtoDoesNotExistException();
            }
        }
    }
}
