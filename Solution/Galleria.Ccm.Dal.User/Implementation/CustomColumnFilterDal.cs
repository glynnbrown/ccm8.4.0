﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Modified Update and Delete methods, to use the new identifiers.

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added ids to child objects.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class CustomColumnFilterDal : DalBase, ICustomColumnFilterDal
    {
        #region Helpers

        private CustomColumnLayoutCache Cache
        {
            get { return this.DalCache.GetCache<CustomColumnLayoutCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a list of all <see cref="CustomColumnFilterDto" /> held by a <see cref="CustomColumnLayoutItem" /> in the
        ///     <see cref="CustomColumnLayoutCache" /> with particular <paramref name="customColumnLayoutId" />.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CustomColumnFilterDto> FetchByCustomColumnLayoutId(Object customColumnLayoutId)
        {
            List<CustomColumnFilterDto> dtoList = new List<CustomColumnFilterDto>();

            CustomColumnLayoutItem parent = Cache.FindCustomColumnLayoutById(Convert.ToString(customColumnLayoutId));
            if (parent != null)
            {
                foreach (CustomColumnFilterItem dalObject in parent.ColumnFilters)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(CustomColumnFilterDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(CustomColumnFilterDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete
        public void DeleteById(Int32 id)
        {
            CustomColumnFilterItem dalObject = Cache.FindCustomColumnFilterById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion
    }
}