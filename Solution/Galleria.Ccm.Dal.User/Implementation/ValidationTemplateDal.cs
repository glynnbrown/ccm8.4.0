﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created
// V8-27269 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     Implementation of <see cref="IValidationTemplateDal" /> for the UserDal.
    /// </summary>
    public sealed class ValidationTemplateDal : DalBase, IValidationTemplateDal
    {
        #region Properties

        private ValidationTemplateDalCache Cache
        {
            get { return DalCache.GetCache<ValidationTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a <see cref="ValidationTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="ValidationTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        public ValidationTemplateDto FetchById(Object id)
        {
            var item = Cache.FetchById(Convert.ToString(id));
            if (item == null) throw new DtoDoesNotExistException();

            return item.GetDataTransferObject();
        }

        public ValidationTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            var dtos = Cache.Items.
                Select(i => i.GetDataTransferObject());
            var returnDto = dtos.FirstOrDefault(dto => dto.Name == name);
            if (returnDto == null) throw new DtoDoesNotExistException();
            return returnDto;
        }
        

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="ValidationTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        public void Insert(ValidationTemplateDto dto)
        {
            var dalCache = Cache;
            if (dto.Id == null)
            {
                if (DalContext.IsUnitTesting)
                {
                    dto.Id = Convert.ToString(dalCache.Items.Count() + 1);
                }
                else
                {
                    throw new ArgumentException("Id should be preset to the file path.");
                }
            }
            dalCache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="ValidationTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        public void Update(ValidationTemplateDto dto)
        {
            var item = Cache.FetchById(Convert.ToString(dto.Id));
            if (item == null) throw new ArgumentOutOfRangeException("dto", @"Id not found.");

            item.UpdateFromDto(dto);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        public void DeleteById(Object id)
        {
            var cache = Cache;
            cache.Delete(cache.FetchById(Convert.ToString(id)));
        }

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a validation template for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <returns><c>True</c> if the validation template was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public Boolean LockById(Object id)
        {
            return Cache.LockValidationTemplateById(id as String);
        }

        /// <summary>
        ///     Unlocks a validation template after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        public void UnlockById(Object id)
        {
            Cache.UnlockValidationTemplateById(id as String);
        }

        #endregion
    }
}