﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class ValidationTemplateGroupDal : DalBase, IValidationTemplateGroupDal
    {
        #region Properties

        private ValidationTemplateDalCache Cache
        {
            get { return DalCache.GetCache<ValidationTemplateDalCache>(); }
        }

        #endregion

        #region Insert

        public void Insert(ValidationTemplateGroupDto dto)
        {
            Cache.Insert(dto);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetches the <see cref="ValidationTemplateGroupItem"/> that belong to the given <paramref name="validationTemplateId"/>.
        /// </summary>
        /// <param name="validationTemplateId">Unique identifier of the containing <see cref="ValidationTemplateItem"/></param>.
        /// <returns>An <see cref="IEnumerable{ValidationTemplateGroupDto}"/> with the matching <see cref="ValidationTemplateGroupItem"/>.</returns>
        public IEnumerable<ValidationTemplateGroupDto> FetchByValidationTemplateId(Object validationTemplateId)
        {
            var dtos = new List<ValidationTemplateGroupDto>();
            var item = Cache.FetchById(Convert.ToString(validationTemplateId));
            if (item == null) return dtos;

            dtos.AddRange(item.Groups.Select(groupItem => groupItem.GetDataTransferObject()));
            return dtos;
        }

        #endregion

        #region Update

        public void Update(ValidationTemplateGroupDto dto)
        {
            var item = Cache.FetchGroupById(dto.Id);
            if (item != null) item.UpdateFromDto(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            var item = Cache.FetchGroupById(id);
            Cache.Delete(item);
        }

        #endregion
    }
}
