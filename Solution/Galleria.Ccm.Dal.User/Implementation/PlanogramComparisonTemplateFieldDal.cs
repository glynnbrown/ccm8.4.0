﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     User implementation of <see cref="PlanogramComparisonTemplateFieldDal"/>.
    /// </summary>
    [Serializable]
    public sealed class PlanogramComparisonTemplateFieldDal : DalBase, IPlanogramComparisonTemplateFieldDal
    {
        #region Helpers

        private PlanogramComparisonTemplateDalCache Cache
        {
            get { return DalCache.GetCache<PlanogramComparisonTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PlanogramComparisonTemplateFieldDto FetchById(Int32 id)
        {
            PlanogramComparisonTemplateFieldItem dalObject = Cache.GetPlanogramComparisonTemplateFieldById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        public IEnumerable<PlanogramComparisonTemplateFieldDto> FetchByPlanogramComparisonTemplateId(Object planogramComparisonTemplateId)
        {
            PlanogramComparisonTemplateItem parentItem = Cache.GetPlanogramComparisonTemplateById(Convert.ToString(planogramComparisonTemplateId));
            return parentItem == null
                       ? new List<PlanogramComparisonTemplateFieldDto>()
                       : parentItem.ComparisonFields.Select(dalObject => dalObject.GetDataTransferObject());
        }

        #endregion

        #region Insert

        public void Insert(PlanogramComparisonTemplateFieldDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramComparisonTemplateFieldDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            PlanogramComparisonTemplateFieldItem dalObject = Cache.GetPlanogramComparisonTemplateFieldById((Int32) (id));
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}