﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{  
    [Serializable]
    public sealed class UserEditorSettingsSelectedColumnDal : DalBase, IUserEditorSettingsSelectedColumnDal
    {
       #region Fetch 

        public IEnumerable<UserEditorSettingsSelectedColumnDto> FetchAll()
        {
            List<UserEditorSettingsSelectedColumnDto> dtoList = new List<UserEditorSettingsSelectedColumnDto>();
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();


            foreach (UserEditorSettingsSelectedColumnItem item in cache.SelectedColumn)
            {
                dtoList.Add(item.GetDataTransferObject());
            }
            return dtoList;
        }

        public UserEditorSettingsSelectedColumnDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingsSelectedColumnItem item in cache.SelectedColumn)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }
            throw new DtoDoesNotExistException();
        }
        #endregion

       #region Insert 
    public void Insert(UserEditorSettingsSelectedColumnDto dto)
    {
        (this.DalCache.GetCache<UserEditorSettingDalCache>()).Insert(dto); //(this.DalCache.GetCache<UserEditorSettingDalCache>()).Insert(dto);
    }
    #endregion

       #region Update 
    public void Update(UserEditorSettingsSelectedColumnDto dto)
    {
        UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
        foreach (UserEditorSettingsSelectedColumnItem item in cache.SelectedColumn)
        {
            if (Object.Equals(item.Id, dto.Id))
            {
                item.UpdateFromDto(dto);
                break;
            }
        }
    }
    #endregion

       #region Delete
    public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingsSelectedColumnItem item in cache.SelectedColumn)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }

        #endregion
    }
}
