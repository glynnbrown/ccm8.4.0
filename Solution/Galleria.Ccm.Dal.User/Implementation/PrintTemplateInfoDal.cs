﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public class PrintTemplateInfoDal : DalBase, IPrintTemplateInfoDal
    {
        #region Fetch

        public IEnumerable<PrintTemplateInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            List<PrintTemplateInfoDto> dtoList = new List<PrintTemplateInfoDto>();

            foreach (Object id in ids)
            {
                String fileName = id as String;
                if (File.Exists(fileName))
                {
                    PrintTemplateInfoItem item = PrintTemplateInfoItem.FetchById(fileName);
                    if (item != null)
                    {
                        dtoList.Add(item.GetDataTransferObject());
                    }

                }
            }

            return dtoList;
        }

        public IEnumerable<PrintTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            //Is not currently implemented in user dal as files have no concept of entity - not required.
            throw new NotImplementedException();
        }

        #endregion
    }
}
