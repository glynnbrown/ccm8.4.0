﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IFolderDal
    /// </summary>
    public sealed class FolderDal : DalBase, IFolderDal
    {
        #region Fetch

        public FolderDto FetchById(Object id)
        {
            String folderPath = id as String;

            if (Directory.Exists(folderPath))
            {
                String folderName = folderPath.Split(Path.DirectorySeparatorChar).Last();

                return new FolderDto()
                {
                    Id = folderPath,
                    Name = folderName,
                    FileSystemPath = folderPath,
                    ParentFolderId = folderPath.TrimEnd(folderName.ToCharArray())
                };
            }
            throw new DtoDoesNotExistException();
        }

        public IEnumerable<FolderDto> FetchByParentFolderId(Object parentFolderId)
        {
            List<FolderDto> dtoList = new List<FolderDto>();

            String parentFolderPath = parentFolderId as String;
            if (Directory.Exists(parentFolderPath))
            {
                foreach (String childFolderPath in Directory.GetDirectories(parentFolderPath, "*", SearchOption.TopDirectoryOnly))
                {
                    dtoList.Add(new FolderDto()
                    {
                        Id = childFolderPath,
                        Name = childFolderPath.Split(Path.DirectorySeparatorChar).Last(),
                        FileSystemPath = childFolderPath,
                        ParentFolderId = parentFolderPath
                    });
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(FolderDto dto)
        {
            Directory.CreateDirectory(dto.FileSystemPath);
            dto.Id = dto.FileSystemPath;
        }

        #endregion

        #region Update

        public void Update(FolderDto dto)
        {
            String oldPath = dto.Id as String;

            if (Directory.Exists(oldPath))
            {
                if (oldPath != dto.FileSystemPath)
                {
                    Directory.Move(oldPath, dto.FileSystemPath);

                    dto.Id = dto.FileSystemPath;
                }
            }
            else
            {
                throw new DtoDoesNotExistException();
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            Directory.Delete(id as String, /*delete subs and files*/true);
        }

        #endregion
    }
}
