﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 L.Hodson
//  Created
// V8-28358 : D.Pleasance
//  Added try catch when attempting to fetch fixture package
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.IO;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IFixturePackageInfoDal
    /// </summary>
    public sealed class FixturePackageInfoDal : DalBase, IFixturePackageInfoDal
    {
        #region Constants
        private static readonly Byte[] Aes128Key = new Byte[] { 20, 253, 77, 146, 24, 254, 61, 129, 19, 254, 46, 244, 176, 126, 26, 169, 91, 189, 111, 254, 197, 13, 21, 12, 42, 42, 107, 130, 153, 251, 171, 14 };
        const String TypeIdentifier = "Galleria Fixture File";
        #endregion

        #region Nested Classes

        /// <summary>
        /// The SectionType enum represents the type of a section within a Galleria Fixture File.
        /// </summary>
        internal enum SectionType : ushort
        {
            SchemaVersion = 1,
            FixturePackage = 2,
            FixtureAssemblies = 3,
            FixtureAssemblyComponents = 4,
            FixtureComponents = 5,
            Fixtures = 6,
            FixtureAssemblyItems = 7,
            FixtureComponentItems = 8,
            FixtureItems = 9,
            FixtureImages = 10,
            FixtureSubComponents = 11,
        }

        #region ItemCache

        /// <summary>
        /// The ItemCache class is an abstract class from which classes responsible for managing DTOs corresponding to
        /// a particular FIX file section derive, for the purpose of loading these DTOs from a FIX file on demand and 
        /// holding changes to the FIX file section in memory.
        /// </summary>
        /// <typeparam name="DTO">The type of DTO stored in this cache.</typeparam>
        /// <typeparam name="DTOITEM">The IParentDtoItem or IChildDtoItem type that corresponds to DTO, for the purpose 
        /// of being able to read the item from the file.</typeparam>
        internal abstract class ItemCache<DTO, DTOITEM>
            where DTO : class
        {
            /// <summary>
            /// A delegate to create a DTOITEM from a DTO.
            /// </summary>
            internal delegate DTOITEM CreateDtoItemFromDto(DTO dto);
            /// <summary>
            /// A delegate to create a DTOITEM from an IItem.
            /// </summary>
            internal delegate DTOITEM CreateDtoItemFromItem(GalleriaBinaryFile.IItem item);

            /// <summary>
            /// An object to lock on for the purpose of preventing concurrency errors when multiple threads access the
            /// same DalCache at the same time.
            /// </summary>
            protected Object _lock;
            /// <summary>
            /// The section type in the FIX file used to store items of type DTO.
            /// </summary>
            protected UInt16 _sectionType;
            /// <summary>
            /// The FIX file.
            /// </summary>
            protected GalleriaBinaryFile _file;
            /// <summary>
            /// Whether uncommitted changes have been made to the section.
            /// </summary>
            protected Boolean _hasChanges;
            /// <summary>
            /// The delegate instance to use when creating DTOITEM instances from DTO instances.
            /// </summary>
            protected CreateDtoItemFromDto _createDtoItemFromDto;
            /// <summary>
            /// The delegate instance to use when creating DTOITEM instances from IItem instances.
            /// </summary>
            protected CreateDtoItemFromItem _createDtoItemFromItem;
        }

        #endregion

        #region SingleItemCache

        /// <summary>
        /// The SingleItemCache class is responsible for managing a single DTO, i.e. for an object type that can only
        /// exist once in the FIX file, providing functionality to load it only on demand.
        /// </summary>
        /// <typeparam name="DTO">The type of DTO stored in this cache.</typeparam>
        /// <typeparam name="DTOITEM">The IParentDtoItem type that corresponds to DTO, for the purpose of being able
        /// to read the item from the file.</typeparam>
        internal class SingleItemCache<DTO, DTOITEM> : ItemCache<DTO, DTOITEM>
            where DTO : class
            where DTOITEM : class, IParentDtoItem<DTO>
        {
            #region Protected Fields

            /// <summary>
            /// A DTOITEM representing the DTO currently stored in this cache, if it exists and has been loaded from
            /// the FIX file.
            /// </summary>
            protected DTOITEM _item;

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new empty SingleItemCache.
            /// </summary>
            protected SingleItemCache() { }

             /// <summary>
            /// Creates a new WritableSingleItemCache.
            /// </summary>
            public SingleItemCache(
                Object lockObject,
                GalleriaBinaryFile file,
                SectionType sectionType,
                CreateDtoItemFromDto createDtoItemFromDto,
                CreateDtoItemFromItem createDtoItemFromItem)
            {
                _lock = lockObject;
                _file = file;
                _sectionType = (UInt16)sectionType;
                _createDtoItemFromDto = createDtoItemFromDto;
                _createDtoItemFromItem = createDtoItemFromItem;
            }

            #endregion

            #region Public Methods

            // Here we've got all the usual methods you'd expect to support DAL functionality.  The paradigm is, if the
            // FIX file contains data that has not been loaded yet, load it before proceeding, then act on the DTO
            // within the cache.  Locking is used to prevent simultaneous access to the same data.

            public DTO Fetch()
            {
                DTO returnValue = null;
                lock (_lock)
                {
                    Load();
                    if (_item != null)
                    {
                        returnValue = _item.CopyDto();
                    }
                }
                return returnValue;
            }

            public void InsertOrUpdate(DTO dto)
            {
                lock (_lock)
                {
                    Load();
                    _item = _createDtoItemFromDto(dto);
                    // If only one DTO is allowed, the ID might as well be 1.
                    _item.Id = 1;
                    _hasChanges = true;
                }
            }

            public Boolean Delete()
            {
                Boolean returnValue = false;
                lock (_lock)
                {
                    Load();
                    if (_item != null)
                    {
                        _item = null;
                        _hasChanges = returnValue = true;
                    }
                }
                return returnValue;
            }

            #endregion

            #region Private Helper Methods

            /// <summary>
            /// Load the DTO from the FIX file, if it hasn't already been loaded.
            /// </summary>
            private void Load()
            {
                if ((_item == null) && (!_hasChanges))
                {
                    foreach (GalleriaBinaryFile.IItem item in _file.GetItems(_sectionType))
                    {
                        if (_item != null)
                        {
                            throw new InvalidDataException(String.Format(
                                "A FIX file must not contain more than one {0}.",
                                typeof(DTO).Name.Substring(0, typeof(DTO).Name.Length - 3)));
                        }
                        _item = _createDtoItemFromItem(item);
                    }
                }
            }

            #endregion
        }

        #endregion

        #endregion

        #region Fields
        private Object _lock = new Object();
        #endregion

        #region Fetch

        public IEnumerable<FixturePackageInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            List<FixturePackageInfoDto> dtoList = new List<FixturePackageInfoDto>();

            foreach (Object id in ids)
            {
                String fileName = id as String;
                if (File.Exists(fileName))
                {
                    try
                    {
                        String folderName = Path.GetDirectoryName(fileName);
                        using (GalleriaBinaryFile file = new GalleriaBinaryFile(fileName, TypeIdentifier, true))
                        {
                            var package =
                            new SingleItemCache<FixturePackageInfoDto, FixturePackageInfoItem>(
                            _lock,
                            file,
                            SectionType.FixturePackage,
                            delegate(FixturePackageInfoDto dto)
                            {
                                return new FixturePackageInfoItem(dto);
                            },
                            delegate(GalleriaBinaryFile.IItem item)
                            {
                                return new FixturePackageInfoItem(item, folderName);
                            });

                            FixturePackageInfoDto infoDto = package.Fetch();
                            infoDto.Id = fileName;
                            dtoList.Add(infoDto);
                        }
                    }
                    catch(Exception ex)
                    {
                        //just dont add the file.
                    }
                }
            }


            return dtoList;
        }

        #endregion

        #region Commands

        public void SetFolderId(Object fixturePackageId, Object folderId)
        {
            String filePath = fixturePackageId as String;
            String folderPath = folderId as String;

            String newFilePath = Path.Combine(folderPath, Path.GetFileName(filePath));

            File.Move(filePath, newFilePath);
        }


       #endregion


        
    }
}
