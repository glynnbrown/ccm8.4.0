﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class PrintTemplateSectionDal : DalBase, IPrintTemplateSectionDal
    {
        #region Helpers

        private PrintTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PrintTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PrintTemplateSectionDto FetchById(int id)
        {
            PrintTemplateSectionItem dalObject = Cache.FindPrintTemplateSectionById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();
            else return dalObject.GetDataTransferObject();
        }

        public IEnumerable<PrintTemplateSectionDto> FetchByPrintTemplateSectionGroupId(Int32 printTemplateSectionGroupId)
        {
            List<PrintTemplateSectionDto> dtoList = new List<PrintTemplateSectionDto>();

            PrintTemplateSectionGroupItem parentItem = Cache.FindPrintTemplateSectionGroupById(printTemplateSectionGroupId);
            if (parentItem != null)
            {
                foreach (PrintTemplateSectionItem dalObject in parentItem.Sections)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PrintTemplateSectionDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PrintTemplateSectionDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PrintTemplateSectionItem dalObject = Cache.FindPrintTemplateSectionById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion

       
    }
}
