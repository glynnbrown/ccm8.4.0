﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 J.Pickup
//  Created
// CCM-24265 : N.Haywood
//  Added locking/unlocking
// V8-26248 : L.Luong
//  Renamed cache.Insert to cache.InsertOrUpdate in the Insert Method
// V8-28362 : L.Ineson
//  Reworked.
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class LabelDal : DalBase, ILabelDal
    {
        #region Helpers

        private LabelDalCache Cache
        {
            get { return this.DalCache.GetCache<LabelDalCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a list of all dtos held by the cache with particular id.
        /// </summary>
        /// <returns></returns>
        public LabelDto FetchById(Object id)
        {
            LabelItem dalObject = Cache.FetchFileById(Convert.ToString(id),/*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        public LabelDto FetchByEntityIdName(Int32 entityId, String name)
        {
            //should not be called for this dal.
            throw new NotImplementedException();
        }

        #endregion

        #region Insert

        public void Insert(LabelDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(LabelDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            LabelItem dalObject = Cache.FindLabelById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Locks the label file
        /// </summary>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        /// <summary>
        /// Unlocks the label file
        /// </summary>
        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion
    }
}
