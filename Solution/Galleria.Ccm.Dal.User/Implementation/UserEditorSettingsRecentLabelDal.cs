﻿#region Header Information
#region Version History CCM830
//V8-31699 : A.Heathcote
//  Created this Dal    
#endregion
#endregion
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;
namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class UserEditorSettingsRecentLabelDal : DalBase, IUserEditorSettingsRecentLabelDal
    {
        #region Fetch

        ///  <summary>
        /// Returns a list of all dtos held by the cache.
        /// </summary>
        ///<returns></returns>
        public IEnumerable<UserEditorSettingsRecentLabelDto> FetchAll()
        {
            List<UserEditorSettingsRecentLabelDto> dtoList = new List<UserEditorSettingsRecentLabelDto>();

            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentLabelItem item in cache.RecentLabel)
            {
                dtoList.Add(item.GetDataTransferObject());
            }

            return dtoList;
        }

        ///<summary>
        /// Returns the dto for the given id.
        ///</summary>
        ///<param name="id"></param>
        ///<exception cref="DtoDoesNotExistException"/>
        ///<returns></returns>
        public UserEditorSettingsRecentLabelDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingsRecentLabelItem item in cache.RecentLabel)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }

            throw new DtoDoesNotExistException();
        }
        #endregion

        #region Insert

        public void Insert(UserEditorSettingsRecentLabelDto dto)
        {
            (this.DalCache.GetCache<UserEditorSettingDalCache>()).Insert(dto);
        }

        #endregion

        #region Update

        public void Update(UserEditorSettingsRecentLabelDto dto)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentLabelItem item in cache.RecentLabel)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingsRecentLabelItem item in cache.RecentLabel)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }

        #endregion
    }
}

    
