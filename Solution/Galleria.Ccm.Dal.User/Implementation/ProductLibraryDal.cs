﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (V8.3)
// V8-32361 : L.Ineson
//  Added lock
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class ProductLibraryDal : DalBase, IProductLibraryDal
    {
        #region Helpers

        private ProductLibraryDalCache Cache
        {
            get { return this.DalCache.GetCache<ProductLibraryDalCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// FetchById user implemented
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductLibraryDto FetchById(Object id)
        {
            ProductLibraryItem dalObject = Cache.FetchFileById(Convert.ToString(id),/*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        #endregion

        #region Insert

        public void Insert(ProductLibraryDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(ProductLibraryDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            ProductLibraryItem dalObject = Cache.FindProductLibraryById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Locks the package.
        /// </summary>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion

    }
}
