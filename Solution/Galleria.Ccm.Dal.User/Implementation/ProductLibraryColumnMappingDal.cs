﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (V8.3)
// V8-32361 : L.Ineson
//  Updated
#endregion
#endregion


using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class ProductLibraryColumnMappingDal : DalBase, IProductLibraryColumnMappingDal
    {
        #region Helpers

        private ProductLibraryDalCache Cache
        {
            get { return this.DalCache.GetCache<ProductLibraryDalCache>(); }
        }

        #endregion

        #region Fetch

        public ProductLibraryColumnMappingDto FetchById(Int32 id)
        {
            ProductLibraryColumnMappingItem dalObject = Cache.FindProductLibraryColumnMappingById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();
            else return dalObject.GetDataTransferObject();
        }

        public IEnumerable<ProductLibraryColumnMappingDto> FetchByProductLibraryId(object printTemplateId)
        {
            List<ProductLibraryColumnMappingDto> dtoList = new List<ProductLibraryColumnMappingDto>();

            ProductLibraryItem printTemplate = Cache.FindProductLibraryById(Convert.ToString(printTemplateId));
            if (printTemplate != null)
            {
                foreach (ProductLibraryColumnMappingItem dalObject in printTemplate.ColumnMappings)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(ProductLibraryColumnMappingDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(ProductLibraryColumnMappingDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            ProductLibraryColumnMappingItem dalObject = Cache.FindProductLibraryColumnMappingById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}