﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class ValidationTemplateGroupMetricDal : DalBase, IValidationTemplateGroupMetricDal
    {
        #region Properties

        private ValidationTemplateDalCache Cache
        {
            get { return DalCache.GetCache<ValidationTemplateDalCache>(); }
        }

        #endregion

        #region Insert

        public void Insert(ValidationTemplateGroupMetricDto dto)
        {
            Cache.Insert(dto);
        }

        #endregion

        #region Fetch

        public IEnumerable<ValidationTemplateGroupMetricDto> FetchByValidationTemplateGroupId(
            Int32 validationTemplateGroupId)
        {
            var dtos = new List<ValidationTemplateGroupMetricDto>();
            var item = Cache.FetchGroupById(validationTemplateGroupId);
            if (item == null) return dtos;

            dtos.AddRange(item.Metrics.Select(metricItem => metricItem.GetDataTransferObject()));
            return dtos;
        }

        #endregion

        #region Update

        public void Update(ValidationTemplateGroupMetricDto dto)
        {
            var item = Cache.FetchMetricById(dto.Id);
            if (item != null) item.UpdateFromDto(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            var item = Cache.FetchMetricById(id);
            if (item != null) Cache.Delete(item);
        }

        #endregion
    }
}
