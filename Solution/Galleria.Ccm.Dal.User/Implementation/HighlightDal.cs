﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
// V8-25436 : L.Luong
//  Added Lock for the package and renamed cache.Insert to cache.InsertOrUpdate
// V8-28362 : L.Ineson
//  Reworked.
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class HighlightDal : DalBase, IHighlightDal
    {
        #region Helpers

        private HighlightDalCache Cache
        {
            get { return this.DalCache.GetCache<HighlightDalCache>(); }
        }

        #endregion

        #region Fetch

        public HighlightDto FetchById(Object id)
        {
            HighlightItem dalObject = Cache.FetchFileById(Convert.ToString(id),/*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        public HighlightDto FetchByEntityIdName(Int32 entityId, String name)
        {
            //Not used by this dal.
            throw new NotImplementedException();
        }

        #endregion

        #region Insert

        public void Insert(HighlightDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(HighlightDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            HighlightItem dalObject = Cache.FindHighlightById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands


        /// <summary>
        /// Locks the package.
        /// </summary>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        /// <summary>
        /// Unlocks the package.
        /// </summary>
        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion


        
    }
}
