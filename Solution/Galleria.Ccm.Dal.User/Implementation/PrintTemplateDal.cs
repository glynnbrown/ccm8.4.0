﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class PrintTemplateDal : DalBase, IPrintTemplateDal
    {
        #region Helpers

        private PrintTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PrintTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PrintTemplateDto FetchById(Object id)
        {
            PrintTemplateItem dalObject = Cache.FetchFileById(Convert.ToString(id),/*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }


        #endregion

        #region Insert

        public void Insert(PrintTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PrintTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            PrintTemplateItem dalObject = Cache.FindPrintTemplateById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands


        /// <summary>
        /// Locks the package.
        /// </summary>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        /// <summary>
        /// Unlocks the package.
        /// </summary>
        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion
    }


   
}
