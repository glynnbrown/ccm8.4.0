﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30791 : D.Pleasance
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class UserEditorSettingColorDal : DalBase, IUserEditorSettingColorDal
    {
        #region Fetch

        /// <summary>
        /// Returns a list of all dtos held by the cache.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserEditorSettingColorDto> FetchAll()
        {
            List<UserEditorSettingColorDto> dtoList = new List<UserEditorSettingColorDto>();

            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingColorItem item in cache.Colors)
            {
                dtoList.Add(item.GetDataTransferObject());
            }

            return dtoList;
        }

        /// <summary>
        /// Returns the dto for the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="DtoDoesNotExistException"/>
        /// <returns></returns>
        public UserEditorSettingColorDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingColorItem item in cache.Colors)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }

            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(UserEditorSettingColorDto dto)
        {
            (this.DalCache.GetCache<UserEditorSettingDalCache>()).Insert(dto);
        }

        #endregion

        #region Update

        public void Update(UserEditorSettingColorDto dto)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingColorItem item in cache.Colors)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingColorItem item in cache.Colors)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }

        #endregion
    }
}