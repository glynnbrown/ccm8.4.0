﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.IO;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IProductLibraryInfoDal
    /// </summary>
    public sealed class ProductLibraryInfoDal : DalBase, IProductLibraryInfoDal
    {
        #region Fetch

        /// <summary>
        /// FetchByIds user implementation
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IEnumerable<ProductLibraryInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            List<ProductLibraryInfoDto> dtoList = new List<ProductLibraryInfoDto>();

            foreach (Object id in ids)
            {
                String fileName = id as String;
                if (File.Exists(fileName))
                {
                    ProductLibraryInfoItem item = ProductLibraryInfoItem.FetchById(fileName);
                    if(item != null)
                    {
                        dtoList.Add(item.GetDataTransferObject());
                    }

                }
            }

            return dtoList;
        }

        public IEnumerable<ProductLibraryInfoDto> FetchByEntityId(int entityId)
        {
            throw new NotSupportedException("Not supported by this dal type");
        }

        #endregion


        
    }
}
