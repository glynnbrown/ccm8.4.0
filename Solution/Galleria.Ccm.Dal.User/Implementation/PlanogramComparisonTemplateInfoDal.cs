﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     User implementation of <see cref="PlanogramComparisonTemplateInfoDal"/>.
    /// </summary>
    public sealed class PlanogramComparisonTemplateInfoDal : DalBase, IPlanogramComparisonTemplateInfoDal
    {
        #region Fetch

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="ids" />.
        /// </summary>
        /// <param name="ids"><see cref="Object" /> containing the ids of matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="ids"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            //  Add the dto matching Filename contained in each Id.
            var idsWhereFileExists = ids.Select(id => id as String).Where(File.Exists);
            var foundInfoItems = idsWhereFileExists.Select(PlanogramComparisonTemplateInfoItem.FetchById).Where(item => item != null);
            var dtosFromInfoItems = foundInfoItems.Select(item => item.GetDataTransferObject()).ToList();
            return dtosFromInfoItems;
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityId(Int32 entityId)
        {
            //  NB: Currently not implemented in User Dal as files have no concept of entity - Not required.
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />, including the deleted ones.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        public IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            //Is not currently implemented in user dal as files have no concept of entity - not required.
            throw new NotImplementedException();
        }

        #endregion
    }
}