﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class PrintTemplateSectionGroupDal : DalBase, IPrintTemplateSectionGroupDal
    {
        #region Helpers

        private PrintTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PrintTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PrintTemplateSectionGroupDto FetchById(Int32 id)
        {
            PrintTemplateSectionGroupItem dalObject = Cache.FindPrintTemplateSectionGroupById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();
            else return dalObject.GetDataTransferObject();
        }

        public IEnumerable<PrintTemplateSectionGroupDto> FetchByPrintTemplateId(object printTemplateId)
        {
            List<PrintTemplateSectionGroupDto> dtoList = new List<PrintTemplateSectionGroupDto>();

            PrintTemplateItem printTemplate = Cache.FindPrintTemplateById(Convert.ToString(printTemplateId));
            if (printTemplate != null)
            {
                foreach (PrintTemplateSectionGroupItem dalObject in printTemplate.SectionGroups)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PrintTemplateSectionGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PrintTemplateSectionGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PrintTemplateSectionGroupItem dalObject = Cache.FindPrintTemplateSectionGroupById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}
