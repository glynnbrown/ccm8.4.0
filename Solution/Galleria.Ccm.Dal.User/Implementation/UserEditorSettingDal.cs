﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24863 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class UserEditorSettingDal : DalBase, IUserEditorSettingDal
    {
        #region Fetch

        /// <summary>
        /// Returns a list of all dtos held by the cache.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserEditorSettingDto> FetchAll()
        {
            List<UserEditorSettingDto> dtoList = new List<UserEditorSettingDto>();

            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingItem item in cache.Items)
            {
                dtoList.Add(item.GetDataTransferObject());
            }

            return dtoList;
        }

        /// <summary>
        /// Returns the dto for the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="DtoDoesNotExistException"/>
        /// <returns></returns>
        public UserEditorSettingDto FetchById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();
            foreach (UserEditorSettingItem item in cache.Items)
            {
                if (Object.Equals(item.Id, id))
                {
                    return item.GetDataTransferObject();
                }
            }

            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(UserEditorSettingDto dto)
        {
            (this.DalCache.GetCache<UserEditorSettingDalCache>()).Insert(dto);
        }

        #endregion

        #region Update

        public void Update(UserEditorSettingDto dto)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingItem item in cache.Items)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            UserEditorSettingDalCache cache = this.DalCache.GetCache<UserEditorSettingDalCache>();

            foreach (UserEditorSettingItem item in cache.Items)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }

        #endregion
    }
}
