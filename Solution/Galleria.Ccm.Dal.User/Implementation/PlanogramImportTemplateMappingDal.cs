﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 L.Ineson
//  Created
// V8-28426 : D.Pleasance
//  Amended to obtain cache item directly rather than calling GetPlanogramImportTemplateById
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class PlanogramImportTemplateMappingDal : DalBase, IPlanogramImportTemplateMappingDal
    {
        #region Helpers

        private PlanogramImportTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PlanogramImportTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<PlanogramImportTemplateMappingDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId)
        {
            List<PlanogramImportTemplateMappingDto> dtoList = new List<PlanogramImportTemplateMappingDto>();

            PlanogramImportTemplateItem parent = Cache.FindPlanogramImportTemplateById(Convert.ToString(planogramImportTemplateId));
            if (parent != null)
            {
                foreach (PlanogramImportTemplateMappingItem dalObject in parent.Mappings)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramImportTemplateMappingDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramImportTemplateMappingDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PlanogramImportTemplateMappingItem dalObject = Cache.FindPlanogramImportTemplateMappingItemById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion
    }
}
