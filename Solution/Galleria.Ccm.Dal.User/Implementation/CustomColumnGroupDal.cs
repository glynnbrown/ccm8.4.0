﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Modified Update and Delete methods, to use the new identifiers.

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added ids to child objects.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class CustomColumnGroupDal : DalBase, ICustomColumnGroupDal
    {
        #region Helpers

        private CustomColumnLayoutCache Cache
        {
            get { return this.DalCache.GetCache<CustomColumnLayoutCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Returns a list of all <see cref="CustomColumnGroupDto" /> held by a <see cref="CustomColumnLayoutItem" /> in the
        ///     <see cref="CustomColumnLayoutCache" /> with particular <paramref name="customColumnLayoutId" />.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CustomColumnGroupDto> FetchByCustomColumnLayoutId(Object customColumnLayoutId)
        {
            List<CustomColumnGroupDto> dtoList = new List<CustomColumnGroupDto>();

            CustomColumnLayoutItem parent = Cache.FindCustomColumnLayoutById(Convert.ToString(customColumnLayoutId));
            if (parent != null)
            {
                foreach (CustomColumnGroupItem dalObject in parent.ColumnGroups)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(CustomColumnGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(CustomColumnGroupDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            CustomColumnGroupItem dalObject = Cache.FindCustomColumnGroupById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion
    }
}