﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     User implementation of <see cref="PlanogramComparisonTemplateDal"/>.
    /// </summary>
    [Serializable]
    public sealed class PlanogramComparisonTemplateDal : DalBase, IPlanogramComparisonTemplateDal
    {
        #region Helpers

        private PlanogramComparisonTemplateDalCache Cache
        {
            get { return DalCache.GetCache<PlanogramComparisonTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template</c> model object
        ///     that is associated to the given <paramref name="id" />.
        /// </summary>
        /// <param name="id"><see cref="Object" /> containing the id of the <c>Planogram Comparison Template</c> to fetch.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateDto" />.</returns>
        public PlanogramComparisonTemplateDto FetchById(Object id)
        {
            PlanogramComparisonTemplateItem dalObject = Cache.FetchFileById(Convert.ToString(id), allowReadOnly: true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template</c> model object
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the entity Id from which to fetch.</param>
        /// <param name="name">The name of the template to be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateDto" />.</returns>
        public PlanogramComparisonTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            //  NB: There is no concept of Entity in files. Not required.
            throw new NotImplementedException();
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(PlanogramComparisonTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        public void Update(PlanogramComparisonTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteById(Object id)
        {
            PlanogramComparisonTemplateItem dalObject = Cache.GetPlanogramComparisonTemplateById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Locking

        /// <summary>
        ///     Lock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be locked.</param>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        /// <summary>
        ///     Unlock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be unlocked.</param>
        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion
    }
}