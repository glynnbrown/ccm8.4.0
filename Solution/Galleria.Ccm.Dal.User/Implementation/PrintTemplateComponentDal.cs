﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class PrintTemplateComponentDal : DalBase, IPrintTemplateComponentDal
    {
        #region Helpers

        private PrintTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PrintTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PrintTemplateComponentDto FetchById(int id)
        {
            PrintTemplateComponentItem dalObject = Cache.FindPrintTemplateComponentById(id);
            if (dalObject == null) throw new DtoDoesNotExistException();
            else return dalObject.GetDataTransferObject();
        }

        public IEnumerable<PrintTemplateComponentDto> FetchByPrintTemplateSectionId(Int32 printTemplateSectionId)
        {
            List<PrintTemplateComponentDto> dtoList = new List<PrintTemplateComponentDto>();

            PrintTemplateSectionItem parentItem = Cache.FindPrintTemplateSectionById(printTemplateSectionId);
            if (parentItem != null)
            {
                foreach (PrintTemplateComponentItem dalObject in parentItem.Components)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PrintTemplateComponentDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PrintTemplateComponentDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PrintTemplateComponentItem dalObject = Cache.FindPrintTemplateComponentById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}
