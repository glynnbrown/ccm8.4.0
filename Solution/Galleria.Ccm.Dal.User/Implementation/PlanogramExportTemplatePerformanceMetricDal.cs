﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    class PlanogramExportTemplatePerformanceMetricDal : DalBase, IPlanogramExportTemplatePerformanceMetricDal
    {
        #region Helpers

        private PlanogramExportTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PlanogramExportTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<PlanogramExportTemplatePerformanceMetricDto> FetchByPlanogramExportTemplateId(Object PlanogramExportTemplateId)
        {
            List<PlanogramExportTemplatePerformanceMetricDto> dtoList = new List<PlanogramExportTemplatePerformanceMetricDto>();

            PlanogramExportTemplateItem parent = Cache.FindPlanogramExportTemplateById(Convert.ToString(PlanogramExportTemplateId));
            if (parent != null)
            {
                foreach (PlanogramExportTemplatePerformanceMetricItem dalObject in parent.PerformanceMetrics)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PlanogramExportTemplatePerformanceMetricItem dalObject = Cache.FindPlanogramExportTemplatePerformanceMetricItemById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion
    }
}
