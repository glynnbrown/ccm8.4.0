﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class RecentPlanogramDal : DalBase, IRecentPlanogramDal
    {
        #region Fetch

        /// <summary>
        /// Returns a list of all dtos held by the cache.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RecentPlanogramDto> FetchAll()
        {
            List<RecentPlanogramDto> dtoList = new List<RecentPlanogramDto>();

            RecentPlanogramDalCache cache = this.DalCache.GetCache<RecentPlanogramDalCache>();

            foreach (RecentPlanogramItem item in cache.Items)
            {
                dtoList.Add(item.GetDataTransferObject());
            }

            return dtoList;
        }

        /// <summary>
        /// Returns the dto for the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="DtoDoesNotExistException"/>
        /// <returns></returns>
        public RecentPlanogramDto FetchById(Object id)
        {
            RecentPlanogramDalCache cache = this.DalCache.GetCache<RecentPlanogramDalCache>();
            foreach (RecentPlanogramItem item in cache.Items)
            {
                if (Object.Equals(item.Id, id))
                {
                   return item.GetDataTransferObject();
                }
            }

            throw new DtoDoesNotExistException();
        }

        #endregion

        #region Insert

        public void Insert(RecentPlanogramDto dto)
        {
            (this.DalCache.GetCache<RecentPlanogramDalCache>()).Insert(dto);
        }

        #endregion

        #region Update

        public void Update(RecentPlanogramDto dto)
        {
            RecentPlanogramDalCache cache = this.DalCache.GetCache<RecentPlanogramDalCache>();

            foreach (RecentPlanogramItem item in cache.Items)
            {
                if (Object.Equals(item.Id, dto.Id))
                {
                    item.UpdateFromDto(dto);
                    break;
                }
            }
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            RecentPlanogramDalCache cache = this.DalCache.GetCache<RecentPlanogramDalCache>();

            foreach (RecentPlanogramItem item in cache.Items)
            {
                if (Object.Equals(item.Id, id))
                {
                    cache.Delete(item);
                    break;
                }
            }
        }

        #endregion
    }
}
