﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class PlanogramExportTemplateDal : DalBase, IPlanogramExportTemplateDal
    {
        #region Helpers

        private PlanogramExportTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PlanogramExportTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PlanogramExportTemplateDto FetchById(Object id)
        {
            PlanogramExportTemplateItem dalObject = Cache.FetchFileById(Convert.ToString(id), /*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            PlanogramExportTemplateDto dto = dalObject.GetDataTransferObject();
            return dto;
        }

        public PlanogramExportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            var dtos = Cache.Items.Select(i => i.GetDataTransferObject());
            var returnDto = dtos.FirstOrDefault(dto => dto.Name == name);
            if (returnDto == null) throw new DtoDoesNotExistException();
            return returnDto;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramExportTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramExportTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            PlanogramExportTemplateItem dalObject = Cache.FindPlanogramExportTemplateById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands

        public void LockById(Object id)
        {
            Cache.LockFileById(id as String);
        }

        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(id as String);
        }

        #endregion
    }
}
