﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.IO;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public class LabelInfoDal : DalBase, ILabelInfoDal
    {
        #region Fetch

        public IEnumerable<LabelInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            List<LabelInfoDto> dtoList = new List<LabelInfoDto>();

            foreach (Object id in ids)
            {
                String fileName = id as String;
                if (File.Exists(fileName))
                {
                    LabelInfoItem item = LabelInfoItem.FetchById(fileName);
                    if (item != null)
                    {
                        dtoList.Add(item.GetDataTransferObject());
                    }

                }
            }

            return dtoList;
        }

        public IEnumerable<LabelInfoDto> FetchByEntityId(Int32 entityId)
        {
            throw new DtoDoesNotExistException();
        }

        public IEnumerable<LabelInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            throw new DtoDoesNotExistException();
        }

        #endregion
    }
}
