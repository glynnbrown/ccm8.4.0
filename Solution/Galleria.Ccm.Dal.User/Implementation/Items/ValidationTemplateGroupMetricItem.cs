﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#region Version History: (CCM v8.03)

// V8-29596 : L.Luong 
//  Added Criteria

#endregion

#region Version History: (CCM 810)
//  V8-29739 : M.Brumby
//      Changed ValidationTemplateGroupId to internal so it can be initialised
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     A Validation Template Group Metric dal item.
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateGroupMetricItem : ISerializable
    {
        #region Fields

        #endregion

        #region Non Serializable Properties

        [XmlIgnore]
        internal Int32 Id { get; private set; }

        /// <summary>
        ///     Gets or sets whether this instance is dirty, or not.
        /// </summary>
        [XmlIgnore]
        private bool IsDirty { get; set; }

        [XmlIgnore]
        internal int ValidationTemplateGroupId { get; set; }

        #endregion

        #region Properties

        public PlanogramValidationAggregationType AggregationType { get; set; }
        public string Field { get; set; }

        public Single Score1 { get; set; }

        public Single Score2 { get; set; }

        public Single Score3 { get; set; }
        public float Threshold1 { get; set; }

        public float Threshold2 { get; set; }

        public PlanogramValidationType ValidationType { get; set; }

        public string Criteria { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Private initial constructor. Performs any common initialization.
        /// </summary>
        private ValidationTemplateGroupMetricItem()
        {
            // Not needed in this instance.
        }

        /// <summary>
        ///     Initializes the new instance by loading the information from the provided <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">
        ///     Instance of <see cref="ValidationTemplateGroupMetricDto" /> with the data used to initialize the new
        ///     instance.
        /// </param>
        public ValidationTemplateGroupMetricItem(ValidationTemplateGroupMetricDto dto)
            : this()
        {
            UpdateFromDto(dto);
        }

        private ValidationTemplateGroupMetricItem(SerializationInfo info, StreamingContext context)
            : this()
        {
            SetObjectData(info);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this instance as modified.
        /// </summary>
        private void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        ///     Populates this instance with the values from the <see cref="SerializationInfo" />.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo" /> containing the data to populate this instance with.</param>
        private void SetObjectData(SerializationInfo info)
        {
            Field = info.GetString(FieldNames.ValidationTemplateMetricField);
            AggregationType =
                (PlanogramValidationAggregationType) info.GetByte(FieldNames.ValidationTemplateMetricAggregationType);
            Threshold1 = info.GetSingle(FieldNames.ValidationTemplateMetricThreshold1);
            Threshold2 = info.GetSingle(FieldNames.ValidationTemplateMetricThreshold2);
            Score1 = info.GetSingle(FieldNames.ValidationTemplateMetricScore1);
            Score2 = info.GetSingle(FieldNames.ValidationTemplateMetricScore2);
            Score3 = info.GetSingle(FieldNames.ValidationTemplateMetricScore3);
            ValidationType = (PlanogramValidationType) info.GetByte(FieldNames.ValidationTemplateMetricValidationType);
            Criteria = info.GetString(FieldNames.ValidationTemplateMetricCriteria);            
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Creates and returns the <see cref="ValidationTemplateGroupMetricDto" /> for this instance.
        /// </summary>
        /// <returns>A new instance of <see cref="ValidationTemplateGroupMetricDto" /> loaded with this instance's properties.</returns>
        public ValidationTemplateGroupMetricDto GetDataTransferObject()
        {
            return new ValidationTemplateGroupMetricDto
            {
                Id = Id,
                ValidationTemplateGroupId = ValidationTemplateGroupId,
                Field = Field,
                AggregationType = (Byte) AggregationType,
                Threshold1 = Threshold1,
                Threshold2 = Threshold2,
                Score1 = Score1,
                Score2 = Score2,
                Score3 = Score3,
                ValidationType = (Byte) ValidationType,
                Criteria = Criteria
            };
        }

        /// <summary>
        ///     Loads the data from the provided <paramref name="dto" /> in this instance.
        /// </summary>
        /// <param name="dto">
        ///     Instance of <see cref="ValidationTemplateGroupMetricDto" /> with the data to be loaded in this
        ///     instance.
        /// </param>
        internal void UpdateFromDto(ValidationTemplateGroupMetricDto dto)
        {
            MarkDirty();

            Id = dto.Id;
            ValidationTemplateGroupId = dto.ValidationTemplateGroupId;
            Field = dto.Field;
            AggregationType = (PlanogramValidationAggregationType) dto.AggregationType;
            Threshold1 = dto.Threshold1;
            Threshold2 = dto.Threshold2;
            Score1 = dto.Score1;
            Score2 = dto.Score2;
            Score3 = dto.Score3;
            ValidationType = (PlanogramValidationType) dto.ValidationType;
            Criteria = dto.Criteria;
        }

        #endregion

        #region ISerializable Members

        /// <summary>
        ///     Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the
        ///     target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
        /// <param name="context">
        ///     The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this
        ///     serialization.
        /// </param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.ValidationTemplateMetricField, Field);
            info.AddValue(FieldNames.ValidationTemplateMetricAggregationType, (Byte) AggregationType);
            info.AddValue(FieldNames.ValidationTemplateMetricThreshold1, Threshold1);
            info.AddValue(FieldNames.ValidationTemplateMetricThreshold2, Threshold2);
            info.AddValue(FieldNames.ValidationTemplateMetricScore1, Score1);
            info.AddValue(FieldNames.ValidationTemplateMetricScore2, Score2);
            info.AddValue(FieldNames.ValidationTemplateMetricScore3, Score3);
            info.AddValue(FieldNames.ValidationTemplateMetricValidationType, (Byte) ValidationType);
            info.AddValue(FieldNames.ValidationTemplateMetricCriteria, Criteria);
        }

        #endregion
    }
}