﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramImportTemplatePerformanceMetric dal item.
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplatePerformanceMetricItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public PlanogramImportTemplatePerformanceMetricDtoKey DtoKey
        {
            get
            {
                return new PlanogramImportTemplatePerformanceMetricDtoKey()
                    {
                        Name = this.Name,
                        PlanogramImportTemplateId = this.PlanogramImportTemplateId
                    };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String PlanogramImportTemplateId { get; set; }
        
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public String ExternalField { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramImportTemplatePerformanceMetricItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramImportTemplatePerformanceMetricItem(PlanogramImportTemplatePerformanceMetricDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///  Creates a new instance of this type from the given
        ///  serialization context.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramImportTemplatePerformanceMetricItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricName, this.Name);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricDescription, this.Description);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricDirection, this.Direction);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricSpecialType, this.SpecialType);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricMetricType, this.MetricType);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricMetricId, this.MetricId);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetricExternalField, this.ExternalField);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.PlanogramImportTemplatePerformanceMetricName);
            Description = info.GetString(FieldNames.PlanogramImportTemplatePerformanceMetricDescription);
            Direction = info.GetByte(FieldNames.PlanogramImportTemplatePerformanceMetricDirection);
            SpecialType = info.GetByte(FieldNames.PlanogramImportTemplatePerformanceMetricSpecialType);
            MetricType = info.GetByte(FieldNames.PlanogramImportTemplatePerformanceMetricMetricType);
            MetricId = info.GetByte(FieldNames.PlanogramImportTemplatePerformanceMetricMetricId);
            ExternalField = info.GetString(FieldNames.PlanogramImportTemplatePerformanceMetricExternalField);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramImportTemplatePerformanceMetricDto GetDataTransferObject()
        {
            return new PlanogramImportTemplatePerformanceMetricDto()
            {
                Id = this.Id,
                PlanogramImportTemplateId = this.PlanogramImportTemplateId,
                Name = this.Name,
                Description = this.Description,
                Direction = this.Direction,
                SpecialType = this.SpecialType,
                MetricType = this.MetricType,
                MetricId = this.MetricId,
                ExternalField = this.ExternalField
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PlanogramImportTemplateId = Convert.ToString(dto.PlanogramImportTemplateId);
            this.Name = dto.Name;
            this.Description = dto.Description;
            this.Direction = dto.Direction;
            this.SpecialType = dto.SpecialType;
            this.MetricType = dto.MetricType;
            this.MetricId = dto.MetricId;
            this.ExternalField = dto.ExternalField;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}