﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added PerformanceMetrics
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion
#region Version History: (CCM 8.30)
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32743 : M.Pettit
//  Old file was not always correctly deleted, leading to keys being added to the existing file
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramImportTemplate dal item.
    /// </summary>
    [Serializable]
    public sealed class PlanogramImportTemplateItem : ISerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<PlanogramImportTemplateMappingItem> _mappings = new List<PlanogramImportTemplateMappingItem>();
        private List<PlanogramImportTemplatePerformanceMetricItem> _performanceMetrics = new List<PlanogramImportTemplatePerformanceMetricItem>();

        private const Byte majorVersion = 1;
        private const Byte minorVersion = 1;
        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get { return _isNew; }
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (Mappings.Any(f => f.IsDirty)) return true;
                if (PerformanceMetrics.Any(f => f.IsDirty)) return true;
                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the PlanogramImportTemplate Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        public String Name { get; set; }
        public Byte FileType { get; set; }
        public String FileVersion { get; set; }
        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }
        public Boolean AllowAdvancedBaySplitting { get; set; }
        public Boolean CanSplitComponents { get; set; }
        public Boolean SplitBaysByComponentStart { get; set; }
        public Boolean SplitBaysByComponentEnd { get; set; }
        public Boolean SplitByFixedSize { get; set; }
        public Boolean SplitByRecurringPattern { get; set; }
        public Boolean SplitByBayCount { get; set; }
        public Double SplitFixedSize { get; set; }
        public String SplitRecurringPattern { get; set; }
        public Int32 SplitBayCount { get; set; }

        /// <summary>
        /// Returns the list of child mappings
        /// </summary>
        public List<PlanogramImportTemplateMappingItem> Mappings
        {
            get { return _mappings; }
            set { _mappings = value; }
        }

        /// <summary>
        /// Returns the list of child mappings
        /// </summary>
        public List<PlanogramImportTemplatePerformanceMetricItem> PerformanceMetrics
        {
            get { return _performanceMetrics; }
            set { _performanceMetrics = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramImportTemplateItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramImportTemplateItem(PlanogramImportTemplateDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramImportTemplateItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the item from the given file id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PlanogramImportTemplateItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            PlanogramImportTemplateItem dalObject = null;
            XmlSerializer x = new XmlSerializer(typeof(PlanogramImportTemplateItem));

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = (PlanogramImportTemplateItem)x.Deserialize(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = (PlanogramImportTemplateItem)x.Deserialize(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Get the filepath
            String filePath = null;
            if (String.IsNullOrEmpty(fixedDirectory))
            {
                filePath = Path.Combine(Path.GetDirectoryName(this.Id), Name);
            }
            else
            {
                filePath = Path.Combine(fixedDirectory, Name);
            }

            filePath = Path.ChangeExtension(filePath, Constants.PlanogramImportTemplateFileExtension);
            this.Id = filePath;
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;
            
            //delete the old file first
            DeleteOldFile(this.Id);

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //create the new file stream and serialize.
            _stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream != null)
            {
                String lastPath = _stream.Name;
                Unlock();
                File.Delete(lastPath);
            }
        }

        /// <summary>
        /// Deletes the old file being overwritten
        /// </summary>
        /// <param name="path"></param>
        private void DeleteOldFile(String path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this highlight.
        /// </summary>
        public void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (PlanogramImportTemplateMappingItem m in this.Mappings)
            {
                m.PlanogramImportTemplateId = this.Id;
            }
            foreach (PlanogramImportTemplatePerformanceMetricItem m in this.PerformanceMetrics)
            {
                m.PlanogramImportTemplateId = this.Id;
            }
        }

        /// <summary>
        /// Inserts a new child from the given dto.
        /// </summary>
        public PlanogramImportTemplateMappingItem Insert(PlanogramImportTemplateMappingDto dto)
        {
            dto.Id = PlanogramImportTemplateMappingItem.GetNextId();
            PlanogramImportTemplateMappingItem item = new PlanogramImportTemplateMappingItem(dto);
            this.Mappings.Add(item);
            MarkDirty();
            return item;
        }

        /// <summary>
        /// Inserts a new child from the given dto.
        /// </summary>
        public PlanogramImportTemplatePerformanceMetricItem Insert(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            dto.Id = PlanogramImportTemplatePerformanceMetricItem.GetNextId();
            PlanogramImportTemplatePerformanceMetricItem item = new PlanogramImportTemplatePerformanceMetricItem(dto);
            this.PerformanceMetrics.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramImportTemplateDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                String curPath = _stream.Name;
                String newFilename = dto.Name;

                String filePath = Path.Combine(Path.GetDirectoryName(curPath), newFilename);
                filePath = Path.ChangeExtension(filePath, Constants.PlanogramImportTemplateFileExtension);

                Id = filePath;
                dto.Id = filePath;
            }
            else
            {
                //just load the id from the dto.
                Id = Path.ChangeExtension(Convert.ToString(dto.Id), Constants.PlanogramImportTemplateFileExtension);
                dto.Id = Id; //make sure the dto id is the same.
            }

            Name = dto.Name;
            FileType = dto.FileType;
            FileVersion = dto.FileVersion;
            AllowAdvancedBaySplitting = dto.AllowAdvancedBaySplitting;
            CanSplitComponents = dto.CanSplitComponents;
            SplitBaysByComponentStart = dto.SplitBaysByComponentStart;
            SplitBaysByComponentEnd = dto.SplitBaysByComponentEnd;
            SplitByFixedSize = dto.SplitByFixedSize;
            SplitByRecurringPattern = dto.SplitByRecurringPattern;
            SplitByBayCount = dto.SplitByBayCount;
            SplitFixedSize = dto.SplitFixedSize;
            SplitRecurringPattern = dto.SplitRecurringPattern;
            SplitBayCount = dto.SplitBayCount;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramImportTemplateDto GetDataTransferObject()
        {
            return new PlanogramImportTemplateDto()
            {
                Id = this.Id,
                Name = this.Name,
                FileType = this.FileType,
                FileVersion = this.FileVersion,
                AllowAdvancedBaySplitting = this.AllowAdvancedBaySplitting,
                CanSplitComponents = this.CanSplitComponents,
                SplitBaysByComponentStart = this.SplitBaysByComponentStart,
                SplitBaysByComponentEnd = this.SplitBaysByComponentEnd,
                SplitByFixedSize = this.SplitByFixedSize,
                SplitByRecurringPattern = this.SplitByRecurringPattern,
                SplitByBayCount = this.SplitByBayCount,
                SplitFixedSize = this.SplitFixedSize,
                SplitRecurringPattern = this.SplitRecurringPattern,
                SplitBayCount = this.SplitBayCount
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramImportTemplateName, this.Name);
            info.AddValue(FieldNames.PlanogramImportTemplateFileType, this.FileType);
            info.AddValue(FieldNames.PlanogramImportTemplateFileVersion, this.FileVersion);
            info.AddValue(FieldNames.PlanogramImportTemplateMajorVersion, this.MajorVersion);
            info.AddValue(FieldNames.PlanogramImportTemplateMinorVersion, this.MinorVersion);

            info.AddValue(FieldNames.PlanogramImportTemplateAllowAdvancedBaySplitting, this.AllowAdvancedBaySplitting);
            info.AddValue(FieldNames.PlanogramImportTemplateCanSplitComponents, this.CanSplitComponents);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitBaysByComponentStart, this.SplitBaysByComponentStart);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitBaysByComponentEnd, this.SplitBaysByComponentEnd);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitByFixedSize, this.SplitByFixedSize);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitByRecurringPattern, this.SplitByRecurringPattern);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitByBayCount, this.SplitByBayCount);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitFixedSize, this.SplitFixedSize);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitRecurringPattern, this.SplitRecurringPattern);
            info.AddValue(FieldNames.PlanogramImportTemplateSplitBayCount, this.SplitBayCount);


            info.AddValue(FieldNames.PlanogramImportTemplateMappings, this.Mappings);
            info.AddValue(FieldNames.PlanogramImportTemplatePerformanceMetrics, this.PerformanceMetrics);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.PlanogramImportTemplateName);
            FileType = info.GetByte(FieldNames.PlanogramImportTemplateFileType);
            FileVersion = info.GetString(FieldNames.PlanogramImportTemplateFileVersion);
            MajorVersion = info.GetByte(FieldNames.PlanogramImportTemplateMajorVersion);
            MinorVersion = info.GetByte(FieldNames.PlanogramImportTemplateMinorVersion);

            AllowAdvancedBaySplitting = info.GetBoolean(FieldNames.PlanogramImportTemplateAllowAdvancedBaySplitting);
            CanSplitComponents = info.GetBoolean(FieldNames.PlanogramImportTemplateCanSplitComponents);
            SplitBaysByComponentStart = info.GetBoolean(FieldNames.PlanogramImportTemplateSplitBaysByComponentStart);
            SplitBaysByComponentEnd = info.GetBoolean(FieldNames.PlanogramImportTemplateSplitBaysByComponentEnd);
            SplitByFixedSize = info.GetBoolean(FieldNames.PlanogramImportTemplateSplitByFixedSize);
            SplitByRecurringPattern = info.GetBoolean(FieldNames.PlanogramImportTemplateSplitByRecurringPattern);
            SplitByBayCount = info.GetBoolean(FieldNames.PlanogramImportTemplateSplitByBayCount);
            SplitFixedSize = info.GetDouble(FieldNames.PlanogramImportTemplateSplitFixedSize);
            SplitRecurringPattern = info.GetString(FieldNames.PlanogramImportTemplateSplitRecurringPattern);
            SplitBayCount = info.GetInt32(FieldNames.PlanogramImportTemplateSplitBayCount);

            Mappings = (List<PlanogramImportTemplateMappingItem>)info.GetValue(FieldNames.PlanogramImportTemplateMappings, typeof(List<PlanogramImportTemplateMappingItem>));
            PerformanceMetrics = (List<PlanogramImportTemplatePerformanceMetricItem>)info.GetValue(FieldNames.PlanogramImportTemplatePerformanceMetrics, typeof(List<PlanogramImportTemplatePerformanceMetricItem>));

            UpdateChildIdLinks();
        }

        #endregion
    }
}
