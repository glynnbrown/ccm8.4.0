#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property and tidied up.

#endregion
#region Version History: (CCM 820)
//V8-30870 : L.Ineson 
//  Added IDalItem implementation.
#endregion
#region Version History: (CCM 8.3)
//V8-31541 : L.Ineson
//  Added Id
#endregion
#endregion

using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Custom column group dal item.
    /// </summary>
    [Serializable]
    public sealed class CustomColumnGroupItem : IDalItem
    {
        #region Properties

        [XmlIgnore]
        public Boolean IsDirty { get; set; }

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get;
            set;
        }

        [XmlIgnoreAttribute]
        public CustomColumnGroupDtoKey DtoKey
        {
            get
            {
                return new CustomColumnGroupDtoKey()
                {
                   Grouping = this.Grouping
                };
            }
        }

        [XmlIgnore]
        public String CustomColumnLayoutId { get; set; }

        public String Grouping { get; set; }

       

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnGroupItem" />.
        /// </summary>
        public CustomColumnGroupItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnGroupItem" /> from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public CustomColumnGroupItem(CustomColumnGroupDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this <see cref="CustomColumnGroupItem" /> as dirty.
        /// </summary>
        private void MarkDirty()
        {
            IsDirty = true;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Returns a <see cref="CustomColumnGroupDto" /> for this <see cref="CustomColumnGroupItem" />.
        /// </summary>
        /// <returns>A new instance of a <see cref="CustomColumnGroupDto" />.</returns>
        public CustomColumnGroupDto GetDataTransferObject()
        {
            return new CustomColumnGroupDto
            {
                Id = this.Id,
                CustomColumnLayoutId = CustomColumnLayoutId,
                Grouping = Grouping
            };
        }

        /// <summary>
        ///     Updates this <see cref="CustomColumnGroupItem" /> from the given <see cref="CustomColumnGroupDto" />.
        /// </summary>
        /// <param name="dto">
        ///     The <see cref="CustomColumnGroupDto" /> used to initialize the
        ///     <see cref="CustomColumnGroupItem" />.
        /// </param>
        public void UpdateFromDto(CustomColumnGroupDto dto)
        {
            this.Id = dto.Id;
            CustomColumnLayoutId = Convert.ToString(dto.CustomColumnLayoutId);
            Grouping = dto.Grouping;

            MarkDirty();
        }

        #endregion

        #region IDalItem Support

        /// <summary>
        ///Populates the given element
        /// </summary>
        public void GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.CustomColumnGroupGrouping, Grouping);
        }

        /// <summary>
        /// Populates this item from the given xml element.
        /// </summary>
        public void SetObjectData(XElement element)
        {
            Grouping = element.GetString(FieldNames.CustomColumnGroupGrouping);
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}