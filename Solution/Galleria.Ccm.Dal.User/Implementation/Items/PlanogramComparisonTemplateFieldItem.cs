﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    [Serializable]
    public sealed class PlanogramComparisonTemplateFieldItem : IDalItem
    {
        #region Properties

        [XmlIgnore]
        public Boolean IsDirty { get; private set; }

        [XmlIgnore]
        public Int32 Id { get; set; }

        [XmlIgnore]
        public String PlanogramComparisonTemplateId { get; set; }

        public Byte ItemType { get; set; }
        public String FieldPlaceholder { get; set; }
        public String DisplayName { get; set; }
        public Int16 Number { get; set; }
        public Boolean Display { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramComparisonTemplateFieldItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramComparisonTemplateFieldItem(PlanogramComparisonTemplateFieldDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.PlanogramComparisonTemplateFieldItemType, ItemType);
            element.AddValue(FieldNames.PlanogramComparisonTemplateFieldFieldPlaceholder, FieldPlaceholder);
            element.AddValue(FieldNames.PlanogramComparisonTemplateFieldDisplayName, DisplayName);
            element.AddValue(FieldNames.PlanogramComparisonTemplateFieldNumber, Number);
            element.AddValue(FieldNames.PlanogramComparisonTemplateFieldDisplay, Display);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            ItemType = element.GetByte(FieldNames.PlanogramComparisonTemplateFieldItemType);
            FieldPlaceholder = element.GetString(FieldNames.PlanogramComparisonTemplateFieldFieldPlaceholder);
            DisplayName = element.GetString(FieldNames.PlanogramComparisonTemplateFieldDisplayName);
            Number = (Int16)element.GetInt32(FieldNames.PlanogramComparisonTemplateFieldNumber);
            Display = element.GetBoolean(FieldNames.PlanogramComparisonTemplateFieldDisplay);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramComparisonTemplateFieldDto GetDataTransferObject()
        {
            return new PlanogramComparisonTemplateFieldDto
                   {
                       Id = Id,
                       PlanogramComparisonTemplateId = PlanogramComparisonTemplateId,
                       ItemType = ItemType,
                       FieldPlaceholder = FieldPlaceholder,
                       DisplayName = DisplayName,
                       Number = Number,
                       Display = Display
                   };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramComparisonTemplateFieldDto dto)
        {
            Id = (Int32)dto.Id;
            PlanogramComparisonTemplateId = Convert.ToString(dto.PlanogramComparisonTemplateId);
            ItemType = dto.ItemType;
            FieldPlaceholder = dto.FieldPlaceholder;
            DisplayName = dto.DisplayName;
            Number = dto.Number;
            Display = dto.Display;

            MarkDirty();
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}