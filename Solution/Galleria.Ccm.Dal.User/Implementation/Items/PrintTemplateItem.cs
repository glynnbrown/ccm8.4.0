﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32340 : L.Ineson
//  Now throws an exception if trying to save when readonly.
// CCM-14014 : M.Pettit
//  Now closes stream to avoid locking the file when fetched readonly
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplate dal item.
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateItem : IDalItem, IXmlSerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<PrintTemplateSectionGroupItem> _sectionGroups = new List<PrintTemplateSectionGroupItem>();

        #endregion

        #region Properties

        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get { return _isNew; }
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (SectionGroups.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the PrintTemplate Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        public String Name { get; set; }
        public String Description { get; set; }
        public Byte PaperSize { get; set; }
        public Boolean IsPlanMirrored { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        

        /// <summary>
        /// Returns the list of child section groups
        /// </summary>
        public List<PrintTemplateSectionGroupItem> SectionGroups
        {
            get { return _sectionGroups; }
            set { _sectionGroups = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PrintTemplateItem() {}

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public PrintTemplateItem(PrintTemplateDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);

            //set version numbers to the latest available.
            Byte major, minor;
            DalItemHelper.GetLatestVersionNumber(GetScriptsType(), out major, out minor);
            this.MajorVersion = major;
            this.MinorVersion = minor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the highlight item from the given file id.
        /// </summary>
        internal static PrintTemplateItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            PrintTemplateItem dalObject = null;

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = DalItemHelper.Deserialize<PrintTemplateItem>(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;

                    stream.Close();
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = DalItemHelper.Deserialize<PrintTemplateItem>(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Check that we are not in readonly mode.
            if (this.IsReadOnly)
            {
                throw new InvalidOperationException("File is readonly");
            }

            //Get the filepath
            this.Id = Path.ChangeExtension(
                Path.Combine(Path.GetDirectoryName(this.Id), this.Name), 
                Constants.PrintTemplateFileExtension);

            //delete the old file first
            Delete();

            Directory.CreateDirectory(Path.GetDirectoryName(this.Id));

            //create the new file stream and serialize.
            _stream = File.Open(this.Id, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream != null)
            {
                String lastPath = _stream.Name;
                Unlock();
                File.Delete(lastPath);
            }
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this highlight.
        /// </summary>
        public void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        #region Child related methods

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (PrintTemplateSectionGroupItem grp in SectionGroups)
            {
                grp.PrintTemplateId = this.Id;
            }
        }

        public PrintTemplateSectionGroupItem Insert(PrintTemplateSectionGroupDto dto)
        {
            dto.Id = PrintTemplateSectionGroupItem.GetNextId();
            PrintTemplateSectionGroupItem item = new PrintTemplateSectionGroupItem(dto);
            this.SectionGroups.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        public void UpdateFromDto(PrintTemplateDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                dto.Id = Path.ChangeExtension(
                Path.Combine(Path.GetDirectoryName(_stream.Name), dto.Name),
                Constants.PrintTemplateFileExtension);
            }


            Id = (String)dto.Id;
            Name = dto.Name;
            Description = dto.Description;
            PaperSize = dto.PaperSize;
            IsPlanMirrored = dto.IsPlanMirrored;
            DateCreated = dto.DateCreated;
            DateLastModified = dto.DateLastModified;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        public PrintTemplateDto GetDataTransferObject()
        {
            return new PrintTemplateDto()
            {
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                PaperSize = this.PaperSize,
                IsPlanMirrored = this.IsPlanMirrored,
                DateCreated = this.DateCreated,
                DateLastModified = this.DateLastModified,
            };
        }

        #endregion

        #endregion

        #region IDalItem

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.MajorVersion, this.MajorVersion);
            element.AddValue(FieldNames.MinorVersion, this.MinorVersion);

            element.AddValue(FieldNames.PrintTemplateName, this.Name);
            element.AddValue(FieldNames.PrintTemplateDescription, this.Description);
            element.AddValue(FieldNames.PrintTemplatePaperSize, this.PaperSize);
            element.AddValue(FieldNames.PrintTemplateIsPlanMirrored, this.IsPlanMirrored);
            element.AddValue(FieldNames.PrintTemplateDateCreated, this.DateCreated);
            element.AddValue(FieldNames.PrintTemplateDateLastModified, this.DateLastModified);

            element.AddValue(FieldNames.PrintTemplateSectionGroups, this.SectionGroups);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.MajorVersion = element.GetByte(FieldNames.MajorVersion);
            this.MinorVersion = element.GetByte(FieldNames.MinorVersion);

            this.Name = element.GetString(FieldNames.PrintTemplateName);
            this.Description = element.GetString(FieldNames.PrintTemplateDescription);
            this.PaperSize = element.GetByte(FieldNames.PrintTemplatePaperSize);
            this.IsPlanMirrored = element.GetBoolean(FieldNames.PrintTemplateIsPlanMirrored);
            this.DateCreated = element.GetDateTime(FieldNames.PrintTemplateDateCreated);
            this.DateLastModified = element.GetDateTime(FieldNames.PrintTemplateDateLastModified);

            this.SectionGroups = element.GetChildList<PrintTemplateSectionGroupItem>(FieldNames.PrintTemplateSectionGroups);


            UpdateChildIdLinks();
        }

        #endregion

        #region IXmlSerializable Members

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
        {
            //read the xelement
            var x = XElement.ReadFrom(reader) as XElement;

            //Perform the upgrade
            DalItemHelper.Upgrade(GetScriptsType(), x);

            //Load
           ((IDalItem)this).SetObjectData(x);
        }

        void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
        {
            XElement rootElement = new XElement(this.GetType().Name);
            ((IDalItem)this).GetObjectData(rootElement);

            //don't write from root element otherwise we end up with double.
            foreach (XElement element in rootElement.Elements())
            {
                element.WriteTo(writer);
            }
        }

        private Type GetScriptsType()
        {
            return typeof(Scripts.PrintTemplateScripts);
        }

        #endregion
    }
}
