﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32340 : L.Ineson
//  Corrected IsDirty flag
#endregion
#endregion

using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplateSectionGroup dal item
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionGroupItem : IDalItem
    {
        #region Fields
        private List<PrintTemplateSectionItem> _sections = new List<PrintTemplateSectionItem>();
        private Boolean _isDirty;
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (Sections.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }


        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String PrintTemplateId { get; set; }

        public Byte Number { get; set; }
        public String Name { get; set; }
        public Byte BaysPerPage { get; set; }

        public List<PrintTemplateSectionItem> Sections
        {
            get { return _sections; }
            set { _sections = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PrintTemplateSectionGroupItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PrintTemplateSectionGroupItem(PrintTemplateSectionGroupDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.PrintTemplateSectionGroupNumber, this.Number);
            element.AddValue(FieldNames.PrintTemplateSectionGroupName, this.Name);
            element.AddValue(FieldNames.PrintTemplateSectionGroupBaysPerPage, this.BaysPerPage);

            element.AddValue(FieldNames.PrintTemplateSectionGroupSections, this.Sections);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.Number = element.GetByte(FieldNames.PrintTemplateSectionGroupNumber);
            this.Name = element.GetString(FieldNames.PrintTemplateSectionGroupName);
            this.BaysPerPage = element.GetByte(FieldNames.PrintTemplateSectionGroupBaysPerPage);

            this.Sections = element.GetChildList<PrintTemplateSectionItem>(FieldNames.PrintTemplateSectionGroupSections);

            UpdateChildIdLinks();
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateSectionGroupDto GetDataTransferObject()
        {
            return new PrintTemplateSectionGroupDto()
            {
                Id = this.Id,
                PrintTemplateId = this.PrintTemplateId,
                Number = this.Number,
                Name = this.Name,
                BaysPerPage = this.BaysPerPage,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PrintTemplateSectionGroupDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PrintTemplateId = Convert.ToString(dto.PrintTemplateId);
            this.Number = dto.Number;
            this.Name = dto.Name;
            this.BaysPerPage = dto.BaysPerPage;

            MarkDirty();
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            //update id on children
            foreach (var s in this.Sections)
            {
                s.PrintTemplateSectionGroupId = this.Id;
            }
        }

        public PrintTemplateSectionItem Insert(PrintTemplateSectionDto dto)
        {
            dto.Id = PrintTemplateSectionItem.GetNextId();
            PrintTemplateSectionItem item = new PrintTemplateSectionItem(dto);
            this.Sections.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
