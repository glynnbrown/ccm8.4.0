﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.Runtime.Serialization;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Schema;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     Report column filter dal item.
//    /// </summary>
//    [Serializable]
//    public sealed class ReportColumnFilterItem : ISerializable
//    {
//        #region Properties

//        [XmlIgnore]
//        public String ReportColumnLayoutId { get; set; }

//        private String Header { get; set; }

//        [XmlIgnore]
//        public Boolean IsDirty { get; set; }

//        public String Path { get; set; }
//        public String Text { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnFilterItem" />.
//        /// </summary>
//        public ReportColumnFilterItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnFilterItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnFilterItem(ReportColumnFilterDto dto)
//        {
//            UpdateFromDto(dto);
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnFilterItem" /> from the given stream.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private ReportColumnFilterItem(SerializationInfo info, StreamingContext context)
//        {
//            SetObjectData(info, context);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnFilterItem" /> as dirty.
//        /// </summary>
//        private void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnFilterDto" /> for this <see cref="ReportColumnFilterItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnFilterDto" />.</returns>
//        public ReportColumnFilterDto GetDataTransferObject()
//        {
//            return new ReportColumnFilterDto
//            {
//                ReportColumnLayoutId = ReportColumnLayoutId,
//                Text = Text,
//                Path = Path,
//                Header = Header
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnFilterItem" /> from the given <see cref="ReportColumnFilterDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnFilterDto" /> used to initialize the
//        ///     <see cref="ReportColumnFilterItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnFilterDto dto)
//        {
//            MarkDirty();

//            ReportColumnLayoutId = Convert.ToString(dto.ReportColumnLayoutId);
//            Text = dto.Text;
//            Path = dto.Path;
//            Header = dto.Header;
//        }

//        #endregion

//        #region ISerializable Support

//        /// <summary>
//        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="ReportColumnFilterItem" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            //TODO Add the strings as constants in FieldNames.
//            info.AddValue(FieldNames.ReportColumnFilterText, Text);
//            info.AddValue(FieldNames.ReportColumnFilterPath, Path);
//            info.AddValue(FieldNames.ReportColumnFilterHeader, Header);
//        }

//        /// <summary>
//        ///     Populates this <see cref="ReportColumnFilterItem" /> from the given <see cref="SerializationInfo" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private void SetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            //TODO Add the strings as constants in FieldNames.
//            Text = info.GetString(FieldNames.ReportColumnFilterText);
//            Path = info.GetString(FieldNames.ReportColumnFilterPath);
//            Header = info.GetString(FieldNames.ReportColumnFilterHeader);
//        }

//        #endregion
//    }
//}