﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// HighlightFilter dal item
    /// </summary>
    [Serializable]
    public sealed class HighlightFilterItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty {get; private set;}

        [XmlIgnoreAttribute]
        public HighlightFilterDtoKey DtoKey
        {
            get
            {
                return new HighlightFilterDtoKey()
                {
                    Id = this.Id
                };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String HighlightId { get; set; }

        public String Field { get; set; }
        public Byte Type { get; set; }
        public String Value { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightFilterItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        /// Creates a new instance of this type from the given dto
        /// </summary>
        public HighlightFilterItem(HighlightFilterDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightFilterItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightFilterField, this.Field);
            info.AddValue(FieldNames.HighlightFilterType, this.Type);
            info.AddValue(FieldNames.HighlightFilterValue, this.Value);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Field = info.GetString(FieldNames.HighlightFilterField);
            Type = info.GetByte(FieldNames.HighlightFilterType);
            Value = info.GetString(FieldNames.HighlightFilterValue);
        }

        /// <summary>
        /// Returns the HighlightFilterDto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightFilterDto GetDataTransferObject()
        {
            return new HighlightFilterDto()
            {
                Id = this.Id,
                HighlightId = this.HighlightId,
                Field = this.Field,
                Type = this.Type,
                Value = this.Value,
            };

        }

        /// <summary>
        /// Updates this item from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(HighlightFilterDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.HighlightId = Convert.ToString(dto.HighlightId);
            this.Field = dto.Field;
            this.Type = dto.Type;
            this.Value = dto.Value;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
