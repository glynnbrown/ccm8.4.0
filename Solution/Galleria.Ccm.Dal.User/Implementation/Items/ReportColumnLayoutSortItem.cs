﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.Runtime.Serialization;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Schema;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     Report column sort dal item.
//    /// </summary>
//    [Serializable]
//    public sealed class ReportColumnSortItem : ISerializable
//    {
//        #region Properties

//        [XmlIgnore]
//        public String ReportColumnLayoutId { get; set; }

//        public Byte Direction { get; set; }

//        [XmlIgnore]
//        public Boolean IsDirty { get; set; }

//        public String Path { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnSortItem" />.
//        /// </summary>
//        public ReportColumnSortItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnSortItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnSortItem(ReportColumnSortDto dto)
//        {
//            UpdateFromDto(dto);
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnSortItem" /> from the given stream.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private ReportColumnSortItem(SerializationInfo info, StreamingContext context)
//        {
//            SetObjectData(info, context);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnSortItem" /> as dirty.
//        /// </summary>
//        private void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnSortDto" /> for this <see cref="ReportColumnSortItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnSortDto" />.</returns>
//        public ReportColumnSortDto GetDataTransferObject()
//        {
//            return new ReportColumnSortDto
//            {
//                ReportColumnLayoutId = ReportColumnLayoutId,
//                Path = Path,
//                Direction = Direction
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnSortItem" /> from the given <see cref="ReportColumnSortDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnSortDto" /> used to initialize the
//        ///     <see cref="ReportColumnSortItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnSortDto dto)
//        {
//            MarkDirty();

//            ReportColumnLayoutId = Convert.ToString(dto.ReportColumnLayoutId);
//            Path = dto.Path;
//            Direction = dto.Direction;
//        }

//        #endregion

//        #region ISerializable Support

//        /// <summary>
//        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="ReportColumnSortItem" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            info.AddValue(FieldNames.ReportColumnSortPath, Path);
//            info.AddValue(FieldNames.ReportColumnSortDirection, Direction);
//        }

//        /// <summary>
//        ///     Populates this <see cref="ReportColumnSortItem" /> from the given <see cref="SerializationInfo" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private void SetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            Path = info.GetString(FieldNames.ReportColumnSortPath);
//            Direction = info.GetByte(FieldNames.ReportColumnSortDirection);
//        }

//        #endregion
//    }
//}