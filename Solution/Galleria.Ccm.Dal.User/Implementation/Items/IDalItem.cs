﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.20)
// V8-30870 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Galleria.Ccm.Dal.User.Schema;
using System.Globalization;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    public interface IDalItem
    {
        void GetObjectData(XElement element);
        void SetObjectData(XElement element);
    }

    /// <summary>
    /// Provides helper methods for use with IDalItem.
    /// </summary>
    public static class DalItemHelper
    {

        public static T Deserialize<T>(Stream stream)
            where T : IDalItem
        {
            T dalObject = default(T);
            using (XmlReader reader = XmlReader.Create(stream))
            {
                XmlSerializer x = new XmlSerializer(typeof(T));
                dalObject = (T)x.Deserialize(reader);
            }

            return dalObject;
        }

        #region Get XElement Value

        public static Byte GetByte(this XElement element, String propertyName)
        {
            return Convert.ToByte(element.XPathSelectElement(propertyName).Value, CultureInfo.InvariantCulture);
        }

        public static Byte? GetNullableByte(this XElement element, String propertyName)
        {
            String val = element.XPathSelectElement(propertyName).Value;
            if (String.IsNullOrWhiteSpace(val)) return null;
            return Convert.ToByte(val);
        }

        public static Int32 GetInt32(this XElement element, String propertyName)
        {
            return Convert.ToInt32(element.XPathSelectElement(propertyName).Value, CultureInfo.InvariantCulture);
        }

        public static String GetString(this XElement element, String propertyName)
        {
            String val = element.XPathSelectElement(propertyName).Value;
            if (val != null) val.Trim();
            return val;
        }

        public static Boolean GetBoolean(this XElement element, String propertyName)
        {
            return Convert.ToBoolean(element.XPathSelectElement(propertyName).Value, CultureInfo.InvariantCulture);
        }

        public static DateTime GetDateTime(this XElement element, String propertyName)
        {
            return Convert.ToDateTime(element.XPathSelectElement(propertyName).Value, CultureInfo.InvariantCulture);
        }

        public static Single GetSingle(this XElement element, String propertyName)
        {
            return Convert.ToSingle(element.XPathSelectElement(propertyName).Value, CultureInfo.InvariantCulture);
        }

        public static List<T> GetChildList<T>(this XElement element, String propertyName)
            where T : IDalItem
        {
            List<T> returnList = new List<T>();

            XElement childListElement = element.XPathSelectElement(propertyName);
            if (childListElement.HasElements)
            {
                foreach (XElement child in childListElement.Elements())
                {
                    T childItem = Activator.CreateInstance<T>();
                    childItem.SetObjectData(child);
                    returnList.Add(childItem);
                }
            }

            return returnList;
        }

        public static Boolean HasElement(this XElement element, String propertyName)
        {
            return element.XPathSelectElement(propertyName) != null;
        }

        #endregion

        #region Add XElement value

        public static void AddValue(this XElement element, String propertyName, Object value)
        {
            element.Add(new XElement(propertyName, value));
        }

        public static void AddValue(this XElement element, String propertyName, String value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                element.Add(new XElement(propertyName));
            }
            else
            {
                element.Add(new XElement(propertyName, value));
            }
        }

        public static void AddValue<T>(this XElement element, String propertyName, List<T> childList)
            where T : IDalItem
        {
            XElement listElement = new XElement(propertyName);
            element.Add(listElement);

            foreach (T child in childList)
            {
                XElement childElement = new XElement(typeof(T).Name);
                listElement.Add(childElement);

                child.GetObjectData(childElement);
            }
        }

        #endregion

        #region Script Methods

        /// <summary>
        /// Returns true if the specified script exists
        /// as a resource within this assembly
        /// </summary>
        /// <param name="majorVersion">The script set major version</param>
        /// <param name="minorVersion">The script set minor version</param>
        /// <param name="scriptNumber">The script number</param>
        /// <returns>True if the script exists, else false</returns>
        private static Boolean ScriptExists(Type itemType, Int32 majorVersion, Int32 minorVersion)
        {
            // generate the script method name
            String scriptName = String.Format(
                "Script{0}_{1}",
                majorVersion.ToString().PadLeft(4, '0'),
                minorVersion.ToString().PadLeft(4, '0'));

            //get the script method
            var scriptMethod = itemType.GetMethod(scriptName,
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);

            return scriptMethod != null;
        }

        private static void ExecuteScript(Type itemType, Byte majorVersion, Byte minorVersion, XElement rootElement)
        {
            String scriptName = String.Format(
                "Script{0}_{1}",
                majorVersion.ToString().PadLeft(4, '0'),
                minorVersion.ToString().PadLeft(4, '0'));

            //get the script method
            var scriptMethod = itemType.GetMethod(scriptName,
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);


            //perform the upgrade
            scriptMethod.Invoke(null, new Object[] { rootElement });

            //update the schema version
            rootElement.XPathSelectElement(FieldNames.MajorVersion).Value = majorVersion.ToString();
            rootElement.XPathSelectElement(FieldNames.MinorVersion).Value = minorVersion.ToString();
        }

        public static void Upgrade(Type scriptType, XElement rootElement)
        {
            //get the schema version of the file
            Byte majorVersion = 1;
            if (rootElement.HasElement(FieldNames.MajorVersion))
            {
                majorVersion = rootElement.GetByte(FieldNames.MajorVersion);
            }
            else
            {
                //add it in
                rootElement.Add(new XElement(FieldNames.MajorVersion, 1));
            }

            Byte minorVersion = 1;
            if (rootElement.HasElement(FieldNames.MinorVersion))
            {
                minorVersion = rootElement.GetByte(FieldNames.MinorVersion);
            }
            else
            {
                //add it in
                rootElement.Add(new XElement(FieldNames.MinorVersion, 1));
            }

            // loop through the major versions to upgrade
            while (true)
            {
                // loop through the minor versions to upgrade
                while (true)
                {
                    // determine if a script set exists for the specified
                    // major and minor version. If so, then execute the
                    // script set else
                    if (ScriptExists(scriptType, majorVersion, minorVersion))
                    {
                        ExecuteScript(scriptType, majorVersion, minorVersion, rootElement);
                    }
                    else
                    {
                        break;
                    }

                    // increase the minor version number
                    minorVersion++;
                }

                // we have now executed all minor version scripts
                // so reset the minor version number to zero and
                // increase the major version number
                majorVersion++;
                minorVersion = 0;

                // if a set of scripts does not exists for this
                // major version, then we stop our upgrade process
                if (!ScriptExists(scriptType, majorVersion, minorVersion))
                {
                    break;
                }
            }
        }

        public static void GetLatestVersionNumber(Type scriptType, out Byte majorVersion, out Byte minorVersion)
        {
            Int32 major = 1;
            Int32 minor = 1;

            // loop through the major versions to upgrade
            while (true)
            {
                // loop through the minor versions to upgrade
                while (true)
                {
                    // determine if a script set exists for the specified
                    // major and minor version. If so, then execute the
                    // script set else
                    if (!ScriptExists(scriptType, major, minor + 1))
                    {
                        break;
                    }

                    // increase the minor version number
                    minor++;
                }

                // we have now executed all minor version scripts
                // so reset the minor version number to zero and
                // increase the major version number
                minor = 0;

                // if a set of scripts does not exists for this
                // major version, then we stop our upgrade process
                if (!ScriptExists(scriptType, major+1, minor))
                {
                    break;
                }
                major++;
            }


            majorVersion = (Byte)major;
            minorVersion = (Byte)minor;
        }
        
        #endregion

    }
}
