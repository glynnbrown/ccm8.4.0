﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (CCM 8.0.1)
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion
#region Version History: (CCM 8.3)
// V8-32361 : L.Ineson
//  Added name and dates, updated to use scripts.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;
using System.Xml.Linq;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// ProductLibrary dal item.
    /// </summary>
    [Serializable]
    public sealed class ProductLibraryItem : IDalItem, IXmlSerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<ProductLibraryColumnMappingItem> _columnMappings = new List<ProductLibraryColumnMappingItem>();

        #endregion

        #region Properties

        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get { return _isNew; }
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (ColumnMappings.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the Product Library Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;

                //update children
                foreach (ProductLibraryColumnMappingItem mapping in ColumnMappings)
                {
                    mapping.ProductLibraryId = value;
                }
            }
        }

        public String Name { get; set; }
        public Byte FileType { get; set; }
        public String ExcelWorkbookSheet { get; set; }
        public Int32 StartAtRow { get; set; }
        public Boolean IsFirstRowHeaders { get; set; }
        public Byte? TextDataFormat { get; set; }
        public Byte? TextDelimiterType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        /// <summary>
        /// Returns the list of child column mappings
        /// </summary>
        public List<ProductLibraryColumnMappingItem> ColumnMappings
        {
            get { return _columnMappings; }
            set { _columnMappings = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ProductLibraryItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public ProductLibraryItem(ProductLibraryDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);

            //set version numbers to the latest available.
            Byte major, minor;
            DalItemHelper.GetLatestVersionNumber(GetScriptsType(), out major, out minor);
            this.MajorVersion = major;
            this.MinorVersion = minor;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Fetches the product library item from the given file id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static ProductLibraryItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            ProductLibraryItem dalObject = null;

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = DalItemHelper.Deserialize<ProductLibraryItem>(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = DalItemHelper.Deserialize<ProductLibraryItem>(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            dalObject.Name = Path.GetFileName(id);
            dalObject.DateCreated = File.GetCreationTimeUtc(id);
            dalObject.DateLastModified = File.GetLastAccessTimeUtc(id);

            return dalObject;
        }

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save()
        {
            //Check that we are not in readonly mode.
            if (this.IsReadOnly)
            {
                throw new InvalidOperationException("File is readonly");
            }

            //Get the filepath
            this.Id = Path.ChangeExtension(
                Path.Combine(Path.GetDirectoryName(this.Id), this.Name),
                Constants.ProductLibraryFileExtension);

            //delete the old file first
            Delete();

            Directory.CreateDirectory(Path.GetDirectoryName(this.Id));

            //create the new file stream and serialize.
            _stream = File.Open(this.Id, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream == null) return;

            String lastPath = _stream.Name;
            Unlock();
            File.Delete(lastPath);
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this product library.
        /// </summary>
        public void Unlock()
        {
            if (_stream == null) return;

            _stream.Close();
            _stream.Dispose();
            _stream = null;

            this.IsReadOnly = true;
        }


        #region Child related methods
        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (ProductLibraryColumnMappingItem i in ColumnMappings)
            {
                i.ProductLibraryId = this.Id;
            }
        }

        /// <summary>
        /// Adds a new child to this item.
        /// </summary>
        public ProductLibraryColumnMappingItem Insert(ProductLibraryColumnMappingDto dto)
        {
            dto.Id = ProductLibraryColumnMappingItem.GetNextId();
            ProductLibraryColumnMappingItem item = new ProductLibraryColumnMappingItem(dto);
            this.ColumnMappings.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(ProductLibraryDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                dto.Id = Path.ChangeExtension(
                Path.Combine(Path.GetDirectoryName(_stream.Name), dto.Name),
                Constants.ProductLibraryFileExtension);
            }

            this.Id = (String)dto.Id;
            this.Name = dto.Name;
            this.FileType = dto.FileType;
            this.ExcelWorkbookSheet = dto.ExcelWorkbookSheet;
            this.StartAtRow = dto.StartAtRow;
            this.IsFirstRowHeaders = dto.IsFirstRowHeaders;
            this.TextDataFormat = dto.TextDataFormat;
            this.TextDelimiterType = dto.TextDelimiterType;
            this.DateCreated = dto.DateCreated;
            this.DateLastModified = dto.DateLastModified;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public ProductLibraryDto GetDataTransferObject()
        {
            return new ProductLibraryDto()
            {
                Id = this.Id,
                Name = this.Name,
                FileType = this.FileType,
                ExcelWorkbookSheet = this.ExcelWorkbookSheet,
                StartAtRow = this.StartAtRow,
                IsFirstRowHeaders = this.IsFirstRowHeaders,
                TextDataFormat = this.TextDataFormat,
                TextDelimiterType = this.TextDelimiterType,
                DateCreated = this.DateCreated,
                DateLastModified = this.DateLastModified,
            };
        }

        #endregion

        #endregion

        #region IDalItem


        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.MajorVersion, this.MajorVersion);
            element.AddValue(FieldNames.MinorVersion, this.MinorVersion);

            element.AddValue(FieldNames.ProductLibraryFileType, this.FileType);
            element.AddValue(FieldNames.ProductLibraryExcelWorkbookSheet, this.ExcelWorkbookSheet);
            element.AddValue(FieldNames.ProductLibraryStartAtRow, this.StartAtRow);
            element.AddValue(FieldNames.ProductLibraryIsFirstRowHeaders, this.IsFirstRowHeaders);
            element.AddValue(FieldNames.ProductLibraryTextDataFormat, this.TextDataFormat);
            element.AddValue(FieldNames.ProductLibraryTextDelimiterType, this.TextDelimiterType);

            element.AddValue(FieldNames.ProductLibraryColumnMappings, this.ColumnMappings);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.MajorVersion = element.GetByte(FieldNames.MajorVersion);
            this.MinorVersion = element.GetByte(FieldNames.MinorVersion);

            this.FileType = element.GetByte(FieldNames.ProductLibraryFileType);
            this.ExcelWorkbookSheet = element.GetString(FieldNames.ProductLibraryExcelWorkbookSheet);
            this.StartAtRow = element.GetInt32(FieldNames.ProductLibraryStartAtRow);
            this.IsFirstRowHeaders = element.GetBoolean(FieldNames.ProductLibraryIsFirstRowHeaders);
            this.TextDataFormat = element.GetNullableByte(FieldNames.ProductLibraryTextDataFormat);
            this.TextDelimiterType = element.GetNullableByte(FieldNames.ProductLibraryTextDelimiterType);

            this.ColumnMappings = element.GetChildList<ProductLibraryColumnMappingItem>(FieldNames.ProductLibraryColumnMappings);

            UpdateChildIdLinks();
        }


        #endregion

        #region IXmlSerializable Members

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
        {
            //read the xelement
            var x = XElement.ReadFrom(reader) as XElement;

            //Perform the upgrade
            DalItemHelper.Upgrade(GetScriptsType(), x);

            //Load
            ((IDalItem)this).SetObjectData(x);
        }

        void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
        {
            XElement rootElement = new XElement(this.GetType().Name);
            ((IDalItem)this).GetObjectData(rootElement);

            //don't write from root element otherwise we end up with double.
            foreach (XElement element in rootElement.Elements())
            {
                element.WriteTo(writer);
            }
        }

        private Type GetScriptsType()
        {
            return typeof(Scripts.ProductLibraryScripts);
        }

        #endregion
    }
}

