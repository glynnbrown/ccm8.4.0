#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property and tidied up.
// V8-26472 : A.Probyn
//      Changed properties to be public for xml serializer to output.

#endregion
#region Version History: (CCM 820)
//V8-30870 : L.Ineson 
//  Added IDalItem implementation.
#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added Id
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Xml.Linq;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Custom column sort dal item.
    /// </summary>
    [Serializable]
    public sealed class CustomColumnSortItem : IDalItem
    {
        #region Fields
        private Int32 _id;
        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty 
        { 
            get; 
            set; 
        }

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get;
            set;
        }

        [XmlIgnoreAttribute]
        public CustomColumnSortDtoKey DtoKey
        {
            get
            {
                return new CustomColumnSortDtoKey()
                {
                    Path = this.Path
                };
            }
        }

        [XmlIgnoreAttribute]
        public String CustomColumnLayoutId { get; set; }

        public Byte Direction { get; set; }
        public String Path { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnSortItem" />.
        /// </summary>
        public CustomColumnSortItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnSortItem" /> from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public CustomColumnSortItem(CustomColumnSortDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this <see cref="CustomColumnSortItem" /> as dirty.
        /// </summary>
        private void MarkDirty()
        {
            IsDirty = true;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Returns a <see cref="CustomColumnSortDto" /> for this <see cref="CustomColumnSortItem" />.
        /// </summary>
        /// <returns>A new instance of a <see cref="CustomColumnSortDto" />.</returns>
        public CustomColumnSortDto GetDataTransferObject()
        {
            return new CustomColumnSortDto
            {
                Id = this.Id,
                CustomColumnLayoutId = CustomColumnLayoutId,
                Path = Path,
                Direction = Direction
            };
        }

        /// <summary>
        ///     Updates this <see cref="CustomColumnSortItem" /> from the given <see cref="CustomColumnSortDto" />.
        /// </summary>
        /// <param name="dto">
        ///     The <see cref="CustomColumnSortDto" /> used to initialize the
        ///     <see cref="CustomColumnSortItem" />.
        /// </param>
        public void UpdateFromDto(CustomColumnSortDto dto)
        {
            this.Id = dto.Id;
            CustomColumnLayoutId = Convert.ToString(dto.CustomColumnLayoutId);
            Path = dto.Path;
            Direction = dto.Direction;

            MarkDirty();
        }

        #endregion

        #region IDalItem Support

        /// <summary>
        ///  Populates the given element
        /// </summary>
        public void GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.CustomColumnSortPath, Path);
            element.AddValue(FieldNames.CustomColumnSortDirection, Direction);
        }

        /// <summary>
        /// Populates this item from the given xml element.
        /// </summary>
        public void SetObjectData(XElement element)
        {
            Path = element.GetString(FieldNames.CustomColumnSortPath);
            Direction = element.GetByte(FieldNames.CustomColumnSortDirection);
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}