﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 J.Pickup
//  Created
// CCM-24265 : N.Haywood
//  Added unlocking
// V8-26248 : L.Luong
//  Removed the unlock in save
#endregion

#region Version History: (CCM 8.0.1)
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion
#endregion

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;
using System.Diagnostics;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// Label dal item.
    /// </summary>
    [Serializable]
    public sealed class LabelItem : ISerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private const Byte majorVersion = 1;
        private const Byte minorVersion = 1;

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get { return _isNew; }
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the Label Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
            }
        }

        public String Name { get; set; }
        public Byte Type { get; set; }
        public Int32 BackgroundColour { get; set; }
        public Int32 BorderColour { get; set; }
        public Int32 BorderThickness { get; set; }
        public String Text { get; set; }
        public Int32 TextColour { get; set; }
        public String Font { get; set; }
        public Int32 FontSize { get; set; }
        public Boolean IsRotateToFitOn { get; set; }
        public Boolean IsShrinkToFitOn { get; set; }
        public Boolean TextBoxFontBold { get; set; }
        public Boolean TextBoxFontItalic { get; set; }
        public Boolean TextBoxFontUnderlined { get; set; }
        public Byte LabelHorizontalPlacement { get; set; }
        public Byte LabelVerticalPlacement { get; set; }
        public Byte TextBoxTextAlignment { get; set; }
        public Byte TextDirection { get; set; }
        public Byte TextWrapping { get; set; }
        public Single BackgroundTransparency { get; set; }
        public Boolean ShowOverImages { get; set; }
        public Boolean ShowLabelPerFacing { get; set; }
        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public LabelItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public LabelItem(LabelDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public LabelItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the label item from the given file id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static LabelItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            LabelItem dalObject = null;
            XmlSerializer x = new XmlSerializer(typeof(LabelItem));

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = (LabelItem)x.Deserialize(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = (LabelItem)x.Deserialize(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Check that we are not in readonly mode.
            Debug.Assert(!this.IsReadOnly, "Trying to save when readonly");
            if (this.IsReadOnly) return;

            //Get the filepath
            String filePath = null;
            if (String.IsNullOrEmpty(fixedDirectory))
            {
                filePath = Path.Combine(Path.GetDirectoryName(this.Id), Name);
            }
            else
            {
                filePath = Path.Combine(fixedDirectory, Name);
            }
            filePath = Path.ChangeExtension(filePath, Constants.LabelFileExtension);
            this.Id = filePath;
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;

            //delete the old file first
            Delete();

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //create the new file stream and serialize.
            _stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream != null)
            {
                String lastPath = _stream.Name;
                Unlock();
                File.Delete(lastPath);
            }
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this highlight.
        /// </summary>
        public void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(LabelDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                String curPath = _stream.Name;
                String newFilename = dto.Name;

                String filePath = Path.Combine(Path.GetDirectoryName(curPath), newFilename);
                filePath = Path.ChangeExtension(filePath, Constants.LabelFileExtension);

                Id = filePath;
                dto.Id = filePath;
            }
            else
            {
                //just load the id from the dto.
                Id = (String)dto.Id;
            }

            Name = dto.Name;
            Type = dto.Type;
            BackgroundColour = dto.BackgroundColour;
            BorderColour = dto.BorderColour;
            BorderThickness = dto.BorderThickness;
            Text = dto.Text;
            TextColour = dto.TextColour;
            Font = dto.Font;
            FontSize = dto.FontSize;
            IsRotateToFitOn = dto.IsRotateToFitOn;
            IsShrinkToFitOn = dto.IsShrinkToFitOn;
            TextBoxFontBold = dto.TextBoxFontBold;
            TextBoxFontItalic = dto.TextBoxFontItalic;
            TextBoxFontUnderlined = dto.TextBoxFontUnderlined;
            LabelHorizontalPlacement = (Byte)dto.LabelHorizontalPlacement;
            LabelVerticalPlacement = (Byte)dto.LabelVerticalPlacement;
            TextBoxTextAlignment = (Byte)dto.TextBoxTextAlignment;
            TextDirection = (Byte)dto.TextDirection;
            TextWrapping = (Byte)dto.TextWrapping;
            BackgroundTransparency = dto.BackgroundTransparency;
            ShowOverImages = dto.ShowOverImages;
            ShowLabelPerFacing = dto.ShowLabelPerFacing;

        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public LabelDto GetDataTransferObject()
        {
            return new LabelDto()
            {
                Id = this.Id,
                Name = this.Name,
                Type = this.Type,
                BackgroundColour = this.BackgroundColour,
                BorderColour = this.BorderColour,
                BorderThickness = this.BorderThickness,
                Text = this.Text,
                TextColour = this.TextColour,
                Font = this.Font,
                FontSize = this.FontSize,
                IsRotateToFitOn = this.IsRotateToFitOn,
                IsShrinkToFitOn = this.IsShrinkToFitOn,
                TextBoxFontBold = this.TextBoxFontBold,
                TextBoxFontItalic = this.TextBoxFontItalic,
                TextBoxFontUnderlined = this.TextBoxFontUnderlined,
                LabelHorizontalPlacement = this.LabelHorizontalPlacement,
                LabelVerticalPlacement = this.LabelVerticalPlacement,
                TextBoxTextAlignment = this.TextBoxTextAlignment,
                TextDirection = this.TextDirection,
                TextWrapping = this.TextWrapping,
                BackgroundTransparency = this.BackgroundTransparency,
                ShowOverImages = this.ShowOverImages,
                ShowLabelPerFacing = this.ShowLabelPerFacing,
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.LabelName, this.Name);
            info.AddValue(FieldNames.LabelType, this.Type);
            info.AddValue(FieldNames.LabelBackgroundColour, this.BackgroundColour);
            info.AddValue(FieldNames.LabelBorderColour, this.BorderColour);
            info.AddValue(FieldNames.LabelBorderThickness, this.BorderThickness);
            info.AddValue(FieldNames.LabelText, this.Text);
            info.AddValue(FieldNames.LabelTextColour, this.TextColour);
            info.AddValue(FieldNames.LabelFont, this.Font);
            info.AddValue(FieldNames.LabelFontSize, this.FontSize);
            info.AddValue(FieldNames.LabelIsRotateToFitOn, this.IsRotateToFitOn);
            info.AddValue(FieldNames.LabelIsShrinkToFitOn, this.IsShrinkToFitOn);
            info.AddValue(FieldNames.LabelTextBoxFontBold, this.TextBoxFontBold);
            info.AddValue(FieldNames.LabelTextBoxFontItalic, this.TextBoxFontItalic);
            info.AddValue(FieldNames.LabelTextBoxFontUnderlined, this.TextBoxFontUnderlined);
            info.AddValue(FieldNames.LabelLabelHorizontalPlacement, this.LabelHorizontalPlacement);
            info.AddValue(FieldNames.LabelLabelVerticalPlacement, this.LabelVerticalPlacement);
            info.AddValue(FieldNames.LabelTextBoxTextAlignment, this.TextBoxTextAlignment);
            info.AddValue(FieldNames.LabelTextDirection, this.TextDirection);
            info.AddValue(FieldNames.LabelTextWrapping, this.TextWrapping);
            info.AddValue(FieldNames.LabelBackgroundTransparency , this.BackgroundTransparency);
            info.AddValue(FieldNames.LabelShowOverImages, this.ShowOverImages);
            info.AddValue(FieldNames.LabelShowLabelPerFacing, this.ShowLabelPerFacing);
            info.AddValue(FieldNames.LabelMajorVersion, this.MajorVersion);
            info.AddValue(FieldNames.LabelMinorVersion, this.MinorVersion);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.Name = info.GetString(FieldNames.LabelName);
            this.Type = info.GetByte(FieldNames.LabelType);
            this.BackgroundColour = info.GetInt32(FieldNames.LabelBackgroundColour);
            this.BorderColour = info.GetInt32(FieldNames.LabelBorderColour);
            this.BorderThickness = info.GetInt32(FieldNames.LabelBorderThickness);
            this.Text = info.GetString(FieldNames.LabelText);
            this.TextColour = info.GetInt32(FieldNames.LabelTextColour);
            this.Font = info.GetString(FieldNames.LabelFont);
            this.FontSize = info.GetInt32(FieldNames.LabelFontSize);
            this.IsRotateToFitOn = info.GetBoolean(FieldNames.LabelIsRotateToFitOn);
            this.IsShrinkToFitOn = info.GetBoolean(FieldNames.LabelIsShrinkToFitOn);
            this.TextBoxFontBold = info.GetBoolean(FieldNames.LabelTextBoxFontBold);
            this.TextBoxFontItalic = info.GetBoolean(FieldNames.LabelTextBoxFontItalic);
            this.TextBoxFontUnderlined = info.GetBoolean(FieldNames.LabelTextBoxFontUnderlined);
            this.LabelHorizontalPlacement = info.GetByte(FieldNames.LabelLabelHorizontalPlacement);
            this.LabelVerticalPlacement = info.GetByte(FieldNames.LabelLabelVerticalPlacement);
            this.TextBoxTextAlignment = info.GetByte(FieldNames.LabelTextBoxTextAlignment);
            this.TextDirection = info.GetByte(FieldNames.LabelTextDirection);
            this.TextWrapping = info.GetByte(FieldNames.LabelTextWrapping);
            this.BackgroundTransparency = info.GetSingle(FieldNames.LabelBackgroundTransparency);
            this.ShowOverImages = info.GetBoolean(FieldNames.LabelShowOverImages);
            this.ShowLabelPerFacing = info.GetBoolean(FieldNames.LabelShowLabelPerFacing);
            this.MajorVersion = info.GetByte(FieldNames.LabelMajorVersion);
            this.MinorVersion = info.GetByte(FieldNames.LabelMinorVersion);
        }

        #endregion
    }
}