﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30791: D.Pleasance
//		Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// SystemSetting dal item
    /// </summary>
    [Serializable]
    public sealed class UserEditorSettingColorItem : ISerializable
    {
        #region Fields

        private Int32 _id;

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Int32 Color { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public UserEditorSettingColorItem() { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public UserEditorSettingColorItem(UserEditorSettingColorDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserEditorSettingColorItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(UserEditorSettingColorDto dto)
        {
            Id = (Int32)dto.Id;
            Color = dto.Color;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public UserEditorSettingColorDto GetDataTransferObject()
        {
            return new UserEditorSettingColorDto()
            {
                Id = this.Id,
                Color = this.Color
            };
        }

        #endregion

        #region XML Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingColor, this.Color);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Color = info.GetInt32(FieldNames.SystemSettingColor);
        }

        #endregion
    }
}