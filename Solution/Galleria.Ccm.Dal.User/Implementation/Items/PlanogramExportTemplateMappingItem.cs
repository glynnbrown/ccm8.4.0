﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramExportTemplateMapping dal item.
    /// </summary>
    [Serializable]
    public class PlanogramExportTemplateMappingItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public PlanogramExportTemplateMappingDtoKey DtoKey
        {
            get
            {
                return new PlanogramExportTemplateMappingDtoKey()
                    {
                        Field = this.Field,
                        PlanogramExportTemplateId = this.PlanogramExportTemplateId
                    };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String PlanogramExportTemplateId { get; set; }

        public Byte FieldType { get; set; }
        public String Field { get; set; }
        public String ExternalField { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramExportTemplateMappingItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramExportTemplateMappingItem(PlanogramExportTemplateMappingDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///  Creates a new instance of this type from the given
        ///  serialization context.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramExportTemplateMappingItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return String.Format("{0}-{1}-{2}", this.FieldType.ToString(), this.Field, this.ExternalField);
        }

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramExportTemplateMappingFieldType, this.FieldType);
            info.AddValue(FieldNames.PlanogramExportTemplateMappingField, this.Field);
            info.AddValue(FieldNames.PlanogramExportTemplateMappingExternalField, this.ExternalField);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            FieldType = info.GetByte(FieldNames.PlanogramExportTemplateMappingFieldType);
            Field = info.GetString(FieldNames.PlanogramExportTemplateMappingField);
            ExternalField = info.GetString(FieldNames.PlanogramExportTemplateMappingExternalField);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramExportTemplateMappingDto GetDataTransferObject()
        {
            return new PlanogramExportTemplateMappingDto()
            {
                Id = this.Id,
                PlanogramExportTemplateId = this.PlanogramExportTemplateId,
                FieldType = this.FieldType,
                Field = this.Field,
                ExternalField = this.ExternalField
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramExportTemplateMappingDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PlanogramExportTemplateId = Convert.ToString(dto.PlanogramExportTemplateId);
            this.FieldType = dto.FieldType;
            this.Field = dto.Field;
            this.ExternalField = dto.ExternalField;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
