﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-25559 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// Connection dal item
    /// </summary>
    [Serializable]
    public sealed class ConnectionItem : ISerializable
    {
        #region Fields
        private Int32 _id;
        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String ServerName { get; set; }
        public String DatabaseName { get; set; }
        public String DatabaseFileName { get; set; }
        public String HostName { get; set; }
        public Byte PortNumber { get; set; }
        public Int32 BusinessEntityId { get; set; }
        public Boolean IsAutoConnect { get; set; }
        public Boolean IsCurrentConnection { get; set; }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ConnectionItem() { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public ConnectionItem(ConnectionDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public ConnectionItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(ConnectionDto dto)
        {
            Id = dto.Id;
            ServerName = dto.ServerName;
            DatabaseName = dto.DatabaseName;
            DatabaseFileName = dto.DatabaseFileName;
            HostName = dto.HostName;
            PortNumber = dto.PortNumber;
            BusinessEntityId = dto.BusinessEntityId;
            IsAutoConnect = dto.IsAutoConnect;
            IsCurrentConnection = dto.IsCurrentConnection;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public ConnectionDto GetDataTransferObject()
        {
            return new ConnectionDto()
            {
                Id = this.Id,
                ServerName = this.ServerName,
                DatabaseName = this.DatabaseName,
                DatabaseFileName = this.DatabaseFileName,
                HostName = this.HostName,
                PortNumber = this.PortNumber,
                BusinessEntityId = this.BusinessEntityId,
                IsAutoConnect = this.IsAutoConnect,
                IsCurrentConnection = this.IsCurrentConnection,
            };
        }

        #endregion

        #region XML Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.ConnectionServerName, this.ServerName);
            info.AddValue(FieldNames.ConnectionDatabaseName, this.DatabaseName);
            info.AddValue(FieldNames.ConnectionDatabaseFileName, this.DatabaseFileName);
            info.AddValue(FieldNames.ConnectionHostName, this.HostName);
            info.AddValue(FieldNames.ConnectionPortNumber, this.PortNumber);
            info.AddValue(FieldNames.ConnectionBusinessEntityId, this.BusinessEntityId);
            info.AddValue(FieldNames.ConnectionIsAutoConnect, this.IsAutoConnect);
            info.AddValue(FieldNames.ConnectionIsCurrentConnection, this.IsCurrentConnection);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.ServerName = info.GetString(FieldNames.ConnectionServerName);
            this.DatabaseName = info.GetString(FieldNames.ConnectionDatabaseName);
            this.DatabaseFileName = info.GetString(FieldNames.ConnectionDatabaseFileName);
            this.HostName = info.GetString(FieldNames.ConnectionHostName);
            this.PortNumber = info.GetByte(FieldNames.ConnectionPortNumber);
            this.BusinessEntityId = info.GetInt32(FieldNames.ConnectionBusinessEntityId);
            this.IsAutoConnect = info.GetBoolean(FieldNames.ConnectionIsAutoConnect);
            this.IsCurrentConnection = info.GetBoolean(FieldNames.ConnectionIsCurrentConnection);
        }

        #endregion

        #region Id Helpers

        private static Int32 _nextId = 1;

        public static Int32 GetNextId()
        {
            Int32 returnVal = _nextId;
            _nextId++;
            return returnVal;
        }

        public static void ResetId()
        {
            _nextId = 1;
        }

        #endregion
    }
}
