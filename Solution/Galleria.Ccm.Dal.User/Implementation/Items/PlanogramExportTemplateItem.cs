﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
// V8-32743 : M.Pettit
//  Old file was not correctly deleted
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramExportTemplate dal item.
    /// </summary>
    [Serializable]
    public sealed class PlanogramExportTemplateItem: ISerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<PlanogramExportTemplateMappingItem> _mappings = new List<PlanogramExportTemplateMappingItem>();
        private List<PlanogramExportTemplatePerformanceMetricItem> _performanceMetrics = new List<PlanogramExportTemplatePerformanceMetricItem>();

        private const Byte majorVersion = 1;
        private const Byte minorVersion = 1;
        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get { return _isNew; }
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (Mappings.Any(f => f.IsDirty)) return true;
                if (PerformanceMetrics.Any(f => f.IsDirty)) return true;
                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the PlanogramExportTemplate Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        public String Name { get; set; }
        public Byte FileType { get; set; }
        public String FileVersion { get; set; }
        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        /// <summary>
        /// Returns the list of child mappings
        /// </summary>
        public List<PlanogramExportTemplateMappingItem> Mappings
        {
            get { return _mappings; }
            set { _mappings = value; }
        }

        /// <summary>
        /// Returns the list of child mappings
        /// </summary>
        public List<PlanogramExportTemplatePerformanceMetricItem> PerformanceMetrics
        {
            get { return _performanceMetrics; }
            set { _performanceMetrics = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramExportTemplateItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramExportTemplateItem(PlanogramExportTemplateDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramExportTemplateItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the item from the given file id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PlanogramExportTemplateItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            PlanogramExportTemplateItem dalObject = null;
            XmlSerializer x = new XmlSerializer(typeof(PlanogramExportTemplateItem));

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = (PlanogramExportTemplateItem)x.Deserialize(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = (PlanogramExportTemplateItem)x.Deserialize(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Get the filepath
            String filePath = null;
            if (String.IsNullOrEmpty(fixedDirectory))
            {
                filePath = Path.Combine(Path.GetDirectoryName(this.Id), Name);
            }
            else
            {
                filePath = Path.Combine(fixedDirectory, Name);
            }

            filePath = Path.ChangeExtension(filePath, Constants.PlanogramExportTemplateFileExtension);
            this.Id = filePath;
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;
            
            //delete the old file first
            DeleteOldFile(this.Id);

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //create the new file stream and serialize.
            _stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream != null)
            {
                String lastPath = _stream.Name;
                Unlock();
                File.Delete(lastPath);
            }
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this highlight.
        /// </summary>
        public void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        private void DeleteOldFile(String path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (PlanogramExportTemplateMappingItem m in this.Mappings)
            {
                m.PlanogramExportTemplateId = this.Id;
            }
            foreach (PlanogramExportTemplatePerformanceMetricItem m in this.PerformanceMetrics)
            {
                m.PlanogramExportTemplateId = this.Id;
            }
        }

        /// <summary>
        /// Inserts a new child from the given dto.
        /// </summary>
        public PlanogramExportTemplateMappingItem Insert(PlanogramExportTemplateMappingDto dto)
        {
            dto.Id = PlanogramExportTemplateMappingItem.GetNextId();
            PlanogramExportTemplateMappingItem item = new PlanogramExportTemplateMappingItem(dto);
            this.Mappings.Add(item);
            MarkDirty();
            return item;
        }

        /// <summary>
        /// Inserts a new child from the given dto.
        /// </summary>
        public PlanogramExportTemplatePerformanceMetricItem Insert(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            dto.Id = PlanogramExportTemplatePerformanceMetricItem.GetNextId();
            PlanogramExportTemplatePerformanceMetricItem item = new PlanogramExportTemplatePerformanceMetricItem(dto);
            this.PerformanceMetrics.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramExportTemplateDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                String curPath = _stream.Name;
                String newFilename = dto.Name;

                String filePath = Path.Combine(Path.GetDirectoryName(curPath), newFilename);
                filePath = Path.ChangeExtension(filePath, Constants.PlanogramExportTemplateFileExtension);

                Id = filePath;
                dto.Id = filePath;
            }
            else
            {
                //just load the id from the dto.
                Id = Path.ChangeExtension(Convert.ToString(dto.Id), Constants.PlanogramExportTemplateFileExtension);
                dto.Id = Id; //make sure the dto id is the same.
            }

            Name = dto.Name;
            FileType = dto.FileType;
            FileVersion = dto.FileVersion;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramExportTemplateDto GetDataTransferObject()
        {
            return new PlanogramExportTemplateDto()
            {
                Id = this.Id,
                Name = this.Name,
                FileType = this.FileType,
                FileVersion = this.FileVersion
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramExportTemplateName, this.Name);
            info.AddValue(FieldNames.PlanogramExportTemplateFileType, this.FileType);
            info.AddValue(FieldNames.PlanogramExportTemplateFileVersion, this.FileVersion);
            info.AddValue(FieldNames.PlanogramExportTemplateMajorVersion, this.MajorVersion);
            info.AddValue(FieldNames.PlanogramExportTemplateMinorVersion, this.MinorVersion);

            info.AddValue(FieldNames.PlanogramExportTemplateMappings, this.Mappings);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetrics, this.PerformanceMetrics);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.PlanogramExportTemplateName);
            FileType = info.GetByte(FieldNames.PlanogramExportTemplateFileType);
            FileVersion = info.GetString(FieldNames.PlanogramExportTemplateFileVersion);
            MajorVersion = info.GetByte(FieldNames.PlanogramExportTemplateMajorVersion);
            MinorVersion = info.GetByte(FieldNames.PlanogramExportTemplateMinorVersion);

            Mappings = (List<PlanogramExportTemplateMappingItem>)info.GetValue(FieldNames.PlanogramExportTemplateMappings, typeof(List<PlanogramExportTemplateMappingItem>));
            PerformanceMetrics = (List<PlanogramExportTemplatePerformanceMetricItem>)info.GetValue(FieldNames.PlanogramExportTemplatePerformanceMetrics, typeof(List<PlanogramExportTemplatePerformanceMetricItem>));

            UpdateChildIdLinks();
        }

        #endregion
    }
}
