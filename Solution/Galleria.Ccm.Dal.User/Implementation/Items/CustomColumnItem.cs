#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva 
//  Created.
// V8-26076 : A.Silva 
//  Removed Id Property and tidied up.
// V8-26408 : L.Ineson
//  Removed MappingId
// V8-26472 : A.Probyn
//      Removed obsolete properties.
//      Changed properties to be public for xml serializer to output.
// V8-27504 : A.Silva
//      Added HeaderGroupNumber property.

#endregion

#region Version History: (CCM 8.2)
//V8-30870 : L.Ineson
//  Added Width and Display name properties
#endregion

#region Version History: (CCM 8.3)
//V8-31541 : L.Ineson
//  Added Id
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Xml.Linq;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Custom column dal item.
    /// </summary>
    [Serializable]
    public sealed class CustomColumnItem :  IDalItem
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty 
        {
            get; private set;
        }

        [XmlIgnoreAttribute]
        public Int32 Id 
        {
            get; set;
        }

        [XmlIgnoreAttribute]
        public CustomColumnDtoKey DtoKey
        {
            get
            {
                return new CustomColumnDtoKey()
                {
                   CustomColumnLayoutId = this.CustomColumnLayoutId,
                   Path =this.Path
                };
            }
        }

        [XmlIgnoreAttribute]
        public String CustomColumnLayoutId { get; set; }
        
        public Int32 Number { get; set; }
        public String Path { get; set; }
        public Byte TotalType { get; set; }
        public Int32 Width { get; set; }
        public String DisplayName { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnItem" />.
        /// </summary>
        public CustomColumnItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnItem" /> from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public CustomColumnItem(CustomColumnDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this item as dirty.
        /// </summary>
        private void MarkDirty()
        {
            IsDirty = true;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Returns a <see cref="CustomColumnDto" /> for this <see cref="CustomColumnItem" />.
        /// </summary>
        /// <returns>A new instance of a <see cref="CustomColumnDto" />.</returns>
        public CustomColumnDto GetDataTransferObject()
        {
            return new CustomColumnDto
            {
                Id = this.Id,
                CustomColumnLayoutId = this.CustomColumnLayoutId,
                Path = this.Path,
                Number = this.Number,
                TotalType = this.TotalType,
                DisplayName = this.DisplayName,
                Width = this.Width,
            };
        }

        /// <summary>
        /// Updates this from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(CustomColumnDto dto)
        {
            this.Id = dto.Id;
            this.CustomColumnLayoutId = Convert.ToString(dto.CustomColumnLayoutId);
            this.Path = dto.Path;
            this.Number = dto.Number;
            this.TotalType = dto.TotalType;
            this.DisplayName = dto.DisplayName;
            this.Width = dto.Width;

            MarkDirty();
        }

        #endregion

        #region IDalItem Support

        /// <summary>
        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="CustomColumnItem" />.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.CustomColumnPath, Path);
            element.AddValue(FieldNames.CustomColumnNumber, Number);
            element.AddValue(FieldNames.CustomColumnTotalType, TotalType);
            element.AddValue(FieldNames.CustomColumnWidth, Width);
            element.AddValue(FieldNames.CustomColumnDisplayName, DisplayName);
        }

        /// <summary>
        /// Populates this item from the given Xml element.
        /// </summary>
        public void SetObjectData(XElement element)
        {
            Path = element.GetString(FieldNames.CustomColumnPath);
            Number = element.GetInt32(FieldNames.CustomColumnNumber);
            TotalType = element.GetByte(FieldNames.CustomColumnTotalType);
            Width = element.GetInt32(FieldNames.CustomColumnWidth);
            DisplayName = element.GetString(FieldNames.CustomColumnDisplayName);
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion

    }
}