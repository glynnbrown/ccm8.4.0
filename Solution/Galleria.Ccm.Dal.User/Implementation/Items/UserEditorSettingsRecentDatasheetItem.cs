﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM 830
// V8-31699 : A.Heathcote
//     Created   
#endregion

#endregion
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Ccm.Model;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    public sealed class UserEditorSettingsRecentDatasheetItem : ISerializable
    {
        #region Fields

        private Int32 _id;
        private Object _datasheetId;
        

        #endregion

        #region Properties
        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Object DatasheetId
        {
            get { return _datasheetId; }
            set { _datasheetId = value; }

        }

       
    #endregion

          #region Constructor
        public UserEditorSettingsRecentDatasheetItem() { }


        public UserEditorSettingsRecentDatasheetItem(UserEditorSettingsRecentDatasheetDto dto)
        {
            UpdateFromDto(dto);
        }


        public UserEditorSettingsRecentDatasheetItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }
        #endregion

        #region Data Transfer Object


        public void UpdateFromDto(UserEditorSettingsRecentDatasheetDto dto)
        {
            Id = dto.Id;
            DatasheetId = dto.DatasheetId;
           
        }

        public UserEditorSettingsRecentDatasheetDto GetDataTransferObject()
        {
            return new UserEditorSettingsRecentDatasheetDto()
            {
                Id = this.Id,
                DatasheetId = this.DatasheetId,
                
            };
        }
        #endregion

        #region XML Object
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingRecentDatasheetId, this.DatasheetId);
            
        }

        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
              DatasheetId = info.GetString(FieldNames.SystemSettingRecentDatasheetId);
            }
           
        }
        #endregion
    }


        