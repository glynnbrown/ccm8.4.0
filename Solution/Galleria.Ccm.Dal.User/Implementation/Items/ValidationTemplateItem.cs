﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva 
//  Created
// V8-26812 : A.Silva 
//  Corrected Field names.
// V8-27635 : L.Ineson
//  Corrected save method implementation.
// V8-28444 : M.Shelley
//  Modified the Groups property to use a private backing variable as the "private set" accessor 
//  causes the XmlSerializer class to fail in the Save method.
#endregion

#region Version History : CCM 801
// V8-28400 : A.Kuszyk
//  Amended FetchById to allow read only files to be opened.
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion

#region Version History: (CCM 810)
//  V8-29739 : M.Brumby
//      Initialised groupIds
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     A Validation Template dal item.
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateItem  : ISerializable
    {
        #region Fields

        private bool _isDirty;

        /// <summary>
        ///     Holds the file stream used to access this instance's data.
        /// </summary>
        /// <remarks>If the file was not locked it will be closed.</remarks>
        private FileStream _stream;

        List<ValidationTemplateGroupItem> _groups;

        private const Byte majorVersion = 1;
        private const Byte minorVersion = 1;

        #endregion

        #region Non Serializable Properties

        /// <summary>
        ///     Gets the name of the currently open stream.
        /// </summary>
        /// <remarks>New objects, or objects missing a stream do not have current stream name.</remarks>
        [XmlIgnore]
        internal String CurrentStreamName
        {
            get { return IsNew || _stream == null ? null : _stream.Name; }
        }

        /// <summary>
        ///     Gets the Id value for this instance.
        /// </summary>
        /// <remarks>The Id is the file path.</remarks>
        [XmlIgnore]
        internal String Id { get; private set; }

        /// <summary>
        ///     Gets whether this instance is considered dirty or not.
        /// </summary>
        [XmlIgnore]
        internal Boolean IsDirty
        {
            get { return _isDirty || Groups.Any(item => item.IsDirty); }
            private set { _isDirty = value; }
        }

        /// <summary>
        ///     Gets whether this instance is considered new or not.
        /// </summary>
        [XmlIgnore]
        private Boolean IsNew { get; set; }

        #endregion

        #region Properties

        public String Name { get; set; }
        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        //public List<ValidationTemplateGroupItem> Groups { get; private set; }
        public List<ValidationTemplateGroupItem> Groups
        {
            get { return _groups; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Private initial constructor. Performs any common initialization.
        /// </summary>
        private ValidationTemplateItem()
        {
            _groups = new List<ValidationTemplateGroupItem>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationTemplateItem" /> class from the given
        ///     <paramref name="dto" />.
        /// </summary>
        /// <param name="dto"><see cref="ValidationTemplateDto" /> containing the data to initialize the new instance.</param>
        public ValidationTemplateItem(ValidationTemplateDto dto) : this()
        {
            IsNew = true;

            UpdateFromDto(dto);
        }

        private ValidationTemplateItem(SerializationInfo info, StreamingContext context) : this()
        {
            SetObjectData(info);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fetches the <see cref="ValidationTemplateItem" /> from the file path in its <paramref name="id" />. Optionally
        ///     locks the file so it cannot be used by another process.
        /// </summary>
        /// <param name="id">File path to use when retrieving the <see cref="ValidationTemplateItem" />.</param>
        /// <param name="lockFile"><c>Optional</c> Whether to keep the file locked or not.</param>
        /// <returns>
        ///     A new instance of <see cref="ValidationTemplateItem" /> from the file path in the <paramref name="id" />, OR
        ///     <c>null</c> if there was any exception.
        /// </returns>
        public static ValidationTemplateItem FetchById(String id, Boolean lockFile = false)
        {
            ValidationTemplateItem item;

            try
            {
                // Check to see if the file is read-only first.
                var fileInfo = new FileInfo(id);
                FileAccess fileAccess;
                if (fileInfo.IsReadOnly)
                {
                    fileAccess = FileAccess.Read;
                }
                else
                {
                    fileAccess = FileAccess.ReadWrite;
                }
                // Open the file irrespective of its read-only status.
                FileStream fileStream = File.Open(id, FileMode.Open, fileAccess, FileShare.None);

                var xmlSerializer = new XmlSerializer(typeof (ValidationTemplateItem));

                item = (ValidationTemplateItem) xmlSerializer.Deserialize(fileStream);
                item._stream = fileStream;
                item.Id = id;

                //Set the group ids for all the objects so that they can be loaded
                //from the dal correctly.
                //group ids are internal to the validation template item so
                //can always start from 1
                Int32 groupId = 1;
                foreach (ValidationTemplateGroupItem group in item.Groups)
                {
                    group.Id = groupId;

                    foreach (ValidationTemplateGroupMetricItem metric in group.Metrics)
                    {
                        metric.ValidationTemplateGroupId = groupId;
                    }
                    groupId++;
                }

                // Close the stream if we don't need to lock the file, or if the file is read only.
                if (!lockFile || fileInfo.IsReadOnly) fileStream.Close();
            }
            catch (Exception)
            {
                return null; // do not return anyhing if there was an exception of any sort.
            }

            return item;
        }

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Get the filepath
            String filePath = null;
            if (String.IsNullOrEmpty(fixedDirectory))
            {
                filePath = Path.Combine(Path.GetDirectoryName(this.Id), Name);
            }
            else
            {
                filePath = Path.Combine(fixedDirectory, Name);
            }
            filePath = Path.ChangeExtension(filePath, Constants.ValidationTemplateFileExtension);
            this.Id = filePath;

            //delete the old file first
            Delete();

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //create the new file stream and serialize.
            _stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        ///     Deletes the underlying file for this <see cref="ValidationTemplateItem" />.
        /// </summary>
        public void Delete()
        {
            if (_stream == null) return;

            var itemPath = _stream.Name;
            Unlock();
            File.Delete(itemPath);
        }

        /// <summary>
        ///     Marks this instance as modified.
        /// </summary>
        internal void MarkDirty()
        {
            IsDirty = true;
        }

        internal void Unlock()
        {
            if (_stream == null) return;

            _stream.Close();
            _stream.Dispose();
            _stream = null;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Returns a <see cref="ValidationTemplateDto" /> with data loaded from this instance.
        /// </summary>
        /// <returns></returns>
        public ValidationTemplateDto GetDataTransferObject()
        {
            return new ValidationTemplateDto
            {
                Id = Id,
                Name = Name
            };
        }

        /// <summary>
        ///     Updates the properties of this instance from the data in the given <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">An instance of <see cref="ValidationTemplateDto" /> containing the data to update from.</param>
        /// <remarks>If there is a stream, the Id property will be recreated to agree with the current folder path.</remarks>
        internal void UpdateFromDto(ValidationTemplateDto dto)
        {
            MarkDirty();

            if (_stream != null) UpdateIdFromStream(dto, _stream);

            Id = (String) dto.Id;
            Name = dto.Name;
        }

        #endregion

        #region ISerializable Members

        /// <summary>
        ///     Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the
        ///     target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
        /// <param name="context">
        ///     The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this
        ///     serialization.
        /// </param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.ValidationTemplateName, Name);
            info.AddValue(FieldNames.ValidationTemplateMajorVersion, MajorVersion);
            info.AddValue(FieldNames.ValidationTemplateMinorVersion, MinorVersion);
            info.AddValue(FieldNames.ValidationTemplateGroups, Groups);
        }

        /// <summary>
        ///     Populates this instance with the values from the <see cref="SerializationInfo" />.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo" /> containing the data to populate this instance with.</param>
        private void SetObjectData(SerializationInfo info)
        {
            Name = info.GetString(FieldNames.ValidationTemplateName);
            MajorVersion = info.GetByte(FieldNames.ValidationTemplateMajorVersion);
            MinorVersion = info.GetByte(FieldNames.ValidationTemplateMinorVersion);
            _groups = (List<ValidationTemplateGroupItem>)info.GetValue(FieldNames.ValidationTemplateGroups, typeof(List<ValidationTemplateGroupItem>));
        }

        #endregion

        #region Private Static Helpers

        /// <summary>
        ///     Changes the name and/or extension of a file, keeping the folder path.
        /// </summary>
        /// <param name="filePath">Existing file path.</param>
        /// <param name="newFileName">New name of the file.</param>
        /// <param name="newFileExtension">New extension that the file will have.</param>
        /// <returns>
        ///     A <see cref="String" /> with the original <paramref name="filePath" /> but the new
        ///     <paramref name="newFileName" /> and/or <paramref name="newFileExtension" />.
        /// </returns>
        private static String ChangeFileName(String filePath, String newFileName = null, String newFileExtension = null)
        {
            if (String.IsNullOrEmpty(newFileName) && String.IsNullOrEmpty(newFileExtension)) return filePath;

            var newFilePath = Path.GetDirectoryName(filePath);
            if (String.IsNullOrEmpty(newFilePath)) return null;

            if (!String.IsNullOrEmpty(newFileName)) newFilePath = Path.Combine(newFilePath, newFileName);
            if (!String.IsNullOrEmpty(newFileExtension))
                newFilePath = Path.ChangeExtension(newFilePath, newFileExtension);

            return newFilePath;
        }

        /// <summary>
        ///     Updates the <paramref name="dto" />'s Id to have the correct path if there is a stream in this instance
        /// </summary>
        /// <param name="dto">An instance of <see cref="ValidationTemplateDto" /> that needs its Id updated.</param>
        /// <param name="fileStream"></param>
        private static void UpdateIdFromStream(ValidationTemplateDto dto, FileStream fileStream)
        {
            var folderPath = Path.GetDirectoryName(fileStream.Name) ?? String.Empty;
            var newFilename = dto.Name;
            var newfilePath = Path.Combine(folderPath, newFilename);
            dto.Id = Path.ChangeExtension(newfilePath, Constants.ValidationTemplateFileExtension);
        }

        #endregion
    }
}