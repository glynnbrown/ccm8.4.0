﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#region Version History: (CCM 810)
//  V8-29739 : M.Brumby
//      Changed Id to internal so it can be initialised
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     A Validation Template Group dal item.
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateGroupItem : ISerializable
    {
        #region Fields

        private List<ValidationTemplateGroupMetricItem> _metrics = new List<ValidationTemplateGroupMetricItem>();

        #endregion

        #region Non Serializable Properties

        [XmlIgnore]
        internal Int32 Id { get; set; }

        /// <summary>
        ///     Gets or sets whether this instance is dirty, or not.
        /// </summary>
        [XmlIgnore]
        internal bool IsDirty { get; private set; }

        [XmlIgnore]
        internal Object ValidationTemplateId { get; private set; }

        #endregion

        #region Properties

        public string Name { get; set; }

        public float Threshold1 { get; set; }

        public float Threshold2 { get; set; }

        public List<ValidationTemplateGroupMetricItem> Metrics
        {
            get { return _metrics; }
            set { _metrics = value; }
        }

        public PlanogramValidationType ValidationType { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Private initial constructor. Performs any common initialization.
        /// </summary>
        private ValidationTemplateGroupItem()
        {
            Metrics = new List<ValidationTemplateGroupMetricItem>();
        }

        /// <summary>
        ///     Initializes the new instance by loading the information from the provided <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">
        ///     Instance of <see cref="ValidationTemplateGroupDto" /> with the data used to initialize the new
        ///     instance.
        /// </param>
        public ValidationTemplateGroupItem(ValidationTemplateGroupDto dto) : this()
        {
            UpdateFromDto(dto);
        }

        private ValidationTemplateGroupItem(SerializationInfo info, StreamingContext context) : this()
        {
            SetObjectData(info);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this instance as modified.
        /// </summary>
        internal void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        ///     Populates this instance with the values from the <see cref="SerializationInfo" />.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo" /> containing the data to populate this instance with.</param>
        private void SetObjectData(SerializationInfo info)
        {
            Name = info.GetString(FieldNames.ValidationTemplateGroupName);
            Threshold1 = info.GetSingle(FieldNames.ValidationTemplateGroupThreshold1);
            Threshold2 = info.GetSingle(FieldNames.ValidationTemplateGroupThreshold2);
            ValidationType = (PlanogramValidationType) info.GetByte(FieldNames.ValidationTemplateGroupValidationType);

            Metrics = (List<ValidationTemplateGroupMetricItem>)info.GetValue(FieldNames.ValidationTemplateGroupMetrics, typeof(List<ValidationTemplateGroupMetricItem>));
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Creates and returns the <see cref="ValidationTemplateGroupDto" /> for this instance.
        /// </summary>
        /// <returns>A new instance of <see cref="ValidationTemplateGroupDto" /> loaded with this instance's properties.</returns>
        public ValidationTemplateGroupDto GetDataTransferObject()
        {
            return new ValidationTemplateGroupDto
            {
                Id = Id,
                ValidationTemplateId = ValidationTemplateId,
                Name = Name,
                Threshold1 = Threshold1,
                Threshold2 = Threshold2,
                ValidationType =  (Byte) ValidationType
            };
        }

        /// <summary>
        ///     Loads the data from teh provided <paramref name="dto" /> in this instance.
        /// </summary>
        /// <param name="dto">Instance of <see cref="ValidationTemplateGroupDto" /> with the data to be loaded in this instance.</param>
        internal void UpdateFromDto(ValidationTemplateGroupDto dto)
        {
            MarkDirty();

            Id = dto.Id;
            ValidationTemplateId = dto.ValidationTemplateId;
            Name = dto.Name;
            Threshold1 = dto.Threshold1;
            Threshold2 = dto.Threshold2;
            ValidationType = (PlanogramValidationType) dto.ValidationType;
        }

        #endregion

        #region ISerializable Members

        /// <summary>
        ///     Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the
        ///     target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
        /// <param name="context">
        ///     The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this
        ///     serialization.
        /// </param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.ValidationTemplateGroupName, Name);
            info.AddValue(FieldNames.ValidationTemplateGroupThreshold1, Threshold1);
            info.AddValue(FieldNames.ValidationTemplateGroupThreshold2, Threshold2);
            info.AddValue(FieldNames.ValidationTemplateGroupValidationType, (Byte) ValidationType);

            info.AddValue(FieldNames.ValidationTemplateGroupMetrics, Metrics);
        }

        #endregion
    }
}