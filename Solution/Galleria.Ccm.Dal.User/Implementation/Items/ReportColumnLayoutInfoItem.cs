﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.IO;
//using System.Runtime.Serialization;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Schema;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     DAL item for <see cref="ReportColumnLayoutInfoDto" />.
//    /// </summary>
//    [Serializable]
//    public sealed class ReportColumnLayoutInfoItem : ISerializable
//    {
//        #region Fields

//        /// <summary>
//        ///     Holds the <see cref="FileStream" /> to access the data for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        private FileStream _stream;

//        #endregion

//        #region Properties

//        /// <summary>
//        ///     Gets or sets the name for the current <see cref="FileStream" />.
//        /// </summary>
//        /// <remarks>If there is no current open <see cref="FileStream" />, it returns <c>null</c>.</remarks>
//        [XmlIgnore]
//        public String CurrentStreamName
//        {
//            get { return _stream != null ? _stream.Name : null; }
//        }

//        /// <summary>
//        ///     Gets or sets the unique id for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        /// <remarks>The Id is the file path.</remarks>
//        [XmlIgnore]
//        public String Id { get; private set; }

//        /// <summary>
//        ///     Gets or sets whether this <see cref="ReportColumnLayoutInfoItem" /> is dirty
//        /// </summary>
//        [XmlIgnore]
//        public Boolean IsDirty { get; private set; }

//        /// <summary>
//        ///     Gets or sets whether this <see cref="ReportColumnLayoutInfoItem" /> is new.
//        /// </summary>
//        [XmlIgnore]
//        public Boolean IsNew { get; private set; }

//        /// <summary>
//        ///     Gets or sets the type for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        public Byte Type { get; set; }

//        /// <summary>
//        ///     Gets or sets the name for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        public String Name { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        private ReportColumnLayoutInfoItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnLayoutInfoItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnLayoutInfoItem(ReportColumnLayoutInfoDto dto)
//        {
//            IsNew = true;

//            UpdateFromDto(dto);
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnLayoutInfoItem" /> from the given stream.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public ReportColumnLayoutInfoItem(SerializationInfo info, StreamingContext context)
//        {
//            SetObjectData(info, context);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Deletes the underlying file for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        public void Delete()
//        {
//            if (_stream == null) return;

//            var lastPath = _stream.Name;
//            Unlock();
//            File.Delete(lastPath);
//        }

//        /// <summary>
//        ///     Fetches the <see cref="ReportColumnLayoutInfoItem" /> from the underlying file, using its <paramref name="id" /> as
//        ///     the file path.
//        /// </summary>
//        /// <param name="id">
//        ///     Id of the <see cref="ReportColumnLayoutInfoItem" /> requested, and also the file path for its
//        ///     underlying file.
//        /// </param>
//        /// <returns>A new instance of <see cref="ReportColumnLayoutInfoItem" />, initialized from the information on the file.</returns>
//        internal static ReportColumnLayoutInfoItem FetchById(String id)
//        {
//            ReportColumnLayoutInfoItem dalObject = null;

//            try
//            {
//                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.None);

//                XmlSerializer x = new XmlSerializer(typeof(ReportColumnLayoutItem));

//                ReportColumnLayoutItem ReportColumnLayoutItem = (ReportColumnLayoutItem)x.Deserialize(stream);

//                if (ReportColumnLayoutItem != null)
//                {
//                    dalObject = new ReportColumnLayoutInfoItem();
//                    dalObject.Id = id;
//                    dalObject.Name = ReportColumnLayoutItem.Name;
//                    dalObject.Type = ReportColumnLayoutItem.Type;
//                }
//                stream.Close();
//            }
//            catch (Exception)
//            {
//                //TODO: handle?
//            }

//            return dalObject;
//        }

//        /// <summary>
//        ///     Generates the file path from the id (as directory), name and extension.
//        /// </summary>
//        /// <param name="currentFilePath">Existing file path.</param>
//        /// <param name="fileName">Name of the file, without the extension.</param>
//        /// <returns>A <see cref="String" /> with the full path generated from the instance's data.</returns>
//        private static string GenerateFilePath(String currentFilePath, String fileName)
//        {
//            return Path.ChangeExtension(
//                Path.Combine(
//                    Path.GetDirectoryName(currentFilePath),
//                    fileName),
//                Constants.ReportColumnLayoutFileExtension);
//        }

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnLayoutInfoItem" /> as dirty.
//        /// </summary>
//        public void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        /// <summary>
//        ///     Saves this <see cref="ReportColumnLayoutInfoItem" /> to a file, using the <see cref="Id" /> property as the file
//        ///     path.
//        /// </summary>
//        public void Save()
//        {
//            Id = GenerateFilePath(Id, Name);

//            //delete the old file first
//            Delete();

//            Directory.CreateDirectory(Path.GetDirectoryName(Id));

//            //create the new file stream and serialize.
//            _stream = File.Open(Id, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
//            var x = new XmlSerializer(GetType());
//            x.Serialize(_stream, this);

//            IsDirty = false;
//            IsNew = false;
//        }

//        /// <summary>
//        ///     Unlocks the source file for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        public void Unlock()
//        {
//            if (_stream == null) return;

//            _stream.Close();
//            _stream.Dispose();
//            _stream = null;
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnLayoutInfoDto" /> for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnLayoutInfoDto" />.</returns>
//        public ReportColumnLayoutInfoDto GetDataTransferObject()
//        {
//            return new ReportColumnLayoutInfoDto
//            {
//                Id = Id,
//                Type = Type,
//                Name = Name
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnLayoutInfoItem" /> from the given <see cref="ReportColumnLayoutInfoDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnLayoutInfoDto" /> used to initialize the
//        ///     <see cref="ReportColumnLayoutInfoItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnLayoutInfoDto dto)
//        {
//            MarkDirty();

//            // If there is a stream, update the file path value generated from it.
//            if (_stream != null) dto.Id = GenerateFilePath(_stream.Name, dto.Name);

//            Id = (String)dto.Id;
//            Type = dto.Type;
//            Name = dto.Name;
//        }

//        #endregion

//        #region ISerializable Support

//        /// <summary>
//        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            info.AddValue(FieldNames.ReportColumnLayoutType, Type);
//            info.AddValue(FieldNames.ReportColumnLayoutName, Name);
//        }

//        /// <summary>
//        ///     Populates this <see cref="ReportColumnLayoutInfoItem" /> from the given <see cref="SerializationInfo" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private void SetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            Name = info.GetString(FieldNames.ReportColumnLayoutName);
//            Type = info.GetByte(FieldNames.ReportColumnLayoutType);
//        }

//        #endregion
//    }
//}