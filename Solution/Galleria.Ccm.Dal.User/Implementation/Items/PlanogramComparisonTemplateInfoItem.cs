﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Planogram Comparison Template Info <c>DAL</c> item.
    /// </summary>
    [Serializable]
    public sealed class PlanogramComparisonTemplateInfoItem
    {
        #region Fields

        private String _id;

        #endregion

        #region Properties

        /// <summary>
        ///     Get/set the Id for this instance of <see cref="PlanogramComparisonTemplateInfoItem"/>.
        /// </summary>
        /// <remarks>The Id is the path to the file.</remarks>
        [XmlIgnoreAttribute]
        public String Id { get { return _id; } private set { _id = value; } }

        /// <summary>
        ///     Get/set the Name for this instance of <see cref="PlanogramComparisonTemplateInfoItem"/>.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        ///     Get/set the Description for this instance of <see cref="PlanogramComparisonTemplateInfoItem"/>.
        /// </summary>
        public String Description { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramComparisonTemplateInfoItem() {}

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramComparisonTemplateInfoItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the printTemplate item from the given file id.
        /// </summary>
        internal static PlanogramComparisonTemplateInfoItem FetchById(String id)
        {
            PlanogramComparisonTemplateInfoItem dalObject = null;

            try
            {
                using (FileStream stream = File.Open(id, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    var xmlSerializer = new XmlSerializer(typeof(PlanogramComparisonTemplateItem));
                    var item = (PlanogramComparisonTemplateItem)xmlSerializer.Deserialize(stream);

                    if (item != null)
                    {
                        dalObject = new PlanogramComparisonTemplateInfoItem
                                    {
                                        Id = id,
                                        Name = item.Name,
                                        Description = item.Description
                                    };
                    }

                    stream.Close();
                }
            }
            catch (Exception)
            {
                //TODO: handle?
            }

            return dalObject;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramComparisonTemplateInfoDto GetDataTransferObject()
        {
            return new PlanogramComparisonTemplateInfoDto
                   {
                       Id = Id,
                       Name = Name,
                       Description = Description
                   };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        ///     Populate this instance with data from the given <paramref name="info"/>.
        /// </summary>
        /// <param name="info">The data to be used when populating this instance.</param>
        /// <param name="context">[Not used]</param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.PlanogramComparisonTemplateName);
            Description = info.GetString(FieldNames.PlanogramComparisonTemplateDescription);
        }

        #endregion
    }
}