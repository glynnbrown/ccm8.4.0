﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.Runtime.Serialization;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Schema;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     Report column dal item.
//    /// </summary>
//    [Serializable]
//    public sealed class ReportColumnItem : ISerializable
//    {
//        #region Properties

//        [XmlIgnore]
//        public String ReportColumnLayoutId { get; set; }

//        [XmlIgnore]
//        public Boolean IsDirty { get; private set; }

//        public Int32 Order { get; set; }
//        public String Path { get; set; }
//        public Byte TotalType { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnItem" />.
//        /// </summary>
//        public ReportColumnItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnItem(ReportColumnDto dto)
//        {
//            UpdateFromDto(dto);
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnItem" /> from the given stream.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private ReportColumnItem(SerializationInfo info, StreamingContext context)
//        {
//            SetObjectData(info, context);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnItem" /> as dirty.
//        /// </summary>
//        private void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnDto" /> for this <see cref="ReportColumnItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnDto" />.</returns>
//        public ReportColumnDto GetDataTransferObject()
//        {
//            return new ReportColumnDto
//            {
//                ReportColumnLayoutId = ReportColumnLayoutId,
//                Path = Path,
//                Number = Order,
//                TotalType = TotalType
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnItem" /> from the given <see cref="ReportColumnDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnDto" /> used to initialize the
//        ///     <see cref="ReportColumnItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnDto dto)
//        {
//            MarkDirty();

//            ReportColumnLayoutId = Convert.ToString(dto.ReportColumnLayoutId);
//            Path = dto.Path;
//            Order = dto.Number;
//            TotalType = dto.TotalType;
//        }

//        #endregion

//        #region ISerializable Support

//        /// <summary>
//        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="ReportColumnItem" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            info.AddValue(FieldNames.ReportColumnPath, Path);
//            info.AddValue(FieldNames.ReportColumnOrder, Order);
//            info.AddValue(FieldNames.ReportColumnTotalType, TotalType);
//        }

//        /// <summary>
//        ///     Populates this <see cref="ReportColumnItem" /> from the given <see cref="SerializationInfo" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private void SetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            Path = info.GetString(FieldNames.ReportColumnPath);
//            Order = info.GetInt32(FieldNames.ReportColumnOrder);
//            TotalType = info.GetByte(FieldNames.ReportColumnTotalType);
//        }

//        #endregion
//    }
//}