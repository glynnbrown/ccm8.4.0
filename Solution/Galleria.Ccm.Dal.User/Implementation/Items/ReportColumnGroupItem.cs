﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.Runtime.Serialization;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Schema;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     Report column group dal item.
//    /// </summary>
//    [Serializable]
//    public sealed class ReportColumnGroupItem : ISerializable
//    {
//        #region Properties

//        [XmlIgnore]
//        public String ReportColumnLayoutId { get; set; }

//        public String Grouping { get; set; }

//        [XmlIgnore]
//        public Boolean IsDirty { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnGroupItem" />.
//        /// </summary>
//        public ReportColumnGroupItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnGroupItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnGroupItem(ReportColumnGroupDto dto)
//        {
//            UpdateFromDto(dto);
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnGroupItem" /> from the given stream.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private ReportColumnGroupItem(SerializationInfo info, StreamingContext context)
//        {
//            SetObjectData(info, context);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnGroupItem" /> as dirty.
//        /// </summary>
//        private void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnGroupDto" /> for this <see cref="ReportColumnGroupItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnGroupDto" />.</returns>
//        public ReportColumnGroupDto GetDataTransferObject()
//        {
//            return new ReportColumnGroupDto
//            {
//                ReportColumnLayoutId = ReportColumnLayoutId,
//                Grouping = Grouping
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnGroupItem" /> from the given <see cref="ReportColumnGroupDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnGroupDto" /> used to initialize the
//        ///     <see cref="ReportColumnGroupItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnGroupDto dto)
//        {
//            MarkDirty();

//            ReportColumnLayoutId = Convert.ToString(dto.ReportColumnLayoutId);
//            Grouping = dto.Grouping;
//        }

//        #endregion

//        #region ISerializable Support

//        /// <summary>
//        ///     Populates the <see cref="SerializationInfo" /> with values from this <see cref="ReportColumnGroupItem" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            //TODO Add the strings as constants in FieldNames.
//            info.AddValue(FieldNames.ReportColumnGroupGrouping, Grouping);
//        }

//        /// <summary>
//        ///     Populates this <see cref="ReportColumnGroupItem" /> from the given <see cref="SerializationInfo" />.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        private void SetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            //TODO Add the strings as constants in FieldNames.
//            Grouping = info.GetString(FieldNames.ReportColumnGroupGrouping);
//        }

//        #endregion
//    }
//}