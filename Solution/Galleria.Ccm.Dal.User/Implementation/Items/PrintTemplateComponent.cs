﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32340 : L.Ineson
//  Corrected IsDirty flag
#endregion

#endregion

using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplateComponent dal item
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentItem : IDalItem
    {
        #region Fields
        private List<PrintTemplateComponentDetailItem> _sections = new List<PrintTemplateComponentDetailItem>();
        private Boolean _isDirty;
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (ComponentDetails.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public Int32 PrintTemplateSectionId { get; set; }

        public Byte Type { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }

        public List<PrintTemplateComponentDetailItem> ComponentDetails
        {
            get { return _sections; }
            set { _sections = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PrintTemplateComponentItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PrintTemplateComponentItem(PrintTemplateComponentDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.PrintTemplateComponentType, this.Type);
            element.AddValue(FieldNames.PrintTemplateComponentX, this.X);
            element.AddValue(FieldNames.PrintTemplateComponentY, this.Y);
            element.AddValue(FieldNames.PrintTemplateComponentHeight, this.Height);
            element.AddValue(FieldNames.PrintTemplateComponentWidth, this.Width);

            element.AddValue(FieldNames.PrintTemplateComponentComponentDetails, this.ComponentDetails);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.Type = element.GetByte(FieldNames.PrintTemplateComponentType);
            this.X = element.GetSingle(FieldNames.PrintTemplateComponentX);
            this.Y = element.GetSingle(FieldNames.PrintTemplateComponentY);
            this.Height = element.GetSingle(FieldNames.PrintTemplateComponentHeight);
            this.Width = element.GetSingle(FieldNames.PrintTemplateComponentWidth);

            this.ComponentDetails = element.GetChildList<PrintTemplateComponentDetailItem>(FieldNames.PrintTemplateComponentComponentDetails);


            UpdateChildIdLinks();
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateComponentDto GetDataTransferObject()
        {
            return new PrintTemplateComponentDto()
            {
                Id = this.Id,
                PrintTemplateSectionId = this.PrintTemplateSectionId,
                Type = this.Type,
                X = this.X,
                Y = this.Y,
                Height = this.Height,
                Width = this.Width,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PrintTemplateComponentDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PrintTemplateSectionId = dto.PrintTemplateSectionId;
            this.Type = dto.Type;
            this.X = dto.X;
            this.Y = dto.Y;
            this.Height = dto.Height;
            this.Width = dto.Width;

            MarkDirty();
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            //update id on children
            foreach (var s in this.ComponentDetails)
            {
                s.PrintTemplateComponentId = this.Id;
            }
        }

        public PrintTemplateComponentDetailItem Insert(PrintTemplateComponentDetailDto dto)
        {
            dto.Id = PrintTemplateComponentDetailItem.GetNextId();
            PrintTemplateComponentDetailItem item = new PrintTemplateComponentDetailItem(dto);
            this.ComponentDetails.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion


    }
}
