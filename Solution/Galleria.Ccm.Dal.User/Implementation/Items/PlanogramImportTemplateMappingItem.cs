﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramImportTemplateMapping dal item.
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplateMappingItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public PlanogramImportTemplateMappingDtoKey DtoKey
        {
            get
            {
                return new PlanogramImportTemplateMappingDtoKey()
                    {
                        Field = this.Field,
                        PlanogramImportTemplateId = this.PlanogramImportTemplateId
                    };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String PlanogramImportTemplateId { get; set; }

        public Byte FieldType { get; set; }
        public String Field { get; set; }
        public String ExternalField { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramImportTemplateMappingItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramImportTemplateMappingItem(PlanogramImportTemplateMappingDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///  Creates a new instance of this type from the given
        ///  serialization context.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramImportTemplateMappingItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramImportTemplateMappingFieldType, this.FieldType);
            info.AddValue(FieldNames.PlanogramImportTemplateMappingField, this.Field);
            info.AddValue(FieldNames.PlanogramImportTemplateMappingExternalField, this.ExternalField);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            FieldType = info.GetByte(FieldNames.PlanogramImportTemplateMappingFieldType);
            Field = info.GetString(FieldNames.PlanogramImportTemplateMappingField);
            ExternalField = info.GetString(FieldNames.PlanogramImportTemplateMappingExternalField);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramImportTemplateMappingDto GetDataTransferObject()
        {
            return new PlanogramImportTemplateMappingDto()
            {
                Id = this.Id,
                PlanogramImportTemplateId = this.PlanogramImportTemplateId,
                FieldType = this.FieldType,
                Field = this.Field,
                ExternalField = this.ExternalField
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramImportTemplateMappingDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PlanogramImportTemplateId = Convert.ToString(dto.PlanogramImportTemplateId);
            this.FieldType = dto.FieldType;
            this.Field = dto.Field;
            this.ExternalField = dto.ExternalField;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
