﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-24863 L.Hodson
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Runtime.Serialization;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///  Our root item for saving down a list of system settings.
//    /// </summary>
//    [Serializable]
//    public sealed class SystemSettingList : ISerializable
//    {
//        #region Constants
//        const String _itemsListName = "Items";
//        #endregion

//        #region Fields
//        private List<SystemSettingItem> _items;
//        #endregion

//        #region Properties

//        /// <summary>
//        /// Returns the list of items held by this cache.
//        /// </summary>
//        public List<SystemSettingItem> Items
//        {
//            get { return _items; }
//            set { _items = value; }
//        }

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public SystemSettingList()
//        {
//            this.Items = new List<SystemSettingItem>();
//        }

//        /// <summary>
//        /// Creates a new instance of this type from the given serialisation context
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public SystemSettingList(SerializationInfo info, StreamingContext context)
//        {
//            this.Items = (List<SystemSettingItem>)info.GetValue(_itemsListName, typeof(List<SystemSettingItem>));
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        /// Populates the serialization context from this item.
//        /// </summary>
//        /// <param name="info"></param>
//        /// <param name="context"></param>
//        public void GetObjectData(SerializationInfo info, StreamingContext context)
//        {
//            info.AddValue(_itemsListName, this.Items);
//        }

//        #endregion
//    } 

//}
