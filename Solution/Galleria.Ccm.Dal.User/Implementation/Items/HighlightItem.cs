﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
// V8-25436 : L.Luong
//  Added lock to the FetchById
#endregion

#region Version History: (CCM 8.0.1)
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;
using System.Diagnostics;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// Highlight dal item.
    /// </summary>
    [Serializable]
    public sealed class HighlightItem : ISerializable
    {
        #region Fields

        private Boolean _isReadOnly;
        private Boolean _isNew;
        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<HighlightGroupItem> _groups = new List<HighlightGroupItem>();
        private List<HighlightFilterItem> _filters = new List<HighlightFilterItem>();
        private List<HighlightCharacteristicItem> _characteristics = new List<HighlightCharacteristicItem>();

        private const Byte majorVersion = 1;
        private const Byte minorVersion = 1;

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get { return _isReadOnly; }
            private set { _isReadOnly = value; }
        }

        /// <summary>
        /// Returns true if this is a new item.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew
        {
            get {return _isNew;}
            private set { _isNew = value; }
        }

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (Filters.Any(f => f.IsDirty)) return true;
                if (Groups.Any(f => f.IsDirty)) return true;
                if (Characteristics.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        /// <summary>
        /// Returns the name of the currently open stream.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get
            {
                String name = null;
                if (!this.IsNew && _stream != null)
                {
                    name = _stream.Name;
                }
                return name;
            }
        }

        /// <summary>
        /// Returns the Highlight Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        public String Name { get; set; }
        public Byte Type { get; set; }
        public Byte MethodType { get; set; }
        public Boolean IsFilterEnabled { get; set; }
        public Boolean IsPreFilter { get; set; }
        public Boolean IsAndFilter { get; set; }
        public String Field1 { get; set; }
        public String Field2 { get; set; }
        public Byte QuadrantXType { get; set; }
        public Single QuadrantXConstant { get; set; }
        public Byte QuadrantYType { get; set; }
        public Single QuadrantYConstant { get; set; }
        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        /// <summary>
        /// Returns the list of child groups
        /// </summary>
        public List<HighlightGroupItem> Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }

        /// <summary>
        /// Returns the list of child filters
        /// </summary>
        public List<HighlightFilterItem> Filters
        {
            get { return _filters; }
            set { _filters = value; }
        }

        /// <summary>
        /// Returns the list of child characteristics
        /// </summary>
        public List<HighlightCharacteristicItem> Characteristics
        {
            get { return _characteristics; }
            set { _characteristics = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public HighlightItem(HighlightDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the highlight item from the given file id.
        /// </summary>
        internal static HighlightItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            HighlightItem dalObject = null;
            XmlSerializer x = new XmlSerializer(typeof(HighlightItem));

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = (HighlightItem)x.Deserialize(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = (HighlightItem)x.Deserialize(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        /// <summary>
        /// Saves this item to the given folder
        /// </summary>
        /// <param name="folderPath"></param>
        public void Save(String fixedDirectory)
        {
            //Check that we are not in readonly mode.
            Debug.Assert(!this.IsReadOnly, "Trying to save when readonly");
            if (this.IsReadOnly) return;

            //Get the filepath
            String filePath = null;
            if (String.IsNullOrEmpty(fixedDirectory))
            {
                filePath = Path.Combine(Path.GetDirectoryName(this.Id), Name);
            }
            else
            {
                filePath = Path.Combine(fixedDirectory, Name);
            }

            filePath = Path.ChangeExtension(filePath, Constants.HighlightFileExtension);
            this.Id = filePath;
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;

            //delete the old file first
            Delete();

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //create the new file stream and serialize.
            _stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);

            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        /// Deletes this item.
        /// </summary>
        public void Delete()
        {
            if (_stream != null)
            {
                String lastPath = _stream.Name;
                Unlock();
                File.Delete(lastPath);
            }
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            this.IsDirty = true;
        }

        /// <summary>
        /// Unlocks the source file for this highlight.
        /// </summary>
        public void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        #region Child related methods

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (HighlightGroupItem grp in Groups)
            {
                grp.HighlightId = this.Id;
            }

            foreach (HighlightFilterItem filter in Filters)
            {
                filter.HighlightId = this.Id;
            }

            foreach (HighlightCharacteristicItem c in Characteristics)
            {
                c.HighlightId = this.Id;
            }
        }

        public HighlightGroupItem Insert(HighlightGroupDto dto)
        {
            dto.Id = HighlightGroupItem.GetNextId();
            HighlightGroupItem item = new HighlightGroupItem(dto);
            this.Groups.Add(item);
            MarkDirty();
            return item;
        }

        public HighlightFilterItem Insert(HighlightFilterDto dto)
        {
            dto.Id = HighlightFilterItem.GetNextId();
            HighlightFilterItem item = new HighlightFilterItem(dto);
            this.Filters.Add(item);
            MarkDirty();
            return item;
        }

        public HighlightCharacteristicItem Insert(HighlightCharacteristicDto dto)
        {
            dto.Id = HighlightCharacteristicItem.GetNextId();
            HighlightCharacteristicItem item = new HighlightCharacteristicItem(dto);
            this.Characteristics.Add(item);
            MarkDirty();
            return item;
        }

        #endregion


        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(HighlightDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //recreate the id
                String curPath = _stream.Name;
                String newFilename = dto.Name;

                String filePath = Path.Combine(Path.GetDirectoryName(curPath), newFilename);
                filePath = Path.ChangeExtension(filePath, Constants.HighlightFileExtension);

                Id = filePath;
                dto.Id = filePath;
            }
            else
            {
                //just load the id from the dto.
                Id = (String)dto.Id;
            }

            Name = dto.Name;
            Type = dto.Type;
            MethodType = dto.MethodType;
            IsFilterEnabled = dto.IsFilterEnabled;
            IsPreFilter = dto.IsPreFilter;
            IsAndFilter = dto.IsAndFilter;
            Field1 = dto.Field1;
            Field2 = dto.Field2;
            QuadrantXType = dto.QuadrantXType;
            QuadrantXConstant = dto.QuadrantXConstant;
            QuadrantYType = dto.QuadrantYType;
            QuadrantYConstant = dto.QuadrantYConstant;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightDto GetDataTransferObject()
        {
            return new HighlightDto()
            {
                Id = this.Id,
                Name = this.Name,
                Type = this.Type,
                MethodType = this.MethodType,
                IsFilterEnabled = this.IsFilterEnabled,
                IsPreFilter = this.IsPreFilter,
                IsAndFilter = this.IsAndFilter,
                Field1 = this.Field1,
                Field2 = this.Field2,
                QuadrantXType = this.QuadrantXType,
                QuadrantXConstant = this.QuadrantXConstant,
                QuadrantYType = this.QuadrantYType,
                QuadrantYConstant = this.QuadrantYConstant,
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightName, this.Name);
            info.AddValue(FieldNames.HighlightType, this.Type);
            info.AddValue(FieldNames.HighlightMethodType, this.MethodType);
            info.AddValue(FieldNames.HighlightIsFilterEnabled, this.IsFilterEnabled);
            info.AddValue(FieldNames.HighlightIsPreFilter, this.IsPreFilter);
            info.AddValue(FieldNames.HighlightIsAndFilter, this.IsAndFilter);
            info.AddValue(FieldNames.HighlightField1, this.Field1);
            info.AddValue(FieldNames.HighlightField2, this.Field2);
            info.AddValue(FieldNames.HighlightQuadrantXType, this.QuadrantXType);
            info.AddValue(FieldNames.HighlightQuadrantXConstant, this.QuadrantXConstant);
            info.AddValue(FieldNames.HighlightQuadrantYType, this.QuadrantYType);
            info.AddValue(FieldNames.HighlightQuadrantYConstant, this.QuadrantYConstant);
            info.AddValue(FieldNames.HighlightMajorVersion, this.MajorVersion);
            info.AddValue(FieldNames.HighlightMinorVersion, this.MinorVersion);

            info.AddValue(FieldNames.HighlightGroups, this.Groups);
            info.AddValue(FieldNames.HighlightFilters, this.Filters);
            info.AddValue(FieldNames.HighlightCharacteristics, this.Characteristics);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.HighlightName);
            Type = info.GetByte(FieldNames.HighlightType);
            MethodType = info.GetByte(FieldNames.HighlightMethodType);
            IsFilterEnabled = info.GetBoolean(FieldNames.HighlightIsFilterEnabled);
            IsPreFilter = info.GetBoolean(FieldNames.HighlightIsPreFilter);
            IsAndFilter = info.GetBoolean(FieldNames.HighlightIsAndFilter);
            Field1 = info.GetString(FieldNames.HighlightField1);
            Field2 = info.GetString(FieldNames.HighlightField2);
            QuadrantXType = info.GetByte(FieldNames.HighlightQuadrantXType);
            QuadrantXConstant = info.GetSingle(FieldNames.HighlightQuadrantXConstant);
            QuadrantYType = info.GetByte(FieldNames.HighlightQuadrantYType);
            QuadrantYConstant = info.GetSingle(FieldNames.HighlightQuadrantYConstant);
            MajorVersion = info.GetByte(FieldNames.HighlightMajorVersion);
            MinorVersion = info.GetByte(FieldNames.HighlightMinorVersion);

            Groups = (List<HighlightGroupItem>)info.GetValue(FieldNames.HighlightGroups, typeof(List<HighlightGroupItem>));
            Filters = (List<HighlightFilterItem>)info.GetValue(FieldNames.HighlightFilters, typeof(List<HighlightFilterItem>));
            Characteristics = (List<HighlightCharacteristicItem>)info.GetValue(FieldNames.HighlightCharacteristics, typeof(List<HighlightCharacteristicItem>));

            UpdateChildIdLinks();
        }

        #endregion
    }
}
