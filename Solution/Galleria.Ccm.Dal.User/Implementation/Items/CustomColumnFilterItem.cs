#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property and tidied up.
// V8-26472 : A.Probyn
//      Changed properties to be public for xml serializer to output.

#endregion

#region Version History: (CCM 820)
//V8-30870 : L.Ineson 
//  Added IDalItem implementation.
#endregion

#region Version History: (CCM 8.3)
//V8-31541 : L.Ineson
//  Added Id
#endregion
#endregion

using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Custom column filter dal item.
    /// </summary>
    [Serializable]
    public sealed class CustomColumnFilterItem : IDalItem
    {
        #region Properties

        [XmlIgnore]
        public Boolean IsDirty { get; set; }

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get;
            set;
        }

        [XmlIgnoreAttribute]
        public CustomColumnFilterDtoKey DtoKey
        {
            get
            {
                return new CustomColumnFilterDtoKey()
                {
                    Path = this.Path
                };
            }
        }

        [XmlIgnore]
        public String CustomColumnLayoutId { get; set; }

        private String Header { get; set; }
        public String Path { get;  set; }
        public String Text { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnFilterItem" />.
        /// </summary>
        public CustomColumnFilterItem() 
        {
            Id = GetNextId();
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnFilterItem" /> from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public CustomColumnFilterItem(CustomColumnFilterDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Marks this <see cref="CustomColumnFilterItem" /> as dirty.
        /// </summary>
        private void MarkDirty()
        {
            IsDirty = true;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Returns a <see cref="CustomColumnFilterDto" /> for this <see cref="CustomColumnFilterItem" />.
        /// </summary>
        /// <returns>A new instance of a <see cref="CustomColumnFilterDto" />.</returns>
        public CustomColumnFilterDto GetDataTransferObject()
        {
            return new CustomColumnFilterDto
            {
                Id = this.Id,
                CustomColumnLayoutId = CustomColumnLayoutId,
                Text = Text,
                Path = Path,
                Header = Header
            };
        }

        /// <summary>
        ///     Updates this <see cref="CustomColumnFilterItem" /> from the given <see cref="CustomColumnFilterDto" />.
        /// </summary>
        /// <param name="dto">
        ///     The <see cref="CustomColumnFilterDto" /> used to initialize the
        ///     <see cref="CustomColumnFilterItem" />.
        /// </param>
        public void UpdateFromDto(CustomColumnFilterDto dto)
        {
            this.Id = dto.Id;
            CustomColumnLayoutId = Convert.ToString(dto.CustomColumnLayoutId);
            Text = dto.Text;
            Path = dto.Path;
            Header = dto.Header;

            MarkDirty();
        }

        #endregion

        #region IDalItem Support

        /// <summary>
        /// Populates the given element from this item.
        /// </summary>
        public void GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.CustomColumnFilterText, Text);
            element.AddValue(FieldNames.CustomColumnFilterPath, Path);
            element.AddValue(FieldNames.CustomColumnFilterHeader, Header);
        }

        /// <summary>
        /// Populates this item from the given XElement.
        /// </summary>
        public void SetObjectData(XElement element)
        {
            Text = element.GetString(FieldNames.CustomColumnFilterText);
            Path = element.GetString(FieldNames.CustomColumnFilterPath);
            Header = element.GetString(FieldNames.CustomColumnFilterHeader);
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}