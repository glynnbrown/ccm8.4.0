﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// HighlightCharacteristic dal item
    /// </summary>
    [Serializable]
    public sealed class HighlightCharacteristicItem : ISerializable
    {
        #region Fields

        private Boolean _isDirty;
        private Int32 _id;
        private List<HighlightCharacteristicRuleItem> _rules = new List<HighlightCharacteristicRuleItem>();

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if(_isDirty)return true;
                if(this.Rules.Any(r=> r.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        [XmlIgnoreAttribute]
        public HighlightCharacteristicDtoKey DtoKey
        {
            get
            {
                return new HighlightCharacteristicDtoKey()
                {
                    HighlightId = this.HighlightId,
                    Name = this.Name
                };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        [XmlIgnoreAttribute]
        public String HighlightId { get; set; }

        public String Name { get; set; }

        public Boolean IsAndFilter { get; set; }

        public List<HighlightCharacteristicRuleItem> Rules
        {
            get { return _rules; }
            set { _rules = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightCharacteristicItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public HighlightCharacteristicItem(HighlightCharacteristicDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightCharacteristicItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightCharacteristicName, this.Name);
            info.AddValue(FieldNames.HighlightCharacteristicIsAndFilter, this.IsAndFilter);

            info.AddValue(FieldNames.HighlightCharacteristicRules, this.Rules);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.HighlightCharacteristicName);
            IsAndFilter = info.GetBoolean(FieldNames.HighlightCharacteristicIsAndFilter);
            Rules = (List<HighlightCharacteristicRuleItem>)info.GetValue(FieldNames.HighlightCharacteristicRules, typeof(List<HighlightCharacteristicRuleItem>));

            UpdateChildIdLinks();
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightCharacteristicDto GetDataTransferObject()
        {
            return new HighlightCharacteristicDto()
            {
                Id = this.Id,
                HighlightId = this.HighlightId,
                Name = this.Name,
                IsAndFilter = this.IsAndFilter,

            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(HighlightCharacteristicDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.HighlightId = Convert.ToString(dto.HighlightId);
            this.Name = dto.Name;
            this.IsAndFilter = dto.IsAndFilter;

            MarkDirty();
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            //update id on children
            foreach (HighlightCharacteristicRuleItem r in Rules)
            {
                r.HighlightCharacteristicId = this.Id;
            }
        }

        public HighlightCharacteristicRuleItem Insert(HighlightCharacteristicRuleDto dto)
        {
            dto.Id = HighlightCharacteristicRuleItem.GetNextId();
            HighlightCharacteristicRuleItem item = new HighlightCharacteristicRuleItem(dto);
            this.Rules.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
