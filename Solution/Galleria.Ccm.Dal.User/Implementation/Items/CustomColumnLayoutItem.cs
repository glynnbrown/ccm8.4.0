#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Tidy up.
// V8-26671 : A.Silva ~ Added code to facilitate Unit Testing.
// V8-26844 : A.Silva ~ Corrected reference to CustomColumnLayout file extension constant.
// V8-27938 : N.Haywood
//  Added Data Sheets
// V8-26322 : A.Silva
//      Amended Save so that it will not care whether we are testing or not (it will use the same code for both).

#endregion

#region Version History: (CCM 8.0.1)
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties
#endregion

#region Version History: (CCM 8.2)
//V8-30870 : L.Ineson
//  Added IXmlSerializable implementation and the ability
// to upgrade files.
#endregion

#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added more properties
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///     Custom column layout dal item.
    /// </summary>
    [Serializable]
    public sealed class CustomColumnLayoutItem : IDalItem, IXmlSerializable
    {
        #region Fields

        private String _id;
        private FileStream _stream;

        private Boolean _isDirty;

        private List<CustomColumnItem> _columns = new List<CustomColumnItem>();
        private List<CustomColumnGroupItem> _columnGroups = new List<CustomColumnGroupItem>();
        private List<CustomColumnSortItem> _columnSorts = new List<CustomColumnSortItem>();
        private List<CustomColumnFilterItem> _columnFilters = new List<CustomColumnFilterItem>();

        #endregion

        #region Properties

        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        [XmlIgnoreAttribute]
        public Boolean IsReadOnly
        {
            get;
            private set;
        }

        /// <summary>
        ///     Gets or sets whether this <see cref="CustomColumnLayoutItem" /> is new.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew 
        {
            get; 
            private set;
        }

        /// <summary>
        ///     Gets or sets whether this <see cref="CustomColumnLayoutItem" /> is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                return _isDirty ||
                       (Columns != null && Columns.Any(item => item.IsDirty)) ||
                       (ColumnGroups != null && ColumnGroups.Any(item => item.IsDirty)) ||
                       (ColumnSorts != null && ColumnSorts.Any(item => item.IsDirty)) ||
                       (ColumnFilters != null && ColumnFilters.Any(item => item.IsDirty));
            }
            private set { _isDirty = value; }
        }

        /// <summary>
        ///     Gets or sets the name for the current <see cref="FileStream" />.
        /// </summary>
        /// <remarks>If there is no current open <see cref="FileStream" />, it returns <c>null</c>.</remarks>
        [XmlIgnoreAttribute]
        public String CurrentStreamName
        {
            get { return IsNew || _stream == null ? null : _stream.Name; }
        }

        /// <summary>
        ///     Gets or sets the unique id for this <see cref="CustomColumnLayoutItem" />.
        /// </summary>
        /// <remarks>The Id is the file path.</remarks>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        public String Description { get; set; }
        public Boolean HasTotals { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }
        public Byte FrozenColumnCount { get; set; }
        public Boolean IsTitleShown { get; set; }
        public Boolean IsGroupDetailHidden { get; set; }
        public Boolean IsGroupBlankRowShown { get; set; }
        public Byte DataOrderType { get; set; }
        public Boolean IncludeAllPlanogams { get; set; }
        
        public List<CustomColumnFilterItem> ColumnFilters
        {
            get { return _columnFilters; }
            set { _columnFilters = value; }
        }

        public List<CustomColumnGroupItem> ColumnGroups
        {
            get { return _columnGroups; }
            set { _columnGroups = value; }
        }

        public List<CustomColumnSortItem> ColumnSorts
        {
            get { return _columnSorts; }
            set { _columnSorts = value; }
        }

        public List<CustomColumnItem> Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnLayoutItem" />.
        /// </summary>
        public CustomColumnLayoutItem()
        {
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnLayoutItem" /> from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public CustomColumnLayoutItem(CustomColumnLayoutDto dto)
        {
            this.IsNew = true;
            UpdateFromDto(dto);

            //set version numbers to the latest available.
            Byte major, minor;
            DalItemHelper.GetLatestVersionNumber(GetScriptsType(), out major, out minor);
            this.MajorVersion = major;
            this.MinorVersion = minor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the item from the given file id.
        /// </summary>
        internal static CustomColumnLayoutItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            CustomColumnLayoutItem dalObject = null;

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    dalObject = DalItemHelper.Deserialize<CustomColumnLayoutItem>(stream);
                    dalObject.Id = id;
                    dalObject.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                dalObject = DalItemHelper.Deserialize<CustomColumnLayoutItem>(stream);
                dalObject.Id = id;

                //keep the original stream open so that the file is locked.
                dalObject._stream = stream;
            }

            return dalObject;
        }

        /// <summary>
        ///     Saves this <see cref="CustomColumnLayoutItem" /> to a file, using the <see cref="Id" /> property as the file
        ///     path.
        /// </summary>
        /// <exception cref="Exception">Thrown when the directory path cannot be extracted from the <see cref="Id" /> property.</exception>
        public void Save()
        {
            //Check that we are not in readonly mode.
            Debug.Assert(!this.IsReadOnly, "Trying to save when readonly");
            if (this.IsReadOnly) return;

            //Get the filepath
            String ext = ((Galleria.Ccm.Model.CustomColumnLayoutType)Type != Model.CustomColumnLayoutType.DataSheet) ?
                 Constants.CustomColumnLayoutFileExtension : Constants.DataSheetFileExtension;

            this.Id = Path.ChangeExtension(Path.Combine(Path.GetDirectoryName(this.Id), this.Name), ext);
                

            // Delete the old file before saving the new one.
            Delete();

            //Make sure the directory exists.
            Directory.CreateDirectory(Path.GetDirectoryName(Path.GetDirectoryName(Id)));


            //create the new file stream and serialize.
            _stream = File.Open(Id, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            XmlSerializer x = new XmlSerializer(this.GetType());
            x.Serialize(_stream, this);


            //mark clean
            this.IsDirty = false;
            this.IsNew = false;
        }

        /// <summary>
        ///     Deletes the underlying file for this <see cref="CustomColumnLayoutItem" />.
        /// </summary>
        public void Delete()
        {
            if (_stream == null) return;

            String lastPath = _stream.Name;
            Unlock();
            File.Delete(lastPath);
        }

        /// <summary>
        ///     Marks this <see cref="CustomColumnLayoutItem" /> as dirty.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        ///     Unlocks the source file for this <see cref="CustomColumnLayoutInfoItem" />.
        /// </summary>
        public void Unlock()
        {
            if (_stream == null) return; // No source to unlock.

            _stream.Close();
            _stream.Dispose();
            _stream = null;
        }

        #region Child related methods

        private void UpdateChildIdLinks()
        {
            if (this.Columns != null) this.Columns.ForEach(item => item.CustomColumnLayoutId = _id);
            if (this.ColumnGroups != null) this.ColumnGroups.ForEach(item => item.CustomColumnLayoutId = _id);
            if (this.ColumnSorts != null) this.ColumnSorts.ForEach(item => item.CustomColumnLayoutId = _id);
            if (this.ColumnFilters != null) this.ColumnFilters.ForEach(item => item.CustomColumnLayoutId = _id);
        }

        public CustomColumnItem Insert(CustomColumnDto dto)
        {
            dto.Id = CustomColumnItem.GetNextId();
            CustomColumnItem item = new CustomColumnItem(dto);
            item.CustomColumnLayoutId = this.Id;
            this.Columns.Add(item);
            MarkDirty();
            return item;
        }

        public CustomColumnGroupItem Insert(CustomColumnGroupDto dto)
        {
            dto.Id = CustomColumnGroupItem.GetNextId();
            CustomColumnGroupItem item = new CustomColumnGroupItem(dto);
            item.CustomColumnLayoutId = this.Id;
            this.ColumnGroups.Add(item);
            MarkDirty();
            return item;
        }

        public CustomColumnSortItem Insert(CustomColumnSortDto dto)
        {
            dto.Id = CustomColumnSortItem.GetNextId();
            CustomColumnSortItem item = new CustomColumnSortItem(dto);
            item.CustomColumnLayoutId = this.Id;
            this.ColumnSorts.Add(item);
            MarkDirty();
            return item;
        }

        public CustomColumnFilterItem Insert(CustomColumnFilterDto dto)
        {
            dto.Id = CustomColumnFilterItem.GetNextId();
            CustomColumnFilterItem item = new CustomColumnFilterItem(dto);
            item.CustomColumnLayoutId = this.Id;
            this.ColumnFilters.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a new dto from this item.
        /// </summary>
        public CustomColumnLayoutDto GetDataTransferObject()
        {
            return new CustomColumnLayoutDto
            {
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                Type = this.Type,
                HasTotals = this.HasTotals,
                FrozenColumnCount = this.FrozenColumnCount,
                IsGroupBlankRowShown = this.IsGroupBlankRowShown,
                IsGroupDetailHidden = this.IsGroupDetailHidden,
                IsTitleShown = this.IsTitleShown,
                DataOrderType = this.DataOrderType,
                IncludeAllPlanograms = this.IncludeAllPlanogams,
            };
        }

        /// <summary>
        /// Updates this item from the given dto.
        /// </summary>
        public void UpdateFromDto(CustomColumnLayoutDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                String ext = ((Galleria.Ccm.Model.CustomColumnLayoutType)Type != Model.CustomColumnLayoutType.DataSheet) ?
                 Constants.CustomColumnLayoutFileExtension : Constants.DataSheetFileExtension;

                dto.Id = Path.ChangeExtension(Path.Combine(Path.GetDirectoryName(_stream.Name), dto.Name), ext);
            }

            Id = (String)dto.Id;
            Name = dto.Name;
            Description = dto.Description;
            Type = dto.Type;
            HasTotals = dto.HasTotals;
            FrozenColumnCount = dto.FrozenColumnCount;
            IsTitleShown = dto.IsTitleShown;
            IsGroupDetailHidden = dto.IsGroupDetailHidden;
            IsGroupBlankRowShown = dto.IsGroupBlankRowShown;
            DataOrderType = dto.DataOrderType;
            IncludeAllPlanogams = dto.IncludeAllPlanograms;

        }

        #endregion

        #region IDalItem

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        public void GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.MajorVersion, this.MajorVersion);
            element.AddValue(FieldNames.MinorVersion, this.MinorVersion);
            
            element.AddValue(FieldNames.CustomColumnLayoutName, this.Name);
            element.AddValue(FieldNames.CustomColumnLayoutDescription, this.Description);
            element.AddValue(FieldNames.CustomColumnLayoutType, this.Type);
            element.AddValue(FieldNames.CustomColumnLayoutHasTotals, this.HasTotals);
            element.AddValue(FieldNames.CustomColumnLayoutFrozenColumnCount, this.FrozenColumnCount);
            element.AddValue(FieldNames.CustomColumnLayoutIsTitleShown, this.IsTitleShown);
            element.AddValue(FieldNames.CustomColumnLayoutIsGroupDetailHidden, this.IsGroupDetailHidden);
            element.AddValue(FieldNames.CustomColumnLayoutIsGroupBlankRowShown, this.IsGroupBlankRowShown);
            element.AddValue(FieldNames.CustomColumnLayoutDataOrderType, this.DataOrderType);
            element.AddValue(FieldNames.CustomColumnLayoutIncludeAllPlanograms, this.IncludeAllPlanogams);

            element.AddValue(FieldNames.CustomColumnLayoutColumns, this.Columns);
            element.AddValue(FieldNames.CustomColumnLayoutColumnGroups, this.ColumnGroups);
            element.AddValue(FieldNames.CustomColumnLayoutColumnFilters, this.ColumnFilters);
            element.AddValue(FieldNames.CustomColumnLayoutColumnSorts, this.ColumnSorts);
        }

        /// <summary>
        /// Populates this item from the given xml element.
        /// </summary>
        public void SetObjectData(XElement element)
        {
            this.MajorVersion = element.GetByte(FieldNames.MajorVersion);
            this.MinorVersion = element.GetByte(FieldNames.MinorVersion);

            this.Name = element.GetString(FieldNames.CustomColumnLayoutName);
            this.Description = element.GetString(FieldNames.CustomColumnLayoutDescription);
            this.Type = element.GetByte(FieldNames.CustomColumnLayoutType);
            this.HasTotals = element.GetBoolean(FieldNames.CustomColumnLayoutHasTotals);
            this.FrozenColumnCount = element.GetByte(FieldNames.CustomColumnLayoutFrozenColumnCount);
            this.IsTitleShown = element.GetBoolean(FieldNames.CustomColumnLayoutIsTitleShown);
            this.IsGroupDetailHidden = element.GetBoolean(FieldNames.CustomColumnLayoutIsGroupDetailHidden);
            this.IsGroupBlankRowShown = element.GetBoolean(FieldNames.CustomColumnLayoutIsGroupBlankRowShown);
            this.DataOrderType = element.GetByte(FieldNames.CustomColumnLayoutDataOrderType);
            this.IncludeAllPlanogams = element.GetBoolean(FieldNames.CustomColumnLayoutIncludeAllPlanograms);

            this.Columns = element.GetChildList<CustomColumnItem>(FieldNames.CustomColumnLayoutColumns);
            this.ColumnGroups = element.GetChildList<CustomColumnGroupItem>(FieldNames.CustomColumnLayoutColumnGroups);
            this.ColumnFilters = element.GetChildList<CustomColumnFilterItem>(FieldNames.CustomColumnLayoutColumnFilters);
            this.ColumnSorts = element.GetChildList<CustomColumnSortItem>(FieldNames.CustomColumnLayoutColumnSorts);
        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            //read the xelement
            var x = XElement.ReadFrom(reader) as XElement;

            //Perform the upgrade
            DalItemHelper.Upgrade(GetScriptsType(), x);

            //Load
            SetObjectData(x);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XElement rootElement = new XElement(this.GetType().Name);
            GetObjectData(rootElement);

            //don't write from root element otherwise we end up with double.
            foreach (XElement element in rootElement.Elements())
            {
                element.WriteTo(writer);
            }
        }

        private Type GetScriptsType()
        {
            return typeof(Scripts.CustomColumnLayoutScripts);
        }

        #endregion
    }

}