﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24863 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// SystemSetting dal item
    /// </summary>
    [Serializable]
    public sealed class UserEditorSettingItem : ISerializable
    {
        #region Fields

        private Int32 _id;

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String Key { get; set; }
        public String Value { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public UserEditorSettingItem() { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public UserEditorSettingItem(UserEditorSettingDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserEditorSettingItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(UserEditorSettingDto dto)
        {
            Id = (Int32)dto.Id;
            Key = dto.Key;
            Value = dto.Value;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public UserEditorSettingDto GetDataTransferObject()
        {
            return new UserEditorSettingDto()
            {
                Id = this.Id,
                Key = this.Key,
                Value = this.Value
            };
        }

        #endregion

        #region XML Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingKey, this.Key);
            info.AddValue(FieldNames.SystemSettingValue, this.Value);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Key = info.GetString(FieldNames.SystemSettingKey);
            Value = info.GetString(FieldNames.SystemSettingValue);
        }

        #endregion
    }
}
