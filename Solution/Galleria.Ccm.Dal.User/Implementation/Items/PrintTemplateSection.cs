﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32340 : L.Ineson
//  Corrected IsDirty flag
#endregion
#endregion

using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;
using System.Xml.Linq;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplateSection dal item
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionItem : IDalItem
    {
        #region Fields
        private List<PrintTemplateComponentItem> _components = new List<PrintTemplateComponentItem>();
        private Boolean _isDirty;
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this item is dirty
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty
        {
            get
            {
                if (_isDirty) return true;
                if (Components.Any(f => f.IsDirty)) return true;

                return false;
            }
            private set
            {
                _isDirty = value;
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public Int32 PrintTemplateSectionGroupId { get; set; }

        public Byte Number { get; set; }
        public Byte Orientation { get; set; }

        public List<PrintTemplateComponentItem> Components
        {
            get { return _components; }
            set { _components = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PrintTemplateSectionItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PrintTemplateSectionItem(PrintTemplateSectionDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.PrintTemplateSectionNumber, this.Number);
            element.AddValue(FieldNames.PrintTemplateSectionOrientation, this.Orientation);

            element.AddValue(FieldNames.PrintTemplateSectionComponents, this.Components);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.Number = element.GetByte(FieldNames.PrintTemplateSectionNumber);
            this.Orientation = element.GetByte(FieldNames.PrintTemplateSectionOrientation);

            this.Components = element.GetChildList<PrintTemplateComponentItem>(FieldNames.PrintTemplateSectionComponents);
 
            UpdateChildIdLinks();
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateSectionDto GetDataTransferObject()
        {
            return new PrintTemplateSectionDto()
            {
                Id = this.Id,
                PrintTemplateSectionGroupId = this.PrintTemplateSectionGroupId,
                Number = this.Number,
                Orientation = this.Orientation,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PrintTemplateSectionDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PrintTemplateSectionGroupId = dto.PrintTemplateSectionGroupId;
            this.Number = dto.Number;
            this.Orientation = dto.Orientation;

            MarkDirty();
        }

        /// <summary>
        /// Mark this item as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        /// Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            //update id on children
            foreach (var s in this.Components)
            {
                s.PrintTemplateSectionId = this.Id;
            }
        }

        public PrintTemplateComponentItem Insert(PrintTemplateComponentDto dto)
        {
            dto.Id = PrintTemplateComponentItem.GetNextId();
            PrintTemplateComponentItem item = new PrintTemplateComponentItem(dto);
            this.Components.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
