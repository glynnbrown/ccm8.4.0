﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-25559 : L.Ineson
//		Created
// V8-24700 : A.Silva ~ Added ColumnLayouts setting.
#endregion

#region Version History: (CCM 8.0.3)
// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
#endregion

#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// UserSystemSetting dal item
    /// </summary>
    [Serializable]
    public sealed class UserSystemSettingItem : ISerializable
    {
        #region Fields
        private List<ConnectionItem> _connections = new List<ConnectionItem>();
        private List<ColumnLayoutSettingItem> _columnLayoutSettings = new List<ColumnLayoutSettingItem>();

        #endregion

        #region Properties

        public String DisplayLanguage { get; set; }
        public Boolean DisplayCEIPWindow { get; set; }
        public Boolean SendCEIPInformation { get; set; }
        public Boolean IsDatabaseSelectionRemembered { get; set; }
        public Boolean IsEntitySelectionRemembered { get; set; }
        public PlanAssignmentViewType PlanAssignmentViewBy { get; set; }

        /// <summary>
        /// Returns the list of connections.
        /// </summary>
        public List<ConnectionItem> Connections
        {
            get { return _connections; }
            set { _connections = value; }
        }

        /// <summary>
        ///     Gets or sets the Column Layout settings per Screen Key.
        /// </summary>
        public List<ColumnLayoutSettingItem> ColumnLayoutSettings
        {
            get { return _columnLayoutSettings; }
            set { _columnLayoutSettings = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public UserSystemSettingItem() 
        { 
            //set defaults
            this.DisplayLanguage = "en-gb";
            this.SendCEIPInformation = true;
            this.IsDatabaseSelectionRemembered = false;
            this.IsEntitySelectionRemembered = false;
            this.DisplayCEIPWindow = true;
            this.PlanAssignmentViewBy = PlanAssignmentViewType.ViewCategory;
        }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public UserSystemSettingItem(UserSystemSettingDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserSystemSettingItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(UserSystemSettingDto dto)
        {
            DisplayLanguage = dto.DisplayLanguage;
            DisplayCEIPWindow = dto.DisplayCEIPWindow;
            SendCEIPInformation = dto.SendCEIPInformation;
            IsDatabaseSelectionRemembered = dto.IsDatabaseSelectionRemembered;
            IsEntitySelectionRemembered = dto.IsEntitySelectionRemembered;
            PlanAssignmentViewBy = dto.PlanAssignmentViewBy;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public UserSystemSettingDto GetDataTransferObject()
        {
            return new UserSystemSettingDto()
            {
                DisplayLanguage = this.DisplayLanguage,
                DisplayCEIPWindow = this.DisplayCEIPWindow,
                SendCEIPInformation = this.SendCEIPInformation,
                IsDatabaseSelectionRemembered = this.IsDatabaseSelectionRemembered,
                IsEntitySelectionRemembered = this.IsEntitySelectionRemembered,
                PlanAssignmentViewBy = this.PlanAssignmentViewBy,
            };
        }

        #endregion

        #region XML Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.UserSystemSettingDisplayLanguage, this.DisplayLanguage);
            info.AddValue(FieldNames.UserSystemSettingDisplayCEIPWindow, this.DisplayCEIPWindow);
            info.AddValue(FieldNames.UserSystemSettingSendCEIPInformationDefault, this.SendCEIPInformation);
            info.AddValue(FieldNames.UserSystemSettingIsDatabaseSelectionRemembered, this.IsDatabaseSelectionRemembered);
            info.AddValue(FieldNames.UserSystemSettingIsEntitySelectionRemembered, this.IsEntitySelectionRemembered);
            info.AddValue(FieldNames.UserSystemSettingColumnLayouts, ColumnLayoutSettings);

            Connections = (List<ConnectionItem>)info.GetValue(FieldNames.UserSystemSettingConnections, typeof(List<ConnectionItem>));
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.DisplayLanguage = info.GetString(FieldNames.UserSystemSettingDisplayLanguage);
            this.DisplayCEIPWindow = info.GetBoolean(FieldNames.UserSystemSettingDisplayCEIPWindow);
            this.SendCEIPInformation = info.GetBoolean(FieldNames.UserSystemSettingSendCEIPInformationDefault);
            this.IsDatabaseSelectionRemembered = info.GetBoolean(FieldNames.UserSystemSettingIsDatabaseSelectionRemembered);
            this.IsEntitySelectionRemembered = info.GetBoolean(FieldNames.UserSystemSettingIsEntitySelectionRemembered);

            this.Connections = (List<ConnectionItem>)info.GetValue(FieldNames.UserSystemSettingConnections, typeof(List<ConnectionItem>));
            ColumnLayoutSettings = (List<ColumnLayoutSettingItem>)info.GetValue((FieldNames.UserSystemSettingColumnLayouts), typeof(List<ColumnLayoutSettingItem>));
        }

        #endregion
    }
}
