﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24863 L.Hodson
//  Created
#endregion
#region Version History: CCM 820
// V8-30791: D.Pleasance
//      Added Colors
#endregion
#region Version History CCM830
// V8-31699 : A.Heathcote
//      Added RecentLabel, Highlight and Datasheet to Constants, Fields, Properties, Constructor and methods regions.
// V8-31934 : A.Heathcote
//      Added SelectedColumnListName (Refactoring name ASAP)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    ///  Our root item for saving down a list of system settings.
    /// </summary>
    [Serializable]
    public sealed class UserEditorSettingList : ISerializable
    {
        #region Constants
        const String _itemsListName = "Items";
        const String _colorsListName = "Colors";
        const String _recentLabelListName = "RecentLabel";
        const String _recentHighlightListName = "RecentHighlight";
        const String _recentDatasheetListName = "RecentDatasheet";
        const String _selectedColumnListName = "SelectedColumn";
        #endregion

        #region Fields
        private List<UserEditorSettingItem> _items;
        private List<UserEditorSettingColorItem> _colors;
        private List<UserEditorSettingsRecentLabelItem> _recentLabel;
        private List<UserEditorSettingsRecentHighlightItem> _recentHighlight;
        private List<UserEditorSettingsRecentDatasheetItem> _recentDatasheet;
        private List<UserEditorSettingsSelectedColumnItem> _selectedColumn;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of items held by this cache.
        /// </summary>
        public List<UserEditorSettingItem> Items
        {
            get { return _items; }
            set { _items = value; }
        }

        /// <summary>
        /// Returns the list of items held by this cache.
        /// </summary>
        public List<UserEditorSettingColorItem> Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }

        public List<UserEditorSettingsRecentLabelItem> RecentLabel
        {
            get { return _recentLabel; }
            set { _recentLabel = value; }
        }

        public List<UserEditorSettingsRecentHighlightItem> RecentHighlight
        {
            get { return _recentHighlight; }
            set { _recentHighlight = value; }
        }

        public List<UserEditorSettingsRecentDatasheetItem> RecentDatasheet
        {
            get { return _recentDatasheet; }
            set { _recentDatasheet = value; }
        }

        public List<UserEditorSettingsSelectedColumnItem> SelectedColumn
        {
            get { return _selectedColumn; }
            set { _selectedColumn = value; }
        }
        #endregion


        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public UserEditorSettingList()
        {
            this.Items = new List<UserEditorSettingItem>();
            this.Colors = new List<UserEditorSettingColorItem>();
            this.RecentLabel = new List<UserEditorSettingsRecentLabelItem>();
            this.RecentHighlight = new List<UserEditorSettingsRecentHighlightItem>();
            this.RecentDatasheet = new List<UserEditorSettingsRecentDatasheetItem>();
            this.SelectedColumn = new List<UserEditorSettingsSelectedColumnItem>();
        }

        /// <summary>
        /// Creates a new instance of this type from the given serialisation context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserEditorSettingList(SerializationInfo info, StreamingContext context)
        {
            this.Items = (List<UserEditorSettingItem>)info.GetValue(_itemsListName, typeof(List<UserEditorSettingItem>));
            this.Colors = (List<UserEditorSettingColorItem>)info.GetValue(_colorsListName, typeof(List<UserEditorSettingColorItem>));
            this.RecentLabel = (List<UserEditorSettingsRecentLabelItem>)info.GetValue(_recentLabelListName, typeof(List<UserEditorSettingsRecentLabelItem>));
            this.RecentHighlight = (List<UserEditorSettingsRecentHighlightItem>)info.GetValue(_recentHighlightListName, typeof(List<UserEditorSettingsRecentHighlightItem>));
            this.RecentDatasheet = (List<UserEditorSettingsRecentDatasheetItem>)info.GetValue(_recentDatasheetListName, typeof(List<UserEditorSettingsRecentDatasheetItem>));
            this.SelectedColumn = (List<UserEditorSettingsSelectedColumnItem>)info.GetValue(_selectedColumnListName, typeof(List<UserEditorSettingsSelectedColumnItem>));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serialization context from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(_itemsListName, this.Items);
            info.AddValue(_colorsListName, this.Colors);
            info.AddValue(_recentLabelListName, this.RecentLabel);
            info.AddValue(_recentHighlightListName, this.RecentHighlight);
            info.AddValue(_recentDatasheetListName, this.RecentDatasheet);
            info.AddValue(_selectedColumnListName, this.SelectedColumn);
        }

        #endregion
    } 

}
