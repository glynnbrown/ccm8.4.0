﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM 830
// V8-31699 : A.Heathcote
//     Created   
#endregion

#endregion
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    [Serializable]
    public sealed class UserEditorSettingsRecentHighlightItem : ISerializable
    {
        #region Fields

        private Int32 _id;
        private Object _highlightId;
        private Byte _saveType;

        #endregion

        #region Properties
        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Object HighlightId
        {
            get { return _highlightId; }
            set { _highlightId = value; }

        }

        public Byte SaveType
        {
            get { return _saveType; }
            set { _saveType = value; }
        }

        #endregion

        #region Constructor
        public UserEditorSettingsRecentHighlightItem() { }


        public UserEditorSettingsRecentHighlightItem(UserEditorSettingsRecentHighlightDto dto)
        {
            UpdateFromDto(dto);
        }


        public UserEditorSettingsRecentHighlightItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }
        #endregion

        #region Data Transfer Object


        public void UpdateFromDto(UserEditorSettingsRecentHighlightDto dto)
        {
            Id = dto.Id;
            HighlightId = dto.HighlightId;
            SaveType = dto.SaveType;
        }

        public UserEditorSettingsRecentHighlightDto GetDataTransferObject()
        {
            return new UserEditorSettingsRecentHighlightDto()
            {
                Id = this.Id,
                HighlightId = this.HighlightId,
                SaveType = this.SaveType
            };
        }
        #endregion

        #region XML Object
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingRecentHighlightId, this.HighlightId);
            info.AddValue(FieldNames.SystemSettingRecentHighlightSaveType, this.SaveType);
        }

        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            SaveType = info.GetByte(FieldNames.SystemSettingRecentHighlightSaveType);

            if (SaveType.Equals((Byte)HighlightDalFactoryType.FileSystem))
            {
                HighlightId = info.GetString(FieldNames.SystemSettingRecentHighlightId);
            }
            else if(SaveType.Equals((Byte)HighlightDalFactoryType.Unknown))
            {
                HighlightId = info.GetInt32(FieldNames.SystemSettingRecentHighlightId);
            } 
            else 
            {
                HighlightId = new Object();
            }
        }
        #endregion
    }
}
