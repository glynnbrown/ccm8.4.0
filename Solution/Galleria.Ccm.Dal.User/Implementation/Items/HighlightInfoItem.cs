﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
// V8-28362 : L.Ineson
//  Amended to only require read access.
//  Removed edit related methods and properties as this is always readonly.
#endregion
#endregion

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// HighlightInfo dal item.
    /// </summary>
    [Serializable]
    public sealed class HighlightInfoItem : ISerializable 
    {
        #region Fields

        private String _id;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the Highlight Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set  { _id = value; }
        }

        public String Name { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightInfoItem()
        { }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightInfoItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the highlight item from the given file id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static HighlightInfoItem FetchById(String id)
        {
            HighlightInfoItem dalObject = null;

            try
            {
                using (FileStream stream = File.Open(id, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    XmlSerializer x = new XmlSerializer(typeof(HighlightItem));
                    HighlightItem highlightItem = (HighlightItem)x.Deserialize(stream);
                    
                    if (highlightItem != null)
                    {
                        dalObject = new HighlightInfoItem();
                        dalObject.Id = id;
                        dalObject.Name = highlightItem.Name;
                    }

                    stream.Close();
                }
            }
            catch (Exception)
            {
                //TODO: handle?
            }

            return dalObject;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightInfoDto GetDataTransferObject()
        {
            return new HighlightInfoDto()
            {
                Id = this.Id,
                Name = this.Name,
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightName, this.Name);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.Name = info.GetString(FieldNames.HighlightName);
        }

        #endregion
    }
}
