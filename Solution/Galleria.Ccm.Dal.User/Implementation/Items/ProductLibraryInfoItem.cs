﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (V8.3)
// V8-32361 : L.Ineson
//  Added Name.
#endregion
#endregion

using System;
using System.IO;
using Galleria.Ccm.Dal.DataTransferObjects;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// ProductLibraryInfo Dal Item
    /// </summary>
    internal class ProductLibraryInfoItem : ProductLibraryInfoDto
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ProductLibraryInfoItem() { }

        /// <summary>
        /// Creates a new ProductLibraryInfoItem and populates its properties with values taken from a ProductLibraryItem.
        /// </summary>
        public ProductLibraryInfoItem(ProductLibraryItem item)
        {
            Id = item.Id;
            Name = item.Name;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Return a ProductLibraryInfoItem for the given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static ProductLibraryInfoItem FetchById(String id)
        {
            ProductLibraryInfoItem dalObject = null;

            dalObject = new ProductLibraryInfoItem();
            dalObject.Id = id;
            dalObject.Name = Path.GetFileName(id);

            //try
            //{
            //    using (FileStream stream = File.Open(id, FileMode.Open, FileAccess.Read, FileShare.None))
            //    {
            //        XmlSerializer x = new XmlSerializer(typeof(PrintTemplateItem));
            //        PrintTemplateItem printTemplateItem = (PrintTemplateItem)x.Deserialize(stream);

            //        if (printTemplateItem != null)
            //        {
            //            dalObject = new ProductLibraryInfoItem();
            //            dalObject.Id = id;
            //            dalObject.Name = printTemplateItem.Name;
            //        }

            //        stream.Close();
            //    }
            //}
            //catch (Exception)
            //{
            //    throw new DataPortalException();
            //}

            return dalObject;

        }

        #endregion

        #region DataTransferObject

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public ProductLibraryInfoDto GetDataTransferObject()
        {
            return new ProductLibraryInfoDto()
            {
                Id = this.Id,
                Name = this.Name
            };
        }

        #endregion
    }
}
