﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{   [Serializable]
    public sealed class UserEditorSettingsSelectedColumnItem : ISerializable
    {
        #region Fields

        private Int32 _id;
        private String _fieldPlaceHolder;
        private Byte _columnType;

        #endregion

        #region Properties 
        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String FieldPlaceHolder
        {
            get { return _fieldPlaceHolder; }
            set { _fieldPlaceHolder = value; }
        }
        
        public Byte ColumnType
        {
            get { return _columnType; }
            set { _columnType = value; }
        }

        #endregion

        #region Constructor 
        

        public UserEditorSettingsSelectedColumnItem() { }

        public UserEditorSettingsSelectedColumnItem(UserEditorSettingsSelectedColumnDto dto)
        {
            UpdateFromDto(dto);
        }

       /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserEditorSettingsSelectedColumnItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        public void UpdateFromDto(UserEditorSettingsSelectedColumnDto dto)
        {
            Id = dto.Id;
            FieldPlaceHolder = dto.FieldPlaceHolder;
            ColumnType = dto.ColumnType;
        } 


        public UserEditorSettingsSelectedColumnDto GetDataTransferObject()
        {
            return new UserEditorSettingsSelectedColumnDto()
            {
                Id = this.Id,
                FieldPlaceHolder = this.FieldPlaceHolder,
                ColumnType = this.ColumnType,

            };
        }
        #endregion

        #region XML Object 
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingSelectedColumnFieldPlaceHolder, this.FieldPlaceHolder);
            info.AddValue(FieldNames.SystemSettingSelectedColumnColumnType, this.ColumnType);
        }


        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            FieldPlaceHolder = info.GetString(FieldNames.SystemSettingSelectedColumnFieldPlaceHolder);
            ColumnType = info.GetByte(FieldNames.SystemSettingSelectedColumnColumnType);        }


        #endregion
    }
}
