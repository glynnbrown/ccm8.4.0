﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 L.Hodson
//  Created
#endregion

#region Version History : CCM 802
// V8-27433 : A.Kuszyk
//  Added Path property.
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// RecentPlanogram dal item
    /// </summary>
    [Serializable]
    public sealed class RecentPlanogramItem : ISerializable
    {
        #region Fields

        private Boolean _isDirty;
        private Int32 _id;

        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty {get; private set;}

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value;}
        }

        public String Name { get; set; }
        public String Path { get; set; }
        public Byte Type { get; set; }
        public DateTime DateLastAccessed { get; set; }
        public Boolean IsPinned { get; set; }
        public String Location { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public RecentPlanogramItem()
        { }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public RecentPlanogramItem(RecentPlanogramDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public RecentPlanogramItem(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.RecentPlanogramName);
            Path = info.GetString(FieldNames.RecentPlanogramPath);
            Type = info.GetByte(FieldNames.RecentPlanogramType);
            DateLastAccessed = info.GetDateTime(FieldNames.RecentPlanogramDateLastAccessed);
            IsPinned = info.GetBoolean(FieldNames.RecentPlanogramIsPinned);
            Location = info.GetString(FieldNames.RecentPlanogramLocation);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(RecentPlanogramDto dto)
        {
            _isDirty = true;

            Id = (Int32)dto.Id;
            Name = dto.Name;
            Path = dto.Path; 
            Type = dto.Type;
            DateLastAccessed = dto.DateLastAccessed;
            IsPinned = dto.IsPinned;
            Location = dto.Location;
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public RecentPlanogramDto GetDataTransferObject()
        {
            return new RecentPlanogramDto()
            {
                Id = this.Id,
                Name = this.Name,
                Path = this.Path,
                Type = this.Type,
                DateLastAccessed = this.DateLastAccessed,
                IsPinned = this.IsPinned,
                Location = this.Location,
            };
        }

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.RecentPlanogramName, this.Name);
            info.AddValue(FieldNames.RecentPlanogramPath, this.Path);
            info.AddValue(FieldNames.RecentPlanogramType, this.Type);
            info.AddValue(FieldNames.RecentPlanogramDateLastAccessed, this.DateLastAccessed);
            info.AddValue(FieldNames.RecentPlanogramIsPinned, this.IsPinned);
            info.AddValue(FieldNames.RecentPlanogramLocation, this.Location);
        }

        #endregion
    }
}
