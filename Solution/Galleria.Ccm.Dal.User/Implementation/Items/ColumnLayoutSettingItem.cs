﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// ColumnLayoutSetting dal item
    /// </summary>
    [Serializable]
    public sealed class ColumnLayoutSettingItem : ISerializable
    {
        #region Properties

        [XmlIgnore]
        public int Id { get; set; }

        public String ScreenKey { get; set; }
        public String ColumnLayoutName { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Creates a new instance of <see cref="ColumnLayoutSettingItem"/>.
        /// </summary>
        public ColumnLayoutSettingItem() { }

        /// <summary>
        ///     Creates a new instance of <see cref="ColumnLayoutSettingItem"/> and initializes it from the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="ColumnLayoutSettingDto"/> instance containing the data to initialize this instance.</param>
        public ColumnLayoutSettingItem(ColumnLayoutSettingDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///     Creates a new instance of <see cref="ColumnLayoutSettingItem"/> and initializes it from the provided serialized <paramref name="info"/>.
        /// </summary>
        /// <param name="info">The serialized data used to initialize this instance.</param>
        /// <param name="context"></param>
        public ColumnLayoutSettingItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Updates this instance with the data in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The instance of <see cref="ColumnLayoutSettingDto"/> containing the data to initialize this instance with.</param>
        public void UpdateFromDto(ColumnLayoutSettingDto dto)
        {
            Id = dto.Id;
            ScreenKey = dto.ScreenKey;
            ColumnLayoutName = dto.ColumnLayoutName;
        }

        /// <summary>
        ///     Creates and initializes a new instance of a <see cref="ColumnLayoutSettingDto"/> containing this instance's data.
        /// </summary>
        /// <returns>Returns a new instance of <see cref="ColumnLayoutSettingDto"/> with this instance's data.</returns>
        public ColumnLayoutSettingDto GetDataTransferObject()
        {
            return new ColumnLayoutSettingDto()
            {
                Id = Id,
                ScreenKey = ScreenKey,
                ColumnLayoutName = ColumnLayoutName
            };
        }

        #endregion

        #region XML Object

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.ColumnLayoutSettingScreenkey, ScreenKey);
            info.AddValue(FieldNames.ColumnLayoutSettingColumnLayoutName, ColumnLayoutName);
        }

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            ScreenKey = info.GetString(FieldNames.ColumnLayoutSettingScreenkey);
            ColumnLayoutName = info.GetString(FieldNames.ColumnLayoutSettingColumnLayoutName);
        }

        #endregion

        #region Id Helpers

        private static Int32 _nextId = 1;

        public static Int32 GetNextId()
        {
            var returnVal = _nextId;
            _nextId++;
            return returnVal;
        }

        public static void ResetId()
        {
            _nextId = 1;
        }

        #endregion
    }
}
