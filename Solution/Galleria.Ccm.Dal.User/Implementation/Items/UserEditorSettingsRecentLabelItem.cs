﻿#region Header Information
//Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote 
//  Created.
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    [Serializable]
    public sealed class UserEditorSettingsRecentLabelItem : ISerializable
    {
        #region Fields

        private Int32 _id;
        private String _name;
        private Byte _labelType;
        private Byte _saveType;
        private Object _labelId;
        #endregion

        #region Properties

        [XmlIgnoreAttribute]
        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Byte LabelType
        {
           get { return _labelType; }
           set { _labelType = value;}
        }

        public Byte SaveType
        {
            get { return _saveType; }
            set { _saveType = value;}
        }

        public Object LabelID
        {
            get { return _labelId; }
            set { _labelId = value;}
        }

        #endregion

        #region Constructor

        public UserEditorSettingsRecentLabelItem() { }

        public UserEditorSettingsRecentLabelItem(UserEditorSettingsRecentLabelDto dto)
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public UserEditorSettingsRecentLabelItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(UserEditorSettingsRecentLabelDto dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            LabelType =dto.LabelType;
            SaveType = dto.SaveType;
            LabelID = dto.LabelId;
        }

        public UserEditorSettingsRecentLabelDto GetDataTransferObject()
        {
            return new UserEditorSettingsRecentLabelDto()
            {
                Id = this.Id,   
                Name = this.Name,
                LabelType = this.LabelType,
                SaveType = this.SaveType,
                LabelId = this.LabelID,
            };
        }

        #endregion

        #region XML Object

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.SystemSettingRecentLabelName, this.Name);
            info.AddValue(FieldNames.SystemSettingRecentLabelType, this.LabelType);
            info.AddValue(FieldNames.SystemSettingRecentLabelSaveType, this.SaveType);
            info.AddValue(FieldNames.SystemSettingRecentLabelId, this.LabelID);
        }

        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.SystemSettingRecentLabelName);
            LabelType = info.GetByte(FieldNames.SystemSettingRecentLabelType);
            SaveType = info.GetByte(FieldNames.SystemSettingRecentLabelSaveType);

            //Checks the SaveType to determine which Type to bring back e.g. FileSystem brings back a String
            if (SaveType == (Byte)LabelDalFactoryType.FileSystem)
            {
                LabelID = info.GetString(FieldNames.SystemSettingRecentLabelId);
            }
            else if (SaveType == (Byte)LabelDalFactoryType.Unknown)
            {
                LabelID = info.GetInt32(FieldNames.SystemSettingRecentLabelId);
            }
            else 
            {
                LabelID = new Object();
            }
        }

        #endregion



    }
}
