﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplateInfo dal item.
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateInfoItem
    {
        #region Fields

        private String _id;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the PrintTemplate Id.
        /// NB- this is also the file path.
        /// </summary>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        public String Name { get; set; }
        public String Description { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PrintTemplateInfoItem()
        { }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PrintTemplateInfoItem(SerializationInfo info, StreamingContext context)
        {
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the printTemplate item from the given file id.
        /// </summary>
        internal static PrintTemplateInfoItem FetchById(String id)
        {
            PrintTemplateInfoItem dalObject = null;

            try
            {
                using (FileStream stream = File.Open(id, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    XmlSerializer x = new XmlSerializer(typeof(PrintTemplateItem));
                    PrintTemplateItem printTemplateItem = (PrintTemplateItem)x.Deserialize(stream);

                    if (printTemplateItem != null)
                    {
                        dalObject = new PrintTemplateInfoItem();
                        dalObject.Id = id;
                        dalObject.Name = printTemplateItem.Name;
                    }

                    stream.Close();
                }
            }
            catch (Exception)
            {
                //TODO: handle?
            }

            return dalObject;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateInfoDto GetDataTransferObject()
        {
            return new PrintTemplateInfoDto()
            {
                Id = this.Id,
                Name = this.Name,
                Description = this.Description
            };
        }

        #endregion

        #region Serialization Object

        /// <summary>
        /// Populates this item from the given context
        /// </summary>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.Name = info.GetString(FieldNames.PrintTemplateName);
            this.Description = info.GetString(FieldNames.PrintTemplateDescription);
        }

        #endregion
    }
}
