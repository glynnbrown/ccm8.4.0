﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// HighlightCharacteristicRule dal item
    /// </summary>
    [Serializable]
    public sealed class HighlightCharacteristicRuleItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public Int32 HighlightCharacteristicId { get; set; }

        public String Field { get; set; }
        public Byte Type { get; set; }
        public String Value { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public HighlightCharacteristicRuleItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public HighlightCharacteristicRuleItem(HighlightCharacteristicRuleDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        /// Creates a new instance from the given stream.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightCharacteristicRuleItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightCharacteristicRuleField, this.Field);
            info.AddValue(FieldNames.HighlightCharacteristicRuleType, this.Type);
            info.AddValue(FieldNames.HighlightCharacteristicRuleValue, this.Value);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Field = info.GetString(FieldNames.HighlightCharacteristicRuleField);
            Type = info.GetByte(FieldNames.HighlightCharacteristicRuleType);
            Value = info.GetString(FieldNames.HighlightCharacteristicRuleValue);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightCharacteristicRuleDto GetDataTransferObject()
        {
            return new HighlightCharacteristicRuleDto()
            {
                Id = this.Id,
                HighlightCharacteristicId = this.HighlightCharacteristicId,
                Field = this.Field,
                Type = this.Type,
                Value = this.Value,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(HighlightCharacteristicRuleDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.HighlightCharacteristicId = (Int32)dto.HighlightCharacteristicId;
            this.Field = dto.Field;
            this.Type = dto.Type;
            this.Value = dto.Value;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
