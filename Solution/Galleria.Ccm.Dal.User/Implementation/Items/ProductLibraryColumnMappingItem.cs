﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (V8.3)
// V8-32361 : L.Ineson
//  Updated to IDalItem
#endregion
#endregion


using System;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// Product Library Column mapping dal item
    /// </summary>
    [Serializable]
    public sealed class ProductLibraryColumnMappingItem : IDalItem
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String ProductLibraryId { get; set; }

        public String Source { get; set; }
        public String Destination { get; set; }
        public Boolean IsMandatory { get; set; }
        public String Type { get; set; }
        public Single Min { get; set; }
        public Single Max { get; set; }
        public Boolean CanDefault { get; set; }
        public String Default { get; set; }
        public Int32 TextFixedWidthColumnStart { get; set; }
        public Int32 TextFixedWidthColumnEnd { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProductLibraryColumnMappingItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public ProductLibraryColumnMappingItem(ProductLibraryColumnMappingDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.ProductLibraryColumnMappingSource, this.Source);
            element.AddValue(FieldNames.ProductLibraryColumnMappingDestination, this.Destination);
            element.AddValue(FieldNames.ProductLibraryColumnMappingIsMandatory, this.IsMandatory);
            element.AddValue(FieldNames.ProductLibraryColumnMappingType, this.Type);
            element.AddValue(FieldNames.ProductLibraryColumnMappingMin, this.Min);
            element.AddValue(FieldNames.ProductLibraryColumnMappingMax, this.Max);
            element.AddValue(FieldNames.ProductLibraryColumnMappingCanDefault, this.CanDefault);
            element.AddValue(FieldNames.ProductLibraryColumnMappingDefault, this.Default);
            element.AddValue(FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnStart, this.TextFixedWidthColumnStart);
            element.AddValue(FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnEnd, this.TextFixedWidthColumnEnd);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            this.Id = GetNextId();
            this.Source = element.GetString(FieldNames.ProductLibraryColumnMappingSource);
            this.Destination = element.GetString(FieldNames.ProductLibraryColumnMappingDestination);
            this.IsMandatory = element.GetBoolean(FieldNames.ProductLibraryColumnMappingIsMandatory);
            this.Type = element.GetString(FieldNames.ProductLibraryColumnMappingType);
            this.Min = element.GetSingle(FieldNames.ProductLibraryColumnMappingMin);
            this.Max = element.GetSingle(FieldNames.ProductLibraryColumnMappingMax);
            this.CanDefault = element.GetBoolean(FieldNames.ProductLibraryColumnMappingCanDefault);
            this.Default = element.GetString(FieldNames.ProductLibraryColumnMappingDefault);
            this.TextFixedWidthColumnStart = element.GetInt32(FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnStart);
            this.TextFixedWidthColumnEnd = element.GetInt32(FieldNames.ProductLibraryColumnMappingTextFixedWidthColumnEnd);
        }


        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public ProductLibraryColumnMappingDto GetDataTransferObject()
        {
            return new ProductLibraryColumnMappingDto()
            {
                Id = this.Id,
                ProductLibraryId = this.ProductLibraryId,
                Source = this.Source,
                Destination = this.Destination,
                IsMandatory = this.IsMandatory,
                Type = this.Type,
                Min = this.Min,
                Max = this.Max,
                CanDefault = this.CanDefault,
                Default = this.Default,
                TextFixedWidthColumnStart = this.TextFixedWidthColumnStart,
                TextFixedWidthColumnEnd = this.TextFixedWidthColumnEnd
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(ProductLibraryColumnMappingDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.ProductLibraryId = Convert.ToString(dto.ProductLibraryId);
            this.Source = Convert.ToString(dto.Source);
            this.Destination = Convert.ToString(dto.Destination);
            this.IsMandatory = Convert.ToBoolean(dto.IsMandatory);
            this.Type = Convert.ToString(dto.Type);
            this.Min = Convert.ToSingle(dto.Min);
            this.Max = Convert.ToSingle(dto.Max);
            this.CanDefault = Convert.ToBoolean(dto.CanDefault);
            this.Default = Convert.ToString(dto.Default);
            this.TextFixedWidthColumnStart = Convert.ToInt32(dto.TextFixedWidthColumnStart);
            this.TextFixedWidthColumnEnd = Convert.ToInt32(dto.TextFixedWidthColumnEnd);
                 
            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
