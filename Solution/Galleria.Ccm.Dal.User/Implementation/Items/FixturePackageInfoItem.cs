﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25873 : A.Probyn
//      Added new FixturePackage CustomAttribute1-5
#endregion
#endregion

using System;
using Galleria.Framework.IO;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    public interface IParentDtoItem<DTO> : GalleriaBinaryFile.IItem
    {
        Int32 Id { get; set; }

        DTO CopyDto();
    }

    internal class FixturePackageInfoItem : FixturePackageInfoDto, IParentDtoItem<FixturePackageInfoDto>, GalleriaBinaryFile.IItem
    {
        #region Constructors

        /// <summary>
        /// Creates a new FixturePackageDtoItem and populates its properties with values taken from a 
        /// GalleriaBinaryFile.IItem.
        /// </summary>
        public FixturePackageInfoItem(GalleriaBinaryFile.IItem item, String folderName)
        {
            Id = FileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageInfoId));
            FolderId = folderName;
            Name = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoName);
            Height = FileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageInfoHeight));
            Width = FileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageInfoWidth));
            Depth = FileItemHelper.GetSingle(item.GetItemPropertyValue(FieldNames.FixturePackageInfoDepth));
            FixtureCount = FileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageInfoFixtureCount));
            AssemblyCount = FileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageInfoAssemblyCount));
            ComponentCount = FileItemHelper.GetInt32(item.GetItemPropertyValue(FieldNames.FixturePackageInfoComponentCount));
            Description = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoDescription);
            ThumbnailImageData = (Byte[])item.GetItemPropertyValue(FieldNames.FixturePackageInfoThumbnailImageData);
            CustomAttribute1 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoCustomAttribute1);
            CustomAttribute2 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoCustomAttribute2);
            CustomAttribute3 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoCustomAttribute3);
            CustomAttribute4 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoCustomAttribute4);
            CustomAttribute5 = (String)item.GetItemPropertyValue(FieldNames.FixturePackageInfoCustomAttribute5);
        }

        /// <summary>
        /// Creates a new FixturePackageDtoItem and populates its properties with values taken from a FixturePackageDto.
        /// </summary>
        public FixturePackageInfoItem(FixturePackageInfoDto dto)
        {
            Id = dto.Id;
            FolderId = dto.FolderId;
            Name = dto.Name;
            Height = dto.Height;
            Width = dto.Width;
            Depth = dto.Depth;
            FixtureCount = dto.FixtureCount;
            AssemblyCount = dto.AssemblyCount;
            ComponentCount = dto.ComponentCount;
            Description = dto.Description;
            ThumbnailImageData = dto.ThumbnailImageData;
            CustomAttribute1 = dto.CustomAttribute1;
            CustomAttribute2 = dto.CustomAttribute2;
            CustomAttribute3 = dto.CustomAttribute3;
            CustomAttribute4 = dto.CustomAttribute4;
            CustomAttribute5 = dto.CustomAttribute5;
        }

        #endregion

        #region Public Properties

        Int32 IParentDtoItem<FixturePackageInfoDto>.Id
        {
            get { return (Int32)Id; }
            set { Id = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns a new DTO with property values identical to this instance.
        /// </summary>
        public FixturePackageInfoDto CopyDto()
        {
            return new FixturePackageInfoDto()
            {
                Id = this.Id,
                FolderId = this.FolderId,
                Name = this.Name,
                Description = this.Description,
                Height = this.Height,
                Width = this.Width,
                Depth = this.Depth,
                FixtureCount= this.FixtureCount,
                AssemblyCount = this.AssemblyCount,
                ComponentCount = this.ComponentCount,
                ThumbnailImageData = this.ThumbnailImageData,
                CustomAttribute1 = this.CustomAttribute1,
                CustomAttribute2 = this.CustomAttribute2,
                CustomAttribute3 = this.CustomAttribute3,
                CustomAttribute4 = this.CustomAttribute4,
                CustomAttribute5 = this.CustomAttribute5,
            };
        }

        #region IItem Members

        public Object GetItemPropertyValue(String itemPropertyName)
        {
            switch (itemPropertyName)
            {
                case FieldNames.FixturePackageInfoId:
                    return Id;
                case FieldNames.FixturePackageInfoFolderId:
                    return FolderId;
                case FieldNames.FixturePackageInfoName:
                    return Name;
                case FieldNames.FixturePackageInfoDescription:
                    return Description;
                case FieldNames.FixturePackageInfoHeight:
                    return Height;
                case FieldNames.FixturePackageInfoWidth:
                    return Width;
                case FieldNames.FixturePackageInfoDepth:
                    return Depth;
                case FieldNames.FixturePackageInfoFixtureCount:
                    return this.FixtureCount;
                case FieldNames.FixturePackageInfoAssemblyCount:
                    return this.AssemblyCount;
                case FieldNames.FixturePackageInfoComponentCount:
                    return this.ComponentCount;
                case FieldNames.FixturePackageInfoThumbnailImageData:
                    return ThumbnailImageData;
      
                default:
                    return null;
            }
        }

        #endregion

        #endregion
    }

     public static class FileItemHelper
    {
        public static Boolean GetBoolean(Object value)
        {
            Boolean returnValue = new Boolean();
            if (value != null)
            {
                returnValue = (Boolean)value;
            }
            return returnValue;
        }

        public static Byte GetByte(Object value)
        {
            Byte returnValue = new Byte();
            if (value != null)
            {
                returnValue = (Byte)value;
            }
            return returnValue;
        }

        public static Char GetChar(Object value)
        {
            Char returnValue = new Char();
            if (value != null)
            {
                returnValue = (Char)value;
            }
            return returnValue;
        }

        public static DateTime GetDateTime(Object value)
        {
            DateTime returnValue = new DateTime();
            if (value != null)
            {
                returnValue = (DateTime)value;
            }
            return returnValue;
        }

        public static Decimal GetDecimal(Object value)
        {
            Decimal returnValue = new Decimal();
            if (value != null)
            {
                returnValue = (Decimal)value;
            }
            return returnValue;
        }

        public static Double GetDouble(Object value)
        {
            Double returnValue = new Double();
            if (value != null)
            {
                returnValue = (Double)value;
            }
            return returnValue;
        }

        public static Int32 GetInt32(Object value)
        {
            Int32 returnValue = new Int32();
            if (value != null)
            {
                returnValue = (Int32)value;
            }
            return returnValue;
        }

        public static Int64 GetInt64(Object value)
        {
            Int64 returnValue = new Int64();
            if (value != null)
            {
                returnValue = (Int64)value;
            }
            return returnValue;
        }

        public static SByte GetSByte(Object value)
        {
            SByte returnValue = new SByte();
            if (value != null)
            {
                returnValue = (SByte)value;
            }
            return returnValue;
        }

        public static Int16 GetInt16(Object value)
        {
            Int16 returnValue = new Int16();
            if (value != null)
            {
                returnValue = (Int16)value;
            }
            return returnValue;
        }

        public static Single GetSingle(Object value)
        {
            Single returnValue = new Single();
            if (value != null)
            {
                returnValue = (Single)value;
            }
            return returnValue;
        }

        public static UInt32 GetUInt32(Object value)
        {
            UInt32 returnValue = new UInt32();
            if (value != null)
            {
                returnValue = (UInt32)value;
            }
            return returnValue;
        }

        public static UInt64 GetUInt64(Object value)
        {
            UInt64 returnValue = new UInt64();
            if (value != null)
            {
                returnValue = (UInt64)value;
            }
            return returnValue;
        }

        public static UInt16 GetUInt16(Object value)
        {
            UInt16 returnValue = new UInt16();
            if (value != null)
            {
                returnValue = (UInt16)value;
            }
            return returnValue;
        }
    }
}
