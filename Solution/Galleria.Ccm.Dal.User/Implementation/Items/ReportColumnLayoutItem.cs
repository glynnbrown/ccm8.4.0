﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-27938 : N.Haywood
////  Copied from Custom Columns.

//#endregion

//#endregion

//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Runtime.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using System.Xml.Serialization;

//namespace Galleria.Ccm.Dal.User.Implementation.Items
//{
//    /// <summary>
//    ///     Report column layout dal item.
//    /// </summary>
//    [DataContract]
//    public sealed class ReportColumnLayoutItem
//    {
//        #region Fields

//        private String _id;
//        private FileStream _stream;

//        private Boolean _isNew;
//        private Boolean _isDirty;

//        #endregion

//        #region Properties

//        public List<ReportColumnFilterItem> ColumnFilters { get; set; }
//        public List<ReportColumnGroupItem> ColumnGroups { get; set; }
//        public List<ReportColumnSortItem> ColumnSorts { get; set; }
//        public List<ReportColumnItem> Columns { get; set; }
//        public String Description { get; set; }
//        public Boolean HasTotals { get; set; }
//        public String Name { get; set; }
//        public Byte Type { get; set; }


//        /// <summary>
//        ///     Gets or sets the name for the current <see cref="FileStream" />.
//        /// </summary>
//        /// <remarks>If there is no current open <see cref="FileStream" />, it returns <c>null</c>.</remarks>
//        [XmlIgnoreAttribute]
//        public String CurrentStreamName
//        {
//            get { return IsNew || _stream == null ? null : _stream.Name; }
//        }

//        /// <summary>
//        ///     Gets or sets the unique id for this <see cref="ReportColumnLayoutItem" />.
//        /// </summary>
//        /// <remarks>The Id is the file path.</remarks>
//        [XmlIgnoreAttribute]
//        public String Id
//        {
//            get { return _id; }
//            private set
//            {
//                _id = value;

//                if (Columns != null) Columns.ForEach(item => item.ReportColumnLayoutId = _id);
//                if (ColumnGroups != null) ColumnGroups.ForEach(item => item.ReportColumnLayoutId = _id);
//                if (ColumnSorts != null) ColumnSorts.ForEach(item => item.ReportColumnLayoutId = _id);
//                if (ColumnFilters != null) ColumnFilters.ForEach(item => item.ReportColumnLayoutId = _id);
//            }
//        }

//        /// <summary>
//        ///     Gets or sets whether this <see cref="ReportColumnLayoutItem" /> is dirty
//        /// </summary>
//        [XmlIgnoreAttribute]
//        public Boolean IsDirty
//        {
//            get
//            {
//                return _isDirty ||
//                       (Columns != null && Columns.Any(item => item.IsDirty)) ||
//                       (ColumnGroups != null && ColumnGroups.Any(item => item.IsDirty)) ||
//                       (ColumnSorts != null && ColumnSorts.Any(item => item.IsDirty)) ||
//                       (ColumnFilters != null && ColumnFilters.Any(item => item.IsDirty));
//            }
//            private set { _isDirty = value; }
//        }

//        /// <summary>
//        ///     Gets or sets whether this <see cref="ReportColumnLayoutItem" /> is new.
//        /// </summary>
//        [XmlIgnoreAttribute]
//        private Boolean IsNew { get; set; }

//        #endregion

//        #region Constructor

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnLayoutItem" />.
//        /// </summary>
//        public ReportColumnLayoutItem()
//        {
//        }

//        /// <summary>
//        ///     Initializes and returns a new instance of <see cref="ReportColumnLayoutItem" /> from the given dto.
//        /// </summary>
//        /// <param name="dto"></param>
//        public ReportColumnLayoutItem(ReportColumnLayoutDto dto)
//        {
//            IsNew = true;
//            Columns = new List<ReportColumnItem>();
//            ColumnGroups = new List<ReportColumnGroupItem>();
//            ColumnSorts = new List<ReportColumnSortItem>();
//            ColumnFilters = new List<ReportColumnFilterItem>();

//            UpdateFromDto(dto);
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        ///     Deletes the underlying file for this <see cref="ReportColumnLayoutItem" />.
//        /// </summary>
//        public void Delete()
//        {
//            if (_stream == null) return;

//            var lastPath = _stream.Name;
//            Unlock();
//            File.Delete(lastPath);
//        }

//        /// <summary>
//        ///     Marks this <see cref="ReportColumnLayoutItem" /> as dirty.
//        /// </summary>
//        public void MarkDirty()
//        {
//            IsDirty = true;
//        }

//        /// <summary>
//        ///     Saves this <see cref="ReportColumnLayoutItem" /> to a file, using the <see cref="Id" /> property as the file
//        ///     path.
//        /// </summary>
//        /// <param name="fixedDirectory"></param>
//        /// <exception cref="Exception">Thrown when the directory path cannot be extracted from the <see cref="Id" /> property.</exception>
//        public void Save(String fixedDirectory = null)
//        {
//            Id = GenerateFilePath(String.IsNullOrEmpty(fixedDirectory) ? Id : fixedDirectory, Name);

//            Delete(); // Delete the old file before saving the new one.

//            var directoryName = Path.GetDirectoryName(Id);
//            if (directoryName == null)
//                throw new Exception(
//                    string.Format(
//                        "{1}{0}The directory used to save Report column layouts was not found. Ensure it exists before trying again, and that the user has permissions to use it.",
//                        Environment.NewLine, Id));

//            Directory.CreateDirectory(directoryName);

//            //create the new file stream and serialize.
//            _stream = File.Open(Id, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
//            XmlSerializer x = new XmlSerializer(this.GetType());
//            x.Serialize(_stream, this);

//            this.IsDirty = false;
//            this.IsNew = false;
//        }

//        /// <summary>
//        ///     Unlocks the source file for this <see cref="ReportColumnLayoutInfoItem" />.
//        /// </summary>
//        public void Unlock()
//        {
//            if (_stream == null) return; // No source to unlock.

//            _stream.Close();
//            _stream.Dispose();
//            _stream = null;
//        }

//        /// <summary>
//        ///     Fetches the <see cref="ReportColumnLayoutItem" /> from the underlying file, using its <paramref name="id" /> as
//        ///     the file path.
//        /// </summary>
//        /// <param name="id">
//        ///     Id of the <see cref="ReportColumnLayoutItem" /> requested, and also the file path for its
//        ///     underlying file.
//        /// </param>
//        /// <returns>A new instance of <see cref="ReportColumnLayoutItem" />, initialized from the information on the file.</returns>
//        internal static ReportColumnLayoutItem FetchById(String id, Boolean doLock = false)
//        {
//            ReportColumnLayoutItem dalObject = null;

//            try
//            {
//                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.None);

//                XmlSerializer x = new XmlSerializer(typeof(ReportColumnLayoutItem));

//                dalObject = (ReportColumnLayoutItem)x.Deserialize(stream);
//                dalObject._stream = stream;
//                dalObject.Id = id;
//                if (!doLock)
//                {
//                    stream.Close();
//                }
//            }
//            catch (Exception)
//            {
//                //TODO: handle?
//            }

//            return dalObject;
//        }

//        #endregion

//        #region Static Helpers

//        /// <summary>
//        ///     Generates the file path from the id (as directory), name and extension.
//        /// </summary>
//        /// <param name="currentFilePath">Existing file path.</param>
//        /// <param name="fileName">Name of the file, without the extension.</param>
//        /// <returns>A <see cref="String" /> with the full path generated from the instance's data.</returns>
//        private static String GenerateFilePath(String currentFilePath, String fileName)
//        {
//            return Path.ChangeExtension(
//                Path.Combine(
//                    Path.GetDirectoryName(currentFilePath),
//                    fileName),
//                Constants.ReportColumnLayoutFileExtension);
//        }

//        #endregion

//        #region Data Transfer Object

//        /// <summary>
//        ///     Returns a <see cref="ReportColumnLayoutDto" /> for this <see cref="ReportColumnLayoutItem" />.
//        /// </summary>
//        /// <returns>A new instance of a <see cref="ReportColumnLayoutDto" />.</returns>
//        public ReportColumnLayoutDto GetDataTransferObject()
//        {
//            return new ReportColumnLayoutDto
//            {
//                Id = Id,
//                Name = Name,
//                Description = Description,
//                Type = Type,
//                HasTotals = HasTotals
//            };
//        }

//        /// <summary>
//        ///     Updates this <see cref="ReportColumnLayoutItem" /> from the given <see cref="ReportColumnLayoutDto" />.
//        /// </summary>
//        /// <param name="dto">
//        ///     The <see cref="ReportColumnLayoutDto" /> used to initialize the
//        ///     <see cref="ReportColumnLayoutItem" />.
//        /// </param>
//        public void UpdateFromDto(ReportColumnLayoutDto dto)
//        {
//            if (_stream != null)
//            {
//                //recreate the id
//                dto.Id = GenerateFilePath(_stream.Name, dto.Name);
//            }

//            Id = (String)dto.Id;
//            Name = dto.Name;
//            Description = dto.Description;
//            Type = dto.Type;
//            HasTotals = dto.HasTotals;
//            MarkDirty();
//        }

//        #endregion
//    }
//}