﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;
using System.Xml.Linq;


namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PrintTemplateComponentDetail dal item
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentDetailItem : IDalItem
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public Int32 PrintTemplateComponentId { get; set; }

        public String Key { get; set; }
        public String Value { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PrintTemplateComponentDetailItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        /// Creates a new instance from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public PrintTemplateComponentDetailItem(PrintTemplateComponentDetailDto dto)
        {
            UpdateFromDto(dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the element with values from this item.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.PrintTemplateComponentDetailKey, this.Key);
            element.AddValue(FieldNames.PrintTemplateComponentDetailValue, this.Value);
        }

        /// <summary>
        /// Populates this item from the given element.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            Key = element.GetString(FieldNames.PrintTemplateComponentDetailKey);
            Value = element.GetString(FieldNames.PrintTemplateComponentDetailValue);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateComponentDetailDto GetDataTransferObject()
        {
            return new PrintTemplateComponentDetailDto()
            {
                Id = this.Id,
                PrintTemplateComponentId = this.PrintTemplateComponentId,
                Key = this.Key,
                Value = this.Value,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PrintTemplateComponentDetailDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PrintTemplateComponentId = (Int32)dto.PrintTemplateComponentId;
            this.Key = dto.Key;
            this.Value = dto.Value;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion

    }
}
