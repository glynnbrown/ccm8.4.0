﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// Highlight group dal item
    /// </summary>
    [Serializable]
    public sealed class HighlightGroupItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public HighlightGroupDtoKey DtoKey
        {
            get
            {
                return new HighlightGroupDtoKey()
                {
                    Id = this.Id
                };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String HighlightId { get; set; }

        public Int32 Order { get; set; }
        public String Name { get; set; }
        public String DisplayName { get; set; }
        public Int32 FillColour { get; set; }
        public Byte FillPatternType { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public HighlightGroupItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public HighlightGroupItem(HighlightGroupDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///  Creates a new instance of this type from the given
        ///  serialization context.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public HighlightGroupItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.HighlightGroupOrder, this.Order);
            info.AddValue(FieldNames.HighlightGroupName, this.Name);
            info.AddValue(FieldNames.HighlightGroupDisplayName, this.DisplayName);
            info.AddValue(FieldNames.HighlightGroupFillColour, this.FillColour);
            info.AddValue(FieldNames.HighlightGroupFillPatternType, this.FillPatternType);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        private void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            Order = info.GetInt32(FieldNames.HighlightGroupOrder);
            Name = info.GetString(FieldNames.HighlightGroupName);
            DisplayName = info.GetString(FieldNames.HighlightGroupDisplayName);
            FillColour = info.GetByte(FieldNames.HighlightGroupFillColour);
            FillPatternType = info.GetByte(FieldNames.HighlightGroupFillPatternType);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public HighlightGroupDto GetDataTransferObject()
        {
            return new HighlightGroupDto()
            {
                Id = this.Id,
                HighlightId = this.HighlightId,
                Order = this.Order,
                Name = this.Name,
                DisplayName = this.DisplayName,
                FillColour = this.FillColour,
                FillPatternType = this.FillPatternType,
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(HighlightGroupDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.HighlightId = Convert.ToString(dto.HighlightId);
            this.Order = dto.Order;
            this.Name = dto.Name;
            this.DisplayName = dto.DisplayName;
            this.FillColour = dto.FillColour;
            this.FillPatternType = dto.FillPatternType;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
