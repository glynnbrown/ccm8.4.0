﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    public sealed class PlanogramComparisonTemplateItem : IDalItem, IXmlSerializable
    {
        #region Fields

        private Boolean _isDirty;

        private String _id;
        private FileStream _stream;

        private List<PlanogramComparisonTemplateFieldItem> _comparisonFields = new List<PlanogramComparisonTemplateFieldItem>();

        #endregion

        #region Properties

        public Byte MajorVersion { get; set; }
        public Byte MinorVersion { get; set; }

        /// <summary>
        ///     Get whether this instance is read only.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsReadOnly { get; private set; }

        /// <summary>
        ///     Get whether this instance is new.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsNew { get; private set; }

        /// <summary>
        ///     Get whether this instance is dirty.
        /// </summary>
        [XmlIgnoreAttribute]
        public Boolean IsDirty { get { return _isDirty || ComparisonFields.Any(f => f.IsDirty); } private set { _isDirty = value; } }

        /// <summary>
        ///     Get the name of the stream that is open currently.
        /// </summary>
        [XmlIgnoreAttribute]
        public String CurrentStreamName { get { return IsNew || _stream == null ? null : _stream.Name; } }

        /// <summary>
        ///     Get the Id for this instance.
        /// </summary>
        /// <remarks>The Id is the path to the file.</remarks>
        [XmlIgnoreAttribute]
        public String Id
        {
            get { return _id; }
            private set
            {
                _id = value;
                UpdateChildIdLinks();
            }
        }

        /// <summary>
        ///     Get/set the name for this instance.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        ///     Get/set the description for this instance.
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        ///     Get/set whether to consider non placed products as existing.
        /// </summary>
        public Boolean IgnoreNonPlacedProducts { get; set; }

        /// <summary>
        ///     Get/set the order in which data is organized on display.
        /// </summary>
        public Byte DataOrderType { get; set; }

        /// <summary>
        ///     Get/set the date this instance was created.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        ///     Get/set the date this instance was last modified.
        /// </summary>
        public DateTime DateLastModified { get; set; }

        /// <summary>
        ///     Get/set the collection of child comparison fields.
        /// </summary>
        public List<PlanogramComparisonTemplateFieldItem> ComparisonFields
        {
            get { return _comparisonFields; }
            set { _comparisonFields = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonTemplateItem"/>.
        /// </summary>
        public PlanogramComparisonTemplateItem() {}

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonTemplateItem"/> populated from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">The <see cref="PlanogramComparisonTemplateDto"/> containing the data to initialize this instance with.</param>
        public PlanogramComparisonTemplateItem(PlanogramComparisonTemplateDto dto)
        {
            IsNew = true;
            UpdateFromDto(dto);

            //  Set version numbers to the latest available.
            Byte major, minor;
            DalItemHelper.GetLatestVersionNumber(GetScriptsType(), out major, out minor);
            MajorVersion = major;
            MinorVersion = minor;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonTemplateItem"/> with a filename that matches the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The Id of the item to be fetched.</param>
        /// <param name="asReadOnly">Whether to open the file in read only mode.</param>
        /// <remarks>The Id provided is the path to the file.</remarks>
        internal static PlanogramComparisonTemplateItem FetchById(String id, Boolean asReadOnly)
        {
            if (!File.Exists(id)) throw new DtoDoesNotExistException();

            PlanogramComparisonTemplateItem item;

            if (asReadOnly)
            {
                using (FileStream stream = File.OpenRead(id))
                {
                    item = DalItemHelper.Deserialize<PlanogramComparisonTemplateItem>(stream);
                    item.Id = id;
                    item.IsReadOnly = true;
                }
            }
            else
            {
                FileStream stream = File.Open(id, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                item = DalItemHelper.Deserialize<PlanogramComparisonTemplateItem>(stream);
                item.Id = id;

                //  Keep the original stream open so that the file is locked.
                item._stream = stream;
            }

            return item;
        }

        /// <summary>
        ///     Save this instance to file.
        /// </summary>
        /// <param name="fixedDirectory"></param>
        public void Save(String fixedDirectory)
        {
            //  Check that we are not in readonly mode.
            Debug.Assert(!IsReadOnly, "Trying to save when readonly");
            if (IsReadOnly) return;

            //  Get the filepath
            String directoryName = Path.GetDirectoryName(Id);
            Id = Path.ChangeExtension(Path.Combine(directoryName, Name), Constants.PlanogramComparisonTemplateFileExtension);

            //delete the old file first
            Delete();

            Directory.CreateDirectory(directoryName);

            //create the new file stream and serialize.
            _stream = File.Open(Id, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            var xmlSerializer = new XmlSerializer(GetType());
            xmlSerializer.Serialize(_stream, this);

            IsDirty = false;
            IsNew = false;
        }

        /// <summary>
        ///     Delete this instance's file.
        /// </summary>
        public void Delete()
        {
            if (_stream == null) return;

            String lastPath = _stream.Name;
            Unlock();
            File.Delete(lastPath);
        }

        /// <summary>
        ///     Mark this instance as changed.
        /// </summary>
        public void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        ///     Unlocks the source file for this instance.
        /// </summary>
        public void Unlock()
        {
            if (_stream == null) return;

            _stream.Close();
            _stream.Dispose();
            _stream = null;
        }

        #region Child related methods

        /// <summary>
        ///     Ensures that all children referencing this id are up to date.
        /// </summary>
        private void UpdateChildIdLinks()
        {
            foreach (PlanogramComparisonTemplateFieldItem child in ComparisonFields)
            {
                child.PlanogramComparisonTemplateId = Id;
            }
        }

        public PlanogramComparisonTemplateFieldItem Insert(PlanogramComparisonTemplateFieldDto dto)
        {
            dto.Id = PlanogramComparisonTemplateFieldItem.GetNextId();
            var item = new PlanogramComparisonTemplateFieldItem(dto);
            ComparisonFields.Add(item);
            MarkDirty();
            return item;
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        ///     Updates this instance from the given dto.
        /// </summary>
        public void UpdateFromDto(PlanogramComparisonTemplateDto dto)
        {
            _isDirty = true;

            if (_stream != null)
            {
                //  Recreate the id.
                String directoryName = Path.GetDirectoryName(_stream.Name);
                dto.Id = Path.ChangeExtension(Path.Combine(directoryName, dto.Name),Constants.PrintTemplateFileExtension);
            }


            Id = (String)dto.Id;
            Name = dto.Name;
            Description = dto.Description;
            IgnoreNonPlacedProducts = dto.IgnoreNonPlacedProducts;
            DataOrderType = dto.DataOrderType;
            DateCreated = dto.DateCreated;
            DateLastModified = dto.DateLastModified;
        }

        /// <summary>
        ///     Returns a <see cref="PlanogramComparisonTemplateDto"/> for this instance.
        /// </summary>
        public PlanogramComparisonTemplateDto GetDataTransferObject()
        {
            return new PlanogramComparisonTemplateDto
                   {
                       Id = Id,
                       Name = Name,
                       Description = Description,
                       IgnoreNonPlacedProducts = IgnoreNonPlacedProducts,
                       DataOrderType = DataOrderType,
                       DateCreated = DateCreated,
                       DateLastModified = DateLastModified,
                   };
        }

        #endregion

        #endregion

        #region IDalItem

        /// <summary>
        ///     Populates the serializaition info with values from this instance.
        /// </summary>
        void IDalItem.GetObjectData(XElement element)
        {
            element.AddValue(FieldNames.MajorVersion, MajorVersion);
            element.AddValue(FieldNames.MinorVersion, MinorVersion);

            element.AddValue(FieldNames.PlanogramComparisonTemplateName, Name);
            element.AddValue(FieldNames.PlanogramComparisonTemplateDescription, Description);
            element.AddValue(FieldNames.PlanogramComparisonTemplateIgnoreNonPlacedProducts, IgnoreNonPlacedProducts);
            element.AddValue(FieldNames.PlanogramComparisonTemplateDataOrderType, DataOrderType);
            element.AddValue(FieldNames.PlanogramComparisonTemplateDateCreated, DateCreated);
            element.AddValue(FieldNames.PlanogramComparisonTemplateDateLastModified, DateLastModified);

            element.AddValue(FieldNames.PlanogramComparisonTemplateComparisonFields, ComparisonFields);
        }

        /// <summary>
        ///     Populates this instance from the given context.
        /// </summary>
        void IDalItem.SetObjectData(XElement element)
        {
            MajorVersion = element.GetByte(FieldNames.MajorVersion);
            MinorVersion = element.GetByte(FieldNames.MinorVersion);

            Name = element.GetString(FieldNames.PlanogramComparisonTemplateName);
            Description = element.GetString(FieldNames.PlanogramComparisonTemplateDescription);
            IgnoreNonPlacedProducts = element.GetBoolean(FieldNames.PlanogramComparisonTemplateIgnoreNonPlacedProducts);
            DataOrderType = element.GetByte(FieldNames.PlanogramComparisonTemplateDataOrderType);
            DateCreated = element.GetDateTime(FieldNames.PlanogramComparisonTemplateDateCreated);
            DateLastModified = element.GetDateTime(FieldNames.PlanogramComparisonTemplateDateLastModified);

            ComparisonFields = element.GetChildList<PlanogramComparisonTemplateFieldItem>(FieldNames.PlanogramComparisonTemplateComparisonFields);


            UpdateChildIdLinks();
        }

        #endregion

        #region IXmlSerializable Members

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
        {
            //  Read the xelement.
            var x = XNode.ReadFrom(reader) as XElement;

            //  Perform the upgrade.
            DalItemHelper.Upgrade(GetScriptsType(), x);

            //  Load.
           ((IDalItem)this).SetObjectData(x);
        }

        void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
        {
            var rootElement = new XElement(GetType().Name);
            ((IDalItem)this).GetObjectData(rootElement);

            //  NB: Don't write from root element otherwise we end up with double.
            foreach (XElement element in rootElement.Elements())
            {
                element.WriteTo(writer);
            }
        }

        private static Type GetScriptsType()
        {
            return typeof(Scripts.PlanogramComparisonTemplateScripts);
        }

        #endregion
    }
}
