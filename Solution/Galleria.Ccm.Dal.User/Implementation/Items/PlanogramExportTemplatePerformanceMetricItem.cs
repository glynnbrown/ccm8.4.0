﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Schema;
using System.Threading;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Dal.User.Implementation.Items
{
    /// <summary>
    /// PlanogramExportTemplatePerformanceMetric dal item.
    /// </summary>
    [Serializable]
    public class PlanogramExportTemplatePerformanceMetricItem : ISerializable
    {
        #region Properties

        [XmlIgnoreAttribute]
        public Boolean IsDirty { get; private set; }

        [XmlIgnoreAttribute]
        public PlanogramExportTemplatePerformanceMetricDtoKey DtoKey
        {
            get
            {
                return new PlanogramExportTemplatePerformanceMetricDtoKey()
                    {
                        Name = this.Name,
                        PlanogramExportTemplateId = this.PlanogramExportTemplateId
                    };
            }
        }

        [XmlIgnoreAttribute]
        public Int32 Id { get; set; }

        [XmlIgnoreAttribute]
        public String PlanogramExportTemplateId { get; set; }
        
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public String ExternalField { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramExportTemplatePerformanceMetricItem()
        {
            Id = GetNextId();
        }

        /// <summary>
        ///  Creates a new instance of this type from the given dto
        /// </summary>
        /// <param name="dto"></param>
        public PlanogramExportTemplatePerformanceMetricItem(PlanogramExportTemplatePerformanceMetricDto dto) 
        {
            UpdateFromDto(dto);
        }

        /// <summary>
        ///  Creates a new instance of this type from the given
        ///  serialization context.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public PlanogramExportTemplatePerformanceMetricItem(SerializationInfo info, StreamingContext context)
        {
            Id = GetNextId();
            SetObjectData(info, context);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates the serializaition info with values from this item.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricName, this.Name);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricDescription, this.Description);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricDirection, this.Direction);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricSpecialType, this.SpecialType);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricMetricType, this.MetricType);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricMetricId, this.MetricId);
            info.AddValue(FieldNames.PlanogramExportTemplatePerformanceMetricExternalField, this.ExternalField);
        }

        /// <summary>
        /// Populates this item from the given serialization info.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void SetObjectData(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(FieldNames.PlanogramExportTemplatePerformanceMetricName);
            Description = info.GetString(FieldNames.PlanogramExportTemplatePerformanceMetricDescription);
            Direction = info.GetByte(FieldNames.PlanogramExportTemplatePerformanceMetricDirection);
            SpecialType = info.GetByte(FieldNames.PlanogramExportTemplatePerformanceMetricSpecialType);
            MetricType = info.GetByte(FieldNames.PlanogramExportTemplatePerformanceMetricMetricType);
            MetricId = info.GetByte(FieldNames.PlanogramExportTemplatePerformanceMetricMetricId);
            ExternalField = info.GetString(FieldNames.PlanogramExportTemplatePerformanceMetricExternalField);
        }

        /// <summary>
        /// Returns a dto for this item.
        /// </summary>
        /// <returns></returns>
        public PlanogramExportTemplatePerformanceMetricDto GetDataTransferObject()
        {
            return new PlanogramExportTemplatePerformanceMetricDto()
            {
                Id = this.Id,
                PlanogramExportTemplateId = this.PlanogramExportTemplateId,
                Name = this.Name,
                Description = this.Description,
                Direction = this.Direction,
                SpecialType = this.SpecialType,
                MetricType = this.MetricType,
                MetricId = this.MetricId,
                ExternalField = this.ExternalField
            };
        }

        /// <summary>
        /// Updates this object from the given dto.
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateFromDto(PlanogramExportTemplatePerformanceMetricDto dto)
        {
            this.Id = (Int32)dto.Id;
            this.PlanogramExportTemplateId = Convert.ToString(dto.PlanogramExportTemplateId);
            this.Name = dto.Name;
            this.Description = dto.Description;
            this.Direction = dto.Direction;
            this.SpecialType = dto.SpecialType;
            this.MetricType = dto.MetricType;
            this.MetricId = dto.MetricId;
            this.ExternalField = dto.ExternalField;

            this.IsDirty = true;
        }

        #endregion

        #region Identity

        private static Int32 _nextIdentity = 0;

        internal static Int32 GetNextId()
        {
            Int32 identity = Interlocked.Increment(ref _nextIdentity);
            return identity;
        }

        #endregion
    }
}
