﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Ccm.Model;
using File = System.IO.File;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    ///     DAL methods for <see cref="CustomColumnLayoutInfoDto" />.
    /// </summary>
    public class CustomColumnLayoutInfoDal : DalBase, ICustomColumnLayoutInfoDal
    {
        #region Fetch

        public IEnumerable<CustomColumnLayoutInfoDto> FetchByIds(IEnumerable<Object> ids)
        {
            return
                ids.Select(id => id as String)
                    .Where(File.Exists)
                    .Select(CustomColumnLayoutInfoItem.FetchById)
                    .Where(item => item != null)
                    .Select(item => item.GetDataTransferObject());
        }

        public IEnumerable<CustomColumnLayoutInfoDto> FetchByType(IEnumerable<Object> ids, Byte type)
        {
            return
                ids.Select(id => id as String)
                    .Where(File.Exists)
                    .Select(CustomColumnLayoutInfoItem.FetchById)
                    .Where(item => item != null && item.Type == type)
                    .Select(item => item.GetDataTransferObject());
        }

        #endregion
    }
}