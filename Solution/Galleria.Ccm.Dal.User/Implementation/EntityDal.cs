﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30738 L.Ineson
//  Created
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class EntityDal : DalBase, IEntityDal
    {
        private EntityDto GetDummyEntity()
        {
            return new EntityDto()
            {
                Id = 1,
                Name = "Entity0"
            };
        }


        public System.Collections.Generic.IEnumerable<EntityDto> FetchAll()
        {
            return new List<EntityDto> { GetDummyEntity() };
        }

        public EntityDto FetchById(int id)
        {
            EntityDto dto = GetDummyEntity();
            dto.Id = id;
            return dto;
        }

        public EntityDto FetchDeletedByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Insert(EntityDto dto)
        {
            dto.Id = 1;
            //do nothing.
        }

        public void Update(EntityDto dto)
        {
            //do nothing.
        }

        public void DeleteById(int id)
        {
            //do nothing.
        }
    }
}
