﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25559 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IConnectionDal
    /// </summary>
    [Serializable]
    public sealed class ConnectionDal : DalBase, IConnectionDal
    {
        public IEnumerable<ConnectionDto> FetchAll()
        {
            List<ConnectionDto> dtoList = new List<ConnectionDto>();

            UserSystemSettingDalCache cache = this.DalCache.GetCache<UserSystemSettingDalCache>();
            
            foreach(ConnectionItem item in cache.Item.Connections)
            {
                dtoList.Add(item.GetDataTransferObject());
            }

            return dtoList;
        }

        public void Insert(ConnectionDto dto)
        {
            UserSystemSettingDalCache cache = this.DalCache.GetCache<UserSystemSettingDalCache>();

            dto.Id = ConnectionItem.GetNextId();

            cache.Item.Connections.Add(new ConnectionItem(dto));
        }

        public void Update(ConnectionDto dto)
        {
            UserSystemSettingDalCache cache = this.DalCache.GetCache<UserSystemSettingDalCache>();

            ConnectionItem item = cache.Item.Connections.FirstOrDefault(c => c.Id == dto.Id);
            if (item != null)
            {
                item.UpdateFromDto(dto);
            }
            else
            {
                throw new DtoDoesNotExistException();
            }
        }
    }
}
