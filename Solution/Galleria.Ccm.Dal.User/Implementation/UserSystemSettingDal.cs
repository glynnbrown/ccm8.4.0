﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25559 L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;

namespace Galleria.Ccm.Dal.User.Implementation
{
    /// <summary>
    /// User dal implementation of IUserSystemSettingDal
    /// </summary>
    [Serializable]
    public sealed class UserSystemSettingDal : DalBase, IUserSystemSettingDal
    {
        public UserSystemSettingDto Fetch()
        {
            return this.DalCache.GetCache<UserSystemSettingDalCache>().Item.GetDataTransferObject();
        }

        public void Insert(UserSystemSettingDto dto)
        {
            this.DalCache.GetCache<UserSystemSettingDalCache>().Item.UpdateFromDto(dto);
        }

        public void Update(UserSystemSettingDto dto)
        {
            this.DalCache.GetCache<UserSystemSettingDalCache>().Item.UpdateFromDto(dto);
        }
    }
}
