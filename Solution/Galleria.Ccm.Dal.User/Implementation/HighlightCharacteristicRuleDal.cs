﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class HighlightCharacteristicRuleDal : DalBase, IHighlightCharacteristicRuleDal
    {
        #region Helpers

        private HighlightDalCache Cache
        {
            get { return this.DalCache.GetCache<HighlightDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<HighlightCharacteristicRuleDto> FetchByHighlightCharacteristicId(Int32 highlightCharacteristicId)
        {
            List<HighlightCharacteristicRuleDto> dtoList = new List<HighlightCharacteristicRuleDto>();

            HighlightCharacteristicItem parent = Cache.FindHighlightCharacteristicById(highlightCharacteristicId);
            if (parent != null)
            {
                foreach (HighlightCharacteristicRuleItem dalObject in parent.Rules)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(HighlightCharacteristicRuleDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(HighlightCharacteristicRuleDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            HighlightCharacteristicRuleItem dalObject = Cache.FindHighlightCharacteristicRuleById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}
