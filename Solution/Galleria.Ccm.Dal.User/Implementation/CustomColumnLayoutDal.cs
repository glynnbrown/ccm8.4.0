﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class CustomColumnLayoutDal : DalBase, ICustomColumnLayoutDal
    {
        #region Helpers

        private CustomColumnLayoutCache Cache
        {
            get { return this.DalCache.GetCache<CustomColumnLayoutCache>(); }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a list of all dtos held by the cache with particular id.
        /// </summary>
        /// <returns></returns>
        public CustomColumnLayoutDto FetchById(Object id)
        {
            CustomColumnLayoutItem dalObject = Cache.FetchFileById(Convert.ToString(id),/*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            return dalObject.GetDataTransferObject();
        }

        #endregion

        #region Insert

        public void Insert(CustomColumnLayoutDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(CustomColumnLayoutDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            CustomColumnLayoutItem dalObject = Cache.FindCustomColumnLayoutById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Locks the item.
        /// </summary>
        public void LockById(Object id)
        {
            Cache.LockFileById(Convert.ToString(id));
        }

        /// <summary>
        /// Unlocks the item.
        /// </summary>
        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(Convert.ToString(id));
        }

        #endregion
    }

}
