﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class PlanogramImportTemplatePerformanceMetricDal : DalBase, IPlanogramImportTemplatePerformanceMetricDal
    {
        #region Helpers

        private PlanogramImportTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PlanogramImportTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<PlanogramImportTemplatePerformanceMetricDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId)
        {
            List<PlanogramImportTemplatePerformanceMetricDto> dtoList = new List<PlanogramImportTemplatePerformanceMetricDto>();

            PlanogramImportTemplateItem parent = Cache.FindPlanogramImportTemplateById(Convert.ToString(planogramImportTemplateId));
            if (parent != null)
            {
                foreach (PlanogramImportTemplatePerformanceMetricItem dalObject in parent.PerformanceMetrics)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            PlanogramImportTemplatePerformanceMetricItem dalObject = Cache.FindPlanogramImportTemplatePerformanceMetricItemById(id);
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion
    }
}