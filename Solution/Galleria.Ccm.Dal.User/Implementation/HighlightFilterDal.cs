﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion


using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Implementation
{
    [Serializable]
    public sealed class HighlightFilterDal : DalBase, IHighlightFilterDal
    {
        #region Helpers

        private HighlightDalCache Cache
        {
            get { return this.DalCache.GetCache<HighlightDalCache>(); }
        }

        #endregion

        #region Fetch

        public IEnumerable<HighlightFilterDto> FetchByHighlightId(object highlightId)
        {
            List<HighlightFilterDto> dtoList = new List<HighlightFilterDto>();

            HighlightItem highlight = Cache.FindHighlightById(Convert.ToString(highlightId));
            if (highlight != null)
            {
                foreach (HighlightFilterItem dalObject in highlight.Filters)
                {
                    dtoList.Add(dalObject.GetDataTransferObject());
                }
            }

            return dtoList;
        }

        #endregion

        #region Insert

        public void Insert(HighlightFilterDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(HighlightFilterDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Int32 id)
        {
            HighlightFilterItem dalObject = Cache.FindHighlightFilterById(id);
            if (dalObject == null) return;

            Cache.Delete(dalObject);
        }

        #endregion
    }
}
