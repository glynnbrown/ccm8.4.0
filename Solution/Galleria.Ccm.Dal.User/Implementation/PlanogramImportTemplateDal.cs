﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.User.Caches;
using Galleria.Ccm.Dal.User.Implementation.Items;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.User.Implementation
{
    public sealed class PlanogramImportTemplateDal : DalBase, IPlanogramImportTemplateDal
    {
        #region Helpers

        private PlanogramImportTemplateDalCache Cache
        {
            get { return this.DalCache.GetCache<PlanogramImportTemplateDalCache>(); }
        }

        #endregion

        #region Fetch

        public PlanogramImportTemplateDto FetchById(Object id)
        {
            PlanogramImportTemplateItem dalObject = Cache.FetchFileById(Convert.ToString(id), /*allowReadonly*/true);
            if (dalObject == null) throw new DtoDoesNotExistException();

            PlanogramImportTemplateDto dto = dalObject.GetDataTransferObject();
            return dto;
        }

        public PlanogramImportTemplateDto FetchByEntityIdName(Int32 entityId, String name)
        {
            var dtos = Cache.Items.Select(i => i.GetDataTransferObject());
            var returnDto = dtos.FirstOrDefault(dto => dto.Name == name);
            if (returnDto == null) throw new DtoDoesNotExistException();
            return returnDto;
        }

        #endregion

        #region Insert

        public void Insert(PlanogramImportTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Update

        public void Update(PlanogramImportTemplateDto dto)
        {
            Cache.InsertOrUpdate(dto);
        }

        #endregion

        #region Delete

        public void DeleteById(Object id)
        {
            PlanogramImportTemplateItem dalObject = Cache.FindPlanogramImportTemplateById(Convert.ToString(id));
            if (dalObject == null) return;
            Cache.Delete(dalObject);
        }

        #endregion

        #region Commands

        public void LockById(Object id)
        {
           Cache.LockFileById(id as String);
        }

        public void UnlockById(Object id)
        {
            Cache.UnlockFileById(id as String);
        }

        #endregion
    }
}
