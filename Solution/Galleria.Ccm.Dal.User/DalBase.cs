﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 : L.Hodson
//      Created
#endregion
#endregion



using Galleria.Framework.Dal;
namespace Galleria.Ccm.Dal.User
{
    /// <summary>
    /// Base dal class
    /// </summary>
    public abstract class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Properties

        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
        }

        /// <summary>
        /// The DalCache for this dal
        /// </summary>
        internal DalCache DalCache
        {
            get { return DalContext.DalCache; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }

        #endregion

    }
}
