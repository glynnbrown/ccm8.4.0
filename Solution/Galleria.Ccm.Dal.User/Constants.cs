﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-24261 L.Hodson
////  Created
//// V8-25395 : A.Probyn
////  Added ProductLibrary file extension
//// V8-24700 : A.Silva ~ Added CustomColumnLayout file extension.
//// V8-26815 : A.Silva ~ Added ValidationTemplate file extension.
//// V8-26844 : A.Silva ~ Corrected declaration of CustomColumnLayout file extenstion.

//#endregion

//#endregion

//using System;

//namespace Galleria.Ccm.Dal.User
//{
//    public static class Constants
//    {
//        public const String AppFolderName = @"Customer Centric Merchandising";
//        public const String EditorAppFolderName = @"Customer Centric Merchandising\Editor";

//        ///// <summary>
//        /////     A file extension like .ccl
//        ///// </summary>
//        //public const String CustomColumnLayoutFileExtension = ".ccl";

//        //public const String HighlightFileExtension = ".pogh";
//        //public const String LabelFileExtension = ".pogl";
//        //public const String ProductLibraryFileExtension = ".pogpl";

//        ///// <summary>
//        /////     A file extension like .pogvt
//        ///// </summary>
//        //public const String ValidationTemplateFileExtension = ".pogvt";

//        public const String RecentPlanogramsFileName = "Mru.xml";
//        public const String SystemSettingsFileName = "UserSettings.xml";
//        public const String UserSettingsFileName = "CCMUsers.xml";
//    }
//}