﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
//V8-30738 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Xml.Linq;
using System.Xml.XPath;
using Galleria.Ccm.Dal.User.Schema;

namespace Galleria.Ccm.Dal.User.Scripts
{
    /// <summary>
    /// Contains upgrade scripts for PrintTemplate files.
    /// </summary>
    public sealed class PrintTemplateScripts
    {
        #region Script0001 (CCM820)

        /// <summary>
        /// Initial version CCM820
        /// </summary>
        private static void Script0001_0000(XElement rootElement)
        {
            //nothing to do.
        }

        #endregion
    }
}
