﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System.Xml.Linq;

namespace Galleria.Ccm.Dal.User.Scripts
{
    /// <summary>
    ///     Upgrade scripts declarations for Planogram Comparison Template files.
    /// </summary>
    public sealed class PlanogramComparisonTemplateScripts
    {
        /// <summary>
        ///     Initial version CCM830
        /// </summary>
        /// <param name="rootElement"></param>
        private static void Script0001_0000(XElement rootElement)
        {
            //  Nothing to do.
        }
    }
}