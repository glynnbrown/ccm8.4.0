﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
//V8-30870 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.3)
//V8-31541 : L.Ineson
//  Added more layout item properties.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
#endregion

#endregion

using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Galleria.Ccm.Dal.User.Schema;
using System.Collections.Generic;


namespace Galleria.Ccm.Dal.User.Scripts
{
    public sealed class CustomColumnLayoutScripts
    {
        #region Script0001 (CCM811)

        /// <summary>
        /// Initial version.
        /// </summary>
        private static void Script0001_0000(XElement rootElement)
        {
            //nothing to do.
        }

        #endregion

        #region Script0002 (CCM820)

        /// <summary>
        /// CCM820 upgrade script
        /// </summary>
        private static void Script0002_0000(XElement rootElement)
        {
            #region CustomColumn
            XElement columnListElement = rootElement.XPathSelectElement(FieldNames.CustomColumnLayoutColumns);
            if (columnListElement.HasElements)
            {
                foreach (XElement child in columnListElement.Elements())
                {
                    //Width
                    child.Add(new XElement(FieldNames.CustomColumnWidth, 0));

                    //DisplayName
                    child.Add(new XElement(FieldNames.CustomColumnDisplayName, String.Empty));
                }
            }
            #endregion
        }

        #endregion

        #region Script0003 (CCM830)

        public const string CustomColumnHeaderGroupNumber = "HeaderGroupNumber"; 

        /// <summary>
        /// CCM830 upgrade script
        /// </summary>
        private static void Script0003_0000(XElement rootElement)
        {
            #region CustomColumnLayout
            if (rootElement.HasElements)
            {
                //FrozenColumnCount
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutFrozenColumnCount, (Byte)0));

                //IsTitleShown
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutIsTitleShown, false));

                //IsGroupDetailHidden
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutIsGroupDetailHidden, false));

                //IsGroupBlankRowShown
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutIsGroupBlankRowShown, false));

                //DataOrderType
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutDataOrderType, (Byte)0));

                //IsMultiplePlanograms
                rootElement.Add(new XElement(FieldNames.CustomColumnLayoutIncludeAllPlanograms, false));
            }
            #endregion

            #region CustomColumn
             XElement columnListElement = rootElement.XPathSelectElement(FieldNames.CustomColumnLayoutColumns);
             if (columnListElement.HasElements)
             {
                 //Remove CustomColumnHeaderGroupNumber:
                 List<Tuple<Int32, Int32, XElement>> orderChildList = 
                     new List<Tuple<int,int,XElement>>(columnListElement.Elements().Count());

                 foreach(XElement child in columnListElement.Elements())
                 {
                     XElement groupNumberElement = child.XPathSelectElement(CustomColumnHeaderGroupNumber);
                     Int32 groupNumber;
                     if (groupNumberElement == null || !Int32.TryParse(groupNumberElement.Value, out groupNumber))
                     {
                         groupNumber = 1;
                     }

                     Int32 colNumber;
                     if(!Int32.TryParse(child.XPathSelectElement(FieldNames.CustomColumnNumber).Value, out colNumber))
                     {
                         colNumber = 1;
                     }

                     orderChildList.Add(new Tuple<int,int,XElement>(groupNumber, colNumber, child));
                     if(groupNumberElement != null) groupNumberElement.Remove();
                 }
                 Int32 colNo = 1;
                 foreach(var child in orderChildList.OrderBy(c=> c.Item1).ThenBy(c=> c.Item2))
                 {
                     child.Item3.XPathSelectElement(FieldNames.CustomColumnNumber).Value =colNo.ToString();
                     colNo++;
                 }
             }
            #endregion
        }

        #endregion
    }
}
