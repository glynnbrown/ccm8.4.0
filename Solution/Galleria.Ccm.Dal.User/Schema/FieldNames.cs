﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// V8-24801 : L.Hodson
//  Created
// V8-24265 : J.Pickup
//  Renamed LabelSettings to Label for all fields.
// V8-25395 : A.Probyn
//  Added ProductLibrary, ProductLibraryColumnMapping fields.
// V8-25873 : A.Probyn
//      Added new FixturePackage CustomAttribute1-5
// V8-24700 : A.Silva ~ Added ColumnLayouts field.
// V8-26076 : A.Silva ~ Added CustomColumn fields.
//                    ~ Added CustomColumnGroup fields.
//                    ~ Added CustomColumnSort fields.
//                    ~ Added CustomColumnFilter fields.
// V8-26472 : A.Probyn
//      Removed obsolete CustomColumn fields.
// V8-26844 : A.Silva ~ Added CustomColumnLayout missing field names.
// V8-27504 : A.Silva
//      Added CustomColumnHeaderGroupNumber.
// V8-27938 : N.Haywood
//      Added Report Column fields

#endregion

#region Version History: (CCM 801)
//V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetric
// V8-28644 : D.Pleasance
//  Added Major \ Minor version properties to CustomColumnLayout, Highlight, Label, Product Library, ValidationTemplate
#endregion

#region Version History: (CCM 802)
// V8-27433 : A.Kuszyk
//  Added RecentPlanogram Path field.
#endregion

#region Version History: (CCM 803)
// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
// V8-29596 : L.Luong
//  Added Criteria
#endregion

#region Version History: (CCM 820)
//V8-30738 : L.Ineson
//  Added PrintTemplate
//V8-30791 : D.Pleasance
//  Added SystemSettingColor
// V8-30870 : L.Ineson
//  Added width and display type to custom column layout
#endregion

#region Version History: (CCM 830)
// V8-31546 : M.Pettit
//  Added PlanogramExportTemplate, PlanogramExportTemplateMapping, PlanogramExportTemplatePerformanceMetric
// V8-31699 : A.Heathcote
//  Added SystemSettingRecentLabel, SystemSettingRecentHighlight and SystemSettingRecentDatasheet.
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplate.
// V8-31934 : A.Heathcote
//  Added SystemSettingSelectedColumn
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.User.Schema
{
    public static class FieldNames
    {
        public const String MajorVersion = "MajorVersion";
        public const String MinorVersion = "MinorVersion";

        #region Connection

        public const String ConnectionServerName = "ServerName";
        public const String ConnectionDatabaseName = "DatabaseName";
        public const String ConnectionDatabaseFileName = "DatabaseFileName";
        public const String ConnectionHostName = "HostName";
        public const String ConnectionPortNumber = "PortNumber";
        public const String ConnectionBusinessEntityId = "BusinessEntityId";
        public const String ConnectionIsAutoConnect = "IsAutoConnect";
        public const String ConnectionIsCurrentConnection = "IsCurrentConnection";

        #endregion

        #region ColumnLayoutSetting

        public const String ColumnLayoutSettingScreenkey = "ScreenKey";
        public const String ColumnLayoutSettingColumnLayoutName = "ColumnLayoutName";

        #endregion

        #region CustomColumnLayout

        public const String CustomColumnLayoutType = "Type";
        public const String CustomColumnLayoutName = "Name";
        public const String CustomColumnLayoutDescription = "Description";
        public const String CustomColumnLayoutHasTotals = "HasTotals";
        public const String CustomColumnLayoutFrozenColumnCount = "FrozenColumnCount";
        public const String CustomColumnLayoutIsTitleShown = "IsTitleShown";
        public const String CustomColumnLayoutIsGroupDetailHidden = "IsGroupDetailHidden";
        public const String CustomColumnLayoutIsGroupBlankRowShown = "IsGroupBlankRowShown";
        public const String CustomColumnLayoutDataOrderType = "DataOrderType";
        public const String CustomColumnLayoutIncludeAllPlanograms = "IncludeAllPlanograms";
        public const String CustomColumnLayoutColumnFilters = "ColumnFilters";
        public const String CustomColumnLayoutColumnGroups = "ColumnGroups";
        public const String CustomColumnLayoutColumnSorts = "ColumnSorts";
        public const String CustomColumnLayoutColumns = "Columns";
        
        #endregion

        #region CustomColumn Field Names

        public const String CustomColumnNumber = "Number";
        public const String CustomColumnPath = "Path";
        public const String CustomColumnTotalType = "TotalType";
        public const String CustomColumnWidth = "Width";
        public const String CustomColumnDisplayName = "DisplayName";

        #endregion

        #region CustomColumnFilter Fields

        public const string CustomColumnFilterText = "Text";
        public const string CustomColumnFilterPath = "Path";
        public const string CustomColumnFilterHeader = "Header";

        #endregion

        #region CustomColumnGroup Fields

        public const string CustomColumnGroupGrouping = "Grouping";

        #endregion

        #region CustomColumnSort Fields

        public const string CustomColumnSortPath = "Path";
        public const string CustomColumnSortDirection = "Direction";

        #endregion

        #region FixturePackageInfo

        public const String FixturePackageInfoId = "Id";
        public const String FixturePackageInfoName = "Name";
        public const String FixturePackageInfoDescription = "Description";
        public const String FixturePackageInfoHeight = "Height";
        public const String FixturePackageInfoWidth = "Width";
        public const String FixturePackageInfoDepth = "Depth";
        public const String FixturePackageInfoFixtureCount = "FixtureCount";
        public const String FixturePackageInfoAssemblyCount = "AssemblyCount";
        public const String FixturePackageInfoComponentCount = "ComponentCount";
        public const String FixturePackageInfoThumbnailImageData = "ThumbnailImageData";
        public const String FixturePackageInfoFolderId = "FolderId";
        public const String FixturePackageInfoCustomAttribute1 = "CustomAttribute1";
        public const String FixturePackageInfoCustomAttribute2 = "CustomAttribute2";
        public const String FixturePackageInfoCustomAttribute3 = "CustomAttribute3";
        public const String FixturePackageInfoCustomAttribute4 = "CustomAttribute4";
        public const String FixturePackageInfoCustomAttribute5 = "CustomAttribute5";

        #endregion

        #region Highlight

        public const String HighlightName = "Name";
        public const String HighlightType = "Type";
        public const String HighlightMethodType = "MethodType";
        public const String HighlightIsFilterEnabled = "IsFilterEnabled";
        public const String HighlightIsPreFilter = "IsPreFilter";
        public const String HighlightIsAndFilter = "IsAndFilter";
        public const String HighlightField1 = "Field1";
        public const String HighlightField2 = "Field2";
        public const String HighlightQuadrantXType = "QuadrantXType";
        public const String HighlightQuadrantXConstant = "QuadrantXConstant";
        public const String HighlightQuadrantYType = "QuadrantYType";
        public const String HighlightQuadrantYConstant = "QuadrantYConstant";
        public const String HighlightMajorVersion = "MajorVersion";
        public const String HighlightMinorVersion = "MinorVersion";

        public const String HighlightGroups = "Groups";
        public const String HighlightFilters = "Filters";
        public const String HighlightCharacteristics = "Characteristics";

        #endregion

        #region HighlightCharacteristic

        public const String HighlightCharacteristicName = "Name";
        public const String HighlightCharacteristicIsAndFilter = "IsAndFilter";

        public const String HighlightCharacteristicRules = "Rules";

        #endregion

        #region HighlightCharacteristicRule

        public const String HighlightCharacteristicRuleField = "Field";
        public const String HighlightCharacteristicRuleType = "Type";
        public const String HighlightCharacteristicRuleValue = "Value";

        #endregion

        #region HighlightFilter

        public const String HighlightFilterField = "Field";
        public const String HighlightFilterType = "Type";
        public const String HighlightFilterValue = "Value";

        #endregion

        #region HighlightGroup

        public const String HighlightGroupOrder = "Order";
        public const String HighlightGroupName = "Name";
        public const String HighlightGroupDisplayName = "DisplayName";
        public const String HighlightGroupFillColour = "FillColour";
        public const String HighlightGroupFillPatternType = "FillPatternType";

        #endregion

        #region Label
        public const String LabelName = "Name";
        public const String LabelType = "Type";
        public const String LabelBackgroundColour = "BackgroundColour";
        public const String LabelBorderColour = "BorderColour";
        public const String LabelBorderThickness = "BorderThickness";
        public const String LabelText = "Text";
        public const String LabelTextColour = "TextColour";
        public const String LabelFont = "Font";
        public const String LabelFontSize = "FontSize";
        public const String LabelIsRotateToFitOn = "IsRotateToFitOn";
        public const String LabelIsShrinkToFitOn = "IsShrinkToFitOn";
        public const String LabelTextBoxFontBold = "TextBoxFontBold";
        public const String LabelTextBoxFontItalic = "TextBoxFontItalic";
        public const String LabelTextBoxFontUnderlined = "TextBoxFontUnderlined";
        public const String LabelLabelHorizontalPlacement = "LabelHorizontalPlacement";
        public const String LabelLabelVerticalPlacement = "LabelVerticalPlacement";
        public const String LabelTextBoxTextAlignment = "TextBoxTextAlignment";
        public const String LabelTextDirection = "TextDirection";
        public const String LabelTextWrapping = "TextWrapping";
        public const String LabelBackgroundTransparency = "BackgroundTransparency";
        public const String LabelShowOverImages = "ShowOverImages";
        public const String LabelShowLabelPerFacing = "ShowLabelPerFacing";
        public const String LabelMajorVersion = "MajorVersion";
        public const String LabelMinorVersion = "MinorVersion";

        #endregion

        #region PlanogramComparisonTemplate

        public const String PlanogramComparisonTemplateName = "Name";
        public const String PlanogramComparisonTemplateDescription = "Description";
        public const String PlanogramComparisonTemplateIgnoreNonPlacedProducts = "IgnoreNonPlacedProducts";
        public const String PlanogramComparisonTemplateDataOrderType = "DataOrderType";
        public const String PlanogramComparisonTemplateDateCreated = "DateCreated";
        public const String PlanogramComparisonTemplateDateLastModified = "DateLastModified";
        public const String PlanogramComparisonTemplateComparisonFields = "ComparisonFields";

        #endregion

        #region PlanogramComparisonTemplateField

        public const String PlanogramComparisonTemplateFieldItemType = "ItemType";
        public const String PlanogramComparisonTemplateFieldFieldPlaceholder = "FieldPlaceholder";
        public const String PlanogramComparisonTemplateFieldDisplayName = "DisplayName";
        public const String PlanogramComparisonTemplateFieldNumber = "Number";
        public const String PlanogramComparisonTemplateFieldDisplay = "Display";

        #endregion

        #region PlanogramExportTemplate
        public const String PlanogramExportTemplateName = "Name";
        public const String PlanogramExportTemplateFileType = "FileType";
        public const String PlanogramExportTemplateFileVersion = "FileVersion";
        public const String PlanogramExportTemplateMajorVersion = "MajorVersion";
        public const String PlanogramExportTemplateMinorVersion = "MinorVersion";
        public const String PlanogramExportTemplateMappings = "Mappings";
        public const String PlanogramExportTemplatePerformanceMetrics = "PerformanceMetrics";
        #endregion

        #region PlanogramExportTemplateMapping
        public const String PlanogramExportTemplateMappingFieldType = "FieldType";
        public const String PlanogramExportTemplateMappingField = "Field";
        public const String PlanogramExportTemplateMappingExternalField = "ExternalField";
        #endregion

        #region PlanogramExportTemplatePerformanceMetric
        public const String PlanogramExportTemplatePerformanceMetricName = "Name";
        public const String PlanogramExportTemplatePerformanceMetricDescription = "Description";
        public const String PlanogramExportTemplatePerformanceMetricDirection = "Direction";
        public const String PlanogramExportTemplatePerformanceMetricSpecialType = "SpecialType";
        public const String PlanogramExportTemplatePerformanceMetricMetricType = "MetricType";
        public const String PlanogramExportTemplatePerformanceMetricMetricId = "MetricId";
        public const String PlanogramExportTemplatePerformanceMetricExternalField = "ExternalField";
        #endregion

        #region PlanogramImportTemplate
        public const String PlanogramImportTemplateName = "Name";
        public const String PlanogramImportTemplateFileType = "FileType";
        public const String PlanogramImportTemplateFileVersion = "FileVersion";
        public const String PlanogramImportTemplateMajorVersion = "MajorVersion";
        public const String PlanogramImportTemplateMinorVersion = "MinorVersion";
        public const String PlanogramImportTemplateMappings = "Mappings";
        public const String PlanogramImportTemplatePerformanceMetrics = "PerformanceMetrics";
        public const String PlanogramImportTemplateAllowAdvancedBaySplitting = "AllowAdvancedBaySplitting";
        public const String PlanogramImportTemplateCanSplitComponents = "CanSplitComponents";
        public const String PlanogramImportTemplateSplitBaysByComponentStart = "SplitBaysByComponentStart";
        public const String PlanogramImportTemplateSplitBaysByComponentEnd = "SplitBaysByComponentEnd";
        public const String PlanogramImportTemplateSplitByFixedSize = "SplitByFixedSize";
        public const String PlanogramImportTemplateSplitByRecurringPattern = "SplitByRecurringPattern";
        public const String PlanogramImportTemplateSplitByBayCount = "SplitByBayCount";
        public const String PlanogramImportTemplateSplitFixedSize = "SplitFixedSize";
        public const String PlanogramImportTemplateSplitRecurringPattern = "SplitRecurringPattern";
        public const String PlanogramImportTemplateSplitBayCount = "SplitBayCount";
        #endregion

        #region PlanogramImportTemplateMapping
        public const String PlanogramImportTemplateMappingFieldType = "FieldType";
        public const String PlanogramImportTemplateMappingField = "Field";
        public const String PlanogramImportTemplateMappingExternalField = "ExternalField";
        #endregion

        #region PlanogramImportTemplatePerformanceMetric
        public const String PlanogramImportTemplatePerformanceMetricName = "Name";
        public const String PlanogramImportTemplatePerformanceMetricDescription = "Description";
        public const String PlanogramImportTemplatePerformanceMetricDirection = "Direction";
        public const String PlanogramImportTemplatePerformanceMetricSpecialType = "SpecialType";
        public const String PlanogramImportTemplatePerformanceMetricMetricType = "MetricType";
        public const String PlanogramImportTemplatePerformanceMetricMetricId = "MetricId";
        public const String PlanogramImportTemplatePerformanceMetricExternalField = "ExternalField";
        #endregion

        #region PrintTemplate
        public const String PrintTemplateName = "Name";
        public const String PrintTemplateDescription = "Description";
        public const String PrintTemplatePaperSize = "PaperSize";
        public const String PrintTemplateIsPlanMirrored = "IsPlanMirrored";
        public const String PrintTemplateDateCreated = "DateCreated";
        public const String PrintTemplateDateLastModified = "DateLastModified";
        public const String PrintTemplateSectionGroups = "SectionGroups";
        #endregion

        #region PrintTemplateComponent
        public const String PrintTemplateComponentType = "Type";
        public const String PrintTemplateComponentX = "X";
        public const String PrintTemplateComponentY = "Y";
        public const String PrintTemplateComponentHeight = "Height";
        public const String PrintTemplateComponentWidth = "Width";
        public const String PrintTemplateComponentComponentDetails = "ComponentDetails";
        #endregion

        #region PrintTemplateComponentDetail
        public const String PrintTemplateComponentDetailKey = "Key";
        public const String PrintTemplateComponentDetailValue = "Value";
        #endregion

        #region PrintTemplateSection
        public const String PrintTemplateSectionNumber = "Number";
        public const String PrintTemplateSectionOrientation = "Orientation";
        public const String PrintTemplateSectionComponents = "Components";
        #endregion

        #region PrintTemplateSectionGroup
        public const String PrintTemplateSectionGroupNumber = "Number";
        public const String PrintTemplateSectionGroupName = "Name";
        public const String PrintTemplateSectionGroupBaysPerPage = "BaysPerPage";
        public const String PrintTemplateSectionGroupSections = "Sections";
        #endregion

        #region Product Library

        public const String ProductLibraryFileType = "FileType";
        public const String ProductLibrarySourceFilePath = "SourceFilePath";
        public const String ProductLibraryExcelWorkbookSheet = "ExcelWorkbookSheet";
        public const String ProductLibraryStartAtRow = "StartAtRow";
        public const String ProductLibraryIsFirstRowHeaders = "IsFirstRowHeaders";
        public const String ProductLibraryTextDataFormat = "TextDataFormat";
        public const String ProductLibraryTextDelimiterType = "TextDelimiterType";
        public const String ProductLibraryColumnMappings = "ColumnMappings";

        #endregion

        #region ProductLibraryColumnMapping

        public const String ProductLibraryColumnMappingProductLibraryId = "ProductLibraryId";
        public const String ProductLibraryColumnMappingSource = "Source";
        public const String ProductLibraryColumnMappingDestination = "Destination";
        public const String ProductLibraryColumnMappingIsMandatory = "IsMandatory";
        public const String ProductLibraryColumnMappingType = "Type";
        public const String ProductLibraryColumnMappingMin = "Min";
        public const String ProductLibraryColumnMappingMax = "Max";
        public const String ProductLibraryColumnMappingCanDefault = "CanDefault";
        public const String ProductLibraryColumnMappingDefault = "Default";
        public const String ProductLibraryColumnMappingTextFixedWidthColumnStart = "TextFixedWidthColumnStart";
        public const String ProductLibraryColumnMappingTextFixedWidthColumnEnd = "TextFixedWidthColumnEnd";

        #endregion

        #region RecentPlanogram

        public const String RecentPlanogramName = "Name";
        public const String RecentPlanogramPath = "Path";
        public const String RecentPlanogramType = "Type";
        public const String RecentPlanogramDateLastAccessed = "DateLastAccessed";
        public const String RecentPlanogramIsPinned = "IsPinned";
        public const String RecentPlanogramLocation = "Location";

        #endregion

        #region ReportColumnLayoutSetting

        public const String ReportColumnLayoutSettingScreenkey = "ScreenKey";
        public const String ReportColumnLayoutSettingColumnLayoutName = "ColumnLayoutName";

        #endregion

        #region ReportColumnLayout

        public const String ReportColumnLayoutType = "ReportLayoutType";
        public const String ReportColumnLayoutName = "Name";

        #endregion

        #region ReportColumn Field Names

        public const string ReportColumnOrder = "Number";
        public const string ReportColumnPath = "Path";
        public const string ReportColumnTotalType = "TotalType";

        #endregion

        #region ReportColumnFilter Fields

        public const string ReportColumnFilterText = "Text";
        public const string ReportColumnFilterPath = "Path";
        public const string ReportColumnFilterHeader = "Header";

        #endregion

        #region ReportColumnGroup Fields

        public const string ReportColumnGroupGrouping = "Grouping";

        #endregion

        #region ReportColumnSort Fields

        public const string ReportColumnSortPath = "Path";
        public const string ReportColumnSortDirection = "Direction";

        #endregion

        #region SystemSetting

        public const String SystemSettingKey = "Key";
        public const String SystemSettingValue = "Value";

        #endregion

        #region SystemSettingColor

        public const String SystemSettingColor = "Color";

        #endregion

        #region SystemSettingRecentLabel
        public const String SystemSettingRecentLabelName = "RecentLabelName";
        public const String SystemSettingRecentLabelType = "RecentLabelType";
        public const String SystemSettingRecentLabelSaveType = "RecentLabelSaveType";
        public const String SystemSettingRecentLabelId = "RecentLabelId";
        #endregion

        #region SystemSettingSelectedColumn
        public const String SystemSettingSelectedColumnFieldPlaceHolder = "SelectedColumnFieldPlaceHolder";
        public const String SystemSettingSelectedColumnColumnType = "SelectedColumnColumnType";
        #endregion

        #region SystemSettingRecentHighlight
        public const String SystemSettingRecentHighlightId = "RecentHighlightId";
        public const String SystemSettingRecentHighlightSaveType = "RecentHighlightSaveType";

        #endregion

        #region SystemSettingRecentDatasheet
        public const String SystemSettingRecentDatasheetId = "RecentDatasheetId";
        
        #endregion
        #region UserSystemSetting

        public const String UserSystemSettingDisplayLanguage = "DisplayLanguage";
        public const String UserSystemSettingDisplayCEIPWindow = "DisplayCEIPWindow";
        public const String UserSystemSettingSendCEIPInformationDefault = "SendCEIPInformationDefault";
        public const String UserSystemSettingIsDatabaseSelectionRemembered = "IsDatabaseSelectionRemembered";
        public const String UserSystemSettingIsEntitySelectionRemembered = "IsEntitySelectionRemembered";
        public const String UserSystemSettingPlanAssignMentViewBy = "PlanAssignmentViewBy";
        public const String UserSystemSettingConnections = "Connections";
        public const String UserSystemSettingColumnLayouts = "ColumnLayouts";

        #endregion

        #region ValidationTemplate Fields

        public const String ValidationTemplateName = "Name";
        public const String ValidationTemplateMajorVersion = "MajorVersion";
        public const String ValidationTemplateMinorVersion = "MinorVersion";
        public const String ValidationTemplateGroups = "Groups";

        #endregion

        #region ValidationTemplateGroup Fields

        public const String ValidationTemplateGroupName = "Name";
        public const String ValidationTemplateGroupThreshold1 = "Threshold1";
        public const String ValidationTemplateGroupThreshold2 = "Threshold2";
        public const String ValidationTemplateGroupMetrics = "Metrics";
        public const String ValidationTemplateGroupValidationType = "ValidationType";

        #endregion

        #region ValidationTemplateMetric Fields

        public const String ValidationTemplateMetricThreshold1 = "Threshold1";
        public const String ValidationTemplateMetricThreshold2 = "Threshold2";
        public const String ValidationTemplateMetricField = "Field";
        public const String ValidationTemplateMetricAggregationType = "AggregationType";
        public const String ValidationTemplateMetricScore1 = "Score1";
        public const String ValidationTemplateMetricScore2 = "Score2";
        public const String ValidationTemplateMetricScore3 = "Score3";
        public const String ValidationTemplateMetricValidationType = "ValidationType";
        public const String ValidationTemplateMetricCriteria = "Criteria";

        #endregion

    }
}
