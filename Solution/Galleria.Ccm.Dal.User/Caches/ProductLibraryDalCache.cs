﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (V8.3)
// V8-32361 : L.Ineson
//  Added lock
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our Product Library dal cache
    /// </summary>
    internal class ProductLibraryDalCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private Object _lock = new Object();

        private List<ProductLibraryItem> _items;
        private readonly List<ProductLibraryItem> _deletedItems = new List<ProductLibraryItem>();
        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of ProductLibraries
        /// </summary>
        public IEnumerable<ProductLibraryItem> ProductLibraries
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<ProductLibraryItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public ProductLibraryDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File Methods

        /// <summary>
        /// locks the printTemplate with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            ProductLibraryItem dalObject = this.ProductLibraries.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = ProductLibraryItem.FetchById(id, /*asReadOnly*/false);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        /// Unlocks and removes the printTemplate with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            ProductLibraryItem dalObject = this.ProductLibraries.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
            else
            {
                Debug.WriteLine("Failed to unlock file: " + id);
            }
        }

        /// <summary>
        /// Returns the item with the given id
        /// or fetches and adds it if required
        /// Note this this should only be called by the Hihglight dal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductLibraryItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            ProductLibraryItem dalObject = this.ProductLibraries.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }


            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = ProductLibraryItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region ProductLibraryItem

        /// <summary>
        /// Returns the item from the cache with the given id.
        /// </summary>
        public ProductLibraryItem FindProductLibraryById(String id)
        {
            return this.ProductLibraries.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ProductLibraryItem InsertOrUpdate(ProductLibraryDto dto)
        {
            if (this.ProductLibraries == null) return null;

            String id = dto.Id as String;
            ProductLibraryItem item = this.ProductLibraries.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id =
                        Path.ChangeExtension(
                            Path.Combine(_unitTestDirectory, dto.Name),
                            Constants.ProductLibraryFileExtension);
                }

                //Insert.
                item = new ProductLibraryItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(ProductLibraryItem item)
        {
            if (ProductLibraries.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }


        #endregion

        #region ProductLibraryColumnMappingItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public ProductLibraryColumnMappingItem InsertOrUpdate(ProductLibraryColumnMappingDto dto)
        {
            if (this.ProductLibraries == null) return null;

            ProductLibraryColumnMappingItem item = null;

            ProductLibraryItem productLibrary = FindProductLibraryById(Convert.ToString(dto.ProductLibraryId));
            if (productLibrary != null)
            {
                item = productLibrary.ColumnMappings.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    //insert
                    item = productLibrary.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Gets the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductLibraryColumnMappingItem FindProductLibraryColumnMappingById(Int32 id)
        {
            foreach (ProductLibraryItem h in this.ProductLibraries)
            {
                foreach (ProductLibraryColumnMappingItem f in h.ColumnMappings)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(ProductLibraryColumnMappingItem item)
        {
            if (this.ProductLibraries == null || ProductLibraries == null) return;

            ProductLibraryItem library = FindProductLibraryById(item.ProductLibraryId);
            if (library != null)
            {
                library.ColumnMappings.Remove(item);
                library.MarkDirty();
            }

        }

        #endregion

        /// <summary>
        /// Unlocks all printTemplates in this cache
        /// </summary>
        private void ClearCache()
        {
            foreach (ProductLibraryItem h in this.ProductLibraries)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (ProductLibraryItem h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (ProductLibraryItem h in _deletedItems)
            {
                h.Delete();
            }
            _deletedItems.Clear();


            //save any changed ProductLibraries.
            // and drop any readonly items.
            if (_items != null)
            {
                foreach (ProductLibraryItem h in _items.ToArray())
                {
                    if (h.IsDirty && !h.IsReadOnly)
                    {
                        h.Save();
                    }

                    if (h.IsReadOnly)
                    {
                        UnlockFileById(h.Id);
                    }
                }
            }
        }

        /// <summary>
        /// Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            List<ProductLibraryItem> oldProductLibraries = this.ProductLibraries.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old printTemplates unlocking files and reloading.
            List<String> reloadIds = new List<String>();
            foreach (ProductLibraryItem old in oldProductLibraries)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ClearCache();
                    _items = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
