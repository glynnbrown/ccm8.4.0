﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25559 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our UserSystemSetting dal cache
    /// This cache writes to the CCMUsers.xml file.
    /// </summary>
    internal class UserSystemSettingDalCache : IDalCache
    {
        #region Fields
        private readonly String _filePath;

        private FileStream _stream;
        private UserSystemSettingItem _item;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the item currently held by this cache.
        /// </summary>
        public UserSystemSettingItem Item
        {
            get
            {
                return _item;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalFactory"></param>
        public UserSystemSettingDalCache(DalFactory dalFactory)
        {
            //Determine the file path.
            if (dalFactory.IsUnitTesting)
            {
                _filePath = Path.Combine(dalFactory.UnitTestFolder, Constants.AppConnectionSettingsFileName);
            }
            else
            {
                _filePath =
               Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                  Constants.AppDataFolderName, Constants.AppConnectionSettingsFileName);
            }

            //Fetch data.
            Fetch();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refetches the data.
        /// </summary>
        private void Fetch()
        {
            //first unlock the existing stream as we are going
            // to start a new one.
            Unlock();

            try
            {
                //Get a new steem and load the list.
                FileStream stream = GetStream();
                if (stream.Length > 0)
                {
                    XmlSerializer x = new XmlSerializer(typeof(UserSystemSettingItem));
                    _item = (UserSystemSettingItem)x.Deserialize(stream);

                    //update connection Ids
                    
                    foreach (ConnectionItem conn in _item.Connections)
                    {
                        conn.Id = ConnectionItem.GetNextId();
                    }
                }
                else
                {
                    _item = new UserSystemSettingItem();
                }
            }
            catch (Exception ex)
            {
                Unlock();
                throw ex;
            }
        }

        /// <summary>
        /// Unlocks the current filestream.
        /// </summary>
        private void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;

                ConnectionItem.ResetId();
            }
        }

        /// <summary>
        /// Returns the existing file stream or
        /// creates a new one if required.
        /// </summary>
        /// <returns></returns>
        private FileStream GetStream()
        {
            if (_stream == null)
            {
                String filePath = _filePath;

                //ensure the directory exists
                String directory = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                _stream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            }

            return _stream;
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits the current transaction changes.
        /// </summary>
        public void Commit()
        {
            if (_item != null)
            {
                FileStream stream = GetStream();

                stream.SetLength(0);
                stream.Flush();

                XmlSerializer x = new XmlSerializer(typeof(UserSystemSettingItem));
                x.Serialize(stream, _item);
            }
        }

        /// <summary>
        /// Rollsback transaction changes.
        /// </summary>
        public void Rollback()
        {
            Fetch();
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Unlock();
                    _item = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }

    
}
