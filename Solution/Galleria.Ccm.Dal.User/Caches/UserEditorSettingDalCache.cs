﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-24863 L.Hodson
//  Created
#endregion

#region Version History: CCM 801 (Critical Patch 1)

// V8-29070: A.Silva
//      Added RemoveDuplicateKeys

#endregion

#region Version History: CCM 820
// V8-30791: D.Pleasance
//      Added Colors
#endregion

#region Version History CCM 830 
// V8-31699 : A.Heathcote
//      Added RecentLabel Highlight and Datasheet to the Properties and Methods regions 
// V8-31934 : A.Heathcote
//      Added UserEditorSettingsSelectedColumn(Item, Dto)  
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
	/// <summary>
	/// Our SystemSetting dal cache
	/// </summary>
	internal class UserEditorSettingDalCache : IDalCache
	{
		#region Fields

		private readonly String _filePath;

		private FileStream _stream;
		private UserEditorSettingList _list;
		private Int32 _nextId = 1;

		#endregion

		#region Properties

		/// <summary>
		/// Returns the items currently held by this cache.
		/// </summary>
		public IEnumerable<UserEditorSettingItem> Items
		{
			get
			{
				if (_list == null && !_isDisposed)
				{
					Fetch();
				}
				return _list.Items;
			}
		}

		/// <summary>
		/// Returns the items currently held by this cache.
		/// </summary>
		public IEnumerable<UserEditorSettingColorItem> Colors
		{
			get
			{
				if (_list == null && !_isDisposed)
				{
					Fetch();
				}
				return _list.Colors;
			}
		}

		public IEnumerable<UserEditorSettingsRecentLabelItem> RecentLabel
		{
			get
			{
				if (_list == null && !_isDisposed)
				{
					Fetch();
				}
				return _list.RecentLabel;
			}
		}

		public IEnumerable<UserEditorSettingsRecentHighlightItem> RecentHighlight
		{
			get
			{
				if (_list == null && !_isDisposed)
				{
					Fetch();
				}
				return _list.RecentHighlight;
			}
		}

		public IEnumerable<UserEditorSettingsRecentDatasheetItem> RecentDatasheet
		{
			get
			{
				if (_list == null && !_isDisposed)
				{
					Fetch();
				}
				return _list.RecentDatasheet;
			}
		}

        public IEnumerable<UserEditorSettingsSelectedColumnItem> SelectedColumn
        {
            get
            {
                if (_list == null && !_isDisposed)
                {
                    Fetch();
                }
                return _list.SelectedColumn;
            }
        }

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new instance of this type
		/// </summary>
		/// <param name="dalFactory"></param>
		public UserEditorSettingDalCache(DalFactory dalFactory)
		{
			//Determine the file path.
			if (dalFactory.IsUnitTesting)
			{
				_filePath = Path.Combine(dalFactory.UnitTestFolder, Constants.EditorUserSettingsFileName);
			}
			else
			{
				_filePath =
			   Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
				  Constants.AppDataFolderName, Constants.EditorUserSettingsFileName);
			}

			//Fetch data.
			Fetch();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Refetches the data.
		/// </summary>
		private void Fetch()
		{
			//first unlock the existing stream as we are going
			// to start a new one.
			Unlock();

			try
			{
				//Get a new steem and load the list.
				FileStream stream = GetStream();
				if (stream.Length > 0)
				{
					XmlSerializer x = new XmlSerializer(typeof(UserEditorSettingList));
					_list = (UserEditorSettingList)x.Deserialize(stream);

					RemoveDuplicateKeys();

					Int32 id = 1;
					foreach (var item in _list.Items)
					{
						item.Id = id;
						id++;
					}

					id = 1;
					foreach (var item in _list.Colors)
					{
						item.Id = id;
						id++;
					}

					id = 1;
					foreach (var item in _list.RecentLabel)
					{
						item.Id = id;
						id++;
					}
				}
				else
				{
					_list = new UserEditorSettingList();
				}
			}
			catch (Exception ex)
			{
				Unlock();

				if (ex.GetType() == typeof(InvalidOperationException))
				{
					File.Delete(_filePath);

					//just return a new list.
					_list = new UserEditorSettingList();
				}
				else
				{
					throw ex;
				}
			}

			//unlock immediately as we don't want to hold
			// onto this file.
			Unlock();
		}

		/// <summary>
		/// Unlocks the current filestream.
		/// </summary>
		private void Unlock()
		{
			if (_stream != null)
			{
				_stream.Close();
				_stream.Dispose();
				_stream = null;
			}
		}

		/// <summary>
		/// Returns the existing file stream or
		/// creates a new one if required.
		/// </summary>
		/// <returns></returns>
		private FileStream GetStream()
		{
			if (_stream == null)
			{
				String filePath = _filePath;

				//ensure the directory exists
				String directory = Path.GetDirectoryName(filePath);
				if (!Directory.Exists(directory))
				{
					Directory.CreateDirectory(directory);
				}

				_stream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
			}

			return _stream;
		}

		/// <summary>
		/// Inserts the given dto
		/// </summary>
		/// <param name="dto"></param>
		/// <returns></returns>
		internal UserEditorSettingItem Insert(UserEditorSettingDto dto)
		{
			//set the dto id
			dto.Id = _nextId;

			//create the item
			UserEditorSettingItem item = new UserEditorSettingItem(dto);
			_list.Items.Add(item);

			//increment ready for the next item.
			_nextId++;

			return item;
		}

		/// <summary>
		/// Removes the given item.
		/// </summary>
		/// <param name="item"></param>
		internal void Delete(UserEditorSettingItem item)
		{
			_list.Items.Remove(item);
		}

		/// <summary>
		/// Inserts the given dto
		/// </summary>
		/// <param name="dto"></param>
		/// <returns></returns>
		internal UserEditorSettingColorItem Insert(UserEditorSettingColorDto dto)
		{
			//set the dto id
			dto.Id = _nextId;

			//create the item
			UserEditorSettingColorItem item = new UserEditorSettingColorItem(dto);
			_list.Colors.Add(item);

			//increment ready for the next item.
			_nextId++;

			return item;
		}

		internal UserEditorSettingsRecentLabelItem Insert(UserEditorSettingsRecentLabelDto dto)
		{
			//set the dto id
			dto.Id = _nextId;

			//create the item
			UserEditorSettingsRecentLabelItem item = new UserEditorSettingsRecentLabelItem(dto);
			_list.RecentLabel.Add(item);

			//increment ready for the next item.
			_nextId++;

			return item;
		}

		internal UserEditorSettingsRecentHighlightItem Insert(UserEditorSettingsRecentHighlightDto dto)
		{
			dto.Id = _nextId;
		   UserEditorSettingsRecentHighlightItem item = new UserEditorSettingsRecentHighlightItem(dto);
			_list.RecentHighlight.Add(item);
			_nextId++;
			return item;
		}

		internal UserEditorSettingsRecentDatasheetItem Insert(UserEditorSettingsRecentDatasheetDto dto)
		{
			dto.Id = _nextId;
			UserEditorSettingsRecentDatasheetItem item = new UserEditorSettingsRecentDatasheetItem(dto);
			_list.RecentDatasheet.Add(item);
			_nextId++;
			return item;
		}

        internal UserEditorSettingsSelectedColumnItem Insert(UserEditorSettingsSelectedColumnDto dto)
        {
            dto.Id = _nextId;
            UserEditorSettingsSelectedColumnItem item = new UserEditorSettingsSelectedColumnItem(dto);
            _list.SelectedColumn.Add(item);
            _nextId++;
            return item;
        }
		/// <summary>
		/// Removes the given item.
		/// </summary>
		/// <param name="item"></param>
		internal void Delete(UserEditorSettingColorItem item)
		{
			_list.Colors.Remove(item);
		}

		internal void Delete(UserEditorSettingsRecentLabelItem item)
		{
			_list.RecentLabel.Remove(item);
		}

		internal void Delete(UserEditorSettingsRecentHighlightItem item)
		{
			_list.RecentHighlight.Remove(item);
		}

		internal void Delete(UserEditorSettingsRecentDatasheetItem item)
		{
			_list.RecentDatasheet.Remove(item);
		}

        internal void Delete(UserEditorSettingsSelectedColumnItem item)
        {
            _list.SelectedColumn.Remove(item);
        }
		/// <summary>
		///     Remove any duplicate keys in the setting items.
		/// </summary>
		/// <remarks>V8-29069: Make sure there are no duplicate keys in case some where created after a version update.</remarks>
		private void RemoveDuplicateKeys()
		{
			ILookup<String, UserEditorSettingItem> lookup = _list.Items.ToLookup(item => item.Key, item => item);
			if (!lookup.Any(items => items.Count() > 1)) return;

			foreach (UserEditorSettingItem item in lookup.SelectMany(items => items.Skip(1)))
			{
				_list.Items.Remove(item);
			}
		}

		#endregion

		#region IDalCache Members

		/// <summary>
		/// Commits the current transaction changes.
		/// </summary>
		public void Commit()
		{
			if (_list != null)
			{
				FileStream stream = GetStream();

				stream.SetLength(0);
				stream.Flush();

				XmlSerializer x = new XmlSerializer(typeof(UserEditorSettingList));
				x.Serialize(stream, _list);

				//unlock the stream so that the file is not held open.
				Unlock();
			}
		}

		/// <summary>
		/// Rollsback transaction changes.
		/// </summary>
		public void Rollback()
		{
			Fetch();
		}

		#endregion

		#region IDisposable

		private Boolean _isDisposed;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(Boolean disposing)
		{
			if (!_isDisposed)
			{
				if (disposing)
				{
					Unlock();
					_list = null;
				}
				_isDisposed = true;
			}
		}

		#endregion
	}
}
