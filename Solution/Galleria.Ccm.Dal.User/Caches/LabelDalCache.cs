﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 J.Pickup
//  Created
// CCM-24265 : N.Haywood
//  Changed so that all files are not locked and only locked files are stored in the list
// V8-26248 : L.Luong
//  Changed if writing over a new file it updated instead
// V8-28362 : L.Ineson
//  Reworked to allow readonly.
// V8-28579 : D.Pleasance
//  Amended LockFileById so that fetch opens as readonly
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;
using System.IO;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our Labels dal cache
    /// </summary>
    internal class LabelDalCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private Object _lock = new Object();

        private List<LabelItem> _items;
        private readonly List<LabelItem> _deletedItems = new List<LabelItem>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of Labels.
        /// </summary>
        public IEnumerable<LabelItem> Items
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<LabelItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public LabelDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File

        /// <summary>
        /// locks the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            LabelItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = LabelItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        /// Unlocks and removes the label with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            LabelItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
        }

        /// <summary>
        /// Returns the item with the given id
        /// or fetches and adds it if required
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LabelItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            LabelItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = LabelItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region LabelItem
        
        /// <summary>
        /// Returns the label item with the given id from the cache.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LabelItem FindLabelById(String id)
        {
            return this.Items.FirstOrDefault(l => l.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public LabelItem InsertOrUpdate(LabelDto dto)
        {
            if (this.Items == null) return null;

            String id = dto.Id as String;
            LabelItem item = this.Items.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id = Path.ChangeExtension(
                        Path.Combine(_unitTestDirectory, dto.Name), 
                        Constants.LabelFileExtension);
                }

                //Insert.
                item = new LabelItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(LabelItem item)
        {
            if (Items.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }

        #endregion

        /// <summary>
        /// Unlocks all highlights in this cache
        /// </summary>
        private void ClearCache()
        {
            foreach (var h in this.Items)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (var h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (LabelItem l in _deletedItems)
            {
                l.Delete();
            }
            _deletedItems.Clear();


            //save any changed highlights.
            if (_items != null)
            {
                foreach (LabelItem l in _items)
                {
                    if (l.IsDirty)
                    {
                        l.Save(_unitTestDirectory);
                    }
                }
            }
        }

        /// <summary>
        /// Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            List<LabelItem> oldLabels = this.Items.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old highlights unlocking files and reloading.
            List<String> reloadIds = new List<String>();
            foreach (LabelItem old in oldLabels)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ClearCache();
                    _items = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}