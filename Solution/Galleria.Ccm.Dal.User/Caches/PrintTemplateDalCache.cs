﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-30738 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;
using System.Diagnostics;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our PrintTemplates dal cache
    /// </summary>
    internal class PrintTemplateDalCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private Object _lock = new Object();

        private List<PrintTemplateItem> _items;
        private readonly List<PrintTemplateItem> _deletedItems = new List<PrintTemplateItem>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of printTemplates.
        /// </summary>
        public IEnumerable<PrintTemplateItem> PrintTemplates
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<PrintTemplateItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public PrintTemplateDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File Methods

        /// <summary>
        /// locks the printTemplate with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            PrintTemplateItem dalObject = this.PrintTemplates.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = PrintTemplateItem.FetchById(id, /*asReadOnly*/false);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        /// Unlocks and removes the printTemplate with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            PrintTemplateItem dalObject = this.PrintTemplates.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
            else
            {
                Debug.WriteLine("Failed to unlock file: " + id);
            }
        }

        /// <summary>
        /// Returns the item with the given id
        /// or fetches and adds it if required
        /// Note this this should only be called by the Hihglight dal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PrintTemplateItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            PrintTemplateItem dalObject = this.PrintTemplates.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }


            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = PrintTemplateItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region PrintTemplateItem

        /// <summary>
        /// Returns the item from the cache with the given id.
        /// </summary>
        public PrintTemplateItem FindPrintTemplateById(String id)
        {
            if (!String.IsNullOrEmpty(_unitTestDirectory))
            {
                id = _unitTestDirectory +"\\"+ id + Constants.PrintTemplateFileExtension;
            }

            return this.PrintTemplates.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public PrintTemplateItem InsertOrUpdate(PrintTemplateDto dto)
        {
            if (this.PrintTemplates == null) return null;

            String id = dto.Id as String;
            PrintTemplateItem item = this.PrintTemplates.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id = Path.ChangeExtension(Path.Combine(_unitTestDirectory, (this.PrintTemplates.Count() + 1).ToString()), Constants.PrintTemplateFileExtension);
                }

                //Insert.
                item = new PrintTemplateItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PrintTemplateItem item)
        {
            if (PrintTemplates.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }

        #endregion

        #region PrintTemplateSectionGroupItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public PrintTemplateSectionGroupItem InsertOrUpdate(PrintTemplateSectionGroupDto dto)
        {
            if (this.PrintTemplates == null) return null;

            PrintTemplateSectionGroupItem item = null;

            PrintTemplateItem printTemplate = FindPrintTemplateById(Convert.ToString(dto.PrintTemplateId));
            if (printTemplate != null)
            {
                item = printTemplate.SectionGroups.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    //insert
                    item = printTemplate.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Gets the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PrintTemplateSectionGroupItem FindPrintTemplateSectionGroupById(Int32 id)
        {
            foreach (PrintTemplateItem h in this.PrintTemplates)
            {
                foreach (PrintTemplateSectionGroupItem f in h.SectionGroups)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PrintTemplateSectionGroupItem item)
        {
            if (this.PrintTemplates == null || PrintTemplates == null) return;

            PrintTemplateItem printTemplate = FindPrintTemplateById(item.PrintTemplateId);
            if (printTemplate != null)
            {
                printTemplate.SectionGroups.Remove(item);
                printTemplate.MarkDirty();
            }

        }

        #endregion

        #region PrintTemplateSectionItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public PrintTemplateSectionItem InsertOrUpdate(PrintTemplateSectionDto dto)
        {
            if (this.PrintTemplates == null) return null;

            PrintTemplateSectionItem item = null;

            PrintTemplateSectionGroupItem parent = FindPrintTemplateSectionGroupById(dto.PrintTemplateSectionGroupId);
            if (parent != null)
            {
                item = parent.Sections.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    item = parent.Insert(dto);
                }
                else
                {
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PrintTemplateSectionItem FindPrintTemplateSectionById(Int32 id)
        {
            foreach (PrintTemplateItem h in this.PrintTemplates)
            {
                foreach (PrintTemplateSectionGroupItem f in h.SectionGroups)
                {
                    foreach (PrintTemplateSectionItem r in f.Sections)
                    {
                        if (r.Id == id)
                        {
                            return r;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PrintTemplateSectionItem item)
        {
            PrintTemplateSectionGroupItem parent = FindPrintTemplateSectionGroupById(item.PrintTemplateSectionGroupId);
            if (parent != null)
            {
                parent.Sections.Remove(item);
                parent.MarkDirty();
            }
        }

        #endregion

        #region PrintTemplateComponentItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public PrintTemplateComponentItem InsertOrUpdate(PrintTemplateComponentDto dto)
        {
            if (this.PrintTemplates == null) return null;

            PrintTemplateComponentItem item = null;

            PrintTemplateSectionItem parent = FindPrintTemplateSectionById(dto.PrintTemplateSectionId);
            if (parent != null)
            {
                item = parent.Components.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    item = parent.Insert(dto);
                }
                else
                {
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        public PrintTemplateComponentItem FindPrintTemplateComponentById(Int32 id)
        {
            foreach (PrintTemplateItem h in this.PrintTemplates)
            {
                foreach (PrintTemplateSectionGroupItem f in h.SectionGroups)
                {
                    foreach (PrintTemplateSectionItem section in f.Sections)
                    {
                        foreach (PrintTemplateComponentItem r in section.Components)
                        {
                            if (r.Id == id)
                            {
                                return r;
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PrintTemplateComponentItem item)
        {
            PrintTemplateSectionItem parent = FindPrintTemplateSectionById(item.PrintTemplateSectionId);
            if (parent != null)
            {
                parent.Components.Remove(item);
                parent.MarkDirty();
            }
        }

        #endregion

        #region PrintTemplateComponentDetailItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public PrintTemplateComponentDetailItem InsertOrUpdate(PrintTemplateComponentDetailDto dto)
        {
            if (this.PrintTemplates == null) return null;

            PrintTemplateComponentDetailItem item = null;

            PrintTemplateComponentItem parent = FindPrintTemplateComponentById(dto.PrintTemplateComponentId);
            if (parent != null)
            {
                item = parent.ComponentDetails.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    item = parent.Insert(dto);
                }
                else
                {
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        public PrintTemplateComponentDetailItem FindPrintTemplateComponentDetailById(Int32 id)
        {
            foreach (PrintTemplateItem h in this.PrintTemplates)
            {
                foreach (PrintTemplateSectionGroupItem f in h.SectionGroups)
                {
                    foreach (PrintTemplateSectionItem s in f.Sections)
                    {
                        foreach (PrintTemplateComponentItem c in s.Components)
                        {
                            foreach (PrintTemplateComponentDetailItem r in c.ComponentDetails)
                            {
                                if (r.Id == id)
                                {
                                    return r;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        public void Delete(PrintTemplateComponentDetailItem item)
        {
            PrintTemplateComponentItem parent = FindPrintTemplateComponentById(item.PrintTemplateComponentId);
            if (parent != null)
            {
                parent.ComponentDetails.Remove(item);
                parent.MarkDirty();
            }
        }

        #endregion


        /// <summary>
        /// Unlocks all printTemplates in this cache
        /// </summary>
        private void ClearCache()
        {
            foreach (PrintTemplateItem h in this.PrintTemplates)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (PrintTemplateItem h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (PrintTemplateItem h in _deletedItems)
            {
                h.Delete();
            }
            _deletedItems.Clear();

            //save any changed printTemplates.
            if (_items != null)
            {
                foreach (PrintTemplateItem h in _items)
                {
                    if (h.IsDirty)
                    {
                        h.Save(_unitTestDirectory);
                    }
                }
            }
        }

        /// <summary>
        /// Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            List<PrintTemplateItem> oldPrintTemplates = this.PrintTemplates.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old printTemplates unlocking files and reloading.
            List<String> reloadIds = new List<String>();
            foreach (PrintTemplateItem old in oldPrintTemplates)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ClearCache();
                    _items = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
