﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-26671 : A.Silva ~ Added code to facilitate Unit Testing.
// V8-28362 : L.Ineson
//  Reworked to allow readonly and brought into line with other dalcaches.
// V8-26322 : A.Silva
//      Amended InsertOrUpdate that was throwing a Null Reference exception when unit testing.
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Added ids to child objects.
#endregion
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;
using System.IO;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    ///     Implementation of <see cref="IDalCache" /> for <see cref="CustomColumnLayoutItem" />.
    /// </summary>
    internal class CustomColumnLayoutCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private Object _lock = new Object();

        private List<CustomColumnLayoutItem> _items;
        private readonly List<CustomColumnLayoutItem> _deletedItems = new List<CustomColumnLayoutItem>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of Items.
        /// </summary>
        public IEnumerable<CustomColumnLayoutItem> Items
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<CustomColumnLayoutItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public CustomColumnLayoutCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File Methods

        /// <summary>
        ///     Adds a locked item to the cache.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            CustomColumnLayoutItem dalObject = this.Items.FirstOrDefault(item => item.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = CustomColumnLayoutItem.FetchById(id, /*asReadOnly*/false);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        ///     Unlocks and removes the item with the given <paramref name="id" />.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            CustomColumnLayoutItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
        }

        /// <summary>
        ///     Returns the <see cref="CustomColumnLayoutItem" /> with the requested <paramref name="id" /> from the cache, or from
        ///     the DAL if not in the cache.
        /// </summary>
        /// <param name="id">Unique id value for the <see cref="CustomColumnLayoutInfoItem" />.</param>
        /// <returns>A <see cref="CustomColumnLayoutItem" /> instance from the cache or from the DAL.</returns>
        public CustomColumnLayoutItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            CustomColumnLayoutItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }


            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = CustomColumnLayoutItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region CustomColumnLayoutItem

        /// <summary>
        /// Returns the item with the given id from the cache
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomColumnLayoutItem FindCustomColumnLayoutById(String id)
        {
            return this.Items.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public CustomColumnLayoutItem InsertOrUpdate(CustomColumnLayoutDto dto)
        {
            if (this.Items == null) return null;

            String id = dto.Id as String;
            CustomColumnLayoutItem item = this.Items.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                //  If we are unit testing, we will need to generate an Id for the Dto.
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    // Choose the right extension based on the dto we got.
                    var fileExtension = dto.Type == (Byte)CustomColumnLayoutType.DataSheet
                                   ? Constants.DataSheetFileExtension
                                   : Constants.CustomColumnLayoutFileExtension;
                    dto.Id = Path.ChangeExtension(Path.Combine(_unitTestDirectory, dto.Name), fileExtension);
                }

                //Insert.
                item = new CustomColumnLayoutItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        ///     Deletes the <see cref="CustomColumnLayoutItem" /> from the cache
        /// </summary>
        /// <param name="item">The item to remove from the cache.</param>
        /// <remarks>The item is added to the <see cref="DeletedItems" /> collection.</remarks>
        public void Delete(CustomColumnLayoutItem item)
        {
            if (Items.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }

        #endregion

        #region CustomColumnItem

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomColumnItem FindCustomColumnById(Int32 id)
        {
            foreach (CustomColumnLayoutItem h in this.Items)
            {
                foreach (CustomColumnItem f in h.Columns)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Inserts the given dto or updates an existing one.
        /// </summary>
        public CustomColumnItem InsertOrUpdate(CustomColumnDto dto)
        {
            if (this.Items == null) return null;

            CustomColumnItem item = null;

            CustomColumnLayoutItem parent = FindCustomColumnLayoutById(Convert.ToString(dto.CustomColumnLayoutId));
            if (parent != null)
            {
                item = parent.Columns.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = parent.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        ///     Deletes the item, removing it from its parent.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(CustomColumnItem item)
        {
            if (this.Items == null || Items == null) return;

            CustomColumnLayoutItem parentItem = FindCustomColumnLayoutById(item.CustomColumnLayoutId);
            if (parentItem != null)
            {
                parentItem.Columns.Remove(item);
                parentItem.MarkDirty();
            }
        }

        #endregion

        #region CustomColumnSortItem

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomColumnSortItem FindCustomColumnSortById(Int32 id)
        {
            foreach (CustomColumnLayoutItem h in this.Items)
            {
                foreach (CustomColumnSortItem f in h.ColumnSorts)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Inserts the given dto or updates an existing one.
        /// </summary>
        public CustomColumnSortItem InsertOrUpdate(CustomColumnSortDto dto)
        {
            if (this.Items == null) return null;

            CustomColumnSortItem item = null;

            CustomColumnLayoutItem parent = FindCustomColumnLayoutById(Convert.ToString(dto.CustomColumnLayoutId));
            if (parent != null)
            {
                item = parent.ColumnSorts.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = parent.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        ///     Deletes the item, removing it from its parent.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(CustomColumnSortItem item)
        {
            if (this.Items == null || Items == null) return;

            CustomColumnLayoutItem parentItem = FindCustomColumnLayoutById(item.CustomColumnLayoutId);
            if (parentItem != null)
            {
                parentItem.ColumnSorts.Remove(item);
                parentItem.MarkDirty();
            }
        }

        #endregion

        #region CustomColumnFilterItem

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomColumnFilterItem FindCustomColumnFilterById(Int32 id)
        {
            foreach (CustomColumnLayoutItem h in this.Items)
            {
                foreach (CustomColumnFilterItem f in h.ColumnFilters)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Inserts the given dto or updates an existing one.
        /// </summary>
        public CustomColumnFilterItem InsertOrUpdate(CustomColumnFilterDto dto)
        {
            if (this.Items == null) return null;

            CustomColumnFilterItem item = null;

            CustomColumnLayoutItem parent = FindCustomColumnLayoutById(Convert.ToString(dto.CustomColumnLayoutId));
            if (parent != null)
            {
                item = parent.ColumnFilters.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = parent.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }
        
        /// <summary>
        ///     Deletes the item, removing it from its parent.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(CustomColumnFilterItem item)
        {
            if (this.Items == null || Items == null) return;

            CustomColumnLayoutItem parentItem = FindCustomColumnLayoutById(item.CustomColumnLayoutId);
            if (parentItem != null)
            {
                parentItem.ColumnFilters.Remove(item);
                parentItem.MarkDirty();
            }
        }

        #endregion

        #region CustomColumnGroupItem

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomColumnGroupItem FindCustomColumnGroupById(Int32 id)
        {
            foreach (CustomColumnLayoutItem h in this.Items)
            {
                foreach (CustomColumnGroupItem f in h.ColumnGroups)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Inserts the given dto or updates an existing one.
        /// </summary>
        public CustomColumnGroupItem InsertOrUpdate(CustomColumnGroupDto dto)
        {
            if (this.Items == null) return null;

            CustomColumnGroupItem item = null;

            CustomColumnLayoutItem parent = FindCustomColumnLayoutById(Convert.ToString(dto.CustomColumnLayoutId));
            if (parent != null)
            {
                item = parent.ColumnGroups.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = parent.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        ///     Deletes the item, removing it from its parent.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(CustomColumnGroupItem item)
        {
            if (this.Items == null || Items == null) return;

            CustomColumnLayoutItem parentItem = FindCustomColumnLayoutById(item.CustomColumnLayoutId);
            if (parentItem != null)
            {
                parentItem.ColumnGroups.Remove(item);
                parentItem.MarkDirty();
            }
        }

        #endregion

        /// <summary>
        /// Unlocks all items in this cache
        /// </summary>
        private void ClearCache()
        {
            foreach (CustomColumnLayoutItem h in this.Items)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (CustomColumnLayoutItem h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        ///     Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (CustomColumnLayoutItem h in _deletedItems)
            {
                h.Delete();
            }
            _deletedItems.Clear();

            //save any changed items.
            if (_items != null)
            {
                foreach (CustomColumnLayoutItem h in _items)
                {
                    if (h.IsDirty)
                    {
                        h.Save();
                    }
                }
            }
        }

        /// <summary>
        ///     Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            List<CustomColumnLayoutItem> oldItems = this.Items.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old highlights unlocking files and reloading.
            List<String> reloadIds = new List<String>();
            foreach (CustomColumnLayoutItem old in oldItems)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                ClearCache();
            }

            _isDisposed = true;
        }

        #endregion
    }
}