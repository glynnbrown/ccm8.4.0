﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    ///     Manages the <see cref="ValidationTemplateItem" /> cache.
    /// </summary>
    internal class ValidationTemplateDalCache : IDalCache
    {
        #region Constants

        /// <summary>
        ///     Value used to indicate that the file should be locked.
        /// </summary>
        private const Boolean LockFile = true;

        private Int32 _nextGroupItemId;
        private Int32 _nextGroupMetricItemId;

        #endregion

        #region Fields

        /// <summary>
        ///     Holds the internal collection of deleted <see cref="ValidationTemplateItem" /> from this instance.
        /// </summary>
        private readonly IList<ValidationTemplateItem> _deletedItems = new List<ValidationTemplateItem>();

        /// <summary>
        ///     Holds the temporary directory to be used when UnitTesting.
        /// </summary>
        private readonly String _unitTestDirectory;

        /// <summary>
        ///     Holds the internal collection of <see cref="ValidationTemplateItem" /> stored in this instance.
        /// </summary>
        /// <remarks>Using <see cref="Lazy{T}" /> which ensures thread safe lazy initialization.</remarks>
        private Lazy<IList<ValidationTemplateItem>> _items =
            new Lazy<IList<ValidationTemplateItem>>(() => new List<ValidationTemplateItem>());

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the collection of <see cref="ValidationTemplateGroupItem" /> in this instance.
        /// </summary>
        private IEnumerable<ValidationTemplateGroupItem> Groups
        {
            get { return Items.SelectMany(item => item.Groups); }
        }

        /// <summary>
        ///     Gets the internal collection of <see cref="ValidationTemplateItem" /> as a list for manipulation inside the
        ///     instance.
        /// </summary>
        private IList<ValidationTemplateItem> ItemList
        {
            get { return _items.Value; }
        }

        /// <summary>
        ///     Gets the collection of <see cref="ValidationTemplateItem" /> in this instance.
        /// </summary>
        internal IEnumerable<ValidationTemplateItem> Items
        {
            get { return ItemList; }
        }

        /// <summary>
        ///     Gets the collection of <see cref="ValidationTemplateGroupMetricItem" /> in this instance
        /// </summary>
        private IEnumerable<ValidationTemplateGroupMetricItem> Metrics
        {
            get { return Groups.SelectMany(group => @group.Metrics); }
        }

        #endregion

        #region Constructor

        public ValidationTemplateDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region ValidationTemplateItem

        /// <summary>
        ///     Deletes the <paramref name="item" /> from the <see cref="ValidationTemplateDalCache" />.
        /// </summary>
        /// <param name="item">The <see cref="ValidationTemplateItem" /> to be deleted.</param>
        public void Delete(ValidationTemplateItem item)
        {
            if (!ItemList.Contains(item)) return;

            ItemList.Remove(item);
            _deletedItems.Add(item);
        }

        public ValidationTemplateItem FetchById(String id, Boolean lockFile = false)
        {
            //  Get if from the cache, if missing try to fetch it from the filesystem.
            var item = ItemList.FirstOrDefault(o => o.Id == id) ?? ValidationTemplateItem.FetchById(id, lockFile);

            //  Store the item in the cache if one was returned and not yet stored.
            if (item != null && ItemList.All(o => o.Id != id)) ItemList.Add(item);

            return item;
        }

        /// <summary>
        ///     Inserts or updates the griven <paramref name="dto" />.
        /// </summary>
        /// <param name="dto">The <see cref="ValidationTemplateDto" /> contining the data to be persisted.</param>
        /// <returns>
        ///     A new (if inserting) or existing (if update) instance of <see cref="ValidationTemplateItem" /> with the
        ///     information from the dto.
        /// </returns>
        public ValidationTemplateItem InsertOrUpdate(ValidationTemplateDto dto)
        {
            String id = dto.Id as String;
            var item = ItemList.FirstOrDefault(o => o.Id == id);
            if (item == null)
            {
                item = new ValidationTemplateItem(dto);
                ItemList.Add(item);
            }
            else
            {
                item.UpdateFromDto(dto);
            }

            return item;
        }

        public Boolean LockValidationTemplateById(String id)
        {
            return FetchById(id, LockFile) != null;
        }

        /// <summary>
        ///     Unlocks and removes the <see cref="ValidationTemplateItem" /> with the given <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the item to unlock and remove.</param>
        public void UnlockValidationTemplateById(String id)
        {
            var match = ItemList.FirstOrDefault(item => item.Id == id);
            if (match == null) return;

            ItemList.Remove(match);
            match.Unlock();
        }

        /// <summary>
        ///     Unlocks all <see cref="ValidationTemplateItem" /> instances in this instance.
        /// </summary>
        private void UnlockAll()
        {
            ItemList.AsParallel().ForAll(item => item.Unlock());
            ItemList.Clear();
        }

        #endregion

        #region ValidationTemplateGroupItem

        /// <summary>
        ///     Removes a <see cref="ValidationTemplateGroupItem" /> from its <see cref="ValidationTemplateItem" />
        ///     group's collection.
        /// </summary>
        /// <param name="item">The instance of <see cref="ValidationTemplateGroupItem" /> to be deleted from the cache.</param>
        public void Delete(ValidationTemplateGroupItem item)
        {
            var parentItem = FetchById((String) item.ValidationTemplateId);
            if (parentItem == null || !parentItem.Groups.Contains(item)) return;

            parentItem.Groups.Remove(item);
            parentItem.MarkDirty();
        }

        /// <summary>
        ///     Finds and retrieves the <see cref="ValidationTemplateGroupItem" /> from the cache and having the given
        ///     <paramref name="id" />.
        /// </summary>
        /// <param name="id">Unique value to find the saught for item.</param>
        /// <returns>
        ///     The instance of <see cref="ValidationTemplateGroupItem" /> from the cache with the required
        ///     <paramref name="id" />.
        /// </returns>
        public ValidationTemplateGroupItem FetchGroupById(Int32 id)
        {
            return Groups.FirstOrDefault(item => item.Id == id);
        }

        public ValidationTemplateGroupItem Insert(ValidationTemplateGroupDto dto)
        {
            var itemParent = FetchById(Convert.ToString(dto.ValidationTemplateId));
            if (itemParent == null) return null;

            dto.Id = _nextGroupItemId++;
            var item = new ValidationTemplateGroupItem(dto);
            itemParent.Groups.Add(item);
            itemParent.MarkDirty();
            return item;
        }

        #endregion

        #region ValidationTemplateGroupMetricItem

        /// <summary>
        ///     Removes a <see cref="ValidationTemplateGroupMetricItem" /> from its <see cref="ValidationTemplateGroupItem" />
        ///     metric's collection.
        /// </summary>
        /// <param name="item">The instance of <see cref="ValidationTemplateGroupMetricItem" /> to be deleted from the cache.</param>
        public void Delete(ValidationTemplateGroupMetricItem item)
        {
            var parentItem = FetchGroupById(item.ValidationTemplateGroupId);
            if (parentItem == null || !parentItem.Metrics.Contains(item)) return;

            parentItem.Metrics.Remove(item);
            parentItem.MarkDirty();
        }

        /// <summary>
        ///     Finds and retrieves the <see cref="ValidationTemplateGroupMetricItem" /> from the cache and having the given
        ///     <paramref name="id" />.
        /// </summary>
        /// <param name="id">Unique value to find the saught for item.</param>
        /// <returns>
        ///     The instance of <see cref="ValidationTemplateGroupMetricItem" /> from the cache with the required
        ///     <paramref name="id" />.
        /// </returns>
        public ValidationTemplateGroupMetricItem FetchMetricById(Int32 id)
        {
            return Metrics.FirstOrDefault(metric => metric.Id == id);
        }

        public ValidationTemplateGroupMetricItem Insert(ValidationTemplateGroupMetricDto dto)
        {
            var itemParent = FetchGroupById(dto.ValidationTemplateGroupId);
            if (itemParent == null) return null;

            dto.Id = _nextGroupMetricItemId++;
            var item = new ValidationTemplateGroupMetricItem(dto);
            itemParent.Metrics.Add(item);
            itemParent.MarkDirty();
            return item;
        }

        #endregion

        #endregion

        #region IDalCache Members

        public void Commit()
        {
            foreach (var deletedItem in _deletedItems)
            {
                deletedItem.Delete();
            }
            _deletedItems.Clear();

            foreach (var source in ItemList.Where(item => item.IsDirty))
            {
                source.Save(_unitTestDirectory);
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Rolls back any pending changes.
        /// </summary>
        public void Rollback()
        {
            var originalIds = new List<String>();
            foreach (var originalItem in ItemList.Union(_deletedItems).ToList())
            {
                var originalStreamName = originalItem.CurrentStreamName;
                originalItem.Unlock();
                if (!String.IsNullOrEmpty(originalStreamName)) originalIds.Add(originalStreamName);
            }

            ItemList.Clear();
            _deletedItems.Clear();
            foreach (var originalId in originalIds)
            {
                FetchById(originalId);
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                UnlockAll();
                _items = null;
            }
            _isDisposed = true;
        }

        #endregion
    }
}