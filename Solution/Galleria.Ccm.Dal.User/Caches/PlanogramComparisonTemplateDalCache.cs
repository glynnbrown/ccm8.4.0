﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    internal class PlanogramComparisonTemplateDalCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private readonly Object _lock = new Object();

        private List<PlanogramComparisonTemplateItem> _items;
        private readonly List<PlanogramComparisonTemplateItem> _deletedItems = new List<PlanogramComparisonTemplateItem>();

        #endregion

        #region Properties

        /// <summary>
        ///     Get the collection of <see cref="PlanogramComparisonTemplateItem"/> in the cache.
        /// </summary>
        public IEnumerable<PlanogramComparisonTemplateItem> PlanogramComparisonTemplates
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<PlanogramComparisonTemplateItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonTemplateDalCache"/>.
        /// </summary>
        /// <param name="dalFactory"></param>
        public PlanogramComparisonTemplateDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File Methods

        /// <summary>
        ///     Locks the file matching the given <paramref name="id"/> and adds the item to the cache.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            PlanogramComparisonTemplateItem dalObject = PlanogramComparisonTemplates.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject != null) return;

            dalObject = PlanogramComparisonTemplateItem.FetchById(id, asReadOnly: false);
            if (dalObject != null)
            {
                _items.Add(dalObject);
            }
        }

        /// <summary>
        ///     Unlocks the file matching the given <paramref name="id"/> and removes the item from the cache.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            PlanogramComparisonTemplateItem dalObject = PlanogramComparisonTemplates.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
            else
            {
                Debug.WriteLine("Failed to unlock file: " + id);
            }
        }

        /// <summary>
        ///     Fetch the <see cref="PlanogramComparisonTemplateItem"/> matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allowReadOnly"></param>
        /// <returns></returns>
        /// <remarks>This method should be call only from the DAL</remarks>
        public PlanogramComparisonTemplateItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            PlanogramComparisonTemplateItem dalObject = PlanogramComparisonTemplates.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }


            if (dalObject != null ||
                !allowReadOnly) return dalObject;

            //fetch as readonly.
            dalObject = PlanogramComparisonTemplateItem.FetchById(id, asReadOnly: true);
            if (dalObject != null)
            {
                _items.Add(dalObject);
            }

            return dalObject;
        }

        #endregion

        #region PlanogramComparisonTemplateItem

        /// <summary>
        ///     Get the <see cref="PlanogramComparisonTemplateItem"/> that matches the given <paramref name="id"/>.
        /// </summary>
        /// <remarks>If not match is found, <c>null</c> will be returned.</remarks>
        public PlanogramComparisonTemplateItem GetPlanogramComparisonTemplateById(String id)
        {
            if (!String.IsNullOrEmpty(_unitTestDirectory))
            {
                id = String.Format("{0}\\{1}{2}", _unitTestDirectory, id, Constants.PrintTemplateFileExtension);
            }

            return PlanogramComparisonTemplates.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        ///     Insert or update the corresponding item with data from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>The item that was inserted or updated.</returns>
        public PlanogramComparisonTemplateItem InsertOrUpdate(PlanogramComparisonTemplateDto dto)
        {
            if (PlanogramComparisonTemplates == null) return null;

            String id = dto.Id as String;
            PlanogramComparisonTemplateItem item = PlanogramComparisonTemplates.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id = Path.ChangeExtension(Path.Combine(_unitTestDirectory, (PlanogramComparisonTemplates.Count() + 1).ToString()), Constants.PrintTemplateFileExtension);
                }

                //Insert.
                item = new PlanogramComparisonTemplateItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        ///     Delete the given <paramref name="item"/> from the chache and mark it for deletion.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PlanogramComparisonTemplateItem item)
        {
            if (!PlanogramComparisonTemplates.Contains(item)) return;

            _items.Remove(item);
            _deletedItems.Add(item);
        }

        #endregion

        #region PlanogramComparisonTemplateFieldItem

        /// <summary>
        ///     Insert or update the given <paramref name="dto"/>.
        /// </summary>
        public PlanogramComparisonTemplateFieldItem InsertOrUpdate(PlanogramComparisonTemplateFieldDto dto)
        {
            if (PlanogramComparisonTemplates == null) return null;

            PlanogramComparisonTemplateFieldItem item = null;

            PlanogramComparisonTemplateItem parentItem = GetPlanogramComparisonTemplateById(Convert.ToString(dto.PlanogramComparisonTemplateId));
            if (parentItem != null)
            {
                item = parentItem.ComparisonFields.FirstOrDefault(i => i.Id == (Int32) dto.Id);

                if (item == null)
                {
                    //insert
                    item = parentItem.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        ///     Get the corresponding <see cref="PlanogramComparisonTemplateFieldItem"/> matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PlanogramComparisonTemplateFieldItem GetPlanogramComparisonTemplateFieldById(Int32 id)
        {
            return PlanogramComparisonTemplates.SelectMany(h => h.ComparisonFields.Where(f => f.Id == id)).FirstOrDefault();
        }

        /// <summary>
        ///     Remove the given item from its parent.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PlanogramComparisonTemplateFieldItem item)
        {
            if (PlanogramComparisonTemplates == null) return;

            PlanogramComparisonTemplateItem printTemplate = GetPlanogramComparisonTemplateById(item.PlanogramComparisonTemplateId);
            if (printTemplate == null) return;

            printTemplate.ComparisonFields.Remove(item);
            printTemplate.MarkDirty();
        }

        #endregion

        /// <summary>
        ///     Clear the chache from all items and unlock their files.
        /// </summary>
        private void ClearCache()
        {
            foreach (PlanogramComparisonTemplateItem h in PlanogramComparisonTemplates)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (PlanogramComparisonTemplateItem h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        ///     Commit all changes made to file, deleting any marked to be deleted.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (PlanogramComparisonTemplateItem h in _deletedItems)
            {
                h.Delete();
            }
            _deletedItems.Clear();

            if (_items == null) return;

            //save any changed printTemplates.
            foreach (PlanogramComparisonTemplateItem h in _items.Where(h => h.IsDirty))
            {
                h.Save(_unitTestDirectory);
            }
        }

        /// <summary>
        ///     Rollback any changes made.
        /// </summary>
        public void Rollback()
        {
            List<PlanogramComparisonTemplateItem> oldPrintTemplates = PlanogramComparisonTemplates.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old printTemplates unlocking files and reloading.
            var reloadIds = new List<String>();
            foreach (PlanogramComparisonTemplateItem old in oldPrintTemplates)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                ClearCache();
                _items = null;
            }

            _isDisposed = true;
        }

        #endregion
    }
}