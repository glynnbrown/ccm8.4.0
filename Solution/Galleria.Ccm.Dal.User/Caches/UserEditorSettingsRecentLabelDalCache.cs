﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Xml.Serialization;
//using Galleria.Ccm.Dal.DataTransferObjects;
//using Galleria.Ccm.Dal.User.Implementation.Items;
//using Galleria.Ccm.Model;


//namespace Galleria.Ccm.Dal.User.Caches
//{
//         /// <summary>
//        /// RecentLabels Dal Cache
//        /// </summary>
//        internal class UserEditorSettingsRecentLabelDalCache : IDalCache
//        {
//            #region Fields

//            private readonly String _filePath;
//            private FileStream _stream;
//            private UserEditorSettingsRecentLabelList _list;
//            private Int32 _nextId = 1;

//            #endregion

//            #region Properties

//            /// <summary>
//            /// Returns the items currently held by this cache.
//            /// </summary>
//            public IEnumerable<UserEditorSettingsRecentLabelItem> Items //should this be UserEditorSettingsRecentLabelItem?
//            {
//                get
//                {
//                    if (_list == null && !_isDisposed)
//                    {
//                        Fetch();
//                    }
//                    return _list.Items;
//                }
//            }

//            /// <summary>
//            /// Returns the items currently held by this cache.
//            /// </summary>
//            public IEnumerable<UserEditorSettingsRecentLabelItem> RecentLabel
//            {
//                get
//                {
//                    if (_list == null && !_isDisposed)
//                    {
//                        Fetch();
//                    }
//                    return _list.RecentLabel;
//                }
//            }

//            #endregion

//            #region Constructor

//            /// <summary>
//            /// Creates a new instance of this type
//            /// </summary>
//            /// <param name="dalFactory"></param>
//            public UserEditorSettingsRecentLabelDalCache(DalFactory dalFactory)
//            {
//                //Determine the file path.
//                if (dalFactory.IsUnitTesting)//points to Usersettings.xml
//                {
//                    _filePath = Path.Combine(dalFactory.UnitTestFolder, Constants.EditorUserSettingsFileName);
//                }
//                else
//                {
//                    _filePath =
//                   Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
//                      Constants.AppDataFolderName, Constants.EditorUserSettingsFileName);
//                }

//                //Fetch data.
//                Fetch();
//            }

//            #endregion

//            #region Methods

//            /// <summary>
//            /// Refetches the data.
//            /// </summary>
//            private void Fetch()
//            {
//                //first unlock the existing stream as we are going
//                // to start a new one.
//                Unlock();

//                try
//                {
//                    //Get a new steem and load the list.
//                    FileStream stream = GetStream();
//                    if (stream.Length > 0)
//                    {
//                        XmlSerializer x = new XmlSerializer(typeof(UserEditorSettingList));
//                        _list = (UserEditorSettingList)x.Deserialize(stream);

//                        RemoveDuplicateKeys();

//                        Int32 id = 1;
//                        foreach (var item in _list.Items)
//                        {
//                            item.Id = id;
//                            id++;
//                        }

//                        id = 1;
//                        foreach (var item in _list.RecentLabel)
//                        {
//                            item.Id = id;
//                            id++;
//                        }
//                    }
//                    else
//                    {
//                        _list = new UserEditorSettingList();
//                    }
//                }
//                catch (Exception ex)
//                {
//                    Unlock();

//                    if (ex.GetType() == typeof(InvalidOperationException))
//                    {
//                        File.Delete(_filePath);

//                        //just return a new list.
//                        _list = new UserEditorSettingList();
//                    }
//                    else
//                    {
//                        throw ex;
//                    }
//                }

//                //unlock immediately as we don't want to hold
//                // onto this file.
//                Unlock();
//            }

//            /// <summary>
//            /// Unlocks the current filestream.
//            /// </summary>
//            private void Unlock()
//            {
//                if (_stream != null)
//                {
//                    _stream.Close();
//                    _stream.Dispose();
//                    _stream = null;
//                }
//            }

//            /// <summary>
//            /// Returns the existing file stream or
//            /// creates a new one if required.
//            /// </summary>
//            /// <returns></returns>
//            private FileStream GetStream()
//            {
//                if (_stream == null)
//                {
//                    String filePath = _filePath;

//                    //ensure the directory exists
//                    String directory = Path.GetDirectoryName(filePath);
//                    if (!Directory.Exists(directory))
//                    {
//                        Directory.CreateDirectory(directory);
//                    }

//                    _stream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
//                }

//                return _stream;
//            }

//            /// <summary>
//            /// Inserts the given dto
//            /// </summary>
//            /// <param name="dto"></param>
//            /// <returns></returns>
//            internal UserEditorSettingsRecentLabelItem Insert(UserEditorSettingsRecentLabelDto dto) // should this item be UserEditorSettingsRecentLabel?
//            {
//                //set the dto id
//                dto.Id = _nextId;

//                //create the item
//                UserEditorSettingsRecentLabelItem item = new UserEditorSettingsRecentLabelItem(dto);
//                _list.Items.Add(item);

//                //increment ready for the next item.
//                _nextId++;

//                return item;
//            }

//            /// <summary>
//            /// Removes the given item.
//            /// </summary>
//            /// <param name="item"></param>
//            internal void Delete(UserEditorSettingsRecentLabelItem item)
//            {
//                _list.Items.Remove(item);
//            }

            

//            /// <summary>
//            ///     Remove any duplicate keys in the setting items.
//            /// </summary>
//            private void RemoveDuplicateKeys()
//            {
//                ILookup<String, UserEditorSettingsRecentLabelItem> lookup = _list.Items.ToLookup(item => item.Key, item => item);
//                if (!lookup.Any(items => items.Count() > 1)) return;

//                foreach (UserEditorSettingsRecentLabelItem item in lookup.SelectMany(items => items.Skip(1)))
//                {
//                    _list.Items.Remove(item);
//                }
//            }

//            #endregion

//            #region IDalCache Members

//            /// <summary>
//            /// Commits the current transaction changes.
//            /// </summary>
//            public void Commit()
//            {
//                if (_list != null)
//                {
//                    FileStream stream = GetStream();

//                    stream.SetLength(0);
//                    stream.Flush();

//                    XmlSerializer x = new XmlSerializer(typeof(UserEditorSettingList));
//                    x.Serialize(stream, _list);

//                    //unlock the stream
//                    // so that the file is not held open.
//                    Unlock();
//                }
//            }

//            /// <summary>
//            /// Rollsback transaction changes.
//            /// </summary>
//            public void Rollback()
//            {
//                Fetch();
//            }

//            #endregion

//            #region IDisposable

//            private Boolean _isDisposed;

//            public void Dispose()
//            {
//                Dispose(true);
//                GC.SuppressFinalize(this);
//            }

//            private void Dispose(Boolean disposing)
//            {
//                if (!_isDisposed)
//                {
//                    if (disposing)
//                    {
//                        Unlock();
//                        _list = null;
//                    }
//                    _isDisposed = true;
//                }
//            }

//            #endregion
//        }
//    }

