﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 L.Ineson
//  Created
// V8-28426 : D.Pleasance
//  Amended InsertOrUpdate \ Delete methods to obtain cache item directly rather than calling GetPlanogramImportTemplateById
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 D.Pleasance
//  Added implementation for PlanogramImportTemplatePerformanceMetricItem
#endregion
#region Version History: (CCM 8.3.0)
// V8-32743 : M.Pettit
//  InsertOrUpdate did not correctly find existing mappings when matching by dtoKey for both tempalte and performance mappings
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;
using System.IO;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our PlanogramImportTemplate dal cache
    /// </summary>
    internal class PlanogramImportTemplateDalCache : IDalCache
    {
        #region Fields
        private readonly String _unitTestDirectory;
        private Object _lock = new Object();
        private List<PlanogramImportTemplateItem> _items;
        private readonly List<PlanogramImportTemplateItem> _deletedItems = new List<PlanogramImportTemplateItem>();
        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of highlights.
        /// </summary>
        public IEnumerable<PlanogramImportTemplateItem> Items
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<PlanogramImportTemplateItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public PlanogramImportTemplateDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File

        /// <summary>
        /// locks the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            PlanogramImportTemplateItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = PlanogramImportTemplateItem.FetchById(id, /*asReadOnly*/false);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        /// Unlocks and removes the highlight with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            PlanogramImportTemplateItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
        }

        /// <summary>
        /// Returns the planogram import template with the given id.
        /// </summary>
        public PlanogramImportTemplateItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            PlanogramImportTemplateItem dalObject = this.Items.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = PlanogramImportTemplateItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region PlanogramImportTemplate

        /// <summary>
        /// Returns the item from the cache with the given id.
        /// </summary>
        public PlanogramImportTemplateItem FindPlanogramImportTemplateById(String id)
        {
            return this.Items.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public PlanogramImportTemplateItem InsertOrUpdate(PlanogramImportTemplateDto dto)
        {
            if (this.Items == null) return null;

            String id = dto.Id as String;
            PlanogramImportTemplateItem item = this.Items.FirstOrDefault(h => h.Id == id);

            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id = Path.ChangeExtension(Path.Combine(_unitTestDirectory, dto.Name), Constants.PlanogramImportTemplateFileExtension);
                }


                item = new PlanogramImportTemplateItem(dto);
                _items.Add(item);
            }
            else
            {
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PlanogramImportTemplateItem item)
        {
            if (Items.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }

        #endregion

        #region PlanogramImportTemplateMappingItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public PlanogramImportTemplateMappingItem InsertOrUpdate(PlanogramImportTemplateMappingDto dto)
        {
            if (this.Items == null) return null;

            PlanogramImportTemplateMappingItem item = null;

            PlanogramImportTemplateItem template = FindPlanogramImportTemplateById(Convert.ToString(dto.PlanogramImportTemplateId));
            if (template != null)
            {
                item = template.Mappings.FirstOrDefault(i => 
                    i.Field == dto.Field && 
                    i.FieldType == dto.FieldType && 
                    i.PlanogramImportTemplateId.Equals(dto.PlanogramImportTemplateId));
                
                if (item == null)
                {
                    //insert
                    item = template.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PlanogramImportTemplateMappingItem FindPlanogramImportTemplateMappingItemById(Int32 id)
        {
            foreach (PlanogramImportTemplateItem item in this.Items)
            {
                foreach (PlanogramImportTemplateMappingItem m in item.Mappings)
                {
                    if (m.Id == id)
                    {
                        return m;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PlanogramImportTemplateMappingItem item)
        {
            if (this.Items == null || item ==null) return;

            PlanogramImportTemplateItem template = FindPlanogramImportTemplateById(Convert.ToString(item.PlanogramImportTemplateId));
            if (template != null)
            {
                template.Mappings.Remove(item);
                template.MarkDirty();
            }
        }

        #endregion

        #region PlanogramImportTemplatePerformanceMetricItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public PlanogramImportTemplatePerformanceMetricItem InsertOrUpdate(PlanogramImportTemplatePerformanceMetricDto dto)
        {
            if (this.Items == null) return null;

            PlanogramImportTemplatePerformanceMetricItem item = null;

            PlanogramImportTemplateItem template = FindPlanogramImportTemplateById(Convert.ToString(dto.PlanogramImportTemplateId));
            if (template != null)
            {
                item = template.PerformanceMetrics.FirstOrDefault(i =>
                    i.Name == dto.Name &&
                    i.MetricId == dto.MetricId &&
                    i.PlanogramImportTemplateId.Equals(dto.PlanogramImportTemplateId));

                if (item == null)
                {
                    //insert
                    item = template.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PlanogramImportTemplatePerformanceMetricItem FindPlanogramImportTemplatePerformanceMetricItemById(Int32 id)
        {
            foreach (PlanogramImportTemplateItem item in this.Items)
            {
                foreach (PlanogramImportTemplatePerformanceMetricItem m in item.PerformanceMetrics)
                {
                    if (m.Id == id)
                    {
                        return m;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(PlanogramImportTemplatePerformanceMetricItem item)
        {
            if (this.Items == null || item == null) return;

            PlanogramImportTemplateItem template = FindPlanogramImportTemplateById(Convert.ToString(item.PlanogramImportTemplateId));
            if (template != null)
            {
                template.PerformanceMetrics.Remove(item);
                template.MarkDirty();
            }
        }

        #endregion

        /// <summary>
        /// Unlocks and removes all items from the cache.
        /// </summary>
        private void ClearCache()
        {
            foreach (var h in this.Items)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (var h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (var i in _deletedItems)
            {
                i.Delete();
            }
            _deletedItems.Clear();

            //save any changed highlights.
            if (_items != null)
            {
                foreach (var i in _items)
                {
                    if (i.IsDirty)
                    {
                        i.Save(_unitTestDirectory);
                    }
                }
            }
        }

        /// <summary>
        /// Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            var oldItems = this.Items.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old highlights unlocking files and reloading.
            List<Tuple<String, Boolean>> reloadIds = new List<Tuple<String, Boolean>>();
            foreach (var old in oldItems)
            {
                String oldStreamName = old.CurrentStreamName;
                Boolean wasLocked = !old.IsReadOnly;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(new Tuple<String, Boolean>(oldStreamName,wasLocked));
                }
            }

            //reload
            foreach (var item in reloadIds)
            {
                FetchFileById(item.Item1, item.Item2);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ClearCache();
                    _items = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
