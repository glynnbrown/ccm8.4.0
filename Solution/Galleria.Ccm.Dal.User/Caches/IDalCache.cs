﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.User.Caches
{
    public interface IDalCache : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
