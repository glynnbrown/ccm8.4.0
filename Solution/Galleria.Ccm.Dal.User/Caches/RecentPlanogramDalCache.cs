﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    internal class RecentPlanogramDalCache : IDalCache
    {
        #region Fields

        private readonly String _filePath;

        private FileStream _stream;
        private RecentPlanogramsList _list;
        private Int32 _nextId = 1;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of items held by this cache.
        /// </summary>
        public IEnumerable<RecentPlanogramItem> Items
        {
            get { return _list.Items; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public RecentPlanogramDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _filePath = Path.Combine(dalFactory.UnitTestFolder, Constants.EditorRecentPlanogramsFileName);
            }
            else
            {
                _filePath =
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                   Constants.EditorAppDataFolderName, Constants.EditorRecentPlanogramsFileName);
            }

            Fetch();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refetches the data.
        /// </summary>
        private void Fetch()
        {
            Unlock();

            try
            {
                FileStream stream = GetStream();
                if (stream.Length > 0)
                {
                    XmlSerializer x = new XmlSerializer(typeof(RecentPlanogramsList));
                    _list = (RecentPlanogramsList)x.Deserialize(stream);

                    Int32 id = 1;
                    foreach (var item in _list.Items)
                    {
                        item.Id = id;
                        id++;
                    }
                }
                else
                {
                    _list = new RecentPlanogramsList();
                }
            }
            catch (Exception ex)
            {
                Unlock();
                throw ex;
            }
        }

        /// <summary>
        /// Unlocks the current filestream.
        /// </summary>
        private void Unlock()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        /// <summary>
        /// Returns the existing file stream or
        /// creates a new one if required.
        /// </summary>
        /// <returns></returns>
        private FileStream GetStream()
        {
            if(_stream == null)
            {
                String filePath = _filePath;

                //ensure the directory exists
                String directory = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                _stream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
            }

            return _stream;
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        internal RecentPlanogramItem Insert(RecentPlanogramDto dto)
        {
            dto.Id = _nextId;

            RecentPlanogramItem item = new RecentPlanogramItem(dto);
            _list.Items.Add(item);

            _nextId++;

            return item;
        }

        /// <summary>
        /// Removes the given item.
        /// </summary>
        /// <param name="item"></param>
        internal void Delete(RecentPlanogramItem item)
        {
            _list.Items.Remove(item);
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits the current transaction changes.
        /// </summary>
        public void Commit()
        {
            if (_list != null)
            {
                FileStream stream = GetStream();

                stream.SetLength(0);
                stream.Flush();

                XmlSerializer x = new XmlSerializer(typeof(RecentPlanogramsList));
                x.Serialize(stream, _list);
            }
        }

        /// <summary>
        /// Rollsback transaction changes.
        /// </summary>
        public void Rollback()
        {
            Fetch();
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Unlock();
                    _list = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }

}
