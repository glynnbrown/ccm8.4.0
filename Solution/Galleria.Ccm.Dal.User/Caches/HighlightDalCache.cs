﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
// V8-25436 : L.Luong
//  Added update to highlights and LockHighlightById
// V8-28362 : L.Ineson
//  Reworked to allow readonly.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.User.Implementation.Items;

namespace Galleria.Ccm.Dal.User.Caches
{
    /// <summary>
    /// Our Highlights dal cache
    /// </summary>
    internal class HighlightDalCache : IDalCache
    {
        #region Fields

        private readonly String _unitTestDirectory;
        private Object _lock = new Object();

        private List<HighlightItem> _items;
        private readonly List<HighlightItem> _deletedItems = new List<HighlightItem>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of highlights.
        /// </summary>
        public IEnumerable<HighlightItem> Highlights
        {
            get
            {
                if (_items == null)
                {
                    lock (_lock)
                    {
                        if (_items == null)
                        {
                            _items = new List<HighlightItem>();
                        }
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="dalFactory"></param>
        public HighlightDalCache(DalFactory dalFactory)
        {
            if (dalFactory.IsUnitTesting)
            {
                _unitTestDirectory = dalFactory.UnitTestFolder;
            }
        }

        #endregion

        #region Methods

        #region File Methods

        /// <summary>
        /// locks the highlight with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void LockFileById(String id)
        {
            HighlightItem dalObject = this.Highlights.FirstOrDefault(h => h.Id == id);

            //if the item already exists but is readonly then remove the old one.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }

            if (dalObject == null)
            {
                dalObject = HighlightItem.FetchById(id, /*asReadOnly*/false);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }
        }

        /// <summary>
        /// Unlocks and removes the highlight with the given id.
        /// </summary>
        /// <param name="id"></param>
        public void UnlockFileById(String id)
        {
            HighlightItem dalObject = this.Highlights.FirstOrDefault(h => h.Id == id);
            if (dalObject != null)
            {
                _items.Remove(dalObject);
                dalObject.Unlock();
            }
        }

        /// <summary>
        /// Returns the item with the given id
        /// or fetches and adds it if required
        /// Note this this should only be called by the Hihglight dal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HighlightItem FetchFileById(String id, Boolean allowReadOnly = false)
        {
            HighlightItem dalObject = this.Highlights.FirstOrDefault(h => h.Id == id);

            //if the item was found but is readonly then force it to be refetched.
            if (dalObject != null && dalObject.IsReadOnly)
            {
                _items.Remove(dalObject);
                dalObject = null;
            }


            if (dalObject == null && allowReadOnly)
            {
                //fetch as readonly.
                dalObject = HighlightItem.FetchById(id, /*asReadOnly*/true);
                if (dalObject != null)
                {
                    _items.Add(dalObject);
                }
            }

            return dalObject;
        }

        #endregion

        #region HighlightItem

        /// <summary>
        /// Returns the item from the cache with the given id.
        /// </summary>
        public HighlightItem FindHighlightById(String id)
        {
            return this.Highlights.FirstOrDefault(h => h.Id == id);
        }

        /// <summary>
        /// Inserts the given dto.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public HighlightItem InsertOrUpdate(HighlightDto dto)
        {
            if (this.Highlights == null) return null;

            String id = dto.Id as String;
            HighlightItem item = this.Highlights.FirstOrDefault(h => h.Id == id);

            //if we found a readonly item then drop it
            // as we are overwriting.
            if (item != null && item.IsReadOnly)
            {
                item.Unlock();
                _items.Remove(item);
                item = null;
            }


            if (item == null)
            {
                if (!String.IsNullOrEmpty(_unitTestDirectory))
                {
                    //force id as we are unit testing
                    dto.Id = Path.ChangeExtension(Path.Combine(_unitTestDirectory, dto.Name), Constants.HighlightFileExtension);
                }

                //Insert.
                item = new HighlightItem(dto);
                _items.Add(item);
            }
            else
            {
                //Update
                item.UpdateFromDto(dto);
            }

            return item;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(HighlightItem item)
        {
            if (Highlights.Contains(item))
            {
                _items.Remove(item);
                _deletedItems.Add(item);
            }
        }

        #endregion

        #region HighlightFilterItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public HighlightFilterItem InsertOrUpdate(HighlightFilterDto dto)
        {
            if (this.Highlights == null) return null;

            HighlightFilterItem item = null;

            HighlightItem highlight = FindHighlightById(Convert.ToString(dto.HighlightId));
            if (highlight != null)
            {
                item = highlight.Filters.FirstOrDefault(i => i.Id == dto.Id);
                //item = highlight.Filters.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = highlight.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HighlightFilterItem FindHighlightFilterById(Int32 id)
        {
            foreach (HighlightItem h in this.Highlights)
            {
                foreach (HighlightFilterItem f in h.Filters)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the given item
        /// </summary>
        /// <param name="item"></param>
        public void Delete(HighlightFilterItem item)
        {
            if (this.Highlights == null || Highlights == null) return;

            HighlightItem highlight = FindHighlightById(item.HighlightId);
            if (highlight != null)
            {
                highlight.Filters.Remove(item);
                highlight.MarkDirty();
            }

        }

        #endregion

        #region HighlightGroupItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public HighlightGroupItem InsertOrUpdate(HighlightGroupDto dto)
        {
            if (this.Highlights == null) return null;

            HighlightGroupItem item = null;

            HighlightItem highlight = FindHighlightById(Convert.ToString(dto.HighlightId));
            if (highlight != null)
            {
                item = highlight.Groups.FirstOrDefault(i => i.Id == dto.Id);
                //item = highlight.Groups.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = highlight.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HighlightGroupItem FindHighlightGroupById(Int32 id)
        {
            foreach (HighlightItem h in this.Highlights)
            {
                foreach (HighlightGroupItem f in h.Groups)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(HighlightGroupItem item)
        {
            if (this.Highlights == null || Highlights == null) return;

            HighlightItem highlight = FindHighlightById(item.HighlightId);
            if (highlight != null)
            {
                highlight.Groups.Remove(item);
                highlight.MarkDirty();
            }

        }

        #endregion

        #region HighlightCharacteristicItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public HighlightCharacteristicItem InsertOrUpdate(HighlightCharacteristicDto dto)
        {
            if (this.Highlights == null) return null;

            HighlightCharacteristicItem item = null;

            HighlightItem highlight = FindHighlightById(Convert.ToString(dto.HighlightId));
            if (highlight != null)
            {
                item = highlight.Characteristics.FirstOrDefault(i => i.Id == dto.Id);
                //if(item ==null) item = highlight.Characteristics.FirstOrDefault(i => i.DtoKey == dto.DtoKey);

                if (item == null)
                {
                    //insert
                    item = highlight.Insert(dto);
                }
                else
                {
                    //update
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Gets the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HighlightCharacteristicItem FindHighlightCharacteristicById(Int32 id)
        {
            foreach (HighlightItem h in this.Highlights)
            {
                foreach (HighlightCharacteristicItem f in h.Characteristics)
                {
                    if (f.Id == id)
                    {
                        return f;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(HighlightCharacteristicItem item)
        {
            if (this.Highlights == null || Highlights == null) return;

            HighlightItem highlight = FindHighlightById(item.HighlightId);
            if (highlight != null)
            {
                highlight.Characteristics.Remove(item);
                highlight.MarkDirty();
            }

        }

        #endregion

        #region HighlightCharacteristicRuleItem

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        public HighlightCharacteristicRuleItem InsertOrUpdate(HighlightCharacteristicRuleDto dto)
        {
            if (this.Highlights == null) return null;

            HighlightCharacteristicRuleItem item = null;

            HighlightCharacteristicItem parent = FindHighlightCharacteristicById(dto.HighlightCharacteristicId);
            if (parent != null)
            {
                item = parent.Rules.FirstOrDefault(i => i.Id == dto.Id);

                if (item == null)
                {
                    item = parent.Insert(dto);
                }
                else
                {
                    item.UpdateFromDto(dto);
                }
            }

            return item;
        }

        /// <summary>
        /// Finds the item with the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HighlightCharacteristicRuleItem FindHighlightCharacteristicRuleById(Int32 id)
        {
            foreach (HighlightItem h in this.Highlights)
            {
                foreach (HighlightCharacteristicItem f in h.Characteristics)
                {
                    foreach (HighlightCharacteristicRuleItem r in f.Rules)
                    {
                        if (r.Id == id)
                        {
                            return r;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="item"></param>
        public void Delete(HighlightCharacteristicRuleItem item)
        {
            HighlightCharacteristicItem parent = FindHighlightCharacteristicById(item.HighlightCharacteristicId);
            if (parent != null)
            {
                parent.Rules.Remove(item);
                parent.MarkDirty();
            }
        }

        #endregion

        /// <summary>
        /// Unlocks all highlights in this cache
        /// </summary>
        private void ClearCache()
        {
            foreach (HighlightItem h in this.Highlights)
            {
                h.Unlock();
            }
            _items.Clear();

            foreach (HighlightItem h in _deletedItems)
            {
                h.Unlock();
            }
            _deletedItems.Clear();
        }

        #endregion

        #region IDalCache Members

        /// <summary>
        /// Commits all changes made to file.
        /// </summary>
        public void Commit()
        {
            //remove any deleted items.
            foreach (HighlightItem h in _deletedItems)
            {
                h.Delete();
            }
            _deletedItems.Clear();

            //save any changed highlights.
            if (_items != null)
            {
                foreach (HighlightItem h in _items)
                {
                    if (h.IsDirty)
                    {
                        h.Save(_unitTestDirectory);
                    }
                }
            }
        }

        /// <summary>
        /// Rollsback any changes made
        /// </summary>
        public void Rollback()
        {
            List<HighlightItem> oldHighlights = this.Highlights.Union(_deletedItems).ToList();

            //clear the old lists
            _deletedItems.Clear();
            _items.Clear();

            //cycle through the old highlights unlocking files and reloading.
            List<String> reloadIds = new List<String>();
            foreach (HighlightItem old in oldHighlights)
            {
                String oldStreamName = old.CurrentStreamName;
                old.Unlock();

                if (!String.IsNullOrEmpty(oldStreamName))
                {
                    reloadIds.Add(oldStreamName);
                }
            }

            //reload
            foreach (String id in reloadIds)
            {
                FetchFileById(id, true);
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ClearCache();
                    _items = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
