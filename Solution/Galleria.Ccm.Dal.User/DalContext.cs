﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
#endregion
#region Version History: CCM820
// V8-31439 : N.Foster
//  Added TransactionInProgress property
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Dal.User
{
    /// <summary>
    /// Dal context implementation.
    /// </summary>
    public class DalContext: DalContextBase
    {
        #region Fields
        private DalFactory _dalFactory; // the DalFactory that created this DalContext.
        private Boolean _transactionInProgress = false; // indicates if a transaction is currently in progress on this context
        #endregion

        #region Properties
        /// <summary>
        /// The dal cache, taken from the dal factory.  
        /// If LockDalCache has never been called then an exception will be 
        /// thrown.
        /// </summary>
        internal DalCache DalCache
        {
            get
            {
                if (_dalFactory.LockedDalCache == null)
                {
                    throw new Exception("The dalcache must be locked before it may be accessed.");
                }
                return _dalFactory.LockedDalCache;
            }
        }

        /// <summary>
        /// Whether a transaction is currently in progress.
        /// </summary>
        public override Boolean TransactionInProgress
        {
            get { return _transactionInProgress; }
        }

        /// <summary>
        /// Returns true if we are currently unit testing
        /// </summary>
        internal Boolean IsUnitTesting
        {
            get { return _dalFactory.IsUnitTesting; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="dalFactory">the dalfactory from which this context was created.</param>
        /// <param name="dalFactoryConfig">The dal configuration.</param>
        public DalContext(DalFactory dalFactory, DalFactoryConfigElement dalFactoryConfig)
        {
            _dalFactory = dalFactory;
            LockDalCache();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Begins a transaction within this context
        /// </summary>
        public override void Begin()
        {
            _transactionInProgress = true;
        }
        
        /// <summary>
        /// Commits a transaction within this context
        /// </summary>
        public override void Commit()
        {
            DalCache.Commit();
            _transactionInProgress = false;
        }

        /// <summary>
        /// Locks the dalcache on the dalfactory
        /// </summary>
        public void LockDalCache()
        {
            if (_dalFactory.LockedDalCache == null)
            {
                _dalFactory.LockedDalCache = new DalCache(_dalFactory);
            }
        }

        /// <summary>
        /// Unlocks the dalcache on the dalfactory.
        /// </summary>
        public void UnlockDalCache()
        {
            if (_dalFactory.LockedDalCache != null)
            {
                _dalFactory.LockedDalCache.Dispose();
            }
            _dalFactory.LockedDalCache = null;
        }

        /// <summary>
        /// Called when disposing of this context
        /// </summary>
        protected override void OnDispose()
        {
            base.OnDispose();
            if (TransactionInProgress && (_dalFactory.LockedDalCache != null))
            {
                _dalFactory.LockedDalCache.Rollback();
            }
        }

        #endregion
    }
}
