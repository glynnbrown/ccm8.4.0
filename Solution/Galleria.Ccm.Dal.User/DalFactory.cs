﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;



namespace Galleria.Ccm.Dal.User
{
    /// <summary>
    /// Dal factory implementation
    /// </summary>
    public class DalFactory: DalFactoryBase
    {
        #region Fields

        private Boolean _isUnitTesting;
        private String _unitTestFolder;

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this factory has been created for
        /// the purpose of unit testing.
        /// </summary>
        public Boolean IsUnitTesting
        {
            get { return _isUnitTesting; }
            private set { _isUnitTesting = value; }
        }

        /// <summary>
        /// Gets/Sets the folder to use when unit testing
        /// </summary>
        public String UnitTestFolder
        {
            get { return _unitTestFolder; }
            set { _unitTestFolder = value; }
        }

        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig, Boolean isTesting = false) : base(dalFactoryConfig) 
        {
            IsUnitTesting = isTesting;
        }
        #endregion

        #region Propprties
        internal DalCache LockedDalCache { get; set; }
        #endregion

        #region Methods

        /// <summary>
        /// Creates a new dal context using the default configuration
        /// </summary>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext()
        {
            return new DalContext(this, DalFactoryConfig);
        }

        ///// <summary>
        ///// Creates a new dal context using the provided configuration
        ///// </summary>
        ///// <param name="dalFactoryConfig">The dal configuration</param>
        ///// <returns>A new dal context</returns>
        //public override IDalContext CreateContext(DalFactoryConfigElement dalFactoryConfig)
        //{
        //    //Only one context type may be created per factory.
        //    throw new NotImplementedException();
        //    //return new DalContext(this, dalFactoryConfig);
        //}

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (LockedDalCache != null)
                    {
                        LockedDalCache.Dispose();
                    }
                }

                _isDisposed = true;
            }
        }

        #endregion
    }
}
