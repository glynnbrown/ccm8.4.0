﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;
using Galleria.Ccm.Dal.User.Caches;

namespace Galleria.Ccm.Dal.User
{
    internal class DalCache : IDisposable
    {
        #region Fields

        private Object _lock = new Object(); //used when making this thread safe.

        private readonly DalFactory _dalFactory;
        private Dictionary<Type, IDalCache> _caches = new Dictionary<Type, IDalCache>();

        #endregion

        #region Constructor

        public DalCache(DalFactory dalFactory)
        {
            _dalFactory = dalFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a cache object that supports the 
        /// given interface
        /// </summary>
        /// <typeparam name="TDalCacheInterface"></typeparam>
        /// <param name="cacheType"></param>
        /// <returns></returns>
        public TDalCacheInterface GetCache<TDalCacheInterface>()
            where TDalCacheInterface : IDalCache
        {
            IDalCache cache;

            if (!_caches.TryGetValue(typeof(TDalCacheInterface), out cache))
            {
                lock (_lock)
                {
                    Type cacheType = typeof(TDalCacheInterface);

                    if (!_caches.TryGetValue(cacheType, out cache))
                    {
                        //create the cache
                        ConstructorInfo constructorInfo = cacheType.GetConstructor(new Type[] { typeof(DalFactory) });
                        if (constructorInfo == null)
                        {
                            throw new ArgumentOutOfRangeException("Failed to created cache");
                        }

                        cache = (IDalCache)constructorInfo.Invoke(new Object[] { _dalFactory });

                        //add to the reference dictionary.
                        _caches.Add(cacheType, cache);
                    }
                }
            }

            return (TDalCacheInterface)cache;
        }

        /// <summary>
        /// Commits the caches
        /// </summary>
        public void Commit()
        {
            foreach (IDalCache cache in _caches.Values)
            {
                cache.Commit();
            }
        }

        /// <summary>
        /// Rollsback the caches.
        /// </summary>
        public void Rollback()
        {
            foreach (IDalCache cache in _caches.Values)
            {
                cache.Rollback();
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    lock (_lock)
                    {
                        if (!_isDisposed)
                        {
                            foreach (IDalCache cache in _caches.Values)
                            {
                                cache.Dispose();
                            }
                            _caches = null;

                            _isDisposed = true;
                        }
                    }
                }
            }
        }

        #endregion
    }
}
