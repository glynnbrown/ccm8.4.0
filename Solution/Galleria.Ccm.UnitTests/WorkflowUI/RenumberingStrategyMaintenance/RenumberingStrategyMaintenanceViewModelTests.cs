﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#region Version History: CCM801

// V8-28923 : A.Silva
//      Amended some unit tests and added new ones.

#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated with new default data updates on entities
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.RenumberingStrategyMaintenance
{
    [TestFixture]
    public sealed class RenumberingStrategyMaintenanceViewModelTests : TestBase
    {
        #region Constants

        private const Int32 InitialItemCount = 20;
        private const Int32 DefaultDataCount = 1;

        #endregion

        #region Test Setup

        public override void Setup()
        {
            base.Setup();
        }

        #endregion

        #region Properties

        #region CurrentRenumberingStrategy Property Tests

        [Test]
        public void Property_CurrentRenumberingStrategy()
        {
            const String expectedBindingPropertyPath = "CurrentRenumberingStrategy";
            const String bindingPropertyPathFail = "The property's binding path was not the expected one.";

            Assert.AreEqual(expectedBindingPropertyPath, RenumberingStrategyMaintenanceViewModel.CurrentRenumberingStrategyProperty.Path, bindingPropertyPathFail);
        }

        [Test]
        public void CurrentRenumberingStrategy_WhenInitialized_ShouldBeNew()
        {
            const String expectation = "CurrentRenumberingStrategy should be new when just initialized.";
            
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Assert.IsTrue(viewModel.CurrentRenumberingStrategy.IsNew, expectation);
        }

        [Test]
        public void CurrentRenumberingStrategy_WhenInitialized_ShouldhaveEmptyName()
        {
            const String expectation = "CurrentRenumberingStrategy should be have an empty name when just initialized.";
            
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Assert.IsTrue(String.IsNullOrEmpty(viewModel.CurrentRenumberingStrategy.Name), expectation);
        }

        #endregion

        #region AvailableRenumberingStrategies Property Tests

        [Test]
        public void Property_AvailableRenumberingStrategies()
        {
            const String expectedBindingPropertyPath = "AvailableRenumberingStrategies";
            const String bindingPropertyPathFail = "The property's binding path was not the expected one.";

            Assert.AreEqual(expectedBindingPropertyPath, RenumberingStrategyMaintenanceViewModel.AvailableRenumberingStrategiesProperty.Path, bindingPropertyPathFail);
        }

        [Test]
        public void AvailableRenumberingStrategies_WhenInitialized_FetchesAvailableRenumberingStrategies()
        {
            const String expectation = "When initialized AvailableRenumberingStrategies should have been fetched.";
            InsertRenumberingStrategies(this.EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Int32 actual = viewModel.AvailableRenumberingStrategies.Count;

            Assert.AreEqual(InitialItemCount, actual, expectation);
        }

        [Test]
        public void AvailableRenumberingStrategies_WhenInsert_Updated()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            Int32 expected = viewModel.AvailableRenumberingStrategies.Count + 1;

            viewModel.CurrentRenumberingStrategy.Name = "InsertedTestItem" + expected.ToString("0000");
            viewModel.SaveCommand.Execute();

            Int32 actual = viewModel.AvailableRenumberingStrategies.Count;
            Assert.AreEqual(expected, actual);
        }

        #endregion

        #endregion

        #region Commands

        #region New Command Tests

        [Test]
        public void Command_New()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.NewCommand;
            const String expectedCommandPath = "NewCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.NewCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void NewCommand_WhenNoCreatePerm_ShouldBeDisabled()
        {
            RemoveUserPermission(DomainPermission.RenumberingStrategyCreate);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Boolean actual = viewModel.NewCommand.CanExecute();

            Assert.IsFalse(actual);
        }

        [Test]
        public void NewCommand_WhenExecuted_ShouldCreateNewCurrent()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RenumberingStrategy oldModel = viewModel.CurrentRenumberingStrategy;

            viewModel.NewCommand.Execute();

            RenumberingStrategy newModel = viewModel.CurrentRenumberingStrategy;
            Assert.AreNotEqual(oldModel, newModel);
            Assert.IsTrue(newModel.IsNew);
        }

        #endregion

        #region Open Command Tests

        [Test]
        public void Command_Open()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand<Int32?> expectedCommand = viewModel.OpenCommand;
            const String expectedCommandPath = "OpenCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.OpenCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        // Cannot test OpenCommand_WhenNoFetchPerm_ShouldBeDisabled - only requires authenticated perm.
        //[Test]
        //public void OpenCommand_WhenNoFetchPerm_ShouldBeDisabled()
        //{
        //    InsertRenumberingStrategies(this.EntityId);
        //    RemoveUserPermission(DomainPermission.RenumberingStrategyGet);
        //    RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
        //    IEnumerable<RenumberingStrategyInfo> infos = viewModel.AvailableRenumberingStrategies;
        //    if (!infos.Any()) throw new InconclusiveException("There were no available renumbering strategies to test.");

        //    foreach (RenumberingStrategyInfo info in infos)
        //    {
        //        Assert.IsTrue(viewModel.OpenCommand.CanExecute(info.Id));
        //    }
        //}

        [Test]
        public void OpenCommand_WhenExecuted_ShouldOpenById()
        {
            InsertRenumberingStrategies(this.EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            IEnumerable<RenumberingStrategyInfo> infos = viewModel.AvailableRenumberingStrategies;
            if (!infos.Any()) throw new InconclusiveException("There were no available renumbering strategies to test.");

            foreach (RenumberingStrategyInfo info in infos)
            {
                viewModel.OpenCommand.Execute(info.Id);
                RenumberingStrategy edit = viewModel.CurrentRenumberingStrategy;
                Assert.AreEqual(info.Id, edit.Id);
                Assert.IsFalse(edit.IsDirty);
            }
        }

        #endregion

        #region Save Command Tests

        [Test]
        public void Command_Save()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.SaveCommand;
            const String expectedCommandPath = "SaveCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.SaveCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void SaveCommand_WhenInvalid_ShouldBeDisabled()
        {
            const String expectation = "Should not be able to save an invalid Renumbering Strategy.";
            const String requisite = "The initial CurrentRenumberingStrategy should not be valid after startup.";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            if (viewModel.CurrentRenumberingStrategy.IsValid) Assert.Inconclusive(requisite);

            Boolean actual = viewModel.SaveCommand.CanExecute();

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void SaveCommand_WhenNoCreatePerm_ShouldBeDisabled()
        {
            const String expectation = "Should not be able to save a Renumbering Strategy without the Create Permission.";
            RemoveUserPermission(DomainPermission.RenumberingStrategyCreate);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Boolean actual = viewModel.SaveCommand.CanExecute();

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void SaveCommand_WhenNoCreatePermAndOpenExisting_ShouldBeEnabled()
        {
            const String expectation = "Should be able to save an opened Renumbering Strategy without the Create Permission.";
            InsertRenumberingStrategies(this.EntityId);
            RemoveUserPermission(DomainPermission.RenumberingStrategyCreate);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            Int32 firstAvailableId = viewModel.AvailableRenumberingStrategies.First().Id;
            viewModel.OpenCommand.Execute(firstAvailableId);
            viewModel.CurrentRenumberingStrategy.Name = "ModifiedTestItem";

            Boolean actual = viewModel.SaveCommand.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveCommand_WhenNoEditPermAndNewItem_ShouldBeEnabled()
        {
            const String expectation = "Should be able to save a new Renumbering Strategy without the Edit Permission.";
            RemoveUserPermission(DomainPermission.RenumberingStrategyEdit);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = {Name = "CreatedTestItem"}
            };

            Boolean actual = viewModel.SaveCommand.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveCommand_WhenExecuted_ShouldSaveCurrent()
        {
            const String expectation = "Should save a valid Renumbering Strategy.";
            const String savedItemName = "SavedTestItem";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = {Name = savedItemName}
            };

            viewModel.SaveCommand.Execute();

            Boolean actual = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Name == savedItemName);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region SaveAs Command Tests

        [Test]
        public void Command_SaveAs()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.SaveAsCommand;
            const String expectedCommandPath = "SaveAsCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.SaveAsCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void SaveAsCommand_WhenInvalid_ShouldBeDisabled()
        {
            const String expectation = "Should not be able to save as an invalid Renumbering Strategy.";
            const String requisite = "The initial CurrentRenumberingStrategy should not be valid after startup.";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            if (viewModel.CurrentRenumberingStrategy.IsValid) Assert.Inconclusive(requisite);

            Boolean actual = viewModel.SaveAsCommand.CanExecute();

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void SaveAsCommand_WhenNoCreatePerm_ShouldBeDisabled()
        {
            const String expectation = "Should not be able to save as a Renumbering Strategy without the Create Permission.";
            RemoveUserPermission(DomainPermission.RenumberingStrategyCreate);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            Boolean actual = viewModel.SaveAsCommand.CanExecute();

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void SaveAsCommand_WhenNoCreatePermAndOpenExisting_ShouldBeDisabled()
        {
            const String expectation = "Should be able to save as an opened Renumbering Strategy without the Create Permission.";
            InsertRenumberingStrategies(this.EntityId);
            RemoveUserPermission(DomainPermission.RenumberingStrategyCreate);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            Int32 firstAvailableId = viewModel.AvailableRenumberingStrategies.First().Id;
            viewModel.OpenCommand.Execute(firstAvailableId);
            viewModel.CurrentRenumberingStrategy.Name = "ModifiedTestItem";

            Boolean actual = viewModel.SaveAsCommand.CanExecute();

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void SaveAsCommand_WhenNoEditPermAndNewItem_ShouldBeEnabled()
        {
            const String expectation = "Should be able to save as a new Renumbering Strategy without the Edit Permission.";
            RemoveUserPermission(DomainPermission.RenumberingStrategyEdit);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = { Name = "CreatedTestItem" }
            };

            Boolean actual = viewModel.SaveCommand.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveAsCommand_WhenExecuted_ShouldSaveCurrentAs()
        {
            InsertRenumberingStrategies(EntityId);
            const String expectation = "Should save a valid Renumbering Strategy as a new item.";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableRenumberingStrategies.First().Id);
            RenumberingStrategy originalItem = viewModel.CurrentRenumberingStrategy;
            const String newItemName = "SaveAsTestItem";
            WindowService.PromptForSaveAsNameSetReponse(true, newItemName);

            viewModel.SaveAsCommand.Execute();

            Boolean actual = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Id != originalItem.Id && info.Name == newItemName);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveAsCommand_WhenExecuted_ShouldKeepOriginal()
        {
            InsertRenumberingStrategies(EntityId);
            const String expectation = "SaveAs should not affect the original item at all.";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableRenumberingStrategies.First().Id);
            RenumberingStrategy originalItem = viewModel.CurrentRenumberingStrategy;
            const String newItemName = "SaveAsTestItem";
            WindowService.PromptForSaveAsNameSetReponse(true, newItemName);

            viewModel.SaveAsCommand.Execute();

            Boolean actual = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Id == originalItem.Id && info.Name == originalItem.Name);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region SaveAndNew Command Tests

        [Test]
        public void Command_SaveAndNew()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.SaveAndNewCommand;
            const String expectedCommandPath = "SaveAndNewCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.SaveAndNewCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void SaveAndNewCommand_WhenExecuted_ShouldSaveCurrent()
        {
            const String expectation = "Should save a valid Renumbering Strategy.";
            const String savedItemName = "SavedTestItem";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = { Name = savedItemName }
            };

            viewModel.SaveAndNewCommand.Execute();

            Boolean actual = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Name == savedItemName);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveAndNewCommand_WhenExecuted_ShouldSetNewCurrent()
        {
            const String expectation = "Should set a new Renumbering Strategy as current.";
            const String savedItemName = "SavedTestItem";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = { Name = savedItemName }
            };

            viewModel.SaveAndNewCommand.Execute();

            Boolean actual = viewModel.CurrentRenumberingStrategy.IsNew;
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region SaveAndClose Command Tests

        [Test]
        public void Command_SaveAndClose()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.SaveAndCloseCommand;
            const String expectedCommandPath = "SaveAndCloseCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.SaveAndCloseCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void SaveAndCloseCommand_WhenExecuted_ShouldSaveCurrent()
        {
            const String expectation = "Should save a valid Renumbering Strategy.";
            const String savedItemName = "SavedTestItem";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = { Name = savedItemName }
            };
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);

            viewModel.SaveAndCloseCommand.Execute();

            Boolean actual = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Name == savedItemName);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void SaveAndCloseCommand_WhenExecuted_ShouldSetNewCurrent()
        {
            const String expectation = "Should set a new Renumbering Strategy as current.";
            const String savedItemName = "SavedTestItem";
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = { Name = savedItemName }
            };
            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, o => winClosed = true);

            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, expectation);
        }

        #endregion

        #region Delete Command Tests

        [Test]
        public void Command_Delete()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.DeleteCommand;
            const String expectedCommandPath = "DeleteCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.DeleteCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void DeleteCommand_WhenNewItem_ShouldBeDisabled()
        {
            const String expectation = "The Delete Command should be disabled for a new item.";
            RenumberingStrategyMaintenanceViewModel viewModel = (new RenumberingStrategyMaintenanceViewModel());

            Boolean condition = viewModel.DeleteCommand.CanExecute();

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void DeleteCommand_WhenNoDeletePermission_ShouldBeDisabled()
        {
            const String expectation = "The Delete Command should be disabled when the user is missing the Delete Permission for the model.";
            const String requisite = "The user should not have the Delete Permission for the model.";
            const String requisite2 = "The current item should not be new.";
            InsertRenumberingStrategies(EntityId);
            RemoveUserPermission(DomainPermission.RenumberingStrategyDelete);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RenumberingStrategyInfo deletedInfo = viewModel.AvailableRenumberingStrategies.First();
            viewModel.OpenCommand.Execute(deletedInfo.Id);
            if (UserHasPermission(DomainPermission.RenumberingStrategyDelete)) Assert.Inconclusive(requisite);
            if (viewModel.CurrentRenumberingStrategy.IsNew) Assert.Inconclusive(requisite2);

            Boolean condition = viewModel.DeleteCommand.CanExecute();

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void DeleteCommand_WhenExecuted_ShouldDeleteCurrent()
        {
            const String expectation = "The Delete Command should delete current when executed.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RenumberingStrategyInfo deletedInfo = viewModel.AvailableRenumberingStrategies.First();
            viewModel.OpenCommand.Execute(deletedInfo.Id);

            viewModel.DeleteCommand.Execute();

            Boolean condition = RenumberingStrategyInfoList.FetchByEntityId(EntityId).All(info => info.Id != deletedInfo.Id);
            Assert.IsTrue(condition, expectation);
        }

        [Test]
        public void DeleteCommand_WhenExecutedAndUserCancel_ShouldNoDeleteCurrent()
        {
            const String expectation = "The Delete Command should not delete current when executed but user cancels.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RenumberingStrategyInfo canceledDeleteInfo = viewModel.AvailableRenumberingStrategies.First();
            viewModel.OpenCommand.Execute(canceledDeleteInfo.Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Boolean condition = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Any(info => info.Id == canceledDeleteInfo.Id);
            Assert.IsTrue(condition, expectation);
        }

        #endregion

        #region Close Command Tests

        [Test]
        public void Command_Close()
        {
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            RelayCommand expectedCommand = viewModel.CloseCommand;
            const String expectedCommandPath = "CloseCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, viewModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, RenumberingStrategyMaintenanceViewModel.CloseCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void CloseCommand_WhenExecuted_ShouldClose()
        {
            const String expectation = "The Close Command should have closed the window.";
            Boolean condition = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, o => condition = true);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();

            viewModel.CloseCommand.Execute();

            Assert.IsTrue(condition, expectation);
        }

        #endregion

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_WhenAbandonNewItemWithoutChanges_ShouldNotWarn()
        {
            const String expectation =
                "When abandoning a new item without changes, ContinueWithItemChange should not warn.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            Boolean condition = false;
            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod, o =>
            {
                condition = true;
                o.Result = false;
            });

            viewModel.OpenCommand.Execute(viewModel.AvailableRenumberingStrategies.First().Id);

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void ContinueWithItemChange_WhenAbandonOldItemWithoutChanges_ShouldNotWarn()
        {
            const String expectation =
                "When abandoning an old item without changes, ContinueWithItemChange should not warn.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            IEnumerable<RenumberingStrategyInfo> availableRenumberingStrategies =
                viewModel.AvailableRenumberingStrategies;
            viewModel.OpenCommand.Execute(availableRenumberingStrategies.First().Id);
            Boolean condition = false;
            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod, o =>
            {
                condition = true;
                o.Result = false;
            });

            viewModel.OpenCommand.Execute(availableRenumberingStrategies.Skip(1).First().Id);

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void ContinueWithItemChange_WhenAbandonNewItemWithChanges_ShouldWarn()
        {
            const String expectation = "When abandoning a new item with changes, ContinueWithItemChange should warn.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel
            {
                CurrentRenumberingStrategy = {Name = "UpdatedNewItem"}
            };
            Boolean condition = false;
            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod, o =>
            {
                condition = true;
                o.Result = false;
            });

            viewModel.OpenCommand.Execute(viewModel.AvailableRenumberingStrategies.First().Id);

            Assert.IsTrue(condition, expectation);
        }

        [Test]
        public void ContinueWithItemChange_WhenAbandonOldItemWithChanges_ShouldWarn()
        {
            const String expectation = "When abandoning an old item with changes, ContinueWithItemChange should warn.";
            InsertRenumberingStrategies(EntityId);
            RenumberingStrategyMaintenanceViewModel viewModel = new RenumberingStrategyMaintenanceViewModel();
            IEnumerable<RenumberingStrategyInfo> availableRenumberingStrategies =
                viewModel.AvailableRenumberingStrategies;
            viewModel.OpenCommand.Execute(availableRenumberingStrategies.First().Id);
            viewModel.CurrentRenumberingStrategy.Name = "UpdatedNewItem";
            Boolean condition = false;
            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod, o =>
            {
                condition = true;
                o.Result = false;
            });

            viewModel.OpenCommand.Execute(availableRenumberingStrategies.Skip(1).First().Id);

            Assert.IsTrue(condition, expectation);
        }

        #endregion

        #region GetRenumberingStrategiesMatches

        [Test]
        public void GetRenumberingStrategiesMatches_WhenNoNameCriteria_ShouldReturnAllAvailable()
        {
            const String expectation =
                "GetRenumberingStrategiesMatches should return all available with no name criteria.";
            InsertRenumberingStrategies(EntityId);
            IEnumerable<Int32> expected = RenumberingStrategyInfoList.FetchByEntityId(EntityId).Select(info => info.Id);

            IEnumerable<Int32> actual =
                new RenumberingStrategyMaintenanceViewModel().GetRenumberingStrategiesMatches(String.Empty).Select(
                    info => info.Id);

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void GetRenumberingStrategiesMatches_WhenNameCriteriaDoesNotMatchAny_ShouldReturnNone()
        {
            const String expectation =
                "GetRenumberingStrategiesMatches should return none available with a name criteria without matches.";
            InsertRenumberingStrategies(EntityId);

            IEnumerable<Int32> actual =
                new RenumberingStrategyMaintenanceViewModel().GetRenumberingStrategiesMatches("NoMatch").Select(
                    info => info.Id);

            CollectionAssert.IsEmpty(actual, expectation);
        }

        [Test]
        public void GetRenumberingStrategiesMatches_WhenameCriteriaHasMatches_ShouldReturnMatches()
        {
            const String expectation =
                "GetRenumberingStrategiesMatches should return all matches for the name criteria.";
            InsertRenumberingStrategies(EntityId);
            IEnumerable<Int32> expected =
                RenumberingStrategyInfoList.FetchByEntityId(EntityId).Where(info => info.Name.Contains("01")).Select(
                    info => info.Id);

            IEnumerable<Int32> actual =
                new RenumberingStrategyMaintenanceViewModel().GetRenumberingStrategiesMatches("01").Select(
                    info => info.Id);

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region Static Helper Methods

        private static void InsertRenumberingStrategies(Int32 entityId)
        {
            for (Int32 i = 1; i <= InitialItemCount; i++)
            {
                RenumberingStrategy item = RenumberingStrategy.NewRenumberingStrategy(entityId);
                item.Name = "TestItem" + i.ToString("0000");
                item.Save();
            }
        }

        private static void RemoveUserPermission(DomainPermission permission)
        {
            DomainPrincipal domainPrincipal = (DomainPrincipal)Csla.ApplicationContext.User;
            DomainIdentity domainIdentity = (DomainIdentity)domainPrincipal.Identity;
            domainIdentity.Roles.Remove(permission.ToString());
        }

        private static Boolean UserHasPermission(DomainPermission permission)
        {
            DomainPrincipal domainPrincipal = (DomainPrincipal)Csla.ApplicationContext.User;
            DomainIdentity domainIdentity = (DomainIdentity)domainPrincipal.Identity;
            return domainIdentity.Roles.Any(s => s == permission.ToString());
        }

        #endregion
    }
}
