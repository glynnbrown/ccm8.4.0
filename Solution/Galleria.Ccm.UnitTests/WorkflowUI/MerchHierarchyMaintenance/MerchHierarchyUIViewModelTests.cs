﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25450 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using System.Collections;
using System.ComponentModel;
using System.Windows.Media;
using Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;


namespace Galleria.Ccm.UnitTests.WorkflowUI.MerchHierarchyMaintenance
{
    [TestFixture]
    public class MerchHierarchyUIViewModelTests : TestBase<MerchHierarchyViewModel>
    {
        #region Test Fixture Helpers

        private ProductHierarchy _hierarchy;

        public override void  Setup()
        {
            //setup the base
            base.Setup();

            //clear the existing hierarchy
            _hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            _hierarchy.RootGroup.Name = "Root";
            _hierarchy.RootGroup.Code = "Root";
            _hierarchy.RootGroup.ChildList.Clear();
            _hierarchy.RootLevel.Name = "Level1";
            _hierarchy.RootLevel.ChildLevel = null;
            _hierarchy = _hierarchy.Save();
            _hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);

            //create a suitable test hierarchy

            //5 levels
            ProductLevel level2 = _hierarchy.RootLevel.ChildLevel;
            if (level2 == null)
            {
                level2 = ProductLevel.NewProductLevel();
                _hierarchy.RootLevel.ChildLevel = level2;
            }
            level2.Name = "Level2";

            ProductLevel level3 = level2.ChildLevel;
            if (level3 == null)
            {
                level3 = ProductLevel.NewProductLevel();
                level2.ChildLevel = level3;
            }
            level3.Name = "Level3";


            ProductLevel level4 = level3.ChildLevel;
            if (level4 == null)
            {
                level4 = ProductLevel.NewProductLevel();
                level3.ChildLevel = level4;
            }
            level4.Name = "Level4";

            ProductLevel level5 = level4.ChildLevel;
            if (level5 == null)
            {
                level5 = ProductLevel.NewProductLevel();
                level4.ChildLevel = level5;
            }
            level5.Name = "Level5";

            //create groups
            _hierarchy.RootGroup.ChildList.Clear();

            ProductGroup groupY = ProductGroup.NewProductGroup(level2.Id);
            groupY.Code = "Y";
            groupY.Name = "Y";
            _hierarchy.RootGroup.ChildList.Add(groupY);

            ProductGroup groupA = ProductGroup.NewProductGroup(level2.Id);
            groupA.Code = "A";
            groupA.Name = "A";
            _hierarchy.RootGroup.ChildList.Add(groupA);

            ProductGroup groupZ = ProductGroup.NewProductGroup(level3.Id);
            groupZ.Code = "Z";
            groupZ.Name = "Z";
            groupA.ChildList.Add(groupZ);

            ProductGroup groupC = ProductGroup.NewProductGroup(level3.Id);
            groupC.Code = "C";
            groupC.Name = "C";
            groupA.ChildList.Add(groupC);


            ProductGroup groupB = ProductGroup.NewProductGroup(level2.Id);
            groupB.Code = "B";
            groupB.Name = "B";
            _hierarchy.RootGroup.ChildList.Add(groupB);

            ProductGroup groupE = ProductGroup.NewProductGroup(level3.Id);
            groupE.Code = "E";
            groupE.Name = "E";
            groupB.ChildList.Add(groupE);

            ProductGroup groupF = ProductGroup.NewProductGroup(level4.Id);
            groupF.Code = "F";
            groupF.Name = "F";
            groupE.ChildList.Add(groupF);

            ProductGroup groupG = ProductGroup.NewProductGroup(level4.Id);
            groupG.Code = "G";
            groupG.Name = "G";
            groupE.ChildList.Add(groupG);

            ProductGroup groupH = ProductGroup.NewProductGroup(level5.Id);
            groupH.Code = "H";
            groupH.Name = "H";
            groupG.ChildList.Add(groupH);

            ProductGroup groupI = ProductGroup.NewProductGroup(level5.Id);
            groupI.Code = "I";
            groupI.Name = "I";
            groupG.ChildList.Add(groupI);

            //save
            _hierarchy = _hierarchy.Save();
            groupY = _hierarchy.RootGroup.ChildList[0];
            groupA = _hierarchy.RootGroup.ChildList[1];
            groupZ = groupA.ChildList[0];
            groupC = groupA.ChildList[1];
            groupB = _hierarchy.RootGroup.ChildList[2];
            groupE = groupB.ChildList[0];
            groupF = groupE.ChildList[0];
            groupG = groupE.ChildList[1];
            groupH = groupG.ChildList[0];
            groupI = groupG.ChildList[1];

            //refetch
            _hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);

            this.TestModel = new MerchHierarchyViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_CurrentStructure()
        {
            String propertyName = MerchHierarchyViewModel.CurrentStructureProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.AreEqual(_hierarchy.Id, this.TestModel.CurrentStructure.Id);
        }

        [Test]
        public void Property_FlattenedLevels()
        {
            String propertyName = "FlattenedLevels";
            Assert.AreEqual(propertyName, MerchHierarchyViewModel.FlattenedLevelsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.FlattenedLevels);


            ProductLevel[] levels = this.TestModel.CurrentStructure.EnumerateAllLevels().ToArray();
            ProductLevel[] flattenedLevels = this.TestModel.FlattenedLevels.ToArray();

            Assert.AreEqual(levels.Count(), flattenedLevels.Count());
            for (Int32 i = 0; i < levels.Count(); i++)
            {
                Assert.AreEqual(levels[i], flattenedLevels[i]);
            }
        }

        [Test]
        public void Property_SelectedLevel()
        {
            String propertyName = "SelectedLevel";
            Assert.AreEqual(propertyName, MerchHierarchyViewModel.SelectedLevelProperty.Path);

            //check setter
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedLevel = this.TestModel.CurrentStructure.RootLevel;
            Assert.Contains(propertyName, base.PropertyChangedNotifications, "Change notification should have fired");
            Assert.AreEqual(this.TestModel.CurrentStructure.RootLevel, this.TestModel.SelectedLevel);

            //check selected level changed event handler
            Assert.Contains(MerchHierarchyViewModel.SelectedUnitProperty.Path, base.PropertyChangedNotifications);
            Assert.IsNull(this.TestModel.SelectedUnit);
        }

        [Test]
        public void Property_FlattenedUnits()
        {
            String propertyName = "FlattenedUnits";
            Assert.AreEqual(propertyName, MerchHierarchyViewModel.FlattenedUnitsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.FlattenedUnits);

            foreach (ProductGroup grp in
                this.TestModel.CurrentStructure.EnumerateAllGroups())
            {
                Assert.IsNotNull(this.TestModel.FlattenedUnits.FirstOrDefault(g => g.ProductGroup == grp),
                    "A matching groupview should have been found");
            }
        }

        [Test]
        public void Property_SelectedUnit()
        {
            String propertyName = "SelectedUnit";
            Assert.AreEqual(propertyName, MerchHierarchyViewModel.SelectedUnitProperty.Path);


            //check setter
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First();
            Assert.Contains(propertyName, base.PropertyChangedNotifications, "Change notification should have fired");
            Assert.AreEqual(this.TestModel.FlattenedUnits.First(), this.TestModel.SelectedUnit);

            //also check selected unit changed event handler
            Assert.Contains(MerchHierarchyViewModel.SelectedLevelProperty.Path, base.PropertyChangedNotifications);
            Assert.IsNull(this.TestModel.SelectedLevel);

        }

        #endregion

        #region Commands

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //add a level
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.Last();
            this.TestModel.AddLevelCommand.Execute();
            this.TestModel.CurrentStructure.RootLevel.ChildLevel.Name = "new last level";

            //add a group
            ProductGroup newGroup = ProductGroup.NewProductGroup(this.TestModel.CurrentStructure.RootLevel.ChildLevel.Id);
            this.TestModel.CurrentStructure.RootGroup.ChildList.Add(newGroup);


            //check can execute states

            //model not valid - disabled
            newGroup.Name = null;
            newGroup.Code = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as node has no name, structure invalid");

            //code is duplicate - disabled
            newGroup.Name = "group1";
            newGroup.Code = this.TestModel.CurrentStructure.RootGroup.Code;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as 2 groups have the same code.");

            //valid - enabled
            newGroup.Name = "group1";
            newGroup.Code = this.TestModel.CurrentStructure.GetNextAvailableDefaultGroupCode(); ;
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");

            //check execution
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            Assert.IsFalse(this.TestModel.CurrentStructure.IsDirty);

            Assert.AreEqual(this.TestModel.FlattenedLevels.First(), this.TestModel.CurrentStructure.RootLevel);
            Assert.AreEqual(this.TestModel.FlattenedUnits.First().ProductGroup, this.TestModel.CurrentStructure.RootGroup);

            //fetch the hierarchy back
            ProductHierarchy hierarchy = ProductHierarchy.FetchById(this.TestModel.CurrentStructure.Id);
            Assert.AreEqual(this.TestModel.CurrentStructure.RowVersion, hierarchy.RowVersion);
            Assert.AreEqual(_hierarchy.RowVersion + 1, hierarchy.RowVersion, "The row version should be increased. (Not passing the original structure anymore)");

        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);
        }

        [Test]
        public void Command_AddLevel()
        {
            RelayCommand cmd = this.TestModel.AddLevelCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states

            //No level selected - disabled
            this.TestModel.SelectedLevel = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as no level is selected");

            //Root level selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => l.IsRoot);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as root is selected");

            //Non-lowest level selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel != null);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as no lowest level is selected");

            //lowesy level selected - enabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel == null);
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");


            //check execution

            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel == null);
            ProductLevel selectedLevel = this.TestModel.SelectedLevel;
            ProductLevel preChild = this.TestModel.SelectedLevel.ChildLevel;

            cmd.Execute();

            Assert.AreNotEqual(preChild, selectedLevel.ChildLevel, "parent child should have changed");
            Assert.AreEqual(this.TestModel.SelectedLevel.ParentLevel, selectedLevel, "New level should be selected");

            Assert.Contains(this.TestModel.SelectedLevel, this.TestModel.FlattenedLevels, "New level should be in the flattened levels collection");
            Assert.Contains(this.TestModel.SelectedLevel.Name, this.TestModel.FlattenedUnits.First().LevelPathValues.Keys, "Level should now be in group level path");

            Assert.IsTrue(this.TestModel.SelectedLevel.IsNew, "Level should be new");
            Assert.IsNotNull(this.TestModel.SelectedLevel.Name, "Level should have a default name");

            IEnumerable<ProductLevel> structureLevels = this.TestModel.CurrentStructure.EnumerateAllLevels();
            Assert.AreEqual(structureLevels.Count(), structureLevels.Select(l => l.Name).Distinct().Count(), "All level names should be distinct");

        }

        [Test]
        public void Command_RemoveLevel()
        {
            //ensure the structure has at least 3 levels
            ProductLevel child1 = this.TestModel.CurrentStructure.RootLevel.ChildLevel;
            if (child1 == null)
            {
                this.TestModel.CurrentStructure.RootLevel.ChildLevel = ProductLevel.NewProductLevel();
            }
            ProductLevel child2 = this.TestModel.CurrentStructure.RootLevel.ChildLevel.ChildLevel;
            if (child2 == null)
            {
                this.TestModel.CurrentStructure.RootLevel.ChildLevel.ChildLevel = ProductLevel.NewProductLevel();
            }

            RelayCommand cmd = this.TestModel.RemoveLevelCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //add n a level for testing
            this.TestModel.SelectedLevel = this.TestModel.CurrentStructure.RootLevel;
            this.TestModel.AddLevelCommand.Execute();

            //check can execute states

            //No selected level - disabled
            this.TestModel.SelectedLevel = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as no level is selected");

            //root level selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.CurrentStructure.RootLevel;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as root level is selected");

            //non root, non lowest level selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel != null);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled non lowest level is selected");

            //lowest level selected but groups reside at this level - enabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel == null);
            Assert.IsTrue(this.TestModel.FlattenedUnits.Any(g => g.AssociatedLevel == this.TestModel.SelectedLevel));
            Assert.IsTrue(cmd.CanExecute());

            //lowest level but no groups - enabled
            this.TestModel.AddLevelCommand.Execute();
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First(l => !l.IsRoot && l.ChildLevel == null);
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");

            //check execution
            ProductLevel levelToRemove = this.TestModel.SelectedLevel;
            cmd.Execute();
            Assert.IsNull(this.TestModel.FlattenedLevels.FirstOrDefault(l => l.Id.Equals(levelToRemove.Id)));
            Assert.AreEqual(levelToRemove.ParentLevel, this.TestModel.SelectedLevel, "The parent level should now be selected");
        }

        [Test]
        public void Command_AddUnit()
        {
            RelayCommand cmd = this.TestModel.AddUnitCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states

            //No selected unit - disabled
            this.TestModel.SelectedLevel = this.TestModel.CurrentStructure.RootLevel;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no unit selected");

            //Selected unit is on the bottom level - disabled [GFS-22251]
            Object lowestLevelRef = this.TestModel.FlattenedLevels.Last().Id;
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.AssociatedLevel.Id.Equals(lowestLevelRef));
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as lowest level unit selected");

            //Selected unit with child units - enabled [GFS-22251]
            this.TestModel.SelectedUnit =
                this.TestModel.FlattenedUnits.First(g => g.ProductGroup.ChildList.Count > 0);
            Assert.IsTrue(cmd.CanExecute(), "Enabled as products are not directly against the selected unit.");

            //Root unit - enabled 
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.IsRoot);
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");


            //nb - cannot test execute as this opens a window.
        }

        [Test]
        public void Command_EditUnit()
        {
            RelayCommand cmd = this.TestModel.EditUnitCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "FriendlyName should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.IsNotNull(cmd.Icon, "Icon should be populated");

            //check can execute

            //No unit selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.CurrentStructure.RootLevel;
            Assert.IsFalse(cmd.CanExecute(), "No unit is selected so should be disabled");

            //Root unit selected - enabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First();
            Assert.IsTrue(cmd.CanExecute());

            //nb - cannot test window open.
        }

        [Test]
        public void Command_RemoveUnit()
        {
            RelayCommand cmd = this.TestModel.RemoveUnitCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states

            //No unit selected - disabled
            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.First();
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as no unit is selected");

            //Root unit selected - disabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.IsRoot);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled as root unit is selected");

            //unit with no child groups selected - enabled 
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => !g.ProductGroup.IsRoot && g.ProductGroup.ChildList.Count == 0);
            Assert.IsTrue(cmd.CanExecute());

            //unit with child units selected - enabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => !g.ProductGroup.IsRoot && g.ProductGroup.ChildList.Count > 0);
            Assert.IsTrue(cmd.CanExecute());


            //check execution
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => !g.ProductGroup.IsRoot);
            ProductGroup removedGroup = this.TestModel.SelectedUnit.ProductGroup;


            Int32 preCount = removedGroup.ParentGroup.ChildList.Count;
            cmd.Execute();
            Assert.AreEqual(preCount - 1, removedGroup.ParentGroup.ChildList.Count);

            //get the flattened unit for the removed group
            var newFlattenedUnit = this.TestModel.FlattenedUnits.FirstOrDefault(g => g.ProductGroup == removedGroup);
            Assert.IsNull(newFlattenedUnit, "A flattened unit should have been removed");
        }

        [Test]
        public void Command_MoveSelectedUnit()
        {
            RelayCommand cmd = this.TestModel.MoveSelectedUnitCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check can execute
            ProductGroupViewModel newParent = null;

            //no selected unit - disabled
            this.TestModel.SelectedUnit = null;
            Assert.IsFalse(cmd.CanExecute(newParent));

            //root selected - disabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.IsRoot);
            Assert.IsFalse(cmd.CanExecute(newParent));

            //no valid param - disabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.Last();
            Assert.IsFalse(cmd.CanExecute(5));

            //selected unit is already a child of that to move to - enabled as whilst pointless this move is still ok
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.Last();
            newParent = this.TestModel.FlattenedUnits.First(g => g.ProductGroup == this.TestModel.SelectedUnit.ProductGroup.ParentGroup);
            Assert.IsTrue(cmd.CanExecute(newParent));

            //same node selected - disabled
            this.TestModel.SelectedUnit = newParent;
            Assert.IsFalse(cmd.CanExecute(newParent));

            //new parent is not at same level as old parent - enabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.Last();
            newParent = this.TestModel.FlattenedUnits.First(g => g.ProductGroup != this.TestModel.SelectedUnit.ProductGroup.ParentGroup
                && g.AssociatedLevel != this.TestModel.SelectedUnit.AssociatedLevel.ParentLevel);
            Assert.IsTrue(cmd.CanExecute(newParent));

            //new parent associated level has no child level - disabled
            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "H");
            newParent = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "I");
            Assert.IsFalse(cmd.CanExecute(newParent));

            this.TestModel.SelectedLevel = this.TestModel.FlattenedLevels.Last();
            this.TestModel.AddLevelCommand.Execute();

            this.TestModel.SelectedUnit = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "H");
            newParent = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "I");
            Assert.IsTrue(cmd.CanExecute(newParent));


            //check execute
            Object selectedGroupId = this.TestModel.SelectedUnit.ProductGroup.Id;
            ProductGroup oldParent = this.TestModel.SelectedUnit.ProductGroup.ParentGroup;
            Int32 oldParentCount = oldParent.ChildList.Count;

            cmd.Execute(newParent);

            Assert.AreEqual(oldParentCount - 1, oldParent.ChildList.Count);
            Assert.IsTrue(newParent.ProductGroup.ChildList.Contains(this.TestModel.SelectedUnit.ProductGroup));
            Assert.AreEqual(selectedGroupId, this.TestModel.SelectedUnit.ProductGroup.Id);
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_CollapseAll()
        {
            RelayCommand cmd = this.TestModel.CollapseAllCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_ExpandAll()
        {
            RelayCommand cmd = this.TestModel.ExpandAllCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region Others

        [Test]
        public void GEM22251_LevelNamesAreForcedUnique()
        {
            ProductLevel level = this.TestModel.CurrentStructure.EnumerateAllLevels().Last();
            this.TestModel.SelectedLevel = level;

            //add a new level
            this.TestModel.AddLevelCommand.Execute();
            Assert.AreEqual(this.TestModel.SelectedLevel, level.ChildLevel, "the new level should be selected");

            //change its name to be the same as its parent level
            this.TestModel.SelectedLevel.Name = level.Name;

            Assert.AreNotEqual(this.TestModel.SelectedLevel.Name, level.Name, "Set of name to an existing not allowed");
        }

        [Test]
        public void GEM22251_MovingGroupToNewLevelUpdatesId()
        {
            //Node I is currently a child of Node G.
            //Move it to be a child of Node E instead and check that the associated level is updated.

            ProductGroupViewModel nodeI = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "I");
            ProductGroupViewModel nodeG = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "G");
            ProductGroupViewModel nodeE = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "E");

            Assert.AreEqual(nodeI.ProductGroup.ParentGroup, nodeG.ProductGroup);
            Assert.AreNotEqual(nodeI.AssociatedLevel, nodeE.AssociatedLevel.ChildLevel);

            this.TestModel.SelectedUnit = nodeI;
            Assert.IsTrue(this.TestModel.MoveSelectedUnitCommand.CanExecute(nodeE));
            this.TestModel.MoveSelectedUnitCommand.Execute(nodeE);

            //have to reget nodei as the instance will have changed
            ProductGroupViewModel newNodeI = this.TestModel.FlattenedUnits.First(g => g.ProductGroup.Code == "I");
            Assert.AreNotEqual(nodeI, newNodeI);

            Assert.AreEqual(newNodeI.AssociatedLevel, nodeE.AssociatedLevel.ChildLevel);
        }

        #endregion
    }
}
