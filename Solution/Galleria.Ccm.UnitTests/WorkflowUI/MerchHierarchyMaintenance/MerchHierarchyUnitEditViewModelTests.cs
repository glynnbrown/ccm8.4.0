﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25450 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.MerchHierarchyMaintenance
{
    [TestFixture]
    public class MerchHierarchyUnitEditViewModelTests : TestBase<MerchHierarchyUnitEditViewModel>
    {
        #region Test Fixture Helpers

        private ProductHierarchyViewModel _prodHierarchyView;

        public override void Setup()
        {
            base.Setup();

            _prodHierarchyView = new ProductHierarchyViewModel();
            _prodHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();

            PopulateProductHierarchy(_prodHierarchyView.Model.Id);

            _prodHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();

            this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, null);
        }

        /// <summary>
        /// Populate a product hierarchy
        /// </summary>
        /// <param name="hierarchyId"></param>
        /// <returns></returns>
        public static ProductHierarchy PopulateProductHierarchy(Int32 hierarchyId)
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchById(hierarchyId);

            //add 2 levels
            ProductLevel level1 = hierarchy.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
                level1.Name = "level1";
            }
            ProductLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = hierarchy.InsertNewChildLevel(level1);
                level2.Name = "level2";
            }

            

            //add 3 level1 groups
            ProductGroup level1Group1 = ProductGroup.NewProductGroup(level1.Id);
            level1Group1.Name = "level1Group1";
            level1Group1.Code = "level1Group1Code";
            ProductGroup level1Group2 = ProductGroup.NewProductGroup(level1.Id);
            level1Group2.Name = "level1Group2";
            level1Group2.Code = "level1Group2Code";
            ProductGroup level1Group3 = ProductGroup.NewProductGroup(level1.Id);
            level1Group3.Name = "level1Group3";
            level1Group3.Code = "level1Group3Code";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            Int32 groupNo = 4;
            foreach (ProductGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    ProductGroup level2Group = ProductGroup.NewProductGroup(level2.Id);
                    level2Group.Name = "level2group" + i.ToString();
                    level2Group.Code = groupNo.ToString();
                    level1Group.ChildList.Add(level2Group);

                    groupNo++;
                }
            }

            return hierarchy.Save();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedGroup()
        {
            String propertyName = MerchHierarchyUnitEditViewModel.SelectedGroupProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsNotNull(this.TestModel.SelectedGroup);
        }

        [Test]
        public void Property_AvailableLevels()
        {
            String propertyName = MerchHierarchyUnitEditViewModel.AvailableLevelsProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableLevels);

            IEnumerable<ProductGroup> groups = _prodHierarchyView.Model.EnumerateAllGroups();

            //should contain all levels, except for the root and any that do not have a group
            foreach (ProductLevel level in _prodHierarchyView.Model.EnumerateAllLevels())
            {
                Boolean isValidLevel = (!level.IsRoot);
                if (isValidLevel)
                {
                    isValidLevel = (groups.Where(g => Object.Equals(g.ProductLevelId, level.ParentLevel.Id)).Count() > 0);
                }

                if (isValidLevel)
                {
                    Assert.Contains(level, this.TestModel.AvailableLevels);
                }
                else
                {
                    Assert.IsFalse(this.TestModel.AvailableLevels.Contains(level),
                        "Should not contain the root or any levels without available parent groups");
                }
            }
        }

        [Test]
        public void Property_SelectedLevel()
        {
            String propertyName = MerchHierarchyUnitEditViewModel.SelectedLevelProperty.Path;

            Assert.IsNotNull(this.TestModel.SelectedLevel, "Should start with a level selected");

            IEnumerable<ProductGroup> allGroups = _prodHierarchyView.Model.EnumerateAllGroups();
            foreach (ProductLevel level in this.TestModel.AvailableLevels)
            {
                this.TestModel.SelectedLevel = level;
                Assert.AreEqual(this.TestModel.SelectedLevel, level);

                IEnumerable<ProductGroup> expectedAvailableParents = allGroups.Where(g => Object.Equals(g.ProductLevelId, level.ParentLevel.Id));
                Assert.AreEqual(expectedAvailableParents.Count(), this.TestModel.AvailableParents.Count);
                foreach (ProductGroup parentGroup in expectedAvailableParents)
                {
                    Assert.Contains(parentGroup, this.TestModel.AvailableParents);
                }
            }
        }

        [Test]
        public void Property_AvailableParents()
        {
            String propertyName = MerchHierarchyUnitEditViewModel.AvailableParentsProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableParents);
            Assert.IsNotNull(this.TestModel.AvailableParents);

            IEnumerable<ProductGroup> expectedAvailableParents =
                _prodHierarchyView.Model.EnumerateAllGroups().Where(g => Object.Equals(g.ProductLevelId, this.TestModel.SelectedLevel.ParentLevel.Id));
            Assert.AreEqual(expectedAvailableParents.Count(), this.TestModel.AvailableParents.Count);
            foreach (ProductGroup parentGroup in expectedAvailableParents)
            {
                Assert.Contains(parentGroup, this.TestModel.AvailableParents);
            }
        }

        [Test]
        public void Property_SelectedParent()
        {
            String propertyName = MerchHierarchyUnitEditViewModel.SelectedParentProperty.Path;

            Assert.IsNotNull(this.TestModel.SelectedParent);

            foreach (ProductGroup group in _prodHierarchyView.Model.EnumerateAllGroups())
            {
                //if not the root
                if (group.ParentGroup != null)
                {
                    ProductLevel parentLevel = _prodHierarchyView.Model.EnumerateAllLevels().FirstOrDefault(l => Object.Equals(l.Id, group.ProductLevelId));
                    if (parentLevel.ChildLevel != null)
                    {

                        this.TestModel.SelectedParent = group;
                        Assert.AreEqual(this.TestModel.SelectedParent, group);

                        Assert.AreEqual(this.TestModel.SelectedLevel.ParentLevel.Id, group.ProductLevelId);
                    }
                }
            }

        }

        [Test]
        public void Property_GroupCode()
        {
            String propertyName = "GroupCode";
            Assert.AreEqual(propertyName, MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path);

            //getter
            Assert.AreEqual(this.TestModel.SelectedGroup.Code, this.TestModel.GroupCode);

            //setter
            String newValue = "xxxNewCode";
            base.PropertyChangedNotifications.Clear();
            this.TestModel.GroupCode = newValue;
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(newValue, this.TestModel.GroupCode);
            Assert.AreEqual(newValue, this.TestModel.SelectedGroup.Code);

        }

        [Test]
        public void Property_GroupName()
        {
            String propertyName = "GroupName";
            Assert.AreEqual(propertyName, MerchHierarchyUnitEditViewModel.GroupNameProperty.Path);

            //getter
            Assert.AreEqual(this.TestModel.SelectedGroup.Name, this.TestModel.GroupName);

            //setter
            String newValue = "xxxNewCode";
            base.PropertyChangedNotifications.Clear();
            this.TestModel.GroupName = newValue;
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(newValue, this.TestModel.GroupName);
            Assert.AreEqual(newValue, this.TestModel.SelectedGroup.Name);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //no group provided
            this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, null);
            Assert.IsNotNull(this.TestModel.SelectedGroup);
            Assert.IsTrue(this.TestModel.SelectedGroup.IsNew);

            //group provided
            foreach (ProductGroup group in _prodHierarchyView.Model.EnumerateAllGroups())
            {
                if (group.ParentGroup != null)
                {
                    this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, group);
                    Assert.AreEqual(group.ParentGroup, this.TestModel.SelectedParent);
                    Assert.AreEqual(group.ProductLevelId, this.TestModel.SelectedLevel.Id);
                }
            }
        }


        #endregion

        #region Commands

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = this.TestModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);

            //check canexecute
            this.TestModel.SelectedGroup.Name = "unit";

            this.TestModel.SelectedGroup.Code = String.Empty;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as is invalid");
            this.TestModel.SelectedGroup.Code = Guid.NewGuid().ToString().Substring(0, 10);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            ProductGroup group = this.TestModel.SelectedGroup;
            ProductGroup selectedParent = this.TestModel.SelectedParent;
            ProductLevel selectedLevel = this.TestModel.SelectedLevel;

            cmd.Execute();

            Assert.Contains(group, selectedParent.ChildList);
            Assert.AreEqual(selectedLevel.Id, group.ProductLevelId);

            //nb cant test close
        }

        [Test]
        public void Command_ApplyAndNew()
        {
            RelayCommand cmd = this.TestModel.ApplyAndNewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);
            this.TestModel.SelectedGroup.Name = "unit";

            //check canexecute
            this.TestModel.SelectedGroup.Code = String.Empty;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as is invalid");
            this.TestModel.SelectedGroup.Code = Guid.NewGuid().ToString().Substring(0, 10);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            ProductGroup group = this.TestModel.SelectedGroup;
            ProductGroup selectedParent = this.TestModel.SelectedParent;
            ProductLevel selectedLevel = this.TestModel.SelectedLevel;

            cmd.Execute();

            Assert.Contains(group, selectedParent.ChildList);
            Assert.AreEqual(selectedLevel.Id, group.ProductLevelId);

            //check a new group has been loaded
            Assert.AreNotEqual(this.TestModel.SelectedGroup, group);
            Assert.AreEqual(selectedParent, this.TestModel.SelectedParent);
            Assert.AreEqual(selectedLevel, this.TestModel.SelectedLevel);
        }


        #endregion

        #region Other

        [Test]
        public void NewGroupsOnInvalidLevelsBecomeAvailable()
        {
            throw new InconclusiveException("Not sure if this is still relevant");

            //ensure the hierarchy has at least 2 levels
            ProductLevel level1 = _prodHierarchyView.Model.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = ProductLevel.NewProductLevel();
                level1.Name = _prodHierarchyView.Model.GetNextAvailableLevelName(_prodHierarchyView.Model.RootLevel);
                _prodHierarchyView.Model.RootLevel.ChildLevel = level1;
            }

            ProductLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = ProductLevel.NewProductLevel();
                level2.Name = _prodHierarchyView.Model.GetNextAvailableLevelName(level1);
                level1.ChildLevel = level1;
            }

            //start with a new item
            this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, null);


            //get an invalid level
            IEnumerable<ProductLevel> invalidLevels =
                _prodHierarchyView.Model.EnumerateAllLevels().Where(l => !l.IsRoot && !this.TestModel.AvailableLevels.Contains(l));

            Assert.AreNotEqual(0, invalidLevels.Count(), "need an invalid level for this test");
            ProductLevel invalidLevel = invalidLevels.First();

            //select the lowest available level
            this.TestModel.SelectedLevel = this.TestModel.AvailableLevels.Last();
            this.TestModel.SelectedGroup.Name = "unit";

            //add
            ProductGroup newGroup = this.TestModel.SelectedGroup;
            this.TestModel.ApplyAndNewCommand.Execute();

            //check the invalid level is now available with the previously added node as the only available parent
            Assert.Contains(invalidLevel, this.TestModel.AvailableLevels);
            this.TestModel.SelectedLevel = invalidLevel;
            Assert.AreEqual(1, this.TestModel.AvailableParents.Count);
            Assert.AreEqual(newGroup, this.TestModel.AvailableParents[0]);
        }

        [Test]
        public void GEM21862_CodeUniqueCheckIsCaseInsensitive()
        {
            //ensure the hierarchy has at least 2 levels
            ProductLevel level1 = _prodHierarchyView.Model.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = ProductLevel.NewProductLevel();
                level1.Name = _prodHierarchyView.Model.GetNextAvailableLevelName(_prodHierarchyView.Model.RootLevel);
                _prodHierarchyView.Model.RootLevel.ChildLevel = level1;
            }

            ProductLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = ProductLevel.NewProductLevel();
                level2.Name = _prodHierarchyView.Model.GetNextAvailableLevelName(level1);
                level1.ChildLevel = level1;
            }


            String codeLower = "md5";
            String codeUpper = "MD5";

            //create new item with code lower
            this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, null);
            this.TestModel.GroupName = "CaseTest1";
            this.TestModel.GroupCode = codeLower;

            //execute apply and new
            Assert.IsTrue(this.TestModel.ApplyAndNewCommand.CanExecute());
            base.PropertyChangedNotifications.Clear();
            this.TestModel.ApplyAndNewCommand.Execute();
            Assert.AreNotEqual(this.TestModel.GroupCode, codeLower, "Group should have changed");
            Assert.Contains(MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(MerchHierarchyUnitEditViewModel.GroupNameProperty.Path, base.PropertyChangedNotifications);

            //set the 2nd group to same code
            this.TestModel.GroupName = "CaseTest1";
            this.TestModel.GroupCode = codeLower;
            Assert.IsFalse(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNotNullOrEmpty(((IDataErrorInfo)this.TestModel)[MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path],
                "Should show error for group code property.");

            //test upper case version of same code.
            this.TestModel.GroupCode = codeUpper;
            Assert.AreNotEqual(this.TestModel.GroupCode, codeLower);
            Assert.IsFalse(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNotNullOrEmpty(((IDataErrorInfo)this.TestModel)[MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path],
                "Should show error for group code property.");

            //set to unique code
            this.TestModel.GroupCode = "md6";
            Assert.IsTrue(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNullOrEmpty(((IDataErrorInfo)this.TestModel)[MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path]);


        }

        [Test]
        public void GEM22251_CodeUniqueCheckIncludesDeletedNodes()
        {
            //get a hierarchy group which is not the root
            ProductGroup groupToDelete = _prodHierarchyView.Model.EnumerateAllGroups().First(g => !g.IsRoot && g.ChildList.Count == 0);
            groupToDelete.Code = "DELETEGROUP";
            _prodHierarchyView.Save();

            String groupCode = groupToDelete.Code;

            //remove, save and reload
            groupToDelete = _prodHierarchyView.Model.EnumerateAllGroups().First(g => !g.IsRoot && g.ChildList.Count == 0);
            Assert.AreEqual(groupCode, groupToDelete.Code);
            groupToDelete.ParentGroup.ChildList.Remove(groupToDelete);
            _prodHierarchyView.Save();

            ProductGroupInfo deletedGroup = ProductGroupInfoList.FetchDeletedByProductHierarchyId(_prodHierarchyView.Model.Id).First(g=> g.Id == groupToDelete.Id);
            Assert.IsNotNull(deletedGroup.DateDeleted);

            this.TestModel = new MerchHierarchyUnitEditViewModel(_prodHierarchyView.Model, null);
            this.TestModel.SelectedGroup.Name = "GEM22251";
            this.TestModel.SelectedGroup.Code = groupCode;

            Assert.IsNotNullOrEmpty(((IDataErrorInfo)this.TestModel)[MerchHierarchyUnitEditViewModel.GroupCodeProperty.Path],
                "Should show error for group code property.");
            Assert.IsFalse(this.TestModel.ApplyAndNewCommand.CanExecute());


            this.TestModel.SelectedGroup.Code = "GEM22251";
            Assert.IsTrue(this.TestModel.ApplyAndNewCommand.CanExecute());
        }

        #endregion
    }
}
