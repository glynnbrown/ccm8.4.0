﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26158 : I.George
//	Initial version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance;
using Galleria.Ccm.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.UnitTests.UI;

namespace Galleria.Ccm.UnitTests.WorkflowUI.MetricMaintenance
{
    [TestFixture]
    class MetricMaintenanceViewModelTest : TestBase<MetricMaintenanceViewModel>
    {
        private MetricList InsertMetrics(IDalFactory dalFactory)
        {
            List<MetricDto> dtoList = new List<MetricDto>();

            dtoList.Add(new MetricDto()
                {
                    Id = 1,
                    Name = "Metric1",
                    DataModelType = 0,
                    Direction = 0,
                    SpecialType = 0,
                    EntityId = this.EntityId
                });

            dtoList.Add(new MetricDto()
            {
                Id = 2,
                Name = "Metric2",
                DataModelType = 0,
                Direction = 0,
                SpecialType = 0,
                EntityId = this.EntityId
            });

            // insert into the data source
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
                {
                    foreach (MetricDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return MetricList.FetchByEntityId(this.EntityId);
        }

        #region TestFixture Helpers
       
        public override void Setup()
        {
            //setup the base
            base.Setup();

            //inset test data
            InsertMetrics(this.DalFactory);

            this.TestModel = new MetricMaintenanceViewModel();
        }

     
        /// <summary>
        /// returns a valid item which exists in the database
        /// </summary>
        /// <returns></returns>
        private Metric GetValidCleanItem()
        {
            Metric saveValid = Metric.NewMetric(1);
            saveValid.Name = "Test Metric";
            Assert.IsTrue(saveValid.IsValid, " Save Valid -Item should be valid");
            Assert.IsFalse(saveValid.IsDirty, "saveValid- item should not need to be dirty before start");
            return saveValid;
        }
        /// <summary>
        /// returns  a valid dirty item which is not in the db
        /// </summary>
        /// <returns></returns>
        private Metric GetValidDirtyItem()
        {
            Metric saveValid = Metric.NewMetric(1);
            saveValid.Name = "Test Metric";
            Assert.IsTrue(saveValid.IsSavable, "saveValid- item should be dirty and valid before start");
            return saveValid;
        }
        /// <summary>
        /// Returns an invalid item
        /// </summary>
        /// <returns></returns>
        private Metric GetInvalidItem()
        {
            Metric saveInvalid = Metric.NewMetric(1);
            saveInvalid.Name = String.Empty;
            Assert.IsFalse(saveInvalid.IsValid, "save Invalid. Item should be invalid to start");
            return saveInvalid;
        }
        #endregion

        #region Properties

        [Test]
        public void Property_SelectedMetric()
        {
            String propertyName = "SelectedMetric";
            Assert.AreEqual(propertyName, MetricMaintenanceViewModel.SelectedMetricProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            Metric testSelectedMetric = this.TestModel.MasterMetricList.First();
            this.TestModel.SelectedMetric = testSelectedMetric;
            Assert.AreEqual(testSelectedMetric, this.TestModel.SelectedMetric, "SelectedMetric");
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_MasterMetricList()
        {
            String propertyName = "MasterMetricList";
            Assert.AreEqual(propertyName, MetricMaintenanceViewModel.MasterMetricListProperty.Path);

            //check type
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.MasterMetricList);

            //check value
            Assert.AreEqual(MetricList.FetchByEntityId(this.EntityId).Count, this.TestModel.MasterMetricList.Count);
        }
        #endregion

        #region Consructor

        [Test]
        public void Constructor(){}
 
        #endregion

        #region Commands

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check the command is disabled is no changes have been made
            Assert.IsFalse(cmd.CanExecute());

            //add a new metric
            this.TestModel.AddCommand.Execute();

            this.TestModel.BeginMetricEdit();

            //validate the selected metric
            this.TestModel.SelectedMetric.Name = "Test Name";
            this.TestModel.SelectedMetric.Description = "Test Description";

            //check the command is enabled now a valid change has been made
            Assert.IsTrue(cmd.CanExecute());

            //Invalidate the selected metric
            this.TestModel.SelectedMetric.Name = string.Empty;

            //check the command is disabled as atleast 1 metric should be invalid
            Assert.IsFalse(cmd.CanExecute());

            string newName = Guid.NewGuid().ToString();

            //Valid the selected metric again
            this.TestModel.SelectedMetric.Name = newName;

            //check the command is enabled again
            Assert.IsTrue(cmd.CanExecute());

            //execute the command
            cmd.Execute();

            //Select newly created metric
            this.TestModel.SelectedMetric = this.TestModel.MasterMetricList.Where(m => m.Name == newName).First();

            //It should no longer be marked as dirty or new
            Assert.IsFalse(this.TestModel.SelectedMetric.IsDirty);
            Assert.IsFalse(this.TestModel.SelectedMetric.IsNew);

            //Check 2 metrics cannot be assigned as historical daily units
            this.TestModel.MasterMetricList[0].SpecialType = MetricSpecialType.RegularSalesUnits;
            this.TestModel.SelectedMetric.SpecialType = MetricSpecialType.RegularSalesUnits;

            //check the command is disabled as 2 metrics are set as history daily units
            Assert.IsTrue(cmd.CanExecute());

            this.TestModel.MasterMetricList[0].SpecialType = MetricSpecialType.None;
            this.TestModel.SelectedMetric.SpecialType = MetricSpecialType.None;

            Assert.IsTrue(cmd.CanExecute());

            //Check 2 metrics cannot be assigned as Replenishment Days
            this.TestModel.MasterMetricList[0].SpecialType = MetricSpecialType.ReplenishmentDays;
            this.TestModel.SelectedMetric.SpecialType = MetricSpecialType.ReplenishmentDays;

            //check the command is disabled as 2 metrics are set as Replenishment Days
            Assert.IsFalse(cmd.CanExecute());

            this.TestModel.MasterMetricList[0].SpecialType = MetricSpecialType.None;
            this.TestModel.SelectedMetric.SpecialType = MetricSpecialType.None;

            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            

            //check the command is disabled is no changes have been made
            Assert.IsFalse(cmd.CanExecute());

            //add a new metric
            this.TestModel.AddCommand.Execute();

            this.TestModel.BeginMetricEdit();

            //validate the selected metric
            this.TestModel.SelectedMetric.Name = "Test Name";
            this.TestModel.SelectedMetric.Description = "Test Description";

            //check the command is enabled now a valid change has been made
            Assert.IsTrue(cmd.CanExecute());

            //Invalidate the selected metric
            this.TestModel.SelectedMetric.Name = string.Empty;

            //check the command is disabled as atleast 1 metric should be invalid
            Assert.IsFalse(cmd.CanExecute());

            string newName = Guid.NewGuid().ToString();

            //Valid the selected metric again
            this.TestModel.SelectedMetric.Name = newName;

            //check the command is enabled again
            Assert.IsTrue(cmd.CanExecute());

            //execute the command
            cmd.Execute();

            //Select newly created metric
            this.TestModel.SelectedMetric = this.TestModel.MasterMetricList.Where(m => m.Name == newName).First();

            //It should no longer be marked as dirty or new
            Assert.IsFalse(this.TestModel.SelectedMetric.IsDirty);
            Assert.IsFalse(this.TestModel.SelectedMetric.IsNew);

            //[TODO] Check the window closes.....
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check the command is disabled is no changes have been made
            Assert.IsFalse(cmd.CanExecute());

            //add a new metric
            this.TestModel.AddCommand.Execute();

            this.TestModel.BeginMetricEdit();

            //validate the selected metric
            this.TestModel.SelectedMetric.Name = "Test Name";
            this.TestModel.SelectedMetric.Description = "Test Description";

            //check the command is enabled now a valid change has been made
            Assert.IsTrue(cmd.CanExecute());

            //Invalidate the selected metric
            this.TestModel.SelectedMetric.Name = string.Empty;

            //check the command is disabled as atleast 1 metric should be invalid
            Assert.IsFalse(cmd.CanExecute());

            string newName = Guid.NewGuid().ToString();

            //Valid the selected metric again
            this.TestModel.SelectedMetric.Name = newName;

            //check the command is enabled again
            Assert.IsTrue(cmd.CanExecute());

            //execute the command
            cmd.Execute();

            //It should no longer be marked as dirty or new
            Assert.IsTrue(this.TestModel.SelectedMetric.IsDirty);
            Assert.IsTrue(this.TestModel.SelectedMetric.IsNew);
        }

        [Test]
        public void Command_Add()
        {
            RelayCommand cmd = this.TestModel.AddCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.SmallIcon, "SmallIcon should be populated");

            //Check selected metric created
            Assert.IsNull(this.TestModel.SelectedMetric);

            //Call command
            cmd.Execute();

            //Check selected metric created
            Assert.IsNotNull(this.TestModel.SelectedMetric);

            //Check the profile has been updated and is now dirty
            Assert.IsTrue(this.TestModel.SelectedMetric.IsDirty, "Item should have been added making the metric dirty");
            Assert.IsTrue(this.TestModel.SelectedMetric.IsNew, "Item should have been added making the metric new");
        }
        

       

        public void Command_RemoveMetric()
        {
            RelayCommand cmd = this.TestModel.RemoveCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.SmallIcon, "SmallIcon should be populated");

           //Take Metric Count
            Int32 metricsAddedCount = this.TestModel.MasterMetricList.Count;

            //Test can execute
            this.TestModel.SelectedMetric = null;
            Assert.IsFalse(cmd.CanExecute());

            //select a metric from the master list
            this.TestModel.SelectedMetric = this.TestModel.MasterMetricList.First();

            //should be able to execute now a selected mmetric is not null
            Assert.IsTrue(cmd.CanExecute());

            //call command
            cmd.Execute();

            //check new metric has been created
            Assert.AreEqual((metricsAddedCount -= 1), this.TestModel.MasterMetricList.Count());

            //check that a new netric has been selected
            Assert.IsNotNull(this.TestModel.SelectedMetric);
        }

        [Test]
        public void CommandEditMetric()
        {
            RelayCommand cmd = this.TestModel.EditCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNull(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsNotNull(cmd.FriendlyDescription, "FriendlyDescription should be populated");
            Assert.IsNotNull(cmd.SmallIcon, "Icon should be populated");

            //Check selected metric created
            Assert.IsNull(this.TestModel.SelectedMetric);

            //add a new metric
            this.TestModel.AddCommand.Execute();

            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should not be populated");
            
        }
        #endregion

    }
}
