﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created
#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated SaveAndCloseCommand_Execution to cater for default data that already exists
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanogramNameTemplateMaintenance
{
    [TestFixture]
    public sealed class PlanogramNameTemplateMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private void InsertPlanogramNameTemplates()
        {
            var planogramNameTemplate1 = Galleria.Ccm.Model.PlanogramNameTemplate.NewPlanogramNameTemplate(this.EntityId);
            planogramNameTemplate1.Name = "PlanogramNameTemplate 1";
            planogramNameTemplate1.BuilderFormula = Planogram.EnumerateDisplayableFieldInfos(false).First().FieldFriendlyPlaceholder;
            planogramNameTemplate1 = planogramNameTemplate1.Save();

            var planogramNameTemplate2 = Galleria.Ccm.Model.PlanogramNameTemplate.NewPlanogramNameTemplate(this.EntityId);
            planogramNameTemplate2.Name = "PlanogramNameTemplate 2";
            planogramNameTemplate2.BuilderFormula = Planogram.EnumerateDisplayableFieldInfos(false).Last().FieldFriendlyPlaceholder;
            planogramNameTemplate2 = planogramNameTemplate2.Save();
        }

        public override void Setup()
        {
            base.Setup();
        }

        #endregion

        #region Properties
        
        [Test]
        public void SelectedPlanogramNameTemplate_InitializedAsNew()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsNew);
            Assert.IsNullOrEmpty(viewModel.SelectedPlanogramNameTemplate.Name);
        }

        [Test]
        public void AvailablePlanogramNameTemplates_FetchedOnStartup()
        {
            //insert some planogram name templates.
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailablePlanogramNameTemplates.Count);
        }

        [Test]
        public void AvailablePlanogramNameTemplates_UpdatedOnPlanogramNameTemplateInsert()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Int32 count = viewModel.AvailablePlanogramNameTemplates.Count;

            //save the new PlanogramNameTemplate
            viewModel.SelectedPlanogramNameTemplate.Name = "w1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailablePlanogramNameTemplates.Count);
        }

        [Test]
        public void AvailablePlanogramNameTemplates_UpdatedOnPlanogramNameTemplateDelete()
        {
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Int32 count = viewModel.AvailablePlanogramNameTemplates.Count;

            //open and delete a PlanogramNameTemplate
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailablePlanogramNameTemplates.Count);
        }

        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            var oldModel = viewModel.SelectedPlanogramNameTemplate;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.SelectedPlanogramNameTemplate);
            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsNew);
        }

        [Test]
        public void NewCommand_DisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateCreate.ToString());

            //check
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Assert.IsFalse(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        [Test]
        public void OpenCommand_Execution()
        {
            InsertPlanogramNameTemplates();
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            foreach (var info in viewModel.AvailablePlanogramNameTemplates)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.SelectedPlanogramNameTemplate.Id);
                Assert.IsFalse(viewModel.SelectedPlanogramNameTemplate.IsDirty);
            }
        }

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Assert.IsFalse (viewModel.SelectedPlanogramNameTemplate.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test1";

            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateCreate.ToString());

            viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertPlanogramNameTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateCreate.ToString());

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);
            viewModel.SelectedPlanogramNameTemplate.Name = "NewName";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateEdit.ToString());

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertPlanogramNameTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateEdit.ToString());

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);
            viewModel.SelectedPlanogramNameTemplate.Name = "NewName";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.SelectedPlanogramNameTemplate.Name);
            Assert.IsFalse(viewModel.SelectedPlanogramNameTemplate.IsDirty);
        }

        [Test]
        public void SaveAsCommand_DisabledWhenNoCreatePerm()
        {
            InsertPlanogramNameTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateCreate.ToString());

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);
            viewModel.SelectedPlanogramNameTemplate.Name = "NewName";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "NewName";

            Assert.IsFalse(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            InsertPlanogramNameTemplates();
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Int32 preCount = PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "SaveAsName";
            viewModel.SaveAsCommand.Execute();

            Assert.AreEqual(newName, viewModel.SelectedPlanogramNameTemplate.Name);
            Assert.IsFalse(viewModel.SelectedPlanogramNameTemplate.IsDirty);

            Assert.AreEqual(preCount + 1, PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAsCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAsCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.F12, cmd.InputGestureKey);
        }

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.SelectedPlanogramNameTemplate.Name);
            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndNew_16, cmd.SmallIcon);
        }

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            viewModel.SelectedPlanogramNameTemplate.Name = "Test1";
            viewModel.SelectedPlanogramNameTemplate.BuilderFormula = "Test1";

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("Test1", PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Last().Name);
        }

        [Test]
        public void SaveAndCloseCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndCloseCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndClose_16, cmd.SmallIcon);
        }

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertPlanogramNameTemplates();
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);
            Assert.IsFalse(viewModel.SelectedPlanogramNameTemplate.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertPlanogramNameTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramNameTemplateDelete.ToString());

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertPlanogramNameTemplates();
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Int32 preCount = PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.SelectedPlanogramNameTemplate.IsNew);
            Assert.AreEqual(preCount - 1, PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertPlanogramNameTemplates();
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Int32 preCount = PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramNameTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.SelectedPlanogramNameTemplate.IsNew);
            Assert.AreEqual(preCount, PlanogramNameTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Delete_16, cmd.SmallIcon);
        }

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }


        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result=false; });
            
            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.SelectedPlanogramNameTemplate.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertPlanogramNameTemplates();

            var viewModel = new PlanogramNameTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.SelectedPlanogramNameTemplate.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion

    }
}