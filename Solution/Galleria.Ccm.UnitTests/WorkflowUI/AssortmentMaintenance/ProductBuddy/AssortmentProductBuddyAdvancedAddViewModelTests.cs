﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup;
using Galleria.Ccm.Model;
using FluentAssertions;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.WorkflowUI.AssortmentMaintenance.ProductBuddy
{
    [TestFixture]
    public class AssortmentProductBuddyAdvancedAddViewModelTests : TestBase
    {
        [Test]
        public void SelectTargetProducts_ShouldAddMultipleRowsForMultipleProductSelection()
        {
            const Int32 noOfProductsToAdd = 3;
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            TestDataHelper.InsertProductDtos(DalFactory, noOfProductsToAdd, entityId);
            var assortment = Assortment.NewAssortment(entityId);
            var products = ProductList.FetchByEntityId(entityId);
            foreach (var prod in products)
            {
                assortment.Products.Add(AssortmentProduct.NewAssortmentProduct(prod));
            }
            var viewModel = new AssortmentProductBuddyAdvancedAddViewModel(assortment);
            var row = viewModel.ProductBuddyRows.First();
            base.WindowService.ShowDialogSetReponse((Galleria.Ccm.UnitTests.Helpers.NUnitWindowService.NUnitWindowServiceArgs a) =>
                {
                    a.Result = true;
                    var prodSelectorViewModel = (ProductSelectorViewModel)a.WindowParameters.First();
                    for (int i = 0; i < noOfProductsToAdd; i++)
                    {
                        prodSelectorViewModel.AssignedProducts.Add(products[i]);
                    }
                    prodSelectorViewModel.OKCommand.Execute();
                });

            viewModel.SelectTargetProducts(row);

            viewModel.ProductBuddyRows.Should().HaveCount(
                noOfProductsToAdd + 1,
                "because calling SelectTargetProducts should add the number of products that were selected");
        } 

        [Test]
        public void SelectNoProductsButHaveTheUserClickOK_ShouldReturnWithoutDoingAnything()
        {
            const Int32 noOfProductsToAdd = 3;
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            TestDataHelper.InsertProductDtos(DalFactory, noOfProductsToAdd, entityId);
            var assortment = Assortment.NewAssortment(entityId);
            var products = ProductList.FetchByEntityId(entityId);
            foreach (var prod in products)
            {
                assortment.Products.Add(AssortmentProduct.NewAssortmentProduct(prod));
            }
            var viewModel = new AssortmentProductBuddyAdvancedAddViewModel(assortment);
            var row = viewModel.ProductBuddyRows.First();
            base.WindowService.ShowDialogSetReponse((Galleria.Ccm.UnitTests.Helpers.NUnitWindowService.NUnitWindowServiceArgs a) =>
                {
                    a.Result = true;
                    var prodSelectorViewModel = (ProductSelectorViewModel)a.WindowParameters.First();                   
                    prodSelectorViewModel.OKCommand.Execute();
                });

            TestDelegate selectTargetProducts = () => viewModel.SelectTargetProducts(row);
           
            Assert.DoesNotThrow(selectTargetProducts);
        }
    }
}
