﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27059 J.Pickup
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup;

namespace Galleria.Ccm.UnitTests.WorkflowUI.AssortmentMaintenance
{
    [TestFixture]
    public class AssortmentMaintenanceCreateViewModelTests : TestBase<AssortmentSetupViewModel>
    {
        #region Test Fixture Helpers

        [SetUp]
        public new void Setup()
        {
            //setup the base
            base.Setup();

            this.TestModel = new AssortmentSetupViewModel();
        }

        [TearDown]
        public new void TearDown()
        {
            //tear down the base
            base.TearDown();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedAssortment()
        {
            String propertyName = "SelectedAssortment";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SelectedAssortmentProperty.Path);
        }

        [Test]
        public void Property_SelectedProducts()
        {
            String propertyName = "SelectedProducts";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SelectedProductsProperty.Path);
        }

        [Test]
        public void Property_AvailableAssortments()
        {
            String propertyName = "AvailableAssortmentInfos";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.AvailableAssortmentsProperty.Path);
        }

        [Test]
        public void Property_SelectedMerchandisingGroup()
        {
            String propertyName = "SelectedMerchandisingGroup";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SelectedMerchandisingGroupProperty.Path);
        }

        [Test]
        public void Property_SelectedConsumerDecisonTree()
        {
            String propertyName = "SelectedConsumerDecisonTree";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SelectedConsumerDecisonTreeProperty.Path);
        }

        [Test]
        public void Property_IsReadOnly()
        {
            String propertyName = "IsReadOnly";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.IsReadOnlyProperty.Path);
        }

        [Test]
        public void Property_LocationBarState()
        {
            String propertyName = "LocationBarState";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.LocationBarStateProperty.Path);
        }

        [Test]
        public void Property_CurrentLocations()
        {
            String propertyName = "CurrentLocations";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.CurrentLocationsProperty.Path);
        }

        [Test]
        public void Property_SelectedCurrentLocations()
        {
            String propertyName = "SelectedCurrentLocations";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SelectedCurrentLocationsProperty.Path);
        }

        [Test]
        public void Property_FilterItemsCountMessage()
        {
            String propertyName = "FilterItemsCountMessage";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.FilterItemsCountMessageProperty.Path);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //should start with new Assortment

            if (this.TestModel.SelectedAssortment.IsValid && this.TestModel.SelectedAssortment.IsDirty)
            {
                Assert.IsTrue(this.TestModel.SaveCommand.CanExecute());
            }
            else
            {
                Assert.IsFalse(this.TestModel.SaveCommand.CanExecute());
            }
            
        }

        #endregion

        #region Commands

        #region NewCommand

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            String propertyName = "NewCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.NewCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region NewFromProductUniverseCommand

        [Test]
        public void Command_NewFromProductUniverse()
        {
            RelayCommand cmd = this.TestModel.NewFromProductUniverseCommand;
            String propertyName = "NewFromProductUniverseCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.NewFromProductUniverseCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region OpenCommand

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            String propertyName = "OpenCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.OpenCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            if (this.TestModel.OpenCommand.Parameter != null)
            {
                Assert.IsTrue(cmd.CanExecute());
            }
            else 
            {
                Assert.IsFalse(cmd.CanExecute());
            }
            
        }

        #endregion

        #region NewFromConsumerDecisionTreeCommand

        [Test]
        public void Command_NewFromConsumerDecisionTree()
        {
            RelayCommand cmd = this.TestModel.NewFromConsumerDecisionTreeCommand;
            String propertyName = "NewFromConsumerDecisionTreeCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.NewFromConsumerDecisionTreeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region SaveCommand

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.NewFromConsumerDecisionTreeCommand;
            String propertyName = "SaveCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SaveCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region SaveAsCommand

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;
            String propertyName = "SaveAsCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SaveAsCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region DeleteCommand

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;
            String propertyName = "DeleteCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.DeleteCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region PrintPreviewCommand

        [Test]
        public void Command_PrintPreview()
        {
            RelayCommand cmd = this.TestModel.PrintPreviewCommand;
            String propertyName = "PrintPreviewCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.PrintPreviewCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region SaveAndCloseCommand

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            String propertyName = "SaveAndCloseCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SaveAndCloseCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region SaveAndNewCommand

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;
            String propertyName = "SaveAndNewCommand";
            Assert.AreEqual(propertyName, AssortmentSetupViewModel.SaveAndNewCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #endregion

    }
}
