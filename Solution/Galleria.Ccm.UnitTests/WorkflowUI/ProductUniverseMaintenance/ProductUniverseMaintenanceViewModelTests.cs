﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25452 : Copied from SA
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.WorkflowUI;
using Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ProductUniverseMaintenance
{
    [TestFixture]
    public sealed class ProductUniverseMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers
        private const String _testProductPrefix = "UniverseTest_Product";

        private void InsertTestData()
        {
            //add some products
            for (int i = 1; i <= 10; i++)
            {
                Product product = Product.NewProduct(this.EntityId);
                product.Gtin = "Product" + i;
                product.Name = _testProductPrefix + i;
                product.CasePackUnits = 1;
                product.TrayPackUnits = 2;
                product = product.Save();
            }

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            Int32 rootGroupId = hierarchy.RootGroup.Id;
            TestDataHelper.PopulateProductHierarchy(hierarchy.Id);

            //create some initial product universes to use
            ProductUniverse universe1 = ProductUniverse.NewProductUniverse(this.EntityId);
            universe1.Name = "universe1";
            universe1.ProductGroupId = rootGroupId;
            universe1 = universe1.Save();

            ProductUniverse universe2 = ProductUniverse.NewProductUniverse(this.EntityId);
            universe2.Name = "universe2";
            universe2.ProductGroupId = rootGroupId;
            universe2 = universe2.Save();

            ProductUniverse universe3 = ProductUniverse.NewProductUniverse(this.EntityId);
            universe3.Name = "universe3";
            universe3.ProductGroupId = rootGroupId;
            universe3 = universe3.Save();


            //add some products
            TestDataHelper.InsertProductDtos(base.DalFactory, 10, 1);
        }

        #endregion

        #region Properties

        [Test]
        public void AvailableProductUniverses_FetchedOnStartup()
        {
            InsertTestData();
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "AvailableProductUniverses";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.AvailableProductUniversesProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(viewmodel.AvailableProductUniverses);
            AssertHelper.AssertPropertyIsReadOnly(viewmodel.GetType(), propertyName);

            //check the correct number of actual infos is available
            Int32 expectedInfoCount = ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count();
            Int32 actualInfoCount = viewmodel.AvailableProductUniverses.Count();
            Assert.AreEqual(expectedInfoCount, actualInfoCount, "All infos should be available");
        }

        [Test]
        public void AvailableProductUniverses_UpdatedOnItemInsert()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();
            Int32 count = viewModel.AvailableProductUniverses.Count;

            //save the new workflow
            viewModel.SelectedProductUniverse.Name = "w1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableProductUniverses.Count);
        }

        [Test]
        public void AvailableProductUniverses_UpdatedOnItemDelete()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Int32 count = viewModel.AvailableProductUniverses.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableProductUniverses.Count);
        }


        [Test]
        public void SelectedProductUniverse_InitializedAsNew()
        {
            InsertTestData();
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SelectedProductUniverse";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SelectedProductUniverseProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(viewmodel.GetType(), propertyName);

            Assert.IsTrue(viewmodel.SelectedProductUniverse.IsNew, "A new universe should have been initially loaded");
        }

        [Test]
        public void AvailableProducts()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "AvailableProducts";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.AvailableProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(viewmodel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(viewmodel.AvailableProducts);
        }

        [Test]
        public void SelectedAvailableProducts()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SelectedAvailableProducts";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SelectedAvailableProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(viewmodel.GetType(), propertyName);
            AssertHelper.AssertIsEditableObservable(viewmodel.SelectedAvailableProducts);
            Assert.IsNotNull(viewmodel.SelectedAvailableProducts);
        }

        [Test]
        public void SelectedTakenProducts()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SelectedTakenProducts";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SelectedTakenProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(viewmodel.GetType(), propertyName);
            AssertHelper.AssertIsEditableObservable(viewmodel.SelectedTakenProducts);
            Assert.IsNotNull(viewmodel.SelectedTakenProducts);
        }

        [Test]
        public void SearchCriteria()
        {
            InsertTestData();
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SearchCriteria";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SearchCriteriaProperty.Path);

            Assert.IsEmpty(viewmodel.SearchCriteria);

            //check setter
            base.PropertyChangedNotifications.Clear();
            viewmodel.PropertyChanged += base.TestModel_PropertyChanged;
            String value = _testProductPrefix;
            viewmodel.SearchCriteria = value;
            Assert.AreEqual(value, viewmodel.SearchCriteria);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            viewmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void IsSearching()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "IsSearching";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.IsSearchingProperty.Path);

            Assert.IsFalse(viewmodel.IsSearching);
        }

        [Test]
        public void SelectedProductGroup()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SelectedProductGroup";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SelectedProductGroupProperty.Path);

            Assert.IsNull(viewmodel.SelectedProductGroup);

            //check setter
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            base.PropertyChangedNotifications.Clear();
            viewmodel.PropertyChanged += base.TestModel_PropertyChanged;
            viewmodel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);
            Assert.AreEqual(testGroup.Id, viewmodel.SelectedProductGroup.Id);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            viewmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void SelectedProductGroupName()
        {
            var viewmodel = new ProductUniverseMaintenanceViewModel();

            String propertyName = "SelectedProductGroupName";
            Assert.AreEqual(propertyName, ProductUniverseMaintenanceViewModel.SelectedProductGroupNameProperty.Path);

            Assert.AreEqual("", viewmodel.SelectedProductGroupName);

            //check setter
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            base.PropertyChangedNotifications.Clear();
            viewmodel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);
            Assert.AreEqual(testGroup.Code + " : " + testGroup.Name, viewmodel.SelectedProductGroupName);
        }

        #endregion

        #region Commands

        #region New

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();
            var oldModel = viewModel.SelectedProductUniverse;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.SelectedProductUniverse);
            Assert.IsTrue(viewModel.SelectedProductUniverse.IsNew);
        }

        [Test]
        public void NewCommand_DisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseCreate.ToString());

            //check
            var viewModel = new ProductUniverseMaintenanceViewModel();
            Assert.IsFalse(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();
            
            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.New_32, cmd.Icon);
            Assert.AreEqual(ImageResources.New_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        #endregion

        #region Open

        [Test]
        public void OpenCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            foreach (var info in viewModel.AvailableProductUniverses)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.SelectedProductUniverse.Id);
                Assert.IsFalse(viewModel.SelectedProductUniverse.IsDirty);
            }
        }

        [Test]
        public void OpenCommand_DisabledWhenNoFetchPerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseGet.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            Assert.IsFalse(viewModel.OpenCommand.CanExecute(viewModel.AvailableProductUniverses.First().Id));
        }

        #endregion

        #region Save

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Assert.IsFalse(viewModel.SelectedProductUniverse.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);

            Assert.IsTrue(viewModel.SelectedProductUniverse.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseCreate.ToString());

            viewModel = new ProductUniverseMaintenanceViewModel();
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseCreate.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);
            viewModel.SelectedProductUniverse.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseEdit.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseEdit.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);
            viewModel.SelectedProductUniverse.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_SimpleExecution()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.SelectedProductUniverse.Name);
            Assert.IsFalse(viewModel.SelectedProductUniverse.IsDirty);
        }

        [Test]
        public void SaveCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual("SaveCommand", ProductUniverseMaintenanceViewModel.SaveCommandProperty.Path);

            //check can execute

            //valid item with category
            ProductGroupInfo testGroup = ProductGroupInfo.NewProductGroupInfo(ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup);
            viewModel.SelectedProductGroup = testGroup;
            viewModel.SelectedProductUniverse.Name = "st1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //invalid item = false
            viewModel.SelectedProductUniverse.Name = null;
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            viewModel.SelectedProductUniverse.Name = "st1";

            //no category = false;
            viewModel.SelectedProductGroup = null;
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as no category");
            viewModel.SelectedProductGroup = testGroup;

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            viewModel.PropertyChanged += base.TestModel_PropertyChanged;
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(viewModel.SelectedProductUniverse.IsNew, "The store should no longer be new");
            Assert.IsFalse(viewModel.SelectedProductUniverse.IsDirty, "The store should have been saved");
            Assert.AreEqual("st1", viewModel.SelectedProductUniverse.Name, "The store should be the same");
            Assert.AreNotEqual(0, viewModel.SelectedProductUniverse.Id, "The store should have been given a valid id");
            Assert.Contains(ProductUniverseMaintenanceViewModel.SelectedProductUniverseProperty.Path, base.PropertyChangedNotifications);
            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;

            //check an info has been added for the new item
            ProductUniverseInfo addedInfo = viewModel.AvailableProductUniverses.FirstOrDefault(l => l.Id == viewModel.SelectedProductUniverse.Id);
            Assert.IsNotNull(addedInfo, "An info for the new item should have been added");

        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        #endregion

        #region Saveas

        [Test]
        public void SaveAsCommand_DisabledWhenNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseCreate.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);
            viewModel.SelectedProductUniverse.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Int32 preCount = ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsCommand.Execute();

            Assert.AreEqual(newName, viewModel.SelectedProductUniverse.Name);
            Assert.IsFalse(viewModel.SelectedProductUniverse.IsDirty);

            Assert.AreEqual(preCount + 1, ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAsCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAsCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.F12, cmd.InputGestureKey);
        }

        #endregion

        #region SaveAndNew

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.SelectedProductUniverse.Name);
            Assert.IsTrue(viewModel.SelectedProductUniverse.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndNew_16, cmd.SmallIcon);
        }

        #endregion

        #region SaveAndClose

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            viewModel.SelectedProductUniverse.Name = "Test1";
            ProductGroup testGroup = ProductHierarchy.FetchByEntityId(this.EntityId).RootGroup;
            viewModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(testGroup);

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("Test1", ProductUniverseInfoList.FetchByEntityId(this.EntityId).First().Name);
        }

        [Test]
        public void SaveAndCloseCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndCloseCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndClose_16, cmd.SmallIcon);
        }

        #endregion

        #region Delete

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Assert.IsTrue(viewModel.SelectedProductUniverse.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);
            Assert.IsFalse(viewModel.SelectedProductUniverse.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ProductUniverseDelete.ToString());

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Int32 preCount = ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.SelectedProductUniverse.IsNew);
            Assert.AreEqual(preCount - 1, ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Int32 preCount = ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableProductUniverses.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.SelectedProductUniverse.IsNew);
            Assert.AreEqual(preCount, ProductUniverseInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Delete_16, cmd.SmallIcon);
        }

        #endregion

        #region Close

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new ProductUniverseMaintenanceViewModel();

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }

        #endregion


        [Test]
        public void AddSelectedProductsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();

            RelayCommand cmd = viewModel.AddSelectedProductsCommand;

            //perform a product search this is async so we must wait till it is complete.
            var waiter = new AsyncWaiter<ProductUniverseMaintenanceViewModelTests, ProductUniverseMaintenanceViewModel>(this, viewModel);
            waiter.StateChecker = (testModel) => !testModel.IsSearching;
            waiter.Run(() => viewModel.SearchCriteria = _testProductPrefix);

            //check canexecute
            Assert.IsFalse(cmd.CanExecute(), "No items are selected so should be disabled");

            List<ProductInfo> prodsToAdd = viewModel.AvailableProducts.Take(10).ToList();
            foreach (ProductInfo p in prodsToAdd)
            {
                viewModel.SelectedAvailableProducts.Add(p);
            }
            Assert.IsTrue(cmd.CanExecute(), "Items are selected so should be enabled");


            //check execution
            Int32 preAvailableCount = viewModel.AvailableProducts.Count();
            Int32 preSelectedCount = viewModel.SelectedAvailableProducts.Count();
            Int32 preTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            cmd.Execute();

            Int32 postAvailableCount = viewModel.AvailableProducts.Count();
            Int32 postSelectedCount = viewModel.SelectedAvailableProducts.Count();
            Int32 postTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            Assert.AreEqual(preAvailableCount - preSelectedCount, postAvailableCount, "Products should no longer be available");
            Assert.AreEqual(0, postSelectedCount, "Products should no longer be selected");
            Assert.AreEqual(preTakenCount + preSelectedCount, postTakenCount, "Selected products should now be taken");
        }

        [Test]
        public void AddAllProductsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();


            RelayCommand cmd = viewModel.AddAllProductsCommand;

            //check canexecute
            Assert.IsFalse(cmd.CanExecute(), "Should be disable as no available products");

            //perform a product search this is async so we must wait till it is complete.
            var waiter = new AsyncWaiter<ProductUniverseMaintenanceViewModelTests, ProductUniverseMaintenanceViewModel>(this, viewModel);
            waiter.StateChecker = (testModel) => !testModel.IsSearching;
            waiter.Run(() => viewModel.SearchCriteria = _testProductPrefix);

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled while products are available");

            //check execution
            Int32 preAvailableCount = viewModel.AvailableProducts.Count();
            Int32 preTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            cmd.Execute();

            Int32 postAvailableCount = viewModel.AvailableProducts.Count();
            Int32 postTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            Assert.AreEqual(0, postAvailableCount, "Products should no longer be available");
            Assert.AreEqual(preTakenCount + preAvailableCount, postTakenCount, "All products should now be taken");

            Assert.IsFalse(cmd.CanExecute(), "No products are available so command should be disabled");
        }

        [Test]
        public void RemoveSelectedProductsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();


            RelayCommand cmd = viewModel.RemoveSelectedProductsCommand;

            //check canexecute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no taken products");

            //perform a product search this is async so we must wait till it is complete.
            var waiter = new AsyncWaiter<ProductUniverseMaintenanceViewModelTests, ProductUniverseMaintenanceViewModel>(this, viewModel);
            waiter.StateChecker = (testModel) => !testModel.IsSearching;
            waiter.Run(() => viewModel.SearchCriteria = _testProductPrefix);

            //add all products for starting point
            viewModel.AddAllProductsCommand.Execute();

            //check canexecute
            Assert.IsFalse(cmd.CanExecute(), "No items are selected so should be disabled");

            List<ProductUniverseProduct> prodsToRemove = viewModel.SelectedProductUniverse.Products.Take(10).ToList();
            foreach (ProductUniverseProduct p in prodsToRemove)
            {
                viewModel.SelectedTakenProducts.Add(p);
            }
            Assert.IsTrue(cmd.CanExecute(), "Items are selected so should be enabled");


            //check execution
            Int32 preAvailableCount = viewModel.AvailableProducts.Count();
            Int32 preSelectedCount = viewModel.SelectedTakenProducts.Count();
            Int32 preTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            cmd.Execute();

            Int32 postAvailableCount = viewModel.AvailableProducts.Count();
            Int32 postSelectedCount = viewModel.SelectedTakenProducts.Count();
            Int32 postTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            Assert.AreEqual(preAvailableCount + preSelectedCount, postAvailableCount, "Products should now be available");
            Assert.AreEqual(0, postSelectedCount, "Products should no longer be selected");
            Assert.AreEqual(preTakenCount - preSelectedCount, postTakenCount, "Selected products should no longer be taken");

        }

        [Test]
        public void RemoveAllProductsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();


            RelayCommand cmd = viewModel.RemoveAllProductsCommand;

            //check canexecute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no taken products");

            //perform a product search this is async so we must wait till it is complete.
            var waiter = new AsyncWaiter<ProductUniverseMaintenanceViewModelTests, ProductUniverseMaintenanceViewModel>(this, viewModel);
            waiter.StateChecker = (testModel) => !testModel.IsSearching;
            waiter.Run(() => viewModel.SearchCriteria = _testProductPrefix);

            //add all products for starting point
            viewModel.AddAllProductsCommand.Execute();

            //check canexecute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled while products are taken");

            //check execution
            Int32 preAvailableCount = viewModel.AvailableProducts.Count();
            Int32 preTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            cmd.Execute();

            Int32 postAvailableCount = viewModel.AvailableProducts.Count();
            Int32 postTakenCount = viewModel.SelectedProductUniverse.Products.Count;

            Assert.AreEqual(0, postTakenCount, "Products should no longer be taken");
            Assert.AreEqual(preTakenCount + preAvailableCount, postAvailableCount, "All products should now be available");

            Assert.IsFalse(cmd.CanExecute(), "No products are taken so command should be disabled");
        }

        [Test]
        public void SelectProductGroupCommand_Execution()
        {
            InsertTestData();
            var viewModel = new ProductUniverseMaintenanceViewModel();


            RelayCommand cmd = viewModel.SelectProductGroupCommand;

            throw new InconclusiveException("launches a window so cant test");
        }


        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertTestData();

            var viewModel = new ProductUniverseMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertTestData();

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertTestData();

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.SelectedProductUniverse.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertTestData();

            var viewModel = new ProductUniverseMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.SelectedProductUniverse.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion

    }
}
