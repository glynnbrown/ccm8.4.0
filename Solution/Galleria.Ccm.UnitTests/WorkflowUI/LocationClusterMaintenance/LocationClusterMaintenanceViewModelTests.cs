﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using System.ComponentModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationClusterMaintenance
{
    [TestFixture]
    [Serializable]
    public class LocationClusterMaintenanceViewModelTests : TestBase<LocationClusterMaintenanceViewModel>
    {
        #region TestFixture Helpers


        public override void  Setup()
        {
            base.Setup();

            //load the test model
            List<LocationDto> locations = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);
            TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locations);

            this.TestModel = new LocationClusterMaintenanceViewModel();
        }


        /// <summary>
        /// Returns a valid clean item that already exists in the db
        /// </summary>
        /// <returns></returns>
        private ClusterScheme GetValidCleanItem()
        {
            ClusterScheme saveValid = ClusterScheme.FetchById(this.TestModel.MasterClusterSchemeInfoList.First().Id);
            Assert.IsTrue(saveValid.IsValid, "saveValid- item should be valid");
            Assert.IsFalse(saveValid.IsDirty, "saveValid- item should not need to be dirty before start");
            return saveValid;
        }

        /// <summary>
        /// Returns a valid dirty item which does not exist in the db
        /// </summary>
        /// <returns></returns>
        private ClusterScheme GetValidDirtyItem()
        {
            ClusterScheme saveValid = ClusterScheme.NewClusterScheme(1);
            saveValid.Name = "TestScheme";

            Cluster cl = Cluster.NewCluster();
            cl.Name = "cl";
            saveValid.Clusters.Add(cl);

            Assert.IsTrue(saveValid.IsSavable, "saveValid- item should be valid  & dirty before start");
            return saveValid;
        }

        /// <summary>
        /// Returns an invalid item
        /// </summary>
        /// <returns></returns>
        private ClusterScheme GetInvalidItem()
        {
            ClusterScheme saveInvalid = ClusterScheme.NewClusterScheme(1);
            saveInvalid.Name = String.Empty;
            Assert.IsFalse(saveInvalid.IsValid, "saveInvalid - item should be invalid to start");
            return saveInvalid;
        }

        private void SelectAllLocations()
        {
            this.TestModel.AddClusterCommand.Execute();
            this.TestModel.CurrentScheme.Clusters[1].Name = "cluster2";
            foreach (LocationInfo info in this.TestModel.UnassignedLocations.ToList())
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            this.TestModel.AddLocationsCommand.Execute();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_MasterLocationInfoList()
        {
            String propertyName = "MasterLocationInfoList";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.MasterLocationInfoListProperty.Path);

            //check type
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.MasterLocationInfoList);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check initial value
            Assert.IsNotEmpty(this.TestModel.MasterLocationInfoList, "Should contain values");
        }

        [Test]
        public void Property_MasterPeerGroupInfoList()
        {
            String propertyName = "MasterClusterSchemeInfoList";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.MasterClusterSchemeInfoListProperty.Path);

            //check type
            AssertHelper.AssertIsReadOnlyCollection(this.TestModel.MasterClusterSchemeInfoList);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check initial value
            Assert.IsNotEmpty(this.TestModel.MasterClusterSchemeInfoList, "Should contain values");
        }

        [Test]
        public void Property_SelectedAvailableLocations()
        {
            String propertyName = "SelectedAvailableLocations";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.SelectedAvailableLocationsProperty.Path);

            //check type
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedAvailableLocations);

            Assert.IsEmpty(this.TestModel.SelectedAvailableLocations, "The selection should initially be empty");
        }

        [Test]
        public void Property_SelectedTakenLocations()
        {
            String propertyName = "SelectedTakenLocations";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.SelectedTakenLocationsProperty.Path);

            //check type
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedTakenLocations);

            Assert.IsEmpty(this.TestModel.SelectedTakenLocations, "The selection should initially be empty");
        }

        [Test]
        public void Property_CurrentScheme()
        {
            String propertyName = "CurrentScheme";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.CurrentSchemeProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.CurrentScheme);
            Assert.IsTrue(this.TestModel.CurrentScheme.IsNew, "A new scheme should be initially loaded");

            //check setter
            ClusterScheme testCurrentScheme = ClusterScheme.FetchById(this.TestModel.MasterClusterSchemeInfoList.First().Id);
            this.TestModel.PropertyChanged += PropertySetters_CurrentScheme;
            this.TestModel.CurrentScheme = testCurrentScheme;
            Assert.AreEqual(testCurrentScheme, this.TestModel.CurrentScheme, "CurrentScheme");
            Assert.IsNotNull(this.TestModel.SelectedCluster, "A cluster should be selected");
            Assert.Contains(this.TestModel.SelectedCluster, testCurrentScheme.Clusters, "Selected cluster should belong to current scheme");
        }

        private void PropertySetters_CurrentScheme(object sender, PropertyChangedEventArgs e)
        {
            this.TestModel.PropertyChanged -= PropertySetters_CurrentScheme;
            Assert.AreEqual(LocationClusterMaintenanceViewModel.CurrentSchemeProperty.Path, e.PropertyName);
        }

        [Test]
        public void Property_ClusterSchemeClustersSource()
        {
            String propertyName = "ClusterSchemeClustersSource";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.ClusterSchemeClustersSourceProperty.Path);

            Assert.IsTrue(this.TestModel.ClusterSchemeClustersSource is ICollectionView);
        }

        [Test]
        public void Property_SelectedCluster()
        {
            String propertyName = "SelectedCluster";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.SelectedClusterProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.SelectedCluster);
            Assert.IsTrue(this.TestModel.SelectedCluster.IsNew, "A new cluster should be initially loaded");

            //check setter
            Cluster testSelectedCluster = this.TestModel.CurrentScheme.Clusters.First();
            this.TestModel.PropertyChanged += PropertySetters_SelectedCluster;
            this.TestModel.SelectedCluster = testSelectedCluster;
            Assert.AreEqual(testSelectedCluster, this.TestModel.SelectedCluster, "SelectedCluster");
        }

        private void PropertySetters_SelectedCluster(object sender, PropertyChangedEventArgs e)
        {
            this.TestModel.PropertyChanged -= PropertySetters_SelectedCluster;
            Assert.AreEqual(LocationClusterMaintenanceViewModel.SelectedClusterProperty.Path, e.PropertyName);
        }

        [Test]
        public void Property_AssignedLocations()
        {
            String propertyName = "AssignedLocations";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.AssignedLocationsProperty.Path);

            //check type
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AssignedLocations);

            //check value
            Assert.IsEmpty(this.TestModel.AssignedLocations, "The selection should initially be empty");
        }

        [Test]
        public void Property_UnassignedLocations()
        {
            String propertyName = "UnassignedLocations";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.UnassignedLocationsProperty.Path);

            //check type
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.UnassignedLocations);

            //check value
            Assert.IsNotEmpty(this.TestModel.UnassignedLocations, "The selection should initially be full");
            Assert.AreEqual(LocationInfoList.FetchByEntityId(this.EntityId).Count, this.TestModel.UnassignedLocations.Count,
                "All stores should be initially unassigned");
        }

        [Test]
        public void Property_ClustersFilterText()
        {
            String propertyName = "ClustersFilterText";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.ClustersFilterTextProperty.Path);

            //check setter
            String testClustersFilterText = "xxx";
            this.TestModel.PropertyChanged += PropertySetters_ClustersFilterText;
            this.TestModel.ClustersFilterText = testClustersFilterText;
            Assert.AreEqual(testClustersFilterText, this.TestModel.ClustersFilterText, "ClustersFilterText");
        }
        private void PropertySetters_ClustersFilterText(object sender, PropertyChangedEventArgs e)
        {
            this.TestModel.PropertyChanged -= PropertySetters_ClustersFilterText;
            Assert.AreEqual(LocationClusterMaintenanceViewModel.ClustersFilterTextProperty.Path, e.PropertyName);
        }

        [Test]
        public void Property_IsUnassignedLocationGridVisible()
        {
            String propertyName = "IsUnassignedLocationGridVisible";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.IsUnassignedLocationGridVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check value
            Assert.IsTrue(this.TestModel.IsUnassignedLocationGridVisible, "Should be initially visibly as stores are unassigned");
            this.TestModel.PropertyChanged += new PropertyChangedEventHandler(IsUnassignedLocationGridVisible_PropertyChanged);

            //assign all locations - property should go to false
            foreach (LocationInfo st in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(st);
            }
            this.TestModel.AddLocationsCommand.Execute();

            Assert.IsFalse(this.TestModel.IsUnassignedLocationGridVisible, "Property should now return false");
            Assert.IsTrue(_isUnassignedLocationGridVisiblePropertyChangedCalled, "Property change notification should have been made");

        }
        private bool _isUnassignedLocationGridVisiblePropertyChangedCalled;
        private void IsUnassignedLocationGridVisible_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationClusterMaintenanceViewModel.IsUnassignedLocationGridVisibleProperty.Path)
            {
                _isUnassignedLocationGridVisiblePropertyChangedCalled = true;
            }
        }

        [Test]
        public void Property_AssignedCategory()
        {
            String propertyName = "AssignedCategory";
            Assert.AreEqual(propertyName, LocationClusterMaintenanceViewModel.AssignedCategoryProperty.Path);

            Assert.IsNull(this.TestModel.AssignedCategory);

            ProductGroup productGroupToAssign = ProductHierarchy.FetchByEntityId(this.EntityId).FetchAllGroups().First();
            ProductGroupInfo info = ProductGroupInfo.NewProductGroupInfo(productGroupToAssign);
            this.TestModel.AssignedCategory = info;
            Assert.AreEqual(info, this.TestModel.AssignedCategory);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            Assert.IsNotNull(this.TestModel.MasterLocationInfoList, "MasterLocationInfoListView should be populated");
            Assert.IsNotNull(this.TestModel.MasterClusterSchemeInfoList, "MasterClusterSchemeInfoListView should be populated");

            //check a new scheme has been loaded
            Assert.IsTrue(this.TestModel.CurrentScheme.IsNew, "A new scheme should have been loaded");
        }


        #endregion

        #region Commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");

            //check execution
            ClusterScheme beforeItem = this.TestModel.CurrentScheme;
            beforeItem.Name = "beforeNew";

            cmd.Execute();

            Assert.IsTrue(this.TestModel.CurrentScheme.IsNew, "Item should be a new one");
            Assert.AreNotEqual(this.TestModel.CurrentScheme.Name, beforeItem.Name, "Item should have chanegd");
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check command can execute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is null");

            this.TestModel.CurrentScheme = GetInvalidItem();
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is invalid");

            this.TestModel.CurrentScheme = GetValidDirtyItem();
            SelectAllLocations();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when selected item is populated & valid & unassigned store collection is empty");

            //check the command execution
            ClusterScheme executeItem = GetValidDirtyItem();
            executeItem.Name = "xxx";
            this.TestModel.CurrentScheme = executeItem;
            SelectAllLocations();
            cmd.Execute();
            Assert.IsFalse(this.TestModel.CurrentScheme.IsDirty, "Post execution -The item should no longer be dirty");
            Assert.IsTrue(this.TestModel.CurrentScheme.Name == executeItem.Name, "Post execution -selected item should not have changed");
        }


        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.NotNull(cmd.SmallIcon, "Icon should be populated");

            //check command can execute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is null");

            this.TestModel.CurrentScheme = GetInvalidItem();
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is invalid");

            this.TestModel.CurrentScheme = GetValidCleanItem();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when selected item is populated & valid");

            //Get the current category and alter it
            ClusterScheme beforeItem = this.TestModel.CurrentScheme;
            beforeItem.Name = "TestCopy";

            //execute
            cmd.Execute();

            //check the selected category has changed
            Assert.AreNotEqual(this.TestModel.CurrentScheme, beforeItem, "The selected item should have changed");
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            base.DeleteCommandCheckExpectedValues(cmd);

            //load the first item
            ClusterScheme removeItem = GetValidCleanItem();
            this.TestModel.CurrentScheme = removeItem;
            Int32 removeItemId = removeItem.Id;

            //check the command is enabled
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");

            //execute
            cmd.Execute();

            //check the item has changed to a new one
            Assert.AreNotEqual(this.TestModel.CurrentScheme, removeItem, "The item should have changed");
            Assert.IsTrue(this.TestModel.CurrentScheme.IsNew, "A new item should have been loaded");

            //check the item has been deleted from the viewModel list
            ClusterSchemeInfo foundInfo = this.TestModel.MasterClusterSchemeInfoList.FirstOrDefault(p => p.Id == removeItemId);
            Assert.IsNull(foundInfo, "Item should have been removed from the master list");

        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            base.OpenCommandCheckExpectedValues(cmd);

            //get a peergroup to load
            ClusterSchemeInfo loadItem = this.TestModel.MasterClusterSchemeInfoList.First();

            //check the command is enabled
            Assert.IsTrue(cmd.CanExecute(loadItem.Id), "Command should be enabled");

            //execute
            cmd.Execute(loadItem.Id);

            //check the loaded category is the requested one
            Assert.AreEqual(this.TestModel.CurrentScheme.Id, loadItem.Id, "The requested item should have been loaded");
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            base.CloseCommandCheckExpectedValues(cmd);

            //No way to test if window has closed.
        }

        [Test]
        public void Command_AddCluster()
        {
            RelayCommand cmd = this.TestModel.AddClusterCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.CurrentScheme = GetValidCleanItem();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");

            //check execution
            ClusterScheme executionItem = GetValidCleanItem();
            int preSublevelCount = executionItem.Clusters.Count;
            this.TestModel.CurrentScheme = executionItem;

            cmd.Execute();

            Assert.AreEqual(preSublevelCount + 1, executionItem.Clusters.Count, "A cluster should have been added");
            Assert.AreEqual(executionItem, this.TestModel.CurrentScheme, "The scheme should still be selected");
        }

        [Test]
        public void Command_DeleteCluster()
        {
            RelayCommand cmd = this.TestModel.DeleteClusterCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check can execute states
            this.TestModel.SelectedCluster = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.SelectedCluster = GetValidCleanItem().Clusters.First();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");

            //check execution
            ClusterScheme ownerPeerGroup = GetValidCleanItem();
            this.TestModel.CurrentScheme = ownerPeerGroup;
            Cluster removeCluster = ownerPeerGroup.Clusters.First();
            this.TestModel.SelectedCluster = removeCluster;
            List<ClusterLocation> removedClusterLocations = removeCluster.Locations.ToList();
            Assert.Greater(ownerPeerGroup.Clusters.Count, 1, "Start with a scheme with more than one cluster");

            cmd.Execute();

            Assert.IsFalse(ownerPeerGroup.Clusters.Contains(removeCluster), "Sublevel should have been removed");
            Assert.AreEqual(ownerPeerGroup, this.TestModel.CurrentScheme, "The scheme should still be selected");
            Assert.IsNotNull(this.TestModel.SelectedCluster, "Another cluster should have been selected");


            foreach (ClusterLocation pgs in removedClusterLocations)
            {
                LocationInfo unassignedInfo = this.TestModel.UnassignedLocations.FirstOrDefault(s => s.Id == pgs.Id);
                Assert.IsNotNull(unassignedInfo, "Removed cluster locations should be back in unassigned list");
            }
        }

        [Test]
        public void Command_RemoveAllClusters()
        {
            RelayCommand cmd = this.TestModel.RemoveAllClustersCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check canexecute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.CurrentScheme = GetValidCleanItem();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");

            //check execution
            ClusterScheme executionItem = GetValidCleanItem();
            Assert.IsNotEmpty(executionItem.Clusters);
            this.TestModel.CurrentScheme = executionItem;

            cmd.Execute();

            Assert.IsEmpty(executionItem.Clusters, "All clusters should have been removed");
            Assert.IsNotEmpty(this.TestModel.UnassignedLocations, "Unassigned locations should have been reallocated");
        }

        [Test]
        public void Command_AutoCreateSublevels()
        {
            RelayCommand cmd = this.TestModel.AutoCreateClustersCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check canexecute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(Location.CodeProperty.Name), "Command should be disabled when item is null");
            this.TestModel.CurrentScheme = GetValidCleanItem();
            Assert.IsTrue(cmd.CanExecute(Location.CodeProperty.Name), "Command should be enabled when item is populated");

            //check execution
            ClusterScheme executionItem = GetValidCleanItem();
            this.TestModel.CurrentScheme = executionItem;
            int expectedSublevelCount = this.TestModel.MasterLocationInfoList.Count;

            //execute with code property so that a sublevel is created per store
            cmd.Execute(Location.CodeProperty.Name);

            Assert.AreEqual(expectedSublevelCount, executionItem.Clusters.Count, "Clusters should have been autocreated");
            foreach (Cluster sublevel in executionItem.Clusters)
            {
                Assert.AreEqual(1, sublevel.Locations.Count, "Cluster should have been allocated one location");
            }
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check command can execute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is null");

            this.TestModel.CurrentScheme = GetInvalidItem();
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is invalid");

            this.TestModel.CurrentScheme = GetValidDirtyItem();
            SelectAllLocations();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when selected item is populated & valid");

            //[TODO] Need to check the window closes
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

            //check command can execute states
            this.TestModel.CurrentScheme = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is null and unassigned stores is populated");

            this.TestModel.CurrentScheme = GetInvalidItem();
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when selected item is invalid");

            this.TestModel.CurrentScheme = GetValidDirtyItem();
            SelectAllLocations();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when selected item is populated & valid & unassigned stores is empty");

            //execute
            ClusterScheme executeItem = GetValidDirtyItem();
            executeItem.Name = "xxx";
            this.TestModel.CurrentScheme = executeItem;
            SelectAllLocations();
            cmd.Execute();

            //check the initial item was saved
            ClusterSchemeInfo savedItem = ClusterSchemeInfoList.FetchByEntityId(1).FirstOrDefault(s => s.Name == executeItem.Name);
            Assert.IsNotNull(savedItem, "Item should have been saved to the database");

            //check the item has changed to a new one
            Assert.AreNotEqual(this.TestModel.CurrentScheme.Name, executeItem.Name, "The item should have changed");
            Assert.IsTrue(this.TestModel.CurrentScheme.IsNew, "A new item should have been loaded");
        }

        [Test]
        public void Command_AddLocations()
        {
            RelayCommand cmd = this.TestModel.AddLocationsCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check can execute states
            this.TestModel.SelectedCluster = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.SelectedCluster = GetValidCleanItem().Clusters.First();
            //Add locations 
            foreach (LocationInfo info in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");
            this.TestModel.SelectedAvailableLocations.Clear();

            //prep for execution
            ClusterScheme executionItem = ClusterScheme.NewClusterScheme(1);
            this.TestModel.CurrentScheme = executionItem;
            this.TestModel.AddClusterCommand.Execute();
            Cluster cluster = executionItem.Clusters.First();
            this.TestModel.SelectedCluster = cluster;

            Assert.IsEmpty(cluster.Locations);

            //check execution
            int addStoreCount = this.TestModel.UnassignedLocations.Count;
            this.TestModel.SelectedAvailableLocations.Clear();
            foreach (LocationInfo info in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            cmd.Execute();
            Assert.AreEqual(addStoreCount, cluster.Locations.Count, "Locations should have been assigned to the selected cluster");
        }

        [Test]
        public void Command_RemoveLocations()
        {
            RelayCommand cmd = this.TestModel.RemoveLocationsCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check can execute states
            this.TestModel.SelectedCluster = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.SelectedCluster = GetValidCleanItem().Clusters.First();

            //Insert 1 store into taken store for can excute check
            this.TestModel.SelectedAvailableLocations.Add(this.TestModel.UnassignedLocations.First());
            this.TestModel.AddLocationsCommand.Execute();
            this.TestModel.SelectedTakenLocations.Add(this.TestModel.AssignedLocations.First());
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");

            //prep for execution
            ClusterScheme executionItem = ClusterScheme.NewClusterScheme(1);
            this.TestModel.CurrentScheme = executionItem;
            this.TestModel.AddClusterCommand.Execute();
            Cluster cluster = executionItem.Clusters.First();
            this.TestModel.SelectedCluster = cluster;

            this.TestModel.SelectedAvailableLocations.Clear();
            foreach (LocationInfo info in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            this.TestModel.AddLocationsCommand.Execute();
            Assert.IsNotEmpty(cluster.Locations, "prep requires locations have been added");

            //check execution

            this.TestModel.SelectedTakenLocations.Clear();
            foreach (LocationInfo info in this.TestModel.AssignedLocations)
            {
                this.TestModel.SelectedTakenLocations.Add(info);
            }
            cmd.Execute();
            Assert.IsEmpty(cluster.Locations, "Locations should have been removed");
        }

        [Test]
        public void Command_RemoveAllLocations()
        {
            RelayCommand cmd = this.TestModel.RemoveAllLocationsCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check can execute states
            this.TestModel.SelectedCluster = null;
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when item is null");
            this.TestModel.SelectedCluster = GetValidCleanItem().Clusters.First();
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when item is populated");

            //prep for execution
            ClusterScheme executionItem = ClusterScheme.NewClusterScheme(1);
            this.TestModel.CurrentScheme = executionItem;
            this.TestModel.AddClusterCommand.Execute();
            Cluster cluster = executionItem.Clusters.First();
            this.TestModel.SelectedCluster = cluster;

            this.TestModel.SelectedAvailableLocations.Clear();
            foreach (LocationInfo info in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            this.TestModel.AddLocationsCommand.Execute();
            Assert.IsNotEmpty(cluster.Locations, "prep requires locations have been added");

            //check execution
            cmd.Execute();
            Assert.IsEmpty(cluster.Locations, "Locations should have been removed");
        }

        [Test]
        public void Command_NewImportFromBehaviouralClustering()
        {
            RelayCommand cmd = this.TestModel.NewImportFromBehaviouralClusteringCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.NotNull(cmd.Icon, "Icon should be populated");

        }

        [Test]
        public void Command_SelectAssignedCategory()
        {
           String commandName = "SelectAssignedCategoryCommand";
            RelayCommand cmd = this.TestModel.SelectAssignedCategoryCommand;

            Assert.AreEqual(commandName, LocationClusterMaintenanceViewModel.SelectAssignedCategoryCommandProperty.Path);
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
            Assert.NotNull(cmd.FriendlyName, "Should have a friendly name");
        }
        #endregion

        #region Event Handlers

        [Test]
        public void OnEvent_ClusterLocationsCollectionChanged()
        {
            //setup
            this.TestModel.CurrentScheme = ClusterScheme.NewClusterScheme(1);
            this.TestModel.AddClusterCommand.Execute();
            this.TestModel.SelectedCluster = this.TestModel.CurrentScheme.Clusters.Last();
            Assert.IsEmpty(this.TestModel.SelectedCluster.Locations);

            foreach (LocationInfo info in this.TestModel.MasterLocationInfoList)
            {
                Assert.Contains(info, this.TestModel.UnassignedLocations);
                Assert.IsFalse(this.TestModel.AssignedLocations.Contains(info));
            }

            //add locations - should not be in unassigned, should be in assigned
            foreach (LocationInfo info in this.TestModel.MasterLocationInfoList)
            {
                this.TestModel.SelectedCluster.Locations.Add(info);
            }

            foreach (LocationInfo info in this.TestModel.MasterLocationInfoList)
            {
                Assert.Contains(info, this.TestModel.AssignedLocations);
                Assert.IsFalse(this.TestModel.UnassignedLocations.Contains(info));
            }

            //Check if grid is visble now there are no unassigned locations
            Assert.IsFalse(this.TestModel.IsUnassignedLocationGridVisible);


            //remove stores - should be in unassigned and not in assigend
            foreach (LocationInfo info in this.TestModel.MasterLocationInfoList)
            {
                ClusterLocation cl = this.TestModel.SelectedCluster.Locations.First(s => s.LocationId == info.Id);
                this.TestModel.SelectedCluster.Locations.Remove(cl);
            }

            foreach (LocationInfo info in this.TestModel.MasterLocationInfoList)
            {
                Assert.Contains(info, this.TestModel.UnassignedLocations);
                Assert.IsFalse(this.TestModel.AssignedLocations.Contains(info));
            }

            //Check if grid is visble now there are unassigned locations
            Assert.IsTrue(this.TestModel.IsUnassignedLocationGridVisible);
        }

        [Test]
        public void OnEvent_CurrentSchemeChanged()
        {
            this.TestModel.CurrentScheme = GetValidCleanItem();
            Assert.IsNotEmpty(this.TestModel.CurrentScheme.Clusters);

            Assert.IsNotNull(this.TestModel.SelectedCluster, "A cluster should be selected");

            foreach (Cluster cluster in this.TestModel.CurrentScheme.Clusters)
            {
                Assert.IsTrue(this.TestModel.ClusterSchemeClustersSource.Contains(cluster), "Clusters should have been loaded into the source");
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks that 2 clusters can be merged together
        /// </summary>
        [Test]
        public void Method_MergeClusters()
        {
            //setup
            ClusterScheme parentScheme = GetValidCleanItem();
            this.TestModel.CurrentScheme = parentScheme;

            Cluster mergeCluster = parentScheme.Clusters.First();
            Assert.IsNotEmpty(mergeCluster.Locations);
            int locationCount = mergeCluster.Locations.Count;

            this.TestModel.AddClusterCommand.Execute();
            Cluster mergeIntoCluster = parentScheme.Clusters.Last();
            Assert.IsEmpty(mergeIntoCluster.Locations);

            //run the method
            this.TestModel.MergeClusters(mergeCluster, mergeIntoCluster);

            //check
            Assert.IsFalse(parentScheme.Clusters.Contains(mergeCluster), "MergeCluster should have been removed");
            Assert.AreEqual(locationCount, mergeIntoCluster.Locations.Count, "Locations should have been moved accross");
            Assert.AreEqual(mergeIntoCluster, this.TestModel.SelectedCluster, "Merge into cluster should be selected");
        }

        /// <summary>
        /// Checks that locations can be moved from one cluster to another
        /// </summary>
        [Test]
        public void Method_MoveLocations()
        {
            //setup
            ClusterScheme parentScheme = GetValidCleanItem();
            this.TestModel.CurrentScheme = parentScheme;

            Cluster mergeCluster = parentScheme.Clusters.First();
            Assert.IsNotEmpty(mergeCluster.Locations);
            int storeCount = mergeCluster.Locations.Count;

            this.TestModel.AddClusterCommand.Execute();
            Cluster mergeIntoCluster = parentScheme.Clusters.Last();
            Assert.IsEmpty(mergeIntoCluster.Locations);

            List<LocationInfo> moveLocations = new List<LocationInfo>();
            foreach (ClusterLocation cl in mergeCluster.Locations)
            {
                moveLocations.Add(this.TestModel.MasterLocationInfoList.First(s => s.Id == cl.LocationId));
            }


            //run the method
            this.TestModel.MoveLocations(moveLocations, mergeCluster, mergeIntoCluster);

            //check 
            Assert.AreEqual(storeCount, mergeIntoCluster.Locations.Count, "Locations should have been moved accross");
            Assert.IsTrue(parentScheme.Clusters.Contains(mergeCluster), "MergeCluster should not have been removed");
            Assert.AreEqual(mergeCluster, this.TestModel.SelectedCluster, "MergeCluster should still be selected");
        }

        /// <summary>
        /// Checks that a scheme can be set as default and only
        /// one scheme is default at a time
        /// </summary>
        [Test]
        public void Method_SetAsDefaultScheme()
        {
            //set a default scheme
            ClusterSchemeInfo firstTest = this.TestModel.MasterClusterSchemeInfoList.First();
            this.TestModel.SetAsDefaultScheme(firstTest);
            Assert.IsTrue(ClusterScheme.FetchById(firstTest.Id).IsDefault, "Should now be default");

            //Create new cluster scheme
            String newClusterName = Guid.NewGuid().ToString();
            this.TestModel.NewCommand.Execute();
            //Set properties so validate the object
            this.TestModel.CurrentScheme.Name = newClusterName;
            this.TestModel.SelectedCluster.Name = Guid.NewGuid().ToString();
            this.TestModel.SelectedCluster = this.TestModel.CurrentScheme.Clusters.First();
            foreach (LocationInfo info in this.TestModel.UnassignedLocations)
            {
                this.TestModel.SelectedAvailableLocations.Add(info);
            }
            this.TestModel.AddLocationsCommand.Execute();
            //Save
            this.TestModel.SaveCommand.Execute();

            //select new default scheme
            ClusterSchemeInfo secondTest = this.TestModel.MasterClusterSchemeInfoList.Where(p => p.Name == newClusterName).FirstOrDefault();
            Assert.IsNotNull(secondTest);

            this.TestModel.SetAsDefaultScheme(secondTest);

            Assert.IsFalse(ClusterScheme.FetchById(firstTest.Id).IsDefault, "First should no longer be default");
            Assert.IsTrue(ClusterScheme.FetchById(secondTest.Id).IsDefault, "Second should now be default");
        }

        #endregion

        #region Others

        //[Test]
        //public void CheckPermissions()
        //{
        //    DomainPrincipal domainPrincipal = ApplicationContext.User as DomainPrincipal;
        //    DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;

        //    //fetch
        //    domainIdentity.Roles.Remove(DomainPermission.RoleUser.ToString());
        //    this.TestModel = new LocationClusterMaintenanceViewModel();

        //    Assert.IsFalse(this.TestModel.OpenCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoFetchPermission, this.TestModel.OpenCommand.DisabledReason);

        //    domainIdentity.Roles.Add(DomainPermission.RoleUser.ToString());

        //    //create
        //    domainIdentity.Roles.Remove(DomainPermission.RoleEditor.ToString());
        //    this.TestModel = new LocationClusterMaintenanceViewModel();

        //    Assert.IsFalse(this.TestModel.NewCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoCreatePermission, this.TestModel.NewCommand.DisabledReason);

        //    Assert.IsFalse(this.TestModel.SaveAsCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoCreatePermission, this.TestModel.SaveAsCommand.DisabledReason);

        //    domainIdentity.Roles.Add(DomainPermission.RoleEditor.ToString());

        //    //edit
        //    domainIdentity.Roles.Remove(DomainPermission.RoleEditor.ToString());
        //    this.TestModel = new LocationClusterMaintenanceViewModel();

        //    Assert.IsFalse(this.TestModel.SaveCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoEditPermission, this.TestModel.SaveCommand.DisabledReason);

        //    Assert.IsFalse(this.TestModel.SaveAndNewCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoEditPermission, this.TestModel.SaveAndNewCommand.DisabledReason);

        //    Assert.IsFalse(this.TestModel.SaveAndCloseCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoEditPermission, this.TestModel.SaveAndCloseCommand.DisabledReason);

        //    domainIdentity.Roles.Add(DomainPermission.RoleEditor.ToString());

        //    //delete
        //    domainIdentity.Roles.Remove(DomainPermission.RoleEditor.ToString());
        //    this.TestModel = new LocationClusterMaintenanceViewModel();

        //    Assert.IsFalse(this.TestModel.DeleteCommand.CanExecute());
        //    Assert.AreEqual(Message.Generic_NoDeletePermission, this.TestModel.DeleteCommand.DisabledReason);

        //    domainIdentity.Roles.Add(DomainPermission.RoleEditor.ToString());
        //}

        #endregion
    }
}
