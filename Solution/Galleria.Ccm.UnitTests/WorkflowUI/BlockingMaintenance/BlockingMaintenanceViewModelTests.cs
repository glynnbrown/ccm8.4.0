﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26891 : L.Ineson
//	Created
// V8-28444 : M.Shelley
//  Fix unit tests broken by the addition of a required ProductGroupId
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Moq;
using System.Windows.Media.Animation;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Windows.Media;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;

namespace Galleria.Ccm.UnitTests.WorkflowUI.BlockingMaintenance
{
    [TestFixture]
    public class BlockingMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private List<Blocking> InsertTestData()
        {
            List<Blocking> itemList = new List<Blocking>();

            for (Int32 i = 0; i < 5; i++)
            {
                Blocking item = Blocking.NewBlocking(this.EntityId);
                item.Name = Guid.NewGuid().ToString();
                item.ProductGroupId = 1;

                item.Height = 200;
                item.Width = 400;

                //add groups
                BlockingGroup group1 = item.Groups.AddNew();
                group1.Colour = CommonHelper.ColorToInt(Colors.Orange);

                BlockingGroup group2 = item.Groups.AddNew();
                group2.Colour = CommonHelper.ColorToInt(Colors.Green);

                BlockingGroup group3 = item.Groups.AddNew();
                group3.Colour = CommonHelper.ColorToInt(Colors.Red);

                BlockingGroup group4 = item.Groups.AddNew();
                group4.Colour = CommonHelper.ColorToInt(Colors.Purple);

                //add dividers
                BlockingDivider divider1 = BlockingDivider.NewBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.45F, 0, 1);
                item.Dividers.Add(divider1);
                BlockingDivider divider2 = BlockingDivider.NewBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.80F, 0, 1);
                item.Dividers.Add(divider2);
                BlockingDivider divider3 = BlockingDivider.NewBlockingDivider((Byte)2, PlanogramBlockingDividerType.Horizontal, 0.80F, 0.50F, 0.2F);
                item.Dividers.Add(divider3);

                //add locations
                BlockingLocation loc1 = item.Locations.Add(group1);
                loc1.BlockingDividerRightId = divider1.Id;

                BlockingLocation loc2 = item.Locations.Add(group2);
                loc2.BlockingDividerLeftId = divider1.Id;
                loc2.BlockingDividerRightId = divider2.Id;

                BlockingLocation loc3 = item.Locations.Add(group3);
                loc3.BlockingDividerLeftId = divider2.Id;
                loc3.BlockingDividerBottomId = divider3.Id;

                BlockingLocation loc4 = item.Locations.Add(group4);
                loc4.BlockingDividerLeftId = divider2.Id;
                loc4.BlockingDividerTopId = divider3.Id;

                itemList.Add(item.Save());
            }

            return itemList;
        }

        public override void Setup()
        {
            base.Setup();

            InsertTestData();
        }

        #endregion

        #region Properties

        [Test]
        public void AvailableItems_FetchedOnStartup()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableItems.Count);
        }

        [Test]
        public void AvailableItems_UpdatedOnItemInsert()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //save the new workflow
            viewModel.CurrentItem.Name = "w1";
            viewModel.CurrentItem.ProductGroupId = 1;

            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableItems.Count);
        }

        [Test]
        public void AvailableItems_UpdatedOnItemDelete()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableItems.Count);
        }


        [Test]
        public void CurrentItem_InitializedAsNew()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsNullOrEmpty(viewModel.CurrentItem.Name);
        }

        [Test]
        public void SelectedBlockLocation_SetWhenFirstDividerPlaced()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.CurrentItem.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual(viewModel.CurrentItem.Locations[0], viewModel.SelectedBlockLocation);
        }

        [Test]
        public void SelectedBlockLocation_UpdatedOnSelectedGroupChange()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.CurrentItem.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);

            //select group 2
            viewModel.SelectedBlockGroup = viewModel.CurrentItem.Groups[1];
            Assert.AreEqual(viewModel.CurrentItem.Locations[1], viewModel.SelectedBlockLocation);

            //select group 1
            viewModel.SelectedBlockGroup = viewModel.CurrentItem.Groups[0];
            Assert.AreEqual(viewModel.CurrentItem.Locations[0], viewModel.SelectedBlockLocation);
        }

        [Test]
        public void SelectedBlockGroup_SetWhenFirstDividerPlaced()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.CurrentItem.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual(viewModel.CurrentItem.Groups[0], viewModel.SelectedBlockGroup);
        }

        [Test]
        public void SelectedBlockGroup_UpdatedOnSelectedLocationChange()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.CurrentItem.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);

            //select loc 2
            viewModel.SelectedBlockLocation = viewModel.CurrentItem.Locations[1];
            Assert.AreEqual(viewModel.CurrentItem.Groups[1], viewModel.SelectedBlockGroup);

            //select group 1
            viewModel.SelectedBlockLocation = viewModel.CurrentItem.Locations[0];
            Assert.AreEqual(viewModel.CurrentItem.Groups[0], viewModel.SelectedBlockGroup);
        }


        [Test]
        public void ToolType_Initialized()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Assert.AreEqual(BlockingToolType.Pointer, viewModel.ToolType);

        }

        [Test]
        public void ToolType_Setter()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            viewModel.ToolType = BlockingToolType.BlockConnector;
            Assert.AreEqual(BlockingToolType.BlockConnector, viewModel.ToolType);
            Assert.Contains("ToolType", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void DisplayUnits_Initialized()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            Assert.AreNotEqual(DisplayUnitOfMeasureCollection.Empty, viewModel.DisplayUnits);
        }

        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            var oldModel = viewModel.CurrentItem;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentItem);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void OpenCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            foreach (var info in viewModel.AvailableItems)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.CurrentItem.Id);
                Assert.IsFalse(viewModel.CurrentItem.IsDirty);
            }
        }

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            Assert.IsFalse(viewModel.CurrentItem.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.CurrentItem.Name = "Test1";
            viewModel.CurrentItem.ProductGroupId = 1;

            Assert.IsTrue(viewModel.CurrentItem.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";
            viewModel.CurrentItem.ProductGroupId = 1;

            //add a couple of dividers
            viewModel.CurrentItem.Dividers.Add(0.2F, 0, PlanogramBlockingDividerType.Vertical);
            viewModel.CurrentItem.Dividers.Add(0.6F, 0, PlanogramBlockingDividerType.Vertical);

            //save
            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);
            Assert.AreEqual(2, viewModel.CurrentItem.Dividers.Count);

            //fetch back
            Blocking savedProfile = Blocking.FetchById(viewModel.CurrentItem.Id);
            Assert.AreEqual(2, savedProfile.Dividers.Count);
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            Int32 preCount = BlockingInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            String newName = "SaveAsName";

            viewModel.SaveAsCommand.Execute(newName);

            Assert.AreEqual(newName, viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);

            Assert.AreEqual(preCount + 1, BlockingInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";
            viewModel.CurrentItem.ProductGroupId = 1;

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            Assert.IsFalse(viewModel.CurrentItem.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            Int32 preCount = BlockingInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.AreEqual(preCount - 1, BlockingInfoList.FetchByEntityId(this.EntityId).Count);
        }

        

        #endregion

        #region ContinueWithItemChange


        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            var viewModel = new BlockingMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.CurrentItem.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            var viewModel = new BlockingMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.CurrentItem.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }

        #endregion

    }
}
