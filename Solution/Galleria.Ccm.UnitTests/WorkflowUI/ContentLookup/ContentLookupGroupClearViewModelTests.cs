﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
// V8-28444 : M.Shelley
//  Add comment about test Property_ClearSequence failure
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ContentLookup
{
    [TestFixture]
    [Serializable]
    public class ContentLookupGroupClearViewModelTests : TestBase<ContentLookupGroupClearViewModel>
    {
        #region TestFixture Helpers

        public override void Setup()
        {
            base.Setup();

            //load the test model
            this.TestModel = new ContentLookupGroupClearViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_DialogResult()
        {
            Assert.IsNull(TestModel.DialogResult, "Should initially be null");
        }

        [Test]
        public void Property_ClearProductUniverse()
        {
            String propertyName = "ClearProductUniverse";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearProductUniverseProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearProductUniverse, "Should initally be true");
        }

        [Test]
        public void Property_ClearAssortment()
        {
            String propertyName = "ClearAssortment";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearAssortmentProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearAssortment, "Should initally be true");
        }

        // 21/12/2014
        // ToDo: This test has been left as a failure because the Auto Assign functionality has been moved to version 801
        // In the Beta release, this needs to be set to false, but will need to be changed when it is added back to the UI
        [Test]
        public void Property_ClearSequence()
        {
            String propertyName = "ClearSequence";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearSequenceProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearSequence, "Should initally be true - but refer to ToDo comment");
        }

        [Test]
        public void Property_ClearCDT()
        {
            String propertyName = "ClearCDT";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearConsumerDecisionTreeProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearCDT, "Should initally be true");
        }

        [Test]
        public void Property_ClearAssortmentMinorRevision()
        {
            String propertyName = "ClearAssortmentMinorRevision";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearAssortmentMinorRevisionProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearAssortmentMinorRevision, "Should initally be true");
        }

        [Test]
        public void Property_ClearMetricProfile()
        {
            String propertyName = "ClearMetricProfile";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearMetricProfileProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearMetricProfile, "Should initally be true");
        }

        [Test]
        public void Property_ClearInventoryProfile()
        {
            String propertyName = "ClearInventoryProfile";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearInventoryProfileProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearInventoryProfile, "Should initally be true");
        }

        [Test]
        public void ClearPerformanceSelections()
        {
            String propertyName = "ClearPerformanceSelections";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearPerformanceSelectionsProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearPerformanceSelections, "Should initally be true");
        }


        [Test]
        public void Property_ClearPlanogramNameTemplate()
        {
            String propertyName = "ClearPlanogramNameTemplate";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearPlanogramNameTemplateProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearPlanogramNameTemplate, "Should initally be true");
        }

        [Test]
        public void Property_ClearBlocking()
        {
            String propertyName = "ClearBlocking";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearBlockingProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearBlocking, "Should initally be true");
        }

        [Test]
        public void Property_ClearCluster()
        {
            String propertyName = "ClearCluster";
            Assert.AreEqual(propertyName, ContentLookupGroupClearViewModel.ClearClusterProperty.Path);

            //check initial value
            Assert.True(this.TestModel.ClearCluster, "Should initally be true");
        }

        #endregion

        #region Commands

        [Test]
        public void Command_OkPressedCommand()
        {
            RelayCommand cmd = this.TestModel.OkPressedCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            Assert.IsTrue(cmd.CanExecute(), "Command should always be enabled");

            //check execution
            cmd.Execute();

            Assert.IsNotNull(this.TestModel.DialogResult, "Dialog Result should not be null");
            Assert.IsNotNull((Boolean)this.TestModel.DialogResult, "Should be true but was not.");
        }

        [Test]
        public void Command_CancelPressedCommand()
        {
            RelayCommand cmd = this.TestModel.CancelPressedCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            Assert.IsTrue(cmd.CanExecute(), "Command should always be enabled");

            //check execution
            cmd.Execute();

            Assert.IsNotNull(this.TestModel.DialogResult, "Dialog Result should not be null");
            Assert.IsNotNull((Boolean)this.TestModel.DialogResult, "Should be true but was not.");
        }

        #endregion

        #region Constructor

        //Nothing to test here. (Simple VM).

        #endregion
    }
}
