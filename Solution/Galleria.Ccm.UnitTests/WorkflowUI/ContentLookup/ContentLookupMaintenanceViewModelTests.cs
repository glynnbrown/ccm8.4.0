﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ContentLookup
{
    [TestFixture]
    [Serializable]
    public class ContentLookupMaintenanceViewModelTests : TestBase<ContentLookupMaintenanceViewModel>
    {
        #region TestFixture Helpers

        public override void Setup()
        {
            base.Setup();

            //load the test model
            this.TestModel = new ContentLookupMaintenanceViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_ProductGroups()
        {
            String propertyName = "ProductGroups";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.ProductGroupsProperty.Path);
        }

        [Test]
        public void Property_ProductGroupViewModels()
        {
            String propertyName = "ProductGroupViewModels";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.ProductGroupViewModelsProperty.Path);
        }

        [Test]
        public void Property_SelectedProductGroup()
        {
            String propertyName = "SelectedProductGroupViewModel";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.SelectedProductGroupViewModelProperty.Path);
        }

        [Test]
        public void Property_CurrentProductGroupViewModel()
        {
            String propertyName = "CurrentProductGroupViewModel";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.CurrentProductGroupViewModelProperty.Path);
        }

        [Test]
        public void Property_ContentLookupRows()
        {
            String propertyName = "ContentLookupRows";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.ContentLookupRowsProperty.Path);
        }

        [Test]
        public void Property_SelectedContentLookupRows()
        {
            String propertyName = "SelectedContentLookupRows";
            Assert.AreEqual(propertyName, ContentLookupMaintenanceViewModel.SelectedContentLookupRowsProperty.Path);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_Open()
        {
            RelayCommand cmd = this.TestModel.OpenCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled when no selected item");
            }
        }

        [Test]
        public void Command_SaveCommand()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_SaveAndCloseCommand()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_DeleteCommand()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));
            
            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_AutoAssignAllCommand()
        {
            RelayCommand cmd = this.TestModel.AutoAssignAllCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearAllCommand()
        {
            RelayCommand cmd = this.TestModel.ClearAllCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_AutoAssignProductUniverseCommand()
        {
            RelayCommand cmd = this.TestModel.AutoAssignProductUniverseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManualAssignProductUniverseCommand()
        {
            RelayCommand cmd = this.TestModel.ManualAssignProductUniverseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearProductUniverseCommand()
        {
            RelayCommand cmd = this.TestModel.ClearProductUniverseCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }


        [Test]
        public void Command_ClearPlanogramNameTemplateCommand()
        {
            RelayCommand cmd = this.TestModel.ClearPlanogramNameTemplateCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_AutoAssignAssortmentCommand()
        {
            RelayCommand cmd = this.TestModel.AutoAssignAssortmentCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignAssortmentCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignAssortmentCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearAssortmentCommand()
        {
            RelayCommand cmd = this.TestModel.ClearAssortmentCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignMetricProfileCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignMetricProfileCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearMetricProfileCommand()
        {
            RelayCommand cmd = this.TestModel.ClearMetricProfileCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        //[Test]
        //public void Command_AutoAssignSequenceCommand()
        //{
        //    RelayCommand cmd = this.TestModel.AutoAssignSequenceCommand;

        //    //check the command is registered
        //    Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

        //    //check the command properties
        //    Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
        //    Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

        //    //check can execute state
        //    if (this.TestModel.SelectedProductGroupViewModel == null)
        //    {
        //        Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
        //    }

        //    //create blank contentLookup
        //    this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

        //    //check can execute state
        //    if (this.TestModel.SelectedProductGroupViewModel != null)
        //    {
        //        Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
        //    }
        //}

        [Test]
        public void Command_ClearSequenceCommand()
        {
            RelayCommand cmd = this.TestModel.ClearSequenceCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignSequenceCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignSequenceCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_AutoAssignConsumerDecisionTreeCommand()
        {
            RelayCommand cmd = this.TestModel.AutoAssignConsumerDecisionTreeCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignConsumerDecisoonTreeCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignConsumerDecisionTreeCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearConsumerDecisionTreeCommand()
        {
            RelayCommand cmd = this.TestModel.ClearConsumerDecisionTreeCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_AutoAssignMinorAssortmentCommand()
        {
            RelayCommand cmd = this.TestModel.AutoAssignMinorAssortmentCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignMinorAssortmentCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignMinorAssortmentCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignPerformanceSelectionCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignPerformanceSelectionCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignPlanogramNameTemplateCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignPlanogramNameTemplateCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearPerformanceSelectionCommand()
        {
            RelayCommand cmd = this.TestModel.ClearPerformanceSelectionCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ClearBlockingCommand()
        {
            RelayCommand cmd = this.TestModel.ClearBlockingCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        [Test]
        public void Command_ManuallyAssignBlockingCommand()
        {
            RelayCommand cmd = this.TestModel.ManuallyAssignBlockingCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel == null)
            {
                Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
            }

            //create blank contentLookup
            this.TestModel.SelectedContentLookupRows.Add(new ContentLookupRow(null));

            //check can execute state
            if (this.TestModel.SelectedProductGroupViewModel != null)
            {
                Assert.IsTrue(cmd.CanExecute(), "Command should be disabled");
            }
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            Assert.IsFalse(TestModel.SaveAndCloseCommand.CanExecute());
            Assert.IsFalse(TestModel.SaveCommand.CanExecute());
            Assert.IsFalse(TestModel.DeleteCommand.CanExecute());
            Assert.IsFalse(TestModel.AutoAssignAllCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearAllCommand.CanExecute());
            Assert.IsFalse(TestModel.AutoAssignAssortmentCommand.CanExecute());
            Assert.IsFalse(TestModel.AutoAssignConsumerDecisionTreeCommand.CanExecute());
            Assert.IsFalse(TestModel.AutoAssignMinorAssortmentCommand.CanExecute());
            Assert.IsFalse(TestModel.AutoAssignProductUniverseCommand.CanExecute());
            //Assert.IsFalse(TestModel.AutoAssignSequenceCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearAssortmentCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearBlockingCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearClusterCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearConsumerDecisionTreeCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearInventoryProfileCommand.CanExecute());
            Assert.IsFalse(TestModel.ClearMetricProfileCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignClusterCommand.CanExecute());
            Assert.IsFalse(TestModel.ManualAssignProductUniverseCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignAssortmentCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignConsumerDecisionTreeCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignInventoryProfileCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignPerformanceSelectionCommand.CanExecute());
            Assert.IsFalse(TestModel.ManuallyAssignSequenceCommand.CanExecute());
        }

        #endregion
    }
}
