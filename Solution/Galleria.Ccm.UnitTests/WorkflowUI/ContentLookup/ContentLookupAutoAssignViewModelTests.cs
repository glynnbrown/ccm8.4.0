﻿
#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
// V8-28444 : M.Shelley
//  Add comment about test Property_AutoAssignSequence failure
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ContentLookup
{
    [TestFixture]
    [Serializable]
    public class ContentLookupAutoAssignViewModelTests : TestBase<ContentLookupAutoAssignViewModel>
    {
        #region TestFixture Helpers

        public override void Setup()
        {
            base.Setup();

            //load the test model
            this.TestModel = new ContentLookupAutoAssignViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_DialogResult()
        {
            Assert.IsNull(TestModel.DialogResult, "Should initially be null");
        }

        [Test]
        public void Property_AutoAssignProductUniverse()
        {
            String propertyName = "AutoAssignProductUniverse";
            Assert.AreEqual(propertyName, ContentLookupAutoAssignViewModel.AutoAssignProductUniverseProperty.Path);

            //check initial value
            Assert.True(this.TestModel.AutoAssignProductUniverse, "Should initally be true");
        }

        [Test]
        public void Property_AutoAssignAssortment()
        {
            String propertyName = "AutoAssignAssortment";
            Assert.AreEqual(propertyName, ContentLookupAutoAssignViewModel.AutoAssignAssortmentProperty.Path);

            //check initial value
            Assert.True(this.TestModel.AutoAssignAssortment, "Should initally be true");
        }

        // 21/12/2014
        // ToDo: This test has been left as a failure because the Auto Assign functionality has been moved to version 801
        // In the Beta release, this needs to be set to false, but will need to be changed when it is added back to the UI
        //[Test]
        //public void Property_AutoAssignSequence()
        //{
        //    String propertyName = "AutoAssignSequence";
        //    Assert.AreEqual(propertyName, ContentLookupAutoAssignViewModel.AutoAssignSequenceProperty.Path);

        //    //check initial value
        //    Assert.True(this.TestModel.AutoAssignSequence, "Should initally be true - but refer to ToDo comment");
        //}

        [Test]
        public void Property_AutoAssignCDT()
        {
            String propertyName = "AutoAssignCDT";
            Assert.AreEqual(propertyName, ContentLookupAutoAssignViewModel.AutoAssignConsumerDecisionTreeProperty.Path);

            //check initial value
            Assert.True(this.TestModel.AutoAssignCDT, "Should initally be true");
        }

        [Test]
        public void Property_AutoAssignAssortmentMinorRevision()
        {
            String propertyName = "AutoAssignAssortmentMinorRevision";
            Assert.AreEqual(propertyName, ContentLookupAutoAssignViewModel.AutoAssignAssortmentMinorRevisionProperty.Path);

            //check initial value
            Assert.True(this.TestModel.AutoAssignAssortmentMinorRevision, "Should initally be true");
        }

        #endregion

        #region Commands

        [Test]
        public void Command_OkPressedCommand()
        {
            RelayCommand cmd = this.TestModel.OkPressedCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            Assert.IsTrue(cmd.CanExecute(), "Command should always be enabled");

            //check execution
            cmd.Execute();

            Assert.IsNotNull(this.TestModel.DialogResult, "Dialog Result should not be null");
            Assert.IsNotNull((Boolean)this.TestModel.DialogResult, "Should be true but was not.");
        }

        [Test]
        public void Command_CancelPressedCommand()
        {
            RelayCommand cmd = this.TestModel.CancelPressedCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "Friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //check can execute states
            Assert.IsTrue(cmd.CanExecute(), "Command should always be enabled");

            //check execution
            cmd.Execute();

            Assert.IsNotNull(this.TestModel.DialogResult, "Dialog Result should not be null");
            Assert.IsNotNull((Boolean)this.TestModel.DialogResult, "Should be true but was not.");
        }

        #endregion

        #region Constructor

        //Nothing to test here. (Simple VM).

        #endregion
    }
}
