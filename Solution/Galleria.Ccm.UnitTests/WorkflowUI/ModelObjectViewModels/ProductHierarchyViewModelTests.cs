﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25450 : L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ModelObjectViewModels
{
    [TestFixture]
    public class ProductHierarchyViewModelTests : TestBase<ProductHierarchyViewModel>
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            //setup the base
            base.Setup();
            this.TestModel = new ProductHierarchyViewModel();
        }


        #endregion

        #region Methods

        [Test]
        public void FetchForCurrentEntity()
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);

            this.TestModel.FetchMerchandisingHierarchyForCurrentEntity();

            Assert.AreEqual(this.TestModel.Model.Id, hierarchy.Id);
        }

        [Test]
        public void Save()
        {
            this.TestModel.FetchMerchandisingHierarchyForCurrentEntity();

            String newName = "TEST";
            this.TestModel.Model.Name = newName;
            this.TestModel.Save();

            Assert.AreEqual(this.TestModel.Model.Name, newName);

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            Assert.AreEqual(hierarchy.Name, newName);
        }

        #endregion
    }
}
