﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25450 : L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ModelObjectViewModels
{
    [TestFixture]
    public class ProductGroupViewModelTests : TestBase<ProductGroupViewModel>
    {
        #region Test Fixture Helpers

        private ProductHierarchyViewModel _structureView;

        public override void Setup()
        {
            base.Setup();

            _structureView = new ProductHierarchyViewModel();
            _structureView.Model = ProductHierarchy.NewProductHierarchy(1);

            this.TestModel = new ProductGroupViewModel(_structureView.Model.RootGroup);
        }

        #endregion

        #region Properties

        [Test]
        public void Property_ProductGroup()
        {
            String propertyName = "ProductGroup";
            Assert.AreEqual(propertyName, ProductGroupViewModel.ProductGroupProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.AreEqual(_structureView.Model.RootGroup, this.TestModel.ProductGroup);
        }

        [Test]
        public void Property_AssociatedLevel()
        {
            String propertyName = "AssociatedLevel";
            Assert.AreEqual(propertyName, ProductGroupViewModel.AssociatedLevelProperty.Path);

            Assert.AreEqual(_structureView.Model.RootLevel, this.TestModel.AssociatedLevel);
        }


        [Test]
        public void Property_LevelPathValues()
        {
            String propertyName = "LevelPathValues";
            Assert.AreEqual(propertyName, ProductGroupViewModel.LevelPathValuesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

    }
}
