﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.EntityMaintenance
{
    [TestFixture]

    class EntityMaintenanceViewModelTest : TestBase<EntityMaintenanceViewModel>
    {
        #region Test Fixture Helpers

        private const Int32 _numentitiesInserted = 10;

        public override void Setup()
        {
            base.Setup();


            Helpers.TestDataHelper.InsertLocationDtos(this.DalFactory, _numentitiesInserted, 10);

            this.TestModel = new EntityMaintenanceViewModel();
        }


        #endregion

        #region Properties


        [Test]
        public void Property_AvailableEntities()
        {
            String propertyName = "AvailableEntities";
            Assert.AreEqual(propertyName, EntityMaintenanceViewModel.AvailableEntitiesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableEntities);

            EntityInfoList locInfos = EntityInfoList.FetchAllEntityInfos();
            Assert.AreEqual(locInfos.Count, this.TestModel.AvailableEntities.Count);
        }

        [Test]
        public void Property_SelectedEntity()
        {
            String propertyName = "SelectedEntity";
            Assert.AreEqual(propertyName, EntityMaintenanceViewModel.SelectedEntityProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Constructors

        [Test]
        public void Constructor()
        {

            this.TestModel = new EntityMaintenanceViewModel();

            //we should start with a new store
            Assert.IsTrue(this.TestModel.SelectedEntity.IsNew);

            //the info list model should also contain existing stores
            Assert.AreEqual(_numentitiesInserted, this.TestModel.AvailableEntities.Count());
        }
        #endregion

        #region commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;

            //check that the command is registered and value are correct 
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);


            //check and execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            Entity preExecuteItem = this.TestModel.SelectedEntity;

            cmd.Execute();

            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedEntity, "this item should have changed.");
            Assert.IsTrue(this.TestModel.SelectedEntity.IsNew, "this item should be new");
            Assert.IsTrue(this.TestModel.SelectedEntity.IsInitialized, " this item should be marked initialized");

            Assert.AreEqual(0, this.TestModel.SelectedEntity.Id, "No ID has been assigned");
        }

        #endregion

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            Entity preExecuteIem = this.TestModel.SelectedEntity;

            EntityInfo loadInfo = this.TestModel.AvailableEntities.Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteIem, this.TestModel.SelectedEntity, "item should not be changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedEntity.Id, "the entity id should have been loaded");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications, "selectedstore should have change notification");
        }


        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check that command can execute

            Assert.IsFalse(cmd.CanExecute(), "save shoud be disabled as invalid object");
            this.TestModel.SelectedEntity.Name = "st1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check that the command execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();


            //check that the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedEntity.IsNew, "the store should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedEntity.IsDirty, "the store should have been saved");
            Assert.AreEqual("st1", this.TestModel.SelectedEntity.Name, "the store should be thesame");
            Assert.AreNotEqual(0, this.TestModel.SelectedEntity.Id, "the store should have been given a valid id");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications);

            //check an info has been added for the new store
            EntityInfo addedInfo = this.TestModel.AvailableEntities.FirstOrDefault(l => l.Id == this.TestModel.SelectedEntity.Id);
            Assert.IsNotNull(addedInfo, "an info for new store should have been added");

            //check that another store with the same location cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedEntity.Name = "st2";
            cmd.Execute();


        }


        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check that the command is registered

            Assert.AreNotEqual(cmd, TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "save should be disabled as invalid object");
            this.TestModel.SelectedEntity.Name = "st1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            EntityInfo loadInfo = this.TestModel.AvailableEntities.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Entity preExecuteItem = this.TestModel.SelectedEntity;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedEntity, "item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.SelectedEntity, "id should be different");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications, "should have change notifications");

            //check infos are correct
            EntityInfo oldItemInfo = this.TestModel.AvailableEntities.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "an info for the old item should still exist");

            EntityInfo newItemInfo = this.TestModel.AvailableEntities.FirstOrDefault(i => loadInfo.Id == preExecuteItem.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");

        }




        [Test]
        public void Command_SavaAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check execution
            base.PropertyChangedNotifications.Clear();
            String EntityName = "st1";

            Assert.IsFalse(cmd.CanExecute(), "save should be disabled as an invalid object");
            this.TestModel.SelectedEntity.Name = EntityName;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check excution
            cmd.Execute();


            Assert.IsTrue(this.TestModel.SelectedEntity.IsNew, "A new store should have been loaded");
            Assert.AreEqual(EntityName, this.TestModel.AvailableEntities.Last().Name, "the saved store should be in the available list");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications, "selected store should have changed notifications");

            

          


        }


        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "save should be disabled as invalid object");
            this.TestModel.SelectedEntity.Name = "st1";
            Assert.True(cmd.CanExecute(), "save enabled as store is valid");


            //check execution
            //nb: cannot test that the window is closed.

            Int32 preLocationCount = this.TestModel.AvailableEntities.Count();

            cmd.Execute();
            Assert.AreEqual(preLocationCount + 1, this.TestModel.AvailableEntities.Count);
        }


        [Test]

        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check that command can execute
            Assert.IsFalse(cmd.CanExecute(), "should not be able to delete a store");

            //add a store and open
            EntityInfo loadInfo = this.TestModel.AvailableEntities.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "it should be possible to delete an existing store");

            //check execute
            Int32 oldEntityId = this.TestModel.SelectedEntity.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedEntity.IsNew, "A new store should have been loaded");

            EntityInfo oldInfo = this.TestModel.AvailableEntities.FirstOrDefault(l => l.Id == oldEntityId);
            Assert.IsNull(oldInfo, "the deleted store should have been removed from the available list");


            Entity oldentity = Entity.FetchById(oldEntityId);
            Assert.IsNotNull(oldentity.DateDeleted, "should be marked deleted");

            

        }


        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", EntityMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Gem26314()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;
            //checks that the Entity opened cannot be deleted
            this.TestModel.OpenCommand.Execute(base.EntityId);
            Assert.IsFalse(cmd.CanExecute());

           

            //add and delete an entity
            EntityInfo entity = this.TestModel.AvailableEntities.Last();
            this.TestModel.OpenCommand.Execute(entity.Id);
            Assert.IsTrue(cmd.CanExecute(), "delete an entity");
        }

    }
}
