﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25732 : L.Ineson
//  Created
#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated with new default data updates on entities
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkflowMaintenance
{
    [TestFixture]
    public sealed class WorkflowMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private void InsertWorkflows()
        {
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            var flow1 = Galleria.Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow1.Name = "Workflow 1";
            flow1.Tasks.Add(engineTasks.First());
            flow1.Tasks.Add(engineTasks.First());
            flow1 = flow1.Save();

            var flow2 = Galleria.Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow2.Name = "Workflow 2";
            flow2.Tasks.Add(engineTasks.First());
            flow2 = flow2.Save();
        }

        public override void Setup()
        {
            base.Setup();
        }

        #endregion

        #region Properties

        [Test]
        public void AvailableEngineTasks()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableEngineTasks.Count);
        }

        [Test]
        public void CurrentWorkflow_InitializedAsNew()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsTrue(viewModel.CurrentWorkflow.IsNew);
            Assert.IsNullOrEmpty(viewModel.CurrentWorkflow.Name);
        }

        [Test]
        public void AvailableWorkflows_FetchedOnStartup()
        {
            //insert some workflows.
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableWorkflows.Count);
        }

        [Test]
        public void AvailableWorkflows_UpdatedOnWorkflowInsert()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            Int32 count = viewModel.AvailableWorkflows.Count;

            //save the new workflow
            viewModel.CurrentWorkflow.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableWorkflows.Count);
        }

        [Test]
        public void AvailableWorkflows_UpdatedOnWorkflowDelete()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();
            Int32 count = viewModel.AvailableWorkflows.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableWorkflows.Count);
        }

        [Test]
        public void SelectedWorkflowTask_InitializedAsNull()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsNull(viewModel.SelectedWorkflowTask);
        }

        [Test]
        public void SelectedWorkflowTask_FirstInWorkflowSelected()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();

            //open a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Assert.AreEqual(0, viewModel.SelectedWorkflowTask.SequenceId);
        }

        [Test]
        public void CurrentTaskParameters_InitializedAsEmpty()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.AreEqual(0, viewModel.CurrentTaskParameters.Count);
        }

        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            var oldModel = viewModel.CurrentWorkflow;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentWorkflow);
            Assert.IsTrue(viewModel.CurrentWorkflow.IsNew);
        }

        [Test]
        public void NewCommand_DisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowCreate.ToString());

            //check
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsFalse(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        [Test]
        public void OpenCommand_Execution()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();

            foreach (var info in viewModel.AvailableWorkflows)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.CurrentWorkflow.Id);
                Assert.IsFalse(viewModel.CurrentWorkflow.IsDirty);
            }
        }

        //[Test]
        //public void OpenCommand_DisabledWhenNoFetchPerm()
        //{
        //      Cant test this - only requires authenticated perm.
        //    InsertWorkflows();

        //    //remove the permission from the user.
        //    DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
        //    DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
        //    domainIdentity.Roles.Remove(DomainPermission.WorkflowGet.ToString());

        //    var viewModel = new WorkflowMaintenanceViewModel();
        //    Assert.IsFalse(viewModel.OpenCommand.CanExecute(viewModel.AvailableWorkflows.First().Id));
        //}

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            Assert.IsFalse (viewModel.CurrentWorkflow.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.CurrentWorkflow.Name = "Test1";

            Assert.IsTrue(viewModel.CurrentWorkflow.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.CurrentWorkflow.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowCreate.ToString());

            viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertWorkflows();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowCreate.ToString());

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);
            viewModel.CurrentWorkflow.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowEdit.ToString());

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.CurrentWorkflow.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertWorkflows();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowEdit.ToString());

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);
            viewModel.CurrentWorkflow.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            viewModel.CurrentWorkflow.Name = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.CurrentWorkflow.Name);
            Assert.IsFalse(viewModel.CurrentWorkflow.IsDirty);
        }

        [Test]
        public void SaveAsCommand_DisabledWhenNoCreatePerm()
        {
            InsertWorkflows();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowCreate.ToString());

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);
            viewModel.CurrentWorkflow.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();

            Int32 preCount = WorkflowInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsCommand.Execute();

            Assert.AreEqual(newName, viewModel.CurrentWorkflow.Name);
            Assert.IsFalse(viewModel.CurrentWorkflow.IsDirty);

            Assert.AreEqual(preCount + 1, WorkflowInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAsCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAsCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.F12, cmd.InputGestureKey);
        }

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            viewModel.CurrentWorkflow.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.CurrentWorkflow.Name);
            Assert.IsTrue(viewModel.CurrentWorkflow.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndNew_16, cmd.SmallIcon);
        }

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            viewModel.CurrentWorkflow.Name = "Test1";

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("Test1", WorkflowInfoList.FetchByEntityId(this.EntityId).Last().Name);
        }

        [Test]
        public void SaveAndCloseCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndCloseCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndClose_16, cmd.SmallIcon);
        }

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();

            Assert.IsTrue(viewModel.CurrentWorkflow.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);
            Assert.IsFalse(viewModel.CurrentWorkflow.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertWorkflows();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.WorkflowDelete.ToString());

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();

            Int32 preCount = WorkflowInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.CurrentWorkflow.IsNew);
            Assert.AreEqual(preCount - 1, WorkflowInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();

            Int32 preCount = WorkflowInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.CurrentWorkflow.IsNew);
            Assert.AreEqual(preCount, WorkflowInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Delete_16, cmd.SmallIcon);
        }

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }

        [Test]
        public void AddTaskCommand_DisabledWhenNoSelectedTask()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            viewModel.SelectedEngineTask = null;

            Assert.IsFalse(viewModel.AddTaskCommand.CanExecute());

            viewModel.SelectedEngineTask = viewModel.AvailableEngineTasks.First();

            Assert.IsTrue(viewModel.AddTaskCommand.CanExecute());
        }

        [Test]
        public void AddTaskCommand_Execution()
        {
            var viewModel = new WorkflowMaintenanceViewModel();

            Int32 taskCount = viewModel.CurrentWorkflow.Tasks.Count;

            viewModel.SelectedEngineTask = viewModel.AvailableEngineTasks.First();
            viewModel.AddTaskCommand.Execute();

            Assert.AreEqual(taskCount + 1, viewModel.CurrentWorkflow.Tasks.Count);

            Assert.AreEqual(viewModel.SelectedEngineTask.Name, viewModel.CurrentWorkflow.Tasks.Last().Details.Name);
        }

        [Test]
        public void RemoveTaskCommand_DisabledWhenNoSelectedTask()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsNull(viewModel.SelectedWorkflowTask);
            Assert.IsFalse(viewModel.RemoveTaskCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Assert.IsNotNull(viewModel.SelectedWorkflowTask);
            Assert.IsTrue(viewModel.RemoveTaskCommand.CanExecute());
        }

        [Test]
        public void RemoveTaskCommand_Execution()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Int32 taskCount = viewModel.CurrentWorkflow.Tasks.Count;

            viewModel.RemoveTaskCommand.Execute();

            Assert.AreEqual(taskCount - 1, viewModel.CurrentWorkflow.Tasks.Count);
        }

        [Test]
        public void MoveTaskUpCommand_DisabledWhenNoSelectedTask()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsNull(viewModel.SelectedWorkflowTask);
            Assert.IsFalse(viewModel.MoveTaskUpCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Assert.IsNotNull(viewModel.SelectedWorkflowTask);
            Assert.IsTrue(viewModel.MoveTaskUpCommand.CanExecute());
        }

        [Test]
        public void MoveTaskUpCommand_Execution()
        {
            //create a flow with multiple tasks
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();
            var flow1 = Galleria.Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow1.Name = "Workflow 1";
            flow1.Tasks.Add(engineTasks.First());
            flow1.Tasks.Add(engineTasks.First());
            flow1 = flow1.Save();

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            WorkflowTask task1 = viewModel.CurrentWorkflow.Tasks[0];
            WorkflowTask task2 = viewModel.CurrentWorkflow.Tasks[1];

            viewModel.SelectedWorkflowTask = task2;
            viewModel.MoveTaskUpCommand.Execute();

            Assert.AreEqual(0, task2.SequenceId);
            Assert.AreEqual(1, task1.SequenceId);

            //move up again
            Assert.AreEqual(task2, viewModel.SelectedWorkflowTask);
            viewModel.MoveTaskUpCommand.Execute();

            Assert.AreEqual(0, task2.SequenceId);
            Assert.AreEqual(1, task1.SequenceId);
        }

        [Test]
        public void MoveTaskDownCommand_DisabledWhenNoSelectedTask()
        {
            InsertWorkflows();
            var viewModel = new WorkflowMaintenanceViewModel();
            Assert.IsNull(viewModel.SelectedWorkflowTask);
            Assert.IsFalse(viewModel.MoveTaskDownCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.First().Id);

            Assert.IsNotNull(viewModel.SelectedWorkflowTask);
            Assert.IsTrue(viewModel.MoveTaskDownCommand.CanExecute());
        }

        [Test]
        public void MoveTaskDownCommand_Execution()
        {
            //create a flow with multiple tasks
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();
            var flow1 = Galleria.Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow1.Name = "Workflow 1";
            flow1.Tasks.Add(engineTasks.First());
            flow1.Tasks.Add(engineTasks.First());
            flow1 = flow1.Save();

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableWorkflows.Last().Id);

            WorkflowTask task1 = viewModel.CurrentWorkflow.Tasks[0];
            WorkflowTask task2 = viewModel.CurrentWorkflow.Tasks[1];

            viewModel.SelectedWorkflowTask = task1;
            viewModel.MoveTaskDownCommand.Execute();

            Assert.AreEqual(0, task2.SequenceId);
            Assert.AreEqual(1, task1.SequenceId);

            //move down again
            Assert.AreEqual(task1, viewModel.SelectedWorkflowTask);
            viewModel.MoveTaskDownCommand.Execute();

            Assert.AreEqual(0, task2.SequenceId);
            Assert.AreEqual(1, task1.SequenceId);
        }

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result=false; });
            
            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.CurrentWorkflow.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertWorkflows();

            var viewModel = new WorkflowMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.CurrentWorkflow.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion

    }
}
