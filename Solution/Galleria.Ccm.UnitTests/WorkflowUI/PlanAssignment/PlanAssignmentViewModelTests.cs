﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)

// V8-25879 : M.Shelley
// Created.

#endregion

#region Version History: (CCM 8.0.0)

// V8-30732/V8-30308/V8-30508 : M.Shelley
//  Extended unit test coverage to cover these tickets.

#endregion

#region Version History: CCM830

// V8-32385 : A.Silva
//  Added test coverage to ensure the correct auto selection of the currently selected cluster scheme.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanAssignment
{
    [TestFixture]
    public class PlanAssignmentViewModelTests : TestBase<PlanAssignmentViewModel>
    {
        private const int PlansToInsert = 5;

        private Int16 _selectedLocationId;
        private ProductGroupInfo _selectedProductGroup1Info;
        private ProductGroupInfo _selectedProductGroup2Info;
        private LocationInfo _selectedLocation;
        private PlanogramHierarchy _planHierarchy;
        private List<Int32> _planIdList;
        private PlanogramGroup _selectedPlanGroup;
        private ClusterSchemeInfoList _clusterSchemesInfoList;

        #region Test Helpers

        [SetUp]
        public void SetUp()
        {
            List<EntityDto> entityDtos = Helpers.TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            // Setup some planograms
            _planIdList = Helpers.TestDataHelper.InsertPlanograms(PlansToInsert);

            // ToDo: Setup a product group for a plan
            var plan1 = PlanogramInfoList.FetchByIds(new List<Int32>() { _planIdList.First() }).First();
            
            // Get the plan total width and bay count to use later
            Single planWidth = plan1.Width;
            Int32? planBayCount = plan1.MetaBayCount;

            // Setup a planogram group tree
            _planHierarchy = Helpers.TestDataHelper.PopulatePlanogramHierarchy(EntityId);

            var firstChildGroup = _planHierarchy.RootGroup.ChildList.FirstOrDefault();
            _selectedPlanGroup = firstChildGroup.ChildList.FirstOrDefault();
            PlanogramGroup.AssociatePlanograms(_selectedPlanGroup.Id, _planIdList);

            // Helpers.TestDataHelper.InsertProductDtos(base.DalFactory, 5, this.EntityId);

            var heirarachyId = ProductHierarchy.FetchByEntityId(base.EntityId).Id;
            var productLevels = Helpers.TestDataHelper.InsertProductLevelDtos(this.DalFactory, heirarachyId, 2);
            var productGroupDtos = Helpers.TestDataHelper.InsertProductGroupDtos(this.DalFactory, productLevels, 2);

            List<LocationHierarchyDto> locHierarchyDtos = Helpers.TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityDtos);
            List<LocationLevelDto> locLevelsDtos = Helpers.TestDataHelper.InsertLocationLevelDtos(base.DalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = Helpers.TestDataHelper.InsertLocationGroupDtos(base.DalFactory, locLevelsDtos, 2);
            List<LocationDto> locDtos = Helpers.TestDataHelper.InsertLocationDtos(base.DalFactory, base.EntityId, locGroupDtos, 5);
            List<LocationSpaceDto> locationSpaces = Helpers.TestDataHelper.InsertLocationSpaceDtos(this.DalFactory, locDtos, base.EntityId);

            // Save 2 different product group ids for use later
            var selectedProductGroup1Id = productGroupDtos[0].Id;
            var selectedProductGroup2Id = productGroupDtos[2].Id;

            // ToDo: Configure a location space so the width / bay count matches a planogram

            Int16 locationCount = 0;
            foreach (LocationSpaceDto locationSpace in locationSpaces)
            {
                var locationSpaceProductGroups = Helpers.TestDataHelper.InsertLocationSpaceProductGroupDtos(this.DalFactory, locationSpace.Id, productGroupDtos);
                foreach (LocationSpaceProductGroupDto locationSpaceProductGroup in locationSpaceProductGroups)
                {
                    // Set up a single bay location space for use later
                    Int32 bayCount = 3;
                    Single? bayWidth = null;

                    if (locationSpaceProductGroup.ProductGroupId == selectedProductGroup2Id) 
                    {
                        bayCount = planBayCount ?? 1;
                        bayWidth = planWidth;
                    }

                    var locationSpaceBayList = Helpers.TestDataHelper.InsertLocationSpaceBayDtos(this.DalFactory, locationSpaceProductGroup.Id, bayCount, bayWidth);
                }

                locationCount++;
            }

            // set cluster schemes
            List<ClusterSchemeDto> clusterSchemesDtos = Helpers.TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, base.EntityId, 5);
            List<ClusterDto> clustersDtos = Helpers.TestDataHelper.InsertClusterDtos(this.DalFactory, clusterSchemesDtos, 5);
            List<ClusterLocationDto> clusterLocationsDtos = Helpers.TestDataHelper.InsertClusterLocationDtos(this.DalFactory, clustersDtos, locDtos);

            _clusterSchemesInfoList = ClusterSchemeInfoList.FetchByEntityId(base.EntityId);

            var tmpPlanAssignments = Helpers.TestDataHelper.InsertLocationPlanAssignment(EntityId);

            // Set up a plan assignment for the 1st item in the plan assignment list
            var firstItem = tmpPlanAssignments[0];
            firstItem.PlanogramId = _planIdList.First();
            firstItem.LocationId = locDtos.First().Id;
            firstItem.ProductGroupId = selectedProductGroup1Id;
            //firstItem.DateAssigned = DateTime.Now;
            firstItem.SetAssignedDate(DateTime.Now);
            firstItem.AssignedByUserId = 1;

            // Set up a plan assignment for the 2nd item in the plan assignment list, using the same plan id and product group id
            var secondItem = tmpPlanAssignments[1];
            secondItem.PlanogramId = _planIdList.First();
            _selectedLocationId = locDtos[1].Id;
            var selectedLocationCode = locDtos.First().Code;
            secondItem.LocationId = _selectedLocationId;
            secondItem.ProductGroupId = selectedProductGroup2Id;
            //secondItem.DateAssigned = DateTime.Now;
            secondItem.SetAssignedDate(DateTime.Now);
            secondItem.AssignedByUserId = 2;

            tmpPlanAssignments.Save();

            // Set up selected product groups
            _selectedProductGroup1Info = ProductGroupInfo.FetchById(selectedProductGroup1Id);
            Assert.IsTrue(_selectedProductGroup1Info != null, "Could not set first selected product group.");

            _selectedProductGroup2Info = ProductGroupInfo.FetchById(selectedProductGroup2Id);
            Assert.IsTrue(_selectedProductGroup2Info != null, "Could not set second selected product group.");

            // Set up a selected location
            var locationList = LocationInfoList.FetchByEntityIdLocationCodes(base.EntityId, new List<String>() { selectedLocationCode });
            _selectedLocation = locationList.FirstOrDefault();
            Assert.IsTrue(_selectedLocation != null, "Could not set selected location.");

            // Create a new PlanAssignmentViewModel
            this.TestModel = new PlanAssignmentViewModel();
        }

        #endregion

        #region Command tests

        /// <summary>
        /// Check that by default the Assign button is disabled
        /// </summary>
        [Test]
        public void AssignCommand_StartsDisabled_Test()
        {
            RelayCommand assignCmd = this.TestModel.AssignPlanogramsCommand;

            // Check the command is registered
            Assert.Contains(assignCmd, this.TestModel.ViewModelCommands);

            bool canSelect = assignCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + assignCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(assignCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_NoItemSelected);
        }

        /// <summary>
        /// Test that when a row in the data grid is selected that the Assign button becomes enabled
        /// </summary>
        [Test]
        public void AssignCommand_BecomesEnabled_Test()
        {
            RelayCommand assignCmd = this.TestModel.AssignPlanogramsCommand;

            // Test it starts disabled 
            bool canSelect = assignCmd.CanExecute();
            Assert.IsFalse(canSelect, "The " + assignCmd.FriendlyName + " should not initially be enabled.");

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row
            TestModel.PlanAssignmentSelectedItems.Add(TestModel.MasterAssignmentItemRows.FirstOrDefault());

            // And check the Assign command is now enabled
            bool canExecute = assignCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + assignCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(assignCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Check that the "View By Product Group" button is enabled
        /// </summary>
        [Test]
        public void ViewByProductCategoryCommand_Enabled_Test()
        {
            RelayCommand viewByProductGroupCommand = this.TestModel.ViewByProductGroupCommand;

            // Check the command is registered
            Assert.Contains(viewByProductGroupCommand, this.TestModel.ViewModelCommands);

            Assert.IsTrue(viewByProductGroupCommand.CanExecute());
        }

        /// <summary>
        /// Check that the "View By Location" button is enabled
        /// </summary>
        [Test]
        public void ViewByLocationCommand_Enabled_Test()
        {
            RelayCommand viewByLocationCommand = this.TestModel.ViewByLocationCommand;

            // Check the command is registered
            Assert.Contains(viewByLocationCommand, this.TestModel.ViewModelCommands);

            Assert.IsTrue(viewByLocationCommand.CanExecute());
        }

        /// <summary>
        /// Check that the "Remove All" button is enabled
        /// </summary>
        [Test]
        public void RemoveAllPlanogramsCommand_IsEnabled_Test()
        {
            RelayCommand removeAllCmd = this.TestModel.RemoveAllPlanogramsCommand;

            // Check the command is registered
            Assert.Contains(removeAllCmd, this.TestModel.ViewModelCommands);

            bool canSelect = removeAllCmd.CanExecute();

            Assert.IsTrue(canSelect, "The " + removeAllCmd.FriendlyName + " should always be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(removeAllCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Test that the remove all command clears all plan assignments
        /// </summary>
        [Test]
        public void RemoveAllPlanogramsCommand_Test()
        {
            RelayCommand removeAllCmd = TestModel.RemoveAllPlanogramsCommand;

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);
            Assert.IsTrue(removeAllCmd.CanExecute(), "The " + removeAllCmd.FriendlyName + " should always be enabled.");

            removeAllCmd.Execute();

            // Check that there are no plans assigned any longer
            var plansAssignedList = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).ToList();
            var arePlansAssigned = plansAssignedList.Any();

            Assert.IsFalse(arePlansAssigned, "There should be no plans assigned.");
        }

        /// <summary>
        /// Check that by default the Unassign button is disabled
        /// </summary>
        [Test]
        public void RemovePlanogramCommand_StartsDisabled_Test()
        {
            RelayCommand removePlanCmd = this.TestModel.RemovePlanogramCommand;

            // Check the command is registered
            Assert.Contains(removePlanCmd, this.TestModel.ViewModelCommands);

            bool canSelect = removePlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + removePlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(removePlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_NoItemSelected);
        }

        /// <summary>
        /// Test that when a row in the data grid that has no plan assignment is selected that the Unassign button stays disabled
        /// </summary>
        [Test]
        public void RemovePlanogramCommand_StaysDisabled_Test()
        {
            RelayCommand removePlanCmd = this.TestModel.RemovePlanogramCommand;

            bool canSelect = removePlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + removePlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(removePlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_NoItemSelected);

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that does NOT have a plan assignment setup
            var notAssignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo == null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(notAssignedRowItem);

            // And check the Unassign command is still disabled
            bool canExecute = removePlanCmd.CanExecute();

            Assert.IsFalse(canExecute, "The " + removePlanCmd.FriendlyName + " button should be enabled.");
            Assert.AreEqual(removePlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_RemoveNoPlansSelected);
        }

        /// <summary>
        /// Test that when a row in the data grid with a plan assignment is selected that the Unassign button becomes enabled
        /// </summary>
        [Test]
        public void RemovePlanogramCommand_BecomesEnabled_Test()
        {
            RelayCommand removePlanCmd = this.TestModel.RemovePlanogramCommand;

            bool canSelect = removePlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + removePlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(removePlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_NoItemSelected);

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that has a plan assignment setup
            var assignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(assignedRowItem);

            // And check the Unassign command is now enabled
            bool canExecute = removePlanCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + removePlanCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(removePlanCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Test that the remove planogram removes the currently selected assignments
        /// </summary>
        [Test]
        public void RemovePlanogramCommand_Test()
        {
            RelayCommand removePlanCmd = this.TestModel.RemovePlanogramCommand;

            bool canSelect = removePlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + removePlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(removePlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_NoItemSelected);

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that has a plan assignment setup
            var assignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(assignedRowItem);

            // And check the Unassign command is now enabled
            bool canExecute = removePlanCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + removePlanCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(removePlanCmd.DisabledReason), "There should be no disabled reason.");

            // remove the currently selected plan assignment
            removePlanCmd.Execute();

            // Check that the plan assignment has indeed been removed
            Assert.IsTrue(assignedRowItem.PlanogramInfo == null, "The currently selected plan assignment should be cleared.");
        }

        /// <summary>
        /// Check that by default the "Open Planogram" button is disabled
        /// </summary>
        [Test]
        public void OpenPlanogramCommand_StartsDisabled_Test()
        {
            RelayCommand openPlanCmd = this.TestModel.OpenPlanogramCommand;

            // Check the command is registered
            Assert.Contains(openPlanCmd, this.TestModel.ViewModelCommands);

            bool canSelect = openPlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + openPlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(openPlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection);
        }

        /// <summary>
        /// Test that when a row in the data grid that has no plan assignment is selected that the "Open Planogram" button stays disabled
        /// </summary>
        [Test]
        public void OpenPlanogramCommand_StaysDisabled_Test()
        {
            RelayCommand openPlanCmd = this.TestModel.OpenPlanogramCommand;

            bool canSelect = openPlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + openPlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(openPlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection);

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that does NOT have a plan assignment setup
            var notAssignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo == null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(notAssignedRowItem);

            // And check the Unassign command is still disabled
            bool canExecute = openPlanCmd.CanExecute();

            Assert.IsFalse(canExecute, "The " + openPlanCmd.FriendlyName + " button should be enabled.");
            Assert.AreEqual(openPlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection);
        }

        /// <summary>
        /// Test that when a row in the data grid with a plan assignment is selected that the "Open Planogram" button becomes enabled
        /// </summary>
        [Test]
        public void OpenPlanogramCommand_BecomesEnabled_Test()
        {
            RelayCommand openPlanCmd = this.TestModel.OpenPlanogramCommand;

            bool canSelect = openPlanCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + openPlanCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(openPlanCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection);

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that has a plan assignment setup
            var assignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(assignedRowItem);

            // And check the Unassign command is now enabled
            bool canExecute = openPlanCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + openPlanCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(openPlanCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Check that the "Select Cluster Scheme" button is enabled
        /// </summary>
        [Test]
        public void ClusterSchemeSelectCommand_IsEnabled_Test()
        {
            RelayCommand clusterSchemeSelectCmd = this.TestModel.ClusterSchemeSelectCommand;

            // Check the command is registered
            Assert.Contains(clusterSchemeSelectCmd, this.TestModel.ViewModelCommands);

            bool canSelect = clusterSchemeSelectCmd.CanExecute();

            Assert.IsTrue(canSelect, "The " + clusterSchemeSelectCmd.FriendlyName + " should always be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(clusterSchemeSelectCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Check that by default the "Publish" button is disabled
        /// </summary>
        [Test]
        public void PublishCommand_StartsDisabled_Test()
        {
            RelayCommand publishCmd = this.TestModel.PublishPlanogramsCommand;

            // Check the command is registered
            Assert.Contains(publishCmd, this.TestModel.ViewModelCommands);

            bool canSelect = publishCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + publishCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(publishCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_Publish_NoAssignedPlanograms);
        }

        /// <summary>
        /// Test that when a location is specified with plan assignment data, that the Publish button becomes enabled
        /// </summary>
        [Test]
        public void PublishCommand_BecomesEnabled_Test()
        {
            RelayCommand publishCmd = this.TestModel.PublishPlanogramsCommand;

            bool canSelect = publishCmd.CanExecute();

            Assert.IsFalse(canSelect, "The " + publishCmd.FriendlyName + " should not initially be enabled.");
            Assert.AreEqual(publishCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_Publish_NoAssignedPlanograms);

            // Set the view to "view by location"
            TestModel.ViewByLocationCommand.Execute();

            // Set a selected product group
            TestModel.SelectedLocation = _selectedLocation;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0, 
                    "There is no plan assignment data for the selected location '" + _selectedLocation.Name + "'");

            // Check there are some rows in the MasterAssignmentItemRows collection that have plan assignment data
            var planAssignDataList = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).ToList();
            Assert.IsTrue(planAssignDataList.Any());

            // And check the Unassign command is now enabled
            bool canExecute = publishCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + publishCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(publishCmd.DisabledReason), "There should be no disabled reason.");
        }

        /// <summary>
        /// Test that when a database exception occurs (which we cannot easily simulate here) that following 
        /// buttons become disabled:
        ///     Assign, Unassign, Publish, Auto-Match and "Select Cluster Scheme"
        /// </summary>
        [Test]
        public void CommandsDisabledAfterException_Test()
        {
            RelayCommand assignCmd = TestModel.AssignPlanogramsCommand;
            RelayCommand removeCmd = TestModel.RemovePlanogramCommand;
            RelayCommand removeAllCmd = TestModel.RemoveAllPlanogramsCommand;
            RelayCommand publishCmd = TestModel.PublishPlanogramsCommand;
            RelayCommand autoMatchCmd = TestModel.AutoMatchCommand;
            RelayCommand selectClusterSchemeCmd = TestModel.ClusterSchemeSelectCommand;

            // Setup data so that the buttons are initially enabled
            // Set the view to "view by location"
            TestModel.ViewByLocationCommand.Execute();

            // Set a selected product group
            TestModel.SelectedLocation = _selectedLocation;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0,
                    "There is no plan assignment data for the selected location '" + _selectedLocation.Name + "'");

            // Configure a selected row that has a plan assignment so all the buttons are enabled
            var assignedRowItem = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo != null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(assignedRowItem);

            // And check the commands are now enabled
            Assert.IsTrue(assignCmd.CanExecute(), "The " + assignCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(removeCmd.CanExecute(), "The " + removeCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(removeAllCmd.CanExecute(), "The " + removeAllCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(publishCmd.CanExecute(), "The " + publishCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(autoMatchCmd.CanExecute(), "The " + autoMatchCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(selectClusterSchemeCmd.CanExecute(), "The " + selectClusterSchemeCmd.FriendlyName + " button should be enabled.");

            // simulate an exception and disable the commands
            TestModel.DisableCommands = true;

            // Check that the commands are disabled and the disabled message is the database timeout warning
            Assert.IsFalse(assignCmd.CanExecute(), "The " + assignCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(assignCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);

            Assert.IsFalse(removeCmd.CanExecute(), "The " + removeCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(removeCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);

            Assert.IsFalse(removeAllCmd.CanExecute(), "The " + removeAllCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(removeAllCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);

            Assert.IsFalse(publishCmd.CanExecute(), "The " + publishCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(publishCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);

            Assert.IsFalse(autoMatchCmd.CanExecute(), "The " + autoMatchCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(autoMatchCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);

            Assert.IsFalse(selectClusterSchemeCmd.CanExecute(), "The " + selectClusterSchemeCmd.FriendlyName + " button should be disabled.");
            Assert.AreEqual(selectClusterSchemeCmd.DisabledReason,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_DatabaseTimedOut);
        }

        ///// <summary>
        ///// Test that when a plan is selected that matches the width / bay count of the store space it is assigned
        ///// that the header is "Information" (rather than "Planogram / Location Mismatch").
        ///// </summary>
        //[Test]
        //public void AssignPlan_NoWarnings_Test()
        //{
        //    Assert.Ignore("This test needs to be reviewed.");
        //    // ToDo: This is currently broken as the bay count / size does not match - need to fix location space data in Setup to match
        //    RelayCommand assignCmd = this.TestModel.AssignPlanogramsCommand;

        //    // Test it starts disabled 
        //    bool canSelect = assignCmd.CanExecute();
        //    Assert.IsFalse(canSelect, "The " + assignCmd.FriendlyName + " should not initially be enabled.");

        //    // Set a selected product group
        //    TestModel.SelectedProductGroup = _selectedProductGroup2Info;

        //    // Check there are some rows in the MasterAssignmentItemRows collection
        //    Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

        //    // Select a row that has no plan assignment and a single bay
        //    var singleBayList = TestModel.MasterAssignmentItemRows.Where(x => x.StoreSpaceBayCount == 1).ToList();
        //    var noPlanAssignRow = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo == null).FirstOrDefault();
        //    TestModel.PlanAssignmentSelectedItems.Add(noPlanAssignRow);

        //    Assert.IsTrue(noPlanAssignRow.PlanogramInfo == null);

        //    // And check the Assign command is now enabled
        //    bool canExecute = assignCmd.CanExecute();

        //    Assert.IsTrue(canExecute, "The " + assignCmd.FriendlyName + " button should be enabled.");
        //    Assert.IsTrue(String.IsNullOrEmpty(assignCmd.DisabledReason), "There should be no disabled reason.");

        //    // Now go about selecting a plan
        //    var selectVm = InitialiseViewmodel(_selectedPlanGroup, null);

        //    Assert.IsTrue(selectVm.AvailablePlanogramInfos != null);

        //    selectVm.SelectedAvailablePlans.Add(selectVm.AvailablePlanogramInfos.First());
        //    selectVm.AddSelectedCommand.Execute();

        //    // Check the Ok button is enabled
        //    Assert.IsTrue(selectVm.OKCommand.CanExecute());
        //    selectVm.OKCommand.Execute();

        //    WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
        //    WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
        //    WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
        //    WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);

        //    this.TestModel.DoPlanAssignment(selectVm.AssignedPlans);

        //    Debug.WriteLine("MessageDisplayTitle: " + this.TestModel.MessageDisplayTitle);
        //    Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayTitle));

        //    Debug.WriteLine("MessageDisplayHeader: " + this.TestModel.MessageDisplayHeader);
        //    Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayHeader));
        //    Assert.IsTrue(this.TestModel.MessageDisplayHeader.Equals(Message.PlanAssignment_Information_Header));

        //    Debug.WriteLine("MessageDisplayText: " + this.TestModel.MessageDisplayText);
        //    Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayText));

        //    // As the bay mismatch warning starts with "{0} {1} ", strip this off
        //    var baySizeMismatchString = Message.PlanAssignment_PlanBayWarning_SizeMismatch.Substring(8,
        //                Message.PlanAssignment_PlanBayWarning_SizeMismatch.Length - 8);

        //    Assert.IsFalse(this.TestModel.MessageDisplayText.Contains(baySizeMismatchString),
        //                "Warning message should not contain '" + baySizeMismatchString + "'");

        //    // As the product category warning starts with "{0} ", strip this off
        //    var productCategoryMismatchString = Message.PlanAssignment_CategoryMismatchWarning_Content.Substring(4,
        //                Message.PlanAssignment_CategoryMismatchWarning_Content.Length - 4);

        //    Assert.IsFalse(this.TestModel.MessageDisplayText.Contains(productCategoryMismatchString),
        //                "Warning message should not contain '" + productCategoryMismatchString + "'");

        //    var assignData = this.TestModel.PlanAssignmentSelectedItems.FirstOrDefault();

        //    Assert.IsTrue(assignData.PlanogramInfo != null);
        //}

        /// <summary>
        /// Test that when a plan is selected that does not match the width / bay count of the store space it is assigned
        /// that an appropriate error is raised.
        /// </summary>
        [Test]
        public void AssignPlan_BaySizeMismatch_Test()
        {
            RelayCommand assignCmd = this.TestModel.AssignPlanogramsCommand;

            // Test it starts disabled 
            bool canSelect = assignCmd.CanExecute();
            Assert.IsFalse(canSelect, "The " + assignCmd.FriendlyName + " should not initially be enabled.");

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select a row that has no plan assignment
            var noPlanAssignRow = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo == null).FirstOrDefault();
            TestModel.PlanAssignmentSelectedItems.Add(noPlanAssignRow);

            Assert.IsTrue(noPlanAssignRow.PlanogramInfo == null);

            // And check the Assign command is now enabled
            bool canExecute = assignCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + assignCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(assignCmd.DisabledReason), "There should be no disabled reason.");

            // Now go about selecting a plan
            var selectVm = InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(selectVm.AvailablePlanogramInfos != null);

            selectVm.SelectedAvailablePlans.Add(selectVm.AvailablePlanogramInfos.First());
            selectVm.AddSelectedCommand.Execute();

            // Check the Ok button is enabled
            Assert.IsTrue(selectVm.OKCommand.CanExecute());
            selectVm.OKCommand.Execute();

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);

            this.TestModel.DoPlanAssignment(selectVm.AssignedPlans);

            Debug.WriteLine("MessageDisplayTitle: " + this.TestModel.MessageDisplayTitle);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayTitle));

            Debug.WriteLine("MessageDisplayHeader: " + this.TestModel.MessageDisplayHeader);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayHeader));
            Assert.IsTrue(this.TestModel.MessageDisplayHeader.Equals(Message.PlanAssignment_PlanBayWarning_Header));

            Debug.WriteLine("MessageDisplayText: " + this.TestModel.MessageDisplayText);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayText));

            // As the warning starts with "{0} {1} ", strip this off
            var matchString = Message.PlanAssignment_PlanBayWarning_SizeMismatch.Substring(8,
                        Message.PlanAssignment_PlanBayWarning_SizeMismatch.Length - 8);

            Assert.IsTrue(this.TestModel.MessageDisplayText.Contains(matchString),
                        "Warning message does not contain '" + matchString + "'");

            var assignData = this.TestModel.PlanAssignmentSelectedItems.FirstOrDefault();

            Assert.IsTrue(assignData.PlanogramInfo != null);
        }

        /// <summary>
        /// Test that when a plan is selected that does not match product group of the store space it is assigned
        /// that an appropriate error is raised.
        /// </summary>
        [Test]
        public void AssignPlan_ProductGroupMismatch_Test()
        {
            RelayCommand assignCmd = this.TestModel.AssignPlanogramsCommand;

            // Test it starts disabled 
            bool canSelect = assignCmd.CanExecute();
            Assert.IsFalse(canSelect, "The " + assignCmd.FriendlyName + " should not initially be enabled.");

            // Set a selected product group
            TestModel.SelectedProductGroup = _selectedProductGroup2Info;

            // Check there are some rows in the MasterAssignmentItemRows collection
            Assert.IsTrue(TestModel.MasterAssignmentItemRows != null && TestModel.MasterAssignmentItemRows.Count() > 0);

            // Select the rows that have no plan assignments
            var noPlanAssignRowsList = TestModel.MasterAssignmentItemRows.Where(x => x.PlanogramInfo == null).ToList();
            Assert.IsTrue(noPlanAssignRowsList.Count() > 1);

            // And take the 2nd item
            var noPlanAssignRow = noPlanAssignRowsList[1];
            TestModel.PlanAssignmentSelectedItems.Add(noPlanAssignRow);

            Assert.IsTrue(noPlanAssignRow.PlanogramInfo == null);

            // And check the Assign command is now enabled
            bool canExecute = assignCmd.CanExecute();

            Assert.IsTrue(canExecute, "The " + assignCmd.FriendlyName + " button should be enabled.");
            Assert.IsTrue(String.IsNullOrEmpty(assignCmd.DisabledReason), "There should be no disabled reason.");

            // Now go about selecting a plan
            var selectVm = InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(selectVm.AvailablePlanogramInfos != null && selectVm.AvailablePlanogramInfos.Count() > 1);

            selectVm.SelectedAvailablePlans.Add(selectVm.AvailablePlanogramInfos[1]);
            selectVm.AddSelectedCommand.Execute();

            // Check the Ok button is enabled
            Assert.IsTrue(selectVm.OKCommand.CanExecute());
            selectVm.OKCommand.Execute();

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);

            this.TestModel.DoPlanAssignment(selectVm.AssignedPlans);

            Debug.WriteLine("MessageDisplayTitle: " + this.TestModel.MessageDisplayTitle);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayTitle));

            Debug.WriteLine("MessageDisplayHeader: " + this.TestModel.MessageDisplayHeader);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayHeader));
            Assert.IsTrue(this.TestModel.MessageDisplayHeader.Equals(Message.PlanAssignment_PlanBayWarning_Header));

            Debug.WriteLine("MessageDisplayText: " + this.TestModel.MessageDisplayText);
            Assert.IsFalse(String.IsNullOrEmpty(this.TestModel.MessageDisplayText));

            // As the category mismatch warning starts with "{0}" strip this off
            var matchString = Message.PlanAssignment_CategoryMismatchWarning_Content.Substring(4, 
                        Message.PlanAssignment_CategoryMismatchWarning_Content.Length-4);

            Assert.IsTrue(this.TestModel.MessageDisplayText.Contains(matchString),
                        "Warning message does not contain '" + matchString + "'");

            var assignData = this.TestModel.PlanAssignmentSelectedItems.FirstOrDefault();

            Assert.IsTrue(assignData.PlanogramInfo != null);
        }

        #endregion

        #region Correct Cluster Scheme is selected when selecting store or category

        [Test]
        public void SelectsDefaultClusterSchemeWhenStoreIsSelectedAndNoCurrentOneIsSelected()
        {
            const String expectation =
                "Whenever a store is selected by the user and there is no currently selected one, the currently selected cluster scheme should be set to the default one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetDefault(_clusterSchemesInfoList.First().Id);
            SetMostRecent(_clusterSchemesInfoList.First(info => info.IsDefault == false).Id);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId != null || info.IsDefault == false))
                Assert.Inconclusive("There should be at least one cluster scheme that is default generic for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.IsDefault).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedLocation = _selectedLocation;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a store is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectsMostRecentClusterSchemeWhenStoreIsSelectedAndNoCurrentOneIsSelectedIfNoDefault()
        {
            const String expectation =
                "Whenever a store is selected by the user and there is no currently selected one, the currently selected cluster scheme should be set to the most recent one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId != null))
                Assert.Inconclusive("There should be at least one cluster scheme that is generic for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.ProductGroupId == null).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedLocation = _selectedLocation;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a store is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectsMostRecentClusterSchemeWhenStoreIsSelectedAndCurrentOneIsNotGeneric()
        {
            const String expectation =
                "Whenever a store is selected by the user and the currently selected is not generic, the currently selected cluster scheme should be set to the most recent one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId != null))
                Assert.Inconclusive("There should be at least one cluster scheme that is generic for this test.");
            TestModel.SelectedClusterScheme = _clusterSchemesInfoList.FirstOrDefault(info => info.ProductGroupId != null);
            if (TestModel.SelectedClusterScheme == null ||
                TestModel.SelectedClusterScheme.ProductGroupId == null)
                Assert.Inconclusive("There should be a selected category specific cluster scheme for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.ProductGroupId == null).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedLocation = _selectedLocation;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a store is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectsMostRecentSpecificClusterSchemeWhenCategoryIsSelectedAndNoCurrentOneIsSelected()
        {
            const String expectation =
                "Whenever a category is selected by the user and there is no currently selected one, the currently selected cluster scheme should be set to the most recent specific one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId == null))
                Assert.Inconclusive("There should be at least one cluster scheme that is specific for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.ProductGroupId != null).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a category is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectsMostRecentSpecificClusterSchemeWhenCategoryIsSelectedAndCurrentOneIsGeneric()
        {
            const String expectation =
                "Whenever a category is selected by the user and there is currently selected a generic one, the currently selected cluster scheme should be set to the most recent specific one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId == null))
                Assert.Inconclusive("There should be at least one cluster scheme that is specific for this test.");
            TestModel.SelectedClusterScheme = _clusterSchemesInfoList.FirstOrDefault(info => info.ProductGroupId == null);
            if (TestModel.SelectedClusterScheme == null ||
                TestModel.SelectedClusterScheme.ProductGroupId != null)
                Assert.Inconclusive("There should be a selected non category specific cluster scheme for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.ProductGroupId != null).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a category is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectsMostRecentSpecificClusterSchemeWhenCategoryIsSelectedAndCurrentOneIsSpecificToAnother()
        {
            const String expectation =
                "Whenever a category is selected by the user and there is currently selected a specific one for another category, the currently selected cluster scheme should be set to the most recent specific one.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            SetSpecificClusterScheme(_clusterSchemesInfoList.First(info => info.ProductGroupId == null).Id, _selectedProductGroup2Info);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId == null))
                Assert.Inconclusive("There should be at least one cluster scheme that is specific for this test.");
            TestModel.SelectedClusterScheme = _clusterSchemesInfoList.FirstOrDefault(info => info.ProductGroupId == _selectedProductGroup2Info.Id);
            if (TestModel.SelectedClusterScheme == null ||
                TestModel.SelectedClusterScheme.ProductGroupId != _selectedProductGroup2Info.Id)
                Assert.Inconclusive("There should be a different selected category specific cluster scheme for this test.");
            String expected =
                _clusterSchemesInfoList.Where(info => info.ProductGroupId == _selectedProductGroup1Info.Id).OrderByDescending(info => info.DateLastModified).First().Name;

            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a category is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectionRemainsWhenSelectingStoreEvenIfCurrentlyGenericSchemeIsNotMostRecent()
        {
            const String expectation =
                "Whenever a store is selected by the user and there is currently selected a non recent generic scheme, the currently selected cluster scheme should remain selected.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            SetSpecificClusterScheme(_clusterSchemesInfoList.First(info => info.ProductGroupId == null).Id, _selectedProductGroup2Info);
            SetMostRecent(_clusterSchemesInfoList.First(info => info.ProductGroupId == null).Id);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId == null))
                Assert.Inconclusive("There should be at least one cluster scheme that is specific for this test.");
            TestModel.SelectedClusterScheme = _clusterSchemesInfoList.OrderBy(info => info.DateLastModified).FirstOrDefault(info => info.ProductGroupId == null);
            if (TestModel.SelectedClusterScheme == null ||
                TestModel.SelectedClusterScheme.ProductGroupId != null)
                Assert.Inconclusive("There should be a different selected non category specific cluster scheme for this test.");
            String expected = TestModel.SelectedClusterScheme.Name;

            TestModel.SelectedLocation = _selectedLocation;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There shouldhave been a cluster selected after a store is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        [Test]
        public void SelectionRemainsWhenSelectingCategoryEvenIfCurrentlySpecificIsNotMostRecent()
        {
            const String expectation =
                "Whenever a category is selected by the user and there is currently selected a specific one for the category, the currently selected cluster scheme should stay even if not the most recent.";
            //  Assign a new store.
            if (_clusterSchemesInfoList == null ||
                _clusterSchemesInfoList.Count < 2) Assert.Inconclusive("There should be at least two cluster schemes available for this test.");
            SetSpecificClusterScheme(_clusterSchemesInfoList.First().Id, _selectedProductGroup1Info);
            SetSpecificClusterScheme(_clusterSchemesInfoList.First(info => info.ProductGroupId == null).Id, _selectedProductGroup1Info);
            if (_clusterSchemesInfoList.All(info => info.ProductGroupId == null))
                Assert.Inconclusive("There should be at least one cluster scheme that is specific for this test.");
            TestModel.SelectedClusterScheme = _clusterSchemesInfoList.OrderBy(info => info.DateLastModified).FirstOrDefault(info => info.ProductGroupId == _selectedProductGroup1Info.Id);
            if (TestModel.SelectedClusterScheme == null ||
                TestModel.SelectedClusterScheme.ProductGroupId == null)
                Assert.Inconclusive("There should be selected a category specific cluster scheme for this test.");
            String expected = TestModel.SelectedClusterScheme.Name;

            TestModel.SelectedProductGroup = _selectedProductGroup1Info;

            // Check whether the currently selected cluster scheme is the right one.
            ClusterSchemeInfo selectedClusterScheme = TestModel.SelectedClusterScheme;
            Assert.IsNotNull(selectedClusterScheme, "There should have been a cluster selected after a category is selected.");
            Assert.AreEqual(expected, selectedClusterScheme.Name, expectation);
        }

        #endregion

        #region Helpers

        private SelectPlanogramViewModel InitialiseViewmodel(PlanogramGroup selectedPlanogramGroup, IEnumerable<PlanAssignmentRowViewModel> initalPlansAssigned)
        {
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);

            PlanogramHierarchyViewModel hierarchyView = new PlanogramHierarchyViewModel();
            hierarchyView.FetchForCurrentEntity();

            Assert.AreEqual(3, hierarchyView.RootGroup.ChildViews.Count(), "Planogram groups incorrect.");

            var localSelectVm = new SelectPlanogramViewModel("", base.EntityId, hierarchyView, selectedPlanogramGroup, initalPlansAssigned);

            return localSelectVm;
        }

        private void SetSpecificClusterScheme(Int32 clusterSchemeId, ProductGroupInfo productGroupInfo)
        {
            ClusterScheme specificClusterScheme = ClusterScheme.FetchById(clusterSchemeId);
            specificClusterScheme.ProductGroupId = productGroupInfo.Id;
            specificClusterScheme.Save(true);

            //  Update the list of schemes.
            _clusterSchemesInfoList = ClusterSchemeInfoList.FetchByEntityId(EntityId);
        }
        
        private void SetDefault(Int32 clusterSchemeId)
        {
            ClusterScheme specificClusterScheme = ClusterScheme.FetchById(clusterSchemeId);
            specificClusterScheme.Name += "(Default)";
            specificClusterScheme.IsDefault = true;
            specificClusterScheme.Save(true);

            //  Update the list of schemes.
            _clusterSchemesInfoList = ClusterSchemeInfoList.FetchByEntityId(EntityId);
        }

        private void SetMostRecent(Int32 clusterSchemeId)
        {
            ClusterScheme specificClusterScheme = ClusterScheme.FetchById(clusterSchemeId);
            specificClusterScheme.Name += "(Most Recent)";
            specificClusterScheme.Save(true);

            //  Update the list of schemes.
            _clusterSchemesInfoList = ClusterSchemeInfoList.FetchByEntityId(EntityId);
        }

        #endregion
    }
}
