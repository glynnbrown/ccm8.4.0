﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-29598 : L.Luong
//  Created
#endregion

#region Version History : CCM830

// V8-31917 : A.Silva
//  Refactored to eliminate duplicate code and to clarify intention.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.PlanAssignmentAutoMatch;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanAssignment.PlanAssignmentAutoMatch
{
    [TestFixture]
    public class PlanAssignmentAutoMatchViewModelTests : TestBase<PlanAssignmentAutoMatchViewModel>
    {
        #region Constants

        private const Boolean UseClusterSchemes = true;
        private const PlanAssignmentViewType ByCategory = PlanAssignmentViewType.ViewCategory;
        private const PlanAssignmentViewType ByLocation = PlanAssignmentViewType.ViewLocation;
        private const Boolean IsStoreSpecific = true;
        private const MatchType ExactMatch = MatchType.ExactMatch;
        private const MatchType ClosestAbove = MatchType.ClosestAbove;
        private const MatchType ClosestBelow = MatchType.ClosestBelow;
        private const String PlanNameMask = "Plan{0}";
        private const Int32 ExactBayCount = 3;
        private const Single ExactHeight = 4.1F;
        private const Int32 AboveBayCount = 4;
        private const Single AboveHeight = 5F;
        private const Single AboveWidth = 13F;
        private const Int32 StartingPlanCount = 20;
        private const Int32 BelowBayCount = 2;
        private const Single BelowHeight = 3F;
        private const Single BelowWidth = 11F;

        #endregion

        #region Fields

        private PlanAssignmentAutoMatchViewModel _viewModel;
        private Int32 _entityId;
        private ProductGroupInfo _defaultProductGroup;
        private LocationInfo _defaultLocation;
        private Single _exactWidth;
        private PlanogramGroup _planogramGroup;
        private PlanogramHierarchy _planogramHierarchy;
        private readonly List<Int32> _planIds = new List<Int32>();
        private String _clusterSchemeName;
        private String _clusterName;

        private MatchParams ExactMatchParams { get { return new MatchParams(ExactBayCount, ExactHeight, _exactWidth); } }

        private MatchParams AboveMatchParams { get { return new MatchParams(AboveBayCount, AboveHeight, AboveWidth); } }

        private MatchParams BelowMatchParams { get { return new MatchParams(BelowBayCount, BelowHeight, BelowWidth); } }

        #endregion

        #region TestHelpers

        [SetUp]
        public void SetUp()
        {
            _entityId = EntityInfoList.FetchAllEntityInfos().First().Id;

            List<ProductGroupDto> productGroupDtos = SetUpProductGroups(out _defaultProductGroup);
            List<LocationDto> locationDtos = SetUpLocations(productGroupDtos, out _defaultLocation, out _exactWidth);
            List<ClusterDto> clusterDtos;
            List<ClusterSchemeDto> clusterSchemeDtos = SetUpClusterSchemes(locationDtos, out clusterDtos, out _clusterSchemeName, out _clusterName);
            SetClusterProductGroup(clusterSchemeDtos, productGroupDtos);
            _planogramHierarchy = TestDataHelper.PopulatePlanogramHierarchy(_entityId);
            AssociatePlansToGroup(SetUpMatchingPlans(locationDtos, clusterSchemeDtos, clusterDtos));

            _viewModel = new PlanAssignmentAutoMatchViewModel();
            MockDialogWindows();
        }

        private void AssociatePlansToGroup(IEnumerable<Int32> matchingPlanIds)
        {
            _planogramGroup = _planogramHierarchy.EnumerateAllGroups().Skip(1).First();
            _planogramGroup.AssociatePlanograms(PlanogramInfoList.FetchByIds(matchingPlanIds));
            _planogramHierarchy.Save();
        }

        private void MockDialogWindows()
        {
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);
        }

        private static void SetClusterProductGroup(IEnumerable<ClusterSchemeDto> clusterSchemeDtos, IEnumerable<ProductGroupDto> productGroupDtos)
        {
            ClusterScheme clusterScheme = ClusterScheme.FetchById(clusterSchemeDtos.Skip(2).First().Id);
            clusterScheme.ProductGroupId = productGroupDtos.Skip(1).First().Id;
            clusterScheme.Save();
        }

        private IEnumerable<Int32> SetUpMatchingPlans(ICollection<LocationDto> locationDtos, ICollection<ClusterSchemeDto> clusterSchemeDtos, ICollection<ClusterDto> clusterDtos)
        {
            var planIds = new List<Int32>();
            for (var id = 0; id < StartingPlanCount; id++)
            {
                using (Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User))
                {

                    Planogram plan;
                    if (!TryCreateTestPlan(id, locationDtos, clusterSchemeDtos, clusterDtos, out plan)) continue;
                    package.Name = plan.Name;
                    package.Planograms.Add(plan);

                    planIds.Add((Int32) package.Save().Id);
                }
            }
            return planIds;
        }

        private Boolean TryCreateTestPlan(Int32 id, ICollection<LocationDto> locationDtos, ICollection<ClusterSchemeDto> clusterSchemeDtos, ICollection<ClusterDto> clusterDtos, out Planogram testPlanogram)
        {
            testPlanogram = null;

            Int32 locationId = id % 10;
            if (locationDtos.Count <= locationId) return false;
            Int32 clusterSchemeId = id % 3;
            if (clusterSchemeDtos.Count <= clusterSchemeId) return false;
            Int32 clusterId = id % 15;
            if (clusterDtos.Count <= clusterId) return false;

            testPlanogram = TestDataHelper.CreatePlanCreweSparklingWater().Planograms.First();
            LocationDto locationDto = locationDtos.Skip(locationId).First();
            testPlanogram.LocationCode = locationDto.Code;
            testPlanogram.LocationName = locationDto.Name;
            testPlanogram.ClusterSchemeName = clusterSchemeDtos.Skip(clusterSchemeId).First().Name;
            testPlanogram.ClusterName = clusterDtos.Skip(clusterId).First().Name;
            testPlanogram.CategoryCode = _defaultProductGroup.Code;
            testPlanogram.CategoryName = _defaultProductGroup.Name;
            testPlanogram.Name = String.Format(PlanNameMask, id);
            return true;
        }

        private List<ClusterSchemeDto> SetUpClusterSchemes(IEnumerable<LocationDto> locationDtos, out List<ClusterDto> clusterDtos, out String clusterSchemeName, out String clusterName)
        {
            List<ClusterSchemeDto> clusterSchemeDtos = TestDataHelper.InsertClusterSchemeDtos(DalFactory, _entityId, 5);
            clusterDtos = TestDataHelper.InsertClusterDtos(DalFactory, clusterSchemeDtos, 5);
            TestDataHelper.InsertClusterLocationDtos(DalFactory, clusterDtos, locationDtos);
            clusterSchemeName = clusterSchemeDtos.First().Name;
            clusterName = clusterDtos.First().Name;
            return clusterSchemeDtos;
        }

        private List<LocationDto> SetUpLocations(ICollection<ProductGroupDto> productGroupDtos, out LocationInfo defaultLocation, out Single totalWidth)
        {
            TestDataHelper.PopulateLocationHierarchy(DalFactory, _entityId);
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, _entityId, productGroupDtos.Select(o => o.Id).ToList(), 5);
            List<LocationSpaceDto> locationSpaceDtos = TestDataHelper.InsertLocationSpaceDtos(DalFactory, locationDtos, _entityId);
            totalWidth = 0;
            IEnumerable<LocationSpaceProductGroupDto> locationSpaceProductGroupDtos =
                locationSpaceDtos.Select(dto => TestDataHelper.InsertLocationSpaceProductGroupDtos(DalFactory, dto.Id, productGroupDtos))
                                 .SelectMany(dtos => dtos);
            foreach (LocationSpaceProductGroupDto dto in locationSpaceProductGroupDtos)
            {
                totalWidth = TestDataHelper.InsertLocationSpaceBayDtos(DalFactory, dto.Id, 3).Sum(l => l.Width);
            }
            Int16 defaultLocationId = locationDtos[0].Id;
            defaultLocation = LocationInfoList.FetchByEntityId(_entityId).FirstOrDefault(l => l.Id == defaultLocationId);
            return locationDtos;
        }

        private List<ProductGroupDto> SetUpProductGroups(out ProductGroupInfo defaultProductGroup)
        {
            Int32 hierarchyId = ProductHierarchy.FetchByEntityId(_entityId).Id;
            List<ProductLevelDto> productLevelDtos = TestDataHelper.InsertProductLevelDtos(DalFactory, hierarchyId, 2);
            List<ProductGroupDto> productGroupDtos = TestDataHelper.InsertProductGroupDtos(DalFactory, productLevelDtos, 1);
            defaultProductGroup = ProductGroupInfo.FetchById(productGroupDtos.First().Id);
            return productGroupDtos;
        }

        private void AssertAssignedPlansCountEquals(Int32 expected)
        {
            const String expectation = "There should have been {0} assigned plans, but {1} were assigned instead.";
            Int32 actual = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(_viewModel.EntityId, _defaultProductGroup.Id).Count;
            Assert.AreEqual(expected, actual, String.Format(expectation, expected, actual));
        }

        private void SetAssigmentAutoMatchSettings(PlanAssignmentViewType planAssignmentViewType, Boolean isStoreSpecific = !IsStoreSpecific, Boolean useClusterSchemes = !UseClusterSchemes, MatchType matchType = ExactMatch)
        {
            _viewModel.EntityId = _entityId;
            if (planAssignmentViewType == ByCategory) _viewModel.ProductGroup = _defaultProductGroup;
            if (planAssignmentViewType == ByLocation) _viewModel.Location = _defaultLocation;
            _viewModel.SelectedClusterScheme = ClusterSchemeInfoList.FetchByEntityId(_entityId).First();
            _viewModel.SelectedPlanogramGroup = _planogramGroup;
            _viewModel.UseClusterSchemes = useClusterSchemes;
            _viewModel.IsStoreSpecific = isStoreSpecific;
            _viewModel.StatusType = PlanogramStatusType.None;
            _viewModel.ViewType = planAssignmentViewType;
            foreach (AutoMatchRowView row in _viewModel.AvailableAutoMatchRows)
            {
                row.MatchType = matchType;
            }
        }

        #endregion

        #region Plan Assignment Auto Match By Category

        [Test]
        public void AssignsPlansWhenByCategoryAndStoreSpecific()
        {
            SetAssigmentAutoMatchSettings(ByCategory, IsStoreSpecific);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(2);
        }

        [Test]
        public void AssignsPlansWhenByCategoryAndUseClusterSchemes()
        {
            SetAssigmentAutoMatchSettings(ByCategory, IsStoreSpecific, UseClusterSchemes);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(2);
        }

        [Test]
        public void AssignsPlansWhenByCategoryAndExactMatch()
        {
            SetAssigmentAutoMatchSettings(ByCategory);
            CreatePlanogramFor(ExactMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(2);
        }

        [Test]
        public void AssignsPlansWhenByCategoryAndClosestAbove()
        {
            SetAssigmentAutoMatchSettings(ByCategory, matchType: ClosestAbove);
            CreatePlanogramFor(AboveMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(2);
        }

        [Test]
        public void AssignsPlansWhenByCategoryAndClosestBelow()
        {
            SetAssigmentAutoMatchSettings(ByCategory, matchType: ClosestBelow);
            CreatePlanogramFor(BelowMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(2);
        }

        #endregion

        #region Plan Assignment Auto Match By Location

        [Test]
        public void AssignsPlansWhenByLocationAndStoreSpecific()
        {
            SetAssigmentAutoMatchSettings(ByLocation, IsStoreSpecific);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(1);
        }

        [Test]
        public void AssignsPlansWhenByLocationAndUseClusterSchemes()
        {
            SetAssigmentAutoMatchSettings(ByLocation, IsStoreSpecific, UseClusterSchemes);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(1);
        }

        [Test]
        public void AssignsPlansWhenByLocationAndExactMatch()
        {
            SetAssigmentAutoMatchSettings(ByLocation);
            CreatePlanogramFor(ExactMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(1);
        }

        [Test]
        public void AssignsPlansWhenByLocationAndClosestAbove()
        {
            SetAssigmentAutoMatchSettings(ByLocation, matchType: ClosestAbove);
            CreatePlanogramFor(AboveMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(1);
        }

        [Test]
        public void AssignsPlansWhenByLocationAndClosestBelow()
        {
            SetAssigmentAutoMatchSettings(ByLocation, matchType: ClosestBelow);
            CreatePlanogramFor(BelowMatchParams);

            _viewModel.OkCommand.Execute();

            AssertAssignedPlansCountEquals(1);
        }

        #endregion

        #region Helper Test Methods

        private class MatchParams
        {
            public MatchParams(Int32 bayCount, Single height, Single width)
            {
                BayCount = bayCount;
                Height = height;
                Width = width;
            }

            public Int32 BayCount { get; private set; }
            public Single Height { get; private set; }
            public Single Width { get; private set; }
        }

        /// <summary>
        /// Creates Planogram which should pass exact match
        /// </summary>
        /// <param name="matchParams"></param>
        private void CreatePlanogramFor(MatchParams matchParams)
        {
            using (Package package = String.Format(PlanNameMask, StartingPlanCount + 1).CreatePackage())
            {
                Planogram planogram = package.AddPlanogram();

                planogram.AddFixtureItems(matchParams.BayCount);

                planogram.Height = matchParams.Height;
                planogram.Width = matchParams.Width;

                planogram.ClusterSchemeName = _clusterSchemeName;
                planogram.ClusterName = _clusterName;
                planogram.CategoryCode = _defaultProductGroup.Code;
                planogram.CategoryName = _defaultProductGroup.Name;

                planogram.CalculateMetadataAndGetValidationWarnings(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(_entityId, package));

                planogram.Name = package.Name;

                _planIds.Add((Int32)package.Save().Id);
            }

            AssociatePlansToGroup(_planIds);
        }

        #endregion
    }
}