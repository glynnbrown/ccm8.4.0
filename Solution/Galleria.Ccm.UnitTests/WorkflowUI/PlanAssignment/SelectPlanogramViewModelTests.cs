﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.2.0
// V8-30732 : M.Shelley
//  Created
#endregion

#region Version History: (CCM 8.3.0)
//V8-32052 : L.Ineson
//  Added support for multiselection.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;


namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanAssignment
{
    [TestFixture]
    public class SelectPlanogramViewModelTests : TestBase<SelectPlanogramViewModel>
    {
        #region Constants

        private const int PlansToInsert = 5;

        #endregion

        #region Fields

        private Int32 _selectedProductGroupId;
        private PlanogramHierarchy _planHierarchy;
        private List<Int32> _planIdList;
        private PlanogramGroup _selectedPlanGroup;
        private ClusterSchemeInfo _clusterScheme;

        #endregion

        #region Setup

        [SetUp]
        public void SetUp()
        {
            List<EntityDto> entityDtos = Helpers.TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            // Setup some planograms
            _planIdList = Helpers.TestDataHelper.InsertPlanograms(PlansToInsert);

            // Setup a planogram group tree
            _planHierarchy = Helpers.TestDataHelper.PopulatePlanogramHierarchy(EntityId);

            var firstChildGroup = _planHierarchy.RootGroup.ChildList.FirstOrDefault();
            _selectedPlanGroup = firstChildGroup.ChildList.FirstOrDefault();
            PlanogramGroup.AssociatePlanograms(_selectedPlanGroup.Id, _planIdList);

            // Helpers.TestDataHelper.InsertProductDtos(base.DalFactory, 5, this.EntityId);

            var heirarachyId = ProductHierarchy.FetchByEntityId(base.EntityId).Id;
            var productLevels = Helpers.TestDataHelper.InsertProductLevelDtos(this.DalFactory, heirarachyId, 2);
            var productGroupDtos = Helpers.TestDataHelper.InsertProductGroupDtos(this.DalFactory, productLevels, 1);

            List<LocationHierarchyDto> locHierarchyDtos = Helpers.TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityDtos);
            List<LocationLevelDto> locLevelsDtos = Helpers.TestDataHelper.InsertLocationLevelDtos(base.DalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = Helpers.TestDataHelper.InsertLocationGroupDtos(base.DalFactory, locLevelsDtos, 2);
            List<LocationDto> locDtos = Helpers.TestDataHelper.InsertLocationDtos(base.DalFactory, base.EntityId, locGroupDtos, 5);
            List<LocationSpaceDto> locationSpaces = Helpers.TestDataHelper.InsertLocationSpaceDtos(this.DalFactory, locDtos, base.EntityId);

            foreach (LocationSpaceDto locationSpace in locationSpaces)
            {
                var locationSpaceProductGroups = Helpers.TestDataHelper.InsertLocationSpaceProductGroupDtos(this.DalFactory, locationSpace.Id, productGroupDtos);
                foreach (LocationSpaceProductGroupDto locationSpaceProductGroup in locationSpaceProductGroups)
                {
                    Helpers.TestDataHelper.InsertLocationSpaceBayDtos(this.DalFactory, locationSpaceProductGroup.Id, 3);
                }
            }

            // set cluster schemes
            List<ClusterSchemeDto> clusterSchemesDtos = Helpers.TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, base.EntityId, 5);
            List<ClusterDto> clustersDtos = Helpers.TestDataHelper.InsertClusterDtos(this.DalFactory, clusterSchemesDtos, 5);
            List<ClusterLocationDto> clusterLocationsDtos = Helpers.TestDataHelper.InsertClusterLocationDtos(this.DalFactory, clustersDtos, locDtos);

            var clusterSchemesInfoList = ClusterSchemeInfoList.FetchByEntityId(base.EntityId);
            _clusterScheme = clusterSchemesInfoList.FirstOrDefault();

            var tmpPlanAssignments = Helpers.TestDataHelper.InsertLocationPlanAssignment(EntityId);

            // Set up a plan assignment for the 1st item in the plan assignment list
            var firstItem = tmpPlanAssignments[0];
            firstItem.PlanogramId = _planIdList.First();
            firstItem.LocationId = locDtos.First().Id;
            _selectedProductGroupId = productGroupDtos.First().Id;
            firstItem.ProductGroupId = _selectedProductGroupId;
            //firstItem.DateAssigned = DateTime.Now;
            firstItem.SetAssignedDate(DateTime.Now);
            firstItem.AssignedByUserId = 1;

            // Set up a plan assignment for the 2nd item in the plan assignment list, using the same plan id and product group id
            var secondItem = tmpPlanAssignments[1];
            secondItem.PlanogramId = _planIdList.First();
            secondItem.LocationId = locDtos.First().Id;
            secondItem.ProductGroupId = _selectedProductGroupId;
            //secondItem.DateAssigned = DateTime.Now;
            secondItem.SetAssignedDate(DateTime.Now);
            secondItem.AssignedByUserId = 1;

            tmpPlanAssignments.Save();
        }

        #endregion

        #region Tests

        [Test]
        public void TestConstructor()
        {
            InitialiseViewmodel(null, null);
        }

        /// <summary>
        /// Test the Ok button is initially disabled
        /// </summary>
        [Test]
        public void OkCommand_IsDisabled()
        {
            InitialiseViewmodel(null, null);

            // Check the Ok button is disabled
            Assert.IsFalse(TestModel.OKCommand.CanExecute());

            // And the disabled reason is "You must select an item."
            Assert.AreEqual(Message.Generic_NoItemSelected, TestModel.OKCommand.DisabledReason);
        }

        /// <summary>
        /// Test that adding a planogram to the selected planograms grid enables the Ok button
        /// </summary>
        [Test]
        public void OkCommand_IsEnabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());
            this.TestModel.AddSelectedCommand.Execute();

            // Check the Ok button is enabled
            Assert.IsTrue(TestModel.OKCommand.CanExecute());

            // And the disabled reason is empty
            Assert.AreEqual(string.Empty, TestModel.OKCommand.DisabledReason);
        }

        /// <summary>
        /// Test that once a plan has been added to the selected plans grid, that removing it disables the Ok button
        /// </summary>
        [Test]
        public void OkCommand_BecomesDisabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());

            Assert.IsTrue(this.TestModel.AssignedPlans.Count > 0);
            this.TestModel.SelectedAssignedPlans.Add(this.TestModel.AssignedPlans.FirstOrDefault());

            this.TestModel.RemoveSelectedCommand.Execute();

            // Check the Ok button is now disabled
            Assert.IsFalse(TestModel.OKCommand.CanExecute());

            // And the disabled reason is "You must select an item."
            Assert.AreEqual(Message.Generic_NoItemSelected, TestModel.OKCommand.DisabledReason);
        }

        /// <summary>
        /// Test the Cancel button is enabled
        /// </summary>
        [Test]
        public void CancelCommand_IsEnabled()
        {
            InitialiseViewmodel(null, null);

            Assert.IsTrue(TestModel.CancelCommand.CanExecute());
        }

        [Test]
        public void AddSelectedCommand_IsDisabled()
        {
            InitialiseViewmodel(null, null);

            // Test the "Add" button is disabled
            Assert.IsFalse(this.TestModel.AddSelectedCommand.CanExecute());

            // And the disabled reason is "You must select an item to add to the selected planograms."
            Assert.AreEqual(Message.PlanAssignment_PlanSelector_AddItemsWarning, TestModel.AddSelectedCommand.DisabledReason);
        }

        [Test]
        public void AddSelectedCommand_IsEnabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());

            // Test the "Add" button is enabled
            Assert.IsTrue(TestModel.AddSelectedCommand.CanExecute());
        }

        /// <summary>
        /// Test the Add command does as it says on the tin
        /// </summary>
        [Test]
        public void AddSelectedCommand()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());

            // Test the "Add" button is enabled
            Assert.IsTrue(TestModel.AddSelectedCommand.CanExecute());

            TestModel.AddSelectedCommand.Execute();

            // Check there are no plans in the selected plans grid
            Assert.AreEqual(1, TestModel.AssignedPlans.Count());
        }

        /// <summary>
        /// Check the Remove button is disabled
        /// </summary>
        [Test]
        public void RemoveSelectedCommand_IsDisabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            // Test the "Remove" button is disabled
            Assert.IsFalse(this.TestModel.RemoveSelectedCommand.CanExecute());

            // And the disabled reason is "You must select an item to remove from the selected planograms."
            Assert.AreEqual(Message.PlanAssignment_PlanSelector_SelectItemRemove, TestModel.RemoveSelectedCommand.DisabledReason);
        }

        /// <summary>
        /// Test the Remove command is enabled
        /// </summary>
        [Test]
        public void RemoveSelectedCommand_IsEnabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            // Add a plan to the seletced grid
            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());
            Assert.IsTrue(this.TestModel.AssignedPlans.Count > 0);

            // select the only plan to remove it
            this.TestModel.SelectedAssignedPlans.Add(this.TestModel.AssignedPlans.FirstOrDefault());

            // Test the "Remove" button is enabled
            Assert.IsTrue(TestModel.RemoveSelectedCommand.CanExecute());
        }

        /// <summary>
        /// Test the remove command actually removes the plan from the selected plans grid
        /// </summary>
        [Test]
        public void RemoveSelectedCommand()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());
            Assert.IsTrue(this.TestModel.AssignedPlans.Count > 0);

            this.TestModel.SelectedAssignedPlans.Add(this.TestModel.AssignedPlans.FirstOrDefault());

            Assert.IsTrue(TestModel.RemoveSelectedCommand.CanExecute());

            TestModel.RemoveSelectedCommand.Execute();

            // Check there are no plans in the selected plans grid
            Assert.AreEqual(0, TestModel.AssignedPlans.Count);
        }

        /// <summary>
        /// Test the RemoveAll command is initially disabled
        /// </summary>
        [Test]
        public void RemoveAllCommand_IsDisabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            Assert.IsFalse(this.TestModel.RemoveAllCommand.CanExecute());
        }

        /// <summary>
        /// Test the RemoveAll command becomes enabled
        /// </summary>
        [Test]
        public void RemoveAllCommand_IsEnabled()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos.First());
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());
            Assert.IsTrue(this.TestModel.AssignedPlans.Count > 0);

            Assert.IsTrue(TestModel.RemoveAllCommand.CanExecute());
        }

        /// <summary>
        /// Test the RemoveAll command does indeed remove all the items
        /// </summary>
        [Test]
        public void RemoveAllCommand()
        {
            InitialiseViewmodel(_selectedPlanGroup, null);

            Assert.IsTrue(this.TestModel.AvailablePlanogramInfos != null);
            Assert.AreEqual(PlansToInsert, this.TestModel.AvailablePlanogramInfos.Count());

            // Add 1 plan
            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos[0]);
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());
            Assert.AreEqual(1, this.TestModel.AssignedPlans.Count());

            // Add another different plan
            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos[1]);
            this.TestModel.AddSelectedCommand.Execute();

            Assert.IsTrue(TestModel.OKCommand.CanExecute());

            // Check there are now 2 added plans
            Assert.AreEqual(2, this.TestModel.AssignedPlans.Count());

            Assert.IsTrue(TestModel.RemoveAllCommand.CanExecute());

            TestModel.RemoveAllCommand.Execute();

            // Check that all the added plans have been removed
            Assert.AreEqual(0, TestModel.AssignedPlans.Count());
        }

        /// <summary>
        /// Test that when a plan assignment is selected, that the selector window adds the plan
        /// to the "selected plans" grid
        /// </summary>
        [Test]
        public void Initial_PlanAssignedSelection()
        {
            // Set up a list of rows that a user has selected that contain a previously plan assignment
            var mainPlanModel = new PlanAssignmentViewModel();
            mainPlanModel.SelectedClusterScheme = _clusterScheme;
            mainPlanModel.SelectedProductGroup = ProductGroupInfo.FetchById(_selectedProductGroupId);

            // Get a list of selected plan assignments (in this case only 1 row)
            var tmpList = mainPlanModel.MasterAssignmentItemRows.Where(x => !String.IsNullOrEmpty(x.PlanName)).ToList();

            // Pass this to the constructor
            InitialiseViewmodel(_selectedPlanGroup, tmpList);

            // And check that the assigned plan already assigned is in the "selected" list of planograms
            Assert.AreEqual(1, this.TestModel.AssignedPlans.Count(), "The initial number of selected plans should be 1");
        }

        /// <summary>
        /// Test that when the viewmodel is created with an existing plan assignment, that adding another
        /// plan to the selected grid returns 2 plans
        /// </summary>
        [Test]
        public void Initial_PlanAddNewPlan()
        {
            // Set up a list of rows that a user has selected that contain a previously plan assignment
            var mainPlanModel = new PlanAssignmentViewModel();
            mainPlanModel.SelectedClusterScheme = _clusterScheme;
            mainPlanModel.SelectedProductGroup = ProductGroupInfo.FetchById(_selectedProductGroupId);

            // Get a list of selected plan assignments (in this case only 1 row)
            var tmpList = mainPlanModel.MasterAssignmentItemRows.Where(x => !String.IsNullOrEmpty(x.PlanName)).ToList();

            // Pass this to the constructor
            InitialiseViewmodel(_selectedPlanGroup, tmpList);

            // And check that the assigned plan already assigned is in the "selected" list of planograms
            Assert.AreEqual(1, this.TestModel.AssignedPlans.Count(), "The initial number of selected plans should be 1");

            // Select another different plan
            this.TestModel.SelectedAvailablePlans.Add(this.TestModel.AvailablePlanogramInfos[2]);
            Assert.IsTrue(TestModel.AddSelectedCommand.CanExecute());

            // Add it to the selected list of plans
            TestModel.AddSelectedCommand.Execute();

            // And "click" the Ok button
            TestModel.OKCommand.Execute();

            // Check there are still 2 selected plans in the viewmodel
            Assert.AreEqual(2, this.TestModel.AssignedPlans.Count(), "The number of selected plans should be 2");
        }

        #endregion

        #region Helpers

        private void InitialiseViewmodel(PlanogramGroup selectedPlanogramGroup, IEnumerable<PlanAssignmentRowViewModel> initalPlansAssigned)
        {
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.ShowMessageMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseWindowMethod);
            
            PlanogramHierarchyViewModel hierarchyView = new PlanogramHierarchyViewModel();
            hierarchyView.FetchForCurrentEntity();

            Assert.AreEqual(3, hierarchyView.RootGroup.ChildViews.Count(), "Planogram groups incorrect.");

            this.TestModel = new SelectPlanogramViewModel("", base.EntityId, hierarchyView, selectedPlanogramGroup, initalPlansAssigned);
        }

        #endregion
    }
}
